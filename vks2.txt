SELECT barang.kdbrg
     , (SELECT ifnull((sum(kartustok.saldoawal)), 0) AS sa
        FROM
          kartustok
        WHERE
          kartustok.kdbrg = barang.kdbrg
          AND
          kartustok.tglkartustok BETWEEN '2014-11-' AND '2014-11-28'
        ORDER BY
          kartustok.kdbrg DESC
        LIMIT
          1) AS sa

     , (SELECT ifnull((sum(kartustok.jmlmasuk)), 0) AS jml
        FROM
          kartustok
        WHERE
          kartustok.kdbrg = barang.kdbrg
          AND
          kartustok.tglkartustok BETWEEN '2014-11-21' AND '2014-11-28'
        ORDER BY
          kartustok.kdbrg DESC
        LIMIT
          1) AS jmlmasuk

     , (SELECT ifnull((sum(kartustok.jmlkeluar)), 0) AS jml
        FROM
          kartustok
        WHERE
          kartustok.kdbrg = barang.kdbrg
          AND
          kartustok.tglkartustok BETWEEN '2014-11-21' AND '2014-11-28'
        ORDER BY
          kartustok.kdbrg DESC
        LIMIT
          1) AS jmlkeluar

     , (SELECT ifnull((sum(kartustok.saldoakhir)), 0) AS sa
        FROM
          kartustok
        WHERE
          kartustok.kdbrg = barang.kdbrg
          AND
          kartustok.tglkartustok BETWEEN '2014-11-21' AND '2014-11-28'
        ORDER BY
          kartustok.kdbrg DESC
        LIMIT
          1) AS sisa

     , (SELECT ifnull((sum(v_lapstokpersediaanv2.nbeli)), 0) AS nbli
        FROM
          v_lapstokpersediaanv2
        WHERE
          v_lapstokpersediaanv2.kdbrg = barang.kdbrg
          AND
          v_lapstokpersediaanv2.tglkartustok BETWEEN '2014-11-21' AND '2014-11-28'
        ORDER BY
          v_lapstokpersediaanv2.kdbrg DESC
        LIMIT
          1) AS nbli

     , (SELECT ifnull((sum(v_lapstokpersediaanv2.njual)), 0) AS njual
        FROM
          v_lapstokpersediaanv2
        WHERE
          v_lapstokpersediaanv2.kdbrg = barang.kdbrg
          AND
          v_lapstokpersediaanv2.tglkartustok BETWEEN '2014-11-21' AND '2014-11-28'
        ORDER BY
          v_lapstokpersediaanv2.kdbrg DESC
        LIMIT
          1) AS njual

FROM
  barang
LEFT JOIN kartustok kstok
ON kstok.kdbrg = barang.kdbrg
GROUP BY
  barang.kdbrg