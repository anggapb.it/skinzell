<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename = laporan.xls");
header("Pragma: no-cache");
header("Expires: 0");

function cleanData($str) { 

	if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) 
	{ 
	return "'$str"; 
	} else {
	return "$str"; 
	}

}

$settonumber = array('uobat', 'upemeriksaan', 'utindakan', 'uimun', 'ulab', 'ulain', 'uracik', 'udiskon', 'ucc', 'utotal', 'ujumlah', 'total' );

echo ("\n");
echo ("\n");
 
foreach($fieldname as $field) {
  
	echo $field. "\t"; 
	
} 

echo ("\n");	

foreach ($eksport as $row) {

    foreach ($row as $key => $value) {
			if(in_array($key, $settonumber) || (is_numeric($value) && $key != 'notelp'))
				echo number_format($value,0,',',''). "\t";
			else	
				echo  $value. "\t";
    }
	
	echo ("\n");
		
}
?>                                                                 
