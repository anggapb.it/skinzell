<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename = $filename.xls");
header("Pragma: no-cache");
header("Expires: 0");


function cleanData($str) { 

	if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) 
	{ 
		return "'$str"; 
	} else {
		return "$str"; 
	}

}

echo ("\n");
echo $filter."\n"; 
echo ("\n");
 
foreach($fieldname as $field) {
  
	echo $field. "\t"; 	
} 

echo ("\n");	

$total_keseluruhan = 0;
foreach($eksport as $i=> $val){
	
	echo ($i+1) ."\t";
	echo cleanData($val->nmjnspelayanan) ."\t";
	echo cleanData($val->noreg) ."\t";
	echo date_format(date_create($val->tglkuitansi), 'd-m-Y') ."\t";
	echo cleanData( $val->norm ) ."\t";
	echo cleanData( $val->nmpasien ) ."\t";
	echo cleanData( $val->atasnama ) ."\t";
	echo cleanData( $val->kditem ) ."\t";
	echo cleanData( $val->nmbrg ) ."\t";
	echo $val->hrgjual ."\t";
	echo number_format($val->qty, 0, ',', '') ."\t";
	echo cleanData( $val->nmsatuan ) ."\t";
	echo number_format($val->subtotal, 0, ',', '')  ."\t";
	
	echo ("\n");	
	
	$total_keseluruhan += $val->subtotal;
}
	echo " \t \t \t \t \t \t \t \t \t \t \t  Total \t {$total_keseluruhan}";	


?>                                                                 
