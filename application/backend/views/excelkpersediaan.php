<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename = kartupersediaan.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo ("\t\t\t\t\t\t Kartu Persediaan \n");
echo ("\t\t\t\t\t Tanggal : ". date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)));
echo ("\n");

/* foreach($fieldname as $field) {
	echo $field. "\t";
} */
#echo ("\n");

$no = 0;
$kdbrg = '';
$nmbrg = '';
foreach ($eksport as $row) {
	if($row->kdbrg == $kdbrg && $row->nmbrg == $nmbrg){
		$s_akhir = $s_akhir + $row->saldoawal + $row->jmlmasuk - $row->rbm - $row->jmlkeluar + $row->rbk;
		echo  $row->noref. "\t";
		echo  $row->tglkartustok. "\t";
		echo  $row->jamkartustok. "\t";
		echo  number_format($row->saldoawal,0,',',''). "\t";
		echo  number_format($row->jmlmasuk,0,',',''). "\t";
		echo  number_format($row->rbm,0,',',''). "\t";
		echo  number_format($row->jmlkeluar,0,',',''). "\t";
		echo  number_format($row->rbk,0,',',''). "\t";
		echo  number_format($s_akhir,0,',',''). "\t";
		echo  number_format($row->hrgbeli,0,',',''). "\t";
		echo  number_format($row->hrgjual,0,',',''). "\t";
		echo  number_format($row->nbelik,0,',',''). "\t";
		echo  number_format($row->njualk,0,',',''). "\t";
		
		$saldoawal += $row->saldoawal;
		$jmlmasuk += $row->jmlmasuk;
		$rbm += $row->rbm;
		$jmlkeluar += $row->jmlkeluar;
		$rbk += $row->rbk;
		$n_beli += $row->nbeli;
		$n_jual += $row->njual;
		$nb_rbk += $row->nbrbk;
		$nj_rbk += $row->njrbk;
		$n_beli_rbk = $n_beli - $nb_rbk;
		$n_jual_rbk = $n_jual - $nj_rbk;
		
	} else {
		$no++;
		if ($no > 1){
			echo "\t\t Total \t".number_format($saldoawal,0,',','')."\t".number_format($jmlmasuk,0,',','')."\t".number_format($rbm,0,',','')."\t".number_format($jmlkeluar,0,',','')."\t".number_format($rbk,0,',','')."\t".number_format($s_akhir,0,',','')."\t\t\t".number_format($n_beli_rbk,0,',','')."\t".number_format($n_jual_rbk,0,',','');
		}
		$s_akhir = 0;
		$saldoawal = 0;
		$jmlmasuk = 0;
		$rbm = 0;
		$jmlkeluar = 0;
		$rbk = 0;
		$n_beli = 0;
		$n_jual = 0;
		$nb_rbk = 0;
		$nj_rbk = 0;
		$n_beli_rbk = 0;
		$n_jual_rbk = 0;
		
		echo ("\n");
		echo ("\n");
		$kdbrg = $row->kdbrg;
		$nmbrg = $row->nmbrg;
		echo  $row->kdbrg. "\t";
		echo  $row->nmbrg. "\t";
		echo ("\n");
		foreach($fieldname as $field) {
			echo $field. "\t";
		}
		echo ("\n");		
		$s_akhir = $s_akhir + $row->saldoawal + $row->jmlmasuk - $row->rbm - $row->jmlkeluar + $row->rbk;
		
		echo  $row->noref. "\t";
		echo  $row->tglkartustok. "\t";
		echo  $row->jamkartustok. "\t";
		echo  number_format($row->saldoawal,0,',',''). "\t";
		echo  number_format($row->jmlmasuk,0,',',''). "\t";
		echo  number_format($row->rbm,0,',',''). "\t";
		echo  number_format($row->jmlkeluar,0,',',''). "\t";
		echo  number_format($row->rbk,0,',',''). "\t";
		echo  number_format($s_akhir,0,',',''). "\t";
		echo  number_format($row->hrgbeli,0,',',''). "\t";
		echo  number_format($row->hrgjual,0,',',''). "\t";
		echo  number_format($row->nbelik,0,',',''). "\t";
		echo  number_format($row->njualk,0,',',''). "\t";
		$saldoawal += $row->saldoawal;
		$jmlmasuk += $row->jmlmasuk;
		$rbm += $row->rbm;
		$jmlkeluar += $row->jmlkeluar;
		$rbk += $row->rbk;
		$n_beli += $row->nbeli;
		$n_jual += $row->njual;
		$nb_rbk += $row->nbrbk;
		$nj_rbk += $row->njrbk;
		$n_beli_rbk = $n_beli - $nb_rbk;
		$n_jual_rbk = $n_jual - $nj_rbk;
	}
	echo ("\n");
		
}
echo "\t\t Total \t".number_format($saldoawal,0,',','')."\t".number_format($jmlmasuk,0,',','')."\t".number_format($rbm,0,',','')."\t".number_format($jmlkeluar,0,',','')."\t".number_format($rbk,0,',','')."\t".number_format($s_akhir,0,',','')."\t\t\t".number_format($n_beli_rbk,0,',','')."\t".number_format($n_jual_rbk,0,',','');
?>