<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename = transaksifarmasi.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo ("\n\n");

echo ("\t Rincian Pemakaian Obat Pasien\n\n");

echo ("Nama \t". $nama);
echo ("\t\t Tanggal Masuk \t". $tglmasuk);
echo ("\n");

echo ("No. RM \t". $norm);
echo ("\t\t Tanggal Keluar \t". $tglkeluar);
echo ("\n\n");

echo ("No. Kuitansi \t". $noreg);
echo ("\t\t Ruangan \t". $ruangan);
echo ("\n\n");

foreach($fieldname as $field) {
	echo $field. "\t";
} 
echo ("\n");	

foreach ($eksport as $row) {
    foreach ($row as $key => $value) {
		echo  $value. "\t";
    }
	echo ("\n");
}
echo ("\n\n");
echo ("\t\tGrand Total \t\t". $total);
echo ("\n");
echo ("\t\tBandung\t". date('d/m/Y'));
?>