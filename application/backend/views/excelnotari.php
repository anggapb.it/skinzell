<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename = transaksifarmasi.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo ("\n\n");

echo ("\t\t KUITANSI \n\n");

echo ("No Kuitansi \t". $noreg);
echo ("\t\t\t Lama \t". $lamainap);
echo ("\n");

echo ("No. RM \t". $norm);
echo ("\t\t\t Unit \t". $unit);
echo ("\n");

echo ("\t\t\t\t Status \t". $status);
echo ("\n");

echo ("\t\t\t\t Tgl. Masuk \t". $tglreg);
echo ("\n");

echo ("\t\t\t\t Tgl. Keluar \t". $tglkeluar);
echo ("\n");

echo ("Sudah Terima Dari");
echo ("\n");

echo ("Nama Pasien ". $nama);
echo ("\n");

echo ("Untuk Pembayaran Biaya");
echo ("\n\n");

foreach($fieldname as $field) {
	echo $field. "\t";
}
echo ("\n");

$i=1;
foreach ($eksport as $row) {
	echo  $i ."\t";
    foreach ($row as $key => $value) {
		echo  $value. "\t";
    }
	$i++;
	echo ("\n");
}
if($jumobat > 0){
	echo ($i ."\t obat". "\t\t\t\t". $jumobat);
	echo ("\n");
}

echo ("\t\t\t Racik \t\t". $uangr);
echo ("\n");
echo ("\t\t\t Jumlah \t\t". $jumlah);
echo ("\n");
echo ("\t\t\t Diskon \t\t". $diskon);
echo ("\n");
echo ("\t\t\t Total \t\t". $total);
echo ("\n");
echo ("\t\t\t Deposit \t\t". $deposit);
echo ("\n");
echo ("\t\t\t Total Harus Dibayar \t\t". $bayar);
echo ("\n");
echo ("\t\t\t Sisa Deposit \t\t". $sisadp);
echo ("\n");

echo ("\n\n");

if($jumobat > 0){
	echo ("\t\t Detail Obat Farmasi \t\t");
	echo ("\n");

	foreach($fieldname1 as $field) {
		echo $field. "\t";
	}
	echo ("\n");

	$i=1;
	foreach ($eksport1 as $row) {
		echo  $i ."\t";
		foreach ($row as $key => $value) {
			echo  $value. "\t";
		}
		$i++;
		echo ("\n");
	}
	echo ("\t\t\t Total \t". $jumobat);
	echo ("\n");
}
?>