<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename = potong_gaji.xls");
header("Pragma: no-cache");
header("Expires: 0");

function cleanData($str) { 

	if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) 
	{ 
	return "'$str"; 
	} else {
	return "$str"; 
	}

}
echo ("\n");
echo ("\n");
echo ("Rawat Jalan / UGD / Rawat Inap \n");
echo ("\n");

 
foreach($fieldname as $field) {
  
	echo $field. "\t"; 
	
} 

echo ("\n");	

foreach ($eksport as $row) {

    foreach ($row as $key => $value) {
			if(is_numeric($value)){
				echo  number_format($value, 0, ',', ''). "\t";
			}else{
				echo  $value. "\t";
			}
    }
	
	echo ("\n");
			
}
	echo ("\n");	
	echo ("\t\t\t\t Total \t". $total_transaksi." \t". $dibayarpasien." \t". $potonggaji." \t". $jml_byr." \t". $sisa);
	
echo ("\n");
echo ("\n");
echo ("Pelayanan Tambahan / Farmasi Pasien Luar \n");
echo ("\n");

 
foreach($fieldname2 as $field) {
  
	echo $field. "\t"; 
	
} 

echo ("\n");	


foreach ($eksport2 as $row) {

    foreach ($row as $key => $value) {
			if(is_numeric($value)){
				echo  number_format($value, 0, ',', ''). "\t";
			}else{
				echo  $value. "\t";
			}
    }
	
	echo ("\n");
		
}
	echo ("\n");
	echo ("\t\t\t\t Total \t". $total_transaksi2." \t". $dibayarpasien2." \t". $potonggaji2." \t". $jml_byr2." \t". $sisa2);
	echo ("\n");
	echo ("\n");
	echo ("\t\t\t\t Total Semua \t". $total_all." \t". $dibayarpasien_all." \t". $potonggaji_all." \t". $jml_byr_all." \t". $sisa_all);
	
?>                                                                 
