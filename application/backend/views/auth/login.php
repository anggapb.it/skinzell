﻿<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> 	<html lang="en"> <!--<![endif]-->
<head>

	<!-- General Metas -->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	<!-- Force Latest IE rendering engine -->
	<title><?php echo NMKLINIK ?></title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
	
	<link rel="shortcut icon" href="<?php echo base_url(); ?>resources/web-images/favicon-new.png" />
	
	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>application/backend/views/auth/css/base.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>application/backend/views/auth/css/skeleton.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>application/backend/views/auth/css/layout.css">
	
</head>
<body>

	<!--<div class="notice">		
		<p class="warn"><img width="140" height="50" src="<?php echo base_url(); ?>resources/img/web-img/logoutc.png"  />
		</p>	
	</div>-->
	
	<!-- Primary Page Layout -->
	<div class="container">	
	</br></br></br>
		<center>
			<img src="<?php echo base_url(); ?>resources/web-images/logo-skinzell.png" alt="Skinzell">
		</center>

		<div class="form-bg">
			<form method="POST" id="frmLogin" action="<?php echo base_url(); ?>auth/rh_ext_login">
				<h2>&nbsp;</h2>
				<p><input type="text" placeholder="User ID" name="username" id="logUsername" autofocus></p>
				<p><input type="password" placeholder="Password" name="password" id="logPassword"></p>
				<label for="remember">
				  <font color="red" ><?php echo $msg; ?></font>
				  <!--<span>Remember me on this computer</span>-->
				</label>
				<button type="submit"></button>
			<form>
		</div>
		<font color="white"><center>&copy; Copyright 2019 <?php echo NMKLINIK ?>. All rights reserved.<br>
		Sistem Informasi Klinik</center></font>
	</div><!-- container -->
<!-- End Document -->
</body>
</html>