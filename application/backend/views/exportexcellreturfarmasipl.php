<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename = $filename.xls");
header("Pragma: no-cache");
header("Expires: 0");

function cleanData($str) { 

	if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) 
	{ 
		return "'$str"; 
	} else {
		return "$str"; 
	}

}

echo ("\n");
echo $filter."\n"; 
echo ("\n");
 
foreach($fieldname as $field) {
  
	echo $field. "\t"; 
	
} 

echo ("\n");	

$total_keseluruhan = 0;
foreach($eksport as $i=> $val){
	$kditem = '';
	$namabarang = '';
	$jmlbeli = '';
	$jmlretur = '';
	$tarif = '';
	$subtotal = '';
	$totalperpasien = 0;
	
	echo ($i+1) ."\t";
	echo $val->noreturfarmasi ."\t";
	echo date_format(date_create($val->tglreturfarmasi), 'd-m-Y') ."\t";
	echo $val->nmlengkap ."\t";
	echo $val->noreg ."\t";
	echo $val->norm ."\t";
	echo $val->atasnama ."\t";
	
	if(!empty($val->returfarmasi_detail['data'])){
		foreach($val->returfarmasi_detail['data'] as $idxdtl => $returfarmasi_detail){
			$totalperpasien += $returfarmasi_detail->subtotal;
			
			echo $returfarmasi_detail->kditem."\t";
			echo $returfarmasi_detail->nmbrg."\t";
			echo $returfarmasi_detail->qtyterima."\t";
			echo $returfarmasi_detail->jmlretur."\t";
			//echo cleanData($returfarmasi_detail->tarif)."\t";
			//echo cleanData($returfarmasi_detail->subtotal)."\t";
			echo $returfarmasi_detail->tarif."\t";
			echo $returfarmasi_detail->subtotal."\t";
			echo ("\n");
			echo " \t \t \t \t \t \t \t";
		}	
	}
	
	echo " \t \t \t \t Total Perpasien \t {$totalperpasien}";
	echo ("\n \n");	
	
	$total_keseluruhan += $totalperpasien;
}
	echo " \t \t \t \t \t \t \t \t \t \t \t Total \t {$total_keseluruhan}";
		


?>                                                                 
