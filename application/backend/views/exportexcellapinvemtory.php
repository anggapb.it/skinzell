<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename = $filename.xls");
header("Pragma: no-cache");
header("Expires: 0");

function cleanData($str) { 

	if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) 
	{ 
	return "'$str"; 
	} else {
	return "$str"; 
	}

}

echo ("\n");
echo $filter."\n"; 
echo ("\n");
 
foreach($fieldname as $field) {
  
	echo $field. "\t"; 
	
} 

echo ("\n");	

$nominals = array(
	'sa' => 0,
	'beli' => 0,
	'jmlretbeli' => 0,
	'jmltbl' => 0,
	'jmldistribusibrg' => 0,
	'jmldistribusimasukbrg' => 0,
	'jmlrkbb' => 0,
	'jmlrmbg' => 0,
	'jmlbhp' => 0,
	'jmljualbrg' => 0,
	'jmlretjualbrg' => 0,
	'sisa' => 0,
	'hrgbeli' => 0,
	'hrgjual' => 0,
	'nbli' => 0,
	'njual' => 0
	);

foreach ($eksport as $row) {

    foreach ($row as $key => $value) {
			if(isset($nominals[$key])){
				$nominals[$key] += $value;
			}

			if(is_numeric($value)){
				echo  number_format($value, 0, ',', ''). "\t";
			}else{
				echo  $value. "\t";
			}
    }
	
	echo ("\n");
		
}

//sum data
echo "\t Total \t";

foreach ($nominals as $key => $value) {
	echo  $value. "\t";
}


?>                                                                 
