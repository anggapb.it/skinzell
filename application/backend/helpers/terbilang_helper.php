<?php
function _convert_terbilang($number)
{
	$number = str_replace('.', '', $number);

	if ( ! is_numeric($number)) return 'data yang dimasukan bukan angka';

	$base    = array('nol', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
	$numeric = array('1000000000000000', '1000000000000', '1000000000000', 1000000000, 1000000, 1000, 100, 10, 1);
	$unit    = array('kuadriliun', 'triliun', 'biliun', 'milyar', 'juta', 'ribu', 'ratus', 'puluh', '');
	$str     = null;

	$i = 0;

	if ($number == 0)
	{
		$str = 'nol';
	}
	else
	{
		while ($number != 0)
		{
			$count = (int)($number / $numeric[$i]);

			if ($count >= 10)
			{
				$str .= convert_terbilang($count) . ' ' . $unit[$i] . ' ';
			}
			elseif ($count > 0 && $count < 10)
			{
				$str .= $base[$count] . ' ' . $unit[$i] . ' ';
			}

			$number -= $numeric[$i] * $count;

			$i++;
		}

		$str = preg_replace('/satu puluh (\w+)/i', '\1 belas', $str);
		$str = preg_replace('/satu (ribu|ratus|puluh|belas)/', 'se\1', $str);
		$str = preg_replace('/\s{2,}/', ' ', trim($str));
	}

	return $str;
}


function rp_satuan($angka,$debug){
	$terbilang = '';
	$a_str['1']="Satu";
	$a_str['2']="Dua";
	$a_str['3']="Tiga";
	$a_str['4']="Empat";
	$a_str['5']="Lima";
	$a_str['6']="Enam";
	$a_str['7']="Tujuh";
	$a_str['8']="Delapan";
	$a_str['9']="Sembilan";
	
	
	$panjang=strlen($angka);
	for ($b=0;$b<$panjang;$b++)
	{
		$a_bil[$b]=substr($angka,$panjang-$b-1,1);
	}
	
	if ($panjang>2)
	{
		if ($a_bil[2]=="1")
		{
			$terbilang=" Seratus";
		}
		else if ($a_bil[2]!="0")
		{
			$terbilang= " ".$a_str[$a_bil[2]]. " Ratus";
		}
	}

	if ($panjang>1)
	{
		if ($a_bil[1]=="1")
		{
			if ($a_bil[0]=="0")
			{
				$terbilang .=" Sepuluh";
			}
			else if ($a_bil[0]=="1")
			{
				$terbilang .=" Sebelas";
			}
			else 
			{
				$terbilang .=" ".$a_str[$a_bil[0]]." Belas";
			}
			return $terbilang;
		}
		else if ($a_bil[1]!="0")
		{
			$terbilang .=" ".$a_str[$a_bil[1]]." Puluh";
		}
	}
	
	if ($a_bil[0]!="0")
	{
		$terbilang .=" ".$a_str[$a_bil[0]];
	}
	return $terbilang;
}
	
function convert_terbilang($angka,$debug = false){
	$terbilang = '';
	
	$angka = str_replace(".",",",$angka);
	
	list ($angka) = explode(",",$angka);
	$panjang=strlen($angka);
	for ($b=0;$b<$panjang;$b++)
	{
		$myindex=$panjang-$b-1;
		$a_bil[$b]=substr($angka,$myindex,1);
	}
	if ($panjang>9)
	{
		$bil=$a_bil[9];
		if ($panjang>10)
		{
			$bil=$a_bil[10].$bil;
		}

		if ($panjang>11)
		{
			$bil=$a_bil[11].$bil;
		}
		if ($bil!="" && $bil!="000")
		{
			$terbilang .= rp_satuan($bil,$debug)." milyar";
		}
		
	}

	if ($panjang>6)
	{
		$bil=$a_bil[6];
		if ($panjang>7)
		{
			$bil=$a_bil[7].$bil;
		}

		if ($panjang>8)
		{
			$bil=$a_bil[8].$bil;
		}
		if ($bil!="" && $bil!="000")
		{
			$terbilang .= rp_satuan($bil,$debug)." Juta";
		}
		
	}
	
	if ($panjang>3)
	{
		$bil=$a_bil[3];
		if ($panjang>4)
		{
			$bil=$a_bil[4].$bil;
		}

		if ($panjang>5)
		{
			$bil=$a_bil[5].$bil;
		}
		if ($bil!="" && $bil!="000")
		{
			$terbilang .= rp_satuan($bil,$debug)." Ribu";
		}
		
	}

	$bil=$a_bil[0];
	if ($panjang>1)
	{
		$bil=$a_bil[1].$bil;
	}

	if ($panjang>2)
	{
		$bil=$a_bil[2].$bil;
	}
	//die($bil);
	if ($bil!="" && $bil!="000")
	{
		$terbilang .= rp_satuan($bil,$debug);
	}
	return trim($terbilang);
}

