<?php
// bulan aktif penjurnalan
class Bulanaktif extends Model{
  var $bulan;
  var $key_bulanaktif = 'jurnal_bulan_aktif';
  var $key_awaljurnal = 'bulan_awal_jurnal';
  function __construct (){
    parent::Model();
    $this->load->model(array('generalsetting'));

    $this->bulan = array(
      '1'  => 'Januari',     '2'  => 'Februari',   '3'  => 'Maret',
      '4'  => 'April',       '5'  => 'Mei',        '6'  => 'Juni',
      '7'  => 'Juli',        '8'  => 'Agustus',    '9'  => 'September',
      '10' => 'Oktober',     '11' => 'November',   '12' => 'Desember',
    );
  }

  function get_bulan_aktif()
  {
    $bulan = $this->generalsetting->get_setting_item($this->key_bulanaktif, 'bulan');
    return $bulan;
  }

  function get_tahun_aktif()
  {
    $tahun = $this->generalsetting->get_setting_item($this->key_bulanaktif, 'tahun');
    return $tahun;
  }

  function get_bulan_awal_jurnal()
  {
    $bulan = $this->generalsetting->get_setting_item($this->key_awaljurnal, 'bulan');
    return $bulan;
  }

  function get_tahun_awal_jurnal()
  {
    $tahun = $this->generalsetting->get_setting_item($this->key_awaljurnal, 'tahun');
    return $tahun;
  }

  function tutup_buku_bulanan()
  {
    $bulan = $this->get_bulan_aktif();
    $tahun = $this->get_tahun_aktif();

    if($bulan != '12'){
      $bulanbaru = str_pad(($bulan + 1), 2, "0", STR_PAD_LEFT);
      $update = array('tahun' => $tahun, 'bulan' => $bulanbaru);
    }else{
      $update = array('tahun' => ($tahun + 1), 'bulan' => '01');
    }

    $this->generalsetting->set_setting($this->key_bulanaktif, $update);
  }

}