<?php 

class Access extends Model {
    public function __construct()
    {
        parent::Model();
        //$this->forsecure();
    }
	
	function forsecure($session)
    {
        if (!$session->logged_in)
        {
            $data['title'] = 'User Login';
			if($session->userdata('login_res') == 0){
				$data['msg'] = 'Masukkan User ID dan Password';
			}
			if($session->userdata('login_res') == 1){
				$data['msg'] = 'User tidak aktif!';
			}
			if($session->userdata('login_res') == 2){
				$data['msg'] = 'User atau password salah!';
			}
            $this->load->view('auth/login', $data);
        }
		//return true;
    }
}
