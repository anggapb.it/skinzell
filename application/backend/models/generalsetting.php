<?php 

class Generalsetting extends Model {
  public function __construct()
  {
      parent::Model();
  }

  public function get_setting($setting_key = '', $isJson = false)
  {
    $return = '';
    $getsetting = $this->db->get_where('general_setting', array('setting_key' => $setting_key))->row_array();
    
    if(!empty($getsetting)){
      $return = $getsetting['setting_value'];
      if($isJson)
        $return = json_decode($return, true);
    }

    return $return;
  }

  //get_setting_item berfungsi mengambil item dari setting_value yang telah di encode oleh json
  public function get_setting_item($setting_key = '', $itemkey = '')
  {
    $data = $this->get_setting($setting_key, true);
    if(isset($data) && is_array($data) && isset($data[$itemkey]))
      return $data[$itemkey];
    else
      return '';
  }

  public function set_setting($setting_key = '', $setting_value = '')
  {
    if(is_array($setting_value))
      $setting_value = json_encode($setting_value);

    $check = $this->db->get_where('general_setting', array('setting_key' => $setting_key))->num_rows();
    if($check > 0)
    {
      $this->db->where('setting_key', $setting_key);
      $this->db->update('general_setting', array('setting_value' => $setting_value));
    }else{
      $this->db->insert('general_setting', array('setting_value' => $setting_value));
    }
  }

}
