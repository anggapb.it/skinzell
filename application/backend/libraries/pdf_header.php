<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'third_party/tcpdf/tcpdf'.EXT;

class Pdf_header extends TCPDF{
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = TRUE, $encoding = 'UTF-8', $diskcache = FALSE, $pdfa = FALSE) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }
    
	public function addHeaderParams($datahead){
		$this->title = $datahead['title'];
		$this->tahun = $datahead['tahun'];
    }
    
    public function Header() {
        // Logo
        $image_file = base_url().'resources/img/report.png';
        $this->Image($image_file, 8, 8, 23, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        $text1 = "RSIA HARAPAN BUNDA BANDUNG";
        $text2 = "dr. Bambang Suhardijanto, SpOg";
        $text3 = "Pluto Raya Blok C Margahayu Raya Bandung";          
        $text4 = "Telp. (022) 7506490 Fax (022) 7514712";          
        $y_text1 = 35;
        $x_text1 = 8;
            
        $y_text2 = 35;
        $x_text2 = 16;

        $y_text3 = 35;
        $x_text3 = 23;

        $y_text4 = 35;
        $x_text4 = 29;
        
        $this->SetFont('helvetica', 'B', 20);     
        $this->Text($y_text1,$x_text1,$text1,false);
        $this->SetFont('helvetica', 'B', 16);
        $this->Text($y_text2,$x_text2,$text2,false);
        $this->SetFont('helvetica', '', 13);
        $this->Text($y_text3,$x_text3,$text3,false);
        $this->SetFont('helvetica', '', 13);
        $this->Text($y_text4,$x_text4,$text4,false);
        
        $style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
        $style2 = array('width' => 0.7, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));

        $this->Line(10, 36, 317, 36, $style);
        $this->Line(10, 37, 317, 37, $style2);
		
		$this->Ln();
		$this->Ln();
		$this->SetFont('helvetica', 'B', 12);
		$this->Cell(0, 0, $this->title, 0, 1, 'C', 0, '', 0);
		$this->Cell(0, 0, $this->tahun, 0, 1, 'L', 0, '', 0);
		$this->Ln();
		$this->Ln();
    }
    
}