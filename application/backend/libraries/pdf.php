<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'third_party/tcpdf/tcpdf'.EXT;

class Pdf extends TCPDF{
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = TRUE, $encoding = 'UTF-8', $diskcache = FALSE, $pdfa = FALSE) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }
    
    
    public function Header() {
        // Logo
        $image_file = base_url().'resources/img/logo.png';
        $this->Image($image_file, 13, 15, 18, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        $text1 = NMKLINIK_KAPITAL;
        $text2 = ALAMAT_SATU;
        $text3 = ALAMAT_DUA;          
        $text4 = "";          
        $y_text1 = 35;
        $x_text1 = 8;
            
        $y_text2 = 35;
        $x_text2 = 16;

        $y_text3 = 35;
        $x_text3 = 23;

        $y_text4 = 35;
        $x_text4 = 29;
        
        $this->SetFont('helvetica', 'B', 20);     
        $this->Text($y_text1,$x_text1,$text1,false);
        $this->SetFont('helvetica', '', 13);
        $this->Text($y_text2,$x_text2,$text2,false);
        $this->SetFont('helvetica', '', 11);
        $this->Text($y_text3,$x_text3,$text3,false);
        $this->SetFont('helvetica', '', 13);
        $this->Text($y_text4,$x_text4,$text4,false);
        
        $style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
        $style2 = array('width' => 0.7, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));

        $this->Line(10, 35, 200, 35, $style);
        $this->Line(10, 36, 200, 36, $style2);
    }
	
	function headerNota(){
        //$text1 = "<div style='letter-spacing:5;'>RSIA HARAPAN BUNDA</div>";
        //$text2 = "dr. Bambang Suhardijanto, SpOg";
        //$text3 = "Pluto Raya Blok C Margahayu Raya Bandung";          
        //$text4 = "Telp. (022) 7506490 Fax (022) 7514712";          
        $y_text1 = 5;
        $x_text1 = 5;
            
        $y_text2 = 5;
        $x_text2 = 10;

        $y_text3 = 5;
        $x_text3 = 14;

        $y_text4 = 5;
        $x_text4 = 18;
        
        $this->SetFont('helvetica', 'B', 14);
	$this->writeHTMLCell(0, 10, $y_text1, $x_text1, '<div style="letter-spacing:5;">'.NMKLINIK.'</div>');
        //$this->Text($y_text1,$x_text1,$text1,false);
        $this->SetFont('helvetica', '', 9);
	$this->writeHTMLCell(0, 10, $y_text2, $x_text2, '<div style="letter-spacing:5;">'.ALAMAT_SATU.'</div>');
        //$this->Text($y_text2,$x_text2,$text2,false);
        $this->SetFont('helvetica', '',8);
	$this->writeHTMLCell(0, 10, $y_text3, $x_text3, '<div style="letter-spacing:3;">'.ALAMAT_DUA.'</div>');
        //$this->Text($y_text3,$x_text3,$text3,false);
     /*   $this->SetFont('helvetica', '', 10);
	$this->writeHTMLCell(0, 10, $y_text4, $x_text4, '<div style="letter-spacing:3;">Telp. 0813-1651-8088</div>');
    */    //$this->Text($y_text4,$x_text4,$text4,false);
        
        $style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));

        $this->Line(5, 20, 145, 20, $style);
	}
    
	function headerNotaRI(){
        $y_text1 = 5;
        $x_text1 = 5;
            
        $y_text2 = 5;
        $x_text2 = 10;

        $y_text3 = 5;
        $x_text3 = 14;

        $y_text4 = 5;
        $x_text4 = 18;
        
        $this->SetFont('helvetica', 'B', 14);
		$this->writeHTMLCell(0, 10, $y_text1, $x_text1, '<div style="letter-spacing:5;">RSIA HARAPAN BUNDA</div>');
	
        $this->SetFont('helvetica', 'B', 10);
		$this->writeHTMLCell(0, 10, $y_text2, $x_text2, '<div style="letter-spacing:5;">dr. Bambang Suhardijanto, SpOg</div>');
		
        $this->SetFont('helvetica', '', 10);
		$this->writeHTMLCell(0, 10, $y_text3, $x_text3, '<div style="letter-spacing:3;">Pluto Raya Blok C Margahayu Raya Bandung</div>');
		
        $this->SetFont('helvetica', '', 10);
		$this->writeHTMLCell(0, 10, $y_text4, $x_text4, '<div style="letter-spacing:3;">Telp. (022) 7506490 Fax (022) 7514712</div>');
        
        $style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));

        $this->Line(5, 23, 390, 23, $style);
	}
    
}