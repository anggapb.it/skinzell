<?php

class Jstpenyakit_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_jstpenyakit(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("jstpenyakit");
        
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select("*");
        $this->db->from("jstpenyakit");
        
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	/*
	function cekStatus(){
        $noreg = $this->input->post("noreg");
        $idpenyakit = $this->input->post("idpenyakit");
		
		$q = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg));
		$reg = $q->row_array();
		
		$q = $this->db->getwhere('v_registrasi',array('norm'=>$reg['norm']));
		$reg = $q->row_array();
		$status['stat'] = 'Baru';
		
		foreach($reg AS $value){
			$q = $this->db->getwhere('kodifikasi',array('noreg'=>$value));
			$reg = $q->row_array();
			if($reg){
				$q = $this->db->getwhere('kodifikasidet',array('idkodifikasi'=>$reg['idkodifikasi'], 'idpenyakit'=>$idpenyakit));
				$reg = $q->num_rows();
				if($reg > 0){
					$status['stat'] = 'Lama';
				}
			}
		}
		
		echo json_encode($status);
	}
	*/
	
	/*
	function cekStatus(){
        $noreg = $this->input->post("noreg");
        $idpenyakit = $this->input->post("idpenyakit");
		
		$q = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg));
		$reg = $q->row_array();
		
		$q = $this->db->getwhere('v_registrasi',array('norm'=>$reg['norm']));
		$reg = $q->row_array();
		$status['stat'] = 'Baru';
		
		foreach($reg AS $value){
			$q = $this->db->getwhere('kodifikasi',array('noreg'=>$value));
			$reg = $q->row_array();
			if($reg){
				$q = $this->db->getwhere('kodifikasidet',array('idkodifikasi'=>$reg['idkodifikasi'], 'idpenyakit'=>$idpenyakit));
				$reg = $q->num_rows();
				if($reg > 0){
					$status['stat'] = 'Lama';
				}
			}
		}
		
		echo json_encode($status);
	}*/
	
	
	function cekStatus(){
        $noreg = $this->input->post("noreg");
        $idpenyakit = $this->input->post("idpenyakit");
		$where = array(
			'noreg' => $noreg,
			'idpenyakit' => $idpenyakit,
		);
		
		$status['stat'] = 'Baru';
		
		$get = $this->db->get_where('v_kodifikasidet', $where);
		$count = $get->num_rows();
		if($count > 0){
			$status['stat'] = 'Lama';
		}else{
			$status['stat'] = 'Baru';
		}
		echo json_encode($status);
		die;
	}
	
	
}
