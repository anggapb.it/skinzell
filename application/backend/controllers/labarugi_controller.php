<?php

class Labarugi_controller extends Controller {

  public function __construct()
  {
    parent::Controller();
    $this->load->library('pdf_lap');
    $this->load->library('session');
    $this->load->library('lib_phpexcel');


  }
  
  function get_pendapatan(){
    $tglawal    = $this->input->post("tglawal");
    $tglakhir   = $this->input->post("tglakhir");
    
    if(empty($tglawal) && empty($tglakhir)){
      $build_array = array ("success"=>true, "results"=>0, "data"=> array());
      echo json_encode($build_array);
      die;
    }

    $this->db->order_by('kdakun', 'ASC');
    $dafakun = $this->db->get('v_pendapatan')->result_array();

    foreach($dafakun as $idx => $akun)
    {
        $q = "SELECT getTotalPendapatan('".$tglawal."', '".$tglakhir."', '".$akun['idakun']."') as nominal;";
        $query  = $this->db->query($q)->row_array();
        $dafakun[$idx]['nominal'] = $query['nominal'];
    }

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }
  
  function get_biaya(){
    $tglawal    = $this->input->post("tglawal");
    $tglakhir   = $this->input->post("tglakhir");
    
    if(empty($tglawal) && empty($tglakhir)){
      $build_array = array ("success"=>true, "results"=>0, "data"=> array());
      echo json_encode($build_array);
      die;
    }

    $this->db->order_by('kdakun', 'ASC');
    $dafakun = $this->db->get('v_biaya')->result_array();

    foreach($dafakun as $idx => $akun)
    {
        $q = "SELECT getTotalBiaya('".$tglawal."', '".$tglakhir."', '".$akun['idakun']."') as nominal;";
        $query  = $this->db->query($q)->row_array();
        $dafakun[$idx]['nominal'] = $query['nominal'];
    }

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }

  function lap_labarugi($tglawal = '', $tglakhir = ''){
    $subject_file = 'Periode : '. date("d/m/Y", strtotime($tglawal)). ' - '. date("d/m/Y", strtotime($tglakhir));

    //akun pendapatan
    $this->db->order_by('kdakun', 'ASC');
    $dafakun_pendapatan = $this->db->get('v_pendapatan')->result_array();
    foreach($dafakun_pendapatan as $idx => $akun)
    {
      $query  = $this->db->query("SELECT getTotalPendapatan('".$tglawal."', '".$tglakhir."', '".$akun['idakun']."') as nominal")->row_array();
      $dafakun_pendapatan[$idx]['nominal'] = $query['nominal'];
    }

    //akun biaya
    $this->db->order_by('kdakun', 'ASC');
    $dafakun_biaya = $this->db->get('v_biaya')->result_array();
    foreach($dafakun_biaya as $idx => $akun)
    {
      $query  = $this->db->query("SELECT getTotalBiaya('".$tglawal."', '".$tglakhir."', '".$akun['idakun']."') as nominal")->row_array();
      $dafakun_biaya[$idx]['nominal'] = $query['nominal'];
    }

    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
        'font'  => array('bold'  => true)
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    $summarygrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT),
        'font'  => array('bold'  => true)
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Laporan Laba - Rugi")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Laporan Laba - Rugi'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:D1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Laporan Laba - Rugi');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:D2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(70);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
    
    $active_row_left = 4;
    if(!empty($dafakun_pendapatan)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:D{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Pendapatan');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Kode')
           ->setCellValue("C{$active_row_left}", 'Nama Akun')
           ->setCellValue("D{$active_row_left}", 'Nominal');
      $active_row_left += 1; 
      
      $total_pendapatan = 0;
      foreach($dafakun_pendapatan as $idx => $dakun){
            
        $total_pendapatan += $dakun['nominal']; 
        //set content grid
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($contentgrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row_left}", $dakun['kdakun'])
             ->setCellValue("C{$active_row_left}", $dakun['nmakun'])
             ->setCellValue("D{$active_row_left}", $dakun['nominal']);
        $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:C{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Total')
           ->setCellValue("D{$active_row_left}", $total_pendapatan);
      $active_row_left += 2; // set active row to the next row
     
    }

    if(!empty($dafakun_biaya)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:D{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Biaya');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Kode')
           ->setCellValue("C{$active_row_left}", 'Nama Akun')
           ->setCellValue("D{$active_row_left}", 'Nominal');
      $active_row_left += 1; 
      
      $total_biaya = 0;
      foreach($dafakun_biaya as $idx => $dakun){
            
        $total_biaya += $dakun['nominal']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("B{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("C{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("D{$active_row_left}", $dakun['nominal']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:C{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Total')
           ->setCellValue("D{$active_row_left}", $total_biaya);
      $active_row_left += 2; // set active row to the next row
     
    }

    //set grand total total
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:C{$active_row_left}"); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($summarygrid);
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B{$active_row_left}", 'Total Laba / Rugi')
         ->setCellValue("D{$active_row_left}", ($total_pendapatan - $total_biaya));
   

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'laba_rugi_'. date("d-m-Y", strtotime($tglawal)). '_'. date("d_m_Y", strtotime($tglakhir));

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

}
