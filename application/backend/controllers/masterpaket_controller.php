<?php

class Masterpaket_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
		
	function get_masterpaket(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_masterpaket");
		$this->db->where('v_masterpaket.tar_idtarifpaket !=','null');
        $this->db->order_by('v_masterpaket.idtarifpaket');
        
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        /* $this->db->select('tarifpaket.*, klstarif.nmklstarif, jhirarki.nmjnshirarki, jpelayanan.nmjnspelayanan');
        $this->db->from('tarifpaket');
		$this->db->join('klstarif',
                'klstarif.idklstarif = tarifpaket.idklstarif', 'left');
		$this->db->join('jhirarki',
                'jhirarki.idjnshirarki = tarifpaket.idjnshirarki', 'left');
		$this->db->join('jpelayanan',
                'jpelayanan.idjnspelayanan = tarifpaket.idjnspelayanan', 'left'); */
		$this->db->select("*");
		$this->db->from("v_masterpaket");
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_masterpaket(){     
		$where['idtarifpaket'] = $_POST['idtarifpaket'];
		$where['idklstarif'] = $_POST['idklstarif'];
		$del = $this->rhlib->deleteRecord('tarifpaket',$where);
        return $del;
    }
			
	function insert_masterpaket(){
		$dataArray = $this->getFieldsAndValues();
		$this->rhlib->insertRecord('tarifpaket',$dataArray);
        return $dataArray;
    }
	
	function update_masterpaket(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('idtarifpaket', $_POST['idtarifpaket']);
		$this->db->update('tarifpaket', $dataArray);
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){		
		
		$idtf = $this->getNoMpaket();
		
		$tarifpaket =  $_POST['tar_idtarifpaket']; 
		
		if($tarifpaket=="") $tarifpaket=0;
		
		$dataArray = array(
             'idtarifpaket'		=> ($_POST['idtarifpaket']) ? $_POST['idtarifpaket']: $idtf, //$_POST['idtarifpaket'],
             'idklstarif'		=> $_POST['idklstarif'],
			 'nmpaket'			=> $_POST['nmpaket'],
             'idjnspelayanan'	=> $_POST['idjnspelayanan'],
			 'idjnshirarki'		=> $_POST['idjnshirarki'],
			 'idstatus'			=> $_POST['idstatus'],
			 'tar_idtarifpaket'	=> $this->id_tarifpaket('nmpaket',$tarifpaket)
        );
		return $dataArray;
		
	}
	
	function getNoMpaket(){
		$q = "SELECT getotomasterpaket() as kode;";
        $query  = $this->db->query($q);
        $kode= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $kode=$row->kode;
        }
        return $kode;
	}
	
	function get_parent_masterpaket(){ 
		$query = $this->db->getwhere('tarifpaket',array('idjnshirarki'=>'0'));
		$parent = $query->result();
		$ttl = count($parent);
        $arrMasterpaket = array ("success"=>true,"results"=>$ttl,"data"=>$parent);
		echo json_encode($arrMasterpaket);
    }
	
	function id_tarifpaket($where, $val){
		$query = $this->db->getwhere('tarifpaket',array($where=>$val));
		$id = $query->row_array();
		return  $id['idtarifpaket'];
    }
	
	function getNmpaket(){
		$query = $this->db->getwhere('tarifpaket',array('idtarifpaket'=>$_POST['tar_idtarifpaket']));
		$nm = $query->row_array();
		echo json_encode($nm['nmpaket']);
    }
	
}
