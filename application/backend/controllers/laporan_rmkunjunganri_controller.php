<?php 
class Laporan_rmkunjunganri_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_laprmkunjunganri(){

		$tglawal = $this->input->post("tglawal");
        $tglakhir = $this->input->post("tglakhir");
		
		/* $q = "SELECT `registrasidet`.tglmasuk AS tglmasuk
					 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
					 , `registrasidet`.`noreg` AS `noreg`
					 , `registrasi`.norm
					 , if(((SELECT min(rd.idregdet) AS stp
							FROM
							  registrasidet rd
							LEFT JOIN registrasi r
							ON r.noreg = rd.noreg
							LEFT JOIN nota n
							ON n.idregdet = rd.idregdet
							LEFT JOIN kuitansi k
							ON k.nokuitansi = n.nokuitansi
							WHERE
							  r.norm = `pasien`.norm
							  AND
							  r.idjnspelayanan = '2'
							  AND
							  rd.userbatal IS NULL
							  AND
							  k.idstkuitansi = 1
					   ) = registrasidet.idregdet), 'Baru', 'Lama') AS stpasien
					 , `pasien`.`nmpasien` AS `nmpasien`
					 , `registrasidet`.`umurtahun` AS `umurtahun`
					 , `pasien`.`idjnskelamin` AS `idjnskelamin`
					 , `jkelamin`.`nmjnskelamin` AS `nmjnskelamin`
					 , `pasien`.alamat AS alamat
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , `pasien`.notelp AS notelp
					 , (
					   CASE
					   WHEN isnull(`registrasidet`.`tglkeluar`) THEN
						 ((to_days(curdate()) - to_days(`registrasidet`.`tglmasuk`)) + 1)
					   WHEN (`registrasidet`.`tglmasuk` = `registrasidet`.`tglkeluar`) THEN
						 '1'
					   ELSE
						 ((to_days(`registrasidet`.`tglkeluar`) - to_days(`registrasidet`.`tglmasuk`)) + 1)
					   END) AS lamarawat
					 , `registrasidet`.`idbagian` AS `idbagian`
					 , `bagian`.`nmbagian` AS `nmbagian`
					 , `registrasidet`.idklstarif AS idklstarif
					 , `klstarif`.nmklstarif AS nmklstarif
					 , `registrasidet`.`iddokter` AS `iddokter`
					 , `dokter`.`nmdoktergelar` AS `nmdoktergelar`
					 , `kodifikasi`.idjnskasus AS idjnskasus
					 , `jkasus`.nmjnskasus AS nmjnskasus
					 , `registrasidet`.`idcaradatang` AS `idcaradatang`
					 , `caradatang`.`nmcaradatang` AS `nmcaradatang`
					 , `registrasidet`.`idstkeluar` AS `idstkeluar`
					 , `stkeluar`.`nmstkeluar` AS `nmstkeluar`
					 , `registrasidet`.`idcarakeluar` AS `idcarakeluar`
					 , `jcarakeluar`.`nmcarakeluar` AS `nmcarakeluar`
					 , `registrasi`.idpenjamin AS idpenjamin
					 , penjamin.nmpenjamin AS nmpenjamin

				FROM
				  ((((((((((((((`registrasidet`
				LEFT JOIN `registrasi`
				ON ((`registrasidet`.`noreg` = `registrasi`.`noreg`)))
				LEFT JOIN `penjamin`
				ON ((`registrasi`.idpenjamin = `penjamin`.idpenjamin)))
				LEFT JOIN `pasien`
				ON ((`registrasi`.`norm` = `pasien`.`norm`)))
				LEFT JOIN `jkelamin`
				ON ((`pasien`.`idjnskelamin` = `jkelamin`.`idjnskelamin`)))
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = `pasien`.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN `bagian`
				ON ((`registrasidet`.`idbagian` = `bagian`.`idbagian`)))
				LEFT JOIN `dokter`
				ON ((`registrasidet`.`iddokter` = `dokter`.`iddokter`)))
				LEFT JOIN `kodifikasi`
				ON ((`kodifikasi`.`noreg` = `registrasi`.`noreg`)))
				LEFT JOIN `jkasus`
				ON ((`kodifikasi`.idjnskasus = jkasus.idjnskasus)))
				LEFT JOIN `klstarif`
				ON ((`registrasidet`.idklstarif = `klstarif`.idklstarif)))
				LEFT JOIN `stkeluar`
				ON ((`registrasidet`.idstkeluar = `stkeluar`.idstkeluar)))
				LEFT JOIN `caradatang`
				ON ((`registrasidet`.idcaradatang = `caradatang`.idcaradatang)))
				LEFT JOIN jcarakeluar
				ON ((`registrasidet`.idcarakeluar = jcarakeluar.idcarakeluar)))
				LEFT JOIN `nota`
				ON ((`registrasidet`.`idregdet` = `nota`.`idregdet`)))
				LEFT JOIN `kuitansi`
				ON ((`kuitansi`.`nokuitansi` = `nota`.`nokuitansi`)))
				WHERE
				  (((`registrasidet`.`idcarakeluar` <> 6)
				  OR isnull(`registrasidet`.`idcarakeluar`))
				  AND isnull(`registrasidet`.`tglbatal`))
				  AND `kuitansi`.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'
				  AND `registrasi`.idjnspelayanan = 2
				  AND `kuitansi`.idstkuitansi = 1
				GROUP BY
				  `registrasi`.`noreg`
				, `registrasidet`.`idbagian`

				UNION

				SELECT `skl`.`tglkelahiran` AS tglmasuk
					 , `kuitansi`.tglkuitansi AS tglkuitansi
					 , `skl`.`noreganak` AS `noreganak`
					 , `skl`.`normanak` AS `normanak`
					 , 'Baru' AS stpasien
					 , `pasien`.`nmpasien` AS `nmbayi`
					 , (
					   CASE
					   WHEN isnull(skl.`tglkelahiran`) THEN
						 ((to_days(curdate()) - to_days(skl.`tglkelahiran`)) + 1)
					   WHEN (skl.`tglkelahiran` = kuitansi.tglkuitansi) THEN
						 '1'
					   ELSE
						 ((to_days(kuitansi.tglkuitansi) - to_days(skl.`tglkelahiran`)) + 1)
					   END) AS `umurtahun`
					 , `skl`.`idjnskelamin` AS `idjnskelamin`
					 , `jkelamin`.`nmjnskelamin` AS `nmjnskelamin`
					 , `pasien`.alamat AS alamat
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , `pasien`.notelp AS notelp
					 , (
					   CASE
					   WHEN isnull(skl.`tglkelahiran`) THEN
						 ((to_days(curdate()) - to_days(skl.`tglkelahiran`)) + 1)
					   WHEN (skl.`tglkelahiran` = kuitansi.tglkuitansi) THEN
						 '1'
					   ELSE
						 ((to_days(kuitansi.tglkuitansi) - to_days(skl.`tglkelahiran`)) + 1)
					   END) AS lamarawat
					 , 46 AS `idbagian`
					 , 'Perinatologi' AS `nmbagian`
					 , 5 AS idklstarif
					 , 'All Room' AS nmklstarif
					 , `skl`.`iddokter` AS `iddokter`
					 , `dokter`.`nmdoktergelar` AS `nmdoktergelar`
					 , `kodifikasi`.idjnskasus AS idjnskasus
					 , `jkasus`.nmjnskasus AS nmjnskasus
					 , `registrasidet`.`idcaradatang` AS `idcaradatang`
					 , `caradatang`.`nmcaradatang` AS `nmcaradatang`
					 , NULL AS `idstkeluar`
					 , NULL AS `nmstkeluar`
					 , NULL AS `idcarakeluar`
					 , NULL AS `nmcarakeluar`
					 , `registrasi`.idpenjamin AS idpenjamin
					 , penjamin.nmpenjamin AS nmpenjamin
				FROM
				  (((((((((((`skl`
				LEFT JOIN `dokter`
				ON ((`skl`.`iddokter` = `dokter`.`iddokter`)))
				LEFT JOIN `kodifikasi`
				ON ((`kodifikasi`.`noreg` = `skl`.`noreganak`)))
				LEFT JOIN `jkasus`
				ON ((`kodifikasi`.idjnskasus = jkasus.idjnskasus)))
				LEFT JOIN `jkelamin`
				ON ((`skl`.`idjnskelamin` = `jkelamin`.`idjnskelamin`)))
				LEFT JOIN `pasien`
				ON ((`pasien`.`norm` = `skl`.`normanak`)))
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = `pasien`.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN registrasi
				ON ((registrasi.noreg = `skl`.noreg)))
				LEFT JOIN `penjamin`
				ON ((`registrasi`.idpenjamin = `penjamin`.idpenjamin)))
				LEFT JOIN registrasidet
				ON ((`registrasidet`.noreg = registrasi.noreg)))
				LEFT JOIN `caradatang`
				ON ((`registrasidet`.idcaradatang = `caradatang`.idcaradatang)))
				LEFT JOIN nota
				ON ((nota.idregdet = registrasidet.idregdet)))
				LEFT JOIN kuitansi
				ON ((kuitansi.nokuitansi = nota.nokuitansi)))
				WHERE
				  (`skl`.`noreganak` IS NOT NULL)
				  AND `kuitansi`.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'
				  AND `kuitansi`.idstkuitansi = 1
				  AND registrasidet.userbatal IS NULL
				GROUP BY
				  registrasi.noreg"; */
		//$query = $this->db->query($q);
		
		$this->db->select("*");
        $this->db->from("v_lap_rmkunjunganri");
		$this->db->order_by("v_lap_rmkunjunganri.tglkuitansi ASC");
		
		if(!empty($tglawal) && !empty($tglakhir)){
			$this->db->where('tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		$query = $this->db->get();
		
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
		
		$ttl = count($data);
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
		$noreg = '';
        $datax = array();
		$zx = 0;
		
		foreach($data as $x=>$val){
			if($noreg == $val->noreg){
			//var_dump($val->senin);
				$zx--;
				$datax[$zx]->nmpenyakiteng .= '<br>'.$val->nmpenyakiteng;
				$zx++;
			} else {
				$datax[$zx] = $val;
				$zx++;
			}
			$noreg = $val->noreg;
			
		}
		
        if($ttl>0){
            $build_array["data"]=$datax;
        }
		
		echo json_encode($build_array);
	}

}
