<?php

class Timjasamedis_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }	
	
	function get_jasamedis(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");  
		
		$tglawal 				= $this->input->post("tglawal");  
		$tglakhir 				= $this->input->post("tglakhir");  
		
		$this->db->select("*");
        $this->db->from("v_jasamedis");
		if($tglakhir) $this->db->where("tglreg BETWEEN '".$tglawal."' AND '".$tglakhir."'");     
        		
		if($query !=""){
			$fields2 = '["noreg","tglreg","norm","nmpasien","nmbagian","nmpelayanan","tarifjasa","status"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
                
        /* if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        } */
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
		$tglawal 				= $this->input->post("tglawal");  
		$tglakhir 				= $this->input->post("tglakhir");  
		
		$this->db->select("*");
        $this->db->from("v_jasamedis");
		if($tglakhir) $this->db->where("tglreg BETWEEN '".$tglawal."' AND '".$tglakhir."'"); 
    		
		if($query !=""){
			$fields2 = '["noreg","tglreg","norm","nmpasien","nmbagian","nmpelayanan","tarifjasa","status"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_vdokter(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_dokter");
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_dokter');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_jtm(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$jtm    				= $this->input->post("idjtm");
		
		$this->db->select("*");
		$this->db->from("v_jtmdet");
		if($jtm){
			$this->db->where('idjtm',$jtm);
		}else{
			$this->db->where('idjtm',null);
		}
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_jtmdet');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function hapus(){
		$where['idjtm'] = $_POST['idjtm'];
		
		$del = $this->rhlib->deleteRecord('jtmdet',$where);
		$del = $this->rhlib->deleteRecord('jtm',$where);
        return $del;
	}
	
	
	
	function simpan(){
		$dataArray = $this->getFieldJasaMedis();
		$jtm = $this->db->insert('jtm',$dataArray);
		$ambilID = $this->db->query("select idjtm from jtm ORDER BY idjtm DESC Limit 1");
		$diambil = $ambilID->result_array();
	/* 	$query = $this->db->getwhere('jtmdet',array('idjtm'=>$diambil['idjtm']));
		
		$jtmdet = $query->row_array();
		if($query->num_rows() == 0){ */
			$jtmdet = $this->simpanJasaMedisDet($dataArray);
	//	}
		
		if($jtmdet){
			$ret = $dataArray;
		}else{
			$ret=false;
		}
		return $ret;
	}
	
	function simpanJasaMedisDet($jtmdet){
//	$where['idjtm'] = $jtmdet['idjtm'];

		$k = array('[',']','"');
		$r = str_replace($k, '', $_POST['arrdok']);
		$b = explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldJasaMedisDet($vale[0], $vale[1], $vale[2], $vale[3],$vale[4]);
			$z = $this->db->insert('jtmdet',$dataArray);
		}
		if($z){
			$ret = $dataArray;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	function getFieldJasaMedis(){
		$dataArray = array(
			//'idjtm' => $_POST['idjtm'],
			'idnotadet' => $_POST['idnotadet'],
			'tgljtm' => $_POST['tgljtm'],
			'userid' => $_POST['userid'],
			'catatan' => $_POST['catatan'],
			'js' => $_POST['js'],
		);
		return $dataArray;
	}
	
	function getFieldJasaMedisDet($val1,$val2,$val3,$val4,$val5){

	$ambilID = $this->db->query("Select idjtm from jtm ORDER BY idjtm DESC limit 1");
	
	foreach($ambilID->result() as $row)
		$idjtm = $row->idjtm;
		$dataArray = array(
			'iddokter' => $val1,
			'idstdoktertim' => $this->searchId('idstdoktertim','stdoktertim','nmstdoktertim',$val2),
			'idjtm' => $idjtm,
			'jumlah'=>$val3,
			'diskon'=> $val4,
			'tarifjm'=> $val5

		);
		return $dataArray;
	}

	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function hapusdet(){
		$where['iddokter'] = $_POST['iddokter'];
		$where['idjtm'] = $_POST['idjtm'];
		
		$del = $this->rhlib->deleteRecord('jtmdet',$where);
        return $del;
	}
	
	function simpanupdatedet(){
		$dataArray = array(
			'iddokter' => $_POST['iddokter'],
			'idjtm' => $_POST['idjtm'],
			'jumlah' => 0,
			'diskon' => 0,
			'tarifjm' => 0,
		);
		$jtm = $this->db->insert('jtmdet',$dataArray);

		if($jtm){
			$ret = true;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	function simpanupdatedetedit(){
		$dataArray = array(
			'idstdoktertim' => ($_POST['idstdoktertim']) ? $_POST['idstdoktertim']:null,
			'jumlah' => $_POST['jumlah'],
			'diskon' => $_POST['diskon'],
			'tarifjm' => $_POST['tarifjm'],
		);
		$this->db->where('idjtm', $_POST['idjtm']);
		$this->db->where('iddokter', $_POST['iddokter']);
		$this->db->update('jtmdet', $dataArray);

		$this->simpanupdatedetedithead();
		
		return $this->db->affected_rows();
	}
	
	function simpanupdatedetedithead(){
		$dataArray = array(
			'js' => $_POST['js'],
		);
		$this->db->where('idjtm', $_POST['idjtm']);
		$this->db->update('jtm', $dataArray);

		return $this->db->affected_rows();
	}

}