<?php

class Vtarifall_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_vtarifall(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cidbagian             	= $this->input->post("cidbagian");
        $cklstarif            	= $this->input->post("cklstarif");
        $this->db->select("*");
        $this->db->from("v_tarifall");
		$this->db->where('idstatus', 1);
		$this->db->where('left(kditem, 1) <> "B"');
		$this->db->order_by('nmitem');
		if($cidbagian != 0){
			$idbagian = array($cidbagian, '');
			$this->db->where_in('idbagian', $idbagian);
		} else {
			$this->db->where('idbagian', $cidbagian);
		}
		
		if($cklstarif){
			$klstarif = array($cklstarif, '', 5);
			$this->db->where_in('klstarif', $klstarif);
		}
		
		//var_dump($cklstarif);
		//exit;
		
        if($query !=""){
            /*$k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
			for($i=0;$i<$c;$i++){*/
				$arrquery = explode(' ', $query);
				foreach($arrquery AS $val){
					$this->db->like('nmitem', $val);
					//$d[$b[$i]]=$val;
				}
			//}
        }
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cidbagian             	= $this->input->post("cidbagian");
        $cklstarif            	= $this->input->post("cklstarif");
      
        $this->db->select("*");
        $this->db->from("v_tarifall");
		$this->db->where('idstatus', 1);
		$this->db->where('left(kditem, 1) <> "B"');
		$this->db->order_by('kditem');
		if($cidbagian != 0){
			$idbagian = array($cidbagian, '');
			$this->db->where_in('idbagian', $idbagian);
		} else {
			$this->db->where('idbagian', $cidbagian);
		}
			
		if($cklstarif){
			$klstarif = array($cklstarif, '', 5);
			$this->db->where_in('klstarif', $klstarif);
		}
		
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nmitem', $val);
			}
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
}
