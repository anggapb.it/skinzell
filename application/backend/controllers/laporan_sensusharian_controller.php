 <?php 
class Laporan_sensusharian_controller extends Controller{
		function __construct(){
			parent::Controller();  
		}
	
	function get_bagian_all() {
		$q = $this->db->query("SELECT * FROM bagian WHERE idjnspelayanan=2");
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_pasien_masuk(){
		$opttgl = ($this->input->post("cbxtgl")=='true') ? "=":"<>";
		$optjam = ($this->input->post("cbxjam")=='true') ? "=":"<>";
		$optruangan = ($this->input->post("cbxruangan")=='true') ? "=":"<>";
		
		$valtgl = ($this->input->post("cbxtgl")=='true') ? $this->input->post("tgl"):"-";
		$valjam = ($this->input->post("cbxjam")=='true') ? $this->input->post("jam"):"-";
		$valruangan = ($this->input->post("cbxruangan")=='true') ? $this->input->post("ruangan"):"-";
		
		$q = "SELECT registrasi.norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglmasuk ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jammasuk,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
		if ($this->input->post("cbxtgl")=='false' && $this->input->post("cbxjam")=='false' && $this->input->post("cbxruangan")=='false') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_pindahan(){
		$opttgl = ($this->input->post("cbxtgl")=='true') ? "=":"<>";
		$optjam = ($this->input->post("cbxjam")=='true') ? "=":"<>";
		$optruangan = ($this->input->post("cbxruangan")=='true') ? "=":"<>";
		
		$valtgl = ($this->input->post("cbxtgl")=='true') ? $this->input->post("tgl"):"-";
		$valjam = ($this->input->post("cbxjam")=='true') ? $this->input->post("jam"):"-";
		$valruangan = ($this->input->post("cbxruangan")=='true') ? $this->input->post("ruangan"):"-";
		
		$q = "SELECT * FROM (SELECT registrasi.norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
			 , (SELECT concat('Pindahan dari kamar ',b.nmbagian) from registrasidet r 
				 LEFT JOIN bagian b ON r.idbagian = b.idbagian
				  where r.noreg = registrasidet.noreg and r.idregdet < registrasidet.idregdet
				  order by r.idregdet desc  limit 1) as keterangan
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglmasuk ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jammasuk,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg) a where keterangan is not null";
		
		if ($this->input->post("cbxtgl")=='false' && $this->input->post("cbxjam")=='false' && $this->input->post("cbxruangan")=='false') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_dipindahkan(){
		$opttgl = ($this->input->post("cbxtgl")=='true') ? "=":"<>";
		$optjam = ($this->input->post("cbxjam")=='true') ? "=":"<>";
		$optruangan = ($this->input->post("cbxruangan")=='true') ? "=":"<>";
		
		$valtgl = ($this->input->post("cbxtgl")=='true') ? $this->input->post("tgl"):"-";
		$valjam = ($this->input->post("cbxjam")=='true') ? $this->input->post("jam"):"-";
		$valruangan = ($this->input->post("cbxruangan")=='true') ? $this->input->post("ruangan"):"-";
		
		$q = "SELECT * FROM (SELECT registrasi.norm
			 , registrasidet.noreg
			 , registrasidet.tglkeluar
			 , time_format(registrasidet.jamkeluar,'%H:%i') as jamkeluar
			 , pasien.nmpasien
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
			 , (SELECT concat('Dipindahkan ke kamar ',b.nmbagian) from registrasidet r 
				 LEFT JOIN bagian b ON r.idbagian = b.idbagian
				  where r.noreg = registrasidet.noreg and r.idregdet > registrasidet.idregdet
				  order by r.idregdet desc  limit 1) as keterangan
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglkeluar ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jamkeluar,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg) a where keterangan is not null";
		
		if ($this->input->post("cbxtgl")=='false' && $this->input->post("cbxjam")=='false' && $this->input->post("cbxruangan")=='false') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_keluar_hidup(){
		$opttgl = ($this->input->post("cbxtgl")=='true') ? "=":"<>";
		$optjam = ($this->input->post("cbxjam")=='true') ? "=":"<>";
		$optruangan = ($this->input->post("cbxruangan")=='true') ? "=":"<>";
		
		$valtgl = ($this->input->post("cbxtgl")=='true') ? $this->input->post("tgl"):"-";
		$valjam = ($this->input->post("cbxjam")=='true') ? $this->input->post("jam"):"-";
		$valruangan = ($this->input->post("cbxruangan")=='true') ? $this->input->post("ruangan"):"-";
		
		$q = "SELECT registrasi.norm
			 , registrasidet.noreg
			 , registrasidet.tglkeluar
			 , time_format(registrasidet.jamkeluar,'%H:%i') as jamkeluar
			 , pasien.nmpasien
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 1
		  AND registrasidet.tglkeluar ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jamkeluar,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
		if ($this->input->post("cbxtgl")=='false' && $this->input->post("cbxjam")=='false' && $this->input->post("cbxruangan")=='false') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_meninggal_kurang_48_jam(){
		$opttgl = ($this->input->post("cbxtgl")=='true') ? "=":"<>";
		$optjam = ($this->input->post("cbxjam")=='true') ? "=":"<>";
		$optruangan = ($this->input->post("cbxruangan")=='true') ? "=":"<>";
		
		$valtgl = ($this->input->post("cbxtgl")=='true') ? $this->input->post("tgl"):"-";
		$valjam = ($this->input->post("cbxjam")=='true') ? $this->input->post("jam"):"-";
		$valruangan = ($this->input->post("cbxruangan")=='true') ? $this->input->post("ruangan"):"-";
		
		$q = "SELECT registrasi.norm
			 , registrasidet.noreg
			 , kodifikasi.tglmeninggal
			 , time_format(kodifikasi.jammeninggal,'%H:%i') as jammeninggal
			 , pasien.nmpasien
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN kodifikasi
		ON registrasi.noreg = kodifikasi.noreg
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 2
		  AND kodifikasi.tglmeninggal ".$opttgl." '".$valtgl."' 
		  AND time_format(kodifikasi.jammeninggal,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
		if ($this->input->post("cbxtgl")=='false' && $this->input->post("cbxjam")=='false' && $this->input->post("cbxruangan")=='false') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_meninggal_lebih_sm_dgn_48_jam(){
		$opttgl = ($this->input->post("cbxtgl")=='true') ? "=":"<>";
		$optjam = ($this->input->post("cbxjam")=='true') ? "=":"<>";
		$optruangan = ($this->input->post("cbxruangan")=='true') ? "=":"<>";
		
		$valtgl = ($this->input->post("cbxtgl")=='true') ? $this->input->post("tgl"):"-";
		$valjam = ($this->input->post("cbxjam")=='true') ? $this->input->post("jam"):"-";
		$valruangan = ($this->input->post("cbxruangan")=='true') ? $this->input->post("ruangan"):"-";
		
		$q = "SELECT registrasi.norm
			 , registrasidet.noreg
			 , kodifikasi.tglmeninggal
			 , time_format(kodifikasi.jammeninggal,'%H:%i') as jammeninggal
			 , pasien.nmpasien
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN kodifikasi
		ON registrasi.noreg = kodifikasi.noreg
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 3
		  AND kodifikasi.tglmeninggal ".$opttgl." '".$valtgl."' 
		  AND time_format(kodifikasi.jammeninggal,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
		if ($this->input->post("cbxtgl")=='false' && $this->input->post("cbxjam")=='false' && $this->input->post("cbxruangan")=='false') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_hari_ini(){
		$opttgl = ($this->input->post("cbxtgl")=='true') ? "<=":"<>";
		$optjam = ($this->input->post("cbxjam")=='true') ? "=":"<>";
		$optruangan = ($this->input->post("cbxruangan")=='true') ? "=":"<>";
		
		$valtgl = ($this->input->post("cbxtgl")=='true') ? $this->input->post("tgl"):"-";
		$valjam = ($this->input->post("cbxjam")=='true') ? $this->input->post("jam"):"-";
		$valruangan = ($this->input->post("cbxruangan")=='true') ? $this->input->post("ruangan"):"-";
		
		$start = $this->input->post("start");
        $limit = $this->input->post("limit");
		
		if ($start==null){
            $start = 0;
			$limit = 20;
		}
		
		$q = "SELECT registrasi.norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasidet.tglkeluar is null
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglmasuk ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jammasuk,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.tglmasuk desc";
		
		if ($this->input->post("cbxtgl")=='false' && $this->input->post("cbxjam")=='false' && $this->input->post("cbxruangan")=='false') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q." LIMIT ".$start.", ".$limit);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$queryall  = $this->db->query($q);
			
			$ttl = $queryall->num_rows();
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
		echo json_encode($build_array);
	}

}
