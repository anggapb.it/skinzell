 <?php 
class Laporan_registrasi_controller extends Controller{
		function __construct(){
			parent::__construct();      
		}
	
	function numrowregpasien(){
		$fields				    = $this->input->post("fields");
        $keyword				= $this->input->post("keyword");
		$checkdate				= $this->input->post("checkdate");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		
        $this->db->select("*");
		$this->db->from("v_lap_reg_pasien");
		if ($checkdate=="true") {
			$this->db->where('`tgldaftar` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
    
       if ($keyword) {
			$this->db->or_like($fields, $keyword);
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }

	function get_laporan_registrasi($items){
        
        $fields				    = $this->input->post("fields");
        $keyword				= $this->input->post("keyword");
		$checkdate				= $this->input->post("checkdate");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		
		$start                  = $this->input->post("start");
		$limit                  = $this->input->post("limit");
		
		if ($items=="lapregdaftarpasien") {
			$this->db->select("*");
			$this->db->from("v_lap_reg_pasien");
			if ($checkdate=="true") {
				$this->db->where('`tgldaftar` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			}
		} else if ($items=="lapregrawatjalan") {
			$this->db->select("*");
			$this->db->from("v_lap_reg_rj");
			$this->db->where('`tglreg` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('userbatal IS NULL');
		} else if ($items=="lapregugd") {
			$this->db->select("*");
			$this->db->from("v_lap_reg_ugd");
			$this->db->where('`tglreg` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('userbatal IS NULL');
		} else if ($items=="lapregrawatinap") {
			$this->db->select("*");
			$this->db->from("v_lap_reg_ri");
			$this->db->where('`tglreg` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('userbatal IS NULL');
		}
		
		if ($keyword) {
			$this->db->or_like($fields, $keyword);
		}
		
		if ($items=="lapregdaftarpasien") {
			if ($start!=null){
				$this->db->limit($limit,$start);
			}else{
				$this->db->limit(25,0);
			}
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = ($items=="lapregdaftarpasien") ? $this->numrowregpasien():count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}

}
