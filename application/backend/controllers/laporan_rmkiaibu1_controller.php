<?php 
class Laporan_rmkiaibu1_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_laprmkiaibu1(){

		$tglawal = $this->input->post("tglawal");
        $tglakhir = $this->input->post("tglakhir");
		
		$q = "SELECT rd.idregdet
					 , n.nonota
					 , k.tglkuitansi
					 , r.noreg
					 , trim(LEADING '0' FROM `p`.`norm`) AS `norm`
					 , if(((SELECT min(registrasidet.idregdet) AS stp
							FROM
							  registrasidet
							LEFT JOIN registrasi
							ON registrasi.noreg = registrasidet.noreg
							LEFT JOIN nota
							ON nota.idregdet = registrasidet.idregdet
							LEFT JOIN kuitansi
							ON kuitansi.nokuitansi = nota.nokuitansi
							WHERE
							  registrasi.norm = p.norm
							  AND
							  registrasi.idjnspelayanan = '1'
							  AND
							  registrasidet.userbatal IS NULL
							  AND
							  kuitansi.idstkuitansi = 1
					   ) = rd.idregdet), 'Baru', 'Lama') AS stpasien
					 , p.nmpasien
					 , r.gravidag
					 , if(r.gravidap <> '0', r.gravidap, NULL) as gravidap
					 , if(r.gravidaa <> '0', r.gravidaa, NULL) as gravidaa
					 , r.gravidai AS usiakehamilan
					 , r.kunjhamil AS kunkehamilan
					 , pel.nmpelayanan
					 , nd.kditem
					 , (SELECT group_concat(pelayanan.nmpelayanan SEPARATOR ',<br>') AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						LEFT JOIN pelayanan
						ON pelayanan.kdpelayanan = notadet.kditem
						WHERE
						  registrasidet.idregdet = rd.idregdet
						  AND
						  notadet.kditem = 'T000000012'
					   ) AS imtt
					 , (SELECT group_concat(pelayanan.nmpelayanan SEPARATOR ',<br>') AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						LEFT JOIN pelayanan
						ON pelayanan.kdpelayanan = notadet.kditem
						WHERE
						  registrasidet.idregdet = rd.idregdet
						  AND
						  notadet.kditem IN ('T000000660', 'T000000661', 'T000000769')
					   ) AS labhcv
					 , (SELECT group_concat(pelayanan.nmpelayanan SEPARATOR ',<br>') AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						LEFT JOIN pelayanan
						ON pelayanan.kdpelayanan = notadet.kditem
						WHERE
						  registrasidet.idregdet = rd.idregdet
						  AND
						  notadet.kditem = 'T000000676'
					   ) AS labvdrl
					 , (SELECT group_concat(pelayanan.nmpelayanan SEPARATOR ',<br>') AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						LEFT JOIN pelayanan
						ON pelayanan.kdpelayanan = notadet.kditem
						WHERE
						  registrasidet.idregdet = rd.idregdet
						  AND
						  notadet.kditem IN ('T000000881', 'T000000911')
					   ) AS iud
					 , (SELECT group_concat(penyakit.nmpenyakit SEPARATOR ',<br>') AS nmpenyakiteng
						FROM
						  kodifikasidet
						LEFT JOIN kodifikasi
						ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
						LEFT JOIN registrasi
						ON registrasi.noreg = kodifikasi.noreg
						LEFT JOIN penyakit
						ON penyakit.idpenyakit = kodifikasidet.idpenyakit
						WHERE
						  registrasi.noreg = r.noreg
					   ) AS nmpenyakiteng
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , cdatang.nmcaradatang AS nmcaradatang
					 , r.idpenjamin
					 , pj.nmpenjamin

				FROM
				  registrasi r
				LEFT JOIN registrasidet rd
				ON r.noreg = rd.noreg
				LEFT JOIN caradatang cdatang
				ON cdatang.idcaradatang = rd.idcaradatang
				LEFT JOIN pasien p
				ON p.norm = r.norm
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = p.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN penjamin pj
				ON pj.idpenjamin = r.idpenjamin
				LEFT JOIN nota n
				ON n.idregdet = rd.idregdet
				LEFT JOIN notadet nd
				ON nd.nonota = n.nonota
				LEFT JOIN pelayanan pel
				ON pel.kdpelayanan = nd.kditem
				LEFT JOIN kuitansi k
				ON k.nokuitansi = n.nokuitansi
				WHERE
				  r.idjnspelayanan = 1
				  AND
				  rd.userbatal IS NULL
				  AND
				  k.idstkuitansi = 1
				  AND
				  rd.idbagian = 3
				  AND
				  nd.kditem LIKE '%T%'
				  AND
				  k.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'
				GROUP BY
				  r.noreg
				ORDER BY
				  r.noreg";
		$query = $this->db->query($q);
		
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
		
		$ttl = count($data);
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
        if($ttl>0){
            $build_array["data"]=$data;
        }
		
		echo json_encode($build_array);
	}

}
