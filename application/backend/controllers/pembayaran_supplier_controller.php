<?php 

class Pembayaran_Supplier_Controller extends Controller {
	public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function retValOrNull($val){
		//especially for combo & looukup with no item selected
		$val = ($val=='')? null : $val;
		return $val;
	}
	
	function get_po_supp(){
	
		$this->db->select("*");
        $this->db->from("v_bayar_supplier");
        $this->db->order_by("v_bayar_supplier.nopo DESC");			
			
		if ($this->input->post("cbxstbayar") == 'true'){
			if ($this->input->post("status") != 'Semua'){
				$this->db->where('stlunas =', $this->input->post("status"));
			}
		}
		
		if ($this->input->post("cbxsearch") == 'true'){
			$this->db->or_like($this->input->post("key"), $this->input->post("value"));
		}
		
		if ($this->input->post("cbxbeli") == 'true'){
			$this->db->where("date(tglpo) between '". $this->input->post("tglbeli1") ."' and '". $this->input->post("tglbeli2")."'" );
		}
		
		if ($this->input->post("cbxjthtempo") == 'true'){
			$this->db->where("date(tgljatuhtempo) between '". $this->input->post("jthtempo1") ."' and '". $this->input->post("jthtempo2")."'" );
		}

		if ($this->input->post("cbxpembayaran") == 'true'){
			$this->db->where("date(tglbayar) between '". $this->input->post("pembayaran1") ."' and '". $this->input->post("pembayaran2")."'" );
		}

		/* if ($this->input->post("start")!=null){
            $this->db->limit($this->input->post("limit"),$this->input->post("start"));
        } else {
            $this->db->limit(25,0);
        } */
	
		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_bayardet(){
		$q = "SELECT b.*,j.nmjnspembayaran FROM bayardet b
				LEFT JOIN jpembayaran j ON j.idjnspembayaran = b.idjnspembayaran
				WHERE b.nobayar='".$this->input->post("nobayar")."'";
			
		$this->rhlib->jsonFromQuery($q);
	}
	
	function get_bayarhistori(){
		$q = "select bayar.*,pengguna.nmlengkap,sum(bayardet.nominal) as jumlah from bayar
				  left join bayardet on bayar.nobayar = bayardet.nobayar 
				  left join pengguna on bayar.userid = pengguna.userid 
				  WHERE bayar.nopo='".$this->input->post("nopo")."' group by bayar.nobayar";
			
		$this->rhlib->jsonFromQuery($q);
	}
	
	function getNoBayar(){
		date_default_timezone_set('Asia/Jakarta');
		$datenow = date("Y-m-d");
		$q = "SELECT getOtoNobayar('".$datenow."') as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
	
	function appkeu(){
		$sql =	$this->db->query("select * from setting where idklpsetting = '14'");
		$num = $sql->num_rows();
			if($num>0){
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);			
			}else{
				return 0;
			} 
	
	}
	
	function simpan_update() {
		$nobayar = $this->input->post("nobayar") ? $this->input->post("nobayar"):$this->getNoBayar();
		$return;
		if (!$this->input->post("nobayar")) { // JIKA BARU
			$dataArray = array(
				'nobayar' => $nobayar,
				'tglbayar' => $this->retValOrNull($this->input->post("tglbayar")),
				'nopo' => $this->retValOrNull($this->input->post("nopo")),
				'userid' => $this->retValOrNull($this->input->post("userid")),
				'approval' => $this->retValOrNull($this->input->post("app1")),
				'catatan' => $this->retValOrNull($this->input->post("catatan")),

			);
			
			$ret = $this->rhlib->insertRecord('bayar',$dataArray);
			if ($ret) {
				$det = $this->simpan_bayar_det($nobayar);
				
				if ($det) {
					$return["nobayar"]=$nobayar;
					$return["success"]=true;
					$return["message"]="Simpan Data Berhasil";
          
          //tambah data ke jurnal
          //$this->db->query("CALL JURN_save_ps (?,?)",array($nobayar,$this->session->userdata['user_id']));
          
				} else {
					$return["nobayar"]=null;
					$return["success"]=false;
					$return["message"]="Simpan Data Detail gagal";
				}
			} else {
				$return["nobayar"]=null;
				$return["success"]=false;
				$return["message"]="Simpan Data gagal";
			}
		
		} else { // JIKA LAMA
		
			$dataArray = array(
				'nobayar' => $nobayar,
				'tglbayar' => $this->retValOrNull($this->input->post("tglbayar")),
				'nopo' => $this->retValOrNull($this->input->post("nopo")),
				'userid' => $this->retValOrNull($this->input->post("userid")),
				'approval' => $this->retValOrNull($this->input->post("app1")),
				'catatan' => $this->retValOrNull($this->input->post("catatan")),

			);

			//UPDATE
			$this->db->where('nobayar', $nobayar);
			$this->db->update('bayar', $dataArray);
			
			//if($this->db->affected_rows()){
				$det = $this->simpan_bayar_det($nobayar);
				
				if ($det) {
					$return["nobayar"]=$nobayar;
					$return["success"]=true;
					$return["message"]="Update Data Berhasil";
				} else {
					$return["nobayar"]=null;
					$return["success"]=false;
					$return["message"]="Update Data Detail gagal";
				}
			/* } else {
				$return["nobayar"]=null;
				$return["success"]=false;
				$return["message"]="Update Data gagal";
			} */

		
		}
		
		echo json_encode($return);
    }
	
	function simpan_bayar_det($nobayar){      // ISTRA
    $par=$this->input->post("par");

    $this->db->trans_begin();
    $rows = explode(";",$par);
    $row_count = count($rows);
    for($ri=0;$ri<$row_count;$ri++){
      $rows2 = explode("&",$rows[$ri]);
			$rows2[3] = $this->retValOrNull($rows2[3]);
			$rows2[4] = $this->retValOrNull($rows2[4]);
					
			$this->db->query("CALL SP_simpan_bayar_det (?,?,?,?,?)",
			array($nobayar,$rows2[1],$rows2[2],$rows2[3],$rows2[4]));
     
    }
        
    if ($this->db->trans_status() === FALSE)
    {
      $this->db->trans_rollback();
			return false;
    }
    else
    {
      $this->db->trans_commit();
			return true;
    }
  }
	
	function delete_bayar(){     
		$where['nobayar'] = $this->input->post("nobayar");
		$del = $this->rhlib->deleteRecord('bayar',$where);
        return $del;
    }
	
	function get_detbeli(){

		$nopo = $this->input->post("nopo");
		$q = $this->db->query("select * from v_podet_supplier where nopo='".$nopo."'");
		
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

					
		$ppnn = 0;
		$s1 = 0;
		$subtotal = 0;
		$hbelisatuankcl = 0;
		$hj1 = 0;
		$hj2 = 0;
		$hjual = 0;
					
		foreach($data as $row) {
					
						$ppn = ($row->ppn==10.00) ? 0.1:0;
						
						//untuk memnentukan subtotal
						$s1 = (($row->hrgbeli * ($row->qty - $row->qtyretur))-$row->diskonrp);
						$subtotal = ($s1 + ($s1*$ppn));
						
						//untuk memnentukan harga belisatuan kecil
						$hbelisatuankcl = ($row->qty - $row->qtyretur==0) ? 0:($subtotal / (($row->qty - $row->qtyretur) * $row->rasio));
						
						//untuk memnentukan hrgjual
						$hj1 = $row->hargajual;
						$hj2 = ($hj1 + ($hj1 * $ppn));				
						$hjual = ($hj2 + ($hj2 *($row->margin / 100)));	
						
						array_push($build_array["data"],array(
							'nopo'=>$row->nopo,
							'kdbrg'=>$row->kdbrg,
							'nmbrg'=>$row->nmbrg,
							'idsatuanbsr'=>$row->idsatuanbsr,
							'nmsatuanbsr'=>$row->nmsatuanbsr,
							'qty'=>$row->qty,
							'qtyretur'=>$row->qtyretur,
							'qtyb'=>$row->qtyb,
							'hrgbeli'=>$row->hrgbeli,
							'diskon'=>($row->qty - $row->qtyretur==0) ? 0:$row->diskon,
							'tamppn'=>$row->ppn,
							'ppn'=>($row->ppn==10.00) ? '1':'0',
							'hargajual1'=>$row->hargajual1,
							'diskonrp'=>($row->qty - $row->qtyretur==0) ? 0:$row->diskonrp,
							'margin'=>$row->margin,
							'subtotal'=>($row->qty - $row->qtyretur==0) ? 0:($s1 + ($s1 * $ppn)),
							'subtotaltemp'=>$row->subtotal,
							'hsubtotal'=>$row->hsubtotal,
							'htsubtotal'=>$row->htsubtotal,
							'hargajual'=>$row->hargajual1,
							'hargajualtemp'=>$row->hargajualtemp,
							'rasio'=>$row->rasio,
							'hrgbelikcl'=>$hbelisatuankcl,
							'tglpo'=>$row->tglpo,
							'nopp'=>$row->nopp,
							
						));
		}  
		
        echo json_encode($build_array);
    }
	
	
}
