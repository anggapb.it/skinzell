<?php

class Tpumum_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_tpumum(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $_POST["key"]; 
		
		$this->db->select("*");
		$this->db->from("v_tarif");		
		$this->db->order_by('v_tarif.kdpelayanan');
		
		$where = array();
        //$where['v_tarif.idpenjamin']= '1';      
        $where['v_tarif.idklstarif']= $_POST['klstarif'];      
        
       if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){	
        
		$this->db->select("*");
		$this->db->from("v_tarif");
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_tpumum(){ 
		//$where['idpenjamin'] 	= '1';
		$where['kdpelayanan'] 	= $_POST['kdpelayanan'];
		$where['idklstarif'] 	= $_POST['idklstarif'];
		$del = $this->rhlib->deleteRecord('tarif',$where);
        return $del;
    }
		
	function insert_tpumum(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('tarif',$dataArray);
        return $ret;
    }	
	
	function update_tarifjs(){
		//$idpenjamin = '1';
        
		//UPDATE
		//$this->db->where('idpenjamin', $idpenjamin);
        $this->db->where('kdpelayanan', $_POST['kdpelayanan']);
		$this->db->where('idklstarif', $_POST['idklstarif']);
		$this->db->set('tarifjs', $_POST['tarifjs']);
		$this->db->update('tarif'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function update_tarifjm(){
		//$idpenjamin = '1';
		
		//UPDATE
		//$this->db->where('idpenjamin', $idpenjamin);
        $this->db->where('kdpelayanan', $_POST['kdpelayanan']);
		$this->db->where('idklstarif', $_POST['idklstarif']);
		$this->db->set('tarifjm', $_POST['tarifjm']);
		$this->db->update('tarif'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function update_tarifjp(){
		//$idpenjamin = '1';
        
		//UPDATE
		//$this->db->where('idpenjamin', $idpenjamin);
        $this->db->where('kdpelayanan', $_POST['kdpelayanan']);
		$this->db->where('idklstarif', $_POST['idklstarif']);
		$this->db->set('tarifjp', $_POST['tarifjp']);
		$this->db->update('tarif'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function update_tarifbhp(){
		//$idpenjamin = '1';
        
		//UPDATE
		//$this->db->where('idpenjamin', $idpenjamin);
        $this->db->where('kdpelayanan', $_POST['kdpelayanan']);
		$this->db->where('idklstarif', $_POST['idklstarif']);
		$this->db->set('tarifbhp', $_POST['tarifbhp']);
		$this->db->update('tarif'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}	    
	
	function getFieldsAndValues(){
		//$idpenjamin = '1';
		//idpenjamin perlu diextract digit terakhir utk default idpenjamin --belum
		$kdpelayanan = (isset($_POST['kdpelayanan']))? $_POST['kdpelayanan'] : null;
		$idklstarif = (isset($_POST['idklstarif']))? $_POST['idklstarif'] : 2;
		$tarifjs 	= (isset($_POST['tarifjs']))? $_POST['tarifjs'] : 0;
		$tarifjm 	= (isset($_POST['tarifjm']))? $_POST['tarifjm'] : 0;
		$tarifjp 	= (isset($_POST['tarifjp']))? $_POST['tarifjp'] : 0;
		$tarifbhp 	= (isset($_POST['tarifbhp']))? $_POST['tarifbhp'] : 0;
		
		$dataArray = array(
		    // 'idpenjamin'	=> $idpenjamin, 
			 'kdpelayanan' 	=> $kdpelayanan,
             'idklstarif'	=> $idklstarif,
			 'tarifjs'		=> $tarifjs, 
			 'tarifjm' 		=> $tarifjm,
             'tarifjp'		=> $tarifjp,
			 'tarifbhp'		=> $tarifbhp,
			 'userid'		=> $this->session->userdata("user_id"),
			 'tglinput'		=> date('Y-m-d H:i:s')
        );
		
		return $dataArray;
	}
	
	function cekkdpelayanan(){
        $q = "SELECT count(*) as kdpelayanan FROM tarif where kdpelayanan='".$_POST['kdpelayanan']."' AND idklstarif='".$_POST['idklstarif']."'" ;
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->kdpelayanan;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
	
	function copy_data(){ 
		//$where['idpenjamin'] 	= '1';
		$where['idklstarif'] 	= $_POST['idklstarif_baru'];
		$this->rhlib->deleteRecord('tarif',$where);
				
		$idklstarif_lama = $_POST['idklstarif_lama'];
		$idklstarif_baru = $_POST['idklstarif_baru'];
		$sql = "INSERT INTO tarif (kdpelayanan, idklstarif)
				SELECT kdpelayanan
					 , ".$idklstarif_baru."
				FROM
				  tarif
				WHERE
				  tarif.idklstarif = '".$idklstarif_lama."'";
		$ret = $this->db->query($sql);
        return $ret;
    }
	
	function get_pitpelayanan(){        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("v_tarif");
		$this->db->order_by('v_tarif.kdpelayanan');
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all('v_tarif');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}
