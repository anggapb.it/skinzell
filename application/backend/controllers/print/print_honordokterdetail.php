<?php 
	class Print_honordokterdetail extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
      $this->load->library('lib_phpexcel');  
		}
		
		function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
        }		
		
		function namaBulanTahun($tgl){
			$arrtgl = explode('-', $tgl);
			$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
			$bulan = $query->row_array();
			$tanggalInd = $bulan['nmbulan'] .' '. $arrtgl[0];
			
			return $tanggalInd ;
		}
		
		function rp_satuan($angka,$debug){
			$terbilang = '';
			$a_str['1']="Satu";
			$a_str['2']="Dua";
			$a_str['3']="Tiga";
			$a_str['4']="Empat";
			$a_str['5']="Lima";
			$a_str['6']="Enam";
			$a_str['7']="Tujuh";
			$a_str['8']="Delapan";
			$a_str['9']="Sembilan";
			
			
			$panjang=strlen($angka);
			for ($b=0;$b<$panjang;$b++)
			{
				$a_bil[$b]=substr($angka,$panjang-$b-1,1);
			}
			
			if ($panjang>2)
			{
				if ($a_bil[2]=="1")
				{
					$terbilang=" Seratus";
				}
				else if ($a_bil[2]!="0")
				{
					$terbilang= " ".$a_str[$a_bil[2]]. " Ratus";
				}
			}

			if ($panjang>1)
			{
				if ($a_bil[1]=="1")
				{
					if ($a_bil[0]=="0")
					{
						$terbilang .=" Sepuluh";
					}
					else if ($a_bil[0]=="1")
					{
						$terbilang .=" Sebelas";
					}
					else 
					{
						$terbilang .=" ".$a_str[$a_bil[0]]." Belas";
					}
					return $terbilang;
				}
				else if ($a_bil[1]!="0")
				{
					$terbilang .=" ".$a_str[$a_bil[1]]." Puluh";
				}
			}
			
			if ($a_bil[0]!="0")
			{
				$terbilang .=" ".$a_str[$a_bil[0]];
			}
			return $terbilang;
		   
		}		
		
		function rp_terbilang($angka,$debug){
			$terbilang = '';
			
			$angka = str_replace(".",",",$angka);
			
			list ($angka) = explode(",",$angka);
			$panjang=strlen($angka);
			for ($b=0;$b<$panjang;$b++)
			{
				$myindex=$panjang-$b-1;
				$a_bil[$b]=substr($angka,$myindex,1);
			}
			if ($panjang>9)
			{
				$bil=$a_bil[9];
				if ($panjang>10)
				{
					$bil=$a_bil[10].$bil;
				}

				if ($panjang>11)
				{
					$bil=$a_bil[11].$bil;
				}
				if ($bil!="" && $bil!="000")
				{
					$terbilang .= $this->rp_satuan($bil,$debug)." milyar";
				}
				
			}

			if ($panjang>6)
			{
				$bil=$a_bil[6];
				if ($panjang>7)
				{
					$bil=$a_bil[7].$bil;
				}

				if ($panjang>8)
				{
					$bil=$a_bil[8].$bil;
				}
				if ($bil!="" && $bil!="000")
				{
					$terbilang .= $this->rp_satuan($bil,$debug)." Juta";
				}
				
			}
			
			if ($panjang>3)
			{
				$bil=$a_bil[3];
				if ($panjang>4)
				{
					$bil=$a_bil[4].$bil;
				}

				if ($panjang>5)
				{
					$bil=$a_bil[5].$bil;
				}
				if ($bil!="" && $bil!="000")
				{
					$terbilang .= $this->rp_satuan($bil,$debug)." Ribu";
				}
				
			}

			$bil=$a_bil[0];
			if ($panjang>1)
			{
				$bil=$a_bil[1].$bil;
			}

			if ($panjang>2)
			{
				$bil=$a_bil[2].$bil;
			}
			//die($bil);
			if ($bil!="" && $bil!="000")
			{
				$terbilang .= $this->rp_satuan($bil,$debug);
			}
			return trim($terbilang)." Rupiah";
		}
		
		function get_hondok($nohondok){
		
			$query  = $this->db->query("SELECT * FROM v_hondok WHERE nohondok='$nohondok'");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_rj($iddokter,$tglawal,$tglakhir){
		
			$query  = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
			 , `notadet`.`nonota` AS `nonota`
			 , `notadet`.`kditem` AS `kditem`
			 , `pelayanan`.`pel_kdpelayanan` AS `pel_kdpelayanan`
			 , `notadet`.`idjnstarif` AS `idjnstarif`
			 , `notadet`.`koder` AS `koder`
			 , `notadet`.`idsatuan` AS `idsatuan`
			 , `notadet`.`qty` AS `qty`
			 , `notadet`.`tarifjs` AS `tarifjs`
			 , `notadet`.`tarifjm` AS `tarifjm`
			 , `notadet`.`tarifjp` AS `tarifjp`
			 , `notadet`.`tarifbhp` AS `tarifbhp`
			 , `notadet`.`diskonjs` AS `diskonjs`
			 , `notadet`.`diskonjm` AS `diskonjm`
			 , `notadet`.`diskonjp` AS `diskonjp`
			 , `notadet`.`diskonbhp` AS `diskonbhp`
			 , `notadet`.`uangr` AS `uangr`
			 , `notadet`.`hrgjual` AS `hrgjual`
			 , `notadet`.`hrgbeli` AS `hrgbeli`
			 , `notadet`.`iddokter` AS `iddokter`
			 , `notadet`.`idperawat` AS `idperawat`
			 , `notadet`.`idstbypass` AS `idstbypass`
			 , `notadet`.`aturanpakai` AS `aturanpakai`
			 , `notadet`.`dijamin` AS `dijamin`
			 , `notadet`.`stdijamin` AS `stdijamin`
			 , `registrasidet`.`noreg` AS `noreg`
			 , `registrasidet`.`tglmasuk` AS `tglmasuk`
			 , `registrasidet`.`tglkeluar` AS `tglkeluar`
			 , `registrasidet`.`tglreg` AS `tglreg`
			 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
			 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
			 , `pasien`.`nmpasien` AS `nmpasien`
			 , `registrasidet`.`idbagian` AS `idbagian`
			 , `bagian`.`nmbagian` AS `nmbagian`
			 , sum((
			   CASE
			   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000001') THEN
				 (`notadet`.`tarifjm` * `notadet`.`qty`)
			   ELSE
				 0
			   END)) AS `pemeriksaan`
			 , sum((
			   CASE
			   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000048') THEN
				 (`notadet`.`tarifjm` * `notadet`.`qty`)
			   ELSE
				 0
			   END)) AS `tindakan`
			 , sum(`notadet`.`tarifjm` * `notadet`.`qty`) AS `jasamedis`
			 , sum(`notadet`.`diskonjm`) AS `diskonmedis`
			 , ceiling((((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 2.5) / 100)) AS `zis`
			 , ((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) - ceiling(((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 0.025))) AS `jumlah`
		FROM
		  `registrasidet`
		LEFT JOIN `registrasi`
		ON `registrasidet`.`noreg` = `registrasi`.`noreg`
		LEFT JOIN `pasien`
		ON `pasien`.`norm` = `registrasi`.`norm`
		LEFT JOIN `bagian`
		ON `bagian`.`idbagian` = `registrasidet`.`idbagian`
		LEFT JOIN `nota`
		ON `nota`.`idregdet` = `registrasidet`.`idregdet`
		LEFT JOIN `notadet`
		ON `notadet`.`nonota` = `nota`.`nonota` AND `notadet`.`iddokter` = '".$iddokter."'
		LEFT JOIN `pelayanan`
		ON `notadet`.`kditem` = `pelayanan`.`kdpelayanan`
		LEFT JOIN kuitansi
		ON kuitansi.nokuitansi = `nota`.nokuitansi
		WHERE
		  `registrasi`.`idjnspelayanan` IN (1, 3)
		  AND `notadet`.`tarifjm` > 0
		  AND `pelayanan`.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
		  AND `registrasidet`.`userbatal` IS NULL
		  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
		GROUP BY
		  `registrasi`.`noreg`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_ri($iddokter,$tglawal,$tglakhir){
		
			$query  = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
			 , `notadet`.`nonota` AS `nonota`
			 , `notadet`.`kditem` AS `kditem`
			 , `notadet`.`idjnstarif` AS `idjnstarif`
			 , `notadet`.`koder` AS `koder`
			 , `notadet`.`idsatuan` AS `idsatuan`
			 , `notadet`.`qty` AS `qty`
			 , `notadet`.`tarifjs` AS `tarifjs`
			 , `notadet`.`tarifjm` AS `tarifjm`
			 , `notadet`.`tarifjp` AS `tarifjp`
			 , `notadet`.`tarifbhp` AS `tarifbhp`
			 , `notadet`.`diskonjs` AS `diskonjs`
			 , `notadet`.`diskonjm` AS `diskonjm`
			 , `notadet`.`diskonjp` AS `diskonjp`
			 , `notadet`.`diskonbhp` AS `diskonbhp`
			 , `notadet`.`uangr` AS `uangr`
			 , `notadet`.`hrgjual` AS `hrgjual`
			 , `notadet`.`hrgbeli` AS `hrgbeli`
			 , `notadet`.`iddokter` AS `iddokter`
			 , `notadet`.`idperawat` AS `idperawat`
			 , `notadet`.`idstbypass` AS `idstbypass`
			 , `notadet`.`aturanpakai` AS `aturanpakai`
			 , `notadet`.`dijamin` AS `dijamin`
			 , `notadet`.`stdijamin` AS `stdijamin`
			 , `registrasidet`.`noreg` AS `noreg`
			 , `registrasidet`.`tglmasuk` AS `tglmasuk`
			 , `registrasidet`.`tglkeluar` AS `tglkeluar`
			 , `registrasidet`.`tglreg` AS `tglreg`
			 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
			 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
			 , `pasien`.`nmpasien` AS `nmpasien`
			 , `registrasidet`.`idbagian` AS `idbagian`
			 , `bagian`.`nmbagian` AS `nmbagian`
			 , sum((
			   CASE
			   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000001') THEN
				 (`notadet`.`tarifjm` * `notadet`.`qty`)
			   ELSE
				 0
			   END)) AS `pemeriksaan`
			 , sum((
			   CASE
			   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000048') THEN
				 (`notadet`.`tarifjm` * `notadet`.`qty`)
			   ELSE
				 0
			   END)) AS `tindakan`
			 , sum(`notadet`.`tarifjm` * `notadet`.`qty`) AS `jasamedis`
			 , sum(`notadet`.`diskonjm`) AS `diskonmedis`
			 , ceiling((((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 2.5) / 100)) AS `zis`
			 , ((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) - ceiling(((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 0.025))) AS `jumlah`
			 , sum(((`notadet`.`qty` * `notadet`.`tarifjm`) - `notadet`.`diskonjm`)) AS `sum((``notadet``.qty * ``notadet``.tarifjm) - ``notadet``.diskonjm)`
		FROM
		  `registrasidet`
		LEFT JOIN `registrasi`
		ON `registrasidet`.`noreg` = `registrasi`.`noreg`
		LEFT JOIN `reservasi`
		ON `registrasidet`.`idregdet` = `reservasi`.`idregdet`
		LEFT JOIN `pasien`
		ON `pasien`.`norm` = `registrasi`.`norm`
		LEFT JOIN `nota`
		ON `nota`.`idregdet` = `registrasidet`.`idregdet`
		LEFT JOIN `notadet`
		ON `notadet`.`nonota` = `nota`.`nonota` AND `notadet`.`iddokter` = '".$iddokter."'
		LEFT JOIN `bagian`
		ON `bagian`.`idbagian` = `registrasidet`.`idbagian`
		LEFT JOIN `pelayanan`
		ON `notadet`.`kditem` = `pelayanan`.`kdpelayanan`
		LEFT JOIN kuitansi
		ON kuitansi.nokuitansi = `nota`.nokuitansi
		WHERE
		  `registrasi`.`idjnspelayanan` = 2
		  AND `reservasi`.`idstposisipasien` = 6
		  AND `notadet`.`tarifjm` > 0
		  AND `pelayanan`.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
		  AND `registrasidet`.`userbatal` IS NULL
		  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'

		GROUP BY
		  `registrasi`.`noreg`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_jm($iddokter,$tglawal,$tglakhir){
		
			$query  = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
			 , `notadet`.`nonota` AS `nonota`
			 , `notadet`.`kditem` AS `kditem`
			 , `notadet`.`idjnstarif` AS `idjnstarif`
			 , `notadet`.`koder` AS `koder`
			 , `notadet`.`idsatuan` AS `idsatuan`
			 , `notadet`.`qty` AS `qty`
			 , `notadet`.`tarifjs` AS `tarifjs`
			 , `notadet`.`tarifjm` AS `tarifjm`
			 , `notadet`.`tarifjp` AS `tarifjp`
			 , `notadet`.`tarifbhp` AS `tarifbhp`
			 , `notadet`.`diskonjs` AS `diskonjs`
			 , `notadet`.`diskonjm` AS `diskonjm`
			 , `notadet`.`diskonjp` AS `diskonjp`
			 , `notadet`.`diskonbhp` AS `diskonbhp`
			 , `notadet`.`uangr` AS `uangr`
			 , `notadet`.`hrgjual` AS `hrgjual`
			 , `notadet`.`hrgbeli` AS `hrgbeli`
			 , `jtmdet`.`iddokter` AS `iddokter`
			 , `notadet`.`idperawat` AS `idperawat`
			 , `notadet`.`idstbypass` AS `idstbypass`
			 , `notadet`.`aturanpakai` AS `aturanpakai`
			 , `notadet`.`dijamin` AS `dijamin`
			 , `notadet`.`stdijamin` AS `stdijamin`
			 , `registrasidet`.`noreg` AS `noreg`
			 , `registrasidet`.`tglmasuk` AS `tglmasuk`
			 , `registrasidet`.`tglkeluar` AS `tglkeluar`
			 , `registrasidet`.`tglreg` AS `tglreg`
			 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
			 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
			 , `pasien`.`nmpasien` AS `nmpasien`
			 , sum(`jtmdet`.`jumlah`) AS `jasamedis`
			 , sum(`jtmdet`.`diskon`) AS `diskonmedis`
			 , ceiling((((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) * 2.5) / 100)) AS `zis`
			 , ((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) - ceiling(((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) * 0.025))) AS `jumlah`
		FROM
		  `registrasidet`
		LEFT JOIN `registrasi`
		ON `registrasidet`.`noreg` = `registrasi`.`noreg`
		LEFT JOIN `nota`
		ON `nota`.`idregdet` = `registrasidet`.`idregdet`
		LEFT JOIN `notadet`
		ON `notadet`.`nonota` = `nota`.`nonota`
		LEFT JOIN `pasien`
		ON `pasien`.`norm` = `registrasi`.`norm`
		LEFT JOIN `jtm`
		ON `jtm`.`idnotadet` = `notadet`.`idnotadet`
		LEFT JOIN `jtmdet`
		ON `jtm`.`idjtm` = `jtmdet`.`idjtm` AND `jtmdet`.`iddokter` = '".$iddokter."'
		LEFT JOIN kuitansi
		ON kuitansi.nokuitansi = `nota`.nokuitansi
		WHERE
		  (`jtmdet`.`tarifjm` > 0)
		  AND `registrasidet`.`userbatal` IS NULL
		  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
		GROUP BY
		  `registrasi`.`noreg`
		, `jtmdet`.`iddokter`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_peltam($iddokter,$tglawal,$tglakhir){
		
			$query  = $this->db->query("SELECT notadet.idnotadet AS idnotadet
											 , notadet.nonota AS nonota
											 , nota.tglnota AS tglnota
											 , kuitansi.tglkuitansi AS tglkuitansi
											 , notadet.kditem AS kditem
											 , pelayanan.pel_kdpelayanan AS pel_kdpelayanan
											 , notadet.iddokter AS iddokter
											 , kuitansi.atasnama AS nmpasien
											 , sum((
											   CASE
											   WHEN (pelayanan.pel_kdpelayanan = 'T000000001') THEN
												 (notadet.tarifjm * notadet.qty)
											   ELSE
												 0
											   END)) AS pemeriksaan
											 , sum((
											   CASE
											   WHEN (pelayanan.pel_kdpelayanan = 'T000000048') THEN
												 (notadet.tarifjm * notadet.qty)
											   ELSE
												 0
											   END)) AS tindakan
											 , sum(notadet.tarifjm * notadet.qty) AS jasamedis
											 , sum(notadet.diskonjm) AS diskonmedis
											 , ceiling((((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) * 2.5) / 100)) AS zis
											 , ((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) - ceiling(((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) * 0.025))) AS jumlah

										FROM
										  nota
										LEFT JOIN notadet
										ON nota.nonota = notadet.nonota AND notadet.iddokter = '".$iddokter."'
										LEFT JOIN pelayanan
										ON pelayanan.kdpelayanan = notadet.kditem
										LEFT JOIN kuitansi
										ON kuitansi.nokuitansi = nota.nokuitansi
										WHERE
										  notadet.tarifjm > 0
										  AND nota.idjnstransaksi = 10
										  AND pelayanan.pel_kdpelayanan IN ('T000000001', 'T000000048')
										  AND nota.idsttransaksi = 1
										  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
										GROUP BY
										  nota.nonota, notadet.kditem");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
	
		function pdf_honordokterdetail($nohondok){
		
		$this->pdf->SetMargins('10', '10', '10');
		$this->pdf->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf->setPrintHeader(false); // enabled ? true
		$this->pdf->setPrintFooter(true); // enabled ? true
		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf->AddPage('P', 'A4', false, false); 

		$hondok = $this->get_hondok($nohondok);
		
		foreach ($hondok as $item) {
			$nohondok = $item->nohondok;
			$tglhondok = $this->TanggalIndo(date("Ymd",strtotime($item->tglhondok)));
			$iddokter = $item->iddokter;
			$nmdoktergelar = $item->nmdoktergelar;
			$nmspesialisasi = $item->nmspesialisasi;
			$nmstdokter = $item->nmstdokter;
			$nmstatus = $item->nmstatus;
			$tglawal = $this->TanggalIndo(date("Ymd",strtotime($item->tglawal)));
			$tglakhir = $this->TanggalIndo(date("Ymd",strtotime($item->tglakhir)));
			$tglawalparams = $item->tglawal;
			$tglakhirparams = $item->tglakhir;
			
			$norek = $item->norek;
			$nmbank = $item->nmbank;
			$atasnama = $item->atasnama;
			$notelp = $item->notelp;
			$nohp = $item->nohp;
			$nmlengkap = $item->nmlengkap;
			$tglinput = $this->TanggalIndo(date("Ymd",strtotime($item->tglinput)));
			$approval = $item->approval;
			$nmstbayar = $item->nmstbayar;
			$nmjnspembayaran = $item->nmjnspembayaran;
			$jasadrjaga = number_format(ceil($item->jasadrjaga) , 0 , ',' , '.' );
			$jasadrjagaval = ceil($item->jasadrjaga);
			$potonganlain = number_format(ceil($item->potonganlain) , 0 , ',' , '.' );
			$potonganlainval = ceil($item->potonganlain);
			$catatan = $item->catatan;
		}
		
		$total123 = 0;
		$total123val = 0;
		
		//========================RAWAT JALAN
		$hd_rj = $this->get_hd_rj($iddokter,$tglawalparams,$tglakhirparams);
		$no = 1;
		$grid_hd_rj = "";
		$sumpemeriksaan = 0;
		$sumtindakan = 0;
		$sumjasamedis = 0;
		$sumdiskon = 0;
		$sumzis = 0;
		$sumjumlah = 0;
		foreach ($hd_rj as $itemrj) {
		
			$grid_hd_rj .= "<tr>
						  <th width=\"4%\" align=\"center\">".$no++."</th>
						  <th width=\"10%\" align=\"left\">".$itemrj->noreg."</th>
						  <th width=\"10%\" align=\"left\">".date("d/m/Y",strtotime($itemrj->tglkuitansi))."</th>
						  <th width=\"10%\" align=\"left\">".$itemrj->norm."</th>
						  <th width=\"16%\" align=\"left\">".$itemrj->nmpasien."</th>
						  <th width=\"9%\" align=\"right\">".number_format(ceil($itemrj->pemeriksaan) , 0 , ',' , '.' )."</th>
						  <th width=\"6.5%\" align=\"right\">".number_format(ceil($itemrj->tindakan) , 0 , ',' , '.' )."</th>
						  <th width=\"12.5%\" align=\"right\">".number_format(ceil($itemrj->jasamedis) , 0 , ',' , '.' )."</th>
						  <th width=\"6.5%\" align=\"right\">".number_format(ceil($itemrj->diskonmedis) , 0 , ',' , '.' )."</th>
						  <th width=\"6.5%\" align=\"right\">".number_format(ceil($itemrj->zis) , 0 , ',' , '.' )."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itemrj->jumlah) , 0 , ',' , '.' )."</th>
					 </tr>";
					 
			$sumpemeriksaan = $sumpemeriksaan + ceil($itemrj->pemeriksaan);
			$sumtindakan = $sumtindakan + ceil($itemrj->tindakan);
			$sumjasamedis = $sumjasamedis + ceil($itemrj->jasamedis);
			$sumdiskon = $sumdiskon + ceil($itemrj->diskonmedis);
			$sumzis = $sumzis + ceil($itemrj->zis);
			$sumjumlah = $sumjumlah + ceil($itemrj->jumlah);
		}
		$grid_hd_rj .= "<tr>
						  <th width=\"50%\" align=\"right\" colspan=\"5\"><b>Total 1:</b></th>
						  <th width=\"9%\" align=\"right\"><b>".number_format($sumpemeriksaan , 0 , ',' , '.' )."</b></th>
						  <th width=\"6.5%\" align=\"right\"><b>".number_format($sumtindakan , 0 , ',' , '.' )."</b></th>
						  <th width=\"12.5%\" align=\"right\"><b>".number_format($sumjasamedis , 0 , ',' , '.' )."</b></th>
						  <th width=\"6.5%\" align=\"right\"><b>".number_format($sumdiskon , 0 , ',' , '.' )."</b></th>
						  <th width=\"6.5%\" align=\"right\"><b>".number_format($sumzis , 0 , ',' , '.' )."</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumjumlah , 0 , ',' , '.' )."</b></th> 
					 </tr>";
		$total123 = $total123 + ceil($sumjumlah);
		$total123val = $total123;
		//========================RAWAT INAP
		$hd_ri = $this->get_hd_ri($iddokter,$tglawalparams,$tglakhirparams);
		$no = 1;
		$grid_hd_ri = "";
		$sumpemeriksaan = 0;
		$sumtindakan = 0;
		$sumjasamedis = 0;
		$sumdiskon = 0;
		$sumzis = 0;
		$sumjumlah = 0;
		foreach ($hd_ri as $itemri) {
		
			$grid_hd_ri .= "<tr>
						  <th width=\"4%\" align=\"center\">".$no++."</th>
						  <th width=\"8%\" align=\"left\">".$itemri->noreg."</th>
						  <!-- <th width=\"7%\" align=\"left\">".$itemri->tglreg."</th> -->
						  <th width=\"7%\" align=\"left\">".date("d/m/Y",strtotime($itemri->tglmasuk))."</th>
						  <th width=\"7%\" align=\"left\">".date("d/m/Y",strtotime($itemri->tglkuitansi))."</th>
						  <th width=\"7.5%\" align=\"left\">".$itemri->norm."</th>
						  <th width=\"14%\" align=\"left\">".$itemri->nmpasien."</th>
						  <th width=\"8%\" align=\"left\">".$itemri->nmbagian."</th>
						  <th width=\"7%\" align=\"right\">".number_format(ceil($itemri->pemeriksaan) , 0 , ',' , '.' )."</th>
						  <th width=\"7%\" align=\"right\">".number_format(ceil($itemri->tindakan) , 0 , ',' , '.' )."</th>
						  <th width=\"12.5%\" align=\"right\">".number_format(ceil($itemri->jasamedis) , 0 , ',' , '.' )."</th>
						  <th width=\"6.5%\" align=\"right\">".number_format(ceil($itemri->diskonmedis) , 0 , ',' , '.' )."</th>
						  <th width=\"6.5%\" align=\"right\">".number_format(ceil($itemri->zis) , 0 , ',' , '.' )."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itemri->jumlah) , 0 , ',' , '.' )."</th>
					 </tr>";
					 
			$sumpemeriksaan = $sumpemeriksaan + ceil($itemri->pemeriksaan);
			$sumtindakan = $sumtindakan + ceil($itemri->tindakan);
			$sumjasamedis = $sumjasamedis + ceil($itemri->jasamedis);
			$sumdiskon = $sumdiskon + ceil($itemri->diskonmedis);
			$sumzis = $sumzis + ceil($itemri->zis);
			$sumjumlah = $sumjumlah + ceil($itemri->jumlah);
		}
		$grid_hd_ri .= "<tr>
						  <th width=\"55.5%\" align=\"right\" colspan=\"5\"><b>Total 2:</b></th>
						  <th width=\"7%\" align=\"right\"><b>".number_format($sumpemeriksaan , 0 , ',' , '.' )."</b></th>
						  <th width=\"7%\" align=\"right\"><b>".number_format($sumtindakan , 0 , ',' , '.' )."</b></th>
						  <th width=\"12.5%\" align=\"right\"><b>".number_format($sumjasamedis , 0 , ',' , '.' )."</b></th>
						  <th width=\"6.5%\" align=\"right\"><b>".number_format($sumdiskon , 0 , ',' , '.' )."</b></th>
						  <th width=\"6.5%\" align=\"right\"><b>".number_format($sumzis , 0 , ',' , '.' )."</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumjumlah , 0 , ',' , '.' )."</b></th> 
					 </tr>";
		$total123 = $total123 + ceil($sumjumlah);
		$total123val = $total123;
		//========================JASA MEDIS
		$hd_jm = $this->get_hd_jm($iddokter,$tglawalparams,$tglakhirparams);
		$no = 1;
		$grid_hd_jm = "";
		$sumjasamedis = 0;
		$sumdiskon = 0;
		$sumzis = 0;
		$sumjumlah = 0;
		foreach ($hd_jm as $itemjm) {
		
			$grid_hd_jm .= "<tr>
						  <th width=\"4%\" align=\"center\">".$no++."</th>
						  <th width=\"12%\" align=\"left\">".$itemjm->noreg."</th>
						  <th width=\"12%\" align=\"left\">".date("d/m/Y",strtotime($itemjm->tglkuitansi))."</th>
						  <th width=\"10%\" align=\"left\">".$itemjm->norm."</th>
						  <th width=\"28%\" align=\"left\">".$itemjm->nmpasien."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itemjm->jasamedis) , 0 , ',' , '.' )."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itemjm->diskonmedis) , 0 , ',' , '.' )."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itemjm->zis) , 0 , ',' , '.' )."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itemjm->jumlah) , 0 , ',' , '.' )."</th>
					 </tr>";
					 
			$sumjasamedis = $sumjasamedis + ceil($itemjm->jasamedis);
			$sumdiskon = $sumdiskon + ceil($itemjm->diskonmedis);
			$sumzis = $sumzis + ceil($itemjm->zis);
			$sumjumlah = $sumjumlah + ceil($itemjm->jumlah);
		}
		$grid_hd_jm .= "<tr>
						  <th width=\"66%\" align=\"right\" colspan=\"5\"><b>Total 3:</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumjasamedis , 0 , ',' , '.' )."</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumdiskon , 0 , ',' , '.' )."</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumzis , 0 , ',' , '.' )."</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumjumlah , 0 , ',' , '.' )."</b></th> 
					 </tr>";
		$total123 = $total123 + ceil($sumjumlah);
		$total123val = ceil($total123);
		//========================Pel Tambahan
		$hd_peltam = $this->get_hd_peltam($iddokter,$tglawalparams,$tglakhirparams);
		$no = 1;
		$grid_hd_peltam = "";
		$sumjasamedis = 0;
		$sumdiskon = 0;
		$sumzis = 0;
		$sumjumlah = 0;
		foreach ($hd_peltam as $itempeltam) {
		
			$grid_hd_peltam .= "<tr>
						  <th width=\"4%\" align=\"center\">".$no++."</th>
						  <th width=\"12%\" align=\"left\">".$itempeltam->nonota."</th>
						  <th width=\"12%\" align=\"left\">".date("d/m/Y",strtotime($itempeltam->tglkuitansi))."</th>
						  <th width=\"38%\" align=\"left\">".$itempeltam->nmpasien."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itempeltam->jasamedis) , 0 , ',' , '.' )."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itempeltam->diskonmedis) , 0 , ',' , '.' )."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itempeltam->zis) , 0 , ',' , '.' )."</th>
						  <th width=\"8%\" align=\"right\">".number_format(ceil($itempeltam->jumlah) , 0 , ',' , '.' )."</th>
					 </tr>";
					 
			$sumjasamedis = $sumjasamedis + ceil($itempeltam->jasamedis);
			$sumdiskon = $sumdiskon + ceil($itempeltam->diskonmedis);
			$sumzis = $sumzis + ceil($itempeltam->zis);
			$sumjumlah = $sumjumlah + ceil($itempeltam->jumlah);
		}
		$grid_hd_peltam .= "<tr>
						  <th width=\"66%\" align=\"right\" colspan=\"5\"><b>Total 4:</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumjasamedis , 0 , ',' , '.' )."</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumdiskon , 0 , ',' , '.' )."</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumzis , 0 , ',' , '.' )."</b></th>
						  <th width=\"8%\" align=\"right\"><b>".number_format($sumjumlah , 0 , ',' , '.' )."</b></th> 
					 </tr>";
		$total123 = $total123 + ceil($sumjumlah);
		$total123val = ceil($total123);
		$totalterima = number_format((ceil($total123val) + ceil($jasadrjagaval) - ceil($potonganlainval)) , 0 , ',' , '.' );
		
		$total123 = number_format($total123, 0 , ',' , '.' );

//========================rj================================
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'DETAIL JASA MEDIS', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'PENERIMAAN RAWAT JALAN / UGD', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->SetFont('helvetica', '', 6.5);
		
		$tblrj = <<<EOD
	<table border="1" cellpadding="2" nobr="false">
	<thead>
     <tr>
	  <th width="99%" align="left"><b>Nama : $nmdoktergelar</b></th>
     </tr> 
     <tr>
	  <th width="4%" align="center"><b>No.</b></th>
	  <th width="10%" align="center"><b>No. Registrasi</b></th>
      <th width="10%" align="center"><b>Tgl. Kuitansi</b></th>
	  <th width="10%" align="center"><b>No. RM</b></th>
      <th width="16%" align="center"><b>Nama Pasien</b></th>
	  <th width="9%" align="center"><b>Pemeriksaan</b></th>
	  <th width="6.5%" align="center"><b>Tindakan</b></th>
	  <th width="12.5%" align="center"><b>Total Pemeriksaan<br>dan Tindakan</b></th>
      <th width="6.5%" align="center"><b>Diskon</b></th>
	  <th width="6.5%" align="center"><b>ZIS</b></th>
	  <th width="8%" align="center"><b>Di Terima</b></th>
     </tr> 
	</thead>
		$grid_hd_rj
    </table>
	
EOD;
     	$this->pdf->writeHTML($tblrj,true,false,false,false);	
		
		//$this->pdf->AddPage('P', 'F4', false, false); 
//======================ri==================================
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'DETAIL JASA MEDIS', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'PENERIMAAN RAWAT INAP / RI', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->SetFont('helvetica', '', 6.5);
		
		$tblri = <<<EOD
    <table border="1" cellpadding="2" nobr="false">
	 <thead>
     <tr>
	  <th width="103%" align="left"><b>Nama : $nmdoktergelar</b></th>
     </tr> 
     <tr>
	  <th width="4%" align="center"><b>No.</b></th>
	  <th width="8%" align="center"><b>No. Registrasi</b></th>
      <!-- <th width="7%" align="center"><b>Tgl. Registrasi</b></th> -->
	  <th width="7%" align="center"><b>Tgl. Masuk</b></th>
	  <th width="7%" align="center"><b>Tgl. Keluar</b></th>
	  <th width="7.5%" align="center"><b>No. RM</b></th>
      <th width="14%" align="center"><b>Nama Pasien</b></th>
	  <th width="8%" align="center"><b>Ruangan</b></th>
	  <th width="7%" align="center"><b>Visite</b></th>
	  <th width="7%" align="center"><b>Tindakan</b></th>
	  <th width="12.5%" align="center"><b>Total Visite<br>dan Tindakan</b></th>
      <th width="6.5%" align="center"><b>Diskon</b></th>
	  <th width="6.5%" align="center"><b>ZIS</b></th>
	  <th width="8%" align="center"><b>Di Terima</b></th>
     </tr> 
	 </thead>
		$grid_hd_ri
    </table>
	
EOD;
		$this->pdf->writeHTML($tblri,true,false,false,false);	
		
		//$this->pdf->AddPage('P', 'F4', false, false); 
//======================tm==================================
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'DETAIL JASA MEDIS', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'PENERIMAAN TINDAKAN TIM MEDIS', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->SetFont('helvetica', '', 6.5);
		
		$tbltm = <<<EOD
    <table border="1" cellpadding="2" nobr="false">
	 <thead>
     <tr>
	  <th width="98%" align="left"><b>Nama : $nmdoktergelar</b></th>
     </tr> 
     <tr>
	  <th width="4%" align="center"><b>No.</b></th>
	  <th width="12%" align="center"><b>No. Registrasi</b></th>
      <th width="12%" align="center"><b>Tgl. Kuitansi</b></th>
	  <th width="10%" align="center"><b>No. RM</b></th>
      <th width="28%" align="center"><b>Nama Pasien</b></th>
	  <th width="8%" align="center"><b>Jasa Medis</b></th>
      <th width="8%" align="center"><b>Diskon</b></th>
	  <th width="8%" align="center"><b>ZIS</b></th>
	  <th width="8%" align="center"><b>Di Terima</b></th>
     </tr> 
	 </thead>
		$grid_hd_jm
    </table>
	
EOD;
     	$this->pdf->writeHTML($tbltm,true,false,false,false);		

//======================pl==================================
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'DETAIL JASA MEDIS', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'PENERIMAAN PELAYANAN TAMBAHAN', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->SetFont('helvetica', '', 6.5);
		
		$tbltm = <<<EOD
    <table border="1" cellpadding="2" nobr="false">
	 <thead>
     <tr>
	  <th width="98%" align="left"><b>Nama : $nmdoktergelar</b></th>
     </tr> 
     <tr>
	  <th width="4%" align="center"><b>No.</b></th>
	  <th width="12%" align="center"><b>No. Nota</b></th>
      <th width="12%" align="center"><b>Tgl. Kuitansi</b></th>
      <th width="38%" align="center"><b>Nama Pasien</b></th>
	  <th width="8%" align="center"><b>Jasa Medis</b></th>
      <th width="8%" align="center"><b>Diskon</b></th>
	  <th width="8%" align="center"><b>ZIS</b></th>
	  <th width="8%" align="center"><b>Di Terima</b></th>
     </tr> 
	 </thead>
		$grid_hd_peltam
    </table>
	
EOD;
     	$this->pdf->writeHTML($tbltm,true,false,false,false);		

		$this->pdf->Output('honordokter-'.$nohondok.'.pdf', 'I');
		}
		
		
		function get_hd_rj_eksp($iddokter,$tglawal,$tglakhir){
										
			$query  = $this->db->query("SELECT @no := @no + 1 no
				 , `registrasidet`.`noreg` AS `noreg`
				 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
				 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
				 , `pasien`.`nmpasien` AS `nmpasien`
				 , sum((
				   CASE
				   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000001') THEN
					 `notadet`.`tarifjm`
				   ELSE
					 0
				   END)) AS `pemeriksaan`
				 , sum((
				   CASE
				   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000048') THEN
					 `notadet`.`tarifjm`
				   ELSE
					 0
				   END)) AS `tindakan`
				 , sum(`notadet`.`tarifjm`) AS `jasamedis`
				 , sum(`notadet`.`diskonjm`) AS `diskonmedis`
				 , ceiling((((sum(`notadet`.`tarifjm`) - sum(`notadet`.`diskonjm`)) * 2.5) / 100)) AS `zis`
				 , ((sum(`notadet`.`tarifjm`) - sum(`notadet`.`diskonjm`)) - ceiling(((sum(`notadet`.`tarifjm`) - sum(`notadet`.`diskonjm`)) * 0.025))) AS `jumlah`
			FROM
			  `registrasidet`
			LEFT JOIN `registrasi`
			ON `registrasidet`.`noreg` = `registrasi`.`noreg`
			LEFT JOIN `pasien`
			ON `pasien`.`norm` = `registrasi`.`norm`
			LEFT JOIN `bagian`
			ON `bagian`.`idbagian` = `registrasidet`.`idbagian`
			LEFT JOIN `nota`
			ON `nota`.`idregdet` = `registrasidet`.`idregdet`
			LEFT JOIN `notadet`
			ON `notadet`.`nonota` = `nota`.`nonota` AND `notadet`.`iddokter` = '".$iddokter."'
			LEFT JOIN `pelayanan`
			ON `notadet`.`kditem` = `pelayanan`.`kdpelayanan`
			LEFT JOIN kuitansi
			ON kuitansi.nokuitansi = `nota`.nokuitansi
			, (SELECT @no := 0) m
			WHERE
			  `registrasi`.`idjnspelayanan` IN (1, 3)
			  AND `notadet`.`tarifjm` > 0
			  AND `pelayanan`.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
			  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
			GROUP BY
			  `registrasi`.`noreg`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_ri_eksp($iddokter,$tglawal,$tglakhir){
			
			$query  = $this->db->query("SELECT @no := @no + 1 no
				 , `registrasidet`.`noreg` AS `noreg`
				 , `registrasidet`.`tglmasuk` AS `tglmasuk`
				 , `kuitansi`.`tglkuitansi` AS `tglkeluar`
				 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
				 , `pasien`.`nmpasien` AS `nmpasien`
				 , `bagian`.`nmbagian` AS `nmbagian`
				 , sum((
				   CASE
				   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000001') THEN
					 `notadet`.`tarifjm`
				   ELSE
					 0
				   END)) AS `pemeriksaan`
				 , sum((
				   CASE
				   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000048') THEN
					 `notadet`.`tarifjm`
				   ELSE
					 0
				   END)) AS `tindakan`
				 , sum(`notadet`.`tarifjm`) AS `jasamedis`
				 , sum(`notadet`.`diskonjm`) AS `diskonmedis`
				 , ceiling((((sum(`notadet`.`tarifjm`) - sum(`notadet`.`diskonjm`)) * 2.5) / 100)) AS `zis`
				 , ((sum(`notadet`.`tarifjm`) - sum(`notadet`.`diskonjm`)) - ceiling(((sum(`notadet`.`tarifjm`) - sum(`notadet`.`diskonjm`)) * 0.025))) AS `jumlah`
			FROM
			  `registrasidet`
			LEFT JOIN `registrasi`
			ON `registrasidet`.`noreg` = `registrasi`.`noreg`
			LEFT JOIN `reservasi`
			ON `registrasidet`.`idregdet` = `reservasi`.`idregdet`
			LEFT JOIN `pasien`
			ON `pasien`.`norm` = `registrasi`.`norm`
			LEFT JOIN `nota`
			ON `nota`.`idregdet` = `registrasidet`.`idregdet`
			LEFT JOIN `notadet`
			ON `notadet`.`nonota` = `nota`.`nonota` AND `notadet`.`iddokter` = '".$iddokter."'
			LEFT JOIN `bagian`
			ON `bagian`.`idbagian` = `registrasidet`.`idbagian`
			LEFT JOIN `pelayanan`
			ON `notadet`.`kditem` = `pelayanan`.`kdpelayanan`
			LEFT JOIN kuitansi
			ON kuitansi.nokuitansi = `nota`.nokuitansi
			, (SELECT @no := 0) m
			WHERE
			  `registrasi`.`idjnspelayanan` = 2
			  AND `reservasi`.`idstposisipasien` = 6
			  AND `notadet`.`tarifjm` > 0
			  AND `pelayanan`.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
			  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'

			GROUP BY
			  `registrasi`.`noreg`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_jm_eksp($iddokter,$tglawal,$tglakhir){
			
			$query  = $this->db->query("SELECT @no := @no + 1 no
				 , `registrasidet`.`noreg` AS `noreg`
				 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
				 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
				 , `pasien`.`nmpasien` AS `nmpasien`
				 , sum(`jtmdet`.`jumlah`) AS `jasamedis`
				 , sum(`jtmdet`.`diskon`) AS `diskonmedis`
				 , ceiling((((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) * 2.5) / 100)) AS `zis`
				 , ((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) - ceiling(((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) * 0.025))) AS `jumlah`
			FROM
			  `registrasidet`
			LEFT JOIN `registrasi`
			ON `registrasidet`.`noreg` = `registrasi`.`noreg`
			LEFT JOIN `nota`
			ON `nota`.`idregdet` = `registrasidet`.`idregdet`
			LEFT JOIN `notadet`
			ON `notadet`.`nonota` = `nota`.`nonota`
			LEFT JOIN `pasien`
			ON `pasien`.`norm` = `registrasi`.`norm`
			LEFT JOIN `jtm`
			ON `jtm`.`idnotadet` = `notadet`.`idnotadet`
			LEFT JOIN `jtmdet`
			ON `jtm`.`idjtm` = `jtmdet`.`idjtm` AND `jtmdet`.`iddokter` = '".$iddokter."'
			LEFT JOIN kuitansi
			ON kuitansi.nokuitansi = `nota`.nokuitansi
			, (SELECT @no := 0) m
			WHERE
			  (`jtmdet`.`tarifjm` > 0)
			  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
			GROUP BY
			  `registrasi`.`noreg`
			, `jtmdet`.`iddokter`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		
		function eksport_honordokterdetail_bu($nohondok){	
			$hondok = $this->get_hondok($nohondok);
			
			foreach ($hondok as $item) {
				$nohondok = $item->nohondok;
				$tglhondok = $this->TanggalIndo(date("Ymd",strtotime($item->tglhondok)));
				$iddokter = $item->iddokter;
				$nmdoktergelar = $item->nmdoktergelar;
				$tglawalparams = $item->tglawal;
				$tglakhirparams = $item->tglakhir;
	
				$data['header'] = 'Nama : '.$nmdoktergelar;
				
				$data['header1'] = 'PENERIMAAN RAWAT JALAN / UGD';
				$data['header2'] = 'PENERIMAAN RAWAT INAP / RI';
				$data['header3'] = 'PENERIMAAN TINDAKAN TIM MEDIS';
				
				$data['rj_fieldname'] = array('No.'
											,'No Registrasi'
											,'Tgl Kuitansi'
											,'No. RM'
											,'Nama Pasien'
											,'Pemeriksaan'
											,'Tindakan'
											,'Jasa Medis'
											,'Diskon'
											,'ZIS'
											,'Jumlah');
				$data['ri_fieldname'] = array('No.'
											,'No Registrasi'
											,'Tgl Masuk'
											,'Tgl Keluar'
											,'No. RM'
											,'Nama Pasien'
											,'Ruangan'
											,'Visite'
											,'Tindakan'
											,'Jasa Medis'
											,'Diskon'
											,'ZIS'
											,'Jumlah');
				$data['jm_fieldname'] = array('No.'
											,'No Registrasi'
											,'Tgl Kuitansi'
											,'No. RM'
											,'Nama Pasien'
											,'Jasa Medis'
											,'Diskon'
											,'ZIS'
											,'Jumlah');
				
				$data['rj'] = $this->get_hd_rj_eksp($iddokter,$tglawalparams,$tglakhirparams);
				$data['ri'] = $this->get_hd_ri_eksp($iddokter,$tglawalparams,$tglakhirparams);
				$data['jm'] = $this->get_hd_jm_eksp($iddokter,$tglawalparams,$tglakhirparams);
				
				$data['filename'] = 'honordokter-'.$nohondok;
				$this->load->view('exportexcelhondok', $data); 	

			}
			
			
		}

	  function eksport_honordokterdetail($nohondok)
	  {
			$hondok = $this->db->query("SELECT * FROM v_hondok WHERE nohondok='$nohondok'")->row();
			$nohondok = $hondok->nohondok;
			$tglhondok = $this->TanggalIndo(date("Ymd",strtotime($hondok->tglhondok)));
			$iddokter = $hondok->iddokter;
			$nmdoktergelar = $hondok->nmdoktergelar;
			$tglawalparams = $hondok->tglawal;
			$tglakhirparams = $hondok->tglakhir;
      $nmspesialisasi = $hondok->nmspesialisasi;
      $nmstdokter = $hondok->nmstdokter;
      $nmstatus = $hondok->nmstatus;
      $tglawal = $this->TanggalIndo(date("Ymd",strtotime($hondok->tglawal)));
      $tglakhir = $this->TanggalIndo(date("Ymd",strtotime($hondok->tglakhir)));
      $tglawalparams = $hondok->tglawal;
      $tglakhirparams = $hondok->tglakhir;
      
      $norek = $hondok->norek;
      $nmbank = $hondok->nmbank;
      $atasnama = $hondok->atasnama;
      $notelp = $hondok->notelp;
      $nohp = $hondok->nohp;
      $nmlengkap = $hondok->nmlengkap;
      $tglinput = $this->TanggalIndo(date("Ymd",strtotime($hondok->tglinput)));
      $approval = $hondok->approval;
      $nmstbayar = $hondok->nmstbayar;
      $nmjnspembayaran = $hondok->nmjnspembayaran;
      $jasadrjaga = number_format(ceil($hondok->jasadrjaga) , 0 , ',' , '.' );
      $jasadrjagaval = ceil($hondok->jasadrjaga);
      $potonganlain = number_format(ceil($hondok->potonganlain) , 0 , ',' , '.' );
      $potonganlainval = ceil($hondok->potonganlain);
      $catatan = $hondok->catatan;
      $total123 = 0;
			
			$data['header1'] = 'PENERIMAAN RAWAT JALAN / UGD';
			$data['header2'] = 'PENERIMAAN RAWAT INAP / RI';
			$data['header3'] = 'PENERIMAAN TINDAKAN TIM MEDIS';
			
	    //list of style
	    $headergrid = array(
	      'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
	      ),
	      'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	      ),
	      'font'  => array(
	        'bold'  => true,
	      )
	    );

	    $contentgrid = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )
	    );

	    // Set document properties
	    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
	         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
	         ->setTitle("Honor Dokter")
	         ->setSubject("Honor Dokter :".$nohondok);

	    $this->lib_phpexcel->getActiveSheet()->setTitle('Honor Dokter'); // set worksheet name


	    //set title
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:L1'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B1', 'Detail Jasa Medis');

	    //set subtitle
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:L2'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B2', 'Nama : '.$nmdoktergelar);

	    //set width of column
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('a')->setWidth(4);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(16);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('J')->setWidth(16);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('L')->setWidth(16);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('N')->setWidth(16);

	    $active_row = 4;

	    // Rawat Jalan
			$hd_rj = $this->get_hd_rj($iddokter,$tglawalparams,$tglakhirparams);
			if(!empty($hd_rj)){
				//set subtitle
				$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:L{$active_row}"); //merge cell
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
				$this->lib_phpexcel->setActiveSheetIndex(0)
					 ->setCellValue("B{$active_row}", 'PENERIMAAN RAWAT JALAN / UGD');

				$active_row++;

					//set header grid
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:L{$active_row}")->applyFromArray($headergrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)
					 ->setCellValue("B{$active_row}", 'No.')
					 ->setCellValue("C{$active_row}", 'No. Registrasi')
					 ->setCellValue("D{$active_row}", 'Tgl. Kuitansi')
					 ->setCellValue("E{$active_row}", 'No. RM')
					 ->setCellValue("F{$active_row}", 'Nama Pasien')
					 ->setCellValue("G{$active_row}", 'Pemeriksaan')
					 ->setCellValue("H{$active_row}", 'Tindakan')
					 ->setCellValue("I{$active_row}", 'Total Pemeriksaan dan Tindakan')
					 ->setCellValue("J{$active_row}", 'Diskon')
					 ->setCellValue("K{$active_row}", 'ZIS')
					 ->setCellValue("L{$active_row}", 'Di Terima');
				$active_row += 1; // set active row to the next row

				$no = 1;
				$sumpemeriksaan = 0;
				$sumtindakan = 0;
				$sumjasamedis = 0;
				$sumdiskon = 0;
				$sumzis = 0;
				$sumjumlah = 0;
				foreach ($hd_rj as $itemrj) {
					//set content grid
					$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:L{$active_row}")->applyFromArray($contentgrid);
					$this->lib_phpexcel->setActiveSheetIndex(0)
					   ->setCellValue("B{$active_row}", ($no++))
					   ->setCellValue("C{$active_row}", $itemrj->noreg)
					   ->setCellValue("D{$active_row}", date("d/m/Y",strtotime($itemrj->tglkuitansi)))
					   ->setCellValue("E{$active_row}", $itemrj->norm)
					   ->setCellValue("F{$active_row}", $itemrj->nmpasien)
					   ->setCellValue("G{$active_row}", ceil($itemrj->pemeriksaan))
					   ->setCellValue("H{$active_row}", ceil($itemrj->tindakan))
					   ->setCellValue("I{$active_row}", ceil($itemrj->jasamedis))
					   ->setCellValue("J{$active_row}", ceil($itemrj->diskonmedis))
					   ->setCellValue("K{$active_row}", ceil($itemrj->zis))
					   ->setCellValue("L{$active_row}", ceil($itemrj->jumlah));
					$active_row += 1; // set active row to the next row

							$sumpemeriksaan = $sumpemeriksaan + ceil($itemrj->pemeriksaan);
							$sumtindakan = $sumtindakan + ceil($itemrj->tindakan);
							$sumjasamedis = $sumjasamedis + ceil($itemrj->jasamedis);
							$sumdiskon = $sumdiskon + ceil($itemrj->diskonmedis);
							$sumzis = $sumzis + ceil($itemrj->zis);
							$sumjumlah = $sumjumlah + ceil($itemrj->jumlah);
				}

				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:L{$active_row}")->applyFromArray($contentgrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:F{$active_row}"); //merge cell
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
				$this->lib_phpexcel->setActiveSheetIndex(0)
					 ->setCellValue("B{$active_row}", 'Total 1 : ')
					 ->setCellValue("G{$active_row}", $sumpemeriksaan)
					 ->setCellValue("H{$active_row}", $sumtindakan)
					 ->setCellValue("I{$active_row}", $sumjasamedis)
					 ->setCellValue("J{$active_row}", $sumdiskon)
					 ->setCellValue("K{$active_row}", $sumzis)
					 ->setCellValue("L{$active_row}", $sumjumlah);

					$total123 = $total123 + ceil($sumjumlah);
					$total123val = $total123;

					$active_row += 2;
			}

			// Rawat Inap
			$hd_ri = $this->get_hd_ri($iddokter,$tglawalparams,$tglakhirparams);
			if(!empty($hd_ri)){
				//set subtitle
		    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:L{$active_row}"); //merge cell
		    $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
		    $this->lib_phpexcel->setActiveSheetIndex(0)
		         ->setCellValue("B{$active_row}", 'PENERIMAAN RAWAT INAP / RI');

		    $active_row++;

		    //set header grid
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:N{$active_row}")->applyFromArray($headergrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'No.')
             ->setCellValue("C{$active_row}", 'No. Registrasi')
             ->setCellValue("D{$active_row}", 'Tgl Masuk')
             ->setCellValue("E{$active_row}", 'Tgl Keluar')
             ->setCellValue("F{$active_row}", 'No. RM')
             ->setCellValue("G{$active_row}", 'Nama Pasien')
             ->setCellValue("H{$active_row}", 'Ruangan')
             ->setCellValue("I{$active_row}", 'Visite')
             ->setCellValue("J{$active_row}", 'Tindakan')
             ->setCellValue("K{$active_row}", 'Total Visite dan Tindakan')
             ->setCellValue("L{$active_row}", 'Diskon')
             ->setCellValue("M{$active_row}", 'ZIS')
             ->setCellValue("N{$active_row}", 'Di Terima');

        $active_row += 1; // set active row to the next row

				$no = 1;
				$sumpemeriksaan = 0;
				$sumtindakan = 0;
				$sumjasamedis = 0;
				$sumdiskon = 0;
				$sumzis = 0;
				$sumjumlah = 0;
				foreach ($hd_ri as $itemri) {
					//set content grid
          $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:N{$active_row}")->applyFromArray($contentgrid);
          $this->lib_phpexcel->setActiveSheetIndex(0)
               ->setCellValue("B{$active_row}", ($no++))
               ->setCellValue("C{$active_row}", $itemri->noreg)
               ->setCellValue("D{$active_row}", date("d/m/Y",strtotime($itemri->tglmasuk)))
               ->setCellValue("E{$active_row}", date("d/m/Y",strtotime($itemri->tglkuitansi)))
               ->setCellValue("F{$active_row}", $itemri->norm)
               ->setCellValue("G{$active_row}", $itemri->nmpasien)
               ->setCellValue("H{$active_row}", $itemri->nmbagian)
               ->setCellValue("I{$active_row}", ceil($itemri->pemeriksaan))
               ->setCellValue("J{$active_row}", ceil($itemri->tindakan))
               ->setCellValue("K{$active_row}", ceil($itemri->jasamedis))
               ->setCellValue("L{$active_row}", ceil($itemri->diskonmedis))
               ->setCellValue("M{$active_row}", ceil($itemri->zis))
               ->setCellValue("N{$active_row}", ceil($itemri->jumlah));
          $active_row += 1; // set active row to the next row

					$sumpemeriksaan = $sumpemeriksaan + ceil($itemri->pemeriksaan);
					$sumtindakan = $sumtindakan + ceil($itemri->tindakan);
					$sumjasamedis = $sumjasamedis + ceil($itemri->jasamedis);
					$sumdiskon = $sumdiskon + ceil($itemri->diskonmedis);
					$sumzis = $sumzis + ceil($itemri->zis);
					$sumjumlah = $sumjumlah + ceil($itemri->jumlah);
				}

        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:N{$active_row}")->applyFromArray($contentgrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:H{$active_row}"); //merge cell
		    $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
		    $this->lib_phpexcel->setActiveSheetIndex(0)
		         ->setCellValue("B{$active_row}", 'Total 2 : ')
		         ->setCellValue("I{$active_row}", $sumpemeriksaan)
		         ->setCellValue("J{$active_row}", $sumtindakan)
		         ->setCellValue("K{$active_row}", $sumjasamedis)
		         ->setCellValue("L{$active_row}", $sumdiskon)
		         ->setCellValue("M{$active_row}", $sumzis)
		         ->setCellValue("N{$active_row}", $sumjumlah);

				$total123 = $total123 + ceil($sumjumlah);
				$total123val = $total123;

				$active_row += 2;
			}

			// Jasa Medis
			$hd_jm = $this->get_hd_jm($iddokter,$tglawalparams,$tglakhirparams);
			if(!empty($hd_jm)){
				//set subtitle
				$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:J{$active_row}"); //merge cell
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
				$this->lib_phpexcel->setActiveSheetIndex(0)
					 ->setCellValue("B{$active_row}", 'PENERIMAAN TINDAKAN TIM MEDIS');
					$active_row++;

				//set header grid
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:J{$active_row}")->applyFromArray($headergrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)
					 ->setCellValue("B{$active_row}", 'No.')
					 ->setCellValue("C{$active_row}", 'No. Registrasi')
					 ->setCellValue("D{$active_row}", 'Tgl Kuitansi')
					 ->setCellValue("E{$active_row}", 'No. RM')
					 ->setCellValue("F{$active_row}", 'Nama Pasien')
					 ->setCellValue("G{$active_row}", 'Jasa Medis')
					 ->setCellValue("H{$active_row}", 'Diskon')
					 ->setCellValue("I{$active_row}", 'ZIS')
					 ->setCellValue("J{$active_row}", 'Di Terima');
						$active_row++;

						$no = 1;
						$sumjasamedis = 0;
						$sumdiskon = 0;
						$sumzis = 0;
						$sumjumlah = 0;
						foreach ($hd_jm as $itemjm) {

							//set content grid
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:J{$active_row}")->applyFromArray($contentgrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)
					   ->setCellValue("B{$active_row}", ($no++))
					   ->setCellValue("C{$active_row}", $itemjm->noreg)
					   ->setCellValue("D{$active_row}", date("d/m/Y",strtotime($itemjm->tglkuitansi)))
					   ->setCellValue("E{$active_row}", $itemjm->norm)
					   ->setCellValue("F{$active_row}", $itemjm->nmpasien)
					   ->setCellValue("G{$active_row}", ceil($itemjm->jasamedis))
					   ->setCellValue("H{$active_row}", ceil($itemjm->diskonmedis))
					   ->setCellValue("I{$active_row}", ceil($itemjm->zis))
					   ->setCellValue("J{$active_row}", ceil($itemjm->jumlah));
				  $active_row += 1; // set active row to the next row

							$sumjasamedis = $sumjasamedis + ceil($itemjm->jasamedis);
							$sumdiskon = $sumdiskon + ceil($itemjm->diskonmedis);
							$sumzis = $sumzis + ceil($itemjm->zis);
							$sumjumlah = $sumjumlah + ceil($itemjm->jumlah);
						}

				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:J{$active_row}")->applyFromArray($contentgrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:F{$active_row}"); //merge cell
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
				$this->lib_phpexcel->setActiveSheetIndex(0)
						 ->setCellValue("B{$active_row}", 'Total 3 : ')
						 ->setCellValue("G{$active_row}", $sumjasamedis)
						 ->setCellValue("H{$active_row}", $sumdiskon)
						 ->setCellValue("I{$active_row}", $sumzis)
						 ->setCellValue("J{$active_row}", $sumjumlah);

						$total123 = $total123 + ceil($sumjumlah);
						$total123val = ceil($total123);
						$totalterima = number_format((ceil($total123val) + ceil($jasadrjagaval) - ceil($potonganlainval)) , 0 , ',' , '.' );
						
						$total123 = number_format($total123, 0 , ',' , '.' );
			}
			// Pelayanan Tambahan
			$hd_peltam = $this->get_hd_peltam($iddokter,$tglawalparams,$tglakhirparams);
			if(!empty($hd_peltam)){
				//set subtitle
				$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:I{$active_row}"); //merge cell
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
				$this->lib_phpexcel->setActiveSheetIndex(0)
					 ->setCellValue("B{$active_row}", 'PENERIMAAN PELAYANAN TAMBAHAN');
					$active_row++;

				//set header grid
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:I{$active_row}")->applyFromArray($headergrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)
					 ->setCellValue("B{$active_row}", 'No.')
					 ->setCellValue("C{$active_row}", 'No. Nota')
					 ->setCellValue("D{$active_row}", 'Tgl Kuitansi')
					 ->setCellValue("E{$active_row}", 'Nama Pasien')
					 ->setCellValue("F{$active_row}", 'Jasa Medis')
					 ->setCellValue("G{$active_row}", 'Diskon')
					 ->setCellValue("H{$active_row}", 'ZIS')
					 ->setCellValue("I{$active_row}", 'Di Terima');
						$active_row++;

						$no = 1;
						$sumjasamedis = 0;
						$sumdiskon = 0;
						$sumzis = 0;
						$sumjumlah = 0;
						foreach ($hd_peltam as $itempeltam) {

							//set content grid
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:I{$active_row}")->applyFromArray($contentgrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)
					   ->setCellValue("B{$active_row}", ($no++))
					   ->setCellValue("C{$active_row}", $itempeltam->nonota)
					   ->setCellValue("D{$active_row}", date("d/m/Y",strtotime($itempeltam->tglkuitansi)))
					   ->setCellValue("E{$active_row}", $itempeltam->nmpasien)
					   ->setCellValue("F{$active_row}", ceil($itempeltam->jasamedis))
					   ->setCellValue("G{$active_row}", ceil($itempeltam->diskonmedis))
					   ->setCellValue("H{$active_row}", ceil($itempeltam->zis))
					   ->setCellValue("I{$active_row}", ceil($itempeltam->jumlah));
				  $active_row += 1; // set active row to the next row

							$sumjasamedis = $sumjasamedis + ceil($itempeltam->jasamedis);
							$sumdiskon = $sumdiskon + ceil($itempeltam->diskonmedis);
							$sumzis = $sumzis + ceil($itempeltam->zis);
							$sumjumlah = $sumjumlah + ceil($itempeltam->jumlah);
						}

				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:I{$active_row}")->applyFromArray($contentgrid);
				$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:E{$active_row}"); //merge cell
				$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
				$this->lib_phpexcel->setActiveSheetIndex(0)
						 ->setCellValue("B{$active_row}", 'Total 4 : ')
						 ->setCellValue("F{$active_row}", $sumjasamedis)
						 ->setCellValue("G{$active_row}", $sumdiskon)
						 ->setCellValue("H{$active_row}", $sumzis)
						 ->setCellValue("I{$active_row}", $sumjumlah);

						$total123 = $total123 + ceil($sumjumlah);
						$total123val = ceil($total123);
						$totalterima = number_format((ceil($total123val) + ceil($jasadrjagaval) - ceil($potonganlainval)) , 0 , ',' , '.' );
						
						$total123 = number_format($total123, 0 , ',' , '.' );
			}


	    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
	    $this->lib_phpexcel->setActiveSheetIndex(0);

	    $filename = "honor_dokter_".strtolower(str_replace(' ', '_', $nohondok));

	    // Redirect output to a client’s web browser (Excel5)
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
	    header('Cache-Control: max-age=0');
	    // If you're serving to IE 9, then the following may be needed
	    header('Cache-Control: max-age=1');

	    // If you're serving to IE over SSL, then the following may be needed
	    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	    header ('Pragma: public'); // HTTP/1.0

	    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
	    $objWriter->save('php://output');
	    exit;

	  }
	  
	function eksport_honordokterrekap($nohondok){
			$hondok = $this->db->query("SELECT * FROM v_hondok WHERE nohondok='$nohondok'")->row();
			$nohondok = $hondok->nohondok;
			$tglhondok = $this->TanggalIndo(date("Ymd",strtotime($hondok->tglhondok)));
			$iddokter = $hondok->iddokter;
			$nmdoktergelar = $hondok->nmdoktergelar;
			$tglawalparams = $hondok->tglawal;
			$tglakhirparams = $hondok->tglakhir;
      $nmspesialisasi = $hondok->nmspesialisasi;
      $nmstdokter = $hondok->nmstdokter;
      $nmstatus = $hondok->nmstatus;
      $tglawal = $this->TanggalIndo(date("Ymd",strtotime($hondok->tglawal)));
      $tglakhir = $this->TanggalIndo(date("Ymd",strtotime($hondok->tglakhir)));
      $tglawalparams = $hondok->tglawal;
      $tglakhirparams = $hondok->tglakhir;
      
      $norek = $hondok->norek;
      $nmbank = $hondok->nmbank;
      $atasnama = $hondok->atasnama;
      $notelp = $hondok->notelp;
      $nohp = $hondok->nohp;
      $nmlengkap = $hondok->nmlengkap;
      $tglinput = $this->TanggalIndo(date("Ymd",strtotime($hondok->tglinput)));
      $approval = $hondok->approval;
      $nmstbayar = $hondok->nmstbayar;
      $nmjnspembayaran = $hondok->nmjnspembayaran;
      $jasadrjaga = number_format(ceil($hondok->jasadrjaga) , 0 , ',' , '.' );
      $jasadrjagaval = ceil($hondok->jasadrjaga);
      $potonganlain = number_format(ceil($hondok->potonganlain) , 0 , ',' , '.' );
      $potonganlainval = ceil($hondok->potonganlain);
      $catatan = $hondok->catatan;
	  $tgldikeluarkan = $this->TanggalIndo(date("Ymd",strtotime($hondok->tgldikeluarkan)));
      $total123 = 0;
	  $total123val = 0;
	  
	  $bulan = $this->namaBulanTahun($tglakhirparams);
						
	    //list of style
	    $headergrid = array(
	      'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
	      ),
	      'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	      ),
	      'font'  => array(
	        'bold'  => true,
	      )
	    );

	    $contentgrid = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )
	    );

	    // Set document properties
	    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
	         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
	         ->setTitle("Jasa Medis")
	         ->setSubject("Jasa Medis :".$nohondok);

	    $this->lib_phpexcel->getActiveSheet()->setTitle('Jasa Medis'); // set worksheet name


	    //set title
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:H1'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B1', 'Rekap Jasa Medis');

	    //set subtitle
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B3:B3'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(19);
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B3', 'Bulan / Periode');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C3:C3'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C3', ':');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D3:H3'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D3', ''.$bulan. ' / ('.$tglawal.' s.d '.$tglakhir.')');
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B4:B4'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(19);
		$this->lib_phpexcel->getActiveSheet()->getStyle("B4")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B4', 'Nama');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C4:C4'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C4")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C4', ':');
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D4:H4'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("D4")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D4', ''.$nmdoktergelar);
			  
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B5:B5'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(19);
		$this->lib_phpexcel->getActiveSheet()->getStyle("B5")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
		$this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B5', 'Spesialisasi');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C5:C5'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C5")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C5', ':');
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D5:H5'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("D5")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D5', ''.$nmspesialisasi);
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B6:B6'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(19);
		$this->lib_phpexcel->getActiveSheet()->getStyle("B6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
		$this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B6', 'Jenis Pembayaran');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C6:C6'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C6', ':');
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D6:H6'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("D6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D6', ''.$nmjnspembayaran);
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B7:B7'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(19);
		$this->lib_phpexcel->getActiveSheet()->getStyle("B7")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
		$this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B7', 'Nomor');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C7:C7'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C7")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C7', ':');
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D7:H7'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("D7")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D7', ''.$nohondok);
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C9:H9'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("C9")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C9', 'Daftar Pendapatan');			 
		
	    //set width of column
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(33);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);

	    $active_row = 9;

	    // Rawat Jalan		
		//$hd_rj = $this->get_hd_rj($iddokter,$tglawalparams,$tglakhirparams);
		//if(!empty($hd_rj)){
			$active_row++;

				//set header grid
			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}:H{$active_row}")->applyFromArray($headergrid);
			$this->lib_phpexcel->setActiveSheetIndex(0)
				 ->setCellValue("C{$active_row}", 'No')
				 ->setCellValue("D{$active_row}", 'Pendapatan')
				 ->setCellValue("E{$active_row}", 'Jasa Medis')
				 ->setCellValue("F{$active_row}", 'Diskon')
				 ->setCellValue("G{$active_row}", 'ZIS')
				 ->setCellValue("H{$active_row}", 'Jumlah');
			$active_row += 1; // set active row to the next row
			
			$no = 1;			
			$hd_rj = $this->get_hd_rj($iddokter,$tglawalparams,$tglakhirparams);
			
			$sumjasamedis = 0;
			$sumdiskon = 0;
			$sumzis = 0;
			$sumjumlah1 = 0;
			foreach ($hd_rj as $itemrj) {
				$sumjasamedis = $sumjasamedis + ceil($itemrj->jasamedis);
				$sumdiskon = $sumdiskon + ceil($itemrj->diskonmedis);
				$sumzis = $sumzis + ceil($itemrj->zis);
				$sumjumlah1 = $sumjumlah1 + ceil($itemrj->jumlah);
			}

			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}:H{$active_row}")->applyFromArray($contentgrid);
			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
			$this->lib_phpexcel->setActiveSheetIndex(0)
				 ->setCellValue("C{$active_row}", $no++)
				 ->setCellValue("D{$active_row}", 'Rawat Jalan / UGD')
				 ->setCellValue("E{$active_row}", $sumjasamedis)
				 ->setCellValue("F{$active_row}", $sumdiskon)
				 ->setCellValue("G{$active_row}", $sumzis)
				 ->setCellValue("H{$active_row}", $sumjumlah1);				 

				$active_row += 1;
			
			$hd_ri = $this->get_hd_ri($iddokter,$tglawalparams,$tglakhirparams);
			
			$sumjasamedis = 0;
			$sumdiskon = 0;
			$sumzis = 0;
			$sumjumlah2 = 0;
			foreach ($hd_ri as $itemri) {
				$sumjasamedis = $sumjasamedis + ceil($itemri->jasamedis);
				$sumdiskon = $sumdiskon + ceil($itemri->diskonmedis);
				$sumzis = $sumzis + ceil($itemri->zis);
				$sumjumlah2 = $sumjumlah2 + ceil($itemri->jumlah);
			}

			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}:H{$active_row}")->applyFromArray($contentgrid);
			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
			$this->lib_phpexcel->setActiveSheetIndex(0)
				 ->setCellValue("C{$active_row}", $no++)
				 ->setCellValue("D{$active_row}", 'Rawat Inap')
				 ->setCellValue("E{$active_row}", $sumjasamedis)
				 ->setCellValue("F{$active_row}", $sumdiskon)
				 ->setCellValue("G{$active_row}", $sumzis)
				 ->setCellValue("H{$active_row}", $sumjumlah2);				 

				$active_row += 1;
				
			$hd_jm = $this->get_hd_jm($iddokter,$tglawalparams,$tglakhirparams);		
			
			$sumjasamedis = 0;
			$sumdiskon = 0;
			$sumzis = 0;
			$sumjumlah3 = 0;
			foreach ($hd_jm as $itemjm) {
				$sumjasamedis = $sumjasamedis + ceil($itemjm->jasamedis);
				$sumdiskon = $sumdiskon + ceil($itemjm->diskonmedis);
				$sumzis = $sumzis + ceil($itemjm->zis);
				$sumjumlah3 = $sumjumlah3 + ceil($itemjm->jumlah);
			}

			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}:H{$active_row}")->applyFromArray($contentgrid);
			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
			$this->lib_phpexcel->setActiveSheetIndex(0)
				 ->setCellValue("C{$active_row}", $no++)
				 ->setCellValue("D{$active_row}", 'Tindakan Tim Medis')
				 ->setCellValue("E{$active_row}", $sumjasamedis)
				 ->setCellValue("F{$active_row}", $sumdiskon)
				 ->setCellValue("G{$active_row}", $sumzis)
				 ->setCellValue("H{$active_row}", $sumjumlah3);				 

				$active_row += 1;
			
			$hd_peltam = $this->get_hd_peltam($iddokter,$tglawalparams,$tglakhirparams);		
			
			$sumjasamedis = 0;
			$sumdiskon = 0;
			$sumzis = 0;
			$sumjumlah4 = 0;
			foreach ($hd_peltam as $itempeltam) {
				$sumjasamedis = $sumjasamedis + ceil($itempeltam->jasamedis);
				$sumdiskon = $sumdiskon + ceil($itempeltam->diskonmedis);
				$sumzis = $sumzis + ceil($itempeltam->zis);
				$sumjumlah4 = $sumjumlah3 + ceil($itempeltam->jumlah);
			}

			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}:H{$active_row}")->applyFromArray($contentgrid);
			$this->lib_phpexcel->getActiveSheet()->getStyle("C{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
			$this->lib_phpexcel->setActiveSheetIndex(0)
				 ->setCellValue("C{$active_row}", $no++)
				 ->setCellValue("D{$active_row}", 'Pelayanan Tambahan')
				 ->setCellValue("E{$active_row}", $sumjasamedis)
				 ->setCellValue("F{$active_row}", $sumdiskon)
				 ->setCellValue("G{$active_row}", $sumzis)
				 ->setCellValue("H{$active_row}", $sumjumlah4);				 

				$active_row += 1;
			
			$total123val = $total123val + ceil($sumjumlah1) + ceil($sumjumlah2) + ceil($sumjumlah3) + ceil($sumjumlah4);
			$totalterima = number_format((ceil($total123val) + ceil($jasadrjagaval) - ceil($potonganlainval)) , 0 , ',' , '.' );			
			$totalterbilang = $this->rp_terbilang(ceil($total123val) + ceil($jasadrjagaval) - ceil($potonganlainval), 0);
			
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('G15:G15'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$this->lib_phpexcel->getActiveSheet()->getStyle("G15")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('G15', 'Jasa Piket');
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('H15:H15'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("H15")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('H15', '' .$jasadrjaga);
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('G16:G16'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("G16")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('G16', 'Potongan Lainnya');
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('H16:H16'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("H16")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('H16', '' .$potonganlain);
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('G17:G17'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("G17")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('G17', 'Jumlah Yang Diterima');
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('H17:H17'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("H17")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('H17', '' .$total123val);
			 
		$this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C19:H19'); //merge cell
		$this->lib_phpexcel->getActiveSheet()->getStyle("C19")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C19', 'Terbilang : ' .$totalterbilang);			 
		
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B21:B21'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B21")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B21', 'No Rekening');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C21:C21'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C21")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C21', ':');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D21:H21'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D21")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D21', ''.$norek);
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B22:B22'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B22")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B22', 'Bank');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C22:C22'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C22")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C22', ':');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D22:H22'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D22")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D22', ''.$nmbank);
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B23:B23'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B23")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B23', 'Atas Nama');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C23:C23'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C23")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C23', ':');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D23:H23'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D23")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D23', ''.$atasnama);
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B24:B24'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B24")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B24', 'No. Telepon');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C24:C24'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C24")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C24', ':');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D24:H24'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D24")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D24', ''.$notelp);
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B26:B26'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("B26")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('B26', 'Catatan');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('C26:C26'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("C26")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('C26', ':');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D26:H26'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D26")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D26', ''.$catatan);
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D28:D28'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D28")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D28', 'Bandung, ' .$tgldikeluarkan);
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D29:D29'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D29")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D29', 'Dikeluarkan Oleh :');
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D34:D34'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D34")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D34', ''.$approval);
			 
	    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D35:D35'); //merge cell
	    $this->lib_phpexcel->getActiveSheet()->getStyle("D35")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
	    $this->lib_phpexcel->setActiveSheetIndex(0)
	         ->setCellValue('D35', '(Keuangan)');
			
		//}
		
	    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
	    $this->lib_phpexcel->setActiveSheetIndex(0);

	    $filename = "honor_dokter_rkp_".strtolower(str_replace(' ', '_', $nohondok));

	    // Redirect output to a client’s web browser (Excel5)
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
	    header('Cache-Control: max-age=0');
	    // If you're serving to IE 9, then the following may be needed
	    header('Cache-Control: max-age=1');

	    // If you're serving to IE over SSL, then the following may be needed
	    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	    header ('Pragma: public'); // HTTP/1.0

	    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
	    $objWriter->save('php://output');
	    exit;

	  }

	}

?>