<?php

class Lap_buku_besar extends Controller{
  function __construct(){
    parent::__construct();
    $this->load->library('pdf_lap');
    $this->load->library('lib_phpexcel');
  }
  
  function pdf($tglawal_transaksi, $tglakhir_transaksi, $tglawal_input, $tglakhir_input)
  {    
    $isi = '<br/><font size=\"7\" face=\"Helvetica\">';       
        
    //get akun
    $this->db->select('distinct(idakun), nmakun, kdakun');
    $this->db->orderby('nmakun', 'ASC');
    $this->db->from('v_lap_bukubesar');
    $this->_set_search_query($tglawal_transaksi, $tglakhir_transaksi, $tglawal_input, $tglakhir_input, '');
    
    $q = $this->db->get();
    $data_akun = array();
    if ($q->num_rows() > 0) {
        $data_akun = $q->result_array();
    }

    if(!empty($data_akun)){
      foreach($data_akun as $idx => $dt_akun){
        $this->db->select('*');
        $this->db->orderby('nmakun', 'ASC');
        $this->db->from('v_lap_bukubesar');
        $this->_set_search_query($tglawal_transaksi, $tglakhir_transaksi, $tglawal_input, $tglakhir_input, $dt_akun['idakun']);
        $get_detail_jurnal = $this->db->get();
        $data_akun[$idx]['detail_jurnal'] = $get_detail_jurnal->result_array();
      }
    }
    
    // add a page
    $page_format = array(
      'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      'Rotate' => 0,
      'PZ' => 1,
    );
    
    //Set Footer
    $this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
    $this->pdf_lap->setPrintFooter(true); // enabled ? true
    $this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $this->pdf_lap->SetAutoPageBreak(TRUE, '15');
    
    $this->pdf_lap->SetPrintHeader(false);
    $this->pdf_lap->AddPage('L', $page_format, false, false);
    $this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
    $this->pdf_lap->SetFont('helvetica', '', 14);
    
    $x=0;$y=10;
    $this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
    $this->pdf_lap->Cell(0, 0, 'Buku Besar', 0, 1, 'C', 0, '', 0);
    
    if($tglawal_transaksi != '-' && $tglakhir_transaksi != '-'){
      $this->pdf_lap->SetFont('helvetica', '', 10);
      if($tglawal_transaksi == $tglakhir_transaksi){
        $this->pdf_lap->Cell(0, 0, 'Pertanggal transaksi : '. date("d F Y", strtotime($tglawal_transaksi)), 0, 1, 'C', 0, '', 0);
      }else{
        $this->pdf_lap->Cell(0, 0, 'Periode tanggal transaksi : '. date("d F Y", strtotime($tglawal_transaksi)) .' - '.date("d F Y", strtotime($tglakhir_transaksi)), 0, 1, 'C', 0, '', 0);
      }
    }
    
    if($tglawal_input != '-' && $tglakhir_input != '-'){
      $this->pdf_lap->SetFont('helvetica', '', 10);
      if($tglawal_input == $tglakhir_input){
        $this->pdf_lap->Cell(0, 0, 'Pertanggal jurnal : '. date("d F Y", strtotime($tglawal_input)), 0, 1, 'C', 0, '', 0);
      }else{
        $this->pdf_lap->Cell(0, 0, 'Periode tanggal jurnal : '. date("d F Y", strtotime($tglawal_input)) .' - '.date("d F Y", strtotime($tglakhir_input)), 0, 1, 'C', 0, '', 0);
      }
    }

    $no = 1;
    $totalsemuaakundebit = 0;
    $totalsemuaakunkredit = 0;
    foreach($data_akun as $i=> $dt){
      $totalakundebit = 0;
      $totalakunkredit = 0;
      $table_akun ='
      <br><br>
      '. $dt['nmakun'] .' ('. $dt['kdakun'] .')
      <br>
      <table width="100%" border="1">
        <tr>
          <td width="10%" align="center"><strong>Kode Jurnal</strong></td>
          <td width="10%" align="center"><strong>Tanggal<br>Transaksi</strong></td>
          <td width="10%;" align="center"><strong>Tanggal<br>Jurnal</strong></td>     
          <td width="15%" align="center"><strong>No. Reff / No. Bon</strong></td>
          <td width="30%" align="center"><strong>Keterangan</strong></td>
          <td width="12%" align="center"><strong>Debit</strong></td>
          <td width="12%;" align="center"><strong>Kredit</strong></td>
        </tr>';

      if(!empty($dt['detail_jurnal'])){
        foreach($dt['detail_jurnal'] as $idxdetail => $detail_jurnal){
          $totalakundebit += $detail_jurnal['debit'];
          $totalakunkredit += $detail_jurnal['kredit'];
          $tgl_transaksi_j =  date("d/m/Y", strtotime($detail_jurnal['tgltransaksi']));
          $tgl_jurnal_j =  date("d/m/Y", strtotime($detail_jurnal['tgljurnal']));
          $debit_j = number_format($detail_jurnal['debit'],0,',','.');
          $kredit_j = number_format($detail_jurnal['kredit'],0,',','.');
          $table_akun .='<tr>
            <td align="center">'. $detail_jurnal["kdjurnal"] .'</td>
            <td align="center">'. $tgl_transaksi_j .'</td>
            <td align="center">'. $tgl_jurnal_j .'</td>
            <td align="center">'. $detail_jurnal["noreff"] .'</td>
            <td>&nbsp; '. $detail_jurnal["keterangan"] .'</td>
            <td align="right">Rp. '. $debit_j .' &nbsp;</td>
            <td align="right">Rp. '. $kredit_j .' &nbsp;</td>
          </tr>';
        }

      }
      
      $totdebit_j = number_format($totalakundebit,0,',','.');
      $totkredit_j = number_format($totalakunkredit,0,',','.');
        
      $table_akun .='
        <tr>
          <td colspan="5" align="right"><strong>Total</strong> &nbsp;</td>
          <td align="right"><strong>Rp. '. $totdebit_j .'</strong> &nbsp;</td>
          <td align="right"><strong>Rp. '. $totkredit_j .'</strong> &nbsp;</td>
        </tr>
      </table>';
      $isi .= $table_akun;

      //kalkulasi debit & kredit di semua akun
      $totalsemuaakundebit += $totalakundebit;
      $totalsemuaakunkredit += $totalakunkredit;
    }
    
    $totsemuadebit_j = number_format($totalsemuaakundebit,0,',','.');
    $totsemuakredit_j = number_format($totalsemuaakunkredit,0,',','.');
    $isi .='<br><br>
      <table width="100%" border="1">
        <tr>
          <td width="75%" align="right"><strong>Total Transaksi Seluruh Akun &nbsp;</strong></td>
          <td width="12%" align="right"><strong>Rp. '. $totsemuadebit_j .' &nbsp;</strong></td>
          <td width="12%;" align="right"><strong>Rp. '. $totsemuakredit_j .' &nbsp;</strong></td>
        </tr>
      </table>';

    $isi .= "</font>";
    $this->pdf_lap->writeHTML($isi,true,false,false,false);

    //Close and output PDF document
    $this->pdf_lap->Output('lap_buku_besar.pdf', 'I');
  }


  function excel($tglawal_transaksi, $tglakhir_transaksi, $tglawal_input, $tglakhir_input)
  {
    //get akun
    $this->db->select('distinct(idakun), nmakun, kdakun');
    $this->db->orderby('nmakun', 'ASC');
    $this->db->from('v_lap_bukubesar');
    $this->_set_search_query($tglawal_transaksi, $tglakhir_transaksi, $tglawal_input, $tglakhir_input, '');
    
    $q = $this->db->get();
    $data_akun = array();
    if ($q->num_rows() > 0) {
        $data_akun = $q->result_array();
    }

    if(!empty($data_akun)){
      foreach($data_akun as $idx => $dt_akun){
        $this->db->select('*');
        $this->db->orderby('nmakun', 'ASC');
        $this->db->from('v_lap_bukubesar');
        $this->_set_search_query($tglawal_transaksi, $tglakhir_transaksi, $tglawal_input, $tglakhir_input, $dt_akun['idakun']);
        $get_detail_jurnal = $this->db->get();
        $data_akun[$idx]['detail_jurnal'] = $get_detail_jurnal->result_array();
      }
    }
    

    $subject_file = '';
    if($tglawal_transaksi != '-' && $tglakhir_transaksi != '-'){
      if($tglawal_transaksi == $tglakhir_transaksi){
        $subject_file = 'Pertanggal '.date("d F Y", strtotime($tglawal_transaksi));
      }else{
        $subject_file = 'Periode '. date("d F Y", strtotime($tglawal_transaksi)) .' s/d '. date("d F Y", strtotime($tglakhir_transaksi));
      }
    }

    if($tglawal_input != '-' && $tglakhir_input != '-'){
      if($tglawal_input == $tglakhir_input){
        $subject_file = 'Pertanggal '.date("d F Y", strtotime($tglawal_input));
      }else{
        $subject_file = 'Periode '. date("d F Y", strtotime($tglawal_input)) .' s/d '. date("d F Y", strtotime($tglakhir_input));
      }
    }

    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        ),
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'font'  => array(
          'bold'  => true,
          //'color' => array('rgb' => 'FF0000'),
          //'size'  => 15,
          //'name'  => 'Verdana'
        )
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Buku Besar")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Buku Besar'); // set worksheet name


    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('A1:H1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('A1', 'Buku Besar');

    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('A2:H2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("A2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('A2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(45);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);


    $active_row = 4;
    if(!empty($data_akun))
    {
      foreach($data_akun as $idx => $dt_akun)
      {
        //show akun
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue('B'.$active_row, $dt_akun['nmakun'] .' ('. $dt_akun['kdakun'] .')');
        $active_row += 1;     

        //set header grid
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($headergrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Kode Jurnal')
             ->setCellValue("C{$active_row}", 'Tanggal Transaksi')
             ->setCellValue("D{$active_row}", 'Tanggal Jurnal')
             ->setCellValue("E{$active_row}", 'No. Reff / No. Bon')
             ->setCellValue("F{$active_row}", 'Keterangan')
             ->setCellValue("G{$active_row}", 'Debit')
             ->setCellValue("H{$active_row}", 'Kredit');
        $active_row += 1; // set active row to the next row
        
        if(!empty($dt_akun['detail_jurnal']))
        {
          foreach($dt_akun['detail_jurnal'] as $idx_jrn => $detail_jurnal)
          {
            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("B{$active_row}", $detail_jurnal['kdjurnal'])
                 ->setCellValue("C{$active_row}", $detail_jurnal['tgltransaksi'])
                 ->setCellValue("D{$active_row}", $detail_jurnal['tgljurnal'])
                 ->setCellValue("E{$active_row}", $detail_jurnal['noreff'])
                 ->setCellValue("F{$active_row}", $detail_jurnal['keterangan'])
                 ->setCellValue("G{$active_row}", $detail_jurnal['debit'])
                 ->setCellValue("H{$active_row}", $detail_jurnal['kredit']);
            $active_row += 1; // set active row to the next row
          
          }
          $active_row += 1; // add new row, before create new grid

        }
        
      }

    }

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = "buku_besar_".strtolower(str_replace(' ', '_', $subject_file));

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;

  }


  function _set_search_query($tglawal_transaksi, $tglakhir_transaksi, $tglawal_input, $tglakhir_input, $akun)
  {
    if($tglawal_transaksi != '-' && $tglakhir_transaksi != '-'){
      if($tglawal_transaksi == $tglakhir_transaksi){
        $this->db->where('tgltransaksi', $tglawal_transaksi);
      }else{
        $this->db->where('tgltransaksi >=', $tglawal_transaksi);
        $this->db->where('tgltransaksi <=', $tglakhir_transaksi);
      }
    }
    
    if($tglawal_input != '-' && $tglakhir_input != '-'){
      if($tglawal_input == $tglakhir_input){
        $this->db->where('tglinput', $tglawal_input);
      }else{
        $this->db->where('tglinput >=', $tglawal_input);
        $this->db->where('tglinput <=', $tglakhir_input);
      }
    }

    if($akun != '-' && $akun != '')
      $this->db->where('idakun', $akun);
  }
}
