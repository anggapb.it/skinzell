<?php 
class Lap_rekamedis_pemakaiandarah extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf_header');
        
		}
	
	function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
    }
	
	function BulanIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$BulanIndo[(int)$bulan-1];		
            return($result);
    }
	
	function get_pemakaiandarah($tglawal,$tglakhir){

		$q = "SELECT * FROM v_pemakaiandarah WHERE tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'";
		
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$no = 1;
			$data_row = "";
			
			foreach ($data as $value) {
				$data_row .= 
				"<tr>
				  <td align=\"center\" width=\"4%\">".$no++."</td>
				  <td align=\"center\" width=\"6%\">".date("d/m/Y",strtotime($value->tglkuitansi))."</td>
				  <td align=\"center\" width=\"8%\">".$value->noreg."</td>
				  <td align=\"left\" width=\"16%\">".$value->nmpasien."</td>
				  <td align=\"left\" width=\"9%\">".$value->nmdaerah."</td>
				  <td align=\"left\" width=\"10%\">".$value->nmpenjamin."</td>
				  <td align=\"left\" width=\"20%\">".$value->nmpenyakit."</td>
				  <td align=\"center\" width=\"4%\">".$value->nmgoldarah."</td>
				  <td align=\"left\" width=\"9%\">".$value->nmpelayanan."</td>
				  <td align=\"center\" width=\"6%\">".$value->sumqty."</td>
				  <td align=\"center\" width=\"7%\"></td>
				 </tr>";
			}
			
			$data_table = <<<EOD
	<br />
    <table border="1" cellpadding="2" nobr="true">
	<thead>
     <tr>
	  <td width="4%" align="center"><b>No</b></td>
	  <td width="6%" align="center"><b>Tanggal</b></td>
	  <td width="8%" align="center"><b>No. Registrasi</b></td>
	  <td width="16%" align="center"><b>Nama</b></td>
	  <td width="9%" align="center"><b>Daerah</b></td>
	  <td width="10%" align="center"><b>Penjamin</b></td>
	  <td width="20%" align="center"><b>Diagnosa</b></td>
	  <td width="4%" align="center"><b>Gol.<br>Darah</b></td>
	  <td width="9%" align="center"><b>Jenis Darah</b></td>
      <td width="6%" align="center"><b>Jumlah<br>Pemakaian</b></td>
	  <td width="7%" align="center"><b>Keterangan</b></td>
     </tr> 
	</thead>
		$data_row
    </table>
	
EOD;

		return $data_table;

	}
	
	function pemakaiandarah($tglawal,$tglakhir){
		$bulan = $this->BulanIndo(date("d/m/Y",strtotime($tglawal)));
		
		$pdf = new Pdf_header; 
		
		$datahead['title'] = 'LAPORAN PEMAKAIAN DARAH BULAN : '.strtoupper($bulan);	
		$datahead['tahun'] = 'TAHUN : '.date("Y",strtotime($tglawal));
		
		$pdf->addHeaderParams($datahead);
		
		$pdf->SetPrintHeader(true);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins('10', '51', '10');
		$pdf->SetHeaderMargin('51');
		$pdf->SetAutoPageBreak(TRUE, '20');
		
		$pdf->AddPage('L', 'F4', false, false); 
		
        $pdf->SetFont('helvetica', '', 9);
		
		$data_pemakaiandarah = $this->get_pemakaiandarah($tglawal,$tglakhir);
		$pdf->writeHTML($data_pemakaiandarah,true,false,false,false);	
		
		$pdf->Output('laporan_pemakaian_darah.pdf', 'I');
    
       
	}
}

?>