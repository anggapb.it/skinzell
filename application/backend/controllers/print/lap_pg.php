<?php

class Lap_pg extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
	
	function namaBulan($tgl){
		$arrtgl = explode('-', $tgl);
		$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
		$bulan = $query->row_array();
		$tanggalInd = $arrtgl[2] .' '. $bulan['nmbulan'] .' '. $arrtgl[0];
		
		return $tanggalInd ;
	}
	
	
	function cetak_pg($tglawal, $tglakhir){
	
		$this->db->select("*");
        $this->db->from("v_treg1");
        $this->db->order_by("v_treg1.noreg ASC");					
		$this->db->where("date(tglkuitansi) between ", "'". $tglawal ."' AND '". $tglakhir ."'", false);

		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$this->db->select("*");
        $this->db->from("v_tagihan_farmasi");
        $this->db->order_by("v_tagihan_farmasi.nonota ASC");					
		$this->db->where("date(tglkuitansi) between ", "'". $tglawal ."' AND '". $tglakhir ."'", false);

		$q2 = $this->db->get();
		$data2 = array();
        if ($q2->num_rows() > 0) {
            $data2 = $q2->result();
        }
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		$tgl = $this->namaBulan(date('Y-m-d'));
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Tagihan Karyawan (PG)', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$this->pdf->Cell(0, 0, 'Periode : '.$tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$isi2 = '';
		$isicb = '';
		$total = 0;
		$dibayarpasien = 0;
		$sisa = 0;
		$potonggaji = 0;
		$jmlbyr = 0;
		$total2 = 0;
		$dibayarpasien2 = 0;
		$sisa2 = 0;
		$potonggaji2 = 0;
		$jmlbyr2 = 0;
		$total_all = 0;
		$dibayarpasien_all = 0;
		$sisa_all = 0;
		$potonggaji_all = 0;
		$jmlbyr_all = 0;
		
		foreach($data AS $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"8%\">". $val->noreg ."</td>
					<td width=\"10%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"8%\">". $val->norm ."</td>
					<td width=\"14%\">". $val->nmpasien ."</td>
					<td width=\"14%\">". $val->nokartu ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->total_transaksi,0,',','.') ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->total_bayar,0,',','.') ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->jml_tagihan,0,',','.') ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->jml_byr,0,',','.') ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->sisa,0,',','.') ."</td>
			</tr>";
			
			$total += $val->total_transaksi;
			$dibayarpasien += $val->total_bayar;
			$potonggaji += $val->jml_tagihan;
			$jmlbyr += $val->jml_byr;
			$sisa += $val->sisa;
			
		}
		
		$html = "<br/><br/><font size=\"9\" face=\"Helvetica\">
			<p><b>Rawat Jalan / UGD / Rawat Inap</b></p>
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"8%\"><b>No. Registrasi</b></td>
					<td width=\"10%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"8%\"><b>No. RM</b></td>
					<td width=\"14%\"><b>Nama Pasien</b></td>
					<td width=\"14%\"><b>Nama Karyawan</b></td>
					<td width=\"9%\"><b>Total Transaksi</b></td>
					<td width=\"9%\"><b>Dibayar Pasien</b></td>
					<td width=\"9%\"><b>Jumlah <br />Potong Gaji</b></td>
					<td width=\"9%\"><b>Jumlah Dibayar</b></td>
					<td width=\"9%\"><b>Sisa Tagihan</b></td>
				</tr>
			  </thead>
			  <tbody>'".$isi."'</tbody>
				<tr>
					<td  width=\"56.8%\" colspan=\"6\" align=\"center\"><b>T O T A L</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($total,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($dibayarpasien,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($potonggaji,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($jmlbyr,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($sisa,0,',','.') ."</b></td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		
		
		//GRID FARMASI PASIEN LUAR
		
		foreach($data2 AS $i=>$val){
			$isi2 .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"8%\">". $val->nonota ."</td>
					<td width=\"10%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"8%\">". $val->norm ."</td>
					<td width=\"14%\">". $val->atasnama ."</td>
					<td width=\"14%\">". $val->nokartu ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->total_transaksi,0,',','.') ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->total_bayar,0,',','.') ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->jml_tagihan,0,',','.') ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->jml_byr,0,',','.') ."</td>
					<td width=\"9%\" align=\"right\">". number_format($val->sisa,0,',','.') ."</td>
			</tr>";
			
			$total2 += $val->total_transaksi;
			$dibayarpasien2 += $val->total_bayar;
			$potonggaji2 += $val->jml_tagihan;
			$jmlbyr2 += $val->jml_byr;
			$sisa2 += $val->sisa;
			
		}
		$total_all = $total + $total2;
		$dibayarpasien_all = $dibayarpasien + $dibayarpasien2;
		$potonggaji_all = $potonggaji + $potonggaji2;
		$jmlbyr_all = $jmlbyr + $jmlbyr2;
		$sisa_all = $sisa + $sisa2;
		
		$html2 = "<br/><br/><font size=\"9\" face=\"Helvetica\">
			<p><b>Pelayanan Tambahan / Farmasi Pasien Luar</b></p>
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"8%\"><b>No. Nota</b></td>
					<td width=\"10%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"8%\"><b>No. RM</b></td>
					<td width=\"14%\"><b>Nama Pasien</b></td>
					<td width=\"14%\"><b>Nama Karyawan</b></td>
					<td width=\"9%\"><b>Total Transaksi</b></td>
					<td width=\"9%\"><b>Dibayar Pasien</b></td>
					<td width=\"9%\"><b>Jumlah <br />Potong Gaji</b></td>
					<td width=\"9%\"><b>Jumlah Dibayar</b></td>
					<td width=\"9%\"><b>Sisa Tagihan</b></td>
				</tr>
			  </thead>
			  <tbody>'".$isi2."'</tbody>
				<tr>
					<td  width=\"56.8%\" colspan=\"6\" align=\"center\"><b>T O T A L</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($total2,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($dibayarpasien2,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($potonggaji2,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($jmlbyr2,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($sisa2,0,',','.') ."</b></td>
				</tr>
				<tr>
					<td colspan=\"11\" align=\"center\">&nbsp;</td>
				</tr>
				<tr>
					<td  width=\"56.8%\" colspan=\"6\" align=\"center\"><b>T O T A L    S E M U A</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($total_all,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($dibayarpasien_all,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($potonggaji_all,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($jmlbyr_all,0,',','.') ."</b></td>
					<td width=\"9%\" align=\"right\"><b>". number_format($sisa_all,0,',','.') ."</b></td>
				</tr>
				
			</table></font>
		";
		$this->pdf->writeHTML($html2,true,false,false,false);
		
		//Close and output PDF document
		$this->pdf->Output('Tagihan_Karyawan.pdf', 'I');
	
	
	}
	
	
	function excelpg($tglawal, $tglakhir) {
		$header = array(
			'No. Registrasi',
			'Tgl. Kuitansi',
			'No. RM',
			'Nama Pasien',
			'Nama Karyawan',
			'Total Transaksi',
			'Jumlah Bayar',
			'Jumlah Potong Gaji',
			'Jumlah Dibayar',
			'Sisa Tagihan',
		);
		
		$header2 = array(
			'No. Nota',
			'Tgl. Kuitansi',
			'No. RM',
			'Nama Pasien',
			'Nama Karyawan',
			'Total Transaksi',
			'Jumlah Bayar',
			'Jumlah Potong Gaji',
			'Jumlah Dibayar',
			'Sisa Tagihan',
		);
		
		$this->db->select("*");
        $this->db->from("v_treg1");
        $this->db->order_by("v_treg1.noreg ASC");			
		$this->db->where("date(tglkuitansi) between '". $tglawal ."' and '". $tglakhir."'" );
		$query = $this->db->get();
		
		$fpl = $query->result();
        $fplnum = $query->num_rows();
		$total = 0;
		$dibayarpasien = 0;
		$potonggaji = 0;
		$jml_byr = 0;
		$sisa = 0;
		
		foreach($fpl AS $i=>$val){
			$total += $val->total_transaksi;
			$dibayarpasien += $val->total_bayar;
			$potonggaji += $val->jml_tagihan;
			$jml_byr += $val->jml_byr;
			$sisa += $val->sisa;
				
		}
		
		
		
		
		
		$this->db->select("*");
        $this->db->from("v_tagihan_farmasi");
        $this->db->order_by("v_tagihan_farmasi.nonota ASC");			
		$this->db->where("date(tglkuitansi) between '". $tglawal ."' and '". $tglakhir."'" );
		$query = $this->db->get();
		
		$fpl2 = $query->result();
        $fplnum2 = $query->num_rows();
		
		$total2 = 0;
		$dibayarpasien2 = 0;
		$potonggaji2 = 0;
		$jml_byr2 = 0;
		$sisa2 = 0;
		
		foreach($fpl2 AS $i=>$val){
			$total2 += $val->total_transaksi;
			$dibayarpasien2 += $val->total_bayar;
			$potonggaji2 += $val->jml_tagihan;
			$jml_byr2 += $val->jml_byr;
			$sisa2 += $val->sisa;
				
		}
		
		$total_all = 0;
		$dibayarpasien_all = 0;
		$potonggaji_all = 0;
		$jml_byr_all = 0;
		$sisa_all = 0;
		
		$total_all = $total + $total2;
		$dibayarpasien_all = $dibayarpasien + $dibayarpasien2;
		$potonggaji_all = $potonggaji + $potonggaji2;
		$jml_byr_all = $jml_byr + $jml_byr2;
		$sisa_all = $sisa + $sisa2;
		

		$data['eksport'] = $fpl;
		$data['eksport2'] = $fpl2;
		$data['fieldname'] = $header;
		$data['fieldname2'] = $header2;
		$data['numrows'] = $fplnum;
		$data['numrows2'] = $fplnum2;
		$data['total_transaksi'] = $total;
		$data['dibayarpasien'] = $dibayarpasien;
		$data['potonggaji'] = $potonggaji;
		$data['jml_byr'] = $jml_byr;
		$data['sisa'] = $sisa;
		$data['total_transaksi2'] = $total2;
		$data['dibayarpasien2'] = $dibayarpasien2;
		$data['potonggaji2'] = $potonggaji2;
		$data['jml_byr2'] = $jml_byr2;
		$data['sisa2'] = $sisa2;
		$data['total_all'] = $total_all;
		$data['dibayarpasien_all'] = $dibayarpasien_all;
		$data['potonggaji_all'] = $potonggaji_all;
		$data['jml_byr_all'] = $jml_byr_all;
		$data['sisa_all'] = $sisa_all;
		$this->load->view('exportexcellappg', $data); 	
	}
   
	
}	