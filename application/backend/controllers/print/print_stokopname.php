<?php 
	class Print_stokopname extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_pp');        
		}

	function stokopname_pdf($noso){
		$this->pdf_pp->SetPrintFooter(true);
		$this->db->select("*");
		$this->db->from("v_so");
		
		$this->db->where('v_so.noso', $noso);
		$query = $this->db->get();
        $noso = $query->row_array();
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 148),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => -90,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_pp->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_pp->setPrintFooter(true); // enabled ? true
		//$this->pdf_pp->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_pp->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
				
			// add a page
        $this->pdf_pp->AddPage('L');		
		//$this->pdf_pp->AddPage('L', $page_format, false, false);
        $this->pdf_pp->SetFont('helvetica', '', 9);
		$kop = "<br>
				<table border=\"0\">
					<tr align=\"left\">
						<td width=\"0.7%\"></td>
						<td width=\"99.5%\"><font size=\"13\" face=\"Helvetica\"><b>".NMKLINIK_KAPITAL."</b></font></td>
					</tr>
					<tr align=\"left\">
						<td></td>
						<td>".ALAMAT_SATU."</td>
					</tr>
					<tr align=\"left\">
						<td><font size=\"9\" face=\"Helvetica\"></font><hr height=\"2\"></td>
						<td>".ALAMAT_DUA."<hr height=\"2\"></td>
					</tr>
					<tr align=\"center\">
						<td></td>
						<td><h3><b><i>Stock Opname</i></b></h3></td>
					</tr>
				</table>
		";
     	$this->pdf_pp->writeHTML($kop,true,false,false,false);
		
		$kop2 = "<br><br>
				 <table border=\"0\">
					<tr align=\"left\">
						<td width=\"2%\"></td>
						<td width=\"20%\"><font size=\"8\" face=\"Helvetica\">NO. Stock Opname</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"37%\"><font size=\"9\" face=\"Helvetica\"><b>".$noso["noso"]."</b></font></td>
					
						
						<td width=\"12%\"><font size=\"8\" face=\"Helvetica\">Status</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"38%\"><font size=\"8\" face=\"Helvetica\">".$noso["nmstso"]."</font></td>
					</tr>
					<tr>
						<td width=\"2%\"></td>
						<td width=\"20%\"><font size=\"8\" face=\"Helvetica\">Tanggal </font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"37%\"><font size=\"8\" face=\"Helvetica\">".date("d-m-Y", strtotime($noso['tglso']))." / ".$noso["jamso"]."</font></td>
					
						
						<td width=\"12%\"><font size=\"8\" face=\"Helvetica\">User Input</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"38%\"><font size=\"8\" face=\"Helvetica\">".$noso["nmlengkap"]."</font></td>
					</tr>					
					<tr>
						<td width=\"2%\"></td>
						<td width=\"20%\"><font size=\"8\" face=\"Helvetica\">Bagian</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"37%\"><font size=\"8\" face=\"Helvetica\">".$noso["nmbagian"]."</font></td>

						<td width=\"9%\"><font size=\"8\" face=\"Helvetica\"></font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\"></font></td>
						<td width=\"38%\"><font size=\"9\" face=\"Helvetica\"></font></td>
					</tr>
				 </table>
			";
		$this->pdf_pp->writeHTML($kop2,true,false,false,false);
	//	var_dump($nopo);
				$isi = '';
				$this->db->select("*");
				$this->db->from("v_sodet");
				$this->db->where('noso',$noso['noso']);
			//	var_dump($nopo);
				$querys = $this->db->get();
				
			//	$ambil = $querys->row_array();
				$aaa = $querys->result();
				$no = 1;
				foreach($aaa as $i=>$val){
				$isi .= "<tr>
								<td width=\"5%\"><font size=\"7\" face=\"Helvetica\" align=\"center\">".$no++.".</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\"> ".$val->kdbrg."</font></td>
								<td width=\"27%\"><font size=\"7\" face=\"Helvetica\"> ".$val->nmbrg."</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\"> ".$val->nmjnsbrg."</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\"> ".$val->nmsatuankcl."</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->jmlkomputer."&nbsp;&nbsp;</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->jmlfisik."&nbsp;&nbsp;</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->selisih."&nbsp;&nbsp;</font></td>
								<td width=\"14%\"><font size=\"7\" face=\"Helvetica\">&nbsp;".$val->catatan."</font></td>
						</tr>";
				}
				$detail = "<table border=\"1\" cellpadding=\"2\">
						<thead>
							<tr align=\"center\">
								<th width=\"5%\"><font size=\"7\" face=\"Helvetica\">No.</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Kode Barang</font></th>
								<th width=\"27%\"><font size=\"7\" face=\"Helvetica\">Nama Barang</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Jenis Barang</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Satuan</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Stock<br>Komputer</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Stock<br>Fisik</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Selisih</font></th>
								<th width=\"14%\"><font size=\"7\" face=\"Helvetica\">Catatan</font></th>
							</tr>
						</thead>".$isi."
						   </table>
				";
		$this->pdf_pp->writeHTML($detail,true,false,false,false);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td width=\"20%\"><b>Mengetahui</b></td>
				<td></td>
			</tr>
			<tr>
				<td height=\"40\"></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><font size=\"9\" face=\"Helvetica\">".$noso['nmlengkap']."</font><hr></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf_pp->writeHTML($approve,true,false,false,false);
		$this->pdf_pp->Output('stock_opname.pdf', 'I');
	}
	
	function excelso($noso, $idbagian) {
		$tablename='v_sodet';
		$header = array(
			'Kode Barang',
			'Nama Barang',
			'Jenis Barang',
			'Satuan',
			'Stok Komputer',
			'Stok Fisik',
			'Selisih',
			'Catatan'
		);
		
		$this->db->select("
			kdbrg,
			nmbrg,
			nmjnsbrg,
			nmsatuankcl,
			jmlkomputer,
			jmlfisik,
			selisih,
			catatan
		");
        $this->db->from($tablename);
		$this->db->where("noso", $noso);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();
		
		$query = $this->db->getwhere('bagian',array('idbagian'=>$idbagian));
		$nmbagian = $query->row_array();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = $noso;	
		$data['filter'] = strtoupper('Stok Opname')."\n Nama Bagian : ".$nmbagian['nmbagian']."";	
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellso', $data); 	
	}
}

?>