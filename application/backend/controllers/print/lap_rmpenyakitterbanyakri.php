<?php

	class Lap_rmpenyakitterbanyakri extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function get_lap_rmpenyakitterbanyakri($tglawal,$tglakhir){
		$isi = '';
		
		$this->db->select("rd.tglkeluar
						 , r.noreg
						 , trim(LEADING '0' FROM p.norm) AS norm
						 , if(((SELECT min(registrasidet.idregdet) AS stp
								FROM
								  registrasidet
								LEFT JOIN registrasi
								ON registrasi.noreg = registrasidet.noreg
								LEFT JOIN nota
								ON nota.idregdet = registrasidet.idregdet
								LEFT JOIN kuitansi
								ON kuitansi.nokuitansi = nota.nokuitansi
								WHERE
								  registrasi.norm = p.norm
								  AND
								  registrasi.idjnspelayanan = '1'
								  AND
								  registrasidet.userbatal IS NULL
								  AND
								  kuitansi.idstkuitansi = 1
						   ) = rd.idregdet), 'Baru', 'Lama') AS stpasien
						 , p.nmpasien
						 , rd.umurtahun
						 , p.idjnskelamin
						 , jkel.nmjnskelamin
						 , p.alamat
						 , dkel.nmdaerah AS kel
						 , dkec.nmdaerah AS kec
						 , dkot.nmdaerah AS kot
						 , p.notelp
						 , rd.iddokter
						 , dk.nmdoktergelar
						 , r.idjnspelayanan
						 , jpel.nmjnspelayanan
						 , r.idpenjamin
						 , pj.nmpenjamin", false);
		$this->db->from('registrasi r', false);
		$this->db->join("registrasidet rd", "r.noreg = rd.noreg", "left", false);
		$this->db->join("pasien p", "p.norm = r.norm", "left", false);
		$this->db->join("daerah dkel", "dkel.iddaerah = p.iddaerah", "left", false);
		$this->db->join("daerah dkec", "dkec.iddaerah = dkel.dae_iddaerah", "left", false);
		$this->db->join("daerah dkot", "dkot.iddaerah = dkec.dae_iddaerah", "left", false);
		$this->db->join("jkelamin jkel", "jkel.idjnskelamin = p.idjnskelamin", "left", false);
		$this->db->join("jpelayanan jpel", "jpel.idjnspelayanan = r.idjnspelayanan", "left", false);
		$this->db->join("penjamin pj", "pj.idpenjamin = r.idpenjamin", "left", false);
		$this->db->join("dokter dk", "dk.iddokter = rd.iddokter", "left", false);
		$this->db->join("nota n", "n.idregdet = rd.idregdet", "left", false);
		$this->db->join("kuitansi k", "k.nokuitansi = n.nokuitansi", "left", false);
		$this->db->where("r.idjnspelayanan IN(1,3)");
		$this->db->where("rd.tglkeluar BETWEEN '".$tglawal."' AND '".$tglakhir."'");
		$this->db->where("rd.userbatal IS NULL");
		$this->db->group_by("r.noreg");
		$this->db->order_by('rd.tglkeluar, r.noreg');
		//$this->db->limit("5");
		$query = $this->db->get();
		$rj = $query->result();
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Kunjungan', 0, 1, 'C', 0, '', 0);
		$this->pdf_lap->Cell(0, 0, 'Rawat Jalan', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);

		foreach($rj as $i=>$val){
			$isi .= "<tr>
					<td width=\"3.5%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"6%\" align=\"center\">". $val->tglkuitansi ."</td>
					<td width=\"6%\" align=\"center\">". $val->noreg ."</td>
					<td width=\"4%\" align=\"center\">". $val->stpasien ."</td>
					<td width=\"12%\" align=\"left\">". $val->nmpasien ."</td>
					<td width=\"3.5%\" align=\"center\">". $val->umurtahun ."</td>
					<td width=\"5%\" align=\"center\">". $val->nmjnskelamin ."</td>
					<td width=\"13%\" align=\"left\">". $val->alamat ."</td>
					<td width=\"7%\" align=\"center\">". $val->kec ."</td>
					<td width=\"10%\" align=\"center\">". $val->kot ."</td>
					<td width=\"7%\" align=\"center\">". $val->notelp ."</td>
					<td width=\"8%\" align=\"left\">". $val->nmdoktergelar ."</td>
					<td width=\"5.5%\" align=\"center\">". $val->nmjnspelayanan ."</td>
					<td width=\"12%\" align=\"left\">". $val->nmpenjamin ."</td>
				</tr>";	
		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"3.5%\" rowspan=\"2\">No.</td>
					<td width=\"6%\" rowspan=\"2\">Pulang</td>
					<td width=\"6%\" rowspan=\"2\">No. Registrasi</td>
					<td width=\"4%\" rowspan=\"2\">Status<br>Pasien</td>
					<td width=\"12%\" rowspan=\"2\">Nama Pasien</td>
					<td width=\"3.5%\" rowspan=\"2\">Usia</td>
					<td width=\"5%\" rowspan=\"2\">Jenis Kelamin</td>
					<td width=\"13%\" rowspan=\"2\">Alamat</td>
					<td width=\"17%\" colspan=\"2\">Daerah</td>
					<td width=\"7%\" rowspan=\"2\">No. Tlp</td>
					<td width=\"8%\" rowspan=\"2\">Dokter</td>
					<td width=\"5.5%\" rowspan=\"2\">Unit<br>Pelayanan</td>
					<td width=\"12%\" rowspan=\"2\">Penjamin</td>
				</tr>
				<tr align=\"center\">
					<td width=\"7%\">Kecamatan</td>
					<td width=\"10%\">Kota</td>
				</tr>
			  <tbody>
			  ". $isi ."
			  </tbody>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);

		//Close and output PDF document
		$this->pdf_lap->Output('Lap_rmkunjunganrj.pdf', 'I');
	}
	
	function laporan_excelrmpenyakitterbanyakri($tglawal, $tglakhir, $vdkot, $vdkott2, $vpenj, $idpenjamin2) {
		if($vdkott2 != 'null'){
			$vdkott = $vdkott2;
			$query = $this->db->getwhere('daerah',array('iddaerah'=>$vdkott));
			$nmkot = $query->row_array();
			if($nmkot != NULL){				
				$nmkott = $nmkot['nmdaerah'];
			}else{
				$nmkott = 'LUAR KOTA';				
			}
		}else{
			$vdkott = 'D';
			$nmkott = '-';
		}
		
		if($idpenjamin2 != 'null'){
			$idpenjamin = $idpenjamin2;
			$query = $this->db->getwhere('penjamin',array('idpenjamin'=>$idpenjamin));
			$nmpnj = $query->row_array();
			$nmpnjj = $nmpnj['nmpenjamin'];
		}else{
			$idpenjamin = 'P';
			$nmpnjj = '-';
		}
		
		$header = array(
			'No DTD',
			'Kode ICD X',
			'Golongan Sebab Penyakit',
			'Usia <= 6 Hari(L)',
			'Usia <= 6 Hari(P)',
			'Usia <= 28 Hari(L)',
			'Usia <= 28 Hari(P)',
			'Usia <= 1 Thn(L)',
			'Usia <= 1 Thn(P)',
			'Usia <= 4 Thn(L)',
			'Usia <= 4 Thn(P)',
			'Usia <= 14 Thn(L)',
			'Usia <= 14 Thn(P)',
			'Usia <= 24 Thn(L)',
			'Usia <= 24 Thn(P)',
			'Usia <= 44 Thn(L)',
			'Usia <= 44 Thn(P)',
			'Usia <= 64 Thn(L)',
			'Usia <= 64 Thn(P)',
			'Usia > 64 Thn(L)',
			'Usia > 64 Thn(P)',
			'Jml Keluar Hidup(L)',
			'Jml Keluar Hidup(P)',
			'Jml Keluar Mati(L)',
			'Jml Keluar Mati(P)',
			'Jumlah',
			
		);
		
		$q = "SELECT dtd
					 , kdpenyakit
					 , nmpenyakiteng
					 , sum(ukd6haril) AS ukd6haril
					 , sum(ukd6harip) AS ukd6harip
					 , sum(ukd28haril) AS ukd28haril
					 , sum(ukd28harip) AS ukd28harip
					 , sum(ukd1thnl) AS ukd1thnl
					 , sum(ukd1thnp) AS ukd1thnp
					 , sum(ukd4thnl) AS ukd4thnl
					 , sum(ukd4thnp) AS ukd4thnp
					 , sum(ukd14thnl) AS ukd14thnl
					 , sum(ukd14thnp) AS ukd14thnp
					 , sum(ukd24thnl) AS ukd24thnl
					 , sum(ukd24thnp) AS ukd24thnp
					 , sum(ukd44thnl) AS ukd44thnl
					 , sum(ukd44thnp) AS ukd44thnp
					 , sum(ukd64thnl) AS ukd64thnl
					 , sum(ukd64thnp) AS ukd64thnp
					 , sum(uld64thnl) AS uld64thnl
					 , sum(uld64thnp) AS uld64thnp
					 , sum(jmlkhidupl) AS jmlkhidupl
					 , sum(jmlkhidupp) AS jmlkhidupp
					 , sum(jmlkmatil) AS jmlkmatil
					 , sum(jmlkmatip) AS jmlkmatip
					 , sum(jumlah) AS jumlah

				FROM
				  (
				  SELECT kodifikasi.idkodifikasi
					   , kodifikasidet.idpenyakit
					   , penyakit.dtd
					   , penyakit.kdpenyakit
					   , penyakit.nmpenyakit
					   , penyakit.nmpenyakit as nmpenyakiteng
					   , kodifikasi.noreg
					   , kuitansi.tglkuitansi
					   , pasien.norm
					   , pasien.nmpasien
					   , dkel.nmdaerah AS kel
					   , dkec.nmdaerah AS kec
					   , dkot.nmdaerah AS kot
					   , registrasi.idpenjamin
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari <= 6, '0')
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd6haril
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari <= 6, '0')
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd6harip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari > 6, '0')
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari <= 28, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd28haril
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari > 6, '0')
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari <= 28, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd28harip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((rd.umurtahun = '0'), if(rd.umurbulan = '0', rd.umurhari > 28, rd.umurbulan >= 1), rd.umurtahun = 1)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd1thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((rd.umurtahun = '0'), if(rd.umurbulan = '0', rd.umurhari > 28, rd.umurbulan >= 1), rd.umurtahun = 1)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd1thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun <= 4, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd4thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun <= 4, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd4thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND rd.tglkeluar BETWEEN '2015-06-01' AND '2015-06-30'
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 4, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 14, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd14thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 4, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 14, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd14thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 14, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 24, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd24thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 14, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 24, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd24thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 24, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 44, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd44thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 24, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 44, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd44thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 44, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 64, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd64thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 44, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 64, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd64thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 64, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS uld64thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 64, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS uld64thnp

					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkhidupl
					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkhidupp

					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut = 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkmatil
					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND kd.idtindaklanjut = 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkmatip

					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(r.noreg)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN registrasi r
							  ON r.noreg = kd.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = r.norm
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 2
								AND kd.idtindaklanjut <> 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(r.noreg)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN registrasi r
							  ON r.noreg = kd.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = r.norm
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 1
								AND kd.idtindaklanjut = 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(r.noreg)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN registrasi r
							  ON r.noreg = kd.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = r.norm
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 2
								AND kd.idtindaklanjut = 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jumlah

				  FROM
					kodifikasidet
				  LEFT JOIN kodifikasi
				  ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
				  LEFT JOIN penyakit
				  ON penyakit.idpenyakit = kodifikasidet.idpenyakit
				  LEFT JOIN registrasi
				  ON registrasi.noreg = kodifikasi.noreg
				  LEFT JOIN registrasidet
				  ON registrasidet.noreg = registrasi.noreg				  
				  LEFT JOIN (
				  SELECT idregdet
					   , nokuitansi
				  FROM
					nota
				  GROUP BY
					idregdet
				  ) nota
				  ON nota.idregdet = registrasidet.idregdet
				  LEFT JOIN kuitansi
				  ON kuitansi.nokuitansi = nota.nokuitansi
				  LEFT JOIN pasien
				  ON pasien.norm = registrasi.norm
				  LEFT JOIN daerah dkel
				  ON dkel.iddaerah = pasien.iddaerah
				  LEFT JOIN daerah dkec
				  ON dkec.iddaerah = dkel.dae_iddaerah
				  LEFT JOIN daerah dkot
				  ON dkot.iddaerah = dkec.dae_iddaerah
				  WHERE
					registrasi.idjnspelayanan = 2
					AND kodifikasidet.idkodifikasi IS NOT NULL
					AND if(1 = '".$vdkot."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('26005' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('30457' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('30754' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkot.iddaerah NOT IN (26005, 30457, 30754)
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

					AND if(1 = '".$vpenj."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', registrasi.idpenjamin = '".$idpenjamin."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
				  GROUP BY
					kodifikasidet.idpenyakit

				  UNION

				  SELECT kodifikasi.idkodifikasi
					   , kodifikasidet.idpenyakit
					   , penyakit.dtd
					   , penyakit.kdpenyakit
					   , penyakit.nmpenyakit
					   , penyakit.nmpenyakit as nmpenyakiteng
					   , kodifikasi.noreg
					   , kuitansi.tglkuitansi
					   , pasien.norm
					   , pasien.nmpasien
					   , dkel.nmdaerah AS kel
					   , dkec.nmdaerah AS kec
					   , dkot.nmdaerah AS kot
					   , registrasi.idpenjamin
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 6, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd6haril
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 6, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd6harip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) > 6, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 28, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd28haril
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) > 6, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 28, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd28harip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) > 28, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 365, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd1thn1
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) > 28, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 365, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd1thnp
					   , '0' AS ukd4thnl
					   , '0' AS ukd4thnp
					   , '0' AS ukd14thnl
					   , '0' AS ukd14thnp
					   , '0' AS ukd24thnl
					   , '0' AS ukd24thnp
					   , '0' AS ukd44thnl
					   , '0' AS ukd44thnp
					   , '0' AS ukd64thnl
					   , '0' AS ukd64thnp
					   , '0' AS uld64thnl
					   , '0' AS uld64thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkhidupl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkhidupp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut = 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkmatil
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND kd.idtindaklanjut = 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkmatip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(kdt.idpenyakit)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN skl s
							  ON s.noreganak = kd.noreg
							  LEFT JOIN registrasi r
							  ON r.noreg = s.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = s.normanak
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 2
								AND kd.idtindaklanjut <> 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(kdt.idpenyakit)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN skl s
							  ON s.noreganak = kd.noreg
							  LEFT JOIN registrasi r
							  ON r.noreg = s.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = s.normanak
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 1
								AND kd.idtindaklanjut = 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(kdt.idpenyakit)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN skl s
							  ON s.noreganak = kd.noreg
							  LEFT JOIN registrasi r
							  ON r.noreg = s.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = s.normanak
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 2
								AND kd.idtindaklanjut = 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jumlah

				  FROM
					kodifikasidet
				  LEFT JOIN kodifikasi
				  ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
				  LEFT JOIN penyakit
				  ON penyakit.idpenyakit = kodifikasidet.idpenyakit
				  LEFT JOIN skl
				  ON skl.noreganak = kodifikasi.noreg
				  LEFT JOIN registrasi
				  ON registrasi.noreg = skl.noreg
				  LEFT JOIN registrasidet
				  ON registrasidet.noreg = registrasi.noreg
				  LEFT JOIN (
				  SELECT idregdet
					   , nokuitansi
				  FROM
					nota
				  GROUP BY
					idregdet
				  ) nota
				  ON nota.idregdet = registrasidet.idregdet
				  LEFT JOIN kuitansi
				  ON kuitansi.nokuitansi = nota.nokuitansi
				  LEFT JOIN pasien
				  ON pasien.norm = skl.normanak
				  LEFT JOIN daerah dkel
				  ON dkel.iddaerah = pasien.iddaerah
				  LEFT JOIN daerah dkec
				  ON dkec.iddaerah = dkel.dae_iddaerah
				  LEFT JOIN daerah dkot
				  ON dkot.iddaerah = dkec.dae_iddaerah
				  WHERE
					registrasi.idjnspelayanan = 2
					AND kodifikasidet.idkodifikasi IS NOT NULL
					AND if(1 = '".$vdkot."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('26005' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('30457' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('30754' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('D' = '".$vdkott."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkot.iddaerah NOT IN (26005, 30457, 30754)
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

					AND if(1 = '".$vpenj."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('P' = '".$idpenjamin."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', registrasi.idpenjamin = '".$idpenjamin."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
				  GROUP BY
					kodifikasidet.idpenyakit
				  ) laprm
				GROUP BY
				  idpenyakit
				ORDER BY
				  dtd ASC";
		$query = $this->db->query($q);
		$fpl = $query->result();
		$fplnum = $query->num_rows();
		
		$data['eksport'] = $fpl;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('Lap_RM_Penyakit_Terbanyak_RI');
		$data['filter'] = "Laporan : Rekam Medis Penyakit Terbanyak RI \n".strtoupper('Periode')." : ".$tglawal." s/d ".$tglakhir." \nDaerah : ".$nmkott." \nPenjamin : ".$nmpnjj."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellpo', $data); 	
	}
}
