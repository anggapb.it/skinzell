<?php
class Laporan_registrasi extends Controller{
		function __construct(){
			parent::__construct();      
		}
	
	function export_laporan_registrasi($items,$tglawal,$tglakhir,$fields,$keyword){
		
		$arrsearch = array();
		if ($items=="lapregdaftarpasien") {
			$table = "v_lap_reg_pasien";
			$datefield = 'tgldaftar';
			$this->db->select("*");
			$this->db->from($table);
			if ($tglawal!="null" && $tglakhir!="null") {
				$this->db->where($datefield.' BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			}
		} else if ($items=="lapregrawatjalan") {
			$table = "v_lap_reg_rj";
			$datefield = 'tglreg';
			$this->db->select("*");
			$this->db->from($table);
			$this->db->where($datefield.' BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('userbatal IS NULL');	
		} else if ($items=="lapregugd") {
			$table = "v_lap_reg_ugd";
			$datefield = 'tglreg';
			$this->db->select("*");
			$this->db->from($table);
			$this->db->where($datefield.' BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('userbatal IS NULL');	
		} else if ($items=="lapregrawatinap") {
			$table = "v_lap_reg_ri";
			$datefield = 'tglreg';
			$this->db->select("*");
			$this->db->from($table);
			$this->db->where($datefield.' BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('userbatal IS NULL');			
		}
		
		if ($keyword) {
			$keyword = ($keyword=="null") ? "":$keyword;
			$this->db->or_like($fields, $keyword);
		}
		
		$query = $this->db->get();
		
		if ($tglawal!="null" && $tglakhir!="null") {
			$arrsearch['periode'] = 'PERIODE: '.$tglawal.' s/d '.$tglakhir;
			$arrsearch['keyword'] = 'KEYWORD: '.$fields.' = '.$keyword;
		} else {
			$arrsearch['keyword'] = 'KEYWORD: '.$fields.' = '.$keyword;
		}
		$data['filter'] = $arrsearch;
		$data['data'] = $query->result();
		$data['filename'] = $items;
		$data['fieldname'] = $this->db->list_fields($table);
		$this->load->view('exportexcelfilterarray', $data); 	
	}
}
