<?php 
	class Lap_rekamedis_pasienkeluar extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function pasienkeluar($tglawal,$tglakhir){
		
		$this->db->select("*");
		$this->db->from("v_lappasienkeluar");
	//	$this->db->groupby("nopo");
		$this->db->where("tglkeluar BETWEEN '".$tglawal."' and '".$tglakhir."'");
	//	$this->db->where("tglkeluar",!null);
		$query = $this->db->get();
		$po = $query->result();
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
	//	$this->pdf->AddPage();
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Pasien Keluar Per Tanggal Keluar', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->Cell(0, 0, 'TANGGAL : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		$isi ='';
		foreach($query->result_array() as $i=>$r){
		$no = (1+ $i);
			$isi .="<tr>
						<td align=\"center\" width=\"3%\">".$no."</td>
						<th width=\"7%\" align=\"center\">".$r['norm']."</th>
						<th width=\"7%\" align=\"center\">".$r['noreg']."</th>
						<th width=\"10%\" >".$r['nmpasien']."</th>
						<th width=\"5%\" align=\"right\">".$r['tglmasuk']."</th>
						<th align=\"center\" width=\"5%\">".$r['jammasuk']."</th>
						<th align=\"right\" width=\"5%\">".$r['tglkeluar']."</th>
						<th align=\"center\" width=\"5%\">".$r['jamkeluar']."</th>
						<th width=\"8%\" >".$r['nmbagian']."</th>
						<th width=\"5%\" align=\"center\">".$r['nmklsrawat']."</th>
						<th align=\"center\">".$r['nmkamar']."</th>
						<th>".$r['nmdoktergelar']."</th>
						<th>".$r['nmpenjamin']."</th>
						<th></th>
						<th>".$r['nmcarakeluar']."</th>
			</tr>";
		}
		$heads = "<br><br><font size=\"6\" face=\"Helvetica\"> <table border=\"1\">
					<tr align=\"center\">
						<th width=\"3%\">NO.</th>
						<th width=\"7%\">NO. RM</th>
						<th width=\"7%\">NO. REG</th>
						<th width=\"10%\">NAMA PASIEN</th>
						<th width=\"5%\">TGL. MASUK</th>
						<th width=\"5%\">JAM MASUK</th>
						<th width=\"5%\">TGL. KELUAR</th>
						<th width=\"5%\">JAM KELUAR</th>
						<th width=\"8%\">RUANGAN</th>
						<th width=\"5%\">KELAS</th>
						<th>KAMAR</th>
						<th width=\"15%\">DOKTER RAWAT</th>
						<th>PENANGGUNG BIAYA</th>
						<th>KEADAAN KELUAR</th>
						<th>CARA KELUAR</th>
					</tr>".$isi."
		</table></font>";
		
		
		$this->pdf->writeHTML($heads,true,false,false,false);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>