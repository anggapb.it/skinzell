<?php 
	class Lap_persediaan_rekapstok extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function pasienkeluar($tglawal,$tglakhir,$bagian){
		
		$this->db->select("*");
		$this->db->from("v_rekapstok");
		$this->db->where("tglkartustok BETWEEN '".$tglawal."' and '".$tglakhir."'");
		$this->db->where("idbagian",$bagian);
		$query = $this->db->get();
		$aa = $query->row_array();
		
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
	//	$this->pdf->AddPage();
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Rekapitulasi Stok per Jenis Transaksi', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->Cell(0, 0, 'Tanggal : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		$this->pdf->Cell(0, 0, 'Bagian : '. $aa['nmbagian'], 0, 1, 'C', 0, '', 0);
		
		$isi ='';
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
			$isi .="<tr>
						<td>".++$no."</td>
						<td>".$r['kdbrg']."</td>
						<td>".$r['nmbrg']."</td>
						<td align=\"center\">".$r['so']."</td>
						<td align=\"center\">".$r['beli']."</td>
						<td align=\"center\">".$r['returbeli']."</td>
						<td align=\"center\"></td>
						<td align=\"center\">".$r['tbl']."</td>
						<td align=\"center\">".$r['pbb']."</td>
						<td align=\"center\">".$r['terimabarang']."</td>
						<td align=\"center\">".$r['returpbb']."</td>
						<td align=\"center\">".$r['returpnbb']."</td>
						<td align=\"center\">".$r['bhp']."</td>
						<td align=\"center\">".$r['pengfarmasi']."</td>
						<td align=\"center\">".$r['returfarmasi']."</td>
						<td align=\"center\">".$r['sisa']."</td>
						<td></td>
						<td></td>
			</tr>";
		}
	}
		$heads = "<br><br><font size=\"6\" face=\"Helvetica\"> <table border=\"1\">
					<tr align=\"center\">
						<th width=\"3%\">No.</th>
						<th width=\"7%\">Kode</th>
						<th width=\"15%\">Nama Barang</th>
						<th width=\"5%\">SA</th>
						<th width=\"5%\">Beli</th>
						<th width=\"5%\">R.Beli</th>
						<th width=\"5%\">Bonus</th>
						<th width=\"5%\">TBL</th>
						<th width=\"5%\">D.KB</th>
						<th width=\"5%\">D.MB</th>
						<th width=\"5%\">R.KBG</th>
						<th width=\"5%\">R.MBG</th>
						<th width=\"5%\">BHP</th>
						<th width=\"5%\">Jual</th>
						<th width=\"5%\">R.Jual</th>
						<th width=\"5%\">Sisa</th>
						<th width=\"7%\">N.Beli</th>
						<th width=\"7%\">N.Jual</th>
					</tr>".$isi."
					<tr>
						<td colspan=\"16\" align=\"right\">TOTAL</td>
						<td align =\"right\">0</td>
						<td align =\"right\">0</td>
					</tr>
		</table></font>";
		
		
		$this->pdf->writeHTML($heads,true,false,false,false);
		$approve = " <br><br><br><br>
			<font size=\"6\" face=\"Helvetica\">
			Keterangan<br>
			<table border=\"0\" >
			<tr>
				<td width=\"2%\">1.</td>
				<td width=\"5%\">S.A</td>
				<td width=\"1%\">:</td>
				<td >Saldo Awal / Stock Opname</td>
				
				<td width=\"2%\">8.</td>
				<td width=\"5%\">R.KBG</td>
				<td width=\"1%\">:</td>
				<td>Retur Keluar Barang Bagian</td>
			</tr>
			<tr>
				<td width=\"2%\">2.</td>
				<td>Beli</td>
				<td width=\"1%\">:</td>
				<td>Pembelian Barang</td>
				
				<td width=\"2%\">9.</td>
				<td>R.MBG</td>
				<td width=\"1%\">:</td>
				<td>Retur Masuk Barang Bagian</td>
			</tr>
			<tr>
				<td width=\"2%\">3.</td>
				<td>R.Beli</td>
				<td width=\"1%\">:</td>
				<td>Retur Pembelian Supplier</td>
				
				<td width=\"2%\">10.</td>
				<td>BHP</td>
				<td width=\"1%\">:</td>
				<td>Barang Habis Pakai</td>
			</tr>
			<tr>
				<td width=\"2%\">4.</td>
				<td>Bonus</td>
				<td width=\"1%\">:</td>
				<td>Penerimaan Barang Bonus Supplier</td>
				
				<td width=\"2%\">11.</td>
				<td>Jual</td>
				<td width=\"1%\">:</td>
				<td>Penjualan Barang</td>
			</tr>
			<tr>
				<td width=\"2%\">5.</td>
				<td>TBL</td>
				<td width=\"1%\">:</td>
				<td>Penerimaan Lain-Lain</td>
				
				<td width=\"2%\">12.</td>
				<td>R.Jual</td>
				<td width=\"1%\">:</td>
				<td>Retur Penjualan Barang(Farmasi)</td>
			</tr>
			<tr>
				<td width=\"2%\">6.</td>
				<td>D.KB</td>
				<td width=\"1%\">:</td>
				<td>Distribusi Keluar Barang</td>
				
				<td width=\"2%\">13.</td>
				<td>N.Jual</td>
				<td width=\"1%\">:</td>
				<td>Akumulasi Nilai Pembelian</td>
			</tr>
			<tr>
				<td width=\"2%\">7.</td>
				<td>D.MB</td>
				<td width=\"1%\">:</td>
				<td>Distribusi Masuk Barang</td>
				
				<td width=\"2%\">14.</td>
				<td>N.Jual</td>
				<td width=\"1%\">:</td>
				<td>Akumulasi Nilai Penjualan</td>
			</tr>
			</table></font>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>