<?php 
	class Lap_rekamedis_borperruangan extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function rekammedis($tglawal,$tglakhir,$ruangan){
		
		$query = $this->db->query("SELECT bagian.nmbagian as nmbagian
			 , count(bed.idbed) AS jumlahbed
			 , (
			   SELECT ((((to_days(registrasidet.tglkeluar) - to_days(registrasidet.tglmasuk))) / (count(bed.idbed) * 365))* 100)
			   FROM
				 registrasidet
			   WHERE
				 registrasidet.idbagian = bagian.idbagian
				 and
				 registrasidet.tglmasuk between '".$tglawal."' and '".$tglakhir."'
			   GROUP BY
				 bagian.idbagian
			   ) AS BOR
		FROM
		  bagian
		LEFT JOIN kamar
		ON kamar.idbagian = bagian.idbagian
		LEFT JOIN bed
		ON bed.idkamar = kamar.idkamar
		WHERE
		  bagian.idjnspelayanan = 2
		  bagian.idbagian = '".$ruangan."'
		GROUP BY
		  bagian.idbagian
		");
	//	$query = $this->db->get();
	//	$q = $query->row_array();
			//var_dump($tglawal);
			//var_dump($tglakhir);
		
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
	//	$this->pdf->AddPage();
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'BOR(Bed Occupancy Rate)', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->Cell(0, 0, 'Tanggal : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
	//	$this->pdf->Cell(0, 0, 'Asal Rujukan : '. $aa['nmbagian'], 0, 1, 'C', 0, '', 0);
		$isi ='';
		$this->pdf->SetMargins(PDF_MARGIN_LEFT+50, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		  /* 	if($r['idcaradatang'] != $stkeluar){
				$stkeluar = $r['idcaradatang'];
				$isi .= "<tr><td colspan=\"13\">Penanggung Biaya: ".$r['nmcaradatang']."</td></tr>";
				$no = 0;
			}   */
			$isi .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['nmbagian']."</td>
						<td align=\"center\">".$r['jumlahbed']."</td>
						<td align=\"center\">".$r['BOR']."</td>
			</tr>";
		}
	}
		$heads = "<br><br><font size=\"10\" face=\"Helvetica\"> <table border=\"1\">
					<tr align=\"center\">
						<th width=\"3%\">No.</th>
						<th width=\"30%\">Ruangan</th>
						<th width=\"20%\">TT<br> (Tempat Tidur)</th>
						<th width=\"20%\">BOR(%)</th>
					</tr>".$isi."
		</table></font>";
		$this->pdf->writeHTML($heads,true,false,false,false); 
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>