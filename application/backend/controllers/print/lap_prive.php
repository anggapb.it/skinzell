<?php

class Lap_prive extends Controller{
	function __construct(){
		parent::__construct();
		$this->load->library('pdf_lap');        
	}
	
	function laporan_prive_pdf($tglawal, $tglakhir){
		$isi = '';
		$this->db->select('*');
        $this->db->from('v_lapprive');
		
		if($tglakhir == $tglawal){
			$this->db->where('`tglkuitansi`', $tglakhir);
		}else{
			$this->db->where('`tglkuitansi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Prive (UP)', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		
		$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
			
		$no = 1;
		$totaleverything = 0;
		
		foreach($data as $i=>$val){
			$isi .= "
			    <tr>
					<td align=\"center\">". ($i+1) .".</td>
					<td align=\"left\">". $val->nmjnspelayanan ."</td>
					<td align=\"left\">". $val->noreg ."</td>
					<td align=\"center\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td align=\"center\">". $val->norm ."</td>
					<td align=\"left\">". $val->nmpasien ."</td>
					<td align=\"left\">". $val->atasnama ."</td>
					<td align=\"left\">". $val->kditem ."</td>
					<td align=\"left\">". $val->nmbrg ."</td>
					<td align=\"right\">". number_format($val->hrgjual,0,',','.') ."</td>
					<td align=\"right\">". $val->qty ."</td>
					<td align=\"center\">". $val->nmsatuan ."</td>
					<td align=\"right\">". number_format($val->subtotal,0,',','.') ."</td>
				</tr>";	
				
			$totaleverything += $val->subtotal;
		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"5%\"><strong>No.</strong></td>
					<td width=\"10%\"><strong>Jenis Pelayanan</strong></td>
					<td width=\"8%\"><strong>No. Reg / Nota</strong></td>
					<td width=\"5%\"><strong>Tgl. Kuitansi </strong></td>
					<td width=\"5%\"><strong>No. RM</strong></td>
					<td width=\"10%\"><strong>Nama Pasien</strong></td>
					<td width=\"10%\"><strong>Atas Nama</strong></td>
					<td width=\"7%\"><strong>Kode</strong></td>
					<td width=\"12%\"><strong>Obat</strong></td>
					<td width=\"8%\"><strong>Tarif</strong></td>
					<td width=\"5%\"><strong>Qty</strong></td>
					<td width=\"5%\"><strong>Satuan</strong></td>
					<td width=\"10%\"><strong>Subtotal</strong></td>
				</tr>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td align=\"right\" colspan=\"12\"><strong>Total</strong></td>
					<td align=\"right\"><strong>". number_format($totaleverything,0,',','.')."</strong></td>
				</tr>
			</table></font>
		";

		$this->pdf_lap->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf_lap->Output('lap_prive_'.$tglawal.'_'.$tglakhir.'.pdf', 'I');
	}

	function laporan_prive_excel($tglawal, $tglakhir) {
		$isi = '';
		$this->db->select('*');
        $this->db->from('v_lapprive');
		
		if($tglakhir == $tglawal){
			$this->db->where('`tglkuitansi`', $tglakhir);
		}else{
			$this->db->where('`tglkuitansi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$header = array(
			'No.',
			'Jenis Pelayanan',
			'No. Reg / Nota',
			'Tgl. Kuitansi ',
			'No. RM',
			'Nama Pasien',
			'Atas Nama',
			'Kode',
			'Obat',
			'Tarif',
			'Qty',
			'Satuan',
			'Subtotal',
		);
		
		$data['eksport'] = $data;
		$data['fieldname'] = $header;
		$data['filename'] = 'Laporan_prive_UP_'.$tglawal.'_'.$tglakhir;
		$data['filter'] = strtoupper('Tanggal')." : ".$tglawal." s/d ".$tglakhir;		
		$this->load->view('exportexcellpriveup', $data); 	
		
	}
	
		
}