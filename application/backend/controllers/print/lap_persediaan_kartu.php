<?php 
class Lap_persediaan_kartu extends Controller{
	function __construct(){
		parent::__construct();
		$this->load->library('pdf');
	
	}
	
	function persediaan($tglawal,$tglakhir){
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('P', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-5, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-5);
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf->Cell(0, 0, 'Kartu Persediaan', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Tanggal : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 0);
		
		$this->db->select("
			barang.kdbrg AS kdbrg,
			barang.nmbrg AS nmbrg,
			kartustok.noref AS noref,
			kartustok.tglkartustok AS tglkartustok,
			kartustok.jamkartustok AS jamkartustok,
			if(kartustok.idjnskartustok=9, kartustok.saldoawal, 0) AS saldoawal
			 , if(kartustok.idjnskartustok = 1, (kartustok.jmlmasuk - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													 FROM
																													   podet
																													 WHERE
																													   podet.nopo = `retursupplierdet`.nopo
																													   AND
																													   podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																	   FROM
																		 `retursupplierdet`
																	   WHERE
																		 ((`retursupplierdet`.`nopo` = kartustok.noref)
																		 AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		 AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS jmlmasuk
			 , if(kartustok.idjnskartustok = 2, (kartustok.jmlkeluar - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													  FROM
																														podet
																													  WHERE
																														podet.nopo = `retursupplierdet`.nopo
																														AND
																														podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																		FROM
																		  `retursupplierdet`
																		WHERE
																		  ((`retursupplierdet`.noretursupplier = kartustok.noref)
																		  AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		  AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS rbm
			 , if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0) AS jmlkeluar,
			if(kartustok.idjnskartustok IN (14, 15), kartustok.jmlmasuk, 0) AS rbk,
			kartustok.saldoakhir AS saldoakhir,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgbeli, 0) AS hrgbeli,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgjual, 0) AS hrgjual
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgbeli * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgbeli * kartustok.jmlmasuk), 0)) AS nbeli
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgjual * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgjual * kartustok.jmlmasuk), 0)) AS njual
		", false);
		
		$this->db->from("kartustok");
		$this->db->join('barang',
				'kartustok.kdbrg = barang.kdbrg', 'left');
		if($tglawal){
			$this->db->where('kartustok.tglkartustok BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$this->db->where("kartustok.idbagian",11);
		#$this->db->where("kartustok.kdbrg",'B000000143');
		$this->db->where_in('kartustok.idjnskartustok',array(1, 3, 2, 9, 14, 15));
		$this->db->orderby("kartustok.kdbrg, kartustok.tglkartustok, kartustok.jamkartustok");
		$this->db->where("kartustok.noref NOT IN (SELECT kstok.noref AS kd
												  FROM
													kartustok kstok
												  WHERE
													kstok.kdbrg = kartustok.kdbrg
													AND
													kstok.idbagian = kartustok.idbagian
													AND
													kstok.tglkartustok BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
													AND
													kstok.`idjnskartustok` = '19')"
						);
		#$this->db->limit('70');
		$query = $this->db->get();
		
		$ckdbrg = '';
		$this->pdf->SetFont('helvetica', '', 7);
		$x=18;
		if($query->num_rows>0){
			foreach($query->result_array() as $r){
				if($ckdbrg != $r['kdbrg']){
					//$saldoakhir = $r['saldoawal'] + 0;
					$saldoakhir = 0;
					$isi ='';
					$no = 0;
					$x+=7;
					$this->pdf->SetFont('helvetica', '', 8);
					$this->pdf->writeHTMLCell(0, 0, 10, $x, $r['kdbrg'] ." ". $r['nmbrg'], 0, 1, 0, true, 'L', true);
					$x+=5;
					$this->pdf->SetFont('helvetica', '', 7);
					$this->pdf->writeHTMLCell(6, 4, 10, $x, 'No.', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(15, 4, 16, $x, 'Tanggal', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(15, 4, 31, $x, 'Jam', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(15, 4, 46, $x, 'Saldo Awal', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(15, 4, 61, $x, 'Masuk', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(13, 4, 76, $x, 'RBM', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(15, 4, 89, $x, 'Jual', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(13, 4, 104, $x, 'RBK', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(16, 4, 117, $x, 'Saldo Akhir', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(17, 4, 133, $x, 'Hrg.Beli', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(17, 4, 150, $x, 'Hrg.Jual', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(17, 4, 167, $x, 'N.Beli', 1, 1, 0, true, 'C', true);
					$this->pdf->writeHTMLCell(17, 4, 184, $x, 'N.Jual', 1, 1, 0, true, 'C', true);
					$saldoakhir = $saldoakhir + $r['saldoawal'] + $r['jmlmasuk'] - $r['rbm'] - $r['jmlkeluar'] + $r['rbk'];
					$ckdbrg = $r['kdbrg'];
				}else{
					$saldoakhir = $saldoakhir + $r['saldoawal'] + $r['jmlmasuk'] - $r['rbm'] - $r['jmlkeluar'] + $r['rbk'];
				}
				$x+=4;
				$this->pdf->writeHTMLCell(6, 4, 10, $x, ++$no, 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(15, 4, 16, $x, $r['tglkartustok'], 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(15, 4, 31, $x, $r['jamkartustok'], 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(15, 4, 46, $x, $r['saldoawal'], 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(15, 4, 61, $x, $r['jmlmasuk'], 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(13, 4, 76, $x, $r['rbm'], 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(15, 4, 89, $x, number_format($r['jmlkeluar'], 0, ',', ''), 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(13, 4, 104, $x, $r['rbk'], 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(16, 4, 117, $x, $saldoakhir, 1, 1, 0, true, 'C', true);
				$this->pdf->writeHTMLCell(17, 4, 133, $x, number_format($r['hrgbeli'],2,',','.'), 1, 1, 0, true, 'R', true);
				$this->pdf->writeHTMLCell(17, 4, 150, $x, number_format($r['hrgjual'],2,',','.'), 1, 1, 0, true, 'R', true);
				$this->pdf->writeHTMLCell(17, 4, 167, $x, number_format($r['nbeli'],2,',','.'), 1, 1, 0, true, 'R', true);
				$this->pdf->writeHTMLCell(17, 4, 184, $x, number_format($r['njual'],2,',','.'), 1, 1, 0, true, 'R', true);
				if($x>280){$this->pdf->AddPage('P', 'F4', false, false);$x = 10;}
			}
		}
		
		$this->pdf->SetFont('helvetica', '', 8);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		$this->pdf->Output('kartu_persediaan.pdf', 'I');
	}
	/* 
	function excelkpersediaan2($tglawal, $tglakhir) {
		$header = array(
			'No',
			'Nama',
			'Nomor',
			'Tanggal',
			'Jam',
			'Saldo Awal',
			'Jumlah Masuk',
			'RBM',
			'Jumlah Keluar',
			'RBK',
			'Saldo Akhir',
			'Harga Beli',
			'Harga Jual',
			'Nilai Beli',
			'Nilai Jual'
		);
		
		$data['tglawal'] = $tglawal;
		$data['tglakhir'] = $tglakhir;
		$data['fieldname'] = $header;
		$this->load->view('excelkpersediaan', $data); 	
	} */
	
	function excelkpersediaan($tglawal, $tglakhir) {
		$header = array(
			'Nomor',
			'Tanggal',
			'Jam',
			'Saldo Awal',
			'Masuk',
			'RBM',
			'Jual',
			'RBK',
			'Saldo Akhir',
			'Harga Beli',
			'Harga Jual',
			'Nilai Beli',
			'Nilai Jual'
		);
		
		
		$this->db->select("
			barang.kdbrg AS kdbrg,
			barang.nmbrg AS nmbrg,
			kartustok.noref AS noref,
			kartustok.tglkartustok AS tglkartustok,
			kartustok.jamkartustok AS jamkartustok,
			if(kartustok.idjnskartustok=9, kartustok.saldoawal, 0) AS saldoawal
			 , if(kartustok.idjnskartustok = 1, (kartustok.jmlmasuk - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													 FROM
																													   podet
																													 WHERE
																													   podet.nopo = `retursupplierdet`.nopo
																													   AND
																													   podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																	   FROM
																		 `retursupplierdet`
																	   WHERE
																		 ((`retursupplierdet`.`nopo` = kartustok.noref)
																		 AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		 AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS jmlmasuk
			 , if(kartustok.idjnskartustok = 2, (kartustok.jmlkeluar - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													  FROM
																														podet
																													  WHERE
																														podet.nopo = `retursupplierdet`.nopo
																														AND
																														podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																		FROM
																		  `retursupplierdet`
																		WHERE
																		  ((`retursupplierdet`.noretursupplier = kartustok.noref)
																		  AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		  AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS rbm
			 , if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0) AS jmlkeluar,
			if(kartustok.idjnskartustok IN (14, 15), kartustok.jmlmasuk, 0) AS rbk,
			kartustok.saldoakhir AS saldoakhir,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgbeli, 0) AS hrgbeli,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgjual, 0) AS hrgjual,
			if(kartustok.idjnskartustok = 3,(kartustok.hrgbeli*kartustok.jmlkeluar),0) AS nbeli,
			if(kartustok.idjnskartustok = 3,(kartustok.hrgjual*kartustok.jmlkeluar),0) AS njual			
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgbeli * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgbeli * kartustok.jmlmasuk), 0)) AS nbelik
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgjual * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgjual * kartustok.jmlmasuk), 0)) AS njualk
			 , if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgbeli * kartustok.jmlmasuk), 0) AS nbrbk
			 , if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgjual * kartustok.jmlmasuk), 0) AS njrbk
			
		", false);
		
		$this->db->from("kartustok");
		$this->db->join('barang',
				'kartustok.kdbrg = barang.kdbrg', 'left');
		if($tglawal){
			$this->db->where('kartustok.tglkartustok BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$this->db->where("kartustok.idbagian",11);
		#$this->db->where("kartustok.kdbrg",'B000000143');
		$this->db->where_in('kartustok.idjnskartustok',array(1, 3, 2, 9, 14, 15));
		$this->db->where("kartustok.noref NOT IN (SELECT kstok.noref AS kd
												  FROM
													kartustok kstok
												  WHERE
													kstok.kdbrg = kartustok.kdbrg
													AND
													kstok.idbagian = kartustok.idbagian
													AND
													kstok.tglkartustok BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
													AND
													kstok.`idjnskartustok` = '19')"
						);
		$this->db->orderby("kartustok.kdbrg, kartustok.tglkartustok, kartustok.jamkartustok");
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();
				
		$tablename = 'barang';
		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);	
		$data['numrows'] = $fplnum;
		$data['tglawal'] = $tglawal;
		$data['tglakhir'] = $tglakhir;
		$this->load->view('excelkpersediaan', $data); 	
	}
}
?>