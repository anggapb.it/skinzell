<?php

	class Lap_rmkiaanak1 extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function get_lap_rmkiaanak1($tglawal,$tglakhir){
		$isi = '';
		
		$this->db->select("k.tglkuitansi
						 , r.noreg
						 , trim(LEADING '0' FROM `p`.`norm`) AS `norm`
						 , if(((SELECT min(registrasidet.idregdet) AS stp
								FROM
								  registrasidet
								LEFT JOIN registrasi
								ON registrasi.noreg = registrasidet.noreg
								LEFT JOIN nota
								ON nota.idregdet = registrasidet.idregdet
								LEFT JOIN kuitansi
								ON kuitansi.nokuitansi = nota.nokuitansi
								WHERE
								  registrasi.norm = p.norm
								  AND
								  registrasi.idjnspelayanan = '1'
								  AND
								  registrasidet.userbatal IS NULL
								  AND
								  kuitansi.idstkuitansi = 1
						   ) = rd.idregdet), 'Baru', 'Lama') AS stpasien
						 , group_concat(peny.nmpenyakit SEPARATOR '<br>') AS nmpenyakit
						 , p.nmpasien
						 , rd.umurtahun
						 , p.idjnskelamin
						 , jkel.nmjnskelamin
						 , p.alamat
						 , dkel.nmdaerah AS kel
						 , dkec.nmdaerah AS kec
						 , dkot.nmdaerah AS kot
						 , p.notelp
						 , rd.iddokter
						 , dk.nmdoktergelar
						 , r.idjnspelayanan
						 , jpel.nmjnspelayanan
						 , r.idpenjamin
						 , pj.nmpenjamin", false);
		$this->db->from('registrasi r', false);
		$this->db->join("registrasidet rd", "r.noreg = rd.noreg", "left", false);
		$this->db->join("pasien p", "p.norm = r.norm", "left", false);
		$this->db->join("daerah dkel", "dkel.iddaerah = p.iddaerah", "left", false);
		$this->db->join("daerah dkec", "dkec.iddaerah = dkel.dae_iddaerah", "left", false);
		$this->db->join("daerah dkot", "dkot.iddaerah = dkec.dae_iddaerah", "left", false);
		$this->db->join("jkelamin jkel", "jkel.idjnskelamin = p.idjnskelamin", "left", false);
		$this->db->join("jpelayanan jpel", "jpel.idjnspelayanan = r.idjnspelayanan", "left", false);
		$this->db->join("penjamin pj", "pj.idpenjamin = r.idpenjamin", "left", false);
		$this->db->join("dokter dk", "dk.iddokter = rd.iddokter", "left", false);
		$this->db->join("nota n", "n.idregdet = rd.idregdet", "left", false);
		$this->db->join("kuitansi k", "k.nokuitansi = n.nokuitansi", "left", false);
		$this->db->join("kodifikasi kd", "kd.noreg = r.noreg", "left", false);
		$this->db->join("kodifikasidet kdt", "kdt.idkodifikasi = kd.idkodifikasi", "left", false);
		$this->db->join("penyakit peny", "peny.idpenyakit = kdt.idpenyakit", "left", false);
		$this->db->where("r.idjnspelayanan IN(1,3)");
		$this->db->where("k.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'");
		$this->db->where("rd.userbatal IS NULL");
		$this->db->where("k.idstkuitansi = 1");
		$this->db->group_by("r.noreg");
		$this->db->order_by('k.tglkuitansi, r.noreg');
		//$this->db->limit("5");
		$query = $this->db->get();
		$rj = $query->result();
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Kunjungan', 0, 1, 'C', 0, '', 0);
		$this->pdf_lap->Cell(0, 0, 'Rawat Jalan', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);

		foreach($rj as $i=>$val){
			$isi .= "<tr>
					<td width=\"3.5%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"6%\" align=\"center\">". $val->tglkuitansi ."</td>
					<td width=\"6%\" align=\"center\">". $val->noreg ."</td>
					<td width=\"4%\" align=\"center\">". $val->stpasien ."</td>
					<td width=\"12%\" align=\"left\">". $val->nmpasien ."</td>
					<td width=\"3.5%\" align=\"center\">". $val->umurtahun ."</td>
					<td width=\"5%\" align=\"center\">". $val->nmjnskelamin ."</td>
					<td width=\"13%\" align=\"left\">". $val->alamat ."</td>
					<td width=\"7%\" align=\"center\">". $val->kec ."</td>
					<td width=\"10%\" align=\"center\">". $val->kot ."</td>
					<td width=\"7%\" align=\"center\">". $val->notelp ."</td>
					<td width=\"8%\" align=\"left\">". $val->nmdoktergelar ."</td>
					<td width=\"5.5%\" align=\"center\">". $val->nmjnspelayanan ."</td>
					<td width=\"12%\" align=\"left\">". $val->nmpenjamin ."</td>
				</tr>";	
		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"3.5%\" rowspan=\"2\">No.</td>
					<td width=\"6%\" rowspan=\"2\">Pulang</td>
					<td width=\"6%\" rowspan=\"2\">No. Registrasi</td>
					<td width=\"4%\" rowspan=\"2\">Status<br>Pasien</td>
					<td width=\"12%\" rowspan=\"2\">Nama Pasien</td>
					<td width=\"3.5%\" rowspan=\"2\">Usia</td>
					<td width=\"5%\" rowspan=\"2\">Jenis Kelamin</td>
					<td width=\"13%\" rowspan=\"2\">Alamat</td>
					<td width=\"17%\" colspan=\"2\">Daerah</td>
					<td width=\"7%\" rowspan=\"2\">No. Tlp</td>
					<td width=\"8%\" rowspan=\"2\">Dokter</td>
					<td width=\"5.5%\" rowspan=\"2\">Unit<br>Pelayanan</td>
					<td width=\"12%\" rowspan=\"2\">Penjamin</td>
				</tr>
				<tr align=\"center\">
					<td width=\"7%\">Kecamatan</td>
					<td width=\"10%\">Kota</td>
				</tr>
			  <tbody>
			  ". $isi ."
			  </tbody>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);

		//Close and output PDF document
		$this->pdf_lap->Output('Lap_rmkunjunganrj.pdf', 'I');
	}
	
	function laporan_excelrmkiaanak1($tglawal, $tglakhir) {
		$header = array(
			'Pulang',
			'No. Registrasi',
			'No. RM',
			'Status Pasien',
			'Nama Pasien',
			'B2',
			'B4',
			'B9',
			'B12',
			'ICD X',
			'Kota',
			'Unit Pengirim',
			'Penjamin',
		);
		
		
		$q = "SELECT k.tglkuitansi
					 , r.noreg
					 , trim(LEADING '0' FROM `p`.`norm`) AS `norm`
					 , if(((SELECT min(registrasidet.idregdet) AS stp
							FROM
							  registrasidet
							LEFT JOIN registrasi
							ON registrasi.noreg = registrasidet.noreg
							LEFT JOIN nota
							ON nota.idregdet = registrasidet.idregdet
							LEFT JOIN kuitansi
							ON kuitansi.nokuitansi = nota.nokuitansi
							WHERE
							  registrasi.norm = p.norm
							  AND
							  registrasi.idjnspelayanan = '1'
							  AND
							  registrasidet.userbatal IS NULL
							  AND
							  kuitansi.idstkuitansi = 1
					   ) = rd.idregdet), 'Baru', 'Lama') AS stpasien
					 , p.nmpasien
					 , (SELECT if((registrasidet.noreg = registrasidet.noreg), '1', NULL)
						FROM
						  registrasidet
						LEFT JOIN nota
						ON nota.idregdet = registrasidet.idregdet
						LEFT JOIN kuitansi
						ON kuitansi.nokuitansi = nota.nokuitansi
						WHERE
						  registrasidet.idbagian = 4
						  AND registrasidet.noreg = rd.noreg
						  AND if((registrasidet.umurtahun = '0'), if(registrasidet.umurbulan = '0', registrasidet.umurhari > 28, registrasidet.umurbulan <= 3), NULL)
						  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
					   ) AS u29hr3bln

					 , (SELECT if((registrasidet.noreg = registrasidet.noreg), '1', NULL)
						FROM
						  registrasidet
						LEFT JOIN nota
						ON nota.idregdet = registrasidet.idregdet
						LEFT JOIN kuitansi
						ON kuitansi.nokuitansi = nota.nokuitansi
						WHERE
						  registrasidet.idbagian = 4
						  AND registrasidet.noreg = rd.noreg
						  AND if((registrasidet.umurtahun = '0'), registrasidet.umurbulan > 3
						  AND registrasidet.umurbulan <= 6, NULL)
						  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
					   ) AS u3bln6bln

					 , (SELECT if((registrasidet.noreg = registrasidet.noreg), '1', NULL)
						FROM
						  registrasidet
						LEFT JOIN nota
						ON nota.idregdet = registrasidet.idregdet
						LEFT JOIN kuitansi
						ON kuitansi.nokuitansi = nota.nokuitansi
						WHERE
						  registrasidet.idbagian = 4
						  AND registrasidet.noreg = rd.noreg
						  AND if((registrasidet.umurtahun = '0'), registrasidet.umurbulan > 6
						  AND registrasidet.umurbulan <= 9, NULL)
						  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
					   ) AS u6bln9bln

					 , (SELECT if((registrasidet.noreg = registrasidet.noreg), '1', NULL)
						FROM
						  registrasidet
						LEFT JOIN nota
						ON nota.idregdet = registrasidet.idregdet
						LEFT JOIN kuitansi
						ON kuitansi.nokuitansi = nota.nokuitansi
						WHERE
						  registrasidet.idbagian = 4
						  AND registrasidet.noreg = rd.noreg
						  AND if((registrasidet.umurtahun = '0'), registrasidet.umurbulan > 9
						  AND registrasidet.umurbulan <= 11, NULL)
						  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
					   ) AS u9bln11bln

					 , (SELECT group_concat(penyakit.nmpenyakit SEPARATOR ',\n') AS nmpenyakiteng
						FROM
						  kodifikasidet
						LEFT JOIN kodifikasi
						ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
						LEFT JOIN registrasi
						ON registrasi.noreg = kodifikasi.noreg
						LEFT JOIN penyakit
						ON penyakit.idpenyakit = kodifikasidet.idpenyakit
						WHERE
						  registrasi.noreg = r.noreg
					   ) AS nmpenyakiteng
					 , dkot.nmdaerah AS kot
					 , cdatang.nmcaradatang AS nmcaradatang
					 , pj.nmpenjamin


				FROM
				  registrasi r
				LEFT JOIN registrasidet rd
				ON r.noreg = rd.noreg
				LEFT JOIN caradatang cdatang
				ON cdatang.idcaradatang = rd.idcaradatang
				LEFT JOIN pasien p
				ON p.norm = r.norm
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = p.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN penjamin pj
				ON pj.idpenjamin = r.idpenjamin
				LEFT JOIN nota n
				ON n.idregdet = rd.idregdet
				LEFT JOIN kuitansi k
				ON k.nokuitansi = n.nokuitansi
				LEFT JOIN kodifikasi kd
				ON r.noreg = kd.noreg
				LEFT JOIN kodifikasidet kdt
				ON kdt.idkodifikasi = kd.idkodifikasi
				LEFT JOIN penyakit peny
				ON peny.idpenyakit = kdt.idpenyakit
				WHERE
				  r.idjnspelayanan = 1
				  AND
				  rd.userbatal IS NULL
				  AND
				  k.idstkuitansi = 1
				  AND
				  rd.idbagian = 4
				  AND
				  k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
				GROUP BY
				  r.noreg
				ORDER BY
				  r.noreg";
		$query = $this->db->query($q);
		$fpl = $query->result();
		$fplnum = $query->num_rows();
		
		$data['eksport'] = $fpl;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('Lap_RM_Kia_Anak1');
		$data['filter'] = "Laporan : Rekam Medis KIA Anak 1 \n".strtoupper('Periode')." : ".$tglawal." s/d ".$tglakhir."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellkiaibu1', $data); 	
	}
}
