<?php
class Printnotareturfarmasi extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
	
	function nota_retfarmasi($noreturfarmasi) {
		$this->pdf->SetPrintHeader(false);
      
        $this->db->select("*");
        $this->db->from("v_returfarmasihedrjri");        
		$this->db->where("noreturfarmasi", $noreturfarmasi);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('v_returfarmasidet',array('noreturfarmasi'=>$noreturfarmasi));
		$notadet = $query->result();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		//$this->db->getwhere('notadet',array('koder IS NOT NULL' => NULL, 'noreturfarmasi'=>$noreturfarmasi));
		$notadetf = $query->result();
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 160, 'ury' => 150),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('times', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">NOTA RETUR FARMASI</div>', '', 1, 0, true, 'C', true);
		
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">No. Retur');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['noreturfarmasi']);
		
		$x+=0;$y=80;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.date_format(date_create($nota['tglreturfarmasi']), 'd-m-Y'));
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['atasnama']);
		
		$x+=0;$y=80;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['jamreturfarmasi']);
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['norm']);
		
		$x+=0;$y=80;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">'.$nota['nmbagian']);
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmdokter']);
		
		$x+=0;$y=80;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);
		$isi = '';
		$total = 0;
		
		if($notadet){
			foreach($notadet AS $i=>$val){
				$x+=4;
				//$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				//$tarifall = $query->row_array();
				
				//$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty - ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
				$total += $val->subtotal;
				
				$isi .= "<tr>
					<td width=\"7%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"37%\">". $val->nmbrg ."</td>
					<td width=\"5%\" align=\"right\">".number_format($val->qtyretur,0,',','.')."</td>
					<td width=\"15%\" align=\"right\">". number_format($val->tarif,0,',','.') ."</td>
					<td width=\"13%\" align=\"right\">". $val->diskon."%</td>
					<td width=\"21%\" align=\"right\">". number_format($val->subtotal,0,',','.')."</td>
				</tr>";
			}
		}
		
		$x+=6;
		$html = "<div style=\"letter-spacing:3;\"><br/><br/><font size=\"10\" face=\"Times\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"77%\" align=\"right\"> Total </td>
					<td width=\"21%\" align=\"right\">". number_format($total,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		$x+=17;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"># '.$this->rp_terbilang($total, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=13;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">( '. $nota['nmlengkap'] .' )');
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=0;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Petugas Transaksi');
		//$x+=5;$y=5;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"><i>*Obat yang telah dibeli tidak bisa di tukar / retur</i>');

		$this->pdf->Output('notaRF.pdf', 'I');
	}
    	
	function nota_retfarmasiumum($noreturfarmasi) {
		$this->pdf->SetPrintHeader(false);
      
        $this->db->select("*");
        $this->db->from("v_returfarmasihed");        
		$this->db->where("noreturfarmasi", $noreturfarmasi);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('v_returfarmasidet',array('noreturfarmasi'=>$noreturfarmasi));
		$notadet = $query->result();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		//$this->db->getwhere('notadet',array('koder IS NOT NULL' => NULL, 'noreturfarmasi'=>$noreturfarmasi));
		$notadetf = $query->result();
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 160, 'ury' => 150),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('times', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">NOTA RETUR FARMASI PASIEN LUAR</div>', '', 1, 0, true, 'C', true);
		
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">No. Retur');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['noreturfarmasi']);
		
		$x+=0;$y=80;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.date_format(date_create($nota['tglreturfarmasi']), 'd-m-Y'));
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['atasnama']);
		
		$x+=0;$y=80;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['jamreturfarmasi']);
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['norm']);
		
		$x+=0;$y=80;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">'.$nota['nmbagian']);
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmdokter']);
		
		/* $x+=0;$y=80;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmstpelayanan']); */
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);
		$isi = '';
		$total = 0;
		
		if($notadet){
			foreach($notadet AS $i=>$val){
				$x+=4;
				//$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				//$tarifall = $query->row_array();
				
				//$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty - ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
				$total += $val->subtotal;
				
				$isi .= "<tr>
					<td width=\"7%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"37%\">". $val->nmbrg ."</td>
					<td width=\"5%\" align=\"right\">".number_format($val->qtyretur,0,',','.')."</td>
					<td width=\"15%\" align=\"right\">". number_format($val->tarif,0,',','.') ."</td>
					<td width=\"13%\" align=\"right\">". $val->diskon."%</td>
					<td width=\"21%\" align=\"right\">". number_format($val->subtotal,0,',','.')."</td>
				</tr>";
			}
		}
		
		
		$x+=6;
		$html = "<div style=\"letter-spacing:3;\"><br/><br/><font size=\"10\" face=\"Times\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"77%\" align=\"right\"> Total </td>
					<td width=\"21%\" align=\"right\">". number_format($total,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		$x+=17;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"># '.$this->rp_terbilang($total, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=13;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">( '. $nota['nmlengkap'] .' )');
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=0;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Petugas Transaksi');
		//$x+=5;$y=5;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"><i>*Obat yang telah dibeli tidak bisa di tukar / retur</i>');

		$this->pdf->Output('notaRFU.pdf', 'I');
	}
	
	//=============================================================================
	
	function rp_satuan($angka,$debug){
		$terbilang = '';
		$a_str['1']="Satu";
		$a_str['2']="Dua";
		$a_str['3']="Tiga";
		$a_str['4']="Empat";
		$a_str['5']="Lima";
		$a_str['6']="Enam";
		$a_str['7']="Tujuh";
		$a_str['8']="Delapan";
		$a_str['9']="Sembilan";
		
		
		$panjang=strlen($angka);
		for ($b=0;$b<$panjang;$b++)
		{
			$a_bil[$b]=substr($angka,$panjang-$b-1,1);
		}
		
		if ($panjang>2)
		{
			if ($a_bil[2]=="1")
			{
				$terbilang=" Seratus";
			}
			else if ($a_bil[2]!="0")
			{
				$terbilang= " ".$a_str[$a_bil[2]]. " Ratus";
			}
		}

		if ($panjang>1)
		{
			if ($a_bil[1]=="1")
			{
				if ($a_bil[0]=="0")
				{
					$terbilang .=" Sepuluh";
				}
				else if ($a_bil[0]=="1")
				{
					$terbilang .=" Sebelas";
				}
				else 
				{
					$terbilang .=" ".$a_str[$a_bil[0]]." Belas";
				}
				return $terbilang;
			}
			else if ($a_bil[1]!="0")
			{
				$terbilang .=" ".$a_str[$a_bil[1]]." Puluh";
			}
		}
		
		if ($a_bil[0]!="0")
		{
			$terbilang .=" ".$a_str[$a_bil[0]];
		}
		return $terbilang;
	   
	}
	
	function rp_terbilang($angka,$debug){
		$terbilang = '';
		
		$angka = str_replace(".",",",$angka);
		
		list ($angka) = explode(",",$angka);
		$panjang=strlen($angka);
		for ($b=0;$b<$panjang;$b++)
		{
			$myindex=$panjang-$b-1;
			$a_bil[$b]=substr($angka,$myindex,1);
		}
		if ($panjang>9)
		{
			$bil=$a_bil[9];
			if ($panjang>10)
			{
				$bil=$a_bil[10].$bil;
			}

			if ($panjang>11)
			{
				$bil=$a_bil[11].$bil;
			}
			if ($bil!="" && $bil!="000")
			{
				$terbilang .= $this->rp_satuan($bil,$debug)." milyar";
			}
			
		}

		if ($panjang>6)
		{
			$bil=$a_bil[6];
			if ($panjang>7)
			{
				$bil=$a_bil[7].$bil;
			}

			if ($panjang>8)
			{
				$bil=$a_bil[8].$bil;
			}
			if ($bil!="" && $bil!="000")
			{
				$terbilang .= $this->rp_satuan($bil,$debug)." Juta";
			}
			
		}
		
		if ($panjang>3)
		{
			$bil=$a_bil[3];
			if ($panjang>4)
			{
				$bil=$a_bil[4].$bil;
			}

			if ($panjang>5)
			{
				$bil=$a_bil[5].$bil;
			}
			if ($bil!="" && $bil!="000")
			{
				$terbilang .= $this->rp_satuan($bil,$debug)." Ribu";
			}
			
		}

		$bil=$a_bil[0];
		if ($panjang>1)
		{
			$bil=$a_bil[1].$bil;
		}

		if ($panjang>2)
		{
			$bil=$a_bil[2].$bil;
		}
		//die($bil);
		if ($bil!="" && $bil!="000")
		{
			$terbilang .= $this->rp_satuan($bil,$debug);
		}
		return trim($terbilang);
	}

}