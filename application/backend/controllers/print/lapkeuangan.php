<?php
class Lapkeuangan extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
    
    function lapfpasienluar($tglawal, $tglakhir) {
        $this->db->select("
			n.nonota AS nonota,
			n.tglnota AS tglnota,
			n.noresep AS noresep,
			n.diskon AS diskon,
			n.uangr AS racik,
			n.idbagian AS idbagian,
			n.userinput AS userid,
			pg.nmlengkap AS nmlengkap,
			(
				select sum(((((nd2.tarifjs + nd2.tarifjm) + nd2.tarifjp) + nd2.tarifbhp) * nd2.qty)) 
				from notadet nd2
				where (nd2.nonota = n.nonota)
			) AS tottindakan,
			nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp AS diskontindakan,
			d.nmdoktergelar AS nmdokter,
			k.atasnama AS atasnama,
			k.total AS total,
			(
				(
					select sum(((((nd2.tarifjs + nd2.tarifjm) + nd2.tarifjp) + nd2.tarifbhp) * nd2.qty)) 
					from notadet nd2 
					where (nd2.nonota = n.nonota)
				) + n.uangr - n.diskon
			) AS jumlah,
			(
				(
					select sum(((((nd2.tarifjs + nd2.tarifjm) + nd2.tarifjp) + nd2.tarifbhp) * nd2.qty))
					from notadet nd2
					where (nd2.nonota = n.nonota)
				) - (((nd.diskonjs + nd.diskonjm) + nd.diskonjp) + nd.diskonbhp)
			) AS jumlahtindakan,
			k.nokuitansi AS nokuitansi,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar <> 1
			) AS nontunai,(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar = 1
			) AS tunai
		", false);
        $this->db->from("nota n");
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = n.iddokter', 'left', false);
		$this->db->join('pengguna pg',
					'pg.userid = n.userinput', 'left', false);
					
		$this->db->where('n.idregdet IS NULL',null, false);
		$this->db->where('`n`.`idsttransaksi` <>', 2, false);
		$this->db->where('`k`.`idstkuitansi` <>', 2, false);
		$this->db->groupby('n.nonota');
		$this->db->where('n.idbagian', 11);
		$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$q = $this->db->get();
		$fpl = $q->result();
		
		$query = $this->db->getwhere('carabayar');
		$carabayar = $query->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		$tgl = $this->namaBulan(date('Y-m-d'));
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Farmasi Pasien Luar', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		$this->pdf->Cell(0, 0, 'Bagian : Farmasi', 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$isicb = '';
		$total = 0;
		$totdiskon = 0;
		$totracik = 0;
		$totjumlah = 0;
		$totjumlahcb = 0;
		$totnontunai = 0;
		$tottunai = 0;
		
		foreach($fpl AS $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"8%\">". $val->nonota ."</td>
					<td width=\"6%\">". date_format(date_create($val->tglnota), 'd-m-Y') ."</td>
					<td width=\"15%\">". $val->atasnama ."</td>
					<td width=\"16%\">". $val->nmdokter ."</td>
					<td width=\"7%\" align=\"right\">". $val->noresep ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->tottindakan,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->diskon,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->racik,0,',','.') ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->nontunai,0,',','.') ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->tunai,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->jumlah,0,',','.') ."</td>
					<td width=\"8%\">". $val->userid ."</td>
			</tr>";
			
			$total += $val->tottindakan;
			$totdiskon += $val->diskon;
			$totracik += $val->racik;
			$totjumlah += $val->jumlah;
			$totnontunai += $val->nontunai;
			$tottunai += $val->tunai;
			
		}
		
		$html = "<br/><br/><font size=\"9\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"8%\"><b>No. Nota</b></td>
					<td width=\"6%\"><b>Tgl. Nota</b></td>
					<td width=\"15%\"><b>Nama Pasien</b></td>
					<td width=\"16%\"><b>Dokter</b></td>
					<td width=\"7%\"><b>No. Resep</b></td>
					<td width=\"7%\"><b>Total</b></td>
					<td width=\"5%\"><b>Diskon</b></td>
					<td width=\"5%\"><b>Racik</b></td>
					<td width=\"7%\"><b>Non Tunai</b></td>
					<td width=\"7%\"><b>Tunai</b></td>
					<td width=\"8%\"><b>Jumlah</b></td>
					<td width=\"8%\"><b>User ID</b></td>
				</tr>
			  </thead>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"55%\" align=\"center\"><b>T O T A L</b></td>
					<td width=\"7%\" align=\"right\"><b>". number_format($total,0,',','.') ."</b></td>
					<td width=\"5%\" align=\"right\"><b>". number_format($totdiskon,0,',','.') ."</b></td>
					<td width=\"5%\" align=\"right\"><b>". number_format($totracik,0,',','.') ."</b></td>
					<td width=\"7%\" align=\"right\"><b>". number_format($totnontunai,0,',','.') ."</b></td>
					<td width=\"7%\" align=\"right\"><b>". number_format($tottunai,0,',','.') ."</b></td>
					<td width=\"8%\" align=\"right\"><b>". number_format($totjumlah,0,',','.') ."</b></td>
					<td width=\"8%\">&nbsp;</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		foreach($carabayar AS $i2=>$val2){
			$this->db->select("(SUM(kuitansidet.jumlah)) AS jumtotal");
			$this->db->from("kuitansidet");
			$this->db->join('nota',
					'nota.nokuitansi = kuitansidet.nokuitansi', 'left');
			$this->db->join('kuitansi',
					'kuitansi.nokuitansi = kuitansidet.nokuitansi', 'left');
			$this->db->where('nota.idsttransaksi <>', 2, false);
			$this->db->where('kuitansi.idstkuitansi <>', 2, false);
			$this->db->where('nota.idbagian', 11);
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('kuitansidet.idcarabayar', $val2->idcarabayar, false);
			$this->db->where('nota.idregdet IS NULL', null, false);
			$q = $this->db->get();
			$jumlahkd = $q->row_array();
			
			$isicb .= "<tr>
					<td width=\"58%\">". $val2->nmcarabayar ."</td>
					<td width=\"10%\">:</td>
					<td width=\"33%\" align=\"right\">". number_format($jumlahkd['jumtotal'],0,',','.') ."</td>
			</tr>";
			
			$totjumlahcb += $jumlahkd['jumtotal'];
		}
		
		$isihtml2 = "<table border=\"0px\" cellpadding=\"2\">
				". $isicb ."
				<tr>
					<td width=\"58%\"><b>Total</b></td>
					<td width=\"10%\">:</td>
					<td width=\"33%\" align=\"right\"><b>". number_format($totjumlahcb,0,',','.') ."</b></td>
				</tr>
			</table>
		";
		
		$html2 = "<font size=\"9\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"2\">
				<tr>
					<td width=\"17%\" align=\"center\">Bandung, ". $tgl ."<br/><br/><br/><br/><br/><br/>( ". $this->session->userdata['username'] ." )</td>
					<td width=\"52%\"></td>
					<td width=\"24%\">". $isihtml2 ."</td>
					<td width=\"8%\">&nbsp;</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html2,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('FarmasiPL.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	function excelfpasienluar($tglawal, $tglakhir) {
		$header = array(
			'No. Nota',
			'Tgl. Nota',
			'Nama Pasien',
			'Dokter',
			'No. Resep',
			'Total',
			'Diskon',
			'Racik',
			'Non Tunai',
			'Tunai',
			'Jumlah',
			'User ID'
		);
		
		$this->db->select("
			n.nonota AS nonota,
			n.tglnota AS tglnota,
			k.atasnama AS atasnama,
			d.nmdoktergelar AS nmdokter,
			n.noresep AS noresep,
			(
				select sum(((((nd2.tarifjs + nd2.tarifjm) + nd2.tarifjp) + nd2.tarifbhp) * nd2.qty)) 
				from notadet nd2
				where (nd2.nonota = n.nonota)
			) AS tottindakan,
			n.diskon AS diskon,
			n.uangr AS racik,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar <> 1
			) AS nontunai,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar = 1
			) AS tunai,
			k.total AS total,
			pg.nmlengkap AS nmlengkap
		", false);
        $this->db->from("nota n");
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = n.iddokter', 'left', false);
		$this->db->join('pengguna pg',
					'pg.userid = n.userinput', 'left', false);
					
		$this->db->where('n.idregdet IS NULL',null, false);
		$this->db->groupby('n.nonota');
		$this->db->where('n.idbagian', 11);
		$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['fieldname'] = $header;
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprj', $data); 	
	}
    
    function lapppasienluar($tglawal, $tglakhir) {
		$this->db->select("
			n.nonota AS nonota,
			n.tglnota AS tglnota,
			n.noresep AS noresep,
			n.diskon AS diskon,
			n.uangr AS racik,
			n.idbagian AS idbagian,
			n.userinput AS userid,
			pg.nmlengkap AS nmlengkap,
			(
				select sum(((((nd2.tarifjs + nd2.tarifjm) + nd2.tarifjp) + nd2.tarifbhp) * nd2.qty)) 
				from notadet nd2
				where (nd2.nonota = n.nonota)
			) AS tottindakan,
			nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp AS diskontindakan,
			d.nmdoktergelar AS nmdokter,
			k.atasnama AS atasnama,
			k.total AS total,
			(
				(
					select sum(((((nd2.tarifjs + nd2.tarifjm) + nd2.tarifjp) + nd2.tarifbhp) * nd2.qty)) 
					from notadet nd2 
					where (nd2.nonota = n.nonota)
				) + n.uangr - n.diskon
			) AS jumlah,
			(
				(
					select sum(((((nd2.tarifjs + nd2.tarifjm) + nd2.tarifjp) + nd2.tarifbhp) * nd2.qty))
					from notadet nd2
					where (nd2.nonota = n.nonota)
				) - (((nd.diskonjs + nd.diskonjm) + nd.diskonjp) + nd.diskonbhp)
			) AS jumlahtindakan,
			k.nokuitansi AS nokuitansi,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar <> 1
			) AS nontunai,(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar = 1
			) AS tunai
		", false);
        $this->db->from("nota n");
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = n.iddokter', 'left', false);
		$this->db->join('pengguna pg',
					'pg.userid = n.userinput', 'left', false);
					
		$this->db->where('n.idregdet IS NULL',null, false);
		$this->db->where('`n`.`idsttransaksi` <>', 2, false);
		$this->db->where('`k`.`idstkuitansi` <>', 2, false);
		$this->db->groupby('n.nonota');
		$this->db->where('n.idbagian', 35);
		$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$q = $this->db->get();
		$fpl = $q->result();
		
		$query = $this->db->getwhere('carabayar');
		$carabayar = $query->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		$tgl = $this->namaBulan(date('Y-m-d'));
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Pelayanan Tambahan', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$isicb = '';
		$total = 0;
		$totdiskon = 0;
		$totjumlah = 0;
		$totjumlahcb = 0;
		$totnontunai = 0;
		$tottunai = 0;
		
		foreach($fpl AS $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"8%\">". $val->nonota ."</td>
					<td width=\"6%\" align=\"center\">". date_format(date_create($val->tglnota), 'd-m-Y') ."</td>
					<td width=\"23%\">". $val->atasnama ."</td>
					<td width=\"16%\">". $val->nmdokter ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->total,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->diskontindakan,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->nontunai,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->tunai,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->jumlahtindakan,0,',','.') ."</td>
					<td width=\"8%\">". $val->nmlengkap ."</td>
			</tr>";
			
			$total += $val->total;
			$totdiskon += $val->diskontindakan;
			$totjumlah += $val->jumlahtindakan;
			$totnontunai += $val->nontunai;
			$tottunai += $val->tunai;
			
		}
		
		$html = "<br/><br/><font size=\"9\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"8%\"><b>No. Nota</b></td>
					<td width=\"6%\"><b>Tgl. Nota</b></td>
					<td width=\"23%\"><b>Nama Pasien</b></td>
					<td width=\"16%\"><b>Dokter</b></td>
					<td width=\"8%\"><b>Total</b></td>
					<td width=\"6%\"><b>Diskon</b></td>
					<td width=\"8%\"><b>Non Tunai</b></td>
					<td width=\"8%\"><b>Tunai</b></td>
					<td width=\"8%\"><b>Jumlah</b></td>
					<td width=\"8%\"><b>User ID</b></td>
				</tr>
			  </thead>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"56%\" align=\"center\"><b>T O T A L</b></td>
					<td width=\"8%\" align=\"right\"><b>". number_format($total,0,',','.') ."</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($totdiskon,0,',','.') ."</b></td>
					<td width=\"8%\" align=\"right\"><b>". number_format($totnontunai,0,',','.') ."</b></td>
					<td width=\"8%\" align=\"right\"><b>". number_format($tottunai,0,',','.') ."</b></td>
					<td width=\"8%\" align=\"right\"><b>". number_format($totjumlah,0,',','.') ."</b></td>
					<td width=\"8%\">&nbsp;</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		foreach($carabayar AS $i2=>$val2){
			$this->db->select("(SUM(kuitansidet.jumlah)) AS jumtotal");
			$this->db->from("kuitansidet");
			$this->db->join('nota',
					'nota.nokuitansi = kuitansidet.nokuitansi', 'left');
			$this->db->join('kuitansi',
					'kuitansi.nokuitansi = kuitansidet.nokuitansi', 'left');
			$this->db->where('nota.idbagian', 35);
			$this->db->where('nota.idsttransaksi <>', 2, false);
			$this->db->where('kuitansi.idstkuitansi <>', 2, false);
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('kuitansidet.idcarabayar', $val2->idcarabayar, false);
			$q = $this->db->get();
			$jumlahkd = $q->row_array();
			
			$isicb .= "<tr>
					<td width=\"43%\">". $val2->nmcarabayar ."</td>
					<td width=\"10%\"><b>:</b></td>
					<td width=\"48%\" align=\"right\">". number_format($jumlahkd['jumtotal'],0,',','.') ."</td>
			</tr>";
			
			$totjumlahcb += $jumlahkd['jumtotal'];
		}
		
		$isihtml2 = "<table border=\"0px\" cellpadding=\"2\">
				". $isicb ."
				<tr>
					<td width=\"43%\"><b>Total</b></td>
					<td width=\"10%\"><b>:</b></td>
					<td width=\"48%\" align=\"right\"><b>". number_format($totjumlahcb,0,',','.') ."</b></td>
				</tr>
			</table>
		";
		
		$html2 = "<font size=\"9\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"2\">
				<tr>
					<td width=\"17%\" align=\"center\">Bandung, ". $tgl ."<br/><br/><br/><br/><br/><br/>( ". $this->session->userdata['username'] ." )</td>
					<td width=\"56%\"></td>
					<td width=\"21%\">". $isihtml2 ."</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html2,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('FarmasiPL.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	function excelppasienluar($tglawal, $tglakhir) {
		$header = array(
			'No. Nota',
			'Tgl. Nota',
			'Nama Pasien',
			'Dokter',
			'Total',
			'Diskon',
			'Non Tunai',
			'Tunai',
			'Jumlah',
			'User ID'
		);
		
		$this->db->select("
			n.nonota AS nonota,
			n.tglnota AS tglnota,
			k.atasnama AS atasnama,
			d.nmdoktergelar AS nmdokter,
			(
				select sum(((((nd2.tarifjs + nd2.tarifjm) + nd2.tarifjp) + nd2.tarifbhp) * nd2.qty)) 
				from notadet nd2
				where (nd2.nonota = n.nonota)
			) AS tottindakan,
			nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp AS diskontindakan,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar <> 1
			) AS nontunai,(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar = 1
			) AS tunai,
			k.total AS total,
			pg.nmlengkap AS nmlengkap
		", false);
        $this->db->from("nota n");
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = n.iddokter', 'left', false);
		$this->db->join('pengguna pg',
					'pg.userid = n.userinput', 'left', false);
					
		$this->db->where('n.idregdet IS NULL',null, false);
		$this->db->groupby('n.nonota');
		$this->db->where('n.idbagian', 35);
		$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['fieldname'] = $header;
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprj', $data); 	
	}

    function lapdeposit($tglawal, $tglakhir) {
		$this->db->select("*, TRIM(LEADING '0' FROM norm) AS norm");
        $this->db->from("v_lapdeposit");
		$this->db->where('tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$q = $this->db->get();
		$dp = $q->result();
		
		$query = $this->db->getwhere('carabayar');
		$carabayar = $query->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		$tgl = $this->namaBulan(date('Y-m-d'));
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Penerimaan Deposit', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$isicb = '';
		$total = 0;
		$totjumlahcb = 0;
		$totnontunai = 0;
		$tottunai = 0;
		
		foreach($dp AS $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"8%\">". $val->nokuitansi ."</td>
					<td width=\"8%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"8%\">". $val->noreg ."</td>
					<td width=\"6%\">". $val->norm ."</td>
					<td width=\"15%\">". $val->nmpasien ."</td>
					<td width=\"8%\">". $val->nmbagian ."</td>
					<td width=\"15%\">". $val->nmdokter ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->nontunai,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->tunai,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->total,0,',','.') ."</td>
					<td width=\"7%\">". $val->userid ."</td>
			</tr>";
			
			$total += $val->total;
			$totnontunai += $val->nontunai;
			$tottunai += $val->tunai;
			
		}
		
		$html = "<br/><br/><font size=\"9\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"8%\"><b>No. Kuitansi</b></td>
					<td width=\"8%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"8%\"><b>No. Registrasi</b></td>
					<td width=\"6%\"><b>No. RM</b></td>
					<td width=\"15%\"><b>Nama Pasien</b></td>
					<td width=\"8%\"><b>Ruangan</b></td>
					<td width=\"15%\"><b>Dokter</b></td>
					<td width=\"8%\"><b>Non Tunai</b></td>
					<td width=\"8%\"><b>Tunai</b></td>
					<td width=\"8%\"><b>Total</b></td>
					<td width=\"7%\"><b>User ID</b></td>
				</tr>
			  </thead>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"71%\" align=\"center\"><b>T O T A L</b></td>
					<td width=\"8%\" align=\"right\"><b>". number_format($totnontunai,0,',','.') ."</b></td>
					<td width=\"8%\" align=\"right\"><b>". number_format($tottunai,0,',','.') ."</b></td>
					<td width=\"8%\" align=\"right\"><b>". number_format($total,0,',','.') ."</b></td>
					<td width=\"7%\">&nbsp;</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		foreach($carabayar AS $i2=>$val2){
			$this->db->select("SUM(kuitansidet.jumlah) AS jumtotal");
			$this->db->from("kuitansidet");
			$this->db->join('kuitansi',
					'kuitansi.nokuitansi = kuitansidet.nokuitansi', 'left');
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('kuitansi.idjnskuitansi', 5, false);
			$this->db->where('kuitansi.idstkuitansi', 1, false);
			$this->db->where('kuitansidet.idcarabayar', $val2->idcarabayar, false);
			$q = $this->db->get();
			$jumlahkd = $q->row_array();
			
			$isicb .= "<tr>
					<td width=\"43%\">". $val2->nmcarabayar ."</td>
					<td width=\"10%\">:</td>
					<td width=\"48%\" align=\"right\">". number_format($jumlahkd['jumtotal'],0,',','.') ."</td>
			</tr>";
			
			$totjumlahcb += $jumlahkd['jumtotal'];
		}
		
		$isihtml2 = "<table border=\"0px\" cellpadding=\"2\">
				". $isicb ."
				<tr>
					<td width=\"43%\"><b>Total</b></td>
					<td width=\"10%\">:</td>
					<td width=\"48%\" align=\"right\"><b>". number_format($totjumlahcb,0,',','.') ."</b></td>
				</tr>
			</table>
		";
		
		$html2 = "<font size=\"9\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"2\">
				<tr>
					<td width=\"17%\" align=\"center\">Bandung, ". $tgl ."<br/><br/><br/><br/><br/><br/>( ". $this->session->userdata['username'] ." )</td>
					<td width=\"56%\"></td>
					<td width=\"22%\">". $isihtml2 ."</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html2,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('Deposit.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	function exceldeposit($tglawal, $tglakhir) {
		$tablename='v_lapdeposit';
		$header = array(
			'No. Kuitansi',
			'Tgl. Kuitansi',
			'No. Registrasi',
			'No. RM',
			'Nama Pasien',
			'Ruangan',
			'Dokter',
			'Non Tunai',
			'Tunai',
			'Total',
			'User ID'
		);
		
		$this->db->select("
			nokuitansi,
			tglkuitansi,
			noreg,
			TRIM(LEADING '0' FROM norm) AS norm,
			nmpasien,
			nmbagian,
			nmdokter,
			nontunai,
			tunai,
			total,
			userid
		");
        $this->db->from($tablename);
		$this->db->where('tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprj', $data); 	
	}

    function lappenerimaanri($tglawal, $tglakhir, $ctransfer) {
		$this->db->select("
			kuitansi.tglkuitansi,
			registrasi.noreg,
			TRIM(LEADING '0' FROM pasien.norm) AS norm,
			pasien.nmpasien,
			penjamin.nmpenjamin,
			dokter.nmdoktergelar,
			(
				select nmbagian
				from bagian, registrasidet rd
				where bagian.idbagian = rd.idbagian AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS nmbagian,
			(
				select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
				from nota nt, registrasidet rd, notadet ntd
				where nt.idregdettransfer = registrasi.noreg AND
					rd.idregdet = nt.idregdet AND
					ntd.nonota = nt.nonota
			) AS nominalugd,
			
			
			(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty) + nota.uangr) AS nominalri,
			
			
			(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon) AS diskon,
			(
				select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
				from kuitansi kui, registrasidet rd
				where kui.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg AND
					kui.idjnskuitansi = 5 AND
					kui.idstkuitansi = 1
			) AS deposit,
			(
				select nt.userinput
				from nota nt, registrasidet rd
				where nt.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS userid,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar <> 1
			) AS nontunai,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar = 1
			) AS tunai
		", false);
        $this->db->from("registrasi");
        $this->db->join("registrasidet",
				"registrasidet.noreg = registrasi.noreg", "left");
        $this->db->join("nota",
				"nota.idregdet = registrasidet.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        $this->db->join("nota nota2",
				"nota2.idregdettransfer = registrasi.noreg", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
				
		$this->db->where('registrasi.idjnspelayanan', 2, false);
		$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$this->db->groupby("registrasi.noreg");
		
		if($ctransfer == '1'){$this->db->where('nota2.nonota IS NULL', null, false);}
		else if($ctransfer == '2'){$this->db->where('nota2.nonota IS NOT NULL', null, false);}
		
		$q = $this->db->get();
		$penerimaanri = $q->result();
		
		$query = $this->db->getwhere('carabayar');
		$carabayar = $query->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		$tgl = $this->namaBulan(date('Y-m-d'));
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Penerimaan Rawat Inap', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$isicb = '';
		$totjumlahcb = 0;
		$totugd = 0;
		$totri = 0;
		$totdiskon = 0;
		$totnontunai = 0;
		$tottunai = 0;
		$totdeposit = 0;
		$totjumlah = 0;
		$totretur = 0;
		
		foreach($penerimaanri AS $i=>$val){
			$cnominalugd = $val->nominalugd;
			$cnominalri = $val->nominalri;
			$cdiskon = $val->diskon;
			$cdeposit = $val->deposit;
			$cnontunai = $val->nontunai;
			$ctunai = $val->tunai;
			
			
			$jumlah = $cnominalugd+$cnominalri-$cdiskon-$cdeposit;
			$retur = $cdeposit - ($cnominalugd+$cnominalri-$cdiskon);
			if($jumlah<0)$jumlah=0;
			if($retur<0)$retur=0;
			
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"6%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"6%\">". $val->noreg ."</td>
					<td width=\"5%\">". $val->norm ."</td>
					<td width=\"8%\">". $val->nmpasien ."</td>
					<td width=\"7%\">". $val->nmpenjamin ."</td>
					<td width=\"8%\">". $val->nmdoktergelar ."</td>
					<td width=\"6%\">". $val->nmbagian ."</td>
					<td width=\"6%\" align=\"right\">". number_format($cnominalugd,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($cnominalri,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($cdiskon,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($cdeposit,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($cnontunai,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($ctunai,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($jumlah,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($retur,0,',','.') ."</td>
					<td width=\"5%\">". $val->userid ."</td>
			</tr>";
			
			$totugd += $cnominalugd;
			$totri += $cnominalri;
			$totdiskon += $cdiskon;
			$totnontunai += $cnontunai;
			$tottunai += $ctunai;
			$totdeposit += $cdeposit;
			$totjumlah += $jumlah;
			$totretur += $retur;
			
		}
		
		$html = "<br/><br/><font size=\"8\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"6%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"6%\"><b>No. Registrasi</b></td>
					<td width=\"5%\"><b>No. RM</b></td>
					<td width=\"8%\"><b>Nama Pasien</b></td>
					<td width=\"7%\"><b>Penjamin</b></td>
					<td width=\"8%\"><b>Dokter</b></td>
					<td width=\"6%\"><b>Ruangan</b></td>
					<td width=\"6%\"><b>Nominal UGD</b></td>
					<td width=\"6%\"><b>Nominal RI</b></td>
					<td width=\"6%\"><b>Diskon</b></td>
					<td width=\"6%\"><b>Deposit</b></td>
					<td width=\"6%\"><b>Non Tunai</b></td>
					<td width=\"6%\"><b>Tunai</b></td>
					<td width=\"6%\"><b>Jumlah</b></td>
					<td width=\"6%\"><b>Retur Deposit</b></td>
					<td width=\"5%\"><b>User ID</b></td>
				</tr>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"49%\" align=\"center\"><b>T O T A L</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($totugd,0,',','.') ."</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($totri,0,',','.') ."</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($totdiskon,0,',','.') ."</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($totdeposit,0,',','.') ."</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($totnontunai,0,',','.') ."</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($tottunai,0,',','.') ."</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($totjumlah,0,',','.') ."</b></td>
					<td width=\"6%\" align=\"right\"><b>". number_format($totretur,0,',','.') ."</b></td>
					<td width=\"5%\">&nbsp;</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		foreach($carabayar AS $i2=>$val2){
			$this->db->select("kuitansidet.jumlah AS jumtotal");
			$this->db->from("registrasi");
			$this->db->join("registrasidet",
					"registrasidet.noreg = registrasi.noreg", "left");
			$this->db->join("nota",
					"nota.idregdet = registrasidet.idregdet", "left");
			$this->db->join("notadet",
					"notadet.nonota = nota.nonota", "left");
			$this->db->join("kuitansi",
					"kuitansi.nokuitansi = nota.nokuitansi", "left");
			$this->db->join("kuitansidet",
					"kuitansidet.nokuitansi = kuitansi.nokuitansi", "left");
					
			$this->db->where('registrasi.idjnspelayanan', 2, false);
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
			$this->db->where('kuitansidet.idcarabayar', $val2->idcarabayar, false);
			$this->db->groupby("registrasi.noreg");
			$q = $this->db->get();
			
			$jmlkd = 0;
			if($q->num_rows()>0){
				for($i=0;$i<$q->num_rows();$i++){
					$jumlahkd = $q->result();
					$jmlkd += $jumlahkd[$i]->jumtotal;
				}
			}
			$isicb .= "<tr>
					<td width=\"43%\">". $val2->nmcarabayar ."</td>
					<td width=\"10%\">:</td>
					<td width=\"48%\" align=\"right\">". number_format($jmlkd,0,',','.') ."</td>
			</tr>";
			
			$totjumlahcb += $jmlkd;
		}
		
		$isihtml2 = "<table border=\"0px\" cellpadding=\"2\">
				". $isicb ."
				<tr>
					<td width=\"43%\"><b>Total</b></td>
					<td width=\"10%\">:</td>
					<td width=\"48%\" align=\"right\"><b>". number_format($totjumlahcb,0,',','.') ."</b></td>
				</tr>
			</table>
		";
		
		$html2 = "<font size=\"8\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"2\">
				<tr>
					<td width=\"17%\" align=\"center\">Bandung, ". $tgl ."<br/><br/><br/><br/><br/><br/>( ". $this->session->userdata['username'] ." )</td>
					<td width=\"55%\"></td>
					<td width=\"20%\">". $isihtml2 ."</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html2,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('Deposit.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	function excelpenerimaanri($tglawal, $tglakhir, $ctransfer) {
		/* $tablename='
			(SELECT
				kuitansi.tglkuitansi,
				registrasi.noreg,
				pasien.norm,
				pasien.nmpasien,
				penjamin.nmpenjamin,
				dokter.nmdoktergelar,
				(
					select nmbagian
					from bagian, registrasidet rd
					where bagian.idbagian = rd.idbagian AND
						rd.noreg = registrasi.noreg
					order by rd.idregdet desc
					limit 1
				) AS nmbagian,
				(
					select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
					from nota nt, registrasidet rd, notadet ntd
					where nt.idregdettransfer = registrasi.noreg AND
						rd.idregdet = nt.idregdet AND
						ntd.nonota = nt.nonota
				) AS nominalugd,
				(SUM(notadet.tarifjs*notadet.qty)+SUM(notadet.tarifjm*notadet.qty)+SUM(notadet.tarifjp*notadet.qty)+SUM(notadet.tarifbhp*notadet.qty)+nota.uangr) AS nominalri,
				(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon) AS diskon,
				(
					select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
					from kuitansi kui, registrasidet rd
					where kui.idregdet = rd.idregdet AND
						rd.noreg = registrasi.noreg AND
						kui.idjnskuitansi = 5
				) AS deposit,
				(
					select nt.userinput
					from nota nt, registrasidet rd
					where nt.idregdet = rd.idregdet AND
						rd.noreg = registrasi.noreg
					order by rd.idregdet desc
					limit 1
				) AS userid,
				(
					select sum(kdet.jumlah) 
					from kuitansidet kdet
					where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar <> 1
				) AS nontunai,
				(
					select sum(kdet.jumlah) 
					from kuitansidet kdet
					where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar = 1
				) AS tunai
				from registrasi
					LEFT JOIN registrasidet ON registrasidet.noreg = registrasi.noreg
					LEFT JOIN nota ON nota.idregdet = registrasidet.idregdet
					LEFT JOIN notadet ON notadet.nonota = nota.nonota
					LEFT JOIN kuitansi ON kuitansi.nokuitansi = nota.nokuitansi
					LEFT JOIN pasien ON pasien.norm = registrasi.norm
					LEFT JOIN penjamin ON penjamin.idpenjamin = registrasi.idpenjamin
					LEFT JOIN dokter ON dokter.iddokter = registrasidet.iddokter
					LEFT JOIN nota nota2 ON nota2.idregdettransfer = registrasi.noreg
			WHERE registrasi.idjnspelayanan = 2
			GROUP BY registrasi.noreg
			) AS abcd
		'; */
		$header = array(
			'Tgl. Masuk',
			'Tgl. Kuitansi',
			'No. Registrasi',
			'No. RM',
			'Nama Pasien',
			'Penjamin',
			'Dokter',
			'Ruangan',
			'Nominal UGD',
			'Obat',
			'Nominal RI',
			'Diskon',
			'Dijamin/PG',
			'Deposit',
			'Jumlah',
			'Retur Deposit',
			'User ID'
		);
		
		/* $this->db->select("
			tglkuitansi,
			noreg,
			TRIM(LEADING '0' FROM norm) AS norm,
			nmpasien,
			nmpenjamin,
			nmdoktergelar,
			nmbagian,
			if(nominalugd>0, nominalugd, 0),
			nominalri,
			diskon,
			0 as abc,
			deposit,
			if(if(nominalugd>0, nominalugd, 0)+nominalri-diskon<deposit, 0, if(nominalugd>0, nominalugd, 0)+nominalri-diskon-deposit) as jumlah,
			if(if(nominalugd>0, nominalugd, 0)+nominalri-diskon<deposit, deposit-if(nominalugd>0, nominalugd, 0)+nominalri-diskon, 0) as retur,
			userid
		", false);
        $this->db->from($tablename);
		$this->db->where('tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false); */
	#======================================================================================================	
		$tablename = 'SELECT * From registrasi';
		
		$this->db->select("
			(select DATE(registrasidet.tglmasuk)) as tglmasuk,
			kuitansi.tglkuitansi,
			registrasi.noreg,
			TRIM(LEADING '0' FROM pasien.norm) AS norm,
			pasien.nmpasien,
			penjamin.nmpenjamin,
			dokter.nmdoktergelar,
			(
				select nmbagian
				from bagian, registrasidet rd
				where bagian.idbagian = rd.idbagian AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS nmbagian,
			ifnull((
				select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
				from notadet ntd
				left join nota nt
				on ntd.nonota = nt.nonota
				left join registrasidet rd
				on rd.idregdet = nt.idregdet
				where nt.idregdettransfer = registrasi.noreg
			), 0) AS nominalugd,
			 (SELECT sum(nd.tarifbhp * nd.qty) AS uobt
				FROM
				  notadet nd
				LEFT JOIN nota n
				ON n.nonota = nd.nonota
				LEFT JOIN registrasidet rd
				ON rd.idregdet = n.idregdet
				WHERE
				  rd.noreg = registrasidet.noreg
				  AND
				  nd.kditem LIKE 'B%'
			   ) AS uobat,
			
			(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty) + nota.uangr) AS nominalri,
			(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon) AS diskon,

			ifnull((select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar IN(6,8)
			), 0) AS djaminorpg,
			(
				select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
				from kuitansi kui, registrasidet rd
				where kui.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg AND
					kui.idjnskuitansi = 5 AND
					kui.idstkuitansi = 1
			) AS deposit,
			
			if(ifnull((
				select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
				from notadet ntd
				left join nota nt
				on ntd.nonota = nt.nonota
				left join registrasidet rd
				on rd.idregdet = nt.idregdet
				where nt.idregdettransfer = registrasi.noreg
				), 0)+(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty) + nota.uangr
					   )-(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon)<(select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
																																 from kuitansi kui, registrasidet rd
																																 where kui.idregdet = rd.idregdet AND
																																	rd.noreg = registrasi.noreg AND
																																	kui.idjnskuitansi = 5 AND
																																	kui.idstkuitansi = 1
																																), 0, ifnull((select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
																																				from nota nt, registrasidet rd, notadet ntd
																																				where nt.idregdettransfer = registrasi.noreg AND
																																					rd.idregdet = nt.idregdet AND
																																					ntd.nonota = nt.nonota
																																				), 0)+(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty) + nota.uangr
																																					   )-(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon
																																					   )- (select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
																																							 from kuitansi kui, registrasidet rd
																																							 where kui.idregdet = rd.idregdet AND
																																								rd.noreg = registrasi.noreg AND
																																								kui.idjnskuitansi = 5 AND
																																								kui.idstkuitansi = 1
																																							) - ifnull((select sum(kdet.jumlah) 
																																								from kuitansidet kdet
																																								where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar IN(6,8)), 0
																																								)
			) as jumlah,
			
			if(ifnull((
				select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
				from notadet ntd
				left join nota nt
				on ntd.nonota = nt.nonota
				left join registrasidet rd
				on rd.idregdet = nt.idregdet
				where nt.idregdettransfer = registrasi.noreg
				), 0)+(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty) + nota.uangr
					   )-(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon)<(select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
																																 from kuitansi kui, registrasidet rd
																																 where kui.idregdet = rd.idregdet AND
																																	rd.noreg = registrasi.noreg AND
																																	kui.idjnskuitansi = 5 AND
																																	kui.idstkuitansi = 1
																																), (select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
																																	 from kuitansi kui, registrasidet rd
																																	 where kui.idregdet = rd.idregdet AND
																																		rd.noreg = registrasi.noreg AND
																																		kui.idjnskuitansi = 5 AND
																																		kui.idstkuitansi = 1
																																	) - ((ifnull((select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
																																				from nota nt, registrasidet rd, notadet ntd
																																				where nt.idregdettransfer = registrasi.noreg AND
																																					rd.idregdet = nt.idregdet AND
																																					ntd.nonota = nt.nonota
																																				), 0)+(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty) + nota.uangr
																																					   ))-(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon
																																					   )), 0
			) as retur,
			(
				select nt.userinput
				from nota nt, registrasidet rd
				where nt.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS userid
		", false);
        $this->db->from("registrasi");
        $this->db->join("registrasidet",
				"registrasidet.noreg = registrasi.noreg", "left");
        $this->db->join("nota",
				"nota.idregdet = registrasidet.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        if($ctransfer == '2'){
			$this->db->join("nota nota2",
					"nota2.idregdettransfer = registrasi.noreg", "left");
		}
				
		$this->db->where('registrasi.idjnspelayanan', 2, false);
		$this->db->groupby("registrasi.noreg");
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($ctransfer == '1'){$this->db->where('(SELECT nt.idregdettransfer
												  FROM
													nota nt
												  WHERE
													nt.idregdettransfer = registrasi.noreg
												  ) IS NULL');}
		if($ctransfer == '2'){$this->db->where('nota2.nonota IS NOT NULL', null, false);}
		
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprj', $data); 	
	}

	function lappenerimaanugd($tglawal, $tglakhir) {
		$this->db->select("
			r.noreg AS noreg,
			rd.tglreg AS tglreg,
			TRIM(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			(
				SELECT sum(t.tarifbhp*t.qty)
				FROM
				  notadet t
				WHERE
				  ((t.nonota = n.nonota)
				  AND (t.kditem LIKE 'B%'))
			) AS uobat,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS upemeriksaan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000001'
				  AND t.nonota = n.nonota
			) AS upemeriksaan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS utindakan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000048'
				  AND t.nonota = n.nonota
			) AS utindakan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS uimun
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000009'
				  AND t.nonota = n.nonota
			) AS uimun,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulab
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000037'
				  AND t.nonota = n.nonota
			) AS ulab,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulain
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000073'
				  AND t.nonota = n.nonota
			) AS ulain,
			n.uangr AS uracik,
			sum(nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp) + n.diskon AS udiskon,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar <> 1
			) AS ucc,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar = 1
			) AS utotal,
			b.nmbagian AS nmbagian,
			b.idbagian AS idbagian,
			d.nmdoktergelar AS nmdoktergelar,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
			) AS total,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND k.idregdet IS NOT NULL
			) AS utotaltrans
		", false);
        $this->db->from("registrasi r", false);
		$this->db->join('pasien p',
					'p.norm = r.norm', 'left', false);
		$this->db->join('registrasidet rd',
					'rd.noreg = r.noreg', 'left', false);
		$this->db->join('bagian b',
					'b.idbagian = rd.idbagian', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = rd.iddokter', 'left', false);
		$this->db->join('nota n',
					'n.idregdet = rd.idregdet', 'left', false);
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->where('rd.userbatal IS NULL', null, false);
		$this->db->where('b.idjnspelayanan', 3);
		$this->db->groupby('r.noreg');
		
		if($tglakhir){
			$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$q = $this->db->get();
		$reg = $q->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN PENERIMAAN UGD', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$total = 0;
		
		$totupemeriksaan = 0;
		$totuobat = 0;
		$totutindakan = 0;
		$totuimun = 0;
		$totulab = 0;
		$totulain = 0;
		$toturacik = 0;
		$totudiskon = 0;
		$totucc = 0;
		$totutotal = 0;
		$tottotal = 0;
		$totutotaltrans = 0;
		
		foreach($reg AS $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"7%\">". $val->noreg ."</td>
					<td width=\"6%\">". date_format(date_create($val->tglreg), 'd-m-Y') ."</td>
					<td width=\"5%\">". $val->norm ."</td>
					<td width=\"9%\">". $val->nmpasien ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->uobat,0,',','.') ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->upemeriksaan,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->utindakan,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->uimun,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->ulab,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->ulain,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->uracik,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->udiskon,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->ucc,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->utotal,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->total,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->utotaltrans,0,',','.') ."</td>
					<td width=\"10%\">". $val->nmdoktergelar ."</td>
			</tr>";
			
			$totuobat += $val->uobat;
			$totupemeriksaan += $val->upemeriksaan;
			$totutindakan += $val->utindakan;
			$totuimun += $val->uimun;
			$totulab += $val->ulab;
			$totulain += $val->ulain;
			$toturacik += $val->uracik;
			$totudiskon += $val->udiskon;
			$totucc += $val->ucc;
			$totutotal += $val->utotal;
			$tottotal += $val->total;
			$totutotaltrans += $val->utotaltrans;
			
		}
		
		$html = "<br/><br/><font size=\"8.5\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <!-- <thead> -->
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"7%\"><b>No. Reg</b></td>
					<td width=\"6%\"><b>Tgl. Reg</b></td>
					<td width=\"5%\"><b>No. RM</b></td>
					<td width=\"9%\"><b>Nama Pasien</b></td>
					<td width=\"5%\"><b>Obat</b></td>
					<td width=\"7%\"><b>Pemeriksaan</b></td>
					<td width=\"6%\"><b>Tindakan</b></td>
					<td width=\"5%\"><b>Imunisasi</b></td>
					<td width=\"5%\"><b>LAB</b></td>
					<td width=\"5%\"><b>Lain-lain</b></td>
					<td width=\"4%\"><b>Racik</b></td>
					<td width=\"4%\"><b>Diskon</b></td>
					<td width=\"6%\"><b>Non Tunai</b></td>
					<td width=\"5%\"><b>Tunai</b></td>
					<td width=\"6%\"><b>Total UGD</b></td>
					<td width=\"5%\"><b>Total Transfer</b></td>
					<td width=\"10%\"><b>Dokter</b></td>
				</tr>
			  <!-- </thead> -->
			  <tbody>". $isi ."</tbody>
				<tr align=\"right\">
					<td width=\"30%\">Total</td>
					<td width=\"5%\">". number_format($totuobat,0,',','.') ."</td>
					<td width=\"7%\">". number_format($totupemeriksaan,0,',','.') ."</td>
					<td width=\"6%\">". number_format($totutindakan,0,',','.') ."</td>
					<td width=\"5%\">". number_format($totuimun,0,',','.') ."</td>
					<td width=\"5%\">". number_format($totulab,0,',','.') ."</td>
					<td width=\"5%\">". number_format($totulain,0,',','.') ."</td>
					<td width=\"4%\">". number_format($toturacik,0,',','.') ."</td>
					<td width=\"4%\">". number_format($totudiskon,0,',','.') ."</td>
					<td width=\"6%\">". number_format($totucc,0,',','.') ."</td>
					<td width=\"5%\">". number_format($totutotal,0,',','.') ."</td>
					<td width=\"6%\">". number_format($tottotal,0,',','.') ."</td>
					<td width=\"5%\">". number_format($totutotaltrans,0,',','.') ."</td>
					<td width=\"10%\" colspan=\"2\">&nbsp;</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('penerimaanugd.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function excelpenerimaanugd($tglawal, $tglakhir) {
		$header = array(
			'No. reg',
			'Tgl. Reg',
			'No. RM',
			'Nama Pasien',
			'Obat',
			'Pemeriksaan',
			'Tindakan',
			'Imunisasi',
			'LAB',
			'Lain-lain',
			'Racik',
			'Diskon',
			'Non Tunai',
			'Tunai',
			'Total UGD',
			'Total Transfer',
			'Klinik',
			'Dokter'
		);
		
		$this->db->select("
			r.noreg AS noreg,
			rd.tglreg AS tglreg,
			TRIM(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			(
				SELECT sum(t.tarifbhp*t.qty)
				FROM
				  notadet t
				WHERE
				  ((t.nonota = n.nonota)
				  AND (t.kditem LIKE 'B%'))
			) AS uobat,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS upemeriksaan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000001'
				  AND t.nonota = n.nonota
			) AS upemeriksaan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS utindakan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000048'
				  AND t.nonota = n.nonota
			) AS utindakan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS uimun
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000009'
				  AND t.nonota = n.nonota
			) AS uimun,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulab
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000037'
				  AND t.nonota = n.nonota
			) AS ulab,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulain
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000073'
				  AND t.nonota = n.nonota
			) AS ulain,
			n.uangr AS uracik,
			sum(nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp) + n.diskon AS udiskon,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar <> 1
			) AS ucc,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar = 1
			) AS utotal,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
			) AS ujumlah,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND k.idregdet IS NOT NULL
			) AS utotaltrans,
			b.nmbagian AS nmbagian,
			d.nmdoktergelar AS nmdoktergelar
		", false);
        $this->db->from("registrasi r", false);
		$this->db->join('pasien p',
					'p.norm = r.norm', 'left', false);
		$this->db->join('registrasidet rd',
					'rd.noreg = r.noreg', 'left', false);
		$this->db->join('bagian b',
					'b.idbagian = rd.idbagian', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = rd.iddokter', 'left', false);
		$this->db->join('nota n',
					'n.idregdet = rd.idregdet', 'left', false);
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->where('rd.userbatal IS NULL', null, false);
		$this->db->where('b.idjnspelayanan', 3);
		$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$this->db->groupby('r.noreg');
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		//$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprj', $data); 	
	}
	
	function lapobatdokter($tglawal, $tglakhir, $cbagian) {
		$this->db->select("
			dokter.nmdoktergelar,
			kuitansi.nokuitansi,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			TRIM(LEADING '0' FROM pasien.norm) AS norm,
			pasien.nmpasien,
			barang.nmbrg,
			SUM(notadet.qty) AS qty,
			notadet.hrgjual as hrgjual,
			(SUM(notadet.qty) * ifnull(notadet.hrgjual, 0)) AS total,
			jpelayanan.nmjnspelayanan,			
			notadet.hrgbeli as hrgbeli
		", false);
        $this->db->from("barang");
        $this->db->join("notadet",
				"notadet.kditem = barang.kdbrg", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = registrasi.idjnspelayanan", "left");
				
		$this->db->groupby("barang.kdbrg, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, barang.nmbrg");
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($cbagian == '1') $this->db->where('registrasi.idjnspelayanan', 1, false);
		else if($cbagian == '2') $this->db->where('registrasi.idjnspelayanan', 2, false);
		else if($cbagian == '3') $this->db->where('registrasi.idjnspelayanan', 3, false);
		else $this->db->where_in('registrasi.idjnspelayanan',array(1,2,3));
		
		$q = $this->db->get();
		$reg = $q->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN OBAT DOKTER', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		
		foreach($reg AS $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"14%\">". $val->nmdoktergelar ."</td>
					<td width=\"8%\">". $val->nokuitansi ."</td>
					<td width=\"7%\">". $val->noreg ."</td>
					<td width=\"6%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"5%\">". $val->norm ."</td>
					<td width=\"11%\">". $val->nmpasien ."</td>
					<td width=\"15.5%\">". $val->nmbrg ."</td>
					<td width=\"5%\" align=\"right\">". $val->qty ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->hrgjual,0,',','.') ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->total,0,',','.') ."</td>
					<td width=\"9%\">". $val->nmjnspelayanan ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->hrgbeli,0,',','.') ."</td>
			</tr>";
		}
		
		$html = "<br/><br/><font size=\"8.5\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <!-- <thead> -->
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"14%\"><b>Dokter</b></td>
					<td width=\"8%\"><b>No. Kuitansi</b></td>
					<td width=\"7%\"><b>No. Registrasi</b></td>
					<td width=\"6%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"5%\"><b>No. RM</b></td>
					<td width=\"11%\"><b>Nama Pasien</b></td>
					<td width=\"15.5%\"><b>Nama Obat</b></td>
					<td width=\"5%\"><b>Qty</b></td>
					<td width=\"6%\"><b>Hrg. Jual</b></td>
					<td width=\"7%\"><b>Total</b></td>
					<td width=\"9%\"><b>Bagian</b></td>
					<td width=\"6%\"><b>Hrg. Beli</b></td>
				</tr>
			  <!-- </thead> -->
			  <tbody>". $isi ."</tbody>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('obatdokter.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function excelobatdokter($tglawal, $tglakhir, $cbagian) {
		$header = array(
			'Dokter',
			'No. Kuitansi',
			'No. Registrasi',
			'Tgl. Kuitansi',
			'No. RM',
			'Nama Pasien',
			'Nama Obat',
			'Qty',
			'Hrg.Jual',
			'Total',
			'Bagian',
			'Hrg.Beli'
		);
		
		$this->db->select("
			dokter.nmdoktergelar,
			kuitansi.nokuitansi,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			TRIM(LEADING '0' FROM pasien.norm) AS norm,
			pasien.nmpasien,
			barang.nmbrg,
			SUM(notadet.qty) AS qty,
			notadet.hrgjual as hrgjual,
			(SUM(notadet.qty) * ifnull(notadet.hrgjual, 0)) AS total,
			jpelayanan.nmjnspelayanan,			
			notadet.hrgbeli as hrgbeli
		", false);
        $this->db->from("barang");
        $this->db->join("notadet",
				"notadet.kditem = barang.kdbrg", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = registrasi.idjnspelayanan", "left");
				
		$this->db->groupby("barang.kdbrg, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, barang.nmbrg");
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($cbagian == '1') $this->db->where('registrasi.idjnspelayanan', 1, false);
		else if($cbagian == '2') $this->db->where('registrasi.idjnspelayanan', 2, false);
		else if($cbagian == '3') $this->db->where('registrasi.idjnspelayanan', 3, false);
		else $this->db->where_in('registrasi.idjnspelayanan',array(1,2,3));
		
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();
		
		$data['eksport'] = $fpl;
		//$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = 'Lap_Obat_Dokter_RJ_RI_UGD';
		$data['filter'] = "Laporan Obat Dokter RJ, RI, UGD\n".strtoupper('Tanggal')." : ".$tglawal." s/d ".$tglakhir."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellapfarmasi', $data); 	
	}
	
	function lapobatdokter_fl($tglawal, $tglakhir) {
		$this->db->select("
			   kuitansi.nokuitansi
			 , kuitansi.tglkuitansi
			 , kuitansi.atasnama as nmpasien
			 , barang.nmbrg
			 , sum(notadet.qty) AS qty
			 , notadet.hrgjual AS hrgjual
			 , (sum(notadet.qty) * ifnull(notadet.hrgjual, 0)) AS total
			 , notadet.hrgbeli AS hrgbeli
		", false);
        $this->db->from("barang");
        $this->db->join("notadet",
				"notadet.kditem = barang.kdbrg", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
				
		$this->db->groupby("barang.kdbrg, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, barang.nmbrg");
		$this->db->where('nota.idjnstransaksi = 8');
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		$q = $this->db->get();
		$reg = $q->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('P', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN OBAT FARMASI PASIEN LUAR', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		
		foreach($reg AS $i=>$val){
			$isi .= "<tr>
					<td width=\"7%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"12%\">". $val->nokuitansi ."</td>
					<td width=\"9%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"17%\">". $val->nmpasien ."</td>
					<td width=\"22%\">". $val->nmbrg ."</td>
					<td width=\"7%\" align=\"right\">". $val->qty ."</td>
					<td width=\"10%\" align=\"right\">". number_format($val->hrgjual,0,',','.') ."</td>
					<td width=\"10%\" align=\"right\">". number_format($val->total,0,',','.') ."</td>
					<td width=\"10%\" align=\"right\">". number_format($val->hrgbeli,0,',','.') ."</td>
			</tr>";
		}
		
		$html = "<br/><br/><font size=\"8\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <!-- <thead> -->
				<tr align=\"center\">
					<td width=\"7%\"><b>No.</b></td>
					<td width=\"12%\"><b>No. Kuitansi</b></td>
					<td width=\"9%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"17%\"><b>Nama Pasien</b></td>
					<td width=\"22%\"><b>Nama Obat</b></td>
					<td width=\"7%\"><b>Qty</b></td>
					<td width=\"10%\"><b>Hrg. Jual</b></td>
					<td width=\"10%\"><b>Total</b></td>
					<td width=\"10%\"><b>Hrg. Beli</b></td>
				</tr>
			  <!-- </thead> -->
			  <tbody>". $isi ."</tbody>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('obatfarmasipasienluar.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function excelobatdokter_fl($tglawal, $tglakhir) {
		$header = array(
			'No. Kuitansi',
			'Tgl. Kuitansi',
			'Nama Pasien',
			'Nama Obat',
			'Qty',
			'Hrg.Jual',
			'Total',
			'Hrg.Beli'
		);
		
		$this->db->select("
			   kuitansi.nokuitansi
			 , kuitansi.tglkuitansi
			 , kuitansi.atasnama as nmpasien
			 , barang.nmbrg
			 , sum(notadet.qty) AS qty
			 , notadet.hrgjual AS hrgjual
			 , (sum(notadet.qty) * ifnull(notadet.hrgjual, 0)) AS total
			 , notadet.hrgbeli AS hrgbeli
		", false);
        $this->db->from("barang");
        $this->db->join("notadet",
				"notadet.kditem = barang.kdbrg", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
				
		$this->db->groupby("barang.kdbrg, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, barang.nmbrg");
		$this->db->where('nota.idjnstransaksi = 8');
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		/* $data['eksport'] = $fpl;
		//$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprj', $data);  */

		$data['eksport'] = $fpl;
		//$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = 'Lap_Obat_Farmasi_Pasien_Luar';
		$data['filter'] = "Laporan Obat Farmasi Pasien Luar\n".strtoupper('Tanggal')." : ".$tglawal." s/d ".$tglakhir."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellapfarmasi', $data); 
		
	}
	
	function lappelayanan($tglawal, $tglakhir, $cberdasarkan1, $cberdasarkan2, $cberdasarkan3, $cberdasarkan4, $ccari1, $ccari2, $ccari3, $ccari4) {
		$this->db->select("
			jpelayanan.nmjnspelayanan,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			TRIM(LEADING '0' FROM pasien.norm) AS norm,
			if(pasien.norm>0, pasien.nmpasien, kuitansi.atasnama) AS nmpasien,
			pelayanan.nmpelayanan,
			pel.nmpelayanan AS parent,
			(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) AS tarif,
			notadet.qty AS qty,
			(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS diskon,
			((notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) * notadet.qty) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS total,
			dokter.nmdoktergelar
		", false);
        $this->db->from("pelayanan");
        $this->db->join("pelayanan pel",
				"pel.kdpelayanan = pelayanan.pel_kdpelayanan", "left");
        $this->db->join("notadet",
				"notadet.kditem = pelayanan.kdpelayanan", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = notadet.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = bagian.idjnspelayanan", "left");
				
		$this->db->groupby("pelayanan.kdpelayanan, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, pelayanan.nmpelayanan");
		$this->db->where('kuitansi.idstkuitansi = 1');
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($cberdasarkan1 == '1' && $ccari1 != '') $this->db->like('registrasi.noreg', $ccari1, false);
		else if($cberdasarkan1 == '2' && $ccari1 != '') $this->db->like('pasien.norm', $ccari1, false);
		else if($cberdasarkan1 == '3' && $ccari1 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari1), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari1), false);
		}
		else if($cberdasarkan1 == '4' && $ccari1 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '5' && $ccari1 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '6' && $ccari1 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '7' && $ccari1 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari1), false);
		
		if($cberdasarkan2 == '1' && $ccari2 != '') $this->db->like('registrasi.noreg', $ccari2, false);
		else if($cberdasarkan2 == '2' && $ccari2 != '') $this->db->like('pasien.norm', $ccari2, false);
		else if($cberdasarkan2 == '3' && $ccari2 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari2), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari2), false);
		}
		else if($cberdasarkan2 == '4' && $ccari2 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '5' && $ccari2 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '6' && $ccari2 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '7' && $ccari2 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari2), false);
		
		if($cberdasarkan3 == '1' && $ccari3 != '') $this->db->like('registrasi.noreg', $ccari3, false);
		else if($cberdasarkan3 == '2' && $ccari3 != '') $this->db->like('pasien.norm', $ccari3, false);
		else if($cberdasarkan3 == '3' && $ccari3 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari3), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari3), false);
		}
		else if($cberdasarkan3 == '4' && $ccari3 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '5' && $ccari3 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '6' && $ccari3 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '7' && $ccari3 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari3), false);
		
		if($cberdasarkan4 == '1' && $ccari4 != '') $this->db->like('registrasi.noreg', $ccari4, false);
		else if($cberdasarkan4 == '2' && $ccari4 != '') $this->db->like('pasien.norm', $ccari4, false);
		else if($cberdasarkan4 == '3' && $ccari4 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari4), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari4), false);
		}
		else if($cberdasarkan4 == '4' && $ccari4 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '5' && $ccari4 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '6' && $ccari4 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '7' && $ccari4 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari4), false);
		
		$q = $this->db->get();
		$pelayanan = $q->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN PELAYANAN', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		
		foreach($pelayanan AS $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"8%\">". $val->nmjnspelayanan ."</td>
					<td width=\"7%\">". $val->noreg ."</td>
					<td width=\"6%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"4%\">". $val->norm ."</td>
					<td width=\"10%\">". $val->nmpasien ."</td>
					<td width=\"14%\">". $val->nmpelayanan ."</td>
					<td width=\"10%\">". $val->parent ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->tarif,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->qty,2,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->diskon,0,',','.') ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->total,0,',','.') ."</td>
					<td width=\"16%\">". $val->nmdoktergelar ."</td>
			</tr>";
		}
		
		$html = "<br/><br/><font size=\"8.5\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <!-- <thead> -->
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"8%\"><b>Jenis Pelayanan</b></td>
					<td width=\"7%\"><b>No. Registrasi</b></td>
					<td width=\"6%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"4%\"><b>No. RM</b></td>
					<td width=\"10%\"><b>Nama Pasien</b></td>
					<td width=\"14%\"><b>Nama Pelayanan</b></td>
					<td width=\"10%\"><b>Parent</b></td>
					<td width=\"7%\"><b>Tarif</b></td>
					<td width=\"5%\"><b>Qty</b></td>
					<td width=\"5%\"><b>Diskon</b></td>
					<td width=\"7%\"><b>Total</b></td>
					<td width=\"16%\"><b>Dokter</b></td>
				</tr>
			  <!-- </thead> -->
			  <tbody>". $isi ."</tbody>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('obatdokter.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function excellappelayanan($tglawal, $tglakhir, $cberdasarkan1, $cberdasarkan2, $cberdasarkan3, $cberdasarkan4, $ccari1, $ccari2, $ccari3, $ccari4) {
		$header = array(
			'Jenis Pelayanan',
			'No. Registrasi',
			'Tgl. Kuitansi',
			'No. RM',
			'Nama Pasien',
			'Nama Pelayanan',
			'Parent',
			'Tarif',
			'Qty',
			'Diskon',
			'Total',
			'Dokter'
		);
		
		$this->db->select("
			jpelayanan.nmjnspelayanan,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			TRIM(LEADING '0' FROM pasien.norm) AS norm,
			if(pasien.norm>0, pasien.nmpasien, kuitansi.atasnama) AS nmpasien,
			pelayanan.nmpelayanan,
			pel.nmpelayanan AS parent,
			(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) AS tarif,
			notadet.qty AS qty,
			(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS diskon,
			((notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) * notadet.qty) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS total,
			dokter.nmdoktergelar
		", false);
        $this->db->from("pelayanan");
        $this->db->join("pelayanan pel",
				"pel.kdpelayanan = pelayanan.pel_kdpelayanan", "left");
        $this->db->join("notadet",
				"notadet.kditem = pelayanan.kdpelayanan", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = notadet.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = bagian.idjnspelayanan", "left");
				
		$this->db->groupby("pelayanan.kdpelayanan, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, pelayanan.nmpelayanan");
		$this->db->where('kuitansi.idstkuitansi = 1');
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($cberdasarkan1 == '1' && $ccari1 != '') $this->db->like('registrasi.noreg', $ccari1, false);
		else if($cberdasarkan1 == '2' && $ccari1 != '') $this->db->like('pasien.norm', $ccari1, false);
		else if($cberdasarkan1 == '3' && $ccari1 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari1), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari1), false);
		}
		else if($cberdasarkan1 == '4' && $ccari1 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '5' && $ccari1 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '6' && $ccari1 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '7' && $ccari1 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari1), false);
		
		if($cberdasarkan2 == '1' && $ccari2 != '') $this->db->like('registrasi.noreg', $ccari2, false);
		else if($cberdasarkan2 == '2' && $ccari2 != '') $this->db->like('pasien.norm', $ccari2, false);
		else if($cberdasarkan2 == '3' && $ccari2 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari2), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari2), false);
		}
		else if($cberdasarkan2 == '4' && $ccari2 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '5' && $ccari2 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '6' && $ccari2 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '7' && $ccari2 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari2), false);
		
		if($cberdasarkan3 == '1' && $ccari3 != '') $this->db->like('registrasi.noreg', $ccari3, false);
		else if($cberdasarkan3 == '2' && $ccari3 != '') $this->db->like('pasien.norm', $ccari3, false);
		else if($cberdasarkan3 == '3' && $ccari3 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari3), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari3), false);
		}
		else if($cberdasarkan3 == '4' && $ccari3 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '5' && $ccari3 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '6' && $ccari3 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '7' && $ccari3 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari3), false);
		
		if($cberdasarkan4 == '1' && $ccari4 != '') $this->db->like('registrasi.noreg', $ccari4, false);
		else if($cberdasarkan4 == '2' && $ccari4 != '') $this->db->like('pasien.norm', $ccari4, false);
		else if($cberdasarkan4 == '3' && $ccari4 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari4), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari4), false);
		}
		else if($cberdasarkan4 == '4' && $ccari4 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '5' && $ccari4 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '6' && $ccari4 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '7' && $ccari4 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari4), false);
		
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		//$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprj', $data); 	
	}
	
	function excellappelayananadm($tglawal, $tglakhir, $cberdasarkan1, $cberdasarkan2, $cberdasarkan3, $cberdasarkan4, $ccari1, $ccari2, $ccari3, $ccari4) {
		$header = array(
			'Jenis Pelayanan',
			'No. Registrasi',
			'Tgl. Kuitansi',
			'No. RM',
			'Nama Pasien',
			'Nama Pelayanan',
			'Parent',
			'Tarif',
			'Qty',
			'Diskon',
			'Total',
			'Dokter',
			'Tarif JS',
			'Tarif JM',
			'Tarif JP',
			'Tarif BHP',
			'Total Tarif',
			'Diskon JS',
			'Diskon JM',
			'Diskon JP',
			'Diskon BHP',
			'Jumlah Diskon'
		);
		
		$this->db->select("
			jpelayanan.nmjnspelayanan,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			TRIM(LEADING '0' FROM pasien.norm) AS norm,
			if(pasien.norm>0, pasien.nmpasien, kuitansi.atasnama) AS nmpasien,
			pelayanan.nmpelayanan,
			pel.nmpelayanan AS parent,
			(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) AS tarif,
			notadet.qty AS qty,
			(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS diskon,
			((notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) * notadet.qty) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS total,
			dokter.nmdoktergelar,
			(notadet.tarifjs * notadet.qty) as tarifjs,
			(notadet.tarifjm * notadet.qty) as tarifjm,
			(notadet.tarifjp * notadet.qty) as tarifjp,
			(notadet.tarifbhp * notadet.qty) as tarifbhp,
			((notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) * notadet.qty) AS totaltarif,
			notadet.diskonjs,
			notadet.diskonjm,
			notadet.diskonjp,
			notadet.diskonbhp,
			(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS totaldiskon
		", false);
        $this->db->from("pelayanan");
        $this->db->join("pelayanan pel",
				"pel.kdpelayanan = pelayanan.pel_kdpelayanan", "left");
        $this->db->join("notadet",
				"notadet.kditem = pelayanan.kdpelayanan", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = notadet.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = bagian.idjnspelayanan", "left");
				
		$this->db->groupby("pelayanan.kdpelayanan, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, pelayanan.nmpelayanan");
		$this->db->where('kuitansi.idstkuitansi = 1');
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($cberdasarkan1 == '1' && $ccari1 != '') $this->db->like('registrasi.noreg', $ccari1, false);
		else if($cberdasarkan1 == '2' && $ccari1 != '') $this->db->like('pasien.norm', $ccari1, false);
		else if($cberdasarkan1 == '3' && $ccari1 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari1), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari1), false);
		}
		else if($cberdasarkan1 == '4' && $ccari1 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '5' && $ccari1 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '6' && $ccari1 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '7' && $ccari1 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari1), false);
		
		if($cberdasarkan2 == '1' && $ccari2 != '') $this->db->like('registrasi.noreg', $ccari2, false);
		else if($cberdasarkan2 == '2' && $ccari2 != '') $this->db->like('pasien.norm', $ccari2, false);
		else if($cberdasarkan2 == '3' && $ccari2 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari2), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari2), false);
		}
		else if($cberdasarkan2 == '4' && $ccari2 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '5' && $ccari2 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '6' && $ccari2 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '7' && $ccari2 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari2), false);
		
		if($cberdasarkan3 == '1' && $ccari3 != '') $this->db->like('registrasi.noreg', $ccari3, false);
		else if($cberdasarkan3 == '2' && $ccari3 != '') $this->db->like('pasien.norm', $ccari3, false);
		else if($cberdasarkan3 == '3' && $ccari3 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari3), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari3), false);
		}
		else if($cberdasarkan3 == '4' && $ccari3 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '5' && $ccari3 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '6' && $ccari3 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '7' && $ccari3 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari3), false);
		
		if($cberdasarkan4 == '1' && $ccari4 != '') $this->db->like('registrasi.noreg', $ccari4, false);
		else if($cberdasarkan4 == '2' && $ccari4 != '') $this->db->like('pasien.norm', $ccari4, false);
		else if($cberdasarkan4 == '3' && $ccari4 != ''){
			$this->db->like('LOWER(pasien.nmpasien)', strtolower($ccari4), false);
			$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari4), false);
		}
		else if($cberdasarkan4 == '4' && $ccari4 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '5' && $ccari4 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '6' && $ccari4 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '7' && $ccari4 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari4), false);
		
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		//$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprj', $data); 	
	}
	
	function namaBulan($tgl){
		$arrtgl = explode('-', $tgl);
		$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
		$bulan = $query->row_array();
		$tanggalInd = $arrtgl[2] .' '. $bulan['nmbulan'] .' '. $arrtgl[0];
		
		return $tanggalInd ;
	}
	

function lappenerimaanbank($tglawal, $tglakhir, $idbank) {
		
    if(empty($tglawal)) $tglawal = date('Y-m-d');
    if(empty($tglakhir)) $tglakhir = date('Y-m-d');
    if(empty($idbank)) $idbank = 1;

    $query = "SELECT * FROM 
              (((
                  kuitansi join 
                  kuitansidet ON 
                    (
                      kuitansi.nokuitansi = kuitansidet.nokuitansi AND
                      kuitansi.idstkuitansi = 1 AND 
                      kuitansidet.idcarabayar = 2
                    ) 
                  ) left join bank on kuitansidet.idbank = bank.idbank) 
                  left join jkuitansi on kuitansi.idjnskuitansi = jkuitansi.idjnskuitansi)
              having 
                kuitansi.tglkuitansi >= '".$tglawal."' AND 
                kuitansi.tglkuitansi <= '".$tglakhir."' AND 
                kuitansidet.idbank = '".$idbank."'";
    $get = $this->db->query($query);
    $ttl = $get->num_rows();
    $data = $get->result();        
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		$tgl = $this->namaBulan(date('Y-m-d'));
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Penerimaan Bank', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$tot_tagihan = 0;
		$tot_transfer = 0;
		
		foreach($data AS $i=>$val){
						
			$isi .= "<tr>
					<td align=\"center\">". ($i+1) ."</td>
					<td>". $val->nokuitansi ."</td>
					<td>". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td>". $val->atasnama ."</td>
					<td>". $val->nmjnskuitansi ."</td>
					<td>". $val->nmbank ."</td>
					<td>". $val->nokartu ."</td>
					<td align=\"right\">". number_format($val->pembayaran,0,',','.') ."</td>
					<td align=\"right\">". number_format($val->jumlah,0,',','.') ."</td>
			</tr>";
			
			$tot_tagihan += $val->pembayaran;
			$tot_transfer += $val->jumlah;
			
		}
		
		$html = "<br/><br/><font size=\"8\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"5%\"><b>No.</b></td>
					<td width=\"10%\" align=\"center\"><b>No. Kuitansi</b></td>
					<td width=\"10%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"20%\"><b>Atas Nama</b></td>
					<td width=\"20%\"><b>Jenis Kuitansi</b></td>
					<td width=\"5%\"><b>Bank</b></td>
					<td width=\"10%\"><b>No. Kartu</b></td>
					<td width=\"10%\"><b>Total Tagihan</b></td>
					<td width=\"10%\"><b>Total Transfer</b></td>
				</tr>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td colspan=\"7\" align=\"center\"><b>T O T A L</b></td>
					<td align=\"right\"><b>". number_format($tot_tagihan,0,',','.') ."</b></td>
					<td align=\"right\"><b>". number_format($tot_transfer,0,',','.') ."</b></td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		

		//Close and output PDF document
		$this->pdf->Output('Pendapatan_Bank.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	function excelpenerimaanbank($tglawal, $tglakhir, $ctransfer) {
		if(empty($tglawal)) $tglawal = date('Y-m-d');
    if(empty($tglakhir)) $tglakhir = date('Y-m-d');
    if(empty($idbank)) $idbank = 1;

    $query = "SELECT
    					kuitansi.nokuitansi as nokuitansi,
    					kuitansi.tglkuitansi as tglkuitansi,
    					kuitansi.atasnama as atasnama,
    					jkuitansi.nmjnskuitansi as nmjnskuitansi,
    					kuitansidet.idbank as idbank,
    					bank.nmbank as nmbank,
    					kuitansidet.nokartu as nokartu,
    					kuitansi.pembayaran as pembayaran,
    					kuitansidet.jumlah as jumlah
    					FROM 
              (((
                  kuitansi join 
                  kuitansidet ON 
                    (
                      kuitansi.nokuitansi = kuitansidet.nokuitansi AND
                      kuitansi.idstkuitansi = 1 AND 
                      kuitansidet.idcarabayar = 2
                    ) 
                  ) left join bank on kuitansidet.idbank = bank.idbank) 
                  left join jkuitansi on kuitansi.idjnskuitansi = jkuitansi.idjnskuitansi)
              having 
                kuitansi.tglkuitansi >= '".$tglawal."' AND 
                kuitansi.tglkuitansi <= '".$tglakhir."' AND 
                kuitansidet.idbank = ".$idbank."";
    $get = $this->db->query($query);
    $ttl = $get->num_rows();
    $data = $get->result();        
		
		$header = array(
			'No. Kuitansi',
			'Tgl. Kuitansi',
			'Atas Nama',
			'Jenis Kuitansi',
			'Nama Bank',
			'No. Kartu',
			'Total Pembayaran',
			'Total Transfer',
		);
		
		foreach($data as $idx => $dt){
			$data[$idx]->nokartu = "'".$dt->nokartu;
			unset($data[$idx]->idbank);
		}
		
		//$data['eksport'] = $data;
		$data['data'] = $data;
		$data['fieldname'] = $header;
		$data['numrows'] = $ttl;
		$data['filter'] = '';
		$data['filename'] = 'penerimaan_bank_'.$tglawal.'-'.$tglakhir;
		$this->load->view('exportexcelfilterarray', $data); 	
	}

}
