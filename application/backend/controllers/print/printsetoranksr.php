<?php
class Printsetoranksr extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
    
	function printstrnksr($tglawal) {
		//rj		
		$this->db->select("`nota`.`tglnota` AS `tglnota`
					 , `bag`.`nmbagian` AS `nmbagian`     
					 , sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
							FROM
							  (`kuitansidet` `kd`
							LEFT JOIN `kuitansi` `k`
							ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
							WHERE
							  ((`kd`.`idcarabayar` = '1')
							  AND (`k`.`nokuitansi` = nota.`nokuitansi`)
							  AND (`k`.`tglkuitansi` = nota.`tglnota`)
							  AND (`kui`.`idstkuitansi` <> 2)))) AS `totbyrtunai`
					, sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
						  FROM
							(`kuitansidet` `kd`
						  LEFT JOIN `kuitansi` `k`
						  ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
						  WHERE
							((`kd`.`idcarabayar` <> '1')
							AND (`k`.`nokuitansi` = nota.`nokuitansi`)
							AND (`k`.`tglkuitansi` = nota.`tglnota`)
							AND (`k`.`idstkuitansi` = '1')))) AS `totbyrnontunai`
								
					, (sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
							FROM
							  (`kuitansidet` `kd`
							LEFT JOIN `kuitansi` `k`
							ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
							WHERE
							  ((`kd`.`idcarabayar` = '1')
							  AND (`k`.`nokuitansi` = nota.`nokuitansi`)
							  AND (`k`.`tglkuitansi` = nota.`tglnota`)
							  AND (`kui`.`idstkuitansi` <> 2)))) + sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
																	   FROM
																		 (`kuitansidet` `kd`
																	   LEFT JOIN `kuitansi` `k`
																	   ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
																	   WHERE
																		 ((`kd`.`idcarabayar` <> '1')
																		 AND (`k`.`nokuitansi` = nota.`nokuitansi`)
																		 AND (`k`.`tglkuitansi` = nota.`tglnota`)
																		 AND (`k`.`idstkuitansi` = '1'))))) as total
		", false);
        $this->db->from("nota");
		$this->db->join("bagian bag",
				"bag.idbagian = nota.idbagian", "left");
		$this->db->join("kuitansi kui",
				"kui.nokuitansi = nota.nokuitansi", "left");
				
		$this->db->where("bag.idjnspelayanan", 1, false);
		$this->db->where("nota.tglnota = '". $tglawal ."'");
		$this->db->groupby("nota.tglnota, nota.idbagian");
		
		$q = $this->db->get();
		$totrjunitpelayanan = $q->result();
		
		//ri
		/* $this->db->select("
			       v_sumv5.nmbagian AS nmbagian
				 , v_sumv5.tglkuitansi AS tglkuitansi
				 , sum(v_sumv5.total) AS total
				 , sum(v_sumv5.jmlbyar) AS jmlbayar
				 , v_sumv5.dijamin AS dijamin
				 , v_sumv5.`totbyrtunai` AS `totbyrtunai`
				 , v_sumv5.`totbyrnontunai` AS `totbyrnontunai`
		", false);
        $this->db->from("v_sumv5_uangrdiskdep_strnksr v_sumv5");
				
		$this->db->where('v_sumv5.idjnspelayanan', 2, false);
		$this->db->where('v_sumv5.tglkuitansi', "'". $tglawal ."'", false);
		$this->db->groupby("v_sumv5.tglkuitansi, v_sumv5.idbagian");
		$this->db->order_by("v_sumv5.nmbagian"); */
		
		$this->db->select("
			kuitansi.nokuitansi,
			kuitansi.tglkuitansi,
			registrasi.noreg,
			trim(LEADING '0' FROM pasien.norm) AS norm,
			pasien.nmpasien,
			penjamin.nmpenjamin,
			dokter.nmdoktergelar,
			(
				select tglmasuk
				from registrasidet rd
				where rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS tglmasuk, 
			(
				select nmbagian
				from bagian, registrasidet rd
				where bagian.idbagian = rd.idbagian AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS nmbagian, 
			(
				select rd.noreg
				from nota nt, registrasidet rd
				where nt.idregdettransfer = registrasi.noreg AND
					rd.idregdet = nt.idregdet
			) AS transferugd,
			(
				select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
				from nota nt, registrasidet rd, notadet ntd
				where nt.idregdettransfer = registrasi.noreg AND
					rd.idregdet = nt.idregdet AND
					ntd.nonota = nt.nonota
			) AS nominalugd,
			(SUM(notadet.tarifjs*notadet.qty)+SUM(notadet.tarifjm*notadet.qty)+SUM(notadet.tarifjp*notadet.qty)+SUM(notadet.tarifbhp*notadet.qty)+nota.uangr) AS nominalri,
			(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon) AS diskon,
			(
				select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
				from kuitansi kui, registrasidet rd
				where kui.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg AND
					kui.idjnskuitansi = 5 AND
					kui.idstkuitansi = 1
			) AS deposit,
			(
				select nt.userinput
				from nota nt, registrasidet rd
				where nt.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS userid,
			ifnull((
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar <> 1
			), 0) AS totbyrnontunai,
			ifnull((
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar = 1
			), 0) AS totbyrtunai
		", false);
        $this->db->from("registrasi");
        $this->db->join("registrasidet",
				"registrasidet.noreg = registrasi.noreg", "left");
        $this->db->join("nota",
				"nota.idregdet = registrasidet.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        $this->db->join("nota nota2",
				"nota2.idregdettransfer = registrasi.noreg", "left");
				
		$this->db->where('registrasi.idjnspelayanan', 2, false);
		$this->db->where("kuitansi.tglkuitansi = '". $tglawal ."'");
		$this->db->groupby("registrasi.noreg");
		
		$q = $this->db->get();
		$totriunitpelayanan = $q->result();
		
		//detaildepposit
		$this->db->select("tglkuitansi as tglkuitansi
						, noreg as noreg						
						, trim(LEADING '0' FROM norm) AS norm
						, nmpasien as nmpasien
						, nmbagian as nmbagian
						, total as total
						, ifnull(nontunai, 0) as nontunai
						, ifnull(tunai, 0) as tunai", false);
        $this->db->from("v_lapdeposit");		
		$this->db->where("v_lapdeposit.tglkuitansi = '". $tglawal ."'");
		$q = $this->db->get();
		$totdetdaposit = $q->result();
		
		//totfarpasluar
		$this->db->select("sum((SELECT sum(`kd`.`jumlah`) AS `totaldijamin`
								FROM
								  ((`kuitansidet` `kd`
								LEFT JOIN `kuitansi` `kui`
								ON ((`kui`.`nokuitansi` = `kd`.`nokuitansi`)))
								LEFT JOIN `nota` `n`
								ON ((`n`.`nokuitansi` = `kui`.`nokuitansi`)))
								WHERE
								  `n`.`idjnstransaksi` = '8'
								  AND
								  kui.tglkuitansi = `kuitansi`.tglkuitansi
								  AND `kd`.`idcarabayar` <> 1
								  AND `kui`.`idstkuitansi` = '1'
								  AND `kd`.nokuitansi = kuitansi.nokuitansi)) AS totnontunai
						 , sum((SELECT sum(`kd`.`jumlah`) AS `totaldijamin`
								FROM
								  ((`kuitansidet` `kd`
								LEFT JOIN `kuitansi` `kui`
								ON ((`kui`.`nokuitansi` = `kd`.`nokuitansi`)))
								LEFT JOIN `nota` `n`
								ON ((`n`.`nokuitansi` = `kui`.`nokuitansi`)))
								WHERE
								  `n`.`idjnstransaksi` = '8'
								  AND
								  kui.tglkuitansi = `kuitansi`.tglkuitansi
								  AND `kd`.`idcarabayar` = 1
								  AND `kui`.`idstkuitansi` = '1'
								  AND `kd`.nokuitansi = kuitansi.nokuitansi)) AS tottunai
		", false);
        $this->db->from("kuitansi");
		$this->db->where('kuitansi.tglkuitansi', "'". $tglawal ."'", false);
		
		$q = $this->db->get();
		$farpasluartot = $q->result();		
		
		//ugd
		$this->db->select("kui.tglkuitansi AS tglnota
				 , sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
						FROM
						  (`kuitansidet` `kd`
						LEFT JOIN `kuitansi` `k`
						ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
						WHERE
						  ((`kd`.`idcarabayar` = '1')
						  AND (`k`.`nokuitansi` = `nt`.`nokuitansi`)
						  AND (`k`.`tglkuitansi` = `nt`.`tglnota`)
						  AND (kui.idstkuitansi = 1)))) AS totbyrtunai
				 , sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
						FROM
						  (`kuitansidet` `kd`
						LEFT JOIN `kuitansi` `k`
						ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
						WHERE
						  ((`kd`.`idcarabayar` <> '1')
						  AND (`k`.`nokuitansi` = `nt`.`nokuitansi`)
						  AND (`k`.`tglkuitansi` = `nt`.`tglnota`)
						  AND (kui.idstkuitansi = 1)))) AS totbyrnontunai
		", false);
        $this->db->from("`nota` nt");
		$this->db->join("`kuitansi` kui",
				"kui.nokuitansi = nt.nokuitansi", "left");		
		$this->db->where('nt.idbagian =', 12, false);
		$this->db->where('kui.tglkuitansi', "'". $tglawal ."'", false);
		$this->db->where('nt.idregdettransfer IS NULL');
		$this->db->groupby('kui.tglkuitansi');	
		
		$q = $this->db->get();
		$ugdpasluartot = $q->result();		
		
		//peltambahan
		$this->db->select("kuitansi.`tglkuitansi` AS `tglkuitansi`
						 , ifnull(sum((SELECT `kd`.`jumlah` AS `totbyrtunai`
								FROM
								  ((`kuitansidet` `kd`
								LEFT JOIN `kuitansi` `kui`
								ON ((`kui`.`nokuitansi` = `kd`.`nokuitansi`)))
								LEFT JOIN `nota` `n`
								ON ((`n`.`nokuitansi` = `kui`.`nokuitansi`)))
								WHERE
								  `n`.idbagian = '35'
								  AND
								  kui.tglkuitansi = kuitansi.tglkuitansi
								  AND `kd`.`idcarabayar` = '1'
								  AND `kui`.`idstkuitansi` = kuitansi.idstkuitansi
								  AND `kui`.nokuitansi = `kuitansi`.nokuitansi)), 0) AS totbyrtunai
						 , ifnull(sum((SELECT `kd`.`jumlah` AS totbyrnontunai
								FROM
								  ((`kuitansidet` `kd`
								LEFT JOIN `kuitansi` `kui`
								ON ((`kui`.`nokuitansi` = `kd`.`nokuitansi`)))
								LEFT JOIN `nota` `n`
								ON ((`n`.`nokuitansi` = `kui`.`nokuitansi`)))
								WHERE
								  `n`.idbagian = '35'
								  AND
								  kui.tglkuitansi = kuitansi.tglkuitansi
								  AND `kd`.`idcarabayar` <> '1'
								  AND `kui`.`idstkuitansi` = kuitansi.idstkuitansi
								  AND `kui`.nokuitansi = `kuitansi`.nokuitansi)), 0) AS totbyrnontunai
		", false);
        $this->db->from("kuitansi");
		$this->db->where('kuitansi.tglkuitansi', "'". $tglawal ."'", false);
		
		$q = $this->db->get();
		$peltambahan = $q->result();
						
		//retfarrj
		$this->db->select("*");
        $this->db->from("v_totretfarrawjln");
		$this->db->where('v_totretfarrawjln.tglreturfarmasi', "'". $tglawal ."'", false);
		
		$q = $this->db->get();
		$retfarrj = $q->result();
		
		//retfarri
		$this->db->select("sum(v_returfarmasidet.subtotal) AS totretfarrawri", false);
        $this->db->from("v_returfarmasidet");
		$this->db->where_in('v_returfarmasidet.idjnspelayanan', array(2,3), false);
		$this->db->where('v_returfarmasidet.tglreturfarmasi', "'". $tglawal ."'", false);
		
		$q = $this->db->get();
		$retfarri = $q->result();
		
		//retfarpasluar
		$this->db->select("(100 * ceiling((sum(v_returfarmasidet.subtotal) / 100))) AS totretfarluar", false);
        $this->db->from("v_returfarmasidet");
		$this->db->where('v_returfarmasidet.idjnspelayanan IS NULL');
		$this->db->where('v_returfarmasidet.tglreturfarmasi', "'". $tglawal ."'", false);
		
		$q = $this->db->get();
		$retfarpasluar = $q->result();	
		
		//retdepposit
		$this->db->select("jurnal.tgltransaksi AS tgltransaksi
				, sum(jurnal.nominal) AS totretdeposit", false);
        $this->db->from("jurnal");
		$this->db->where('jurnal.idjnstransaksi', 16, false);
		$this->db->where('jurnal.tgltransaksi', "'". $tglawal ."'", false);
		
		$q = $this->db->get();
		$retdepposit = $q->result();
		
		//totalallcarabayar
		$this->db->select("ifnull(sum(kd.jumlah), 0) AS totallcarabyrtunai", false);
        $this->db->from("kuitansidet kd");
		$this->db->join("kuitansi k",
				"k.nokuitansi = kd.nokuitansi", "left"); 
				
		$this->db->where("kd.idcarabayar <> '9'");
		$this->db->where("k.idstkuitansi = '1'");
		$this->db->where('k.tglkuitansi', "'". $tglawal ."'", false);
		
		$q = $this->db->get();
		$totalallcarabayar = $q->result();
		
		//carabayar
		$this->db->select("kui.tglkuitansi AS tglkuitansi
						 , cb.idcarabayar AS idcarabayar
						 , cb.nmcarabayar AS nmcarabayar
						 , (SELECT ifnull(sum(kd.jumlah), 0) AS carabayar
							FROM
							  kuitansidet kd
							LEFT JOIN kuitansi k
							ON k.nokuitansi = kd.nokuitansi
							WHERE
							  kd.idcarabayar = kdt.idcarabayar
							  AND
							  k.tglkuitansi = '".$tglawal."'
							  AND k.idstkuitansi = '1'
						   ) AS totcarabyr", false);
        $this->db->from("carabayar cb");
		$this->db->join("kuitansidet kdt",
				"kdt.idcarabayar = cb.idcarabayar", "left"); 
		$this->db->join("kuitansi kui",
				"kui.nokuitansi = kdt.nokuitansi", "left"); 
				
		$this->db->where("cb.idcarabayar <> '9'");
		$this->db->where("kui.idstkuitansi = '1'");
		$this->db->groupby("cb.idcarabayar");
		
		$q = $this->db->get();
		$vcarabayar = $q->result();
				
		//setoranksr
		$this->db->select("*");
        $this->db->from("v_setorankasirv2");
		$this->db->where('v_setorankasirv2.tgltransaksi', "'". $tglawal ."'", false);
		
		$q = $this->db->get();
		$setoranksr = $q->result();
		$setorankasir = $q->row_array();
		
		/* var_dump($setorankasir);
		exit ; */
				
		$tanggal = $this->namaBulan($tglawal);
		$tgl = $this->namaBulan(date('Y-m-d'));
				
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(true);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT+5, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=30;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(170, 0, 'SETORAN KASIR', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(170, 0, 'Tgl. Transaksi : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$x+=25;$y=21;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, 'PEMASUKAN : ', '', 1, 0, true, 'L', true);
				
		$isi = '';
		$totalrj = 0;
		$totbyrtunairj = 0;
		$totbyrnontunairj = 0;
		
		foreach($totrjunitpelayanan AS $i=>$val){
			
			$total 		 	= $val->total;
			$totbyrtunai 	= $val->totbyrtunai;
			$totbyrnontunai = $val->totbyrnontunai;
			
			$isi .= "<tr>
					<td width=\"4%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"56%\">". $val->nmbagian ."</td>
					<td width=\"12%\" align=\"right\">". number_format($total,0,',','.') ."</td>
					<td width=\"12%\" align=\"right\">". number_format($totbyrtunai,0,',','.') ."</td>
					<td width=\"12%\" align=\"right\">". number_format($totbyrnontunai,0,',','.') ."</td>
			</tr>";
			
			$totalrj += $total;
			$totbyrtunairj += $totbyrtunai;
			$totbyrnontunairj += $totbyrnontunai;
			
		}
		
		//
		$isiri = '';
		$nominalugd = 0;
		$tertagih = 0;
		$deposit = 0;
		$totbyrtunairi = 0;
		$totbyrnontunairi = 0;
		$totalri = 0;
		
		foreach($totriunitpelayanan AS $i=>$val){
			if($nominalugd == null) $nominalugd = 0;
			$tertagih = $nominalugd + $val->nominalri - $val->diskon;
			if($tertagih < 0) $tertagih = 0;
			
			$deposit 	= $val->deposit;
			$totbyrtunai 	= $val->totbyrtunai;
			$totbyrnontunai = $val->totbyrnontunai;
			
			$isiri .= "<tr>
					<td width=\"4%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"10%\">". $val->noreg ."</td>
					<td width=\"7%\">". $val->norm ."</td>
					<td width=\"16%\">". $val->nmpasien ."</td>
					<td width=\"11%\" align=\"center\">". $val->nmbagian ."</td>
					<td width=\"12%\" align=\"right\">". number_format($tertagih,0,',','.') ."</td>
					<td width=\"12%\" align=\"right\">". number_format($deposit,0,',','.') ."</td>
					<td width=\"12%\" align=\"right\">". number_format($totbyrtunai,0,',','.') ."</td>
					<td width=\"12%\" align=\"right\">". number_format($totbyrnontunai,0,',','.') ."</td>
			</tr>";
			
			$totbyrtunairi += $totbyrtunai;
			$totbyrnontunairi += $totbyrnontunai;
			$totalri = $totbyrtunairi + $totbyrnontunairi;
			
		}
		
		//
		$isidetdep = '';
		$total = 0;
		$totbyrtunaidetdap = 0;
		$totbyrnontunaidetdap = 0;
		$totaldetdep = 0;
		
		foreach($totdetdaposit AS $i=>$val){
			
			$total 		= $val->total;
			$totbyrtunai 	= $val->tunai;
			$totbyrnontunai = $val->nontunai;
			
			$isidetdep .= "<tr>
					<td width=\"4%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"9%\">". $val->noreg ."</td>
					<td width=\"8%\">". $val->norm ."</td>
					<td width=\"27%\">". $val->nmpasien ."</td>
					<td width=\"12%\" align=\"center\">". $val->nmbagian ."</td>
					<td width=\"12%\" align=\"right\">". number_format($total,0,',','.') ."</td>
					<td width=\"12%\" align=\"right\">". number_format($totbyrtunai,0,',','.') ."</td>
					<td width=\"12%\" align=\"right\">". number_format($totbyrnontunai,0,',','.') ."</td>
			</tr>";
			
			$totaldetdep += $total;
			$totbyrtunaidetdap += $totbyrtunai;
			$totbyrnontunaidetdap += $totbyrnontunai;
			
		}
		
		//
		$isifarpasluar 		= '';
		$totfarpasluar 		= 0;
		$totfarpasluar 		= 0;
		$totfarpasluar 		= 0;
		
		foreach($farpasluartot AS $i=>$val){
			
			$farpasluartottunai 	= $val->tottunai;
			$farpasluartotnontunai 	= $val->totnontunai;
			$totfarpasluar	= $farpasluartottunai + $farpasluartotnontunai;
			
			$isifarpasluar .= "<tr>
					<td width=\"60%\" align=\"left\"><b>IV. Farmasi Pasien Luar</b></td>					
					<td width=\"12%\" align=\"right\"><b>". number_format($totfarpasluar,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($farpasluartottunai,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($farpasluartotnontunai,0,',','.') ."</b></td>
			</tr>";		
				
			
		}
		
		//
		$isiugd = '';
		$totugd	= 0;
		$ugdtotbyrtunai	= 0;
		$ugdtotbyrnontunai 	= 0;
		
		foreach($ugdpasluartot AS $i=>$val){
			
			//$total 		 	= $val->totalugd;			
			$totbyrtunai 	= $val->totbyrtunai;			
			$totbyrnontunai	= $val->totbyrnontunai;	
			$ugdtotbyrtunai 	+= $totbyrtunai;
			$ugdtotbyrnontunai 	+= $totbyrnontunai;		
			$totugd				= $ugdtotbyrtunai + $ugdtotbyrnontunai;
			
			$isiugd .= "<tr>
					<td width=\"60%\" align=\"left\"><b>V. UGD</b></td>					
					<td width=\"12%\" align=\"right\"><b>". number_format($totugd,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($ugdtotbyrtunai,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($ugdtotbyrnontunai,0,',','.') ."</b></td>
			</tr>";		
			
		}
		
		//
		$isipeltambahan = '';
		$totpeltambahan	= 0;
		$peltambahantotbyrtunai	= 0;
		$peltambahantotbyrnontunai 	= 0;
		
		foreach($peltambahan AS $i=>$val){		
			
			$totbyrtunai 				= $val->totbyrtunai;			
			$totbyrnontunai				= $val->totbyrnontunai;	
			$peltambahantotbyrtunai 	+= $totbyrtunai;
			$peltambahantotbyrnontunai 	+= $totbyrnontunai;		
			$totpeltambahan				= $peltambahantotbyrtunai + $peltambahantotbyrnontunai;
			
			$isipeltambahan .= "<tr>
					<td width=\"60%\" align=\"left\"><b>VI. Pelayanan Tambahan</b></td>					
					<td width=\"12%\" align=\"right\"><b>". number_format($totpeltambahan,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($peltambahantotbyrtunai,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($peltambahantotbyrnontunai,0,',','.') ."</b></td>
				</tr>";		
			
		}
		
		//jumlah pemasukan
		$totpemasukan 			 = $totalrj + $totalri + $totfarpasluar + $totugd + $totpeltambahan + $totaldetdep;
		$totbyrtunaipemasukan 	 = $totbyrtunairj + $totbyrtunairi + $farpasluartottunai + $ugdtotbyrtunai + $peltambahantotbyrtunai + $totbyrtunaidetdap;
		$totbyrnontunaipemasukan = $totbyrnontunairj + $totbyrnontunairi + $farpasluartotnontunai + $ugdtotbyrnontunai + $peltambahantotbyrnontunai + $totbyrnontunaidetdap;
		
		//---------------------------------------------------------------------------------------
		
		$htmlpemasukan = "<br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
			    <tr align=\"left\">
					<td width=\"96%\"><b>I. Rawat Jalan</b></td>
				</tr>
				<tr align=\"center\">
					<td width=\"4%\"><b>No.</b></td>
					<td width=\"56%\"><b>Unit Pelayanan</b></td>
					<td width=\"12%\"><b>Total</b></td>
					<td width=\"12%\"><b>Tunai</b></td>
					<td width=\"12%\"><b>Non Tunai</b></td>
				</tr>
			  </thead>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"60%\" align=\"right\"><b>Jumlah</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totalrj,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totbyrtunairj,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totbyrnontunairj,0,',','.') ."</b></td>
				</tr>
				<tr align=\"left\">
					<td width=\"96%\"><b>II. Rawat Inap</b></td>
				</tr>
				<tr align=\"center\">
					<td width=\"4%\"><b>No.</b></td>
					<td width=\"10%\"><b>No. Reg</b></td>
					<td width=\"7%\"><b>No. RM</b></td>
					<td width=\"16%\"><b>Nama Pasien</b></td>
					<td width=\"11%\"><b>Ruangan</b></td>
					<td width=\"12%\"><b>Tertagih</b></td>
					<td width=\"12%\"><b>Deposit</b></td>
					<td width=\"12%\"><b>Tunai</b></td>
					<td width=\"12%\"><b>Non Tunai</b></td>
				</tr>
			  <tbody>". $isiri ."</tbody>
				<tr>
					<td width=\"60%\" align=\"right\"><b>Jumlah</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totalri,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totbyrtunairi,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totbyrnontunairi,0,',','.') ."</b></td>
				</tr>
				<tr align=\"left\">
					<td width=\"96%\"><b>III. Detail Deposit Per Hari</b></td>
				</tr>
				<tr align=\"center\">
					<td width=\"4%\"><b>No.</b></td>
					<td width=\"9%\"><b>No. Reg</b></td>
					<td width=\"8%\"><b>No. RM</b></td>
					<td width=\"27%\"><b>Nama Pasien</b></td>
					<td width=\"12%\"><b>Ruangan</b></td>
					<td width=\"12%\"><b>Total</b></td>
					<td width=\"12%\"><b>Tunai</b></td>
					<td width=\"12%\"><b>Non Tunai</b></td>
				</tr>
			  <tbody>". $isidetdep ."</tbody>
				<tr>
					<td width=\"60%\" align=\"right\"><b>Jumlah</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totaldetdep,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totbyrtunaidetdap,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totbyrnontunaidetdap,0,',','.') ."</b></td>
				</tr>
			  <tbody>". $isifarpasluar ."</tbody>
			  <tbody>". $isiugd ."</tbody>
			  <tbody>". $isipeltambahan ."</tbody>
				<tr>
					<td width=\"60%\" align=\"right\"><b>Jumlah Pemasukan</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totpemasukan,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totbyrtunaipemasukan,0,',','.') ."</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totbyrnontunaipemasukan,0,',','.') ."</b></td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($htmlpemasukan,true,false,false,false); 
		
		//---------------------------------------------------------------------------------------
				
		//
		$tretfarrawjln = 0;		
		foreach($retfarrj AS $i=>$val){			
			$total 		 	= $val->totretfarrawjln;				
			$tretfarrawjln	+= $total;				
		}
		
		//
		$totretfarri = 0;		
		foreach($retfarri AS $i=>$val){			
			$total 		 	= $val->totretfarrawri;				
			$totretfarri	+= $total;		
		}
		
		//
		$totretfarpasluar = 0;		
		foreach($retfarpasluar AS $i=>$val){			
			$total 		 		= $val->totretfarluar;				
			$totretfarpasluar	+= $total;	
		}
		
		//
		$isiretdefosit= '';
		$totretdefosit = 0;
		
		foreach($retdepposit AS $i=>$val){
			
			$total 		 	= $val->totretdeposit;				
			$totretdefosit	+= $total;		
			
			$isiretdefosit .= "<tr>
					<td width=\"4%\" align=\"center\"><b>II.</b></td>
					<td width=\"80%\" align=\"left\"><b>Retur Deposit</b></td>					
					<td width=\"12%\" align=\"right\"><b>". number_format($totretdefosit,0,',','.') ."</b></td>
				</tr>";		
			
		} 
		
		//jumlah pengeluaran
		$totpengeluaran = $tretfarrawjln + $totretfarri + $totretfarpasluar;
		$jumpengeluaran = $totpengeluaran + $totretdefosit;
		
		//---------------------------------------------------------------------------------------
		
		$htmlpengeluaran = "<font size=\"7\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"2\">
				<tr align=\"left\">
					<td width=\"68%\"><font size=\"9\" face=\"Helvetica\">PENGELUARAN :</font></td>
				</tr>
			</table>
			<table border=\"1px\" cellpadding=\"2\">
				<tr>
					<td width=\"84%\" align=\"left\"><b>Bagian</b></td>
					<td width=\"12%\" align=\"center\"><b>Total</b></td>
				</tr>
				<tr align=\"center\">
					<td width=\"4%\"><b>I.</b></td>
					<td width=\"92%\" align=\"left\"><b>Farmasi</b></td>
				</tr>
				<tr >
					<td width=\"4%\" rowspan=\"4\"><b></b></td>
					<td width=\"3%\"><b>1.</b></td>
					<td width=\"77%\" align=\"left\">Retur Farmasi Rawat Jalan</td>
					<td width=\"12%\" align=\"right\">". number_format($tretfarrawjln,0,',','.') ."</td>
				</tr>
				<tr > 
					<td width=\"3%\"><b>2.</b></td>
					<td width=\"77%\" align=\"left\">Retur Farmasi Rawat Inap</td>
					<td width=\"12%\" align=\"right\">". number_format($totretfarri,0,',','.') ."</td>
				</tr>
				<tr > 
					<td width=\"3%\"><b>3.</b></td>
					<td width=\"77%\" align=\"left\">Retur Farmasi Pasien Luar</td>
					<td width=\"12%\" align=\"right\">". number_format($totretfarpasluar,0,',','.') ."</td>
				</tr>
				<tr align=\"center\">
					<td width=\"80%\" align=\"right\"><b>Jumlah</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totpengeluaran,0,',','.') ."</b></td>
				</tr>				
			  <tbody>". $isiretdefosit ."</tbody>
				<tr>
					<td width=\"84%\" align=\"right\"><b>Jumlah Pengeluaran</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($jumpengeluaran,0,',','.') ."</b></td>
				</tr>
			</table></font>
		";
		
		$this->pdf->writeHTML($htmlpengeluaran,true,false,false,false);
		
		//---------------------------------------------------------------------------------------
		
		//
		$isiallcarabayartunai= '';
		$totallcarabayartunai = 0;
		
		foreach($totalallcarabayar AS $i=>$val){
			
			$total 		 	= $val->totallcarabyrtunai;				
			$totallcarabayartunai	+= $total;		
			
			$isiallcarabayartunai.= "<tr>
					<td width=\"4%\" align=\"center\"><b>I.</b></td>				
					<td width=\"80%\" align=\"left\"><b>Jumlah Pemasukan</b></td>				
					<td width=\"12%\" align=\"right\"><b>". number_format($totallcarabayartunai,0,',','.') ."</b></td>
				</tr>";	
			
		}
		
		//				
		$isivcarabayar= '';
		$total = 0;
		
		foreach($vcarabayar AS $i=>$val){
			
			$total 		 	= $val->totcarabyr;	
						
			$isivcarabayar.= "<tr>
					<td width=\"4%\" align=\"center\"></td>				
					<td width=\"4%\" align=\"center\"><b>I.".($i+1).".</b></td>				
					<td width=\"76%\" align=\"left\">".$val->nmcarabayar."</td>				
					<td width=\"12%\" align=\"right\">". number_format($total,0,',','.') ."</td>
				</tr>";	
			
		} 
				
		//
		$totalpengeluaran = $totpemasukan - $jumpengeluaran;
		
		//
		$isisetoranksr= '';		
		foreach($setoranksr AS $i=>$val){
			
			$total 		 	= $val->jmlsetoranfisik;			
			$isisetoranksr.= "<tr>
					<td width=\"4%\" align=\"center\"><b>IV.</b></td>
					<td width=\"80%\" align=\"left\"><b>Total Uang Tunai Yang Disetorkan</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($total,0,',','.') ."</b></td>
				</tr>";	
			
		}
		
		//
		$isisetoranksr2= '';		
		foreach($setoranksr AS $i=>$val){ 	
			$isisetoranksr2.= "<tr>
					<td width=\"4%\" align=\"center\"><b>V.</b></td>
					<td width=\"80%\" align=\"left\"><b>Selisih</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($val->selisih,0,',','.') ."</b></td>
				</tr>";		
		} 
		
		$htmlsummary = "<font size=\"7\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"2\">
				<tr align=\"left\">
					<td width=\"68%\"><font size=\"9\" face=\"Helvetica\">SUMMARY :</font></td>
				</tr>
			</table>
			<table border=\"1px\" cellpadding=\"2\"> 
			  ".$isiallcarabayartunai." 
			  ".$isivcarabayar." 
				<tr>
					<td width=\"4%\" align=\"center\"><b>II.</b></td>
					<td width=\"80%\" align=\"left\"><b>Jumlah Pengeluaran</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($jumpengeluaran,0,',','.') ."</b></td>
				</tr>
				<tr>
					<td width=\"4%\" align=\"center\"><b>III.</b></td>
					<td width=\"80%\" align=\"left\"><b>Total (Jumlah Pemasukan - Jumlah Pengeluaran)</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($totalpengeluaran,0,',','.') ."</b></td>
				</tr>
				".$isisetoranksr."
				".$isisetoranksr2."
			</table>
			<table border=\"0px\" cellpadding=\"2\">
				<tr align=\"left\">
					<td width=\"75%\"><font size=\"9\" face=\"Helvetica\">Catatan : ".$setorankasir['catatan']."</font></td>
				</tr>
			</table></font>
		";
		
		$this->pdf->writeHTML($htmlsummary,true,false,false,false);
		
		$htmlttd = "<font size=\"8\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"2\" align=\"center\">
				<tr>
					<td width=\"10%\"></td>  
					<td width=\"27%\"></td>  
					<td width=\"24%\">Bandung, ".$tgl."</td> 
					<td width=\"24%\"></td>
				</tr>
				<tr>
					<td width=\"10%\"></td>  
					<td width=\"27%\">Kasir</td> 
					<td width=\"24%\"></td> 
					<td width=\"24%\">Keuangan</td> 
				</tr>
				<tr>  
					<td width=\"10%\"></td>  
					<td width=\"27%\"></td>  
					<td width=\"24%\"></td> 
					<td width=\"24%\"></td>
				</tr>
				<tr>
					<td width=\"10%\"></td>  
					<td width=\"27%\"></td> 
					<td width=\"24%\"></td> 
					<td width=\"24%\"></td>
				</tr>
				<tr>
					<td width=\"10%\"></td>  
					<td width=\"27%\">(".$setorankasir['nmlengkap'].")</td> 				
					<td width=\"24%\"></td>
					<td width=\"24%\">(".$setorankasir['nmset'].")</td> 
				</tr>
			</table></font>
		";
		
		$this->pdf->writeHTML($htmlttd,true,false,false,false);
		 
		//Close and output PDF document
		$this->pdf->Output('setorankasir.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function namaBulan($tgl){
		$arrtgl = explode('-', $tgl);
		$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
		$bulan = $query->row_array();
		$tanggalInd = $arrtgl[2] .' '. $bulan['nmbulan'] .' '. $arrtgl[0];
		
		return $tanggalInd ;
	}
	
}