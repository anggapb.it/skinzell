<?php
class Laprawatjalan extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
    
    function regperiode($tglawal, $tglakhir, $bagian, $iddokter) {
		$this->db->select("
			r.noreg AS noreg,
			rd.tglreg AS tglreg,
			TRIM(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			(
				SELECT sum(t.tarifbhp*t.qty)
				FROM
				  notadet t
				WHERE
				  ((t.nonota = n.nonota)
				  AND (t.kditem LIKE 'B%'))
			) AS uobat,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS upemeriksaan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000001'
				  AND t.nonota = n.nonota
			) AS upemeriksaan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS utindakan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000048'
				  AND t.nonota = n.nonota
			) AS utindakan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS uimun
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000009'
				  AND t.nonota = n.nonota
			) AS uimun,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulab
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000037'
				  AND t.nonota = n.nonota
			) AS ulab,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulain
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000073'
				  AND t.nonota = n.nonota
			) AS ulain,
			n.uangr AS uracik,
			sum(nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp) + n.diskon AS udiskon,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar <> 1
			) AS ucc,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar = 1
			) AS utotal,
			b.nmbagian AS nmbagian,
			b.idbagian AS idbagian,
			d.nmdoktergelar AS nmdoktergelar
		", false);
        $this->db->from("registrasi r", false);
		$this->db->join('pasien p',
					'p.norm = r.norm', 'left', false);
		$this->db->join('registrasidet rd',
					'rd.noreg = r.noreg', 'left', false);
		$this->db->join('bagian b',
					'b.idbagian = rd.idbagian', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = rd.iddokter', 'left', false);
		$this->db->join('nota n',
					'n.idregdet = rd.idregdet', 'left', false);
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->where('rd.userbatal IS NULL', null, false);
		$this->db->where('b.idjnspelayanan', 1);
		$this->db->where('n.idsttransaksi', 1);
		if($bagian != 'all')$this->db->where('rd.idbagian', $bagian);
		if($iddokter != 'all')$this->db->where('rd.iddokter', $iddokter);
		if($tglakhir){
			$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$this->db->groupby('r.noreg');
		$q = $this->db->get();
		$reg = $q->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN PENERIMAAN RAWAT JALAN', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$total = 0;
		
		$totupemeriksaan = 0;
		$totuobat = 0;
		$totutindakan = 0;
		$totuimun = 0;
		$totulab = 0;
		$totulain = 0;
		$toturacik = 0;
		$totudiskon = 0;
		$totucc = 0;
		$totutotal = 0;
		
		$bagshow = "";
		foreach($reg AS $i=>$val){
			$bagshow = "Poli: ".$val->nmbagian;
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"6%\">". $val->noreg ."</td>
					<td width=\"5%\">". date_format(date_create($val->tglreg), 'd-m-Y') ."</td>
					<td width=\"4%\">". $val->norm ."</td>
					<td width=\"7%\">". $val->nmpasien ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->uobat,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->upemeriksaan,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->utindakan,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->uimun,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->ulab,0,',','.') ."</td>
					<td width=\"5%\" align=\"right\">". number_format($val->ulain,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->uracik,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->udiskon,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->ucc,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->utotal,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". number_format(($val->ucc+$val->utotal),0,',','.') ."</td>
					<td width=\"8%\">". $val->nmbagian ."</td>
					<td width=\"12%\">". $val->nmdoktergelar ."</td>
			</tr>";
			
			$totuobat += $val->uobat;
			$totupemeriksaan += $val->upemeriksaan;
			$totutindakan += $val->utindakan;
			$totuimun += $val->uimun;
			$totulab += $val->ulab;
			$totulain += $val->ulain;
			$toturacik += $val->uracik;
			$totudiskon += $val->udiskon;
			$totucc += $val->ucc;
			$totutotal += $val->utotal;
			
		}
		
		if ($bagian != 'all') {
			$this->pdf->Cell(0, 0, $bagshow, 0, 1, 'C', 0, '', 0);
		}
		$html = "<br/><br/><font size=\"7.5\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <!-- <thead> -->
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"6%\"><b>No. Reg</b></td>
					<td width=\"5%\"><b>Tgl. Reg</b></td>
					<td width=\"4%\"><b>No. RM</b></td>
					<td width=\"7%\"><b>Nama Pasien</b></td>
					<td width=\"5%\"><b>Obat</b></td>
					<td width=\"6%\"><b>Pemeriksaan</b></td>
					<td width=\"6%\"><b>Tindakan</b></td>
					<td width=\"5%\"><b>Imunisasi</b></td>
					<td width=\"5%\"><b>LAB</b></td>
					<td width=\"5%\"><b>Lain-lain</b></td>
					<td width=\"4%\"><b>Racik</b></td>
					<td width=\"4%\"><b>Diskon</b></td>
					<td width=\"6%\"><b>Non Tunai</b></td>
					<td width=\"6%\"><b>Tunai</b></td>
					<td width=\"6%\"><b>Total</b></td>
					<td width=\"8%\"><b>Klinik</b></td>
					<td width=\"12%\"><b>Dokter</b></td>
				</tr>
			  <!-- </thead> -->
			  <tbody>". $isi ."</tbody>
				<tr align=\"right\">
					<td width=\"25%\">Total</td>
					<td width=\"5%\">". number_format($totuobat,0,',','.') ."</td>
					<td width=\"6%\">". number_format($totupemeriksaan,0,',','.') ."</td>
					<td width=\"6%\">". number_format($totutindakan,0,',','.') ."</td>
					<td width=\"5%\">". number_format($totuimun,0,',','.') ."</td>
					<td width=\"5%\">". number_format($totulab,0,',','.') ."</td>
					<td width=\"5%\">". number_format($totulain,0,',','.') ."</td>
					<td width=\"4%\">". number_format($toturacik,0,',','.') ."</td>
					<td width=\"4%\">". number_format($totudiskon,0,',','.') ."</td>
					<td width=\"6%\">". number_format($totucc,0,',','.') ."</td>
					<td width=\"6%\">". number_format($totutotal,0,',','.') ."</td>
					<td width=\"6%\">". number_format(($totucc+$totutotal),0,',','.') ."</td>
					<td width=\"20%\" colspan=\"2\">&nbsp;</td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('RJ_Periode.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	function exportexcel($tglawal, $tglakhir, $bagian, $iddokter) {
		/* $tablename = '
			(SELECT
				r.noreg AS noreg,
				rd.tglreg AS tglreg,
				TRIM(LEADING "0" FROM p.norm) AS norm,
				p.nmpasien AS nmpasien,
				(
					SELECT sum(t.tarifbhp*t.qty)
					FROM
					  notadet t
					WHERE
					  ((t.nonota = n.nonota)
					  AND (t.kditem LIKE "B%"))
				) AS uobat,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS upemeriksaan
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000001"
					  AND t.nonota = n.nonota
				) AS upemeriksaan,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS utindakan
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000048"
					  AND t.nonota = n.nonota
				) AS utindakan,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS uimun
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000009"
					  AND t.nonota = n.nonota
				) AS uimun,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulab
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000037"
					  AND t.nonota = n.nonota
				) AS ulab,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulain
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000073"
					  AND t.nonota = n.nonota
				) AS ulain,
				n.uangr AS uracik,
				sum(nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp + n.diskon) AS udiskon,
				(
					SELECT sum(kd.jumlah)
					FROM
					  kuitansidet kd
					WHERE
					  kd.nokuitansi = k.nokuitansi
					  AND kd.idcarabayar <> 1
				) AS ucc,
				(
					SELECT sum(kd.jumlah)
					FROM
					  kuitansidet kd
					WHERE
					  kd.nokuitansi = k.nokuitansi
					  AND kd.idcarabayar = 1
				) AS utotal,
				b.nmbagian AS nmbagian,
				b.idbagian AS idbagian,
				d.nmdoktergelar AS nmdoktergelar
			FROM registrasi r
			LEFT JOIN pasien p ON p.norm = r.norm
			LEFT JOIN registrasidet rd ON rd.noreg = r.noreg
			LEFT JOIN bagian b ON b.idbagian = rd.idbagian
			LEFT JOIN dokter d ON d.iddokter = rd.iddokter
			LEFT JOIN nota n ON n.idregdet = rd.idregdet
			LEFT JOIN notadet nd ON nd.nonota = n.nonota
			LEFT JOIN kuitansi k ON k.nokuitansi = n.nokuitansi
			WHERE
				rd.userbatal IS NULL AND
				b.idjnspelayanan = 1 AND
				rd.idbagian = "'. $bagian .'" AND
				rd.tglreg BETWEEN "' . $tglawal . '" AND "'. $tglakhir .'"
			GROUP BY r.noreg) AS xx
		'; */
		$header = array(
			'No. reg',
			'Tgl. Reg',
			'No. RM',
			'Nama Pasien',
			'Obat',
			'Pemeriksaan',
			'Tindakan',
			'Imunisasi',
			'LAB',
			'Lain-lain',
			'Racik',
			'Diskon',
			'Non Tunai',
			'Tunai',
			'Total',
			'Klinik',
			'Dokter'
		);

		$data['eksport'] = $this->getExport($tglawal, $tglakhir, $bagian, $iddokter);
		//$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		//$data['numrows'] = $this->getNumRows($tablename);
		$this->load->view('exportexcellaprj', $data); 	
	}
	
	function getNumRows($table) {
        $query = $this->db->get($table);
        return $query->num_rows();
    }
	
	function getExport($tglawal, $tglakhir, $bagian, $iddokter) {
		if($bagian == "all") $bgn = "";
		else $bgn = '" AND rd.idbagian = "'. $bagian;
		if($iddokter == "all") $dktr = "";
		else $dktr = '" AND rd.iddokter = "'. $iddokter;
		
		$q = '
			SELECT
				r.noreg AS noreg,
				rd.tglreg AS tglreg,
				TRIM(LEADING "0" FROM p.norm) AS norm,
				p.nmpasien AS nmpasien,
				(
					SELECT sum(t.tarifbhp*t.qty)
					FROM
					  notadet t
					WHERE
					  ((t.nonota = n.nonota)
					  AND (t.kditem LIKE "B%"))
				) AS uobat,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS upemeriksaan
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000001"
					  AND t.nonota = n.nonota
				) AS upemeriksaan,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS utindakan
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000048"
					  AND t.nonota = n.nonota
				) AS utindakan,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS uimun
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000009"
					  AND t.nonota = n.nonota
				) AS uimun,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulab
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000037"
					  AND t.nonota = n.nonota
				) AS ulab,
				(
					SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulain
					FROM
					  notadet t, pelayanan p
					WHERE
					  t.kditem = p.kdpelayanan
					  AND p.pel_kdpelayanan = "T000000073"
					  AND t.nonota = n.nonota
				) AS ulain,
				n.uangr AS uracik,
				sum(nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp) + n.diskon AS udiskon,
				(
					SELECT sum(kd.jumlah)
					FROM
					  kuitansidet kd
					WHERE
					  kd.nokuitansi = k.nokuitansi
					  AND kd.idcarabayar <> 1
				) AS ucc,
				(
					SELECT sum(kd.jumlah)
					FROM
					  kuitansidet kd
					WHERE
					  kd.nokuitansi = k.nokuitansi
					  AND kd.idcarabayar = 1
				) AS utotal,
				(
					SELECT sum(kd.jumlah)
					FROM
					  kuitansidet kd
					WHERE
					  kd.nokuitansi = k.nokuitansi
				) AS ujumlah,
				b.nmbagian AS nmbagian,
				d.nmdoktergelar AS nmdoktergelar
			FROM registrasi r
			LEFT JOIN pasien p ON p.norm = r.norm
			LEFT JOIN registrasidet rd ON rd.noreg = r.noreg
			LEFT JOIN bagian b ON b.idbagian = rd.idbagian
			LEFT JOIN dokter d ON d.iddokter = rd.iddokter
			LEFT JOIN nota n ON n.idregdet = rd.idregdet
			LEFT JOIN notadet nd ON nd.nonota = n.nonota
			LEFT JOIN kuitansi k ON k.nokuitansi = n.nokuitansi
			WHERE
				rd.userbatal IS NULL AND
				b.idjnspelayanan = 1 AND
				n.idsttransaksi= 1 AND
				k.tglkuitansi BETWEEN "' . $tglawal . '" AND "'. $tglakhir . $bgn . $dktr.'"
			GROUP BY r.noreg
		';
        $query = $this->db->query($q);
		
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
       } 
	   
    }

	function namaBulan($tgl){
		$arrtgl = explode('-', $tgl);
		$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
		$bulan = $query->row_array();
		$tanggalInd = $arrtgl[2] .' '. $bulan['nmbulan'] .' '. $arrtgl[0];
		
		return $tanggalInd ;
	}
	
}
