<?php 
	class Lap_rekamedis_penyakitriruangan extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function pasienkeluar($tglawal,$tglakhir,$ruangan){
		
		$this->db->select("*");
		$this->db->from("v_penyakitterbanyakri");
	//	$this->db->groupby("nopo");
		$this->db->where("tglmasuk BETWEEN '".$tglawal."' and '".$tglakhir."'");
		$this->db->where('idbagian',$ruangan);
		$query = $this->db->get();
		$aa = $query->row_array();
			//var_dump($tglawal);
			//var_dump($tglakhir);
		
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
	//	$this->pdf->AddPage();
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Penyakit Terbanyak Per Ruangan Rawat Inap', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->Cell(0, 0, 'Tanggal : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		$this->pdf->Cell(0, 0, 'Ruangan : '. $aa['nmbagian'], 0, 1, 'C', 0, '', 0);
		$isi ='';
		
		$this->pdf->SetMargins(PDF_MARGIN_LEFT+30, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			$isi .="<tr>
						<td align=\"center\">".$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
						<td align=\"center\">".$r['mati']."</td>
			</tr>";
		}
	}
		$heads = "<br><br><font size=\"7\" face=\"Helvetica\"> 
        
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR MATI</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>
						<th></th>	
					</tr>
					".$isi."
                    
		</table></font>";
		$this->pdf->writeHTML($heads,true,false,false,false); 
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>