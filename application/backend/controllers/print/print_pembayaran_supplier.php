<?php 
	class Print_pembayaran_supplier extends Controller{
		function __construct(){
		  parent::__construct();
			$this->load->library('pdf');
			$this->load->library('pdf_lap');        
		}
		
		function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
        }
		
		function get_bayarsupp($nopo){
		
			$query  = $this->db->query("SELECT * FROM v_bayar_supplier WHERE nopo='$nopo'");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_bayarhistorisupp($nopo){
		
			$query  = $this->db->query("select bayar.*,pengguna.nmlengkap,sum(bayardet.nominal) as jumlah from bayar
				  left join bayardet on bayar.nobayar = bayardet.nobayar 
				  left join pengguna on bayar.userid = pengguna.userid 
				  WHERE bayar.nopo='$nopo' group by bayar.nobayar");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
	
		function pdf_bayarsupp($nopo){
		
		
		$this->pdf->SetMargins('10', '40', '10');
		$this->pdf->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf->setPrintFooter(true); // enabled ? true
		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf->AddPage('P', 'F4', false, false); 
		
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'PEMBAYARAN SUPPLIER', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->SetFont('helvetica', '', 8);
		
		$bayarsupp = $this->get_bayarsupp($nopo);
		
		foreach ($bayarsupp as $item) {
			$nopo = $item->nopo;
			$tglpo = $this->TanggalIndo(date("Ymd",strtotime($item->tglpo)));
			$nmjnspp = $item->nmjnspp;
			$nopp = $item->nopp;
			$tglpp = $item->tglpp;
			
			$nmsupplier = $item->nmsupplier;
			$notelp = $item->notelp;
			$nofax = $item->nofax;
			$npwp = $item->npwp;
			$nmbagian = $item->nmbagian;
			
			$nmsypembayaran = $item->nmsypembayaran;
			$tgljatuhtempo = $this->TanggalIndo(date("Ymd",strtotime($item->tgljatuhtempo)));
			$nmjnspembayaran = $item->nmjnspembayaran;
			$nmstpo = $item->nmstpo;
			$bpb = $item->bpb;
			
			$bli = $item->bli;
			$byr = $item->byr;
			$selisih = $item->selisih;
			$stlunas = $item->stlunas;
		}
		
		$now = $this->TanggalIndo(date("Ymd"));
		$user = $this->session->userdata['username'];
		
		//========================RAWAT JALAN
		$bayarhistorisupp = $this->get_bayarhistorisupp($nopo);
		$no = 1;
		$tabeldet = "";
		$sumjumlah = 0;

		foreach ($bayarhistorisupp as $itemdet) {
		
			$tabeldet .= "<tr>
						  <th width=\"5%\" align=\"center\">".$no++."</th>
						  <th width=\"15%\" align=\"left\">".$itemdet->nobayar."</th>
						  <th width=\"15%\" align=\"left\">".$itemdet->tglbayar."</th>
						  <th width=\"12%\" align=\"left\">".number_format(ceil($itemdet->jumlah) , 0 , ',' , '.' )."</th>
						  <th width=\"22%\" align=\"left\">".$itemdet->catatan."</th>
						  <th width=\"12%\" align=\"left\">".$itemdet->nmlengkap."</th>
						  <th width=\"22%\" align=\"left\">".$itemdet->approval."</th>
					 </tr>";
					 
			$sumjumlah = $sumjumlah + ceil($itemdet->jumlah);
		}
		$tabeldet .= "<tr>
						  <th width=\"99%\" align=\"left\" colspan=\"7\">
						  <b>
						  Total Bayar : ".number_format($bli , 0 , ',' , '.' )." 
						  | Sudah Di Bayar : ".number_format($byr , 0 , ',' , '.' )." 
						  | Sisa Tagihan : ".number_format($selisih , 0 , ',' , '.' )."
						  | Status Lunas : ".$stlunas."
						  </b>
						  </th>
					 </tr>";
		
					 
		$tbl = <<<EOD
	<br />
    <br />
	
	<table border="0" cellpadding="2" nobr="true">
     <tr>
	  <th width="20%"><b>No. Pembelian</b></th>
	  <th width="32.5%">:$nopo</th>
      <th width="15%"><b>Supplier</b></th>
	  <th width="32.5%">:$nmsupplier</th> 
	 </tr>  
	 <tr> 
	  <th width="20%"><b>Tgl. Pembelian</b></th>
	  <th width="32.5%">:$tglpo</th>
      <th width="15%"><b>No. Telepon</b></th>
	  <th width="32.5%">:$notelp</th> 
	 </tr>
	  <tr> 
	  <th width="20%"><b>Jenis Pesanan</b></th>
	  <th width="32.5%">:$nmjnspp</th>
      <th width="15%"><b>Fax</b></th>
	  <th width="32.5%">:$nofax</th> 
	 </tr>
	 <tr> 
	  <th width="20%"><b>No. Pesanan</b></th>
	  <th width="32.5%">:$nopp</th>
      <th width="15%"><b>NPWP</b></th>
	  <th width="32.5%">:$npwp</th> 
	 </tr>
	 <tr> 
	  <th width="20%"><b>Tanggal Pesanan</b></th>
	  <th width="32.5%">:$tglpp</th>
      <th width="15%"><b>Bagian</b></th>
	  <th width="32.5%">:$nmbagian</th> 
	 </tr>
	 
	 <tr> 
	  <th width="100%" colspan="4"><hr></th>
	 </tr>
	 
	 <tr> 
	  <th width="20%"><b>Syarat Pembayaran</b></th>
	  <th width="32.5%">:$nmsypembayaran</th>
      <th width="15%"><b>Status Pesanan</b></th>
	  <th width="32.5%">:$nmstpo</th> 
	 </tr>
	 <tr> 
	  <th width="20%"><b>Tgl. Jatuh Tempo</b></th>
	  <th width="32.5%">:$tgljatuhtempo</th>
      <th width="15%"><b>No. Reff</b></th>
	  <th width="32.5%">:$bpb</th> 
	 </tr>
	 <tr> 
	  <th width="20%"><b>Jenis Pembayaran</b></th>
	  <th width="32.5%">:$nmjnspembayaran</th>
      <th width="15%"></th>
	  <th width="32.5%"></th> 
	 </tr>
	 
	 <tr> 
	  <th width="100%" colspan="4"><hr></th>
	 </tr>
    </table>
	
    <br />
    <br />
	<br />
	<b><u>Daftar Pembayaran</u> :</b> <br /><br />
    <table border="1" cellpadding="2" nobr="true">
     <tr>
	  <th width="5%" align="center"><b>No.</b></th>
	  <th width="15%" align="center"><b>No. Pembayaran</b></th>
      <th width="15%" align="center"><b>Tgl. Pembayaran</b></th>
	  <th width="12%" align="center"><b>Jumlah</b></th>
	  <th width="22%" align="center"><b>Catatan</b></th>
      <th width="15%" align="center"><b>User Input</b></th>
	  <th width="15%" align="center"><b>Approval</b></th>
     </tr> 
	$tabeldet
    </table>
	<br />
	<br />
	<br />
	<br />
	
	<table border="0" cellpadding="2" nobr="true">
     <tr align="center">
	  <th width="30%"></th>
	  <th width="39%"></th>
      <th width="30%">Bandung, $now</th>
	 </tr>  
	 <tr align="center">
	  <th width="30%"></th>
	  <th width="39%"></th>
      <th width="30%">Petugas</th>
	 </tr>  
	 <tr align="center">
	  <th width="30%"></th>
	  <th width="39%"></th>
      <th width="30%"></th>
	 </tr>
	<tr align="center">
	  <th width="30%"></th>
	  <th width="39%"></th>
      <th width="30%"></th>
	 </tr>
	 <tr align="center">
	  <th width="30%"></th>
	  <th width="39%"></th>
      <th width="30%"></th>
	 </tr>
	 <tr align="center">
	  <th width="30%"></th>
	  <th width="39%"></th>
      <th width="30%"></th>
	 </tr>
	 <tr align="center">
	  <th width="30%"></th>
	  <th width="39%"></th>
      <th width="30%">( $user )</th>
	 </tr>
    </table>

    
EOD;
     	$this->pdf->writeHTML($tbl,true,false,false,false);
		
		
		$this->pdf->Output('po.pdf', 'I');
		}
		
	//================================================	
		
	function export_pembayaran_supplier($cbxstbayar,$cbxsearch,$cbxbeli,$cbxjthtempo,$cbxpembayaran,
		$status,$key,$value,$tglbeli1,$tglbeli2,$jthtempo1,$jthtempo2,$pembayaran1,$pembayaran2){
			
			$arrsearch = array();
			$this->db->select("*");
      $this->db->order_by("v_bayar_supplier.nopo DESC");
			$this->db->from("v_bayar_supplier");
			if ($cbxstbayar == 'true'){
				if ($status != 'Semua'){
					$this->db->where('stlunas =', $status);
					$arrsearch['cbxstbayar'] = 'STATUS BAYAR: '.$status;
				}
			}
			
			if ($cbxsearch == 'true'){
				if ($value != '-') {
					$this->db->or_like($key, $value);
					$arrsearch['cbxsearch'] = 'PENCARIAN: '.$key.' = '.$value;
				}
			}
			
			if ($cbxbeli == 'true'){
				$this->db->where("date(tglpo) between '". $tglbeli1 ."' and '". $tglbeli2 ."'" );
				$arrsearch['cbxbeli'] = 'TANGGAL BELI: '.$tglbeli1.' s/d '.$tglbeli2;
			}
			
			if ($cbxpembayaran == 'true'){
				$this->db->where("date(tglbayar) between '". $pembayaran1 ."' and '". $pembayaran2 ."'" );
				$arrsearch['cbxjthtempo'] = 'TANGGAL Pembayaran: '.$pembayaran1.' s/d '.$pembayaran2;
			}

			if ($cbxjthtempo == 'true'){
				$this->db->where("date(tgljatuhtempo) between '". $jthtempo1 ."' and '". $jthtempo2 ."'" );
				$arrsearch['cbxjthtempo'] = 'TANGGAL JATUH TEMPO: '.$jthtempo1.' s/d '.$jthtempo2;
			}
			
			$query = $this->db->get();
			
			$data['filter'] = $arrsearch;//"STATUS BAYAR: PENCARIAN: =, TANGGAL BELI: TANGGAL JATUH TEMPO:";
			$data['data'] = $query->result();
			$data['filename'] = 'pembayaran_supplier';
			$data['fieldname'] = $this->db->list_fields("v_bayar_supplier");
			$this->load->view('exportexcelfilterarray', $data); 	
	  }

	  function rekap_pembelian($cbxstbayar,$cbxsearch,$cbxbeli,$cbxjthtempo,$cbxpembayaran,$status,$key,$value,$tglbeli1,$tglbeli2,$jthtempo1,$jthtempo2,$pembayaran1,$pembayaran2)
	  {
			$isi = '';
			$this->db->select('*');
      $this->db->order_by("v_bayar_supplier.nopo DESC");
      $this->db->from('v_bayar_supplier');
			
			if ($cbxstbayar == 'true'){
				if ($status != 'Semua'){
					$this->db->where('stlunas =', $status);
					//$this->pdf_lap->Cell(0, 0, 'Status Bayar : '. $status, 0, 1, 'C', 0, '', 0);
				}
			}
			
			if ($cbxsearch == 'true'){
				if ($value != '-') {
					$this->db->or_like($key, $value);
					//$this->pdf_lap->Cell(0, 0, 'Pencarian : '. $key .' = '. $value, 0, 1, 'C', 0, '', 0);
				}
			}
			
			if ($cbxbeli == 'true'){
				$this->db->where("date(tglpo) between '". $tglbeli1 ."' and '". $tglbeli2 ."'" );
				//$this->pdf_lap->Cell(0, 0, 'Tanggal Beli : '. date("d F Y", strtotime($tglbeli1)) .' - '.date("d F Y", strtotime($tglbeli2)), 0, 1, 'C', 0, '', 0);
			}
			
			if ($cbxpembayaran == 'true'){
				$this->db->where("date(tglbayar) between '". $pembayaran1 ."' and '". $pembayaran2 ."'" );
				//$this->pdf_lap->Cell(0, 0, 'Tanggal Pembayaran : '. date("d F Y", strtotime($pembayaran1)) .' - '.date("d F Y", strtotime($pembayaran2)), 0, 1, 'C', 0, '', 0);
			}

			if ($cbxjthtempo == 'true'){
				$this->db->where("date(tgljatuhtempo) between '". $jthtempo1 ."' and '". $jthtempo2 ."'" );
				//$this->pdf_lap->Cell(0, 0, 'Tanggal Jatuh Tempo : '. date("d F Y", strtotime($jthtempo1)) .' - '.date("d F Y", strtotime($jthtempo2)), 0, 1, 'C', 0, '', 0);
			}
			
			
      $q = $this->db->get();
      $data = array();
      if ($q->num_rows() > 0) {
          $data = $q->result();
      }
			
			// add a page
			$page_format = array(
				'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
				'Dur' => 3,
				'trans' => array(
					'D' => 1.5,
					'S' => 'Split',
					'Dm' => 'V',
					'M' => 'O'
				),
				'Rotate' => 0,
				'PZ' => 1,
			);
			
			//Set Footer
			$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
			$this->pdf_lap->setPrintFooter(true); // enabled ? true
			$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
			
			$this->pdf_lap->SetPrintHeader(false);
			$this->pdf_lap->AddPage('P', $page_format, false, false);
			$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
			$this->pdf_lap->SetFont('helvetica', '', 14);
			
			$x=0;$y=10;
			$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
			$this->pdf_lap->Cell(0, 0, 'Rekap Pembelian', 0, 1, 'C', 0, '', 0);
			
			$this->pdf_lap->SetFont('helvetica', '', 10);
			

			if ($cbxstbayar == 'true'){
				if ($status != 'Semua'){
					$this->pdf_lap->Cell(0, 0, 'Status Bayar : '. $status, 0, 1, 'C', 0, '', 0);
				}
			}
			
			if ($cbxsearch == 'true'){
				if ($value != '-') {
					$this->pdf_lap->Cell(0, 0, 'Pencarian : '. $key .' = '. $value, 0, 1, 'C', 0, '', 0);
				}
			}
			
			if ($cbxbeli == 'true'){
				$this->pdf_lap->Cell(0, 0, 'Tanggal Beli : '. date("d F Y", strtotime($tglbeli1)) .' - '.date("d F Y", strtotime($tglbeli2)), 0, 1, 'C', 0, '', 0);
			}
			
			if ($cbxpembayaran == 'true'){
				$this->pdf_lap->Cell(0, 0, 'Tanggal Pembayaran : '. date("d F Y", strtotime($pembayaran1)) .' - '.date("d F Y", strtotime($pembayaran2)), 0, 1, 'C', 0, '', 0);
			}

			if ($cbxjthtempo == 'true'){
				$this->pdf_lap->Cell(0, 0, 'Tanggal Jatuh Tempo : '. date("d F Y", strtotime($jthtempo1)) .' - '.date("d F Y", strtotime($jthtempo2)), 0, 1, 'C', 0, '', 0);
			}
			
			$no = 1;
			$total_transfer = 0;
			$total_tunai = 0;
			$totaleverything = 0;
			
			foreach($data as $i=>$val){
				$isi .= "
				    <tr>
							<td align=\"center\">". date("d F Y", strtotime($val->tglpo)) ."</td>
							<td align=\"center\">". date("d F Y", strtotime($val->tgljatuhtempo)) ."</td>
							<td align=\"center\">". $val->nopo ."</td>
							<td align=\"center\">". $val->nmsupplier ."</td>
							<td align=\"right\"> Rp. ". number_format($val->bliret,2,',','.') ."</td>
							<td align=\"center\">". $val->nmjnspembayaran ."</td>
						</tr>";	
					
					if($val->nmjnspembayaran =='Transfer'){
						$total_transfer += $val->bliret;
					}else{
						$total_tunai += $val->bliret;
					}
			}
			$totaleverything = $total_transfer + $total_tunai;
			
			$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
				<table border=\"1px\" cellpadding=\"2\">
					<tr align=\"center\">
						<td width=\"30%\" colspan=\"2\"><strong>Tanggal</strong></td>
						<td width=\"10%\"rowspan=\"2\"><strong>Invoice</strong></td>
						<td width=\"30%\"rowspan=\"2\"><strong>Nama perusahaan</strong></td>
						<td width=\"15%\"rowspan=\"2\"><strong>Jumlah</strong></td>
						<td width=\"15%\"rowspan=\"2\"><strong>Jenis Pembayaran</strong></td>
					</tr>
					<tr align=\"center\">
						<td><strong>Masuk</strong></td>
						<td><strong>Tempo</strong></td>
					</tr>
				  <tbody>". $isi ."</tbody>
					<tr align=\"center\">
						<td colspan=\"4\" align=\"right\"><strong>Total</strong></td>
						<td align=\"right\"><strong> Rp. ". $totaleverything ."</strong></td>
						<td> </td>
					</tr>
					<tr align=\"center\">
						<td colspan=\"4\" align=\"right\"><strong>Transfer</strong></td>
						<td align=\"right\"><strong> Rp. ". $total_transfer ."</strong></td>
						<td> </td>
					</tr>
					<tr align=\"center\">
						<td colspan=\"4\" align=\"right\"><strong>Tunai</strong></td>
						<td align=\"right\"><strong> Rp. ". $total_tunai ."</strong></td>
						<td> </td>
					</tr>
				</table></font>
			";

			$this->pdf_lap->writeHTML($html,true,false,false,false);


			//Close and output PDF document
			$this->pdf_lap->Output('rekap_pembelian.pdf', 'I');

	  }

	  function detail_rekap_pembelian($cbxstbayar,$cbxsearch,$cbxbeli,$cbxjthtempo,$cbxpembayaran,$status,$key,$value,$tglbeli1,$tglbeli2,$jthtempo1,$jthtempo2,$pembayaran1,$pembayaran2)
	  {
			$isi = '';
			$this->db->select('*');
      $this->db->order_by("v_bayar_supplier.nopo DESC");
      $this->db->from('v_bayar_supplier');
			
			if ($cbxstbayar == 'true'){
				if ($status != 'Semua'){
					$this->db->where('stlunas =', $status);
					//$this->pdf_lap->Cell(0, 0, 'Status Bayar : '. $status, 0, 1, 'C', 0, '', 0);
				}
			}
			
			if ($cbxsearch == 'true'){
				if ($value != '-') {
					$this->db->or_like($key, $value);
					//$this->pdf_lap->Cell(0, 0, 'Pencarian : '. $key .' = '. $value, 0, 1, 'C', 0, '', 0);
				}
			}
			
			if ($cbxbeli == 'true'){
				$this->db->where("date(tglpo) between '". $tglbeli1 ."' and '". $tglbeli2 ."'" );
				//$this->pdf_lap->Cell(0, 0, 'Tanggal Beli : '. date("d F Y", strtotime($tglbeli1)) .' - '.date("d F Y", strtotime($tglbeli2)), 0, 1, 'C', 0, '', 0);
			}
			
			if ($cbxpembayaran == 'true'){
				$this->db->where("date(tglbayar) between '". $pembayaran1 ."' and '". $pembayaran2 ."'" );
				//$this->pdf_lap->Cell(0, 0, 'Tanggal Pembayaran : '. date("d F Y", strtotime($pembayaran1)) .' - '.date("d F Y", strtotime($pembayaran2)), 0, 1, 'C', 0, '', 0);
			}

			if ($cbxjthtempo == 'true'){
				$this->db->where("date(tgljatuhtempo) between '". $jthtempo1 ."' and '". $jthtempo2 ."'" );
				//$this->pdf_lap->Cell(0, 0, 'Tanggal Jatuh Tempo : '. date("d F Y", strtotime($jthtempo1)) .' - '.date("d F Y", strtotime($jthtempo2)), 0, 1, 'C', 0, '', 0);
			}
			
			
      $q = $this->db->get();
      $data = array();
      if ($q->num_rows() > 0) {
          $data = $q->result();
      }
			
			// add a page
			$page_format = array(
				'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
				'Dur' => 3,
				'trans' => array(
					'D' => 1.5,
					'S' => 'Split',
					'Dm' => 'V',
					'M' => 'O'
				),
				'Rotate' => 0,
				'PZ' => 1,
			);
			
			//Set Footer
			$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
			$this->pdf_lap->setPrintFooter(true); // enabled ? true
			$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
			
			$this->pdf_lap->SetPrintHeader(false);
			$this->pdf_lap->AddPage('L', $page_format, false, false);
			$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
			$this->pdf_lap->SetFont('helvetica', '', 14);
			
			$x=0;$y=10;
			$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
			$this->pdf_lap->Cell(0, 0, 'Rekap Pembelian', 0, 1, 'C', 0, '', 0);
			
			$this->pdf_lap->SetFont('helvetica', '', 10);
			

			if ($cbxstbayar == 'true'){
				if ($status != 'Semua'){
					$this->pdf_lap->Cell(0, 0, 'Status Bayar : '. $status, 0, 1, 'C', 0, '', 0);
				}
			}
			
			if ($cbxsearch == 'true'){
				if ($value != '-') {
					$this->pdf_lap->Cell(0, 0, 'Pencarian : '. $key .' = '. $value, 0, 1, 'C', 0, '', 0);
				}
			}
			
			if ($cbxbeli == 'true'){
				$this->pdf_lap->Cell(0, 0, 'Tanggal Beli : '. date("d F Y", strtotime($tglbeli1)) .' - '.date("d F Y", strtotime($tglbeli2)), 0, 1, 'C', 0, '', 0);
			}
			
			if ($cbxpembayaran == 'true'){
				$this->pdf_lap->Cell(0, 0, 'Tanggal Pembayaran : '. date("d F Y", strtotime($pembayaran1)) .' - '.date("d F Y", strtotime($pembayaran2)), 0, 1, 'C', 0, '', 0);
			}

			if ($cbxjthtempo == 'true'){
				$this->pdf_lap->Cell(0, 0, 'Tanggal Jatuh Tempo : '. date("d F Y", strtotime($jthtempo1)) .' - '.date("d F Y", strtotime($jthtempo2)), 0, 1, 'C', 0, '', 0);
			}
			
			$no = 1;
			$total_transfer = 0;
			$total_tunai = 0;
			$totaleverything = 0;
			
			foreach($data as $i=>$val){
				//get detail pembelian
				$get_detail = $this->db->get_where('v_podet_supplier', array('nopo' => $val->nopo))->result();
				$kode_barang = array();
				$nama_barang = array();
				$qty_barang = array();
				$qty_retur = array();
				$nama_satuan = array();
				$harga_barang = array();
				$diskon = array();
				$ppn = array();
				$subtotal_barang = array();
				if($get_detail){
					foreach($get_detail as $idx => $dtl){
						$kode_barang[] = $dtl->kdbrg;
						$nama_barang[] = $dtl->nmbrg;
						$qty_barang[] = $dtl->qty;
						$qty_retur[] = $dtl->qtyretur;
						$nama_satuan[] = ' '.$dtl->nmsatuanbsr;
						$harga_barang[] = 'Rp. '.number_format($dtl->hrgbeli,0,',','.').' ';
						$diskon[] = $dtl->diskon.' % ';
						$ppn[] = $dtl->ppn.' % ';

						//perhitungan subtotal
						/*
							rumus : 
							((((qtybeli - qtyretur) * hrgbeli)) - diskon) * (perhitungan ppn)
						*/
						$hitung_ppn = (100 + $dtl->ppn) / 100;
						$hitung_subtotal = ((($dtl->qty - $dtl->qtyretur) * $dtl->hrgbeli) - $dtl->diskonrp) * $hitung_ppn;
						$subtotal_barang[] = 'Rp. '. number_format($hitung_subtotal,2,',','.') .' ';
					}
				}

				$isi .= "
				    <tr>
							<td align=\"center\">". date("d F Y", strtotime($val->tglpo)) ."</td>
							<td align=\"center\">". date("d F Y", strtotime($val->tgljatuhtempo)) ."</td>
							<td align=\"center\">". $val->nopo ."</td>
							<td align=\"left\">". $val->nmsupplier ."</td>
							<td align=\"left\"> ". $val->nmjnspembayaran ."</td>
							<td align=\"center\">". implode("<br>", $kode_barang) ."</td>
							<td align=\"left\">". implode("<br>", $nama_barang) ."</td>
							<td align=\"right\">". implode("<br>", $qty_barang) ."</td>
							<td align=\"right\">". implode("<br>", $qty_retur) ."</td>
							<td align=\"center\">". implode("<br>", $nama_satuan) ."</td>
							<td align=\"right\">". implode("<br>", $harga_barang) ."</td>
							<td align=\"right\">". implode("<br>", $diskon) ."</td>
							<td align=\"right\">". implode("<br>", $ppn) ."</td>
							<td align=\"right\">". implode("<br>", $subtotal_barang) ."</td>
						</tr>
						<tr>
							<td align=\"right\" colspan=\"13\"><strong>Total </strong></td>
							<td align=\"right\"><strong>Rp. ". number_format($val->bliret,2,',','.') ." </strong></td>
						</tr>
						";	
					
				if($val->nmjnspembayaran =='Transfer'){
					$total_transfer += $val->bliret;
				}else{
					$total_tunai += $val->bliret;
				}
			}
			$totaleverything = $total_transfer + $total_tunai;
			
			$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
				<table border=\"1px\" cellpadding=\"2\">
					<tr align=\"center\">
						<td width=\"13%\" colspan=\"2\"><strong>Tanggal</strong></td>
						<td width=\"7%\"rowspan=\"2\"><strong>Invoice</strong></td>
						<td width=\"10%\"rowspan=\"2\"><strong>Nama perusahaan</strong></td>
						<td width=\"8%\"rowspan=\"2\"><strong>Jenis Pembayaran</strong></td>
						<td width=\"8%\"rowspan=\"2\"><strong>Kode Barang</strong></td>
						<td width=\"11%\"rowspan=\"2\"><strong>Nama Barang</strong></td>
						<td width=\"5%\"rowspan=\"2\"><strong>Qty<br>Beli</strong></td>
						<td width=\"5%\"rowspan=\"2\"><strong>Qty<br>Retur</strong></td>
						<td width=\"5%\"rowspan=\"2\"><strong>Satuan</strong></td>
						<td width=\"8%\"rowspan=\"2\"><strong>Harga</strong></td>
						<td width=\"6%\"rowspan=\"2\"><strong>Diskon</strong></td>
						<td width=\"6%\"rowspan=\"2\"><strong>PPN</strong></td>
						<td width=\"8%\"rowspan=\"2\"><strong>Subtotal</strong></td>
					</tr>
					<tr align=\"center\">
						<td><strong>Masuk</strong></td>
						<td><strong>Tempo</strong></td>
					</tr>
				  <tbody>". $isi ."</tbody>
					<tr align=\"center\">
						<td colspan=\"13\" align=\"right\"><strong>Grand Total</strong></td>
						<td align=\"right\"><strong> Rp. ". number_format($totaleverything,2,',','.') ."</strong></td>
					</tr>
					<tr align=\"center\">
						<td colspan=\"13\" align=\"right\"><strong>Transfer</strong></td>
						<td align=\"right\"><strong> Rp. ". number_format($total_transfer,2,',','.') ."</strong></td>
					</tr>
					<tr align=\"center\">
						<td colspan=\"13\" align=\"right\"><strong>Tunai</strong></td>
						<td align=\"right\"><strong> Rp. ". number_format($total_tunai,2,',','.') ."</strong></td>
					</tr>
				</table></font>
			";

			$this->pdf_lap->writeHTML($html,true,false,false,false);


			//Close and output PDF document
			$this->pdf_lap->Output('rekap_detail_pembelian.pdf', 'I');

	  }

    function rekap_pembelian_excel($cbxstbayar,$cbxsearch,$cbxbeli,$cbxjthtempo,$cbxpembayaran,$status,$key,$value,$tglbeli1,$tglbeli2,$jthtempo1,$jthtempo2,$pembayaran1,$pembayaran2)
	  {
	    $this->load->library('lib_phpexcel');
			$isi = '';
			$this->db->select('*');
      $this->db->order_by("v_bayar_supplier.nopo DESC");
      $this->db->from('v_bayar_supplier');
			
      //list of style
      $headergrid = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        ),
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'font'  => array(
        'bold'  => true,
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'startcolor' => array(
            'rgb' => 'CCCCCA',
          ),
        ),
      );

      $contentgrid = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )
      );      
      
      $subject_file = '';
			if ($cbxstbayar == 'true'){
				if ($status != 'Semua'){
					$this->db->where('stlunas =', $status);
          $subject_file .="Status Bayar : ". $status; 
				}
			}
			
			if ($cbxsearch == 'true'){
				if ($value != '-') {
					$this->db->or_like($key, $value);
          if(!empty($subject_file)) $subject_file .= " & ";
          $subject_file .= "Pencarian {$key} : ". $value; 
				}
			}
			
			if ($cbxbeli == 'true'){
				$this->db->where("date(tglpo) between '". $tglbeli1 ."' and '". $tglbeli2 ."'" );
        if(!empty($subject_file)) $subject_file .= " & ";
        $subject_file .= "Tanggal Beli : ". date("d F Y", strtotime($tglbeli1)) .' - '.date("d F Y", strtotime($tglbeli2)); 
			}
			
			if ($cbxpembayaran == 'true'){
				$this->db->where("date(tglbayar) between '". $pembayaran1 ."' and '". $pembayaran2 ."'" );
        if(!empty($subject_file)) $subject_file .= " & ";
        $subject_file .= "Tanggal Pembayaran : ". date("d F Y", strtotime($pembayaran1)) .' - '.date("d F Y", strtotime($pembayaran2)); 
			}

			if ($cbxjthtempo == 'true'){
				$this->db->where("date(tgljatuhtempo) between '". $jthtempo1 ."' and '". $jthtempo2 ."'" );
        if(!empty($subject_file)) $subject_file .= " & ";
        $subject_file .= "Tanggal Jatuh Tempo : ". date("d F Y", strtotime($jthtempo1)) .' - '.date("d F Y", strtotime($jthtempo2)); 
        
			}
			
      // Set document properties
      $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
           ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
           ->setTitle("Rekap Pembelian")
           ->setSubject($subject_file);

      $this->lib_phpexcel->getActiveSheet()->setTitle('Rekap Pembelian'); // set worksheet name

      $q = $this->db->get();
      $data = array();
      if ($q->num_rows() > 0) {
          $data = $q->result();
      }
			
      //set title
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:G1'); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
      $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue('B1', 'Rekap Pembelian');
      
      //set subtitle
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:G2'); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue('B2', $subject_file);

      //set width of column
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
      
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B4:C4'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D4:D5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('E4:E5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('F4:F5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('G4:G5'); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B4:G4")->applyFromArray($headergrid);
      $this->lib_phpexcel->getActiveSheet()->getStyle("B5:G5")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B4", 'Tanggal')
           ->setCellValue("D4", 'Invoice')
           ->setCellValue("E4", 'Nama Perusahaan')
           ->setCellValue("F4", 'Jumlah')
           ->setCellValue("G4", 'Jenis Pembayaran')
           ->setCellValue("B5", 'Masuk')
           ->setCellValue("C5", 'Tempo');
      
			$total_transfer = 0;
			$total_tunai = 0;
			$totaleverything = 0;
			$active_row = 6;
      if(!empty($data)){
      
        foreach($data as $i=>$val){
          $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:G{$active_row}")->applyFromArray($contentgrid);
          $this->lib_phpexcel->setActiveSheetIndex(0)
               ->setCellValue("B{$active_row}", date("d F Y", strtotime($val->tglpo)))
               ->setCellValue("C{$active_row}", date("d F Y", strtotime($val->tgljatuhtempo)))
               ->setCellValue("D{$active_row}", $val->nopo)
               ->setCellValue("E{$active_row}", $val->nmsupplier)
               ->setCellValue("F{$active_row}", $val->bliret)
               ->setCellValue("G{$active_row}", $val->nmjnspembayaran);
          $active_row += 1; // set active row to the next row
          if($val->nmjnspembayaran =='Transfer'){
            $total_transfer += $val->bliret;
          }else{
            $total_tunai += $val->bliret;
          }
          
        }
      }
      //all total
			$totaleverything = $total_transfer + $total_tunai;
			$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:G{$active_row}")->applyFromArray($contentgrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("E{$active_row}", "Total")
           ->setCellValue("F{$active_row}", $totaleverything);
      $active_row += 1;
      
      //total transfer
			$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:G{$active_row}")->applyFromArray($contentgrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("E{$active_row}", "Transfer")
           ->setCellValue("F{$active_row}", $total_transfer);
      $active_row += 1;

      //total Tunai
			$this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:G{$active_row}")->applyFromArray($contentgrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("E{$active_row}", "Tunai")
           ->setCellValue("F{$active_row}", $total_tunai);

      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $this->lib_phpexcel->setActiveSheetIndex(0);

      $filename = "rekap_pembelian_".strtolower(str_replace(array(' ', '&'), array('_', '-'), $subject_file));

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');

      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
      $objWriter->save('php://output');
      exit;

	  }

	  function detail_rekap_pembelian_excel($cbxstbayar,$cbxsearch,$cbxbeli,$cbxjthtempo,$cbxpembayaran,$status,$key,$value,$tglbeli1,$tglbeli2,$jthtempo1,$jthtempo2,$pembayaran1,$pembayaran2)
	  {
      $this->load->library('lib_phpexcel');

      $isi = '';
			$this->db->select('*');
      $this->db->order_by("v_bayar_supplier.nopo DESC");
      $this->db->from('v_bayar_supplier');
			
      //list of style
      $headergrid = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        ),
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
        'font'  => array(
        'bold'  => true,
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'startcolor' => array(
            'rgb' => 'CCCCCA',
          ),
        ),
      );

      $contentgrid = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'startcolor' => array(
            'rgb' => 'F0F0F0',
          ),
        ),
      );
      
      $main_item = array(
        'borders' => array(
          'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
          'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
          'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
          'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_NONE,
          ), 
        )
      );
      
      $sub_item = array(
        'borders' => array(
          'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_NONE,
          ),
          'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
          'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
          ),
          'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_NONE,
          ), 
        )
      );
      
      $subject_file = '';
			if ($cbxstbayar == 'true'){
				if ($status != 'Semua'){
					$this->db->where('stlunas =', $status);
          $subject_file .="Status Bayar : ". $status; 
				}
			}
			
			if ($cbxsearch == 'true'){
				if ($value != '-') {
					$this->db->or_like($key, $value);
          if(!empty($subject_file)) $subject_file .= " & ";
          $subject_file .= "Pencarian {$key} : ". $value; 
				}
			}
			
			if ($cbxbeli == 'true'){
				$this->db->where("date(tglpo) between '". $tglbeli1 ."' and '". $tglbeli2 ."'" );
        if(!empty($subject_file)) $subject_file .= " & ";
        $subject_file .= "Tanggal Beli : ". date("d F Y", strtotime($tglbeli1)) .' - '.date("d F Y", strtotime($tglbeli2)); 
			}
			
			if ($cbxpembayaran == 'true'){
				$this->db->where("date(tglbayar) between '". $pembayaran1 ."' and '". $pembayaran2 ."'" );
        if(!empty($subject_file)) $subject_file .= " & ";
        $subject_file .= "Tanggal Pembayaran : ". date("d F Y", strtotime($pembayaran1)) .' - '.date("d F Y", strtotime($pembayaran2)); 
			}

			if ($cbxjthtempo == 'true'){
				$this->db->where("date(tgljatuhtempo) between '". $jthtempo1 ."' and '". $jthtempo2 ."'" );
        if(!empty($subject_file)) $subject_file .= " & ";
        $subject_file .= "Tanggal Jatuh Tempo : ". date("d F Y", strtotime($jthtempo1)) .' - '.date("d F Y", strtotime($jthtempo2)); 
        
			}
			
      // Set document properties
      $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
           ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
           ->setTitle("Detail Rekap Pembelian")
           ->setSubject($subject_file);

      $this->lib_phpexcel->getActiveSheet()->setTitle('Detail Rekap Pembelian'); // set worksheet name
      
      $q = $this->db->get();
      $data = array();
      if ($q->num_rows() > 0) {
          $data = $q->result();
      }

      //set title
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:O1'); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
      $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue('B1', 'Detail Rekap Pembelian');
      
      //set subtitle
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:O2'); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue('B2', $subject_file);

      //set width of column
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('L')->setWidth(14);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('N')->setWidth(14);
      $this->lib_phpexcel->getActiveSheet()->getColumnDimension('O')->setWidth(14);
      
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B4:C4'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('D4:D5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('E4:E5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('F4:F5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('G4:G5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('H4:H5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('I4:I5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('J4:J5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('K4:K5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('L4:L5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('M4:M5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('N4:N5'); //merge cell
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('O4:O5'); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B4:O4")->applyFromArray($headergrid);
      $this->lib_phpexcel->getActiveSheet()->getStyle("B5:O5")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B4", 'Tanggal')
           ->setCellValue("D4", 'Invoice')
           ->setCellValue("E4", 'Nama Perusahaan')
           ->setCellValue("F4", 'Jenis Pembayaran')
           ->setCellValue("G4", 'Kode Barang')
           ->setCellValue("H4", 'Nama Barang')
           ->setCellValue("I4", 'Qty Barang')
           ->setCellValue("J4", 'Qty Retur')
           ->setCellValue("K4", 'Satuan')
           ->setCellValue("L4", 'Harga')
           ->setCellValue("M4", 'Diskon')
           ->setCellValue("N4", 'PPN')
           ->setCellValue("O4", 'Subtotal')
           ->setCellValue("B5", 'Masuk')
           ->setCellValue("C5", 'Tempo');			
			
			$total_transfer = 0;
			$total_tunai = 0;
			$totaleverything = 0;
			$active_row = 6;
			foreach($data as $i=>$val){
				//get detail pembelian
				$get_detail = $this->db->get_where('v_podet_supplier', array('nopo' => $val->nopo))->result();
        
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:O{$active_row}")->applyFromArray($main_item);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", date("d F Y", strtotime($val->tglpo)))
             ->setCellValue("C{$active_row}", date("d F Y", strtotime($val->tgljatuhtempo)))
             ->setCellValue("D{$active_row}", $val->nopo)
             ->setCellValue("E{$active_row}", $val->nmsupplier)
             ->setCellValue("F{$active_row}", $val->nmjnspembayaran);
        
        //loop detail data
        if($get_detail){
					foreach($get_detail as $idx => $dtl){
            if($idx > 0)
              $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:O{$active_row}")->applyFromArray($sub_item);
            
						//perhitungan subtotal
						/*
							rumus : 
							((((qtybeli - qtyretur) * hrgbeli)) - diskon) * (perhitungan ppn)
						*/
						$hitung_ppn = (100 + $dtl->ppn) / 100;
						$hitung_subtotal = ((($dtl->qty - $dtl->qtyretur) * $dtl->hrgbeli) - $dtl->diskonrp) * $hitung_ppn;
						
            $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("G{$active_row}", $dtl->kdbrg)
             ->setCellValue("H{$active_row}", $dtl->nmbrg)
             ->setCellValue("I{$active_row}", number_format($dtl->qty,0,',','.'))
             ->setCellValue("J{$active_row}", number_format($dtl->qtyretur,0,',','.'))
             ->setCellValue("K{$active_row}", $dtl->nmsatuanbsr)
             ->setCellValue("L{$active_row}", $dtl->hrgbeli)
             ->setCellValue("M{$active_row}", $dtl->diskon.' % ')
             ->setCellValue("N{$active_row}", $dtl->ppn.' % ')
             ->setCellValue("O{$active_row}", $hitung_subtotal);
             
             $active_row += 1; // set active row to the next row
					}
				}else{
             $active_row += 1; // set active row to the next row  
        }

        //set subtotal
        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:N{$active_row}"); //merge cell
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:O{$active_row}")->applyFromArray($contentgrid);
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getFont()->setBold(true); //set font to bold
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", "Total")
             ->setCellValue("O{$active_row}", $val->bliret);
        
        $active_row += 1; // set active row to the next row
                  		
				if($val->nmjnspembayaran =='Transfer'){
					$total_transfer += $val->bliret;
				}else{
					$total_tunai += $val->bliret;
				}
			
      }
			
      //all total
			$totaleverything = $total_transfer + $total_tunai;
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:N{$active_row}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:O{$active_row}")->applyFromArray($contentgrid);
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getFont()->setBold(true); //set font to bold
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row}", "Grand Total")
           ->setCellValue("O{$active_row}", $totaleverything);
      $active_row += 1;

    
      //total transfer
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:N{$active_row}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:O{$active_row}")->applyFromArray($contentgrid);
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getFont()->setBold(true); //set font to bold
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row}", "Transfer")
           ->setCellValue("O{$active_row}", $total_transfer);
      $active_row += 1;

      //total Tunai
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:N{$active_row}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:O{$active_row}")->applyFromArray($contentgrid);
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}")->getFont()->setBold(true); //set font to bold
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row}", "Tunai")
           ->setCellValue("O{$active_row}", $total_tunai);

      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $this->lib_phpexcel->setActiveSheetIndex(0);

      $filename = "detail_rekap_pembelian_".strtolower(str_replace(array(' ', '&'), array('_', '-'), $subject_file));

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');

      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
      $objWriter->save('php://output');
      exit;
	  }

	}

?>
