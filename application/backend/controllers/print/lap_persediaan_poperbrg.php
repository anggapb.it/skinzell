<?php

	class Lap_persediaan_poperbrg extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function laporan_poperbrg($tglawal,$tglakhir){		
		$isi = '';		
		$this->db->select('*');
        $this->db->from('v_lappodet');
		//$this->db->limit(5,0);
		//$this->db->where("v_lappoperbrgsubtotal.kdbrg", $kdbrg);
		if($tglakhir){
			$this->db->where('`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$query = $this->db->get();
		$aaa = $query->result();
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Pembelian Per Barang', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		//$this->pdf_lap->Cell(0, 0, 'Nama Barang : '. $nmbrg, 0, 1, 'C', 0, '', 0);

		$no = 1;
		//$totutotal = 0;
		foreach($aaa as $i=>$val){
			$isi .= "<tr>
					<td width=\"4%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"7%\" align=\"center\">". $val->nopo ."</td>
					<td width=\"6%\" align=\"center\">". date_format(date_create($val->tglpo), 'd-m-Y') ."</td>
					<td width=\"6%\" align=\"center\">". $val->kdbrg ."</td>
					<td width=\"15%\" align=\"left\">". $val->nmbrg ."</td>
					<td width=\"5%\" align=\"center\">". $val->nmsatuan ."</td>
					<td width=\"4%\" align=\"right\">". $val->qty ."</td>
					<td width=\"4%\" align=\"right\">". $val->qtybonus ."</td>
					<td width=\"6%\" align=\"right\">". number_format($val->hargabeli,0,',','.') ."</td>
					<td width=\"6%\" align=\"right\">". $val->diskon ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->jmlhrgtmbhdiskon,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". $val->ppn ."</td>
					<td width=\"7%\" align=\"right\">". number_format($val->jmlhrgtmbhppn,0,',','.') ."</td>
					<td width=\"15%\">". $val->nmsupplier ."</td>
					<td width=\"7%\" align=\"left\">". $val->stlunas ."</td>
				</tr>";	
			
			//$totutotal += $val->subtotal;
		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"4%\">No.</td>
					<td width=\"7%\">No.<br>Pembelian</td>
					<td width=\"6%\">Tgl<br>Pembelian</td>
					<td width=\"6%\">Kode Barang</td>
					<td width=\"15%\">Nama Barang</td>
					<td width=\"5%\">Satuan</td>
					<td width=\"4%\">Qty</td>
					<td width=\"4%\">Qty Bonus</td>
					<td width=\"6%\">Harga Beli</td>
					<td width=\"6%\">Diskon</td>
					<td width=\"7%\">JumlahHrg+Disk</td>
					<td width=\"4%\">PPN</td>
					<td width=\"7%\">JumlahHrg+PPN</td>
					<td width=\"15%\">Supplier</td>
					<td width=\"7%\">Keterangan</td>
				</tr>
			  <tbody>". $isi ."</tbody>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf_lap->Output('lappoperbrg.pdf', 'I');
	}
	
	function excelpoperbrg($tglawal, $tglakhir) {
		$tablename='v_lappodet';
		$header = array(
			'No. Pembelian',
			'Tgl. Pembelian',
			'Nama Barang',
			'Satuan',
			'Qty',
			'Qty Bonus',
			'Harga Beli',
			'Diskon',
			'JumlahHrg+Disk',
			'PPN',
			'JumlahHrg+PPN',
			'Supplier',
			'Keterangan'
		);
		
		$this->db->select("
			nopo,
			tglpo,
			nmbrg,
			nmsatuan,
			qty,
			qtybonus,
			hargabeli,
			diskon,
			jmlhrgtmbhdiskon,
			ppn,
			jmlhrgtmbhppn,
			nmsupplier,
			stlunas,
		");
        $this->db->from($tablename);
		$this->db->where('`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		//$this->db->where("kdbrg", $kdbrg);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = 'Lap_po_perbarang';
		$data['filter'] = "Laporan Pembelian Per Barang\n".strtoupper('Tanggal')." : ".$tglawal." s/d ".$tglakhir."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellapfarmasi', $data); 	
	}
}
