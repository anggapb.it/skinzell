<?php

	class Lap_persediaan_pengeluaranbrg extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function laporan_pengeluaranbrg($tglawal,$tglakhir,$idbagian,$nmbagian){		
		$isi = '';		
		$this->db->select('*');
        $this->db->from('v_lappengeluaranbrg');
		//$this->db->limit(200,0);
		$this->db->where("v_lappengeluaranbrg.idbagiandari", $idbagian);
		if($tglakhir){
			$this->db->where('`tglkeluar` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$query = $this->db->get();
		$aaa = $query->result();
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Pengeluaran Barang', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		$this->pdf_lap->Cell(0, 0, 'Bagian : '. $nmbagian, 0, 1, 'C', 0, '', 0);

		$no = 1;
		foreach($aaa as $i=>$val){
			$isi .= "<tr>
					<td width=\"3.5%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"6.5%\" align=\"center\">". $val->nokeluarbrg ."</td>
					<td width=\"6%\" align=\"center\">". date_format(date_create($val->tglkeluar), 'd-m-Y') ."</td>
					<td width=\"6%\" align=\"center\">". $val->jamkeluar ."</td>
					<td width=\"17%\" align=\"center\">". $val->nmbagianuntuk ."</td>
					<td width=\"10%\" align=\"center\">". $val->penerima ."</td>
					<td width=\"6%\" align=\"center\">". $val->nmstsetuju ."</td>
					<td width=\"6%\" align=\"center\">". $val->nmsttransaksi ."</td>
					<td width=\"6%\" align=\"center\">". $val->kdbrg ."</td>
					<td width=\"17%\">". $val->nmbrg ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->nmjnsbrg ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->nmsatuankcl ."</td>
					<td width=\"5.5%\" align=\"right\">". $val->qty ."</td>
				</tr>";	
		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"3.5%\">No.</td>
					<td width=\"6.5%\">No. <br> Pengeluaran</td>
					<td width=\"6%\">Tgl <br> Pengeluaran</td>
					<td width=\"6%\">Jam <br> Pengeluaran</td>
					<td width=\"17%\">Bagian Meminta</td>
					<td width=\"10%\">Penerima</td>
					<td width=\"6%\">Status <br> Persetujuan</td>
					<td width=\"6%\">Status <br> Transaksi</td>
					<td width=\"6%\">Kode Barang</td>
					<td width=\"17%\">Nama Barang</td>
					<td width=\"6.5%\">Jenis Barang</td>
					<td width=\"6.5%\">Satuan</td>
					<td width=\"5.5%\">Qty</td>
				</tr>
			  <tbody>". $isi ."</tbody>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf_lap->Output('daftar_supplier.pdf', 'I');
	}
	
	function laporan_excelpengeluaranbrg($tglawal, $tglakhir, $idbagian) {
		$tablename='v_lappengeluaranbrg';
		$header = array(
			'No. Pengeluaran',
			'Tgl. Pengeluaran',
			'Jam Pengeluaran',
			'Bagian Meminta',
			'Penerima',
			'Status Persetujuan',
			'Status Transaksi',
			'Kode Barang',
			'Nama Barang',
			'Jenis Barang',
			'Satuan',
			'Qty'
		);
		
		$this->db->select("
			nokeluarbrg,
			tglkeluar,
			jamkeluar,
			nmbagianuntuk,
			penerima,
			nmstsetuju,
			nmsttransaksi,
			kdbrg,
			nmbrg,
			nmjnsbrg,
			nmsatuankcl,
			qty
		");
        $this->db->from($tablename);
		$this->db->where('tglkeluar BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$this->db->where("idbagiandari", $idbagian);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();
		
		$query = $this->db->getwhere('v_lappengeluaranbrg',array('idbagiandari'=>$idbagian));
		$nmbagian = $query->row_array();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('Lap_Pengeluaranbrg');
		$data['filter'] = strtoupper('Tanggal')." : ".$tglawal." s/d ".$tglakhir."\n Nama Bagian : ".$nmbagian['nmbagiandari']."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellapfarmasi', $data); 	
	}
}
