<?php 
	class Lap_rekamedis_pasienmenurutkeadaan extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function pasienkeluar($tglawal,$tglakhir){
		
		$this->db->select("*");
		$this->db->from("v_laporanrikeadaankeluar");
	//	$this->db->groupby("nopo");
		$this->db->where("tglkeluar BETWEEN '".$tglawal."' and '".$tglakhir."'");
		$query = $this->db->get();
			//var_dump($tglawal);
			//var_dump($tglakhir);
		
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
	//	$this->pdf->AddPage();
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN PASIEN MENURUT KEADAAN KELUAR', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->Cell(0, 0, 'TANGGAL : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		$isi ='';
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
			if($r['idstkeluar'] != $stkeluar){
				$stkeluar = $r['idstkeluar'];
				$isi .= "<tr><td colspan=\"13\">Keadaan Pulang/Keluar: ".$r['nmstkeluar']."</td></tr>";
				$no = 0;
			}
			$isi .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['norm']."</td>
						<td align=\"center\">".$r['noreg']."</td>
						<td>".$r['nmpasien']."</td>
						<td align=\"right\">".$r['tglmasuk']."</td>
						<td align=\"right\">".$r['jammasuk']."</td>
						<td align=\"right\">".$r['tglkeluar']."</td>
						<td align=\"right\">".$r['jamkeluar']."</td>
						<td>".$r['nmbagian']."</td>
						<td align=\"center\">".$r['nmklsrawat']."</td>
						<td align=\"center\">".$r['nmkamar']."</td>
						<td>".$r['nmdoktergelar']."</td>
						<td>".$r['nmpenjamin']."</td>
			</tr>";
		}
	}
		$heads = "<br><br><font size=\"7\" face=\"Helvetica\"> <table border=\"1\">
					<tr align=\"center\">
						<th width=\"3%\">NO.</th>
						<th width=\"10%\">NO. RM</th>
						<th width=\"10%\">NO. REG</th>
						<th width=\"12%\">NAMA PASIEN</th>
						<th width=\"6%\">TGL<br> MASUK</th>
						<th width=\"6%\">JAM<br> MASUK</th>
						<th width=\"6%\">TGL<br> KELUAR</th>
						<th width=\"6%\">JAM<br> KELUAR</th>
						<th width=\"6%\">RUANGAN</th>
						<th width=\"4%\">KELAS</th>
						<th width=\"4%\">KAMAR</th>
						<th width=\"19.5%\">DOKTER RAWAT</th>
						<th width=\"11%\">PENANGGUNG BIAYA</th>
					</tr>".$isi."
		</table></font>";
		$this->pdf->writeHTML($heads,true,false,false,false); 
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>