<?php
class Printnota extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }

    function nota_rj_new($nonota, $noreg) {
		$this->db->select("
			r.noreg AS noreg,
			p.nmpasien AS nmpasien,
			trim(LEADING '0' FROM p.norm) AS norm,
			sbt.kdsbtnm AS kdsbtnm,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			nt.nonota AS nonota,
			dokter.nmdoktergelar AS nmdoktergelar
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('dokter',
                'dokter.iddokter = rd.iddokter', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $noreg);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
      
        $this->db->select("*");
        $this->db->from("nota");
        $this->db->join("shift",
				"shift.idshift = nota.idshift", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("stpelayanan",
				"stpelayanan.idstpelayanan = nota.idstpelayanan", "left");
		$this->db->where("nonota", $nonota);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('notadet',array('nonota'=>$nonota));
		$notadet = $query->result();

			$isi = '';
		$total = 0;
		$totdiskon = 0;
		$header = 0;
		foreach($notadet AS $i=>$val){
			
			
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
			$tarifall = $query->row_array();
			
			$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp)  * $val->qty;
			$total += $tarif;
			$totdiskon += ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp + $val->dijamin);
			
			if($val->koder){
				$item = $tarifall['nmitem'] ." (". $val->qty .") ". $tarifall['satuankcl'];
				$no = $val->koder;
				if($header == 1){
					
					$isi .= "<tr>
						<td colspan='3'><div class='fnota'><b>Obat / Produk</b></div></td>
					</tr>";
					$header = 2;
				}
			} else {
				$item = $tarifall['nmitem'];
				$no = ($i+1);
				if($header == 0){
					
					$isi .= "<tr>
						<td colspan='3'><div class='fnota'><b>Tindakan</b></div></td>
					</tr>";
					$header = 1;
				}
			}
			
			$isi .= "<tr>
				<td width=\"85%\"><div class='fnota'>".$no.". ".$item ."</div></td>
				<td width=\"15%\" align=\"right\"><div class='fnota'>". number_format($tarif,0,',','.') ."</div></td>
			</tr>";
		}

		$totalr = $total + $reg['uangr'];
		$jumtotal = ($totalr - $reg['diskonr'] - $totdiskon);
		
		$html = "
		<style>
			.fnota {
				font-size: 10;
				letter-spacing:4;
				font-family: Arial, Helvetica, sans-serif;
			}
			
		</style>
		<html>
		<head>
			<title>NOTA RAWAT JALAN</title>
		</head>
		<body>
		<div style='font-size: 14;letter-spacing:5;font-family: Arial, Helvetica, sans-serif;'>".NMKLINIK."</div>
		<div style='font-size: 10;letter-spacing:5;font-family: Arial, Helvetica, sans-serif;'>".ALAMAT_SATU."</div>
		<div style='font-size: 10;letter-spacing:5;font-family: Arial, Helvetica, sans-serif;'>".ALAMAT_DUA."</div>
			<hr>
			<center><div class='fnota'><b>NOTA TRANSAKSI</b></div></center>
			<div class='fnota'><br/>
			<table border='0px' cellpadding='0'>
			  <tbody>
			 	
				<tr>
					<td><div class='fnota'>No. Nota</div></td>
					<td><div class='fnota'>:</div></td>
					<td width='50%' align='left'><div class='fnota'>".$reg['nonota']."</div></td>
					
					<td><div class='fnota'>Tgl</div></td>
					<td><div class='fnota'>:</div></td>
					<td align='left'><div class='fnota'>".date_format(date_create($nota['tglnota']), 'd-m-Y')."</div></td>

				</tr>
				<tr>
					<td><div class='fnota'>No. RM</div></td>
					<td><div class='fnota'>:</div></td>
					<td width='50%' align='left'><div class='fnota'>".str_pad($reg['norm'], 10, "0", STR_PAD_LEFT)."</div></td>
					
					<td><div class='fnota'>Jam</div></td>
					<td><div class='fnota'>:</div></td>
					<td align='left'><div class='fnota'>".$nota['jamnota']."</div></td>

				</tr>
				<tr>
					<td><div class='fnota'>Pasien</div></td>
					<td><div class='fnota'>:</div></td>
					<td align='left'><div class='fnota'>".$reg['kdsbtnm'] .' '. substr($pasien,0,15)."</div></td>
				</tr>
				<tr>
					<td><div class='fnota'>Dokter</div></td>
					<td><div class='fnota'>:</div></td>
					<td align='left'><div class='fnota'>".$reg['nmdoktergelar']."</div></td>
				</tr>
				</tbody>
			</table></div>
			<hr>
			<div class='fnota'>
			<table border='0px' cellpadding='0'>
			  <tbody>
			  ". $isi ."
			  </tbody>
			</table></div>
			<table border='0px' cellpadding='0'>
				<tr>
					<td width=\"70%\" align='right'><div class='fnota'>Total</div></td>
					
					<td width=\"15%\" align='right'><div class='fnota'>". number_format((int)$totalr,0,',','.') ."</div></td>
				</tr>
				<tr>
					<td align='right'><div class='fnota'> Diskon </div></td>
					<td align='right'><div class='fnota'>". number_format(((int)$reg['diskonr']+(int)$totdiskon),0,',','.') ."</div></td>
				</tr>
				<tr>
					<td><div class='fnota'><p align='right'><b> Jumlah </b></p></div></td>
					<td align='right'><div class='fnota'><b>". number_format((int)$jumtotal,0,',','.') ."</b></div></td>
				</tr>
			</table>
			<br>
			<div align='right' class='fnota'># ".$this->rp_terbilang($jumtotal, 0)." Rupiah #</div>
			<br>
			<div align='right' class='fnota'><i>Tgl. & Jam Cetak : ".date('d-m-Y')." & ".date('H:i:s')."</i></div>
			<br>
			<div class='fnota'>( ". $nota['userinput'] .")</div>
			<br>
			<br>
			<div class='fnota'>Petugas Kasir</div>
		</body>
		</html>";
		print_r($html);
		
	}
    
    function notari($nona, $noreg) {
		$this->pdf->SetPrintHeader(false);
		//$query = $this->db->orderby('idregdet','desc')->getwhere('v_registrasi',array('noreg'=>$noreg));
		$this->db->select("
			r.noreg AS noreg,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			bag.nmbagian AS nmbagian,
			sbt.kdsbtnm AS kdsbtnm,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit
		");
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
        $this->db->where('r.noreg', $noreg);
		$q = $this->db->get();
		$reg = $q->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
      
        $this->db->select("
			nt.nonota AS nonota,
			nt.nona AS nona,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			nt.idregdettransfer AS idregdettransfer,
			ntdet.kditem AS kditem,
			(SUM(ntdet.qty) - if(rfdet.qty, rfdet.qty, 0 )) AS qty,
			
			(SUM(ntdet.tarifjs*ntdet.qty) + SUM(ntdet.tarifjm*ntdet.qty) + SUM(ntdet.tarifjp*ntdet.qty) + SUM(ntdet.tarifbhp*ntdet.qty)) - if(( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet )>0, SUM((ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp)* ( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet ) ), 0) AS tottarif,
			
			(ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp) AS tarif,
			sum(ntdet.diskonjs+ntdet.diskonjm+ntdet.diskonjp+ntdet.diskonbhp) AS totdiskon,
			ntdet.koder AS koder,
			left(bp.nmitem, 10) AS nmitem1,
			bp.nmitem AS nmitem,
			nta.tglna AS tglna,
			nta.jamna AS jamna,
			kui.nokuitansi AS nokuitansi,
			kui.atasnama AS atasnama,
			kui.total AS total,
			r.noreg AS noreg,
			TRIM(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			rd.userinput AS userinput,
			rd.tglreg AS tglreg,
			rd.jamreg AS jamreg,
			rd.tglkeluar AS tglkeluar,
			rd.jamkeluar AS jamkeluar,
			TIMESTAMPDIFF(SECOND,CONCAT(rd.tglreg,' ',rd.jamreg),CONCAT(rd.tglkeluar,' ',rd.jamkeluar)) AS lamainap,
			(
				select bag.nmbagian
				from bagian bag, registrasidet rd2
				where bag.idbagian = rd2.idbagian and rd2.noreg = r.noreg
				group by rd2.noreg
				order by rd2.idregdet desc
			) AS nmbagian,
			d.nmdokter AS nmdokter,
			d.nmdoktergelar AS nmdoktergelar,
			sp.nmstpelayanan AS nmstpelayanan
		", false);
        $this->db->from('nota nt');
		$this->db->join('registrasidet rd',
                'rd.idregdet = nt.idregdet', 'left');
		$this->db->join('registrasi r',
                'r.noreg = rd.noreg', 'left');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('notaakhir nta',
                'nta.nona = nt.nona', 'left');
		$this->db->join('notadet ntdet',
                'ntdet.nonota = nt.nonota', 'left');
		$this->db->join('returfarmasidet rfdet',
                'rfdet.idnotadet = ntdet.idnotadet', 'left');
		$this->db->join('v_brgpel bp',
                'bp.kditem = ntdet.kditem', 'left');
		$this->db->join('dokter d',
                'd.iddokter = ntdet.iddokter', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->groupby("nt.nona, ntdet.kditem");
		$this->db->orderby("r.noreg");
		$this->db->where("nt.nona", $nona);		
        $query = $this->db->get();
		$nota = $query->row_array();
		$arrnota = $query->result();
      
        $this->db->select("total, nota.nonota");
        $this->db->from("nota");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
		$this->db->where("idregdettransfer", $noreg);
        $queryugd = $this->db->get();
		$totalugd = $queryugd->row_array();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 400, 'ury' => 345),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		//$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNotaRI();
		$this->pdf->SetFont('helvetica', 'B', 12);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">KUITANSI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Kuitansi');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
		
		$x+=0;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Lama');
		$x+=0;$y+=42;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'. CEIL($nota['lamainap']/86400). ' Hari' );
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['nmbagian']);
		
		$x+=5;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Status');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'. $nota['nmstpelayanan']);
		
		$x+=5;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl. Masuk');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglreg']), 'd-m-Y'));
		
		$x+=5;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl. Keluar');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglkeluar']), 'd-m-Y'));
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;"><b><i>Sudah Terima Dari</i></b>');
		
		$x+=5;$y=3;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Nama Pasien '.$reg['kdsbtnm'] .' '. substr($pasien,0,15));
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Nama Pasien '.$reg['kdsbtnm'] .' '. $reg['nmpasien']);
		
		$x+=8;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;"><b>Untuk Pembayaran Biaya</b>');
		
		$x+=5;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 390, $x+2, $style);

		$isi = "";
		$total = 0;
		$totugd = 0;
		$totalobat = 0;
		$totdiskon = 0;
		$totdiskonf = 0;
		$totuangr = 0;
		$header = 0;
		$i = 0;
		$no = 0;
		foreach($arrnota AS $j=>$value){
			$totdiskon += $value->totdiskon;
			if(!$value->koder){
				if($value->kditem){
					$total += $value->tottarif;
					$x+=5;
					$item = htmlentities($value->nmitem);	
					$no = ($i+1);
					if(strlen($item)>40)$x+=5;
					$isi .= "<tr>
						<td width=\"5%\" align=\"center\">". $no ."</td>
						<td width=\"37%\">". $item ."</td>
						<td width=\"30%\">". $value->nmdoktergelar ."</td>
						<td width=\"5%\" align=\"right\">". $value->qty ."</td>
						<td width=\"10%\" align=\"right\">". number_format($value->tarif,0,',','.') ."</td>
						<td width=\"13%\" align=\"right\">". number_format($value->tottarif,0,',','.') ."</td>
					</tr>";
					$i++;
				}
			} else {
				$totalobat += $value->tottarif;
				$totuangr = $value->uangr;
				$totdiskonf =  $value->diskonr;
				$header = 1;
			}
		}
		if($header == 1){
			$no += 1;
			$x+=6;
			$isi .= "<tr>
				<td width=\"5%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">Obat</td>
				<td width=\"25%\" align=\"right\">". number_format($totalobat,0,',','.') ."</td>
			</tr>";
		}
		
		if($queryugd->num_rows()>0){
			$totugd = $totalugd['total'];
			$no += 1;
			$x+=6;
			$isi .= "<tr>
				<td width=\"5%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">Transaksi UGD</td>
				<td width=\"25%\" align=\"right\">". number_format($totugd,0,',','.') ."</td>
			</tr>";
		}
		
		$jmlttl = $total+$totalobat + $totuangr + $totugd;
		$totalr = (($total+$totalobat + $totuangr) - ($totdiskon + $totdiskonf)) + $totugd;
		$totaljum = ($totalr - $reg['deposit']);
		$sisa = $reg['deposit'] - $totalr;
		if($sisa<0)$sisa=0;
		if($totaljum<0)$totaljum=0;
		$x+=10;
		$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
			  <theader>
				<tr>
					<td width=\"6%\" align=\"center\">No</td>
					<td width=\"37%\" align=\"center\">Nama Tindakan</td>
					<td width=\"30%\" align=\"center\">Dokter</td>
					<td width=\"5%\" align=\"center\">Qty</td>
					<td width=\"10%\" align=\"center\">Harga</td>
					<td width=\"13%\" align=\"center\">Subtotal</td>
				</tr>
				<tr>
					<td width=\"100%\" align=\"center\">&nbsp;</td>
				</tr>
			  </theader>
			  <tbody>
			  ". $isi ."
			  </tbody>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Racik </p></td>
					<td width=\"25%\" align=\"right\">". number_format($totuangr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Jumlah </p></td>
					<td width=\"25%\" align=\"right\">". number_format(($jmlttl),0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"25%\" align=\"right\">". number_format(($totdiskon+$totdiskonf),0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total </p></td>
					<td width=\"25%\" align=\"right\"><b>". number_format((int)$totalr,0,',','.') ."</b></td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Deposit </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$reg['deposit'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total Harus Dibayar </p></td>
					<td width=\"25%\" align=\"right\"><b>". number_format($totaljum,0,',','.') ."</b></td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Sisa Deposit </p></td>
					<td width=\"25%\" align=\"right\">". number_format($sisa,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
			if($x>330) $x = $x%310;
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		$x+=35;$y=5;
		$this->pdf->writeHTMLCell(380, 10, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($totaljum, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		$x+=10;$y+=0;
		$this->pdf->writeHTMLCell(380, 10, $y, $x, '<div style="letter-spacing:5;">Bandung, '.date('d-m-Y').' '.date('H:i:s'), '', 1, 0, true, 'R', true);
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		
		$x+=8;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Keuangan', '', 1, 0, true, 'C', true);
		$x+=25;$y=300;
		$this->pdf->writeHTMLCell(0, 50, $y, $x, '<div style="letter-spacing:5;">( '. $nota['userinput'] .' )', '', 1, 0, true, 'C', true);
		
//detail obat
		$this->pdf->AddPage('L', $page_format, false, false);$x = 20;
		$this->pdf->headerNotaRI();
		$this->pdf->SetFont('helvetica', 'B', 12);
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Detail Obat Farmasi</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $pasien);
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $reg['nmpasien']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['nmbagian']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdoktergelar']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Status');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 390, $x+2, $style);
		$isi = '';
		$total = 0;
		$totalobat = 0;
		$i = 0;
		
		foreach($arrnota AS $j=>$value){
			if($value->koder){
				$x+=5;
				$item = $value->nmitem;
				$no = ($i+1);
				
				$totalobat += $value->tottarif;
				$isi .= "<tr>
					<td width=\"5%\" align=\"center\">". $no ."</td>
					<td width=\"38%\">". $item ."</td>
					<td width=\"12%\" align=\"right\">". $value->qty ."</td>
					<td width=\"20%\" align=\"right\">". number_format($value->tarif,0,',','.') ."</td>
					<td width=\"25%\" align=\"right\">". number_format($value->tottarif,0,',','.') ."</td>
				</tr>";
				/* $isi .= "<tr>
					<td width=\"5%\" align=\"center\">". $no ."</td>
					<td width=\"70%\">". $item ." - ". $value->qty ."</td>
					<td width=\"20%\" align=\"right\">". number_format($value->tottarif,0,',','.') ."</td>
				</tr>"; */
				$i++;
			}
		}
		
		$x+=6;
		$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
			  <theader>
				<tr>
					<td width=\"6%\" align=\"center\">No</td>
					<td width=\"40%\" align=\"center\">Nama Item</td>
					<td width=\"10%\" align=\"center\">Qty</td>
					<td width=\"20%\" align=\"center\">Harga</td>
					<td width=\"25%\" align=\"center\">Subtotal</td>
				</tr>
				<tr>
					<td width=\"100%\" align=\"center\">&nbsp;</td>
				</tr>
			  </theader>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Total </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$totalobat,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";		
		$this->pdf->writeHTML($html,true,false,false,false);
		
		if($x>330) $x = $x%305;
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		$x+=25;$y=0;
		$this->pdf->writeHTMLCell(385, 10, $y, $x, '<div style="letter-spacing:5;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		$x+=20;$y+=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">( '. $nota['userinput'] .' )');
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Petugas Transaksi');

//detail obat ugd
		if(isset($totalugd['nonota'])){
			$query = $this->db->from('notadet')->where(array('nonota'=>$totalugd['nonota']))->order_by('koder', 'ASC')->get();
			$notadetf = $query->result();
			$query = $this->db->getwhere('nota',array('nonota'=>$totalugd['nonota']));
			$notaugd = $query->row_array();
			
			$this->pdf->AddPage('L', $page_format, false, false);$x = 20;
			$this->pdf->headerNotaRI();
			$this->pdf->SetFont('helvetica', 'B', 12);
			$x=25;$y=3;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Detail Transaksi UGD</div>', '', 1, 0, true, 'C', true);
			
			$this->pdf->SetFont('helvetica', '', 10);
			$x+=5;$y+=0;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
			$x+=0;$y+=40;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
			
			$x+=0;$y=320;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
			$x+=0;$y+=25;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
			
			$x+=5;$y=3;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
			$x+=0;$y+=40;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			//$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $pasien);
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $reg['nmpasien']);
			
			$x+=0;$y=320;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
			$x+=0;$y+=25;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
			
			$x+=5;$y=3;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
			$x+=0;$y+=40;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
			
			$x+=0;$y=320;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
			$x+=0;$y+=25;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
			
			$x+=5;$y=3;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
			$x+=0;$y+=40;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdoktergelar']);
			
			$x+=0;$y=320;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Status');
			$x+=0;$y+=25;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmstpelayanan']);
			
			$x+=3;$y=0;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
			

			$this->pdf->Line(3, $x+2, 390, $x+2, $style);
			$isi = '';
			$total = 0;
			$totuangr = 0;
			$totdiskonf = 0;
			$i = 0;
			
			/* foreach($notadetf AS $i=>$val){
				$x+=6;
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				$tarifall = $query->row_array();
				
				$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty;
				$total += $tarif;
				
				$isi .= "<tr>
					<td width=\"10%\" align=\"center\">R/". $val->koder ."</td>
					<td width=\"70%\">". $tarifall['nmitem'] ." - ". $val->qty ." - ". $tarifall['satuankcl'] ."</td>
					<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
				</tr>";
			} */
			
			foreach($notadetf AS $i=>$val){
				$x+=5;
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				$tarifall = $query->row_array();
				
				$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty - ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
				$total += $tarif;
				
				if($val->koder){
					$item = $tarifall['nmitem'] ." - ". $val->qty ." - ". $tarifall['satuankcl'];
					$no = 'R/ '. $val->koder;
					if($header == 1){
						$x+=5;
						$isi .= "<tr>
							<td width=\"100%\" colspan=\"3\"><b>Obat / Alkes</b></td>
						</tr>";
						$header = 2;
					}
				} else {
					$item = $tarifall['nmitem'];
					$no = ($i+1);
					if($header == 0){
						$x+=5;
						$isi .= "<tr>
							<td width=\"100%\" colspan=\"3\"><b>Tindakan</b></td>
						</tr>";
						$header = 1;
					}
				}
				
				$isi .= "<tr>
					<td width=\"10%\" align=\"center\">". $no ."</td>
					<td width=\"70%\">". $item ."</td>
					<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
				</tr>";
			}
			
			$totalr = ($notaugd['uangr'] + $total) - $notaugd['diskon'];
			$x+=6;
			$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
				<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
				  <tbody>". $isi ."</tbody>
					<tr>
						<td width=\"80%\"><p align=\"right\"> Racik </p></td>
						<td width=\"20%\" align=\"right\">". number_format((int)$notaugd['uangr'],0,',','.') ."</td>
					</tr>
					<tr>
						<td width=\"80%\"><p align=\"right\"> Diskon </p></td>
						<td width=\"20%\" align=\"right\">". number_format((int)$notaugd['diskon'],0,',','.') ."</td>
					</tr>
					<tr>
						<td width=\"80%\"><p align=\"right\"> Total </p></td>
						<td width=\"20%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
					</tr>
					<tr>
						<td width=\"80%\"><p align=\"right\"> Total Harus Dibayar </p></td>
						<td width=\"20%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
					</tr>
				</table></font></div>
			";
			$this->pdf->writeHTML($html,true,false,false,false);
			
			if($x>330) $x = $x%310;
			if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
			$x+=25;$y=5;
			$this->pdf->writeHTMLCell(380, 10, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($totalr, 0).' Rupiah #', '', 1, 0, true, 'R', true);
			
			if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
			$x+=5;$y=5;
			$this->pdf->writeHTMLCell(380, 10, $y, $x, '<div style="letter-spacing:5;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
			if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
			$x+=15;$y+=10;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">( '. $nota['userinput'] .' )');
			
			$x+=5;$y=5;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Petugas Transaksi');
		}
		

		//Close and output PDF document
		$this->pdf->Output('notari.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
    function notaris($noreg) {
		$this->pdf->SetPrintHeader(false);
		$this->db->select("
			r.noreg AS noreg,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			bag.nmbagian AS nmbagian,
			sbt.kdsbtnm AS kdsbtnm,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit
		");
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
        $this->db->where('r.noreg', $noreg);
		$q = $this->db->get();
		$reg = $q->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
		
		$this->db->select("*, sp.nmstpelayanan AS nmstpelayanan,
				TIMESTAMPDIFF(SECOND,CONCAT(registrasidet.tglreg,' ',registrasidet.jamreg),IF(registrasidet.tglkeluar, CONCAT(registrasidet.tglkeluar,' ',registrasidet.jamkeluar), NOW())) AS lamainap,
				bagian.nmbagian AS nmbagian, notadet.qty AS qty,
				nota.diskon AS diskonr, (notadet.diskonjs+notadet.diskonjm+notadet.diskonjp+notadet.diskonbhp) AS totdiskon,
				((notadet.tarifjs*notadet.qty) + (notadet.tarifjm*notadet.qty) + (notadet.tarifjp*notadet.qty) + (notadet.tarifbhp*notadet.qty)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - if((
						select if(rfd.qty, rfd.qty, 0)
						from returfarmasidet rfd
						where notadet.idnotadet = rfd.idnotadet
					)>0,((notadet.tarifjs+notadet.tarifjm+notadet.tarifjp+notadet.tarifbhp)*
					(
						select if(rfd.qty, rfd.qty, 0)
						from returfarmasidet rfd
						where notadet.idnotadet = rfd.idnotadet
					)
				), 0) AS tottarif,
				(notadet.tarifjs+notadet.tarifjm+notadet.tarifjp+notadet.tarifbhp) AS tarif
		", false);
        $this->db->from("notadet");
        $this->db->join("nota",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg = registrasidet.noreg", "left");
        $this->db->join("bagian",
				"bagian.idbagian = registrasidet.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        $this->db->join("v_brgpel",
				"v_brgpel.kditem = notadet.kditem", "left");
        $this->db->join("jsatuan",
				"jsatuan.idsatuan = notadet.idsatuan", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nota.idstpelayanan', 'left');
		$this->db->where("registrasi.noreg", $noreg);
		$query = $this->db->get();
		$nota = $query->row_array();
		$arrnota = $query->result();
	  
        $this->db->select("total, nota.nonota");
        $this->db->from("nota");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
		$this->db->where("idregdettransfer", $noreg);
        $queryugd = $this->db->get();
		$totalugd = $queryugd->row_array();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 400, 'ury' => 345),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		//$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNotaRI();
		$this->pdf->SetFont('helvetica', 'B', 12);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">KUITANSI SEMENTARA</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Kuitansi');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
		
		$x+=0;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Lama');
		$x+=0;$y+=42;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'. CEIL($nota['lamainap']/86400). ' Hari' );
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
		
		$x+=5;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Status');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'. $nota['nmstpelayanan']);
		
		$x+=5;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl. Masuk');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglreg']), 'd-m-Y'));
		
		$x+=5;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl. Keluar');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglkeluar']), 'd-m-Y'));
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;"><b><i>Sudah Terima Dari</i></b>');
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Nama Pasien '.$reg['kdsbtnm'] .' '. $reg['nmpasien']);
		
		$x+=8;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;"><b>Untuk Pembayaran Biaya</b>');
		
		$x+=5;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 390, $x+2, $style);

		$isi = "";
		$total = 0;
		$totugd = 0;
		$totalobat = 0;
		$totdiskon = 0;
		$totdiskonf = 0;
		$totuangr = 0;
		$header = 0;
		$i = 0;
		$no = 0;
		foreach($arrnota AS $j=>$value){
			$totdiskon += $value->totdiskon;
			if(!$value->koder){
				$total += $value->tarif*$value->qty;
				$x+=5;
				$item = htmlentities($value->nmitem);
				$no = ($i+1);
			
				$isi .= "<tr>
					<td width=\"5%\" align=\"center\">". $no ."</td>
					<td width=\"40%\">". $item ."</td>
					<td width=\"10%\" align=\"right\">". $value->qty ."</td>
					<td width=\"20%\" align=\"right\">". number_format($value->tarif,0,',','.') ."</td>
					<td width=\"25%\" align=\"right\">". number_format($value->tarif*$value->qty,0,',','.') ."</td>
				</tr>";
				$i++;
			} else {
				$totalobat += $value->tarif*$value->qty;
				$totuangr = $value->uangr;
				$totdiskonf =  $value->diskonr;
				$header = 1;
			}
		}
		
		if($header == 1){
			$no += 1;
			$x+=6;
			$isi .= "<tr>
				<td width=\"5%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">Obat</td>
				<td width=\"25%\" align=\"right\">". number_format($totalobat,0,',','.') ."</td>
			</tr>";
		}
		
		if($queryugd->num_rows()>0){
			$totugd = $totalugd['total'];
			$no += 1;
			$x+=6;
			$isi .= "<tr>
				<td width=\"5%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">Transaksi UGD</td>
				<td width=\"25%\" align=\"right\">". number_format($totugd,0,',','.') ."</td>
			</tr>";
		}
		
		$jmlttl = $total + $totalobat + $totuangr + $totugd;
		$totalr = (($total+$totalobat + $totuangr) - ($totdiskon + $totdiskonf)) + $totugd;
		$totaljum = ($totalr - $reg['deposit']);
		$sisa = $reg['deposit'] - $totalr;
		if($sisa<0)$sisa=0;
		if($totaljum<0)$totaljum=0;
		$x+=10;
		$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
			  <theader>
				<tr>
					<td width=\"6%\" align=\"center\">No</td>
					<td width=\"40%\" align=\"center\">Nama Tindakan</td>
					<td width=\"10%\" align=\"center\">Qty</td>
					<td width=\"20%\" align=\"center\">Harga</td>
					<td width=\"25%\" align=\"center\">Subtotal</td>
				</tr>
				<tr>
					<td width=\"100%\" align=\"center\">&nbsp;</td>
				</tr>
			  </theader>
			  <tbody>
			  ". $isi ."
			  </tbody>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Racik </p></td>
					<td width=\"25%\" align=\"right\">". number_format($totuangr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Jumlah </p></td>
					<td width=\"25%\" align=\"right\">". number_format(($jmlttl),0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"25%\" align=\"right\">". number_format(($totdiskon+$totdiskonf),0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Deposit </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$reg['deposit'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total Harus Dibayar </p></td>
					<td width=\"25%\" align=\"right\">". number_format($totaljum,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Sisa Deposit </p></td>
					<td width=\"25%\" align=\"right\">". number_format($sisa,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		if($x>330) $x = $x%310;
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		$x+=30;$y=5;
		$this->pdf->writeHTMLCell(380, 10, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($totaljum, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		$x+=10;$y+=0;
		$this->pdf->writeHTMLCell(380, 10, $y, $x, '<div style="letter-spacing:5;">Bandung, '.date('d-m-Y').' '.date('H:i:s'), '', 1, 0, true, 'R', true);
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		
		$x+=8;$y=300;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Keuangan', '', 1, 0, true, 'C', true);
		$x+=25;$y=300;
		$this->pdf->writeHTMLCell(0, 50, $y, $x, '<div style="letter-spacing:5;">( '. $nota['userinput'] .' )', '', 1, 0, true, 'C', true);
		
//detail obat
		$this->pdf->AddPage('L', $page_format, false, false);$x = 20;
		$this->pdf->headerNotaRI();
		$this->pdf->SetFont('helvetica', 'B', 12);
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Detail Obat Farmasi Sementara</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $pasien);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Status');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 390, $x+2, $style);
		$isi = '';
		$total = 0;
		$totalobat = 0;
		$i = 0;
		
		foreach($arrnota AS $j=>$value){
			if($value->koder){
				$x+=5;
				$item = $value->nmitem;
				$no = ($i+1);
				
				$totalobat += $value->tottarif;
				$isi .= "<tr>
					<td width=\"5%\" align=\"center\">". $no ."</td>
					<td width=\"38%\">". $item ."</td>
					<td width=\"12%\" align=\"right\">". $value->qty ."</td>
					<td width=\"20%\" align=\"right\">". number_format($value->tarif,0,',','.') ."</td>
					<td width=\"25%\" align=\"right\">". number_format($value->tottarif,0,',','.') ."</td>
				</tr>";
				$i++;
			}
		}
		
		$x+=6;
		$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
			  <theader>
				<tr>
					<td width=\"6%\" align=\"center\">No</td>
					<td width=\"40%\" align=\"center\">Nama Item</td>
					<td width=\"10%\" align=\"center\">Qty</td>
					<td width=\"20%\" align=\"center\">Harga</td>
					<td width=\"25%\" align=\"center\">Subtotal</td>
				</tr>
				<tr>
					<td width=\"100%\" align=\"center\">&nbsp;</td>
				</tr>
			  </theader>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Total </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$totalobat,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		if($x>330) $x = $x%310;
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		$x+=25;$y=0;
		$this->pdf->writeHTMLCell(385, 10, $y, $x, '<div style="letter-spacing:5;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
		$x+=20;$y+=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">( '. $nota['userinput'] .' )');
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Petugas Transaksi');

//detail obat ugd
		if(isset($totalugd['nonota'])){
			$query = $this->db->from('notadet')->where(array('nonota'=>$totalugd['nonota']))->order_by('koder', 'ASC')->get();
			$notadetf = $query->result();
			$query = $this->db->getwhere('nota',array('nonota'=>$totalugd['nonota']));
			$notaugd = $query->row_array();
			
			$this->pdf->AddPage('L', $page_format, false, false);$x = 20;
			$this->pdf->headerNotaRI();
			$this->pdf->SetFont('helvetica', 'B', 12);
			$x=25;$y=3;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Detail Transaksi UGD</div>', '', 1, 0, true, 'C', true);
			
			$this->pdf->SetFont('helvetica', '', 10);
			$x+=5;$y+=0;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
			$x+=0;$y+=40;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
			
			$x+=0;$y=320;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
			$x+=0;$y+=25;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
			
			$x+=5;$y=3;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
			$x+=0;$y+=40;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $pasien);
			
			$x+=0;$y=320;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
			$x+=0;$y+=25;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
			
			$x+=5;$y=3;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
			$x+=0;$y+=40;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
			
			$x+=0;$y=320;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
			$x+=0;$y+=25;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
			
			$x+=5;$y=3;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
			$x+=0;$y+=40;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
			
			$x+=0;$y=320;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Status');
			$x+=0;$y+=25;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
			$x+=0;$y+=2;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmstpelayanan']);
			
			$x+=3;$y=0;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
			

			$this->pdf->Line(3, $x+2, 390, $x+2, $style);
			$isi = '';
			$total = 0;
			$totuangr = 0;
			$totdiskonf = 0;
			$i = 0;
			
			foreach($notadetf AS $i=>$val){
				$x+=5;
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				$tarifall = $query->row_array();
				
				$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty - ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
				$total += $tarif;
				
				if($val->koder){
					$item = $tarifall['nmitem'] ." - ". $val->qty ." - ". $tarifall['satuankcl'];
					$no = 'R/ '. $val->koder;
					if($header == 1){
						$x+=5;
						$isi .= "<tr>
							<td width=\"100%\" colspan=\"3\">Obat / Alkes</td>
						</tr>";
						$header = 2;
					}
				} else {
					$item = $tarifall['nmitem'];
					$no = ($i+1);
					if($header == 0){
						$x+=5;
						$isi .= "<tr>
							<td width=\"100%\" colspan=\"3\">Tindakan</td>
						</tr>";
						$header = 1;
					}
				}
				
				$isi .= "<tr>
					<td width=\"10%\" align=\"center\">". $no ."</td>
					<td width=\"70%\">". $item ."</td>
					<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
				</tr>";
			}
			
			$totalr = ($notaugd['uangr'] + $total) - $notaugd['diskon'];
			$x+=6;
			$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
				<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
				  <tbody>". $isi ."</tbody>
					<tr>
						<td width=\"80%\"><p align=\"right\"> Racik </p></td>
						<td width=\"20%\" align=\"right\">". number_format((int)$notaugd['uangr'],0,',','.') ."</td>
					</tr>
					<tr>
						<td width=\"80%\"><p align=\"right\"> Diskon </p></td>
						<td width=\"20%\" align=\"right\">". number_format((int)$notaugd['diskon'],0,',','.') ."</td>
					</tr>
					<tr>
						<td width=\"80%\"><p align=\"right\"> Total </p></td>
						<td width=\"20%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
					</tr>
					<tr>
						<td width=\"80%\"><p align=\"right\"> Total Harus Dibayar </p></td>
						<td width=\"20%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
					</tr>
				</table></font></div>
			";
			$this->pdf->writeHTML($html,true,false,false,false);
			if($x>330) $x = $x%310;
			if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
			$x+=25;$y=5;
			$this->pdf->writeHTMLCell(380, 10, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($totalr, 0).' Rupiah #', '', 1, 0, true, 'R', true);
			
			if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
			$x+=5;$y=5;
			$this->pdf->writeHTMLCell(380, 10, $y, $x, '<div style="letter-spacing:5;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
			if($x>330) {$this->pdf->AddPage('L', $page_format, false, false);$x = 20;}
			$x+=15;$y+=10;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">( '. $nota['userinput'] .' )');
			
			$x+=5;$y=5;
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Petugas Transaksi');
		}
		

		//Close and output PDF document
		$this->pdf->Output('notaris.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
    function notafarmasitrans($noreg) {
		$this->pdf->SetPrintHeader(false);

		$query = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg));
		$reg = $query->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
      
        $this->db->select("
			nt.nonota AS nonota,
			nt.nona AS nona,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			nt.idregdettransfer AS idregdettransfer,
			ntdet.kditem AS kditem,
			(SUM(ntdet.qty) - if(rfdet.qty, rfdet.qty, 0 )) AS qty,
			
			(SUM(ntdet.tarifjs*ntdet.qty) + SUM(ntdet.tarifjm*ntdet.qty) + SUM(ntdet.tarifjp*ntdet.qty) + SUM(ntdet.tarifbhp*ntdet.qty)) - if(( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet )>0, SUM((ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp)* ( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet ) ), 0) AS tottarif,
			
			(ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp) AS tarif,
			(ntdet.diskonjs+ntdet.diskonjm+ntdet.diskonjp+ntdet.diskonbhp) AS totdiskon,
			ntdet.koder AS koder,
			bp.nmitem AS nmitem,
			nta.tglna AS tglna,
			nta.jamna AS jamna,
			kui.nokuitansi AS nokuitansi,
			kui.atasnama AS atasnama,
			kui.total AS total,
			r.noreg AS noreg,
			TRIM(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			rd.userinput AS userinput,
			rd.tglreg AS tglreg,
			rd.jamreg AS jamreg,
			rd.tglkeluar AS tglkeluar,
			rd.jamkeluar AS jamkeluar,
			TIMESTAMPDIFF(SECOND,CONCAT(rd.tglreg,' ',rd.jamreg),CONCAT(rd.tglkeluar,' ',rd.jamkeluar)) AS lamainap,
			(
				select bag.nmbagian
				from bagian bag, registrasidet rd2
				where bag.idbagian = rd2.idbagian and rd2.noreg = r.noreg
				group by rd2.noreg
				order by rd2.idregdet desc
			) AS nmbagian,
			d.nmdokter AS nmdokter,
			d.nmdoktergelar AS nmdoktergelar,
			sp.nmstpelayanan AS nmstpelayanan
		", false);
        $this->db->from('nota nt');
		$this->db->join('registrasidet rd',
                'rd.idregdet = nt.idregdet', 'left');
		$this->db->join('registrasi r',
                'r.noreg = rd.noreg', 'left');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('notaakhir nta',
                'nta.nona = nt.nona', 'left');
		$this->db->join('notadet ntdet',
                'ntdet.nonota = nt.nonota', 'left');
		$this->db->join('returfarmasidet rfdet',
                'rfdet.idnotadet = ntdet.idnotadet', 'left');
		$this->db->join('v_brgpel bp',
                'bp.kditem = ntdet.kditem', 'left');
		$this->db->join('dokter d',
                'd.iddokter = ntdet.iddokter', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->groupby("nt.nona, ntdet.kditem");
		$this->db->orderby("r.noreg");
	//	$this->db->where("nt.nona", $nona);
        $query = $this->db->get();
		$nota = $query->row_array();
      
    
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 274),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		//$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
        $this->pdf->Line(3, $x-2, 205, $x-2, $style);
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">KUITANSI FARMASI</div>', '', 1, 0, true, 'C', true);
		
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">No. Kuitansi');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['noreg']);
		
		$x+=0;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Lama');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'. CEIL($nota['lamainap']/86400). ' Hari' );
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=24;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">'.$nota['nmbagian']);
		
		$x+=3;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">Status');
		$x+=0;$y+=24;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'. $nota['nmstpelayanan']);
		
		$x+=3;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Tgl. Masuk');
		$x+=0;$y+=24;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.date_format(date_create($nota['tglreg']), 'd-m-Y'));
		
		$x+=3;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Tgl. Keluar');
		$x+=0;$y+=24;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.date_format(date_create($nota['tglkeluar']), 'd-m-Y'));
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;"><b><i>Sudah Terima Dari</i></b>');
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Nama Pasien '.$reg['kdsbtnm'] .' '. substr($pasien,0,15));
		
		$x+=6;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;"><b>Untuk Pembayaran Biaya</b>');
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
        $this->pdf->Line(3, $x+2, 205, $x+2, $style);

		$isi = "";
		$total = 0;
		$totugd = 0;
		$totalobat = 0;
		$totdiskonf = 0;
		$totuangr = 0;
		$header = 0;
		$arrnota = $query->result();
		foreach($arrnota AS $j=>$value){
			if($value->koder){
				$totalobat += $value->tottarif;
				$totuangr = $value->uangr;
				$totdiskonf =  $value->diskonr;
				$header = 1;
			}
		}
		
		if($header == 1){
			$x+=5;
			$isi .= "<tr>
				<td width=\"75%\">Obat</td>
				<td width=\"20%\" align=\"right\">". number_format($totalobat,0,',','.') ."</td>
			</tr>";
		}
		
		$totalr = (($total+$totalobat + $totuangr) - $totdiskonf) + $totugd;
		$totaljum = ($totalr);
		$x+=10;
		$html = "<div style=\"letter-spacing:3;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>
			  ". $isi ."
			  </tbody>
				<tr>
					<td width=\"70%\"><p align=\"right\"> Racik </p></td>
					<td width=\"25%\" align=\"right\">". number_format($totuangr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"70%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"25%\" align=\"right\">". number_format($totdiskonf,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"70%\"><p align=\"right\"> Total </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"70%\"><p align=\"right\"> Total Harus Dibayar </p></td>
					<td width=\"25%\" align=\"right\">". number_format($totaljum,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		if($x>200) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=18;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"># '.$this->rp_terbilang($totaljum, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>200) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=10;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Bandung, '.date('d-m-Y').' '.date('H:i:s'), '', 1, 0, true, 'R', true);
		if($x>200) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		
		$x+=8;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Keuangan', '', 1, 0, true, 'C', true);
		$x+=20;$y=140;
		$this->pdf->writeHTMLCell(0, 50, $y, $x, '<div style="letter-spacing:2;">( '. $nota['userinput'] .' )', '', 1, 0, true, 'C', true);

//detail obat
		$this->pdf->AddPage('P', $page_format, false, false);$x = 20;
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
        $this->pdf->Line(3, $x-2, 205, $x-2, $style);
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Detail Obat Farmasi</div>', '', 1, 0, true, 'C', true);
		
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['noreg']);
		
		$x+=0;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['kdsbtnm'] .' '. $pasien);
		
		$x+=0;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['jamnota']);
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">'.$nota['nmbagian']);
		
		$x+=3;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmdokter']);
		
		$x+=0;$y=140;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
        $this->pdf->Line(3, $x+2, 205, $x+2, $style);
		
		$isi = '';
		$total = 0;
		$totalobat = 0;
		$totuangr = 0;
		$totdiskonf = 0;
		$i = 0;
		
		foreach($arrnota AS $j=>$value){
			if($value->koder){
				$x+=6;
				$item = htmlentities($value->nmitem);
				$no = ($i+1);
				
				$totalobat += $value->tottarif;
				$totuangr = $value->uangr;
				$totdiskonf =  $value->diskonr;
				$isi .= "<tr>
					<td width=\"10%\" align=\"center\">". $no ."</td>
					<td width=\"65%\">". $item ." - ". $value->qty ."</td>
					<td width=\"20%\" align=\"right\">". number_format($value->tottarif,0,',','.') ."</td>
				</tr>";
				$i++;
			}
		}
		
		$totalr = $totuangr + $totalobat - $totdiskonf;
		$x+=6;
		$html = "<div style=\"letter-spacing:3;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Racik </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$totuangr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$totdiskonf,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total Harus Dibayar </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$totalr - $reg['diskonr'],0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		if($x>306) $x = $x%290;
		if($x>306) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=17;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"># '.$this->rp_terbilang($totalr - $reg['diskonr'], 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>306) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=25;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">( '. $nota['userinput'] .' )');
		if($x>306) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=0;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Petugas Transaksi');

		
		//Close and output PDF document
		$this->pdf->Output('nota.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
    function nota_ri($nonota, $noreg) {
		$this->pdf->SetPrintHeader(false);
		$this->db->select("
			r.noreg AS noreg,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			bag.nmbagian AS nmbagian,
			sbt.kdsbtnm AS kdsbtnm,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit,
			nt.uangr AS uangr,
			nt.diskon AS diskonr
		");
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
        $this->db->where('r.noreg', $noreg);
		$q = $this->db->get();
		$reg = $q->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
      
        $this->db->select("*");
        $this->db->from("nota");
        $this->db->join("shift",
				"shift.idshift = nota.idshift", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("stpelayanan",
				"stpelayanan.idstpelayanan = nota.idstpelayanan", "left");
		$this->db->where("nonota", $nonota);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('notadet',array('nonota'=>$nonota));
		$notadet = $query->result();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 400, 'ury' => 345),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNotaRI();
		$this->pdf->SetFont('helvetica', 'B', 12);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA TRANSAKSI RI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $pasien);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Status');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 390, $x+2, $style);

		$isi = '';
		$total = 0;
		$header = 0;
		foreach($notadet AS $i=>$val){
			$x+=9;
			
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
			$tarifall = $query->row_array();
			
			$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty - ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
			$total += $tarif;
			
			if($val->koder){
				$item = $tarifall['nmitem'] ." - ". $val->qty ." - ". $tarifall['satuankcl'];
				$no = 'R/'. $val->koder;
				if($header == 1){
					$x+=5;
					$isi .= "<tr>
						<td width=\"100%\" colspan=\"3\"><b>Obat / Alkes</b></td>
					</tr>";
					$header = 2;
				}
			} else {
				$item = htmlentities($tarifall['nmitem']);
				$no = ($i+1);
				if($header == 0){
					$x+=5;
					$isi .= "<tr>
						<td width=\"100%\" colspan=\"3\"><b>Tindakan</b></td>
					</tr>";
					$header = 1;
				}
			}
			
			$isi .= "<tr>
				<td width=\"10%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">". $item ."</td>
				<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
			</tr>";
		}
		
		$totalr = $total + $reg['uangr'];
		$jumtotal = ($totalr - $reg['diskonr']);
		$x+=6;
		$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
			  <tbody>
			  ". $isi ."</tbody>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Racik </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$reg['uangr'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$reg['diskonr'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"><b> Jumlah </b></p></td>
					<td width=\"25%\" align=\"right\"><b>". number_format((int)$jumtotal,0,',','.') ."</b></td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		if($x>330) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=23;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($jumtotal, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>330) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		//$this->pdf->SetFont('helvetica', '', 8);
		$x+=13;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">( '. $nota['userinput'] .' )');
		if($x>330) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=0;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Petugas Transaksi');
		//$x+=5;$y=0;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '[ '.$reg['noreg'].' ]', '', 1, 0, true, 'C', true);


		//Close and output PDF document
		$this->pdf->Output('notari.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function nota_rj($nonota, $noreg) {
		$this->pdf->SetPrintHeader(false);
		$this->pdf->SetPrintFooter(false);
		$this->db->select("
			r.noreg AS noreg,
			p.nmpasien AS nmpasien,
			trim(LEADING '0' FROM p.norm) AS norm,
			sbt.kdsbtnm AS kdsbtnm,
			nt.diskon AS diskonr,
			nt.uangr AS uangr
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $noreg);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
      
        $this->db->select("*");
        $this->db->from("nota");
        $this->db->join("shift",
				"shift.idshift = nota.idshift", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("stpelayanan",
				"stpelayanan.idstpelayanan = nota.idstpelayanan", "left");
		$this->db->where("nonota", $nonota);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('notadet',array('nonota'=>$nonota));
		$notadet = $query->result();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		// add a page 14.8 x 21
		// set page format (read source code documentation for further information)
		// MediaBox - width = urx - llx 210 (mm), height = ury - lly = 297 (mm) this is A4
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 150, 'ury' => 160),
			//'CropBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 297),
			//'BleedBox' => array ('llx' => 5, 'lly' => 5, 'urx' => 205, 'ury' => 292),
			//'TrimBox' => array ('llx' => 10, 'lly' => 10, 'urx' => 200, 'ury' => 287),
			//'ArtBox' => array ('llx' => 15, 'lly' => 15, 'urx' => 195, 'ury' => 282),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA TRANSAKSI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">No. Nota');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
		
		$x+=0;$y=82;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		if(strlen(substr($pasien,0,15)) > 11){
			$this->pdf->SetFont('helvetica', '', 7);
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. substr($pasien,0,15));
		} else {
			$this->pdf->SetFont('helvetica', '', 8);
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. substr($pasien,0,15));
		}
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=0;$y=82;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		// $x+=0;$y=82;
		// $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		// $x+=0;$y+=16;
		// $this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		// $x+=0;$y+=2;
		// $this->pdf->SetFont('helvetica', '', 7);
		// if($nota['idbagian'] == 6) $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmbagian']);
		// else $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		if(strlen($nota['nmdokter']) > 15){
			$this->pdf->SetFont('helvetica', '', 7);
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
		} else {
			$this->pdf->SetFont('helvetica', '', 8);
			$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
		}
		
		// $this->pdf->SetFont('helvetica', '', 8);
		// $x+=0;$y=82;
		// $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Status');
		// $x+=0;$y+=16;
		// $this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		// $x+=0;$y+=2;
		// $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);

		$isi = '';
		$total = 0;
		$totdiskon = 0;
		$header = 0;
		foreach($notadet AS $i=>$val){
			$x+=5;
			
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
			$tarifall = $query->row_array();
			
			$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp)  * $val->qty;
			$total += $tarif;
			$totdiskon += ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp + $val->dijamin);
			
			if($val->koder){
				$item = $tarifall['nmitem'] ." (". $val->qty .") ". $tarifall['satuankcl'];
				$no = $val->koder;
				if($header == 1){
					$x+=5;
					$isi .= "<tr>
						<td width=\"100%\" colspan=\"3\"><b>Obat / Produk</b></td>
					</tr>";
					$header = 2;
				}
			} else {
				$item = $tarifall['nmitem'];
				$no = ($i+1);
				if($header == 0){
					$x+=5;
					$isi .= "<tr>
						<td width=\"100%\" colspan=\"3\"><b>Tindakan</b></td>
					</tr>";
					$header = 1;
				}
			}
			
			$isi .= "<tr>
				<td width=\"10%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">". $item ."</td>
				<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
			</tr>";
		}
		
		$totalr = $total + $reg['uangr'];
		$jumtotal = ($totalr - $reg['diskonr'] - $totdiskon);
		$x+=7;
		$html = "<div style=\"letter-spacing:4;\"><br/><br/><font size=\"8\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>
			  ". $isi ."</tbody>
				
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"25%\" align=\"right\">". number_format(((int)$reg['diskonr']+(int)$totdiskon),0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"><b> Jumlah </b></p></td>
					<td width=\"25%\" align=\"right\"><b>". number_format((int)$jumtotal,0,',','.') ."</b></td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);


		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 0;}
		$x+=11;$y=5;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '<i>* catatan</i>');
		//$x+=0;$y+=0;
		$this->pdf->SetFont('helvetica', '', 8);
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"># '.$this->rp_terbilang($jumtotal, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 0;}
		//$this->pdf->SetFont('helvetica', '', 8);
		$x+=7;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		$x+=15;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">( '. $nota['userinput'] .' )');
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Petugas Transaksi');
		//$x+=5;$y=0;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '[ '.$reg['noreg'].' ]', '', 1, 0, true, 'C', true);


		//Close and output PDF document
		$this->pdf->Output('notarj.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function nota_psh($nonota, $noreg) {
		$this->pdf->SetPrintHeader(false);
		$this->db->select("
			r.noreg AS noreg,
			p.nmpasien AS nmpasien,
			trim(LEADING '0' FROM p.norm) AS norm,
			sbt.kdsbtnm AS kdsbtnm,
			nt.diskon AS diskonr,
			nt.uangr AS uangr
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $noreg);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
      
        $this->db->select("*");
        $this->db->from("nota");
        $this->db->join("shift",
				"shift.idshift = nota.idshift", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("stpelayanan",
				"stpelayanan.idstpelayanan = nota.idstpelayanan", "left");
		$this->db->where("nonota", $nonota);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('notadet',array('nonota'=>$nonota, 'koder'=> NULL));
		$notadet = $query->result();
		
		$query = $this->db->from('notadet')->where(array('koder IS NOT NULL' => NULL, 'nonota'=>$nonota))->order_by('koder', 'ASC')->get();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		//$this->db->getwhere('notadet',array('koder IS NOT NULL' => NULL, 'nonota'=>$nonota));
		$notadetf = $query->result();
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 160, 'ury' => 150),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA TRANSAKSI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">No. Nota');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $pasien);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		if($nota['idbagian'] == 6) $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmbagian']);
		else $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmstpelayanan']);
		
		$x+=4;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);
		$isi = '';
		$total = 0;
		$totdiskon = 0;
		
		if($notadet){
			foreach($notadet AS $i=>$val){
				$x+=5;
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				$tarifall = $query->row_array();
				
				$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty;
				$total += $tarif;
				$totdiskon += ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
				
				$isi .= "<tr>
					<td width=\"10%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"70%\">". $tarifall['nmitem'] ."</td>
					<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
				</tr>";
			}
		}
		$tot = $total;
		$jml = ($total - $totdiskon);
		$x+=0;
		$html = "<div style=\"letter-spacing:4;\"><br/><br/><font size=\"8\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"80%\" align=\"right\"> Total </td>
					<td width=\"20%\" align=\"right\">". number_format($tot,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Diskon </td>
					<td width=\"20%\" align=\"right\">". number_format($totdiskon,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Jumlah </td>
					<td width=\"20%\" align=\"right\">". number_format($jml,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		$x+=24;$y=5;
		/* $this->pdf->writeHTMLCell(0, 10, $y, $x, '<i>* catatan</i>');
		$x+=0;$y+=0; */
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"># '.$this->rp_terbilang($jml, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		//$this->pdf->SetFont('helvetica', '', 8);
		$x+=7;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		$x+=15;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">( '. $nota['userinput'] .' )');
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Petugas Transaksi');
		//$x+=5;$y=0;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '[ '.$reg['noreg'].' ]', '', 1, 0, true, 'C', true);
	
	
	//beda
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA TRANSAKSI RJ</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['noreg']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['kdsbtnm'] .' '. $pasien);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		if($nota['idbagian'] == 6) $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmbagian']);
		else $this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmstpelayanan']);
		
		$x+=4;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);
		$isi = '';
		$total = 0;
		foreach($notadetf AS $i=>$val){
			$x+=5;
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
			$tarifall = $query->row_array();
			
			$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty;
			$total += $tarif;
			
			$isi .= "<tr>
				<td width=\"10%\" align=\"center\">R/". $val->koder ."</td>
				<td width=\"70%\">". $tarifall['nmitem'] ." - ". $val->qty ." - ". $tarifall['satuankcl'] ."</td>
				<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
			</tr>";
		}
		$totalr = $total + $reg['uangr'];
		$tot = ($totalr-$reg['diskonr']);
		$x+=6;
		$html = "<div style=\"letter-spacing:4;\"><br/><br/><font size=\"8\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Racik </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$reg['uangr'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Total </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$reg['diskonr'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Jumlah </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$tot,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		$x+=17;$y=5;
		/* $this->pdf->writeHTMLCell(0, 10, $y, $x, '<i>* catatan</i>');
		$x+=0;$y+=0; */
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"># '.$this->rp_terbilang($tot, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		//$this->pdf->SetFont('helvetica', '', 8);
		$x+=7;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		$x+=15;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">( '. $nota['userinput'] .' )');
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Petugas Transaksi');
		//$x+=5;$y=0;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '[ '.$reg['noreg'].' ]', '', 1, 0, true, 'C', true);

		$this->pdf->Output('nota.pdf', 'I');
	}
	
	function nota_ugd($nonota, $noreg) {
		$this->pdf->SetPrintHeader(false);
		$this->db->select("
			r.noreg AS noreg,
			p.nmpasien AS nmpasien,
			trim(LEADING '0' FROM p.norm) AS norm,
			sbt.kdsbtnm AS kdsbtnm,
			nt.diskon AS diskonr,
			nt.uangr AS uangr
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $noreg);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
      
        $this->db->select("*");
        $this->db->from("nota");
        $this->db->join("shift",
				"shift.idshift = nota.idshift", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("stpelayanan",
				"stpelayanan.idstpelayanan = nota.idstpelayanan", "left");
		$this->db->where("nonota", $nonota);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('notadet',array('nonota'=>$nonota));
		$notadet = $query->result();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 150, 'ury' => 160),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA TRANSAKSI UGD</div>', '', 1, 0, true, 'C', true);
		
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">No. Nota');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['noreg']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['kdsbtnm'] .' '. substr($pasien,0,15));
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['jamnota']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">'.$nota['nmbagian']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmdokter']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);

		$isi = '';
		$total = 0;
		$totdiskon = 0;
		$header = 0;
		foreach($notadet AS $i=>$val){
			$x+=5;
			
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
			$tarifall = $query->row_array();
			
			$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty;
			$total += $tarif;
			$totdiskon += ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
			
			$itemobt = $tarifall['nmitem'] ." - ". $val->qty ." - ". $tarifall['satuankcl'];
			if(strlen($itemobt)>42)$x+=5;
			
			if($val->koder){
				$item = $itemobt;
				$no = 'R/ '. $val->koder;
				if($header == 1){
					$x+=5;
					$isi .= "<tr>
						<td width=\"100%\" colspan=\"3\">Obat / Alkes</td>
					</tr>";
					$header = 2;
				}
			} else {
				$item = $tarifall['nmitem'];
				$no = ($i+1);
				if($header == 0){
					$x+=5;
					$isi .= "<tr>
						<td width=\"100%\" colspan=\"3\">Tindakan</td>
					</tr>";
					$header = 1;
				}
			}
			
			$isi .= "<tr>
				<td width=\"10%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">". $item ."</td>
				<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
			</tr>";
		}
		
		$totalr = $total + $reg['uangr'];
		$jumtotal = ($totalr - $reg['diskonr'] - $totdiskon);
		$x+=6;
		$html = "<div style=\"letter-spacing:3;\"><br/><br/><font size=\"8\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>
			  ". $isi ."</tbody>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Racik </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$reg['uangr'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Total </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"25%\" align=\"right\">". number_format(((int)$reg['diskonr']+(int)$totdiskon),0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"75%\"><p align=\"right\"> Jumlah </p></td>
					<td width=\"25%\" align=\"right\">". number_format((int)$jumtotal,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 0;}
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=13;$y=5;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '<i>* catatan</i>');
		//$x+=0;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"># '.$this->rp_terbilang($jumtotal, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 0;}
		$x+=13;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">( '. $nota['userinput'] .' )');
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=0;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Petugas Transaksi');
		//$x+=5;$y=0;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '[ '.$reg['noreg'].' ]', '', 1, 0, true, 'C', true);


		//Close and output PDF document
		$this->pdf->Output('notarj.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function nota_pshugd($nonota, $noreg) {
		$this->pdf->SetPrintHeader(false);
		$this->db->select("
			r.noreg AS noreg,
			p.nmpasien AS nmpasien,
			trim(LEADING '0' FROM p.norm) AS norm,
			sbt.kdsbtnm AS kdsbtnm,
			nt.diskon AS diskonr,
			nt.uangr AS uangr
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $noreg);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
		$arrpasien = explode(' ',$reg['nmpasien']);
		if(count($arrpasien)>1) $pasien = $arrpasien[0] .' '. $arrpasien[1];
		else $pasien = $arrpasien[0];
      
        $this->db->select("*");
        $this->db->from("nota");
        $this->db->join("shift",
				"shift.idshift = nota.idshift", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("stpelayanan",
				"stpelayanan.idstpelayanan = nota.idstpelayanan", "left");
		$this->db->where("nonota", $nonota);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('notadet',array('nonota'=>$nonota, 'koder'=> NULL));
		$notadet = $query->result();
		
		$query = $this->db->from('notadet')->where(array('koder IS NOT NULL' => NULL, 'nonota'=>$nonota))->order_by('koder', 'ASC')->get();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		//$this->db->getwhere('notadet',array('koder IS NOT NULL' => NULL, 'nonota'=>$nonota));
		$notadetf = $query->result();
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 160, 'ury' => 150),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA TRANSAKSI UGD</div>', '', 1, 0, true, 'C', true);
		
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">No. Nota');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['noreg']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['kdsbtnm'] .' '. $pasien);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['jamnota']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">'.$nota['nmbagian']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmdokter']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmstpelayanan']);
		
		$x+=4;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);
		$isi = '';
		$total = 0;
		$totdiskon = 0;	
		
		if($notadet){
			foreach($notadet AS $i=>$val){
				$x+=5;
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				$tarifall = $query->row_array();
				
				$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty;
				$total += $tarif;
				$totdiskon += ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
				
				$isi .= "<tr>
					<td width=\"10%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"70%\">". $tarifall['nmitem'] ."</td>
					<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
				</tr>";
			}
		}
		$tot = $total;
		$jml = ($total - $totdiskon);
		$x+=0;
		$html = "<div style=\"letter-spacing:3;\"><br/><br/><font size=\"8\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			   <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"80%\" align=\"right\"> Total </td>
					<td width=\"20%\" align=\"right\">". number_format($tot,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Diskon </td>
					<td width=\"20%\" align=\"right\">". number_format($totdiskon,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Jumlah </td>
					<td width=\"20%\" align=\"right\">". number_format($jml,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=24;$y=5;
		/* $this->pdf->writeHTMLCell(0, 10, $y, $x, '<i>* catatan</i>');
		$x+=0;$y+=0; */
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"># '.$this->rp_terbilang($jml, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		//$this->pdf->SetFont('helvetica', '', 8);
		$x+=7;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		$x+=15;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">( '. $nota['userinput'] .' )');
		if($x>120) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Petugas Transaksi');
		//$x+=5;$y=0;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '[ '.$reg['noreg'].' ]', '', 1, 0, true, 'C', true);
	
	
	//beda
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA TRANSAKSI UGD</div>', '', 1, 0, true, 'C', true);
		
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['noreg']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$reg['kdsbtnm'] .' '. $pasien);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['jamnota']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$reg['norm']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">'.$nota['nmbagian']);
		
		$x+=4;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=26;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmdokter']);
		
		$x+=0;$y=90;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmstpelayanan']);
		
		$x+=4;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);
		$isi = '';
		$total = 0;
		foreach($notadetf AS $i=>$val){
			$x+=5;
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
			$tarifall = $query->row_array();
			
			$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty;
			$total += $tarif;
			
			$itemobt = $tarifall['nmitem'] ." - ". $val->qty ." - ". $tarifall['satuankcl'];
			if(strlen($itemobt)>42)$x+=5;
			$isi .= "<tr>
				<td width=\"10%\" align=\"center\">R/". $val->koder ."</td>
				<td width=\"70%\">". $itemobt ."</td>
				<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
			</tr>";
		}
		$totalr = $total + $reg['uangr'];
		$tot = ($totalr-$reg['diskonr']);
		$x+=6;
		$html = "<div style=\"letter-spacing:3;\"><br/><br/><font size=\"8\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Racik </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$reg['uangr'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Total </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$totalr,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Diskon </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$reg['diskonr'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\"><p align=\"right\"> Jumlah </p></td>
					<td width=\"20%\" align=\"right\">". number_format((int)$tot,0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=17;$y=5;
		/* $this->pdf->writeHTMLCell(0, 10, $y, $x, '<i>* catatan</i>');
		$x+=0;$y+=0; */
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"># '.$this->rp_terbilang($tot, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=13;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">( '. $nota['userinput'] .' )');
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=0;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:2;">Petugas Transaksi');
		//$x+=5;$y=0;
		//$this->pdf->writeHTMLCell(0, 10, $y, $x, '[ '.$reg['noreg'].' ]', '', 1, 0, true, 'C', true);

		$this->pdf->Output('nota.pdf', 'I');
	}
	
	function nota_farmasipl($nonota) {
		$this->pdf->SetPrintHeader(false);
      
        $this->db->select("*");
        $this->db->from("nota");
        $this->db->join("shift",
				"shift.idshift = nota.idshift", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("stpelayanan",
				"stpelayanan.idstpelayanan = nota.idstpelayanan", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
		$this->db->where("nonota", $nonota);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->orderby('koder','asc')->getwhere('notadet',array('nonota'=>$nonota));
		$notadet = $query->result();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		//$this->db->getwhere('notadet',array('koder IS NOT NULL' => NULL, 'nonota'=>$nonota));
		$notadetf = $query->result();
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 160, 'ury' => 150),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA FARMASI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=27;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nonota']);
		
		$x+=0;$y=85;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=27;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['atasnama']);
		
		$x+=0;$y=85;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=27;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '-');
		
		$x+=0;$y=85;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmbagian']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=27;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
		
		$x+=0;$y=85;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);
		$isi = '';
		$total = 0;
		
		if($notadet){
			foreach($notadet AS $i=>$val){
				$x+=5;
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				$tarifall = $query->row_array();
				
				$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty;
				$total += $tarif;
				
				$isi .= "<tr>
					<td width=\"5%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"10%\" align=\"center\">R/". $val->koder ."</td>
					<td width=\"45%\">". $tarifall['nmitem'] ."</td>
					<td width=\"20%\">". number_format($val->qty,0,',','.')."</td>
					<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
				</tr>";
			}
		}
		
		$jmlh = $nota['uangr'] + $total - $nota['diskon'];
		
		$x+=6;
		$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"8\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"80%\" align=\"right\"> Racik </td>
					<td width=\"20%\" align=\"right\">". number_format($nota['uangr'],0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Total </td>
					<td width=\"20%\" align=\"right\">". number_format($total,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Diskon </td>
					<td width=\"20%\" align=\"right\">". number_format($nota['diskon'],1,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Jumlah </td>
					<td width=\"20%\" align=\"right\">". number_format($jmlh,1,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		$x+=20;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"># '.$this->rp_terbilang($jmlh, 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=8;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">( '. $nota['userinput'] .' )');
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Petugas Transaksi');
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"><i>*Obat yang telah dibeli tidak bisa di tukar / retur</i>');

		$this->pdf->Output('notaFPL.pdf', 'I');
	}
	
	function nota_ptambahan($nonota) {
		$this->pdf->SetPrintHeader(false);
      
        $this->db->select("*");
        $this->db->from("nota");
        $this->db->join("shift",
				"shift.idshift = nota.idshift", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("stpelayanan",
				"stpelayanan.idstpelayanan = nota.idstpelayanan", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
		$this->db->where("nonota", $nonota);
        $query = $this->db->get();
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('notadet',array('nonota'=>$nonota));
		$notadet = $query->result();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		//$this->db->getwhere('notadet',array('koder IS NOT NULL' => NULL, 'nonota'=>$nonota));
		$notadetf = $query->result();
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 160, 'ury' => 150),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('P', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		$this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNota();
		$this->pdf->SetFont('helvetica', '', 10);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">NOTA PELAYANAN TAMBAHAN</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 8);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=27;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nonota']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl.');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($nota['tglnota']), 'd-m-Y'));
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=27;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['atasnama']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Jam');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['jamnota']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=27;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '-');
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Unit');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:3;">'.$nota['nmbagian']);
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Dokter');
		$x+=0;$y+=27;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$nota['nmdokter']);
		
		$x+=0;$y=75;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Status');
		$x+=0;$y+=16;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">'.$nota['nmstpelayanan']);
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');
		

        $this->pdf->Line(3, $x+2, 145, $x+2, $style);
		$isi = '';
		$total = 0;
		$totdiskon = 0;
		
		if($notadet){
			foreach($notadet AS $i=>$val){
				$x+=4;
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val->kditem));
				$tarifall = $query->row_array();
				
				$tarif = ($val->tarifjs + $val->tarifjm + $val->tarifjp + $val->tarifbhp) * $val->qty;
				$total += $tarif;
				$totdiskon += ($val->diskonjs + $val->diskonjm + $val->diskonjp + $val->diskonbhp);
				
				$isi .= "<tr>
					<td width=\"10%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"70%\">". $tarifall['nmitem'] ."</td>
					<td width=\"20%\" align=\"right\">". number_format($tarif,0,',','.') ."</td>
				</tr>";
			}
		}
		
		$x+=6;
		$html = "<div style=\"letter-spacing:4;\"><br/><br/><font size=\"8\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\">
			  <tbody>". $isi ."</tbody>
				<tr>
					<td width=\"80%\" align=\"right\"> Total </td>
					<td width=\"20%\" align=\"right\">". number_format($total,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Diskon </td>
					<td width=\"20%\" align=\"right\">". number_format($totdiskon,0,',','.') ."</td>
				</tr>
				<tr>
					<td width=\"80%\" align=\"right\"> Jumlah </td>
					<td width=\"20%\" align=\"right\">". number_format(($total-$totdiskon),0,',','.') ."</td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		$x+=17;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"># '.$this->rp_terbilang(($total-$totdiskon), 0).' Rupiah #', '', 1, 0, true, 'R', true);
		
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		$x+=13;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;"><i>Tgl. & Jam Cetak : '.date('d-m-Y').' & '.date('H:i:s').'</i>', '', 1, 0, true, 'R', true);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">( '. $nota['userinput'] .' )');
		if($x>125) {$this->pdf->AddPage('P', $page_format, false, false);$x = 20;}
		
		$x+=5;$y=5;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">Petugas Transaksi');

		$this->pdf->Output('notaFPL.pdf', 'I');
	}
	
	function kuitansi($nonota, $noreg) {
		$tgl = $this->namaBulan(date('Y-m-d'));
		$this->pdf->SetPrintHeader(false);
		$this->pdf->SetPrintFooter(false);
		/* $query = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg));
		$reg = $query->row_array(); */
		$query = $this->db->getwhere('nota',array('nonota'=>$nonota));
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('kuitansi',array('nokuitansi'=>$nota['nokuitansi']));
		$kui = $query->row_array();
		if(!isset($kui['pembayaran']))$kui['pembayaran']=$kui['total'];
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 140, 'ury' => 340),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 00,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT, PDF_MARGIN_BOTTOM-5);
		$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-15);
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		/* 
		$this->pdf->Rotate(90);
		$x=5;$y=-120;
		$this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->writeHTMLCell(140, 10, $y, $x, '<div style="letter-spacing:5;">RSIA HARAPAN BUNDA</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->writeHTMLCell(140, 10, $y, $x, '<div style="letter-spacing:5;">dr. Bambang Suhardijanto, SpOg</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(140, 10, $y, $x, '<div style="letter-spacing:3;">Pluto Raya Blok C Margahayu Raya Bandung</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(140, 10, $y, $x, '<div style="letter-spacing:3;">Telp. (022) 7506490 Fax (022) 7514712</div>', '', 1, 0, true, 'C', true);
		$this->pdf->StopTransform(); */

		//$this->pdf->StartTransform();
		//$this->pdf->Rotate(90);

		$x=5;$y=0;
		$this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">'.NMKLINIK.'</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">'.ALAMAT_SATU.' </div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:3;">'.ALAMAT_DUA.'</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:3;"></div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->Line(3, $x+6, 337, $x+6, $style);
		$x+=7;
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">KUITANSI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=10;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">No. : <b>'. $noreg .'</b>', '', 1, 0, true, 'L', true);
		
		$x+=9;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Telah terima dari', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"><b>'.$kui['atasnama'].'</b>', '', 1, 0, true, 'L', true);
		
		$x+=13;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Uang sejumlah', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($kui['pembayaran'], 0).' Rupiah #', '', 1, 0, true, 'L', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=15;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Untuk pembayaran', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;">'.$nota['catatan'], '', 1, 0, true, 'L', true);

		// $x+=15;$y=10;
		// $this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Diagnosa : ..............................................', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;">', '', 1, 0, true, 'L', true);
		
		
		$x+=10;$y=220;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">'.TEMPAT.', '. $tgl, '', 1, 0, true, 'C', true);
		
		
		$x+=25;$y=220;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '.............................................', '', 1, 0, true, 'C', true);
		
		$x-=7;
        $this->pdf->Line(10, $x, 70, $x, $style);
		$x+=5;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:4;">Rp. '.number_format($kui['pembayaran'], 0, '.',','), '', 1, 0, true, 'L', true);
		$x+=10;
		$this->pdf->Line(10, $x, 70, $x, $style);
		//Close and output PDF document
		$this->pdf->Output('kuitansi.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	function kuitansipenj($nonota, $noreg) {
		$tgl = $this->namaBulan(date('Y-m-d'));
		$this->pdf->SetPrintHeader(false);
		$query = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg));
		$reg = $query->row_array();
		$query = $this->db->getwhere('nota',array('nonota'=>$nonota));
		$nota = $query->row_array();
		
		$query = $this->db->getwhere('kuitansidet',array('nokuitansi'=>$nota['nokuitansi'], 'idcarabayar'=>8));
		$kuidet = $query->row_array();
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 140, 'ury' => 340),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 00,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT, PDF_MARGIN_BOTTOM-5);
		$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-15);
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		$x=5;$y=0;
		$this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">RSIA HARAPAN BUNDA</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">dr. Bambang Suhardijanto, SpOg</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:3;">Pluto Raya Blok C Margahayu Raya Bandung</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:3;">Telp. (022) 7506490 Fax (022) 7514712</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->Line(3, $x+6, 337, $x+6, $style);
		$x+=7;
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">KUITANSI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=10;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">No. : <b>'. $reg['noreg'] .'</b>', '', 1, 0, true, 'L', true);
		
		$x+=9;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Telah terima dari', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"><b>'. $reg['nmpenjamin'] .'</b>', '', 1, 0, true, 'L', true);
		
		$x+=13;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Uang sejumlah', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($kuidet['jumlah'], 0).' Rupiah #', '', 1, 0, true, 'L', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=15;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Untuk pembayaran', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;">'.$nota['catatan'], '', 1, 0, true, 'L', true);
		
		$x+=20;$y=220;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Bandung, '. $tgl, '', 1, 0, true, 'C', true);
		
		
		$x+=25;$y=220;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">'. $this->session->userdata['user_id'], '', 1, 0, true, 'C', true);
		
		$x-=7;
        $this->pdf->Line(10, $x, 70, $x, $style);
		$x+=5;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:4;">Rp. '.number_format($kuidet['jumlah'], 0, '.',','), '', 1, 0, true, 'L', true);
		$x+=10;
		$this->pdf->Line(10, $x, 70, $x, $style);
		//Close and output PDF document
		$this->pdf->Output('kuitansipenjamin.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	function deposit($nokui) {
		$tgl = $this->namaBulan(date('Y-m-d'));
		$this->pdf->SetPrintHeader(false);
		$query = $this->db->getwhere('kuitansi',array('nokuitansi'=>$nokui));
		$kui = $query->row_array();
		
		$this->db->select("
			rd.noreg AS noreg,
			bed.nmbed AS nmbed
		", false);
		
        $this->db->from('registrasidet rd');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('rd.idregdet', $kui['idregdet']);
		$q = $this->db->get();
		$regdet = $q->row_array();
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 140, 'ury' => 340),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT, PDF_MARGIN_BOTTOM-5);
		$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-15);
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		$x=5;$y=0;
		$this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">RSIA HARAPAN BUNDA</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">dr. Bambang Suhardijanto, SpOg</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:3;">Pluto Raya Blok C Margahayu Raya Bandung</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:3;">Telp. (022) 7506490 Fax (022) 7514712</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->Line(3, $x+6, 337, $x+6, $style);
		$x+=7;
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">KUITANSI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=10;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">No. Reg / Kuitansi: <b>'. $regdet['noreg'] .' / '.$nokui.'<b>', '', 1, 0, true, 'L', true);
		
		$x+=9;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Telah terima dari', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"><b>'.$kui['atasnama'] .'</b>', '', 1, 0, true, 'L', true);
		
		$x+=13;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Uang sejumlah', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($kui['total'], 0).' Rupiah #', '', 1, 0, true, 'L', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=15;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Untuk pembayaran', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"> Deposit Rawat Inap Ruang '. $regdet['nmbed'], '', 1, 0, true, 'L', true);
		
		$x+=20;$y=220;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Bandung, '. $tgl, '', 1, 0, true, 'C', true);
		
		
		$x+=25;$y=220;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">'. $this->session->userdata['user_id'], '', 1, 0, true, 'C', true);
		
		$x-=7;
        $this->pdf->Line(10, $x, 70, $x, $style);
		$x+=5;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:4;">Rp. '.number_format($kui['total'], 0, '.',','), '', 1, 0, true, 'L', true);
		$x+=10;
		$this->pdf->Line(10, $x, 70, $x, $style);
		
		//Close and output PDF document
		$this->pdf->Output('deposit.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function ret_deposit($kdjurnal) {	
		$tgl = $this->namaBulan(date('Y-m-d'));
		$this->pdf->SetPrintHeader(false);
		$query = $this->db->getwhere('v_returdeposit',array('kdjurnal'=>$kdjurnal));
		$jurnal = $query->row_array();
		
		$total = 0;
		$total = ($jurnal['totdeposit'] - $jurnal['tottransaksi']);
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 140, 'ury' => 340),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT, PDF_MARGIN_BOTTOM-5);
		$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-15);
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		
		$x=5;$y=0;
		$this->pdf->SetFont('helvetica', 'B', 16);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">RSIA HARAPAN BUNDA</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:5;">dr. Bambang Suhardijanto, SpOg</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:3;">Pluto Raya Blok C Margahayu Raya Bandung</div>', '', 1, 0, true, 'C', true);
		
		$x+=6;
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->writeHTMLCell(350, 10, $y, $x, '<div style="letter-spacing:3;">Telp. (022) 7506490 Fax (022) 7514712</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=10;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">No. : <b>'. $jurnal['kdjurnal'] .'<b>', '', 1, 0, true, 'L', true);
		
		$x+=9;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Telah terima dari', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"><b>'.$jurnal['penerima'] .'</b>', '', 1, 0, true, 'L', true);
		
		$x+=9;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Uang sejumlah', '', 1, 0, true, 'L', true);
		$y=75;
		$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"># '.$this->rp_terbilang($total, 0).' Rupiah #', '', 1, 0, true, 'L', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=9;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Retur Deposit Rawat Inap', '', 1, 0, true, 'L', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=9;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Atas Nama / Pasien : '.$jurnal['nmpasien'], '', 1, 0, true, 'L', true);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$x+=9;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">No. Registrasi : '.$jurnal['noreg']. '&nbsp;&nbsp; Ruangan : '.$jurnal['nmbagian'], '', 1, 0, true, 'L', true);
		//$y=75;
		//$this->pdf->writeHTMLCell(260, 0, $y, $x, '<div style="letter-spacing:5;"> Deposit Rawat Inap', '', 1, 0, true, 'L', true);
		
		$x+=17;$y=215;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">Bandung '. $tgl, '', 1, 0, true, 'L', true);
		
		
		$x+=22;$y=230;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:5;">'. $this->session->userdata['user_id'], '', 1, 0, true, 'C', true);
		
		$x+=-2;
        $this->pdf->Line(10, $x, 70, $x, $style);
		$x+=5;$y=10;
		$this->pdf->writeHTMLCell(0, 0, $y, $x, '<div style="letter-spacing:4;">Rp. '.number_format($total, 0, '.',','), '', 1, 0, true, 'L', true);
		$x+=10;
		$this->pdf->Line(10, $x, 70, $x, $style);
		
		//Close and output PDF document
		$this->pdf->Output('retdeposit.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}
	
	function excellappelayanan($nona, $noreg) {
		$this->db->select("
			r.noreg AS noreg,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			bag.nmbagian AS nmbagian,
			rd.tglreg AS tglreg,
			rd.tglkeluar AS tglkeluar
		");
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
        $this->db->where('r.noreg', $noreg);
		$q = $this->db->get();
		$reg = $q->row_array();
	
		$header = array(
			'Obat-obatan',
			'Satuan',
			'Harga',
			'Jumlah',
			'Total'
		);
		
		$this->db->select("
			bp.nmbrg,
			js.nmsatuan,
			(ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp) AS harga,
			(SUM(ntdet.qty) - if(rfdet.qty, rfdet.qty, 0 )) AS qty,
			(SUM(ntdet.tarifjs*ntdet.qty) + SUM(ntdet.tarifjm*ntdet.qty) + SUM(ntdet.tarifjp*ntdet.qty) + SUM(ntdet.tarifbhp*ntdet.qty)) - if(( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet )>0, SUM((ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp)* ( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet ) ), 0) AS total
		", false);
        $this->db->from('nota nt');
		$this->db->join('registrasidet rd',
                'rd.idregdet = nt.idregdet', 'left');
		$this->db->join('registrasi r',
                'r.noreg = rd.noreg', 'left');
		$this->db->join('notadet ntdet',
                'ntdet.nonota = nt.nonota', 'left');
		$this->db->join('returfarmasidet rfdet',
                'rfdet.idnotadet = ntdet.idnotadet', 'left');
		$this->db->join('barang bp',
                'bp.kdbrg = ntdet.kditem', 'left');
		$this->db->join('jsatuan js',
                'js.idsatuan = ntdet.idsatuan', 'left');
		$this->db->groupby("nt.nona, ntdet.kditem");
		$this->db->orderby("r.noreg");
		$this->db->where("nt.nona", $nona);
		$this->db->like("ntdet.kditem", 'B');
		$query = $this->db->get();
		$fpl = $query->result();
		
		$total = 0;
		foreach ($fpl as $key=>$row) {
			$total += $row->total;
		}
		
		$data['nama'] = $reg['nmpasien'];
		$data['norm'] = $reg['norm'];
		$data['noreg'] = $reg['noreg'];
		$data['tglmasuk'] = $reg['tglreg'];
		$data['tglkeluar'] = $reg['tglkeluar'];
		$data['ruangan'] = $reg['nmbagian'];
		$data['fieldname'] = $header;
		$data['eksport'] = $fpl;
		$data['total'] = $total;
		$this->load->view('exportexceltf', $data); 	
	}
	
	function excelnotari($nona, $noreg) {
		$this->db->select("
			r.noreg AS noreg,
			rd.tglreg AS tglreg,
			rd.tglkeluar AS tglkeluar,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			bag.nmbagian AS nmbagian,
			sbt.kdsbtnm AS kdsbtnm,
			TIMESTAMPDIFF(SECOND,CONCAT(rd.tglreg,' ',rd.jamreg),CONCAT(rd.tglkeluar,' ',rd.jamkeluar)) AS lamainap,
			sp.nmstpelayanan AS nmstpelayanan,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit
		", false);
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
        $this->db->where('r.noreg', $noreg);
		$q = $this->db->get();
		$reg = $q->row_array();
      
        $this->db->select("
			bp.nmitem AS nmitem,
			d.nmdoktergelar AS nmdoktergelar,
			(SUM(ntdet.qty) - if(rfdet.qty, rfdet.qty, 0 )) AS qty,
			(ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp) AS tarif,
			CEIL((SUM(ntdet.tarifjs*ntdet.qty) + SUM(ntdet.tarifjm*ntdet.qty) + SUM(ntdet.tarifjp*ntdet.qty) + SUM(ntdet.tarifbhp*ntdet.qty)) - if(( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet )>0, SUM((ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp)* ( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet ) ), 0)) AS tottarif
		", false);
        $this->db->from('nota nt');
		$this->db->join('registrasidet rd',
                'rd.idregdet = nt.idregdet', 'left');
		$this->db->join('registrasi r',
                'r.noreg = rd.noreg', 'left');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('notaakhir nta',
                'nta.nona = nt.nona', 'left');
		$this->db->join('notadet ntdet',
                'ntdet.nonota = nt.nonota', 'left');
		$this->db->join('returfarmasidet rfdet',
                'rfdet.idnotadet = ntdet.idnotadet', 'left');
		$this->db->join('v_brgpel bp',
                'bp.kditem = ntdet.kditem', 'left');
		$this->db->join('dokter d',
                'd.iddokter = ntdet.iddokter', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->groupby("nt.nona, ntdet.kditem");
		$this->db->orderby("r.noreg");
		$this->db->where("nt.nona", $nona);
		$this->db->where("ntdet.koder IS NULL", null, false);
        $query = $this->db->get();
		$arrnota = $query->result();
    
        $this->db->select("
			bp.nmitem AS nmitem,
			(SUM(ntdet.qty) - if(rfdet.qty, rfdet.qty, 0 )) AS qty,
			(ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp) AS tarif,
			CEIL((SUM(ntdet.tarifjs*ntdet.qty) + SUM(ntdet.tarifjm*ntdet.qty) + SUM(ntdet.tarifjp*ntdet.qty) + SUM(ntdet.tarifbhp*ntdet.qty)) - if(( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet )>0, SUM((ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp)* ( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet ) ), 0)) AS tottarif
		", false);
        $this->db->from('nota nt');
		$this->db->join('registrasidet rd',
                'rd.idregdet = nt.idregdet', 'left');
		$this->db->join('registrasi r',
                'r.noreg = rd.noreg', 'left');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('notaakhir nta',
                'nta.nona = nt.nona', 'left');
		$this->db->join('notadet ntdet',
                'ntdet.nonota = nt.nonota', 'left');
		$this->db->join('returfarmasidet rfdet',
                'rfdet.idnotadet = ntdet.idnotadet', 'left');
		$this->db->join('v_brgpel bp',
                'bp.kditem = ntdet.kditem', 'left');
		$this->db->join('dokter d',
                'd.iddokter = ntdet.iddokter', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->groupby("nt.nona, ntdet.kditem");
		$this->db->orderby("r.noreg");
		$this->db->where("nt.nona", $nona);
		$this->db->where("ntdet.koder IS NOT NULL", null, false);
        $query = $this->db->get();
		$arrnota1 = $query->result();
    
        $this->db->select("
			nt.uangr AS uangr,
			SUM(ntdet.diskonjs+ntdet.diskonjm+ntdet.diskonjp+ntdet.diskonbhp)+nt.diskon AS diskon,
			CEIL((SUM(ntdet.tarifjs*ntdet.qty) + SUM(ntdet.tarifjm*ntdet.qty) + SUM(ntdet.tarifjp*ntdet.qty) + SUM(ntdet.tarifbhp*ntdet.qty)) - if(( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet )>0, SUM((ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp)* ( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet ) ), 0)) AS tottarif
		", false);
        $this->db->from('nota nt');
		$this->db->join('notaakhir nta',
                'nta.nona = nt.nona', 'left');
		$this->db->join('notadet ntdet',
                'ntdet.nonota = nt.nonota', 'left');
		$this->db->join('returfarmasidet rfdet',
                'rfdet.idnotadet = ntdet.idnotadet', 'left');
		$this->db->where("nt.nona", $nona);
        $query = $this->db->get();
		$jumtot = $query->row_array();
    
        $this->db->select("
			CEIL((SUM(ntdet.tarifjs*ntdet.qty) + SUM(ntdet.tarifjm*ntdet.qty) + SUM(ntdet.tarifjp*ntdet.qty) + SUM(ntdet.tarifbhp*ntdet.qty)) - if(( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet )>0, SUM((ntdet.tarifjs+ntdet.tarifjm+ntdet.tarifjp+ntdet.tarifbhp)* ( select if(rfd.qty, rfd.qty, 0) from returfarmasidet rfd where ntdet.idnotadet = rfd.idnotadet ) ), 0)) AS jumobat
		", false);
        $this->db->from('nota nt');
		$this->db->join('notaakhir nta',
                'nta.nona = nt.nona', 'left');
		$this->db->join('notadet ntdet',
                'ntdet.nonota = nt.nonota', 'left');
		$this->db->join('returfarmasidet rfdet',
                'rfdet.idnotadet = ntdet.idnotadet', 'left');
		$this->db->where("nt.nona", $nona);
		$this->db->where("ntdet.koder IS NOT NULL", null, false);
        $qobat = $this->db->get();
		$jumobat = $qobat->row_array();
      
        /* $this->db->select("total");
        $this->db->from("nota");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
		$this->db->where("idregdettransfer", $noreg);
        $queryugd = $this->db->get();
		$totalugd = $queryugd->row_array(); */
	
		$header = array(
			'No',
			'Nama Tindakan',
			'Dokter',
			'Qty',
			'Harga',
			'Sub Total'
		);
		
		$header1 = array(
			'No',
			'Nama Item',
			'Qty',
			'Harga',
			'Sub Total'
		);
		
		$jumlah = $jumtot['uangr']+$jumtot['tottarif'];
		$total = $jumlah-$jumtot['diskon'];
		$bayar = $total-$reg['deposit'];
		$sisadp = 0;
		if($bayar < 1){
			$bayar = 0;
			$sisadp = $reg['deposit'] - $total;
		}
		
		$data['nama'] = $reg['kdsbtnm'] .' '. $reg['nmpasien'];
		$data['norm'] = $reg['norm'];
		$data['noreg'] = $reg['noreg'];
		$data['tglreg'] = $reg['tglreg'];
		$data['tglkeluar'] = $reg['tglkeluar'];
		$data['unit'] = $reg['nmbagian'];
		$data['lamainap'] = CEIL($reg['lamainap']/86400). ' Hari';
		$data['status'] = $reg['nmstpelayanan'];
		$data['uangr'] = $jumtot['uangr'];
		$data['jumlah'] = $jumlah;
		$data['diskon'] = $jumtot['diskon'];
		$data['total'] = $total;
		$data['deposit'] = $reg['deposit'];
		$data['bayar'] = $bayar;
		$data['sisadp'] = $sisadp;
		$data['jumobat'] = $jumobat['jumobat'];
		$data['fieldname'] = $header;
		$data['fieldname1'] = $header1;
		$data['eksport'] = $arrnota;
		$data['eksport1'] = $arrnota1;
		//$data['totalugd'] = $totalugd['total'];
		$this->load->view('excelnotari', $data); 	
	}
	
	function konversi_tglindo($tanggal){ 
		$format = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu',
			'Jan' => 'Januari',
			'Feb' => 'Februari',
			'Mar' => 'Maret',
			'Apr' => 'April',
			'May' => 'Mei',
			'Jun' => 'Juni',
			'Jul' => 'Juli',
			'Aug' => 'Agustus',
			'Sep' => 'September',
			'Oct' => 'Oktober',
			'Nov' => 'November',
			'Dec' => 'Desember'
		);	 
		return strtr($tanggal, $format);
	}
	
	function rp_satuan($angka,$debug){
		$terbilang = '';
		$a_str['1']="Satu";
		$a_str['2']="Dua";
		$a_str['3']="Tiga";
		$a_str['4']="Empat";
		$a_str['5']="Lima";
		$a_str['6']="Enam";
		$a_str['7']="Tujuh";
		$a_str['8']="Delapan";
		$a_str['9']="Sembilan";
		
		
		$panjang=strlen($angka);
		for ($b=0;$b<$panjang;$b++)
		{
			$a_bil[$b]=substr($angka,$panjang-$b-1,1);
		}
		
		if ($panjang>2)
		{
			if ($a_bil[2]=="1")
			{
				$terbilang=" Seratus";
			}
			else if ($a_bil[2]!="0")
			{
				$terbilang= " ".$a_str[$a_bil[2]]. " Ratus";
			}
		}

		if ($panjang>1)
		{
			if ($a_bil[1]=="1")
			{
				if ($a_bil[0]=="0")
				{
					$terbilang .=" Sepuluh";
				}
				else if ($a_bil[0]=="1")
				{
					$terbilang .=" Sebelas";
				}
				else 
				{
					$terbilang .=" ".$a_str[$a_bil[0]]." Belas";
				}
				return $terbilang;
			}
			else if ($a_bil[1]!="0")
			{
				$terbilang .=" ".$a_str[$a_bil[1]]." Puluh";
			}
		}
		
		if ($a_bil[0]!="0")
		{
			$terbilang .=" ".$a_str[$a_bil[0]];
		}
		return $terbilang;
	   
	}
	
	function rp_terbilang($angka,$debug){
		$terbilang = '';
		
		$angka = str_replace(".",",",$angka);
		
		list ($angka) = explode(",",$angka);
		$panjang=strlen($angka);
		for ($b=0;$b<$panjang;$b++)
		{
			$myindex=$panjang-$b-1;
			$a_bil[$b]=substr($angka,$myindex,1);
		}
		if ($panjang>9)
		{
			$bil=$a_bil[9];
			if ($panjang>10)
			{
				$bil=$a_bil[10].$bil;
			}

			if ($panjang>11)
			{
				$bil=$a_bil[11].$bil;
			}
			if ($bil!="" && $bil!="000")
			{
				$terbilang .= $this->rp_satuan($bil,$debug)." milyar";
			}
			
		}

		if ($panjang>6)
		{
			$bil=$a_bil[6];
			if ($panjang>7)
			{
				$bil=$a_bil[7].$bil;
			}

			if ($panjang>8)
			{
				$bil=$a_bil[8].$bil;
			}
			if ($bil!="" && $bil!="000")
			{
				$terbilang .= $this->rp_satuan($bil,$debug)." Juta";
			}
			
		}
		
		if ($panjang>3)
		{
			$bil=$a_bil[3];
			if ($panjang>4)
			{
				$bil=$a_bil[4].$bil;
			}

			if ($panjang>5)
			{
				$bil=$a_bil[5].$bil;
			}
			if ($bil!="" && $bil!="000")
			{
				$terbilang .= $this->rp_satuan($bil,$debug)." Ribu";
			}
			
		}

		$bil=$a_bil[0];
		if ($panjang>1)
		{
			$bil=$a_bil[1].$bil;
		}

		if ($panjang>2)
		{
			$bil=$a_bil[2].$bil;
		}
		//die($bil);
		if ($bil!="" && $bil!="000")
		{
			$terbilang .= $this->rp_satuan($bil,$debug);
		}
		return trim($terbilang);
	}

	function namaBulan($tgl){
		$arrtgl = explode('-', $tgl);
		$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
		$bulan = $query->row_array();
		$tanggalInd = $arrtgl[2] .' '. $bulan['nmbulan'] .' '. $arrtgl[0];
		
		return $tanggalInd ;
	}
	
}