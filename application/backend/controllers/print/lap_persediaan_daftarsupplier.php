<?php

	class Lap_persediaan_daftarsupplier extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function laporan_daftarsupplier(){
		$isi = '';		
		$this->db->select('supplier.*, bank.nmbank, status.nmstatus');
        $this->db->from('supplier');
		$this->db->join('bank',
                'bank.idbank = supplier.idbank', 'left');	
		$this->db->join('status',
                'status.idstatus = supplier.idstatus', 'left');
		//$this->db->limit(200,0);
		$query = $this->db->get();
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Daftar Supplier', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		//$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		
		$aaa = $query->result();
		$no = 1;
		foreach($aaa as $i=>$val){
			
			$vartgl;
			if($val->tgldaftar != null){
				$vartgl = date('d-m-Y', strtotime($val->tgldaftar));
			}else{
				$vartgl = null;
			}
			$isi .= "<tr>
					<td width=\"2.5%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"3.5%\" align=\"center\">". $val->kdsupplier ."</td>
					<td width=\"4.5%\" align=\"center\">".$vartgl."</td>
					<td width=\"9%\">". $val->nmsupplier ."</td>
					<td width=\"10%\">". $val->alamat ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->notelp ."</td>
					<td width=\"4%\" align=\"center\">". $val->nofax ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->email ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->website ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->kontakperson ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->nohp ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->npwp ."</td>
					<td width=\"5%\" align=\"center\">". $val->nmbank ."</td>
					<td width=\"7%\" align=\"center\">". $val->norek ."</td>
					<td width=\"7%\" align=\"center\">". $val->atasnama ."</td>
					<td width=\"4%\" align=\"center\">". $val->nmstatus ."</td>
					<td width=\"7.5%\">". $val->keterangan ."</td>
				</tr>";	
		}
		
		$html = "<br/><br/><font size=\"6\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"2.5%\">No.</td>
					<td width=\"3.5%\">Kode Supplier</td>
					<td width=\"4.5%\">Tgl Daftar</td>
					<td width=\"9%\">Nama Supplier</td>
					<td width=\"10%\">Alamat</td>
					<td width=\"6.5%\">No. Telp</td>
					<td width=\"4%\">No. Fax</td>
					<td width=\"6.5%\">Email</td>
					<td width=\"6.5%\">Website</td>
					<td width=\"6.5%\">Kontak Person</td>
					<td width=\"6.5%\">No. Hp</td>
					<td width=\"6.5%\">Npwp</td>
					<td width=\"5%\">Bank</td>
					<td width=\"7%\">No. Rek</td>
					<td width=\"7%\">Atas Nama</td>
					<td width=\"4%\">Status</td>
					<td width=\"7.5%\">Keterangan</td>
				</tr>
			  <tbody>". $isi ."</tbody>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf_lap->Output('daftar_supplier.pdf', 'I');
	}
	
	function exceldaftarsupplier(){		
		$tablename='v_lapdaftarsupplier';
		$header = array(
			'Kode Supplier',
			'Tgl Daftar',
			'Nama Supplier',
			//'Satuan Besar',
			'Alamat',
			'No. Telp',
			'No. Fax',
			'Email',
			'Website',
			'Kontak Person',
			'No. Hp',
			'Npwp',
			'Bank',
			'No. Rek',
			'Atas Nama',
			'Status',
			'Keterangan',
		);
		
		$this->db->select("
			kdsupplier,
			tgldaftar,
			nmsupplier,
			alamat,
			notelp,
			nofax,
			email,
			website,
			kontakperson,
			nohp,
			npwp,
			nmbank,
			norek,
			atasnama,
			nmstatus,
			keterangan,
		");
        $this->db->from($tablename);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('Daftar_Supplier');
		$data['filter'] = strtoupper('Daftar Supplier');		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellapfarmasi', $data); 	
	}	
}
