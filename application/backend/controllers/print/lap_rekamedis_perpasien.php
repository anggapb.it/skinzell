<?php 
class Lap_rekamedis_perpasien extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
	function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
    }
		
	function get_by_id($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as data FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $data= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $data=$row->data;
        }
        return $data;
    }
	
	function get_data($norm){
		$query  = $this->db->query("SELECT * FROM v_penyakitperpasien WHERE norm='".$norm."' ORDER BY tglkodifikasi");
		$result = array();
		if ($query->num_rows() > 0) {
			$result = $query->result();
		}
		$rows = "";
		foreach ($result as $items) {
			$rows .= "<tr>
						  <td align=\"center\">".$items->tglkodifikasirender."</td>
						  <td align=\"center\">".$items->jamkodifikasirender."</td>
						  <td align=\"center\">".$items->kdpenyakit."</td>
						  <td align=\"left\">".$items->nmpenyakit."</td>
						  <td align=\"left\">".$items->nmpenyakiteng."</td>
						  <td align=\"center\">".$items->nmjstpenyakit."</td>
						  <td align=\"center\">".$items->nmjnsdiagnosa."</td>
					 </tr>";

		}
		return $rows;
	}

	function get_rmperpasien($norm,$before=1,$between=4){

		$this->pdf->SetMargins('10', '40', '10');
		$this->pdf->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf->setPrintFooter(true); // enabled ? true
		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf->AddPage('P', 'F4', false, false); 
		
		$this->pdf->SetFont('helvetica', '', 8);
		
		$wherecust = "trim(LEADING '0' FROM norm)";
		$tbl = "pasien";
		
		$nmpasien = $this->get_by_id("nmpasien",$tbl,$wherecust, $norm);
		
		$idjnskelamin = $this->get_by_id("idjnskelamin",$tbl,$wherecust, $norm);
		$nmjnskelamin = $this->get_by_id("nmjnskelamin",'jkelamin','idjnskelamin', $idjnskelamin);
		
		$idgoldarah = $this->get_by_id("idgoldarah",$tbl,$wherecust, $norm);
		$nmgoldarah = $this->get_by_id("nmgoldarah",'goldarah','idgoldarah', $idgoldarah);
		
		$tptlahir = $this->get_by_id("tptlahir",$tbl,$wherecust, $norm);
		$tgllahir = $this->get_by_id("tgllahir",$tbl,$wherecust, $norm);
		$alamat = $this->get_by_id("alamat",$tbl,$wherecust, $norm);
		$notelp = $this->get_by_id("notelp",$tbl,$wherecust, $norm);
		
		$head = <<<EOD
	<br />
    <br />
	
	<table border="0" cellpadding="2" nobr="true">
	 <tr> 
      <th width="17%"><b>No. RM</b></th>
	  <th width="32%">: $norm</th> 
	  <th width="2%"></th> 
	  <th width="17%"><b>Tempat Lahir</b></th>
	  <th width="32%">: $tptlahir</th> 
	 </tr>
	 <tr> 
	  <th width="17%"><b>Nama Pasien</b></th>
	  <th width="32%">: $nmpasien</th> 
	  <th width="2%"></th> 
	  <th width="17%"><b>Tanggal Lahir</b></th>
	  <th width="32%">: $tgllahir</th> 
	 </tr>
	 <tr> 
	  <th width="17%"><b>Jenis Kelamin</b></th>
	  <th width="32%">: $nmjnskelamin</th> 
	  <th width="2%"></th> 
	  <th width="17%"><b>Alamat</b></th>
	  <th width="32%">: $alamat</th> 
	 </tr>
	 <tr> 
	  <th width="17%"><b>Golongan Darah</b></th>
	  <th width="32%">: $nmgoldarah</th> 
	  <th width="2%"></th> 
	  <th width="17%"><b>No. Telp</b></th>
	  <th width="32%">: $notelp</th> 
	 </tr>
    </table>
	<br />
    <br />
EOD;
     	$this->pdf->writeHTML($head,true,false,false,false);
		
		$get_rows = $this->get_data($norm);
		$datatable = <<<EOD
	<b>DAFTAR PENYAKIT</b> <br />
    <table border="1" cellpadding="2" nobr="true">
     <tr>
	  <th width="9%" align="center"><b>Tanggal</b></th>
	  <th width="6%" align="center"><b>Jam</b></th>
	  <th width="12%" align="center"><b>Kode Penyakit<br>(ICD-X)</b></th>
      <th width="25%" align="center"><b>Nama Penyakit<br>(Bhs. Indonesia)</b></th>
	  <th width="25%" align="center"><b>Nama Penyakit<br>(Bhs. Inggris)</b></th>
	  <th width="12%" align="center"><b>( Baru / Lama )</b></th>
      <th width="12%" align="center"><b>Jenis Diagnosa</b></th>
     </tr> 
		$get_rows
    </table>
	
EOD;
		$this->pdf->writeHTML($datatable,true,false,false,false);
		
		$this->pdf->Output('laporan_rm_perpasien.pdf', 'I');
    
       
	}
}

?>