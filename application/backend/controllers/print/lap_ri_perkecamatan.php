<?php 
class Lap_ri_perkecamatan extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
	
	function rawatinap($tglawal,$tglakhir){
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-1, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'PENERIMAAN RAWAT INAP PER KECAMATAN', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		
		
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
				<tr bgcolor=\"#cccccc\" align=\"center\">
					<td width=\"60%\">KELURAHAN</td>
					<td width=\"20%\">KODE POS</td>
					<td width=\"20%\">JUMLAH</td>
				</tr>
			  </thead>
			  <tbody></tbody>
				<tr align=\"left\">
					<td width=\"100%\">PROVINSI : JAWA BARAT</td>
					
				</tr>
				<tr align=\"left\">
					<td width=\"100%\">KOTA/KAB : KOTA BANDUNG</td>
					
				</tr>
				<tr align=\"left\">
					<td width=\"100%\">KECAMATAN : Regol</td>
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">CIKAPUNDUNG</td>
					<td width=\"20%\">2242</td>
					<td width=\"20%\">1</td>
					
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">............................</td>
					<td width=\"20%\">.................</td>
					<td width=\"20%\">0</td>
					
					
				</tr>
				
				<tr align=\"right\">
					<td width=\"80%\">SUB TOTAL</td>
					<td ALIGN=\"CENTER\" width=\"20%\">1</td>
					
				</tr>
				<tr align=\"left\">
					<td width=\"100%\">KECAMATAN : Sukajadi</td>
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">PASTEUR</td>
					<td width=\"20%\">2342</td>
					<td width=\"20%\">1</td>
					
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">............................</td>
					<td width=\"20%\">.................</td>
					<td width=\"20%\">0</td>
					
					
				</tr>
				
				<tr align=\"right\">
					<td width=\"80%\">SUB TOTAL</td>
					<td ALIGN=\"CENTER\" width=\"20%\">1</td>
					
				</tr>
				<tr align=\"left\">
					<td width=\"100%\">KECAMATAN : Bandung Kulon</td>
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">BOJONG</td>
					<td width=\"20%\">2420</td>
					<td width=\"20%\">1</td>
					
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">CIJERAH</td>
					<td width=\"20%\">2430</td>
					<td width=\"20%\">1</td>
					
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">............................</td>
					<td width=\"20%\">..............</td>
					<td width=\"20%\">0</td>
					
					
				</tr>
				
				<tr align=\"right\">
					<td width=\"80%\">SUB TOTAL</td>
					<td ALIGN=\"CENTER\" width=\"20%\">2</td>
					
				</tr>
				<tr align=\"left\">
					<td width=\"100%\">KECAMATAN : Babakan</td>
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">Jagakarsa</td>
					<td width=\"20%\">2613</td>
					<td width=\"20%\">1</td>
					
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">............................</td>
					<td width=\"20%\">..............</td>
					<td width=\"20%\">0</td>
					
					
				</tr>
				<tr align=\"right\">
					<td width=\"80%\">SUB TOTAL</td>
					<td ALIGN=\"CENTER\" width=\"20%\">1</td>
					
				</tr>
				<tr align=\"right\">
					<td width=\"80%\">SUB TOTAL 1</td>
					<td ALIGN=\"CENTER\" width=\"20%\">5</td>
					
				</tr>
				
				<tr align=\"left\">
					<td width=\"100%\">KOTA/KAB : KAB. BANDUNG</td>
					
				</tr>
				<tr align=\"left\">
					<td width=\"100%\">KECAMATAN : Margahayu</td>
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">SAYATI KIDUL</td>
					<td width=\"20%\">3210</td>
					<td width=\"20%\">2</td>
					
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">SAYATI</td>
					<td width=\"20%\">3211</td>
					<td width=\"20%\">1</td>
					
					
				</tr>
				<tr align=\"center\">
					<td width=\"60%\">............................</td>
					<td width=\"20%\">..............</td>
					<td width=\"20%\">0</td>
					
					
				</tr>
				<tr align=\"right\">
					<td width=\"80%\">SUB TOTAL</td>
					<td ALIGN=\"CENTER\" width=\"20%\">3</td>
					
				</tr>
				<tr align=\"right\">
					<td width=\"80%\">SUB TOTAL 2</td>
					<td ALIGN=\"CENTER\" width=\"20%\">3</td>
					
				</tr>
				<tr align=\"right\">
					<td width=\"80%\">TOTAL</td>
					<td ALIGN=\"CENTER\" width=\"20%\">8</td>
					
				</tr>
				

				
			</table>
			<br/><br/>
			<br/><br/>
			<br/><br/>
			
						
				<table>
				<tr align=\"RIGHT\">
					<td width=\"98%\"><p style=\" text-indent: 235m;\"></td>Bandung, ".date("d F Y", strtotime($tglawal)) ."</td>
						
				</tr>
				<br/><br/>
				<br/>
				<tr align=\"RIGHT\">
					<td width=\"100%\">(..........................................................)</td>
						
				</tr>
				<tr align=\"RIGHT\">
					<td width=\"97%\">Petugas registrasi</td>
						
				</tr>
			  </table>
			</font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('RJ_PerKec.pdf', 'I');
		}
}
?>