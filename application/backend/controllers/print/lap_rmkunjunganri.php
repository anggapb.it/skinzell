<?php

	class Lap_rmkunjunganri extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function get_lap_rmkunjunganri($tglawal,$tglakhir){
		$isi = '';
		
		$this->db->select("*");
		$this->db->from("v_lap_rmkunjunganri_pdf");	
		$this->db->order_by("v_lap_rmkunjunganri_pdf.tglkuitansi ASC");
		$this->db->where("tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'");
		//$this->db->limit("5");
		$query = $this->db->get();
		$ri = $query->result();
				
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Kunjungan', 0, 1, 'C', 0, '', 0);
		$this->pdf_lap->Cell(0, 0, 'Rawat Inap', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);

		foreach($ri as $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"3.5%\" align=\"center\">". $val->tglmasuk ."</td>
					<td width=\"3.5%\" align=\"center\">". $val->tglkuitansi ."</td>
					<td width=\"4%\" align=\"center\">". $val->noreg ."</td>
					<td width=\"2.5%\" align=\"center\">". $val->stpasien ."</td>
					<td width=\"6.5%\" align=\"left\">". $val->nmpasien ."</td>
					<td width=\"2%\" align=\"center\">". $val->umurtahun ."</td>
					<td width=\"3.5%\" align=\"center\">". $val->nmjnskelamin ."</td>
					<td width=\"10%\" align=\"left\">". $val->alamat ."</td>
					<td width=\"4.5%\" align=\"center\">". $val->kec ."</td>
					<td width=\"5.5%\" align=\"center\">". $val->kot ."</td>
					<td width=\"5.5%\" align=\"left\">". $val->notelp ."</td>
					<td width=\"3.5%\" align=\"center\">". $val->lamarawat ." Hari</td>
					<td width=\"4.5%\" align=\"center\">". $val->nmbagian ."</td>
					<td width=\"3.5%\" align=\"center\">". $val->nmklstarif ."</td>
					<td width=\"8%\" align=\"left\">". $val->nmdoktergelar ."</td>
					<td width=\"5.5%\" align=\"center\">". $val->nmjnskasus ."</td>
					<td width=\"5.5%\" align=\"center\">". $val->nmcaradatang ."</td>
					<td width=\"5.5%\" align=\"center\">". $val->nmstkeluar ."</td>
					<td width=\"5.5%\" align=\"center\">". $val->nmcarakeluar ."</td>
					<td width=\"8%\" align=\"left\">". $val->nmpenjamin ."</td>
				</tr>";	
		}
		
		$html = "<br/><br/><font size=\"5\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"3%\" rowspan=\"2\">No.</td>
					<td width=\"3.5%\" rowspan=\"2\">Masuk</td>
					<td width=\"3.5%\" rowspan=\"2\">Pulang</td>
					<td width=\"4%\" rowspan=\"2\">No. Registrasi</td>
					<td width=\"2.5%\" rowspan=\"2\">Status<br>Pasien</td>
					<td width=\"6.5%\" rowspan=\"2\">Nama Pasien</td>
					<td width=\"2%\" rowspan=\"2\">Usia</td>
					<td width=\"3.5%\" rowspan=\"2\">Jenis Kelamin</td>
					<td width=\"10%\" rowspan=\"2\">Alamat</td>
					<td width=\"10%\" colspan=\"2\">Daerah</td>
					<td width=\"5.5%\" rowspan=\"2\">No. Tlp</td>
					<td width=\"3.5%\" rowspan=\"2\">Lama Hari<br>di rawat</td>
					<td width=\"4.5%\" rowspan=\"2\">Ruangan</td>
					<td width=\"3.5%\" rowspan=\"2\">Kelas<br>di rawat</td>
					<td width=\"8%\" rowspan=\"2\">Dokter</td>
					<td width=\"5.5%\" rowspan=\"2\">Jenis Kasus</td>
					<td width=\"5.5%\" rowspan=\"2\">Rujukan</td>
					<td width=\"5.5%\" rowspan=\"2\">Keadaan<br>Keluar</td>
					<td width=\"5.5%\" rowspan=\"2\">Cara<br>Keluar</td>
					<td width=\"8%\" rowspan=\"2\">Penjamin</td>
				</tr>
				<tr align=\"center\">
					<td width=\"4.5%\">Kecamatan</td>
					<td width=\"5.5%\">Kota</td>
				</tr>
			  <tbody>
			  ". $isi ."
			  </tbody>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);

		//Close and output PDF document
		$this->pdf_lap->Output('Lap_rmkunjunganrj.pdf', 'I');
	}
	
	function laporan_excelrmkri($tglawal, $tglakhir) {
		$header = array(
			'Masuk',
			'Pulang',
			'No. Registrasi',
			'No. RM',
			'Status Pasien',
			'Nama Pasien',
			'Gologan Sebab Penyakit',
			'Usia',
			'Jenis Kelamin',
			'Alamat',
			'Kecamatan',
			'Kota',
			'No. Tlp',
			'Lama Hari di rawat',
			'Ruangan',
			'Kelas di rawat',
			'Dokter',
			'Jenis Kasus',
			'Rujukan',
			'Keadaan Keluar',
			'Cara Keluar',
			'Penjamin',
		);
		
		$this->db->select("
			tglmasuk,
			tglkuitansi,
			noreg,
			norm,
			stpasien,
			nmpasien,
			nmpenyakiteng,
			umurtahun,
			nmjnskelamin,
			alamat,
			kec,
			kot,
			notelp,
			lamarawat,
			nmbagian,
			nmklstarif,
			nmdoktergelar,
			nmjnskasus,
			nmcaradatang,
			nmstkeluar,
			nmcarakeluar,
			nmpenjamin,		
		");
		$this->db->from('v_lap_rmkunjunganri');
		$this->db->order_by("v_lap_rmkunjunganri.tglkuitansi ASC");
		$this->db->where("tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'");
		$query = $this->db->get();
		$fpl = $query->result();
		$fplnum = $query->num_rows();
		
		$data['eksport'] = $fpl;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('Lap_RM_Kunjungan_RI');
		$data['filter'] = "Laporan : Rekam Medis Kunjungan RI \n".strtoupper('Periode')." : ".$tglawal." s/d ".$tglakhir."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellaprmkpri', $data); 	
	}
}
