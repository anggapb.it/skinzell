<?php 
class Lap_rekamedis_sensusharian extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
	function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
    }
		
	function get_by_id($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as data FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $data= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $data=$row->data;
        }
        return $data;
    }
	
	function get_pasien_masuk($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan){
		$opttgl = ($cbxtgl=='true') ? "=":"<>";
		$optjam = ($cbxjam=='true') ? "=":"<>";
		$optruangan = ($cbxruangan=='true') ? "=":"<>";
		
		$valtgl = ($cbxtgl=='true') ? $tgl:"-";
		$valjam = ($cbxjam=='true') ? $jam:"-";
		$valruangan = ($cbxruangan=='true') ? $ruangan:"-";
		
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglmasuk ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jammasuk,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$no = 1;
			$data_row = "";
			
			foreach ($data as $value) {
				$data_row .= 
				"<tr>
				  <th align=\"center\">".$no++."</th>
				  <th align=\"left\">".$value->norm."</th>
				  <th align=\"left\">".$value->noreg."</th>
				  <th align=\"left\">".date("d/m/Y",strtotime($value->tglmasuk))."</th>
				  <th align=\"left\">".$value->jammasuk."</th>
				  <th align=\"left\">".$value->nmpasien."</th>
				  <th align=\"left\">".$value->nmdoktergelar."</th>
				  <th align=\"left\">".$value->nmklsrawat."</th>
				  <th align=\"left\">".$value->nmkamar."</th>
				  <th align=\"left\">".$value->nmbed."</th>
				 </tr>";
			}
			
			$data_table = <<<EOD
	<b>1. PASIEN MASUK</b> <br />
    <table border="1" cellpadding="2" nobr="true">

     <tr>
	  <th width="4%" align="left"><b>No.</b></th>
	  <th width="5%" align="left"><b>No. RM</b></th>
      <th width="8%" align="left"><b>No. REG</b></th>
	  <th width="8%" align="left"><b>Tgl. Masuk</b></th>
      <th width="8%" align="left"><b>Jam Masuk</b></th>
	  <th width="17%" align="left"><b>Nama Pasien</b></th>
	  <th width="17%" align="left"><b>Dokter</b></th>
	  <th width="11%" align="left"><b>Kelas</b></th>
	  <th width="11%" align="left"><b>Kamar/TT</b></th>
      <th width="11%" align="left"><b>Bed</b></th>
     </tr> 

		$data_row
    </table>
	
EOD;

		return $data_table;

	}
	
	function get_pasien_pindahan($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan){
		$opttgl = ($cbxtgl=='true') ? "=":"<>";
		$optjam = ($cbxjam=='true') ? "=":"<>";
		$optruangan = ($cbxruangan=='true') ? "=":"<>";
		
		$valtgl = ($cbxtgl=='true') ? $tgl:"-";
		$valjam = ($cbxjam=='true') ? $jam:"-";
		$valruangan = ($cbxruangan=='true') ? $ruangan:"-";
		
		$q = "SELECT * FROM (SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
			 , (SELECT concat('Pindahan dari kamar ',b.nmbagian) from registrasidet r 
				 LEFT JOIN bagian b ON r.idbagian = b.idbagian
				  where r.noreg = registrasidet.noreg and r.idregdet < registrasidet.idregdet
				  order by r.idregdet desc  limit 1) as keterangan
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglmasuk ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jammasuk,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg) a where keterangan is not null";
		
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$no = 1;
			$data_row = "";
			
			foreach ($data as $value) {
				$data_row .= 
				"<tr>
				  <th align=\"center\">".$no++."</th>
				  <th align=\"left\">".$value->norm."</th>
				  <th align=\"left\">".$value->noreg."</th>
				  <th align=\"left\">".date("d/m/Y",strtotime($value->tglmasuk))."</th>
				  <th align=\"left\">".$value->jammasuk."</th>
				  <th align=\"left\">".$value->nmpasien."</th>
				  <th align=\"left\">".$value->nmdoktergelar."</th>
				  <th align=\"left\">".$value->nmklsrawat."</th>
				  <th align=\"left\">".$value->nmkamar."</th>
				  <th align=\"left\">".$value->nmbed."</th>
				  <th align=\"left\">".$value->keterangan."</th>
				 </tr>";
			}
			
			$data_table = <<<EOD
	<b>2. PASIEN PINDAHAN</b> <br />
    <table border="1" cellpadding="2" nobr="true">

     <tr>
	  <th width="4%" align="left"><b>No.</b></th>
	  <th width="5%" align="left"><b>No. RM</b></th>
      <th width="8%" align="left"><b>No. REG</b></th>
	  <th width="8%" align="left"><b>Tgl. Masuk</b></th>
      <th width="8%" align="left"><b>Jam Masuk</b></th>
	  <th width="11%" align="left"><b>Nama Pasien</b></th>
	  <th width="12%" align="left"><b>Dokter</b></th>
	  <th width="9%" align="left"><b>Kelas</b></th>
	  <th width="9%" align="left"><b>Kamar/TT</b></th>
      <th width="9%" align="left"><b>Bed</b></th>
	  <th width="17%" align="left"><b>Keterangan</b></th>
     </tr> 

		$data_row
    </table>
	
EOD;

		return $data_table;

	}
	
	function get_pasien_dipindahkan($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan){
		$opttgl = ($cbxtgl=='true') ? "=":"<>";
		$optjam = ($cbxjam=='true') ? "=":"<>";
		$optruangan = ($cbxruangan=='true') ? "=":"<>";
		
		$valtgl = ($cbxtgl=='true') ? $tgl:"-";
		$valjam = ($cbxjam=='true') ? $jam:"-";
		$valruangan = ($cbxruangan=='true') ? $ruangan:"-";
		
		$q = "SELECT * FROM (SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglkeluar
			 , time_format(registrasidet.jamkeluar,'%H:%i') as jamkeluar
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
			 , (SELECT concat('Dipindahkan ke kamar ',b.nmbagian) from registrasidet r 
				 LEFT JOIN bagian b ON r.idbagian = b.idbagian
				  where r.noreg = registrasidet.noreg and r.idregdet > registrasidet.idregdet
				  order by r.idregdet desc  limit 1) as keterangan
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglkeluar ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jamkeluar,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg) a where keterangan is not null";
		
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$no = 1;
			$data_row = "";
			
			foreach ($data as $value) {
				$data_row .= 
				"<tr>
				  <th align=\"center\">".$no++."</th>
				  <th align=\"left\">".$value->norm."</th>
				  <th align=\"left\">".$value->noreg."</th>
				  <th align=\"left\">".date("d/m/Y",strtotime($value->tglkeluar))."</th>
				  <th align=\"left\">".$value->jamkeluar."</th>
				  <th align=\"left\">".$value->nmpasien."</th>
				  <th align=\"left\">".$value->nmdoktergelar."</th>
				  <th align=\"left\">".$value->nmklsrawat."</th>
				  <th align=\"left\">".$value->nmkamar."</th>
				  <th align=\"left\">".$value->nmbed."</th>
				  <th align=\"left\">".$value->keterangan."</th>
				 </tr>";
			}
			
			$data_table = <<<EOD
	<b>3. PASIEN DIPINDAHKAN</b> <br />
    <table border="1" cellpadding="2" nobr="true">

     <tr>
	  <th width="4%" align="left"><b>No.</b></th>
	  <th width="5%" align="left"><b>No. RM</b></th>
      <th width="8%" align="left"><b>No. REG</b></th>
	  <th width="8%" align="left"><b>Tgl. Keluar</b></th>
      <th width="8%" align="left"><b>Jam Keluar</b></th>
	  <th width="11%" align="left"><b>Nama Pasien</b></th>
	  <th width="12%" align="left"><b>Dokter</b></th>
	  <th width="9%" align="left"><b>Kelas</b></th>
	  <th width="9%" align="left"><b>Kamar/TT</b></th>
      <th width="9%" align="left"><b>Bed</b></th>
	  <th width="17%" align="left"><b>Keterangan</b></th>
     </tr> 

		$data_row
    </table>
	
EOD;

		return $data_table;

	}
	
	function get_pasien_keluar_hidup($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan){
		$opttgl = ($cbxtgl=='true') ? "=":"<>";
		$optjam = ($cbxjam=='true') ? "=":"<>";
		$optruangan = ($cbxruangan=='true') ? "=":"<>";
		
		$valtgl = ($cbxtgl=='true') ? $tgl:"-";
		$valjam = ($cbxjam=='true') ? $jam:"-";
		$valruangan = ($cbxruangan=='true') ? $ruangan:"-";
		
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglkeluar
			 , time_format(registrasidet.jamkeluar,'%H:%i') as jamkeluar
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 1
		  AND registrasidet.tglkeluar ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jamkeluar,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$no = 1;
			$data_row = "";
			
			foreach ($data as $value) {
				$data_row .= 
				"<tr>
				  <th align=\"center\">".$no++."</th>
				  <th align=\"left\">".$value->norm."</th>
				  <th align=\"left\">".$value->noreg."</th>
				  <th align=\"left\">".date("d/m/Y",strtotime($value->tglkeluar))."</th>
				  <th align=\"left\">".$value->jamkeluar."</th>
				  <th align=\"left\">".$value->nmpasien."</th>
				  <th align=\"left\">".$value->nmdoktergelar."</th>
				  <th align=\"left\">".$value->nmklsrawat."</th>
				  <th align=\"left\">".$value->nmkamar."</th>
				  <th align=\"left\">".$value->nmbed."</th>
				 </tr>";
			}
			
			$data_table = <<<EOD
	<b>4. PASIEN KELUAR HIDUP</b> <br />
    <table border="1" cellpadding="2" nobr="true">

     <tr>
	  <th width="4%" align="left"><b>No.</b></th>
	  <th width="5%" align="left"><b>No. RM</b></th>
      <th width="8%" align="left"><b>No. REG</b></th>
	  <th width="8%" align="left"><b>Tgl. Keluar</b></th>
      <th width="8%" align="left"><b>Jam Keluar</b></th>
	  <th width="15%" align="left"><b>Nama Pasien</b></th>
	  <th width="16%" align="left"><b>Dokter</b></th>
	  <th width="12%" align="left"><b>Kelas</b></th>
	  <th width="12%" align="left"><b>Kamar/TT</b></th>
      <th width="12%" align="left"><b>Bed</b></th>
     </tr> 

		$data_row
    </table>
	
EOD;

		return $data_table;

	}
	
	function get_pasien_meninggal_kurang_48_jam($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan){
		$opttgl = ($cbxtgl=='true') ? "=":"<>";
		$optjam = ($cbxjam=='true') ? "=":"<>";
		$optruangan = ($cbxruangan=='true') ? "=":"<>";
		
		$valtgl = ($cbxtgl=='true') ? $tgl:"-";
		$valjam = ($cbxjam=='true') ? $jam:"-";
		$valruangan = ($cbxruangan=='true') ? $ruangan:"-";
		
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , kodifikasi.tglmeninggal
			 , time_format(kodifikasi.jammeninggal,'%H:%i') as jammeninggal
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN kodifikasi
		ON registrasi.noreg = kodifikasi.noreg
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 2
		  AND kodifikasi.tglmeninggal ".$opttgl." '".$valtgl."' 
		  AND time_format(kodifikasi.jammeninggal,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$no = 1;
			$data_row = "";
			
			foreach ($data as $value) {
				$data_row .= 
				"<tr>
				  <th align=\"center\">".$no++."</th>
				  <th align=\"left\">".$value->norm."</th>
				  <th align=\"left\">".$value->noreg."</th>
				  <th align=\"left\">".date("d/m/Y",strtotime($value->tglmeninggal))."</th>
				  <th align=\"left\">".$value->jammeninggal."</th>
				  <th align=\"left\">".$value->nmpasien."</th>
				  <th align=\"left\">".$value->nmdoktergelar."</th>
				  <th align=\"left\">".$value->nmklsrawat."</th>
				  <th align=\"left\">".$value->nmkamar."</th>
				  <th align=\"left\">".$value->nmbed."</th>
				 </tr>";
			}
			
			$kurangdari = htmlentities('<');
			$data_table = <<<EOD
	<b>5. PASIEN MENINGGAL $kurangdari 48 JAM</b> <br />
    <table border="1" cellpadding="2" nobr="true">

     <tr>
	  <th width="4%" align="left"><b>No.</b></th>
	  <th width="5%" align="left"><b>No. RM</b></th>
      <th width="8%" align="left"><b>No. REG</b></th>
	  <th width="9%" align="left"><b>Tgl. Meninggal</b></th>
      <th width="9%" align="left"><b>Jam Meninggal</b></th>
	  <th width="14%" align="left"><b>Nama Pasien</b></th>
	  <th width="15%" align="left"><b>Dokter</b></th>
	  <th width="12%" align="left"><b>Kelas</b></th>
	  <th width="12%" align="left"><b>Kamar/TT</b></th>
      <th width="12%" align="left"><b>Bed</b></th>
     </tr> 

		$data_row
    </table>
	
EOD;

		return $data_table;

	}
	
	function get_pasien_meninggal_lebih_sm_dgn_48_jam($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan){
		$opttgl = ($cbxtgl=='true') ? "=":"<>";
		$optjam = ($cbxjam=='true') ? "=":"<>";
		$optruangan = ($cbxruangan=='true') ? "=":"<>";
		
		$valtgl = ($cbxtgl=='true') ? $tgl:"-";
		$valjam = ($cbxjam=='true') ? $jam:"-";
		$valruangan = ($cbxruangan=='true') ? $ruangan:"-";
		
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , kodifikasi.tglmeninggal
			 , time_format(kodifikasi.jammeninggal,'%H:%i') as jammeninggal
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN kodifikasi
		ON registrasi.noreg = kodifikasi.noreg
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 3
		  AND kodifikasi.tglmeninggal ".$opttgl." '".$valtgl."' 
		  AND time_format(kodifikasi.jammeninggal,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$no = 1;
			$data_row = "";
			
			foreach ($data as $value) {
				$data_row .= 
				"<tr>
				  <th align=\"center\">".$no++."</th>
				  <th align=\"left\">".$value->norm."</th>
				  <th align=\"left\">".$value->noreg."</th>
				  <th align=\"left\">".date("d/m/Y",strtotime($value->tglmeninggal))."</th>
				  <th align=\"left\">".$value->jammeninggal."</th>
				  <th align=\"left\">".$value->nmpasien."</th>
				  <th align=\"left\">".$value->nmdoktergelar."</th>
				  <th align=\"left\">".$value->nmklsrawat."</th>
				  <th align=\"left\">".$value->nmkamar."</th>
				  <th align=\"left\">".$value->nmbed."</th>
				 </tr>";
			}
			
			$data_table = <<<EOD
	<b>6. PASIEN MENINGGAL >= 48 JAM</b> <br />
    <table border="1" cellpadding="2" nobr="true">

     <tr>
	  <th width="4%" align="left"><b>No.</b></th>
	  <th width="5%" align="left"><b>No. RM</b></th>
      <th width="8%" align="left"><b>No. REG</b></th>
	  <th width="9%" align="left"><b>Tgl. Meninggal</b></th>
      <th width="9%" align="left"><b>Jam Meninggal</b></th>
	  <th width="14%" align="left"><b>Nama Pasien</b></th>
	  <th width="15%" align="left"><b>Dokter</b></th>
	  <th width="12%" align="left"><b>Kelas</b></th>
	  <th width="12%" align="left"><b>Kamar/TT</b></th>
      <th width="12%" align="left"><b>Bed</b></th>
     </tr> 

		$data_row
    </table>
	
EOD;

		return $data_table;

	}
	
	function get_pasien_hari_ini($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan){
		$opttgl = ($cbxtgl=='true') ? "<=":"<>";
		$optjam = ($cbxjam=='true') ? "=":"<>";
		$optruangan = ($cbxruangan=='true') ? "=":"<>";
		
		$valtgl = ($cbxtgl=='true') ? $tgl:"-";
		$valjam = ($cbxjam=='true') ? $jam:"-";
		$valruangan = ($cbxruangan=='true') ? $ruangan:"-";
		
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglmasuk ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jammasuk,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.tglmasuk desc";
		
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$no = 1;
			$data_row = "";
			
			foreach ($data as $value) {
				$data_row .= 
				"<tr>
				  <th align=\"center\">".$no++."</th>
				  <th align=\"left\">".$value->norm."</th>
				  <th align=\"left\">".$value->noreg."</th>
				  <th align=\"left\">".date("d/m/Y",strtotime($value->tglmasuk))."</th>
				  <th align=\"left\">".$value->jammasuk."</th>
				  <th align=\"left\">".$value->nmpasien."</th>
				  <th align=\"left\">".$value->nmdoktergelar."</th>
				  <th align=\"left\">".$value->nmklsrawat."</th>
				  <th align=\"left\">".$value->nmkamar."</th>
				  <th align=\"left\">".$value->nmbed."</th>
				 </tr>";
			}
			
			$data_table = <<<EOD
	<b>7. PASIEN HARI INI (Sesuai Dengan Tanggal Masuk Yang Di Filter)</b> <br />
    <table border="1" cellpadding="2" nobr="true">

     <tr>
	  <th width="4%" align="left"><b>No.</b></th>
	  <th width="5%" align="left"><b>No. RM</b></th>
      <th width="8%" align="left"><b>No. REG</b></th>
	  <th width="8%" align="left"><b>Tgl. Masuk</b></th>
      <th width="8%" align="left"><b>Jam Masuk</b></th>
	  <th width="17%" align="left"><b>Nama Pasien</b></th>
	  <th width="17%" align="left"><b>Dokter</b></th>
	  <th width="11%" align="left"><b>Kelas</b></th>
	  <th width="11%" align="left"><b>Kamar/TT</b></th>
      <th width="11%" align="left"><b>Bed</b></th>
     </tr> 

		$data_row
    </table>
	
EOD;

		return $data_table;

	}
	
	function sensusharian($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan,$before=1,$between=4){

		$disptgl = ($cbxtgl=='true') ? $this->TanggalIndo(date("Ymd",strtotime($tgl))):"-";
		$dispjam = ($cbxjam=='true' && $jam) ? $jam:"-";
		$dispruangan = ($cbxruangan=='true' && $ruangan) ? $this->get_by_id("nmbagian","bagian","idbagian", $ruangan):"-";

		$this->pdf->SetPrintHeader(false);
		$this->pdf->setPrintFooter(false);
		$this->pdf->SetMargins('10', '10', '10');
		$this->pdf->SetAutoPageBreak(TRUE, '10');
		
		$this->pdf->AddPage('L', 'F4', false, false); 
		
		$this->pdf->SetFont('helvetica', 'B', 14);
		$this->pdf->Cell(0, 0, 'LAPORAN SENSUS HARIAN', 0, 1, 'C', 0, '', 0);
		
        $this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Tanggal : '.$disptgl.', Jam : '.$dispjam, 0, 1, 'C', 0, '', 0);
        $this->pdf->Cell(0, 0, 'Ruangan : '.$dispruangan, 0, 1, 'C', 0, '', 0);
		
		$data_pasien_masuk = $this->get_pasien_masuk($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan);
		$this->pdf->writeHTML($data_pasien_masuk,true,false,false,false);	

		//$this->pdf->AddPage('L', 'F4', false, false);
		$data_pasien_keluar_hidup = $this->get_pasien_pindahan($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan);
		$this->pdf->writeHTML($data_pasien_keluar_hidup,true,false,false,false);	
		
		//$this->pdf->AddPage('L', 'F4', false, false);
		$data_pasien_keluar_hidup = $this->get_pasien_dipindahkan($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan);
		$this->pdf->writeHTML($data_pasien_keluar_hidup,true,false,false,false);	
		
		//$this->pdf->AddPage('L', 'F4', false, false);
		$data_pasien_keluar_hidup = $this->get_pasien_keluar_hidup($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan);
		$this->pdf->writeHTML($data_pasien_keluar_hidup,true,false,false,false);	
		
		//$this->pdf->AddPage('L', 'F4', false, false);	
		$data_pasien_meninggal_kurang_48_jam = $this->get_pasien_meninggal_kurang_48_jam($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan);
		$this->pdf->writeHTML($data_pasien_meninggal_kurang_48_jam,true,false,false,false);	
		
		//$this->pdf->AddPage('L', 'F4', false, false);	
		$data_pasien_meninggal_lebih_sm_dgn_48_jam = $this->get_pasien_meninggal_lebih_sm_dgn_48_jam($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan);
		$this->pdf->writeHTML($data_pasien_meninggal_lebih_sm_dgn_48_jam,true,false,false,false);	
		
		//$this->pdf->AddPage('L', 'F4', false, false);	
		$data_pasien_hari_ini = $this->get_pasien_hari_ini($cbxtgl,$cbxjam,$cbxruangan,$tgl,$jam,$ruangan);
		$this->pdf->writeHTML($data_pasien_hari_ini,true,false,false,false);

		for($a=1;$a<$before;$a++){
			$this->pdf->Cell(110,5,'',0,1,'L');//space
		}
		$this->pdf->Cell(110,5,'',0,0,'L');
        $this->pdf->Cell(110,5,'',0,0,'L');
        $this->pdf->Cell(80,5,'Bandung, '.$this->TanggalIndo(date("Ymd")),0,1,'C');

		for($a=0;$a<$between;$a++){
			$this->pdf->Cell(110,5,'',0,1,'L');//space
		}
		
		$this->pdf->Cell(110,5,'',0,0,'L');
        $this->pdf->Cell(110,5,'',0,0,'L');
		$titik = "";
		for ($a=1;$a<=55;$a++) {
			$titik .= ".";
		}
        $this->pdf->Cell(80,5,'('.$titik.')',0,1,'C');
		$this->pdf->Cell(110,5,'',0,0,'L');
        $this->pdf->Cell(110,5,'',0,0,'L');
        $this->pdf->Cell(80,5,'Petugas Registrasi',0,1,'C');
		
		$this->pdf->Output('laporan_sensus_harian.pdf', 'I');
    
       
	}
}

?>