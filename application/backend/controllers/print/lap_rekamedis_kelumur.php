<?php 
	class Lap_rekamedis_kelumur extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function pasienkeluar($tglawal,$tglakhir){
		
		$this->db->select("*");
		$this->db->from("v_penyakitterbanyakri");
	//	$this->db->groupby("nopo");
		$this->db->where("tglmasuk BETWEEN '".$tglawal."' and '".$tglakhir."'");
	//	$this->db->where('idbagian',$ruangan);
		$query = $this->db->get();
		$aa = $query->row_array();
			//var_dump($tglawal);
			//var_dump($tglakhir);
		
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf->AddPage('L', $page_format, false, false);
	//	$this->pdf->SetAutoPageBreak(TRUE, 0);
		$this->pdf->SetFont('helvetica', '', 14);
	//	$this->pdf->AddPage();
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Penyakit Terbanyak Per Kelompok Umur RI', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->Cell(0, 0, 'Tanggal : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
	//	$this->pdf->Cell(0, 0, 'Ruangan : '. $aa['nmbagian'], 0, 1, 'C', 0, '', 0);
		$isi ='';
		$isi1 ='';
		
		$this->pdf->SetMargins(PDF_MARGIN_LEFT+30, PDF_MARGIN_TOP-45, PDF_MARGIN_RIGHT);
		$this->pdf->SetAutoPageBreak(TRUE, 0);
		
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			
			if($r['umurhari'] == 0 || $r['umurhari'] < 28 && $r['umurtahun'] == 0 || $r['umurtahun'] == 0){
			$isi .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
			</tr>";
			}
		}
	}
		$heads = "<br><br><font size=\"7\" face=\"Helvetica\"> 
				NOL - 28Hari<br>
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>	
					</tr>
					".$isi."
                    
		</table></font>";
		$this->pdf->writeHTML($heads,true,false,false,false); 
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			
			if($r['umurhari'] >= 28 && $r['umurtahun'] <= 1){
			$isi1 .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
			</tr>";
			}
		}
	}
		$umur1 = "<br><br><font size=\"7\" face=\"Helvetica\"> 
				28Hari - 1 Tahun<br>
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>	
					</tr>
					".$isi1."
                    
		</table></font>";
		$this->pdf->writeHTML($umur1,true,false,false,false); 
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			
			if($r['umurtahun'] == 1 && $r['umurtahun'] <= 4){
			$isi1 .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
			</tr>";
			}
		}
	}
		$umur2 = "<br><br><font size=\"7\" face=\"Helvetica\"> 
				1 Tahun - 4 Tahun<br>
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>	
					</tr>
					".$isi1."
                    
		</table></font>";
		$this->pdf->writeHTML($umur2,true,false,false,false); 
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			
			if($r['umurtahun'] == 5 || $r['umurtahun'] >= 14){
			$no = 0;
			$isi1 .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
			</tr>";
			}
		}
	}
		$umur3 = "<br><br><font size=\"7\" face=\"Helvetica\"> 
				3 Tahun - 14 Tahun<br>
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>	
					</tr>
					".$isi1."
                    
		</table></font>";
		$this->pdf->writeHTML($umur3,true,false,false,false); 
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			
			if($r['umurtahun'] == 15 || $r['umurtahun'] >= 24){
			$isi1 .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
			</tr>";
			}
		}
	}
		$umur4 = "<br><br><font size=\"7\" face=\"Helvetica\"> 
				15 Tahun -  24 Tahun<br>
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>	
					</tr>
					".$isi1."
                    
		</table></font>";
		$this->pdf->writeHTML($umur4,true,false,false,false); 
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			
			if($r['umurtahun'] == 25 || $r['umurtahun'] == 44){
			$isi1 .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
			</tr>";
			}
		}
	}
		$umur5 = "<br><br><font size=\"7\" face=\"Helvetica\"> 
				25 Tahun- 44 Tahun<br>
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>	
					</tr>
					".$isi1."
                    
		</table></font>";
		$this->pdf->writeHTML($umur5,true,false,false,false); 
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			
			if($r['umurtahun'] == 45 || $r['umurtahun'] == 64){
			$isi1 .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
			</tr>";
			}
		}
	}
		$umur6 = "<br><br><font size=\"7\" face=\"Helvetica\"> 
				45 Tahun - 64 Tahun<br>
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>	
					</tr>
					".$isi1."
                    
		</table></font>";
	//	$this->pdf->writeHTML($umur6,true,false,false,false); 
		
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		/* 	if($r['idbagiankirim'] != $stkeluar){
				$stkeluar = $r['idbagiankirim'];
				$isi .= "<tr><td colspan=\"13\">Ruangan: ".$r['nmbagian']."</td></tr>";
				$no = 0;
			} */
			
			if($r['umurtahun'] >= 100){
			$isi1 .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
			</tr>";
			}
		}
	}
		$umur7 = "<br><br><font size=\"7\" face=\"Helvetica\"> 
				65 Tahun ke atas<br>
                <table border=\"1\">
					<tr align=\"center\">
                        <th width=\"3%\"><br><br>NO</th>
                        <th ><br><br>KODE I CD-X </th>
                        <th width=\"30%\"><br><br>NAMA PENYAKIT</th>
                        <th width=\"10%\" colspan=\"2\">JENIS KELAMIN</th>
                        <th width=\"12%\"><br><br>JUMLAH PASIEN KELUAR</th>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th width=\"5%\" align=\"center\">L</th>
                        <th width=\"5%\" align=\"center\">P</th>
						<th></th>	
					</tr>
					".$isi1."
                    
		</table></font>";
	//	$this->pdf->writeHTML($umur7,true,false,false,false); 
		
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>