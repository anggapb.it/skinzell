<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reg_ri extends Controller{
	
	 function __construct(){
        parent::__construct();
		$this->load->library('pdf');
		$this->load->library('rhrpt');
		$this->load->library('rhlib');
        
    }
	
	function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
        }
    
    function get_bilangan($num){
		return $this->rhlib->get_bilangan($num);
	}
	
	function get_par_val($table,$fieldName,$condition){		
        return $this->rhlib->get_par_val($table,$fieldName,$condition);
    }
	function get_row_pars($table,$fields,$condition){
		return $this->rhlib->get_row_pars($table,$fields,$condition);
    }
    
    function printRI($noreg){
        
        $row = $this->get_row_pars("v_registrasi_copy","noreg,nmpasien,norm,nmdokter,alamatp,nmibu
									,tglreg,jamreg,nmjnskelamin,tptlahirp,tgllahirp
									,nmstkawin,kelurahan,kec,kota
									,notelpp,nohpp,nmagama
									,nmgoldarah,nmpendidikan,nmpekerjaan
									,nmsukubangsa,negara,noidentitas
									,alergi,nmpenjamin, nmcaradatang,nmbagian,kdkamar","noreg='$noreg'");
		
        $tglreg = $row->tglreg;
        $jamreg = $row->jamreg;
        $noreg = $row->noreg;
        $alamatp = $row->alamatp;
        $nmpasien = $row->nmpasien;
        $norm = $row->norm;
        $nmdokter = $row->nmdokter;
        $nmibu = $row->nmibu;
        $nmjnskelamin = $row->nmjnskelamin;
        $tptlahirp = $row->tptlahirp;
        $tgllahirp = $row->tgllahirp;
        $nmstkawin = $row->nmstkawin;
        $kelurahan = $row->kelurahan;
        $kec = $row->kec;
        $kota = $row->kota;
        $notelpp = $row->notelpp;
        $nohpp = $row->nohpp;
        $nmagama = $row->nmagama;
        $nmgoldarah = $row->nmgoldarah;
        $nmpendidikan = $row->nmpendidikan;
        $nmpekerjaan = $row->nmpekerjaan;
        $nmsukubangsa = $row->nmsukubangsa;
        $negara = $row->negara;
        $noidentitas = $row->noidentitas;
        $alergi = $row->alergi;
        $nmpenjamin = $row->nmpenjamin;
        $nmcaradatang = $row->nmcaradatang;
        $nmbagian = $row->nmbagian;
        $kdkamar = $row->kdkamar;
        
       
        
        $this->pdf->SetCreator(PDF_CREATOR);
        
        // set default header data  
        $this->pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH,PDF_HEADER_TITLE,PDF_HEADER_STRING);
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);
        // set header and footer fonts
        $this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
        $this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        
        // set default monospaced font
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        
        // set margins
        $this->pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
        $this->pdf->SetHeaderMargin('10');
        $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        // set auto page breaks
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        
        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        
        // set some language-dependent strings (optional)

        // ---------------------------------------------------------
        
        // set font
        $this->pdf->SetCellPadding(0);
        // add a page
        $this->pdf->AddPage('P','F4');
        
        $this->pdf->SetFont('helvetica', 'B', 9);
        $this->pdf->Write(0, 'RSKIA HARAPAN BUNDA BANDUNG', '', 0, '', true, 0, false, false, 0);
        
		$this->pdf->SetFont('helvetica', '', 10);
        $tbl = 
        '
<p align="center" style="font-family: Arial, Helvetica, sans-serif; 
		  font-weight: bold;
	      font-size: 12px;">LEMBARAN MASUK </p>
<table border="1">
  <tr>
    <td height="20">&nbsp;Tanggal Masuk</td>
    <td>&nbsp;'.$this->TanggalIndo(date("Ymd",strtotime($tglreg))).'</td>
    <td>&nbsp;No. RM </td>
    <td>&nbsp;'.$norm.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Jam Masuk</td>
    <td>&nbsp;'.$jamreg.'</td>
    <td>&nbsp;No. Reg </td>
    <td>&nbsp;'.$noreg.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Tanggal Keluar</td>
    <td></td>
    <td>&nbsp;Nama Dokter </td>
    <td>&nbsp;'.$nmdokter.'</td>
  </tr>
</table>
<p style="font-style: italic;">* data diisi dengan lengkap sesuai dengan kartu identitas tanpa singkatan, coret yang tidak perlu</p>
<table border="1">
  <tr>
    <td height="20">&nbsp;Nama Pasien </td>
    <td colspan="3">&nbsp;'.$nmpasien.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Suami / Orang Tua </td>
    <td colspan="3">&nbsp;'.$nmibu.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Jenis Kelamin </td>
    <td>&nbsp;'.$nmjnskelamin.'</td>
    <td>&nbsp;Status Kawin </td>
    <td>&nbsp;'.$nmstkawin.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Tempat Lahir </td>
    <td>&nbsp;'.$tptlahirp.'</td>
    <td>&nbsp;Tanggal Lahir </td>
    <td>&nbsp;'.$this->TanggalIndo(date("Ymd",strtotime($tgllahirp))).'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Alamat</td>
    <td colspan="3">&nbsp;'.$alamatp.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Kota</td>
    <td>&nbsp;'.$kota.'</td>
    <td>&nbsp;Kecamatan</td>
    <td>&nbsp;'.$kec.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;Kelurahan</td>
    <td>&nbsp;'.$kelurahan.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;No Handphone </td>
   <td colspan="3">&nbsp;'.$nohpp.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;No Telephone </td>
    <td colspan="3">&nbsp;'.$notelpp.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Agama</td>
    <td colspan="3">&nbsp;'.$nmagama.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Gol Darah </td>
    <td colspan="3">&nbsp;'.$nmgoldarah.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Pendidikan</td>
    <td colspan="3">&nbsp;'.$nmpendidikan.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Pekerjaan</td>
    <td colspan="3">&nbsp;'.$nmpekerjaan.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Suku Bangsa </td>
    <td colspan="3">&nbsp;'.$nmsukubangsa.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;No KTP / SIM / Paspor </td>
   <td colspan="3">&nbsp;'.$noidentitas.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Kebangsaan</td>
    <td colspan="3">&nbsp;'.$negara.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Riwayat Alergi </td>
    <td colspan="3">&nbsp;'.$alergi.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Penjamin</td>
    <td colspan="3">&nbsp;'.$nmpenjamin.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Cara Datang </td>
    <td colspan="3">&nbsp;'.$nmcaradatang.'</td>
  </tr>
  <tr>
    <td height="20">&nbsp;Nama Ruangan </td>
    <td>&nbsp;'.$nmbagian.'</td>
    <td>&nbsp;Nomor Bed </td>
    <td>&nbsp;'.$kdkamar.'</td>
  </tr>
</table>
<br />
<br />
<table border="1">
  <tr>
    <td>
	<p style="font-weight: bold;" align="center"> PERNYATAAN PERSETUJUAN RAWAT INAP, PENGOBATAN & TINDAKAN MEDIS</p>
	<br />
      <p align="left">&nbsp;&nbsp;Saya yang bertanda tangan di bawah ini selaku penanggung jawab pasien di atas</p>
       <table width="640" border="0">
        <tr>
          <td height="25" width="184">&nbsp;&nbsp;Nama</td>
          <td width="440">..............................................................................................................................</td>
        </tr>
        <tr>
          <td height="25">&nbsp;&nbsp;Alamat</td>
          <td>..............................................................................................................................</td>
        </tr>
        <tr>
          <td height="25">&nbsp;&nbsp;..................................................</td>
          <td>............. Telp: .......................................................................................................</td>
        </tr>
        <tr>
          <td height="25">&nbsp;&nbsp;No KTP </td>
          <td>..............................................................................................................................</td>
        </tr>
        <tr>
          <td height="25">&nbsp;&nbsp;Hubungan dengan Pasien </td>
          <td>..............................................................................................................................</td>
        </tr>
      </table>
	  <br/>
      <p align="left">&nbsp;&nbsp;Meminta pasien tersebut di atas untuk dirawat di RSKIA Harapan Bunda Bandung sesuai ruangan dan Kelas &nbsp;&nbsp;yang
      tertulis di atas, dan saya bertanggung jawab terhadap semua pembiayaan rumah sakit selama yang &nbsp;&nbsp;bersangkutan
      dalam perawatan RSKIA Harapan Bunda Bandung, dan juga saya</p>
      <p align="center"><b>MENYETUJUI / TIDAK MENYETUJUI</b></p>
      <p align="left">&nbsp;&nbsp;dilakukan pengobatan dan tindakan medis terhadap pasien tersebut</p>
      <table>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;Bandung , ........................................................ </td>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;Mengetahui Petugas Penerima </td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;Yang Membuat Pernyataan </td>
        </tr>
        <tr>
          <td height="70">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;(...................................................)</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;(.............................................................)</td>
        </tr>
      </table>
      <br />
      <br />
	  </td>
  </tr>
</table>';
        //$this->pdf->SetFont('times', '', 11);
        $this->pdf->writeHTML($tbl,true,false,false,false);
        
        $this->pdf->Output('cetak_registrasi_ri.pdf', 'I');
        
        
    }
    
    




}