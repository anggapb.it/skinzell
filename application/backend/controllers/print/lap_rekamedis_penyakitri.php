<?php 
	class Lap_rekamedis_penyakitri extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function pasienkeluar($tglakhir,$tglawal){
		
		$this->db->select("*");
		$this->db->from("v_penyakitterbanyakri");
	//	$this->db->groupby("nopo");
	//	$this->db->where("tglmasuk BETWEEN '".$tglawal."' and '".$tglakhir."'");
		$query = $this->db->get();
		$po = $query->result();
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
	//	$this->pdf->AddPage();
		$this->pdf->SetMargins(PDF_MARGIN_LEFT+29, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN PENYAKIT TERBANYAK RAWAT INAP', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 7);
		
		$this->pdf->Cell(0, 3, 'TANGGAL : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
        $this->pdf->SetFont('helvetica', '', 7);
	//	$this->pdf->Cell(0, 5, 'Jenis Diagnosa : '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
        
		$isi ='';
	if($query->num_rows>0){
	$no = 0;
	$dokter = 0;

		foreach($query->result_array() as $i=>$r){
		/* if($r['iddokter'] != $dokter){
			$dokter = $r['iddokter'];
			$isi .= "<tr>
				<td colspan =\"18\">Nama Dokter : ".$r['nmdoktergelar']."</td>
			</tr>";
			
		} */
		$no = (1+ $i);
		if($r['umurhari'] == 0 || $r['umurhari'] < 28 && $r['umurtahun'] == 0 || $r['umurtahun'] == 0){
			$satu = 1;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if(29 <= 29 && 1 < 1){
			$satu = 0;
			$dua = 1;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 1 || 1 <= 4){
			$satu = 0;
			$dua = 0;
			$tiga = 1;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 5 || $r['umurtahun'] <= 14){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 1;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 15 || $r['umurtahun'] <= 24){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 1;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 25 || $r['umurtahun'] <= 44){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 1;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 45 || $r['umurtahun'] <= 64){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 1;
			$delapan = 0;
		}else if($r['umurtahun'] >= 65){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 1;
		}
			$isi .="<tr>
						<td align=\"center\">".$no."</td>
						<td align=\"center\">".$r['kdpenyakit']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td align=\"center\">".$satu."</td>
						<td align=\"center\">".$dua."</td>
						<td align=\"center\">".$tiga."</td>
						<td align=\"center\">".$empat."</td>
						<td align=\"center\">".$lima."</td>
						<td align=\"center\">".$enam."</td>
						<td align=\"center\">".$tujuh."</td>
						<td align=\"center\">".$delapan."</td>
						<td align=\"center\">".$r['perempuan']."</td>
						<td align=\"center\">".$r['laki']."</td>
						<td align=\"center\">".$r['pasienkeluar']."</td>
						<td align=\"center\">".$r['mati']."</td>
			</tr>";
		}
	}
		$heads = "<br><br><font size=\"7\" face=\"Helvetica\"> 
        
                <table border=\"1\">
					<tr align=\"center\">
                        <th rowspan=\"2\" width=\"3%\"><br><br>NO</th>
                        <th rowspan=\"2\"><br><br>KODE I CD-X </th>
                        <th rowspan=\"2\" width=\"20%\"><br><br>NAMA PENYAKIT</th>
                        <th colspan=\"8\" width=\"24%\">KELOMPOK UMUR</th>
                        <th colspan=\"2\" width=\"6%\">JENIS KELAMIN</th>
                        <th rowspan=\"2\" width=\"10%\"><br><br>JUMLAH PASIEN KELUAR</th>
                        <th rowspan=\"2\" width=\"10%\"><br><br>JUMLAH PASIEN KELUAR MATI</th>
					</tr>
                    <tr align=\"center\">
                        <th width=\"3%\">0-28 HR</th>
                        <th width=\"3%\">28HR - 1 TH</th>
                        <th width=\"3%\">1 - 4 TH</th>
                        <th width=\"3%\">5 - 14 TH</th>
                        <th width=\"3%\">14-24 TH</th>
                        <th width=\"3%\">25-44 TH</th>
                        <th width=\"3%\">45-64 TH</th>
                        <th width=\"3%\">65 +</th>
                        <th width=\"3%\">L</th>
                        <th width=\"3%\">P</th>
                    </tr>".$isi."
                    
		</table></font>";
		
		
		$this->pdf->writeHTML($heads,true,false,false,false);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>