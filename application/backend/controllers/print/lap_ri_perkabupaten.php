<?php 
class Lap_ri_perkabupaten extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
	
	function rawatinap($tglawal,$tglakhir){
		
		$this->db->select("*");
		$this->db->from("v_daerah");
	//	$this->db->groupby("nopo");
		$this->db->where("tglmasuk BETWEEN '".$tglawal."' and '".$tglakhir."'");
	//	$this->db->where('idbagiankirim',$ruangan);
		$query = $this->db->get();
		$aa = $query->row_array();
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
		
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'PENERIMAAN RAWAT INAP PER KABUPATEN', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetMargins(PDF_MARGIN_LEFT+30, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$isi = '';
		$total = 0;
	if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		  	if($r['idprop'] != $stkeluar){
				$stkeluar = $r['idprop'];
				$isi .= "<tr><td colspan=\"2\">Provinsi: ".$r['prop']."</td></tr>";
				$no = 0;
			}  
			$isi .= "<tr>
					<td align=\"center\">".$r['kota']."</td>
					<td align=\"right\">".$r['jumlah']."</td>
			</tr>";
			
			$total += $r['jumlah'];
			}
		}
		
		$html = "<br><br><br><table border=\"1\" >
					<tr  bgcolor=\"#cccccc\">
						<td width=\"63%\" align=\"center\">Nama Kota / Kabupaten</td>
						<td width=\"20%\" align=\"center\">Jumlah</td>
					</tr>
					".$isi."
					<tr>
						<td align=\"right\">Total</td>
						<td align=\"right\">".number_format($total,0,',','.')."</td>
					
					</tr>
				</table>	
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);

		//Close and output PDF document
		$this->pdf->Output('RJ_PerKab.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
		}
}
?>