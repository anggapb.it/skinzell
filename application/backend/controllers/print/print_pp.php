<?php 
	class Print_pp extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_pp');        
		}

	function pp_pdf($nopp){
		$this->pdf_pp->SetPrintFooter(true);
		$this->db->select("*");
		$this->db->from("pp");		
		$this->db->join('bagian',
				'bagian.idbagian = pp.idbagian', 'left');
		$this->db->join('stsetuju',
				'stsetuju.idstsetuju = pp.idstsetuju', 'left');
		$this->db->join('pengguna',
				'pengguna.userid = pp.userid', 'left');
		$this->db->join('supplier',
				'supplier.kdsupplier = pp.kdsupplier', 'left');
		$this->db->join('stpo',
                'stpo.idstpo = pp.idstpo', 'left');
		
		$this->db->where('pp.nopp', $nopp);
		$query = $this->db->get();
        $nopp = $query->row_array();
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 148),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => -90,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_pp->SetFooterMargin(10); // margin footer 1 CM
		$this->pdf_pp->setPrintFooter(true); // enabled ? true
		//$this->pdf_pp->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_pp->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
				
			// add a page
        //$this->pdf_pp->AddPage();		
		$this->pdf_pp->AddPage('P', $page_format, false, false);
        $this->pdf_pp->SetFont('helvetica', '', 9);
		$kop = "<br>
				<table border=\"0\">
					<tr align=\"left\">
						<td width=\"0.7%\"></td>
						<td width=\"99.5%\"><font size=\"13\" face=\"Helvetica\"><b>RSIA HARAPAN BUNDA</b></font></td>
					</tr>
					<tr align=\"left\">
						<td></td>
						<td><font size=\"11\" face=\"Helvetica\"><b>dr. Bambang Suhardijant, SpOg</b></font></td>
					</tr>
					<tr align=\"left\">
						<td></td>
						<td>Pluto Raya Blok C Margahayu Raya Bandung</td>
					</tr>
					<tr align=\"left\">
						<td><font size=\"9\" face=\"Helvetica\"></font><hr height=\"2\"></td>
						<td>Telp. (022) 7506490 Fax (022) 7514712<hr height=\"2\"></td>
					</tr>
					<tr align=\"center\">
						<td></td>
						<td><h3><b><i>Surat Pemesanan Barang</i></b></h3></td>
					</tr>
				</table>
		";
     	$this->pdf_pp->writeHTML($kop,true,false,false,false);
		
		$kop2 = "<br><br>
				 <table border=\"0\">
					<tr align=\"left\">
						<td width=\"2%\"></td>
						<td width=\"15%\"><font size=\"9\" face=\"Helvetica\"><b>Kepada Yth.</b></font></td>
						<td width=\"1%\"></td>
						<td width=\"41%\"></td>
						<td width=\"2.5%\"></td>
						
						<td width=\"16%\"><font size=\"8\" face=\"Helvetica\">NO. Pesanan</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"38%\"><font size=\"9\" face=\"Helvetica\"><b>".$nopp["nopp"]."</b></font></td>
					</tr>
					<tr>
						<td width=\"2%\"></td>
						<td width=\"36%\"><font size=\"9\" face=\"Helvetica\"><b>".$nopp["nmsupplier"]."<hr></b>Tlp &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;".$nopp["notelp"]."</font></td>
						<td width=\"1.5%\"></td>
						<td width=\"19.5%\"></td>
						<td width=\"2.5%\"></td>
						
						<td width=\"16%\"><font size=\"8\" face=\"Helvetica\">Tanggal Kirim Persetujuan</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">: &nbsp; :</font></td>
						<td width=\"19%\"><font size=\"9\" face=\"Helvetica\">".date("d-m-Y", strtotime($nopp['tglpp']))." ".$nopp['nmstsetuju']."</font></td>
					</tr>
					<tr>
						<td width=\"2%\"></td>
						<td width=\"8%\"><font size=\"9\" face=\"Helvetica\">Fax<hr>NPWP</font></td>
						<td ><font size=\"9\" face=\"Helvetica\">.<hr>:</font></td>
						<td width=\"26.5%\"><font size=\"9\" face=\"Helvetica\">".$nopp["nofax"]."<hr>".$nopp["npwp"]."</font></td>
						<td width=\"23.5%\"></td>
						
						<td><font size=\"8\" face=\"Helvetica\">User Input</font></td>
						<td><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"30%\"><font size=\"9\" face=\"Helvetica\">".$nopp['nmlengkap']."</font></td>
					</tr>
				 </table>
			";
		$this->pdf_pp->writeHTML($kop2,true,false,false,false);
	//	var_dump($nopo);
				$isi = '';
				$this->db->select("*");
				$this->db->from("v_ppdetail");
				$this->db->where('nopp',$nopp['nopp']);
			//	var_dump($nopo);
				$querys = $this->db->get();
				
			//	$ambil = $querys->row_array();
				$aaa = $querys->result();
				$no = 1;
				foreach($aaa as $i=>$val){
				$isi .= "<tr>
								<td width=\"7%\"><font size=\"8\" face=\"Helvetica\" align=\"center\">".$no++.".</font></td>
								<td width=\"58%\"><font size=\"8\" face=\"Helvetica\"> ".$val->nmbrg."</font></td>
								<td width=\"20%\"><font size=\"8\" face=\"Helvetica\" align=\"center\"> ".$val->nmsatuan."</font></td>
								<td width=\"15%\"><font size=\"8\" face=\"Helvetica\" align=\"right\">".$val->qty."&nbsp;&nbsp;</font></td>
						</tr>";
				}
				$detail = "<table border=\"1\">
						<thead>
							<tr align=\"center\">
								<th width=\"7%\">No.</th>
								<th width=\"58%\">Nama Barang</th>
								<th width=\"20%\">Satuan</th>
								<th width=\"15%\">Qty</th>
							</tr>
						</thead>".$isi."
						   </table>
				";
		$this->pdf_pp->writeHTML($detail,true,false,false,false);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td width=\"20%\"><b>Yang Meminta</b></td>
				<td></td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><font size=\"9\" face=\"Helvetica\">".$nopp['nmlengkap']."</font><hr></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf_pp->writeHTML($approve,true,false,false,false);
		$this->pdf_pp->Output('spb.pdf', 'I');
	}
}

?>