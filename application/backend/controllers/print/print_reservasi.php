<?php 
class Print_reservasi extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
	function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
    }
	
	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }

	function get_data($tgl,$idbagian,$iddokter){
		$optbagian = ($idbagian!="null") ? "=":"<>";
		$optdokter = ($iddokter!="null") ? "=":"<>";
		$query  = $this->db->query("SELECT b.nmbagian
				 , d.nmdoktergelar
				 , r.noantrian
				 , r.norm
				 , r.nmpasien
				 , r.nohp
				 , rd.noreg
				 , s.nmstreservasi
				 , if(r.stdatang=1,'Ya','Tidak') AS stdatang
			FROM
			  reservasi r
			LEFT JOIN bagian b
			ON b.idbagian = r.idbagian
			LEFT JOIN dokter d
			ON d.iddokter = r.iddokter
			LEFT JOIN registrasidet rd
			ON rd.idregdet = r.idregdet
			LEFT JOIN streservasi s
			ON s.idstreservasi = r.idstreservasi
			WHERE r.tglreservasi = '".$tgl."'
			  AND r.idbagian ".$optbagian." '".$idbagian."'
			  AND r.iddokter ".$optdokter." '".$iddokter."'
			ORDER BY
			  r.tglreservasi DESC, r.idbagian, r.iddokter, r.noantrian DESC");
		$result = array();
		if ($query->num_rows() > 0) {
			$result = $query->result();
		}
		
		$rows = "";
		$unit_dokter = "";
		$no = 1;
		$nodata = 1;
		foreach($result as $item) {
			if($item->nmbagian.' - '.$item->nmdoktergelar == $unit_dokter){
				$rows .= "<tr>
						 <td  align=\"center\">".$nodata++.".</td>
						 <td  align=\"center\">".$item->noantrian."</td>
						 <td  align=\"center\">".$item->norm."</td>
						 <td  align=\"left\">".$item->nmpasien."</td>
						 <td  align=\"center\">".$item->nohp."</td>
						 <td  align=\"center\">".$item->noreg."</td>
						 <td  align=\"center\">".$item->nmstreservasi."</td>
						 <td  align=\"center\">".$item->stdatang."</td>
						 </tr>";
			} else {
				$nodata = 1;
				$unit_dokter = $item->nmbagian.' - '.$item->nmdoktergelar;
				$rows .= "<tr>
						 <td  align=\"left\" colspan=\"7\"><b>".$no++.". $unit_dokter</b></td>
						 </tr>
						 <tr>
						 <td  align=\"center\">".$nodata++.".</td>
						 <td  align=\"center\">".$item->noantrian."</td>
						 <td  align=\"center\">".$item->norm."</td>
						 <td  align=\"left\">".$item->nmpasien."</td>
						 <td  align=\"center\">".$item->nohp."</td>
						 <td  align=\"center\">".$item->noreg."</td>
						 <td  align=\"center\">".$item->nmstreservasi."</td>
						 <td  align=\"center\">".$item->stdatang."</td>
						 </tr>";
			}
		}
		
		return $rows;

	}

	function cetak_reservasi($tgl,$idbagian,$iddokter){
		
		$nmbagian = $this->searchId("nmbagian", "bagian", "idbagian", $idbagian);
		$nmdokter = $this->searchId("nmdoktergelar", "dokter", "iddokter", $iddokter);
		
		$this->pdf->SetMargins('8', '40', '10');
		$this->pdf->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf->setPrintFooter(true); // enabled ? true
		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf->AddPage('P', 'F4', false, false); 
		
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Cell(0, 0, 'RESERVASI RJ (Rawat Jalan)', 0, 1, 'C', 0, '', 0);
		$this->pdf->Cell(0, 0, 'Tanggal '.$this->TanggalIndo(date("Ymd",strtotime($tgl))), 0, 1, 'C', 0, '', 0);
		if ($nmbagian) {
			$this->pdf->Cell(0, 0, 'UNIT PELAYANAN '.$nmbagian, 0, 1, 'C', 0, '', 0);
		}
		if ($nmdokter) {
			$this->pdf->Cell(0, 0, 'DOKTER '.$nmdokter, 0, 1, 'C', 0, '', 0);
		}
		
		$this->pdf->SetFont('helvetica', '', 8);
		
		$get_rows = $this->get_data($tgl,$idbagian,$iddokter);
		$datatable = <<<EOD
	<br>
    <br>
    <table border="1" cellpadding="2" nobr="true">
     <tr>
	  <th width="4%" align="center"><b>No.</b></th>
	  <th width="11%" align="center"><b>No. Antrian</b></th>
	  <th width="12%" align="center"><b>No. RM</b></th>
	  <th width="28%" align="center"><b>Nama Pasien</b></th>
      <th width="14%" align="center"><b>No. Handphone</b></th>
	  <th width="12%" align="center"><b>No. Registrasi</b></th>
	  <th width="12%" align="center"><b>Status</b></th>
      <th width="8%" align="center"><b>Datang</b></th>
     </tr> 
		$get_rows 
    </table>
	
EOD;
		$this->pdf->writeHTML($datatable,true,false,false,false);
		
		$this->pdf->Output('reservasi-'.$tgl.'.pdf', 'I');
    
       
	}
}

?>