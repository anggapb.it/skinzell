<?php 
	class Print_honordokter extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
		
		function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
        }
		
		function namaBulanTahun($tgl){
			$arrtgl = explode('-', $tgl);
			$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
			$bulan = $query->row_array();
			$tanggalInd = $bulan['nmbulan'] .' '. $arrtgl[0];
			
			return $tanggalInd ;
		}
		
		function rp_satuan($angka,$debug){
			$terbilang = '';
			$a_str['1']="Satu";
			$a_str['2']="Dua";
			$a_str['3']="Tiga";
			$a_str['4']="Empat";
			$a_str['5']="Lima";
			$a_str['6']="Enam";
			$a_str['7']="Tujuh";
			$a_str['8']="Delapan";
			$a_str['9']="Sembilan";
			
			
			$panjang=strlen($angka);
			for ($b=0;$b<$panjang;$b++)
			{
				$a_bil[$b]=substr($angka,$panjang-$b-1,1);
			}
			
			if ($panjang>2)
			{
				if ($a_bil[2]=="1")
				{
					$terbilang=" Seratus";
				}
				else if ($a_bil[2]!="0")
				{
					$terbilang= " ".$a_str[$a_bil[2]]. " Ratus";
				}
			}

			if ($panjang>1)
			{
				if ($a_bil[1]=="1")
				{
					if ($a_bil[0]=="0")
					{
						$terbilang .=" Sepuluh";
					}
					else if ($a_bil[0]=="1")
					{
						$terbilang .=" Sebelas";
					}
					else 
					{
						$terbilang .=" ".$a_str[$a_bil[0]]." Belas";
					}
					return $terbilang;
				}
				else if ($a_bil[1]!="0")
				{
					$terbilang .=" ".$a_str[$a_bil[1]]." Puluh";
				}
			}
			
			if ($a_bil[0]!="0")
			{
				$terbilang .=" ".$a_str[$a_bil[0]];
			}
			return $terbilang;
		   
		}
		
		function rp_terbilang($angka,$debug){
			$terbilang = '';
			
			$angka = str_replace(".",",",$angka);
			
			list ($angka) = explode(",",$angka);
			$panjang=strlen($angka);
			for ($b=0;$b<$panjang;$b++)
			{
				$myindex=$panjang-$b-1;
				$a_bil[$b]=substr($angka,$myindex,1);
			}
			if ($panjang>9)
			{
				$bil=$a_bil[9];
				if ($panjang>10)
				{
					$bil=$a_bil[10].$bil;
				}

				if ($panjang>11)
				{
					$bil=$a_bil[11].$bil;
				}
				if ($bil!="" && $bil!="000")
				{
					$terbilang .= $this->rp_satuan($bil,$debug)." milyar";
				}
				
			}

			if ($panjang>6)
			{
				$bil=$a_bil[6];
				if ($panjang>7)
				{
					$bil=$a_bil[7].$bil;
				}

				if ($panjang>8)
				{
					$bil=$a_bil[8].$bil;
				}
				if ($bil!="" && $bil!="000")
				{
					$terbilang .= $this->rp_satuan($bil,$debug)." Juta";
				}
				
			}
			
			if ($panjang>3)
			{
				$bil=$a_bil[3];
				if ($panjang>4)
				{
					$bil=$a_bil[4].$bil;
				}

				if ($panjang>5)
				{
					$bil=$a_bil[5].$bil;
				}
				if ($bil!="" && $bil!="000")
				{
					$terbilang .= $this->rp_satuan($bil,$debug)." Ribu";
				}
				
			}

			$bil=$a_bil[0];
			if ($panjang>1)
			{
				$bil=$a_bil[1].$bil;
			}

			if ($panjang>2)
			{
				$bil=$a_bil[2].$bil;
			}
			//die($bil);
			if ($bil!="" && $bil!="000")
			{
				$terbilang .= $this->rp_satuan($bil,$debug);
			}
			return trim($terbilang)." Rupiah";
		}
		
		function get_hondok($nohondok){
		
			$query  = $this->db->query("SELECT * FROM v_hondok WHERE nohondok='$nohondok'");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_rj($iddokter,$tglawal,$tglakhir){
		
			$query  = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
			 , `notadet`.`nonota` AS `nonota`
			 , `notadet`.`kditem` AS `kditem`
			 , `pelayanan`.`pel_kdpelayanan` AS `pel_kdpelayanan`
			 , `notadet`.`idjnstarif` AS `idjnstarif`
			 , `notadet`.`koder` AS `koder`
			 , `notadet`.`idsatuan` AS `idsatuan`
			 , `notadet`.`qty` AS `qty`
			 , `notadet`.`tarifjs` AS `tarifjs`
			 , `notadet`.`tarifjm` AS `tarifjm`
			 , `notadet`.`tarifjp` AS `tarifjp`
			 , `notadet`.`tarifbhp` AS `tarifbhp`
			 , `notadet`.`diskonjs` AS `diskonjs`
			 , `notadet`.`diskonjm` AS `diskonjm`
			 , `notadet`.`diskonjp` AS `diskonjp`
			 , `notadet`.`diskonbhp` AS `diskonbhp`
			 , `notadet`.`uangr` AS `uangr`
			 , `notadet`.`hrgjual` AS `hrgjual`
			 , `notadet`.`hrgbeli` AS `hrgbeli`
			 , `notadet`.`iddokter` AS `iddokter`
			 , `notadet`.`idperawat` AS `idperawat`
			 , `notadet`.`idstbypass` AS `idstbypass`
			 , `notadet`.`aturanpakai` AS `aturanpakai`
			 , `notadet`.`dijamin` AS `dijamin`
			 , `notadet`.`stdijamin` AS `stdijamin`
			 , `registrasidet`.`noreg` AS `noreg`
			 , `registrasidet`.`tglmasuk` AS `tglmasuk`
			 , `registrasidet`.`tglkeluar` AS `tglkeluar`
			 , `registrasidet`.`tglreg` AS `tglreg`
			 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
			 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
			 , `pasien`.`nmpasien` AS `nmpasien`
			 , `registrasidet`.`idbagian` AS `idbagian`
			 , `bagian`.`nmbagian` AS `nmbagian`
			 , sum((
			   CASE
			   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000001') THEN
				 (`notadet`.`tarifjm` * `notadet`.`qty`)
			   ELSE
				 0
			   END)) AS `pemeriksaan`
			 , sum((
			   CASE
			   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000048') THEN
				 (`notadet`.`tarifjm` * `notadet`.`qty`)
			   ELSE
				 0
			   END)) AS `tindakan`
			 , sum(`notadet`.`tarifjm` * `notadet`.`qty`) AS `jasamedis`
			 , sum(`notadet`.`diskonjm`) AS `diskonmedis`
			 , ceiling((((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 2.5) / 100)) AS `zis`
			 , ((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) - ceiling(((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 0.025))) AS `jumlah`
		FROM
		  `registrasidet`
		LEFT JOIN `registrasi`
		ON `registrasidet`.`noreg` = `registrasi`.`noreg`
		LEFT JOIN `pasien`
		ON `pasien`.`norm` = `registrasi`.`norm`
		LEFT JOIN `bagian`
		ON `bagian`.`idbagian` = `registrasidet`.`idbagian`
		LEFT JOIN `nota`
		ON `nota`.`idregdet` = `registrasidet`.`idregdet`
		LEFT JOIN `notadet`
		ON `notadet`.`nonota` = `nota`.`nonota` AND `notadet`.`iddokter` = '".$iddokter."'
		LEFT JOIN `pelayanan`
		ON `notadet`.`kditem` = `pelayanan`.`kdpelayanan`
		LEFT JOIN kuitansi
		ON kuitansi.nokuitansi = `nota`.nokuitansi
		WHERE
		  `registrasi`.`idjnspelayanan` IN (1, 3)
		  AND `notadet`.`tarifjm` > 0
		  AND `pelayanan`.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
		  AND `registrasidet`.`userbatal` IS NULL
		  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
		GROUP BY
		  `registrasi`.`noreg`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_ri($iddokter,$tglawal,$tglakhir){
		
			$query  = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
			 , `notadet`.`nonota` AS `nonota`
			 , `notadet`.`kditem` AS `kditem`
			 , `notadet`.`idjnstarif` AS `idjnstarif`
			 , `notadet`.`koder` AS `koder`
			 , `notadet`.`idsatuan` AS `idsatuan`
			 , `notadet`.`qty` AS `qty`
			 , `notadet`.`tarifjs` AS `tarifjs`
			 , `notadet`.`tarifjm` AS `tarifjm`
			 , `notadet`.`tarifjp` AS `tarifjp`
			 , `notadet`.`tarifbhp` AS `tarifbhp`
			 , `notadet`.`diskonjs` AS `diskonjs`
			 , `notadet`.`diskonjm` AS `diskonjm`
			 , `notadet`.`diskonjp` AS `diskonjp`
			 , `notadet`.`diskonbhp` AS `diskonbhp`
			 , `notadet`.`uangr` AS `uangr`
			 , `notadet`.`hrgjual` AS `hrgjual`
			 , `notadet`.`hrgbeli` AS `hrgbeli`
			 , `notadet`.`iddokter` AS `iddokter`
			 , `notadet`.`idperawat` AS `idperawat`
			 , `notadet`.`idstbypass` AS `idstbypass`
			 , `notadet`.`aturanpakai` AS `aturanpakai`
			 , `notadet`.`dijamin` AS `dijamin`
			 , `notadet`.`stdijamin` AS `stdijamin`
			 , `registrasidet`.`noreg` AS `noreg`
			 , `registrasidet`.`tglmasuk` AS `tglmasuk`
			 , `registrasidet`.`tglkeluar` AS `tglkeluar`
			 , `registrasidet`.`tglreg` AS `tglreg`
			 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
			 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
			 , `pasien`.`nmpasien` AS `nmpasien`
			 , `registrasidet`.`idbagian` AS `idbagian`
			 , `bagian`.`nmbagian` AS `nmbagian`
			 , sum((
			   CASE
			   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000001') THEN
				 (`notadet`.`tarifjm` * `notadet`.`qty`)
			   ELSE
				 0
			   END)) AS `pemeriksaan`
			 , sum((
			   CASE
			   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000048') THEN
				 (`notadet`.`tarifjm` * `notadet`.`qty`)
			   ELSE
				 0
			   END)) AS `tindakan`
			 , sum(`notadet`.`tarifjm` * `notadet`.`qty`) AS `jasamedis`
			 , sum(`notadet`.`diskonjm`) AS `diskonmedis`
			 , ceiling((((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 2.5) / 100)) AS `zis`
			 , ((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) - ceiling(((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 0.025))) AS `jumlah`
			 , sum(((`notadet`.`qty` * `notadet`.`tarifjm`) - `notadet`.`diskonjm`)) AS `sum((``notadet``.qty * ``notadet``.tarifjm) - ``notadet``.diskonjm)`
		FROM
		  `registrasidet`
		LEFT JOIN `registrasi`
		ON `registrasidet`.`noreg` = `registrasi`.`noreg`
		LEFT JOIN `reservasi`
		ON `registrasidet`.`idregdet` = `reservasi`.`idregdet`
		LEFT JOIN `pasien`
		ON `pasien`.`norm` = `registrasi`.`norm`
		LEFT JOIN `nota`
		ON `nota`.`idregdet` = `registrasidet`.`idregdet`
		LEFT JOIN `notadet`
		ON `notadet`.`nonota` = `nota`.`nonota` AND `notadet`.`iddokter` = '".$iddokter."'
		LEFT JOIN `bagian`
		ON `bagian`.`idbagian` = `registrasidet`.`idbagian`
		LEFT JOIN `pelayanan`
		ON `notadet`.`kditem` = `pelayanan`.`kdpelayanan`
		LEFT JOIN kuitansi
		ON kuitansi.nokuitansi = `nota`.nokuitansi
		WHERE
		  `registrasi`.`idjnspelayanan` = 2
		  AND `reservasi`.`idstposisipasien` = 6
		  AND `notadet`.`tarifjm` > 0
		  AND `pelayanan`.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
		  AND `registrasidet`.`userbatal` IS NULL
		  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'

		GROUP BY
		  `registrasi`.`noreg`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_jm($iddokter,$tglawal,$tglakhir){
		
			$query  = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
			 , `notadet`.`nonota` AS `nonota`
			 , `notadet`.`kditem` AS `kditem`
			 , `notadet`.`idjnstarif` AS `idjnstarif`
			 , `notadet`.`koder` AS `koder`
			 , `notadet`.`idsatuan` AS `idsatuan`
			 , `notadet`.`qty` AS `qty`
			 , `notadet`.`tarifjs` AS `tarifjs`
			 , `notadet`.`tarifjm` AS `tarifjm`
			 , `notadet`.`tarifjp` AS `tarifjp`
			 , `notadet`.`tarifbhp` AS `tarifbhp`
			 , `notadet`.`diskonjs` AS `diskonjs`
			 , `notadet`.`diskonjm` AS `diskonjm`
			 , `notadet`.`diskonjp` AS `diskonjp`
			 , `notadet`.`diskonbhp` AS `diskonbhp`
			 , `notadet`.`uangr` AS `uangr`
			 , `notadet`.`hrgjual` AS `hrgjual`
			 , `notadet`.`hrgbeli` AS `hrgbeli`
			 , `jtmdet`.`iddokter` AS `iddokter`
			 , `notadet`.`idperawat` AS `idperawat`
			 , `notadet`.`idstbypass` AS `idstbypass`
			 , `notadet`.`aturanpakai` AS `aturanpakai`
			 , `notadet`.`dijamin` AS `dijamin`
			 , `notadet`.`stdijamin` AS `stdijamin`
			 , `registrasidet`.`noreg` AS `noreg`
			 , `registrasidet`.`tglmasuk` AS `tglmasuk`
			 , `registrasidet`.`tglkeluar` AS `tglkeluar`
			 , `registrasidet`.`tglreg` AS `tglreg`
			 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
			 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
			 , `pasien`.`nmpasien` AS `nmpasien`
			 , sum(`jtmdet`.`jumlah`) AS `jasamedis`
			 , sum(`jtmdet`.`diskon`) AS `diskonmedis`
			 , ceiling((((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) * 2.5) / 100)) AS `zis`
			 , ((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) - ceiling(((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) * 0.025))) AS `jumlah`
		FROM
		  `registrasidet`
		LEFT JOIN `registrasi`
		ON `registrasidet`.`noreg` = `registrasi`.`noreg`
		LEFT JOIN `nota`
		ON `nota`.`idregdet` = `registrasidet`.`idregdet`
		LEFT JOIN `notadet`
		ON `notadet`.`nonota` = `nota`.`nonota`
		LEFT JOIN `pasien`
		ON `pasien`.`norm` = `registrasi`.`norm`
		LEFT JOIN `jtm`
		ON `jtm`.`idnotadet` = `notadet`.`idnotadet`
		LEFT JOIN `jtmdet`
		ON `jtm`.`idjtm` = `jtmdet`.`idjtm` AND `jtmdet`.`iddokter` = '".$iddokter."'
		LEFT JOIN kuitansi
		ON kuitansi.nokuitansi = `nota`.nokuitansi
		WHERE
		  (`jtmdet`.`tarifjm` > 0)
		  AND `registrasidet`.`userbatal` IS NULL
		  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
		GROUP BY
		  `registrasi`.`noreg`
		, `jtmdet`.`iddokter`");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_hd_peltam($iddokter,$tglawal,$tglakhir){
		
			$query  = $this->db->query("SELECT notadet.idnotadet AS idnotadet
											 , notadet.nonota AS nonota
											 , nota.tglnota AS tglnota
											 , kuitansi.tglkuitansi AS tglkuitansi
											 , notadet.kditem AS kditem
											 , pelayanan.pel_kdpelayanan AS pel_kdpelayanan
											 , notadet.iddokter AS iddokter
											 , kuitansi.atasnama AS nmpasien
											 , sum((
											   CASE
											   WHEN (pelayanan.pel_kdpelayanan = 'T000000001') THEN
												 (notadet.tarifjm * notadet.qty)
											   ELSE
												 0
											   END)) AS pemeriksaan
											 , sum((
											   CASE
											   WHEN (pelayanan.pel_kdpelayanan = 'T000000048') THEN
												 (notadet.tarifjm * notadet.qty)
											   ELSE
												 0
											   END)) AS tindakan
											 , sum(notadet.tarifjm * notadet.qty) AS jasamedis
											 , sum(notadet.diskonjm) AS diskonmedis
											 , ceiling((((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) * 2.5) / 100)) AS zis
											 , ((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) - ceiling(((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) * 0.025))) AS jumlah

										FROM
										  nota
										LEFT JOIN notadet
										ON nota.nonota = notadet.nonota AND notadet.iddokter = '".$iddokter."'
										LEFT JOIN pelayanan
										ON pelayanan.kdpelayanan = notadet.kditem
										LEFT JOIN kuitansi
										ON kuitansi.nokuitansi = nota.nokuitansi
										WHERE
										  notadet.tarifjm > 0
										  AND nota.idjnstransaksi = 10
										  AND pelayanan.pel_kdpelayanan IN ('T000000001', 'T000000048')
										  AND nota.idsttransaksi = 1
										  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
										GROUP BY
										  nota.nonota, notadet.kditem");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
	
		function pdf_honordokter($nohondok){
		
		
		$this->pdf->SetMargins('10', '40', '10');
		$this->pdf->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf->setPrintFooter(true); // enabled ? true
		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf->AddPage('P', 'F4', false, false); 
		
		$this->pdf->SetFont('helvetica', 'B', 10);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'JASA MEDIS', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->SetFont('helvetica', '', 8);
		
		$hondok = $this->get_hondok($nohondok);
		
		foreach ($hondok as $item) {
			$nohondok = $item->nohondok;
			$tglhondok = $this->TanggalIndo(date("Ymd",strtotime($item->tglhondok)));
			$iddokter = $item->iddokter;
			$nmdoktergelar = $item->nmdoktergelar;
			$nmspesialisasi = $item->nmspesialisasi;
			$nmstdokter = $item->nmstdokter;
			$nmstatus = $item->nmstatus;
			$tglawal = $this->TanggalIndo(date("Ymd",strtotime($item->tglawal)));
			$tglakhir = $this->TanggalIndo(date("Ymd",strtotime($item->tglakhir)));
			$tglawalparams = $item->tglawal;
			$tglakhirparams = $item->tglakhir;
			
			$norek = $item->norek;
			$nmbank = $item->nmbank;
			$atasnama = $item->atasnama;
			$notelp = $item->notelp;
			$nohp = $item->nohp;
			$nmlengkap = $item->nmlengkap;
			$tglinput = $this->TanggalIndo(date("Ymd",strtotime($item->tglinput)));
			$approval = $item->approval;
			$nmstbayar = $item->nmstbayar;
			$nmjnspembayaran = $item->nmjnspembayaran;
			$jasadrjaga = number_format(ceil($item->jasadrjaga) , 0 , ',' , '.' );
			$jasadrjagaval = ceil($item->jasadrjaga);
			$potonganlain = number_format(ceil($item->potonganlain) , 0 , ',' , '.' );
			$potonganlainval = ceil($item->potonganlain);
			$catatan = $item->catatan;
			$tgldikeluarkan = $this->TanggalIndo(date("Ymd",strtotime($item->tgldikeluarkan)));
		}
		
		$bulan = $this->namaBulanTahun($tglakhirparams);
		
		$total123 = 0;
		$total123val = 0;

		$no = 1;
		$grid_pendapatan = "";
		$sumjasamedis = 0;
		$sumdiskon = 0;
		$sumzis = 0;
		$sumjumlah = 0;
		
		//========================1
		$hd_rj = $this->get_hd_rj($iddokter,$tglawalparams,$tglakhirparams);
		
		$jasamedis = 0;
		$diskon = 0;
		$zis = 0;
		$jumlah = 0;
			
		foreach ($hd_rj as $itemrj) {
			$jasamedis = $jasamedis + ceil($itemrj->jasamedis);
			$diskon = $diskon + ceil($itemrj->diskonmedis);
			$zis = $zis + ceil($itemrj->zis);
			$jumlah = $jumlah + ceil($itemrj->jumlah);

		}
		
		$grid_pendapatan .= "<tr>
						  <th width=\"7%\" align=\"center\">".$no++."</th>
						  <th width=\"30%\" align=\"left\">Rawat Jalan / UGD</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($jasamedis) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($diskon) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($zis) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($jumlah) , 0 , ',' , '.' )."</th>
					 </tr>";
		
		$sumjasamedis = $sumjasamedis + ceil($jasamedis);
		$sumdiskon = $sumdiskon + ceil($diskon);
		$sumzis = $sumzis + ceil($zis);
		$sumjumlah = $sumjumlah + ceil($jumlah);		
		//========================2
		$hd_ri = $this->get_hd_ri($iddokter,$tglawalparams,$tglakhirparams);
		
		$jasamedis = 0;
		$diskon = 0;
		$zis = 0;
		$jumlah = 0;
			
		foreach ($hd_ri as $itemri) {
			$jasamedis = $jasamedis + ceil($itemri->jasamedis);
			$diskon = $diskon + ceil($itemri->diskonmedis);
			$zis = $zis + ceil($itemri->zis);
			$jumlah = $jumlah + ceil($itemri->jumlah);

		}
		
		$grid_pendapatan .= "<tr>
						  <th width=\"7%\" align=\"center\">".$no++."</th>
						  <th width=\"30%\" align=\"left\">Rawat Inap</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($jasamedis) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($diskon) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($zis) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($jumlah) , 0 , ',' , '.' )."</th>
					 </tr>";
		
		$sumjasamedis = $sumjasamedis + ceil($jasamedis);
		$sumdiskon = $sumdiskon + ceil($diskon);
		$sumzis = $sumzis + ceil($zis);
		$sumjumlah = $sumjumlah + ceil($jumlah);
		//========================3
		$hd_jm = $this->get_hd_jm($iddokter,$tglawalparams,$tglakhirparams);
		
		$jasamedis = 0;
		$diskon = 0;
		$zis = 0;
		$jumlah = 0;
			
		foreach ($hd_jm as $itemjm) {
			$jasamedis = $jasamedis + ceil($itemjm->jasamedis);
			$diskon = $diskon + ceil($itemjm->diskonmedis);
			$zis = $zis + ceil($itemjm->zis);
			$jumlah = $jumlah + ceil($itemjm->jumlah);

		}
		
		$grid_pendapatan .= "<tr>
						  <th width=\"7%\" align=\"center\">".$no++."</th>
						  <th width=\"30%\" align=\"left\">Tindakan Tim Medis</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($jasamedis) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($diskon) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($zis) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($jumlah) , 0 , ',' , '.' )."</th>
					 </tr>";
		
		$sumjasamedis = $sumjasamedis + ceil($jasamedis);
		$sumdiskon = $sumdiskon + ceil($diskon);
		$sumzis = $sumzis + ceil($zis);
		$sumjumlah = $sumjumlah + ceil($jumlah);
		//========================4
		$hd_peltam = $this->get_hd_peltam($iddokter,$tglawalparams,$tglakhirparams);
		
		$jasamedis = 0;
		$diskon = 0;
		$zis = 0;
		$jumlah = 0;
			
		foreach ($hd_peltam as $itempeltam) {
			$jasamedis = $jasamedis + ceil($itempeltam->jasamedis);
			$diskon = $diskon + ceil($itempeltam->diskonmedis);
			$zis = $zis + ceil($itempeltam->zis);
			$jumlah = $jumlah + ceil($itempeltam->jumlah);

		}
		
		$grid_pendapatan .= "<tr>
						  <th width=\"7%\" align=\"center\">".$no++."</th>
						  <th width=\"30%\" align=\"left\">Pelayanan Tambahan</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($jasamedis) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($diskon) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($zis) , 0 , ',' , '.' )."</th>
						  <th width=\"15%\" align=\"right\">".number_format(ceil($jumlah) , 0 , ',' , '.' )."</th>
					 </tr>";
		
		$sumjasamedis = $sumjasamedis + ceil($jasamedis);
		$sumdiskon = $sumdiskon + ceil($diskon);
		$sumzis = $sumzis + ceil($zis);
		$sumjumlah = $sumjumlah + ceil($jumlah);
		//=====================================
		$grid_pendapatan .= "<tr>
						  <th width=\"37%\" align=\"right\" colspan=\"5\"><b>Total :</b></th>
						  <th width=\"15%\" align=\"right\"><b>".number_format($sumjasamedis , 0 , ',' , '.' )."</b></th>
						  <th width=\"15%\" align=\"right\"><b>".number_format($sumdiskon , 0 , ',' , '.' )."</b></th>
						  <th width=\"15%\" align=\"right\"><b>".number_format($sumzis , 0 , ',' , '.' )."</b></th>
						  <th width=\"15%\" align=\"right\"><b>".number_format($sumjumlah , 0 , ',' , '.' )."</b></th> 
					 </tr>";
		$total123 = $total123 + ceil($sumjumlah);
		$total123val = $total123;

		$totalterima = number_format((ceil($total123val) + ceil($jasadrjagaval) - ceil($potonganlainval)) , 0 , ',' , '.' );
		$totalterbilang = $this->rp_terbilang(ceil($total123val) + ceil($jasadrjagaval) - ceil($potonganlainval), 0);
		
		$total123 = number_format($total123, 0 , ',' , '.' );

		
					 
		$tbl = <<<EOD
	<br />
    <br />
	
	<table border="0" cellpadding="2" nobr="true">
	 <tr> 
      <th width="20%"><b>Bulan / Periode</b></th>
	  <th width="2%">:</th> 
	  <th width="50%">$bulan / ($tglawal s.d $tglakhir)</th> 
	 </tr>
	 <tr> 
	  <th width="20%"><b>Nama</b></th>
	  <th width="2%">:</th> 
	  <th width="50%">$nmdoktergelar</th>
	 </tr>
	 <tr> 
	  <th width="20%"><b>Spesialisasi</b></th>
	  <th width="2%">:</th> 
	  <th width="50%">$nmspesialisasi</th>
	 </tr>
     <tr> 
	  <th width="20%"><b>Jenis Pembayaran</b></th>
	  <th width="2%">:</th> 
	  <th width="50%">$nmjnspembayaran</th>
	 </tr>
     <tr>
	  <th width="20%"><b>Nomor</b></th>
	  <th width="2%">:</th> 
	  <th width="50%">$nohondok</th>
	 </tr>  
    </table>
	<br />
    <br />
EOD;
     	$this->pdf->writeHTML($tbl,true,false,false,false);
	
//========================rj================================

		$tblrj = <<<EOD
	<b><i>Daftar Pendapatan</i></b> <br />
    <table border="1" cellpadding="2" nobr="true">
     <tr>
	  <th width="7%" align="center"><b>No.</b></th>
	  <th width="30%" align="center"><b>Pendapatan</b></th>
	  <th width="15%" align="center"><b>Jasa Medis</b></th>
      <th width="15%" align="center"><b>Diskon</b></th>
	  <th width="15%" align="center"><b>ZIS</b></th>
	  <th width="15%" align="center"><b>Jumlah</b></th>
     </tr> 

		$grid_pendapatan
    </table>
	
EOD;
     	$this->pdf->writeHTML($tblrj,true,false,false,false);	

		$foot = <<<EOD
			<br />
			<table border="0" cellpadding="2" nobr="true">
			 <tr>
			  <th width="28.5%"></th>
			  <th width="32.5%"></th>
			  <th width="20%"><b>Jasa Piket</b></th>
			  <th width="4%">:</th> 
			  <th width="12%" align="right"><b>$jasadrjaga</b></th>
			 </tr>  
			 <tr>
			  <th width="28.5%"></th>
			  <th width="32.5%"></th>
			  <th width="20%"><b>Potongan Lainnya</b></th>
			  <th width="4%">:</th> 
			  <th width="12%" align="right"><b>$potonganlain</b></th>
			 </tr>  
			 <tr>
			  <th width="28.5%"></th>
			  <th width="32.5%"></th>
			  <th width="20%"><b>Jumlah Yang Terima</b></th>
			  <th width="4%">:</th> 
			  <th width="12%" align="right"><b>$totalterima</b></th>
			 </tr> 

			 <tr>
			  <th width="88.5%"></th>
			 </tr>
			 <tr>
			  <th width="88.5%">Terbilang : "<i>$totalterbilang</i>"</th>
			 </tr>  
			 <tr>
			  <th width="88.5%"></th>
			 </tr> 
			 
			 <tr>
			  <th width="20%"><b>No. Rekening</b></th>
			  <th width="2%">:</th>
			  <th width="32.5%">$norek</th>
			  <th width="20%"></th>
			  <th width="4%"></th> 
			  <th width="12%" align="right"></th>
			 </tr>  
			 <tr> 
			  <th width="20%"><b>Bank</b></th>
			  <th width="2%">:</th>
			  <th width="32.5%">$nmbank</th>
			  <th width="20%"></th>
			  <th width="4%"></th> 
			  <th width="12%" align="right"></th>
			 </tr>
			  <tr> 
			  <th width="20%"><b>Atas Nama</b></th>
			  <th width="2%">:</th>
			  <th width="32.5%">$atasnama</th>
			  <th width="20%"></th>
			  <th width="4%"></th> 
			  <th width="12%" align="right"></th>
			 </tr>
			 <tr>
			  <th width="20%"><b>No. Telepon:</b></th>
			  <th width="2%">:</th>
			  <th width="32.5%">$notelp</th>
			  <th width="20%"></th>
			  <th width="4%"></th> 
			  <th width="12%" align="right"></th>
			 </tr>  
			</table>
			
			<br />
			<br />
			<br />
			<table border="0" cellpadding="2" nobr="true">
			 <tr>
			  <th width="9%"><b><i><u>Catatan</u> :</i></b></th>
			  <th width="70%"><b><i>$catatan</i></b></th>
			 </tr>
			</table>
			<br />
			<br />
			
			<table border="0" cellpadding="2" nobr="true">
			 <tr align="left">
			  <th width="30%">Bandung, $tgldikeluarkan</th>
			  <th width="39%"></th>
			  <th width="30%"></th>
			 </tr>  
			 <tr align="left">
			  <th width="30%">Dikeluarkan Oleh :</th>
			  <th width="39%"></th>
			  <th width="30%"></th>
			 </tr>  
			 <tr align="left">
			  <th width="30%"></th>
			  <th width="39%"></th>
			  <th width="30%"></th>
			 </tr>
			<tr align="left">
			  <th width="30%"></th>
			  <th width="39%"></th>
			  <th width="30%"></th>
			 </tr>
			 <tr align="left">
			  <th width="30%"></th>
			  <th width="39%"></th>
			  <th width="30%"></th>
			 </tr>
			 <tr align="left">
			  <th width="30%"></th>
			  <th width="39%"></th>
			  <th width="30%"></th>
			 </tr>
			 <tr align="left">
			  <th width="30%">$approval</th>
			  <th width="39%"></th>
			  <th width="30%"></th>
			 </tr>
			  <tr align="left">
			  <th width="30%">( Keuangan )</th>
			  <th width="39%"></th>
			  <th width="30%"></th>
			 </tr>
			</table>
			
EOD;
     	$this->pdf->writeHTML($foot,true,false,false,false);		

		$this->pdf->Output('honordokter-'.$nohondok.'.pdf', 'I');
		}
	}

?>