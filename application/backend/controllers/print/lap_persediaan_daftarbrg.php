<?php 
	class Lap_persediaan_daftarbrg extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function laporan_daftarbrg($var){
		$par                    = explode("-", $var);
        $idbagian          		= $par[0];
        $nmbagian		        = $par[1];
		
		$isi = '';		
		$this->db->select("*");
		$this->db->from("v_barangbagian");
		$this->db->where("v_barangbagian.idbagian", $idbagian);
		//$this->db->limit(200,0);
		$query = $this->db->get();
		
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
				
			// add a page
        $this->pdf_lap->AddPage('L');		
		//$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-6 , PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
        $this->pdf_lap->SetFont('helvetica', '', 10);
		
		$kop2 = "<table border=\"0\" >					
					<tr align=\"center\">
						<td><h3>Daftar Barang</h3></td>
					</tr>
					<tr>	
						<td width=\"44.5%\"></td>
						<td width=\"4%\"><font size=\"8\" face=\"Helvetica\">Bagian</font></td>
						<td width=\"1%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"50%\"><font size=\"9\" face=\"Helvetica\">".$nmbagian."</font></td>
					</tr>					
				 </table>
			";
			
		$this->pdf_lap->writeHTML($kop2,true,false,false,false);
				$aaa = $query->result();
				$no = 1;
				foreach($aaa as $i=>$val){
				$isi .= "<tr>
								<td width=\"5%\"><font size=\"7\" face=\"Helvetica\" align=\"center\">".$no++.".</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\"> ".$val->kdbrg."</font></td>
								<td width=\"27%\"><font size=\"7\" face=\"Helvetica\"> ".$val->nmbrg."</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\"> ".$val->nmjnsbrg."</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\"> ".$val->nmsatuanbsr."</font></td>
								<td width=\"7%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->rasio."&nbsp;&nbsp;</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\"> ".$val->nmsatuankcl."</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->stoknowbagian."&nbsp;&nbsp;</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->stokminbagian."&nbsp;&nbsp;</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->stokmaxbagian."&nbsp;&nbsp;</font></td>
						</tr>";
				}
				$detail = "<table border=\"1\" cellpadding=\"2\" >
							<tr align=\"center\">
								<th width=\"5%\"><font size=\"7\" face=\"Helvetica\">No.</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Kode Barang</font></th>
								<th width=\"27%\"><font size=\"7\" face=\"Helvetica\">Nama Barang</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Jenis <br> Barang</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Satuan <br> Besar</font></th>
								<th width=\"7%\"><font size=\"7\" face=\"Helvetica\">Rasio</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Satuan <br> Kecil</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Stock<br>Sekarang</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Stock<br>Minimal</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Stock<br>Maksimal</font></th>
							</tr>".$isi."
						   </table>
				";
		$this->pdf_lap->writeHTML($detail,true,false,false,false);
		$this->pdf_lap->Output('daftar_barang.pdf', 'I');
	}
	
	function excellaporan_daftarbrg($var){
		$par                    = explode("-", $var);
        $idbagian          		= $par[0];
        $nmbagian		        = $par[1];
		
		$tablename='v_barangbagian';
		$header = array(
			'Kode Barang',
			'Nama Barang',
			'Jenis Barang',
			'Satuan Besar',
			'Rasio',
			'Satuan Kecil',
			'Setok Sekarang',
			'Setok Minimal',
			'Setok Makimal',
		);
		
		$this->db->select("
			kdbrg,
			nmbrg,
			nmjnsbrg,
			nmsatuanbsr,
			rasio,
			nmsatuankcl,
			stoknowbagian,
			stokminbagian,
			stokmaxbagian,
		");
        $this->db->from($tablename);
		$this->db->where("v_barangbagian.idbagian", $idbagian);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('Daftar_Barang');
		$data['filter'] = strtoupper('Daftar Barang')."\n Nama Barang : ".$nmbagian."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellapfarmasi', $data); 	
	}
}

?>