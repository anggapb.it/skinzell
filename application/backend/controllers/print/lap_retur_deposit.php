<?php

class Lap_retur_deposit extends Controller{
	function __construct(){
		parent::__construct();
		$this->load->library('pdf_lap');        
	}
	
	function laporan_retur_deposit_pdf_old($tglawal,$tglakhir){		
		$isi = '';
		$this->db->select('*');
        $this->db->from('v_returfarmasihed');
		
		if($tglakhir){
			$this->db->where('`tglreturfarmasi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Retur Deposit', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);

		$no = 1;
		$totaleverything = 0;
		foreach($data as $i=>$val){
			$kditem = '';
			$namabarang = '';
			$jmlbeli = '';
			$jmlretur = '';
			$tarif = '';
			$subtotal = '';
			$totalperpasien = 0;
			if(!empty($val->returfarmasi_detail['data'])){
				foreach($val->returfarmasi_detail['data'] as $idxdtl => $returfarmasi_detail){
				
					if($idxdtl == 0){
						$kditem .= $returfarmasi_detail->kditem;
						$namabarang .= $returfarmasi_detail->nmbrg;
						$jmlbeli .= $returfarmasi_detail->qtyterima;
						$jmlretur .= $returfarmasi_detail->jmlretur;
						$tarif .= number_format($returfarmasi_detail->tarif,0,',','.');
						$subtotal .= number_format($returfarmasi_detail->subtotal,0,',','.');
					}else{
						$kditem .= '<br>'.$returfarmasi_detail->kditem;
						$namabarang .= '<br>'.$returfarmasi_detail->nmbrg;
						$jmlbeli .= '<br>'.$returfarmasi_detail->qtyterima;
						$jmlretur .= '<br>'.$returfarmasi_detail->jmlretur;
						$tarif .= '<br>'.number_format($returfarmasi_detail->tarif,0,',','.');
						$subtotal .= '<br>'.number_format($returfarmasi_detail->subtotal,0,',','.');
					}
					
					$totalperpasien += $returfarmasi_detail->subtotal;
				}
			}
			
			$isi .= "
			    <tr>
					<td align=\"center\">". ($i+1) .".</td>
					<td align=\"center\">". $val->noreturfarmasi ."</td>
					<td align=\"center\">". date_format(date_create($val->tglreturfarmasi), 'd-m-Y') ."</td>
					<td align=\"center\">". $val->nmlengkap ."</td>
					<td align=\"center\">". $val->noreg ."</td>
					<td align=\"center\">". $val->norm ."</td>
					<td align=\"left\">". $val->atasnama ."</td>
					<td align=\"center\">". $kditem ."</td>
					<td align=\"left\">". $namabarang ."</td>
					<td align=\"right\">". $jmlbeli ."</td>
					<td align=\"right\">". $jmlretur ."</td>
					<td align=\"right\">". $tarif ."</td>
					<td align=\"right\">". $subtotal ."</td>
				</tr>
				<tr>
					<td align=\"right\" colspan=\"12\"><strong>Total Perpasien</strong></td>
					<td align=\"right\"><strong>". number_format($totalperpasien,0,',','.') ."</strong></td>
				</tr>";	
			
			$totaleverything += $totalperpasien;
		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"5%\">No.</td>
					<td width=\"8%\">No. <br> Retur Farmasi</td>
					<td width=\"8%\">Tgl <br> Retur Farmasi</td>
					<td width=\"8%\">Penerima</td>
					<td width=\"8%\">No. Nota</td>
					<td width=\"7%\">No. RM</td>
					<td width=\"10%\">Nama Pasien</td>
					<td width=\"8%\">Kode Barang</td>
					<td width=\"8%\">Nama Barang</td>
					<td width=\"8%\">Qty Beli</td>
					<td width=\"8%\">Qty Retur</td>
					<td width=\"7%\">Tarif</td>
					<td width=\"10%\">Subtotal</td>
				</tr>
			  <tbody>". $isi ."</tbody>
				<tr align=\"right\">
					<td colspan=\"12\"><strong>Total</strong></td>
					<td><strong>". number_format($totaleverything,0,',','.')."</strong></td>
				</tr>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf_lap->Output('lappoperbrg.pdf', 'I');
	}

	function laporan_retur_deposit_excel_old($tglawal, $tglakhir) {
		$isi = '';
		$this->db->select('*');
        $this->db->from('v_returfarmasihed');
		
		if($tglakhir){
			$this->db->where('`tglreturfarmasi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		//get retur farmasi detail
		foreach($data as $idx => $dt){
			$data[$idx]->returfarmasi_detail = $this->_get_returfarmasi_detail($dt->noreturfarmasi);
		}
					
		
		$header = array(
			'No.',
			'No. Retur Farmasi',
			'Tgl Retur Farmasi',
			'Penerima',
			'No. Nota',
			'No. RM',
			'Nama Pasien',
			'Kode Barang',
			'Nama Barang',
			'Qty Beli',
			'Qty Retur',
			'Tarif',
			'Subtotal',
		);
		
		$data['eksport'] = $data;
		$data['fieldname'] = $header;
		$data['filename'] = 'Laporan_retur_farmasi_pasien_luar_'.$tglawal.'_'.$tglakhir;
		$data['filter'] = strtoupper('Tanggal')." : ".$tglawal." s/d ".$tglakhir;		
		$this->load->view('exportexcellreturfarmasipl', $data); 	
	}
	
	function laporan_retur_deposit_pdf($tglawal, $tglakhir){
		$isi = '';
		$this->db->select('*');
        $this->db->from('v_returdeposit');
		
		if($tglakhir == $tglawal){
			$this->db->where('`tgltransaksi`', $tglakhir);
		}else{
			$this->db->where('`tgltransaksi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Retur Deposit', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		
		if($tglawal == $tglakhir)
			$this->pdf_lap->Cell(0, 0, 'Pertanggal '. date("d F Y", strtotime($tglawal)), 0, 1, 'C', 0, '', 0);
		else
			$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
			
		$no = 1;
		$totaleverything = 0;
		
		foreach($data as $i=>$val){
			$isi .= "
			    <tr>
					<td align=\"center\">". ($i+1) .".</td>
					<td align=\"center\">". $val->kdjurnal ."</td>
					<td align=\"center\">". date_format(date_create($val->tgltransaksi), 'd-m-Y') ."</td>
					<td align=\"center\">". $val->noreg ."</td>
					<td align=\"center\">". $val->norm ."</td>
					<td align=\"center\">". $val->nmpasien ."</td>
					<td align=\"center\">". $val->nmkamar ."</td>
					<td align=\"center\">". $val->nmdoktergelar ."</td>
					<td align=\"center\">". $val->userid ."</td>
					<td align=\"right\">". number_format($val->tottransaksi,0,',','.') ."</td>
					<td align=\"right\">". number_format($val->totdeposit,0,',','.') ."</td>
					<td align=\"right\">". number_format($val->nominal,0,',','.') ."</td>
				</tr>";	
				
			$totaleverything += $val->nominal;
		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"5%\"><strong>No.</strong></td>
					<td width=\"10%\"><strong>No. Retur Deposit</strong></td>
					<td width=\"10%\"><strong>Tgl Retur Deposit</strong></td>
					<td width=\"10%\"><strong>No. Registrasi</strong></td>
					<td width=\"5%\"><strong>No. RM</strong></td>
					<td width=\"10%\"><strong>Nama Pasien</strong></td>
					<td width=\"7%\"><strong>Ruangan</strong></td>
					<td width=\"14%\"><strong>Dokter</strong></td>
					<td width=\"5%\"><strong>User ID</strong></td>
					<td width=\"8%\"><strong>Total Transaksi</strong></td>
					<td width=\"8%\"><strong>Total Deposit</strong></td>
					<td width=\"8%\"><strong>Retur Deposit</strong></td>
				</tr>
			  <tbody>". $isi ."</tbody>
				<tr>
					<td align=\"right\" colspan=\"11\"><strong>Total</strong></td>
					<td align=\"right\"><strong>". number_format($totaleverything,0,',','.')."</strong></td>
				</tr>
			</table></font>
		";

		$this->pdf_lap->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf_lap->Output('lap_retur_deposit_'.$tglawal.'_'.$tglakhir.'.pdf', 'I');
	}

	function laporan_retur_deposit_excel($tglawal, $tglakhir) {
		$isi = '';
		$this->db->select('*');
        $this->db->from('v_returdeposit');
		
		if($tglakhir == $tglawal){
			$this->db->where('`tgltransaksi`', $tglakhir);
		}else{
			$this->db->where('`tgltransaksi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$header = array(
			'No.',
			'No. Retur Deposit',
			'Tgl Retur Deposit',
			'No. Registrasi',
			'No. RM',
			'Nama Pasien',
			'Ruangan',
			'Dokter',
			'User ID',
			'Total Transaksi',
			'Total Deposit',
			'Retur Deposit'
		);
		
		$data['eksport'] = $data;
		$data['fieldname'] = $header;
		$data['filename'] = 'Laporan_retur_deposit_'.$tglawal.'_'.$tglakhir;
		$data['filter'] = strtoupper('Tanggal')." : ".$tglawal." s/d ".$tglakhir;		
		$this->load->view('exportexcellreturdeposit', $data); 	
	}
	
		
}