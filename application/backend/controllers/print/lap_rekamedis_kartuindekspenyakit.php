<?php 
	class Lap_rekamedis_kartuindekspenyakit extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function pasienkeluar($tglawal,$tglakhir){
		
		$this->db->select("*");
		$this->db->from("v_lapri_kartuindeksdokter");
	//	$this->db->groupby("nopo");
		$this->db->where("tglkeluar BETWEEN '".$tglawal."' and '".$tglakhir."'");
		$query = $this->db->get();
		$po = $query->result();
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
	//	$this->pdf->AddPage();
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-4, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN KARTU INDEKS PENYAKIT PASIEN KELUAR', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 7);
		
		$this->pdf->Cell(0, 3, 'TANGGAL : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
        $this->pdf->SetFont('helvetica', '', 7);
	    
		$isi ='';
	if($query->num_rows>0){
	$no = 0;
	$dokter = 0;

		foreach($query->result_array() as $i=>$r){
		if($r['idpenyakit'] != $dokter){
			$dokter = $r['idpenyakit'];
			$isi .= "<tr>
				<td colspan =\"19\">KODE : ".$r['kdpenyakit']."<BR>NAMA PENYAKIT: ".$r['nmpenyakiteng']."</td>
			</tr>";
			
		}
		$no = (1+ $i);
		if($r['umurhari'] == 0 || $r['umurhari'] < 28 && $r['umurtahun'] == 0 || $r['umurtahun'] == 0){
			$satu = 1;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurhari'] <= 29 && $r['umurtahun'] <= 1){
			$satu = 0;
			$dua = 1;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 1 || $r['umurtahun'] <= 4){
			$satu = 0;
			$dua = 0;
			$tiga = 1;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 5 || $r['umurtahun'] <= 14){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 1;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 15 || $r['umurtahun'] <= 24){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 1;
			$enam = 0;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 25 || $r['umurtahun'] <= 44){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 1;
			$tujuh = 0;
			$delapan = 0;
		}else if($r['umurtahun'] == 45 || $r['umurtahun'] <= 64){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 1;
			$delapan = 0;
		}else if($r['umurtahun'] >= 65){
			$satu = 0;
			$dua = 0;
			$tiga = 0;
			$empat = 0;
			$lima = 0;
			$enam = 0;
			$tujuh = 0;
			$delapan = 1;
		}
			$isi .="<tr>
						<td align=\"center\">".$no."</td>
						<td align=\"center\">".$r['noreg']."</td>
						<td align=\"right\">".$r['tglmasuk']."</td>
						<td align=\"right\">".$r['jammasuk']."</td>
						<td align=\"right\">".$r['tglkeluar']."</td>
						<td align=\"right\">".$r['jamkeluar']."</td>
						<td align=\"center\">".$r['kdjnskelamin']."</td>
						<td align=\"center\">".$satu."</td>
						<td align=\"center\">".$dua."</td>
						<td align=\"center\">".$tiga."</td>
						<td align=\"center\">".$empat."</td>
						<td align=\"center\">".$lima."</td>
						<td align=\"center\">".$enam."</td>
						<td align=\"center\">".$tujuh."</td>
						<td align=\"center\">".$delapan."</td>
						<td>".$r['nmdoktergelar']."</td>
						<td>".$r['nmpenyakiteng']."</td>
						<td>".$r['nmstkeluar']."</td>
						<td>".$r['nmcarakeluar']."</td>
			</tr>";
		}
	}
		$heads = "<br><br><font size=\"6\" face=\"Helvetica\"> 
        
                <table border=\"1\">
					<tr align=\"center\">
                        <th rowspan=\"2\" width=\"2%\"><br><br>NO</th>
                        <th rowspan=\"2\" width=\"6%\"><br><br>NO. REG</th>
                        <th rowspan=\"2\" width=\"5%\">TGL <br> MASUK</th>
                        <th rowspan=\"2\" width=\"5%\">JAM <br> MASUK</th>
                        <th rowspan=\"2\" width=\"5%\">TGL <br> KELUAR</th>
                        <th rowspan=\"2\" width=\"5%\">JAM <br> KELUAR</th>
                        <th rowspan=\"2\" width=\"2%\"><br><br>L/P</th>
                        <th colspan=\"8\" width=\"24%\">KELOMPOK UMUR</th>
                        <th rowspan=\"2\" width=\"10%\"><br><br>Dokter Rawat </th>
                        <th rowspan=\"2\" width=\"20%\"><br><br>NAMA PENYAKIT</th>
                        <th rowspan=\"2\" width=\"8%\"><br><br>KEADAAN KELUAR</th>
                        <th rowspan=\"2\" width=\"10%\"><br><br>CARA KELUAR</th>
					</tr>
                    <tr align=\"center\">
                        <th width=\"3%\">0-28 HR</th>
                        <th width=\"3%\">28HR - 1 TH</th>
                        <th width=\"3%\">1 - 4 TH</th>
                        <th width=\"3%\">5 - 14 TH</th>
                        <th width=\"3%\">14-24 TH</th>
                        <th width=\"3%\">25-44 TH</th>
                        <th width=\"3%\">45-64 TH</th>
                        <th width=\"3%\">65 +</th>
                    </tr>".$isi."
                    
		</table></font>";
		
		
		$this->pdf->writeHTML($heads,true,false,false,false);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>