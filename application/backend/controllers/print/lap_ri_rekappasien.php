<?php 
	class Lap_ri_rekappasien extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function rawatinap($tglawal,$tglakhir){
		
		$this->db->select("*");
		$this->db->from("v_laprirekappasienri");
	//	$this->db->groupby("nopo");
		$this->db->where("tglmasuk BETWEEN '".$tglawal."' and '".$tglakhir."'");
	//	$this->db->where('idcaradatang',$ruangan);
		$query = $this->db->get();
		$aa = $query->row_array();
			//var_dump($tglawal);
			//var_dump($tglakhir);
		
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
	//	$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
		$this->pdf->AddPage();
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Rekap Pasien Rawat Inap', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->Cell(0, 0, 'Tanggal : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
	//	$this->pdf->Cell(0, 0, 'Asal Rujukan : '. $aa['nmcaradatang'], 0, 1, 'C', 0, '', 0);
		$isi ='';
		$this->pdf->SetMargins(PDF_MARGIN_LEFT+30, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		if($query->num_rows>0){
			$no = 0;
			$stkeluar = 0;
			$grupnya = '';
		foreach($query->result_array() as $r){
		 	/* if($r['idpekerjaan'] != $stkeluar){
				$stkeluar = $r['idpekerjaan'];
				$isi .= "<tr><td colspan=\"13\">Pekerjaan: ".$r['nmpekerjaan']."</td></tr>";
				$no = 0;
			}  */
			$isi .="<tr>
						<td align=\"center\">".++$no."</td>
						<td align=\"center\">".$r['nmbagian']."</td>
						<td align=\"right\">".$r['expr1']."</td>
						
			</tr>";
		}
	}
		$heads = "<br><br><font size=\"8\" face=\"Helvetica\"> <table border=\"1\">
					<tr align=\"center\">
						<th width=\"4%\">No.</th>
						<th width=\"60%\">Ruang Perawatan</th>
						<th width=\"10%\">Jumlah</th>
					
					</tr>".$isi."
		</table></font>";
		$this->pdf->writeHTML($heads,true,false,false,false); 
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td>Bandung, ".date('d F Y')."</td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>(.........................)</td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td><font size=\"7\" face=\"Helvetica\">Petugas Registrasi</font></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		$this->pdf->Output('laporan_pasien.pdf', 'I');
		}
	}
?>