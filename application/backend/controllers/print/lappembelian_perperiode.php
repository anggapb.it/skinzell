<?php 
class Lappembelian_perperiode extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
		
	function laporan_perperiode($tglawal,$tglakhir){
		$isi = '';
	 	$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$this->db->select("*");
		$this->db->from("v_lappo");
	//	$this->db->groupby("nopo");
		$this->db->where("tglpo BETWEEN '".$tglawal."' and '".$tglakhir."'");
		$query = $this->db->get();
        
		
		/* $this->db->select("*");
		$this->db->from("v_lappo");
		$this->db->where("tglpo BETWEEN '".$tglawal."' and '".$tglakhir."'");
	//	$this->db->groupby("nopo");
		$query2 = $this->db->get();
        
		$grup = $query2->result(); */
			$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 280),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Pembelian Barang', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		
		$this->pdf->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' s/d '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		$no = 0;
		$totalppn = 0;
		$totaldisk = 0;
		foreach($query->result_array() as $i=>$r){
			$hbeli = explode("<br>", $r['hargabeli']);
			$diskon15 = explode("<br>", $r['diskon1']);
			$diskonya = explode("<br>", $r['diskonnya']);
			$ppnnya = explode("<br>", $r['ppn1']);
			$totppnnya = explode("<br>", $r['ppn2']);
			$no = ($i+1);
				//perhitungan Diskon
		$diskon = $r['diskon'] / 100;
		$jumlahharga = $r['qty']  * $r['hargabeli'] ;
		$diskon1 = $jumlahharga * $diskon;
		$diskon2 = $jumlahharga - $diskon1;
		
		//End
		
		//Perhitungan PPN
		$ppn = 10 / 100;
		$jumlahppn = $diskon2 * $ppn;
		$ppn1 = $diskon2 + $jumlahppn;
		
		$vhbeli = '';
		$diskonx = '';
		$diskonxx = '';
		$ppnnyax = '';
		$ppnnyaxx = '';
		$totalppn += $r['hasilppn'];
		for($i=0; $i<$r['noporow']; $i++){
			$vhbeli .= number_format($hbeli[$i],0,',','.') . '<br>';
			$diskonx .= number_format($diskon15[$i],0,',','.') . '<br>';
			$diskonxx .= number_format($diskonya[$i],0,',','.') . '<br>';
			$ppnnyax .= number_format($ppnnya[$i],0,',','.') . '<br>';
			$ppnnyaxx .= number_format($totppnnya[$i],0,',','.') . '<br>';
			
		}
		
		//var_dump($diskonx);
		$isi .= "<tr align=\"center\">
				<td width=\"3%\" align=\"center\">".$no."</td>
				<td width=\"8%\">".$r['nopo']."</td>
				<td width=\"8%\">".date_format(date_create($r['tglpo']), 'd-m-Y')."</td>
				<td width=\"14%\" align=\"left\">".$r['nmbrg']."</td>
				<td width=\"5%\">".$r['nmsatuan']."</td>
				<td width=\"4%\" align=\"right\">".$r['qty']."</td>
				<td width=\"5%\" align=\"right\">".$r['qtybonus']."</td>
				<td width=\"6%\"><font face=\"Helvetica\" align=\"right\">".$vhbeli."</font></td>
				<td width=\"5%\"><font face=\"Helvetica\" align=\"right\">".$diskonxx."</font></td>
				<td width=\"8%\"><font face=\"Helvetica\" align=\"right\">".$diskonx."</font></td>
				<td width=\"10%\"><font face=\"Helvetica\" align=\"right\">".$ppnnyax."</font></td>
				<td width=\"8%\"><font face=\"Helvetica\" align=\"right\">".$ppnnyaxx."</font></td>
				<td width=\"11%\">".$r['nmsupplier']."</td>
				<td width=\"8%\">".$r['stlunas']."</td>
			</tr>
			
			<tr>
				<td colspan=\"9\" align=\"right\"><b>Total JumlahHrg+Disk</b></td>
				<td align=\"right\"><b>".($r['nopo']=='TOTAL'?'':number_format($r['hasildiskon'],0,',','.'))."</b></td>
				<td align=\"right\"><b>Total JumlahHrg+Ppn</b></td>
				<td align=\"right\"><b>".($r['nopo']=='TOTAL'?'':number_format($r['hasilppn'],0,',','.'))."</b></td>
			</tr>";
			
			
		
		}
		$heads = "
		<br/><br/><font size=\"6\" face=\"Helvetica\">
		<table border=\"1px\" cellpadding=\"2\" >
				<thead>
					<tr align=\"center\">
						<th width=\"3%\"><b>No</b></th>
						<th width=\"8%\"><b>No. Pembelian</b></th>
						<th width=\"8%\"><b>Tgl. Pembelian</b></th>
						<th width=\"14%\"><b>Nama Obat</b></th>
						<th width=\"5%\"><b>Satuan</b></th>
						<th width=\"4%\"><b>Qty</b></th>
						<th width=\"5%\"><b>Qty Bonus</b></th>
						<th width=\"6%\"><b>Harga Beli</b></th>
						<th width=\"5%\"><b>Diskon</b></th>
						<th width=\"8%\"><b>JumlahHrg+Disk</b></th>
						<th width=\"10%\"><b>PPN</b></th>
						<th width=\"8%\"><b>JumlahHrg+Ppn</b></th>
						<th width=\"11%\"><b>Supplier</b></th>
						<th width=\"8%\"><b>Keterangan</b></th>
					</tr>
				</thead>".$isi."
				<tr>
					<td colspan=\"11\" align=\"right\"><b>Grand Total Pembelian</b></td>
					<td><font face=\"Helvetica\" align=\"right\"><b>".number_format($totalppn,0,',','.')."</b></font></td>
					<td></td>
					<td></td>
				</tr>
		</table>";
		
		$this->pdf->writeHTML($heads,true,false,false,false); 
		$this->pdf->Output('laporan_pembelian.pdf', 'I');
	}
	
	function excelpoperperiode($tglawal, $tglakhir) {
		$tablename='v_lappodet';
		$header = array(
			'No. Pembelian',
			'Tgl. Pembelian',
			'Nama Barang',
			'Satuan',
			'Rasio',
			'Qty(Satuan Besar)',
			'Qty(Satuan Kecil)',
			'Qty Bonus',
			'Harga Beli',
			'Diskon (Rp)',
			'JumlahHrg+Disk',
			'PPN',
			'JumlahHrg+PPN',
			'Supplier',
			'Keterangan',
		);
		
		$this->db->select("
			nopo,
			tglpo,
			nmbrg,
			nmsatuan,
			rasio,
			qty,
			qtysatuankcl,
			qtybonus,
			hargabeli,
			jmlhrgtmbhdiskon,
			diskonrp,
			jmlppn,
			jmlhrgtmbhppn,
			nmsupplier,
			stlunas
		");
        $this->db->from($tablename);
		$this->db->where('`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		 /*if(($tglawal && $tglakhir != ''))
			$this->db->where('`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		if ($idcombo !=''){
			$this->db->like($idcombo, $nmcombo);
		} */
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('lappoperperiode');
		$data['filter'] = "Laporan Pembelian \n Periode : ".$tglawal." s/d ".$tglakhir."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellappo', $data); 	
	}
	
	function excelpembelian($tglawal, $tglakhir) {
		$tablename='v_po';
		$header = array(
			'No. Pembelian',
			'Tgl. Pembelian',
			'Nama Supplier',
			'Jenis Pembayaran',
			'Konta Bon',
			'Tgl. Jatuh Tempo',
			'Approval',
			'User Input',
			'Total',
			'Status Bayar',
		);
		
		$this->db->select("
			nopo,
			tglpo,
			nmsupplier,
			nmjnspembayaran,
			nmstkontrabon,
			tgljatuhtempo,
			approval1,
			nmlengkap,
			totalpo2,
			stlunas
		");
        $this->db->from($tablename);
		$this->db->where('`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		$query = $this->db->get();
		$fpl = $query->result();
        $fplnum = $query->num_rows();

		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('pembelianperiode');
		$data['filter'] = "Pembelian \n Periode : ".$tglawal." s/d ".$tglakhir."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellso', $data); 	
	}
	
}
?>