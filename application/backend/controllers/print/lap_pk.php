<?php

class Lap_pk extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
	
	function namaBulan($tgl){
		$arrtgl = explode('-', $tgl);
		$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
		$bulan = $query->row_array();
		$tanggalInd = $arrtgl[2] .' '. $bulan['nmbulan'] .' '. $arrtgl[0];
		
		return $tanggalInd ;
	}
	
	
	function cetak_pk($tglawal, $tglakhir){
	
		$this->db->select("*");
        $this->db->from("v_treg_lap");
        $this->db->order_by("v_treg_lap.noreg ASC");					
		$this->db->where("date(tglkuitansi) between ", "'". $tglawal ."' AND '". $tglakhir ."'", false);

		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$this->db->select("*");
        $this->db->from("v_tagihan_farmasilap");
        $this->db->order_by("v_tagihan_farmasilap.nonota ASC");					
		$this->db->where("date(tglkuitansi) between ", "'". $tglawal ."' AND '". $tglakhir ."'", false);

		$q2 = $this->db->get();
		$data2 = array();
        if ($q2->num_rows() > 0) {
            $data2 = $q2->result();
        }
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		$tgl = $this->namaBulan(date('Y-m-d'));
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'Laporan Pembayaran Karyawan (PG)', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 12);
		$this->pdf->Cell(0, 0, 'Periode : '.$tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$isi2 = '';
		$isicb = '';
		$total = 0;
		$dibayarpasien = 0;
		$sisa = 0;
		$potonggaji = 0;
		$jmlbyr = 0;
		$total2 = 0;
		$dibayarpasien2 = 0;
		$sisa2 = 0;
		$potonggaji2 = 0;
		$jmlbyr2 = 0;
		$total_all = 0;
		$dibayarpasien_all = 0;
		$sisa_all = 0;
		$potonggaji_all = 0;
		$jmlbyr_all = 0;
		
		foreach($data AS $i=>$val){
			$isi .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"10%\">". date_format(date_create($val->tglkuitansipgreg), 'd-m-Y') ."</td>
					<td width=\"12%\">". $val->noreg ."</td>
					<td width=\"10%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"12%\">". $val->norm ."</td>
					<td width=\"20%\">". $val->nmpasien ."</td>
					<td width=\"20%\">". $val->nama_karyawan ."</td>
					<td width=\"12%\" align=\"right\">". number_format($val->jumlah,0,',','.') ."</td>
			</tr>";
			
			
			$jmlbyr += $val->jumlah;
			
		}
		
		$html = "<br/><br/><font size=\"9\" face=\"Helvetica\">
			<p><b>Rawat Jalan / UGD / Rawat Inap</b></p>
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"10%\"><b>Tgl. Pembayaran</b></td>
					<td width=\"12%\"><b>No. Registrasi</b></td>
					<td width=\"10%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"12%\"><b>No. RM</b></td>
					<td width=\"20%\"><b>Nama Pasien</b></td>
					<td width=\"20%\"><b>Nama Karyawan</b></td>
					<td width=\"12%\"><b>Jumlah Bayar</b></td>
				</tr>
			  </thead>
			  <tbody>'".$isi."'</tbody>
				<tr>
					<td  width=\"86.8%\" colspan=\"7\" align=\"right\"><b>T O T A L</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($jmlbyr,0,',','.') ."</b></td>
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
		
		
		//GRID FARMASI PASIEN LUAR
		
		foreach($data2 AS $i=>$val){
			$isi2 .= "<tr>
					<td width=\"3%\" align=\"center\">". ($i+1) ."</td>
					<td width=\"10%\">". date_format(date_create($val->tglkuitansipgreg), 'd-m-Y') ."</td>
					<td width=\"12%\">". $val->nonota ."</td>
					<td width=\"10%\">". date_format(date_create($val->tglkuitansi), 'd-m-Y') ."</td>
					<td width=\"12%\">". $val->norm ."</td>
					<td width=\"20%\">". $val->atasnama ."</td>
					<td width=\"20%\">". $val->nama_karyawan ."</td>
					<td width=\"12%\" align=\"right\">". number_format($val->jumlah,0,',','.') ."</td>
				</tr>";
			
			
			$jmlbyr2 += $val->jumlah;
			
		}
		
		$jmlbyr_all = $jmlbyr + $jmlbyr2;
		
		
		$html2 = "<font size=\"9\" face=\"Helvetica\">
			<p><b>Pelayanan Tambahan / Farmasi Pasien Luar</b></p>
			<table border=\"1px\" cellpadding=\"2\">
			  <thead>
				<tr align=\"center\">
					<td width=\"3%\"><b>No.</b></td>
					<td width=\"10%\"><b>Tgl. Pembayaran</b></td>
					<td width=\"12%\"><b>No. Nota</b></td>
					<td width=\"10%\"><b>Tgl. Kuitansi</b></td>
					<td width=\"12%\"><b>No. RM</b></td>
					<td width=\"20%\"><b>Nama Pasien</b></td>
					<td width=\"20%\"><b>Nama Karyawan</b></td>
					<td width=\"12%\"><b>Jumlah Bayar</b></td>
				</tr>
			  </thead>
			  <tbody>'".$isi2."'</tbody>
				<tr>
					<td  width=\"86.8%\" colspan=\"7\" align=\"right\"><b>T O T A L</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($jmlbyr2,0,',','.') ."</b></td>
				</tr>
				<tr>
					<td colspan=\"8\" align=\"right\">&nbsp;</td>
				</tr>
				<tr>
					<td  width=\"86.8%\" colspan=\"7\" align=\"right\"><b>T O T A L    S E M U A</b></td>
					<td width=\"12%\" align=\"right\"><b>". number_format($jmlbyr_all,0,',','.') ."</b></td>
				</tr>
				
			</table></font>
		";
		$this->pdf->writeHTML($html2,true,false,false,false); 
		
		//Close and output PDF document
		$this->pdf->Output('Tagihan_Karyawan.pdf', 'I');
	
	
	}
	
	function excelpk($tglawal, $tglakhir) {
		$header = array(
			'Tgl. Pembayaran',
			'No. Registrasi',
			'Tgl. Kuitansi',
			'No. RM',
			'Nama Pasien',
			'Nama Karyawan',
			'Jumlah Bayar',
		);
		
		$header2 = array(
			'Tgl. Pembayaran',
			'No. Nota',
			'Tgl. Kuitansi',
			'No. RM',
			'Nama Pasien',
			'Nama Karyawan',
			'Jumlah Bayar',
		);
		
		$this->db->select("*");
        $this->db->from("v_treg_lap");
        $this->db->order_by("v_treg_lap.noreg ASC");			
		$this->db->where("date(tglkuitansi) between '". $tglawal ."' and '". $tglakhir."'" );
		$query = $this->db->get();
		
		$fpl = $query->result();
        $fplnum = $query->num_rows();
		$total = 0;
		$dibayarpasien = 0;
		$potonggaji = 0;
		$jml_byr = 0;
		$sisa = 0;
		
		foreach($fpl AS $i=>$val){
			
			$jml_byr += $val->jumlah;
			
				
		}
		
		
		
		
		
		$this->db->select("*");
        $this->db->from("v_tagihan_farmasilap");
        $this->db->order_by("v_tagihan_farmasilap.nonota ASC");			
		$this->db->where("date(tglkuitansi) between '". $tglawal ."' and '". $tglakhir."'" );
		$query = $this->db->get();
		
		$fpl2 = $query->result();
        $fplnum2 = $query->num_rows();
		
		$total2 = 0;
		$dibayarpasien2 = 0;
		$potonggaji2 = 0;
		$jml_byr2 = 0;
		$sisa2 = 0;
		
		foreach($fpl2 AS $i=>$val){
			
			$jml_byr2 += $val->jumlah;
			
		}
		
		$total_all = 0;
		$dibayarpasien_all = 0;
		$potonggaji_all = 0;
		$jml_byr_all = 0;
		$sisa_all = 0;
		
		
		$jml_byr_all = $jml_byr + $jml_byr2;
	
		$data['eksport'] = $fpl;
		$data['eksport2'] = $fpl2;
		$data['fieldname'] = $header;
		$data['fieldname2'] = $header2;
		$data['numrows'] = $fplnum;
		$data['numrows2'] = $fplnum2;
		$data['jumlah'] = $jml_byr;
		$data['jumlah2'] = $jml_byr2;
		$data['jml_byr_all'] = $jml_byr_all;
		$this->load->view('exportexcelpk', $data); 	
	}
   
	
}	