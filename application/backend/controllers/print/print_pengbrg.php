<?php 
	class Print_pengbrg extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_pp');        
		}

	function pengbrg_pdf($nokeluarbrg){
		$this->pdf_pp->SetPrintFooter(true);
		$this->db->select("*");
		$this->db->from("v_keluarbrg");
		
		$this->db->where('v_keluarbrg.nokeluarbrg', $nokeluarbrg);
		$query = $this->db->get();
        $nokeluarbrg = $query->row_array();
		
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 148),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => -90,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_pp->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_pp->setPrintFooter(true); // enabled ? true
		//$this->pdf_pp->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_pp->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
				
			// add a page
        $this->pdf_pp->AddPage('L');		
		//$this->pdf_pp->AddPage('P', $page_format, false, false);
        $this->pdf_pp->SetFont('helvetica', '', 9);
		$kop = "<br>
				<table border=\"0\">
					<tr align=\"left\">
						<td width=\"0.7%\"></td>
						<td width=\"99.5%\"><font size=\"13\" face=\"Helvetica\"><b>RSIA HARAPAN BUNDA</b></font></td>
					</tr>
					<tr align=\"left\">
						<td></td>
						<td><font size=\"11\" face=\"Helvetica\"><b>dr. Bambang Suhardijant, SpOg</b></font></td>
					</tr>
					<tr align=\"left\">
						<td></td>
						<td>Pluto Raya Blok C Margahayu Raya Bandung</td>
					</tr>
					<tr align=\"left\">
						<td><font size=\"9\" face=\"Helvetica\"></font><hr height=\"2\"></td>
						<td>Telp. (022) 7506490 Fax (022) 7514712<hr height=\"2\"></td>
					</tr>
					<tr align=\"center\">
						<td></td>
						<td><h3><b><i>Pengeluaran Barang</i></b></h3></td>
					</tr>
				</table>
		";
     	$this->pdf_pp->writeHTML($kop,true,false,false,false);
		
		$kop2 = "<br><br>
				 <table border=\"0\">
					<tr align=\"left\">
						<td width=\"2%\"></td>
						<td width=\"20%\"><font size=\"8\" face=\"Helvetica\">NO. Keluar Barang</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"36%\"><font size=\"9\" face=\"Helvetica\"><b>".$nokeluarbrg["nokeluarbrg"]."</b></font></td>
					
						
						<td width=\"14%\"><font size=\"8\" face=\"Helvetica\">Untuk Baigan</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"38%\"><font size=\"8\" face=\"Helvetica\">".$nokeluarbrg["nmbagianuntuk"]."</font></td>
					</tr>
					<tr>
						<td width=\"2%\"></td>
						<td width=\"20%\"><font size=\"8\" face=\"Helvetica\">Tanggal Keluar</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"36%\"><font size=\"8\" face=\"Helvetica\">".date("d-m-Y", strtotime($nokeluarbrg['tglkeluar']))." / ".$nokeluarbrg["jamkeluar"]."</font></td>
					
						
						<td width=\"14%\"><font size=\"8\" face=\"Helvetica\">Penerima</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"38%\"><font size=\"8\" face=\"Helvetica\">".$nokeluarbrg["penerima"]."</font></td>
					</tr>					
					<tr>
						<td width=\"2%\"></td>
						<td width=\"20%\"><font size=\"8\" face=\"Helvetica\">Status Persetujuan</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"36%\"><font size=\"8\" face=\"Helvetica\">".$nokeluarbrg["nmstsetuju"]."</font></td>

						<td width=\"14%\"><font size=\"8\" face=\"Helvetica\">Status Transaksi</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"38%\"><font size=\"8\" face=\"Helvetica\">".$nokeluarbrg["nmsttransaksi"]."</font></td>
					</tr>					
					<tr>
						<td width=\"2%\"></td>
						<td width=\"20%\"><font size=\"8\" face=\"Helvetica\">Dari Bagian</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"36%\"><font size=\"8\" face=\"Helvetica\">".$nokeluarbrg["nmbagiandari"]."</font></td>

						<td width=\"14%\"><font size=\"8\" face=\"Helvetica\">User Input</font></td>
						<td width=\"2%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td width=\"38%\"><font size=\"8\" face=\"Helvetica\">".$nokeluarbrg["nmlengkap"]."</font></td>
					</tr>
				 </table>
			";
		$this->pdf_pp->writeHTML($kop2,true,false,false,false);
	//	var_dump($nopo);
				$isi = '';
				$this->db->select("*");
				$this->db->from("v_keluarbrgdet");
				$this->db->where('nokeluarbrg',$nokeluarbrg['nokeluarbrg']);
			//	var_dump($nopo);
				$querys = $this->db->get();
				
			//	$ambil = $querys->row_array();
				$aaa = $querys->result();
				$no = 1;
				foreach($aaa as $i=>$val){
				$isi .= "<tr>
								<td width=\"5%\"><font size=\"7\" face=\"Helvetica\" align=\"center\">".$no++.".</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\"> ".$val->kdbrg."</font></td>
								<td width=\"33.5%\"><font size=\"7\" face=\"Helvetica\" > ".$val->nmbrg."</font></td>
								<td width=\"9%\"><font size=\"7\" face=\"Helvetica\" align=\"center\">".$val->nmjnsbrg."&nbsp;&nbsp;</font></td>
								<td width=\"8%\"><font size=\"7\" face=\"Helvetica\" align=\"center\">".$val->nmsatuan."&nbsp;&nbsp;</font></td>
								<td width=\"8%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->qty."&nbsp;&nbsp;</font></td>
								<td width=\"9.5%\"><font size=\"7\" face=\"Helvetica\" align=\"right\">".$val->stoknowbagian."&nbsp;&nbsp;</font></td>
								<td width=\"18%\"><font size=\"7\" face=\"Helvetica\" align=\"left\">&nbsp;".$val->catatan."&nbsp;&nbsp;</font></td>
						</tr>";
				}
				$detail = "<table border=\"1\" cellpadding=\"2\">
						<thead>
							<tr align=\"center\">
								<th width=\"5%\"><font size=\"7\" face=\"Helvetica\">No.</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Kode Barang</font></th>
								<th width=\"33.5%\"><font size=\"7\" face=\"Helvetica\">Nama Barang</font></th>
								<th width=\"9%\"><font size=\"7\" face=\"Helvetica\">Jenis Barang</font></th>
								<th width=\"8%\"><font size=\"7\" face=\"Helvetica\">Satuan</font></th>
								<th width=\"8%\"><font size=\"7\" face=\"Helvetica\">Qty</font></th>
								<th width=\"9.5%\"><font size=\"7\" face=\"Helvetica\">Stok Sekarang</font></th>
								<th width=\"18%\"><font size=\"7\" face=\"Helvetica\">Catatan</font></th>
							</tr>
						</thead>".$isi."
						   </table>
				";
		$this->pdf_pp->writeHTML($detail,true,false,false,false);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td width=\"20%\"><b>Mengetahui</b></td>
				<td></td>
			</tr>
			<tr>
				<td height=\"40\"></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><font size=\"9\" face=\"Helvetica\">".$nokeluarbrg['nmlengkap']."</font><hr></td>
				<td></td>
			</tr>
			</table>
		";
		$this->pdf_pp->writeHTML($approve,true,false,false,false);
		$this->pdf_pp->Output('pengeluaran_barang.pdf', 'I');
	}
}

?>