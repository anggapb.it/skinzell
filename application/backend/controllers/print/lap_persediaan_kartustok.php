<?php

	class Lap_persediaan_kartustok extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function laporan_kartustok($tglawal,$tglakhir,$idbagian,$kdbrg){
	
		$this->db->select('*');
        $this->db->from('v_barangbagian');		
		$this->db->where('v_barangbagian.idbagian', $idbagian);
		$query = $this->db->get();
        $bagian = $query->row_array();
		
		$this->db->select('*');
        $this->db->from('v_barang');		
		$this->db->where('v_barang.kdbrg', $kdbrg);
		$query = $this->db->get();
        $brg = $query->row_array();
		
		
		$isi = '';		
		$this->db->select('*');
        $this->db->from('v_kartustok');
		//$this->db->limit(200,0);
		if($idbagian)$this->db->where('v_kartustok.idbagian',$idbagian);
		if($kdbrg)$this->db->where('v_kartustok.kdbrg', $kdbrg);
    
        if($tglawal!='null'){
			$this->db->where('`tglkartustok` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$query = $this->db->get();
		$aaa = $query->result();
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'DAFTAR TRANSAKSI BARANG', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		if($tglawal!='null'){
			$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		}
		$this->pdf_lap->Cell(0, 0, 'Bagian : '. $bagian["nmbagian"], 0, 1, 'C', 0, '', 0);
		$this->pdf_lap->Cell(0, 0, 'Nama Barang : '. $brg["nmbrg"], 0, 1, 'C', 0, '', 0); 

		$no = 1;
		$saldoawal = 0;
		$jmlmasuk = 0;
		$jmlkeluar = 0;
		$saldoakhir = 0;
		$nilaisaldoawal = 0;
		$nilaisaldomasuk = 0;
		$nilaisaldokeluar = 0;
		$nilaisaldoakhir = 0;
		foreach($aaa as $i=>$val){
			$isi .= "<tr>
					<td width=\"3.5%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"8%\" align=\"right\">". $val->saldoawal ."</td>
					<td width=\"8%\" align=\"right\">". $val->jmlmasuk ."</td>
					<td width=\"8%\" align=\"right\">". $val->jmlkeluar ."</td>
					<td width=\"8%\" align=\"right\">". $val->saldoakhir ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->nilaisaldoawal,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->nilaisaldomasuk,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->nilaisaldokeluar,0,',','.') ."</td>
					<td width=\"8%\" align=\"right\">". number_format($val->nilaisaldoakhir,0,',','.') ."</td>
					<td width=\"8%\" align=\"center\">". date_format(date_create($val->tglkartustok), 'd-m-Y') ."</td>
					<td width=\"6.5%\" align=\"center\">". $val->jamkartustok ."</td>
					<td width=\"10%\" align=\"center\">". $val->noref ."</td>
					
					<td width=\"12%\" align=\"left\">". $val->nmjnskartustok ."</td>
				</tr>";
			
			$saldoawal += $val->saldoawal;
			$jmlmasuk += $val->jmlmasuk;
			$jmlkeluar += $val->jmlkeluar;
			$saldoakhir += ($val->saldoawal + $val->jmlmasuk) - $val->jmlkeluar;
			$nilaisaldoawal += $val->nilaisaldoawal;
			$nilaisaldomasuk += $val->nilaisaldomasuk;
			$nilaisaldokeluar += $val->nilaisaldokeluar;
			$nilaisaldoakhir += ($val->nilaisaldoawal + $val->nilaisaldomasuk) - $val->nilaisaldokeluar;
		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"3.5%\">No.</td>
					<td width=\"8%\">Stok Awal</td>
					<td width=\"8%\">Stok Masuk</td>
					<td width=\"8%\">Stok Keluar</td>
					<td width=\"8%\">Stok Akhir</td>
					<td width=\"8%\">Saldo Awal</td>
					<td width=\"8%\">Saldo Masuk</td>
					<td width=\"8%\">Saldo Keluar</td>
					<td width=\"8%\">Saldo Akhir</td>
					<td width=\"8%\">Tgl. Kartustok</td>
					<td width=\"6.5%\">Jam Kartustok</td>
					<td width=\"10%\">Noref</td>
					
					<td width=\"12%\">Jenis Transaksi</td>
				</tr>
			  <tbody>". $isi ."</tbody>
				<tr align=\"right\">
					<td width=\"3.5%\">Total</td>
					<td width=\"8%\">". number_format($saldoawal,0,',','.')."</td>
					<td width=\"8%\">". number_format($jmlmasuk,0,',','.')."</td>
					<td width=\"8%\">". number_format($jmlkeluar,0,',','.')."</td>
					<td width=\"8%\">". number_format($saldoakhir,0,',','.')."</td>
					<td width=\"8%\">". number_format($nilaisaldoawal,0,',','.')."</td>
					<td width=\"8%\">". number_format($nilaisaldomasuk,0,',','.')."</td>
					<td width=\"8%\">". number_format($nilaisaldokeluar,0,',','.')."</td>
					<td width=\"8%\">". number_format($nilaisaldoakhir,0,',','.')."</td>
				</tr>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf_lap->Output('kartustok.pdf', 'I');
	}
}
