<?php 
	class Print_skl extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
	
		function skl_pdf($idskl){
			
		$this->db->select("*");
		$this->db->from("v_skldetail");
		$this->db->where('idskl',$idskl);
		$query = $this->db->get();
        $skl = $query->row_array();

			// add a page
		 	$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 148),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 90,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->SetPrintFooter(false);
		$this->pdf->AddPage('P', 'A5', false, false); 
    //    $this->pdf->AddPage();
        $this->pdf->SetMargins(PDF_MARGIN_LEFT+2.7, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		//$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);
		$this->pdf->SetFont('helvetica', '', 12);
		$kop = "<br><br><br><br><br><br><br>
				<table align=\"center\" >
				<tr align=\"center\">
					<td><h3><b><u>Surat Keterangan Lahir</u></b></h3></td>
				</tr>
				<tr>
					<td>".$skl['kdskl']."</td>
				</tr>
				</table>
		";
     	$this->pdf->writeHTML($kop,true,false,false,false);
		$this->pdf->SetFont('helvetica', '', 11);
		
		$kop2 = "<br>
				<table border=\"0\">
				<tr align=\"left\">
					<td width=\"13%\"></td>
					<td width=\"80%\">Yang bertanda tangan di bawah ini menerangkan bahwa telah lahir bayi pada : </td>
					
				</tr>
				</table>";
		$this->pdf->writeHTML($kop2,true,false,false,false);
		//format Hari
		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
		$hari = $array_hari[date("N", strtotime($skl['tglkelahiran']))];
		$hari1 = $array_hari[date("N")];
		//Format Tanggal 
		$tanggal = date("j",strtotime($skl['tglkelahiran']));
		$tanggal1 = date("j");
		//Formar Bulan
		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei",
								"Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$bulan = $array_bulan[date("n",strtotime($skl['tglkelahiran']))]; 
		$bulan1 = $array_bulan[date("n")]; 
		
		$tahun = date("Y",strtotime($skl['tglkelahiran']));
		$tahun1 = date("Y");
		if($skl['nmbayi'] == "null"){
			$nmbayi = "";
		}else{
			$nmbayi = $skl['nmbayi'];
		}
		$kop3 = "<br>
				<table border=\"0\">
				<tr>
					<td width=\"13%\"></td>
					<td width=\"25%\">Hari / Tanggal</td>
					<td width=\"3%\">:</td>
					<td width=\"63%\">".$hari.", ".$tanggal." ".$bulan." ".$tahun."</td>
				</tr>
				<tr>
					<td></td>
					<td>Jam</td>
					<td width=\"3%\">:</td>
					<td>".substr($skl['jamkelahiran'],0,5)."</td>
				</tr>
				<tr>
					<td></td>
					<td>Berat Badan</td>
					<td width=\"3%\">:</td>
					<td>".$skl['beratkelahiran']."  Kg</td>
				</tr>
				<tr>
					<td></td>
					<td>Panjang Badan</td>
					<td>:</td>
					<td>".$skl['panjangkelahiran']."  Cm</td>
				</tr>
				<tr>
					<td></td>
					<td>Jenis Kelamin</td>
					<td>:</td>
					<td>".$skl['nmjnskelamin']."</td>
				</tr>
				<tr>
					<td></td>
					<td>Jenis Kelahiran</td>
					<td>:</td>
					<td>".$skl['nmkembar']."</td>
				</tr>
				<tr>
					<td></td>
					<td>Nama Bayi</td>
					<td>:</td>
					<td>".$nmbayi."</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><hr></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><hr></td>
				</tr>
				<tr>
					<td></td>
					<td>Anak Ke</td>
					<td>:</td>
					<td>".$skl['anakke']."</td>
				</tr>
				<tr>
					<td></td>
					<td>Nama Ibu</td>
					<td>:</td>
					<td>".$skl['nmibu']."</td>
				</tr>
				<tr>
					<td></td>
					<td>Nama Ayah</td>
					<td>:</td>
					<td>".$skl['nmayah']."</td>
				</tr>
				<tr>
					<td></td>
					<td>Alamat</td>
					<td>:</td>
					<td>".$skl['alamat']."</td>
				</tr>
				</table>";
		$this->pdf->writeHTML($kop3,true,false,false,false);
		$approve = "
			<table border=\"0\" align=\"center\" >
			<tr>
				<td width=\"20%\"></td>
				<td></td>
				<td width=\"50%\"></td>
			</tr>
			<tr>
				<td height=\"50\"></td>
				<td></td>
				<td width=\"50%\"><b>Bandung,".$tanggal1 ." ".$bulan1." ".$tahun1."</b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr >
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>".$skl['nmdoktergelar']."</td>
			</tr>
			</table>
		";
		$this->pdf->writeHTML($approve,true,false,false,false);
		$this->pdf->Output('skl_'.str_replace("/","-",$skl['kdskl']).'.pdf', 'I');
		}
	}

?>