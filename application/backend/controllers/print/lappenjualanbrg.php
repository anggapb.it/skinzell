<?php
class Lappenjualanbrg extends Controller {
    function __construct(){
        parent::__construct();
		$this->load->library('pdf');
        
    }
    
    function data($tglawal, $tglakhir) {
		$this->db->select("
			barang.kdbrg AS kdbrg,
			barang.nmbrg AS nmbrg,
			kartustok.noref AS noref,
			kartustok.tglkartustok AS tglkartustok,
			kartustok.jamkartustok AS jamkartustok,
			if(kartustok.idjnskartustok=9, kartustok.saldoawal, 0) AS saldoawal
			 , if(kartustok.idjnskartustok = 1, (kartustok.jmlmasuk - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													 FROM
																													   podet
																													 WHERE
																													   podet.nopo = `retursupplierdet`.nopo
																													   AND
																													   podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																	   FROM
																		 `retursupplierdet`
																	   WHERE
																		 ((`retursupplierdet`.`nopo` = kartustok.noref)
																		 AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		 AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS jmlmasuk
			 , if(kartustok.idjnskartustok = 2, (kartustok.jmlkeluar - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													  FROM
																														podet
																													  WHERE
																														podet.nopo = `retursupplierdet`.nopo
																														AND
																														podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																		FROM
																		  `retursupplierdet`
																		WHERE
																		  ((`retursupplierdet`.noretursupplier = kartustok.noref)
																		  AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		  AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS rbm
			 , if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0) AS jmlkeluar,
			if(kartustok.idjnskartustok IN (14, 15), kartustok.jmlmasuk, 0) AS rbk,
			kartustok.saldoakhir AS saldoakhir,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgbeli, 0) AS hrgbeli,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgjual, 0) AS hrgjual,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgbeli, 0) * if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0) AS modal,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgjual, 0) * if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0) AS penjualan,
			concat(ROUND(if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgjual, 0) * if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0) / (if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgbeli, 0) * if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0)) * 100,2),' %') AS persentase_margin
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgbeli * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgbeli * kartustok.jmlmasuk), 0)) AS nbeli
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgjual * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgjual * kartustok.jmlmasuk), 0)) AS njual
		", false);
		
		$this->db->from("kartustok");
		$this->db->join('barang',
				'kartustok.kdbrg = barang.kdbrg', 'left');
		if($tglawal){
			$this->db->where('kartustok.tglkartustok BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$this->db->where("kartustok.idbagian",11);
		$this->db->where_in('kartustok.idjnskartustok',array(1, 3, 2, 9, 14, 15));
		$this->db->where("kartustok.noref NOT IN (SELECT kstok.noref AS kd
												  FROM
													kartustok kstok
												  WHERE
													kstok.kdbrg = kartustok.kdbrg
													AND
													kstok.idbagian = kartustok.idbagian
													AND
													kstok.tglkartustok BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
													AND
													kstok.`idjnskartustok` = '19')"
						);
		$this->db->orderby("kartustok.kdbrg, kartustok.tglkartustok, kartustok.jamkartustok");
		$q = $this->db->get();
		$reg = $q->result();
		
		$tglAwalFix = $this->namaBulan($tglawal);
		$tglAkhirFix = $this->namaBulan($tglakhir);
		
		if($tglAwalFix == $tglAkhirFix) $tanggal = $tglAwalFix;
		else $tanggal = $tglAwalFix .' - '. $tglAkhirFix;
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->SetPrintHeader(false);
		$this->pdf->AddPage('L', 'F4', false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		$this->pdf->SetFont('helvetica', 'B', 14);
		
		$x=0;$y=10;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf->Cell(0, 0, 'LAPORAN PENJUALAN BARANG', 0, 1, 'C', 0, '', 0);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$this->pdf->Cell(0, 0, 'Periode : '. $tanggal, 0, 1, 'C', 0, '', 0);
		
		$isi = '';
		$total = 0;
		
		$totpenjualan = 0;
		$totuobat = 0;
		$totutindakan = 0;
		$totuimun = 0;
		$totulab = 0;
		$totulain = 0;
		$toturacik = 0;
		$totudiskon = 0;
		$totucc = 0;
		$totutotal = 0;
		
		$bagshow = "";
		foreach($reg AS $i=>$val){
			$isi .= "<tr>
					<td  align=\"center\">". ($i+1) ."</td>
					<td >". $val->kdbrg ."</td>
					<td >". $val->nmbrg ."</td>
					<td  align=\"right\">". number_format($val->hrgbeli,0,',','.') ."</td>
					<td  align=\"right\">". number_format($val->hrgjual,0,',','.') ."</td>
					<td>". $val->jmlkeluar ."</td>
					<td  align=\"right\">". number_format($val->modal,0,',','.') ."</td>
					<td  align=\"right\">". number_format($val->penjualan,0,',','.') ."</td>
					<td>". $val->persentase_margin ."</td>
					
			</tr>";
			
			$totpenjualan += $val->penjualan;
			
			
		}
		
		
		$html = "<br/><br/><font size=\"7.5\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
			  <!-- <thead> -->
				<tr align=\"center\">
					<td><b>No</b></td>
					<td><b>Kode Barang</b></td>
					<td><b>Nama Barang</b></td>
					<td><b>Harga Beli</b></td>
					<td><b>Harga Jual</b></td>
					<td><b>Qty</b></td>
					<td><b>Modal <br> (Harga Beli x Qty)</b></td>
					<td><b>Penjualan <br> (Harga Jual x Qty)</b></td>
					<td><b>Profit Margin</b></td>
					
				</tr>
			  <!-- </thead> -->
			  <tbody>". $isi ."</tbody>
				<tr align=\"right\">
					<td colspan=\"9\">TOTAL PENJUALAN : ". number_format($totpenjualan,0,',','.') ."</td>
					
				</tr>
			</table></font>
		";
		$this->pdf->writeHTML($html,true,false,false,false);


		//Close and output PDF document
		$this->pdf->Output('laporang_penjualan_barang.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
	}

	
	
	function getNumRows($table) {
        $query = $this->db->get($table);
        return $query->num_rows();
    }
	
	function namaBulan($tgl){
		$arrtgl = explode('-', $tgl);
		$query = $this->db->getwhere('bulan',array('idbulan'=>$arrtgl[1]));
		$bulan = $query->row_array();
		$tanggalInd = $arrtgl[2] .' '. $bulan['nmbulan'] .' '. $arrtgl[0];
		
		return $tanggalInd ;
	}
	
}
