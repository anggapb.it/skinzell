<?php 
	class Print_jasatimmedis extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf');
        
		}
		
		function TanggalIndo($date){
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun = substr($date, 0, 4);
            $bulan = substr($date, 4, 2);
            $tgl   = substr($date, 6, 2);
             
            $result =$tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
            return($result);
        }
		
		function get_jtm($idjtm){
		
			$query  = $this->db->query("SELECT * FROM v_jasamedis WHERE idjtm='$idjtm'");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
		
		function get_jtmdet($idjtm){
		
			$query  = $this->db->query("SELECT * FROM v_jtmdet WHERE idjtm='$idjtm'");
			$result = array();
			if ($query->num_rows() > 0) {
				$result = $query->result();
			}
			return $result;
		}
	
		function pdf_jasatimmedis($idjtm){

		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 148),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		
		$this->pdf->SetMargins('10', '40', '10');
		$this->pdf->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf->setPrintFooter(true); // enabled ? true
		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf->AddPage('P', 'A4', false, false); 
		
		$this->pdf->SetFont('helvetica', 'B', 12);
		$this->pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
		$this->pdf->Write(0, 'JASA TIM MEDIS', '', 0, 'C', true, 0, false, false, 0);
		
		$this->pdf->SetFont('helvetica', '', 8);
		
		$jtm = $this->get_jtm($idjtm);
		
		foreach ($jtm as $item) {
			$noreg = $item->noreg;
			$tglreg = $this->TanggalIndo(date("Ymd",strtotime($item->tglreg)));
			$norm = $item->norm;
			$nmpasien = $item->nmpasien;
			$tarifjasa = $item->tarifjasa;
			$diskon = $item->diskon;
			$tarifdiskon = $item->tarifdiskon;
			$tanggal = $this->TanggalIndo(date("Ymd"));
			$nmlengkap = $item->nmlengkap;
			$catatan = $item->catatan;	
		}
		
		$jtmdet = $this->get_jtmdet($idjtm);
		$no = 1;
		$det = "";
		$sumtarif = 0;
		$sumdiskon = 0;
		$sumjumlah = 0;
		foreach ($jtmdet as $itemdet) {
		
			$det .= "<tr>
						  <th width=\"5%\" align=\"center\">".$no++."</th>
						  <th width=\"20%\" align=\"left\">".$itemdet->nmdoktergelar."</th>
						  <th width=\"20%\" align=\"left\">".$itemdet->nmspesialisasi."</th>
						  <th width=\"15%\" align=\"left\">".$itemdet->nmstdoktertim."</th>
						  <th width=\"5%\" align=\"right\">".number_format(ceil($itemdet->persentarif) , 0 , ',' , '.' )."</th>
						  <th width=\"10%\" align=\"right\">".number_format(ceil($itemdet->tarifjm) , 2 , ',' , '.' )."</th>
						  <th width=\"5%\" align=\"right\">".number_format(ceil($itemdet->persendiskon) , 0 , ',' , '.' )."</th>
						  <th width=\"10%\" align=\"right\">".number_format(ceil($itemdet->diskonrp) , 2 , ',' , '.' )."</th>
						  <th width=\"10%\" align=\"right\">".number_format(ceil($itemdet->jumlah) , 2 , ',' , '.' )."</th> 
					 </tr>";
					 
			$sumtarif = $sumtarif + ceil($itemdet->tarifjm);
			$sumdiskon = $sumdiskon + ceil($itemdet->diskonrp);
			$sumjumlah = $sumjumlah + ceil($itemdet->jumlah);
		}
		
		$det .= "<tr>
						  <th width=\"65%\" align=\"right\" colspan=\"5\"><b>Total:</b></th>
						  <th width=\"10%\" align=\"right\"><b>".number_format($sumtarif , 2 , ',' , '.' )."</b></th>
						  <th width=\"5%\" align=\"right\"></th>
						  <th width=\"10%\" align=\"right\"><b>".number_format($sumdiskon , 2 , ',' , '.' )."</b></th>
						  <th width=\"10%\" align=\"right\"><b>".number_format($sumjumlah , 2 , ',' , '.' )."</b></th> 
					 </tr>";
		
		$tbl = <<<EOD
	<br />
    <br />
	
	<table border="0" cellpadding="2" nobr="true">
     <tr>
	  <th width="20%"><b>No. / Tgl registrasi</b></th>
	  <th width="40%">:$noreg / $tglreg</th>
      <th width="15%"><b>Tanggal</b></th>
	  <th width="25%">:$tanggal</th> 
	 </tr>  
	 <tr> 
	  <th width="20%"><b>No. RM</b></th>
	  <th width="40%">:$norm</th>
      <th width="15%"><b>User Input</b></th>
	  <th width="25%">:$nmlengkap</th> 
	 </tr>
	 <tr> 
	  <th width="20%"><b>Nama Pasien</b></th>
	  <th width="40%">:$nmpasien</th>
      <th width="15%"><b>Keterangan</b></th>
	  <th width="25%" rowspan="4">:$catatan</th> 
	 </tr>
	 <tr> 
	  <th width="20%"><b>Tarif Jasa Medis</b></th>
	  <th width="40%">:$tarifjasa</th>
	  <th width="15%"></th>
	 </tr>	
	 <tr> 
	  <th width="20%"><b>Diskon</b></th>
	  <th width="40%">:$diskon</th>
	  <th width="15%"></th>
	 </tr> 
	 <tr> 
	  <th width="20%"><b>Tarif Jasa Medis - Diskon</b></th>
	  <th width="40%">:$tarifdiskon</th>
      <th width="15%"></th>

     </tr> 
    </table>
	
    <br />
    <br />
	<br />
    <br />
    
    <table border="1" cellpadding="2" nobr="true">
     <tr>
	  <th width="5%" align="center"><b>No.</b></th>
	  <th width="20%" align="center"><b>Nama Dokter</b></th>
      <th width="20%" align="center"><b>Spesialisasi</b></th>
	  <th width="15%" align="center"><b>Status</b></th>
	  <th width="5%" align="center"><b>%</b></th>
	  <th width="10%" align="center"><b>Tarif Jasa Medis</b></th>
	  <th width="5%" align="center"><b>%</b></th>
	  <th width="10%" align="center"><b>Diskon</b></th>
	  <th width="10%" align="center"><b>Jumlah</b></th>
      
     </tr> 

	 $det
		
    </table>
	
	
	
    <br />
    
EOD;
     	$this->pdf->writeHTML($tbl,true,false,false,false);
		
		
		$this->pdf->Output('po.pdf', 'I');
		}
	}

?>