<?php

	class Lap_stok_persediaan extends Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('pdf_lap');        
		}
		
	function get_lap_stok_persediaan($tglawal,$tglakhir,$idbagian,$nmbagian){		
		$isi = '';
		
		$this->db->select("barang.kdbrg
						 , barang.nmbrg
						 , ifnull((SELECT sum(kartustok.saldoawal) AS sa
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS sa

						 , ifnull((SELECT sum(kartustok.jmlmasuk) - (ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																			 FROM
																			   kartustok
																			 WHERE
																			   ((kartustok.`kdbrg` = barang.`kdbrg`)
																			   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																			   AND (kartustok.idbagian = bbagian.idbagian)
																			   AND (kartustok.`idjnskartustok` = 2))
																			   AND
																			   kartustok.noref = (SELECT rsdet.noretursupplier AS noretursupplier
																								  FROM
																									retursupplierdet rsdet
																								  WHERE
																									rsdet.idstbayar <> '2'
																									AND
																									rsdet.noretursupplier = kartustok.noref
																									AND
																									rsdet.kdbrg = kartustok.kdbrg
																								  GROUP BY
																									kartustok.kdbrg DESC
																								  LIMIT
																									1)
																			 ORDER BY
																			   kartustok.kdbrg DESC
																			 LIMIT
																			   1), 0)) AS jml
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
									 AND
									 kartustok.idjnskartustok = 1
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS beli

						 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
								   FROM
									 kartustok
								   WHERE
									 ((kartustok.`kdbrg` = barang.`kdbrg`)
									 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
									 AND (kartustok.idbagian = bbagian.idbagian)
									 AND (kartustok.`idjnskartustok` = 2))
									 AND
									 kartustok.noref = (SELECT rsdet.noretursupplier AS noretursupplier
														FROM
														  retursupplierdet rsdet
														WHERE
														  rsdet.idstbayar = '2'
														  AND
														  rsdet.noretursupplier = kartustok.noref
														  AND
														  rsdet.kdbrg = kartustok.kdbrg
														GROUP BY
														  kartustok.kdbrg DESC
														LIMIT
														  1)
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS jmlretbeli

						 , ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
								   FROM
									 kartustok
								   WHERE
									 ((kartustok.`kdbrg` = barang.`kdbrg`)
									 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
									 AND (kartustok.idbagian = bbagian.idbagian)
									 AND (kartustok.`idjnskartustok` = 18))
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS jmltbl

						 , ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
								   FROM
									 kartustok
								   WHERE
									 ((kartustok.`kdbrg` = barang.`kdbrg`)
									 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
									 AND (kartustok.idbagian = bbagian.idbagian)
									 AND (kartustok.`idjnskartustok` = 5))
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS `jmldistribusibrg`

						 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok = 10
									 AND
									 kartustok.kode = '0306'
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmldistribusimasukbrg`

						 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok = 11
									 AND
									 kartustok.kode = '0306'
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmlrkbb`

						 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok = 6
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmlrmbg`

						 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok = 7
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmlbhp`

						 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
									 AND
									 kartustok.`idjnskartustok` IN (3, 16, 17)
									 AND
									 kartustok.noref NOT IN (SELECT kstok.noref AS kd
														 FROM
														   kartustok kstok
														 WHERE
														   kstok.kdbrg = barang.kdbrg
														   AND
														   kstok.idbagian = bbagian.idbagian
														   AND
														   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
														   AND
														   kstok.`idjnskartustok` = '19')
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS `jmljualbrg`

						 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok IN (14, 15)
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmlretjualbrg`

					 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																				  FROM
																					kartustok
																				  WHERE
																					kartustok.kdbrg = barang.kdbrg
																					AND
																					kartustok.idjnskartustok = 10
																					AND
																					kartustok.kode = '0306'
																					AND
																					kartustok.idbagian = bbagian.idbagian
																					AND
																					kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																				  ORDER BY
																					kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																														 FROM
																														   kartustok
																														 WHERE
																														   kartustok.kdbrg = barang.kdbrg
																														   AND
																														   kartustok.idjnskartustok = 11
																														   AND
																														   kartustok.kode = '0306'
																														   AND
																														   kartustok.idbagian = bbagian.idbagian
																														   AND
																														   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																														 ORDER BY
																														   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																								FROM
																																								  kartustok
																																								WHERE
																																								  kartustok.kdbrg = barang.kdbrg
																																								  AND
																																								  kartustok.idjnskartustok = 7
																																								  AND
																																								  kartustok.idbagian = bbagian.idbagian
																																								  AND
																																								  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																								ORDER BY
																																								  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
																																																			  FROM
																																																				kartustok
																																																			  WHERE
																																																				kartustok.kdbrg = barang.kdbrg
																																																				AND
																																																				kartustok.idbagian = bbagian.idbagian
																																																				AND
																																																				kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																				AND
																																																				kartustok.idjnskartustok = 1
																																																			  ORDER BY
																																																				kartustok.kdbrg DESC
																																																			  LIMIT
																																																				1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																								 FROM
																																																								   kartustok
																																																								 WHERE
																																																								   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																								   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																								   AND (kartustok.idbagian = bbagian.idbagian)
																																																								   AND (kartustok.`idjnskartustok` = 2))
																																																								 ORDER BY
																																																								   kartustok.kdbrg DESC
																																																								 LIMIT
																																																								   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
																																																													 FROM
																																																													   kartustok
																																																													 WHERE
																																																													   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																													   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																													   AND (kartustok.idbagian = bbagian.idbagian)
																																																													   AND (kartustok.`idjnskartustok` = 18))
																																																													 ORDER BY
																																																													   kartustok.kdbrg DESC
																																																													 LIMIT
																																																													   1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
																																																																		 FROM
																																																																		   kartustok
																																																																		 WHERE
																																																																		   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																																		   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																																		   AND (kartustok.idbagian = bbagian.idbagian)
																																																																		   AND (kartustok.`idjnskartustok` = 5))
																																																																		 ORDER BY
																																																																		   kartustok.kdbrg DESC
																																																																		 LIMIT
																																																																		   1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																							 FROM
																																																																							   kartustok
																																																																							 WHERE
																																																																							   kartustok.kdbrg = barang.kdbrg
																																																																							   AND
																																																																							   kartustok.idjnskartustok = 6
																																																																							   AND
																																																																							   kartustok.idbagian = bbagian.idbagian
																																																																							   AND
																																																																							   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																							 ORDER BY
																																																																							   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																																																	FROM
																																																																																	  kartustok
																																																																																	WHERE
																																																																																	  kartustok.kdbrg = barang.kdbrg
																																																																																	  AND
																																																																																	  kartustok.idjnskartustok = 7
																																																																																	  AND
																																																																																	  kartustok.idbagian = bbagian.idbagian
																																																																																	  AND
																																																																																	  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																	ORDER BY
																																																																																	  kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
																																																																																										   FROM
																																																																																											 kartustok
																																																																																										   WHERE
																																																																																											 kartustok.kdbrg = barang.kdbrg
																																																																																											 AND
																																																																																											 kartustok.idbagian = bbagian.idbagian
																																																																																											 AND
																																																																																											 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																											 AND
																																																																																											 kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																											 AND
																																																																																											 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																																 FROM
																																																																																																   kartustok kstok
																																																																																																 WHERE
																																																																																																   kstok.kdbrg = barang.kdbrg
																																																																																																   AND
																																																																																																   kstok.idbagian = bbagian.idbagian
																																																																																																   AND
																																																																																																   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																   AND
																																																																																																   kstok.`idjnskartustok` = '19')
																																																																																										   ORDER BY
																																																																																											 kartustok.kdbrg DESC
																																																																																										   LIMIT
																																																																																											 1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																																															   FROM
																																																																																																 kartustok
																																																																																															   WHERE
																																																																																																 kartustok.kdbrg = barang.kdbrg
																																																																																																 AND
																																																																																																 kartustok.idjnskartustok IN (14, 15)
																																																																																																 AND
																																																																																																 kartustok.idbagian = bbagian.idbagian
																																																																																																 AND
																																																																																																 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																															   ORDER BY
																																																																																																 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
																																																																																																									  FROM
																																																																																																										kartustok
																																																																																																									  WHERE
																																																																																																										kartustok.kdbrg = barang.kdbrg
																																																																																																										AND
																																																																																																										kartustok.idbagian = bbagian.idbagian
																																																																																																										AND
																																																																																																										kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																									  ORDER BY
																																																																																																										kartustok.kdbrg DESC
																																																																																																									  LIMIT
																																																																																																										1), 0))) AS sa
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS sisa			
									 
					 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																				  FROM
																					kartustok
																				  WHERE
																					kartustok.kdbrg = barang.kdbrg
																					AND
																					kartustok.idjnskartustok = 10
																					AND
																					kartustok.kode = '0306'
																					AND
																					kartustok.idbagian = bbagian.idbagian
																					AND
																					kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																				  ORDER BY
																					kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																														 FROM
																														   kartustok
																														 WHERE
																														   kartustok.kdbrg = barang.kdbrg
																														   AND
																														   kartustok.idjnskartustok = 11
																														   AND
																														   kartustok.kode = '0306'
																														   AND
																														   kartustok.idbagian = bbagian.idbagian
																														   AND
																														   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																														 ORDER BY
																														   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																								FROM
																																								  kartustok
																																								WHERE
																																								  kartustok.kdbrg = barang.kdbrg
																																								  AND
																																								  kartustok.idjnskartustok = 7
																																								  AND
																																								  kartustok.idbagian = bbagian.idbagian
																																								  AND
																																								  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																								ORDER BY
																																								  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
																																																			  FROM
																																																				kartustok
																																																			  WHERE
																																																				kartustok.kdbrg = barang.kdbrg
																																																				AND
																																																				kartustok.idbagian = bbagian.idbagian
																																																				AND
																																																				kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																				AND
																																																				kartustok.idjnskartustok = 1
																																																			  ORDER BY
																																																				kartustok.kdbrg DESC
																																																			  LIMIT
																																																				1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																								 FROM
																																																								   kartustok
																																																								 WHERE
																																																								   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																								   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																								   AND (kartustok.idbagian = bbagian.idbagian)
																																																								   AND (kartustok.`idjnskartustok` = 2))
																																																								 ORDER BY
																																																								   kartustok.kdbrg DESC
																																																								 LIMIT
																																																								   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
																																																													 FROM
																																																													   kartustok
																																																													 WHERE
																																																													   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																													   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																													   AND (kartustok.idbagian = bbagian.idbagian)
																																																													   AND (kartustok.`idjnskartustok` = 18))
																																																													 ORDER BY
																																																													   kartustok.kdbrg DESC
																																																													 LIMIT
																																																													   1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
																																																																		 FROM
																																																																		   kartustok
																																																																		 WHERE
																																																																		   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																																		   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																																		   AND (kartustok.idbagian = bbagian.idbagian)
																																																																		   AND (kartustok.`idjnskartustok` = 5))
																																																																		 ORDER BY
																																																																		   kartustok.kdbrg DESC
																																																																		 LIMIT
																																																																		   1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																							 FROM
																																																																							   kartustok
																																																																							 WHERE
																																																																							   kartustok.kdbrg = barang.kdbrg
																																																																							   AND
																																																																							   kartustok.idjnskartustok = 6
																																																																							   AND
																																																																							   kartustok.idbagian = bbagian.idbagian
																																																																							   AND
																																																																							   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																							 ORDER BY
																																																																							   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																																																	FROM
																																																																																	  kartustok
																																																																																	WHERE
																																																																																	  kartustok.kdbrg = barang.kdbrg
																																																																																	  AND
																																																																																	  kartustok.idjnskartustok = 7
																																																																																	  AND
																																																																																	  kartustok.idbagian = bbagian.idbagian
																																																																																	  AND
																																																																																	  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																	ORDER BY
																																																																																	  kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
																																																																																										   FROM
																																																																																											 kartustok
																																																																																										   WHERE
																																																																																											 kartustok.kdbrg = barang.kdbrg
																																																																																											 AND
																																																																																											 kartustok.idbagian = bbagian.idbagian
																																																																																											 AND
																																																																																											 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																											 AND
																																																																																											 kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																											 AND
																																																																																											 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																																	 FROM
																																																																																																	   kartustok kstok
																																																																																																	 WHERE
																																																																																																	   kstok.kdbrg = barang.kdbrg
																																																																																																	   AND
																																																																																																	   kstok.idbagian = bbagian.idbagian
																																																																																																	   AND
																																																																																																	   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																	   AND
																																																																																																	   kstok.`idjnskartustok` = '19')
																																																																																										   ORDER BY
																																																																																											 kartustok.kdbrg DESC
																																																																																										   LIMIT
																																																																																											 1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																																															   FROM
																																																																																																 kartustok
																																																																																															   WHERE
																																																																																																 kartustok.kdbrg = barang.kdbrg
																																																																																																 AND
																																																																																																 kartustok.idjnskartustok IN (14, 15)
																																																																																																 AND
																																																																																																 kartustok.idbagian = bbagian.idbagian
																																																																																																 AND
																																																																																																 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																															   ORDER BY
																																																																																																 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
																																																																																																									  FROM
																																																																																																										kartustok
																																																																																																									  WHERE
																																																																																																										kartustok.kdbrg = barang.kdbrg
																																																																																																										AND
																																																																																																										kartustok.idbagian = bbagian.idbagian
																																																																																																										AND
																																																																																																										kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																									  ORDER BY
																																																																																																										kartustok.kdbrg DESC
																																																																																																									  LIMIT
																																																																																																										1), 0))) AS sa
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0)  * ifnull((SELECT max(kartustok.hrgbeli) AS hbli
												  FROM
													kartustok
												  WHERE
													kartustok.kdbrg = barang.kdbrg
													AND
													kartustok.idbagian = bbagian.idbagian
													AND
													kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
												  ORDER BY
													kartustok.idkartustok DESC
												  LIMIT
													1), 0) AS hrgbeli
								 
					 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																				  FROM
																					kartustok
																				  WHERE
																					kartustok.kdbrg = barang.kdbrg
																					AND
																					kartustok.idjnskartustok = 10
																					AND
																					kartustok.kode = '0306'
																					AND
																					kartustok.idbagian = bbagian.idbagian
																					AND
																					kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																				  ORDER BY
																					kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																														 FROM
																														   kartustok
																														 WHERE
																														   kartustok.kdbrg = barang.kdbrg
																														   AND
																														   kartustok.idjnskartustok = 11
																														   AND
																														   kartustok.kode = '0306'
																														   AND
																														   kartustok.idbagian = bbagian.idbagian
																														   AND
																														   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																														 ORDER BY
																														   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																								FROM
																																								  kartustok
																																								WHERE
																																								  kartustok.kdbrg = barang.kdbrg
																																								  AND
																																								  kartustok.idjnskartustok = 7
																																								  AND
																																								  kartustok.idbagian = bbagian.idbagian
																																								  AND
																																								  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																								ORDER BY
																																								  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
																																																			  FROM
																																																				kartustok
																																																			  WHERE
																																																				kartustok.kdbrg = barang.kdbrg
																																																				AND
																																																				kartustok.idbagian = bbagian.idbagian
																																																				AND
																																																				kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																				AND
																																																				kartustok.idjnskartustok = 1
																																																			  ORDER BY
																																																				kartustok.kdbrg DESC
																																																			  LIMIT
																																																				1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																								 FROM
																																																								   kartustok
																																																								 WHERE
																																																								   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																								   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																								   AND (kartustok.idbagian = bbagian.idbagian)
																																																								   AND (kartustok.`idjnskartustok` = 2))
																																																								 ORDER BY
																																																								   kartustok.kdbrg DESC
																																																								 LIMIT
																																																								   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
																																																													 FROM
																																																													   kartustok
																																																													 WHERE
																																																													   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																													   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																													   AND (kartustok.idbagian = bbagian.idbagian)
																																																													   AND (kartustok.`idjnskartustok` = 18))
																																																													 ORDER BY
																																																													   kartustok.kdbrg DESC
																																																													 LIMIT
																																																													   1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
																																																																		 FROM
																																																																		   kartustok
																																																																		 WHERE
																																																																		   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																																		   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																																		   AND (kartustok.idbagian = bbagian.idbagian)
																																																																		   AND (kartustok.`idjnskartustok` = 5))
																																																																		 ORDER BY
																																																																		   kartustok.kdbrg DESC
																																																																		 LIMIT
																																																																		   1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																							 FROM
																																																																							   kartustok
																																																																							 WHERE
																																																																							   kartustok.kdbrg = barang.kdbrg
																																																																							   AND
																																																																							   kartustok.idjnskartustok = 6
																																																																							   AND
																																																																							   kartustok.idbagian = bbagian.idbagian
																																																																							   AND
																																																																							   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																							 ORDER BY
																																																																							   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																																																	FROM
																																																																																	  kartustok
																																																																																	WHERE
																																																																																	  kartustok.kdbrg = barang.kdbrg
																																																																																	  AND
																																																																																	  kartustok.idjnskartustok = 7
																																																																																	  AND
																																																																																	  kartustok.idbagian = bbagian.idbagian
																																																																																	  AND
																																																																																	  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																	ORDER BY
																																																																																	  kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
																																																																																										   FROM
																																																																																											 kartustok
																																																																																										   WHERE
																																																																																											 kartustok.kdbrg = barang.kdbrg
																																																																																											 AND
																																																																																											 kartustok.idbagian = bbagian.idbagian
																																																																																											 AND
																																																																																											 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																											 AND
																																																																																											 kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																											 AND
																																																																																											 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																																	 FROM
																																																																																																	   kartustok kstok
																																																																																																	 WHERE
																																																																																																	   kstok.kdbrg = barang.kdbrg
																																																																																																	   AND
																																																																																																	   kstok.idbagian = bbagian.idbagian
																																																																																																	   AND
																																																																																																	   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																	   AND
																																																																																																	   kstok.`idjnskartustok` = '19')
																																																																																										   ORDER BY
																																																																																											 kartustok.kdbrg DESC
																																																																																										   LIMIT
																																																																																											 1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																																															   FROM
																																																																																																 kartustok
																																																																																															   WHERE
																																																																																																 kartustok.kdbrg = barang.kdbrg
																																																																																																 AND
																																																																																																 kartustok.idjnskartustok IN (14, 15)
																																																																																																 AND
																																																																																																 kartustok.idbagian = bbagian.idbagian
																																																																																																 AND
																																																																																																 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																															   ORDER BY
																																																																																																 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
																																																																																																									  FROM
																																																																																																										kartustok
																																																																																																									  WHERE
																																																																																																										kartustok.kdbrg = barang.kdbrg
																																																																																																										AND
																																																																																																										kartustok.idbagian = bbagian.idbagian
																																																																																																										AND
																																																																																																										kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																									  ORDER BY
																																																																																																										kartustok.kdbrg DESC
																																																																																																									  LIMIT
																																																																																																										1), 0))) AS sa
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) * ifnull((SELECT max(kartustok.hrgjual) AS hbli
												  FROM
													kartustok
												  WHERE
													kartustok.kdbrg = barang.kdbrg
													AND
													kartustok.idbagian = bbagian.idbagian
													AND
													kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
												  ORDER BY
													kartustok.idkartustok DESC
												  LIMIT
													1), 0) AS hrgjual						 

						 , ifnull((SELECT if(kartustok.`idjnskartustok` = 3, sum(kartustok.jmlkeluar * kartustok.hrgbeli), 0) AS jml
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
									 AND
									 kartustok.`idjnskartustok` IN (3)
									 AND
									 kartustok.noref NOT IN (SELECT kstok.noref AS kd
														 FROM
														   kartustok kstok
														 WHERE
														   kstok.kdbrg = barang.kdbrg
														   AND
														   kstok.idbagian = bbagian.idbagian
														   AND
														   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
														   AND
														   kstok.`idjnskartustok` = '19')
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) - ifnull((SELECT sum(kartustok.jmlmasuk * kartustok.hrgbeli) AS jmlmasuk
													  FROM
														kartustok
													  WHERE
														kartustok.kdbrg = barang.kdbrg
														AND
														kartustok.idjnskartustok IN (14, 15)
														AND
														kartustok.idbagian = bbagian.idbagian
														AND
														kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
													  ORDER BY
														kartustok.kdbrg DESC), 0) AS nbli

						 , ifnull((SELECT if(kartustok.`idjnskartustok` = 3, sum(kartustok.jmlkeluar * kartustok.hrgjual), 0) AS jml
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
									 AND
									 kartustok.`idjnskartustok` IN (3)
									 AND
									 kartustok.noref NOT IN (SELECT kstok.noref AS kd
														 FROM
														   kartustok kstok
														 WHERE
														   kstok.kdbrg = barang.kdbrg
														   AND
														   kstok.idbagian = bbagian.idbagian
														   AND
														   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
														   AND
														   kstok.`idjnskartustok` = '19')

								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) - ifnull((SELECT sum(kartustok.jmlmasuk * kartustok.hrgjual) AS jmlmasuk
													  FROM
														kartustok
													  WHERE
														kartustok.kdbrg = barang.kdbrg
														AND
														kartustok.idjnskartustok IN (14, 15)
														AND
														kartustok.idbagian = bbagian.idbagian
														AND
														kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
													  ORDER BY
														kartustok.kdbrg DESC), 0) AS njual
								 ", false);
		$this->db->from('barangbagian bbagian', false);
		$this->db->join("barang", "bbagian.kdbrg = barang.kdbrg", "left", false);
		$this->db->where("barang.idstatus <> '0'");
		$this->db->where("bbagian.idbagian = '". $idbagian ."'");
		$this->db->group_by("barang.kdbrg");
		$this->db->order_by('barang.nmbrg');
		//$this->db->limit("5");
		$query = $this->db->get();
		$aaa = $query->result();
		
		// add a page
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		//Set Footer
		$this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_lap->setPrintFooter(true); // enabled ? true
		//$this->pdf_lap->setFooterData($tc=array(0,64,10), $lc=array(0,64,0)); //to color line
		$this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$this->pdf_lap->SetAutoPageBreak(TRUE, '15');
		
		$this->pdf_lap->SetPrintHeader(false);
		$this->pdf_lap->AddPage('L', $page_format, false, false);
		$this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
		$this->pdf_lap->SetFont('helvetica', '', 14);
		
		$x=0;$y=10;
		$this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
		$this->pdf_lap->Cell(0, 0, 'Laporan Inventory', 0, 1, 'C', 0, '', 0);
		
		$this->pdf_lap->SetFont('helvetica', '', 10);
		$this->pdf_lap->Cell(0, 0, 'Periode : '. date("d F Y", strtotime($tglawal)) .' - '.date("d F Y", strtotime($tglakhir)), 0, 1, 'C', 0, '', 0);
		$this->pdf_lap->Cell(0, 0, 'Bagian : '. $nmbagian, 0, 1, 'C', 0, '', 0);


		$total_sa = 0;
		$total_beli = 0;
		$total_jmlretbeli = 0;
		$total_jmltbl = 0;
		$total_jmldistribusibrg = 0;
		$total_jmldistribusimasukbrg = 0;
		$total_jmlrkbb = 0;
		$total_jmlrmbg = 0;
		$total_jmlbhp = 0;
		$total_jmljualbrg = 0;
		$total_jmlretjualbrg = 0;
		$total_sisa = 0;
		$total_hrgb = 0;
		$total_hrgj = 0;
		$total_nbli = 0;
		$total_njual = 0;

		$no = 1;
		foreach($aaa as $i=>$val){
			$isi .= "<tr>
					<td width=\"3.5%\" align=\"center\">". ($i+1) .".</td>
					<td width=\"6%\" align=\"center\">". $val->kdbrg ."</td>
					<td width=\"16%\" align=\"left\">". $val->nmbrg ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->sa,0,',','.') ."</td>
					<td width=\"4.5%\" align=\"right\">". number_format($val->beli,0,',','.') ."</td>
					<td width=\"4.5%\" align=\"right\">". number_format($val->jmlretbeli,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->jmltbl,0,',','.') ."</td>
					<td width=\"4.5%\" align=\"right\">". number_format($val->jmldistribusibrg,0,',','.') ."</td>
					<td width=\"4.5%\" align=\"right\">". number_format($val->jmldistribusimasukbrg,0,',','.') ."</td>
					<td width=\"4.5%\" align=\"right\">". number_format($val->jmlrkbb,0,',','.') ."</td>
					<td width=\"4.5%\" align=\"right\">". number_format($val->jmlrmbg,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->jmlbhp,0,',','.') ."</td>
					<td width=\"4.5%\" align=\"right\">". number_format($val->jmljualbrg,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->jmlretjualbrg,0,',','.') ."</td>
					<td width=\"4%\" align=\"right\">". number_format($val->sisa,0,',','.') ."</td>
					<td width=\"6.5%\" align=\"right\">". number_format($val->hrgbeli,0,',','.') ."</td>
					<td width=\"6.5%\" align=\"right\">". number_format($val->hrgjual,0,',','.') ."</td>
					<td width=\"6.5%\" align=\"right\">". number_format($val->nbli,0,',','.') ."</td>
					<td width=\"6.5%\" align=\"right\">". number_format($val->njual,0,',','.') ."</td>
				</tr>";	

				$total_sa += $val->sa;
				$total_beli += $val->beli;
				$total_jmlretbeli += $val->jmlretbeli;
				$total_jmltbl += $val->jmltbl;
				$total_jmldistribusibrg += $val->jmldistribusibrg;
				$total_jmldistribusimasukbrg += $val->jmldistribusimasukbrg;
				$total_jmlrkbb += $val->jmlrkbb;
				$total_jmlrmbg += $val->jmlrmbg;
				$total_jmlbhp += $val->jmlbhp;
				$total_jmljualbrg += $val->jmljualbrg;
				$total_jmlretjualbrg += $val->jmlretjualbrg;
				$total_sisa += $val->sisa;
				$total_hrgb += $val->hrgbeli;
				$total_hrgj += $val->hrgjual;
				$total_nbli += $val->nbli;
				$total_njual += $val->njual;

		}
		
		$html = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"1px\" cellpadding=\"2\">
				<tr align=\"center\">
					<td width=\"3.5%\">No.</td>
					<td width=\"6%\">Kode <br> Barang</td>
					<td width=\"16%\">Nama Barang</td>
					<td width=\"4%\">SA</td>
					<td width=\"4.5%\">BELI</td>
					<td width=\"4.5%\">R.BELI</td>
					<td width=\"4%\">TBL</td>
					<td width=\"4.5%\">D.KB</td>
					<td width=\"4.5%\">D.MB</td>
					<td width=\"4.5%\">R.KBG</td>
					<td width=\"4.5%\">R.MBG</td>
					<td width=\"4%\">BHP</td>
					<td width=\"4.5%\">Jual</td>
					<td width=\"4%\">R.Jual</td>
					<td width=\"4%\">Sisa</td>
					<td width=\"6.5%\">Harga Beli</td>
					<td width=\"6.5%\">Harga Jual</td>
					<td width=\"6.5%\">N.Beli</td>
					<td width=\"6.5%\">N.Jual</td>
				</tr>
			  <tbody>
			  	". $isi ."
			  	<tr>
					<td colspan=\"3\" align=\"right\"><strong>Total</strong></td>
					<td align=\"right\">". number_format($total_sa,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_beli,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmlretbeli,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmltbl,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmldistribusibrg,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmldistribusimasukbrg,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmlrkbb,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmlrmbg,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmlbhp,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmljualbrg,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_jmlretjualbrg,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_sisa,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_hrgb,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_hrgj,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_nbli,0,',','.') ."</td>
					<td align=\"right\">". number_format($total_njual,0,',','.') ."</td>
				</tr>
			  </tbody>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html,true,false,false,false);
		
		$html1 = "<br/><br/><font size=\"7\" face=\"Helvetica\">
			<table border=\"0px\" cellpadding=\"2\">
				<tr align=\"left\">
					<td> KETERANGAN :</td>
				</tr>
				<tr>
					<td width=\"2%\" align=\"center\">1.</td>
					<td width=\"6.5%\" align=\"left\">SA</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Saldo Awal/Stock Opname</td>
					<td width=\"2%\" align=\"center\">8.</td>
					<td width=\"6.5%\" align=\"left\">R.MBG</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Retur Masuk Barang Bagian</td>
				</tr>
				<tr>
					<td width=\"2%\" align=\"center\">2.</td>
					<td width=\"6.5%\" align=\"left\">Beli</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Pembelian Barang</td>
					<td width=\"2%\" align=\"center\">9.</td>
					<td width=\"6.5%\" align=\"left\">BHP</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Barang Habis Pakai</td>
				</tr>
				<tr>
					<td width=\"2%\" align=\"center\">3.</td>
					<td width=\"6.5%\" align=\"left\">R.Beli</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Returt Pembelian Supplier</td>
					<td width=\"2%\" align=\"center\">10.</td>
					<td width=\"6.5%\" align=\"left\">Jual</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Penjualan Barang(Farmasi)</td>
				</tr>
				<tr>
					<td width=\"2%\" align=\"center\">4.</td>
					<td width=\"6.5%\" align=\"left\">TBL</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Penerimaan Barang Lain-Lain</td>
					<td width=\"2%\" align=\"center\">11.</td>
					<td width=\"6.5%\" align=\"left\">R.Jual</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Returt Penjualan Barang(Farmasi)</td>
				</tr>
				<tr>
					<td width=\"2%\" align=\"center\">5.</td>
					<td width=\"6.5%\" align=\"left\">D.KB</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Distribusi Keluar Barang</td>
					<td width=\"2%\" align=\"center\">12.</td>
					<td width=\"6.5%\" align=\"left\">N.Beli</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Akumulasi Nilai Pembelian</td>
				</tr>
				<tr>
					<td width=\"2%\" align=\"center\">6.</td>
					<td width=\"6.5%\" align=\"left\">D.MB</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Distribusi Masuk Barang</td>
					<td width=\"2%\" align=\"center\">13.</td>
					<td width=\"6.5%\" align=\"left\">N.Jual</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Akumulasi Nilai Penjualan</td>
				</tr>
				<tr>
					<td width=\"2%\" align=\"center\">7.</td>
					<td width=\"6.5%\" align=\"left\">R.KBG</td>
					<td width=\"1%\" align=\"left\">:</td>
					<td width=\"17%\" align=\"left\">Retur Keluar Barang Bagian</td>
				</tr>
			</table></font>
		";
		$this->pdf_lap->writeHTML($html1,true,false,false,false);


		//Close and output PDF document
		$this->pdf_lap->Output('Lap_inventory.pdf', 'I');
	}
	
	function laporan_excelinventory($tglawal, $tglakhir, $idbagian) {
		$header = array(
			'Kode Barang',
			'Nama Barang',
			'SA',
			'BELI',
			'R.BELI',
			'TBL',
			'D.KB',
			'D.MB',
			'R.KBG',
			'R.RMBG',
			'BHP',
			'JUAL',
			'R.JUAL',
			'SISA',
			'HARGA BELI',
			'HARGA JUAL',
			'N.BELI',
			'N.NJUAL',
		);
		
		$this->db->select("barang.kdbrg
						 , barang.nmbrg
						 , ifnull((SELECT sum(kartustok.saldoawal) AS sa
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS sa

						 , ifnull((SELECT sum(kartustok.jmlmasuk) - (ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																			 FROM
																			   kartustok
																			 WHERE
																			   ((kartustok.`kdbrg` = barang.`kdbrg`)
																			   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																			   AND (kartustok.idbagian = bbagian.idbagian)
																			   AND (kartustok.`idjnskartustok` = 2))
																			   AND
																			   kartustok.noref = (SELECT rsdet.noretursupplier AS noretursupplier
																								  FROM
																									retursupplierdet rsdet
																								  WHERE
																									rsdet.idstbayar <> '2'
																									AND
																									rsdet.noretursupplier = kartustok.noref
																									AND
																									rsdet.kdbrg = kartustok.kdbrg
																								  GROUP BY
																									kartustok.kdbrg DESC
																								  LIMIT
																									1)
																			 ORDER BY
																			   kartustok.kdbrg DESC
																			 LIMIT
																			   1), 0)) AS jml
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
									 AND
									 kartustok.idjnskartustok = 1
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS beli

						 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
								   FROM
									 kartustok
								   WHERE
									 ((kartustok.`kdbrg` = barang.`kdbrg`)
									 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
									 AND (kartustok.idbagian = bbagian.idbagian)
									 AND (kartustok.`idjnskartustok` = 2))
									 AND
									 kartustok.noref = (SELECT rsdet.noretursupplier AS noretursupplier
														FROM
														  retursupplierdet rsdet
														WHERE
														  rsdet.idstbayar = '2'
														  AND
														  rsdet.noretursupplier = kartustok.noref
														  AND
														  rsdet.kdbrg = kartustok.kdbrg
														GROUP BY
														  kartustok.kdbrg DESC
														LIMIT
														  1)
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS jmlretbeli

						 , ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
								   FROM
									 kartustok
								   WHERE
									 ((kartustok.`kdbrg` = barang.`kdbrg`)
									 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
									 AND (kartustok.idbagian = bbagian.idbagian)
									 AND (kartustok.`idjnskartustok` = 18))
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS jmltbl

						 , ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
								   FROM
									 kartustok
								   WHERE
									 ((kartustok.`kdbrg` = barang.`kdbrg`)
									 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
									 AND (kartustok.idbagian = bbagian.idbagian)
									 AND (kartustok.`idjnskartustok` = 5))
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS `jmldistribusibrg`

						 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok = 10
									 AND
									 kartustok.kode = '0306'
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmldistribusimasukbrg`

						 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok = 11
									 AND
									 kartustok.kode = '0306'
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmlrkbb`

						 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok = 6
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmlrmbg`

						 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok = 7
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmlbhp`

						 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
									 AND
									 kartustok.`idjnskartustok` IN (3, 16, 17)
									 AND
									 kartustok.noref NOT IN (SELECT kstok.noref AS kd
														 FROM
														   kartustok kstok
														 WHERE
														   kstok.kdbrg = barang.kdbrg
														   AND
														   kstok.idbagian = bbagian.idbagian
														   AND
														   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
														   AND
														   kstok.`idjnskartustok` = '19')

								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS `jmljualbrg`

						 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idjnskartustok IN (14, 15)
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC), 0) AS `jmlretjualbrg`

						 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																					  FROM
																						kartustok
																					  WHERE
																						kartustok.kdbrg = barang.kdbrg
																						AND
																						kartustok.idjnskartustok = 10
																						AND
																						kartustok.kode = '0306'
																						AND
																						kartustok.idbagian = bbagian.idbagian
																						AND
																						kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																					  ORDER BY
																						kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																															 FROM
																															   kartustok
																															 WHERE
																															   kartustok.kdbrg = barang.kdbrg
																															   AND
																															   kartustok.idjnskartustok = 11
																															   AND
																															   kartustok.kode = '0306'
																															   AND
																															   kartustok.idbagian = bbagian.idbagian
																															   AND
																															   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																															 ORDER BY
																															   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																									FROM
																																									  kartustok
																																									WHERE
																																									  kartustok.kdbrg = barang.kdbrg
																																									  AND
																																									  kartustok.idjnskartustok = 7
																																									  AND
																																									  kartustok.idbagian = bbagian.idbagian
																																									  AND
																																									  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																									ORDER BY
																																									  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
																																																				  FROM
																																																					kartustok
																																																				  WHERE
																																																					kartustok.kdbrg = barang.kdbrg
																																																					AND
																																																					kartustok.idbagian = bbagian.idbagian
																																																					AND
																																																					kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																					AND
																																																					kartustok.idjnskartustok = 1
																																																				  ORDER BY
																																																					kartustok.kdbrg DESC
																																																				  LIMIT
																																																					1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																									 FROM
																																																									   kartustok
																																																									 WHERE
																																																									   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																									   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																									   AND (kartustok.idbagian = bbagian.idbagian)
																																																									   AND (kartustok.`idjnskartustok` = 2))
																																																									 ORDER BY
																																																									   kartustok.kdbrg DESC
																																																									 LIMIT
																																																									   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
																																																														 FROM
																																																														   kartustok
																																																														 WHERE
																																																														   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																														   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																														   AND (kartustok.idbagian = bbagian.idbagian)
																																																														   AND (kartustok.`idjnskartustok` = 18))
																																																														 ORDER BY
																																																														   kartustok.kdbrg DESC
																																																														 LIMIT
																																																														   1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
																																																																			 FROM
																																																																			   kartustok
																																																																			 WHERE
																																																																			   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																																			   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																																			   AND (kartustok.idbagian = bbagian.idbagian)
																																																																			   AND (kartustok.`idjnskartustok` = 5))
																																																																			 ORDER BY
																																																																			   kartustok.kdbrg DESC
																																																																			 LIMIT
																																																																			   1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																								 FROM
																																																																								   kartustok
																																																																								 WHERE
																																																																								   kartustok.kdbrg = barang.kdbrg
																																																																								   AND
																																																																								   kartustok.idjnskartustok = 6
																																																																								   AND
																																																																								   kartustok.idbagian = bbagian.idbagian
																																																																								   AND
																																																																								   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																								 ORDER BY
																																																																								   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																																																		FROM
																																																																																		  kartustok
																																																																																		WHERE
																																																																																		  kartustok.kdbrg = barang.kdbrg
																																																																																		  AND
																																																																																		  kartustok.idjnskartustok = 7
																																																																																		  AND
																																																																																		  kartustok.idbagian = bbagian.idbagian
																																																																																		  AND
																																																																																		  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																		ORDER BY
																																																																																		  kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
																																																																																											   FROM
																																																																																												 kartustok
																																																																																											   WHERE
																																																																																												 kartustok.kdbrg = barang.kdbrg
																																																																																												 AND
																																																																																												 kartustok.idbagian = bbagian.idbagian
																																																																																												 AND
																																																																																												 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																												 AND
																																																																																												 kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																												 AND
																																																																																												 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																																	 FROM
																																																																																																	   kartustok kstok
																																																																																																	 WHERE
																																																																																																	   kstok.kdbrg = barang.kdbrg
																																																																																																	   AND
																																																																																																	   kstok.idbagian = bbagian.idbagian
																																																																																																	   AND
																																																																																																	   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																	   AND
																																																																																																	   kstok.`idjnskartustok` = '19')
																																																																																											   ORDER BY
																																																																																												 kartustok.kdbrg DESC
																																																																																											   LIMIT
																																																																																												 1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																																																   FROM
																																																																																																	 kartustok
																																																																																																   WHERE
																																																																																																	 kartustok.kdbrg = barang.kdbrg
																																																																																																	 AND
																																																																																																	 kartustok.idjnskartustok IN (14, 15)
																																																																																																	 AND
																																																																																																	 kartustok.idbagian = bbagian.idbagian
																																																																																																	 AND
																																																																																																	 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																   ORDER BY
																																																																																																	 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
																																																																																																										  FROM
																																																																																																											kartustok
																																																																																																										  WHERE
																																																																																																											kartustok.kdbrg = barang.kdbrg
																																																																																																											AND
																																																																																																											kartustok.idbagian = bbagian.idbagian
																																																																																																											AND
																																																																																																											kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																										  ORDER BY
																																																																																																											kartustok.kdbrg DESC
																																																																																																										  LIMIT
																																																																																																											1), 0))) AS sa
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) AS sisa		
									 
					 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																				  FROM
																					kartustok
																				  WHERE
																					kartustok.kdbrg = barang.kdbrg
																					AND
																					kartustok.idjnskartustok = 10
																					AND
																					kartustok.kode = '0306'
																					AND
																					kartustok.idbagian = bbagian.idbagian
																					AND
																					kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																				  ORDER BY
																					kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																														 FROM
																														   kartustok
																														 WHERE
																														   kartustok.kdbrg = barang.kdbrg
																														   AND
																														   kartustok.idjnskartustok = 11
																														   AND
																														   kartustok.kode = '0306'
																														   AND
																														   kartustok.idbagian = bbagian.idbagian
																														   AND
																														   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																														 ORDER BY
																														   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																								FROM
																																								  kartustok
																																								WHERE
																																								  kartustok.kdbrg = barang.kdbrg
																																								  AND
																																								  kartustok.idjnskartustok = 7
																																								  AND
																																								  kartustok.idbagian = bbagian.idbagian
																																								  AND
																																								  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																								ORDER BY
																																								  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
																																																			  FROM
																																																				kartustok
																																																			  WHERE
																																																				kartustok.kdbrg = barang.kdbrg
																																																				AND
																																																				kartustok.idbagian = bbagian.idbagian
																																																				AND
																																																				kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																				AND
																																																				kartustok.idjnskartustok = 1
																																																			  ORDER BY
																																																				kartustok.kdbrg DESC
																																																			  LIMIT
																																																				1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																								 FROM
																																																								   kartustok
																																																								 WHERE
																																																								   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																								   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																								   AND (kartustok.idbagian = bbagian.idbagian)
																																																								   AND (kartustok.`idjnskartustok` = 2))
																																																								 ORDER BY
																																																								   kartustok.kdbrg DESC
																																																								 LIMIT
																																																								   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
																																																													 FROM
																																																													   kartustok
																																																													 WHERE
																																																													   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																													   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																													   AND (kartustok.idbagian = bbagian.idbagian)
																																																													   AND (kartustok.`idjnskartustok` = 18))
																																																													 ORDER BY
																																																													   kartustok.kdbrg DESC
																																																													 LIMIT
																																																													   1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
																																																																		 FROM
																																																																		   kartustok
																																																																		 WHERE
																																																																		   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																																		   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																																		   AND (kartustok.idbagian = bbagian.idbagian)
																																																																		   AND (kartustok.`idjnskartustok` = 5))
																																																																		 ORDER BY
																																																																		   kartustok.kdbrg DESC
																																																																		 LIMIT
																																																																		   1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																							 FROM
																																																																							   kartustok
																																																																							 WHERE
																																																																							   kartustok.kdbrg = barang.kdbrg
																																																																							   AND
																																																																							   kartustok.idjnskartustok = 6
																																																																							   AND
																																																																							   kartustok.idbagian = bbagian.idbagian
																																																																							   AND
																																																																							   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																							 ORDER BY
																																																																							   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																																																	FROM
																																																																																	  kartustok
																																																																																	WHERE
																																																																																	  kartustok.kdbrg = barang.kdbrg
																																																																																	  AND
																																																																																	  kartustok.idjnskartustok = 7
																																																																																	  AND
																																																																																	  kartustok.idbagian = bbagian.idbagian
																																																																																	  AND
																																																																																	  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																	ORDER BY
																																																																																	  kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
																																																																																										   FROM
																																																																																											 kartustok
																																																																																										   WHERE
																																																																																											 kartustok.kdbrg = barang.kdbrg
																																																																																											 AND
																																																																																											 kartustok.idbagian = bbagian.idbagian
																																																																																											 AND
																																																																																											 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																											 AND
																																																																																											 kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																											 AND
																																																																																											 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																																	 FROM
																																																																																																	   kartustok kstok
																																																																																																	 WHERE
																																																																																																	   kstok.kdbrg = barang.kdbrg
																																																																																																	   AND
																																																																																																	   kstok.idbagian = bbagian.idbagian
																																																																																																	   AND
																																																																																																	   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																	   AND
																																																																																																	   kstok.`idjnskartustok` = '19')
																																																																																										   ORDER BY
																																																																																											 kartustok.kdbrg DESC
																																																																																										   LIMIT
																																																																																											 1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																																															   FROM
																																																																																																 kartustok
																																																																																															   WHERE
																																																																																																 kartustok.kdbrg = barang.kdbrg
																																																																																																 AND
																																																																																																 kartustok.idjnskartustok IN (14, 15)
																																																																																																 AND
																																																																																																 kartustok.idbagian = bbagian.idbagian
																																																																																																 AND
																																																																																																 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																															   ORDER BY
																																																																																																 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
																																																																																																									  FROM
																																																																																																										kartustok
																																																																																																									  WHERE
																																																																																																										kartustok.kdbrg = barang.kdbrg
																																																																																																										AND
																																																																																																										kartustok.idbagian = bbagian.idbagian
																																																																																																										AND
																																																																																																										kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																									  ORDER BY
																																																																																																										kartustok.kdbrg DESC
																																																																																																									  LIMIT
																																																																																																										1), 0))) AS sa
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0)  * ifnull((SELECT max(kartustok.hrgbeli) AS hbli
												  FROM
													kartustok
												  WHERE
													kartustok.kdbrg = barang.kdbrg
													AND
													kartustok.idbagian = bbagian.idbagian
													AND
													kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
												  ORDER BY
													kartustok.idkartustok DESC
												  LIMIT
													1), 0) AS hrgbeli
								 
					 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																				  FROM
																					kartustok
																				  WHERE
																					kartustok.kdbrg = barang.kdbrg
																					AND
																					kartustok.idjnskartustok = 10
																					AND
																					kartustok.kode = '0306'
																					AND
																					kartustok.idbagian = bbagian.idbagian
																					AND
																					kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																				  ORDER BY
																					kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																														 FROM
																														   kartustok
																														 WHERE
																														   kartustok.kdbrg = barang.kdbrg
																														   AND
																														   kartustok.idjnskartustok = 11
																														   AND
																														   kartustok.kode = '0306'
																														   AND
																														   kartustok.idbagian = bbagian.idbagian
																														   AND
																														   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																														 ORDER BY
																														   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																								FROM
																																								  kartustok
																																								WHERE
																																								  kartustok.kdbrg = barang.kdbrg
																																								  AND
																																								  kartustok.idjnskartustok = 7
																																								  AND
																																								  kartustok.idbagian = bbagian.idbagian
																																								  AND
																																								  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																								ORDER BY
																																								  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
																																																			  FROM
																																																				kartustok
																																																			  WHERE
																																																				kartustok.kdbrg = barang.kdbrg
																																																				AND
																																																				kartustok.idbagian = bbagian.idbagian
																																																				AND
																																																				kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																				AND
																																																				kartustok.idjnskartustok = 1
																																																			  ORDER BY
																																																				kartustok.kdbrg DESC
																																																			  LIMIT
																																																				1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																								 FROM
																																																								   kartustok
																																																								 WHERE
																																																								   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																								   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																								   AND (kartustok.idbagian = bbagian.idbagian)
																																																								   AND (kartustok.`idjnskartustok` = 2))
																																																								 ORDER BY
																																																								   kartustok.kdbrg DESC
																																																								 LIMIT
																																																								   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
																																																													 FROM
																																																													   kartustok
																																																													 WHERE
																																																													   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																													   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																													   AND (kartustok.idbagian = bbagian.idbagian)
																																																													   AND (kartustok.`idjnskartustok` = 18))
																																																													 ORDER BY
																																																													   kartustok.kdbrg DESC
																																																													 LIMIT
																																																													   1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
																																																																		 FROM
																																																																		   kartustok
																																																																		 WHERE
																																																																		   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																																		   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																																		   AND (kartustok.idbagian = bbagian.idbagian)
																																																																		   AND (kartustok.`idjnskartustok` = 5))
																																																																		 ORDER BY
																																																																		   kartustok.kdbrg DESC
																																																																		 LIMIT
																																																																		   1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																							 FROM
																																																																							   kartustok
																																																																							 WHERE
																																																																							   kartustok.kdbrg = barang.kdbrg
																																																																							   AND
																																																																							   kartustok.idjnskartustok = 6
																																																																							   AND
																																																																							   kartustok.idbagian = bbagian.idbagian
																																																																							   AND
																																																																							   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																							 ORDER BY
																																																																							   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																																																	FROM
																																																																																	  kartustok
																																																																																	WHERE
																																																																																	  kartustok.kdbrg = barang.kdbrg
																																																																																	  AND
																																																																																	  kartustok.idjnskartustok = 7
																																																																																	  AND
																																																																																	  kartustok.idbagian = bbagian.idbagian
																																																																																	  AND
																																																																																	  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																	ORDER BY
																																																																																	  kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
																																																																																										   FROM
																																																																																											 kartustok
																																																																																										   WHERE
																																																																																											 kartustok.kdbrg = barang.kdbrg
																																																																																											 AND
																																																																																											 kartustok.idbagian = bbagian.idbagian
																																																																																											 AND
																																																																																											 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																											 AND
																																																																																											 kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																											 AND
																																																																																											 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																																	 FROM
																																																																																																	   kartustok kstok
																																																																																																	 WHERE
																																																																																																	   kstok.kdbrg = barang.kdbrg
																																																																																																	   AND
																																																																																																	   kstok.idbagian = bbagian.idbagian
																																																																																																	   AND
																																																																																																	   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																	   AND
																																																																																																	   kstok.`idjnskartustok` = '19')
																																																																																										   ORDER BY
																																																																																											 kartustok.kdbrg DESC
																																																																																										   LIMIT
																																																																																											 1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																																															   FROM
																																																																																																 kartustok
																																																																																															   WHERE
																																																																																																 kartustok.kdbrg = barang.kdbrg
																																																																																																 AND
																																																																																																 kartustok.idjnskartustok IN (14, 15)
																																																																																																 AND
																																																																																																 kartustok.idbagian = bbagian.idbagian
																																																																																																 AND
																																																																																																 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																															   ORDER BY
																																																																																																 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
																																																																																																									  FROM
																																																																																																										kartustok
																																																																																																									  WHERE
																																																																																																										kartustok.kdbrg = barang.kdbrg
																																																																																																										AND
																																																																																																										kartustok.idbagian = bbagian.idbagian
																																																																																																										AND
																																																																																																										kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																									  ORDER BY
																																																																																																										kartustok.kdbrg DESC
																																																																																																									  LIMIT
																																																																																																										1), 0))) AS sa
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0)  * ifnull((SELECT max(kartustok.hrgjual) AS hbli
												  FROM
													kartustok
												  WHERE
													kartustok.kdbrg = barang.kdbrg
													AND
													kartustok.idbagian = bbagian.idbagian
													AND
													kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
												  ORDER BY
													kartustok.idkartustok DESC
												  LIMIT
													1), 0) AS hrgjual									 

						 , ifnull((SELECT if(kartustok.`idjnskartustok` = 3, sum(kartustok.jmlkeluar * kartustok.hrgbeli), 0) AS jml
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
									 AND
									 kartustok.`idjnskartustok` IN (3)
									 AND
									 kartustok.noref NOT IN (SELECT kstok.noref AS kd
														 FROM
														   kartustok kstok
														 WHERE
														   kstok.kdbrg = barang.kdbrg
														   AND
														   kstok.idbagian = bbagian.idbagian
														   AND
														   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
														   AND
														   kstok.`idjnskartustok` = '19')

								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) - ifnull((SELECT sum(kartustok.jmlmasuk * kartustok.hrgbeli) AS jmlmasuk
													  FROM
														kartustok
													  WHERE
														kartustok.kdbrg = barang.kdbrg
														AND
														kartustok.idjnskartustok IN (14, 15)
														AND
														kartustok.idbagian = bbagian.idbagian
														AND
														kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
													  ORDER BY
														kartustok.kdbrg DESC), 0) AS nbli

						 , ifnull((SELECT if(kartustok.`idjnskartustok` = 3, sum(kartustok.jmlkeluar * kartustok.hrgjual), 0) AS jml
								   FROM
									 kartustok
								   WHERE
									 kartustok.kdbrg = barang.kdbrg
									 AND
									 kartustok.idbagian = bbagian.idbagian
									 AND
									 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
									 AND
									 kartustok.`idjnskartustok` IN (3)
									 AND
									 kartustok.noref NOT IN (SELECT kstok.noref AS kd
														 FROM
														   kartustok kstok
														 WHERE
														   kstok.kdbrg = barang.kdbrg
														   AND
														   kstok.idbagian = bbagian.idbagian
														   AND
														   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
														   AND
														   kstok.`idjnskartustok` = '19')

								   ORDER BY
									 kartustok.kdbrg DESC
								   LIMIT
									 1), 0) - ifnull((SELECT sum(kartustok.jmlmasuk * kartustok.hrgjual) AS jmlmasuk
													  FROM
														kartustok
													  WHERE
														kartustok.kdbrg = barang.kdbrg
														AND
														kartustok.idjnskartustok IN (14, 15)
														AND
														kartustok.idbagian = bbagian.idbagian
														AND
														kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
													  ORDER BY
														kartustok.kdbrg DESC), 0) AS njual
								 ", false);
		$this->db->from('barangbagian bbagian', false);
		$this->db->join("barang", "bbagian.kdbrg = barang.kdbrg", "left", false);
		$this->db->where("barang.idstatus <> '0'");
		$this->db->where("bbagian.idbagian = '". $idbagian ."'");
		$this->db->group_by("barang.kdbrg");
		$this->db->order_by('barang.nmbrg');
		$query = $this->db->get();
		$fpl = $query->result();
		$fplnum = $query->num_rows();
		
		$query = $this->db->getwhere('bagian',array('idbagian'=>$idbagian));
		$nmbagian = $query->row_array();
		$tablename = 'barang';
		$data['eksport'] = $fpl;
		$data['table'] = $tablename;
		$data['fieldname'] = $header;//$this->db->list_fields($tablename);
		$data['filename'] = ('Lap_Inventory');
		$data['filter'] = strtoupper('Periode')." : ".$tglawal." s/d ".$tglakhir."\n Nama Bagian : ".$nmbagian['nmbagian']."";		
		$data['numrows'] = $fplnum;
		$this->load->view('exportexcellapinvemtory', $data); 	
	}
}
