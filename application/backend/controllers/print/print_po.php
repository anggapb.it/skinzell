<?php 
	class Print_po extends Controller{
		function __construct(){
			 parent::__construct();
			$this->load->library('pdf_pp');
        
		}
	
		function po_pdf($nopo){
			
		$this->db->select("*, pengguna.nmlengkap as nmlengkap");
		$this->db->from("po");
		
		$this->db->join("podet", "po.nopo = podet.nopo", "left");
		$this->db->join("supplier", "supplier.kdsupplier = po.kdsupplier","left");
	//	$this->db->join("stpo","stpo.idstpo = po.idstpo","left");
	
		$this->db->join("stsetuju", "stsetuju.idstsetuju = po.idstsetuju","left");
		$this->db->join("sypembayaran", "sypembayaran.idsypembayaran = po.idsypembayaran", "left");
		$this->db->join("jpembayaran", "jpembayaran.idjnspembayaran = po.idjnspembayaran", "left");
		$this->db->join("pengguna","pengguna.userid = po.userid", "left");
		
		$this->db->where('po.nopo', $nopo);
		$query = $this->db->get();
        $nopo = $query->row_array();
		
			// add a page
		 	/* $page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 148),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => -90,
			'PZ' => 1,
		); */
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 148),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		
		$this->pdf_pp->AddPage('L', $page_format, false, false); 
        //$this->pdf_pp->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT);
		//$this->pdf_pp->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM-15);
		
		//Set Footer
		$this->pdf_pp->SetFooterMargin(8); // margin footer 1 CM
		$this->pdf_pp->setPrintFooter(true); // enabled ? true
		$this->pdf_pp->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		$this->pdf_pp->SetFont('helvetica', '', 8);
		$kopp = "<br>
				<table border=\"0\">
					<tr align=\"left\">
						<td width=\"0.7%\"></td>
						<td width=\"99.5%\"><font size=\"13\" face=\"Helvetica\"><b>".NMKLINIK_KAPITAL."</b></font></td>
					</tr>
					<tr align=\"left\">
						<td></td>
						<td><font size=\"11\" face=\"Helvetica\">".ALAMAT_SATU."</font></td>
					</tr>
					<tr align=\"left\">
						<td></td>
						<td>".ALAMAT_DUA."</td>
					</tr>
					<tr align=\"left\">
						<td><font size=\"8\" face=\"Helvetica\"></font><hr height=\"2\"></td>
						<td>Telp. ".NOTELP."<hr height=\"2\"></td>
					</tr>
					<tr align=\"center\">
						<td></td>
						<td><h3><b><i>Surat Pembelian Barang</i></b></h3></td>
					</tr>
				</table>
		";
     	$this->pdf_pp->writeHTML($kopp,true,false,false,false);
		
		$kop2 = "<br><br>
				 <table border=\"0\">
					<tr align=\"left\">
						<td colspan=\"3\"><font size=\"10\" face=\"Helvetica\"><b>Diterima Dari :</b></font></td>
						
						<td width=\"25%\"><font size=\"8\" face=\"Helvetica\">PO No.</font></td>
						<td width=\"1%\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td><font size=\"8\" face=\"Helvetica\"><b>".$nopo['nopo']."</b></font></td>
					</tr>
					<tr>
						<td colspan=\"3\"><font size=\"8\" face=\"Helvetica\"><b>".$nopo['nmsupplier']."</b></font></td>
						
						<td width=\"25%\"><font size=\"8\" face=\"Helvetica\">Tanggal Terima</font></td>
						<td><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td><font size=\"8\" face=\"Helvetica\">".$nopo['tglpo']."</font></td>
					</tr>
					<tr>
						<td colspan=\"3\"><hr width=\"70%\" color=\"black\"></td>
						
						<td><font size=\"8\" face=\"Helvetica\">Syarat Pembayaran</font></td>
						<td><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td><font size=\"8\" face=\"Helvetica\">".$nopo['nmsypembayaran']."</font></td>
						
					</tr>
					<tr>
						<td colspan=\"3\"><font size=\"8\" face=\"Helvetica\">Telp. ".$nopo['notelp']." <br>Fax. ".$nopo['nofax']."</font></td>
						
						<td><font size=\"8\" face=\"Helvetica\">Jenis Pembayaran</font></td>
						<td><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td><font size=\"8\" face=\"Helvetica\">".$nopo['nmjnspembayaran']."</font></td>

					</tr>
					<tr>
						<td colspan=\"3\"><hr width=\"70%\" color=\"black\"></td>
						
						<td colspan=\"3\"><hr width=\"100%\" color=\"black\"></td>
					</tr>
					<tr>
						<td width=\"30\"><font size=\"8\" face=\"Helvetica\">NPWP</font></td>
						<td width=\"10\"><font size=\"8\" face=\"Helvetica\">:</font></td>
						<td><font size=\"8\" face=\"Helvetica\">".$nopo['npwp']."</font></td>
						
						<td colspan=\"3\"></td>
					</tr>
					
				 </table>
			";
		$this->pdf_pp->writeHTML($kop2,true,false,false,false);
	//	var_dump($nopo);
				$isi = '';
				$this->db->select("*");
				$this->db->from("podet");
				$this->db->join("barang","podet.kdbrg = barang.kdbrg", "left");
				$this->db->join("jsatuan", "barang.idsatuanbsr = jsatuan.idsatuan","left");
				$this->db->where('nopo',$nopo['nopo']);
			//	var_dump($nopo);
				$querys = $this->db->get();
				
			//	$ambil = $querys->row_array();
				$aaa = $querys->result();
				$total = '';
				
				$ppnn = 0;
				$s1 = 0; 
				
				foreach($aaa as $i=>$val){
				
				$ppn = ($val->ppn==10.00) ? 0.1:0;
				$s1 = (($val->hargabeli * $val->qty)-$val->diskonrp);
						
				$jumlah = ($s1 + ($s1 * $ppn));//$val->qty * $val->hargabeli;
				
				$total += $jumlah;
				$no = ($i+1);
				$isi .= "<tr>
								<td width=\"18\" font size=\"7\" face=\"Helvetica\" align=\"center\">".$no.".</td>
								<td width=\"11%\"><font size=\"8\" face=\"Helvetica\" align=\"center\">".$val->kdbrg."</font></td>
								<td width=\"23%\"><font size=\"8\" face=\"Helvetica\">".$val->nmbrg."</font></td>
								<td ><font size=\"8\" face=\"Helvetica\" align=\"center\">".$val->nmsatuan."</font></td>
								<td width=\"5%\"><font size=\"8\" face=\"Helvetica\" align=\"right\">".$val->qty."</font></td>
								<td ><font size=\"8\" face=\"Helvetica\" align=\"right\">".$val->qtybonus."</font></td>
								<td ><font size=\"8\" face=\"Helvetica\" align=\"right\">". number_format($val->hargabeli,0,',','.') ."</font></td>
								<td width=\"7%\"><font size=\"8\" face=\"Helvetica\" align=\"right\">".$val->ppn."</font></td>
								<td ><font size=\"8\" face=\"Helvetica\" align=\"right\">".$val->diskon."</font></td>
								<td ><font size=\"8\" face=\"Helvetica\" align=\"right\">". number_format($jumlah,0,',','.') ."</font></td>
						</tr>";
				}
				$detail = "<table border=\"1\" cellpadding=\"2\">
						<thead>
							<tr align=\"center\">
								<th width=\"18\" font size=\"8\" face=\"Helvetica\">No.</th>
								<th width=\"11%\" font size=\"8\" face=\"Helvetica\">Kode Barang</th>
								<th width=\"23%\" font size=\"8\" face=\"Helvetica\">Nama Barang</th>
								<th font size=\"8\" face=\"Helvetica\">Satuan</th>
								<th width=\"5%\" font size=\"8\" face=\"Helvetica\">Qty</th>
								<th font size=\"8\" face=\"Helvetica\">Qty Bonus</th>
								<th font size=\"8\" face=\"Helvetica\">Harga Beli</th>
								<th width=\"7%\" font size=\"8\" face=\"Helvetica\">PPN (%)</th>
								<th font size=\"8\" face=\"Helvetica\">Diskon (%)</th>
								<th font size=\"8\" face=\"Helvetica\">Subtotal</th>
							</tr>
						</thead>".$isi."
							<tr>
								<td width=\"72.4%\" colspan=\"3\"><p align=\"right\"><font size=\"8\" face=\"Helvetica\"> Total  </font></p></td>
								
								<td width=\"27%\" colspan=\"3\"><font size=\"8\" face=\"Helvetica\" align=\"right\">".number_format($total,0,',','.')."</font></td>
				
							</tr>
						   </table>
				";
		$this->pdf_pp->writeHTML($detail,true,false,false,false);
		$approve = " <br><br><br><br>
			<table border=\"0\" align=\"center\" >
			<tr>
				<td width=\"18%\"></td>
				<td width=\"15%\"><b>Approval</b></td>
				<td width=\"35%\"></td>
				<td width=\"15%\"><b>Purchasing</b></td>
			</tr>
			<tr>
				<td></td>
				<td height=\"30\"></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>".$nopo['approval1']."</td>
				<td></td>
				<td>".$nopo['nmlengkap']."</td>
			</tr>
			<tr >
				<td></td>
				<td><hr></td>
				<td></td>
				<td><hr></td>
			</tr>
			</table>
		";
		$this->pdf_pp->writeHTML($approve,true,false,false,false);
		$this->pdf_pp->Output('po.pdf', 'I');
		}
	}

?>