<?php

class Returdeposit_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_returdeposit(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        $key 					= $_POST["key"];
		
        $this->db->select("*");
        $this->db->from("v_returdeposit");        
        
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		} 
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($key);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($key){
      
        $this->db->select("*");
        $this->db->from("v_returdeposit");
   
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		} 
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_registrasii(){
		$key					= $_POST["key"]; 
		$tglawal				= $_POST["tglawal"]; 
		
		$q = ("SELECT *
				FROM
				  (SELECT kuitansi.nokuitansi
						, kuitansi.tglkuitansi
						, registrasi.noreg
						, pasien.norm
						, pasien.nmpasien
						, jk.kdjnskelamin
						, (
						  SELECT tglmasuk
						  FROM
							registrasidet rd
						  WHERE
							rd.noreg = registrasi.noreg
						  ORDER BY
							rd.idregdet DESC
						  LIMIT
							1
						  ) AS tglmasuk
						, (
						  SELECT nmbagian
						  FROM
							bagian, registrasidet rd
						  WHERE
							bagian.idbagian = rd.idbagian
							AND
							rd.noreg = registrasi.noreg
						  ORDER BY
							rd.idregdet DESC
						  LIMIT
							1
						  ) AS nmbagian
						, (
						  SELECT `nmkamar`
						  FROM
							kamar, registrasidet rdt
						  WHERE
							kamar.idkamar = rdt.idkamar
							AND
							rdt.noreg = registrasi.noreg
						  ORDER BY
							rdt.idregdet DESC
						  LIMIT
							1
						  ) AS `nmkamar`
						, (
						  SELECT nmbed
						  FROM
							bed, registrasidet rdt
						  WHERE
							bed.idbed = rdt.idbed
							AND
							rdt.noreg = registrasi.noreg
						  ORDER BY
							rdt.idregdet DESC
						  LIMIT
							1
						  ) AS nmbed
						, (
						  SELECT nmklstarif
						  FROM
							`klstarif`, registrasidet rdt
						  WHERE
							`klstarif`.idklstarif = rdt.idklstarif
							AND
							rdt.noreg = registrasi.noreg
						  ORDER BY
							rdt.idregdet DESC
						  LIMIT
							1
						  ) AS nmklstarif
						, dokter.nmdoktergelar
						, (
						  SELECT nona
						  FROM
							nota, registrasidet rdt
						  WHERE
							nota.idregdet = rdt.idregdet
							AND
							rdt.noreg = registrasi.noreg
						  ORDER BY
							rdt.idregdet DESC
						  LIMIT
							1
						  ) AS nona
						, `res`.`idstposisipasien` AS `idstposisipasien`
						, `pospas`.`nmstposisipasien` AS `nmstposisipasien`
						, (
						  SELECT rd.noreg
						  FROM
							nota nt, registrasidet rd
						  WHERE
							nt.idregdettransfer = registrasi.noreg
							AND
							rd.idregdet = nt.idregdet
						  ) AS transferugd
						, ifnull((
						  SELECT (sum(ntd.tarifjs * ntd.qty) + sum(ntd.tarifjm * ntd.qty) + sum(ntd.tarifjp * ntd.qty) + sum(ntd.tarifbhp * ntd.qty) + nota.uangr - nota.diskon) AS tarif
						  FROM
							nota nt, registrasidet rd, notadet ntd
						  WHERE
							nt.idregdettransfer = registrasi.noreg
							AND
							rd.idregdet = nt.idregdet
							AND
							ntd.nonota = nt.nonota
						  ), 0) AS nominalugd
						, ((sum(notadet.tarifjs * notadet.qty) + sum(notadet.tarifjm * notadet.qty) + sum(notadet.tarifjp * notadet.qty) + sum(notadet.tarifbhp * notadet.qty) + nota.uangr) - (sum(notadet.diskonjs) + sum(notadet.diskonjm) + sum(notadet.diskonjp) + sum(notadet.diskonbhp) + nota.diskon)) AS tottransaksiri
						, (sum(notadet.diskonjs) + sum(notadet.diskonjm) + sum(notadet.diskonjp) + sum(notadet.diskonbhp) + nota.diskon) AS diskon
						, (
						  SELECT if(isnull(sum(kui.total)), 0, sum(kui.total))
						  FROM
							kuitansi kui, registrasidet rd
						  WHERE
							kui.idregdet = rd.idregdet
							AND
							rd.noreg = registrasi.noreg
							AND
							kui.idjnskuitansi = 5
							AND
							kui.idstkuitansi = 1
						  ) AS totdeposit
						, (
						  SELECT nt.userinput
						  FROM
							nota nt, registrasidet rd
						  WHERE
							nt.idregdet = rd.idregdet
							AND
							rd.noreg = registrasi.noreg
						  ORDER BY
							rd.idregdet DESC
						  LIMIT
							1
						  ) AS userid
				   FROM
					 registrasi
				   LEFT JOIN registrasidet
				   ON registrasidet.noreg = registrasi.noreg
				   LEFT JOIN nota
				   ON nota.idregdet = registrasidet.idregdet
				   LEFT JOIN notadet
				   ON notadet.nonota = nota.nonota
				   LEFT JOIN kuitansi
				   ON kuitansi.nokuitansi = nota.nokuitansi
				   LEFT JOIN pasien
				   ON pasien.norm = registrasi.norm
				   LEFT JOIN jkelamin jk
				   ON jk.idjnskelamin = pasien.idjnskelamin
				   LEFT JOIN dokter
				   ON dokter.iddokter = registrasidet.iddokter
				   LEFT JOIN nota nota2
				   ON nota2.idregdettransfer = registrasi.noreg
				   LEFT JOIN `reservasi` `res`
				   ON `res`.`idregdet` = registrasidet.idregdet
				   LEFT JOIN `stposisipasien` `pospas`
				   ON `pospas`.`idstposisipasien` = `res`.`idstposisipasien`
				   WHERE
					 registrasi.idjnspelayanan = 2
					 AND
					 registrasi.noreg NOT IN (SELECT jurnal.noreff
											  FROM
												jurnal
											  WHERE
												jurnal.noreff = registrasi.noreg)
					 AND
					 kuitansi.tglkuitansi = '".$tglawal."' AND if((".$key." = 1), ".$_POST["id"]." LIKE '%".$_POST["name"]."%', kuitansi.tglkuitansi = '".$tglawal."')
				   GROUP BY
					 registrasi.noreg
					 DESC) a
				WHERE
				  tottransaksiri < totdeposit
				");
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	/* function get_registrasii(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
		$key					= $_POST["key"]; 
		$tglawal				= $_POST["tglawal"]; 
      
        $this->db->select("*");
        $this->db->from("v_regdiretdeposit_v3");
			$this->db->where("v_regdiretdeposit_v3.tglkuitansi = '". $tglawal ."'");
		//$this->db->where("v_regdiretdeposit_v2.noreg NOT IN (SELECT v_jurnal.noreg from v_jurnal where v_jurnal.noreg = v_regdiretdeposit_v2.noreg)");
		//$this->db->order_by("v_regdiretdeposit_v2.noreg DESC");
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		} 
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->now($key,$tglawal);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function now($key,$tglawal){
      
        $this->db->select("*");
        $this->db->from("v_regdiretdeposit_v3");
			$this->db->where("v_regdiretdeposit_v3.tglkuitansi = '". $tglawal ."'");
    
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    } */
	
	/* function delete_returdeposit(){     
		$where['kdjurnal'] = $_POST['kdjurnal'];
		$del = $this->rhlib->deleteRecord('jurnal',$where);
        return $del;
    } */
	
	function delete_returdeposit(){
		$passbatal =  $this->input->post('passbatal');
		#$user = 'Batal';
		$cekpass = $this->id_field('userid', 'pengguna', 'password', base64_encode($passbatal));
		 
		if ($cekpass) {			
			$this->db->trans_begin();
			
			$where['kdjurnal'] = $_POST['kdjurnal'];
			$this->rhlib->deleteRecord('jurnaldet',$where);
			
			$where['kdjurnal'] = $_POST['kdjurnal'];
			$this->rhlib->deleteRecord('jurnal',$where);
			
				if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				$return="Hapus gagal";
			}
			else
			{
				$this->db->trans_commit();
				$return="Hapus Berhasil";
			}
			echo $return;
			
		} else {
			$return = "Password Salah";
			echo $return;
		}
    }
		
	function insert_returdeposit(){
    $akun_kas = $this->db->get_where('akun', array('kdakun' => '11000'))->row_array();
    $akun_deposit = $this->db->get_where('akun', array('kdakun' => '21760'))->row_array();
    $idjtransaksi = 16;
    
    $dataArray = $this->getFieldsAndValues();

    //ubah data keterangan
    $keterangan = "Retur Deposit Noreg: ". $_POST['noreff'];
    if(!empty($_POST['keterangan'])) $keterangan .= ". Catatan:".$_POST['keterangan'];
    $dataArray['keterangan'] = $keterangan;

    $dataArray['status_posting'] = 0;
    
    $continue = true;
    $this->db->trans_start();
    
    if( ! $this->db->insert('jurnal', $dataArray) ) $continue = false;
    
    if($continue){
      //akun deposit (posisi debit)
      $ins_deposit = array(
        'kdjurnal' => $dataArray['kdjurnal'],
        'tahun' => date('Y'),
        'idakun' => $akun_deposit['idakun'],  
        'noreff' => $_POST['noreff'],
        'debit' => $_POST['nominal'],
        'kredit' => 0,
      );

      if( ! $this->db->insert('jurnaldet', $ins_deposit) ) $continue = false;
    }


    if($continue){
      //akun kas (posisi kredit)
      $ins_kas = array(
        'kdjurnal' => $dataArray['kdjurnal'],
        'tahun' => date('Y'),
        'idakun' => $akun_kas['idakun'],  
        'noreff' => '',
        'debit' => 0,
        'kredit' => $_POST['nominal'],
      );

      if( ! $this->db->insert('jurnaldet', $ins_kas) ) $continue = false;
    }

    if($continue){
      $this->db->trans_complete();
      $return = array('success' => true, 'message' => 'data berhasil disimpan');
    }else{
      $this->db->trans_rollback();
      $return = array('success' => false, 'message' => 'data gagal disimpan');  
    }
    echo json_encode($return);
  
  }

	function update_returdeposit(){
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('kdjurnal', $_POST['kdjurnal']);
		$this->db->update('jurnal', $dataArray); 
		$ret["kdjurnal"]=$_POST['kdjurnal'];
        echo json_encode($ret);
    }
			
	function getFieldsAndValues(){
			$kdjurnal   = $this->input->post("kdjurnal");
			$Nojurnal = $this->getNojurnal();
		
		$dataArray = array(
			 'kdjurnal'			=> ($kdjurnal) ? $kdjurnal: $Nojurnal,
			 'idjnsjurnal'      => 2, // jenis jurnal = jurnal khusus
             'idbagian'			=> ($_POST['idbagian'])? $this->id_bagian('nmbagian',$_POST['idbagian']) : null,
			 'idjnstransaksi'	=> 16, // jenis transaksi = retur deposit
			 'tgltransaksi'		=> $_POST['tgltransaksi'],
             'tgljurnal'		=> $_POST['tgltransaksi'],
             'keterangan'		=> $_POST['keterangan'],	
             'noreff'			=> $_POST['noreff'],	
             'userid'			=> $_POST['userid'],
             'nokasir'			=> (!empty($_POST['nokasir'])) ? $_POST['nokasir'] : null,
             'nominal'			=> $_POST['nominal'],
             'penerima'			=> $_POST['penerima'],
		);
		return $dataArray;
	}
	
	function getNojurnal(){
		//$q = "SELECT getOtoNojurnal(now()) as nm;";
        $q = "SELECT getOtoNoJurnalKhusus(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
	
	function id_bagian($where, $val){
		$query = $this->db->getwhere('bagian',array($where=>$val));
		$id = $query->row_array();
		return $id['idbagian'];
    }
	
	function getNmbagian(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }	
	
	function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where userid = 'Batal' AND ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }
}
