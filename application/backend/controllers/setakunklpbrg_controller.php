<?php
class Setakunklpbrg_Controller extends Controller {
  public function __construct()
  {
      parent::Controller();
		$this->load->library('session');
		$this->load->library('rhlib');
  }
	
	function get_akunklpbrg(){
		$key					= $_POST["key"]; 
		
		$this->db->select("*");
		$this->db->from("v_akunklpbrg");		
		$this->db->order_by('v_akunklpbrg.nmklpbarang');
		$tahun = $this->input->post("tahun");
		
		$where = isset($_POST["all"]) ? "" : "v_akunklpbrg.tahun = '".$tahun."'";
		
		$where = array();   
    $where['v_akunklpbrg.tahun']= $_POST['tahun'];          
		$this->db->where($where);
		
    if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}		
        
    $q = $this->db->get();
    $data = array();
    if ($q->num_rows() > 0) {
        $data = $q->result();
    }

    $ttl = $this->db->count_all("v_akunklpbrg");
    $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

    if($ttl>0){
        $build_array["data"]=$data;
    }

    echo json_encode($build_array);
  }
	
	
	function insert_akunbrg(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('setakunklpbarang',$dataArray);
        return $ret;
    }
	
	function getFieldsAndValues(){
		
		$dataArray = array(
	   'tahun'		=> $_POST['tahun'], 
		 'idakun'     => $_POST['idakun'],
     'idakunjual'     => $_POST['idakunjual'],
     'idklpbrg' 	=> $_POST['idklpbrg'],
    );
		
		return $dataArray;
	}
	
	
	function copy_data(){
		$where['tahun'] 	= $_POST['tahun_baru'];
		$this->rhlib->deleteRecord('setakunklpbarang',$where);
				
		$tahun_lama = $_POST['tahun_lama'];
		$tahun_baru = $_POST['tahun_baru'];
		$sql = "INSERT INTO setakunklpbarang (tahun, idakun, idakunjual, idklpbrg)
				SELECT 
					".$tahun_baru."
          , idakun
          , idakunjual
					, idklpbrg
				FROM
				  setakunklpbarang
				WHERE
				  tahun = '".$tahun_lama."'";
		$ret = $this->db->query($sql);
        return $ret;
    }
	
	function delete_data(){ 
		$where['tahun'] 	= $_POST['tahun'];
		$where['idklpbrg'] 	= $_POST['idklpbrg'];
		$del = $this->rhlib->deleteRecord('setakunklpbarang',$where);
        return $del;
    }
	
}