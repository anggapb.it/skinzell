<?php

class Brgbagian_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_brgbagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cidbagian             	= $this->input->post("cidbagian");
      
/*         $this->db->select('barangbagian.*, bagian.nmbagian, barang.nmbrg');
        $this->db->from('barangbagian');
		$this->db->join('bagian',
                'bagian.idbagian = barangbagian.idbagian');
		$this->db->join('barang',
                'barang.idbrg = barang.idbrg');
				
		$this->db->order_by('kdbrg');   */
		
			/* 		foreach($data as $row) {
            array_push($build_array["data"],array(
                'iddokter'			=> $row->iddokter,
                'kddokter'			=> $row->kddokter,
                'nmdokter'			=> $row->nmdokter,
                'idjnskelamin'		=> $row->idjnskelamin,
                'nmdoktergelar'		=> $row->nmdoktergelar,
                'nmjnskelamin'		=> $row->nmjnskelamin,
                'tptlahir'			=> $row->tptlahir,
                'tgllahir'			=> $row->tgllahir, //date('d F Y',strtotime($row->tgllahir)), //'tgllahir'=> substr($row->tgllahir,0,10), 
                'alamat'			=> $row->alamat,
                'notelp'			=> $row->notelp,
                'nohp'				=> $row->nohp, 
                'idspesialisasi'	=> $row->idspesialisasi,
                'nmspesialisasi'	=> $row->nmspesialisasi,
                'idstatus'			=> $row->idstatus, 
                'nmstatus'			=> $row->nmstatus, 
                'idstdokter'		=> $row->idstdokter, 
                'nmstdokter'		=> $row->nmstdokter, 
				'catatan'			=> $row->catatan,
            ));
        } */
		
		$this->db->select('*');
        $this->db->from('v_brgbagian');
		if($cidbagian != '')$this->db->where('idbagian', $cidbagian);
		
		$this->db->order_by('nmbrg');
        
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nmbrg', $val);
			}
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

       if($ttl>0){
            $build_array["data"]=$data;
        } 
/* 				foreach($data as $row) {
            array_push($build_array["data"],array(
                'idbagian'			=> $row->idbagian,
                'nmbagian'			=> $row->nmbagian,
                'kdbrg'				=> $row->kdbrg,
                'nmbrg'				=> $row->nmbrg,
                'stoknowbagian'		=> substr($row->stoknowbagian,0,2),
                'stokminbagian'		=> substr($row->stokminbagian,0,2),
                'stokmaxbagian'		=> substr($row->stokmaxbagian,0,2),
            ));
        }  */
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
        $cidbagian             	= $this->input->post("cidbagian");
		
		$this->db->select('*');
        $this->db->from('v_brgbagian');
		if($cidbagian != '')$this->db->where('idbagian', $cidbagian);

        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nmbrg', $val);
			}
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_brgbagian1(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $_POST["key"]; 
		$idbagian				= $_POST['idbagian'];
		
        $this->db->select('*');
        $this->db->from('v_barangbagian');
			
		$where = array();
		$where['v_barangbagian.idbagian']=$idbagian;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
		/* if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        } */
				
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
           // $this->db->limit(15,0);
        }		
        
		$this->db->where($where);
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nw($key, $idbagian);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nw($key, $idbagian){
		
		$this->db->select('*');
        $this->db->from('v_barangbagian');
		
		$where = array();
		$where['v_barangbagian.idbagian']=$idbagian;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_brgbagian(){     
		$where['kdbrg'] = $_POST['kdbrg'];
		$where['idbagian'] = $_POST['idbagian'];
		$del = $this->rhlib->deleteRecord('barangbagian',$where);
        return $del;
    }
		
	function insert_win_barang(){
		$dataArray = $this->getFieldsAndValuesBarang();
		$ret = $this->rhlib->insertRecord('barangbagian',$dataArray);
        return $ret;
    }
	
	function update_stokmin(){
		//UPDATE
        $this->db->where('idbagian', $_POST['idbagian']);
		$this->db->where('kdbrg', $_POST['kdbrg']);
		$this->db->set('stokminbagian', $_POST['stokminbagian']);
		$this->db->update('barangbagian'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function update_stokmax(){
		//UPDATE
        $this->db->where('idbagian', $_POST['idbagian']);
		$this->db->where('kdbrg', $_POST['kdbrg']);
		$this->db->set('stokmaxbagian', $_POST['stokmaxbagian']);
		$this->db->update('barangbagian'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function update_brgbagian(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('idbagian', $_POST['idbagian']);
		$this->db->update('barangbagian', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
		$dataArray = array(
             'idbagian'			=> $_POST['idbagian'],
			 'kdbrg'			=> $_POST['kdbrg'],
			 'stoknowbagian'	=> $_POST['stoknowbagian'],
             'stokminbagian'	=> $_POST['stokminbagian'],
             'stokmaxbagian'	=> $_POST['stokmaxbagian'],
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesBarang(){
		
		$dataArray = array(
		     'idbagian'			=> $_POST['idbagian'],
		     'kdbrg'			=> $_POST['kdbrg'],
		     'stoknowbagian'	=> ($_POST['stoknowbagian']) ? $_POST['stoknowbagian']:0,
			 'stokminbagian'	=> ($_POST['stokminbagian']) ? $_POST['stokminbagian']:0, 
			 'stokmaxbagian'	=> ($_POST['stokmaxbagian']) ? $_POST['stokmaxbagian']:0,
        );
		
		return $dataArray;
	}
	
	function get_brgmedisdibrgbagian(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $this->input->post("key");
		$idbagian 				= $_POST["idbagian"];
		
        $this->db->select('*');
		$this->db->from('v_barangbagian');
		$this->db->order_by('kdbrg');
		$this->db->group_by('kdbrg');
        		
		if($idbagian){		
			$this->db->where("v_barangbagian.idbagian = '11' AND v_barangbagian.kdbrg NOT IN (SELECT kdbrg from barangbagian where idbagian='".$idbagian."')");
        }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nurow($key, $idbagian);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nurow($key, $idbagian){
      
        $this->db->select('*');
		$this->db->from('v_barangbagian');
		
		if($idbagian){		
			$this->db->where("v_barangbagian.idbagian = '11' AND v_barangbagian.kdbrg NOT IN (SELECT kdbrg from barangbagian where idbagian='".$idbagian."')");
        }
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
}
