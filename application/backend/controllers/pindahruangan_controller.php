<?php 
class Pindahruangan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
function get_registrasi(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$noreg					= $this->input->post("noreg");
      
        $this->db->select("*");
        $this->db->from("v_pindahruangan");
		if($noreg){
        $this->db->where('noreg',$noreg);
        }else{
		$this->db->where('noreg',null);
        
		}
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
function numrow($fields, $query){
      
        $this->db->select("*");
        $this->db->from("v_pindahruangan");
    
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function simpan(){;
		/* $query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg'], 'idklstarif'=>4));
		$reg = $query->row_array();
		if(!$reg){
			$this->db->select("*", false);
			$this->db->from("v_registrasi");
			$this->db->where("noreg", $_POST['noreg']);
			//$this->db->where("noreg", 'RI14000004');
			$this->db->orderby("idklstarif");
			$this->db->group_by("idregdet");
			$query = $this->db->get();
			$reg2 = $query->row_array();
			$reg3 = $query->result();
			
			if($_POST['idklstarif']<$reg2['idklstarif'] || $_POST['idklstarif']==4){
				foreach($reg3 AS $val){
					$this->db->select("*", false);
					$this->db->from("nota");
					$this->db->where("idregdet", $val->idregdet);
					$query = $this->db->get();
					$nota = $query->result();
					foreach($nota AS $val1){
						$this->db->select("*", false);
						$this->db->from("notadet");
						$this->db->where("nonota", $val1->nonota);
						$query = $this->db->get();
						$notadet = $query->result();
						foreach($notadet AS $val2){
							if(!$val2->koder){
								$z = $this->updateTarifNota($val2);
							}
						}
					}
				}
			}
		} */
		
		$dataArraya = $this->FieldUpdate();
		$this->db->where('noreg', $_POST['noreg']);
		$this->db->update('registrasidet', $dataArraya); 
		
		$dataArrayR = $this->getUpdateFieldReservasi();
		$this->db->where('norm', $_POST['norm']);
		$this->db->update('reservasi', $dataArrayR); 
		
		$dataArrayas = $this->FieldUpdateBed();
		$this->db->where('idbed', $_POST['stbed']);
		$this->db->update('bed', $dataArrayas); 
		
		$dataArray = $this->getFieldsAndValues();
		$ret['insertnew'] = $this->rhlib->insertRecord('registrasidet',$dataArray);
		
		$dataReservasi = $this->getValueAndFieldReservasi();
		$ret['insertnew'] = $this->rhlib->insertRecord('reservasi',$dataReservasi);
	//	var_dump($aa);
		$dataArrayad = $this->FieldUpdateBeda();
		$this->db->where('idbed', $_POST['idbed']);
		$this->db->update('bed', $dataArrayad);
		
		  echo json_encode($ret);
	}
	
	function updateTarifNota($notadet){
		$strbr = substr($notadet->kditem, 0, 1);
		if($strbr == "B"){
			/* $query = $this->db->getwhere('v_tarifall',array('kditem'=>$notadet->kditem, 'idbagian'=>$_POST['idbagian']));
			$tarif = $query->row_array();
			
			$q = $this->db->getwhere('barang',array('kdbrg'=>$notadet->kditem));
			$barang = $q->row_array();
		
			if(isset($tarif)){
				$dataArray = array(
					'tarifbhp'=> $tarif['tarifbhp'],
					'hrgjual'=> $barang['hrgjual'],
					'hrgbeli'=> $barang['hrgbeli']
				);
				$this->db->where('nonota', $notadet->nonota);
				$this->db->where('kditem', $notadet->kditem);
				$this->db->update('notadet', $dataArray);
			} */
			
		} elseif($strbr == "T") {
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$notadet->kditem, 'klstarif'=>$_POST['idklstarif']));
			$tarif = $query->row_array();
			if(isset($tarif['kditem'])){
				$dataArray = array(
					'tarifjs'=> $tarif['tarifjs'],
					'tarifjm'=> $tarif['tarifjm'],
					'tarifjp'=> $tarif['tarifjp'],
					'tarifbhp'=> $tarif['tarifbhp']
				);
				$this->db->where('nonota', $notadet->nonota);
				$this->db->where('kditem', $notadet->kditem);
				$this->db->update('notadet', $dataArray);
			}
		}
		
		return true;
	}
	
	function updateBed(){
		$dataArrayas = $this->FieldUpdateBed();
		$this->db->where('idbed', $_POST['stbed']);
		$this->db->update('bed', $dataArrayas);
	
		$dataArray = $this->getFieldUpdate();
		$this->db->where('noreg', $_POST['noreg']);
		$this->db->update('registrasidet', $dataArray);
		$dataArrayad = $this->FieldUpdateBeda();
		$this->db->where('idbed', $_POST['idbed']);
		$this->db->update('bed', $dataArrayad); 
	}
	
	function getFieldsAndValues(){
		if($_POST['idbagiankirim'] == "") $_POST['idbagiankirim'] = null;
		if($_POST['iddokterkirim'] == "") $_POST['iddokterkirim'] = null;
		$dataArray = array(
			'noregdet'=> 1,
             'noreg'=> $_POST['noreg'],
             'tglreg'=> date_format(date_create($_POST['tglreg']), 'Y-m-d'),
             'jamreg'=> date_format(date_create($_POST['jamreg']), 'H:i:s'),
             'idshift'=> $_POST['idshift'],
             'idbagiankirim'=> $_POST['idbagiankirim'],
             'iddokterkirim'=> $_POST['iddokterkirim'],
        //     'nmdokterkirim'=> $_POST['nmdokterkirim'],
             'idbagian'=> $_POST['idbagian'],
             'iddokter'=> $_POST['iddokter'],
             'idcaradatang'=> $_POST['idcaradatang'],
             'tglmasuk'=> date('Y-m-d'),
             'jammasuk'=> date('H:i:s'),
             //'tglkeluar'=> $_POST['tf_noreg'],
             //'jamkeluar'=> $_POST['tf_noreg'],
             //'catatankeluar'=> $_POST['tf_noreg'],
             'umurtahun'=> $_POST['umurtahun'],
             'umurbulan'=> $_POST['umurbulan'],
             'umurhari'=> $_POST['umurhari'],
             'idklsrawat'=> $_POST['idklsrawat'],
             'idklstarif'=> $_POST['idklstarif'],
             'idkamar'=> $_POST['idkamar'],
             'idbed'=> $_POST['idbed'],
			// 'idcarakeluar' => 5,
             //'idstkeluar'=> $_POST['tf_noreg'],
             //'nooruangan'=> $_POST['tf_noreg'],
             'idstregistrasi'=> 1,
             'userinput'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d'),
             //'userbatal'=> $noregdetistrasidet,
             //'tglbatal'=> $_POST['tf_notelpkerabat']
		);
		return $dataArray;
	}
	
	function FieldUpdate(){
		$dataArray = array(
			 'tglkeluar'=> date('Y-m-d'),
             'jamkeluar'=> date('H:i:s'),
			 'idcarakeluar'=> 6,
          
		);
		return $dataArray;
	}
	
	function FieldUpdateBed(){
		$dataArray = array(
			'idstbed' => 1,
		);
		return $dataArray;
	}
	
	function FieldUpdateBeda(){
		$dataArray = array(
			'idstbed' => 2,
		);
		return $dataArray;
	}
	
	function getFieldUpdate(){
		$dataArray = array(
			'idbed' => $_POST['idbed'],
			 'tglkeluar'=> null,
             'jamkeluar'=> null,
		);
		return $dataArray;
	}

function get_registrasipindah(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("v_regpindah");
		
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow2($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
function numrow2($fields, $query){
      
        $this->db->select("*");
        $this->db->from("v_regpindah");
    
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }

function getValueAndFieldReservasi(){
	$norm = $this->input->post("norm");
	$noreg = $this->input->post("noreg");
	
	$this->db->select("*");
	$this->db->from("registrasidet");
	$this->db->where("noreg",$noreg);
	$this->db->order_by("idregdet desc");
	$query = $this->db->get();
	$regdet = $query->row_array();

	$this->db->select("*");
	$this->db->from("reservasi");
	$this->db->where("norm",$norm);
	$this->db->order_by("idregdet desc");
	$q = $this->db->get();
	$reserv = $q->row_array();
	$dataArray = array(
			 'tglreservasi'=> date('Y-m-d'),
             'jamreservasi'=> date('H:i:s'),
             'idshift'=> $reserv['idshift'],
             'norm'=> $reserv['norm'],
             'nmpasien'=> $reserv['nmpasien'],
             //'email'=> $_POST['alamat'],
             'notelp'=> $reserv['notelp'],
             'nohp'=> $reserv['nohp'],
             'iddokter'=> $reserv['iddokter'],
             'idbagian'=> $_POST['idbagian'],
             'idstreservasi'=> 1,
             'idregdet'=> $regdet['idregdet'],
            // 'idregdet'=> $reserv['idregdet'],
            // 'noantrian'=> $_POST['tf_noantrian'],
             'userinput'=> $this->session->userdata['username'],
             'tglinput'=> date('Y-m-d'),
             'idstposisipasien'=> 5
	);
	return $dataArray;	
}
function getUpdateFieldReservasi(){
	$norm = $this->input->post("norm");

	$this->db->select("*");
	$this->db->from("reservasi");
	$this->db->where("norm",$norm);
	$this->db->order_by("idregdet desc");
	$q = $this->db->get();
	$reserv = $q->row_array();
	$dataArray = array(
	         'idstposisipasien'=> 6
	);
	return $dataArray;	
	}
}
?>