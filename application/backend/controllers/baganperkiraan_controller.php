<?php

class Baganperkiraan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_baganperkiraan(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_baganperkiraan");
				
		$this->db->where('v_baganperkiraan.kdakun !=','');
		$this->db->order_by('v_baganperkiraan.kdakun');
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
					
		$this->db->select("*");
		$this->db->from("v_baganperkiraan");

        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_baganperkiraan(){     
		$where['idakun'] = $_POST['idakun'];
		$del = $this->rhlib->deleteRecord('akun',$where);
        return $del;
    }
		
	function insert_baganperkiraan(){
		$dataArray = $this->getFieldsAndValues();
        //check kode akun
        $check = $this->db->get_where('akun', array('kdakun' => $dataArray['kdakun']))->num_rows();
        if($check > 0){
            $ret["success"]=false;
            $ret["msg"]= 'Simpan data Gagal';
            echo json_encode($ret);
            die;
        }

		$ret = $this->rhlib->insertRecord('akun',$dataArray);
        echo json_encode($ret);
        die;
    }

	function update_baganperkiraan(){ 				
		$dataArray = $this->getFieldsAndValues();
		
        $check = $this->db->query("SELECT * FROM akun where kdakun = '". $dataArray['kdakun'] ."' AND idakun != '". $_POST['idakun'] ."'")->num_rows();
        if($check > 0){
            $ret["success"]=false;
            $ret["msg"]= 'Simpan data Gagal';
            echo json_encode($ret);
            die;
        }

		//UPDATE
		$this->db->where('idakun', $_POST['idakun']);
		$this->db->update('akun', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
	
	   $idakunparent =  $_POST['idakunparent']; 

	   if($idakunparent=="") $idakunparent=0;
	
		$dataArray = array(		
			 'idakun'			=> $_POST['idakun'],	 
			 'kdakun'			=> $_POST['kdakun'],		 
             'nmakun'			=> $_POST['nmakun'],
             'idklpakun'		=> $_POST['idklpakun'],
			 'idjnsakun'		=> $_POST['idjnsakun'],
			 'idstatus'			=> $_POST['idstatus'],
			 'tglinput'			=> date('Y-m-d H:i:s'),
			 'userid'			=> $this->session->userdata['user_id'],
			 'keterangan'		=> $_POST['keterangan'],
			 'idakunparent'		=> $this->kd_akun('nmakun',$idakunparent),
        );
		/* var_dump($dataArray);
		exit;  */
		return $dataArray;
	}
	
	function get_parent_baganperkiraan(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_baganperkiraan");
				
		$this->db->where('v_baganperkiraan.kdakun !=','');
		$this->db->order_by('v_baganperkiraan.kdakun');
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(10,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numroww($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numroww($fields, $query){
					
		$this->db->select("*");
		$this->db->from("v_baganperkiraan");

        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function kd_akun($where, $val){
		$query = $this->db->getwhere('akun',array($where=>$val));
		$id = $query->row_array();
		return  $id['idakun'];
    }
	
	function getNmakun(){
		$query = $this->db->getwhere('akun',array('idakun'=>$_POST['idakunparent']));
		$nm = $query->row_array();
		echo json_encode($nm['nmakun']);
    }
	
	function get_dataakun(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
		$this->db->from("v_baganperkiraanditahun");
		$this->db->order_by('v_baganperkiraanditahun.kdakun');
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nw($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nw($fields, $query){
					
		$this->db->select("*");
		$this->db->from("v_baganperkiraanditahun");

        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }

    function get_akun_perkelompok()
    {
        $idklpakun = $this->input->post("idklpakun");
        if(empty($idklpakun)) $idklpakun = 1;

        $this->db->select("*");
        $this->db->from("akun");
        $this->db->where('idklpakun', $idklpakun);
        $this->db->order_by('kdakun');
        $q = $this->db->get();
        $ttl = $q->num_rows();
        $data = $q->result();
        
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>$data);
        
        echo json_encode($build_array);
    }
	
}
