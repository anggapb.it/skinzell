<?php

class Tutupkasir_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_tutupkasir(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$userid					= $_POST['userid'];
      
        $this->db->select('*');
		$this->db->from('v_kasir');    
        
		$where = array();
		$where['v_kasir.userid'] = $userid;
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select('*');
		$this->db->from('v_kasir');
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function insert_update_tutupkasir(){
        $this->db->trans_begin();
		$query = $this->db->getwhere('kasir',array('nokasir'=>$_POST['tf_nokasir']));
		if($query->num_rows() > 0) $pas = $this->update_tutupkasir();
		else $pas = $this->insert_tutupkasir();
		
        if($pas){
            $this->db->trans_commit();
            $ret["success"]=true;
        }else{
            $this->db->trans_rollback();
            $ret["success"]=false;
            //$ret["message"]='Simpan Data  Gagal';
        }
        echo json_encode($ret);
    }
		
	function insert_tutupkasir(){
		$dataArray = $this->getFieldsAndValues();
		$this->rhlib->insertRecord('kasir',$dataArray);
        return $dataArray;
    }

	function update_tutupkasir(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('nokasir', $_POST['nokasir']);
		$this->db->update('kasir', $dataArray);
        return $dataArray;
    }
	
	function getFieldsAndValues(){
			
		$idshifttutup		= (isset($_POST['idshifttutup']))? $_POST['idshifttutup'] : null;
		
		$dataArray = array(
			 'nokasir'		=> $_POST['nokasir'],
			 'tgltutup'		=> $_POST['tgltutup'],
             'jamtutup'		=> $_POST['jamtutup'],
             'idshifttutup'	=> $idshifttutup,
			 'saldoakhir'	=> $_POST['saldoakhir'],
			 'selisih'		=> $_POST['selisih'],
			 'catatantutup'	=> $_POST['catatantutup'],		
			 'idstkasir'	=> 2
		);
		return $dataArray;
	}
	
	function get_vlappenerimaankas(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$nokasir				= $_POST['nokasir'];
		$key					= $_POST["key"]; 
      
        $this->db->select("*");
        $this->db->from("v_lappenerimaankas");
		$this->db->order_by('v_lappenerimaankas.nokuitansi');
		
        $where = array();
		$where['nokasir']=$nokasir;
		
       /*  if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        } */
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
		$this->db->where($where);
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nw($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nw($fields, $query){
      
        $this->db->select('*');
		$this->db->from('v_lappenerimaankas');

        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_carabayarbukakasir(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select('carabayar.*');
        $this->db->from('carabayar');
		
		$id = array('1', '2', '3', '4');
		$this->db->where_in('idcarabayar', $id);
                
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_carabayartunai(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                 	= $this->input->post("query");
		$nokasir 				= $_POST['nokasir'];
				
		$this->db->select('kuitansi.nokuitansi, kuitansi.nokasir, kuitansidet.idcarabayar, kuitansidet.jumlah, sum(jumlah) AS total_tunai');
        $this->db->from('kuitansi');
		$this->db->join('kuitansidet',
				'kuitansidet.nokuitansi = kuitansi.nokuitansi', 'left');
		$this->db->join('carabayar',
				'carabayar.idcarabayar = kuitansidet.idcarabayar', 'left');
		
		$this->db->order_by('nokuitansi');
		
		$where = array();
		$where['kuitansidet.idcarabayar']=1;
		$where['nokasir']=$nokasir;
        
        $this->db->where($where);
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_carabayardebit(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                 	= $this->input->post("query");
		$nokasir 				= $_POST['nokasir'];
				
		$this->db->select('kuitansi.nokuitansi, kuitansi.nokasir, kuitansidet.idcarabayar, kuitansidet.jumlah, sum(jumlah) AS total_debit');
        $this->db->from('kuitansi');
		$this->db->join('kuitansidet',
				'kuitansidet.nokuitansi = kuitansi.nokuitansi', 'left');
		$this->db->join('carabayar',
				'carabayar.idcarabayar = kuitansidet.idcarabayar', 'left');
		
		$this->db->order_by('nokuitansi');
		
		$where = array();
		$where['kuitansidet.idcarabayar']=2;
		$where['nokasir']=$nokasir;
        
        $this->db->where($where);
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
		
	function get_carabayarkredit(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                 	= $this->input->post("query");
		$nokasir 				= $_POST['nokasir'];
				
		$this->db->select('kuitansi.nokuitansi, kuitansi.nokasir, kuitansidet.idcarabayar, kuitansidet.jumlah, sum(jumlah) AS total_kredit');
        $this->db->from('kuitansi');
		$this->db->join('kuitansidet',
				'kuitansidet.nokuitansi = kuitansi.nokuitansi', 'left');
		$this->db->join('carabayar',
				'carabayar.idcarabayar = kuitansidet.idcarabayar', 'left');
		
		$this->db->order_by('nokuitansi');
		
		$where = array();
		$where['kuitansidet.idcarabayar']=3;
		$where['nokasir']=$nokasir;
        
        $this->db->where($where);
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_carabayarvocher(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                 	= $this->input->post("query");
		$nokasir 				= $_POST['nokasir'];
				
		$this->db->select('kuitansi.nokuitansi, kuitansi.nokasir, kuitansidet.idcarabayar, kuitansidet.jumlah, sum(jumlah) AS total_vocher');
        $this->db->from('kuitansi');
		$this->db->join('kuitansidet',
				'kuitansidet.nokuitansi = kuitansi.nokuitansi', 'left');
		$this->db->join('carabayar',
				'carabayar.idcarabayar = kuitansidet.idcarabayar', 'left');
		
		$this->db->order_by('nokuitansi');
		
		$where = array();
		$where['kuitansidet.idcarabayar']=4;
		$where['nokasir']=$nokasir;
        
        $this->db->where($where);
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}