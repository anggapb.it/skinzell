<?php 
class Penerimaanlain_Controller extends Controller{
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_penerimaanlain(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
        $userid            		= $this->input->post("userid");
      
        $this->db->select("*");
        $this->db->from("v_penerimaanlain");
		$this->db->order_by('v_penerimaanlain.noterimalain DESC');
        
        if($userid){		
			$this->db->where("v_penerimaanlain.idbagian IN (SELECT idbagian from penggunabagian where userid='".$userid."')");
        }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->numrow($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($key, $userid){
    
        $this->db->select("*");
        $this->db->from("v_penerimaanlain"); 
		
        if($userid){		
			$this->db->where("v_penerimaanlain.idbagian IN (SELECT idbagian from penggunabagian where userid = '".$userid."')");
        }
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        $q = $this->db->get();
        return $q->num_rows();
    }
	
	
	function get_pengbagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
		$userid 				= $this->input->post("userid");
      
        $this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrww($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrww($key, $userid){
      
		$this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }

	function get_terimalaindet(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$noterimalain				= $this->input->post("noterimalain");
		$idbagian				= $this->input->post("idbagian");
		
        /* $this->db->select("*");
        $this->db->from("v_penerimaanlaindet");
		
		//if($noterimalain != null)$this->db->where("v_penerimaanlaindet.noterimalain", $noterimalain);
        $where = array();
        $where['v_penerimaanlaindet.noterimalain']=$noterimalain;
		$this->db->where($where); */
		
        /* if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
				
		
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        } */
        
        $q = $this->db->query("SELECT * FROM v_penerimaanlaindet WHERE noterimalain='".$noterimalain."' and idbagian='".$idbagian."'");
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all("v_penerimaanlaindet");
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	
	function insorupd_pbrg(){
        $this->db->trans_begin();
		$noterimalain = $this->input->post("noterimalain");
		$query 	= $this->db->getwhere('penerimaanlain',array('noterimalain'=>$noterimalain));
		$noterimalain = $query->row_array();
				
		$penerimaanlain = $this->insert_tblpakaibrg($noterimalain);
		$noterimalain = $penerimaanlain['noterimalain'];
				
		if($noterimalain)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["noterimalain"]=$noterimalain;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_tblpakaibrg($noterimalain){
		/* if($noterimalain){
			$dataArray = $this->getFieldsAndValues($noterimalain);
			$penerimaanlain = $this->db->insert('penerimaanlain',$dataArray);
			
		} else {
			$dataArray = $this->update_tblpakaibrg($noterimalain);
		} */
				
		$dataArray = $this->getFieldsAndValues($noterimalain);
		$penerimaanlain = $this->db->insert('penerimaanlain',$dataArray);
		
		$query = $this->db->getwhere('penerimaanlaindet',array('noterimalain'=>$dataArray['noterimalain']));
		$pakaibrgdet = $query->row_array();
		if($query->num_rows() == 0){
		
			$pakaibrgdet = $this->insert_tblpakaibrgdet($dataArray);
		} else {
			$pakaibrgdet = $this->update_tblpakaibrgdet($dataArray);
		}
		
		if($pakaibrgdet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function insert_tblpakaibrgdet($penerimaanlain){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrpbrg']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($penerimaanlain, $vale[0], $vale[1], $vale[2], $vale[3], $vale[4], $vale[5]);
			$z =$this->db->insert('penerimaanlaindet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function update_tblpakaibrg($noterimalain){
		$dataArray = $this->getFieldsAndValues($noterimalain);
		
		//UPDATE
		$this->db->where('noterimalain', $dataArray['noterimalain']);
		$penerimaanlain = $this->db->update('penerimaanlain', $dataArray);
		
		return $dataArray;
    }
	
	function update_tblpakaibrgdet($penerimaanlain){
		$where['noterimalain'] = $penerimaanlain['noterimalain'];
		$this->db->delete('penerimaanlaindet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrpbrg']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($penerimaanlain, $vale[0], $vale[1], $vale[2], $vale[3], $vale[4], $vale[5]);
			
			$z =$this->db->insert('penerimaanlaindet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function getFieldsAndValues(){
			$nopb   	= $this->input->post("noterimalain");
			$noterimalain = $this->getNoPbrg();
		
		$dataArray = array(
			 'noterimalain'		=> ($nopb) ? $nopb: $noterimalain,
			 'tglterimalain'	=> $_POST['tglpakaibrg'],
             'jamterimalain'	=> $_POST['jampakaibrg'],
             'idbagian'			=> $_POST['idbagian'],
			 'dari'			    => $_POST['dari'],
             'idsttransaksi'	=> $_POST['idsttransaksi'],
             'idstsetuju'		=> $_POST['idstsetuju'],
			 'userid'			=> $_POST['userid'],	
			 'keterangan'		=> $_POST['keterangan']			
		);
		/* var_dump($dataArray);
		exit; */
		return $dataArray;
	}
	
	function getNoPbrg(){
		$q = "SELECT getOtoNoterimalain(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}

	function getFieldsAndValuesDet($penerimaanlain,$val1,$val2,$val3,$val4,$val5,$val6){
		$dataArray = array(
			'noterimalain' => $penerimaanlain['noterimalain'],
			'kdbrg'		 => $val1,
			'qty' 		 => $val2,
			'hrgbeli' 	 => $val3,
			'margin'	 => $val4,
			'hrgjual'	 => $val5,
			'catatan'    => $val6,			
		);
		return $dataArray;
	}
	
	function update_qty_tambah(){
		//$noterimalain = $this->getNoPbrg();
		$date = date("Y-m-d", strtotime($_POST['tglpakaibrg']));
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_update_tmbh_qty_brgbagian_terimalain(?,'".$date."',?,?,?,?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3],
							 $vale[4],
							 $vale[5],
							 $vale[6],
							 $vale[7]
							));
		}
        return;
    }
	
	function update_qty_batal(){
		//$noterimalain = $this->getNoPbrg();
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_update_tmbh_qty_brgbagian (?,?,?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3],
							 $vale[4],
							 $vale[5]
							));
		}
        return;
    }
	
	function getNmbagian(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }
	
	
}

?>