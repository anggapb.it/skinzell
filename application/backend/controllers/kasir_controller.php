<?php

class Kasir_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_kasir(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("kasir");
        
        
         if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow(){
    
        $this->db->select("*");
        $this->db->from("kasir"); 
        
        $q = $this->db->get();
        return $q->num_rows();
    }
	
	function LoketKasir(){
    
        $this->db->select("*");
        $this->db->from("kasir");
		$this->db->join("bagian","bagian.idbagian = kasir.idbagian", "left");
        $this->db->where("userid", $this->session->userdata['user_id']);
        $this->db->where("idstkasir", 1);
        $this->db->order_by("tglbuka DESC");
        $q = $this->db->get();
		$loket = $q->row_array();
		
		if($loket['idstkasir'] == 1 ) $ret = $loket;
		else $ret['nmbagian'] = null;
		echo json_encode($ret);
    }
}
