 <?php 
class Laporan_persediaan extends Controller{
	public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_rekapstok(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$ruangan 				= $this->input->post("ruangan");
		
		$this->db->select("*");
		$this->db->from("v_rekapstok");
		if($tglawal){
		$this->db->where('tglkartustok BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($ruangan){
			$this->db->where("idbagian",$ruangan);
		}else{
			$this->db->where("idbagian",null);
		}
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_rekapstok');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_kartupersediaan(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		
		$this->db->select("
			barang.kdbrg AS kdbrg,
			barang.nmbrg AS nmbrg,
			kartustok.noref AS noref,
			kartustok.tglkartustok AS tglkartustok,
			kartustok.jamkartustok AS jamkartustok,
			if(kartustok.idjnskartustok=9, kartustok.saldoawal, 0) AS saldoawal
			 , if(kartustok.idjnskartustok = 1, (kartustok.jmlmasuk - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													 FROM
																													   podet
																													 WHERE
																													   podet.nopo = `retursupplierdet`.nopo
																													   AND
																													   podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																	   FROM
																		 `retursupplierdet`
																	   WHERE
																		 ((`retursupplierdet`.`nopo` = kartustok.noref)
																		 AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		 AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS jmlmasuk
			 , if(kartustok.idjnskartustok = 2, (kartustok.jmlkeluar - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													  FROM
																														podet
																													  WHERE
																														podet.nopo = `retursupplierdet`.nopo
																														AND
																														podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																		FROM
																		  `retursupplierdet`
																		WHERE
																		  ((`retursupplierdet`.noretursupplier = kartustok.noref)
																		  AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		  AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS rbm
			 , if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0) AS jmlkeluar,
			if(kartustok.idjnskartustok IN (14, 15), kartustok.jmlmasuk, 0) AS rbk,
			kartustok.saldoakhir AS saldoakhir,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgbeli, 0) AS hrgbeli,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgjual, 0) AS hrgjual
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgbeli * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgbeli * kartustok.jmlmasuk), 0)) AS nbeli
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgjual * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgjual * kartustok.jmlmasuk), 0)) AS njual
		", false);
		
		$this->db->from("kartustok");
		$this->db->join('barang',
				'kartustok.kdbrg = barang.kdbrg', 'left');
		if($tglawal){
			$this->db->where('kartustok.tglkartustok BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$this->db->where("kartustok.idbagian",11);
		$this->db->where_in('kartustok.idjnskartustok',array(1, 3, 2, 9, 14, 15));
		$this->db->where("kartustok.noref NOT IN (SELECT kstok.noref AS kd
												  FROM
													kartustok kstok
												  WHERE
													kstok.kdbrg = kartustok.kdbrg
													AND
													kstok.idbagian = kartustok.idbagian
													AND
													kstok.tglkartustok BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
													AND
													kstok.`idjnskartustok` = '19')"
						);
		$this->db->orderby("kartustok.kdbrg, kartustok.tglkartustok, kartustok.jamkartustok");
		
		if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->numrow_kartupersediaan();
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function numrow_kartupersediaan(){
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		
		$this->db->select("
			barang.kdbrg AS kdbrg,
			barang.nmbrg AS nmbrg,
			kartustok.noref AS noref,
			kartustok.tglkartustok AS tglkartustok,
			kartustok.jamkartustok AS jamkartustok,
			if(kartustok.idjnskartustok=9, kartustok.saldoawal, 0) AS saldoawal
			 , if(kartustok.idjnskartustok = 1, (kartustok.jmlmasuk - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													 FROM
																													   podet
																													 WHERE
																													   podet.nopo = `retursupplierdet`.nopo
																													   AND
																													   podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																	   FROM
																		 `retursupplierdet`
																	   WHERE
																		 ((`retursupplierdet`.`nopo` = kartustok.noref)
																		 AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		 AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS jmlmasuk
			 , if(kartustok.idjnskartustok = 2, (kartustok.jmlkeluar - (SELECT ifnull(sum(`retursupplierdet`.`qty` * (SELECT rasio
																													  FROM
																														podet
																													  WHERE
																														podet.nopo = `retursupplierdet`.nopo
																														AND
																														podet.kdbrg = `retursupplierdet`.kdbrg)), 0) AS `jumlah`
																		FROM
																		  `retursupplierdet`
																		WHERE
																		  ((`retursupplierdet`.noretursupplier = kartustok.noref)
																		  AND (`retursupplierdet`.`kdbrg` = kartustok.`kdbrg`)
																		  AND (`retursupplierdet`.`idstbayar` <> 2)))), 0) AS rbm
			 , if(kartustok.idjnskartustok = 3, kartustok.jmlkeluar, 0) AS jmlkeluar,
			if(kartustok.idjnskartustok IN (14, 15), kartustok.jmlmasuk, 0) AS rbk,
			kartustok.saldoakhir AS saldoakhir,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgbeli, 0) AS hrgbeli,
			if(kartustok.idjnskartustok IN (3, 14, 15), kartustok.hrgjual, 0) AS hrgjual
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgbeli * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgbeli * kartustok.jmlmasuk), 0)) AS nbeli
			 , if(kartustok.idjnskartustok = 3, (kartustok.hrgjual * kartustok.jmlkeluar), if(kartustok.idjnskartustok IN (14, 15), (kartustok.hrgjual * kartustok.jmlmasuk), 0)) AS njual
		", false);
		
		$this->db->from("kartustok");
		$this->db->join('barang',
				'kartustok.kdbrg = barang.kdbrg', 'left');
		if($tglawal){
			$this->db->where('kartustok.tglkartustok BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		$this->db->where("kartustok.idbagian",11);
		$this->db->where_in('kartustok.idjnskartustok',array(1, 3, 2, 9, 14, 15));
		$this->db->orderby("kartustok.kdbrg, kartustok.tglkartustok, kartustok.jamkartustok");
		
        $q = $this->db->get();
        return $q->num_rows();
	}
	
}