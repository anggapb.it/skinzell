<?php

class Tahun_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_tahun(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("tahun");
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(20,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        /* if($ttl>0){
            $build_array["data"]=$data;
        } */
		
		foreach($data as $row) {
            array_push($build_array["data"],array(
				'tahun'				=>$row->tahun,
                'tglawal'			=>date("Y-m-d",strtotime($row->tglawal)),
                'tglakhir'			=>date("Y-m-d",strtotime($row->tglakhir)),
            ));
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select("*");
        $this->db->from("tahun");
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_tahun(){     
		$where['tahun'] = $_POST['tahun'];
		$del = $this->rhlib->deleteRecord('tahun',$where);
        return $del;
    }
		
	function insert_tahun(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('tahun',$dataArray);
        return $ret;
    }
	
	function update_tahun(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('tahun', $_POST['tahun']);
		$this->db->update('tahun', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
		$dataArray = array(
             'tahun'	=> $_POST['tahun'],
             'tglawal'	=> $_POST['tglawal'],
             'tglakhir'	=> $_POST['tglakhir']
        );		
		return $dataArray;
	}
	
	function get_tahunakun(){
      
        $this->db->select("*");
        $this->db->from("tahun");
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
		foreach($data as $row) {
            array_push($build_array["data"],array(
				'tahun'				=>$row->tahun,
                'tglawal'			=>date("Y-m-d",strtotime($row->tglawal)),
                'tglakhir'			=>date("Y-m-d",strtotime($row->tglakhir)),
            ));
        }
		
        echo json_encode($build_array);
    }
}
