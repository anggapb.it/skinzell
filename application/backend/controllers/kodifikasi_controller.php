<?php

class Kodifikasi_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function get_registrasi_kodifikasi(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $key                	= $this->input->post("key");
        $value                  = $this->input->post("value");
        
		$tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		
		$q = "SELECT `idregdet`
					 , `tglreg`
					 , `jamreg`
					 , `noreg`
					 , `norm`
					 , `normclean`
					 , `nmpasien`
					 , `idjnskelamin`
					 , `kdjnskelamin`
					 , `nmjnskelamin`
					 , `idstpasien`
					 , `nmstpasien`
					 , `idbagian`
					 , `nmbagian`
					 , `iddokter`
					 , `nmdokter`
					 , `nmdoktergelar`
					 , `keluhan`
					 , `idstposisipasien`
					 , `nmstposisipasien`
					 , `umurbulan`
					 , `umurhari`
					 , `umurtahun`
					 , `idstkeluar`
					 , `idcaradatang`
					 , `idcarakeluar`
					 , `idkematian`
					 , `idtindaklanjut`
					 , `tglmeninggal`
					 , `idjnskasus`
					 , `jammeninggal`
					 , `idkamar`
					 , `idbed`
					 , `nmkamar`
					 , `nmbed`
					 , `stinfo`
				FROM
				  (
				  SELECT `registrasidet`.`idregdet` AS `idregdet`
					   , `kuitansi`.`tglkuitansi` AS `tglreg`
					   , `registrasidet`.`jamreg` AS `jamreg`
					   , `registrasidet`.`noreg` AS `noreg`
					   , `registrasi`.`norm` AS `norm`
					   , trim(LEADING '0' FROM `registrasi`.`norm`) AS `normclean`
					   , `pasien`.`nmpasien` AS `nmpasien`
					   , `pasien`.`idjnskelamin` AS `idjnskelamin`
					   , `jkelamin`.`kdjnskelamin` AS `kdjnskelamin`
					   , `jkelamin`.`nmjnskelamin` AS `nmjnskelamin`
					   , `registrasi`.`idstpasien` AS `idstpasien`
					   , `stpasien`.`nmstpasien` AS `nmstpasien`
					   , `registrasidet`.`idbagian` AS `idbagian`
					   , `bagian`.`nmbagian` AS `nmbagian`
					   , `registrasidet`.`iddokter` AS `iddokter`
					   , `dokter`.`nmdokter` AS `nmdokter`
					   , `dokter`.`nmdoktergelar` AS `nmdoktergelar`
					   , `registrasi`.`keluhan` AS `keluhan`
					   , `reservasi`.`idstposisipasien` AS `idstposisipasien`
					   , `stposisipasien`.`nmstposisipasien` AS `nmstposisipasien`
					   , `registrasidet`.`umurbulan` AS `umurbulan`
					   , `registrasidet`.`umurhari` AS `umurhari`
					   , `registrasidet`.`umurtahun` AS `umurtahun`
					   , `registrasidet`.`idstkeluar` AS `idstkeluar`
					   , `registrasidet`.`idcaradatang` AS `idcaradatang`
					   , `registrasidet`.`idcarakeluar` AS `idcarakeluar`
					   , `kodifikasi`.`idkematian` AS `idkematian`
					   , `kodifikasi`.`idtindaklanjut` AS `idtindaklanjut`
					   , `kodifikasi`.`tglmeninggal` AS `tglmeninggal`
					   , `kodifikasi`.`idjnskasus` AS `idjnskasus`
					   , time_format(`kodifikasi`.`jammeninggal`, '%H:%i') AS `jammeninggal`
					   , `registrasidet`.`idkamar` AS `idkamar`
					   , `registrasidet`.`idbed` AS `idbed`
					   , `kamar`.`nmkamar` AS `nmkamar`
					   , `bed`.`nmbed` AS `nmbed`
					   , (SELECT count(`kodifikasidet`.`idkodifikasi`) AS `nreg`
						  FROM
							(`kodifikasidet`
						  LEFT JOIN `kodifikasi`
						  ON ((`kodifikasidet`.`idkodifikasi` = `kodifikasi`.`idkodifikasi`)))
						  WHERE
							(`kodifikasi`.`noreg` = `registrasi`.`noreg`)) AS `stinfo`
				  FROM
					(((((((((((((`registrasidet`
				  LEFT JOIN `registrasi`
				  ON ((`registrasidet`.`noreg` = `registrasi`.`noreg`)))
				  LEFT JOIN `pasien`
				  ON ((`registrasi`.`norm` = `pasien`.`norm`)))
				  LEFT JOIN `jkelamin`
				  ON ((`pasien`.`idjnskelamin` = `jkelamin`.`idjnskelamin`)))
				  LEFT JOIN `stpasien`
				  ON ((`registrasi`.`idstpasien` = `stpasien`.`idstpasien`)))
				  LEFT JOIN `bagian`
				  ON ((`registrasidet`.`idbagian` = `bagian`.`idbagian`)))
				  LEFT JOIN `dokter`
				  ON ((`registrasidet`.`iddokter` = `dokter`.`iddokter`)))
				  LEFT JOIN `reservasi`
				  ON ((`reservasi`.`idregdet` = `registrasidet`.`idregdet`)))
				  LEFT JOIN `stposisipasien`
				  ON ((`reservasi`.`idstposisipasien` = `stposisipasien`.`idstposisipasien`)))
				  LEFT JOIN `kodifikasi`
				  ON ((`kodifikasi`.`noreg` = `registrasi`.`noreg`)))
				  LEFT JOIN `kamar`
				  ON ((`registrasidet`.`idkamar` = `kamar`.`idkamar`)))
				  LEFT JOIN `bed`
				  ON ((`registrasidet`.`idbed` = `bed`.`idbed`)))
				  LEFT JOIN `nota`
				  ON ((`registrasidet`.`idregdet` = `nota`.`idregdet`)))
				  LEFT JOIN `kuitansi`
				  ON ((`kuitansi`.`nokuitansi` = `nota`.`nokuitansi`)))
				  WHERE
					(((`registrasidet`.`idcarakeluar` <> 6)
					OR isnull(`registrasidet`.`idcarakeluar`))
					AND isnull(`registrasidet`.`tglbatal`)
					AND (`registrasi`.`idjnspelayanan` IN (1, 2, 3)
					AND `kuitansi`.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'))
				  GROUP BY
					`registrasi`.`noreg`
				  , `registrasidet`.`idbagian`
				  UNION
				  SELECT 1 AS `idregdet`
					   , `kuitansi`.`tglkuitansi` AS `tglreg`
					   , `skl`.`jamkelahiran` AS `jamreg`
					   , `skl`.`noreganak` AS `noreg`
					   , `skl`.`normanak` AS `norm`
					   , trim(LEADING '0' FROM `skl`.`normanak`) AS `normclean`
					   , `pasien`.`nmpasien` AS `nmpasien`
					   , `skl`.`idjnskelamin` AS `idjnskelamin`
					   , `jkelamin`.`kdjnskelamin` AS `kdjnskelamin`
					   , `jkelamin`.`nmjnskelamin` AS `nmjnskelamin`
					   , 1 AS `idstpasien`
					   , 'Baru' AS `nmstpasien`
					   , NULL AS `idbagian`
					   , NULL AS `nmbagian`
					   , `skl`.`iddokter` AS `iddokter`
					   , `dokter`.`nmdokter` AS `nmdokter`
					   , `dokter`.`nmdoktergelar` AS `nmdoktergelar`
					   , NULL AS `keluhan`
					   , NULL AS `idstposisipasien`
					   , NULL AS `nmstposisipasien`
					   , NULL AS `umurbulan`
					   , NULL AS `umurhari`
					   , NULL AS `umurtahun`
					   , `kodifikasi`.`idstkeluar` AS `idstkeluar`
					   , NULL AS `idcaradatang`
					   , `kodifikasi`.`idcarakeluar` AS `idcarakeluar`
					   , `kodifikasi`.`idkematian` AS `idkematian`
					   , `kodifikasi`.`idtindaklanjut` AS `idtindaklanjut`
					   , `kodifikasi`.`tglmeninggal` AS `tglmeninggal`
					   , `kodifikasi`.`idjnskasus` AS `idjnskasus`
					   , time_format(`kodifikasi`.`jammeninggal`, '%H:%i') AS `jammeninggal`
					   , NULL AS `idkamar`
					   , NULL AS `idbed`
					   , NULL AS `nmkamar`
					   , NULL AS `nmbed`
					   , (SELECT count(`kodifikasidet`.`idkodifikasi`) AS `nreg`
						  FROM
							(`kodifikasidet`
						  LEFT JOIN `kodifikasi`
						  ON ((`kodifikasidet`.`idkodifikasi` = `kodifikasi`.`idkodifikasi`)))
						  WHERE
							(`kodifikasi`.`noreg` = `skl`.`noreganak`)) AS `stinfo`
				  FROM
					((((((((`skl`
				  LEFT JOIN `dokter`
				  ON ((`skl`.`iddokter` = `dokter`.`iddokter`)))
				  LEFT JOIN `kodifikasi`
				  ON ((`kodifikasi`.`noreg` = `skl`.`noreganak`)))
				  LEFT JOIN `jkelamin`
				  ON ((`skl`.`idjnskelamin` = `jkelamin`.`idjnskelamin`)))
				  LEFT JOIN `pasien`
				  ON ((`pasien`.`norm` = `skl`.`normanak`)))
				  LEFT JOIN `registrasi`
				  ON ((`registrasi`.`noreg` = `skl`.`noreg`)))
				  LEFT JOIN `registrasidet`
				  ON ((`registrasidet`.`noreg` = `registrasi`.`noreg`)))
				  LEFT JOIN `nota`
				  ON ((`nota`.`idregdet` = `registrasidet`.`idregdet`)))
				  LEFT JOIN `kuitansi`
				  ON ((`kuitansi`.`nokuitansi` = `nota`.`nokuitansi`)))
				  WHERE
					((`skl`.`noreganak` IS NOT NULL)
					AND (`kuitansi`.`idstkuitansi` = 1)
					AND isnull(`registrasidet`.`userbatal`)
					AND (`registrasi`.`idjnspelayanan` IN (1, 2, 3)
					AND `kuitansi`.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'))
				  ) kdpeny
				WHERE
					if('1' = '".$key."', `tglreg` BETWEEN '".$tglawal."' AND '".$tglakhir."', ".$key." LIKE '%".$value."%'
					AND `tglreg` BETWEEN '".$tglawal."' AND '".$tglakhir."')
				ORDER BY
				  noreg DESC";
				
		$query = $this->db->query($q);
       
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
		
        $ttl = count($data);//$this->count_registrasi_kodifikasi();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_kodifikasi(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $noreg                  = $this->input->post("noreg");
      
        $this->db->select("*");
        $this->db->from("kodifikasidet");
        $this->db->join("kodifikasi","kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi","left");
        $this->db->join("penyakit","penyakit.idpenyakit = kodifikasidet.idpenyakit","left");
        $this->db->join("jstpenyakit","jstpenyakit.idjstpenyakit = kodifikasidet.idjstpenyakit","left");
        $this->db->join("jdiagnosa","jdiagnosa.idjnsdiagnosa = kodifikasidet.idjnsdiagnosa","left");
		if($noreg) $this->db->where('noreg', $noreg);
        
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
        $noreg                  = $this->input->post("noreg");
		
        $this->db->select("*");
        $this->db->from("kodifikasidet");
        $this->db->join("kodifikasi","kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi","left");
        $this->db->join("penyakit","penyakit.idpenyakit = kodifikasidet.idpenyakit","left");
        $this->db->join("jstpenyakit","jstpenyakit.idjstpenyakit = kodifikasidet.idjstpenyakit","left");
        $this->db->join("jdiagnosa","jdiagnosa.idjnsdiagnosa = kodifikasidet.idjnsdiagnosa","left");
		if($noreg) $this->db->where('noreg', $noreg);
        
        
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function insertregistrasitokodifikasi(){
        $this->db->trans_begin();
		$kodifikasi = false;
		$kodifikasidet = false;
		
		$query = $this->db->getwhere('kodifikasi',array('noreg'=>$_POST['tf_noreg']));
		
		if($query->num_rows() > 0) {
			$kodifikasi = $this->updateKod();
			if($_POST['arrdiagnosa'] != '[]'){
				$kodifikasidet = $this->insert_kodifikasidet();
			}
		} else {
			$kodifikasi = $this->insert_kodifikasi();
			if($_POST['arrdiagnosa'] != '[]'){
				$kodifikasidet = $this->insert_kodifikasidet();
			}
		}

		if($kodifikasi)
		{
			$this->updateReg();
            $this->db->trans_commit();
			$ret["success"] = true;
		} else {
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_kodifikasi(){
		$dataArray = $this->getFieldsAndValuesKod();
		$z =$this->db->insert('kodifikasi', $dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret = false;
        }
        return $ret;
    }
	
	function getFieldsAndValuesKod(){
		if($_POST['cb_tlanjut_h'] == '')$_POST['cb_tlanjut_h'] = null;
		if($_POST['cb_pkematian_h'] == '')$_POST['cb_pkematian_h'] = null;
		if($_POST['cb_carakeluar_h'] == '')$_POST['cb_carakeluar_h'] = null;
		if($_POST['cb_stkeluar_h'] == '')$_POST['cb_stkeluar_h'] = null;

		$dataArray = array(
             'tglkodifikasi'=> date('Y-m-d'),
             'jamkodifikasi'=> date('H:i:s'),
             'noreg'=> $_POST['tf_noreg'],
             'idtindaklanjut'=> $_POST['cb_tlanjut_h'],
             'idkematian'=> $_POST['cb_pkematian_h'],
             'userid'=> $this->session->userdata['user_id'],
			 'tglmeninggal'=> ($this->input->post("tglmeninggal")) ? date('Y-m-d',strtotime($this->input->post("tglmeninggal"))):null,
			 'jammeninggal'=> ($this->input->post("jammeninggal")) ? $this->input->post("jammeninggal"):null,			 
             'idjnskasus'=> $_POST['cb_jkasus_h'],
			 'idcarakeluar'=> $_POST['cb_carakeluar_h'],
			 'idstkeluar' => $_POST['cb_stkeluar_h']
        );		
		return $dataArray;
	}
	
	/* function getFieldsAndValuesKode(){
		if($_POST['cb_tlanjut_h'] == '')$_POST['cb_tlanjut_h'] = null;
		if($_POST['cb_pkematian_h'] == '')$_POST['cb_pkematian_h'] = null;
		if($_POST['cb_carakeluar_h'] == '')$_POST['cb_carakeluar_h'] = null;
		if($_POST['cb_stkeluar_h'] == '')$_POST['cb_stkeluar_h'] = null;
		
		$dataArray = array(
             'tglkodifikasi'=> date('Y-m-d'),
             'jamkodifikasi'=> date('H:i:s'),
             'noreg'=> $_POST['tf_noreg'],
             'idtindaklanjut'=> $_POST['cb_tlanjut_h'],
             'idkematian'=> $_POST['cb_pkematian_h'],
             'userid'=> $this->session->userdata['user_id'],
			 'tglmeninggal'=> ($this->input->post("tglmeninggal")) ? date('Y-m-d',strtotime($this->input->post("tglmeninggal"))):null,
			 'jammeninggal'=> ($this->input->post("jammeninggal")) ? $this->input->post("jammeninggal"):null,			 
             'idjnskasus'=> $_POST['cb_jkasus_h'],
			 'idcarakeluar'=> $_POST['cb_carakeluar_h'],
			 'idstkeluar' => $_POST['cb_stkeluar_h']
        );		
		return $dataArray;
	} */
	
	function insert_kodifikasidet(){
		$query = $this->db->getwhere('kodifikasi',array('noreg'=>$_POST['tf_noreg']));
		$reg = $query->row_array();
		
		if($reg){
			$where['idkodifikasi'] = $reg['idkodifikasi'];
			$del = $this->rhlib->deleteRecord('kodifikasidet',$where);
			$idkodifikasi = $reg['idkodifikasi'];
		} else {
			$idkodifikasi = $this->db->insert_id;
		}
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrdiagnosa']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-',$val);
			$dataArray = $this->getFieldsAndValuesKodDet($idkodifikasi, $vale[0], $vale[1], $vale[2]);
			$z =$this->db->insert('kodifikasidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret = false;
        }
        return $ret;
    }
	function updateKod(){
		$dataArray = $this->getFieldsAndValuesKod();
		
		//UPDATE
		$this->db->where('noreg', $_POST['tf_noreg']);
		$this->db->update('kodifikasi', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	function updateReg(){
		$dataArray = $this->getUpdateField();
		
		//UPDATE
		$this->db->where('idregdet', $_POST['tf_idregdet']);
		$this->db->update('registrasidet', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function getFieldsAndValuesKodDet($idkodifikasi, $val1,$val2,$val3){
		/* $hasil = $this->db->query("SELECT idkodifikasi FROM kodifikasi ORDER BY idkodifikasi DESC LIMIT 1");
		foreach($hasil->result() as $row){
			$idkodifikasi= $row->idkodifikasi;
		} */
			
		$dataArray = array(
             'idkodifikasi'=> $idkodifikasi,
             'idpenyakit'=> $val1,
             'idjnsdiagnosa'=> $this->searchId('idjnsdiagnosa','jdiagnosa','nmjnsdiagnosa',$val2),
             'idjstpenyakit'=> $this->searchId('idjstpenyakit','jstpenyakit','nmjstpenyakit',$val3),
             'idmappenyakit'=> null
        );		
		return $dataArray;
	}
	
	function getUpdateField(){
		if($_POST['cb_carakeluar_h'] == '')$_POST['cb_carakeluar_h'] = null;
		if($_POST['cb_stkeluar_h'] == '')$_POST['cb_stkeluar_h'] = null;

		$dataArray = array(
			'idcarakeluar'=> $_POST['cb_carakeluar_h'],
			'idstkeluar' => $_POST['cb_stkeluar_h']
		);
		return $dataArray;
	}
	
	function hapusdetkodifikasi(){     
		$where['idkodifikasi'] = $_POST['idkodifikasi'];
		$where['idpenyakit'] = $_POST['idpenyakit'];
		$del = $this->rhlib->deleteRecord('kodifikasidet',$where);
        echo $del;
    }
}
