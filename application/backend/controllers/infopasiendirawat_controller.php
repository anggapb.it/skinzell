<?php

class Infopasiendirawat_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_pipasiendirawat(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        $cekid                  = $this->input->post("cekid");
      
        $this->db->select('*');
        $this->db->from('v_pipasiendirawat');
		$this->db->order_by('v_pipasiendirawat.tglmasuk', 'desc');
		
		if($cekid){
			if($cekid == '1'){
				//$this->db->where('v_pipasiendirawat.tglmasuk IS NOT NULL AND v_pipasiendirawat.tglkeluar IS NOT NULL');
			} elseif($cekid == '2'){
				$this->db->where("v_pipasiendirawat.idstposisipasien = '5'");
			} elseif($cekid == '3'){
				$this->db->where("v_pipasiendirawat.idstposisipasien != '5'");
			}
        }
		
		$key = $_POST["key"];  
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		}
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}
