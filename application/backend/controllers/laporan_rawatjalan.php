<?php 
class Laporan_rawatjalan extends Controller{
	public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }

	function get_rawatjalan(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$bagian				    = $this->input->post("bagian");
		$penjamin				    = $this->input->post("penjamin");
		$this->db->select("*");
		$this->db->from("v_rjkunjunganpas");
		
		
		if($tglawal){
		$this->db->where('`tglreg` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($bagian){$this->db->where('idbagian',$bagian);}
		if($penjamin){$this->db->where('idpenjamin',$penjamin);}
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_rjkunjunganpas');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_bagianrj(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$this->db->select("*");
		$this->db->from("bagian");
		
		$this->db->where('`idjnspelayanan`', 1);
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_rjkunjunganpas');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
function get_batal(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
	//	$ruang				= $this->input->post("ruangan");
		$this->db->select("*");
		$this->db->from("v_laprjbatal");
		
		if($tglawal){
		$this->db->where('`tglbatal` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_laprjbatal');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
}