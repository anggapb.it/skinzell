<?php 
 
class Skl_Controller extends Controller {
	public function __construct()
	{
			parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
	}
	
	function get_skl(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$noreg                  = $this->input->post("noreg");
		
		$this->db->select("*");
		$this->db->from('v_skldetail');
/* 		$this->db->join('registrasi',
					'registrasi.noreg = skl.noreg', 'left'
		);
		$this->db->join('dokter',
					'dokter.iddokter = skl.iddokter', 'left'
		);
		$this->db->join('jkelamin',
					'jkelamin.idjnskelamin = skl.idjnskelamin', 'left'
		);
		$this->db->join('kembar',
					'kembar.idkembar = skl.idkembar', 'left'
		);
		$this->db->join('caralahir',
					'caralahir.idcaralahir = skl.idcaralahir', 'left'
		);
		$this->db->join('pasien', 'pasien.norm = registrasi.norm', 'left'); */
		if($noreg)
		{
		$this->db->where('noreg',$noreg);
		}else{
		$this->db->where('noreg',null);
		}

		$this->db->order_by('idskl DESC'); 
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>$data);

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
       $this->db->select("*");
		$this->db->from('v_skldetail');
		/* $this->db->join('kembar',
					'kembar.idkembar = skl.idkembar', 'left'
		); */
		$this->db->order_by('idskl DESC'); 
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_vskl(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from('v_skl');
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow2($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function numrow2($fields, $query){
      
     		$this->db->select("*");
		$this->db->from('v_skl');	
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function getDataReg(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $noreg                  = $this->input->post("noreg");
		
		$this->db->select("*");
		$this->db->from('v_registrasi');
		$this->db->where('noreg',$noreg);
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow3($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function numrow3($fields, $query){
      
     	$this->db->select("*");
		$this->db->from('v_registrasi');
		
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function simpanskl(){
		$this->db->trans_begin();
		$arrskl = $this->input->post("arrskl");
		$skl = $this->insert_skl($arrskl);
		
		if($skl){
			$this->db->trans_commit();
			$ret["success"] = true;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		echo json_encode($ret);		
	}


	function insert_skl($arrskl){
		$nmayah = $this->input->post("nmayah");
		$tgllahiribu = $this->input->post("tgllahiribu");
		$thnibu = $this->input->post("thnibu");
		$blnibu = $this->input->post("blnibu");
		$tglibu = $this->input->post("tglibu");
		$tgllahirayah = $this->input->post("tgllahirayah");
		$thnayah = $this->input->post("thnayah");
		$blnayah = $this->input->post("blnayah");
		$tglayah = $this->input->post("tglayah");
		$iddokter = $this->input->post("iddokter");
		$kembar = ($this->input->post("kembar")) ? $this->input->post("kembar"):null;
		$noreg = $this->input->post("noreg");
		$userid = $this->session->userdata['user_id'];
		$k=array('[',']', '"');
		$r= str_replace($k, '', $arrskl);
		$b = explode(';',$r);
		foreach($b as $val){
			$vale = explode('+', $val);
				$anakn = ($vale[5]=="0") ? null:$vale[5];
				$np = $this->getnorm();
				$norm = str_pad($np, 10, "0", STR_PAD_LEFT);
				$dataArray = $this->getFieldsAndValues($vale[0],$vale[1],$vale[2],$vale[3],$vale[4],$vale[5],$vale[6],$vale[7],$vale[8],$vale[9]);
				$this->db->query("CALL SP_skl (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",array(
					$vale[0],
					$nmayah,
					$vale[1],
					date("Ymd",strtotime($vale[3])),
					$vale[4],
					$vale[6],
					$vale[7],
					$anakn,
					$tgllahiribu,
					$thnibu,
					$blnibu,
					$tglibu,
					$tgllahirayah,
					$thnayah,
					$blnayah,
					$tglayah,
					$userid,
					$noreg,
					$iddokter,
					$vale[2],
					$kembar,
					$vale[9],
					$vale[8],
					$norm,
					$vale[10],
					$vale[11],
					$vale[12]
				));
		}
		return true;
	}
	
	function getFieldsAndValues($val0,$val1,$val2,$val3,$val4,$val5,$val6,$val7,$val8,$val9){
		
		$dataArray = array(
			'kdskl' => $val0,
			'nmbayi' => $val1,
			'idjnskelamin' => $val2,
			'tglkelahiran' => $val3,
			'jamkelahiran' => $val4,
			'anakke' => $val5,
			'beratkelahiran' => $val6,
			'panjangkelahiran' => $val7,
			'idcaralahir' => $val8,
			'idkondisilahir' => $val9
			
		);
		return $dataArray;
	}
	
	function getnorm(){
        $this->db->select("max(norm) AS max_np");
        $this->db->from("pasien");
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function update_field_skl(){ 				
		$field = $_POST['field'];
		$value = ($_POST['value']) ? $_POST['value']:null;
		
		//UPDATE
		$this->db->where('idskl', $_POST['idskl']);
		$this->db->set($field, $value);
		$this->db->update('skl'); 
		
		if($field == 'nmbayi'){			
			$this->db->where('norm',str_pad($_POST['normanak'], 10, "0", STR_PAD_LEFT));
			$this->db->set('nmpasien', $value);
			$this->db->update('pasien'); 
		}
		if($field == 'idjnskelamin'){
			$this->db->where('norm', str_pad($_POST['normanak'], 10, "0", STR_PAD_LEFT));
			$this->db->set('idjnskelamin', $value);
			$this->db->update('pasien'); 			
		}
		if($field == 'tglkelahiran'){
			$this->db->where('norm', str_pad($_POST['normanak'], 10, "0", STR_PAD_LEFT));
			$this->db->set('tgllahir', $value);
			$this->db->update('pasien'); 			
		}
		
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
	
	function get_vskl2(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$kdskl					= $this->input->post("kdskl");
		$noreg					= $this->input->post("noreg");
		$this->db->select("*");
		$this->db->from('v_skldetail');

		if($noreg){
		$this->db->where('noreg',$noreg);
		}else{
		$this->db->where('noreg',null);
		
		}
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow4($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function numrow4($fields, $query){
      
     	$this->db->select("*");
		$this->db->from('v_skldetail');
	
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function updateSkl(){
	$dataArray = $this->getValuesAndFieldsUpdate();
	
		$this->db->where('idskl', $_POST['idskl']);
		$this->db->update('skl', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function getValuesAndFieldsUpdate(){
		$dataArray = array(
             'noreg'=> $_POST['noreg'],
             'nmayah'=> $_POST['nmayah'],
			 'tgllahirayah'=> $_POST['tgllahirayah'],
			 'tgllahiribu'=>$_POST['tgllahiribu'],
			 'thnayah'=> $_POST['thnayah'],
			 'blnayah'=> $_POST['blnayah'],
			 'tglayah'=> $_POST['tglayah'],
			 'thnibu'=> $_POST['thnayah'],
			 'blnibu'=> $_POST['blnibu'],
			 'tglibu'=> $_POST['tglibu'],
			 'iddokter'=> $_POST['iddokter'],
			 'idkembar'=> $_POST['kembar'],
			 
        );		
		return $dataArray;
	}
    function deleteskl(){
		$where['idskl'] = $_POST['idskl'];
		$del = $this->rhlib->deleteRecord('skl',$where);
        return $del;
	}
	
}
?>