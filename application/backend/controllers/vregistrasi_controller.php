<?php

class Vregistrasi_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_vregistrasi(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cek                    = $this->input->post("cek");
        $norm                   = $this->input->post("norm");
        $fnorm                  = $this->input->post("fnorm");
        $ftglkeluar             = $this->input->post("ftglkeluar");
		
        $ctglreg                = $this->input->post("ctglreg");
        $cnmjnspelayanan        = $this->input->post("cnmjnspelayanan");
        $cdokter                = $this->input->post("cdokter");
        $cnorm                  = $this->input->post("cnorm");
        $cnmpasien              = $this->input->post("cnmpasien");
        $cnoreg                 = $this->input->post("cnoreg");
        $gnoreg                 = $this->input->post("gnoreg");
        $nokuitansi				= $this->input->post("nokuitansi");
        $groupby                = $this->input->post("groupby");
        $groupbyq				= $this->input->post("groupbyq");
        $oposisipasien          = $this->input->post("oposisipasien");
        $onmpasien              = $this->input->post("onmpasien");
        $onoreg              	= $this->input->post("onoreg");
        $khususri               = $this->input->post("khususri");
		$aa						= $this->input->post("aa");
		$riposisipasien			= $this->input->post("riposisipasien");
		$otanggal				= $this->input->post("otanggal");
		$ctransfar				= $this->input->post("ctransfar");
      
		$this->db->select("
			r.noreg AS noreg,
			r.keluhan AS keluhan,
			r.nmkerabat AS nmkerabat,
			r.notelpkerabat AS notelpkerabat,
			r.catatan AS catatanr,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			p.alamat AS alamatp,
			p.nmibu AS nmibu,
			p.alergi AS alergi,
			p.tptlahir AS tptlahirp,
			p.noidentitas AS noidentitas,
			p.tgllahir AS tgllahirp,
			p.notelp AS notelpp,
			p.nohp AS nohpp,
			p.tgldaftar AS tgldaftar,
			p.negara AS negara,
			rd.idregdet AS idregdet,
			rd.tglreg AS tglreg,
			rd.jamreg AS jamreg,
			rd.tglmasuk AS tglmasuk,
			rd.jammasuk AS jammasuk,
			rd.tglkeluar AS tglkeluar,
			rd.jamkeluar AS jamkeluar,
			rd.catatankeluar AS catatankeluar,
			rd.tglrencanakeluar AS tglrencanakeluar,
			rd.catatanrencanakeluar AS catatanrencanakeluar,
			rd.umurtahun AS umurtahun,
			rd.umurbulan AS umurbulan,
			rd.umurhari AS umurhari,
			rd.userbatal AS userbatal,
			rd.nmdokterkirim AS nmdokterkirim,
			rd.userinput AS userinput,
			rd.idstkeluar AS idstkeluar,
			rd.idcarakeluar AS idcarakeluar,
			r.idpenjamin AS idpenjamin,
			r.idjnspelayanan AS idjnspelayanan,
			r.idstpasien AS idstpasien,
			r.idhubkeluarga AS idhubkeluarga,
			p.idjnskelamin AS idjnskelamin,
			rd.idcaradatang AS idcaradatang,
			rd.idbagian AS idbagian,
			rd.idbagiankirim AS idbagiankirim,
			rd.idkamar AS idkamar,
			rd.iddokter AS iddokter,
			rd.iddokterkirim AS iddokterkirim,
			rd.idklsrawat AS idklsrawat,
			rd.idklstarif AS idklstarif,
			rd.idshift AS idshift,
			rd.idbed AS idbed,
			bed.idstbed AS idstbed,
			bed.nmbed AS nmbed,
			jkel.nmjnskelamin AS nmjnskelamin,
			jkel.kdjnskelamin AS kdjnskelamin,
			jpel.nmjnspelayanan AS nmjnspelayanan,
			dae.nmdaerah AS nmdaerah,
			sp.idstpelayanan AS idstpelayanan,
			sp.nmstpelayanan AS nmstpelayanan,
			pnj.nmpenjamin AS nmpenjamin,
			kr.nmklsrawat AS nmklsrawat,
			bag.nmbagian AS nmbagian,
			bagkrm.nmbagian AS nmbagiankirim,
			kmr.nmkamar AS nmkamar,
			kmr.kdkamar AS kdkamar,
			nt.nonota AS nonota,
			nt.nona AS nona,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.catatandiskon AS catatand,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			nt.idsttransaksi AS idsttransaksi,
			nt.idregdettransfer AS idregdettransfer,
			nt.stplafond AS stplafond,
			na.stplafond AS stplafondna,
			na.catatandiskon AS catatands,
			d.nmdokter AS nmdokter,
			d.nmdoktergelar AS nmdoktergelar,
			dkrm.nmdoktergelar AS nmdokterkirimdlm,
			res.noantrian AS noantrian,
			cd.nmcaradatang AS nmcaradatang,
			pospas.nmstposisipasien AS nmstposisipasien,
			pospas.idstposisipasien AS idstposisipasien,
			kui.nokuitansi AS nokuitansi,
			kui.atasnama AS atasnama,
			kui.total AS total,
			kui.nokasir AS nokasir,
			kui.tglkuitansi AS tglkuitansi,
			kui.jamkuitansi AS jamkuitansi,
			sbt.kdsbtnm AS kdsbtnm,
			kt.nmklstarif AS nmklstarif,
			stkawin.nmstkawin AS nmstkawin,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit,
			daerah.nmdaerah AS kelurahan,
			kec.nmdaerah AS kec,
			kota.nmdaerah AS kota,
			agama.nmagama AS nmagama,
			goldarah.nmgoldarah AS nmgoldarah,
			pendidikan.nmpendidikan AS nmpendidikan,
			pekerjaan.nmpekerjaan AS nmpekerjaan,
			sukubangsa.nmsukubangsa AS nmsukubangsa
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg AND rd.idregdet = (SELECT rd2.idregdet FROM registrasidet rd2 WHERE rd2.noreg = r.noreg ORDER BY rd2.idregdet desc LIMIT 1 )', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
		if($groupby)$this->db->groupby($groupby);
		if($groupbyq)$this->db->groupby('rd.tglreg, rd.jamreg desc');
        if($aa){
			$this->db->where('r.idjnspelayanan',$aa);
		}
        if($query !=""){
			$fields2 = '["r.noreg","p.nmpasien","jkel.kdjnskelamin","bag.nmbagian","p.tgllahir"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
		
		if($cek){
			if($cek == 'RJ'){
				$this->db->where('r.idjnspelayanan', 1);
			} elseif($cek == 'RI'){
				$this->db->where('r.idjnspelayanan', 2);
			} elseif($cek == 'UG'){
				$this->db->where('r.idjnspelayanan', 3);
			} else {
				$this->db->where('p.norm', str_pad($norm, 10, "0", STR_PAD_LEFT));
			}
        }
		
		if($ctglreg != '')$this->db->where('rd.tglreg', $ctglreg);
        if($cnmjnspelayanan != '')$this->db->where('rd.idbagian', $cnmjnspelayanan);
        if($cdokter != '')$this->db->where('rd.iddokter', $cdokter);
        if($cnorm != '')$this->db->like('p.norm', $cnorm);//str_pad($cnorm, 10, "0", STR_PAD_LEFT));
        if($cnmpasien != ''){
			$arrcnmpasien = explode(' ', $cnmpasien);
			foreach($arrcnmpasien AS $val){
				$this->db->like('p.nmpasien', $val);
			}
		}
        if($cnoreg != '')$this->db->like('r.noreg', $cnoreg);
        if($nokuitansi == 1){
			$this->db->where('rd.tglkeluar IS NOT NULL', null);
			$this->db->where('kui.nokuitansi IS NOT NULL', null);
		} elseif($nokuitansi == 2){
			$this->db->where('rd.tglkeluar IS NULL', null);
			$this->db->where('kui.nokuitansi IS NULL', null);
		}
        #if($fnorm != '')$this->db->where('p.norm', '45507');
        if($fnorm != '')$this->db->where("trim(LEADING 0 FROM p.norm) = '".$fnorm."'");
        if($ftglkeluar != '')$this->db->where('rd.tglkeluar IS NULL', null);
        if($oposisipasien != '')$this->db->order_by('pospas.idstposisipasien asc');
        if($onmpasien != '')$this->db->order_by('p.nmpasien asc');
        if($onoreg != '')$this->db->order_by('r.noreg desc');
        if($otanggal != '')$this->db->order_by('rd.tglreg, rd.jamreg desc');
        if($gnoreg != '')$this->db->groupby('r.noreg', $gnoreg);
		if($riposisipasien != '')$this->db->where('pospas.idstposisipasien', $riposisipasien);
		//if($riposisipasien != '')$this->db->where('r.noreg', 'RI15000822');
		if($ctransfar)$this->db->where('rd.tglreg BETWEEN ', "'". $this->input->post("tglawal") ."' AND '". $this->input->post("tglakhir") ."'", false);
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
		if($ttl>0 && !is_null($data[0]->noreg)){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cek                    = $this->input->post("cek");
        $norm                   = $this->input->post("norm");
        $fnorm                  = $this->input->post("fnorm");
        $ftglkeluar             = $this->input->post("ftglkeluar");
		
        $ctglreg                = $this->input->post("ctglreg");
        $cnmjnspelayanan        = $this->input->post("cnmjnspelayanan");
        $cdokter                = $this->input->post("cdokter");
        $cnorm                  = $this->input->post("cnorm");
        $cnmpasien              = $this->input->post("cnmpasien");
        $cnoreg                 = $this->input->post("cnoreg");
        $gnoreg                 = $this->input->post("gnoreg");
        $nokuitansi				= $this->input->post("nokuitansi");
        $groupby                = $this->input->post("groupby");
        $groupbyq				= $this->input->post("groupbyq");
        $oposisipasien          = $this->input->post("oposisipasien");
        $onmpasien              = $this->input->post("onmpasien");
        $onoreg              	= $this->input->post("onoreg");
        $khususri               = $this->input->post("khususri");
		$aa						= $this->input->post("aa");
		$riposisipasien			= $this->input->post("riposisipasien");
		$otanggal				= $this->input->post("otanggal");
		$ctransfar				= $this->input->post("ctransfar");
      
        $this->db->select("
			r.noreg AS noreg,
			r.keluhan AS keluhan,
			r.nmkerabat AS nmkerabat,
			r.notelpkerabat AS notelpkerabat,
			r.catatan AS catatanr,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			p.alamat AS alamatp,
			p.nmibu AS nmibu,
			p.alergi AS alergi,
			p.tptlahir AS tptlahirp,
			p.noidentitas AS noidentitas,
			p.tgllahir AS tgllahirp,
			p.notelp AS notelpp,
			p.nohp AS nohpp,
			p.tgldaftar AS tgldaftar,
			p.negara AS negara,
			rd.idregdet AS idregdet,
			rd.tglreg AS tglreg,
			rd.jamreg AS jamreg,
			rd.tglmasuk AS tglmasuk,
			rd.jammasuk AS jammasuk,
			rd.tglkeluar AS tglkeluar,
			rd.jamkeluar AS jamkeluar,
			rd.catatankeluar AS catatankeluar,
			rd.tglrencanakeluar AS tglrencanakeluar,
			rd.catatanrencanakeluar AS catatanrencanakeluar,
			rd.umurtahun AS umurtahun,
			rd.umurbulan AS umurbulan,
			rd.umurhari AS umurhari,
			rd.userbatal AS userbatal,
			rd.nmdokterkirim AS nmdokterkirim,
			rd.userinput AS userinput,
			rd.idstkeluar AS idstkeluar,
			rd.idcarakeluar AS idcarakeluar,
			r.idpenjamin AS idpenjamin,
			r.idjnspelayanan AS idjnspelayanan,
			r.idstpasien AS idstpasien,
			r.idhubkeluarga AS idhubkeluarga,
			p.idjnskelamin AS idjnskelamin,
			rd.idcaradatang AS idcaradatang,
			rd.idbagian AS idbagian,
			rd.idbagiankirim AS idbagiankirim,
			rd.idkamar AS idkamar,
			rd.iddokter AS iddokter,
			rd.iddokterkirim AS iddokterkirim,
			rd.idklsrawat AS idklsrawat,
			rd.idklstarif AS idklstarif,
			rd.idshift AS idshift,
			rd.idbed AS idbed,
			bed.idstbed AS idstbed,
			bed.nmbed AS nmbed,
			jkel.nmjnskelamin AS nmjnskelamin,
			jkel.kdjnskelamin AS kdjnskelamin,
			jpel.nmjnspelayanan AS nmjnspelayanan,
			dae.nmdaerah AS nmdaerah,
			sp.idstpelayanan AS idstpelayanan,
			sp.nmstpelayanan AS nmstpelayanan,
			pnj.nmpenjamin AS nmpenjamin,
			kr.nmklsrawat AS nmklsrawat,
			bag.nmbagian AS nmbagian,
			bagkrm.nmbagian AS nmbagiankirim,
			kmr.nmkamar AS nmkamar,
			kmr.kdkamar AS kdkamar,
			nt.nonota AS nonota,
			nt.nona AS nona,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			nt.idsttransaksi AS idsttransaksi,
			nt.idregdettransfer AS idregdettransfer,
			nt.stplafond AS stplafond,
			na.stplafond AS stplafondna,
			d.nmdokter AS nmdokter,
			d.nmdoktergelar AS nmdoktergelar,
			dkrm.nmdoktergelar AS nmdokterkirimdlm,
			res.noantrian AS noantrian,
			cd.nmcaradatang AS nmcaradatang,
			pospas.nmstposisipasien AS nmstposisipasien,
			pospas.idstposisipasien AS idstposisipasien,
			kui.nokuitansi AS nokuitansi,
			kui.atasnama AS atasnama,
			kui.total AS total,
			kui.nokasir AS nokasir,
			kui.tglkuitansi AS tglkuitansi,
			kui.jamkuitansi AS jamkuitansi,
			sbt.kdsbtnm AS kdsbtnm,
			kt.nmklstarif AS nmklstarif,
			stkawin.nmstkawin AS nmstkawin,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit,
			daerah.nmdaerah AS kelurahan,
			kec.nmdaerah AS kec,
			kota.nmdaerah AS kota,
			agama.nmagama AS nmagama,
			goldarah.nmgoldarah AS nmgoldarah,
			pendidikan.nmpendidikan AS nmpendidikan,
			pekerjaan.nmpekerjaan AS nmpekerjaan,
			sukubangsa.nmsukubangsa AS nmsukubangsa
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
		if($groupby)$this->db->groupby($groupby);
		if($groupbyq)$this->db->groupby('rd.tglreg, rd.jamreg desc');
        if($aa){
			$this->db->where('r.idjnspelayanan',$aa);
		}
        if($query !=""){
			$fields2 = '["r.noreg","p.nmpasien","jkel.kdjnskelamin","bag.nmbagian","p.tgllahir"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
		
		if($cek){
			if($cek == 'RJ'){
				$this->db->where('r.idjnspelayanan', 1);
			} elseif($cek == 'RI'){
				$this->db->where('r.idjnspelayanan', 2);
			} elseif($cek == 'UG'){
				$this->db->where('r.idjnspelayanan', 3);
			} else {
				$this->db->where('p.norm', str_pad($norm, 10, "0", STR_PAD_LEFT));
			}
        }
		
		if($ctglreg != '')$this->db->where('rd.tglreg', $ctglreg);
        if($cnmjnspelayanan != '')$this->db->where('rd.idbagian', $cnmjnspelayanan);
        if($cdokter != '')$this->db->where('rd.iddokter', $cdokter);
        if($cnorm != '')$this->db->like('p.norm', $cnorm);//str_pad($cnorm, 10, "0", STR_PAD_LEFT));
        if($cnmpasien != ''){
			$arrcnmpasien = explode(' ', $cnmpasien);
			foreach($arrcnmpasien AS $val){
				$this->db->like('p.nmpasien', $val);
			}
		}
        if($cnoreg != '')$this->db->like('r.noreg', $cnoreg);
        if($nokuitansi == 1){
			$this->db->where('rd.tglkeluar IS NOT NULL', null);
			$this->db->where('kui.nokuitansi IS NOT NULL', null);
		} elseif($nokuitansi == 2){
			$this->db->where('rd.tglkeluar IS NULL', null);
			$this->db->where('kui.nokuitansi IS NULL', null);
		}
        #if($fnorm != '')$this->db->where('p.norm', $fnorm);
        if($fnorm != '')$this->db->where("trim(LEADING 0 FROM p.norm) = '".$fnorm."'");
        if($ftglkeluar != '')$this->db->where('rd.tglkeluar IS NULL', null);
        if($oposisipasien != '')$this->db->order_by('pospas.idstposisipasien asc');
        if($onmpasien != '')$this->db->order_by('p.nmpasien asc');
        if($onoreg != '')$this->db->order_by('r.noreg desc');
        if($otanggal != '')$this->db->order_by('rd.tglreg, rd.jamreg desc');
        if($gnoreg != '')$this->db->groupby('r.noreg', $gnoreg);
		if($riposisipasien != '')$this->db->where('pospas.idstposisipasien', $riposisipasien);
		if($ctransfar)$this->db->where('rd.tglreg BETWEEN ', "'". $this->input->post("tglawal") ."' AND '". $this->input->post("tglakhir") ."'", false);
		
        $q = $this->db->get();
		$ttl = $q->num_rows();
		$data = $q->result();
        if($ttl>0 && is_null($data[0]->noreg)){
            $ttl=0;
        }
        return $ttl;
    }
	
	function get_vregistrasifri(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$tglawal = $this->input->post("tglawal");
		$tglakhir = $this->input->post("tglakhir");
		
        $this->db->select("registrasi.noreg AS noreg
					 , registrasidet.idregdet AS idregdet
					 , pasien.norm AS norm
					 , pasien.nmpasien AS nmpasien
					 , pasien.tgllahir AS tgllahirp
					 , pasien.idjnskelamin AS idjnskelamin
					 , jkelamin.kdjnskelamin AS kdjnskelamin
					 , kuitansi.atasnama AS atasnama
					 , bagian.idbagian AS idbagian
					 , bagian.nmbagian AS nmbagian
					 , dokter.nmdoktergelar AS nmdoktergelar
					 , nota.nonota AS nonota
					 , kuitansi.total AS total
					 , nota.tglnota AS tglnota
					 , nota.jamnota AS jamnota
					 , nota.catatan AS catatannota
					 , kamar.nmkamar AS nmkamar
					 , bed.nmbed AS nmbed
					 , klstarif.nmklstarif AS nmklstarif
					 , stposisipasien.nmstposisipasien AS nmstposisipasien
					 , kuitansi.tglkuitansi", false);
					 
        $this->db->from('registrasi');		
		$this->db->join('registrasidet',
                'registrasidet.noreg = registrasi.noreg', 'left');
		$this->db->join('nota',
                'nota.idregdet = registrasidet.idregdet', 'left');
		$this->db->join('notadet',
                'notadet.nonota = nota.nonota', 'left');
		$this->db->join('pasien',
                'pasien.norm = registrasi.norm', 'left');
		$this->db->join('jkelamin',
                'jkelamin.idjnskelamin = pasien.idjnskelamin', 'left');
		$this->db->join('dokter',
                'dokter.iddokter = registrasidet.iddokter', 'left');
		$this->db->join('kuitansi',
                'kuitansi.nokuitansi = nota.nokuitansi', 'left');
		$this->db->join('bagian',
                'bagian.idbagian = registrasidet.idbagian', 'left');
		$this->db->join('kamar',
                'kamar.idkamar = registrasidet.idkamar', 'left');
		$this->db->join('bed',
                'bed.idbed = registrasidet.idbed', 'left');
		$this->db->join('klstarif',
                'klstarif.idklstarif = registrasidet.idklstarif', 'left');
		$this->db->join('reservasi',
                'reservasi.idregdet = registrasidet.idregdet', 'left');
		$this->db->join('stposisipasien',
                'stposisipasien.idstposisipasien = reservasi.idstposisipasien', 'left');
		$this->db->where("(NOT (`registrasi`.`noreg` IN (SELECT `rd`.`noreg` AS `noreg`
                                  FROM
                                    registrasidet rd
                                  LEFT JOIN `nota` `nt`
                                  ON `nt`.idregdet = rd.idregdet
                                  LEFT JOIN `kuitansi` k
                                  ON k.nokuitansi = `nt`.nokuitansi
                                  LEFT JOIN `kartustok`
                                  ON `kartustok`.noref = `nt`.nonota
                                  WHERE
                                    ((`kartustok`.`noref` = `nt`.`nonota`)
                                    AND (`nt`.`idregdet` = `rd`.`idregdet`)
                                    AND k.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                    ))))");
				
		$this->db->where("nota.idbagian = 11");
		$this->db->where("notadet.nonota IS NOT NULL");
		$this->db->where("registrasidet.userbatal IS NULL");
		$this->db->where("reservasi.idstposisipasien = 6");
		$this->db->where("kuitansi.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'");
		$this->db->groupby("registrasi.noreg");
		$this->db->order_by("registrasidet.idregdet DESC");
        if($query !=""){
			$fields2 = '["registrasi.noreg","pasien.nmpasien","jkelamin.kdjnskelamin","bagian.nmbagian"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrowfri();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0 && !is_null($data[0]->noreg)){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrowfri(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$tglawal = $this->input->post("tglawal");
		$tglakhir = $this->input->post("tglakhir");
		
        $this->db->select("registrasi.noreg AS noreg
					 , registrasidet.idregdet AS idregdet
					 , pasien.norm AS norm
					 , pasien.nmpasien AS nmpasien
					 , pasien.tgllahir AS tgllahirp
					 , pasien.idjnskelamin AS idjnskelamin
					 , jkelamin.kdjnskelamin AS kdjnskelamin
					 , kuitansi.atasnama AS atasnama
					 , bagian.idbagian AS idbagian
					 , bagian.nmbagian AS nmbagian
					 , dokter.nmdoktergelar AS nmdoktergelar
					 , nota.nonota AS nonota
					 , kuitansi.total AS total
					 , nota.tglnota AS tglnota
					 , nota.jamnota AS jamnota
					 , nota.catatan AS catatannota
					 , kamar.nmkamar AS nmkamar
					 , bed.nmbed AS nmbed
					 , klstarif.nmklstarif AS nmklstarif
					 , stposisipasien.nmstposisipasien AS nmstposisipasien
					 , kuitansi.tglkuitansi", false);
					 
        $this->db->from('registrasi');		
		$this->db->join('registrasidet',
                'registrasidet.noreg = registrasi.noreg', 'left');
		$this->db->join('nota',
                'nota.idregdet = registrasidet.idregdet', 'left');
		$this->db->join('notadet',
                'notadet.nonota = nota.nonota', 'left');
		$this->db->join('pasien',
                'pasien.norm = registrasi.norm', 'left');
		$this->db->join('jkelamin',
                'jkelamin.idjnskelamin = pasien.idjnskelamin', 'left');
		$this->db->join('dokter',
                'dokter.iddokter = registrasidet.iddokter', 'left');
		$this->db->join('kuitansi',
                'kuitansi.nokuitansi = nota.nokuitansi', 'left');
		$this->db->join('bagian',
                'bagian.idbagian = registrasidet.idbagian', 'left');
		$this->db->join('kamar',
                'kamar.idkamar = registrasidet.idkamar', 'left');
		$this->db->join('bed',
                'bed.idbed = registrasidet.idbed', 'left');
		$this->db->join('klstarif',
                'klstarif.idklstarif = registrasidet.idklstarif', 'left');
		$this->db->join('reservasi',
                'reservasi.idregdet = registrasidet.idregdet', 'left');
		$this->db->join('stposisipasien',
                'stposisipasien.idstposisipasien = reservasi.idstposisipasien', 'left');
		$this->db->where("(NOT (`registrasi`.`noreg` IN (SELECT `rd`.`noreg` AS `noreg`
                                  FROM
                                    registrasidet rd
                                  LEFT JOIN `nota` `nt`
                                  ON `nt`.idregdet = rd.idregdet
                                  LEFT JOIN `kuitansi` k
                                  ON k.nokuitansi = `nt`.nokuitansi
                                  LEFT JOIN `kartustok`
                                  ON `kartustok`.noref = `nt`.nonota
                                  WHERE
                                    ((`kartustok`.`noref` = `nt`.`nonota`)
                                    AND (`nt`.`idregdet` = `rd`.`idregdet`)
                                    AND k.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                    ))))");
				
		$this->db->where("nota.idbagian = 11");
		$this->db->where("notadet.nonota IS NOT NULL");
		$this->db->where("registrasidet.userbatal IS NULL");
		$this->db->where("reservasi.idstposisipasien = 6");
		$this->db->where("kuitansi.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'");
		$this->db->groupby("registrasi.noreg");
		$this->db->order_by("registrasidet.idregdet DESC");
        if($query !=""){
			$fields2 = '["registrasi.noreg","pasien.nmpasien","jkelamin.kdjnskelamin","bagian.nmbagian"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
		
        $q = $this->db->get();
		$ttl = $q->num_rows();
		$data = $q->result();
        if($ttl>0 && is_null($data[0]->noreg)){
            $ttl=0;
        }
        return $ttl;
    }
	
	function getDataRegistrasi(){
        $cek                  = $this->input->post("cek");
        $noreg                  = $this->input->post("noreg");
		
		if($cek){
			if($cek == 'RJ'){
				$q = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg, 'SUBSTRING(noreg,1,2)'=>$cek));
			} elseif($cek == 'RI'){
				$q = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg, 'SUBSTRING(noreg,1,2)'=>$cek));
			} elseif($cek == 'UG'){
				$q = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg, 'SUBSTRING(noreg,1,2)'=>$cek));
			}
        } else {
			$q = $this->db->getwhere('v_registrasi',array('noreg'=>$noreg));
		}
		$reg = $q->row_array();
		echo json_encode($reg);
    }
	
	function cekRegistrasi(){
        $norm = $this->input->post("norm");
        $jpel = $this->input->post("jpel");
		
		$this->db->select("rd.idbagian");
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
		$this->db->where("p.norm", str_pad($norm, 10, "0", STR_PAD_LEFT));
		$this->db->where("nmjnspelayanan", $jpel);
		$this->db->where("tglreg", date('Y-m-d'));
		$q = $this->db->get();
		$ret['validasi'] = $q->num_rows();
		$ret['arr'] = array();
		foreach($q->result() as $val){
			array_push($ret['arr'], $val->idbagian);
		}
		//var_dump($ret['arr']);
		echo json_encode($ret);
	}

	function cekMaksPel(){
        $norm = str_pad($this->input->post("norm"), 10, "0", STR_PAD_LEFT);
        $kditem = $this->input->post("kditem");

        $query = $this->db->query("select setmakspel.*, pelayanan.nmpelayanan from setmakspel 
        			left join pelayanan
        			on pelayanan.kdpelayanan = setmakspel.kdpelayanan
        			where setmakspel.kdpelayanan = '".$kditem."'");
        $row_setmakspel = $query->row();
        if(!empty($row_setmakspel)){
        	$maksimal = $row_setmakspel->maksimal;
        	$nmpelayanan = $row_setmakspel->nmpelayanan;
        }else{
        	$maksimal = 0;
        	$nmpelayanan = '';
        }


        $query_cek_trx = $this->db->query("SELECT
						      count(ntd.kditem) AS jmlpelayanan
						FROM
						  (`nota` nt)
						LEFT JOIN `registrasidet` rd
						ON `nt`.`idregdet` = `rd`.`idregdet`
						LEFT JOIN `registrasi` r
						ON `r`.`noreg` = `rd`.`noreg`
						LEFT JOIN `pasien` p
						ON `p`.`norm` = `r`.`norm`
						LEFT JOIN `notadet` ntd
						ON `ntd`.`nonota` = `nt`.`nonota`
						JOIN kuitansi kui
						ON kui.nokuitansi = nt.nokuitansi
						WHERE
						  `userbatal` IS NULL
						  AND `p`.`norm` = '".$norm."'
						  AND ntd.kditem = '".$kditem."'
						GROUP BY
						  ntd.kditem");
        $row_cek_trx = $query_cek_trx->row();
         if(!empty($row_cek_trx)){
        	$jmlpelayanan = $row_cek_trx->jmlpelayanan;
        }else{
        	$jmlpelayanan = 0;
        }
      
        if($jmlpelayanan > 0){
        	if($jmlpelayanan == $maksimal){
        		$validasi = 1; // true
        	}else{
        		$validasi = 0;
        	}
        }
      	
		$ret['validasi'] = $validasi;
		$ret['nmpelayanan'] = $nmpelayanan;
		
		//var_dump($ret['arr']);
		echo json_encode($ret);
	}
	
	function get_vregistrasipaspulang(){
		$tglawal = '2015-03-01';
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cek                    = $this->input->post("cek");//
        $gnoreg                 = $this->input->post("gnoreg");//
        $groupby                = $this->input->post("groupby");//
        $khususri               = $this->input->post("khususri");//
        $nokuitansi				= $this->input->post("nokuitansi");//
        $onoreg              	= $this->input->post("onoreg");//
		$ctransfar				= $this->input->post("ctransfar");
      
		$this->db->select("r.noreg AS noreg
						 , trim(LEADING 0 FROM p.norm) AS norm
						 , p.nmpasien AS nmpasien
						, jkel.nmjnskelamin AS nmjnskelamin
						, jkel.kdjnskelamin AS kdjnskelamin
						 , kui.atasnama AS atasnama
						 , rd.idbagian AS idbagian
						 , bag.nmbagian AS nmbagian
						 , d.nmdoktergelar AS nmdoktergelar
						 , nt.nonota AS nonota
						 , nt.nona AS nona
						 , kui.nokuitansi AS nokuitansi
						 , kui.total AS total
						 , nt.diskon AS diskonr
						 , nt.uangr AS uangr
						 , sp.idstpelayanan AS idstpelayanan
						 , kui.tglkuitansi AS tglkuitansi
						 , kui.jamkuitansi AS jamkuitansi
						 , nt.catatan AS catatannota
						 , na.catatandiskon AS catatands
						 , p.tgllahir AS tgllahirp
						 , p.idjnskelamin AS idjnskelamin
						 , kmr.nmkamar AS nmkamar
						 , bed.nmbed AS nmbed
						 , kt.nmklstarif AS nmklstarif
						 , pospas.nmstposisipasien AS nmstposisipasien
						 , rd.tglkeluar AS tglkeluar
						 , rd.jamkeluar AS jamkeluar
						 , na.stplafond AS stplafondna
						 , pnj.nmpenjamin AS nmpenjamin
						 , (
						   SELECT sum(kui2.total) AS deposit
						   FROM
							 (kuitansi kui2
						   JOIN registrasidet rd2)
						   WHERE
							 ((kui2.idregdet = rd2.idregdet)
							 AND (kui2.idstkuitansi = 1)
							 AND (rd2.noreg = r.noreg))) AS deposit
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg AND rd.idregdet = (SELECT rd2.idregdet FROM registrasidet rd2 WHERE rd2.noreg = r.noreg ORDER BY rd2.idregdet desc LIMIT 1 )', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
        $this->db->where('userbatal', null);
		if($ctransfar)$this->db->where('kui.tglkuitansi BETWEEN ', "'". $this->input->post("tglawal") ."' AND '". $this->input->post("tglakhir") ."'", false);
		
		//$this->db->order_by('rd.idregdet DESC');
		//if($groupby)$this->db->groupby($groupby);
		
        if($query !=""){
			$fields2 = '["r.noreg","p.nmpasien","jkel.kdjnskelamin","bag.nmbagian","p.tgllahir"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
		
		if($cek == 'RI'){			
			$this->db->where('r.idjnspelayanan', 2);
        }		
        if($gnoreg != '')$this->db->groupby('r.noreg', $gnoreg);
		
        if($nokuitansi == 1){
			$this->db->where('rd.tglkeluar IS NOT NULL', null);
			$this->db->where('kui.nokuitansi IS NOT NULL', null);
		}
        if($onoreg != '')$this->db->order_by('r.noreg desc');
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow2();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0 && !is_null($data[0]->noreg)){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow2(){
		$tglawal = '2015-03-01';
		
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cek                    = $this->input->post("cek");//
        $gnoreg                 = $this->input->post("gnoreg");//
        $groupby                = $this->input->post("groupby");//
        $khususri               = $this->input->post("khususri");//
        $nokuitansi				= $this->input->post("nokuitansi");//
        $onoreg              	= $this->input->post("onoreg");//
		$ctransfar				= $this->input->post("ctransfar");
      
		$this->db->select("r.noreg AS noreg
						 , trim(LEADING 0 FROM p.norm) AS norm
						 , p.nmpasien AS nmpasien
						, jkel.nmjnskelamin AS nmjnskelamin
						, jkel.kdjnskelamin AS kdjnskelamin
						 , kui.atasnama AS atasnama
						 , rd.idbagian AS idbagian
						 , bag.nmbagian AS nmbagian
						 , d.nmdoktergelar AS nmdoktergelar
						 , nt.nonota AS nonota
						 , nt.nona AS nona
						 , kui.nokuitansi AS nokuitansi
						 , kui.total AS total
						 , nt.diskon AS diskonr
						 , nt.uangr AS uangr
						 , sp.idstpelayanan AS idstpelayanan
						 , kui.tglkuitansi AS tglkuitansi
						 , kui.jamkuitansi AS jamkuitansi
						 , nt.catatan AS catatannota
						 , na.catatandiskon AS catatands
						 , p.tgllahir AS tgllahirp
						 , p.idjnskelamin AS idjnskelamin
						 , kmr.nmkamar AS nmkamar
						 , bed.nmbed AS nmbed
						 , kt.nmklstarif AS nmklstarif
						 , pospas.nmstposisipasien AS nmstposisipasien
						 , rd.tglkeluar AS tglkeluar
						 , rd.jamkeluar AS jamkeluar
						 , na.stplafond AS stplafondna
						 , pnj.nmpenjamin AS nmpenjamin
						 , (
						   SELECT sum(kui2.total) AS deposit
						   FROM
							 (kuitansi kui2
						   JOIN registrasidet rd2)
						   WHERE
							 ((kui2.idregdet = rd2.idregdet)
							 AND (kui2.idstkuitansi = 1)
							 AND (rd2.noreg = r.noreg))) AS deposit
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg AND rd.idregdet = (SELECT rd2.idregdet FROM registrasidet rd2 WHERE rd2.noreg = r.noreg ORDER BY rd2.idregdet desc LIMIT 1 )', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
        $this->db->where('userbatal', null);
		if($ctransfar)$this->db->where('kui.tglkuitansi BETWEEN ', "'". $this->input->post("tglawal") ."' AND '". $this->input->post("tglakhir") ."'", false);
		
		//$this->db->order_by('rd.idregdet DESC');
		//if($groupby)$this->db->groupby($groupby);
		
        if($query !=""){
			$fields2 = '["r.noreg","p.nmpasien","jkel.kdjnskelamin","bag.nmbagian","p.tgllahir"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
		
		if($cek == 'RI'){			
			$this->db->where('r.idjnspelayanan', 2);
        }		
        if($gnoreg != '')$this->db->groupby('r.noreg', $gnoreg);
		
        if($nokuitansi == 1){
			$this->db->where('rd.tglkeluar IS NOT NULL', null);
			$this->db->where('kui.nokuitansi IS NOT NULL', null);
		}
        if($onoreg != '')$this->db->order_by('r.noreg desc');		
		
        $q = $this->db->get();
		$ttl = $q->num_rows();
		$data = $q->result();
        if($ttl>0 && is_null($data[0]->noreg)){
            $ttl=0;
        }
        return $ttl;
    }
	
	
	function get_vregugd(){
        $cek                    = $this->input->post("cek");
        $groupby                = $this->input->post("groupby");
        $nokuitansi				= $this->input->post("nokuitansi");
        $norm                   = $this->input->post("norm");
      
		$this->db->select("
			r.noreg AS noreg,
			r.keluhan AS keluhan,
			r.nmkerabat AS nmkerabat,
			r.notelpkerabat AS notelpkerabat,
			r.catatan AS catatanr,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			p.alamat AS alamatp,
			p.nmibu AS nmibu,
			p.alergi AS alergi,
			p.tptlahir AS tptlahirp,
			p.noidentitas AS noidentitas,
			p.tgllahir AS tgllahirp,
			p.notelp AS notelpp,
			p.nohp AS nohpp,
			p.tgldaftar AS tgldaftar,
			p.negara AS negara,
			rd.idregdet AS idregdet,
			rd.tglreg AS tglreg,
			rd.jamreg AS jamreg,
			rd.tglmasuk AS tglmasuk,
			rd.jammasuk AS jammasuk,
			rd.tglkeluar AS tglkeluar,
			rd.jamkeluar AS jamkeluar,
			rd.catatankeluar AS catatankeluar,
			rd.tglrencanakeluar AS tglrencanakeluar,
			rd.catatanrencanakeluar AS catatanrencanakeluar,
			rd.umurtahun AS umurtahun,
			rd.umurbulan AS umurbulan,
			rd.umurhari AS umurhari,
			rd.userbatal AS userbatal,
			rd.nmdokterkirim AS nmdokterkirim,
			rd.userinput AS userinput,
			rd.idstkeluar AS idstkeluar,
			rd.idcarakeluar AS idcarakeluar,
			r.idpenjamin AS idpenjamin,
			r.idjnspelayanan AS idjnspelayanan,
			r.idstpasien AS idstpasien,
			r.idhubkeluarga AS idhubkeluarga,
			p.idjnskelamin AS idjnskelamin,
			rd.idcaradatang AS idcaradatang,
			rd.idbagian AS idbagian,
			rd.idbagiankirim AS idbagiankirim,
			rd.idkamar AS idkamar,
			rd.iddokter AS iddokter,
			rd.iddokterkirim AS iddokterkirim,
			rd.idklsrawat AS idklsrawat,
			rd.idklstarif AS idklstarif,
			rd.idshift AS idshift,
			rd.idbed AS idbed,
			bed.idstbed AS idstbed,
			bed.nmbed AS nmbed,
			jkel.nmjnskelamin AS nmjnskelamin,
			jkel.kdjnskelamin AS kdjnskelamin,
			jpel.nmjnspelayanan AS nmjnspelayanan,
			dae.nmdaerah AS nmdaerah,
			sp.idstpelayanan AS idstpelayanan,
			sp.nmstpelayanan AS nmstpelayanan,
			pnj.nmpenjamin AS nmpenjamin,
			kr.nmklsrawat AS nmklsrawat,
			bag.nmbagian AS nmbagian,
			bagkrm.nmbagian AS nmbagiankirim,
			kmr.nmkamar AS nmkamar,
			kmr.kdkamar AS kdkamar,
			nt.nonota AS nonota,
			nt.nona AS nona,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.catatandiskon AS catatand,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			nt.idsttransaksi AS idsttransaksi,
			nt.idregdettransfer AS idregdettransfer,
			nt.stplafond AS stplafond,
			na.stplafond AS stplafondna,
			na.catatandiskon AS catatands,
			d.nmdokter AS nmdokter,
			d.nmdoktergelar AS nmdoktergelar,
			dkrm.nmdoktergelar AS nmdokterkirimdlm,
			res.noantrian AS noantrian,
			cd.nmcaradatang AS nmcaradatang,
			pospas.nmstposisipasien AS nmstposisipasien,
			pospas.idstposisipasien AS idstposisipasien,
			kui.nokuitansi AS nokuitansi,
			kui.atasnama AS atasnama,
			kui.total AS total,
			kui.nokasir AS nokasir,
			kui.tglkuitansi AS tglkuitansi,
			kui.jamkuitansi AS jamkuitansi,
			sbt.kdsbtnm AS kdsbtnm,
			kt.nmklstarif AS nmklstarif,
			stkawin.nmstkawin AS nmstkawin,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit,
			daerah.nmdaerah AS kelurahan,
			kec.nmdaerah AS kec,
			kota.nmdaerah AS kota,
			agama.nmagama AS nmagama,
			goldarah.nmgoldarah AS nmgoldarah,
			pendidikan.nmpendidikan AS nmpendidikan,
			pekerjaan.nmpekerjaan AS nmpekerjaan,
			sukubangsa.nmsukubangsa AS nmsukubangsa
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg AND rd.idregdet = (SELECT rd2.idregdet FROM registrasidet rd2 WHERE rd2.noreg = r.noreg ORDER BY rd2.idregdet desc LIMIT 1 )', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
		if($groupby)$this->db->groupby($groupby);
		
		if($cek){
			if($cek == 'RJ'){
				$this->db->where('r.idjnspelayanan', 1);
			} elseif($cek == 'RI'){
				$this->db->where('r.idjnspelayanan', 2);
			} elseif($cek == 'UG'){
				$this->db->where('r.idjnspelayanan', 3);
			} else {
				$this->db->where('p.norm', str_pad($norm, 10, "0", STR_PAD_LEFT));
			}
        }
        if($nokuitansi == 1){
			$this->db->where('rd.tglkeluar IS NOT NULL', null);
			$this->db->where('kui.nokuitansi IS NOT NULL', null);
			$this->db->where('kui.idstkuitansi', 1);
		} elseif($nokuitansi == 2){
			$this->db->where('rd.tglkeluar IS NULL', null);
			$this->db->where('kui.nokuitansi IS NULL', null);
			$this->db->where('kui.idstkuitansi', 2);
		}
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		/* var_dump($data);
		exit; */
		if($ttl>0){ //&& !is_null($data[0]->noreg)){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	
	function get_vregistrasi_rj(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cek                    = $this->input->post("cek");
		
        $ctglreg                = $this->input->post("ctglreg");
        $groupbyq				= $this->input->post("groupbyq");
      
		$this->db->select("
			r.noreg AS noreg,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			kui.atasnama AS atasnama,
			rd.idbagian AS idbagian,
			bag.nmbagian AS nmbagian,
			d.nmdoktergelar AS nmdoktergelar,
			nt.nonota AS nonota,
			kui.nokuitansi AS nokuitansi,
			kui.total AS total,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			sp.idstpelayanan AS idstpelayanan,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.catatandiskon AS catatand,
			p.tgllahir AS tgllahirp,
			p.idjnskelamin AS idjnskelamin,
			jkel.kdjnskelamin AS kdjnskelamin,
			nt.stplafond AS stplafond,
			pnj.nmpenjamin AS nmpenjamin,
			rd.idregdet AS idregdet,
			rd.cttdiagnosa AS cttdiagnosa,
			p.foto_pasien as foto_pasien
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg AND rd.idregdet = (SELECT rd2.idregdet FROM registrasidet rd2 WHERE rd2.noreg = r.noreg ORDER BY rd2.idregdet desc LIMIT 1 )', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
        $this->db->where('userbatal', null);
	//	$this->db->order_by('rd.idregdet DESC');
		$this->db->order_by('rd.tglreg, rd.jamreg desc');
		
		if($groupbyq)$this->db->groupby('rd.tglreg, rd.jamreg desc');
        if($query !=""){
			$fields2 = '["r.noreg","p.nmpasien","jkel.kdjnskelamin","bag.nmbagian","p.tgllahir"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
		
		if($cek){
			if($cek == 'RJ'){
				$this->db->where('r.idjnspelayanan', 1);
			}
        }		
		if($ctglreg != '')$this->db->where('rd.tglreg', $ctglreg);
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_rj();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
		if($ttl>0 && !is_null($data[0]->noreg)){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_rj(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $cek                    = $this->input->post("cek");
		
        $ctglreg                = $this->input->post("ctglreg");
        $groupbyq				= $this->input->post("groupbyq");
      
		$this->db->select("
			r.noreg AS noreg,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			kui.atasnama AS atasnama,
			rd.idbagian AS idbagian,
			bag.nmbagian AS nmbagian,
			d.nmdoktergelar AS nmdoktergelar,
			nt.nonota AS nonota,
			kui.nokuitansi AS nokuitansi,
			kui.total AS total,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			sp.idstpelayanan AS idstpelayanan,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.catatandiskon AS catatand,
			p.tgllahir AS tgllahirp,
			p.idjnskelamin AS idjnskelamin,
			jkel.kdjnskelamin AS kdjnskelamin,
			nt.stplafond AS stplafond,
			pnj.nmpenjamin AS nmpenjamin,
			rd.idregdet AS idregdet,
			rd.cttdiagnosa AS cttdiagnosa,
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg AND rd.idregdet = (SELECT rd2.idregdet FROM registrasidet rd2 WHERE rd2.noreg = r.noreg ORDER BY rd2.idregdet desc LIMIT 1 )', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
        $this->db->where('userbatal', null);
        $this->db->order_by('rd.tglreg, rd.jamreg desc');
		//$this->db->order_by('rd.idregdet DESC');
		if($groupbyq)$this->db->groupby('rd.tglreg, rd.jamreg desc');
        if($query !=""){
			$fields2 = '["r.noreg","p.nmpasien","jkel.kdjnskelamin","bag.nmbagian","p.tgllahir"]';
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields2);
            $b=explode(',', $r);
            $c=count($b);
			$orlike = '(';
            for($i=0;$i<$c;$i++){
				$orlike .= $b[$i] .' LIKE "%'.$query.'%" OR ';
            }
			$len = strlen($orlike);
			$orlike = substr($orlike, 0, ($len-3));
			$orlike .= ')';
			$this->db->where($orlike);
        }
		
		if($cek){
			if($cek == 'RJ'){
				$this->db->where('r.idjnspelayanan', 1);
			}
        }		
		if($ctglreg != '')$this->db->where('rd.tglreg', $ctglreg);
		
        $q = $this->db->get();
		$ttl = $q->num_rows();
		$data = $q->result();
        if($ttl>0 && is_null($data[0]->noreg)){
            $ttl=0;
        }
        return $ttl;
    }
	
}
