<?php 
class Laporan_toi_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
			$this->load->library('rhlib');
		}
	
	function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=1;
        }
        return $max;
    }
	
	function get_date_server() {
		$data['date'] = date('d-m-Y');
		echo json_encode($data);
    }
	
	function get_result() {
		if ($_POST['isgetresult']==1) {
			$query  = $this->db->query("CALL sp_getTOIresult (?,?)",array($_POST['tglawal'],$_POST['tglakhir']));
		} else {
			$query  = $this->db->query("SELECT * FROM v_toidet WHERE idtoi='".$_POST['idtoi']."'");
		}
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
		
		$ttl = count($data); 
        
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function simpan_hasil(){
		$dataArray = array(
			 'idtoi'=> $this->autoNumber('idtoi','toi'),
             'daritgl'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglawal']))),
             'sampaitgl'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglakhir']))),
			 'jmltempattidur'=> $this->rhlib->retValOrNull($_POST['jmltempattidur']),
			 'jmlhariperawatan'=> $this->rhlib->retValOrNull($_POST['jmlhariperawatan']),
             'jmlpasien'=> $this->rhlib->retValOrNull($_POST['jmlpasien']),
			 'jmlpasienhidup'=> $this->rhlib->retValOrNull($_POST['jmlpasienhidup']),
			 'jmlpasienmati'=> $this->rhlib->retValOrNull($_POST['jmlpasienmati']),
			 'hasil'=> $this->rhlib->retValOrNull($_POST['hasil']),
			 
			 'catatan'=> $this->rhlib->retValOrNull($_POST['catatan']),
             'userid'=> $this->rhlib->retValOrNull($_POST['userid']),
			 'tglbuat'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglproses']))),
        );
		$ret = $this->rhlib->insertRecord('toi',$dataArray);
		if ($ret) {
			if ($_POST['dataObject'] != "[]") {
				$this->simpan_hasil_detail($dataArray['idtoi']);
			}
		}
        echo json_encode($ret);
    }
	
	function simpan_hasil_detail($idtoi){
		$dataobject = json_decode($_POST['dataObject']);
		foreach ($dataobject as $arrayitems) {
			$datainsert = array(
                'idtoi'=>$idtoi,
                'idbagian'=>$arrayitems->idbagian,
                'jmltempattidur'=>$arrayitems->jmltempattidur,
                'jmlhariperawatan'=>$arrayitems->jmlhariperawatan,
                'jmlpasien'=>$arrayitems->jmlpasien,
                'jmlpasienhidup'=>$arrayitems->jmlpasienhidup,
				'jmlpasienmati'=>$arrayitems->jmlpasienmati,
				'hasil'=>$arrayitems->hasil,
            );
			$ret = $this->db->insert('toidet',$datainsert);
		}
		if ($ret) {
			return true;
		} else {
			return false;
		}
	}
	
	function delete_history(){     
		$where['idtoi'] = $_POST['idtoi'];
		$del = $this->rhlib->deleteRecord('toi',$where);
        return $del;
    }
	
	function get_history(){ //ISTRA
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("v_toi");
 
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(10,0);
        }
        
        $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->db->query("SELECT * FROM v_toi")->num_rows(); 
        
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

}
