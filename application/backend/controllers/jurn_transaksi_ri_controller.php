<?php
/*
  Fungsi Jurnal khusus transaksi pasien (Rawat Inap)
*/
class Jurn_transaksi_ri_Controller extends Controller {
  public function __construct()
  {
    parent::Controller();
    $this->load->library('session');
    $this->load->library('rhlib');
  }
  
  /* =========================================== START JURNAL DEPOSIT RI ============================= */
  function posting_jurnal_supplier_beli(){

    $nopo  = $this->input->post("nopo_posting");
    $tglpo_posting  = $this->input->post("tglpo_posting");
    $nmsupplier  = $this->input->post("nmsupplier");
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();
    $tahun = date('Y', strtotime($tglpo_posting));

    if(empty($nopo) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nopo dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglpo_posting,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Pembelian Pada: '.$nmsupplier,
      'noreff' => $nopo,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => 20, // 20 = pembelian obat supplier (hutang)
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        $ins_jdet = array(
          'kdjurnal' => $kdjurnal,
          'tahun' => $tahun,
          'idakun' => $item_jdet[0],  
          'noreff' => $item_jdet[3],
          'debit' => $item_jdet[4],
          'kredit' => $item_jdet[5],
        );
        
        if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  

  function get_transaksi_ri_deposit(){
    $tglkuitansi     = $this->input->post("tglkuitansi");
    if(empty($tglkuitansi)) $tglkuitansi = date('Y-m-d');

    $this->db->where('tglkuitansi', $tglkuitansi);
    $this->db->select("*");
    $this->db->from("v_jurn_check_trans_depositri");
    $q = $this->db->get();  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_ri_depositdet(){
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $this->db->where('nokuitansi', $nokuitansi);
    $this->db->select("*");
    $this->db->from("v_jurn_check_trans_depositridet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_jtransaksi_supplier_beli_jurnaling(){
    $akun_hutang = $this->db->get_where('akun', array('kdakun' => '21200'))->row_array();
    $akun_diskon = $this->db->get_where('akun', array('kdakun' => '51500'))->row_array();
    $akun_ppn = $this->db->get_where('akun', array('kdakun' => '51700'))->row_array(); 
    
    $nopo     = $this->input->post("nopo");
    if(empty($nopo)) $nopo = '0';

    $this->db->select("
      nopo
     , sum(totbeli) AS totbeli
     , sum(totbeli_diskon) AS totbeli_diskon
     , sum(totbeli + totbonus) AS totbeli_bonus
     , sum(totdiskon) AS totdiskon
     , sum(totbonus) AS totbonus
     , sum(totdiskon + totbonus) AS bonus_diskon
     , sum(beban_ppn) AS beban_ppn
     , sum(totbeli_diskon_ppn) AS totbeli_diskon_ppn
     , sum(totbeli + totbonus + beban_ppn) AS totbeli_bonus_ppn
     , idakunbeli
     , kdakunbeli
     , nmakunbeli
     , kdsupplier
    ");
    $this->db->from("v_jurn_transsupplier_podet");
    $this->db->where("nopo", $nopo);
    $this->db->group_by('kdakunbeli');
    $data = $this->db->get()->result_array();

    $total_hutang = 0;
    $total_diskon = 0;
    $total_ppn = 0;
    $breakdown_jurnal = array();    
    if(!empty($data))
    {
      foreach($data as $idx => $dt)
      {
        //$total_hutang += $dt['totbeli_diskon_ppn'];
        $total_hutang += $dt['totbeli_diskon'];
        if($dt['bonus_diskon'] > 0) $total_diskon += $dt['bonus_diskon'];
        if($dt['beban_ppn'] > 0) $total_ppn += $dt['beban_ppn'];

        $breakdown_jurnal[] = array(
          'idakun' => $dt['idakunbeli'], 
          'kdakun' => $dt['kdakunbeli'], 
          'nmakun' => $dt['nmakunbeli'], 
          'noreff' => '', 
          'debit' => $dt['totbeli_bonus_ppn'], 
          'kredit'=> 0
        );
      }

      if($total_diskon > 0){
        //tambah akun diskon
        $breakdown_jurnal[] = array(
          'idakun' => $akun_diskon['idakun'], 
          'kdakun' => $akun_diskon['kdakun'], 
          'nmakun' => $akun_diskon['nmakun'], 
          'noreff' => '', 
          'debit' => 0, 
          'kredit'=> $total_diskon
        );        
      }

      //tambah akun hutang
      $breakdown_jurnal[] = array(
        'idakun' => $akun_hutang['idakun'], 
        'kdakun' => $akun_hutang['kdakun'], 
        'nmakun' => $akun_hutang['nmakun'], 
        'noreff' => $dt['kdsupplier'], 
        'debit' => 0, 
        'kredit'=> $total_hutang
      );

      if($total_ppn > 0){
        //tambah akun ppn
        $breakdown_jurnal[] = array(
          'idakun' => $akun_ppn['idakun'], 
          'kdakun' => $akun_ppn['kdakun'], 
          'nmakun' => $akun_ppn['nmakun'], 
          'noreff' => '', 
          'debit' => 0, 
          'kredit'=> $total_ppn
        );        
      }

    }

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);

    echo json_encode($build_array);
  }
  /* ============================================= END JURNAL SUPPLIER PEMBELIAN ============================= */

  function get_nojurnal_khusus(){
    //$q = "SELECT getOtoNojurnal(now()) as nm;";
        $q = "SELECT getOtoNoJurnalKhusus(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
  }

}
