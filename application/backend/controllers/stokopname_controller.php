<?php

class Stokopname_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_stokopname(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
        $userid            		= $this->input->post("userid");
      
        $this->db->select("*");
		$this->db->from("v_so");				
		$this->db->order_by('noso DESC');       
        
       /*  if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        } */
		
		if($userid){		
			$this->db->where("v_so.idbagian IN (SELECT idbagian from penggunabagian where userid='".$userid."')");
        }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$arrnm = explode(' ', $name);
			foreach($arrnm AS $vall){
				$this->db->like($id, $vall);
			}
		}	
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($key, $userid){
      
        $this->db->select("*");
		$this->db->from("v_so");     
        
        if($userid){		
			$this->db->where("v_so.idbagian IN (SELECT idbagian from penggunabagian where userid='".$userid."')");
        }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$arrnm = explode(' ', $name);
			foreach($arrnm AS $vall){
				$this->db->like($id, $vall);
			}
		}	
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_stokopnamedet(){		
        $key 				= $this->input->post("key");
		$noso 				= $_POST["noso"];
      
        $this->db->select("*");
        $this->db->from("v_sodet");
		$this->db->order_by('v_sodet.kdbrg');
		
		$where = array();
        $where['noso']=$noso;
        $this->db->where($where); 		       
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all('v_sodet');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function delete_stokopname(){     
		$where['noso'] = $_POST['noso'];
		$del = $this->rhlib->deleteRecord('so',$where);
		
		$where['noso'] = $_POST['noso'];
		$del = $this->rhlib->deleteRecord('sodet',$where);
        return $del;
    }
			
	function insert_stokopname(){
		$dataArray = $this->getFieldsAndValues();
		$this->rhlib->insertRecord('so',$dataArray);
		$ret["noso"]=$dataArray['noso'];
        echo json_encode($ret);
    }

	function update_stokopname(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('noso', $_POST['noso']);
		$this->db->update('so', $dataArray);
        $ret["noso"]=$_POST['noso']; 
        echo json_encode($ret);
    }
	
	function getFieldsAndValues(){
			$so   = $this->input->post("noso");
			$noso = $this->getNoSo();
		
		$dataArray = array(
			 'noso'			=> ($so) ? $so: $noso,
			 'tglso'		=> $_POST['tglso'],
			 'jamso'		=> $_POST['jamso'],
			 'userid'		=> $_POST['userid'],
             'idstso'		=> $_POST['idstso'],
             'idbagian'		=> $_POST['idbagian'],	
             'keterangan'	=> $_POST['keterangan'],	
		);
		return $dataArray;
	}
	
	function insert_stokopnamedet(){
		$dataArray = $this->getFieldsAndValuesdet();
		$ret = $this->rhlib->insertRecord('sodet',$dataArray);
        return $ret;
    }
		
	function update_jmlfisik(){
		//UPDATE
		$this->db->where('noso', $_POST['noso']);
		$this->db->where('kdbrg', $_POST['kdbrg']);
		$this->db->set('jmlfisik', $_POST['jmlfisik']);
		$this->db->update('sodet'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	} 
	
	function update_catatan(){
		//UPDATE
		$this->db->where('noso', $_POST['noso']);
		$this->db->where('kdbrg', $_POST['kdbrg']);
		$this->db->set('catatan', $_POST['catatan']);
		$this->db->update('sodet'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function getFieldsAndValuesdet(){		
		$dataArray = array(
			 'noso'			=> $_POST['noso'],
			 'kdbrg'		=> $_POST['kdbrg'],
             'jmlkomputer'	=> $_POST['jmlkomputer'],
             'jmlfisik'		=> NULL,
			 'catatan'		=> '-'	
		);
		return $dataArray;
	}
	
	function update_qty_so(){

		if($this->Update_DataSoDet($_POST['noso'])){
			$this->db->query("CALL SP_insorupd_brgbagian_dari_stokopname (?,?,?,?)",
			 array(
			 	 $_POST['idbagian'],
				 $_POST['tglso'],
				 $this->session->userdata['user_id'],
				 $_POST['noso']
				)
			);
			$this->update_qty_so_bagian();
			 return;
		}
		
		
       
    }
	
	function update_qty_so_bagian(){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_update_brgbagian_dari_stokopname (?,?,?)",
						 array(
						 	 $_POST['idbagian'],
							 $vale[0],
							 $vale[1],
						//	 $vale[2]
							));
		}		
        return;
    }


    function insorupd_data(){
        $this->db->trans_begin();
        if($_POST['noso'] != ""){
        	$noso = $_POST['noso'];
        	$this->Insert_DataSo($noso);         	

            $where['noso'] = $noso;
            $this->db->delete('sodet',$where);
            $det = $this->Insert_DataSoDet($noso);
        }else{
            $noso = $this->Insert_DataSo($_POST['noso']);
            $det = $this->Insert_DataSoDet($noso);
        }

        if($noso && $det)
        {
            $this->db->trans_commit();
            $ret["success"] = true;
            $ret["noso"]=$noso;
        }else{
            $this->db->trans_rollback();
            $ret["success"] = false;
        }
        echo json_encode($ret);
    }

    function Insert_DataSo($noso){
    	if($noso != ""){
			$dataArray = array(
				 'noso'			=> $noso,
				 'tglso'		=> $_POST['tglso'],
				 'jamso'		=> $_POST['jamso'],
				 'userid'		=> $this->session->userdata['user_id'],
	             'idstso'		=> $_POST['idstso'],
	             'idbagian'		=> $_POST['idbagian'],	
	             'keterangan'	=> $_POST['keterangan'],	
			);
	        $this->db->where('noso', $noso);
	        $this->db->update('so',$dataArray);
	        $dnso = $noso;
        }else{
        	$so   = $this->input->post("noso");
			$noso = $this->getNoSo();
			$dnso = ($so) ? $so: $noso;
			$dataArray = array(
				 'noso'			=> $dnso,
				 'tglso'		=> $_POST['tglso'],
				 'jamso'		=> $_POST['jamso'],
				 'userid'		=> $this->session->userdata['user_id'],
	             'idstso'		=> $_POST['idstso'],
	             'idbagian'		=> $_POST['idbagian'],	
	             'keterangan'	=> $_POST['keterangan'],	
			);
	        $so = $this->db->insert('so',$dataArray);
        }
        // if($so){
            $ret=$dnso;
        // }else{
        //     $ret=false;
        // }
		
        return $ret;
    }

    function Insert_DataSoDet($noso){
    	$k=array('[',']', '"');
		$r= str_replace($k, '', $_POST['arrso']);
		$b = explode(',',$r);
        foreach($b AS $val){
        	$vale = explode('-', $val);        	
			$date = explode('_', $vale[5]);

        	$dataArrayDet = array(
				 'noso'			=> $noso,
				 'kdbrg'		=> $vale[0],
	             'jmlkomputer'	=> $vale[1],
	             'jmlfisik'		=> $vale[2],
				 'catatan'		=> $vale[6]	
			);
            $det =$this->db->insert('sodet',$dataArrayDet);
        }
        if($det){
            $ret=$dataArrayDet;
        }else{
            $ret=false;
        }
		
        return $ret;
    }

     function Update_DataSoDet($noso){
    	$k=array('[',']', '"');
		$r= str_replace($k, '', $_POST['arrso']);
		$b = explode(',',$r);
        foreach($b AS $val){
        	$vale = explode('-', $val);        	
			$date = explode('_', $vale[5]);

        	$dataArrayDet = array(
				 //'noso'			=> $noso,
				 //'kdbrg'		=> $vale[0],
	             'jmlkomputer'	=> $vale[1],
	             'jmlfisik'		=> $vale[2],
				 'catatan'		=> $vale[6]	
			);
			$this->db->where("kdbrg",$vale[0]);
			$this->db->where("noso",$noso);
            $det =$this->db->update('sodet',$dataArrayDet);
        }
        if($det){
            $ret=$dataArrayDet;
        }else{
            $ret=false;
        }
		
        return $ret;
    }

	function getNoSo(){
		$q = "SELECT getOtoNostokopname() as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
}
