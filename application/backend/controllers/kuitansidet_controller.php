<?php

class Kuitansidet_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_kuitansidet(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $blank                  = $this->input->post("blank");
        $nokuitansi             = $this->input->post("nokuitansi");
      
        $this->db->select("*");
        $this->db->from("kuitansidet");
        $this->db->join("bank",
				"bank.idbank = kuitansidet.idbank", "left");
        $this->db->join("carabayar",
				"carabayar.idcarabayar = kuitansidet.idcarabayar", "left");
		if($blank == 1)$this->db->where("idkuitansidet", '');
		if($nokuitansi)$this->db->where("nokuitansi", $nokuitansi);
        else $this->db->where("idkuitansidet", '');
        
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
        $blank                  = $this->input->post("blank");
        $nokuitansi             = $this->input->post("nokuitansi");
      
        $this->db->select("*");
        $this->db->from("kuitansidet");
        $this->db->join("bank",
				"bank.idbank = kuitansidet.idbank", "left");
        $this->db->join("carabayar",
				"carabayar.idcarabayar = kuitansidet.idcarabayar", "left");
		if($blank == 1)$this->db->where("idkuitansidet", '');
		if($nokuitansi)$this->db->where("nokuitansi", $nokuitansi);
        else $this->db->where("idkuitansidet", '');
		
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
}
