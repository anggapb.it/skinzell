<?php

class Lappoperbrg_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_lappoperbrg(){
        $tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		//$kdbrg					= $_POST['kdbrg'];
		
        $this->db->select('*');
        $this->db->from('v_lappodet');
		
		if($tglakhir){
			$this->db->where('`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		//$where = array();
		//$where['v_lappoperbrgsubtotal.kdbrg']=$kdbrg;
		
		/* if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		} */
		
		//$this->db->where($where);
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all('v_lappodet');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_brgdipodet(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $this->input->post("key");
		//$kdbrg					= $this->input->post("kdbrg");		
        $tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		
        $this->db->select('po.tglpo ,podet.kdbrg, barang.nmbrg');
		$this->db->from('podet');
		$this->db->join("barang",
						"barang.kdbrg = podet.kdbrg","left");
		$this->db->join("po",
						"po.nopo = podet.nopo","left");
		$this->db->order_by('kdbrg');
		$this->db->group_by('kdbrg');
		
		if($tglakhir){
			$this->db->where('`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}
