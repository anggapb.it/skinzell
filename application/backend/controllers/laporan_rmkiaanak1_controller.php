<?php 
class Laporan_rmkiaanak1_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_laprmkiaanak1(){

		$tglawal = $this->input->post("tglawal");
        $tglakhir = $this->input->post("tglakhir");
		
		$q = "SELECT rd.idregdet
					 , n.nonota
					 , k.tglkuitansi
					 , r.noreg
					 , trim(LEADING '0' FROM `p`.`norm`) AS `norm`
					 , if(((SELECT min(registrasidet.idregdet) AS stp
							FROM
							  registrasidet
							LEFT JOIN registrasi
							ON registrasi.noreg = registrasidet.noreg
							LEFT JOIN nota
							ON nota.idregdet = registrasidet.idregdet
							LEFT JOIN kuitansi
							ON kuitansi.nokuitansi = nota.nokuitansi
							WHERE
							  registrasi.norm = p.norm
							  AND
							  registrasi.idjnspelayanan = '1'
							  AND
							  registrasidet.userbatal IS NULL
							  AND
							  kuitansi.idstkuitansi = 1
					   ) = rd.idregdet), 'Baru', 'Lama') AS stpasien
					 , p.nmpasien
					 , (SELECT if((registrasidet.noreg = registrasidet.noreg), '1', NULL)
						FROM
						  registrasidet
						LEFT JOIN nota
						ON nota.idregdet = registrasidet.idregdet
						LEFT JOIN kuitansi
						ON kuitansi.nokuitansi = nota.nokuitansi
						WHERE
						  registrasidet.idbagian = 4
						  AND registrasidet.noreg = rd.noreg
						  AND if((registrasidet.umurtahun = '0'), if(registrasidet.umurbulan = '0', registrasidet.umurhari > 1, registrasidet.umurbulan <= 3), NULL)
						  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
					   ) AS u29hr3bln

					 , (SELECT if((registrasidet.noreg = registrasidet.noreg), '1', NULL)
						FROM
						  registrasidet
						LEFT JOIN nota
						ON nota.idregdet = registrasidet.idregdet
						LEFT JOIN kuitansi
						ON kuitansi.nokuitansi = nota.nokuitansi
						WHERE
						  registrasidet.idbagian = 4
						  AND registrasidet.noreg = rd.noreg
						  AND if((registrasidet.umurtahun = '0'), registrasidet.umurbulan > 3
						  AND registrasidet.umurbulan <= 6, NULL)
						  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
					   ) AS u3bln6bln

					 , (SELECT if((registrasidet.noreg = registrasidet.noreg), '1', NULL)
						FROM
						  registrasidet
						LEFT JOIN nota
						ON nota.idregdet = registrasidet.idregdet
						LEFT JOIN kuitansi
						ON kuitansi.nokuitansi = nota.nokuitansi
						WHERE
						  registrasidet.idbagian = 4
						  AND registrasidet.noreg = rd.noreg
						  AND if((registrasidet.umurtahun = '0'), registrasidet.umurbulan > 6
						  AND registrasidet.umurbulan <= 9, NULL)
						  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
					   ) AS u6bln9bln

					 , (SELECT if((registrasidet.noreg = registrasidet.noreg), '1', NULL)
						FROM
						  registrasidet
						LEFT JOIN nota
						ON nota.idregdet = registrasidet.idregdet
						LEFT JOIN kuitansi
						ON kuitansi.nokuitansi = nota.nokuitansi
						WHERE
						  registrasidet.idbagian = 4
						  AND registrasidet.noreg = rd.noreg
						  AND if((registrasidet.umurtahun = '0'), registrasidet.umurbulan > 9
						  AND registrasidet.umurbulan <= 11, NULL)
						  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
					   ) AS u9bln11bln

					 , (SELECT group_concat(penyakit.nmpenyakit SEPARATOR ',<br>') AS nmpenyakiteng
						FROM
						  kodifikasidet
						LEFT JOIN kodifikasi
						ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
						LEFT JOIN registrasi
						ON registrasi.noreg = kodifikasi.noreg
						LEFT JOIN penyakit
						ON penyakit.idpenyakit = kodifikasidet.idpenyakit
						WHERE
						  registrasi.noreg = r.noreg
					   ) AS nmpenyakiteng
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , cdatang.nmcaradatang AS nmcaradatang
					 , r.idpenjamin
					 , pj.nmpenjamin


				FROM
				  registrasi r
				LEFT JOIN registrasidet rd
				ON r.noreg = rd.noreg
				LEFT JOIN caradatang cdatang
				ON cdatang.idcaradatang = rd.idcaradatang
				LEFT JOIN pasien p
				ON p.norm = r.norm
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = p.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN penjamin pj
				ON pj.idpenjamin = r.idpenjamin
				LEFT JOIN nota n
				ON n.idregdet = rd.idregdet
				LEFT JOIN kuitansi k
				ON k.nokuitansi = n.nokuitansi
				LEFT JOIN kodifikasi kd
				ON r.noreg = kd.noreg
				LEFT JOIN kodifikasidet kdt
				ON kdt.idkodifikasi = kd.idkodifikasi
				LEFT JOIN penyakit peny
				ON peny.idpenyakit = kdt.idpenyakit
				WHERE
				  r.idjnspelayanan = 1
				  AND
				  rd.userbatal IS NULL
				  AND
				  k.idstkuitansi = 1
				  AND
				  rd.idbagian = 4
				  AND
				  k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
				GROUP BY
				  r.noreg
				ORDER BY
				  r.noreg";
		$query = $this->db->query($q);
		
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
		
		$ttl = count($data);
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
        if($ttl>0){
            $build_array["data"]=$data;
        }
		
		echo json_encode($build_array);
	}

}
