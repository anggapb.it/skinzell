<?php

class Hartatetap_Controller extends Controller {
  public function __construct()
  {
    parent::Controller();
    $this->load->library('session');
    $this->load->library('lib_phpexcel');
  }
  

  /* ================================ HARTA TETAP TANPA PENYUSUTAN ================================*/
  function get_hartatetap(){
    $start                  = $this->input->post("start");
    $limit                  = $this->input->post("limit");
    $fields                 = $this->input->post("fields");
    $query                  = $this->input->post("query");
  
    $this->db->select("*");
    $this->db->from("v_hartatetap");
    
    if($fields!="" || $query !=""){
      $k=array('[',']','"');
      $r=str_replace($k, '', $fields);
      $b=explode(',', $r);
      $c=count($b);
      for($i=0;$i<$c;$i++){
          $d[$b[$i]]=$query;
      }
      $this->db->or_like($d, $query);
    }
            
    if ($start!=null){
      $this->db->limit($limit,$start);
    }else{
      $this->db->limit(18,0);
    }
    
    $q = $this->db->get();
    $data = array();
    if ($q->num_rows() > 0) {
      $data = $q->result();
    }

    $ttl = $this->numrow($fields, $query);
    $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

    if($ttl>0){
      $build_array["data"]=$data;
    }

    echo json_encode($build_array);
  }
  
  function numrow($fields, $query){
  
    $this->db->select("*");
    $this->db->from("v_hartatetap");        
    
    if($fields!="" || $query !=""){
        $k=array('[',']','"');
        $r=str_replace($k, '', $fields);
        $b=explode(',', $r);
        $c=count($b);
        for($i=0;$i<$c;$i++){
            $d[$b[$i]]=$query;
        }
        $this->db->or_like($d, $query);
    }
    
    $q = $this->db->get();
    
    return $q->num_rows();
  }
  
  function insert_hartatetap(){
    //insert data hartatetap
    $namaharta    = $this->input->post('namaharta');
    $idkelharta   = $this->input->post('idkelharta');
    $tglbeli      = $this->input->post('tglbeli');
    $hrgbeli      = $this->input->post('hrgbeli');
    $jml          = $this->input->post('jml');
    $totalbeli    = $this->input->post('totalbeli');
    $umurterpakai = $this->input->post('umurterpakai');
    $userid       = $this->input->post('userid');
    $kdjurnal     = $this->getKdjurnalUmum();
    $idhartatetap = false;
    $akun_harta = $this->db->get_where('v_hartatetap', array('idhartatetap' => $idhartatetap))->row_array();
    $akun_kas = $this->db->get_where('akun', array('kdakun' => '11100'))->row_array(); //kas kecil
    
    $data_harta = array(
      'idkelharta'   => $idkelharta,
      'jenisharta'   => 0, //0 = harta tidak menyusut
      'nmhartatetap' => $namaharta,
      'tglbeli'      => $tglbeli,
      'hrgbeli'      => $hrgbeli,
      'jml'          => $jml,
      'umurterpakai' => $umurterpakai,
    );

    $continue = true;
    $this->db->trans_start();
    $insert_harta = $this->db->insert('hartatetap', $data_harta);
    if($insert_harta !== false){
      $idhartatetap = $this->db->insert_id();
      $akun_harta = $this->db->get_where('v_hartatetap', array('idhartatetap' => $idhartatetap))->row_array();
    }else{
      $continue = false;
    }
    
    /*
    //tidak lagi dipakai. saat pembelian harta jurnal pembeliannya diinput manual
    //jurnal perpindahan dari kas ke harta 
    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 1, 
      'tgltransaksi' => $tglbeli,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Pembelian :'.$namaharta,
      'noreff' => '',
      'userid' => $userid,
      'nominal' => $totalbeli,
      'status_posting' => 1,
    );
    if( ! $this->db->insert('jurnal', $dataJurnal)) $continue = false;
  
    $ins_jdet_debit = array(
      'kdjurnal' => $kdjurnal,
      'tahun' => date('Y'),
      'idakun' => $akun_harta['idakunharta'],  
      'noreff' => '',
      'debit' => $totalbeli,
      'kredit' => 0,
    );  
    if( ! $this->db->insert('jurnaldet', $ins_jdet_debit) ) $continue = false;

    $ins_jdet_kredit = array(
      'kdjurnal' => $kdjurnal,
      'tahun' => date('Y'),
      'idakun' => $akun_kas['idakun'],  
      'noreff' => '',
      'debit' => 0,
      'kredit' => $totalbeli,
    );  
    if( ! $this->db->insert('jurnaldet', $ins_jdet_kredit) ) $continue = false;
    */

    if($continue){
      $this->db->trans_complete();
      echo json_encode(array('success' => true, 'msg' => 'sukses menyimpan data'));
      die;
    }else{
      $this->db->trans_rollback();
      echo json_encode(array('success' => false, 'msg' => 'gagal menyimpan data'));
      die;
    }

  }

  function update_hartatetap(){
    //edit data hartatetap
    $idhartatetap = $this->input->post('idhartatetap');
    $namaharta    = $this->input->post('namaharta');
    $idkelharta   = $this->input->post('idkelharta');
    $tglbeli      = $this->input->post('tglbeli');
    $hrgbeli      = $this->input->post('hrgbeli');
    $jml          = $this->input->post('jml');
    $totalbeli    = $this->input->post('totalbeli');
    $umurterpakai = $this->input->post('umurterpakai');
    $userid       = $this->input->post('userid');
    
    $data_harta = array(
      'idkelharta'   => $idkelharta,
      'jenisharta'   => 0, //0 = harta tidak menyusut
      'nmhartatetap' => $namaharta,
      'tglbeli'      => $tglbeli,
      'hrgbeli'      => $hrgbeli,
      'jml'          => $jml,
      'umurterpakai' => $umurterpakai,
    );

    $continue = true;
    $this->db->trans_start();
    $this->db->where('idhartatetap', $idhartatetap);
    $update_harta = $this->db->update('hartatetap', $data_harta);
    if($update_harta !== false){
      $akun_harta = $this->db->get_where('v_hartatetap', array('idhartatetap' => $idhartatetap))->row_array();
    }else{
      $continue = false;
    }
    
    if($continue){
      $this->db->trans_complete();
      echo json_encode(array('success' => true, 'msg' => 'sukses menyimpan data'));
      die;
    }else{
      $this->db->trans_rollback();
      echo json_encode(array('success' => false, 'msg' => 'gagal menyimpan data'));
      die;
    }

  }

  function delete_hartatetap(){
    //edit data hartatetap
    $idhartatetap = $this->input->post('idhartatetap');
    
    $continue = true;
    $this->db->trans_start();
    $delete_harta = $this->db->delete('hartatetap', array('idhartatetap' => $idhartatetap));
    if($delete_harta !== false){
      #$akun_harta = $this->db->get_where('v_hartatetap', array('idhartatetap' => $idhartatetap))->row_array();
    }else{
      $continue = false;
    }
    
    if($continue){
      $this->db->trans_complete();
      echo json_encode(array('success' => true, 'msg' => 'sukses menghapus data'));
      die;
    }else{
      $this->db->trans_rollback();
      echo json_encode(array('success' => false, 'msg' => 'gagal menghapus data'));
      die;
    }
  }

  /* ================================ HARTA TETAP DENGAN PENYUSUTAN ================================*/
  function get_hartatetapmenyusut(){
    $start                  = $this->input->post("start");
    $limit                  = $this->input->post("limit");
    $fields                 = $this->input->post("fields");
    $query                  = $this->input->post("query");
  
    $this->db->select("*");
    $this->db->from("v_hartatetap_menyusut");
    
    if($fields!="" || $query !=""){
      $k=array('[',']','"');
      $r=str_replace($k, '', $fields);
      $b=explode(',', $r);
      $c=count($b);
      for($i=0;$i<$c;$i++){
          $d[$b[$i]]=$query;
      }
      $this->db->or_like($d, $query);
    }
            
    if ($start!=null){
      $this->db->limit($limit,$start);
    }else{
      $this->db->limit(18,0);
    }
    
    $q = $this->db->get();
    $data = array();
    if ($q->num_rows() > 0) {
      $data = $q->result();
    }

    $ttl = $this->numrow_menyusut($fields, $query);
    $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

    if($ttl>0){
      $build_array["data"]=$data;
    }

    echo json_encode($build_array);
  }

  function numrow_menyusut($fields, $query){
  
    $this->db->select("*");
    $this->db->from("v_hartatetap_menyusut");        
    
    if($fields!="" || $query !=""){
        $k=array('[',']','"');
        $r=str_replace($k, '', $fields);
        $b=explode(',', $r);
        $c=count($b);
        for($i=0;$i<$c;$i++){
            $d[$b[$i]]=$query;
        }
        $this->db->or_like($d, $query);
    }
    
    $q = $this->db->get();
    
    return $q->num_rows();
  }

  function insert_hartatetapmenyusut(){
    //insert data hartatetap
    $namaharta            = $this->input->post('namaharta'); 
    $idkelharta           = $this->input->post('idkelharta');
    $tglbeli              = $this->input->post('tglbeli');
    $hrgbeli              = $this->input->post('hrgbeli'); 
    $jml                  = $this->input->post('jml'); 
    $residu               = $this->input->post('residu'); 
    $umurekonomis         = $this->input->post('umurekonomis'); 
    $totalbeli            = $this->input->post('totalbeli'); 
    $penyusutan           = $this->input->post('penyusutan'); 
    $totalpenyusutan      = $this->input->post('totalpenyusutan'); 
    $umurterpakai         = $this->input->post('umurterpakai'); 
    $akumulasipenyusutan  = $this->input->post('akumulasipenyusutan'); 
    $nilaiharta           = $this->input->post('nilaiharta');
    $userid               = $this->input->post('userid');
    $kdjurnal             = $this->getKdjurnalUmum();
    
    $dtkelharta                = $this->db->get_where('kelharta', array('idkelharta' => $idkelharta))->row_array();
    $akun_harta                = array();
    $akun_akumulasi_penyusutan = array();
    $akun_biaya_penyusutan     = array();
    $akun_kas                  = $this->db->get_where('akun', array('kdakun' => '11100'))->row_array(); //kas kecil

    if(!empty($dtkelharta)){
      $akun_harta                = $this->db->get_where('akun', array('kdakun' => $dtkelharta['akunharta']))->row_array();
      $akun_akumulasi_penyusutan = $this->db->get_where('akun', array('kdakun' => $dtkelharta['akunakumulasipenyusutan']))->row_array();
      $akun_biaya_penyusutan     = $this->db->get_where('akun', array('kdakun' => $dtkelharta['akunbiayapenyusutan']))->row_array();
    }
    
    $data_harta = array(
      'idkelharta'   => $idkelharta,
      'jenisharta'   => 1, //1 = harta tidak menyusut
      'nmhartatetap' => $namaharta,
      'tglbeli'      => $tglbeli,
      'hrgbeli'      => $hrgbeli,
      'jml'          => $jml,
      'residu'       => $residu,
      'umurekonomi'  => $umurekonomis,
      'umurterpakai' => $umurterpakai,
    );

    $continue = true;
    $this->db->trans_start();
    $insert_harta = $this->db->insert('hartatetap', $data_harta);
    if($insert_harta !== false){
      $idhartatetap = $this->db->insert_id();
    }else{
      $continue = false;
    }
    
    if($continue){
      $this->db->trans_complete();
      echo json_encode(array('success' => true, 'msg' => 'sukses menyimpan data'));
      die;
    }else{
      $this->db->trans_rollback();
      echo json_encode(array('success' => false, 'msg' => 'gagal menyimpan data'));
      die;
    }

    /*
    //tidak dibutuhkan lagi. karena jurnal pembeliannya diinput manual
    //jurnal perpindahan dari kas ke harta 
    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 1, 
      'tgltransaksi' => $tglbeli,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Pembelian :'.$namaharta,
      'noreff' => '',
      'userid' => $userid,
      'nominal' => $totalbeli,
      'status_posting' => 1,
    );
    if( ! $this->db->insert('jurnal', $dataJurnal)) $continue = false;
  
    $ins_jdet_debit = array(
      'kdjurnal' => $kdjurnal,
      'tahun' => date('Y'),
      'idakun' => $akun_harta['idakun'],  
      'noreff' => '',
      'debit' => $totalbeli,
      'kredit' => 0,
    );  
    if( ! $this->db->insert('jurnaldet', $ins_jdet_debit) ) $continue = false;

    $ins_jdet_kredit = array(
      'kdjurnal' => $kdjurnal,
      'tahun' => date('Y'),
      'idakun' => $akun_kas['idakun'],  
      'noreff' => '',
      'debit' => 0,
      'kredit' => $totalbeli,
    );  
    if( ! $this->db->insert('jurnaldet', $ins_jdet_kredit) ) $continue = false;

    if($continue){
      $this->db->trans_complete();
      echo json_encode(array('success' => true, 'msg' => 'sukses menyimpan data'));

       //jurnal penyusutan (jika ada)
      if($umurterpakai > 0 && $totalpenyusutan > 0)
      {
        $kdjurnal_penyusutan     = $this->getKdjurnalUmum();
        //jurnal perpindahan dari harta ke penyusutan 
        $dataJurnal = array(
          'kdjurnal' => $kdjurnal_penyusutan,
          'idjnsjurnal' => 1, 
          'tgltransaksi' => $tglbeli,
          'tgljurnal' => date('Y-m-d'),
          'keterangan' => 'Penyusutan Harta :'.$namaharta,
          'noreff' => '',
          'userid' => $userid,
          'nominal' => $totalpenyusutan,
          'status_posting' => 1,
        );
        if( ! $this->db->insert('jurnal', $dataJurnal)) $continue = false;
        
        $ins_jdet_debit = array(
          'kdjurnal' => $kdjurnal_penyusutan,
          'tahun' => date('Y'),
          'idakun' => $akun_biaya_penyusutan['idakun'],  
          'noreff' => '',
          'debit' => $totalpenyusutan,
          'kredit' => 0,
        );  
        if( ! $this->db->insert('jurnaldet', $ins_jdet_debit) ) $continue = false;

        $ins_jdet_kredit = array(
          'kdjurnal' => $kdjurnal_penyusutan,
          'tahun' => date('Y'),
          'idakun' => $akun_akumulasi_penyusutan['idakun'],  
          'noreff' => '',
          'debit' => 0,
          'kredit' => $totalpenyusutan,
        );  
        if( ! $this->db->insert('jurnaldet', $ins_jdet_kredit) ) $continue = false;

      }

      die;
    }else{
      $this->db->trans_rollback();
      echo json_encode(array('success' => false, 'msg' => 'gagal menyimpan data'));
      die;
    }
    */

  }

  function update_hartatetapmenyusut(){
    //update data hartatetap
    $idhartatetap         = $this->input->post('idhartatetap'); 
    $namaharta            = $this->input->post('namaharta'); 
    $idkelharta           = $this->input->post('idkelharta');
    $tglbeli              = $this->input->post('tglbeli');
    $hrgbeli              = $this->input->post('hrgbeli'); 
    $jml                  = $this->input->post('jml'); 
    $residu               = $this->input->post('residu'); 
    $umurekonomis         = $this->input->post('umurekonomis'); 
    $totalbeli            = $this->input->post('totalbeli'); 
    $penyusutan           = $this->input->post('penyusutan'); 
    $totalpenyusutan      = $this->input->post('totalpenyusutan'); 
    $umurterpakai         = $this->input->post('umurterpakai'); 
    $akumulasipenyusutan  = $this->input->post('akumulasipenyusutan'); 
    $nilaiharta           = $this->input->post('nilaiharta');
    $userid               = $this->input->post('userid');

    $data_harta = array(
      'idkelharta'   => $idkelharta,
      'jenisharta'   => 1, //1 = harta menyusut
      'nmhartatetap' => $namaharta,
      'tglbeli'      => $tglbeli,
      'hrgbeli'      => $hrgbeli,
      'jml'          => $jml,
      'residu'       => $residu,
      'umurekonomi'  => $umurekonomis,
      'umurterpakai' => $umurterpakai,
    );

    $continue = true;
    $this->db->trans_start();
    $this->db->where('idhartatetap', $idhartatetap);
    $update_harta = $this->db->update('hartatetap', $data_harta);
    if($update_harta !== false){
      #$idhartatetap = $this->db->insert_id();
    }else{
      $continue = false;
    }
    
    if($continue){
      $this->db->trans_complete();
      echo json_encode(array('success' => true, 'msg' => 'sukses menyimpan data'));
      die;
    }else{
      $this->db->trans_rollback();
      echo json_encode(array('success' => false, 'msg' => 'gagal menyimpan data'));
      die;
    }

  }
  

  function kalkulasi_penyusutan()
  {
    $data = $this->db->query("
              SELECT * FROM v_hartatetap_menyusut 
              WHERE umurterpakai2 <= umurekonomi AND 
              umurterpakai2 > umurterpakai")->result_array();

    if(empty($data)) die;

    foreach($data as $idx => $dt){
      $selisih_bulan     = $dt['umurterpakai2'] - $dt['umurterpakai'];
      $penyusutan        = (($dt['hrgbeli'] - $dt['residu']) / $dt['umurekonomi']);
      $t_penyusutan      = $penyusutan * $dt['jml'];
      $t_penyusutan_bln  = $t_penyusutan * $selisih_bulan;
      $sisa_nilai_harta  = $dt['nilaiharta'] - $t_penyusutan_bln;

      $calculate_success = true;
      $this->db->trans_begin();

      //tambah data jurnal
      $get_kdjurnal  = $this->db->query("SELECT getOtoNojurnalUmum(now()) as kdjurnal")->row();
      $kdjurnal      = $get_kdjurnal->kdjurnal;
      
      //insert jurnal 
      $do_insert = $this->db->insert('jurnal', array(
        'kdjurnal'      => $kdjurnal,
        'idjnsjurnal'   => 1, // jurnal umum
        'tgltransaksi'  => date('Y-m-d'),
        'tgljurnal'     => date('Y-m-d'),
        'keterangan'    => 'Penyusutan Harta :'.$dt['nmhartatetap'],
        'userid'        => 'admin',
        'nominal'       => $t_penyusutan_bln,
        'status_posting' => '1',
      ));
      if($this->db->affected_rows() != 1) $calculate_success = false; //if insert failed, set to false
    

      //insert jurnal biaya (debit)   
      $data_debit = array(
        'kdjurnal' => $kdjurnal,
        'tahun'    => date('Y'),
        'idakun'   => $dt['idakunbiayapenyusutan'],
        'noreff'   => '',
        'debit'    => $t_penyusutan_bln,
        'kredit'   => 0,
      );
      $this->db->insert('jurnaldet', $data_debit);
      if($this->db->affected_rows() != 1) $calculate_success = false; //if insert failed, set to false
    
      //insert jurnal akumulasi penyusutan (kredit)    
      $data_kredit = array(
        'kdjurnal' => $kdjurnal,
        'tahun'    => date('Y'),
        'idakun'   => $dt['idakunakumulasipenyusutan'],
        'noreff'   => '',
        'debit'    => 0,
        'kredit'   => $t_penyusutan_bln,
      );
      $this->db->insert('jurnaldet', $data_kredit);
      if($this->db->affected_rows() != 1) $calculate_success = false; //if insert failed, set to false

      $this->db->where('idhartatetap', $dt['idhartatetap']);
      $update_harta = $this->db->update('hartatetap', array('umurterpakai' => $dt['umurterpakai2']));
      if($this->db->affected_rows() != 1) $calculate_success = false; //if insert failed, set to false

      if($calculate_success){
        $this->db->trans_commit();        
      }else{
        $this->db->trans_rollback();        
      }
    }
  }

  function getKdjurnalUmum(){
    $q = "SELECT getOtoNojurnalUmum(now()) as nm;";
        $query  = $this->db->query($q);
    $nm= ''; 
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
  }

  function hartatetap_menyusut_pdf(){

    $this->load->library('pdf_lap');
    $isi = '<br/><font size=\"7\" face=\"Helvetica\">';       
    
    //get all akun tahun
    $this->db->orderby('idhartatetap', 'ASC');
    $data_harta = $this->db->get('v_hartatetap_menyusut')->result();
        
    // add a page
    $page_format = array(
      'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      'Rotate' => 0,
      'PZ' => 1,
    );
    
    //Set Footer
    $this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
    $this->pdf_lap->setPrintFooter(true); // enabled ? true
    $this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $this->pdf_lap->SetAutoPageBreak(TRUE, '15');
    
    $this->pdf_lap->SetPrintHeader(false);
    $this->pdf_lap->AddPage('L', $page_format, false, false);
    $this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
    
    $x=0;$y=10;
    $this->pdf_lap->SetFont('helvetica', '', 14);
    $this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
    $this->pdf_lap->Cell(0, 0, 'Harta Tetap', 0, 1, 'C', 0, '', 0);

    $this->pdf_lap->SetFont('helvetica', '', 9);
    
    $isi .='
        <br>
        <table border="1">
        <tr>
          <td align="center" width="5%"><strong>No.</strong></td>
          <td align="center" width="11%"><strong>Nama Harta</strong></td>
          <td align="center" width="7%"><strong>Tanggal Beli</strong></td>
          <td align="center" width="10%"><strong>Kelompok Harta</strong></td>
          <td align="center" width="8%"><strong>Harga Beli</strong></td>
          <td align="center" width="5%"><strong>Jumlah</strong></td>
          <td align="center" width="8%"><strong>Total Beli</strong></td>
          <td align="center" width="8%"><strong>Residu</strong></td>
          <td align="center" width="8%"><strong>Umur Ekonomi</strong></td>
          <td align="center" width="8%"><strong>Umur Terpakai</strong></td>
          <td align="center" width="11%"><strong>Akumulasi Penyusutan</strong></td>
          <td align="center" width="11%"><strong>Nilai Harta</strong></td>
        </tr>';

    if(!empty($data_harta)){
      foreach($data_harta as $idx => $harta){
        if($harta->umurterpakai2 >= $harta->umurekonomi){
          $style = 'style="background-color:#fffb90;"';
        }else{
          $style = '';
        }

        $isi .='
        <tr '.$style.'>
          <td align="center">'. ($idx + 1) .' </td>
          <td align="left">&nbsp;'. $harta->nmhartatetap .' </td>
          <td align="center">'. $harta->tglbeli .' </td>
          <td align="left">&nbsp;'. $harta->nmkelharta .' </td>
          <td align="right">Rp.'. number_format($harta->hrgbeli,2,',','.') .' &nbsp;</td>
          <td align="center">'. $harta->jml .'</td>
          <td align="right">Rp.'. number_format($harta->totalbeli,2,',','.') .' &nbsp;</td>
          <td align="right">Rp.'. number_format($harta->residu,2,',','.') .' &nbsp;</td>
          <td align="center">'. $harta->umurekonomi .' Bulan</td>
          <td align="center">'. $harta->umurterpakai2 .' Bulan</td>
          <td align="right">Rp.'. number_format($harta->akumulasipenyusutan,2,',','.') .' &nbsp;</td>
          <td align="right">Rp.'. number_format($harta->nilaiharta,2,',','.') .' &nbsp;</td>
        </tr>
        ';      
      }
    }

    $isi .= "</table></font>";
    $this->pdf_lap->writeHTML($isi,true,false,false,false);

    //Close and output PDF document
    $this->pdf_lap->Output('lap_hartatetap_menyusut.pdf', 'I');
  }

  function hartatetap_pdf(){

    $this->load->library('pdf_lap');
    $isi = '<br/><font size=\"7\" face=\"Helvetica\">';       
    
    //get all akun tahun
    $this->db->orderby('idhartatetap', 'ASC');
    $data_harta = $this->db->get('v_hartatetap')->result();
        
    // add a page
    $page_format = array(
      'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      'Rotate' => 0,
      'PZ' => 1,
    );
    
    //Set Footer
    $this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
    $this->pdf_lap->setPrintFooter(true); // enabled ? true
    $this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $this->pdf_lap->SetAutoPageBreak(TRUE, '15');
    
    $this->pdf_lap->SetPrintHeader(false);
    $this->pdf_lap->AddPage('L', $page_format, false, false);
    $this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
    
    $x=0;$y=10;
    $this->pdf_lap->SetFont('helvetica', '', 14);
    $this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
    $this->pdf_lap->Cell(0, 0, 'Harta Tetap', 0, 1, 'C', 0, '', 0);

    $this->pdf_lap->SetFont('helvetica', '', 9);
    
    $isi .='
        <br>
        <table border="1">
        <tr>
          <td align="center" width="5%"><strong>No.</strong></td>
          <td align="center" width="20%"><strong>Nama Harta</strong></td>
          <td align="center" width="10%"><strong>Tanggal Beli</strong></td>
          <td align="center" width="15%"><strong>Kelompok Harta</strong></td>
          <td align="center" width="10%"><strong>Harga Beli</strong></td>
          <td align="center" width="10%"><strong>Jumlah</strong></td>
          <td align="center" width="10%"><strong>Total Beli</strong></td>
          <td align="center" width="10%"><strong>Umur Terpakai</strong></td>
        </tr>';

    if(!empty($data_harta)){
      foreach($data_harta as $idx => $harta){
        if($harta->umurterpakai2 >= $harta->umurekonomi){
          $style = 'style="background-color:#fffb90;"';
        }else{
          $style = '';
        }

        $isi .='
        <tr '.$style.'>
          <td align="center">'. ($idx + 1) .' </td>
          <td align="left">&nbsp;'. $harta->nmhartatetap .' </td>
          <td align="center">'. $harta->tglbeli .' </td>
          <td align="left">&nbsp;'. $harta->nmkelharta .' </td>
          <td align="right">Rp.'. number_format($harta->hrgbeli,2,',','.') .' &nbsp;</td>
          <td align="center">'. $harta->jml .'</td>
          <td align="right">Rp.'. number_format($harta->totalbeli,2,',','.') .' &nbsp;</td>
          <td align="center">'. $harta->umurterpakai2 .' Bulan</td>
        </tr>
        ';      
      }
    }

    $isi .= "</table></font>";
    $this->pdf_lap->writeHTML($isi,true,false,false,false);

    //Close and output PDF document
    $this->pdf_lap->Output('lap_hartatetap_menyusut.pdf', 'I');
  }
  

  function hartatetap_menyusut_excel()
  {

    $subject_file = 'Harta Tetap Menyusut';

    $this->db->orderby('idhartatetap', 'ASC');
    $data_harta = $this->db->get('v_hartatetap_menyusut')->result();
    
    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
        'font'  => array('bold'  => true)
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    $contentgrid_mark = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'startcolor' => array(
            'rgb' => 'fffb90',
          ),
        ),
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Harta Tetap")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Harta Tetap'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:M1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Harta Tetap menyusut');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:M2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('L')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('M')->setWidth(18);

    //set header grid
    $this->lib_phpexcel->getActiveSheet()->getStyle("B4:M4")->applyFromArray($headergrid);
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B4", 'No.')
         ->setCellValue("C4", 'Nama Harta')
         ->setCellValue("D4", 'Tanggal Beli')
         ->setCellValue("E4", 'Kelompok Harta')
         ->setCellValue("F4", 'Harga Beli')
         ->setCellValue("G4", 'Jumlah')
         ->setCellValue("H4", 'Total Beli')
         ->setCellValue("I4", 'Residu')
         ->setCellValue("J4", 'Umur Ekonomi')
         ->setCellValue("K4", 'Umur Terpakai')
         ->setCellValue("L4", 'Akumulasi Penyusutan')
         ->setCellValue("M4", 'Nilai Harta');
    

    if(!empty($data_harta)){
      $active_row = 5;
      foreach($data_harta as $idx => $harta){
        
        //set content grid
        if($harta->umurterpakai2 >= $harta->umurekonomi){
          $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:M{$active_row}")->applyFromArray($contentgrid_mark);
        }else{
          $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:M{$active_row}")->applyFromArray($contentgrid);
        }
        
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", ($idx + 1))
             ->setCellValue("C{$active_row}", $harta->nmhartatetap)
             ->setCellValue("D{$active_row}", $harta->tglbeli)
             ->setCellValue("E{$active_row}", $harta->nmkelharta)
             ->setCellValue("F{$active_row}", $harta->hrgbeli)
             ->setCellValue("G{$active_row}", $harta->jml)
             ->setCellValue("H{$active_row}", $harta->totalbeli)
             ->setCellValue("I{$active_row}", $harta->residu)
             ->setCellValue("J{$active_row}", $harta->umurekonomi)
             ->setCellValue("K{$active_row}", $harta->umurterpakai2)
             ->setCellValue("L{$active_row}", $harta->akumulasipenyusutan)
             ->setCellValue("M{$active_row}", $harta->nilaiharta);

        $active_row += 1;
      }
    }


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'lap_hartatetap_menyusut';

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

  function hartatetap_excel()
  {

    $subject_file = 'Harta Tetap';

    $this->db->orderby('idhartatetap', 'ASC');
    $data_harta = $this->db->get('v_hartatetap')->result();
    
    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
        'font'  => array('bold'  => true)
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    $contentgrid_mark = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'fill' => array(
          'type' => PHPExcel_Style_Fill::FILL_SOLID,
          'startcolor' => array(
            'rgb' => 'fffb90',
          ),
        ),
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Harta Tetap")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Harta Tetap'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:I1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Harta Tetap');
  
    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(2);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);

    //set header grid
    $this->lib_phpexcel->getActiveSheet()->getStyle("B4:I4")->applyFromArray($headergrid);
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B4", 'No.')
         ->setCellValue("C4", 'Nama Harta')
         ->setCellValue("D4", 'Tanggal Beli')
         ->setCellValue("E4", 'Kelompok Harta')
         ->setCellValue("F4", 'Harga Beli')
         ->setCellValue("G4", 'Jumlah')
         ->setCellValue("H4", 'Total Beli')
         ->setCellValue("I4", 'Umur Terpakai');
    

    if(!empty($data_harta)){
      $active_row = 5;
      foreach($data_harta as $idx => $harta){
        
        //set content grid
        if($harta->umurterpakai2 >= $harta->umurekonomi){
          $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:I{$active_row}")->applyFromArray($contentgrid_mark);
        }else{
          $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:I{$active_row}")->applyFromArray($contentgrid);
        }
        
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", ($idx + 1))
             ->setCellValue("C{$active_row}", $harta->nmhartatetap)
             ->setCellValue("D{$active_row}", $harta->tglbeli)
             ->setCellValue("E{$active_row}", $harta->nmkelharta)
             ->setCellValue("F{$active_row}", $harta->hrgbeli)
             ->setCellValue("G{$active_row}", $harta->jml)
             ->setCellValue("H{$active_row}", $harta->totalbeli)
             ->setCellValue("I{$active_row}", $harta->umurterpakai2);

        $active_row += 1;
      }
    }


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'lap_hartatetap';

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

}
