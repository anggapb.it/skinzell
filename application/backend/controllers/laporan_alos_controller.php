<?php 
class Laporan_alos_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
			$this->load->library('rhlib');
		}
	
	function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=1;
        }
        return $max;
    }
	
	function get_date_server() {
		$data['date'] = date('d-m-Y');
		echo json_encode($data);
    }
	
	function get_jmlrawat_masuk($tglawal,$tglakhir){	
		$q = "SELECT * FROM v_lamarawat
			WHERE 
				idjnspelayanan = 2
				-- AND idstkeluar IN(1,2,3,4,5)
				AND (cast(tglmasuk AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
        $query  = $this->db->query($q);
        $result = array(); 
        $count = 0;            
        if ($query->num_rows() != 0)
        {
            $result = $query->result();
            foreach ($result as $data) {
				$count = $count + $data->lamarawat;
			}
        }
        return $count;
	}
	
	function get_jmlrawat_keluar($tglawal,$tglakhir){
		$q = "SELECT * , ((to_days(tglkeluar) - to_days('$tglawal')) + 1) AS lamarawat1
			FROM
			  v_lamarawat
			WHERE
				idjnspelayanan = 2
				-- AND idstkeluar IN(1,2,3,4,5)
				AND (cast(tglmasuk AS DATE) < '$tglawal')
				AND (cast(tglkeluar AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
        $query  = $this->db->query($q);
        $result = array(); 
        $count = 0;            
        if ($query->num_rows() != 0)
        {
            $result = $query->result();
            foreach ($result as $data) {
				$count = $count + $data->lamarawat1;
			}
        }
        return $count;
	}
	
	function get_pasien_masuk_hidup($tglawal,$tglakhir){	
		$q = "SELECT count(noreg) as countreg FROM v_lamarawat
			WHERE 
				idjnspelayanan = 2
				AND (idstkeluar IN(1,4,5) OR idstkeluar IS NULL)
				AND (cast(tglmasuk AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
        $queryrows  = $this->db->query($q)->row();
        return $queryrows->countreg;
	}
	
	function get_pasien_keluar_hidup($tglawal,$tglakhir){
		$q = "SELECT count(noreg) as countreg
			FROM
			  v_lamarawat
			WHERE
				idjnspelayanan = 2
				AND (idstkeluar IN(1,4,5) OR idstkeluar IS NULL)
				AND (cast(tglmasuk AS DATE) < '$tglawal')
				AND (cast(tglkeluar AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
        $queryrows  = $this->db->query($q)->row();
        return $queryrows->countreg;
	}
	
	function get_pasien_masuk_mati($tglawal,$tglakhir){	
		$q = "SELECT count(noreg) as countreg FROM v_lamarawat
			WHERE 
				idjnspelayanan = 2
				AND idstkeluar IN(2,3)
				AND (cast(tglmasuk AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
        $queryrows  = $this->db->query($q)->row();
        return $queryrows->countreg;
	}
	
	function get_pasien_keluar_mati($tglawal,$tglakhir){
		$q = "SELECT count(noreg) as countreg
			FROM
			  v_lamarawat
			WHERE
				idjnspelayanan = 2
				AND idstkeluar IN(2,3)
				AND (cast(tglmasuk AS DATE) < '$tglawal')
				AND (cast(tglkeluar AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
        $queryrows  = $this->db->query($q)->row();
        return $queryrows->countreg;
	}
	
	function get_items(){
		$tglawal = $this->input->post("tglawal");
		$tglakhir = $this->input->post("tglakhir");
		$interval = $this->input->post("interval");
		
		$jmlmasuk = $this->get_jmlrawat_masuk($tglawal,$tglakhir);
		$jmlkeluar = $this->get_jmlrawat_keluar($tglawal,$tglakhir);
		
		$jmlpasienmasukhidup = $this->get_pasien_masuk_hidup($tglawal,$tglakhir);
		$jmlpasienkeluarhidup = $this->get_pasien_keluar_hidup($tglawal,$tglakhir);
		
		$jmlpasienmasukmati = $this->get_pasien_masuk_mati($tglawal,$tglakhir);
		$jmlpasienkeluarmati = $this->get_pasien_keluar_mati($tglawal,$tglakhir);

        $data['jmlrawat'] = $jmlmasuk + $jmlkeluar;
		$data['jmlpasienkeluarhidup'] = $jmlpasienmasukhidup + $jmlpasienkeluarhidup;
		$data['jmlpasienkeluarmati'] = $jmlpasienmasukmati + $jmlpasienkeluarmati;
		$data['jmlpasienkeluar'] = $data['jmlpasienkeluarhidup'] + $data['jmlpasienkeluarmati'];
		
		$data['result'] = (!$data['jmlrawat']) ? 0:intval($data['jmlrawat'] / $data['jmlpasienkeluar']);
        echo json_encode($data);
	}
	
	function simpan_hasil(){
		$dataArray = array(
			 'idalos'=> $this->autoNumber('idalos','alos'),
             'daritgl'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglawal']))),
             'sampaitgl'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglakhir']))),
			 'jmllamadirawat'=> $this->rhlib->retValOrNull($_POST['jld']),
			 'jmlpasien'=> $this->rhlib->retValOrNull($_POST['jpk']),
             'jmlpasienhidup'=> $this->rhlib->retValOrNull($_POST['hidup']),
             'jmlpasienmati'=> $this->rhlib->retValOrNull($_POST['mati']),
			 'hasilhari'=> $this->rhlib->retValOrNull($_POST['hasil']),
			 
			 'catatan'=> $this->rhlib->retValOrNull($_POST['catatan']),
             'tglbuat'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglproses']))),
			 'userid'=> $this->rhlib->retValOrNull($_POST['userid']),
        );
		$ret = $this->rhlib->insertRecord('alos',$dataArray);
        echo json_encode($ret);
    }
	
	function delete_history(){     
		$where['idalos'] = $_POST['idalos'];
		$del = $this->rhlib->deleteRecord('alos',$where);
        return $del;
    }
	
	function get_history(){ //ISTRA
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("v_alos");
 
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
				if ($i != 4) {
                $d[$b[$i]]=$query; }
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(10,0);
        }
        
        $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->db->query("SELECT * FROM v_alos")->num_rows(); 
        
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

}
