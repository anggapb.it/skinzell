<?php

class Akunpelayanan_Controller extends Controller {
  public function __construct()
  {
    parent::Controller();
		$this->load->library('session');
		$this->load->library('rhlib');
  }
	
	function get_akunpelayanan(){
		$key					= $_POST["key"]; 
		
		$this->db->select("*");
		$this->db->from("v_akunpelayanan");		
		$this->db->order_by('v_akunpelayanan.kdpelayanan');
		
		$where = array();   
    $where['v_akunpelayanan.tahun']= $_POST['tahun'];          
		$this->db->where($where);
		
    if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}		
        
    $q = $this->db->get();
    $data = array();
    if ($q->num_rows() > 0) {
        $data = $q->result();
    }

    $ttl = $this->db->count_all("v_akunpelayanan");
    $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

    if($ttl>0){
        $build_array["data"]=$data;
    }

    echo json_encode($build_array);
  }
	
	function delete_data(){ 
		$where['tahun'] 	= $_POST['tahun'];
		$where['kdpelayanan'] 	= $_POST['kdpelayanan'];
		$del = $this->rhlib->deleteRecord('setakunpelayanan',$where);
    return $del;
  }
		
	function insert_akun(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('setakunpelayanan',$dataArray);
    return $ret;
  }
	
	function getFieldsAndValues(){
		$dataArray = array(
	    'tahun'	=> $_POST['tahun'], 
			'kdpelayanan' 	=> $_POST['kdpelayanan']      
    );
		return $dataArray;
	}
	
	function cekidakunpel(){
    $q = "SELECT count(*) as kdpelayanan FROM setakunpelayanan where kdpelayanan='".$_POST['kdpelayanan']."' AND tahun='".$_POST['tahun']."'" ;
    $query  = $this->db->query($q);
    $jum = '';
    if ($query->num_rows() != 0){
			$row = $query->row();
      $jum=$row->kdpelayanan;
    }

    if ($jum == null){
        $jum=0;
    }
    echo $jum;
  }
	
	function copy_data(){
		$where['tahun'] 	= $_POST['tahun_baru'];
		$this->rhlib->deleteRecord('setakunpelayanan',$where);
				
		$tahun_lama = $_POST['tahun_lama'];
		$tahun_baru = $_POST['tahun_baru'];
		$sql = "INSERT INTO setakunpelayanan (tahun, kdpelayanan, kdakunjs, kdakunjm, kdakunjp, kdakunbhp
      , akundiskonjs, akundiskonjm, akundiskonjp, akundiskonbhp)
				SELECT 
					".$tahun_baru."
					, kdpelayanan
          , kdakunjs
          , kdakunjm
          , kdakunjp
          , kdakunbhp
          , akundiskonjs
          , akundiskonjm
          , akundiskonjp
          , akundiskonbhp
				FROM
				  setakunpelayanan
				WHERE
				  tahun = '".$tahun_lama."'";
		$ret = $this->db->query($sql);
    return $ret;
  }
	
	function get_akun(){ 
		$this->db->select("*");
		$this->db->from("akun");		
		$this->db->order_by('akun.nmakun');     
		$this->db->where('idklpakun = 4');

	  //$this->db->getwhere('akun',array('idklpakun'=>'4'));
		//$this->db->order_by('akun.nmakun');
		
		$query = $this->db->get();
		
		$akun = $query->result();
    if(!empty($akun)){
      foreach($akun as $idx => $ak)
      {
        $akun[$idx]->kd_nm_akun = '('.$ak->kdakun.') '.$ak->nmakun;
      }
    }

		$ttl = count($akun);
    $arrPelayanan = array ("success"=>true,"results"=>$ttl,"data"=>$akun);
		echo json_encode($arrPelayanan);
  }
	
	function update_akunpelayanan() {
    $records = explode('||', $_POST['postdata']);
    $kdakunjs = $_POST['kdakunjs'];
    $kdakunjm = $_POST['kdakunjm'];
    $kdakunjp = $_POST['kdakunjp'];
    $kdakunbhp = $_POST['kdakunbhp'];
    $row_count = count($records);	
		for($ri=0;$ri<$row_count;$ri++)
    {
      $field = explode(";",$records[$ri]);
			$data = array(
        'kdakunjs' => $kdakunjs,
        'kdakunjm' => $kdakunjm,
        'kdakunjp' => $kdakunjp,
        'kdakunbhp' => $kdakunbhp,
      );
			
			if (is_numeric($field[0]))
      {		
				$this->db->where('tahun', $field[0]);
				$this->db->where('kdpelayanan', $field[1]);
				$this->db->update('setakunpelayanan', $data);
      }
 
    }
  }
	
	function get_pelayanan(){
    $start                  = $this->input->post("start");
    $limit                  = $this->input->post("limit");
    $fields                  = $this->input->post("fields");
    $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_pelayanan");
				
		$this->db->where('v_pelayanan.pel_kdpelayanan !=','null');
		$this->db->where('v_pelayanan.idjnshirarki = 1');
		$this->db->order_by('kdpelayanan');
        
        
    if($fields!="" || $query !=""){
      $k=array('[',']','"');
      $r=str_replace($k, '', $fields);
      $b=explode(',', $r);
      $c=count($b);
      for($i=0;$i<$c;$i++){
        $d[$b[$i]]=$query;
      }
      $this->db->or_like($d, $query);
    }
                
    if ($start!=null){
      $this->db->limit($limit,$start);
    }else{
      $this->db->limit(50,0);
    }
        
    $q = $this->db->get();
    $data = array();
    if ($q->num_rows() > 0) {
        $data = $q->result();
    }
		
    $ttl = $this->numrow($fields, $query);
    $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

    if($ttl>0){
      $build_array["data"]=$data;
    }
		
    echo json_encode($build_array);
  }
	
	function numrow($fields, $query){
		$this->db->select("*");
		$this->db->from("v_pelayanan");
    if($fields!="" || $query !=""){
      $k=array('[',']','"');
      $r=str_replace($k, '', $fields);
      $b=explode(',', $r);
      $c=count($b);
      for($i=0;$i<$c;$i++){
        $d[$b[$i]]=$query;
      }
      $this->db->or_like($d, $query);
    }

    $q = $this->db->get();        
    return $q->num_rows();
  }
}
