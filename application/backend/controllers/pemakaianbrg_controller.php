<?php

class Pemakaianbrg_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_pemakaianbrg(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
        $userid            		= $this->input->post("userid");
      
        $this->db->select("*");
        $this->db->from("v_pakaibrg");
		$this->db->order_by('v_pakaibrg.nopakaibrg DESC');
        
        if($userid){		
			$this->db->where("v_pakaibrg.idbagian IN (SELECT idbagian from penggunabagian where userid='".$userid."')");
        }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->numrow($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($key, $userid){
    
        $this->db->select("*");
        $this->db->from("v_pakaibrg"); 
		
        if($userid){		
			$this->db->where("v_pakaibrg.idbagian IN (SELECT idbagian from penggunabagian where userid = '".$userid."')");
        }
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        $q = $this->db->get();
        return $q->num_rows();
    }
	
	function get_pemakaianbrgdet(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$nopakaibrg				= $this->input->post("nopakaibrg");
		
        $this->db->select("*");
        $this->db->from("v_pakaibrgdet");
		
		//if($nopakaibrg != null)$this->db->where("v_pakaibrgdet.nopakaibrg", $nopakaibrg);
        $where = array();
        $where['v_pakaibrgdet.nopakaibrg']=$nopakaibrg;
		$this->db->where($where);
		
        /* if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
				
		
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        } */
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all("v_pakaibrgdet");
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	/* function nw($fields, $query){
    
        $this->db->select("*");
        $this->db->from("v_pakaibrgdet");
		
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
        $q = $this->db->get();
        return $q->num_rows();
    } */
	
	function get_brgmedisdipemakaianbrg(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $this->input->post("key");
		$idbagian				= $this->input->post("idbagian");
		
        $this->db->select('*');
		$this->db->from('v_barangbagian');
		$this->db->order_by('kdbrg');
        
		$where = array();
		$where['v_barangbagian.idbagian']=$idbagian;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrw($key, $idbagian);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrw($key, $idbagian){
      
        $this->db->select('*');
		$this->db->from('v_barangbagian');
		
		$where = array();
		$where['v_barangbagian.idbagian']=$idbagian;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_pemakaianbrg(){     
		$where['nopakaibrg'] = $_POST['nopakaibrg'];
		$del = $this->rhlib->deleteRecord('pakaibrgdet',$where);
		$del = $this->rhlib->deleteRecord('pakaibrg',$where);
        return $del;
    }
	
	function id_bagian($where, $val){
		$query = $this->db->getwhere('bagian',array($where=>$val));
		$id = $query->row_array();
		return $id['idbagian'];
    }
	
	function getNmbagian(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }
	
	function insorupd_pbrg(){
        $this->db->trans_begin();
		$nopakaibrg = $this->input->post("nopakaibrg");
		$query 	= $this->db->getwhere('pakaibrg',array('nopakaibrg'=>$nopakaibrg));
		$nopakaibrg = $query->row_array();
				
		$pakaibrg = $this->insert_tblpakaibrg($nopakaibrg);
		$nopakaibrg = $pakaibrg['nopakaibrg'];
				
		if($nopakaibrg)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nopakaibrg"]=$nopakaibrg;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_tblpakaibrg($nopakaibrg){
		if(!$nopakaibrg){
			$dataArray = $this->getFieldsAndValues($nopakaibrg);
			$pakaibrg = $this->db->insert('pakaibrg',$dataArray);
		} else {
			$dataArray = $this->update_tblpakaibrg($nopakaibrg);
		}
		
		$query = $this->db->getwhere('pakaibrgdet',array('nopakaibrg'=>$dataArray['nopakaibrg']));
		$pakaibrgdet = $query->row_array();
		if($query->num_rows() == 0){
			$pakaibrgdet = $this->insert_tblpakaibrgdet($dataArray);
		} else {
			$pakaibrgdet = $this->update_tblpakaibrgdet($dataArray);
		}
		
		if($pakaibrgdet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function insert_tblpakaibrgdet($pakaibrg){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrpbrg']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($pakaibrg, $vale[0], $vale[1], $vale[2]);
			$z =$this->db->insert('pakaibrgdet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function update_tblpakaibrg($nopakaibrg){
		$dataArray = $this->getFieldsAndValues($nopakaibrg);
		
		//UPDATE
		$this->db->where('nopakaibrg', $dataArray['nopakaibrg']);
		$pakaibrg = $this->db->update('pakaibrg', $dataArray);
		
		return $dataArray;
    }
	
	function update_tblpakaibrgdet($pakaibrg){
		$where['nopakaibrg'] = $pakaibrg['nopakaibrg'];
		$this->db->delete('pakaibrgdet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrpbrg']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($pakaibrg, $vale[0], $vale[1], $vale[2]);
			$z =$this->db->insert('pakaibrgdet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function getFieldsAndValues(){
			$nopb   	= $this->input->post("nopakaibrg");
			$nopakaibrg = $this->getNoPbrg();
		
		$dataArray = array(
			 'nopakaibrg'		=> ($nopb) ? $nopb: $nopakaibrg,
			 'tglpakaibrg'		=> $_POST['tglpakaibrg'],
             'jampakaibrg'		=> $_POST['jampakaibrg'],
             'idbagian'			=> $_POST['idbagian'],
             'idsttransaksi'	=> $_POST['idsttransaksi'],
             'idstsetuju'		=> $_POST['idstsetuju'],
			 'userid'			=> $_POST['userid'],	
			 'keterangan'		=> $_POST['keterangan']			
		);
		/* var_dump($dataArray);
		exit; */
		return $dataArray;
	}
	
	function getNoPbrg(){
		$q = "SELECT getOtoNopemakaianbrg(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}

	function getFieldsAndValuesDet($pakaibrg,$val1,$val2,$val3){
		$dataArray = array(
			'nopakaibrg' => $pakaibrg['nopakaibrg'],
			'kdbrg'		 => $val1,
			'qty' 		 => $val2,
			'catatan'    => $val3,			
		);
		return $dataArray;
	}
	
	function update_qty_tambah(){
		//$nopakaibrg = $this->getNoPbrg();
		$date = date("Y-m-d", strtotime($_POST['tglpakaibrg']));		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_update_qty_brgbagian (?,'".$date."',?,?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3],
							 $vale[4],
							 $vale[5]
							));
		}
        return;
    }
	
	function update_qty_batal(){
		//$nopakaibrg = $this->getNoPbrg();
		$date = date("Y-m-d", strtotime($_POST['tglpakaibrg']));
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_update_tmbh_qty_brgbagian (?,'".$date."',?,?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3],
							 $vale[4],
							 $vale[5]
							));
		}
        return;
    }
	
	function get_pengbagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
		$userid 				= $this->input->post("userid");
      
        $this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrww($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrww($key, $userid){
      
		$this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
}