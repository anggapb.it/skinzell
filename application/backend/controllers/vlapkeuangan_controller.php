<?php

class Vlapkeuangan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_vlappasienluar(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $idbagian	= $this->input->post("idbagian");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
      
        $this->db->select("
			`n`.`nonota` AS `nonota`,
			`n`.`tglnota` AS `tglnota`,
			`n`.`noresep` AS `noresep`,
			`n`.`diskon` AS `diskon`,
			`n`.`uangr` AS `racik`,
			`n`.`idbagian` AS `idbagian`,
			`n`.`userinput` AS `userid`,
			`pg`.`nmlengkap` AS `nmlengkap`,
			(
				select sum(((((`nd2`.`tarifjs` + `nd2`.`tarifjm`) + `nd2`.`tarifjp`) + `nd2`.`tarifbhp`) * `nd2`.`qty`)) 
				from `notadet` `nd2`
				where (`nd2`.`nonota` = `n`.`nonota`)
			) AS `tottindakan`,
			`nd`.`diskonjs` + `nd`.`diskonjm` + `nd`.`diskonjp` + `nd`.`diskonbhp` AS `diskontindakan`,
			`d`.`nmdoktergelar` AS `nmdokter`,
			`k`.`atasnama` AS `atasnama`,
			`k`.`total` AS `total`,
			(
				(
					select sum(((((`nd2`.`tarifjs` + `nd2`.`tarifjm`) + `nd2`.`tarifjp`) + `nd2`.`tarifbhp`) * `nd2`.`qty`)) 
					from `notadet` `nd2` 
					where (`nd2`.`nonota` = `n`.`nonota`)
				) + `n`.`uangr` - `n`.`diskon`
			) AS `jumlah`,
			(
				(
					select sum(((((`nd2`.`tarifjs` + `nd2`.`tarifjm`) + `nd2`.`tarifjp`) + `nd2`.`tarifbhp`) * `nd2`.`qty`))
					from `notadet` `nd2`
					where (`nd2`.`nonota` = `n`.`nonota`)
				) - (((`nd`.`diskonjs` + `nd`.`diskonjm`) + `nd`.`diskonjp`) + `nd`.`diskonbhp`)
			) AS `jumlahtindakan`,
			`k`.`nokuitansi` AS `nokuitansi`,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar <> 1
			) AS nontunai,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = k.nokuitansi AND kdet.idcarabayar = 1
			) AS tunai
		", false);
        $this->db->from("`nota` `n`");
		$this->db->join('`notadet` `nd`',
					'nd.`nonota` = `n`.`nonota`', 'left', false);
		$this->db->join('`kuitansi` `k`',
					'k.`nokuitansi` = `n`.`nokuitansi`', 'left', false);
		$this->db->join('`dokter` `d`',
					'd.`iddokter` = `n`.`iddokter`', 'left', false);
		$this->db->join('`pengguna` `pg`',
					'pg.`userid` = `n`.`userinput`', 'left', false);
					
		$this->db->where('`n`.`idregdet` IS NULL',null, false);
		$this->db->where('`n`.`idsttransaksi` <>', 2, false);
		$this->db->where('`k`.`idstkuitansi` <>', 2, false);
		$this->db->groupby('`n`.`nonota`');
		if($tglakhir){
			$this->db->where('n.`tglnota` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($idbagian) $this->db->where('n.idbagian',$idbagian, false);
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_vlappasienluar();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_vlappasienluar(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $idbagian	= $this->input->post("idbagian");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
      
        $this->db->select("*");
        $this->db->select("
			`n`.`nonota` AS `nonota`,
			`n`.`tglnota` AS `tglnota`,
			`n`.`noresep` AS `noresep`,
			`n`.`diskon` AS `diskon`,
			`n`.`uangr` AS `racik`,
			`n`.`idbagian` AS `idbagian`,
			`n`.`userinput` AS `userid`,
			`pg`.`nmlengkap` AS `nmlengkap`,
			(
				select sum(((((`nd2`.`tarifjs` + `nd2`.`tarifjm`) + `nd2`.`tarifjp`) + `nd2`.`tarifbhp`) * `nd2`.`qty`)) 
				from `notadet` `nd2`
				where (`nd2`.`nonota` = `n`.`nonota`)
			) AS `tottindakan`,
			`nd`.`diskonjs` + `nd`.`diskonjm` + `nd`.`diskonjp` + `nd`.`diskonbhp` AS `diskontindakan`,
			`d`.`nmdoktergelar` AS `nmdokter`,
			`k`.`atasnama` AS `atasnama`,
			`k`.`total` AS `total`,
			(
				(
					select sum(((((`nd2`.`tarifjs` + `nd2`.`tarifjm`) + `nd2`.`tarifjp`) + `nd2`.`tarifbhp`) * `nd2`.`qty`)) 
					from `notadet` `nd2` 
					where (`nd2`.`nonota` = `n`.`nonota`)
				) + `n`.`uangr` - `n`.`diskon`
			) AS `jumlah`,
			(
				(
					select sum(((((`nd2`.`tarifjs` + `nd2`.`tarifjm`) + `nd2`.`tarifjp`) + `nd2`.`tarifbhp`) * `nd2`.`qty`))
					from `notadet` `nd2`
					where (`nd2`.`nonota` = `n`.`nonota`)
				) - (((`nd`.`diskonjs` + `nd`.`diskonjm`) + `nd`.`diskonjp`) + `nd`.`diskonbhp`)
			) AS `jumlahtindakan`,
			`k`.`nokuitansi` AS `nokuitansi` 
		", false);
        $this->db->from("`nota` `n`");
		$this->db->join('`notadet` `nd`',
					'nd.`nonota` = `n`.`nonota`', 'left', false);
		$this->db->join('`kuitansi` `k`',
					'k.`nokuitansi` = `n`.`nokuitansi`', 'left', false);
		$this->db->join('`dokter` `d`',
					'd.`iddokter` = `n`.`iddokter`', 'left', false);
		$this->db->join('`pengguna` `pg`',
					'pg.`userid` = `n`.`userinput`', 'left', false);
					
		$this->db->where('`n`.`idregdet` IS NULL',null, false);
		$this->db->groupby('`n`.`nonota`');
		
		if($tglakhir){
			$this->db->where('n.`tglnota` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($idbagian) $this->db->where('n.idbagian',$idbagian, false);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_vlapdeposit(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
      
        $this->db->select("*");
        $this->db->from("v_lapdeposit");
		
		if($tglakhir){
			$this->db->where('`tglkuitansi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_vlapdeposit();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_vlapdeposit(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
      
        $this->db->select("*");
        $this->db->from("v_lapdeposit");
		
		if($tglakhir){
			$this->db->where('`tglkuitansi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }

	function get_vlappenerimaanri(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
        $ctransfer	= $this->input->post("ctransfer");
      
        $this->db->select("
			kuitansi.nokuitansi,
			kuitansi.tglkuitansi,
			registrasi.noreg,
			pasien.norm,
			pasien.nmpasien,
			penjamin.nmpenjamin,
			dokter.nmdoktergelar,
			(
				select tglmasuk
				from registrasidet rd
				where rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS tglmasuk, 
			(
				select nmbagian
				from bagian, registrasidet rd
				where bagian.idbagian = rd.idbagian AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS nmbagian, 
			(
				select rd.noreg
				from nota nt, registrasidet rd
				where nt.idregdettransfer = registrasi.noreg AND
					rd.idregdet = nt.idregdet
			) AS transferugd,
			 (SELECT sum(nd.tarifbhp * nd.qty) AS uobt
				FROM
				  notadet nd
				LEFT JOIN nota n
				ON n.nonota = nd.nonota
				LEFT JOIN registrasidet rd
				ON rd.idregdet = n.idregdet
				WHERE
				  rd.noreg = registrasidet.noreg
				  AND
				  nd.kditem LIKE 'B%'
			   ) AS uobat,
			(
				select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
				from notadet ntd
				left join nota nt
				on ntd.nonota = nt.nonota
				left join registrasidet rd
				on rd.idregdet = nt.idregdet
				where nt.idregdettransfer = registrasi.noreg
			) AS nominalugd,
			(SUM(notadet.tarifjs*notadet.qty)+SUM(notadet.tarifjm*notadet.qty)+SUM(notadet.tarifjp*notadet.qty)+SUM(notadet.tarifbhp*notadet.qty)+nota.uangr) AS nominalri,
			(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon) AS diskon,
			(
				select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
				from kuitansi kui, registrasidet rd
				where kui.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg AND
					kui.idjnskuitansi = 5 AND
					kui.idstkuitansi = 1
			) AS deposit,
			(
				select nt.userinput
				from nota nt, registrasidet rd
				where nt.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS userid,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar <> 1
			) AS nontunai,
			(
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar = 1
			) AS tunai
		", false);
        $this->db->from("registrasi");
        $this->db->join("registrasidet",
				"registrasidet.noreg = registrasi.noreg", "left");
        $this->db->join("nota",
				"nota.idregdet = registrasidet.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        if($ctransfer == '2'){
			$this->db->join("nota nota2",
					"nota2.idregdettransfer = registrasi.noreg", "left");
		}
				
		$this->db->where('registrasi.idjnspelayanan', 2, false);
		$this->db->groupby("registrasi.noreg");
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($ctransfer == '1'){$this->db->where('(SELECT nt.idregdettransfer
												  FROM
													nota nt
												  WHERE
													nt.idregdettransfer = registrasi.noreg
												  ) IS NULL');}
		if($ctransfer == '2'){$this->db->where('nota2.nonota IS NOT NULL', null, false);}
		
		/* if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        } */
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);//$this->numrow_vlappenerimaanri();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_vlappenerimaanri(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
        $ctransfer	= $this->input->post("ctransfer");
      
        $this->db->select("
			kuitansi.nokuitansi,
			kuitansi.tglkuitansi,
			registrasi.noreg,
			pasien.norm,
			pasien.nmpasien,
			penjamin.nmpenjamin,
			dokter.nmdoktergelar,
			(
				select nmbagian
				from bagian, registrasidet rd
				where bagian.idbagian = rd.idbagian AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS nmbagian, 
			(
				select rd.noreg
				from nota nt, registrasidet rd
				where nt.idregdettransfer = registrasi.noreg AND
					rd.idregdet = nt.idregdet
			) AS transferugd,
			 (SELECT sum(nd.tarifbhp * nd.qty) AS uobt
				FROM
				  notadet nd
				LEFT JOIN nota n
				ON n.nonota = nd.nonota
				LEFT JOIN registrasidet rd
				ON rd.idregdet = n.idregdet
				WHERE
				  rd.noreg = registrasidet.noreg
				  AND
				  nd.kditem LIKE 'B%'
			   ) AS uobat,
			(
				select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
				from notadet ntd
				left join nota nt
				on ntd.nonota = nt.nonota
				left join registrasidet rd
				on rd.idregdet = nt.idregdet
				where nt.idregdettransfer = registrasi.noreg
			) AS nominalugd,
			(SUM(notadet.tarifjs)+SUM(notadet.tarifjm)+SUM(notadet.tarifjp)+SUM(notadet.tarifbhp)+SUM(nota.uangr)) AS nominalri,
			(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+SUM(nota.diskon)) AS diskon,
			(
				select SUM(kui.total)
				from kuitansi kui, registrasidet rd
				where kui.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg AND
					kui.idjnskuitansi = 5
			)AS deposit,
			(
				select nt.userinput
				from nota nt, registrasidet rd
				where nt.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS userid
		");
        $this->db->from("registrasi");
        $this->db->join("registrasidet",
				"registrasidet.noreg = registrasi.noreg", "left");
        $this->db->join("nota",
				"nota.idregdet = registrasidet.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        if($ctransfer == '2'){
			$this->db->join("nota nota2",
					"nota2.idregdettransfer = registrasi.noreg", "left");
		}
				
		$this->db->where('registrasi.idjnspelayanan', 2, false);
		$this->db->groupby("registrasi.noreg");
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($ctransfer == '1'){$this->db->where('(SELECT nt.idregdettransfer
												  FROM
													nota nt
												  WHERE
													nt.idregdettransfer = registrasi.noreg
												  ) IS NULL');}
		else if($ctransfer == '2'){$this->db->where('nota2.nonota IS NOT NULL', null, false);}
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }

	function get_vlapugdperiode(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
		
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
      
        $this->db->select("
			`r`.`noreg` AS `noreg`,
			`rd`.`tglreg` AS `tglreg`,
			TRIM(LEADING '0' FROM `p`.`norm`) AS `norm`,
			`p`.`nmpasien` AS `nmpasien`,
			(
				SELECT sum(`t`.`tarifbhp`*`t`.`qty`)
				FROM
				  `notadet` `t`
				WHERE
				  ((`t`.`nonota` = `n`.`nonota`)
				  AND (`t`.`kditem` LIKE 'B%'))
			) AS `uobat`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `upemeriksaan`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000001'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `upemeriksaan`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `utindakan`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000048'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `utindakan`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `uimun`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000009'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `uimun`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `ulab`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000037'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `ulab`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `ulain`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000073'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `ulain`,
			`n`.`uangr` AS `uracik`,
			sum(`nd`.`diskonjs` + `nd`.`diskonjm` + `nd`.`diskonjp` + `nd`.`diskonbhp`) + `n`.`diskon` AS `udiskon`,
			(
				SELECT sum(`kd`.`jumlah`)
				FROM
				  `kuitansidet` `kd`
				WHERE
				  `kd`.`nokuitansi` = `k`.`nokuitansi`
				  AND `kd`.`idcarabayar` <> 1
			) AS `ucc`,
			(
				SELECT sum(`kd`.`jumlah`)
				FROM
				  `kuitansidet` `kd`
				WHERE
				  `kd`.`nokuitansi` = `k`.`nokuitansi`
				  AND `kd`.`idcarabayar` = 1
			) AS `utotal`,
			`b`.`nmbagian` AS `nmbagian`,
			`b`.`idbagian` AS `idbagian`,
			`d`.`nmdoktergelar` AS `nmdoktergelar`,
			(
				SELECT sum(`kd`.`jumlah`)
				FROM
				  `kuitansidet` `kd`
				WHERE
				  `kd`.`nokuitansi` = `k`.`nokuitansi`
			) AS total,
			(
				SELECT sum(`kd`.`jumlah`)
				FROM
				  `kuitansidet` `kd`
				WHERE
				  `kd`.`nokuitansi` = `k`.`nokuitansi`
				  AND `k`.idregdet IS NOT NULL
			) AS utotaltrans
		", false);
        $this->db->from("`registrasi` `r`", false);
		$this->db->join('`pasien` `p`',
					'p.`norm` = `r`.`norm`', 'left', false);
		$this->db->join('`registrasidet` `rd`',
					'rd.`noreg` = `r`.`noreg`', 'left', false);
		$this->db->join('`bagian` `b`',
					'b.`idbagian` = `rd`.`idbagian`', 'left', false);
		$this->db->join('`dokter` `d`',
					'd.`iddokter` = `rd`.`iddokter`', 'left', false);
		$this->db->join('`nota` `n`',
					'n.`idregdet` = `rd`.`idregdet`', 'left', false);
		$this->db->join('`notadet` `nd`',
					'nd.`nonota` = `n`.`nonota`', 'left', false);
		$this->db->join('`kuitansi` `k`',
					'k.`nokuitansi` = `n`.`nokuitansi`', 'left', false);
		$this->db->where('`rd`.`userbatal` IS NULL', null, false);
		$this->db->where('`b`.`idjnspelayanan`', 3);
		$this->db->groupby('`r`.`noreg`');
		
		if($tglakhir){
			$this->db->where('`rd`.`tglreg` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_vlapugdperiode();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_vlapugdperiode(){
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
		
        $this->db->select("
			`r`.`noreg` AS `noreg`,
			`rd`.`tglreg` AS `tglreg`,
			TRIM(LEADING '0' FROM `p`.`norm`) AS `norm`,
			`p`.`nmpasien` AS `nmpasien`,
			(
				SELECT sum(`t`.`tarifbhp`*`t`.`qty`)
				FROM
				  `notadet` `t`
				WHERE
				  ((`t`.`nonota` = `n`.`nonota`)
				  AND (`t`.`kditem` LIKE 'B%'))
			) AS `uobat`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `upemeriksaan`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000001'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `upemeriksaan`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `utindakan`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000048'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `utindakan`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `uimun`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000009'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `uimun`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `ulab`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000037'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `ulab`,
			(
				SELECT sum((`t`.`tarifjs`+`t`.`tarifjm`+`t`.`tarifjp`+`t`.`tarifbhp`)*`t`.`qty`) AS `ulain`
				FROM
				  `notadet` `t`, `pelayanan` `p`
				WHERE
				  `t`.`kditem` = `p`.`kdpelayanan`
				  AND `p`.`pel_kdpelayanan` = 'T000000073'
				  AND `t`.`nonota` = `n`.`nonota`
			) AS `ulain`,
			`n`.`uangr` AS `uracik`,
			sum(`nd`.`diskonjs` + `nd`.`diskonjm` + `nd`.`diskonjp` + `nd`.`diskonbhp` + `n`.`diskon`) AS `udiskon`,
			(
				SELECT sum(`kd`.`jumlah`)
				FROM
				  `kuitansidet` `kd`
				WHERE
				  `kd`.`nokuitansi` = `k`.`nokuitansi`
				  AND `kd`.`idcarabayar` <> 1
			) AS `ucc`,
			(
				SELECT sum(`kd`.`jumlah`)
				FROM
				  `kuitansidet` `kd`
				WHERE
				  `kd`.`nokuitansi` = `k`.`nokuitansi`
				  AND `kd`.`idcarabayar` = 1
			) AS `utotal`,
			`b`.`nmbagian` AS `nmbagian`,
			`b`.`idbagian` AS `idbagian`,
			`d`.`nmdoktergelar` AS `nmdoktergelar`,
			(
				SELECT sum(`kd`.`jumlah`)
				FROM
				  `kuitansidet` `kd`
				WHERE
				  `kd`.`nokuitansi` = `k`.`nokuitansi`
			) AS total
		", false);
        $this->db->from("`registrasi` `r`", false);
		$this->db->join('`pasien` `p`',
					'p.`norm` = `r`.`norm`', 'left', false);
		$this->db->join('`registrasidet` `rd`',
					'rd.`noreg` = `r`.`noreg`', 'left', false);
		$this->db->join('`bagian` `b`',
					'b.`idbagian` = `rd`.`idbagian`', 'left', false);
		$this->db->join('`dokter` `d`',
					'd.`iddokter` = `rd`.`iddokter`', 'left', false);
		$this->db->join('`nota` `n`',
					'n.`idregdet` = `rd`.`idregdet`', 'left', false);
		$this->db->join('`notadet` `nd`',
					'nd.`nonota` = `n`.`nonota`', 'left', false);
		$this->db->join('`kuitansi` `k`',
					'k.`nokuitansi` = `n`.`nokuitansi`', 'left', false);
		$this->db->where('`rd`.`userbatal` IS NULL', null, false);
		$this->db->where('`b`.`idjnspelayanan`', 3);
		$this->db->groupby('`r`.`noreg`');
		
		if($tglakhir){
			$this->db->where('`rd`.`tglreg` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_vlapobatdokter(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
        $cbagian	= $this->input->post("cbagian");
      
        $this->db->select("
			dokter.nmdoktergelar,
			kuitansi.nokuitansi,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			pasien.norm,
			pasien.nmpasien,
			notadet.kditem,
			barang.nmbrg,
			SUM(notadet.qty) AS qty,
			notadet.hrgjual as hrgjual,
			(SUM(notadet.qty) * ifnull(notadet.hrgjual, 0)) AS total,
			jpelayanan.nmjnspelayanan,			
			notadet.hrgbeli as hrgbeli
		", false);
        $this->db->from("barang");
        $this->db->join("notadet",
				"notadet.kditem = barang.kdbrg", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = registrasi.idjnspelayanan", "left");
				
		$this->db->groupby("barang.kdbrg, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, barang.nmbrg");
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($cbagian == '1') $this->db->where('registrasi.idjnspelayanan', 1, false);
		else if($cbagian == '2') $this->db->where('registrasi.idjnspelayanan', 2, false);
		else if($cbagian == '3') $this->db->where('registrasi.idjnspelayanan', 3, false);
		else $this->db->where_in('registrasi.idjnspelayanan',array(1,2,3));
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_vlapobatdokter();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_vlapobatdokter(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
        $cbagian	= $this->input->post("cbagian");
      
        $this->db->select("
			dokter.nmdoktergelar,
			kuitansi.nokuitansi,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			pasien.norm,
			pasien.nmpasien,
			notadet.kditem,
			barang.nmbrg,
			registrasi.idjnspelayanan
		", false);
        $this->db->from("barang");
        $this->db->join("notadet",
				"notadet.kditem = barang.kdbrg", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
		
		$this->db->groupby("barang.kdbrg, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi");
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($cbagian == '1') $this->db->where('registrasi.idjnspelayanan', 1, false);
		else if($cbagian == '2') $this->db->where('registrasi.idjnspelayanan', 2, false);
		else if($cbagian == '3') $this->db->where('registrasi.idjnspelayanan', 3, false);
		else $this->db->where_in('registrasi.idjnspelayanan',array(1,2,3));
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_vlapobatdokter_fl(){
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
      
        $this->db->select("
			   kuitansi.nokuitansi
			 , kuitansi.tglkuitansi
			 , kuitansi.atasnama as nmpasien
			 , notadet.kditem
			 , barang.nmbrg
			 , sum(notadet.qty) AS qty
			 , notadet.hrgjual AS hrgjual
			 , (sum(notadet.qty) * ifnull(notadet.hrgjual, 0)) AS total
			 , notadet.hrgbeli AS hrgbeli
		", false);
        $this->db->from("barang");
        $this->db->join("notadet",
				"notadet.kditem = barang.kdbrg", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
				
		$this->db->groupby("barang.kdbrg, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, barang.nmbrg");
		$this->db->where('nota.idjnstransaksi = 8');
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }

	function get_vlappelayanan(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
        $cberdasarkan1	= $this->input->post("cberdasarkan1");
        $cberdasarkan2	= $this->input->post("cberdasarkan2");
        $cberdasarkan3	= $this->input->post("cberdasarkan3");
        $cberdasarkan4	= $this->input->post("cberdasarkan4");
        $ccari1			= $this->input->post("ccari1");
        $ccari2			= $this->input->post("ccari2");
        $ccari3			= $this->input->post("ccari3");
        $ccari4			= $this->input->post("ccari4");	
		
		//var_dump($ccari1);
		//exit;
      
        $this->db->select("
			jpelayanan.nmjnspelayanan,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			pasien.norm,
			if(pasien.norm>0, pasien.nmpasien, kuitansi.atasnama) AS nmpasien,
			pelayanan.nmpelayanan,
			pel.nmpelayanan AS parent,
			(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) AS tarif,
			notadet.qty AS qty,
			(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS diskon,
			((notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) * notadet.qty) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS total,
			dokter.nmdoktergelar,
			notadet.tarifjm as tarifjmx,
			notadet.idnotadet as idnotadetx
		", false);
        $this->db->from("pelayanan");
        $this->db->join("pelayanan pel",
				"pel.kdpelayanan = pelayanan.pel_kdpelayanan", "left");
        $this->db->join("notadet",
				"notadet.kditem = pelayanan.kdpelayanan", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = notadet.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = bagian.idjnspelayanan", "left");
				
		$this->db->groupby("pelayanan.kdpelayanan, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, pelayanan.nmpelayanan");
		$this->db->where('kuitansi.idstkuitansi = 1');
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($cberdasarkan1 == '1' && $ccari1 != '') $this->db->like('registrasi.noreg', $ccari1, false);
		else if($cberdasarkan1 == '2' && $ccari1 != '') $this->db->like('pasien.norm', $ccari1, false);
		else if($cberdasarkan1 == '3' && $ccari1 != ''){
			//if($this->db->or_like('pasien.nmpasien', strtolower($ccari1), false) != null){
				//$this->db->or_like('pasien.nmpasien', strtolower($ccari1), false);
			//}else{
				$this->db->or_like('pasien.nmpasien', strtolower($ccari1), false);
				//$this->db->or_like('kuitansi.atasnama', strtolower($ccari1), false);
			//}
		}
		else if($cberdasarkan1 == '4' && $ccari1 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '5' && $ccari1 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '6' && $ccari1 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '7' && $ccari1 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari1), false);
		
		if($cberdasarkan2 == '1' && $ccari2 != '') $this->db->like('registrasi.noreg', $ccari2, false);
		else if($cberdasarkan2 == '2' && $ccari2 != '') $this->db->like('pasien.norm', $ccari2, false);
		else if($cberdasarkan2 == '3' && $ccari2 != ''){
			$this->db->or_like('LOWER(pasien.nmpasien)', strtolower($ccari2), false);
			//$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari2), false);
		}
		else if($cberdasarkan2 == '4' && $ccari2 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '5' && $ccari2 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '6' && $ccari2 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '7' && $ccari2 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari2), false);
		
		if($cberdasarkan3 == '1' && $ccari3 != '') $this->db->like('registrasi.noreg', $ccari3, false);
		else if($cberdasarkan3 == '2' && $ccari3 != '') $this->db->like('pasien.norm', $ccari3, false);
		else if($cberdasarkan3 == '3' && $ccari3 != ''){
			$this->db->or_like('LOWER(pasien.nmpasien)', strtolower($ccari3), false);
			//$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari3), false);
		}
		else if($cberdasarkan3 == '4' && $ccari3 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '5' && $ccari3 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '6' && $ccari3 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '7' && $ccari3 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari3), false);
		
		if($cberdasarkan4 == '1' && $ccari4 != '') $this->db->like('registrasi.noreg', $ccari4, false);
		else if($cberdasarkan4 == '2' && $ccari4 != '') $this->db->like('pasien.norm', $ccari4, false);
		else if($cberdasarkan4 == '3' && $ccari4 != ''){
			$this->db->or_like('LOWER(pasien.nmpasien)', strtolower($ccari4), false);
			//$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari4), false);
		}
		else if($cberdasarkan4 == '4' && $ccari4 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '5' && $ccari4 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '6' && $ccari4 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '7' && $ccari4 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari4), false);
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_vlappelayanan();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_vlappelayanan(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
        $cberdasarkan1	= $this->input->post("cberdasarkan1");
        $cberdasarkan2	= $this->input->post("cberdasarkan2");
        $cberdasarkan3	= $this->input->post("cberdasarkan3");
        $cberdasarkan4	= $this->input->post("cberdasarkan4");
        $ccari1			= $this->input->post("ccari1");
        $ccari2			= $this->input->post("ccari2");
        $ccari3			= $this->input->post("ccari3");
        $ccari4			= $this->input->post("ccari4");
      
        $this->db->select("
			jpelayanan.nmjnspelayanan,
			registrasi.noreg,
			kuitansi.tglkuitansi,
			pasien.norm,
			if(pasien.norm>0, pasien.nmpasien, kuitansi.atasnama) AS nmpasien,
			pelayanan.nmpelayanan,
			pel.nmpelayanan AS parent,
			(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) AS tarif,
			notadet.qty AS qty,
			(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS diskon,
			((notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) * notadet.qty) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) AS total,
			dokter.nmdoktergelar
		", false);
        $this->db->from("pelayanan");
        $this->db->join("pelayanan pel",
				"pel.kdpelayanan = pelayanan.pel_kdpelayanan", "left");
        $this->db->join("notadet",
				"notadet.kditem = pelayanan.kdpelayanan", "left");
        $this->db->join("nota",
				"nota.nonota = notadet.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg= registrasidet.noreg", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = notadet.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = bagian.idjnspelayanan", "left");
				
		$this->db->groupby("pelayanan.kdpelayanan, nota.nonota");
		$this->db->order_by("kuitansi.nokuitansi, pelayanan.nmpelayanan");
		$this->db->where('kuitansi.idstkuitansi = 1');
		
		if($tglakhir){
			$this->db->where('kuitansi.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if($cberdasarkan1 == '1' && $ccari1 != '') $this->db->like('registrasi.noreg', $ccari1, false);
		else if($cberdasarkan1 == '2' && $ccari1 != '') $this->db->like('pasien.norm', $ccari1, false);
		else if($cberdasarkan1 == '3' && $ccari1 != ''){
			$this->db->or_like('LOWER(pasien.nmpasien)', strtolower($ccari1), false);
			//$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari1), false);
		}
		else if($cberdasarkan1 == '4' && $ccari1 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '5' && $ccari1 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '6' && $ccari1 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari1), false);
		else if($cberdasarkan1 == '7' && $ccari1 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari1), false);
		
		if($cberdasarkan2 == '1' && $ccari2 != '') $this->db->like('registrasi.noreg', $ccari2, false);
		else if($cberdasarkan2 == '2' && $ccari2 != '') $this->db->like('pasien.norm', $ccari2, false);
		else if($cberdasarkan2 == '3' && $ccari2 != ''){
			$this->db->or_like('LOWER(pasien.nmpasien)', strtolower($ccari2), false);
			//$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari2), false);
		}
		else if($cberdasarkan2 == '4' && $ccari2 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '5' && $ccari2 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '6' && $ccari2 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari2), false);
		else if($cberdasarkan2 == '7' && $ccari2 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari2), false);
		
		if($cberdasarkan3 == '1' && $ccari3 != '') $this->db->like('registrasi.noreg', $ccari3, false);
		else if($cberdasarkan3 == '2' && $ccari3 != '') $this->db->like('pasien.norm', $ccari3, false);
		else if($cberdasarkan3 == '3' && $ccari3 != ''){
			$this->db->or_like('LOWER(pasien.nmpasien)', strtolower($ccari3), false);
			//$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari3), false);
		}
		else if($cberdasarkan3 == '4' && $ccari3 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '5' && $ccari3 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '6' && $ccari3 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari3), false);
		else if($cberdasarkan3 == '7' && $ccari3 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari3), false);
		
		if($cberdasarkan4 == '1' && $ccari4 != '') $this->db->like('registrasi.noreg', $ccari4, false);
		else if($cberdasarkan4 == '2' && $ccari4 != '') $this->db->like('pasien.norm', $ccari4, false);
		else if($cberdasarkan4 == '3' && $ccari4 != ''){
			$this->db->or_like('LOWER(pasien.nmpasien)', strtolower($ccari4), false);
			//$this->db->or_like('LOWER(kuitansi.atasnama)', strtolower($ccari4), false);
		}
		else if($cberdasarkan4 == '4' && $ccari4 != '') $this->db->like('LOWER(pelayanan.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '5' && $ccari4 != '') $this->db->like('LOWER(pel.nmpelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '6' && $ccari4 != '') $this->db->like('LOWER(jpelayanan.nmjnspelayanan)', strtolower($ccari4), false);
		else if($cberdasarkan4 == '7' && $ccari4 != '') $this->db->like('LOWER(dokter.nmdoktergelar)', strtolower($ccari4), false);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }

	function get_vlapksbelum(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
		
        $this->db->select("*");
        $this->db->from("v_kartustokbelum");
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_vlapksbelum();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_vlapksbelum(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        $tglawal	= $this->input->post("tglawal");
        $tglakhir	= $this->input->post("tglakhir");
        $cbagian	= $this->input->post("cbagian");
      
        $this->db->select("*");
        $this->db->from("v_kartustokbelum");
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_vlapksbelumrj(){
		/* $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
		
		$this->db->select("*");
        $this->db->from("v_lapksbelumrj");
		
		if($tglakhir){
			$this->db->where('v_lapksbelumrj.tglreg BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		} */
      
        /* $this->db->select("registrasi.noreg AS noreg
					 , registrasidet.tglreg AS tglreg
					 , pasien.norm AS norm
					 , pasien.nmpasien AS nmpasien
					 , dokter.nmdoktergelar AS nmdoktergelar
					 , bagian.nmbagian AS nmbagian
					 , bagian.idbagian AS idbagian
					 , jpelayanan.nmjnspelayanan AS nmjnspelayanan
					 , nota.nonota AS nonota
		", false);
        $this->db->from("registrasi");
        $this->db->join("registrasidet",
				"registrasidet.noreg = registrasi.noreg", "left");
        $this->db->join("nota",
				"nota.idregdet = registrasidet.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = registrasi.idjnspelayanan", "left");
        $this->db->join("bagian",
				"bagian.idbagian = registrasidet.idbagian", "left");
				
		$this->db->groupby("registrasi.noreg");
		$this->db->order_by("registrasidet.tglreg");
		
		$this->db->where(" NOT (registrasi.noreg IN (SELECT rd.noreg AS noreg
                                  FROM
                                    ((kartustok
                                  JOIN nota nt)
                                  JOIN registrasidet rd)
                                  WHERE
                                    ((kartustok.noref = nt.nonota)
                                    AND (nt.idregdet = rd.idregdet))))");
		$this->db->where("nota.idbagian = '4'");							
		$this->db->where("notadet.nonota IS NOT NULL");							
		$this->db->where("isnull(registrasidet.userbatal)");	 */						
		
		/* if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_vlapksbelumrj();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);		
	} */	
	
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
        $idbagian		= $this->input->post("idbagian");
	
	$q = ("SELECT `registrasi`.`noreg` AS `noreg`
				 , `registrasidet`.`tglreg` AS `tglreg`
				 , `nota`.nonota AS nonota
				 , `nota`.tglnota AS tglnota
				 , `pasien`.`norm` AS `norm`
				 , `pasien`.`nmpasien` AS `nmpasien`
				 , `dokter`.`nmdoktergelar` AS `nmdoktergelar`
				 , `bagian`.`nmbagian` AS `nmbagian`
				 , `bagian`.`idbagian` AS `idbagian`
				 , `jpelayanan`.`nmjnspelayanan` AS `nmjnspelayanan`
				 , `nota`.`nonota` AS `nonota`
			FROM
			  (((((((`registrasi`
			LEFT JOIN `registrasidet`
			ON ((`registrasidet`.`noreg` = `registrasi`.`noreg`)))
			LEFT JOIN `nota`
			ON ((`nota`.`idregdet` = `registrasidet`.`idregdet`)))
			LEFT JOIN `notadet`
			ON ((`notadet`.`nonota` = `nota`.`nonota`)))
			LEFT JOIN `pasien`
			ON ((`pasien`.`norm` = `registrasi`.`norm`)))
			LEFT JOIN `dokter`
			ON ((`dokter`.`iddokter` = `registrasidet`.`iddokter`)))
			LEFT JOIN `jpelayanan`
			ON ((`jpelayanan`.`idjnspelayanan` = `registrasi`.`idjnspelayanan`)))
			LEFT JOIN `bagian`
			ON ((`bagian`.`idbagian` = `registrasidet`.`idbagian`)))
			WHERE
			  ((NOT (`registrasi`.`noreg` IN (SELECT `rd`.`noreg` AS `noreg`
											  FROM
												((`kartustok`
											  JOIN `nota` `nt`)
											  JOIN `registrasidet` `rd`)
											  WHERE
												((`kartustok`.`noref` = `nt`.`nonota`)
												AND (`nt`.`idregdet` = `rd`.`idregdet`)))))
			  AND (`nota`.`idbagian` = '".$idbagian."')
			  AND (`notadet`.`nonota` IS NOT NULL)
			  AND (`notadet`.koder IS NOT NULL)
			  AND isnull(`registrasidet`.`userbatal`)
			  AND `registrasidet`.tglreg BETWEEN '". $tglawal ."' AND '". $tglakhir ."')
			GROUP BY
			  `registrasi`.`noreg`
			ORDER BY
			  `registrasidet`.`tglreg`");
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	/* function numrow_vlapksbelumrj(){		
		$start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
		
		$this->db->select("*");
        $this->db->from("v_lapksbelumrj");
		
		if($tglakhir){
			$this->db->where('v_lapksbelumrj.tglreg BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		$this->db->select("registrasi.noreg AS noreg
					 , registrasidet.tglreg AS tglreg
					 , pasien.norm AS norm
					 , pasien.nmpasien AS nmpasien
					 , dokter.nmdoktergelar AS nmdoktergelar
					 , bagian.nmbagian AS nmbagian
					 , bagian.idbagian AS idbagian
					 , jpelayanan.nmjnspelayanan AS nmjnspelayanan
					 , nota.nonota AS nonota
		", false);
        $this->db->from("registrasi");
        $this->db->join("registrasidet",
				"registrasidet.noreg = registrasi.noreg", "left");
        $this->db->join("nota",
				"nota.idregdet = registrasidet.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
        $this->db->join("jpelayanan",
				"jpelayanan.idjnspelayanan = registrasi.idjnspelayanan", "left");
        $this->db->join("bagian",
				"bagian.idbagian = registrasidet.idbagian", "left");
				
		$this->db->groupby("registrasi.noreg");
		$this->db->order_by("registrasidet.tglreg");
		
		$this->db->where(" NOT (registrasi.noreg IN (SELECT rd.noreg AS noreg
                                  FROM
                                    ((kartustok
                                  JOIN nota nt)
                                  JOIN registrasidet rd)
                                  WHERE
                                    ((kartustok.noref = nt.nonota)
                                    AND (nt.idregdet = rd.idregdet))))");
		$this->db->where("nota.idbagian = '4'");							
		$this->db->where("notadet.nonota IS NOT NULL");							
		$this->db->where("isnull(registrasidet.userbatal)");
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
		
        $q = $this->db->get();
        
        return $q->num_rows();
    } */

  function get_penerimaan_bank(){
    $tglawal = $this->input->post('tglawal');
    $tglakhir = $this->input->post('tglakhir');
    $idbank = $this->input->post('idbank');
    
    if(empty($tglawal)) $tglawal = date('Y-m-d');
    if(empty($tglakhir)) $tglakhir = date('Y-m-d');
    if(empty($idbank)) $idbank = 1;

    $query = "SELECT * FROM 
              (((
                  kuitansi join 
                  kuitansidet ON 
                    (
                      kuitansi.nokuitansi = kuitansidet.nokuitansi AND
                      kuitansi.idstkuitansi = 1 AND 
                      kuitansidet.idcarabayar = 2
                    ) 
                  ) left join bank on kuitansidet.idbank = bank.idbank) 
                  left join jkuitansi on kuitansi.idjnskuitansi = jkuitansi.idjnskuitansi)
              having 
                kuitansi.tglkuitansi >= '".$tglawal."' AND 
                kuitansi.tglkuitansi <= '".$tglakhir."' AND 
                kuitansidet.idbank = ".$idbank."";
    $get = $this->db->query($query);
    $ttl = $get->num_rows();
    $data = $get->result();        
    $build_array = array ("success"=>true,"results"=>$ttl,"data"=>$data);

    echo json_encode($build_array);
  }  
	
	function updatenotadet(){
		$dataArray = array(
             'iddokter'	=> ($_POST['iddokter']) ? $_POST['iddokter']:null,
        );
		
		$this->db->where('idnotadet', $_POST['idnotadet']);
		$this->db->update('notadet', $dataArray); 

		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }

}
