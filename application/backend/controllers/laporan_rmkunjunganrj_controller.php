<?php 
class Laporan_rmkunjunganrj_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_laprmkunjunganrj(){

		$tglawal = $this->input->post("tglawal");
        $tglakhir = $this->input->post("tglakhir");
		
		/* $q = "SELECT k.tglkuitansi
					 , r.noreg
					 , trim(LEADING '0' FROM `p`.`norm`) AS `norm`
					 , if(((SELECT min(registrasidet.idregdet) AS stp
							FROM
							  registrasidet
							LEFT JOIN registrasi
							ON registrasi.noreg = registrasidet.noreg
							LEFT JOIN nota
							ON nota.idregdet = registrasidet.idregdet
							LEFT JOIN kuitansi
							ON kuitansi.nokuitansi = nota.nokuitansi
							WHERE
							  registrasi.norm = p.norm
							  AND
							  registrasi.idjnspelayanan = '1'
							  AND
							  registrasidet.userbatal IS NULL
							  AND
							  kuitansi.idstkuitansi = 1
					   ) = rd.idregdet), 'Baru', 'Lama') AS stpasien
					 , p.nmpasien
					 , rd.umurtahun
					 , p.idjnskelamin
					 , jkel.nmjnskelamin
					 , p.alamat
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , p.notelp
					 , rd.iddokter
					 , dk.nmdoktergelar
					 , r.idjnspelayanan
					 , jpel.nmjnspelayanan
					 , r.idpenjamin
					 , pj.nmpenjamin
				FROM
				  registrasi r
				LEFT JOIN registrasidet rd
				ON r.noreg = rd.noreg
				LEFT JOIN pasien p
				ON p.norm = r.norm
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = p.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN jkelamin jkel
				ON jkel.idjnskelamin = p.idjnskelamin
				LEFT JOIN jpelayanan jpel
				ON jpel.idjnspelayanan = r.idjnspelayanan
				LEFT JOIN penjamin pj
				ON pj.idpenjamin = r.idpenjamin
				LEFT JOIN dokter dk
				ON dk.iddokter = rd.iddokter
				LEFT JOIN nota n
				ON n.idregdet = rd.idregdet
				LEFT JOIN kuitansi k
				ON k.nokuitansi = n.nokuitansi
				WHERE
				  r.idjnspelayanan IN(1,3)
				  AND
				  k.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'
				  AND
				  rd.userbatal IS NULL
				GROUP BY
				  r.noreg
				ORDER BY
				  k.tglkuitansi, r.noreg"; */
		$q = "SELECT k.tglkuitansi
					 , r.noreg
					 , trim(LEADING '0' FROM `p`.`norm`) AS `norm`
					 , if(((SELECT min(registrasidet.idregdet) AS stp
							FROM
							  registrasidet
							LEFT JOIN registrasi
							ON registrasi.noreg = registrasidet.noreg
							LEFT JOIN nota
							ON nota.idregdet = registrasidet.idregdet
							LEFT JOIN kuitansi
							ON kuitansi.nokuitansi = nota.nokuitansi
							WHERE
							  registrasi.norm = p.norm
							  AND
							  registrasi.idjnspelayanan = '1'
							  AND
							  registrasidet.userbatal IS NULL
							  AND
							  kuitansi.idstkuitansi = 1
					   ) = rd.idregdet), 'Baru', 'Lama') AS stpasien
					 , p.nmpasien
					 , peny.nmpenyakit as nmpenyakiteng
					 , rd.umurtahun
					 , p.idjnskelamin
					 , jkel.nmjnskelamin
					 , p.alamat
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , concat(p.notelp, '/', p.nohp) AS notelp
					 , rd.iddokter
					 , dk.nmdoktergelar
					 , r.idjnspelayanan
					 , jpel.nmjnspelayanan
					 , r.idpenjamin
					 , pj.nmpenjamin
				FROM
				  kodifikasidet kdt
				LEFT JOIN kodifikasi kd
				ON kdt.idkodifikasi = kd.idkodifikasi
				LEFT JOIN penyakit peny
				ON peny.idpenyakit = kdt.idpenyakit
				LEFT JOIN registrasi r
				ON r.noreg = kd.noreg
				LEFT JOIN registrasidet rd
				ON r.noreg = rd.noreg
				LEFT JOIN pasien p
				ON p.norm = r.norm
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = p.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN jkelamin jkel
				ON jkel.idjnskelamin = p.idjnskelamin
				LEFT JOIN jpelayanan jpel
				ON jpel.idjnspelayanan = r.idjnspelayanan
				LEFT JOIN penjamin pj
				ON pj.idpenjamin = r.idpenjamin
				LEFT JOIN dokter dk
				ON dk.iddokter = rd.iddokter
				LEFT JOIN nota n
				ON n.idregdet = rd.idregdet
				LEFT JOIN kuitansi k
				ON k.nokuitansi = n.nokuitansi
				WHERE
				  r.idjnspelayanan IN (1, 3)
				  AND
				  k.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'
				  AND
				  rd.userbatal IS NULL
				  AND
				  k.idstkuitansi = 1
				GROUP BY
				  kd.idkodifikasi
				, kdt.idpenyakit
				ORDER BY
				  k.tglkuitansi
				, r.noreg";
		$query = $this->db->query($q);
		
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
		
		$ttl = count($data);
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
		$noreg = '';
        $datax = array();
		$zx = 0;
		
		foreach($data as $x=>$val){
			if($noreg == $val->noreg){
			//var_dump($val->senin);
				$zx--;
				$datax[$zx]->nmpenyakiteng .= '<br>'.$val->nmpenyakiteng;
				$zx++;
			} else {
				$datax[$zx] = $val;
				$zx++;
			}
			$noreg = $val->noreg;
			
		}
		
        if($ttl>0){
            $build_array["data"]=$datax;
        }
		
		echo json_encode($build_array);
	}

}
