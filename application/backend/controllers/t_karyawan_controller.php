<?php
class T_karyawan_controller extends Controller{
		public function __construct()
		{
			parent::Controller();
				$this->load->library('session');
				$this->load->library('rhlib');
		}
		
		function retValOrNull($val){
		//especially for combo & looukup with no item selected
		$val = ($val=='')? null : $val;
		return $val;
		}
		
		function numrowregkar(){
		$fields				    = $this->input->post("fields");
        $keyword				= $this->input->post("keyword");
		$checkdate				= $this->input->post("checkdate");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		
        $this->db->select("*");
		$this->db->from("v_treg1");
		if ($checkdate=="true") {
			$this->db->where('`tglreg` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
    
       if ($keyword) {
			$this->db->or_like($fields, $keyword);
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }

	function get_tkar(){
	
		$this->db->select("*");
        $this->db->from("v_treg1");
       // $this->db->order_by("v_treg1.noreg ASC");			
			
		if ($this->input->post("cbxstbayar") == 'true'){
			if ($this->input->post("status") != 'Semua'){
				$this->db->where('stlunas =', $this->input->post("status"));
			}
		}
		
		if ($this->input->post("cbxsearch") == 'true'){
			$this->db->or_like($this->input->post("key"), $this->input->post("value"));
		}
		
		if ($this->input->post("cbxreg") == 'true'){
			$this->db->where("date(tglkuitansi) between '". $this->input->post("tglkuitansi1") ."' and '". $this->input->post("tglkuitansi2")."'" );
		}
		
		
	
		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	
	function get_tkar_lap(){
	
		$this->db->select("*");
        $this->db->from("v_treg_lap");
        $this->db->order_by("v_treg_lap.noreg ASC");			
		$this->db->where("date(tglkuitansi) between '". $this->input->post("tglkuitansi1") ."' and '". $this->input->post("tglkuitansi2")."'" );

		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_tfarmasi_lap(){
	
		$this->db->select("*");
        $this->db->from("v_tagihan_farmasilap");
       // $this->db->order_by("v_tagihan_farmasilap.nonota ASC");			
		$this->db->where("date(tglkuitansi) between '". $this->input->post("tglkuitansi1") ."' and '". $this->input->post("tglkuitansi2")."'" );

		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_tfarmasi(){
	
		$this->db->select("*");
        $this->db->from("v_tagihan_farmasi");
        //$this->db->order_by("v_tagihan_farmasi.nonota ASC");			
			
		if ($this->input->post("cbxstbayar2") == 'true'){
			if ($this->input->post("status2") != 'Semua'){
				$this->db->where('stlunas =', $this->input->post("status2"));
			}
		}
		
		if ($this->input->post("cbxsearch2") == 'true'){
			$this->db->or_like($this->input->post("key"), $this->input->post("value"));
		}
		
		if ($this->input->post("cbxreg2") == 'true'){
			$this->db->where("date(tglkuitansi) between '". $this->input->post("tglkuitansi3") ."' and '". $this->input->post("tglkuitansi4")."'" );
		}
		
		
	
		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_tfarmasi_laporan(){
	
		$this->db->select("*");
        $this->db->from("v_tagihan_farmasi");
        $this->db->order_by("v_tagihan_farmasi.nonota ASC");			
		
		
		if ($this->input->post("cbxreg") == 'true'){
			$this->db->where("date(tglkuitansi) between '". $this->input->post("tglkuitansi1") ."' and '". $this->input->post("tglkuitansi2")."'" );
		}

		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_kuitansipg(){
		$q = "select kuitansidet.*
			  , carabayar.nmcarabayar
			  , pengguna.nmlengkap
			  , kuitansipg.nokuitansipg
				, kuitansipg.tglkuitansipgreg
				, kuitansipg.jumlah AS jml_pg
				, kuitansipg.approval
				, kuitansipg.userid
        , kuitansipg.nama_karyawan
        ,(
          SELECT jurnal.kdjurnal 
          FROM jurnal 
          WHERE jurnal.idjnsjurnal = 2
          AND jurnal.idjnstransaksi = 13
          AND jurnal.status_posting = 1
          AND jurnal.noreff = kuitansipg.nokuitansipg) as kdjurnal
			  from kuitansipg
			  left join kuitansidet
			  on kuitansidet.idkuitansidet = kuitansipg.idkuitansidet
			  left join carabayar
			  on carabayar.idcarabayar = kuitansidet.idcarabayar
			  left join pengguna
			  on pengguna.userid = kuitansipg.userid
			  WHERE kuitansipg.idkuitansidet='".$this->input->post("idkuitansidet")."'";
			
		$this->rhlib->jsonFromQuery($q);
	}
	
	function numrow($fields, $query){
      
        $this->db->select("*");
        $this->db->from("kuitansipg");
    
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function app1(){
	$sql =	$this->db->query("select * from setting where kdset = 'APG01'");
	$num = $sql->num_rows();
			if($num>0){
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);			
			}else{
				return 0;
			} 
	
	}
	
	function update_nokartu(){ 				
		
		$dataArray_kuitansidet = array(
				'nokartu' => $this->retValOrNull($this->input->post("nama_karyawan"))

			);
		
		//UPDATE
		$where['idkuitansidet'] = $_POST['idkuitansidet'];
		$this->db->where('idkuitansidet', $_POST['idkuitansidet']);
		$this->db->update('kuitansidet', $dataArray_kuitansidet); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
	
	function delete_kuitansipg(){     
		$where['nokuitansipg'] = $_POST['nokuitansipg'];
		$ret = $this->rhlib->deleteRecord('kuitansipg',$where);
		 
		return $ret;
    }
	
	function simpan(){
		
		$dataArray = array(
				'nokuitansipg' => $this->retValOrNull($this->input->post("nokuitansipg")),
				'tglkuitansipgreg' => $this->retValOrNull($this->input->post("tglkuitansipgreg")),
				'idkuitansidet' => $this->retValOrNull($this->input->post("idkuitansidet")),
				'userid' => $this->retValOrNull($this->input->post("userid")),
				'approval' => $this->retValOrNull($this->input->post("approval")),
				'jumlah' => $this->retValOrNull($this->input->post("jumlah")),
				'catatan' => $this->retValOrNull($this->input->post("catatan")),
				'nama_karyawan' => $this->retValOrNull($this->input->post("nama_karyawan")),

			);
			
			
		$ret = $this->rhlib->insertRecord('kuitansipg',$dataArray);	
		
		
				if($ret){
					$this->update_nokartu();
					$return["success"]=true;
					$return["message"]="Simpan Data Berhasil";
				} else {
					$return["success"]=false;
					$return["message"]="Simpan Data Detail gagal";
				}
				
		echo json_encode($return);		
	
	
	}
	
	
	
	
	
	function get_autoNOKWI(){
		$tgl = $_POST['tglkuitansipgreg'];
		$q = "SELECT get_oto_tkar('".$tgl."') as nokuitansipg";
		$query = $this->db->query($q);
		if ($query->num_rows() == 1){ 
			$nokuitansipg = $query->row()->nokuitansipg;		
			echo json_encode(array ("success"=>true,"nokuitansipg"=>$nokuitansipg));
		}
		else json_encode(array ("success"=>false,"nokuitansipg"=>"0"));
	}


  function get_pg_jurnaling(){
    $nokuitansipg     = $this->input->post("nokuitansipg");
    $kdakun_kredit    = '12110'; // piutang PG karyawan
    //$kdakun_debit     = '11100'; // kas kecil
    $kdakun_debit     = '61200'; // Biaya Gaji Karyawan

    if(empty($nokuitansipg)) $nokuitansipg = '0';
    
    $data_pg = $this->db->get_where('kuitansipg', array('nokuitansipg' => $nokuitansipg))->row_array();

    $akun_kredit = $this->db->get_where('akun', array('kdakun' => $kdakun_kredit))->row_array();
    $akun_debit = $this->db->get_where('akun', array('kdakun' => $kdakun_debit))->row_array(); 

    $breakdown_jurnal = array();

    $breakdown_jurnal[] = array(
      'idakun' => $akun_debit['idakun'], 
      'kdakun' => $akun_debit['kdakun'], 
      'nmakun' => $akun_debit['nmakun'], 
      'noreff' => '',
      'debit' => $data_pg['jumlah'], 
      'kredit'=> 0
    );

    $breakdown_jurnal[] = array(
      'idakun' => $akun_kredit['idakun'], 
      'kdakun' => $akun_kredit['kdakun'], 
      'nmakun' => $akun_kredit['nmakun'], 
      'noreff' => '',
      'debit' => 0, 
      'kredit'=> $data_pg['jumlah']
    );

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);

    echo json_encode($build_array);
  }

  function posting_pg_karyawan(){

    $nokuitansipg  = $this->input->post("nokuitansipg_posting");
    $tglkuitansipgreg  = $this->input->post("tglkuitansipgreg_posting");
    $nama_karyawan  = $this->input->post("nama_karyawan");
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();

    if(empty($nokuitansipg) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nokuitansipg dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglkuitansipgreg,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Potong Gaji : '.$nama_karyawan,
      'noreff' => $nokuitansipg,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => 13, // 13 = Potong Gaji Karyawan
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //insert jurnal det
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        if(isset($item_jdet[4]) && isset($item_jdet[5]))
        {
          /*
            item_jdet[0] = idakun   
            item_jdet[1] = kdakun 
            item_jdet[2] = nmakun 
            item_jdet[3] = noreff 
            item_jdet[4] = debit 
            item_jdet[5] = kredit
          */
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;

        }

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  


  function get_nojurnal_khusus(){
    //$q = "SELECT getOtoNojurnal(now()) as nm;";
    $q = "SELECT getOtoNoJurnalKhusus(now()) as nm;";
    $query  = $this->db->query($q);
    $nm= ''; 
                
    if ($query->num_rows() != 0)
    {
        $row = $query->row();
        $nm=$row->nm;
    }
    return $nm;
  }

	
}	