<?php 
class Laporan_bor_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
			$this->load->library('rhlib');
		}
	
	function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=1;
        }
        return $max;
    }
	
	function get_date_server() {
		$data['date'] = date('d-m-Y');
		echo json_encode($data);
    }
	
	function get_countbed($tglawal,$tglakhir,$idklstarif,$idpenjamin){
		#$q = "select count(idbed) as countbed from bed where idextrabed=2 and idstatus=1";
				
		if($idklstarif == "" AND $idpenjamin == ""){
			$q = "SELECT count(idbed) AS countbed
					FROM
					  bed
					WHERE
					  bed.idextrabed = 2
					  AND bed.idstatus = 1";					  
					  
			$query  = $this->db->query($q);
			$datarow= null; 
						
			if ($query->num_rows() != 0)
			{
				$row = $query->row();
				$datarow=$row->countbed;
			}
			return $datarow;
		}/* if($idklstarif != "" AND $idpenjamin != ""){
			$q = "SELECT count(countbed) AS countbed
					FROM
					  v_count_bed_bor
					WHERE
					  idklstarif = '$idklstarif'
					  AND idpenjamin = '$idpenjamin'
					  AND tglmasuk BETWEEN '$tglawal' AND '$tglakhir'";
					 
			$query  = $this->db->query($q);
			$datarow= null; 
						
			if ($query->num_rows() != 0)
			{
				$row = $query->row();
				$datarow=$row->countbed;
			}
			return $datarow;
		} */if($idklstarif != ""){
			$q = "SELECT count(bed.idbed) as countbed
					FROM
					  bed
					LEFT JOIN kamar
					ON kamar.idkamar = bed.idkamar
					LEFT JOIN bagian
					ON bagian.idbagian = kamar.idbagian
					LEFT JOIN klsrawat
					ON klsrawat.idbagian = bagian.idbagian
					WHERE
					  idextrabed = 2
					  AND
					  idstatus = 1
					   AND klsrawat.idklstarif = '$idklstarif'";
					 
			$query  = $this->db->query($q);
			$datarow= null; 
						
			if ($query->num_rows() != 0)
			{
				$row = $query->row();
				$datarow=$row->countbed;
			}
			return $datarow;
		}if($idpenjamin != ""){
			$q = "SELECT count(idbed) AS countbed
					FROM
					  bed
					WHERE
					  bed.idextrabed = 2
					  AND bed.idstatus = 1";					  
					  
			$query  = $this->db->query($q);
			$datarow= null;  
						
			if ($query->num_rows() != 0)
			{
				$row = $query->row();
				$datarow=$row->countbed;
			}
			return $datarow;
		}
	}
	
	function get_jmlrawat_masuk($tglawal,$tglakhir,$idklstarif,$idpenjamin){	
		/* $q = "SELECT * FROM v_lamarawat
			WHERE 
				idjnspelayanan = 2
				AND (cast(tglmasuk AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
        $query  = $this->db->query($q);
        $result = array(); 
        $count = 0;            
        if ($query->num_rows() != 0)
        {
            $result = $query->result();
            foreach ($result as $data) {
				$count = $count + $data->lamarawat;
			}
        }
        return $count; */
		
		if($idklstarif == "" AND $idpenjamin == ""){
			$q = "SELECT * FROM v_lamarawat
				WHERE 
					idjnspelayanan = 2
					AND (cast(tglmasuk AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
			$query  = $this->db->query($q);
			$result = array(); 
			$count = 0;            
			if ($query->num_rows() != 0)
			{
				$result = $query->result();
				foreach ($result as $data) {
					$count = $count + $data->lamarawat;
				}
			}
			return $count;
		}if($idklstarif != "" AND $idpenjamin != ""){
			$q = "SELECT * FROM v_lamarawat
				WHERE 
					idjnspelayanan = 2
					AND idklstarif = '$idklstarif'
					AND idpenjamin = '$idpenjamin'
					AND (cast(tglmasuk AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
			$query  = $this->db->query($q);
			$result = array(); 
			$count = 0;            
			if ($query->num_rows() != 0)
			{
				$result = $query->result();
				foreach ($result as $data) {
					$count = $count + $data->lamarawat;
				}
			}
			return $count;
		}if($idklstarif != ""){
			$q = "SELECT * FROM v_lamarawat
				WHERE 
					idjnspelayanan = 2
					AND idklstarif = '$idklstarif'
					AND (cast(tglmasuk AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
			$query  = $this->db->query($q);
			$result = array(); 
			$count = 0;            
			if ($query->num_rows() != 0)
			{
				$result = $query->result();
				foreach ($result as $data) {
					$count = $count + $data->lamarawat;
				}
			}
			return $count;
		}if($idpenjamin != ""){
			$q = "SELECT * FROM v_lamarawat
				WHERE 
					idjnspelayanan = 2
					AND idpenjamin = '$idpenjamin'
					AND (cast(tglmasuk AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
			$query  = $this->db->query($q);
			$result = array(); 
			$count = 0;            
			if ($query->num_rows() != 0)
			{
				$result = $query->result();
				foreach ($result as $data) {
					$count = $count + $data->lamarawat;
				}
			}
			return $count;
		}
		
	}
	
	function get_jmlrawat_keluar($tglawal,$tglakhir,$idklstarif,$idpenjamin){
		/* $q = "SELECT * , ifnull(((to_days(tglkeluar) - to_days('$tglawal')) + 1), 0) AS lamarawat1
			FROM
			  v_lamarawat
			WHERE
				idjnspelayanan = 2
				AND idklstarif = 1
				AND idpenjamin = 3
				AND ((cast(tglmasuk AS DATE) < '$tglawal')
				AND (cast(tglkeluar AS DATE) BETWEEN '$tglawal' AND '$tglakhir'))";
        $query  = $this->db->query($q);
        $result = array(); 
        $count = 0;            
        if ($query->num_rows() != 0)
        {
            $result = $query->result();
            foreach ($result as $data) {
				$count = $count + $data->lamarawat1;
			}
        }
        return $count; */
		
		if($idklstarif == "" AND $idpenjamin == ""){
			$q = "SELECT * , ifnull(((to_days(tglkeluar) - to_days('$tglawal')) + 1), 0) AS lamarawat1
				FROM
				  v_lamarawat
				WHERE
					idjnspelayanan = 2
					 AND (cast(tglkeluar AS DATE) BETWEEN '$tglawal' AND '$tglakhir')";
			$query  = $this->db->query($q);
			$result = array(); 
			$count = 0;            
			if ($query->num_rows() != 0)
			{
				$result = $query->result();
				foreach ($result as $data) {
					$count = $count + $data->lamarawat;
				}
			}
			return $count;
		}if($idklstarif != "" AND $idpenjamin != ""){
			$q = "SELECT * , ifnull(((to_days(tglkeluar) - to_days('$tglawal')) + 1), 0) AS lamarawat1
				FROM
				  v_lamarawat
				WHERE
					idjnspelayanan = 2
					AND idklstarif = '$idklstarif'
					AND idpenjamin = '$idpenjamin'
					AND ((cast(tglmasuk AS DATE) < '$tglawal')
					AND (cast(tglkeluar AS DATE) BETWEEN '$tglawal' AND '$tglakhir'))";
			$query  = $this->db->query($q);
			$result = array(); 
			$count = 0;            
			if ($query->num_rows() != 0)
			{
				$result = $query->result();
				foreach ($result as $data) {
					$count = $count + $data->lamarawat;
				}
			}
			return $count;
		}if($idklstarif != ""){
			$q = "SELECT * , ifnull(((to_days(tglkeluar) - to_days('$tglawal')) + 1), 0) AS lamarawat1
				FROM
				  v_lamarawat
				WHERE
					idjnspelayanan = 2
					AND idklstarif = '$idklstarif'
					AND ((cast(tglmasuk AS DATE) < '$tglawal')
					AND (cast(tglkeluar AS DATE) BETWEEN '$tglawal' AND '$tglakhir'))";
			$query  = $this->db->query($q);
			$result = array(); 
			$count = 0;            
			if ($query->num_rows() != 0)
			{
				$result = $query->result();
				foreach ($result as $data) {
					$count = $count + $data->lamarawat;
				}
			}
			return $count;
		}if($idpenjamin != ""){
			$q = "SELECT * , ifnull(((to_days(tglkeluar) - to_days('$tglawal')) + 1), 0) AS lamarawat1
				FROM
				  v_lamarawat
				WHERE
					idjnspelayanan = 2
					AND idpenjamin = '$idpenjamin'
					AND ((cast(tglmasuk AS DATE) < '$tglawal')
					AND (cast(tglkeluar AS DATE) BETWEEN '$tglawal' AND '$tglakhir'))";
			$query  = $this->db->query($q);
			$result = array(); 
			$count = 0;            
			if ($query->num_rows() != 0)
			{
				$result = $query->result();
				foreach ($result as $data) {
					$count = $count + $data->lamarawat;
				}
			}
			return $count;
		}		
	}
	
	function get_items(){
		$tglawal = $this->input->post("tglawal");
		$tglakhir = $this->input->post("tglakhir");
		$interval = $this->input->post("interval");
		$idklstarif = $this->input->post("idklstarif");
		$idpenjamin = $this->input->post("idpenjamin");
		
		$jmlmasuk = $this->get_jmlrawat_masuk($tglawal,$tglakhir,$idklstarif,$idpenjamin);
		$jmlkeluar = $this->get_jmlrawat_keluar($tglawal,$tglakhir,$idklstarif,$idpenjamin);
		
		$data['countbed'] = $this->get_countbed($tglawal,$tglakhir,$idklstarif,$idpenjamin);
        $data['jmlrawat'] = $jmlkeluar;
		
		$data['result'] =   ($data['jmlrawat']==0) ? 0:intval(($data['jmlrawat'] * (100)) / ($data['countbed'] * $interval));
		
        echo json_encode($data);
	}
	
	function simpan_hasil(){
		$dataArray = array(
			 'idbor'=> $this->autoNumber('idbor','bor'),
             'daritgl'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglawal']))),
             'sampaitgl'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglakhir']))),
			 'jmlperiode'=> $this->rhlib->retValOrNull($_POST['jhdsp']),
			 'jmltempattidur'=> $this->rhlib->retValOrNull($_POST['jtt']),
             'jmlhariperawatan'=> $this->rhlib->retValOrNull($_POST['jhpdrs']),
			 'hasilpersen'=> $this->rhlib->retValOrNull($_POST['hasil']),
			 
			 'catatan'=> $this->rhlib->retValOrNull($_POST['catatan']),
             'tglbuat'=> $this->rhlib->retValOrNull(date('Y-m-d',strtotime($_POST['tglproses']))),
			 'userid'=> $this->rhlib->retValOrNull($_POST['userid']),
        );
		
		$ret = $this->rhlib->insertRecord('bor',$dataArray);
        echo json_encode($ret);
    }
	
	function delete_history(){     
		$where['idbor'] = $_POST['idbor'];
		$del = $this->rhlib->deleteRecord('bor',$where);
        return $del;
    }
	
	function get_history(){ //ISTRA
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("v_bor");
 
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(10,0);
        }
        
        $q = $this->db->get(); 
       
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->db->query("SELECT * FROM v_bor")->num_rows(); 
        
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }

}
