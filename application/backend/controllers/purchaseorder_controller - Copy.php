<?php 

class Purchaseorder_Controller extends Controller {
	public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_PO(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$po					    = $this->input->post("idbagian");
		$tahun 					= $this->input->post("tahun");
		$bulan 					= $this->input->post("bulan");
		$stpo 					= $this->input->post("statuspo");
		$setuju					= $this->input->post("setuju");
		$key					= $_POST["key"]; 
	//	$nopp = $this->input->post("nopp");
		$this->db->select("*");
		$this->db->from("po");
	//	$this->db->join("podet", "po.nopo = podet.nopo", "left");
		$this->db->join("pp", "po.nopp = pp.nopp", "left");
		$this->db->join("supplier", "supplier.kdsupplier = po.kdsupplier","left");
		$this->db->join("stsetuju", "stsetuju.idstsetuju = po.idstsetuju","left");
		$this->db->join("jpembayaran", "jpembayaran.idjnspembayaran = po.idjnspembayaran","left");
		$this->db->join("pengguna", "po.userid = pengguna.userid", "left");
		
		$this->db->join("stpo","stpo.idstpo = pp.idstpo","left");
	//	$this->db->where("nopo", $_POST['tf_nopo']);
		$this->db->order_by('nopo DESC');
	//	if($nopp)$this->db->where("nopp", $nopp);
		if($tahun != '')$this->db->like('tglpo', $tahun);
		if($bulan != '')$this->db->like('month(tglpo)', $bulan);
		
		if($stpo != '')$this->db->where('po.idstpo',$stpo);
		if($setuju != '')$this->db->where('po.idstsetuju',$setuju);
		if($this->input->post('chb_nopo')=='true'){	
			if ($key=='1'){
				$id     = $_POST["id"];
				$name   = $_POST["name"];
				$this->db->or_like($id, $name);
			}  
	    }
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('po');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	
	function insorupd_po(){
		$this->db->trans_begin();
		$arrpo = $this->input->post("arrpo");
		$nopo = $this->input->post("nopo");
		
		$query = $this->db->getwhere('po',array('nopo'=>$nopo));
		$po = $query->row_array();
		
		if($query->num_rows() == 0){
			$pos = $this->insert_po($po);
			$vpo = $pos['nopo'];
		}else{
			$pos = $query->row_array();
			$vpo = $pos['nopo'];
		}
		
		$podet = $this->insert_podet($vpo, $arrpo);
		
		if($vpo && $podet)
		{
            $this->db->trans_commit();
			$this->update_pp();
			$ret["success"] = true;
            $ret["nopo"]=$vpo;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		echo json_encode($ret);
  
		
	}
	
	function insert_po(){
		$dataArray = $this->getFieldsAndValues();
		$z = $this->db->insert('po',$dataArray);
		if($z){
			$ret = $dataArray;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	function insert_podet($nopo, $arrpo){
		//$where['nopol'] = $nopol;
	//	$this->db->delete('podet', $where);
		
		$nmsupplier = $this->input->post('nmsupplier');
		$kdsupplier = $this->input->post('kdsupplier');
		
		$stpo=($_POST['idstpo']) ? $_POST['idstpo']:null;
		$k=array('[',']', '"');
		$r= str_replace($k, '', $arrpo);
		$b = explode(',',$r);
		foreach($b as $val){
			$vale = explode('-', $val);
				$dataArray = $this->getFieldsAndValuesDet($vale[0],$vale[1],$vale[2],$vale[3],$vale[4],$vale[5],$vale[6],$vale[9],$vale[10],$vale[11],$nopo);
				$z = $this->db->insert('podet',$dataArray);
				/* $this->db->query("CALL SP_stokfrompo (?,?,?,?)",array(
					$vale[2]*$vale[7],
					$vale[0],
					$vale[3],
					$vale[8]
				)); */
				$this->db->query("Call in_pokartustok(?,?,?,?,?,?,?,?,?)",array(
							$vale[0],
							$vale[2]*$vale[7],
							$vale[3],
							$vale[8],
							$vale[2],
							$this->session->userdata['user_id'],
							$nopo,
							$kdsupplier,
							$nmsupplier,
				));
				
				$this->db->query("CALL sp_updatebrgfrompodet (?,?,?,?)",array(
					$vale[0],
					$vale[4],
					$vale[6],
					$stpo
				));
		}
		return true;
	}
	
	function update_stok(){
		$k = array('[',']', '"');
		$r = str_replace($k, '', $_POST['arrstok']);
	}
	
	function update_pp(){
		$dataArray = $this->getValueEdit();
		
		$this->db->where("nopp", $dataArray['nopp']);
		$this->db->update("pp",$dataArray);
		
		 if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
            $ret=$dataArray;
        }
        return $ret;
	}
	
// update
	function updateAll(){
		
		$this->db->trans_begin();
		$arrpo = $this->input->post("arrpo");
		$nopo = $this->input->post("nopo");
		
		$query = $this->db->getwhere('po',array('nopo'=>$nopo));
		$po = $query->row_array();
		
	/* 	if($query->num_rows() == 0){
			$pos = $this->update_po($nopo);
			$vpo = $pos['nopo'];
		}else{
			$pos = $query->row_array();
			$vpo = $pos['nopo'];
		} */
		$pos = $this->update_po($nopo);
		$podet = $this->update_podet($pos, $arrpo);
		$pp = $this->update_pp();
		
		if($pos && $podet && $pp)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
        //    $ret["nopo"]=$vpo;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		echo json_encode($ret);
  
	}
	function update_po($nopo){
		$dataArray = $this->getFieldsAndValues();
		$this->db->where('nopo',$dataArray['nopo']);
		$z = $this->db->update('po',$dataArray);
		if($z){
			$ret = $dataArray;
		}else{
			$ret = false;
		}
		return $ret;
	}
	function update_podet($nopo, $arrpo){
		$k=array('[',']', '"');
		$r= str_replace($k, '', $arrpo);
		$b = explode(',',$r);
		foreach($b as $val){
			$vale = explode('-', $val);			
				$dataArray = $this->getFieldsAndValuesDetForUpdate($vale[0],$vale[1],$vale[2],$vale[3],$vale[4],$vale[5],$vale[6],$vale[9],$vale[10],$vale[11]);
				$this->db->where('nopo',$_POST['nopo']);
				$this->db->where('kdbrg',$vale[0]);
				$z = $this->db->update('podet',$dataArray);
				
		}
		return true;
	}
	function getFieldsAndValues(){
		if(is_null($_POST['nopo']) || $_POST['nopo'] == ''){
			$no = $this->getNoPO();
			$nostart = "PO";
			$nomid = date('y');
			$nombln = date('m');
			$noend = str_pad($no, 5, "0", STR_PAD_LEFT);
			$nopo = $nostart.$nomid.$nombln.$noend;
		}else{
			$nopo = $_POST['nopo'];
		}
		$dataArray = array(
			'nopo' => $nopo,
			'tglpo' => date('Y-m-d'),
			'idjnspp' => $_POST['idjnspp'],
			'nopp' => ($_POST['nopp'] != '')?$_POST['nopp']:null,
			'tglpp' => $_POST['tglpp'],
			'idbagian' => 11,
			'kdsupplier' => $_POST['kdsupplier'],
			'idsypembayaran' => ($_POST['idsypembayaran']) ? $_POST['idsypembayaran']:null,
			'idjnspembayaran' => ($_POST['idjnspembayaran']) ? $_POST['idjnspembayaran']:null,
			'tglpengiriman' => date('Y-m-d'),
		//	'idstpo' => $_POST['idstpo'],
			'idstsetuju' => 2,
			'idmatauang' => 1,
			'bpb' => $_POST['bpb'],
			'ketpo' => $_POST['keterangan'],
		//	'idstdiskon' => $_POST['idstdiskon'],
		//	'diskon' => $_POST['diskon'],
		//	'ppnrp' => '',
			'totalpo' => $_POST['totalpo'],
			'userid'=> $this->session->userdata['user_id'],
       //     'tglinput'=> date('Y-m-d'),
			'approval1' => $_POST['approval1'],
		//	'approval2' => $_POST['approval2'], 
		);
		return $dataArray;
	}
	
	function getFieldsAndValuesDet($val1,$val2,$val3,$val4,$val5,$val6,$val7,$val8,$val9,$val12,$nopo){
	$query = $this->db->getwhere('hrgbrgsup',array('kdbrg'=>$val1));
	$item = $query = $query->row_array();
		$dataArray = array(
			'nopo' => $nopo,
			'kdbrg' => $val1,
			'idsatuan' => $val2,
			'qty' => $val3,
			'qtybonus' => $val4,
			'hargabeli' => $val5,
			'diskon' => $val6,
			'ppn' => $val9,
			'hargajual' => $val7,
			'margin' => $val8,
			'diskonrp' => $val12
		);
		return $dataArray;
	}
	function getFieldsAndValuesDetForUpdate($val1,$val2,$val3,$val4,$val5,$val6,$val7,$val8,$val9,$val10){
		
		$dataArray = array(
		
			'idsatuan' => $val2,
			'qty' => $val3,
			'qtybonus' => $val4,
			'hargabeli' => $val5,
			'diskon' => $val6,
			'hargajual' => $val7,
			'margin' => $val8,
			'ppn' => $val9,
			'diskonrp' => $val10,
			
		);
		return $dataArray;
	}
	
	function getValueEdit(){
		$dataArray = array(
			'nopp' => $_POST['nopp'],
			'idstpo' => ($_POST['idstpo']) ? $_POST['idstpo']:null,
		);
		return $dataArray;
	}
	
	function getNoPO(){
		$this->db->select("count(nopo) AS max_np");
        $this->db->from("po");
        $this->db->where('SUBSTRING(nopo,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	function app1(){
	$sql =	$this->db->query("select * from setting where kdset = 'ACCPO1'");
	$num = $sql->num_rows();
			if($num>0){
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);			
			}else{
				return 0;
			} 
	
	}
	function app2(){
	$sql =	$this->db->query("select * from setting where kdset = 'ACCPO2'");
	$num = $sql->num_rows();
			if($num>0){
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);			
			}else{
				return 0;
			} 
	
	}
	function getDataPo(){
		$nopo = str_pad($_POST['nopo'], 10, "0", STR_PAD_LEFT);
        $this->db->select("*");
        //$this->db->from("v_ambilpo");
		//$this->db->where('nopo1',$nopo);
		$this->db->from("v_po"); //dhy15
		$this->db->where('nopo',$nopo); //dhy15
		$q = $this->db->get();
		$po = $q->row_array();
		echo json_encode($po);
    }
	function getPodet(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$nopo 					= $this->input->post("nopo");
		$this->db->select("*");
		$this->db->from("podet");
		$this->db->where("nopo",$nopo);
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('podet');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	function getDataPP(){
		$nopp = str_pad($_POST['nopp'], 10, "0", STR_PAD_LEFT);
        $this->db->select("*, bagian.nmbagian, stsetuju.nmstsetuju, stpo.nmstpo, supplier.nmsupplier, pengguna.userid, pengguna.nmlengkap, pp.keterangan");
        $this->db->from("pp");
		$this->db->join('bagian',
				'bagian.idbagian = pp.idbagian', 'left');
		$this->db->join('stsetuju',
				'stsetuju.idstsetuju = pp.idstsetuju', 'left');
		$this->db->join('pengguna',
				'pengguna.userid = pp.userid', 'left');
		$this->db->join('supplier',
                'supplier.kdsupplier = pp.kdsupplier', 'left');
		$this->db->join('stpo',
                'stpo.idstpo = pp.idstpo', 'left');
				
		$this->db->order_by('nopp DESC');      
		$this->db->where('nopp',$nopp); //dhy15
		$q = $this->db->get();
		$pp = $q->row_array();
		echo json_encode($pp);
    }
	
	function get_podet_ppdet(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$nopp = $this->input->post("nopp");
		$nopo = $this->input->post("nopo");

		$countpp = $this->getcountpp($nopp);
		$maxpp = $this->getmaxtglpo($nopp);
		$this->db->select("*");
		
		if($nopo && !$nopp) {    
			$this->db->from("v_podet");  
		} else {
			if ($countpp==0) {
				$this->db->from("v_podetpp"); 
			} else {
				$this->db->from("v_podet");  
			}
		}

		
		if($nopo && !$nopp) {    
			$this->db->where("nopo", $nopo);  
		} else {
			$this->db->where("nopp", $nopp);
			if ($countpp>0) {
				$this->db->where("tglpo", $maxpp);
			}
		}
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            //$build_array["data"]=$data;
			if($nopo) {
				foreach($data as $row) {
					array_push($build_array["data"],array(
						'nopo'=>$row->nopo,
						'kdbrg'=>$row->kdbrg,
						'nmbrg'=>$row->nmbrg,
						'idsatuanbsr'=>$row->idsatuanbsr,
						'nmsatuanbsr'=>$row->nmsatuanbsr,
						//'qty'=>$row->qty,
						'qty'=>($nopp) ? $this->getqtysum($row->nopp,$row->kdbrg) - $row->qty:$row->qty,
						'qtyb'=>$row->qtyb,
						'hrgbeli'=>$row->hrgbeli,
						'diskon'=>$row->diskon,
						'tamppn'=>$row->ppn,
						'ppn'=>($row->ppn==10.00) ? '1':'0',
						'hargajual1'=>$row->hargajual1,
						'diskonrp'=>$row->diskonrp,
						'margin'=>$row->margin,
						'subtotal'=>$row->subtotal,
						'hsubtotal'=>$row->hsubtotal,
						'htsubtotal'=>$row->htsubtotal,
						'hargajual'=>$row->hargajual,
						'hargajualtemp'=>$row->hargajualtemp,
						'rasio'=>$row->rasio,
						'tglpo'=>$row->tglpo,
						'nopp'=>$row->nopp,

					));
				}
			} else {
				if ($countpp==0) {					
					foreach($data as $row) {
						array_push($build_array["data"],array(
							'nopp'=>$row->nopp,
							'idsatuanbsr'=>$row->idsatuanbsr,
							'nmsatuanbsr'=>$row->nmsatuanbsr,
							'qty'=>$row->qty,
							'catatan'=>$row->catatan,
							'idhrgbrgsup'=>$row->idhrgbrgsup,
							'kdbrg'=>$row->kdbrg,
							'idstpp'=>$row->idstpp,
							'nmstpp'=>$row->nmstpp,
							'margin'=>$row->margin,							
							'nopo'=>$row->nopo,
							'nmbrg'=>$row->nmbrg,
							'subtotal'=>$row->hrgbeli,	
							'hrgbeli'=>$row->hrgbeli,
							'hrgjual'=>$row->hrgjual,
							'harga'=>$row->harga,	
							'rasio'=> $row->rasio,
							'diskonrp'=>0,
							'diskon'=>0

						));
					} 
				} else {
					foreach($data as $row) {
						array_push($build_array["data"],array(
							'nopo'=>$row->nopo,
							'kdbrg'=>$row->kdbrg,
							'nmbrg'=>$row->nmbrg,
							'idsatuanbsr'=>$row->idsatuanbsr,
							'nmsatuanbsr'=>$row->nmsatuanbsr,
							'qty'=>$row->qty,
							'qty'=>$this->getqtysum($row->nopp,$row->kdbrg) - $row->qty,
							'qtyb'=>$row->qtyb,
							'hrgbeli'=>$row->hrgbeli,
							'diskon'=>$row->diskon,
							'tamppn'=>$row->ppn,
							'ppn'=>($row->ppn==10.00) ? '1':'0',
							'hargajual1'=>$row->hargajual1,
							'diskonrp'=>$row->diskonrp,
							'margin'=>$row->margin,
							'subtotal'=>$row->subtotal,
							'hsubtotal'=>$row->hsubtotal,
							'htsubtotal'=>$row->htsubtotal,
							'hargajual'=>$row->hargajual,
							'hargajualtemp'=>$row->hargajualtemp,
							'rasio'=>$row->rasio,
							'tglpo'=>$row->tglpo,
							'nopp'=>$row->nopp,

						));
					}  
				}
		}
        }
		
        echo json_encode($build_array);
    }
	
	function getcountpp($nopp){
		$this->db->select("count(nopp) AS countnopp");
        $this->db->from("po");
        $this->db->where('nopp',$nopp);
		$q = $this->db->get();
		$data = $q->row_array();
		return $data['countnopp'];
	}
	
	function getmaxtglpo($nopp){
		$this->db->select("max(tglpo) AS maxtglpo");
        $this->db->from("v_podet");
        $this->db->where('nopp',$nopp);
		$q = $this->db->get();
		$data = $q->row_array();
		return $data['maxtglpo'];
	}
	
	function getqtysum($nopp,$kdbrg){
		$this->db->select("sum(qty) AS sumqty,count(nopp) AS countpp");
        $this->db->from("v_podet");
        $this->db->where('nopp',$nopp);
		$this->db->where('kdbrg',$kdbrg);
		$q = $this->db->get();
		$data = $q->row_array();
		if ($data['countpp']=='1') {
			return $data['sumqty']+$data['sumqty'];
		} else {
			return $data['sumqty'];
		}
	}
}