<?php

class Tppelayanan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_detail_bhp_alkes(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                 	= $this->input->post("query");
		$kdpelayanan           = $_POST["kdpelayanan"];
				
		$this->db->select("*");
		$this->db->from("v_pelayanan_bhp_alkes");
		
		$this->db->order_by('kdpelayanan');		
		
		$where = array();
        $where['kdpelayanan']=$kdpelayanan;
        
        $this->db->where($where);
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function delete_tppelayanan(){     
		//CEK DATA TARIF
		$cektarif = $this->db->query("select * from tarif where kdpelayanan = '".$_POST['kdpelayanan']."'")->row();
		if($cektarif){
				if($cektarif->tarifbhp == ''){
					$cektrfbhp = 0;	
				} else{
					$cektrfbhp = $cektarif->tarifbhp;	
				}
		} else{
				$cektrfbhp = 'N';			
		}

		$cekbrgbhp = $this->db->query("select * from pelayanandet_bhp_alkes where idpelayanandet = '".$_POST['idpelayanandet']."'")->row();
		if($cektrfbhp > 0){
				$tarifbhpbaru = $cektrfbhp - $cekbrgbhp->tarifbhp_per_treatment;
				$this->db->where('kdpelayanan', $_POST['kdpelayanan']);
				$this->db->set('tarifbhp', $tarifbhpbaru);
				$this->db->update('tarif'); 
		}		
			

		$where['idpelayanandet'] = $_POST['idpelayanandet'];
		$del = $this->rhlib->deleteRecord('pelayanandet_bhp_alkes',$where);
        return $del;
    }
	
	function delete_win_pelayanan(){     
		$where['idtarifpaket'] = $_POST['idtarifpaket'];
		$where['kdtarif'] = $_POST['kdpelayanan'];
		$del = $this->rhlib->deleteRecord('tarifpaketdet',$where);
        return $del;
    }
	
	function delete_win_Barang(){     
		$where['idtarifpaket'] = $_POST['idtarifpaket'];
		$where['kdtarif'] = $_POST['kdbrg'];
		$del = $this->rhlib->deleteRecord('tarifpaketdet',$where);
        return $del;
    }
		
	function insert_win_pelayanan(){
		$dataArray = $this->getFieldsAndValuesPelayanan();
		$ret = $this->rhlib->insertRecord('tarifpaketdet',$dataArray);
		return $ret;
    }
	
	function insert_win_barang(){
		$dataArray = $this->getFieldsAndValuesBarang();
		$ret = $this->rhlib->insertRecord('pelayanandet_bhp_alkes',$dataArray);
		return $ret;
    }
	
	function update_qty(){
		//UPDATE
		$this->db->where('idpelayanandet', $_POST['idpelayanandet']);
		$this->db->set('qty', $_POST['qty']);
		$this->db->update('pelayanandet_bhp_alkes'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	
	function update_tarifbhp(){
		//UPDATE
		$this->db->where('idpelayanandet', $_POST['idpelayanandet']);
		$this->db->set('tarifbhp_per_treatment', $_POST['tarifbhp']);
		$this->db->update('pelayanandet_bhp_alkes'); 

		//CEK DATA TARIF
		$cektarif = $this->db->query("select sum(tarifbhp_per_treatment) as tarifbhp 
					from pelayanandet_bhp_alkes where kdpelayanan = '".$_POST['kdpelayanan']."'")->row();
		if($cektarif){
				if(empty($cektarif->tarifbhp)){
					$cektrfbhp = 0;	
				} else{
					$cektrfbhp = $cektarif->tarifbhp;	
				}
		} else{
				$cektrfbhp = 'N';			
		} 

		if($cektrfbhp != 'N'){
				//UPDATE BHP TARIF
				$tarifbhpbaru = $cektrfbhp;
				$this->db->where('kdpelayanan', $_POST['kdpelayanan']);
				$this->db->set('tarifbhp', $tarifbhpbaru);
				$this->db->set('userid', $this->session->userdata("user_id"));
				$this->db->set('tglupdate', date('Y-m-d H:i:s'));
				$this->db->update('tarif'); 
		}
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}
	
	function update_tppelayanan(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('idtarifpaketdet', $_POST['idtarifpaketdet']);
		$this->db->update('tarifpaketdet', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
	
	function getFieldsAndValuesPelayanan(){
		$qty 		= (isset($_POST['qty']))? $_POST['qty'] : 0;
		$tarifjs 	= (isset($_POST['tarifjs']))? $_POST['tarifjs'] : 0;
		$tarifjm 	= (isset($_POST['tarifjm']))? $_POST['tarifjm'] : 0;
		$tarifjp 	= (isset($_POST['tarifjp']))? $_POST['tarifjp'] : 0;
		$tarifbhp 	= (isset($_POST['tarifbhp']))? $_POST['tarifbhp'] : 0;
		
		$dataArray = array(
		    //'idtarifpaketdet'	=> $_POST['idtarifpaketdet'],
		     'kdtarif'			=> $_POST['kdpelayanan'],
		     'idjnstarif'		=> $_POST['idjnstarif'],
		     'idtarifpaket'		=> $_POST['idtarifpaket'],
			 'qty'				=> $qty, 
			 'tarifjs'			=> $tarifjs, 
			 'tarifjm' 			=> $tarifjm,
             'tarifjp'			=> $tarifjp,
			 'tarifbhp'			=> $tarifbhp,
        );
		
		return $dataArray;
	}
	
	function getFieldsAndValuesBarang(){
		$qty 		= (isset($_POST['qty']))? $_POST['qty'] : 0;
		$tarifbhp 	= (isset($_POST['tarifbhp']))? $_POST['tarifbhp'] : 0;
		
		$dataArray = array(
		    //'idtarifpaketdet'	=> $_POST['idtarifpaketdet'],
		     'kdtarif'			=> $_POST['kdbrg'],
		  //   'idjnstarif'		=> $_POST['idjnstarif'],
		     'kdpelayanan'		=> $_POST['kdpelayanan'],
			 'qty'							=> $qty, 
			 'tarifbhp_per_treatment'			=> $tarifbhp,
			 'userid' => $this->session->userdata("user_id"),
			 'tglinput' => date('Y-m-d H:i:s'),
        );
		
		return $dataArray;
	}
	
	function cekkdpelayanan(){
        $q = "SELECT count(kdtarif) as kdtarif FROM tarifpaketdet where kdtarif='".$_POST['kdpelayanan']."' AND idtarifpaket='".$_POST['idtarifpaket']."' AND idjnstarif='".$_POST['idjnstarif']."'";
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->kdtarif;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
	
	function cekkdbarang(){
        $q = "SELECT count(kdtarif) as kdtarif FROM tarifpaketdet where kdtarif='".$_POST['kdbrg']."' AND idtarifpaket='".$_POST['idtarifpaket']."' AND idjnstarif='".$_POST['idjnstarif']."'";
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->kdtarif;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
	
	function get_pelayanan(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
		
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_pelayanan");
				
        $this->db->order_by('v_pelayanan.kdpelayanan');
        // $this->db->where("v_pelayanan.idjnshirarki = 1");
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
	
		$this->db->select("*");
		$this->db->from("v_pelayanan");
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
}
