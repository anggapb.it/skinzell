<?php

class Mtempelayanan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_mtempelayanan(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
              
		$this->db->select("*");
		$this->db->from("v_mtempelayanan");
		$this->db->where('v_mtempelayanan.nmjnshirarki !=','null');
		
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select("*");
		$this->db->from("v_mtempelayanan");

        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_mtempelayanan(){     
		$where['idtemplayanan'] = $_POST['idtemplayanan'];
		$del = $this->rhlib->deleteRecord('templatepelayanan',$where);
        return $del;
    }
		
	function insert_mtempelayanan(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('templatepelayanan',$dataArray);
        return $ret;
    }

	function update_mtempelayanan(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('idtemplayanan', $_POST['idtemplayanan']);
		$this->db->update('templatepelayanan', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
	
	$tem_idtemplayanan =  $_POST['tem_idtemplayanan']; 

	if($tem_idtemplayanan=="") $tem_idtemplayanan=null;
	
		$dataArray = array(
             'idtemplayanan'		=> $_POST['idtemplayanan'],
			 'idjnshirarki'			=> $_POST['idjnshirarki'],
			 'idstatus'				=> $_POST['idstatus'],
			 'tem_idtemplayanan'	=> $this->id_templayanan('nmtemplayanan',$tem_idtemplayanan),
             'nmtemplayanan'		=> $_POST['nmtemplayanan'],
        );		
/* 	var_dump($dataArray);
	exit; */
		return $dataArray;
	}
	
	function get_parent_tempelayanan(){ 
		$query = $this->db->getwhere('templatepelayanan',array('idjnshirarki'=>'0'));
		$parent = $query->result();
		$ttl = count($parent);
        $arrPenyakit = array ("success"=>true,"results"=>$ttl,"data"=>$parent);
		echo json_encode($arrPenyakit);
    }
	
	function id_templayanan($where, $val){
		$query = $this->db->getwhere('templatepelayanan',array($where=>$val));
		$id = $query->row_array();
		return  $id['idtemplayanan'];
    }
	
	function getNmtempelayanan(){
		$query = $this->db->getwhere('templatepelayanan',array('idtemplayanan'=>$_POST['tem_idtemplayanan']));
		$nm = $query->row_array();
		echo json_encode($nm['nmtemplayanan']);
    }	
}
