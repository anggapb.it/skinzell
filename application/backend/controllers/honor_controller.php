<?php 
class Honor_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_hondok(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        		
		$this->db->select("*");
        $this->db->from("v_hondok");
        $this->db->order_by("v_hondok.nohondok DESC");		
		if ($this->input->post("cbx")=='true') {
			$this->db->where('tglhondok BETWEEN ', "'". $this->input->post("tglawalan") ."' AND '". $this->input->post("tglakhiran") ."'", false);
		}
		if ($this->input->post("iddokter")) {
			$this->db->where("iddokter",$this->input->post("iddokter"));
		}
                
         if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        } 
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function numrow(){
		
		$this->db->select("*");
        $this->db->from("v_hondok");
        $this->db->order_by("v_hondok.nohondok DESC");	
		if ($this->input->post("cbx")=='true') {
			$this->db->where('tglhondok BETWEEN ', "'". $this->input->post("tglawalan") ."' AND '". $this->input->post("tglakhiran") ."'", false);
		}
		if ($this->input->post("iddokter")) {
			$this->db->where("iddokter",$this->input->post("iddokter"));
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_honor_rj(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$iddokter 				= $this->input->post("iddokter");
		$tglawal 				= $this->input->post("tglawal");
		$tglakhir 				= $this->input->post("tglakhir");
		$nohondok 				= null;//$this->input->post("nohondok");
		
		if ($nohondok) {
			$q = $this->db->query($this->get_query_hondokdet($iddokter,$nohondok,"1"));
		} else {
			$q = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
				 , `notadet`.`nonota` AS `nonota`
				 , `notadet`.`kditem` AS `kditem`
				 , `pelayanan`.`pel_kdpelayanan` AS `pel_kdpelayanan`
				 , `notadet`.`idjnstarif` AS `idjnstarif`
				 , `notadet`.`koder` AS `koder`
				 , `notadet`.`idsatuan` AS `idsatuan`
				 , `notadet`.`qty` AS `qty`
				 , `notadet`.`tarifjs` AS `tarifjs`
				 , `notadet`.`tarifjm` AS `tarifjm`
				 , `notadet`.`tarifjp` AS `tarifjp`
				 , `notadet`.`tarifbhp` AS `tarifbhp`
				 , `notadet`.`diskonjs` AS `diskonjs`
				 , `notadet`.`diskonjm` AS `diskonjm`
				 , `notadet`.`diskonjp` AS `diskonjp`
				 , `notadet`.`diskonbhp` AS `diskonbhp`
				 , `notadet`.`uangr` AS `uangr`
				 , `notadet`.`hrgjual` AS `hrgjual`
				 , `notadet`.`hrgbeli` AS `hrgbeli`
				 , `notadet`.`iddokter` AS `iddokter`
				 , `notadet`.`idperawat` AS `idperawat`
				 , `notadet`.`idstbypass` AS `idstbypass`
				 , `notadet`.`aturanpakai` AS `aturanpakai`
				 , `notadet`.`dijamin` AS `dijamin`
				 , `notadet`.`stdijamin` AS `stdijamin`
				 , `registrasidet`.`noreg` AS `noreg`
				 , `registrasidet`.`tglmasuk` AS `tglmasuk`
				 , `registrasidet`.`tglkeluar` AS `tglkeluar`
				 , `registrasidet`.`tglreg` AS `tglreg`
				 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
				 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
				 , `pasien`.`nmpasien` AS `nmpasien`
				 , `registrasidet`.`idbagian` AS `idbagian`
				 , `bagian`.`nmbagian` AS `nmbagian`
				 , sum((
				   CASE
				   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000001') THEN
					 (`notadet`.`tarifjm` * `notadet`.`qty`)
				   ELSE
					 0
				   END)) AS `pemeriksaan`
				 , sum((
				   CASE
				   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000048') THEN
					 (`notadet`.`tarifjm` * `notadet`.`qty`)
				   ELSE
					 0
				   END)) AS `tindakan`
				 , sum(`notadet`.`tarifjm` * `notadet`.`qty`) AS `jasamedis`
				 , sum(`notadet`.`diskonjm`) AS `diskonmedis`
				 , ceiling((((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 2.5) / 100)) AS `zis`
				 , ((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) - ceiling(((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 0.025))) AS `jumlah`
			FROM
			  `registrasidet`
			LEFT JOIN `registrasi`
			ON `registrasidet`.`noreg` = `registrasi`.`noreg`
			LEFT JOIN `pasien`
			ON `pasien`.`norm` = `registrasi`.`norm`
			LEFT JOIN `bagian`
			ON `bagian`.`idbagian` = `registrasidet`.`idbagian`
			LEFT JOIN `nota`
			ON `nota`.`idregdet` = `registrasidet`.`idregdet`
			LEFT JOIN `notadet`
			ON `notadet`.`nonota` = `nota`.`nonota` AND `notadet`.`iddokter` = '".$iddokter."'
			LEFT JOIN `pelayanan`
			ON `notadet`.`kditem` = `pelayanan`.`kdpelayanan`
			LEFT JOIN kuitansi
			ON kuitansi.nokuitansi = `nota`.nokuitansi
			WHERE
			  `registrasi`.`idjnspelayanan` IN (1, 3)
			  AND `notadet`.`tarifjm` > 0
			  AND `pelayanan`.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
			  AND `registrasidet`.`userbatal` IS NULL
			  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
			GROUP BY
			  `registrasi`.`noreg`");
		}
		
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}

	function get_honor_ri(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$iddokter 				= $this->input->post("iddokter");
		$tglawal 				= $this->input->post("tglawal");
		$tglakhir 				= $this->input->post("tglakhir");
		$nohondok 				= null;//$this->input->post("nohondok");
		
		if ($nohondok) {
			$q = $this->db->query($this->get_query_hondokdet($iddokter,$nohondok,"2"));
		} else {
			$q = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
				 , `notadet`.`nonota` AS `nonota`
				 , `notadet`.`kditem` AS `kditem`
				 , `notadet`.`idjnstarif` AS `idjnstarif`
				 , `notadet`.`koder` AS `koder`
				 , `notadet`.`idsatuan` AS `idsatuan`
				 , `notadet`.`qty` AS `qty`
				 , `notadet`.`tarifjs` AS `tarifjs`
				 , `notadet`.`tarifjm` AS `tarifjm`
				 , `notadet`.`tarifjp` AS `tarifjp`
				 , `notadet`.`tarifbhp` AS `tarifbhp`
				 , `notadet`.`diskonjs` AS `diskonjs`
				 , `notadet`.`diskonjm` AS `diskonjm`
				 , `notadet`.`diskonjp` AS `diskonjp`
				 , `notadet`.`diskonbhp` AS `diskonbhp`
				 , `notadet`.`uangr` AS `uangr`
				 , `notadet`.`hrgjual` AS `hrgjual`
				 , `notadet`.`hrgbeli` AS `hrgbeli`
				 , `notadet`.`iddokter` AS `iddokter`
				 , `notadet`.`idperawat` AS `idperawat`
				 , `notadet`.`idstbypass` AS `idstbypass`
				 , `notadet`.`aturanpakai` AS `aturanpakai`
				 , `notadet`.`dijamin` AS `dijamin`
				 , `notadet`.`stdijamin` AS `stdijamin`
				 , `registrasidet`.`noreg` AS `noreg`
				 , `registrasidet`.`tglmasuk` AS `tglmasuk`
				 , `registrasidet`.`tglkeluar` AS `tglkeluar`
				 , `registrasidet`.`tglreg` AS `tglreg`
				 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
				 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
				 , `pasien`.`nmpasien` AS `nmpasien`
				 , `registrasidet`.`idbagian` AS `idbagian`
				 , `bagian`.`nmbagian` AS `nmbagian`
				 , sum((
				   CASE
				   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000001') THEN
					 (`notadet`.`tarifjm` * `notadet`.`qty`)
				   ELSE
					 0
				   END)) AS `pemeriksaan`
				 , sum((
				   CASE
				   WHEN (`pelayanan`.`pel_kdpelayanan` = 'T000000048') THEN
					 (`notadet`.`tarifjm` * `notadet`.`qty`)
				   ELSE
					 0
				   END)) AS `tindakan`
				 , sum(`notadet`.`tarifjm` * `notadet`.`qty`) AS `jasamedis`
				 , sum(`notadet`.`diskonjm`) AS `diskonmedis`
				 , ceiling((((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 2.5) / 100)) AS `zis`
				 , ((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) - ceiling(((sum(`notadet`.`tarifjm` * `notadet`.`qty`) - sum(`notadet`.`diskonjm`)) * 0.025))) AS `jumlah`
				 , sum(((`notadet`.`qty` * `notadet`.`tarifjm`) - `notadet`.`diskonjm`)) AS `sum((``notadet``.qty * ``notadet``.tarifjm) - ``notadet``.diskonjm)`
			FROM
			  `registrasidet`
			LEFT JOIN `registrasi`
			ON `registrasidet`.`noreg` = `registrasi`.`noreg`
			LEFT JOIN `reservasi`
			ON `registrasidet`.`idregdet` = `reservasi`.`idregdet`
			LEFT JOIN `pasien`
			ON `pasien`.`norm` = `registrasi`.`norm`
			LEFT JOIN `nota`
			ON `nota`.`idregdet` = `registrasidet`.`idregdet`
			LEFT JOIN `notadet`
			ON `notadet`.`nonota` = `nota`.`nonota` AND `notadet`.`iddokter` = '".$iddokter."'
			LEFT JOIN `bagian`
			ON `bagian`.`idbagian` = `registrasidet`.`idbagian`
			LEFT JOIN `pelayanan`
			ON `notadet`.`kditem` = `pelayanan`.`kdpelayanan`
			LEFT JOIN kuitansi
			ON kuitansi.nokuitansi = `nota`.nokuitansi
			WHERE
			  `registrasi`.`idjnspelayanan` = 2
			  AND `reservasi`.`idstposisipasien` = 6
			  AND `notadet`.`tarifjm` > 0
			  AND `pelayanan`.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
			  AND `registrasidet`.`userbatal` IS NULL
			  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'

			GROUP BY
			  `registrasi`.`noreg`");
		}
		
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}

	function get_honor_jm(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$iddokter 				= $this->input->post("iddokter");
		$tglawal 				= $this->input->post("tglawal");
		$tglakhir 				= $this->input->post("tglakhir");
		$nohondok 				= null;//$this->input->post("nohondok");
		
		if ($nohondok) {
			$q = $this->db->query($this->get_query_hondokdet($iddokter,$nohondok,"3"));
		} else {
			$q = $this->db->query("SELECT `notadet`.`idnotadet` AS `idnotadet`
				 , `notadet`.`nonota` AS `nonota`
				 , `notadet`.`kditem` AS `kditem`
				 , `notadet`.`idjnstarif` AS `idjnstarif`
				 , `notadet`.`koder` AS `koder`
				 , `notadet`.`idsatuan` AS `idsatuan`
				 , `notadet`.`qty` AS `qty`
				 , `notadet`.`tarifjs` AS `tarifjs`
				 , `notadet`.`tarifjm` AS `tarifjm`
				 , `notadet`.`tarifjp` AS `tarifjp`
				 , `notadet`.`tarifbhp` AS `tarifbhp`
				 , `notadet`.`diskonjs` AS `diskonjs`
				 , `notadet`.`diskonjm` AS `diskonjm`
				 , `notadet`.`diskonjp` AS `diskonjp`
				 , `notadet`.`diskonbhp` AS `diskonbhp`
				 , `notadet`.`uangr` AS `uangr`
				 , `notadet`.`hrgjual` AS `hrgjual`
				 , `notadet`.`hrgbeli` AS `hrgbeli`
				 , `jtmdet`.`iddokter` AS `iddokter`
				 , `notadet`.`idperawat` AS `idperawat`
				 , `notadet`.`idstbypass` AS `idstbypass`
				 , `notadet`.`aturanpakai` AS `aturanpakai`
				 , `notadet`.`dijamin` AS `dijamin`
				 , `notadet`.`stdijamin` AS `stdijamin`
				 , `registrasidet`.`noreg` AS `noreg`
				 , `registrasidet`.`tglmasuk` AS `tglmasuk`
				 , `registrasidet`.`tglkeluar` AS `tglkeluar`
				 , `registrasidet`.`tglreg` AS `tglreg`
				 , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
				 , TRIM(LEADING '0' FROM `registrasi`.`norm`) AS `norm`
				 , `pasien`.`nmpasien` AS `nmpasien`
				 , sum(`jtmdet`.`jumlah`) AS `jasamedis`
				 , sum(`jtmdet`.`diskon`) AS `diskonmedis`
				 , ceiling((((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) * 2.5) / 100)) AS `zis`
				 , ((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) - ceiling(((sum(`jtmdet`.`jumlah`) - sum(`jtmdet`.`diskon`)) * 0.025))) AS `jumlah`
			FROM
			  `registrasidet`
			LEFT JOIN `registrasi`
			ON `registrasidet`.`noreg` = `registrasi`.`noreg`
			LEFT JOIN `nota`
			ON `nota`.`idregdet` = `registrasidet`.`idregdet`
			LEFT JOIN `notadet`
			ON `notadet`.`nonota` = `nota`.`nonota`
			LEFT JOIN `pasien`
			ON `pasien`.`norm` = `registrasi`.`norm`
			LEFT JOIN `jtm`
			ON `jtm`.`idnotadet` = `notadet`.`idnotadet`
			LEFT JOIN `jtmdet`
			ON `jtm`.`idjtm` = `jtmdet`.`idjtm` AND `jtmdet`.`iddokter` = '".$iddokter."'
			LEFT JOIN kuitansi
			ON kuitansi.nokuitansi = `nota`.nokuitansi
			WHERE
			  (`jtmdet`.`tarifjm` > 0)
			  AND `registrasidet`.`userbatal` IS NULL
			  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
			GROUP BY
			  `registrasi`.`noreg`
			, `jtmdet`.`iddokter`");
		}
		
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_honor_peltam(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$iddokter 				= $this->input->post("iddokter");
		$tglawal 				= $this->input->post("tglawal");
		$tglakhir 				= $this->input->post("tglakhir");
		$nohondok 				= null;//$this->input->post("nohondok");
		
		if ($nohondok) {
			$q = $this->db->query($this->get_query_hondokdet($iddokter,$nohondok,"3"));
		} else {
			$q = $this->db->query("SELECT notadet.idnotadet AS idnotadet
										 , notadet.nonota AS nonota
										 , nota.tglnota AS tglnota
										 , kuitansi.tglkuitansi AS tglkuitansi
										 , notadet.kditem AS kditem
										 , pelayanan.pel_kdpelayanan AS pel_kdpelayanan
										 , notadet.iddokter AS iddokter
										 , kuitansi.atasnama AS nmpasien
										 , sum((
										   CASE
										   WHEN (pelayanan.pel_kdpelayanan = 'T000000001') THEN
											 (notadet.tarifjm * notadet.qty)
										   ELSE
											 0
										   END)) AS pemeriksaan
										 , sum((
										   CASE
										   WHEN (pelayanan.pel_kdpelayanan = 'T000000048') THEN
											 (notadet.tarifjm * notadet.qty)
										   ELSE
											 0
										   END)) AS tindakan
										 , sum(notadet.tarifjm * notadet.qty) AS jasamedis
										 , sum(notadet.diskonjm) AS diskonmedis
										 , ceiling((((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) * 2.5) / 100)) AS zis
										 , ((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) - ceiling(((sum(notadet.tarifjm * notadet.qty) - sum(notadet.diskonjm)) * 0.025))) AS jumlah

									FROM
									  nota
									LEFT JOIN notadet
									ON nota.nonota = notadet.nonota AND notadet.iddokter = '".$iddokter."'
									LEFT JOIN pelayanan
									ON pelayanan.kdpelayanan = notadet.kditem
									LEFT JOIN kuitansi
									ON kuitansi.nokuitansi = nota.nokuitansi
									WHERE
									  notadet.tarifjm > 0
									  AND nota.idjnstransaksi = 10
									  AND pelayanan.pel_kdpelayanan IN ('T000000001', 'T000000048')
									  AND nota.idsttransaksi = 1
									  AND date(kuitansi.tglkuitansi) BETWEEN '".$tglawal."' AND '".$tglakhir."'
									GROUP BY
									  nota.nonota, notadet.kditem");
		}
		
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function simpan(){
		$dataArray = $this->getFieldsAndValue();
		$head = $this->insert_hondok($dataArray);
		if ($head) {
			$ret['success']=true;
			$ret['autonum']=$dataArray['nohondok'];
			if ($_POST['countarr1'] > 0) {
				$detail = $this->insert_detail1($dataArray['nohondok']);
			}
			if ($_POST['countarr2'] > 0) {
				$detail = $this->insert_detail2($dataArray['nohondok']);
			}
			if ($_POST['countarr3'] > 0) {
				$detail = $this->insert_detail3($dataArray['nohondok']);
			}
			if ($_POST['countarr4'] > 0) {
				$detail = $this->insert_detail4($dataArray['nohondok']);
			}
		} else {
			$ret['success']=false;
			$ret['autonum']=null;
		}

		echo json_encode($ret);
	}
	
	function insert_hondok($dataArray){
		$insert = $this->db->insert('hondok',$dataArray);
		if($insert){
			$ret = true;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	 function getFieldsAndValue(){
		$nohondok = $this->getNoHondok();
		$dataArray = array(
			'nohondok' => $nohondok,
			'tglhondok' => $_POST['tglhondok'],//date('Y-m-d'),
			'tglawal' => $_POST['tglawal'],//$_POST[''],
			'tglakhir' => $_POST['tglakhir'],//$_POST[''],
			'jasadrjaga' => $_POST['jasadrjaga'],//$_POST[''],
			'potonganlain' => $_POST['potonganlain'],//$_POST[''],
			'userid' => $this->session->userdata['user_id'],
			'iddokter' => $_POST['iddokter'],
			'approval' => $_POST['approval'],
			'idjnspembayaran' => $_POST['idjnspembayaran'],
			'idstbayar' => $_POST['idstbayar'],
			'tglinput' => date('Y-m-d'),
			'catatan' => $_POST['catatan'],
			
			'tgldikeluarkan' => $_POST['tgldikeluarkan'],
			
		);
		return $dataArray;
	}
	
	function insert_detail1($nohondok){
		$k = array('[',']','"');
		$r = str_replace($k, '', $_POST['arrpenerimaan1']);
		$b = explode(',', $r);
		$detinsert;
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValueDet($nohondok, $vale[0], $vale[1], $vale[2], $vale[3],$vale[4]);
			$detinsert = $this->db->insert('hondokdet',$dataArray);
		}
		if($detinsert){
			$ret = true;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	function insert_detail2($nohondok){
		$k = array('[',']','"');
		$r = str_replace($k, '', $_POST['arrpenerimaan2']);
		$b = explode(',', $r);
		$detinsert;
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValueDet($nohondok, $vale[0], $vale[1], $vale[2], $vale[3],$vale[4]);
			$detinsert = $this->db->insert('hondokdet',$dataArray);
		}
		if($detinsert){
			$ret = true;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	function insert_detail3($nohondok){
		$k = array('[',']','"');
		$r = str_replace($k, '', $_POST['arrpenerimaan3']);
		$b = explode(',', $r);
		$detinsert;
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValueDet($nohondok, $vale[0], $vale[1], $vale[2], $vale[3],$vale[4]);
			$detinsert = $this->db->insert('hondokdet',$dataArray);
		}
		if($detinsert){
			$ret = true;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	function insert_detail4($nohondok){
		$k = array('[',']','"');
		$r = str_replace($k, '', $_POST['arrpenerimaan4']);
		$b = explode(',', $r);
		$detinsert;
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValueDet($nohondok, $vale[0], $vale[1], $vale[2], $vale[3],$vale[4]);
			$detinsert = $this->db->insert('hondokdet',$dataArray);
		}
		if($detinsert){
			$ret = true;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	function getFieldsAndValueDet($val0,$val1,$val2,$val3,$val4,$val5){
		$dataArray = array(
			'nohondok' => $val0,
			'noreg' => $val1,
			'idjnshondok' => $val2,
			'jasamedis' => $val3,
			'diskon' => $val4,
			'zis' => $val5,
		);
		return $dataArray;
	}

	function getNoHondok(){
		$q = "SELECT getOtoNoHondok(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
	
	function app1(){
	$sql =	$this->db->query("select * from setting where kdset = 'APHD01'");
	$num = $sql->num_rows();
			if($num>0){
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);			
			}else{
				return 0;
			} 
	
	}
	
	function update(){
		$dataArray = array(
			'nohondok' => $_POST['nohondok'],
			'jasadrjaga' => $_POST['jasadrjaga'],
			'potonganlain' => $_POST['potonganlain'],
			'approval' => $_POST['approval'],
			'idjnspembayaran' => $_POST['idjnspembayaran'],
			'idstbayar' => $_POST['idstbayar'],
			'catatan' => $_POST['catatan'],
			
			'tgldikeluarkan' => $_POST['tgldikeluarkan'],
		);

		$this->db->where('nohondok', $dataArray['nohondok']);
		$this->db->update('hondok', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}

	function hapus(){
		$where['nohondok'] = $_POST['nohondok'];
		
		$del = $this->rhlib->deleteRecord('hondokdet',$where);
		$del = $this->rhlib->deleteRecord('hondok',$where);
        return $del;
	}
	
	function get_query_hondokdet($iddokter,$nohondok,$idjnshondok) {
		return "SELECT rd.noreg AS noreg
			 , k.tglkuitansi AS tglkuitansi
			 , rd.tglmasuk AS tglmasuk
			 , rd.tglkeluar AS tglkeluar
			 , r.norm AS norm
			 , p.nmpasien AS nmpasien
			 , b.nmbagian AS nmbagian
			 , hd.jasamedis AS jasamedis
			 , hd.diskon AS diskonmedis
			 , hd.zis AS zis
			 , hd.jasamedis - hd.diskon - hd.zis AS jumlah
			 , hd.idjnshondok AS idjnshondok
			 , hd.nohondok AS nohondok
			 , nd.iddokter AS iddokter
			 , CASE
				WHEN pl.pel_kdpelayanan = 'T000000001' THEN
					hd.jasamedis
				ELSE
					0
				END AS pemeriksaan
			, CASE
				WHEN pl.pel_kdpelayanan = 'T000000048' THEN
					hd.jasamedis
				ELSE
					0
				END AS tindakan
		FROM
		  hondokdet hd
		LEFT JOIN registrasidet rd
		ON rd.noreg = hd.noreg
		LEFT JOIN registrasi r
		ON r.noreg = rd.noreg
		LEFT JOIN pasien p
		ON p.norm = r.norm
		LEFT JOIN nota n
		ON n.idregdet = rd.idregdet
		LEFT JOIN notadet nd
		ON nd.nonota = n.nonota AND nd.iddokter = '".$iddokter."'
		LEFT JOIN kuitansi k
		ON k.nokuitansi = n.nokuitansi
		LEFT JOIN pelayanan pl
		ON pl.kdpelayanan = nd.kditem
		LEFT JOIN bagian b
		ON b.idbagian = rd.idbagian
		  WHERE hd.nohondok = '".$nohondok."' 
		  AND hd.idjnshondok = '".$idjnshondok."'
		  AND pl.`pel_kdpelayanan` IN ('T000000001', 'T000000048')
		GROUP BY
		  r.noreg
		ORDER BY
		  r.noreg";
	}	
	
	function get_tglawal_(){
		$q = ("SELECT tglawal FROM hondok order by tglawal DESC LIMIT 1");				
		$query  = $this->db->query($q);
		$tglawal = $query->row_array();
		echo json_encode($tglawal);
	}
	
	function get_tglakhir_(){
		$q = ("SELECT tglakhir FROM hondok order by tglakhir DESC LIMIT 1");				
		$query  = $this->db->query($q);
		$tglakhir = $query->row_array();
		echo json_encode($tglakhir);
	}
}

?>