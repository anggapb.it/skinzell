<?php

class setmakspel_controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_setmakspel(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select('setmakspel.*, pelayanan.nmpelayanan');
        $this->db->from('setmakspel');
		$this->db->join('pelayanan',
                'pelayanan.kdpelayanan = setmakspel.kdpelayanan', 'left');
				
		$this->db->order_by('id');
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
       $this->db->select('setmakspel.*, pelayanan.nmpelayanan');
        $this->db->from('setmakspel');
        $this->db->join('pelayanan',
                'pelayanan.kdpelayanan = setmakspel.kdpelayanan', 'left');

        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_setmakspel(){     
		$where['id'] = $_POST['id'];
		$del = $this->rhlib->deleteRecord('setmakspel',$where);
        return $del;
    }
		
	function insert_setmakspel(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('setmakspel',$dataArray);
        return $ret;
    }

	function update_setmakspel(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('id', $_POST['id']);
		$this->db->update('setmakspel', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
		$dataArray = array(
            // 'id'	=> $_POST['id'],
			 'kdpelayanan'		=> ($_POST['kdpelayanan']) ? $this->kd_pelayanan('nmpelayanan',$_POST['kdpelayanan']) : null,
			 'maksimal'		=> $_POST['maksimal'],
        );		
		return $dataArray;
	}
	
	function kd_pelayanan($where, $val){
		$query = $this->db->getwhere('pelayanan',array($where=>$val));
		$id = $query->row_array();
		return $id['kdpelayanan'];
    }
	
	function getNmpelayanan(){
		$query = $this->db->getwhere('pelayanan',array('kdpelayanan'=>$_POST['kdpelayanan']));
		$nm = $query->row_array();
		echo json_encode($nm['nmpelayanan']);
    }
		
}
