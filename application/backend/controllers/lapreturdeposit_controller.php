<?php

class Lapreturdeposit_controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	//data retur deposit
	function get_lapreturdeposit(){
		$tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		
        $this->db->select('*');
        $this->db->from('v_returdeposit');
		
		if($tglakhir){
			$this->db->where('`tgltransaksi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}else{
			//kalau ngga di filter tanggal, gunakan tanggal hari ini.
			$this->db->where('`tgltransaksi`', date('Y-m-d'));
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result_array();
        }
		
        $ttl = $this->db->count_all('v_returdeposit');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    
	}

}
