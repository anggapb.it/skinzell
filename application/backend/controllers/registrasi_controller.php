<?php

class Registrasi_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function getRegistrasi(){
		$start = $this->input->post("start");
        $limit = $this->input->post("limit");
        
        $fields = $this->input->post("fields");
        $query = $this->input->post("query");
      
        $this->db->select("*,
        registrasi.noreg as zzz, registrasi.noreg as `registrasi.noreg`,
		oruangan.nooruangan as xy, oruangan.nooruangan as `oruangan.nooruangan`,
		oruangan.catatan as ooc, oruangan.catatan as `oruangan.catatan`");
        $this->db->from("registrasi");		
        $this->db->join("registrasidet","registrasidet.noreg = registrasi.noreg","left");
		$this->db->join("oruangan","oruangan.nooruangan = registrasidet.nooruangan", "left");
		$this->db->join("pasien","pasien.norm = registrasi.norm", "left");
		$this->db->join("kamar","kamar.idkamar = registrasidet.idkamar", "left");
		$this->db->join("bed","bed.idbed = registrasidet.idbed", "left");		
		$this->db->order_by('registrasi.noreg desc');
                
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('registrasi');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}

	function getStatusBed(){
		$start = $this->input->post("start");
        $limit = $this->input->post("limit");
        
        $fields = $this->input->post("fields");
        $query = $this->input->post("query");
 	    
        $this->db->select("nmklsrawat,nmkamar,nmbed,nmstbed,tglmasuk,jammasuk,nmpasien,nmdokter,tglrencanakeluar,
        		floor(timestampdiff(hour,cast(concat(registrasidet.tglmasuk,' ',registrasidet.jammasuk)as datetime),
                cast(concat(registrasidet.tglkeluar,' ',registrasidet.jamkeluar)as datetime)) / 24) as hari,
                timestampdiff(hour,cast(concat_ws(' ',registrasidet.tglmasuk,registrasidet.jammasuk)as datetime),
        		cast(concat_ws(' ',registrasidet.tglkeluar,registrasidet.jamkeluar)as datetime)) % 24 as jam",false);
        $this->db->from("klsrawat"); 
        $this->db->join("registrasidet","registrasidet.idklsrawat = klsrawat.idklsrawat");
        $this->db->join("registrasi","registrasi.noreg = registrasidet.noreg");
        $this->db->join("kamar","registrasidet.idkamar = kamar.idkamar");
        $this->db->join("bed","registrasidet.idbed = bed.idbed");
        $this->db->join("stbed","bed.idstbed = stbed.idstbed");
		$this->db->join("pasien","registrasi.norm = pasien.norm");
		$this->db->join("dokter","registrasidet.iddokter = dokter.iddokter");
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('registrasidet');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_registrasi(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*, registrasi.noreg as zzz, registrasi.noreg as `registrasi.noreg`, 
						pasien.alamat as xxx, pasien.alamat as `pasien.alamat`, 
						registrasi.catatan as catatanreg, 
						klsrawat.kdklsrawat as kelastarif,klsrawat.kdklsrawat as`klsrawat.kdklsrawat`,klsrawat.idklsrawat as idklsrawat,
						registrasidet.idregdet as`registrasidet.idregdet`,registrasidet.idregdet as idregdet,registrasidet.idregdet as`registrasidet.idregdet`,
						registrasidet.idklstarif as`registrasidet.idklstarif`,registrasidet.idklstarif as idklstarif,registrasidet.idklstarif as`registrasidet.idklstarif`");
        $this->db->from("registrasi");
        $this->db->join("pasien", 
					"pasien.norm = registrasi.norm", "left"
		);
        $this->db->join("jkelamin", 
					"pasien.idjnskelamin = jkelamin.idjnskelamin", "left"
		);
        $this->db->join("jpelayanan", 
					"jpelayanan.idjnspelayanan = registrasi.idjnspelayanan", "left"
		);
        $this->db->join("stpasien", 
					"stpasien.idstpasien = registrasi.idstpasien", "left"
		);
        $this->db->join("registrasidet", 
					"registrasidet.noreg = registrasi.noreg", "left"
		);
		$this->db->join("oruangan",
					"oruangan.nooruangan = registrasidet.nooruangan","left"
		);
        $this->db->join("klsrawat", 
					"registrasidet.idklsrawat = klsrawat.idklsrawat", "left"
		);
        $this->db->join("bagian", 
					"registrasidet.idbagian = bagian.idbagian", "left"
		);
        $this->db->join("kamar", 
					"registrasidet.idkamar = kamar.idkamar", "left"
		);
        $this->db->join("bed", 
					"registrasidet.idbed = bed.idbed", "left"
		);
        $this->db->join("dokter", 
					"registrasidet.iddokter = dokter.iddokter", "left"
		);
        $this->db->join("penjamin", 
					"penjamin.idpenjamin = registrasi.idpenjamin", "left"
		);
		$this->db->join("klstarif",
					"klstarif.idklstarif = registrasidet.idklstarif", "left"
		);	
		$this->db->where('registrasidet.userbatal', null);
		$this->db->order_by('registrasi.noreg desc');
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('registrasi');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function insorupd_registrasi(){
		$arrnota = $this->input->post("arrnota");
	  //registrasi
        $this->db->trans_begin();
		$query = $this->db->getwhere('registrasi',array('noreg'=>$_POST['tf_noreg']));
		if($query->num_rows() > 0){
			$reg = $this->update_registrasi();
			$regdet = true;
			$res = true;
			$mtskrm = true;
			if(!isset($_POST['tf_noantrian'])) $_POST['tf_noantrian'] = null;
			$regdet = array('noreg'=>$_POST['tf_noreg']);
			$res = array('noantrian'=>$_POST['tf_noantrian']);
		} else {
			$reg = $this->insert_registrasi();
		
		  //registrasi detail
			$regdet = $this->insert_registrasidet($reg);
			
		  //reservasi
			$idres = $this->input->post("idres");
			if($idres != ""){
				$res = $this->update_reservasi($reg, $regdet);				
			}else if($idres == ""){
				$res = $this->insert_reservasi($reg, $regdet);
			}
			
		  //mutasi krm
			$query = $this->db->getwhere('mutasikrm',array('idregdet'=>$res['idregdet']));
			if($query->num_rows() > 0){
				$mtskrm = false;
			} else {
				$mtskrm = $this->insert_mutasikrm($res['idregdet']);
			}
			
			if($_POST['ureg'] == 'RI'){
				$dokterrawat = $this->insert_dokterrawat($res);
			}
		}
		
		if($reg && $regdet && $res && $mtskrm)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["noreg"]=$regdet['noreg'];
            $ret["noantrian"]=$res['noantrian'];
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insorupd_regperawat(){
        $this->db->trans_begin();
		$query = $this->db->getwhere('registrasi',array('noreg'=>$_POST['tf_noreg']));
		
		if($query->num_rows() > 0){
			$regper = $this->update_registrasiper();
			$pasien = $this->update_pasien();
			if($_POST['idbag'] == '3'){
				if($_POST['id'] == '1'){
					if($_POST['tf_g'] != ''){
						if($_POST['tf_khamilan'] == ''){
							$this->kunkehamilan();
						}
					}
				}
			}
		}
		
		if(strlen($_POST['arrdiagnosa']) > 5){
			$query = $this->db->getwhere('kodifikasi',array('noreg'=>$_POST['tf_noreg']));
			if($query->num_rows() > 0) $kodifikasi = true;
			else $kodifikasi = $this->insert_kodifikasi();
			$kodifikasidet = $this->insert_kodifikasidet();
		}
		
		if($regper)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function batal_registrasi(){
		$dataArray['noreg'] = $_POST['tf_noreg'];
		$dataArray['userbatal'] = $this->session->userdata['username'];
		$dataArray['tglbatal'] = date('Y-m-d');
		$this->db->where('noreg', $dataArray['noreg']);
		$z = $this->db->update('registrasidet', $dataArray);
		
		$query = $this->db->getwhere('registrasidet',array('noreg'=>$_POST['tf_noreg']));
		$vreg = $query->row_array();
		//update status kamar
		/*$dataArray = array(
			'idstbed' => 1
		);
		$this->db->where('idbed', $vreg['idbed']);
		$z = $this->db->update('bed', $dataArray);
		*/

        if ($z)
        {
            $ret["success"]=true;
        }
        else
        {
            $ret["success"]=false;
        }
        echo json_encode($ret);
    }
	
	function cekreg(){
		$query = $this->db->getwhere('registrasidet',array('noreg'=>$_POST['noreg']));
		$vreg = $query->row_array();
		
        $q = "SELECT count(*) AS idregdet
				FROM
				  kuitansi
				WHERE
				  idregdet ='".$vreg['idregdet']."'
				  AND
				  idjnskuitansi = '5'
				  AND
				  idstkuitansi = '1'" ;
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->idregdet;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
		
	function insert_registrasi(){
		$dataArray = $this->getFieldsAndValues();
		$z =$this->db->insert('registrasi', $dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret = false;
        }
        return $ret;
    }
		
	function insert_registrasidet($reg){
		$dataArray = $this->getFieldsAndValuesDet($reg);
		$z =$this->db->insert('registrasidet', $dataArray);
        if($z){
            $ret=$dataArray;
			$ret['idregdet'] = $this->db->insert_id();
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function insert_registrasiper(){
		$dataArray = $this->getFieldsAndValuesPer();
		$this->rhlib->insertRecord('registrasidet',$dataArray);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret=$dataArray;
        }else{
            $ret["success"]=false;
            $ret=$dataArray;
        }
        return $ret;
    }
		
	function insert_reservasi($reg, $regdet){
		$dataArray = $this->getFieldsAndValuesRes($reg, $regdet);
		$z =$this->db->insert('reservasi', $dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function update_reservasi($reg, $regdet){
		$dataArray = $this->getFieldsAndValuesResup($reg, $regdet);
		
        $this->db->where('idreservasi', $_POST['tf_idres']);
		$this->db->update('reservasi', $dataArray);
		
		if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
            $ret=$dataArray;
        }
        return $ret;
    }
	
	function insert_mutasikrm($regdet){
		$dataArray = $this->getFieldsAndValuesMtsKrm($regdet);
		$z =$this->db->insert('mutasikrm', $dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }

	function insert_kodifikasi(){
		$dataArray = $this->getFieldsAndValuesKod();
		$z =$this->db->insert('kodifikasi', $dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret = false;
        }
        return $ret;
    }
	
	function insert_kodifikasidet(){
		$query = $this->db->getwhere('kodifikasi',array('noreg'=>$_POST['tf_noreg']));
		$reg = $query->row_array();
		
		if($reg){
			$where['idkodifikasi'] = $reg['idkodifikasi'];
			$del = $this->rhlib->deleteRecord('kodifikasidet',$where);
			$idkodifikasi = $reg['idkodifikasi'];
		} else {
			$idkodifikasi = $this->db->insert_id;
		}
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrdiagnosa']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-',$val);
			$dataArray = $this->getFieldsAndValuesKodDet($idkodifikasi, $vale[0], $vale[1], $vale[2]);
			$z =$this->db->insert('kodifikasidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret = false;
        }
        return $ret;
    }
	
	function insert_dokterrawat($res){
		$dataArray = $this->getFieldsAndValuesdrawat($res);
		$z =$this->db->insert('dokterrawat', $dataArray);
		
	//update bed
		$this->db->where('idbed', $_POST['tf_nmbed']);
		$this->db->update('bed', array('idstbed'=>2));
        if($z){
            $ret=$dataArray;
        }else{
            $ret = false;
        }
        return $ret;
    }
	
	function update_registrasi(){ 				
		$dataArray = array(
            'idpenjamin'=> $this->searchId('idpenjamin','penjamin','nmpenjamin',$_POST['tf_penjamin']),
            'catatan'=> $_POST['ta_catatan']
        );
		//UPDATE
		$this->db->where('noreg', $_POST['tf_noreg']);
		$this->db->update('registrasi', $dataArray);
		$dataArraydet = array(
			'idbagiankirim'=> (isset($_POST['tf_upengirim']) && $_POST['tf_upengirim']!='')?$this->searchId('idbagian','bagian','nmbagian',$_POST['tf_upengirim']):null,
			'iddokterkirim'=> ($_POST['tf_dkirim']!='')?$dkirim = $this->searchId('iddokter','dokter','nmdoktergelar',$_POST['tf_dkirim']):null,
			'nmdokterkirim'=> ($_POST['tf_dkirim']!='')?$_POST['tf_dkirim']:null,
			'idcaradatang'=> $this->searchId('idcaradatang','caradatang','nmcaradatang',$_POST['cb_caradatang']),
        );
		//UPDATE
		$this->db->where('noreg', $_POST['tf_noreg']);
		$this->db->update('registrasidet', $dataArraydet);
		
        if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
            $ret=$dataArray;
        }
        return $ret;
    }

	function update_registrasidet($reg){ 				
		$dataArray = $this->getFieldsAndValuesDet($reg);
		
		//UPDATE
		$this->db->where('noregdetistrasidet', $_POST['tf_noregdet']);
		$this->db->update('registrasidet', $dataArray);

        if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
            $ret=$dataArray;
        }
        return $ret;
    }
	
	function update_registrasiper(){ 				
		$dataArray = $this->getFieldsAndValuesPer();
		
		//UPDATE
		$this->db->where('noreg', $dataArray['noreg']);
		$this->db->update('registrasi', $dataArray);

        if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
            $ret=$dataArray;
        }
        return $ret;
    }
	
	function update_pasien(){ 
		$this->db->where('norm', str_pad($_POST['tf_norm'], 10, "0", STR_PAD_LEFT));
		$this->db->update('pasien', array('alergi'=>$_POST['ta_alergiobat']));
		
        if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
        }
        return $ret;
    }
	
	function kunkehamilan(){		
		$query = $this->db->getwhere('registrasi',array('noreg'=>$_POST['tf_noreg']));
		$reg = $query->row_array();
		$q = "SELECT count(rd.idregdet) AS max_j
				FROM
				  registrasi r
				LEFT JOIN registrasidet rd
				ON rd.noreg = r.noreg
				WHERE
				  r.norm = '".$reg['norm']."'
				  AND
				  rd.userbatal IS NULL
				  AND
				  rd.idbagian = 3
				  AND
				  r.gravidag = '".$reg['gravidag']."'
				  AND
				  r.gravidap = '".$reg['gravidap']."'
				  AND
				  r.gravidaa = '".$reg['gravidaa']."'";
		$q2 = $this->db->query($q);
		$data = $q2->row_array();
		$regj = $this->update_regjml($data);
		if($regj){
			$ret= true;
        }else{
            $ret = false;
        }		
		return $ret;
    }
	
	function update_regjml($data){ 
	
		$this->db->where('noreg', $_POST['tf_noreg']);
		$this->db->update('registrasi', array('kunjhamil'=>$data['max_j']));

        if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
        }
        return $ret;
    }

	function getFieldsAndValues(){
		if($_POST['ureg'] == 'RJ'){
			$_POST['ta_keluhan'] = null;
			$_POST['tf_nmkerabat'] = null;
			$_POST['tf_notelpkerabat'] = null;
			$_POST['tf_klsperawatan'] = null;
			$_POST['tf_nmkamar'] = null;
			$_POST['tf_nmbed'] = null;
			$_POST['cb_hubkeluarga'] = 'Pilih...';
			$_POST['idjnspelayanan'] = 1;
			$_POST['tf_dokterrawat'] = $_POST['tf_dokter'];
			$_POST['tf_ruangan'] = $_POST['tf_upelayanan'];
			$nostart = $this->searchId('alias','bagian','nmbagian',$_POST['tf_ruangan']);
			$_POST['tf_noantrian'] = ($_POST['tf_jmlpantri'] + 1);
			$_POST['posisipasien'] = 2;
			$jpel = 'IRJ';
			$_POST['tf_klsperawatan'] = 'RJ';
		} elseif($_POST['ureg'] == 'RI'){
			$nostart = $this->searchId('alias','bagian','nmbagian',$_POST['tf_ruangan']);
			$_POST['tf_nmbed'] = $this->searchBed($_POST['tf_ruangan'],$_POST['tf_nmkamar'],$_POST['tf_nmbed']);
			$_POST['idjnspelayanan'] = 2;
			$_POST['tf_noantrian'] = null;
			$_POST['posisipasien'] = 5;
			$jpel = 'IRI';
		} elseif($_POST['ureg'] == 'UG'){
			$_POST['ta_keluhan'] = null;
			$_POST['tf_klsperawatan'] = null;
			$_POST['tf_nmkamar'] = null;
			$_POST['tf_nmbed'] = null;
			$_POST['idjnspelayanan'] = 3;
			$_POST['tf_dokterrawat'] = $_POST['tf_dokter'];
			$_POST['tf_noantrian'] = null;
			$_POST['posisipasien'] = 5;
			$_POST['tf_ruangan'] = 'UGD';
			$nostart = $this->searchId('alias','bagian','nmbagian',$_POST['tf_ruangan']);
			$jpel = 'IGD';
			$_POST['tf_klsperawatan'] = 'UGD';
		}
		
		if(is_null($_POST['tf_noreg']) || $_POST['tf_noreg'] == '')
		{
			$nr = $this->getNoregistrasi();
			$nomid = date('y');
			$noend = str_pad($nr, 6, "0", STR_PAD_LEFT);
			$noregistrasi = $nostart.$nomid.$noend;
		} else{
			$noregistrasi = $_POST['tf_noreg'];
		}
		
		$dataArray = array(
             'noreg'=> $noregistrasi,
             'norm'=> str_pad($_POST['tf_norm'], 10, "0", STR_PAD_LEFT),
             'idpenjamin'=> $this->searchId('idpenjamin','penjamin','nmpenjamin',$_POST['tf_penjamin']),
             'idjnspelayanan'=> $_POST['idjnspelayanan'],
             'idstpasien'=> 1,
             'keluhan'=> $_POST['ta_keluhan'],
             'nmkerabat'=> $_POST['tf_nmkerabat'],
             'notelpkerabat'=> $_POST['tf_notelpkerabat'],
             'idhubkeluarga'=> ($_POST['cb_hubkeluarga'] == 'Pilih...')?null:$this->searchId('idhubkeluarga','hubkeluarga','nmhubkeluarga',$_POST['cb_hubkeluarga']),
             'catatan'=> $_POST['ta_catatan']
        );		
		return $dataArray;
	}

	function getFieldsAndValuesDet($reg){
		$dataArray = array(
             'noregdet'=> 1,
             'noreg'=> $reg['noreg'],
             'tglreg'=> date_format(date_create($_POST['df_tglshift']), 'Y-m-d'),
             'jamreg'=> date_format(date_create($_POST['tf_jamshift']), 'H:i:s'),
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['tf_waktushift']),
             'idbagiankirim'=> (isset($_POST['tf_upengirim']) && $_POST['tf_upengirim']!='')?$this->searchId('idbagian','bagian','nmbagian',$_POST['tf_upengirim']):null,
             'iddokterkirim'=> ($_POST['tf_dkirim']!='')?$dkirim = $this->searchId('iddokter','dokter','nmdoktergelar',$_POST['tf_dkirim']):null,
             'nmdokterkirim'=> (isset($dkirim))?null:$_POST['tf_dkirim'],
             'idbagian'=> $this->searchId('idbagian','bagian','nmbagian',$_POST['tf_ruangan']),
             'iddokter'=> $this->searchId('iddokter','dokter','nmdoktergelar',$_POST['tf_dokterrawat']),
             'idcaradatang'=> $this->searchId('idcaradatang','caradatang','nmcaradatang',$_POST['cb_caradatang']),
             'tglmasuk'=> date_format(date_create($_POST['df_tglshift']), 'Y-m-d'),
             'jammasuk'=> date_format(date_create($_POST['tf_jamshift']), 'H:i:s'),
             'umurtahun'=> $_POST['tf_umurthn'],
             'umurbulan'=> $_POST['tf_umurbln'],
             'umurhari'=> $_POST['tf_umurhari'],
             'idklsrawat'=> $this->searchId('idklsrawat','klsrawat','nmklsrawat',$_POST['tf_klsperawatan']),
             'idklstarif'=> $this->searchId('idklstarif','klsrawat','nmklsrawat',$_POST['tf_klsperawatan']),
             'idkamar'=> $this->searchId('idkamar','kamar','nmkamar',$_POST['tf_nmkamar']),
             'idbed'=> $_POST['tf_nmbed'],
             'idstregistrasi'=> 1,
             'userinput'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d'),
        );
		return $dataArray;
	}
	
	function getFieldsAndValuesPer(){
		$noreg = $this->input->post("tf_noreg");
		$keluhan = $this->input->post("ta_keluhan");
		$catatan = $this->input->post("ta_catatanreg");
		$tinggi = $this->input->post("tf_tinggi");
		$berat = $this->input->post("tf_berat");
		$systole = $this->input->post("tf_systole");
		$diastole = $this->input->post("tf_diastole");
		$gravidag = $this->input->post("tf_g");
		$gravidap = $this->input->post("tf_p");
		$gravidaa = $this->input->post("tf_a");
		$gravidai = $this->input->post("tf_i");
		
		$dataArray = array(
             'noreg'=> $noreg,
             'keluhan'=> $keluhan,
             'catatan'=> $catatan,
             'tinggibadan'=> $tinggi,
             'beratbadan'=> $berat,
             'systole'=> $systole,
             'diastole'=> $diastole,
             'gravidag'=> $gravidag,
             'gravidap'=> $gravidap,
             'gravidaa'=> $gravidaa,
             'gravidai'=> $gravidai
        );		
		return $dataArray;
	}

	function getFieldsAndValuesRes($reg, $regdet){
		$this->db->select('max(idregdet) as max');
		$this->db->from('registrasidet');
		$q = $this->db->get();
		$dataregdet = $q->row_array();
		$idstposisipasien = 2;
		if($_POST['ureg'] == 'RI') $idstposisipasien = 5;
		$dataArray = array(
             'tglreservasi'=> date_format(date_create($regdet['tglreg']), 'Y-m-d'),
             'jamreservasi'=> date_format(date_create($regdet['jamreg']), 'H:i:s'),
             'idshift'=> $regdet['idshift'],
             'norm'=> $reg['norm'],
             'nmpasien'=> $_POST['tf_nmpasien'],
             //'email'=> $_POST['alamat'],
             'notelp'=> $_POST['notelp'],
             'nohp'=> $_POST['nohp'],
             'iddokter'=> $regdet['iddokter'],
             'idbagian'=> $regdet['idbagian'],
             'idstreservasi'=> 1,
             'idregdet'=> $dataregdet['max'],
             'noantrian'=> $_POST['tf_noantrian'],
             'userinput'=> $this->session->userdata['username'],
             'tglinput'=> date('Y-m-d'),
             'idstposisipasien'=> $idstposisipasien,
             'stdatang'=> 1,
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesResup($reg, $regdet){
		if($_POST['tf_jmlpantrires'] != ""){
			$na = $_POST['tf_jmlpantrires'];
		}else if($_POST['tf_jmlpantrires'] == ""){
			$na = $_POST['tf_jmlpantri'];
		}
		$dataArray = array(
             'tglreservasi'=> date_format(date_create($regdet['tglreg']), 'Y-m-d'),
             'idshift'=> $regdet['idshift'],
             'norm'=> $reg['norm'],
             'nmpasien'=> $_POST['tf_nmpasien'],
             //'email'=> $_POST['alamat'],
             'notelp'=> $_POST['notelp'],
             'nohp'=> $_POST['nohp'],
             'iddokter'=> $regdet['iddokter'],
             'idbagian'=> $regdet['idbagian'],
             'idstreservasi'=> 1,
             'idregdet'=> $regdet['idregdet'],
             'noantrian'=> $na,
             'userinput'=> $this->session->userdata['username'],
             'idstposisipasien'=> 2,
             'stdatang'=> 1
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesMtsKrm($regdet){
		if($_POST['ureg'] == 'RJ'){
			$nostart = 'RJ';
		} elseif($_POST['ureg'] == 'RI'){
			$nostart = 'RI';
		} elseif($_POST['ureg'] == 'UG'){
			$nostart = 'UG';
		} else{
			$nostart = 'EL';
		}
		
		$nr = $this->getNomutasikrm();
		$nomid = date('y');
		$noend = str_pad($nr, 6, "0", STR_PAD_LEFT);
		$nomutasikrm = $nostart.$nomid.$noend;
		
		$this->db->select('max(idregdet) as max');
		$this->db->from('registrasidet');
		$q = $this->db->get();
		$dataregdet = $q->row_array();
		$dataArray = array(
             'nomutasikrm'=> $nomutasikrm,
             'idregdet'=> $regdet,
             'idjnsstkrm'=> 1,
             'idjnsrefkrm'=> 1,
             'tglminta'=> date('Y-m-d'),
             'jamminta'=> date('H:i:s')
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesKod(){
		if($_POST['cb_tlanjut'] == '')$_POST['cb_tlanjut'] = null;
		else $_POST['cb_tlanjut'] = $this->searchId('idtindaklanjut','tindaklanjut','nmtindaklanjut',$_POST['cb_tlanjut']);
		if($_POST['cb_pkematian'] == '')$_POST['cb_pkematian'] = null;
		else $_POST['cb_pkematian'] = $this->searchId('idkematian','sebabkematian','nmkematian',$_POST['cb_pkematian']);
		$dataArray = array(
             'tglkodifikasi'=> date('Y-m-d'),
             'jamkodifikasi'=> date('H:i:s'),
             'noreg'=> $_POST['tf_noreg'],
             'idtindaklanjut'=> $_POST['cb_tlanjut'],
             'idkematian'=> $_POST['cb_pkematian'],
             'userid'=> $this->session->userdata['user_id']
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesKodDet($idkodifikasi, $val1,$val2,$val3){
		/* $hasil = $this->db->query("SELECT idkodifikasi FROM kodifikasi ORDER BY idkodifikasi DESC LIMIT 1");
		foreach($hasil->result() as $row){
			$idkodifikasi= $row->idkodifikasi;
		} */
			
		$dataArray = array(
             'idkodifikasi'=> $idkodifikasi,
             'idpenyakit'=> $val1,
             'idjnsdiagnosa'=> $this->searchId('idjnsdiagnosa','jdiagnosa','nmjnsdiagnosa',$val2),
             'idjstpenyakit'=> $this->searchId('idjstpenyakit','jstpenyakit','nmjstpenyakit',$val3),
             'idmappenyakit'=> null
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesdrawat($res){
		$dataArray = array(
             'idregdet'=> $res['idregdet'],
             'iddokter'=> $res['iddokter'],
             'idstdokterrawat'=> 1,
             'tglmulai'=> date('Y-m-d'),
             'jammulai'=> date('H:i:s')
        );		
		return $dataArray;
	}
	
	function getNoregistrasi(){
		$zzz = $this->searchId('alias','bagian','nmbagian',$_POST['tf_ruangan']);
        $this->db->select("count(noreg) AS max_np");
        $this->db->from("registrasi");
        $this->db->where('SUBSTRING(noreg,1,2)', $zzz);
        $this->db->where('SUBSTRING(noreg,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function getnoregistrasi1(){
		$alias = 'RR';//$this->searchId('alias','bagian','nmbagian',$_POST['tf_ruangan']);
		$q = $this->db->query("SELECT 
			max(noreg) AS max_no,
			max(cast(right(noreg,6) AS UNSIGNED)) AS max_count,
			6 - length(max(cast(right(noreg,6) AS UNSIGNED))) AS length_count
		FROM
			registrasi 
		WHERE 
			SUBSTRING(noreg,1,2)='".$alias."' 
			AND SUBSTRING(noreg,3,2)='".date('y')."'");
		$data = $q->row_array();
		
		if(is_null($data['max_no'])) {
			$max = $alias.strval(date('y'))."000001";
		} else {
			$par = "";
			$add = "";
			for($i=1;$i <= $data['length_count'];$i++) {
				$par .= '0';
			}
			$add = $data['max_count'] + 1;
			$max = $alias.strval(date('y')).$par.$add;
		}
		
		return $max;
	}
	
	function getNomutasikrm(){
        //$this->db->select("count(nomutasikrm) AS max_np");
       /* $this->db->select("substring(max(nomutasikrm),7,4) AS max_np");
        
        $this->db->from("mutasikrm");
        $this->db->where('SUBSTRING(nomutasikrm,1,2)', $_POST['ureg']);
        $this->db->where('SUBSTRING(nomutasikrm,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max; */

		$q = "SELECT SUBSTRING(max(nomutasikrm), 7, 4) AS max_np
		FROM (`mutasikrm`)
		WHERE SUBSTRING(nomutasikrm,1,2) = '".$_POST['ureg']."'
		AND SUBSTRING(nomutasikrm,3,2) = '".date('y')."'";
		$query = $this->db->query($q);

		$query->result();
		$data = $query->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }

	function searchBed($nmbagian, $nmkamar, $nmbed){
        $this->db->select('idbed');
        $this->db->from('v_bed');
        $this->db->where('nmbagian',$nmbagian);
        $this->db->where('nmkamar',$nmkamar);
        $this->db->where('nmbed',$nmbed);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id['idbed'] = null;
		}
		return $id['idbed'];
    }

	function getUpelJadwal(){
		$idjnspelayanan = $this->input->post("idjnspelayanan");
        $this->db->select("d.nmdoktergelar,bag.nmbagian,bag.idbagian");
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('userbatal', null);
		$this->db->order_by('rd.idregdet DESC');
		$this->db->where('rd.userinput',$this->session->userdata['user_id']);
		if($idjnspelayanan != '')$this->db->where('r.idjnspelayanan', $idjnspelayanan);
		$this->db->order_by('rd.idregdet desc');
		$this->db->limit(1);
		$q = $this->db->get();
		$def = $q->row_array();
		echo json_encode($def);
    }
	
	/*function insorupd_nota($regdet, $arrnota){
		$kque = $this->insert_kuitansi($regdet);
		$kui = $kque['nokuitansi'];
		
		$kuidet = $this->insert_kuitansidet($kui);
		
		$nota = $this->insert_nota($regdet, $kui);
		$vnota = $nota['nonota'];
		
		$notadet = $this->insert_notadet($vnota, $regdet, $arrnota);
		$ret['nokuitansi'] = $kui;
		$ret['nonota'] = $vnota;
		return $ret;
    }
		
	function insert_kuitansi($regdet){
		$dataArray = $this->getFieldsAndValuesKui($regdet);
		$z =$this->db->insert('kuitansi',$dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
		
	function insert_kuitansidet($kui){
		$dataArray = $this->getFieldsAndValuesKuiDet($kui);
		$z =$this->db->insert('kuitansidet',$dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
		
	function insert_nota($regdet, $kui){
		$dataArray = $this->getFieldsAndValuesNota($regdet, $kui);
		$z =$this->db->insert('nota',$dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
		
	function insert_notadet($nota, $regdet, $arrnota){
		$k=array('[',']','"');
        $r=str_replace($k, '', $arrnota);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-',$val);
			$dataArray = $this->getFieldsAndValuesNotaDet($nota, $regdet, $vale[0], $vale[1]);
			$z =$this->db->insert('notadet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }

	function getFieldsAndValuesKui($regdet){
		if($_POST['ureg'] == 'RJ'){
			$jpel = 'IRJ';
		} elseif($_POST['ureg'] == 'RI'){
			$jpel = 'IRI';
		} elseif($_POST['ureg'] == 'UG'){
			$jpel = 'IGD';
		}
		
		$nk = $this->getNokuitansi();
		$nostart = 'NK';
		$nomid = date('y');
		$noend = str_pad($nk, 8, "0", STR_PAD_LEFT);
		$nokuitansi = $nostart.$nomid.$noend;
		
		$dataArray = array(
             'nokuitansi'=> $nokuitansi,
             'tglkuitansi'=> date('Y-m-d'),
             'jamkuitansi'=> date('H:i:s'),
             'idshift'=> $regdet['idshift'],
             //'noreg'=> $regdet['noreg'],
             'idstkuitansi'=> 1,
             //'userid'=> $this->session->userdata['user_id'],
             'idsbtnm'=> $this->searchSbtnm('idsbtnm','jsbtnm',$regdet['umurtahun'], $this->searchId('idjnskelamin','pasien','norm',$_POST['tf_norm'])),
             'atasnama'=> $this->searchId('nmpasien','pasien','norm',$_POST['tf_norm']),
             //'idjnskas'=> 1,
             'idjnskuitansi'=> $this->searchId('idjnskuitansi','jkuitansi','kdjnskuitansi',$jpel),
             'pembulatan'=> 0,
             'total'=> 0,
             //'ketina'=> $_POST['catatan'],
             //'keteng'=> 0
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesKuiDet($kui){
		$dataArray = array(
             'nokuitansi'=> $kui,
             //'idbank'=> date('Y-m-d'),
             'idcarabayar'=> $this->searchId('idcarabayar','carabayar','nmcarabayar',$_POST['cb_carabayar']),
             'jumlah'=> 0
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesNota($regdet, $kui){
		$nr = $this->getNonota();
		$nostart = 'N'.$_POST['ureg'];
		$nomid = date('y');
		$nombln = date('m');
		$noend = str_pad($nr, 5, "0", STR_PAD_LEFT);
		$nonota = $nostart.$nomid.$nombln.$noend;
		
		$dataArray = array(
             'nonota'=> $nonota,
             'nona'=> $nonota,
             'tglnota'=> date('Y-m-d'),
             'jamnota'=> date('H:i:s'),
             'idbagian'=> $regdet['idbagian'],
             'iddokter'=> $regdet['iddokter'],
             'idjnstransaksi'=> 1,
             'idsttransaksi'=> 1,
             'nokuitansi'=> $kui,
             'idregdet'=> $regdet['idregdet'],
             'noorder'=> 1,
             'idshift'=> $regdet['idshift'],
             'noresep'=> 1,
             'catatan'=> $_POST['ta_catatan'],
             'diskon'=> 0,
             'uangr'=> 0,
             'userinput'=> $this->session->userdata['username'],
             'tglinput'=> date('Y-m-d'),
			 'idregdettransfer'=> null
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesNotaDet($nota, $regdet, $val0, $val1){
		$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0));
		$item = $query->row_array();
		if(!$item['tarifjs']) $item['tarifjs'] = 0;
		if(!$item['tarifjm']) $item['tarifjm'] = 0;
		if(!$item['tarifjp']) $item['tarifjp'] = 0;
		if(!$item['tarifbhp']) $item['tarifbhp'] = 0;
		
		if($item['satuankcl'] != null) $item['satuankcl'] = $this->searchId('idsatuan','jsatuan','nmsatuan',$item['satuankcl']);
		else $item['satuankcl'] = null;
		
		$dataArray = array(
             'nonota'=> $nota,
             'kditem'=> $val0,
             //'idjnstarif'=> 0,
             //'kdresep'=> $_POST['kdnota'],
             'idsatuan'=> $item['satuankcl'],
             'qty'=> $val1,
             'tarifjs'=> $item['tarifjs'],
             'tarifjm'=> $item['tarifjm'],
             'tarifjp'=> $item['tarifjp'],
             'tarifbhp'=> $item['tarifbhp'],
             'diskonjs'=> 0,
             'diskonjm'=> 0,
             'diskonjp'=> 0,
             'diskonbhp'=> 0,
             //'uangr'=> 0,
             'iddokter'=> $regdet['iddokter'],
             'idperawat'=> 1,
			 'idstbypass'=> 1
        );
		return $dataArray;
	}
	
	function getNonota(){
        $this->db->select("count(nonota) AS max_np");
        $this->db->from("nota");
        $this->db->where('SUBSTRING(nonota,4,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function getNokuitansi(){
        $this->db->select("count(nokuitansi) AS max_np");
        $this->db->from("kuitansi");
        $this->db->where('SUBSTRING(nokuitansi,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function searchSbtnm($select, $tbl, $val, $val2){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where('dariumur >=', $val);
        $this->db->where('sampaiumur <=', $val);
        $this->db->where('idjnskelamin', $val2);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	*/
}
