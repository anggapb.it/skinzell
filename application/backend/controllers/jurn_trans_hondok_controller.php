<?php

class Jurn_trans_Hondok_Controller extends Controller {
  public function __construct()
  {
    parent::Controller();
    $this->load->library('session');
  }
  
  /* =========================================== START JURNAL SUPPLIER PEMBELIAN ============================= */
  function posting_honor_dokter(){

    $nohondok  = $this->input->post("nohondok_posting");
    $tgldikeluarkan_posting  = $this->input->post("tgldikeluarkan_posting");
    //$nmdoktergelar  = $this->input->post("nmdoktergelar");
    $get_nmdoktergelar = $this->db->query("SELECT dokter.nmdoktergelar from dokter where dokter.iddokter = (select hondok.iddokter from hondok where nohondok = '".$nohondok."')")->row_array();
    $nmdoktergelar = $get_nmdoktergelar['nmdoktergelar'];
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();

    if(empty($nohondok) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nopo dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tgldikeluarkan_posting,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Honor Dokter : '.$nmdoktergelar,
      'noreff' => $nohondok,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => 12, // 12 = honor dokter
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //insert jurnal det persediaan
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        if(isset($item_jdet[4]) && isset($item_jdet[5]))
        {
          /*
            item_jdet[0] = idakun   
            item_jdet[1] = kdakun 
            item_jdet[2] = nmakun 
            item_jdet[3] = noreff 
            item_jdet[4] = debit 
            item_jdet[5] = kredit
          */
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '3', //3 = jurnal persediaan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;

        }

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  

  function get_honor_dokter(){

    $tglhondok        = $this->input->post("tglhondok");
    $tglhondok_akhir  = $this->input->post("tglhondok_akhir");
    $pencarian       = $this->input->post("pencarian");
    
    if(empty($tglhondok)) $tglhondok = date('Y-m-d');
    if(empty($tglhondok_akhir)) $tglhondok_akhir= date('Y-m-d');

    $this->db->where('tglhondok >=', $tglhondok);
    $this->db->where('tglhondok <=', $tglhondok_akhir);
    
    if(!empty($pencarian)){
      $fields = array(
        'nohondok',       'tglhondok',      'tglawal',
        'tglakhir',       'jasadrjaga',     'potonganlain',
        'userid',         'iddokter',       'approval',
        'idjnspembayaran','nmjnspembayaran','idstbayar',
        'nmstbayar',      'tglinput',       'catatan',
        'tgldikeluarkan', 'nmdoktergelar',  'kdjurnal',
        'status_posting', 'jumlah',
      );

      foreach($fields as $field){
        $this->db->or_like($field, $pencarian);
      }
    }

    $this->db->select("*");
    $this->db->from("v_jurn_hondok");
    $q = $this->db->get();  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }
 
  function get_honor_dokterdet(){
    $nohondok     = $this->input->post("nohondok");
    if(empty($nohondok)) $nohondok = '0';

    $this->db->where('nohondok', $nohondok);
    $this->db->select("*");
    $this->db->from("v_jurn_hondokdet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_honor_dokter_jurnaling(){
    $nohondok     = $this->input->post("nohondok");
    if(empty($nohondok)) $nohondok = '0';

    $total_kredit = 0;
    $total_honor  = 0;
    $total_zis    = 0;

    $data_hondok = $this->db->get_where('v_hondok', array('nohondok' => $nohondok))->row_array();

    if($data_hondok['idjnspembayaran'] == 1){ // 1 = tunai
      $kdakun_kredit = '11100'; // kas kecil
    }elseif($data_hondok['idjnspembayaran'] == 2){ //2 = transfer
      $kdakun_kredit = '11200'; // kas di bank  
    }

    $akun_kredit = $this->db->get_where('akun', array('kdakun' => $kdakun_kredit))->row_array();
    $akun_hondok = $this->db->get_where('akun', array('kdakun' => '61100'))->row_array(); // Biaya Medis / Honor Dokter
    $akun_zis = $this->db->get_where('akun', array('kdakun' => '21700'))->row_array(); // Titipan ZIS
    

    $total_kredit = ( $data_hondok['jasadrjaga'] - $data_hondok['potonganlain'] );
    $total_honor  = ( $data_hondok['jasadrjaga'] - $data_hondok['potonganlain'] );
    
    $data = $this->db->get_where('hondokdet', array('nohondok' => $nohondok))->result_array();
    if(!empty($data))
    {
      foreach($data as $idx => $dt)
      {
        $total_kredit += ($dt['jasamedis'] - $dt['diskon']);
        $total_honor  += ($dt['jasamedis'] - ($dt['diskon'] + $dt['zis']));
        $total_zis    += $dt['zis'];
      }
    }

    $breakdown_jurnal = array();

    //debit honor
    $breakdown_jurnal[] = array(
      'idakun' => $akun_hondok['idakun'], 
      'kdakun' => $akun_hondok['kdakun'], 
      'nmakun' => $akun_hondok['nmakun'], 
      'noreff' => $data_hondok['kddokter'],
      'debit' => $total_honor, 
      'kredit'=> 0
    );

    //debit titipan zis (jika ada)
    if($total_zis > 0){
      $breakdown_jurnal[] = array(
        'idakun' => $akun_zis['idakun'], 
        'kdakun' => $akun_zis['kdakun'], 
        'nmakun' => $akun_zis['nmakun'], 
        'noreff' => '',
        'debit' => $total_zis, 
        'kredit'=> 0
      );
    }

    //kredit kas
    $breakdown_jurnal[] = array(
      'idakun' => $akun_kredit['idakun'], 
      'kdakun' => $akun_kredit['kdakun'], 
      'nmakun' => $akun_kredit['nmakun'], 
      'noreff' => '',
      'debit' => 0, 
      'kredit'=> $total_kredit
    );

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);

    echo json_encode($build_array);
  }
  
  /* =========================================== START JURNAL SUPPLIER RETUR ================================= */
  
  function get_nojurnal_khusus(){
    //$q = "SELECT getOtoNojurnal(now()) as nm;";
    $q = "SELECT getOtoNoJurnalKhusus(now()) as nm;";
    $query  = $this->db->query($q);
    $nm= ''; 
                
    if ($query->num_rows() != 0)
    {
        $row = $query->row();
        $nm=$row->nm;
    }
    return $nm;
  }

}
