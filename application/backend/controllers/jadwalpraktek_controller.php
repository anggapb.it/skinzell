<?php

class Jadwalpraktek_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_jadwalpraktek(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        
        $fields		= $this->input->post("fields");
        $query		= $this->input->post("query");
      
        $this->db->select('jadwalpraktek.*, bagian.nmbagian, dokter.nmdoktergelar, hari.nmhari, shift.nmshift');
        $this->db->from('jadwalpraktek');
		$this->db->join('bagian',
                'bagian.idbagian = jadwalpraktek.idbagian', 'left');
 		$this->db->join('dokter',
                'dokter.iddokter = jadwalpraktek.iddokter', 'left');
		$this->db->join('hari',
                'hari.idhari = jadwalpraktek.idhari', 'left');
		$this->db->join('shift',
                'shift.idshift = jadwalpraktek.idshift', 'left');
				
		$this->db->order_by('idbagian');
        
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
	
        $this->db->select('jadwalpraktek.*, bagian.nmbagian, dokter.nmdokter, hari.nmhari, shift.nmshift');
        $this->db->from('jadwalpraktek');
		$this->db->join('bagian',
                'bagian.idbagian = jadwalpraktek.idbagian', 'left');		
 		$this->db->join('dokter',
                'dokter.iddokter = jadwalpraktek.iddokter', 'left');
		$this->db->join('hari',
                'hari.idhari = jadwalpraktek.idhari', 'left');
		$this->db->join('shift',
                'shift.idshift = jadwalpraktek.idshift', 'left');
		
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_jadwalpraktek(){     
		$where['idjadwalpraktek'] = $_POST['idjadwalpraktek'];
		$del = $this->rhlib->deleteRecord('jadwalpraktek',$where);
        return $del;
    }
		
	function insert_jadwalpraktek(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('jadwalpraktek',$dataArray);
        return $ret;
    }

	function update_jadwalpraktek(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('idjadwalpraktek', $_POST['idjadwalpraktek']);
		$this->db->update('jadwalpraktek', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
		$dataArray = array(
             //'idjadwalpraktek'	=> $_POST['idjadwalpraktek'],
			 'idbagian'			=> $_POST['idbagian'],
			 'iddokter'			=> ($_POST['iddokter']) ? $this->id_dokter('nmdoktergelar',$_POST['iddokter']) : null,
             'idhari		'	=> $_POST['idhari'],
			 'idshift'			=> $_POST['idshift'],
			 'jampraktek'		=> $_POST['jampraktek'],
             'keterangan'		=> $_POST['keterangan'],
        );		
		return $dataArray;
	}
	
	function getNmdokter(){
		$query = $this->db->getwhere('dokter',array('iddokter'=>$_POST['iddokter']));
		$nm = $query->row_array();
		echo json_encode($nm['nmdoktergelar']);
    }
	
	function id_dokter($where, $val){
		$query = $this->db->getwhere('dokter',array($where=>$val));
		$id = $query->row_array();
		return $id['iddokter'];
    }
	
	function getNmHari(){
        $shift			= $this->input->post("shift");
        $tgl			= $this->input->post("tgl");
        $idbagian		= $this->input->post("idbagian");
        $cidjnspel		= $this->input->post("cidjnspel");
        $cquery			= $this->input->post("cquery");
        /* $allday		= $this->input->post("allday");
        $zxzx                  = date_format(date_create($tgl), 'Y-m-d');
		
		$this->db->select("
			IF(DAYOFWEEK('". $zxzx ."')=1,'minggu',
			  IF(DAYOFWEEK('". $zxzx ."')=2,'senin',
				IF(DAYOFWEEK('". $zxzx ."')=3,'selasa',
				  IF(DAYOFWEEK('". $zxzx ."')=4,'rabu',
					IF(DAYOFWEEK('". $zxzx ."')=5,'kamis',
					  IF(DAYOFWEEK('". $zxzx ."')=6,'jumat','sabtu')))))) AS hariini
		", false);
		$this->db->select("
			IF(DAYOFWEEK(now())=1,'minggu',
			  IF(DAYOFWEEK(now())=2,'senin',
				IF(DAYOFWEEK(now())=3,'selasa',
				  IF(DAYOFWEEK(now())=4,'rabu',
					IF(DAYOFWEEK(now())=5,'kamis',
					  IF(DAYOFWEEK(now())=6,'jumat','sabtu')))))) AS hariini,
		", false);
		$q = $this->db->get();
		$hari = $q->row_array(); */
		
		$this->db->select("*");
        $this->db->from("jadwalpraktek");
		$this->db->join('bagian',
                'bagian.idbagian = jadwalpraktek.idbagian', 'left');
 		$this->db->join('dokter',
                'dokter.iddokter = jadwalpraktek.iddokter', 'left');
		$this->db->join('hari',
                'hari.idhari = jadwalpraktek.idhari', 'left');
		$this->db->join('spesialisasi',
                'spesialisasi.idspesialisasi = dokter.idspesialisasi', 'left');
		//$this->db->where("CURTIME() BETWEEN SUBSTRING(jampraktek,1,5) AND SUBSTRING(jampraktek,9,5)");
		//if(!$allday)$this->db->where("nmhari",$hari['hariini']);
		//if($shift != '')$this->db->where("idshift",$shift);
		
		if($idbagian)$this->db->where("jadwalpraktek.idbagian",$idbagian);
		if($cidjnspel)$this->db->where("bagian.idjnspelayanan",$cidjnspel);
		if($cquery !=""){
			$arrquery = explode(' ', $cquery);
			foreach($arrquery AS $val){
				$this->db->like('dokter.nmdoktergelar', $val);
			}
        }
		$this->db->groupby('nmdoktergelar');
		$this->db->order_by('nmdoktergelar');
		$q = $this->db->get();
		$jadwal = $q->result();
		
		$build_array = array ("success"=>true,"results"=>0,"data"=>$jadwal);
		
		echo json_encode($build_array);
	}
	
	function cekdktr(){
        $idbagian		= $this->input->post("idbagian");
        $cquery			= $this->input->post("cquery");
		
		$this->db->select("*");
        $this->db->from("jadwalpraktek");
		$this->db->join('bagian',
                'bagian.idbagian = jadwalpraktek.idbagian', 'left');
 		$this->db->join('dokter',
                'dokter.iddokter = jadwalpraktek.iddokter', 'left');
		$this->db->join('hari',
                'hari.idhari = jadwalpraktek.idhari', 'left');
		$this->db->join('spesialisasi',
                'spesialisasi.idspesialisasi = dokter.idspesialisasi', 'left');		
		if($idbagian)$this->db->where("jadwalpraktek.idbagian",$idbagian);
		if($cquery !=""){
			$arrquery = explode(' ', $cquery);
			foreach($arrquery AS $val){
				$this->db->like('dokter.nmdoktergelar', $val);
			}
        }
		$this->db->groupby('nmdoktergelar');
		$this->db->order_by('nmdoktergelar');
		$q = $this->db->get();
		$cdktr = $q->row_array();
		
		echo json_encode($cdktr);
	}
	
	function get_cbdijpdokter(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select('*');
        $this->db->from('v_cbdijpdokter');				
		$this->db->order_by('idbagian');
                
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
				
        echo json_encode($build_array);
    }
}
