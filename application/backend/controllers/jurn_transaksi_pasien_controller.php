<?php
/*
  Fungsi Jurnal khusus transaksi pasien
*/
class Jurn_transaksi_pasien_Controller extends Controller {
  public function __construct()
  {
    parent::Controller();
    $this->load->library('session');
    //$this->load->library('rhlib');
    $this->load->model('generalsetting');
  }
  
  /* ==================== START JURNAL TRANSAKSI PASIEN RJ & UGD ==================== */

  function posting_transaksi_pasien_rjugd()
  {

    $nokuitansi  = $this->input->post("nokuitansi_posting");
    $tglkuitansi_posting  = $this->input->post("tglkuitansi_posting");
    $jtransaksi_posting  = $this->input->post("jtransaksi_posting");
    $atasnama  = $this->input->post("atasnama");
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $post_grid_persediaan  = $this->input->post("post_grid_persediaan");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();
    $idjtransaksi = ($jtransaksi_posting == 4) ? 8 : 6;
    $jtransaksi = ($jtransaksi_posting == 4) ? 'UGD' : 'Rawat Jalan';

    if(empty($nokuitansi) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nopo dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglkuitansi_posting,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Transaksi Pasien '. $jtransaksi .': '.$atasnama,
      'noreff' => $nokuitansi,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => $idjtransaksi,
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //jurnal pendapatan
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '1', // 1 = jurnal pendapatan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    //jurnal persediaan
    $jdet_rows = explode(';', $post_grid_persediaan);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '3', // 3 = jurnal persediaan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  

  function get_transaksi_pasien_rjugd(){
    $tglkuitansi     = $this->input->post("tglkuitansi");
    $tglkuitansi_akhir     = $this->input->post("tglkuitansi_akhir");
    if(empty($tglkuitansi)) $tglkuitansi = date('Y-m-d');
    if(empty($tglkuitansi_akhir)) $tglkuitansi_akhir= date('Y-m-d');

    $this->db->where('tglkuitansi >=', $tglkuitansi);
    $this->db->where('tglkuitansi <=', $tglkuitansi_akhir);
    
    $this->db->select("*");
    $this->db->from("v_jurn_trans_rjugd");
    $q = $this->db->get();  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_rjugddet(){
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $this->db->where('nokuitansi', $nokuitansi);
    $this->db->select("*");
    $this->db->from("v_jurn_trans_rjugddet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_rjugd_notadet(){
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    /*
    $this->db->where('nokuitansi', $nokuitansi);
    $this->db->select("*");
    $this->db->from("v_jurn_trans_rjugdnotadet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    */

    $query = "
    SELECT `notadet`.`idnotadet` AS `idnotadet`
         , `nota`.`nokuitansi` AS `nokuitansi`
         , `notadet`.`nonota` AS `nonota`
         , `notadet`.`kditem` AS `kditem`
         , `notadet`.`idjnstarif` AS `idjnstarif`
         , `notadet`.`koder` AS `koder`
         , `notadet`.`idsatuan` AS `idsatuan`
         , `notadet`.`qty` AS `qty`
         , `notadet`.`tarifjs` AS `tarifjs`
         , `notadet`.`tarifjm` AS `tarifjm`
         , `notadet`.`tarifjp` AS `tarifjp`
         , `notadet`.`tarifbhp` AS `tarifbhp`
         , `notadet`.`diskonjs` AS `diskonjs`
         , `notadet`.`diskonjm` AS `diskonjm`
         , `notadet`.`diskonjp` AS `diskonjp`
         , `notadet`.`diskonbhp` AS `diskonbhp`
         , `notadet`.`uangr` AS `uangr`
         , `notadet`.`hrgjual` AS `hrgjual`
         , `notadet`.`hrgbeli` AS `hrgbeli`
         , `notadet`.`iddokter` AS `iddokter`
         , `notadet`.`idperawat` AS `idperawat`
         , `notadet`.`idstbypass` AS `idstbypass`
         , `notadet`.`aturanpakai` AS `aturanpakai`
         , `notadet`.`dijamin` AS `dijamin`
         , `notadet`.`stdijamin` AS `stdijamin`
         , ((`notadet`.`tarifjs` * `notadet`.`qty`) - `notadet`.`diskonjs`) AS `totaljs`
         , ((`notadet`.`tarifjm` * `notadet`.`qty`) - `notadet`.`diskonjm`) AS `totaljm`
         , ((`notadet`.`tarifjp` * `notadet`.`qty`) - `notadet`.`diskonjp`) AS `totaljp`
         , ((`notadet`.`tarifbhp` * `notadet`.`qty`) - `notadet`.`diskonbhp`) AS `totalbhp`
         , (((((`notadet`.`tarifjs` * `notadet`.`qty`) - `notadet`.`diskonjs`) + ((`notadet`.`tarifjm` * `notadet`.`qty`) - `notadet`.`diskonjm`)) + ((`notadet`.`tarifjp` * `notadet`.`qty`) - `notadet`.`diskonjp`)) + ((`notadet`.`tarifbhp` * `notadet`.`qty`) - `notadet`.`diskonbhp`)) AS `bayaritem2`
         , (SELECT (`notadet`.`qty` * (((`notadet`.`tarifjs` + `notadet`.`tarifjm`) + `notadet`.`tarifjp`) + `notadet`.`tarifbhp`))) AS `tagihanitem`
         , (SELECT (((`notadet`.`diskonjs` + `notadet`.`diskonjm`) + `notadet`.`diskonjp`) + `notadet`.`diskonbhp`)) AS `diskonitem`
         , (SELECT ((`notadet`.`qty` * (((`notadet`.`tarifjs` + `notadet`.`tarifjm`) + `notadet`.`tarifjp`) + `notadet`.`tarifbhp`)) - (((`notadet`.`diskonjs` + `notadet`.`diskonjm`) + `notadet`.`diskonjp`) + `notadet`.`diskonbhp`))) AS `bayaritem`
         , (SELECT if((left(`notadet`.`kditem`, 1) = 'B'), 
            (SELECT `barang`.`nmbrg` FROM `barang` WHERE (`barang`.`kdbrg` = `notadet`.`kditem`)), 
            (SELECT `pelayanan`.`nmpelayanan` FROM `pelayanan` WHERE (`pelayanan`.`kdpelayanan` = `notadet`.`kditem`)))
         ) AS `nmitem`
    FROM
      ((`nota`
    JOIN `notadet`))
    WHERE
      `nota`.`idsttransaksi` = 1 AND `nota`.`nonota` = `notadet`.`nonota` AND `nota`.`nokuitansi` = '".$nokuitansi."'
      
    ";
    $data = $this->db->query($query)->result_array();
    $ttl = count($data);

    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_rjugd_jurnaling(){
    $akun_profesi = $this->db->get_where('akun', array('kdakun' => '41103'))->row_array();
    $akun_diskon_js = $this->db->get_where('akun', array('kdakun' => '41260'))->row_array();
    $akun_diskon_jm = $this->db->get_where('akun', array('kdakun' => '41250'))->row_array();
    $akun_diskon_jp = $this->db->get_where('akun', array('kdakun' => '41270'))->row_array();
    $akun_diskon_bhp = $this->db->get_where('akun', array('kdakun' => '41280'))->row_array();
    
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $dafakun  = array();    
    $breakdown_jurnal = array();

    //get akun debit (cara bayar)
    $get_akun_bayar = $this->db->query("
      SELECT sum(jumlah) as jumlah, kdakun, 
        (SELECT akun.idakun FROM akun WHERE akun.kdakun = v_jurn_trans_rjugddet.kdakun) AS idakun, 
        (SELECT akun.nmakun FROM akun WHERE akun.kdakun = v_jurn_trans_rjugddet.kdakun) AS nmakun
      FROM v_jurn_trans_rjugddet
      WHERE nokuitansi = '".$nokuitansi."'
      GROUP BY kdakun")->result_array();
    if(!empty($get_akun_bayar)){
      foreach($get_akun_bayar as $akunbayar)
      {
        $breakdown_jurnal[] = array(
          'idakun' => $akunbayar['idakun'], 
          'kdakun' => $akunbayar['kdakun'], 
          'nmakun' => $akunbayar['nmakun'], 
          'noreff' => '', 
          'debit' => $akunbayar['jumlah'], 
          'kredit'=> 0
        );
      }
    }

    //get akun kredit obat (pendapatan obat / alkes)
    $get_pendapatan_obat = $this->db->query("
      SELECT sum(totalbhp) as jumlah, idakun, kdakun, nmakun
      FROM v_jurn_trans_rjugd_akunbrg
      WHERE nokuitansi = '".$nokuitansi."'
      GROUP BY kdakun")->result_array();
    if(!empty($get_pendapatan_obat)){
      foreach($get_pendapatan_obat as $pendapatan_obat)
      {
        $dafakun[$pendapatan_obat['kdakun'] .'_'] = array(
          'idakun' => $pendapatan_obat['idakun'], 
          'nmakun' => $pendapatan_obat['nmakun'], 
          'idreff' => '', 
          'noreff' => '', 
          'jumlah'=> $pendapatan_obat['jumlah']
        );
      }
    }

    //get akun kredit pelayanan
    $get_pendapatan_pelayanan = $this->db->get_where('v_jurn_trans_rjugd_akunpelayanan', array('nokuitansi' => $nokuitansi))->result_array();
    if(!empty($get_pendapatan_pelayanan)){
      foreach($get_pendapatan_pelayanan as $pendapatan_pelayanan)
      {
        //jasa sarana
        if($pendapatan_pelayanan['totaljs'] > 0){
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjs'] .'_']))
          {
            $dafakun[$pendapatan_pelayanan['kdakunjs'] .'_']['jumlah'] += $pendapatan_pelayanan['totaljs'];
          }else{
            $getakunjs = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjs']))->row_array();
            if(!empty($getakunjs)){
              $dafakun[$pendapatan_pelayanan['kdakunjs'] .'_'] = array(
                'jumlah' => $pendapatan_pelayanan['totaljs'],
                'idakun' => $getakunjs['idakun'],
                'nmakun' => $getakunjs['nmakun'],
                'idnoreff' => '',
                'noreff' => '',
              );
            }
          }
        }

        //jasa medis
        if($pendapatan_pelayanan['totaljm'] > 0){
          //jika akun jasa profesi sudah ada & id dokternya sama maka tambah nominal
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjm'] .'_'. $pendapatan_pelayanan['iddokter']]) AND 
             $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']]['idnoreff'] == $pendapatan_pelayanan['iddokter'])
          {
            $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']]['jumlah'] += $pendapatan_pelayanan['totaljm'];
          }else{
            $getakunjm = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjm']))->row_array();
            $getdokter = $this->db->get_where('dokter', array('iddokter' => $pendapatan_pelayanan['iddokter']))->row_array();
            if(!empty($getakunjm)){
              $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']] = array(
                'jumlah' => $pendapatan_pelayanan['totaljm'],
                'idakun' => $getakunjm['idakun'],
                'nmakun' => $getakunjm['nmakun'],
                'idnoreff' => $pendapatan_pelayanan['iddokter'],
                'noreff' => (isset($getdokter['kddokter'])) ? $getdokter['kddokter'] : '',
              );
            }
          }
        }

        //jasa profesi
        if($pendapatan_pelayanan['totaljp'] > 0){
          //jika akun jasa profesi sudah ada & id tenaga medis sama maka tambah nominal
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]) AND 
             $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]['idnoreff'] == $pendapatan_pelayanan['idperawat'])
          {
            $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]['jumlah'] += $pendapatan_pelayanan['totaljp'];
          }else{
            $getakunjp = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjp']))->row_array();
            $getperawat = $this->db->get_where('dokter', array('iddokter' => $pendapatan_pelayanan['idperawat']))->row_array();
            if(!empty($getakunjp)){
              $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']] = array(
                'jumlah' => $pendapatan_pelayanan['totaljp'],
                'idakun' => $getakunjp['idakun'],
                'nmakun' => $getakunjp['nmakun'],
                'idnoreff' => $pendapatan_pelayanan['idperawat'],
                'noreff' => (isset($getperawat['kddokter'])) ? $getperawat['kddokter'] : '',
              );
            }
          }
        }

        //BHP
        if($pendapatan_pelayanan['totalbhp'] > 0){
          if(isset($dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_']))
          {
            $dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_']['jumlah'] += $pendapatan_pelayanan['totalbhp'];
          }else{
            $getakunbhp = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunbhp']))->row_array();
            if(!empty($getakunbhp)){
              $dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_'] = array(
                'jumlah' => $pendapatan_pelayanan['totalbhp'],
                'idakun' => $getakunbhp['idakun'],
                'nmakun' => $getakunbhp['nmakun'],
                'idnoreff' => '',
                'noreff' => '',
              );
            }
          }
        }
      }
    }


    if(!empty($dafakun))
    {
      foreach($dafakun as $kdakun => $dtjurnal)
      {
        $kdakun_exp = explode('_', $kdakun);

        $breakdown_jurnal[] = array(
          'idakun' => $dtjurnal['idakun'], 
          'kdakun' => $kdakun_exp[0], 
          'nmakun' => $dtjurnal['nmakun'], 
          'noreff' => $dtjurnal['noreff'], 
          'debit'  => 0, 
          'kredit' => $dtjurnal['jumlah']
        );
      }
    }


    //get pendapatan jasa profesi farmasi (uang racik) & diskon barang dagangan
    $diskon_barang = 0;
    $q_nota = $this->db->query("
      SELECT 
      SUM(uangr) as tot_uangr,
      SUM(diskon) as diskon_obat from nota where nokuitansi = '".$nokuitansi."'
    ")->row_array();

    $diskon_barang += $q_nota['diskon_obat'];

    $q_notadet = $this->db->query("
      SELECT 
      sum(diskonjs) as diskonjs,
      sum(diskonjm) as diskonjm,
      sum(diskonjp) as diskonjp,
      sum(diskonbhp) as diskonbhp,
      sum(dijamin) as tot_dijamin
      from v_jurn_trans_rjugdnotadet
      WHERE nokuitansi = '".$nokuitansi."'
    ")->row_array();
    
    $diskon_barang += $q_notadet['diskonbhp'];
    $diskonjs = $q_notadet['diskonjs'];
    $diskonjm = $q_notadet['diskonjm'];
    $diskonjp = $q_notadet['diskonjp'];

    if($q_nota['tot_uangr'] > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_profesi['idakun'], 
        'kdakun' => $akun_profesi['kdakun'], 
        'nmakun' => $akun_profesi['nmakun'], 
        'noreff' => '0306', 
        'debit'  => 0, 
        'kredit' => $q_nota['tot_uangr']
      );
    }

    if($diskon_barang > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_bhp['idakun'], 
        'kdakun' => $akun_diskon_bhp['kdakun'], 
        'nmakun' => $akun_diskon_bhp['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskon_barang, 
        'kredit' => 0
      );
    }

    if($diskonjs > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_js['idakun'], 
        'kdakun' => $akun_diskon_js['kdakun'], 
        'nmakun' => $akun_diskon_js['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjs, 
        'kredit' => 0
      );
    }

    if($diskonjm > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_jm['idakun'], 
        'kdakun' => $akun_diskon_jm['kdakun'], 
        'nmakun' => $akun_diskon_jm['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjm, 
        'kredit' => 0
      );
    }

    if($diskonjp > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_jp['idakun'], 
        'kdakun' => $akun_diskon_jp['kdakun'], 
        'nmakun' => $akun_diskon_jp['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjp, 
        'kredit' => 0
      );
    }

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);
    echo json_encode($build_array);

  }

  function get_transaksi_pasien_rjugd_jurnaling_persediaan(){
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $dafakun  = array();    
    $breakdown_jurnal = array();

    $get_akun_persediaan = $this->db->query("
      SELECT 
        sum(hrgjual * qty) as penjualanbarang,
        sum(hrgbeli * qty) as nominalpersediaan, 
        idakun as idakunjual, 
        kdakun as kdakunjual, 
        nmakun as nmakunjual,
        idakunbeli as idakunbeli, 
        kdakunbeli as kdakunbeli, 
        nmakunbeli as nmakunbeli
      FROM v_jurn_trans_rjugd_akunbrg
      WHERE nokuitansi = '".$nokuitansi."'
      GROUP BY kdakun")->result_array();
    if(!empty($get_akun_persediaan)){
      foreach($get_akun_persediaan as $akun_persediaan)
      {
        //pendapatan barang
        $breakdown_jurnal[] = array(
          'idakun' => $akun_persediaan['idakunjual'], 
          'kdakun' => $akun_persediaan['kdakunjual'], 
          'nmakun' => $akun_persediaan['nmakunjual'], 
          'noreff' => '', 
          'debit' => $akun_persediaan['nominalpersediaan'], 
          'kredit'=> 0
        );

        //pengurangan stok
        $breakdown_jurnal[] = array(
          'idakun' => $akun_persediaan['idakunbeli'], 
          'kdakun' => $akun_persediaan['kdakunbeli'], 
          'nmakun' => $akun_persediaan['nmakunbeli'], 
          'noreff' => '', 
          'debit' => 0,
          'kredit'=> $akun_persediaan['nominalpersediaan']
        );
      }
    }

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);
    echo json_encode($build_array);

  }

  function kalkulasi_transaksi_rjugd()
  {
    $total_uangr = 0;
    $total_tagihan = 0;
    $diskon_pelayanan = 0;
    $diskon_obat = 0;
    $dijamin = 0;
    $total_bayar = 0;
    $nokuitansi = $this->input->post('nokuitansi');
    
    if(empty($nokuitansi)){
      $return = array(
        'total_uangr' => $total_uangr,
        'total_tagihan' => $total_tagihan,
        'diskon_pelayanan' => $diskon_pelayanan,
        'diskon_obat' => $diskon_obat,
        'dijamin' => $dijamin,
        'total_bayar' => $total_bayar
      );
      echo json_encode($return);
      die;
    }


    $q_nota = $this->db->query("
      SELECT 
      SUM(uangr) as tot_uangr,
      SUM(diskon) as diskon_obat from nota where nokuitansi = '".$nokuitansi."'
    ")->row_array();

    $total_uangr    += @$q_nota['tot_uangr'];
    $diskon_obat    += @$q_nota['diskon_obat'];

    $q_notadet = $this->db->query("
      SELECT 
      sum(qty * (tarifjs + tarifjm + tarifjp + tarifbhp)) as tot_tagihan,
      sum(diskonjs + diskonjm + diskonjp + diskonbhp) as tot_diskon,
      sum(dijamin) as tot_dijamin
      from v_jurn_trans_rjugdnotadet
      WHERE nokuitansi = '".$nokuitansi."'
    ")->row_array();

    $total_tagihan    += @$q_notadet['tot_tagihan'];
    $diskon_pelayanan += @$q_notadet['tot_diskon'];
    $dijamin          += @$q_notadet['tot_dijamin'];
    $total_bayar = ($total_uangr + $total_tagihan) - ($diskon_obat + $diskon_pelayanan + $dijamin); 

    $return = array(
      'total_uangr' => $total_uangr,
      'total_tagihan' => $total_tagihan,
      'diskon_pelayanan' => $diskon_pelayanan,
      'diskon_obat' => $diskon_obat,
      'dijamin' => $dijamin,
      'total_bayar' => $total_bayar
    );
    echo json_encode($return);
    die;

  }

  /* ==================== START JURNAL TRANSAKSI DEPOSIT RI ==================== */
  
  function posting_transaksi_pasien_deposit(){

    $nokuitansi  = $this->input->post("nokuitansi_posting");
    $tglkuitansi_posting  = $this->input->post("tglkuitansi_posting");
    $atasnama  = $this->input->post("atasnama");
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();
    $idjtransaksi = 4; //4 = jtransaksi jurnal deposit 
    $jtransaksi = 'Deposit Pasien RI';

    if(empty($nokuitansi) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nokuitansi dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglkuitansi_posting,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Transaksi '. $jtransaksi .': '.$atasnama,
      'noreff' => $nokuitansi,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => $idjtransaksi,
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //jurnal pendapatan
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  

  function get_transaksi_pasien_deposit(){
    $tglkuitansi         = $this->input->post("tglkuitansi");
    $tglkuitansi_akhir   = $this->input->post("tglkuitansi_akhir");
    $pencarian           = $this->input->post("pencarian");
    if(empty($tglkuitansi)) $tglkuitansi = date('Y-m-d');
    if(empty($tglkuitansi_akhir)) $tglkuitansi_akhir= date('Y-m-d');

    $where_like = '';
    if(!empty($pencarian)){
      $fields = array(
        'nokuitansi',     'tglkuitansi',      'jamkuitansi',
        'idshift',        'idstkuitansi',     'idsbtnm',
        'atasnama',       'idjnskuitansi',    'pembulatan',
        'total',          'ketina',           'keteng',
        'nokasir',        'tgljaminput',      'idregdet',
        'pembayaran',     'noreg',            'norm',
        'kdjurnal',       'status_posting',
      );

      $where_like = 'AND (';
      foreach($fields as $idx => $field){
        if($idx > 0){
          $where_like .= " OR {$field} LIKE '%{$pencarian}%' ";
        }else{
          $where_like .= "{$field} LIKE '%{$pencarian}%' ";  
        }
      }
      $where_like .=")";
    }

    $query = "SELECT * 
              FROM v_jurn_ri_deposit 
              WHERE idstkuitansi = 1 AND
              tglkuitansi >= '{$tglkuitansi}' AND 
              tglkuitansi <= '{$tglkuitansi_akhir}'
              {$where_like}
              ";

    $q = $this->db->query($query);  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_depositdet(){
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $this->db->where('nokuitansi', $nokuitansi);
    $this->db->select("*");
    $this->db->from("v_jurn_ri_depositdet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();

    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_deposit_jurnaling(){
    $akun_deposit = $this->db->get_where('akun', array('kdakun' => '21760'))->row_array();
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $totaldeposit = 0;
    //get akun debit (cara bayar)
    $get_akun_bayar = $this->db->query("
      SELECT jumlah, kdakun, ifnull(idbank, '') as idbank,
        (SELECT akun.idakun FROM akun WHERE akun.kdakun = v_jurn_ri_depositdet.kdakun) AS idakun, 
        (SELECT akun.nmakun FROM akun WHERE akun.kdakun = v_jurn_ri_depositdet.kdakun) AS nmakun
      FROM v_jurn_ri_depositdet
      WHERE nokuitansi = '".$nokuitansi."' ")->result_array();
    if(!empty($get_akun_bayar)){
      foreach($get_akun_bayar as $akunbayar)
      {
        $totaldeposit += $akunbayar['jumlah'];
        $breakdown_jurnal[] = array(
          'idakun' => $akunbayar['idakun'], 
          'kdakun' => $akunbayar['kdakun'], 
          'nmakun' => $akunbayar['nmakun'], 
          'noreff' => $akunbayar['idbank'], 
          'debit' => $akunbayar['jumlah'], 
          'kredit'=> 0
        );
      }
    }

    if($totaldeposit > 0)
    {
        $breakdown_jurnal[] = array(
          'idakun' => $akun_deposit['idakun'], 
          'kdakun' => $akun_deposit['kdakun'], 
          'nmakun' => $akun_deposit['nmakun'], 
          'noreff' => '', 
          'debit' => 0,
          'kredit'=> $totaldeposit
        );
    }


    $build_array = array ("success"=>true, "results"=> count($breakdown_jurnal), "data"=> $breakdown_jurnal);

    echo json_encode($build_array);
  }

  /* ==================== START JURNAL TRANSAKSI RETUR DEPOSIT RI ==================== */
  
  function posting_transaksi_pasien_returdeposit()
  {
    $kdjurnal  = $this->input->post("kdjurnal_posting");
    if(!empty($kdjurnal))
    {
      $this->db->where('kdjurnal', $kdjurnal);

      if($this->db->update('jurnal', array('status_posting' => 1)))
      {
        $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
        echo json_encode($return);
        die();
      }else{
        $return = array('success' => false, 'message' => 'gagal memposting jurnal');
        echo json_encode($return);
        die();
      }
      
    }

    $return = array('success' => false, 'message' => 'gagal memposting jurnal');
    echo json_encode($return);
    die();

  }

  function get_transaksi_pasien_returdeposit(){
    $tgltransaksi         = $this->input->post("tgltransaksi");
    $tgltransaksi_akhir   = $this->input->post("tgltransaksi_akhir");
    $pencarian            = $this->input->post("pencarian");
    if(empty($tgltransaksi)) $tgltransaksi = date('Y-m-d');
    if(empty($tgltransaksi_akhir)) $tgltransaksi_akhir= date('Y-m-d');

    $where = "
      tgltransaksi >= '".$tgltransaksi."' AND
      tgltransaksi <= '".$tgltransaksi_akhir."' AND
      idjnstransaksi = '16'
    ";

    if(!empty($pencarian)){
      $where .= " AND 
      (
        kdjurnal like '%".$pencarian."%' OR
        nojurnal like '%".$pencarian."%' OR
        idjnsjurnal like '%".$pencarian."%' OR
        idbagian like '%".$pencarian."%' OR
        nokasir like '%".$pencarian."%' OR
        tgltransaksi like '%".$pencarian."%' OR
        tgljurnal like '%".$pencarian."%' OR
        keterangan like '%".$pencarian."%' OR
        noreff like '%".$pencarian."%' OR
        userid like '%".$pencarian."%' OR
        tglinput like '%".$pencarian."%' OR
        nominal like '%".$pencarian."%' OR
        idjnstransaksi like '%".$pencarian."%' OR
        penerima like '%".$pencarian."%' OR
        status_posting like '%".$pencarian."%'
      )
      ";
    }
    $q = $this->db->query("SELECT * FROM jurnal WHERE ".$where);  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_returdeposit_jurnaling(){
    $kdjurnal     = $this->input->post("kdjurnal");
    if(empty($kdjurnal)) $kdjurnal = '0';

    $breakdown_jurnal = array();

    $get_jurnaldet = $this->db->query("
      SELECT 
        jurnaldet.kdjurnal,
        jurnaldet.tahun,
        jurnaldet.bulan,
        jurnaldet.idakun,
        jurnaldet.noreff,
        jurnaldet.debit,
        jurnaldet.kredit,
        jurnaldet.klpjurnal,
        akun.kdakun,
        akun.nmakun
      FROM (jurnaldet
      LEFT JOIN akun 
      ON (jurnaldet.idakun = akun.idakun)
      )
      WHERE jurnaldet.kdjurnal = '".$kdjurnal."' ")->result_array();

    if(!empty($get_jurnaldet)){
      foreach($get_jurnaldet as $akunjurnal)
      {
        $breakdown_jurnal[] = array(
          'idakun' => $akunjurnal['idakun'], 
          'kdakun' => $akunjurnal['kdakun'], 
          'nmakun' => $akunjurnal['nmakun'], 
          'noreff' => $akunjurnal['noreff'], 
          'debit' => $akunjurnal['debit'], 
          'kredit'=> $akunjurnal['kredit']
        );
      }
    }

    $build_array = array ("success"=>true, "results"=> count($breakdown_jurnal), "data"=> $breakdown_jurnal);

    echo json_encode($build_array);
  }


  /* ==================== START JURNAL TRANSAKSI RI ==================== */
  
  function posting_transaksi_pasien_ri(){

    $nokuitansi  = $this->input->post("nokuitansi_posting");
    $tglkuitansi_posting  = $this->input->post("tglkuitansi_posting");
    $atasnama  = $this->input->post("atasnama");
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $post_grid_persediaan  = $this->input->post("post_grid_persediaan");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();
    
    if(empty($nokuitansi) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nopo dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglkuitansi_posting,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Transaksi Pasien RI: '.$atasnama,
      'noreff' => $nokuitansi,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => 7,
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //jurnal pendapatan
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '1', // 1 = jurnal pendapatan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    //jurnal persediaan
    $jdet_rows = explode(';', $post_grid_persediaan);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '3', // 3 = jurnal persediaan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  

  function get_transaksi_pasien_ri(){
    $tglkuitansi     = $this->input->post("tglkuitansi");
    $tglkuitansi_akhir     = $this->input->post("tglkuitansi_akhir");
    if(empty($tglkuitansi)) $tglkuitansi = date('Y-m-d');
    if(empty($tglkuitansi_akhir)) $tglkuitansi_akhir= date('Y-m-d');

    $this->db->where('tglkuitansi >=', $tglkuitansi);
    $this->db->where('tglkuitansi <=', $tglkuitansi_akhir);
    
    $this->db->select("*");
    $this->db->from("v_jurn_trans_ri");
    $q = $this->db->get();  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_riddet(){
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $this->db->where('nokuitansi', $nokuitansi);
    $this->db->select("*");
    $this->db->from("v_jurn_trans_rjugddet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data =$q->result();
    
    //calculate deposit. get id regdet
    $get_regdet = $this->db->get_where('nota', array('nokuitansi' => $nokuitansi))->row_array();
    if(!empty($get_regdet) && !empty($get_regdet['idregdet'])){
      $idregdet = $get_regdet['idregdet'];

      //get deposit (if any)
      $get_deposit = $this->db->query("
                      SELECT ifnull(sum(pembayaran), 0) as total_deposit 
                      FROM kuitansi 
                      WHERE idjnskuitansi = 5 AND idregdet = '".$idregdet."'")->row_array();

      if(!empty($get_deposit) && $get_deposit['total_deposit'] > 0){
        $data[] = array(
          'idkuitansidet' => '',
          'nokuitansi' => '',
          'idbank' => '',
          'nmbank' => '-',
          'idcarabayar' => '',
          'nmcarabayar' => 'Deposit',
          'jumlah' => $get_deposit['total_deposit'],
          'nokartu' => '-',
          'kdakun' => '21760'
        );        
      }
    }

    $build_array = array ("success"=>true, "results"=>count($data), "data"=> $data);

    echo json_encode($build_array);
  }


  function get_transaksi_pasien_ri_jurnaling(){
    $akun_profesi = $this->db->get_where('akun', array('kdakun' => '41103'))->row_array();
    $akun_diskon_js = $this->db->get_where('akun', array('kdakun' => '41260'))->row_array();
    $akun_diskon_jm = $this->db->get_where('akun', array('kdakun' => '41250'))->row_array();
    $akun_diskon_jp = $this->db->get_where('akun', array('kdakun' => '41270'))->row_array();
    $akun_diskon_bhp = $this->db->get_where('akun', array('kdakun' => '41280'))->row_array();
    $akun_titipan_deposit = $this->db->get_where('akun', array('kdakun' => '21760'))->row_array();
    
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $dafakun  = array();    
    $breakdown_jurnal = array();

    //get akun debit (cara bayar)
    $get_akun_bayar = $this->db->query("
      SELECT sum(jumlah) as jumlah, kdakun, 
        (SELECT akun.idakun FROM akun WHERE akun.kdakun = v_jurn_trans_rjugddet.kdakun) AS idakun, 
        (SELECT akun.nmakun FROM akun WHERE akun.kdakun = v_jurn_trans_rjugddet.kdakun) AS nmakun
      FROM v_jurn_trans_rjugddet
      WHERE nokuitansi = '".$nokuitansi."'
      GROUP BY kdakun")->result_array();
    if(!empty($get_akun_bayar)){
      foreach($get_akun_bayar as $akunbayar)
      {
        $breakdown_jurnal[] = array(
          'idakun' => $akunbayar['idakun'], 
          'kdakun' => $akunbayar['kdakun'], 
          'nmakun' => $akunbayar['nmakun'], 
          'noreff' => '', 
          'debit' => $akunbayar['jumlah'], 
          'kredit'=> 0
        );
      }
    }

    //get akun deposit
    $get_regdet = $this->db->get_where('nota', array('nokuitansi' => $nokuitansi))->row_array();
    if(!empty($get_regdet) && !empty($get_regdet['idregdet'])){
      $idregdet = $get_regdet['idregdet'];

      //get deposit (if any)
      $get_deposit = $this->db->query("
                      SELECT ifnull(sum(pembayaran), 0) as total_deposit 
                      FROM kuitansi 
                      WHERE idjnskuitansi = 5 AND idregdet = '".$idregdet."'")->row_array();

      if(!empty($get_deposit) && $get_deposit['total_deposit'] > 0){
         
        $breakdown_jurnal[] = array(
          'idakun' => $akun_titipan_deposit['idakun'], 
          'kdakun' => $akun_titipan_deposit['kdakun'], 
          'nmakun' => $akun_titipan_deposit['nmakun'], 
          'noreff' => '', 
          'debit' => $get_deposit['total_deposit'], 
          'kredit'=> 0
        );
      }
    }

    //get akun kredit obat (pendapatan obat / alkes)
    $get_pendapatan_obat = $this->db->query("
      SELECT sum(totalbhp) as jumlah, idakun, kdakun, nmakun
      FROM v_jurn_trans_rjugd_akunbrg
      WHERE nokuitansi = '".$nokuitansi."'
      GROUP BY kdakun")->result_array();
    if(!empty($get_pendapatan_obat)){
      foreach($get_pendapatan_obat as $pendapatan_obat)
      {
        $dafakun[$pendapatan_obat['kdakun'] .'_'] = array(
          'idakun' => $pendapatan_obat['idakun'], 
          'nmakun' => $pendapatan_obat['nmakun'], 
          'idreff' => '', 
          'noreff' => '', 
          'jumlah'=> $pendapatan_obat['jumlah']
        );
      }
    }

    //get akun kredit pelayanan
    $get_pendapatan_pelayanan = $this->db->get_where('v_jurn_trans_rjugd_akunpelayanan', array('nokuitansi' => $nokuitansi))->result_array();
    if(!empty($get_pendapatan_pelayanan)){
      foreach($get_pendapatan_pelayanan as $pendapatan_pelayanan)
      {
        //jasa sarana
        if($pendapatan_pelayanan['totaljs'] > 0){
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjs'] .'_']))
          {
            $dafakun[$pendapatan_pelayanan['kdakunjs'] .'_']['jumlah'] += $pendapatan_pelayanan['totaljs'];
          }else{
            $getakunjs = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjs']))->row_array();
            if(!empty($getakunjs)){
              $dafakun[$pendapatan_pelayanan['kdakunjs'] .'_'] = array(
                'jumlah' => $pendapatan_pelayanan['totaljs'],
                'idakun' => $getakunjs['idakun'],
                'nmakun' => $getakunjs['nmakun'],
                'idnoreff' => '',
                'noreff' => '',
              );
            }
          }
        }

        //jasa medis
        if($pendapatan_pelayanan['totaljm'] > 0){
          //jika akun jasa profesi sudah ada & id dokternya sama maka tambah nominal
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjm'] .'_'. $pendapatan_pelayanan['iddokter']]) AND 
             $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']]['idnoreff'] == $pendapatan_pelayanan['iddokter'])
          {
            $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']]['jumlah'] += $pendapatan_pelayanan['totaljm'];
          }else{
            $getakunjm = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjm']))->row_array();
            $getdokter = $this->db->get_where('dokter', array('iddokter' => $pendapatan_pelayanan['iddokter']))->row_array();
            if(!empty($getakunjm)){
              $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']] = array(
                'jumlah' => $pendapatan_pelayanan['totaljm'],
                'idakun' => $getakunjm['idakun'],
                'nmakun' => $getakunjm['nmakun'],
                'idnoreff' => $pendapatan_pelayanan['iddokter'],
                'noreff' => (isset($getdokter['kddokter'])) ? $getdokter['kddokter'] : '',
              );
            }
          }
        }

        //jasa profesi
        if($pendapatan_pelayanan['totaljp'] > 0){
          //jika akun jasa profesi sudah ada & id tenaga medis sama maka tambah nominal
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]) AND 
             $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]['idnoreff'] == $pendapatan_pelayanan['idperawat'])
          {
            $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]['jumlah'] += $pendapatan_pelayanan['totaljp'];
          }else{
            $getakunjp = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjp']))->row_array();
            $getperawat = $this->db->get_where('dokter', array('iddokter' => $pendapatan_pelayanan['idperawat']))->row_array();
            if(!empty($getakunjp)){
              $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']] = array(
                'jumlah' => $pendapatan_pelayanan['totaljp'],
                'idakun' => $getakunjp['idakun'],
                'nmakun' => $getakunjp['nmakun'],
                'idnoreff' => $pendapatan_pelayanan['idperawat'],
                'noreff' => (isset($getperawat['kddokter'])) ? $getperawat['kddokter'] : '',
              );
            }
          }
        }

        //BHP
        if($pendapatan_pelayanan['totalbhp'] > 0){
          if(isset($dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_']))
          {
            $dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_']['jumlah'] += $pendapatan_pelayanan['totalbhp'];
          }else{
            $getakunbhp = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunbhp']))->row_array();
            if(!empty($getakunbhp)){
              $dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_'] = array(
                'jumlah' => $pendapatan_pelayanan['totalbhp'],
                'idakun' => $getakunbhp['idakun'],
                'nmakun' => $getakunbhp['nmakun'],
                'idnoreff' => '',
                'noreff' => '',
              );
            }
          }
        }
      }
    }


    if(!empty($dafakun))
    {
      foreach($dafakun as $kdakun => $dtjurnal)
      {
        $kdakun_exp = explode('_', $kdakun);

        $breakdown_jurnal[] = array(
          'idakun' => $dtjurnal['idakun'], 
          'kdakun' => $kdakun_exp[0], 
          'nmakun' => $dtjurnal['nmakun'], 
          'noreff' => $dtjurnal['noreff'], 
          'debit'  => 0, 
          'kredit' => $dtjurnal['jumlah']
        );
      }
    }


    //get pendapatan jasa profesi farmasi (uang racik) & diskon barang dagangan
    $diskon_barang = 0;
    $q_nota = $this->db->query("
      SELECT 
      SUM(uangr) as tot_uangr,
      SUM(diskon) as diskon_obat from nota where nokuitansi = '".$nokuitansi."'
    ")->row_array();

    $diskon_barang += $q_nota['diskon_obat'];

    $q_notadet = $this->db->query("
      SELECT 
      sum(diskonjs) as diskonjs,
      sum(diskonjm) as diskonjm,
      sum(diskonjp) as diskonjp,
      sum(diskonbhp) as diskonbhp,
      sum(dijamin) as tot_dijamin
      from v_jurn_trans_rjugdnotadet
      WHERE nokuitansi = '".$nokuitansi."'
    ")->row_array();
    
    $diskon_barang += $q_notadet['diskonbhp'];
    $diskonjs = $q_notadet['diskonjs'];
    $diskonjm = $q_notadet['diskonjm'];
    $diskonjp = $q_notadet['diskonjp'];

    if($q_nota['tot_uangr'] > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_profesi['idakun'], 
        'kdakun' => $akun_profesi['kdakun'], 
        'nmakun' => $akun_profesi['nmakun'], 
        'noreff' => '0306', 
        'debit'  => 0, 
        'kredit' => $q_nota['tot_uangr']
      );
    }

    if($diskon_barang > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_bhp['idakun'], 
        'kdakun' => $akun_diskon_bhp['kdakun'], 
        'nmakun' => $akun_diskon_bhp['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskon_barang, 
        'kredit' => 0
      );
    }

    if($diskonjs > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_js['idakun'], 
        'kdakun' => $akun_diskon_js['kdakun'], 
        'nmakun' => $akun_diskon_js['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjs, 
        'kredit' => 0
      );
    }

    if($diskonjm > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_jm['idakun'], 
        'kdakun' => $akun_diskon_jm['kdakun'], 
        'nmakun' => $akun_diskon_jm['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjm, 
        'kredit' => 0
      );
    }

    if($diskonjp > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_jp['idakun'], 
        'kdakun' => $akun_diskon_jp['kdakun'], 
        'nmakun' => $akun_diskon_jp['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjp, 
        'kredit' => 0
      );
    }

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);
    echo json_encode($build_array);

  }


  /* ==================== START JURNAL TRANSAKSI FARMASI PASIEN LUAR ==================== */
  
  function posting_transaksi_farmasi_pl(){

    $nokuitansi  = $this->input->post("nokuitansi_posting");
    $tglkuitansi_posting  = $this->input->post("tglkuitansi_posting");
    $atasnama  = $this->input->post("atasnama");
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $post_grid_persediaan  = $this->input->post("post_grid_persediaan");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();
    
    if(empty($nokuitansi) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nopo dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglkuitansi_posting,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Farmasi Pasien Luar: '.$atasnama,
      'noreff' => $nokuitansi,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => 9, //9 = transaksi farmasi pasien luar
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //jurnal pendapatan
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '1', // 1 = jurnal pendapatan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    //jurnal persediaan
    $jdet_rows = explode(';', $post_grid_persediaan);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '3', // 3 = jurnal persediaan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  

  function get_transaksi_farmasi_pl(){
    $tglkuitansi     = $this->input->post("tglkuitansi");
    $tglkuitansi_akhir     = $this->input->post("tglkuitansi_akhir");
    if(empty($tglkuitansi)) $tglkuitansi = date('Y-m-d');
    if(empty($tglkuitansi_akhir)) $tglkuitansi_akhir= date('Y-m-d');

    $this->db->where('tglkuitansi >=', $tglkuitansi);
    $this->db->where('tglkuitansi <=', $tglkuitansi_akhir);
    
    $this->db->select("*");
    $this->db->from("v_jurn_trans_farmasi_pl");
    $q = $this->db->get();  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }


  /* =============== START JURNAL TRANSAKSI PASIEN (RJ, UGD, RI, P.TAMBAHAN, F. PASIEN LUAR) =============== */
  
  function posting_transaksi_pasien()
  {

    $nokuitansi  = $this->input->post("nokuitansi_posting");
    $tglkuitansi_posting  = $this->input->post("tglkuitansi_posting");
    $jtransaksi_posting  = $this->input->post("jtransaksi_posting");
    $idjtransaksi = $this->input->post("jtransjurnal_posting");
    $atasnama  = $this->input->post("atasnama");
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $post_grid_persediaan  = $this->input->post("post_grid_persediaan");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();
    
    if(empty($nokuitansi) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nopo dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglkuitansi_posting,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Transaksi '. $jtransaksi_posting .': '.$atasnama,
      'noreff' => $nokuitansi,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => $idjtransaksi,
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //jurnal pendapatan
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '1', // 1 = jurnal pendapatan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    //jurnal persediaan
    $jdet_rows = explode(';', $post_grid_persediaan);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        /*
          item_jdet[0] = idakun   
          item_jdet[1] = kdakun 
          item_jdet[2] = nmakun 
          item_jdet[3] = noreff 
          item_jdet[4] = debit 
          item_jdet[5] = kredit
        */
        if(isset($item_jdet['3'])){
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
            'klpjurnal' => '3', // 3 = jurnal persediaan
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
        }

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  

  function get_transaksi_pasien(){
    $tglkuitansi        = $this->input->post("tglkuitansi");
    $tglkuitansi_akhir  = $this->input->post("tglkuitansi_akhir");
    $pencarian          = $this->input->post("pencarian");
    $status_posting     = $this->input->post("status_posting");
    $jenis_transaksi    = $this->input->post("jenis_transaksi");
    
    if(empty($tglkuitansi)) $tglkuitansi = date('Y-m-d');
    if(empty($tglkuitansi_akhir)) $tglkuitansi_akhir= date('Y-m-d');

    //FUNGSI WHERE
    $where_pencarian = '';
    if(!empty($pencarian)){
      $where_pencarian .= " AND (";

      $fields = array(
        'kuitansi.nokuitansi',
        'tglkuitansi',
        'idstkuitansi',
        'kuitansi.atasnama',
        'idjnskuitansi',
        'total',
        'kuitansi.idregdet',
        'pembayaran',
        //'kdjurnal',
        //'status_posting',
        'nonota',
        //'noreg',
        //'jenis_transaksi',
        //'jtransjurnal',
      );

      foreach($fields as $idx => $field){
        if($idx > 0)
            $where_pencarian .= " OR ";

        $where_pencarian .= $field." like '%".$pencarian."%'";

      }

      $where_pencarian .= ")";
    }

    //FUNGSI HAVING
    $having = '';
    if(!empty($status_posting) && !empty($jenis_transaksi)){
      $having .= "
        HAVING 
          status_posting like '%".$status_posting."%' AND
          jenis_transaksi like '%".$jenis_transaksi."%'
      ";
    }elseif(!empty($status_posting)){
      $having .= "
        HAVING 
          status_posting like '%".$status_posting."%'
      ";
    }elseif(!empty($jenis_transaksi)){
      $having .= "
        HAVING 
          jenis_transaksi like '%".$jenis_transaksi."%'
      ";
    }

    $query = "
      SELECT * FROM v_jurn_trans_pasien
      WHERE 
        tglkuitansi >= '".$tglkuitansi."' AND
        tglkuitansi <= '".$tglkuitansi_akhir."'
        {$where_pencarian}
    ";

    $main_query = "
      SELECT `kuitansi`.`nokuitansi` AS `nokuitansi`
       , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
       , `kuitansi`.`idstkuitansi` AS `idstkuitansi`
       , `kuitansi`.`atasnama` AS `atasnama`
       , `kuitansi`.`idjnskuitansi` AS `idjnskuitansi`
       , `kuitansi`.`total` AS `total`
       , `kuitansi`.`idregdet` AS `idregdet`
       , `kuitansi`.`pembayaran` AS `pembayaran`
       , ifnull((SELECT `jurnal`.`kdjurnal` AS `kdjurnal`
                 FROM
                   `jurnal`
                 WHERE
                   ((`kuitansi`.`nokuitansi` = `jurnal`.`noreff`)
                   AND (`jurnal`.`idjnsjurnal` = 2)
                   AND ((`jurnal`.`idjnstransaksi` = 6)
                   OR (`jurnal`.`idjnstransaksi` = 7)
                   OR (`jurnal`.`idjnstransaksi` = 8)
                   OR (`jurnal`.`idjnstransaksi` = 10)
                   OR (`jurnal`.`idjnstransaksi` = 11)))), '-') AS `kdjurnal`
       , if(isnull((SELECT `jurnal`.`kdjurnal` AS `kdjurnal`
                    FROM
                      `jurnal`
                    WHERE
                      ((`kuitansi`.`nokuitansi` = `jurnal`.`noreff`)
                      AND (`jurnal`.`idjnsjurnal` = 2)
                      AND ((`jurnal`.`idjnstransaksi` = 6)
                      OR (`jurnal`.`idjnstransaksi` = 7)
                      OR (`jurnal`.`idjnstransaksi` = 8)
                      OR (`jurnal`.`idjnstransaksi` = 10)
                      OR (`jurnal`.`idjnstransaksi` = 11))))), 'belum', 'sudah') AS `status_posting`
       , if(isnull((SELECT `jurnal`.`kdjurnal` AS `kdjurnal`
                    FROM
                      `jurnal`
                    WHERE
                      ((`kuitansi`.`nokuitansi` = `jurnal`.`noreff`)
                      AND (`jurnal`.`idjnsjurnal` = 2)
                      AND ((`jurnal`.`idjnstransaksi` = 6)
                      OR (`jurnal`.`idjnstransaksi` = 7)
                      OR (`jurnal`.`idjnstransaksi` = 8)
                      OR (`jurnal`.`idjnstransaksi` = 10)
                      OR (`jurnal`.`idjnstransaksi` = 11))))), false, true) AS `status_posting2`
       , `nota`.`nonota` AS `nonota`
       , (SELECT `registrasidet`.`noreg` AS `noreg`
          FROM
            `registrasidet`
          WHERE
            (`registrasidet`.`idregdet` = `nota`.`idregdet`)) AS `noreg`
       , if((`kuitansi`.`idjnskuitansi` = 2), 'Rawat Inap', if((`kuitansi`.`idjnskuitansi` = 4), 'UGD', if(((`kuitansi`.`idjnskuitansi` = 1) AND (`nota`.`idbagian` = 11)), 'Farmasi Pasien Luar', if(((`kuitansi`.`idjnskuitansi` = 1) AND (`nota`.`idbagian` = 35)), 'Pelayanan Tambahan', 'Rawat Jalan')))) AS `jenis_transaksi`
       , if((`kuitansi`.`idjnskuitansi` = 2), '7', if((`kuitansi`.`idjnskuitansi` = 4), '8', if(((`kuitansi`.`idjnskuitansi` = 1) AND (`nota`.`idbagian` = 11)), '10', if(((`kuitansi`.`idjnskuitansi` = 1) AND (`nota`.`idbagian` = 35)), '11', '6')))) AS `jtransjurnal`
      FROM
      (`kuitansi`
      LEFT JOIN `nota`
      ON ((`kuitansi`.`nokuitansi` = `nota`.`nokuitansi`)))
      WHERE
        `kuitansi`.`idstkuitansi` = 1 AND 
        (
          `kuitansi`.`idjnskuitansi` = 1 OR 
          `kuitansi`.`idjnskuitansi` = 2 OR 
          `kuitansi`.`idjnskuitansi` = 4
        ) AND
        tglkuitansi >= '".$tglkuitansi."' AND
        tglkuitansi <= '".$tglkuitansi_akhir."'
        {$where_pencarian}
      GROUP BY
        `kuitansi`.`nokuitansi`
        {$having}
      ";


    $q = $this->db->query($main_query);  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_det_bu(){
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $this->db->where('nokuitansi', $nokuitansi);
    $this->db->select("*");
    $this->db->from("v_jurn_trans_rjugddet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    //check apakah ada deposit yang memiliki regdet yang sama
    $get_nota = $this->db->get_where('nota', array('nokuitansi' => $nokuitansi))->row_array();
    if(!empty($get_nota))
    {
      //cari kuitansi yang memiliki jenis kuitansi deposit & idregdet = nota.idregdet
      $get_kuitansi = $this->db->get_where('kuitansi', array('idjnskuitansi' => 5, 'idregdet' => $get_nota['idregdet']))->result_array();
      $total_deposit = 0;
      if(!empty($get_kuitansi)){
        foreach($get_kuitansi as $k_deposit){
          $total_deposit += $k_deposit['pembayaran'];
        }
      }

      if($total_deposit > 0)
      {
        $q_totaltagihan = $this->db->query("
          SELECT 
          sum(qty * (tarifjs + tarifjm + tarifjp + tarifbhp)) as tot_tagihan
          from v_jurn_trans_rjugdnotadet
          WHERE nokuitansi = '".$nokuitansi."'
        ")->row_array();

        //jika total deposit > total tagihan. deposit diperkecil sampai pas dengan tagihan
        if($total_deposit > $q_totaltagihan['tot_tagihan']){
          $total_deposit = $q_totaltagihan['tot_tagihan'];
        }

        $obj = new stdClass();
        $obj->idkuitansidet  = '';
        $obj->nokuitansi     = $k_deposit['nokuitansi'];
        $obj->idbank         = '-';
        $obj->nmbank         = '-';
        $obj->idcarabayar    = '-';
        $obj->nmcarabayar    = 'Deposit';
        $obj->jumlah         = $total_deposit;
        $obj->nokartu        = '';
        $obj->kdakun         = '21760';
        $data[] = $obj;
      }

    }
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_det(){
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $this->db->where('nokuitansi', $nokuitansi);
    $this->db->select("*");
    $this->db->from("v_jurn_trans_rjugddet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    //check apakah ada deposit yang memiliki regdet yang sama
    $get_nota = $this->db->get_where('nota', array('nokuitansi' => $nokuitansi))->row_array();
    if(!empty($get_nota))
    {
      $total_deposit = 0;

      //cari nomor registrasi dari nota
      $get_registrasi = $this->db->get_where('registrasidet', array('idregdet' => $get_nota['idregdet']))->row_array();
      if(!empty($get_registrasi)){
        $noreg = $get_registrasi['noreg'];

        //hitung total kuitansi yang memiliki jenis kuitansi deposit & noreg = $noreg
        $get_total_deposit = $this->db->query("
          SELECT sum(kuitansi.total) AS deposit
          FROM
            (kuitansi
          JOIN registrasidet)
          WHERE
            kuitansi.idregdet = registrasidet.idregdet
            AND kuitansi.idjnskuitansi = 5
            AND kuitansi.idstkuitansi = 1
            AND registrasidet.noreg = '".$noreg."'")->row_array();
        $total_deposit = $get_total_deposit['deposit'];

      }


      if($total_deposit > 0)
      {
        $q_totaltagihan = $this->db->query("
        SELECT sum(qty * (tarifjs + tarifjm + tarifjp + tarifbhp)) as tot_tagihan
        FROM 
        (
          (`nota` JOIN `notadet`) 
          LEFT JOIN `registrasidet`
          ON ((`nota`.`idregdet` = `registrasidet`.`idregdet`))
        )
        WHERE
        (
          (`nota`.`nokuitansi` = '".$nokuitansi."') AND 
          (`nota`.`idsttransaksi` = 1) AND 
          (`nota`.`nonota` = `notadet`.`nonota`)
        )
        ")->row_array();

        //jika total deposit > total tagihan. deposit diperkecil sampai pas dengan tagihan
        if($total_deposit > $q_totaltagihan['tot_tagihan']){
          $total_deposit = $q_totaltagihan['tot_tagihan'];
        }

        $obj = new stdClass();
        $obj->idkuitansidet  = '';
        $obj->nokuitansi     = $nokuitansi;
        $obj->idbank         = '-';
        $obj->nmbank         = '-';
        $obj->idcarabayar    = '-';
        $obj->nmcarabayar    = 'Deposit';
        $obj->jumlah         = $total_deposit;
        $obj->nokartu        = '';
        $obj->kdakun         = '21760';
        $data[] = $obj;
      }

    }
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_pasien_jurnaling(){
    $akun_deposit = $this->db->get_where('akun', array('kdakun' => '21760'))->row_array();
    $akun_profesi = $this->db->get_where('akun', array('kdakun' => '41103'))->row_array();
    $akun_diskon_js = $this->db->get_where('akun', array('kdakun' => '41260'))->row_array();
    $akun_diskon_jm = $this->db->get_where('akun', array('kdakun' => '41250'))->row_array();
    $akun_diskon_jp = $this->db->get_where('akun', array('kdakun' => '41270'))->row_array();
    $akun_diskon_bhp = $this->db->get_where('akun', array('kdakun' => '41280'))->row_array();
    
    $nokuitansi     = $this->input->post("nokuitansi");
    if(empty($nokuitansi)) $nokuitansi = '0';

    $dafakun  = array();    
    $breakdown_jurnal = array();

    $get_akun_bayar = $this->db->query("SELECT 
      ifnull(`kuitansidet`.`idbank`, '') AS `idbank`
      , sum(`kuitansidet`.`jumlah`) AS `jumlah`
      , (SELECT if(((`carabayar`.`kdakun` IS NOT NULL) AND (`carabayar`.`kdakun` <> '')), `carabayar`.`kdakun`, 
          if((`carabayar`.`idcarabayar` = '2'), ifnull((SELECT `bank`.`kdakun` AS `kdakun` FROM `bank` 
          WHERE (`bank`.`idbank` = `kuitansidet`.`idbank`)), '11000'), '11000')) AS `select1`) AS `kdakun`
      FROM
      (`kuitansidet` LEFT JOIN `carabayar`
      ON (`kuitansidet`.`idcarabayar` = `carabayar`.`idcarabayar`))
      WHERE nokuitansi = '".$nokuitansi."'
      GROUP BY kdakun,idbank
    ")->result_array();

    if(!empty($get_akun_bayar)){
      foreach($get_akun_bayar as $akunbayar)
      {
        $gt_akun = $this->db->get_where('akun', array('kdakun' => $akunbayar['kdakun']))->row_array();
        $breakdown_jurnal[] = array(
          'idakun' => $gt_akun['idakun'], 
          'kdakun' => $gt_akun['kdakun'], 
          'nmakun' => $gt_akun['nmakun'], 
          'noreff' => $akunbayar['idbank'], 
          'debit' => $akunbayar['jumlah'], 
          'kredit'=> 0
        );
      }
    }

    //check apakah ada deposit yang memiliki regdet yang sama
    $get_nota = $this->db->get_where('nota', array('nokuitansi' => $nokuitansi))->row_array();
    if(!empty($get_nota))
    {
      $total_deposit = 0;

      //cari nomor registrasi dari nota
      $get_registrasi = $this->db->get_where('registrasidet', array('idregdet' => $get_nota['idregdet']))->row_array();
      if(!empty($get_registrasi)){
        $noreg = $get_registrasi['noreg'];

        //hitung total kuitansi yang memiliki jenis kuitansi deposit & noreg = $noreg
        $get_total_deposit = $this->db->query("
          SELECT sum(kuitansi.total) AS deposit
          FROM
            (kuitansi
          JOIN registrasidet)
          WHERE
            kuitansi.idregdet = registrasidet.idregdet
            AND kuitansi.idjnskuitansi = 5
            AND kuitansi.idstkuitansi = 1
            AND registrasidet.noreg = '".$noreg."'")->row_array();
        $total_deposit = $get_total_deposit['deposit'];

      }

      //cari kuitansi yang memiliki jenis kuitansi deposit & idregdet = nota.idregdet
      /*
      $get_kuitansi = $this->db->get_where('kuitansi', array('idjnskuitansi' => 5, 'idregdet' => $get_nota['idregdet']))->result_array();
      $total_deposit = 0;
      if(!empty($get_kuitansi)){
        foreach($get_kuitansi as $k_deposit){
          $total_deposit += $k_deposit['pembayaran'];
        }
      }
      */

      if($total_deposit > 0)
      {
        $q_totaltagihan = $this->db->query("
          SELECT sum(
            (qty * (tarifjs + tarifjm + tarifjp + tarifbhp) ) - 
            (`notadet`.`diskonjs` + `notadet`.`diskonjm` + `notadet`.`diskonjp` + `notadet`.`diskonbhp`)
          )  AS tot_tagihan
          FROM (notadet left join nota on (notadet.nonota = nota.nonota))
          WHERE nota.nokuitansi = '".$nokuitansi."'
        ")->row_array();

        //jika total deposit > total tagihan. deposit diperkecil sampai pas dengan tagihan
        if($total_deposit > $q_totaltagihan['tot_tagihan']){
          $total_deposit = $q_totaltagihan['tot_tagihan'];
        }
        
        $breakdown_jurnal[] = array(
          'idakun' => $akun_deposit['idakun'], 
          'kdakun' => $akun_deposit['kdakun'], 
          'nmakun' => $akun_deposit['nmakun'], 
          'noreff' => '', 
          'debit' => $total_deposit, 
          'kredit'=> 0
        );
      }

    }

    //get akun kredit obat (pendapatan obat / alkes)
    $get_pendapatan_obat = $this->db->query("SELECT 
      sum(`notadet`.`tarifbhp` * `notadet`.`qty`) AS `jumlah`
      , `setakunklpbarang`.`idakunjual` AS `idakun`
      , `akun`.`kdakun` AS `kdakun`
      , `akun`.`nmakun` AS `nmakun`
      FROM
      (((((`notadet`
      LEFT JOIN `nota` ON ((`notadet`.`nonota` = `nota`.`nonota`)))
      LEFT JOIN `barang` ON ((`barang`.`kdbrg` = `notadet`.`kditem`)))
      LEFT JOIN `setakunklpbarang` ON (((`setakunklpbarang`.`idklpbrg` = `barang`.`idklpbrg`) AND (`setakunklpbarang`.`tahun` = year(`nota`.`tglnota`)))))
      LEFT JOIN `akun` ON ((`setakunklpbarang`.`idakunjual` = `akun`.`idakun`)))
      LEFT JOIN `akun` `akunbeli` ON ((`setakunklpbarang`.`idakun` = `akunbeli`.`idakun`)))
      WHERE
      `nota`.`idsttransaksi` = 1 AND 
      (left(`notadet`.`kditem`, 1) = 'B') AND 
      nokuitansi = '".$nokuitansi."' GROUP BY kdakun
    ")->result_array();

    if(!empty($get_pendapatan_obat)){
      foreach($get_pendapatan_obat as $pendapatan_obat)
      {
        $dafakun[$pendapatan_obat['kdakun'] .'_'] = array(
          'idakun' => $pendapatan_obat['idakun'], 
          'nmakun' => $pendapatan_obat['nmakun'], 
          'idreff' => '', 
          'noreff' => '', 
          'jumlah'=> $pendapatan_obat['jumlah']
        );
      }
    }


    //get akun kredit pelayanan
    $get_pendapatan_pelayanan = $this->db->query("SELECT 
      `notadet`.`idnotadet` AS `idnotadet`
       , `nota`.`nokuitansi` AS `nokuitansi`
       , `notadet`.`nonota` AS `nonota`
       , `notadet`.`kditem` AS `kditem`
       , `notadet`.`qty` AS `qty`
       , `notadet`.`tarifjs` AS `tarifjs`
       , `notadet`.`tarifjm` AS `tarifjm`
       , `notadet`.`tarifjp` AS `tarifjp`
       , `notadet`.`tarifbhp` AS `tarifbhp`
       , (`notadet`.`tarifjs` * `notadet`.`qty`) AS `totaljs`
       , (`notadet`.`tarifjm` * `notadet`.`qty`) AS `totaljm`
       , (`notadet`.`tarifjp` * `notadet`.`qty`) AS `totaljp`
       , (`notadet`.`tarifbhp` * `notadet`.`qty`) AS `totalbhp`
       , `notadet`.`diskonjs` AS `diskonjs`
       , `notadet`.`diskonjm` AS `diskonjm`
       , `notadet`.`diskonjp` AS `diskonjp`
       , `notadet`.`diskonbhp` AS `diskonbhp`
       , `notadet`.`hrgjual` AS `hrgjual`
       , `notadet`.`hrgbeli` AS `hrgbeli`
       , `notadet`.`iddokter` AS `iddokter`
       , `notadet`.`idperawat` AS `idperawat`
       , `notadet`.`dijamin` AS `dijamin`
       , `setakunpelayanan`.`kdakunjs` AS `kdakunjs`
       , `setakunpelayanan`.`kdakunjm` AS `kdakunjm`
       , `setakunpelayanan`.`kdakunjp` AS `kdakunjp`
       , `setakunpelayanan`.`kdakunbhp` AS `kdakunbhp`
      FROM
        ((`notadet`
      LEFT JOIN `nota`
      ON ((`notadet`.`nonota` = `nota`.`nonota`)))
      LEFT JOIN `setakunpelayanan`
      ON (((`setakunpelayanan`.`kdpelayanan` = `notadet`.`kditem`) AND (`setakunpelayanan`.`tahun` = year(`nota`.`tglnota`)))))
      WHERE
        `nota`.`idsttransaksi` = 1 AND 
        (left(`notadet`.`kditem`, 1) = 'T') AND
        nokuitansi = '".$nokuitansi."'
    ")->result_array();
    
    if(!empty($get_pendapatan_pelayanan)){
      foreach($get_pendapatan_pelayanan as $pendapatan_pelayanan)
      {
        //jasa sarana
        if($pendapatan_pelayanan['totaljs'] > 0){
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjs'] .'_']))
          {
            $dafakun[$pendapatan_pelayanan['kdakunjs'] .'_']['jumlah'] += $pendapatan_pelayanan['totaljs'];
          }else{
            $getakunjs = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjs']))->row_array();
            if(!empty($getakunjs)){
              $dafakun[$pendapatan_pelayanan['kdakunjs'] .'_'] = array(
                'jumlah' => $pendapatan_pelayanan['totaljs'],
                'idakun' => $getakunjs['idakun'],
                'nmakun' => $getakunjs['nmakun'],
                'idnoreff' => '',
                'noreff' => '',
              );
            }
          }
        }

        //jasa medis
        if($pendapatan_pelayanan['totaljm'] > 0){
          //jika akun jasa profesi sudah ada & id dokternya sama maka tambah nominal
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjm'] .'_'. $pendapatan_pelayanan['iddokter']]) AND 
             $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']]['idnoreff'] == $pendapatan_pelayanan['iddokter'])
          {
            $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']]['jumlah'] += $pendapatan_pelayanan['totaljm'];
          }else{
            $getakunjm = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjm']))->row_array();
            $getdokter = $this->db->get_where('dokter', array('iddokter' => $pendapatan_pelayanan['iddokter']))->row_array();
            if(!empty($getakunjm)){
              $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']] = array(
                'jumlah' => $pendapatan_pelayanan['totaljm'],
                'idakun' => $getakunjm['idakun'],
                'nmakun' => $getakunjm['nmakun'],
                'idnoreff' => $pendapatan_pelayanan['iddokter'],
                'noreff' => (isset($getdokter['kddokter'])) ? $getdokter['kddokter'] : '',
              );
            }
          }
        }

        //jasa profesi
        if($pendapatan_pelayanan['totaljp'] > 0){
          //jika akun jasa profesi sudah ada & id tenaga medis sama maka tambah nominal
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]) AND 
             $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]['idnoreff'] == $pendapatan_pelayanan['idperawat'])
          {
            $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]['jumlah'] += $pendapatan_pelayanan['totaljp'];
          }else{
            $getakunjp = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjp']))->row_array();
            $getperawat = $this->db->get_where('dokter', array('iddokter' => $pendapatan_pelayanan['idperawat']))->row_array();
            if(!empty($getakunjp)){
              $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']] = array(
                'jumlah' => $pendapatan_pelayanan['totaljp'],
                'idakun' => $getakunjp['idakun'],
                'nmakun' => $getakunjp['nmakun'],
                'idnoreff' => $pendapatan_pelayanan['idperawat'],
                'noreff' => (isset($getperawat['kddokter'])) ? $getperawat['kddokter'] : '',
              );
            }
          }
        }

        //BHP
        if($pendapatan_pelayanan['totalbhp'] > 0){
          if(isset($dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_']))
          {
            $dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_']['jumlah'] += $pendapatan_pelayanan['totalbhp'];
          }else{
            $getakunbhp = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunbhp']))->row_array();
            if(!empty($getakunbhp)){
              $dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_'] = array(
                'jumlah' => $pendapatan_pelayanan['totalbhp'],
                'idakun' => $getakunbhp['idakun'],
                'nmakun' => $getakunbhp['nmakun'],
                'idnoreff' => '',
                'noreff' => '',
              );
            }
          }
        }
      }
    }

    if(!empty($dafakun))
    {
      foreach($dafakun as $kdakun => $dtjurnal)
      {
        $kdakun_exp = explode('_', $kdakun);

        $breakdown_jurnal[] = array(
          'idakun' => $dtjurnal['idakun'], 
          'kdakun' => $kdakun_exp[0], 
          'nmakun' => $dtjurnal['nmakun'], 
          'noreff' => $dtjurnal['noreff'], 
          'debit'  => 0, 
          'kredit' => $dtjurnal['jumlah']
        );
      }
    }

    //get pendapatan jasa profesi farmasi (uang racik) & diskon barang dagangan
    $diskon_barang = 0;
    $q_nota = $this->db->query("
      SELECT 
      SUM(uangr) as tot_uangr,
      diskon as diskon_obat from nota where nokuitansi = '".$nokuitansi."'
    ")->row_array();

    $diskon_barang += $q_nota['diskon_obat'];

    $q_notadet = $this->db->query("
      SELECT 
        sum(`notadet`.`diskonjs`) AS `diskonjs`
        , sum(`notadet`.`diskonjm`) AS `diskonjm`
        , sum(`notadet`.`diskonjp`) AS `diskonjp`
        , sum(`notadet`.`diskonbhp`) AS `diskonbhp`
        , sum(`notadet`.`dijamin`) AS `tot_dijamin`
      FROM
      (
        (`nota` JOIN `notadet`) 
        LEFT JOIN `registrasidet` 
        ON ((`nota`.`idregdet` = `registrasidet`.`idregdet`))
      ) 
      WHERE 
      (
        (`nota`.`nokuitansi` = '".$nokuitansi."') AND 
        (`nota`.`idsttransaksi` = 1) AND 
        (`nota`.`nonota` = `notadet`.`nonota`)
      )
    ")->row_array();

    $diskon_barang += $q_notadet['diskonbhp'];
    $diskonjs = $q_notadet['diskonjs'];
    $diskonjm = $q_notadet['diskonjm'];
    $diskonjp = $q_notadet['diskonjp'];

    if($q_nota['tot_uangr'] > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_profesi['idakun'], 
        'kdakun' => $akun_profesi['kdakun'], 
        'nmakun' => $akun_profesi['nmakun'], 
        'noreff' => '0306', 
        'debit'  => 0, 
        'kredit' => $q_nota['tot_uangr']
      );
    }

    if($diskon_barang > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_bhp['idakun'], 
        'kdakun' => $akun_diskon_bhp['kdakun'], 
        'nmakun' => $akun_diskon_bhp['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskon_barang, 
        'kredit' => 0
      );
    }

    if($diskonjs > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_js['idakun'], 
        'kdakun' => $akun_diskon_js['kdakun'], 
        'nmakun' => $akun_diskon_js['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjs, 
        'kredit' => 0
      );
    }

    if($diskonjm > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_jm['idakun'], 
        'kdakun' => $akun_diskon_jm['kdakun'], 
        'nmakun' => $akun_diskon_jm['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjm, 
        'kredit' => 0
      );
    }

    if($diskonjp > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_jp['idakun'], 
        'kdakun' => $akun_diskon_jp['kdakun'], 
        'nmakun' => $akun_diskon_jp['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjp, 
        'kredit' => 0
      );
    }

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);
    echo json_encode($build_array);

  }

  function kalkulasi_transaksi_pasien()
  {
    $total_uangr = 0;
    $total_tagihan = 0;
    $diskon_pelayanan = 0;
    $diskon_obat = 0;
    $dijamin = 0;
    $total_bayar = 0;
    $nokuitansi = $this->input->post('nokuitansi');
    
    if(empty($nokuitansi)){
      $return = array(
        'total_uangr' => $total_uangr,
        'total_tagihan' => $total_tagihan,
        'diskon_pelayanan' => $diskon_pelayanan,
        'diskon_obat' => $diskon_obat,
        'dijamin' => $dijamin,
        'total_bayar' => $total_bayar
      );
      echo json_encode($return);
      die;
    }


    $q_nota = $this->db->query("
      SELECT 
      SUM(uangr) as tot_uangr,
      diskon as diskon_obat from nota where nokuitansi = '".$nokuitansi."'
    ")->row_array();

    $total_uangr    += @$q_nota['tot_uangr'];
    $diskon_obat    += @$q_nota['diskon_obat'];

    /*
    $q_notadet = $this->db->query("
      SELECT 
      sum(qty * (tarifjs + tarifjm + tarifjp + tarifbhp)) as tot_tagihan,
      sum(diskonjs + diskonjm + diskonjp + diskonbhp) as tot_diskon,
      sum(dijamin) as tot_dijamin
      from v_jurn_trans_rjugdnotadet
      WHERE nokuitansi = '".$nokuitansi."'
    ")->row_array();
    */

    $q_notadet = $this->db->query("
      SELECT  
      sum(qty * (tarifjs + tarifjm + tarifjp + tarifbhp)) as tot_tagihan,
            sum(diskonjs + diskonjm + diskonjp + diskonbhp) as tot_diskon,
            sum(dijamin) as tot_dijamin
      FROM
        ((`nota`
      JOIN `notadet`)
      LEFT JOIN `registrasidet`
      ON ((`nota`.`idregdet` = `registrasidet`.`idregdet`)))
      WHERE
        (
          (`nota`.`idsttransaksi` = 1) AND 
          (`nota`.`nonota` = `notadet`.`nonota`) AND
          nokuitansi = '".$nokuitansi."'
        )
    ")->row_array();

    $total_tagihan    += @$q_notadet['tot_tagihan'];
    $diskon_pelayanan += @$q_notadet['tot_diskon'];
    $dijamin          += @$q_notadet['tot_dijamin'];
    $total_bayar = ($total_uangr + $total_tagihan) - ($diskon_obat + $diskon_pelayanan + $dijamin); 

    $return = array(
      'total_uangr' => $total_uangr,
      'total_tagihan' => $total_tagihan,
      'diskon_pelayanan' => $diskon_pelayanan,
      'diskon_obat' => $diskon_obat,
      'dijamin' => $dijamin,
      'total_bayar' => $total_bayar
    );
    echo json_encode($return);
    die;

  }



  /* ==================== SECONDARY FUNCTIONS ==================== */

  function get_nojurnal_khusus(){
    //$q = "SELECT getOtoNojurnal(now()) as nm;";
    $q = "SELECT getOtoNoJurnalKhusus(now()) as nm;";
    $query  = $this->db->query($q);
    $nm= ''; 
                
    if ($query->num_rows() != 0)
    {
        $row = $query->row();
        $nm=$row->nm;
    }
    return $nm;
  }
}
