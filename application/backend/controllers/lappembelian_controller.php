 <?php 
class Lappembelian_Controller extends Controller{
	public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_lappo(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$this->db->select("*");
		$this->db->from("v_lappo");
		
		
		if($tglawal){
		$this->db->where('`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('po');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_lappodet(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$this->db->select("*");
		$this->db->from("v_lappodet");		
		
		if($tglawal){
		$this->db->where('v_lappodet.`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->numrow($fields, $query, $tglawal, $tglakhir);
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function numrow($fields, $query, $tglawal, $tglakhir){
      
        $this->db->select("*");
		$this->db->from("v_lappodet");	
		
		if($tglawal){
		$this->db->where('v_lappodet.`tglpo` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
    
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }

}
 ?>