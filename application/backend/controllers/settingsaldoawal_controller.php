<?php

class Settingsaldoawal_Controller extends Controller {
  public function __construct()
  {
    parent::Controller();
    $this->load->library('session');
    $this->load->library('rhlib');
  }
  
  function get_saldoawal_akun(){
    $tahun    = $_POST["tahun"]; 
    $bulan    = $_POST["bulan"]; 
    
    if(empty($tahun) || empty($bulan) || $bulan == '0'){
      $build_array = array ("success"=>true,"results"=>0,"data"=>array());
      echo json_encode($build_array);
      die;
    }

    $matching_bulan = array(
      '1'  => 'jan',    '2'  => 'feb',    '3'  => 'mar',
      '4'  => 'apr',    '5'  => 'mei',    '6'  => 'jun',
      '7'  => 'jul',    '8'  => 'ags',    '9'  => 'sep',
      '10' => 'okt',    '11' => 'nov',    '12' => 'des',
    );

    $fieldsaldodebit = 'saldodebit'.$matching_bulan[$bulan];
    $fieldsaldokredit = 'saldocredit'.$matching_bulan[$bulan];
    $this->db->where('tahun', $tahun);
    $this->db->select("tahun, idakun, kdakun, nmakun, {$fieldsaldodebit} as saldodebit, {$fieldsaldokredit} as saldokredit ");
    $this->db->from("v_akun_tahun");   
    $this->db->order_by('v_akun_tahun.kdakun');
        
    $q    = $this->db->get();
    $data = $q->result();

    if(!empty($data)){
      foreach($data as $idx => $dt){
        $data[$idx]->fieldsaldodebit = $fieldsaldodebit;
        $data[$idx]->fieldsaldokredit = $fieldsaldokredit;
      }
    }

    $build_array = array ("success"=>true,"results"=>count($data),"data"=>$data);

    echo json_encode($build_array);
  }
  
  function getFieldsAndValues(){
    
    $dataArray = array(
         'tahun'  => $_POST['tahun'], 
       'idakun'   => $_POST['idakun'],
             'userid' => $this->session->userdata['user_id'],
       'tglinput' => date('Y-m-d H:i:s'),
        );
    
    return $dataArray;
  }

  function simpan_saldo_awal_akun()
  {
    $arr_akuntahun      = $this->input->post('arr_akuntahun');
    $arr_akuntahun      = json_decode($arr_akuntahun);
    $update_success     = true;
    if(!empty($arr_akuntahun))
    {
      foreach($arr_akuntahun as $idx => $akuntahun){
        $exp_data = explode('-', $akuntahun);
        /*
          $exp_data[0] = tahun;
          $exp_data[1] = idakun;
          $exp_data[2] = saldodebit;
          $exp_data[3] = saldokredit;
          $exp_data[4] = fieldsaldodebit;
          $exp_data[5] = fieldsaldokredit;
        */

        $update = $this->db->query("
                  UPDATE akuntahun 
                  set 
                    ".$exp_data[4]." = '".$exp_data[2]."', 
                    ".$exp_data[5]." = '".$exp_data[3]."' 
                  WHERE tahun = '".$exp_data[0]."' AND idakun = '". $exp_data[1] ."' ");
        
        if(!$update)
          $update_success = false;

      }

    }

    if($update_success)
      $return = array('success' => true);
    else
      $return = array('success' => false);
     
    echo json_encode($return);die;
  }
}
