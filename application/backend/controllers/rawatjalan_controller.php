<?php

class Rawatjalan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
			$this->rhlib->secure($this->my_usession);
    }
		
	function insorupd_nota(){
        $this->db->trans_begin();        
		$arrnota = $this->input->post("arrnota");

		$this->db->select("rd.idregdet AS idregdet,
			rd.idbagian AS idbagian,
			rd.iddokter AS iddokter,
			rd.idklstarif AS idklstarif,
			nt.nokuitansi", false);
        $this->db->from('registrasidet rd');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('rd.noreg', $_POST['noreg']);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
		
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['nonota']));
		$notanr = $query->num_rows();
		$nota = $query->row_array();
		$kui = $reg['nokuitansi'];
		
		/*if($kui == 0){
			//$kque = $this->insert_kuitansi($nota);
			//$kui = $kque['nokuitansi'];
			$kui = null;
			$dataArray = array(
				'tglkeluar' => $_POST['tglnota'],
				'jamkeluar' => $_POST['jamnota']
			);
			$this->db->where('noreg', $_POST['noreg']);
			$regdet = $this->db->update('registrasidet', $dataArray);
			// $this->update_posisispasien($reg['idregdet']);
		} else {
			$kui = $reg['nokuitansi'];
		}*/
		
		$_POST['noresep'] = null;
		if($notanr == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			//$nota = $query->row_array();
			$vnota = $nota['nonota'];
		}
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $nota['idbagian']);
		
		if($nota && $notadet)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$vnota;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
		
	function insorupd_nota_pm(){
        $this->db->trans_begin();
		$kui = 0;
		if(isset($_POST['kui'])) $kui = $_POST['kui'];
		$arrnota = $this->input->post("arrnota");
		$_POST['stposisipasien'] = 3;		
		
		$this->db->select("*", false);		
        $this->db->from('registrasi r');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $_POST['noreg']);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
				
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['nonota']));
		$notanr = $query->num_rows();
		$nota = $query->row_array();
		$kui = $reg['nokuitansi'];

		if($notanr == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			$vnota = $nota['nonota'];
		}
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $nota['idbagian']);

		$this->update_posisispasien($reg['idregdet']);

		//insert kuitansi transfer PM
        $kui = $this->insert_kuitansi_tfpm($nota, $reg); 
		
		if($nota && $notadet)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$vnota;
            $ret["nokuitansi"]=$kui;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }

	function insert_kuitansi_tfpm($nota, $reg){
		$_POST['stposisipasien'] = 4;

		$this->db->select('r.noreg, rd.idregdet', false);
        $this->db->from('registrasi r');
		$this->db->join('registrasidet rd',
                'r.noreg = rd.noreg', 'left');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->where('r.idjnspelayanan', 2);
        $this->db->where('rd.tglkeluar IS NULL');
        $this->db->where('p.norm', str_pad($_POST['norm'], 10, "0", STR_PAD_LEFT));
        $this->db->groupby('r.noreg');
		$q = $this->db->get();
		$noreg = $q->row_array();

		if($q->num_rows() > 0){			
	        $kque = $this->insert_kuitranfer_pm($nota);
			$kui = $kque['nokuitansi'];

			$dataArray = array(
				'tglkeluar' => $_POST['tglnota'],
				'jamkeluar' => $_POST['jamnota']
			);
			$dataArrayNt = array(
				'nokuitansi' 		=> $kui,
				'idregdettransfer' 	=> $noreg['noreg']
			);
			$this->db->where('noreg', $_POST['noreg']);
			$regdet = $this->db->update('registrasidet', $dataArray);
			$this->db->where('idregdet', $reg['idregdet']);
			$updatenota = $this->db->update('nota', $dataArrayNt);
			
			$this->update_posisispasien($reg['idregdet']);
		}

		if($updatenota){
            $ret=$kui;
        }else{
            $ret=null;
        }
		
        return $ret;
	}

	function insert_kasir_rj_ugd(){
        $this->db->trans_begin();
		$kui = 0;
		if(isset($_POST['kui'])) $kui = $_POST['kui'];
		$arrnota = $this->input->post("arrnota");
		
		$this->db->select("
			r.noreg AS noreg,
			r.keluhan AS keluhan,
			r.nmkerabat AS nmkerabat,
			r.notelpkerabat AS notelpkerabat,
			r.catatan AS catatanr,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			p.alamat AS alamatp,
			p.nmibu AS nmibu,
			p.alergi AS alergi,
			p.tptlahir AS tptlahirp,
			p.noidentitas AS noidentitas,
			p.tgllahir AS tgllahirp,
			p.notelp AS notelpp,
			p.nohp AS nohpp,
			p.tgldaftar AS tgldaftar,
			p.negara AS negara,
			rd.idregdet AS idregdet,
			rd.tglreg AS tglreg,
			rd.jamreg AS jamreg,
			rd.tglmasuk AS tglmasuk,
			rd.jammasuk AS jammasuk,
			rd.tglkeluar AS tglkeluar,
			rd.jamkeluar AS jamkeluar,
			rd.catatankeluar AS catatankeluar,
			rd.tglrencanakeluar AS tglrencanakeluar,
			rd.catatanrencanakeluar AS catatanrencanakeluar,
			rd.umurtahun AS umurtahun,
			rd.umurbulan AS umurbulan,
			rd.umurhari AS umurhari,
			rd.userbatal AS userbatal,
			rd.nmdokterkirim AS nmdokterkirim,
			rd.userinput AS userinput,
			rd.idstkeluar AS idstkeluar,
			rd.idcarakeluar AS idcarakeluar,
			r.idpenjamin AS idpenjamin,
			r.idjnspelayanan AS idjnspelayanan,
			r.idstpasien AS idstpasien,
			r.idhubkeluarga AS idhubkeluarga,
			p.idjnskelamin AS idjnskelamin,
			rd.idcaradatang AS idcaradatang,
			rd.idbagian AS idbagian,
			rd.idbagiankirim AS idbagiankirim,
			rd.idkamar AS idkamar,
			rd.iddokter AS iddokter,
			rd.iddokterkirim AS iddokterkirim,
			rd.idklsrawat AS idklsrawat,
			rd.idklstarif AS idklstarif,
			rd.idshift AS idshift,
			rd.idbed AS idbed,
			bed.idstbed AS idstbed,
			bed.nmbed AS nmbed,
			jkel.nmjnskelamin AS nmjnskelamin,
			jkel.kdjnskelamin AS kdjnskelamin,
			jpel.nmjnspelayanan AS nmjnspelayanan,
			dae.nmdaerah AS nmdaerah,
			sp.idstpelayanan AS idstpelayanan,
			sp.nmstpelayanan AS nmstpelayanan,
			pnj.nmpenjamin AS nmpenjamin,
			kr.nmklsrawat AS nmklsrawat,
			bag.nmbagian AS nmbagian,
			bagkrm.nmbagian AS nmbagiankirim,
			kmr.nmkamar AS nmkamar,
			kmr.kdkamar AS kdkamar,
			nt.nonota AS nonota,
			nt.nona AS nona,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.catatandiskon AS catatand,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			nt.idsttransaksi AS idsttransaksi,
			nt.idregdettransfer AS idregdettransfer,
			nt.stplafond AS stplafond,
			na.stplafond AS stplafondna,
			na.catatandiskon AS catatands,
			d.nmdokter AS nmdokter,
			d.nmdoktergelar AS nmdoktergelar,
			dkrm.nmdoktergelar AS nmdokterkirimdlm,
			res.noantrian AS noantrian,
			cd.nmcaradatang AS nmcaradatang,
			pospas.nmstposisipasien AS nmstposisipasien,
			pospas.idstposisipasien AS idstposisipasien,
			kui.nokuitansi AS nokuitansi,
			kui.atasnama AS atasnama,
			kui.total AS total,
			kui.nokasir AS nokasir,
			kui.tglkuitansi AS tglkuitansi,
			kui.jamkuitansi AS jamkuitansi,
			sbt.kdsbtnm AS kdsbtnm,
			kt.nmklstarif AS nmklstarif,
			stkawin.nmstkawin AS nmstkawin,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit,
			daerah.nmdaerah AS kelurahan,
			kec.nmdaerah AS kec,
			kota.nmdaerah AS kota,
			agama.nmagama AS nmagama,
			goldarah.nmgoldarah AS nmgoldarah,
			pendidikan.nmpendidikan AS nmpendidikan,
			pekerjaan.nmpekerjaan AS nmpekerjaan,
			sukubangsa.nmsukubangsa AS nmsukubangsa
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $_POST['noreg']);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
		
		if(isset($_POST['fri'])) $reg['idbagian'] = $_POST['fri'];
		
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['nonota']));
		$notanr = $query->num_rows();
		$nota = $query->row_array();

		if($kui == 0){
			$kque = $this->insert_kuitansi($nota);
			$kui = $kque['nokuitansi'];
			//$kui = null;
			$dataArray = array(
				'tglkeluar' => $_POST['tglnota'],
				'jamkeluar' => $_POST['jamnota']
			);
			$dataArrayNota = array(
				'nokuitansi' => $kui
				
			);
			$this->db->where('noreg', $_POST['noreg']);
			$regdet = $this->db->update('registrasidet', $dataArray);
			
			$this->db->where('idregdet', $reg['idregdet']);
			$updatenota = $this->db->update('nota', $dataArrayNota);
			$v_kuitansi = $kui;
			
			$this->update_posisispasien($reg['idregdet']);
		}
		
		/* $_POST['noresep'] = null;
		if($notanr == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			//$nota = $query->row_array();
			$vnota = $nota['nonota'];
		}
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $nota['idbagian']);
		 */
		if($updatenota)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$v_kuitansi;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_kuitansi($nota){
		/*$dataArray = $this->getFieldsAndValuesKui($nota);
		$kuitansi = $this->db->insert('kuitansi',$dataArray);*/
		if(!$nota['nokuitansi']){
			$dataArray = $this->getFieldsAndValuesKui($nota);
			$kuitansi = $this->db->insert('kuitansi',$dataArray);
		} else {
			$dataArray = $this->update_kuitansi($nota);
		}
		
		$query = $this->db->getwhere('kuitansidet',array('nokuitansi'=>$dataArray['nokuitansi']));
		$kuitansidet = $query->row_array();
		if($query->num_rows() == 0){
			$kuitansidet = $this->insert_kuitansidet($dataArray);
		} else {
			$kuitansidet = $this->update_kuitansidet($dataArray);
		}
		
		if($kuitansidet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }

	function insert_kuitranfer_pm($nota){
		if(!$nota['nokuitansi']){
			$dataArray = $this->getFieldsAndValuesKuiTfLab($nota);
			$this->db->insert('kuitansi',$dataArray);
		}else{
			$dataArray = $this->getFieldsAndValuesKuiTfLab($nota);
			$this->db->where('nokuitansi', $dataArray['nokuitansi']);
			$this->db->update('kuitansi', $dataArray);
		}
		
		$query = $this->db->getwhere('kuitansidet',array('nokuitansi'=>$dataArray['nokuitansi']));
		$kuitansidet = $query->row_array();
		if($query->num_rows() == 0){
			$dataArray = $this->getFieldsAndValuesKuiDetTfPm($dataArray);
			$kuitansidet =$this->db->insert('kuitansidet',$dataArray);
		}else{			
			$where['nokuitansi'] = $dataArray['nokuitansi'];
			$this->db->delete('kuitansidet',$where);

			$dataArray = $this->getFieldsAndValuesKuiDetTfPm($dataArray);
			$kuitansidet =$this->db->insert('kuitansidet',$dataArray);
		}
		
		if($kuitansidet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }

	function insert_kuitansidet($kuitansi){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrcarabayar']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesKuiDet($kuitansi, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('kuitansidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
		
	function insert_nota($reg, $kui){
		$dataArray = $this->getFieldsAndValues($reg, $kui);
		$z =$this->db->insert('nota',$dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
		
	function insert_notadet($nota, $reg, $arrnota, $idbagian){
		$where['nonota'] = $nota;
		//update brgbagian
		$query = $this->db->getwhere('notadet',$where);
		$ceknd = $query->row_array();
		if($ceknd && !in_array($_POST['jtransaksi'],array(4,7)))$stok = $this->tambahStok($where, $idbagian);
		if($ceknd)$this->db->delete('notapaket',array('idnotadet'=>$ceknd['idnotadet']));
		$this->db->delete('notadet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $arrnota);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-',$val);
			if($val != '' && $vale[0] != "null") {
				if(!isset($vale[9]))$vale[9]=0;
				
				if ($_POST['ureg'] != 'FA' && (!isset($vale[8]) || $vale[8]=='')) $dokter=null;
				else if(!isset($vale[8]) || $vale[8]=='null') $dokter=$reg['iddokter'];
				else $dokter=$this->searchIdkhususdr('iddokter','dokter','nmdoktergelar',$vale[8]);
				//if((!isset($vale[8]) || !$dokter) && ($vale[8]=='' || !$dokter) )$dokter=$this->searchIdkhususdr('iddokter','dokter','nmdoktergelar',$vale[8]);
				
				$dataArray = $this->getFieldsAndValuesDet($nota, $reg, $vale[0], $vale[1], $vale[2], $vale[3], $vale[4], $vale[5], $vale[6], $vale[7], $dokter, $vale[9]);
				$z =$this->db->insert('notadet',$dataArray);
				if($ceknd && !in_array($_POST['jtransaksi'],array(4,7)))$stok = $this->kurangStok($idbagian, $vale[0], $vale[1], $vale[2]);
				
				//perintah jika tarif paket di pakai
				/*$strbrp = substr($vale[0], 0, 1);
				if($strbrp == 'P'){
					$this->insorupd_paket($vale[0], $nota);
				}*/
			}
		}
        return true;
    }
	
	function insert_kartustok(){
		/* $stkeluar			= $this->input->post("stkeluar");
		$carakeluar			= $this->input->post("carakeluar");
		if(!$stkeluar) $stkeluar = 1;
		if(!$carakeluar) $carakeluar = 1; */
		
		$query = $this->db->getwhere('kartustok',array('noref'=>$_POST['noref']));
		$cekks = $query->num_rows();
		
		if($cekks < 1){
			$k=array('[',']','"');
			$r=str_replace($k, '', $_POST['arrnota']);
			$b=explode(',', $r);
			foreach($b AS $val){
				$vale = explode('-',$val);
				
				$query = $this->db->getwhere('bagian',array('nmbagian'=>$vale[4]));
				$rec = $query->row_array();
				
				$where = $vale[5].' ORDER BY ';
				$this->db->select("saldoakhir", false);
				$this->db->from("kartustok");
				$this->db->where("idbagian", $rec['idbagian']);
				$this->db->where("kdbrg", $vale[5]);
				$this->db->order_by("tglinput DESC");
				$query = $this->db->get();
				$recks = $query->row_array();
				if(!$recks) $recks['saldoakhir'] = 0;
				$saldoakhir = $recks['saldoakhir'] - $vale[3];
				$tgltransaksi = date_format(date_create($vale[1]), 'Y-m-d');
				
				
				$query = $this->db->getwhere('barang',array('kdbrg'=>$vale[5]));
				$hrgbrg = $query->row_array();
				
				/* $qry = $this->db->getwhere('barangbagian',array('kdbrg'=>$vale[5], 'idbagian'=>$rec['idbagian']));
				$rec1 = $qry->row_array();
				
				$saldoakhir = $rec1['stoknowbagian'] - $vale[3]; */
				
				if($query->num_rows() > 0){
					$this->db->select("SUM(jmlkeluar) AS jmlkeluar", false);
					$this->db->from("kartustok");
					$this->db->where("idbagian", $rec['idbagian']);
					$this->db->where("kdbrg", $vale[5]);
					$this->db->order_by("tglinput DESC");
					$qry = $this->db->get();
					$jmlk = $qry->row_array();

					$qq = $this->db->orderby('tglinput ASC')->getwhere('kartustok',array('kdbrg'=>$vale[5], 'idbagian'=>$rec['idbagian'], 'jmlmasuk > 0'=>null));
					$ks = $qq->result();
					$hrgbeli = 0;
					$c1 = 0;
					if($qq->num_rows()>0){
						$jmlkeluar = $jmlk['jmlkeluar'];
						foreach($ks AS $val1){
							if($c1 == 0){
								$qw = $this->db->select('SUM(jmlkeluar) AS sumj')->getwhere('kartustok',array('kdbrg'=>$vale[5], 'idbagian'=>$rec['idbagian'], 'tglinput < '=>$val1->tglinput));
								$sumj = $qw->row_array();
								$val1->jmlmasuk += $sumj['sumj'];
								$c1 = 1;
							}
							$jmlkeluar -= $val1->jmlmasuk;
							if($jmlkeluar<1 && $hrgbeli==0) $hrgbeli=$val1->hrgbeli;
						}
						if($hrgbeli == 0) $hrgbeli = $hrgbrg['hrgbeli'];
					} else {
						$hrgbeli = $hrgbrg['hrgbeli'];
					}
					/* var_dump($jmlkeluar);
					exit; */ #kartustock
					
					$this->db->query("CALL SP_insert_transaksi (?,?,?,?,?,?,?,?,?,?,?,?,?)",
								array(
									$vale[0],
									$tgltransaksi,
									$vale[2],
									$vale[3],
									$saldoakhir,
									$this->session->userdata['user_id'],
									$rec['idbagian'],
									$_POST['noref'],
									$rec['kdbagian'],
									$rec['nmbagian'],
									$vale[5],
									$hrgbeli,
									$hrgbrg['hrgjual']
								));
				}
				if($this->input->post("ri")){
					$kdr = 11;
					if($rec['idbagian'] != 11) $kdr = 'null';
					$stok = $this->kurangStok($rec['idbagian'], $vale[5], $vale[3], $kdr);
				}
			}
		
			//UPDATE
			/* if(isset($_POST['regis'])){
				$query = $this->db->getwhere('registrasidet',array('noreg'=>$_POST['noreg']));
				$reg = $query->row_array();
				if(is_null($reg['tglkeluar'])){
					$dataArray = array(
						'tglkeluar' => date('Y-m-d'),
						'jamkeluar' => date('H:i:s'),
						/* 'idstkeluar' => $stkeluar,
						'idcarakeluar' => $carakeluar, */
					/* );
					$this->db->where('noreg', $_POST['noreg']);
					$z = $this->db->update('registrasidet', $dataArray);
					$this->update_posisispasien($reg['idregdet']);
				}
			} */
		}
		
        return true;
    }	

	function update_nota($reg, $kui){
		$dataArray = $this->getFieldsAndValues($reg, $kui);
		
		//UPDATE
		$this->db->where('nonota', $_POST['nonota']);
		$z = $this->db->update('nota', $dataArray); 
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function update_kuitansi($nota){
		$dataArray = $this->getFieldsAndValuesKui($nota);
		
		//UPDATE
		$this->db->where('nokuitansi', $dataArray['nokuitansi']);
		$kuitansi = $this->db->update('kuitansi', $dataArray);
		
		return $dataArray;
    }
	
	function update_kuitansidet($kuitansi){
		$where['nokuitansi'] = $kuitansi['nokuitansi'];
		$this->db->delete('kuitansidet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrcarabayar']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesKuiDet($kuitansi, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('kuitansidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function update_transfer(){
		$dataArray = array(
			'idregdettransfer' => $_POST['transfer']
		);
		$this->db->where('nonota', $_POST['nonota']);
		$z = $this->db->update('nota', $dataArray);
        if($z){
			$query = $this->db->getwhere('nota',array('nonota'=>$_POST['nonota']));
			$nokui = $query->row_array();
			
			$dataArrayx = array(
				'idcarabayar' => 9
			);
			$this->db->where('nokuitansi', $nokui['nokuitansi']);
			$y = $this->db->update('kuitansidet', $dataArrayx);
			
			$this->update_posisispasien($_POST['idtransfer']);
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function update_posisispasien($idregdet){
		$dataArray = array(
			'idstposisipasien' => $_POST['stposisipasien']
		);
		$this->db->where('idregdet', $idregdet);
		$z = $this->db->update('reservasi', $dataArray); 
        if($z){
            $ret=$z;
        }else{
            $ret=false;
        }
        return $ret;
    }
		
	function getFieldsAndValuesKui($nota){		
		 if(!$nota['nokuitansi']){
			$nr = $this->getNokuitansi();
			$nostart = 'NK';
			$nomid = date('y');
			$noend = str_pad($nr, 8, "0", STR_PAD_LEFT);
			$nokuitansi = $nostart.$nomid.$noend;
		} else{
			$nokuitansi = $nota['nokuitansi'];
		}

		// $xidregdet = $nota['idregdet'];
		 if(!isset($_POST['idregdet']))$idregdet=null;
		else $idregdet=$_POST['idregdet'];
		 
		if(!isset($_POST['idjnskuitansi']))$idjnskuitansi=1;
		else $idjnskuitansi=$_POST['idjnskuitansi'];
		
		if(!isset($_POST['pembayaran'])) $_POST['pembayaran'] = $_POST['total'];
		
		if(!isset($_POST['tgltrans'])) $tgltrans = $_POST['tglnota'];
		else $tgltrans = $_POST['tgltrans'];
		
		if(!isset($_POST['jamtrans'])) $jamtrans = $_POST['jamnota'];
		else $jamtrans = $_POST['jamtrans'];
		
		$dataArray = array(
             'nokuitansi'=> $nokuitansi,
             'tglkuitansi'=> $tgltrans,
             'jamkuitansi'=> $jamtrans,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'idstkuitansi'=> 1,
             'idsbtnm'=> $this->searchSbtnm('idsbtnm','jsbtnm',$_POST['tahun'],$_POST['jkel']),
             'atasnama'=> $_POST['atasnama'],
             'idjnskuitansi'=> $idjnskuitansi,
             'pembulatan'=> 0,
             'total'=> $_POST['total'],
             'pembayaran'=> $_POST['pembayaran'],
             'idregdet'=> $idregdet,
             //'ketina'=> 0,
             //'keteng'=> 0
        );
        
		return $dataArray;
	}

	function getFieldsAndValuesKuiTfLab($nota){		
		 if(!$nota['nokuitansi']){
			$nr = $this->getNokuitansi();
			$nostart = 'NK';
			$nomid = date('y');
			$noend = str_pad($nr, 8, "0", STR_PAD_LEFT);
			$nokuitansi = $nostart.$nomid.$noend;
		} else{
			$nokuitansi = $nota['nokuitansi'];
		}
		
		$jumlah = $_POST['totaltagihan'] + $_POST['totaldijamin'];

		$dataArray = array(
             'nokuitansi'=> $nokuitansi,
             'tglkuitansi'=> $_POST['tglnota'],
             'jamkuitansi'=> $_POST['jamnota'],
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'idstkuitansi'=> 1,
             'idsbtnm'=> $this->searchSbtnm('idsbtnm','jsbtnm',$_POST['tahun'],$_POST['jkel']),
             'atasnama'=> $_POST['atasnama'],
             'idjnskuitansi'=> 6,
             'pembulatan'=> 0,
             'total'=> $jumlah,
             'pembayaran'=> $jumlah,
             'idregdet'=> null,
             //'ketina'=> 0,
             //'keteng'=> 0
        );
        
		return $dataArray;
	}
			
	function getFieldsAndValuesKuiDet($kuitansi, $val0, $val1, $val2, $val3){
		if($val1 == 'undefined')$idbank = null;
		else $idbank = $this->searchId('idbank','bank','nmbank',$val1);
		if($val2 == 'undefined' || $val2 == ''|| $val2 == 'null')$nokartu = null;
		else $nokartu = $val2;
		$dataArray = array(
			 'nokuitansi'=> $kuitansi['nokuitansi'],
			 'idbank'=> $idbank,
			 'idcarabayar'=> $this->searchId('idcarabayar','carabayar','nmcarabayar',$val0),
			 'nokartu'=> $nokartu,
			 'jumlah'=> $val3
		);
		return $dataArray;
	}	

	function getFieldsAndValuesKuiDetTfPm($kuitansi){
		$dataArray = array(
			 'nokuitansi'=> $kuitansi['nokuitansi'],
			 'idbank'=> null,
			 'idcarabayar'=> 9,
			 'nokartu'=> null,
			 'jumlah'=> $kuitansi['total']
		);
		return $dataArray;
	}
	
	function getFieldsAndValuesKuiDetup($kuitansi, $val0, $val1, $val2, $val3){
		if($val1 == 'undefined')$idbank = null;
		else $idbank = $this->searchId('idbank','bank','nmbank',$val1);
		if($val2 == 'undefined' || $val2 == ''|| $val2 == 'null')$nokartu = null;
		else $nokartu = $val2;
		$dataArray = array(
			 'nokuitansi'=> $kuitansi,
			 'idbank'=> $idbank,
			 'idcarabayar'=> $this->searchId('idcarabayar','carabayar','nmcarabayar',$val0),
			 'nokartu'=> $nokartu,
			 'jumlah'=> $val3
		);
		return $dataArray;
	}
	
	function getFieldsAndValues($reg, $kui){
		if(is_null($_POST['nonota']) || $_POST['nonota'] == '')
		{
			$nr = $this->getNonota();
			$nostart = 'N'.$_POST['ureg'];
			$nomid = date('y');
			$nombln = date('m');
			$noend = str_pad($nr, 5, "0", STR_PAD_LEFT);
			$nonota = $nostart.$nomid.$nombln.$noend;
		} else{
			$nonota = $_POST['nonota'];
		}		

		if($_POST['ureg'] == "PM"){
			if($_POST['totaltagihan'] == 0){
				$_POST['plafond']='true';
				$_POST['ttlplafond'] = $_POST['totaldijamin'];
			}
		}

		if(!isset($_POST['diskon']))$_POST['diskon']=0;
		
		if(!isset($_POST['uangr']))$_POST['uangr']=0;
		
		if(!isset($_POST['stpelayanan']))$stpelayanan=null;
		else $stpelayanan=$this->searchId('idstpelayanan','stpelayanan','nmstpelayanan',$_POST['stpelayanan']);
		
		if(!isset($_POST['plafond'])) $_POST['plafond'] = 'false';
		
		if($_POST['plafond']=='true'){
			$plafond = $_POST['ttlplafond'];
			$stplafond = 1;
		} else {
			$plafond = 0;
			$stplafond = 0;
		}
		
		if(!isset($_POST['catatandskn']))$_POST['catatandskn']=null;
		
		if(!isset($_POST['idbagianfar']))$idbagianfar=null;
		else $idbagianfar=$_POST['idbagianfar'];
		$dataArray = array(
             'nonota'=> $nonota,
             //'nona'=> $nonota,
             'tglnota'=> $_POST['tglnota'],
             'jamnota'=> $_POST['jamnota'],
             'idbagian'=> $reg['idbagian'],
             'idbagianfar'=> $idbagianfar,
             'iddokter'=> $reg['iddokter'],
             'idjnstransaksi'=> $_POST['jtransaksi'],
             'idsttransaksi'=> 1,
             'nokuitansi'=> $kui,
             'idregdet'=> $reg['idregdet'],
             'noorder'=> 1,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'noresep'=> $_POST['noresep'],
             'catatan'=> $_POST['catatan'],
             'catatandiskon'=> $_POST['catatandskn'],
             'diskon'=> $_POST['diskon'],
             'uangr'=> $_POST['uangr'],
             'userinput'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d'),
			 'idregdettransfer'=> null,
			 'idstpelayanan'=> $stpelayanan,
			 'plafond'=> $plafond,
			 'stplafond'=> $stplafond
        );
		return $dataArray;
	}
	
	function getFieldsAndValuesDet($nota, $reg, $val0, $val1, $val2, $val3, $val4, $val5, $val6, $val7, $val8, $val9){
		if ($val2 == 'null') $val2 = null;
		if(!(int)$val4)$val4 = 0;
		if(!(int)$val5)$val5 = 0;
		if(!(int)$val6)$val6 = 0;
		if(!(int)$val7)$val7 = 0;
		
		if($val9>0)$stdijamin = 1;
		else $stdijamin = 0;
		
		$query = $this->db->getwhere('barang',array('kdbrg'=>$val0));
		$barang = $query->row_array();
		if($barang){
			$hrgjual = $barang['hrgjual'];
			$hrgbeli = $barang['hrgbeli'];
		}else{
			$hrgjual = null;
			$hrgbeli = null;
		}
		
		if($val2){
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'idbagian'=>11));
		} else {
			$strbr = substr($val0, 0, 1);
			if($strbr == 'B'){
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'idbagian'=>$reg['idbagian']));
			} elseif($strbr == 'T'){
				/* if($reg['idjnspelayanan'] != 2){
					$reg['idklstarif'] = '';
				} */
				if(!isset($reg['idklstarif'])) $reg['idklstarif'] = $_POST['idklstarif'];
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'klstarif'=>$reg['idklstarif']));
				if(!$query->row_array()){
					$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'klstarif'=>5));
				}
			} else { 
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0));
			}
		}
		$item = $query->row_array();
		/* if(!$item){
			$query = $this->db->getwhere('tarif',array('kdpelayanan'=>$val0));
			$item = $query->row_array();
			$item['satuankcl'] = null;
		} */
		if(!isset($item['tarifjs']) || $item['tarifjs']=='')$item['tarifjs'] = 0;
		if(!isset($item['tarifjm']) || $item['tarifjm']=='')$item['tarifjm'] = 0;
		if(!isset($item['tarifjp']) || $item['tarifjp']=='')$item['tarifjp'] = 0;
		if(!isset($item['tarifbhp']) || $item['tarifbhp']=='')$item['tarifbhp'] = 0;
		
		$diskon = ($val1*((int)$item['tarifjs']+(int)$item['tarifjm']+(int)$item['tarifjp']+(int)$item['tarifbhp'])) - $val3;
		if(!isset($_POST['plafond'])) $_POST['plafond'] = 'false';
		if($_POST['plafond']=='true'){
			$val9 = ($item['tarifjs']+$item['tarifjm']+$item['tarifjp']+$item['tarifbhp']-$val4-$val5-$val6-$val7)*$val1;
			$stdijamin = 1;
		}
		
		if(isset($item['satuankcl'])) $item['satuankcl'] = $this->searchId('idsatuan','jsatuan','nmsatuan',$item['satuankcl']);
		else $item['satuankcl'] = null;
		$dataArray = array(
             'nonota'=> $nota,
             'kditem'=> $val0,
             'koder'=> $val2,
             //'idjnstarif'=> 0,
             //'kdresep'=> $_POST['kdnota'],
             'idsatuan'=> $item['satuankcl'],
             'qty'=> $val1,
             'tarifjs'=> $item['tarifjs'],
             'tarifjm'=> $item['tarifjm'],
             'tarifjp'=> $item['tarifjp'],
             'tarifbhp'=> $item['tarifbhp'],
             'diskonjs'=> $val4,
             'diskonjm'=> $val5,
             'diskonjp'=> $val6,
             'diskonbhp'=> $val7,
             //'uangr'=> 0,
             'iddokter'=> $val8,
             //'idperawat'=> 1,
             'hrgjual'=> $hrgjual,
             'hrgbeli'=> $hrgbeli,
             'dijamin'=> $val9,
             'stdijamin'=> $stdijamin,
			 //'idstbypass'=> 1
        );

		return $dataArray;
	}
	
	function getNonota(){
        $this->db->select("count(nonota) AS max_np");
        $this->db->from("nota");
        $this->db->where('SUBSTRING(nonota,2,2)', $_POST['ureg']);
        $this->db->where('SUBSTRING(nonota,4,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function getNokuitansi(){
        $this->db->select("count(nokuitansi) AS max_np");
        $this->db->from("kuitansi");
        $this->db->where('SUBSTRING(nokuitansi,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}

	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function searchIdkhususdr($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->like($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function searchSbtnm($select, $tbl, $val, $val2){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($val .' BETWEEN ', 'dariumur AND sampaiumur', false);
        $this->db->where('idjnskelamin', $val2);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function tambahStok($where, $idbagian){
        $query = $this->db->getwhere('notadet',$where);
		$ndold = $query->result_array();
		for($i=0;$i<$query->num_rows();$i++){
			$strbr = substr($ndold[$i]['kditem'], 0, 1);
			if($strbr == 'B'){
				($ndold[$i]['koder']) ? $idbag = 11 : $idbag = $idbagian;
				$q = $this->db->getwhere('barangbagian',array('kdbrg'=>$ndold[$i]['kditem'], 'idbagian'=>$idbag));
				$brgbagian = $q->row_array();
				$qty = $brgbagian['stoknowbagian'] + $ndold[$i]['qty'];
				
				$dataArray = array('stoknowbagian'=> $qty);
				$this->db->where('idbagian', $idbag);
				$this->db->where('kdbrg', $ndold[$i]['kditem']);
				$z = $this->db->update('barangbagian', $dataArray);
			}
		}
		return true;
    }
	
	function kurangStok($idbagian, $kdbrg, $qtynow, $koder){
		$strbr = substr($kdbrg, 0, 1);
		if($strbr == 'B'){
			($koder != 'null') ? $idbag = 11 : $idbag = $idbagian;
			$query = $this->db->getwhere('barangbagian',array('kdbrg'=>$kdbrg, 'idbagian'=>$idbag));
			$brgbagian = $query->row_array();
			$qty = $brgbagian['stoknowbagian'] - $qtynow;
			
			$dataArray = array('stoknowbagian'=> $qty);
			$this->db->where('idbagian', $idbag);
			$this->db->where('kdbrg', $kdbrg);
			$z = $this->db->update('barangbagian', $dataArray);
		}
		return true;
    }
	
	function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where userid = 'Batal' AND ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }
////////////////////////	
	
}
