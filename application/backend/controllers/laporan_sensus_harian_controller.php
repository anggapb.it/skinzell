<?php 
class Laporan_sensus_harian_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}
	
	function get_bagian_all() {
		$q = $this->db->query("SELECT * FROM bagian WHERE idjnspelayanan=2");
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_pasien_masuk($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan){
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglmasuk ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jammasuk,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
		if ($opttgl=='<>' && $optjam=='<>' && $optruangan=='<>') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_pindahan($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan){
		$q = "SELECT * FROM (SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
			 , (SELECT concat('Pindahan dari kamar ',b.nmbagian) from registrasidet r 
				 LEFT JOIN bagian b ON r.idbagian = b.idbagian
				  where r.noreg = registrasidet.noreg and r.idregdet < registrasidet.idregdet
				  order by r.idregdet desc  limit 1) as keterangan
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglmasuk ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jammasuk,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg) a where keterangan is not null";
		
		if ($opttgl=='<>' && $optjam=='<>' && $optruangan=='<>') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_dipindahkan($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan){
		$q = "SELECT * FROM (SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglkeluar
			 , time_format(registrasidet.jamkeluar,'%H:%i') as jamkeluar
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
			 , (SELECT concat('Dipindahkan ke kamar ',b.nmbagian) from registrasidet r 
				 LEFT JOIN bagian b ON r.idbagian = b.idbagian
				  where r.noreg = registrasidet.noreg and r.idregdet > registrasidet.idregdet
				  order by r.idregdet desc  limit 1) as keterangan
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglkeluar ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jamkeluar,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg) a where keterangan is not null";
		
		if ($opttgl=='<>' && $optjam=='<>' && $optruangan=='<>') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_keluar_hidup($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan){
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglkeluar
			 , time_format(registrasidet.jamkeluar,'%H:%i') as jamkeluar
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 1
		  AND registrasidet.tglkeluar ".$opttgl." '".$valtgl."' 
		  AND time_format(registrasidet.jamkeluar,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
		if ($opttgl=='<>' && $optjam=='<>' && $optruangan=='<>') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_meninggal_kurang_48_jam($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan){
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , kodifikasi.tglmeninggal
			 , time_format(kodifikasi.jammeninggal,'%H:%i') as jammeninggal
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN kodifikasi
		ON registrasi.noreg = kodifikasi.noreg
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 2
		  AND kodifikasi.tglmeninggal ".$opttgl." '".$valtgl."' 
		  AND time_format(kodifikasi.jammeninggal,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
		if ($opttgl=='<>' && $optjam=='<>' && $optruangan=='<>') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_meninggal_lebih_sm_dgn_48_jam($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan){
		$q = "SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , kodifikasi.tglmeninggal
			 , time_format(kodifikasi.jammeninggal,'%H:%i') as jammeninggal
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN kodifikasi
		ON registrasi.noreg = kodifikasi.noreg
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.idstkeluar = 3
		  AND kodifikasi.tglmeninggal ".$opttgl." '".$valtgl."' 
		  AND time_format(kodifikasi.jammeninggal,'%H:%i') ".$optjam." '".$valjam."'
		  AND registrasidet.idbagian ".$optruangan." '".$valruangan."'
		ORDER BY
		  registrasidet.noreg";
		
		if ($opttgl=='<>' && $optjam=='<>' && $optruangan=='<>') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$ttl = count($data);
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
        echo json_encode($build_array);
	}
	
	function get_pasien_hari_ini($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan){
		$opttgl = ($opttgl=='=') ? "<=":"<>";
		
		$start = $this->input->post("start");
        $limit = $this->input->post("limit");
		
		if ($start==null){
            $start = 0;
			$limit = 20;
		}
		
		$fields = $this->input->post("fields");
		$query = $this->input->post("query");
		
		$qlike = ""; // add query like
		if($fields!="" || $query !=""){
			$k=array('[',']','"');
			$r=str_replace($k, '', $fields);
			$b=explode(',', $r);
			$c=count($b);
			for($i=0;$i<$c;$i++){
				if ($i==0) {
					$qlike .= " WHERE ".$b[$i]." LIKE '%".$query."%'";
				} else {
					$qlike .= " OR ".$b[$i]." LIKE '%".$query."%'";
				}
			}
		}
			
		$q = "SELECT * FROM (SELECT trim(LEADING '0' FROM `registrasi`.`norm`) AS norm
			 , registrasidet.noreg
			 , registrasidet.tglmasuk
			 , time_format(registrasidet.jammasuk,'%H:%i') as jammasuk
			 , pasien.nmpasien
			 , dokter.nmdoktergelar
			 , klsrawat.nmklsrawat
			 , kamar.nmkamar
			 , bed.nmbed
			 , registrasidet.idbagian
			 , bagian.nmbagian
		FROM
		  registrasidet
		LEFT JOIN registrasi
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN pasien
		ON registrasi.norm = pasien.norm
		LEFT JOIN klsrawat
		ON registrasidet.idklsrawat = klsrawat.idklsrawat
		LEFT JOIN kamar
		ON registrasidet.idkamar = kamar.idkamar
		LEFT JOIN bed
		ON registrasidet.idbed = bed.idbed
		LEFT JOIN bagian
		ON registrasidet.idbagian = bagian.idbagian
		LEFT JOIN dokter
		ON registrasidet.iddokter = dokter.iddokter
		WHERE
		  (registrasidet.tglbatal IS NULL OR registrasidet.userbatal IS NULL)
		  AND registrasi.idjnspelayanan = 2
		  AND registrasidet.tglkeluar IS NULL
		  OR registrasidet.tglmasuk = '".$valtgl."'
          AND registrasidet.idbed IS NOT NULL
		ORDER BY
		  registrasidet.tglmasuk desc) A".$qlike."";
		
		if ($opttgl=='<>' && $optjam=='<>' && $optruangan=='<>') {
			$build_array = array ("success"=>true,"results"=>'no data',"data"=>array());
		} else {
			$query = $this->db->query($q." LIMIT ".$start.", ".$limit);
			$data = array();
			if ($query->num_rows() > 0) {
				$data = $query->result();
			}
			
			$queryall  = $this->db->query($q);
			
			$ttl = $queryall->num_rows();
			$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

			if($ttl>0){
				$build_array["data"]=$data;
			}
		}
		
		echo json_encode($build_array);
	}
//===================================================	
	function get_sensus_harian($num){
		$opttgl = ($this->input->post("cbxtgl")=='true') ? "=":"<>";
		$optjam = ($this->input->post("cbxjam")=='true') ? "=":"<>";
		$optruangan = ($this->input->post("cbxruangan")=='true') ? "=":"<>";
		
		$valtgl = ($this->input->post("cbxtgl")=='true') ? $this->input->post("tgl"):"-";
		$valjam = ($this->input->post("cbxjam")=='true') ? $this->input->post("jam"):"-";
		$valruangan = ($this->input->post("cbxruangan")=='true') ? $this->input->post("ruangan"):"-";
		
		if ($num==1) {
			$this->get_pasien_masuk($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan);
		} else if ($num==2) {
			$this->get_pasien_pindahan($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan);
		} else if ($num==3) {
			$this->get_pasien_dipindahkan($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan);
		} else if ($num==4) {
			$this->get_pasien_keluar_hidup($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan);
		} else if ($num==5) {
			$this->get_pasien_meninggal_kurang_48_jam($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan);
		} else if ($num==6) {
			$this->get_pasien_meninggal_lebih_sm_dgn_48_jam($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan);
		} else if ($num==7) {
			$this->get_pasien_hari_ini($opttgl,$optjam,$optruangan,$valtgl,$valjam,$valruangan);
		}
	}

}
