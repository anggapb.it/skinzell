<?php
/*
  Fungsi Jurnal khusus transaksi farmasi
*/
class Jurn_transaksi_Farmasi_Controller extends Controller {
  public function __construct()
  {
    parent::Controller();
    $this->load->library('session');
  }
  
  /* =========================================== START JURNAL FARMASI RETUR ============================= */
  function posting_jurnal_farmasi_retur(){

    $noreturfarmasi   = $this->input->post("noretur_posting");
    $tglretur         = $this->input->post("tglretur_posting");
    $jenistransaksi   = $this->input->post("jenistransaksi");
    $nonota           = $this->input->post("nonota");
    $nominal_jurnal   = $this->input->post("nominal_jurnal");
    $post_grid        = $this->input->post("post_grid");
    $userid           = $this->input->post("userid");
    $kdjurnal         = $this->get_nojurnal_khusus();
    
    if(empty($noreturfarmasi) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nokuitansi dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglretur,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Retur '.$jenistransaksi.': '.$nonota,
      'noreff' => $noreturfarmasi,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => 9, // 1 = retur farmasi
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //insert jurnal det persediaan
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        if(isset($item_jdet[4]) && isset($item_jdet[5]))
        {
          /*
            item_jdet[0] = idakun   
            item_jdet[1] = kdakun 
            item_jdet[2] = nmakun 
            item_jdet[3] = noreff 
            item_jdet[4] = debit 
            item_jdet[5] = kredit
          */
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;

        }

      }
    }

    if($continue)
      $this->db->trans_complete();
    else
      $this->db->trans_rollback();
  

    if($continue)
    {
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    }else{
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  }  

  function get_transaksi_farmasi_retur(){

    $tglreturfarmasi           = $this->input->post("tglreturfarmasi");
    $tglreturfarmasi_akhir     = $this->input->post("tglreturfarmasi_akhir");
    $pencarian     = $this->input->post("pencarian");
    if(empty($tglreturfarmasi)) 
      $tglreturfarmasi   = date('Y-m-d');
    if(empty($tglreturfarmasi_akhir)) 
      $tglreturfarmasi_akhir= date('Y-m-d');

    $this->db->where('tglreturfarmasi >=', $tglreturfarmasi);
    $this->db->where('tglreturfarmasi <=', $tglreturfarmasi_akhir);
    
    if(!empty($pencarian)){
      $fields = array(
        'noreturfarmasi', 'tglreturfarmasi', 'jamreturfarmasi',
        'biayaadm', 'idsttransaksi', 'idstsetuju', 'userid',
        'idstharga', 'qtyretur', 'total',
        'nonota', 'idjnstransaksi', 'nmjnstransaksi',
        'atasnama', 'noreg', 'kdjurnal', 'status_posting'
      );
      foreach($fields as $field){
        $this->db->or_like($field, $pencarian);
      }
    }

    $this->db->select("*");
    $this->db->from("v_jurn_returfarmasi");
    $q = $this->db->get();  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_farmasi_returdet(){
    $noreturfarmasi     = $this->input->post("noreturfarmasi");
    if(empty($noreturfarmasi)) $nopo = '0';

    $this->db->where('noreturfarmasi', $noreturfarmasi);
    $this->db->select("*");
    $this->db->from("v_jurn_returfarmasidet");
    $q = $this->db->get();
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    $build_array = array ("success"=>true, "results"=>$ttl, "data"=> $data);

    echo json_encode($build_array);
  }

  function get_transaksi_farmasi_retur_jurnaling(){
    $akun_kas = $this->db->get_where('akun', array('kdakun' => '11000'))->row_array();
    $akun_diskon_bhp = $this->db->get_where('akun', array('kdakun' => '41280'))->row_array();
    

    $noreturfarmasi     = $this->input->post("noreturfarmasi");
    if(empty($noreturfarmasi)) $noreturfarmasi = '0';

    $this->db->select("
      sum(total) as total
     , sum(diskonitem) as diskonitem
     , sum(grandtotal) as grandtotal
     , idakun
     , nmakun
     , kdakun
    ");
    $this->db->from("v_jurn_returfarmasidet");
    $this->db->where("noreturfarmasi", $noreturfarmasi);
    $this->db->group_by('idakun');
    $data = $this->db->get()->result_array();

    $total_bayar = 0;
    $total_diskon = 0;
    $breakdown_jurnal = array();    
    if(!empty($data))
    {
      foreach($data as $idx => $dt)
      {
        $total_bayar += $dt['grandtotal']; //total beli sudah dikurangi diskon
        $total_diskon += $dt['diskonitem'];
        
        $breakdown_jurnal[] = array(
          'idakun' => $dt['idakun'], 
          'kdakun' => $dt['kdakun'], 
          'nmakun' => $dt['nmakun'], 
          'noreff' => '',
          'debit' => $dt['total'], 
          'kredit'=> 0
        );
      }

      //tambah akun kas
      $breakdown_jurnal[] = array(
        'idakun' => $akun_kas['idakun'], 
        'kdakun' => $akun_kas['kdakun'], 
        'nmakun' => $akun_kas['nmakun'], 
        'noreff' => '', 
        'debit' => 0, 
        'kredit'=> $total_bayar
      );

      if($total_diskon > 0){
        //tambah akun diskon
        $breakdown_jurnal[] = array(
          'idakun' => $akun_diskon_bhp['idakun'], 
          'kdakun' => $akun_diskon_bhp['kdakun'], 
          'nmakun' => $akun_diskon_bhp['nmakun'], 
          'noreff' => '', 
          'debit' => 0, 
          'kredit'=> $total_diskon
        );        
      }

    }

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);

    echo json_encode($build_array);
  }
  
  /* ============================================= END JURNAL SUPPLIER PEMBELIAN ============================= */


  function get_nojurnal_khusus(){
    //$q = "SELECT getOtoNojurnal(now()) as nm;";
    $q = "SELECT getOtoNoJurnalKhusus(now()) as nm;";
    $query  = $this->db->query($q);
    $nm= ''; 
                
    if ($query->num_rows() != 0)
    {
        $row = $query->row();
        $nm=$row->nm;
    }
    return $nm;
  }

}
