 <?php 
class Laporan_rekammedis extends Controller{
	public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_sensusharian(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$bagian				= $this->input->post("bagian");
		
		$this->db->select("*");
		$this->db->from("v_lapsensusharian");
		$this->db->where('tglmasuk',$tglawal);
		
		/* if($tglawal){
		$this->db->where('`tglmasuk` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		} */
		if($bagian){$this->db->where('idbagian',$bagian);}
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_lapsensusharian');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}

	function get_kartuindeks(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$bagian				= $this->input->post("bagian");
		$this->db->select("*");
		$this->db->from("v_lapri_kartuindeksdokter");
		
		
		if($tglawal){
		$this->db->where('`tglkeluar` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($bagian)$this->db->where('idbagian',$bagian);
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_lapsensusharian');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_penyakitri(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$ruangan 				= $this->input->post("ruangan");
		
		$this->db->select("*");
		$this->db->from("v_penyakitterbanyakri");
	//	$this->db->where("idjnspelayanan",2);
		
		
		if($tglawal){
		$this->db->where('`tglmasuk` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($ruangan){
		$this->db->where("idbagian",$ruangan);
		}else{
		}
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_penyakitterbanyakri');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_penyakitrj(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$ruangan 				= $this->input->post("ruangan");
		
		$this->db->select("*");
		$this->db->from("v_penyakitterbanyakri");
	//	$this->db->where("idjnspelayanan",2);
		
		if($ruangan){
		$this->db->where("idbagian",$ruangan);
		}
		if($tglawal){
		$this->db->where('`tglmasuk` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_penyakitterbanyakri');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_kartuindeksrj(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		$ruangan 				= $this->input->post("ruangan");
		
		$this->db->select("*");
		$this->db->from("v_laprj_kartuindeks");
	//	$this->db->where("idjnspelayanan",2);
		
		if($ruangan){
		$this->db->where("idbagian",$ruangan);
		}
		if($tglawal){
		$this->db->where('`tglmasuk` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_laprj_kartuindeks');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_ruanganri(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$this->db->select("*");
		$this->db->from("bagian");
		$this->db->where("idjnspelayanan",2);
		
		
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('bagian');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function bor1(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal 				= $this->input->post("tglawal");
		$tglakhir 				= $this->input->post("tglakhir");
		
		$q = $this->db->query("SELECT bagian.nmbagian as nmbagian
			 , count(bed.idbed) AS jumlahbed
			 , (
			   SELECT ((((to_days(registrasidet.tglkeluar) - to_days(registrasidet.tglmasuk))) / (count(bed.idbed) * 365))* 100)
			   FROM
				 registrasidet
			   WHERE
				 registrasidet.idbagian = bagian.idbagian
				 and
				 registrasidet.tglmasuk between '".$tglawal."' and '".$tglakhir."'
			   GROUP BY
				 bagian.idbagian
			   ) AS BOR
		FROM
		  bagian
		LEFT JOIN kamar
		ON kamar.idbagian = bagian.idbagian
		LEFT JOIN bed
		ON bed.idkamar = kamar.idkamar
		WHERE
		  bagian.idjnspelayanan = 2
		GROUP BY
		  bagian.idbagian
		");
		
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = count($data);
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
		
	}
	
	function avlos(){
		$fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal 				= $this->input->post("tglawal");
		$tglakhir 				= $this->input->post("tglakhir");
		
	
	$q = $this->db->query("SELECT bagian.nmbagian as nmbagian
			 , count(bed.idbed) AS jumlahbed
			 , (
       SELECT (((((to_days(registrasidet.tglkeluar) - to_days(registrasidet.tglmasuk)) / 365) * 365) /
              ((SELECT count(registrasidet.idstkeluar)
               FROM
                 registrasidet
               left join registrasi on registrasi.noreg = registrasidet.noreg
               WHERE
                 registrasidet.idstkeluar = 1 and registrasi.idjnspelayanan = 2) + (SELECT count(registrasidet.idstkeluar)
                 FROM
					registrasidet
                    left join registrasi on registrasi.noreg = registrasidet.noreg
               WHERE
					registrasidet.idstkeluar = 3 and registrasi.idjnspelayanan = 2))
              ))
       FROM
         registrasidet
       WHERE
         registrasidet.idbagian = bagian.idbagian
         AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."'
         AND registrasidet.idstkeluar = 1
         OR registrasidet.idstkeluar = 3
        
       GROUP BY
         bagian.idbagian
       ) AS avlos
		FROM
		  bagian
		LEFT JOIN kamar
		ON kamar.idbagian = bagian.idbagian
		LEFT JOIN bed
		ON bed.idkamar = kamar.idkamar
		WHERE
		  bagian.idjnspelayanan = 2
		GROUP BY
		  bagian.idbagian
		");
		
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = count($data);
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function bto(){
	$fields				    = $this->input->post("fields");
    $query					= $this->input->post("query");
	$tglawal 				= $this->input->post("tglawal");
	$tglakhir 				= $this->input->post("tglakhir");	
	
	$q = $this->db->query("SELECT bagian.nmbagian AS nmbagian
     , bagian.idbagian AS idbagian
     , count(bed.idbed) AS jumlahbed
    
     , ((SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 3 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."')/((SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 3 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."' ) + (
        SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 2 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."'
	) + (
		SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 3 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."') + (
        SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 1 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."'
	))) as bto
		FROM
		  bagian
		LEFT JOIN kamar
			ON kamar.idbagian = bagian.idbagian
		LEFT JOIN bed
			ON bed.idkamar = kamar.idkamar
		LEFT JOIN registrasidet
			ON registrasidet.idbagian = bagian.idbagian AND registrasidet.idbed = bed.idbed
		WHERE
		  bagian.idjnspelayanan = 2
		GROUP BY
			bagian.idbagian
		");
		
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = count($data);
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function ndr(){
	$fields				    = $this->input->post("fields");
    $query					= $this->input->post("query");
	$tglawal 				= $this->input->post("tglawal");
	$tglakhir 				= $this->input->post("tglakhir");
	
	
	$q = $this->db->query("
			SELECT bagian.nmbagian AS nmbagian
     , bagian.idbagian AS idbagian
     , count(bed.idbed) AS jumlahbed
     
     , ((SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 3 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."')/((SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 3 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."' ) + (
        SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 2 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."'
	) + (
		SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 3 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."') + (
        SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 1 AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."'
	))) as ndr
			FROM
			  bagian
			LEFT JOIN kamar
			ON kamar.idbagian = bagian.idbagian
			LEFT JOIN bed
			ON bed.idkamar = kamar.idkamar
			LEFT JOIN registrasidet
			ON registrasidet.idbagian = bagian.idbagian AND registrasidet.idbed = bed.idbed
			WHERE
			  bagian.idjnspelayanan = 2
			GROUP BY
			  bagian.idbagian
		");
		
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = count($data);
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function gdr(){
	$fields				    = $this->input->post("fields");
    $query					= $this->input->post("query");
	$tglawal 				= $this->input->post("tglawal");
	$tglakhir 				= $this->input->post("tglakhir");
	
		$q = $this->db->query("SELECT bagian.nmbagian AS nmbagian
     , bagian.idbagian AS idbagian
     , count(bed.idbed) AS jumlahbed
     ,((((SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 2
         AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."') + (SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 3  AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."')) / ((SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 1  AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."')+(SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 2  AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."')+(SELECT count(rd.idbagian)
        FROM
          registrasidet rd
        WHERE
          rd.idbagian = bagian.idbagian
          AND rd.`idstkeluar` = 3  AND registrasidet.tglmasuk BETWEEN '".$tglawal."' AND '".$tglakhir."')))*1) as gdr
		FROM
		  bagian
		LEFT JOIN kamar
		ON kamar.idbagian = bagian.idbagian
		LEFT JOIN bed
		ON bed.idkamar = kamar.idkamar
		LEFT JOIN registrasidet
		ON registrasidet.idbagian = bagian.idbagian AND registrasidet.idbed = bed.idbed
		WHERE
		  bagian.idjnspelayanan = 2
		GROUP BY
		  bagian.idbagian
		");
		
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = count($data);
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_lapdaerah(){
	$start 					= $this->input->post("start");
    $limit 					= $this->input->post("limit");
        
    $fields				    = $this->input->post("fields");
    $query					= $this->input->post("query");
	$tglawal 				= $this->input->post("tglawal");
	$tglakhir 				= $this->input->post("tglakhir");
		
		
		$this->db->select("*");
		$this->db->from("v_daerah");
	//	$this->db->groupby("nopo");
		$this->db->where("tglmasuk BETWEEN '".$tglawal."' and '".$tglakhir."'");
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_daerah');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
}
 ?>