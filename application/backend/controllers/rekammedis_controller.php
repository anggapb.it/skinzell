<?php
class Rekammedis_controller extends Controller{
	
	function __construct(){
		parent::Controller();
		$this->load->model(array('Rekammedis'=>'Rekammedis'));
	}
	
	function getPermintaanKartuRM(){
		$tmp=$this->Rekammedis->getMutasiKRM();
		echo json_encode($tmp);		
	}
	
	function getPengeluaranKartuRM(){
		$tmp = $this->Rekammedis->getMutasiKRMKeluar();
		echo json_encode($tmp);
	}
	
	function getBagian(){
		$tmp = $this->Rekammedis->getBagian();
		echo json_encode($tmp);	
	}	
	
	function keluarkanRM(){
		$tmp = $this->Rekammedis->keluarkanRM();
		echo json_encode($tmp);
	}
	
	function terimaRM(){
		$tmp = $this->Rekammedis->terimaRM();
		echo json_encode($tmp);
	}
	
	function keluarkanRMScan(){
		$tmp = $this->Rekammedis->keluarkanRMScan();		
		echo json_encode($tmp);
	}
	
	function terimaRMScann(){
		$tmp = $this->Rekammedis->terimaRMScann();
		echo json_encode($tmp);
	}
	
	function getLokasi(){
		$tmp = $this->Rekammedis->getLokasi();
		echo json_encode($tmp);
	}
	
	function ubahLokasi(){
		$tmp = $this->Rekammedis->ubahLokasi();
		echo json_encode($tmp);
	}
	
	function getHistory(){
		$tmp = $this->Rekammedis->getHistory();
		echo json_encode($tmp);
	}

	function getHistoryRM(){
		$tmp = $this->Rekammedis->getHistoryRM();
		echo json_encode($tmp);
	}
	
	function getStatusBerkasRM(){
		$tmp = $this->Rekammedis->getStatusBerkasRM();
		echo json_encode($tmp);
	}

	function ubahRiwayatRM(){
		$tmp = $this->Rekammedis->ubahRiwayatRM();
		echo json_encode($tmp);
	}

	function getRiwayatPasien(){
		$tmp = $this->Rekammedis->getRiwayatPasien();
		echo json_encode($tmp);
	}
	
	function getDataPermintaanRM(){
		$start = $this->input->post("start");
        $limit = $this->input->post("limit");        
		
		$nmjnspelayanan = $this->input->post("nmjnspelayanan");
		$stpasien = $this->input->post("nmstpasien");
		$nmpasien = $this->input->post("nmpasien");
		$norm = $this->input->post("norm");
		$nmbagian = $this->input->post("nmbagian");
		$lokasi = $this->input->post("kdlokasi");
		
		$this->db->select("trim(LEADING '0' FROM v_datamintaberkasrm.norm) AS normclear, v_datamintaberkasrm.*");
		$this->db->from('v_datamintaberkasrm');
		
		if($lokasi) { $this->db->like('kdlokasi', $lokasi); }
		if($nmjnspelayanan) { $this->db->like('nmjnspelayanan', $nmjnspelayanan); }
		if($stpasien) { $this->db->like('nmstpasien',$stpasien); }
		if($nmpasien) { $this->db->like('nmpasien',$nmpasien); }
		if($norm) { $this->db->like('norm',$norm); }
		if($nmbagian) { $this->db->like('bagiandari',$nmbagian); }
        
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }        

        $q = $this->db->get();
        
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->getDataPermintaanRMCount();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);		
	}
	
	function getDataPermintaanRMCount(){
		$start = $this->input->post("start");
        $limit = $this->input->post("limit");        
		
		$nmjnspelayanan = $this->input->post("nmjnspelayanan");
		$stpasien = $this->input->post("nmstpasien");
		$nmpasien = $this->input->post("nmpasien");
		$norm = $this->input->post("norm");
		$nmbagian = $this->input->post("nmbagian");
		$lokasi = $this->input->post("kdlokasi");
		
		$this->db->select("trim(LEADING '0' FROM v_datamintaberkasrm.norm) AS normclear, v_datamintaberkasrm.*");
		$this->db->from('v_datamintaberkasrm');
		
		if($lokasi) { $this->db->like('kdlokasi', $lokasi); }
		if($nmjnspelayanan) { $this->db->like('nmjnspelayanan', $nmjnspelayanan); }
		if($stpasien) { $this->db->like('nmstpasien',$stpasien); }
		if($nmpasien) { $this->db->like('nmpasien',$nmpasien); }
		if($norm) { $this->db->like('norm',$norm); }
		if($nmbagian) { $this->db->like('bagiandari',$nmbagian); }       

        return $this->db->get()->num_rows();
	}
	
	function updatekeluarRM(){
	
		$norm = $_POST['norm'];		
		$this->db->where('norm',$norm);
		$this->db->set('idstkrm', 2);
		$uppasien = $this->db->update('pasien');
		
		$idregdet = $_POST['idregdet'];
		$data = array(
			'idjnsstkrm' => 2,
			'tglkeluar' => date('Y-m-d'),
			'jamkeluar' => date('H:i:s'),
			'useridkeluar' => $this->session->userdata['user_id']
		);
		
		$this->db->where('idregdet', $idregdet);
		$upmutasi = $this->db->update('mutasikrm', $data);
		
		$res["success"]	= true;
        $res["msg"]	 	= 'Update Data Berhasil';
		
		return $res;
	}
}
