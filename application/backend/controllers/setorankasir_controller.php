<?php

class Setorankasir_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_setorankasir(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
		
		$tgl1                   = $this->input->post("tglawal");
		$tgl2                   = $this->input->post("tglakhir");
        $key 					= $_POST["key"];
		
        /* $this->db->select("*");
        $this->db->from("v_setorankasirv2"); 
		$this->db->order_by("v_setorankasirv2.idsetorankasir DESC"); */
		$q = "SELECT `setorankasir`.`idsetorankasir` AS `idsetorankasir`
					 , `setorankasir`.`tgltransaksi` AS `tgltransaksi`
					 , `setorankasir`.`approval` AS `approval`
					 , `setting`.`nmset` AS `nmset`
					 , `setorankasir`.`catatan` AS `catatan`
					 , `setorankasir`.`userid` AS `userid`
					 , `pengguna`.`nmlengkap` AS `nmlengkap`
					 , `setorankasir`.`tglsetoran` AS `tglsetoran`
					 , `setorankasir`.`jamsetoran` AS `jamsetoran`
					 , `setorankasir`.`idstsetuju` AS `idstsetuju`
					 , `stsetuju`.`nmstsetuju` AS `nmstsetuju`
					 , `setorankasir`.`jmlsetoran` AS `jmlsetoran`
					 , `setorankasir`.`jmlsetoranfisik` AS `jmlsetoranfisik`
					 , (SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `totcarabyrtunai`
						FROM
						  (`kuitansidet` `kd`
						LEFT JOIN `kuitansi` `k`
						ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
						WHERE
						  ((`k`.`tglkuitansi` = `setorankasir`.`tgltransaksi`)
						  AND (`kd`.`idcarabayar` = '1')
						  AND (`k`.`idstkuitansi` = '1'))) AS `totcarabyrtunai`
					 , (SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `totcarabyrtunai`
						FROM
						  (`kuitansidet` `kd`
						LEFT JOIN `kuitansi` `k`
						ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
						WHERE
						  ((`k`.`tglkuitansi` = `setorankasir`.`tgltransaksi`)
						  AND (`kd`.`idcarabayar` = '1')
						  AND (`k`.`idstkuitansi` = '1'))) - `setorankasir`.`jmlsetoranfisik` AS `selisih`
				FROM
				  (((`setorankasir`
				LEFT JOIN `pengguna`
				ON ((`setorankasir`.`userid` = `pengguna`.`userid`)))
				LEFT JOIN `stsetuju`
				ON ((`setorankasir`.`idstsetuju` = `stsetuju`.`idstsetuju`)))
				LEFT JOIN `setting`
				ON ((`setting`.`nilai` = `setorankasir`.`approval`)))
				WHERE
				  (`setting`.`idklpsetting` = '17' AND `setorankasir`.tgltransaksi BETWEEN '". $tgl1 ."' and '". $tgl2."')			  
				  ORDER BY
				  `setorankasir`.idsetorankasir DESC";
		$query = $this->db->query($q);
        
        #$q = $this->db->get();
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_stapproval(){      
        $this->db->select("*");
        $this->db->from("setting");
		$this->db->where("idklpsetting = '17'");
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	/* function get_retfarrawjln(){
		$tgl   = $this->input->post("tglreturfarmasi");
		
		$this->db->select("*");
        $this->db->from("v_totretfarrawjln");
		$this->db->where("v_totretfarrawjln.tglreturfarmasi = '". $tgl ."'");
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    } */
	
	function get_retfarrawjln(){
		$tgl   = $this->input->post("tglreturfarmasi");
		
		$q = ("SELECT `v_returfarmasidet`.`tglreturfarmasi` AS `tglreturfarmasi`
					 , (100 * ceiling((sum(`v_returfarmasidet`.`subtotal`) / 100))) AS `totretfarrawjln`
				FROM
				  `v_returfarmasidet`
				WHERE
				  (`v_returfarmasidet`.`idjnspelayanan` = '1')
				  AND
				  `v_returfarmasidet`.tglreturfarmasi = '".$tgl."'");
		
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_retfarrawri(){
		$tgl   = $this->input->post("tglreturfarmasi");
		
		//$this->db->select("*");
       // $this->db->from("v_totretfarrawri");
		//$this->db->where("`v_totretfarrawri`.`idjnspelayanan` IN (2, 3) AND v_totretfarrawri.tglreturfarmasi = '".$tgl."'");
        
		$q = ("SELECT sum(v_returfarmasidet.subtotal) AS totretfarrawri
				FROM
				  v_returfarmasidet
				WHERE
				  v_returfarmasidet.idjnspelayanan IN(2, 3)
				  AND v_returfarmasidet.tglreturfarmasi = '".$tgl."'");
		
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_retfarluar(){
		$tgl   = $this->input->post("tglreturfarmasi");
		
		//$this->db->select("*");
        //$this->db->from("v_totretfarluar");
		//$this->db->where("v_totretfarluar.tglreturfarmasi = '".$tgl."'");
        
        /* $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array); */
		
		
		//$q = $this->db->get();
		//$totretfarluar = $q->row_array();
		//echo json_encode($totretfarluar);
		
		$q = ("SELECT sum(v_returfarmasidet.subtotal) AS totretfarluar
				FROM
				  v_returfarmasidet
				WHERE
					v_returfarmasidet.idjnspelayanan IS NULL
					AND v_returfarmasidet.tglreturfarmasi = '".$tgl."'");
		
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_retdeposit(){
		$tgl   = $this->input->post("tgltransaksi");
		
		$q = ("SELECT  jurnal.tgltransaksi AS tgltransaksi
				, sum(jurnal.nominal) AS totretdeposit
				FROM
				  jurnal
				WHERE
				  jurnal.idjnstransaksi = '16'
				  AND date(jurnal.tgltransaksi) = '".$tgl."'");	
		
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_farpasluarjumbyr(){
		$tgl   = $this->input->post("tglnota");
		
		$q = ("SELECT `n`.`idjnstransaksi` AS `idjnstransaksi`
					 , `kui`.`nokuitansi` AS `nokuitansi`
					 , `kui`.`tglkuitansi` AS `tglkuitansi`
					 , `kd`.`idcarabayar` AS `idcarabayar`
					 , sum(`kd`.`jumlah`) AS `jmlbayar`
				FROM
				  ((`kuitansidet` `kd`
				LEFT JOIN `kuitansi` `kui`
				ON ((`kui`.`nokuitansi` = `kd`.`nokuitansi`)))
				LEFT JOIN `nota` `n`
				ON ((`n`.`nokuitansi` = `kui`.`nokuitansi`)))
				WHERE
				  `n`.`idjnstransaksi` = '8'
				  AND
				  kui.tglkuitansi = '".$tgl."'
				  AND `kd`.`idcarabayar` = 1
				  AND `kui`.`idstkuitansi` = '1'");
		
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_farpasluardijamin(){
		$tgl   = $this->input->post("tglnota");
		
		$q = ("SELECT `n`.`idjnstransaksi` AS `idjnstransaksi`
					 , `kui`.`nokuitansi` AS `nokuitansi`
					 , `kui`.`tglkuitansi` AS `tglkuitansi`
					 , `kd`.`idcarabayar` AS `idcarabayar`
					 , sum(`kd`.`jumlah`) AS `totaldijamin`
				FROM
				  ((`kuitansidet` `kd`
				LEFT JOIN `kuitansi` `kui`
				ON ((`kui`.`nokuitansi` = `kd`.`nokuitansi`)))
				LEFT JOIN `nota` `n`
				ON ((`n`.`nokuitansi` = `kui`.`nokuitansi`)))
				WHERE
				  `n`.`idjnstransaksi` = '8'
				  AND
				  kui.tglkuitansi = '".$tgl."'
				  AND `kd`.`idcarabayar` <> 1
				  AND `kui`.`idstkuitansi` = '1'");
		
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_totudg(){
		$tgl   = $this->input->post("tglkuitansi");
				
		$q = ("SELECT `kui`.nokuitansi
						 , `kui`.tglkuitansi
						 , sum(`kui`.total) AS totalugd
					FROM
					  `kuitansi` `kui`
					LEFT JOIN nota n
					ON n.nokuitansi = `kui`.nokuitansi
					WHERE
					  `kui`.idjnskuitansi = '4'
					  AND
					  `kui`.tglkuitansi = '".$tgl."'
					  AND
					  n.idregdettransfer IS NULL
					  AND `kui`.`idstkuitansi` = '1'");
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_jmlbyrudg(){
		$tgl   = $this->input->post("tglnota");
		
		$q = ("SELECT v_jmlbyrugd.tglnota AS tglnota
					 , sum(v_jmlbyrugd.totbyrtunai) AS `totbyrtunai`
				FROM
				  v_jmlbyrugd
				WHERE
				  v_jmlbyrugd.idbagian = '12'
				  AND
				  v_jmlbyrugd.tglnota = '".$tgl."'
				  AND
				  v_jmlbyrugd.idregdettransfer IS NULL
				GROUP BY
				  v_jmlbyrugd.tglnota ");
		
		/* $q = ("SELECT `v_jmlbyrugd`.nonota
					 ,`v_jmlbyrugd`.tglnota
					 , v_jmlbyrugd.`tottindakan` + `totobat` AS jmlbayarugd
				FROM
				  v_jmlbyrugd
				WHERE
				  `v_jmlbyrugd`.tglnota = '".$tgl."'"); */
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_totdijamin(){
		$tgl   = $this->input->post("tglnota");
		
		$q = ("SELECT v_jmlbyrugd.tglnota AS tglnota
					 , sum(v_jmlbyrugd.totbyrnontunai) AS totbyrnontunai
				FROM
				  v_jmlbyrugd
				WHERE
				  v_jmlbyrugd.idbagian = '12'
				  AND
				  v_jmlbyrugd.tglnota = '".$tgl."'
				  AND
				  v_jmlbyrugd.idregdettransfer IS NULL
				GROUP BY
				  v_jmlbyrugd.tglnota");
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_totpeltambahanbyrtunai(){
		$tgl   = $this->input->post("tglnota");
		
		$q = ("SELECT `n`.`idjnstransaksi` AS `idjnstransaksi`
					 , `kui`.`nokuitansi` AS `nokuitansi`
					 , `kui`.`tglkuitansi` AS `tglkuitansi`
					 , `kd`.`idcarabayar` AS `idcarabayar`
					 , sum(`kd`.`jumlah`) AS `totbyrtunai`
				FROM
				  ((`kuitansidet` `kd`
				LEFT JOIN `kuitansi` `kui`
				ON ((`kui`.`nokuitansi` = `kd`.`nokuitansi`)))
				LEFT JOIN `nota` `n`
				ON ((`n`.`nokuitansi` = `kui`.`nokuitansi`)))
				WHERE
				  `n`.idbagian = '35'
				  AND
				  kui.tglkuitansi = '".$tgl."'
				  AND `kd`.`idcarabayar` = 1
				  AND `kui`.`idstkuitansi` = '1'");		
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_totpeltambahanbyrnontunai(){
		$tgl   = $this->input->post("tglnota");
		
		$q = ("SELECT `n`.`idjnstransaksi` AS `idjnstransaksi`
					 , `kui`.`nokuitansi` AS `nokuitansi`
					 , `kui`.`tglkuitansi` AS `tglkuitansi`
					 , `kd`.`idcarabayar` AS `idcarabayar`
					 , sum(`kd`.`jumlah`) AS `totbyrnontunai`
				FROM
				  ((`kuitansidet` `kd`
				LEFT JOIN `kuitansi` `kui`
				ON ((`kui`.`nokuitansi` = `kd`.`nokuitansi`)))
				LEFT JOIN `nota` `n`
				ON ((`n`.`nokuitansi` = `kui`.`nokuitansi`)))
				WHERE
				  `n`.idbagian = '35'
				  AND
				  kui.tglkuitansi = '".$tgl."'
				  AND `kd`.`idcarabayar` <> 1
				  AND `kui`.`idstkuitansi` = '1'");		
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_totdafosit(){
		$tgl   = $this->input->post("tglkuitansi");
		
		$q = ("SELECT v_totdafosit.`nokuitansi`
					 , v_totdafosit.tglkuitansi
					 ,(sum(v_totdafosit.totbyrtunai) + sum(v_totdafosit.totbyrnontunai)) AS totdafosit
					 , sum(v_totdafosit.totbyrtunai) AS totbyrtunai
					 , sum(v_totdafosit.totbyrnontunai) AS totbyrnontunai
				FROM
				  v_totdafosit
				WHERE
				  v_totdafosit.tglkuitansi = '".$tgl."'");	
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_totrjunitpelayanan(){
		$tgl   = $this->input->post("tglnota");
		
		$q = ("SELECT `nota`.`tglnota` AS `tglnota`
					 , `nota`.`idbagian` AS `idbagian`
					 , `bag`.`nmbagian` AS `nmbagian`     
					 , sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
							FROM
							  (`kuitansidet` `kd`
							LEFT JOIN `kuitansi` `k`
							ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
							WHERE
							  ((`kd`.`idcarabayar` = '1')
							  AND (`k`.`nokuitansi` = nota.`nokuitansi`)
							  AND (`k`.`tglkuitansi` = nota.`tglnota`)
							  AND (`kui`.`idstkuitansi` <> 2)))) AS `totbyrtunai`
						, sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
							  FROM
								(`kuitansidet` `kd`
							  LEFT JOIN `kuitansi` `k`
							  ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
							  WHERE
								((`kd`.`idcarabayar` <> '1')
								AND (`k`.`nokuitansi` = nota.`nokuitansi`)
								AND (`k`.`tglkuitansi` = nota.`tglnota`)
								AND (`k`.`idstkuitansi` = '1')))) AS `totbyrnontunai`
								
								, (sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
										FROM
										  (`kuitansidet` `kd`
										LEFT JOIN `kuitansi` `k`
										ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
										WHERE
										  ((`kd`.`idcarabayar` = '1')
										  AND (`k`.`nokuitansi` = nota.`nokuitansi`)
										  AND (`k`.`tglkuitansi` = nota.`tglnota`)
										  AND (`kui`.`idstkuitansi` <> 2)))) + sum((SELECT ifnull(sum(`kd`.`jumlah`), 0) AS `carabayar`
																				   FROM
																					 (`kuitansidet` `kd`
																				   LEFT JOIN `kuitansi` `k`
																				   ON ((`k`.`nokuitansi` = `kd`.`nokuitansi`)))
																				   WHERE
																					 ((`kd`.`idcarabayar` <> '1')
																					 AND (`k`.`nokuitansi` = nota.`nokuitansi`)
																					 AND (`k`.`tglkuitansi` = nota.`tglnota`)
																					 AND (`k`.`idstkuitansi` = '1'))))) as total				
				FROM
				  nota
				LEFT JOIN `bagian` `bag`
				ON ((`bag`.`idbagian` = nota.`idbagian`))
				LEFT JOIN `kuitansi` `kui`
				ON ((`kui`.`nokuitansi` = nota.`nokuitansi`))
				WHERE
				  `bag`.idjnspelayanan = '1'
				  AND nota.tglnota = '".$tgl."'
				GROUP BY
				  nota.tglnota
				, nota.idbagian");	
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_totriruangan(){
		$tgl   = $this->input->post("tglkuitansi");
		
		/* $q = ("SELECT v_sumv5.nmbagian AS nmbagian
					 , v_sumv5.tglkuitansi AS tglkuitansi
					 , sum(v_sumv5.total) AS total
					 , sum(v_sumv5.jmlbyar) AS jmlbayar
					 , v_sumv5.dijamin AS dijamin
					 , sum(v_sumv5.`totbyrtunai`) AS `totbyrtunai`
					 , sum(v_sumv5.`totbyrnontunai`) AS `totbyrnontunai`
				FROM
				  v_sumv5_uangrdiskdep_strnksr v_sumv5
				WHERE
				  v_sumv5.idjnspelayanan = '2'
				  AND v_sumv5.tglkuitansi = '".$tgl."'
				GROUP BY
				  v_sumv5.idbagian
				, v_sumv5.tglkuitansi
				ORDER BY
				  v_sumv5.nmbagian");	
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array); */
		/* 
		$this->db->select("
			kuitansi.nokuitansi,
			kuitansi.tglkuitansi,
			registrasi.noreg,
			pasien.norm,
			pasien.nmpasien,
			penjamin.nmpenjamin,
			dokter.nmdoktergelar,
			(
				select tglmasuk
				from registrasidet rd
				where rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS tglmasuk, 
			(
				select nmbagian
				from bagian, registrasidet rd
				where bagian.idbagian = rd.idbagian AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS nmbagian, 
			(
				select rd.noreg
				from nota nt, registrasidet rd
				where nt.idregdettransfer = registrasi.noreg AND
					rd.idregdet = nt.idregdet
			) AS transferugd,
			(
				select (SUM(ntd.tarifjs*ntd.qty)+SUM(ntd.tarifjm*ntd.qty)+SUM(ntd.tarifjp*ntd.qty)+SUM(ntd.tarifbhp*ntd.qty)+nota.uangr-nota.diskon) as tarif
				from nota nt, registrasidet rd, notadet ntd
				where nt.idregdettransfer = registrasi.noreg AND
					rd.idregdet = nt.idregdet AND
					ntd.nonota = nt.nonota
			) AS nominalugd,
			(SUM(notadet.tarifjs*notadet.qty)+SUM(notadet.tarifjm*notadet.qty)+SUM(notadet.tarifjp*notadet.qty)+SUM(notadet.tarifbhp*notadet.qty)+nota.uangr) AS nominalri,
			(SUM(notadet.diskonjs)+SUM(notadet.diskonjm)+SUM(notadet.diskonjp)+SUM(notadet.diskonbhp)+nota.diskon) AS diskon,
			(
				select IF(ISNULL(SUM(kui.total)), 0, SUM(kui.total))
				from kuitansi kui, registrasidet rd
				where kui.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg AND
					kui.idjnskuitansi = 5 AND
					kui.idstkuitansi = 1
			) AS deposit,
			(
				select nt.userinput
				from nota nt, registrasidet rd
				where nt.idregdet = rd.idregdet AND
					rd.noreg = registrasi.noreg
				order by rd.idregdet desc
				limit 1
			) AS userid,
			ifnull((
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar <> 1
			), 0) AS totbyrnontunai,
			ifnull((
				select sum(kdet.jumlah) 
				from kuitansidet kdet
				where kdet.nokuitansi = kuitansi.nokuitansi AND kdet.idcarabayar = 1
			), 0) AS totbyrtunai
		", false);
        $this->db->from("registrasi");
        $this->db->join("registrasidet",
				"registrasidet.noreg = registrasi.noreg", "left");
        $this->db->join("nota",
				"nota.idregdet = registrasidet.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
        $this->db->join("dokter",
				"dokter.iddokter = registrasidet.iddokter", "left");
				
		$this->db->where('registrasi.idjnspelayanan', 2, false);
		$this->db->where("kuitansi.tglkuitansi = '". $tgl ."'");
		$this->db->groupby("registrasi.noreg");	 */
		
		$query = "SELECT kuitansi.nokuitansi
					 , kuitansi.tglkuitansi
					 , registrasi.noreg
					 , pasien.norm
					 , pasien.nmpasien
					 , penjamin.nmpenjamin
					 , dokter.nmdoktergelar
					 , (
					   SELECT tglmasuk
					   FROM
						 registrasidet rd
					   WHERE
						 rd.noreg = registrasi.noreg
					   ORDER BY
						 rd.idregdet DESC
					   LIMIT
						 1
					   ) AS tglmasuk
					 , (
					   SELECT nmbagian
					   FROM
						 bagian, registrasidet rd
					   WHERE
						 bagian.idbagian = rd.idbagian
						 AND
						 rd.noreg = registrasi.noreg
					   ORDER BY
						 rd.idregdet DESC
					   LIMIT
						 1
					   ) AS nmbagian
					 , (
					   SELECT rd.noreg
					   FROM
						 nota nt, registrasidet rd
					   WHERE
						 nt.idregdettransfer = registrasi.noreg
						 AND
						 rd.idregdet = nt.idregdet
					   ) AS transferugd
					 , (
					   SELECT (sum(ntd.tarifjs * ntd.qty) + sum(ntd.tarifjm * ntd.qty) + sum(ntd.tarifjp * ntd.qty) + sum(ntd.tarifbhp * ntd.qty) + nota.uangr - nota.diskon) AS tarif
					   FROM
						 nota nt, registrasidet rd, notadet ntd
					   WHERE
						 nt.idregdettransfer = registrasi.noreg
						 AND
						 rd.idregdet = nt.idregdet
						 AND
						 ntd.nonota = nt.nonota
					   ) AS nominalugd
					 , (sum(notadet.tarifjs * notadet.qty) + sum(notadet.tarifjm * notadet.qty) + sum(notadet.tarifjp * notadet.qty) + sum(notadet.tarifbhp * notadet.qty) + nota.uangr) AS nominalri
					 , (sum(notadet.diskonjs) + sum(notadet.diskonjm) + sum(notadet.diskonjp) + sum(notadet.diskonbhp) + nota.diskon) AS diskon
					 , (
					   SELECT if(isnull(sum(kui.total)), 0, sum(kui.total))
					   FROM
						 kuitansi kui, registrasidet rd
					   WHERE
						 kui.idregdet = rd.idregdet
						 AND
						 rd.noreg = registrasi.noreg
						 AND
						 kui.idjnskuitansi = 5
						 AND
						 kui.idstkuitansi = 1
					   ) AS deposit
					 , (
					   SELECT nt.userinput
					   FROM
						 nota nt, registrasidet rd
					   WHERE
						 nt.idregdet = rd.idregdet
						 AND
						 rd.noreg = registrasi.noreg
					   ORDER BY
						 rd.idregdet DESC
					   LIMIT
						 1
					   ) AS userid
					 , ifnull((
					   SELECT sum(kdet.jumlah)
					   FROM
						 kuitansidet kdet
					   WHERE
						 kdet.nokuitansi = kuitansi.nokuitansi
						 AND kdet.idcarabayar <> 1
					   ), 0) AS totbyrnontunai
					 , ifnull((
					   SELECT sum(kdet.jumlah)
					   FROM
						 kuitansidet kdet
					   WHERE
						 kdet.nokuitansi = kuitansi.nokuitansi
						 AND kdet.idcarabayar = 1
					   ), 0) AS totbyrtunai

				FROM
				  registrasi
				LEFT JOIN registrasidet
				ON registrasidet.noreg = registrasi.noreg
				LEFT JOIN nota
				ON nota.idregdet = registrasidet.idregdet
				LEFT JOIN notadet
				ON notadet.nonota = nota.nonota
				LEFT JOIN kuitansi
				ON kuitansi.nokuitansi = nota.nokuitansi
				LEFT JOIN pasien
				ON pasien.norm = registrasi.norm
				LEFT JOIN penjamin
				ON penjamin.idpenjamin = registrasi.idpenjamin
				LEFT JOIN dokter
				ON dokter.iddokter = registrasidet.iddokter
				WHERE
				  registrasi.idjnspelayanan = '2'
				  AND kuitansi.tglkuitansi = '".$tgl."'
				GROUP BY
				  registrasi.noreg 	";
        
        $q = $this->db->query($query);
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function cek_totriruangan(){
		$tgl   = $this->input->post("tglkuitansi");
		$query = "SELECT kuitansi.nokuitansi
					 , kuitansi.tglkuitansi
					 , registrasi.noreg
					 , pasien.norm
					 , pasien.nmpasien
					 , penjamin.nmpenjamin
					 , dokter.nmdoktergelar
					 , (
					   SELECT tglmasuk
					   FROM
						 registrasidet rd
					   WHERE
						 rd.noreg = registrasi.noreg
					   ORDER BY
						 rd.idregdet DESC
					   LIMIT
						 1
					   ) AS tglmasuk
					 , (
					   SELECT nmbagian
					   FROM
						 bagian, registrasidet rd
					   WHERE
						 bagian.idbagian = rd.idbagian
						 AND
						 rd.noreg = registrasi.noreg
					   ORDER BY
						 rd.idregdet DESC
					   LIMIT
						 1
					   ) AS nmbagian
					 , (
					   SELECT rd.noreg
					   FROM
						 nota nt, registrasidet rd
					   WHERE
						 nt.idregdettransfer = registrasi.noreg
						 AND
						 rd.idregdet = nt.idregdet
					   ) AS transferugd
					 , (
					   SELECT (sum(ntd.tarifjs * ntd.qty) + sum(ntd.tarifjm * ntd.qty) + sum(ntd.tarifjp * ntd.qty) + sum(ntd.tarifbhp * ntd.qty) + nota.uangr - nota.diskon) AS tarif
					   FROM
						 nota nt, registrasidet rd, notadet ntd
					   WHERE
						 nt.idregdettransfer = registrasi.noreg
						 AND
						 rd.idregdet = nt.idregdet
						 AND
						 ntd.nonota = nt.nonota
					   ) AS nominalugd
					 , (sum(notadet.tarifjs * notadet.qty) + sum(notadet.tarifjm * notadet.qty) + sum(notadet.tarifjp * notadet.qty) + sum(notadet.tarifbhp * notadet.qty) + nota.uangr) AS nominalri
					 , (sum(notadet.diskonjs) + sum(notadet.diskonjm) + sum(notadet.diskonjp) + sum(notadet.diskonbhp) + nota.diskon) AS diskon
					 , (
					   SELECT if(isnull(sum(kui.total)), 0, sum(kui.total))
					   FROM
						 kuitansi kui, registrasidet rd
					   WHERE
						 kui.idregdet = rd.idregdet
						 AND
						 rd.noreg = registrasi.noreg
						 AND
						 kui.idjnskuitansi = 5
						 AND
						 kui.idstkuitansi = 1
					   ) AS deposit
					 , (
					   SELECT nt.userinput
					   FROM
						 nota nt, registrasidet rd
					   WHERE
						 nt.idregdet = rd.idregdet
						 AND
						 rd.noreg = registrasi.noreg
					   ORDER BY
						 rd.idregdet DESC
					   LIMIT
						 1
					   ) AS userid
					 , ifnull((
					   SELECT sum(kdet.jumlah)
					   FROM
						 kuitansidet kdet
					   WHERE
						 kdet.nokuitansi = kuitansi.nokuitansi
						 AND kdet.idcarabayar <> 1
					   ), 0) AS totbyrnontunai
					 , ifnull((
					   SELECT sum(kdet.jumlah)
					   FROM
						 kuitansidet kdet
					   WHERE
						 kdet.nokuitansi = kuitansi.nokuitansi
						 AND kdet.idcarabayar = 1
					   ), 0) AS totbyrtunai

				FROM
				  registrasi
				LEFT JOIN registrasidet
				ON registrasidet.noreg = registrasi.noreg
				LEFT JOIN nota
				ON nota.idregdet = registrasidet.idregdet
				LEFT JOIN notadet
				ON notadet.nonota = nota.nonota
				LEFT JOIN kuitansi
				ON kuitansi.nokuitansi = nota.nokuitansi
				LEFT JOIN pasien
				ON pasien.norm = registrasi.norm
				LEFT JOIN penjamin
				ON penjamin.idpenjamin = registrasi.idpenjamin
				LEFT JOIN dokter
				ON dokter.iddokter = registrasidet.iddokter
				WHERE
				  registrasi.idjnspelayanan = '2'
				  AND kuitansi.tglkuitansi = '".$tgl."'
				GROUP BY
				  registrasi.noreg 	";
        
        $q = $this->db->query($query);
        $id = $q->row_array();
		echo json_encode($id);
    }
	
	function get_detdeposit(){
		$tgl   = $this->input->post("tglkuitansi");
		
		/* $this->db->select("tglkuitansi as tglkuitansi
						, noreg as noreg
						, norm as norm
						, nmpasien as nmpasien
						, nmbagian as nmbagian
						, total as total
						, ifnull(nontunai, 0) as nontunai
						, ifnull(tunai, 0) as tunai", false);
        $this->db->from("v_lapdeposit");		
		$this->db->where("v_lapdeposit.tglkuitansi = '". $tgl ."'"); */
		
		$query = "SELECT `k`.`tglkuitansi` AS `tglkuitansi`
						 , `r`.`noreg` AS `noreg`
						 , `p`.`norm` AS `norm`
						 , `p`.`nmpasien` AS `nmpasien`
						 , `b`.`nmbagian` AS `nmbagian`
						 , sum(`k`.`total`) AS `total`
						 , ifnull((SELECT sum(`kdet`.`jumlah`) AS `sum(kdet.jumlah)`
							FROM
							  `kuitansidet` `kdet`
							WHERE
							  ((`kdet`.`nokuitansi` = `k`.`nokuitansi`)
							  AND (`kdet`.`idcarabayar` <> 1))), 0) AS `nontunai`
						 , ifnull((SELECT sum(`kdet`.`jumlah`) AS `sum(kdet.jumlah)`
							FROM
							  `kuitansidet` `kdet`
							WHERE
							  ((`kdet`.`nokuitansi` = `k`.`nokuitansi`)
							  AND (`kdet`.`idcarabayar` = 1))), 0) AS `tunai`
					FROM
					  ((((((`kuitansi` `k`
					LEFT JOIN `kuitansidet` `kd`
					ON ((`kd`.`nokuitansi` = `k`.`nokuitansi`)))
					LEFT JOIN `registrasidet` `rd`
					ON ((`rd`.`idregdet` = `k`.`idregdet`)))
					LEFT JOIN `registrasi` `r`
					ON ((`r`.`noreg` = `rd`.`noreg`)))
					LEFT JOIN `pasien` `p`
					ON ((`p`.`norm` = `r`.`norm`)))
					LEFT JOIN `dokter` `d`
					ON ((`d`.`iddokter` = `rd`.`iddokter`)))
					LEFT JOIN `bagian` `b`
					ON ((`b`.`idbagian` = `rd`.`idbagian`)))
					WHERE
					  ((`k`.`idregdet` IS NOT NULL)
					  AND (`k`.`idstkuitansi` = 1)
					  AND k.tglkuitansi = '". $tgl ."')
					GROUP BY
					  `k`.`nokuitansi`
					ORDER BY
					  `k`.`nokuitansi`";
        
        //$q = $this->db->get();
        $q = $this->db->query($query);
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_carabyrtunai(){
		$tgl   = $this->input->post("tglkuitansi");
		
		$q = ("SELECT kui.tglkuitansi AS tglkuitansi
						 , cb.idcarabayar AS idcarabayar
						 , cb.nmcarabayar AS nmcarabayar
						 , (SELECT ifnull(sum(kd.jumlah), 0) AS carabayar
							FROM
							  kuitansidet kd
							LEFT JOIN kuitansi k
							ON k.nokuitansi = kd.nokuitansi
							WHERE
							  kd.idcarabayar = kdt.idcarabayar
							  AND
							  k.tglkuitansi = '".$tgl."'
							  AND k.idstkuitansi = '1'
						   ) AS totcarabyr
						 , (SELECT ifnull(sum(kd.jumlah), 0) AS carabayar
							FROM
							  kuitansidet kd
							LEFT JOIN kuitansi k
							ON k.nokuitansi = kd.nokuitansi
							WHERE
							  kd.idcarabayar = kdt.idcarabayar
							  AND
							  k.tglkuitansi = '".$tgl."'
							  AND
							  kd.idcarabayar = '1'
							  AND k.idstkuitansi = '1'
						   ) AS totcarabyrtunai
					FROM
					  carabayar cb
					LEFT JOIN kuitansidet kdt
					ON kdt.idcarabayar = cb.idcarabayar
					LEFT JOIN kuitansi kui
					ON kui.nokuitansi = kdt.nokuitansi
					WHERE
						cb.idcarabayar <> '9'
					GROUP BY
					  cb.idcarabayar");	
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function delete_setorankasir(){     
		$where['idsetorankasir'] = $_POST['idsetoranksr'];
		$del = $this->rhlib->deleteRecord('setorankasir',$where);
        return $del;
    }
		
	function insert_setorankasir(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('setorankasir',$dataArray);
        return $ret;
    }

	function update_setorankasir(){
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('idsetorankasir', $_POST['idsetoranksr']);
		$this->db->update('setorankasir', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
		$dataArray = array(
             'idsetorankasir'	=> $_POST['idsetoranksr'],
             'tgltransaksi'		=> $_POST['tgltransaksi'],
			 'approval'			=> $_POST['approval'],
			 'tglsetoran'		=> $_POST['tglsetoran'],
			 'jamsetoran'		=> $_POST['jamsetoran'],
			 'userid'			=> $_POST['userid'],
			 'idstsetuju'		=> $_POST['idstsetuju'],
			 'catatan'			=> $_POST['catatan'],
			 'jmlsetoran'		=> $_POST['jmlsetoran'],
			 'jmlsetoranfisik'	=> $_POST['jmlsetoranfisik']
        );		
		return $dataArray;
	}
}
