<?php
/*
  Saldo Awal Akun per tahun = saldoawal akun tahun + (total debit awal tahun hingga tglawal) - (total kredit)
*/

class Bukubesar_controller extends Controller {
  var $bulan = array();
  var $bulanSaldoAwal = array();
  var $bulanSaldoDebit = array();
  var $bulanSaldoCredit = array();
  var $bulanSaldoAkhir = array();

  public function __construct()
  {
    parent::Controller();
    $this->load->library('pdf_lap');
    $this->load->library('session');
    $this->load->library('lib_phpexcel');
    $this->load->model(array('bulanaktif' => 'Bulanaktif'));
    //$this->load->library('rhlib');
    
    $this->bulan = array(
      '01'  => 'Januari',    '02'  => 'Februari',   '03'  => 'Maret',
      '04'  => 'April',      '05'  => 'Mei',        '06'  => 'Juni',
      '07'  => 'Juli',       '08'  => 'Agustus',    '09'  => 'September',
      '10'  => 'Oktober',    '11'  => 'November',   '12'  => 'Desember',
    );

  }
  
  function get_bukubesar(){
    $tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    //$bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    
    if(empty($tahun) || empty($bulan)){
      $build_array = array ("success"=>true,"results"=>0,"data"=>array());
      echo json_encode($build_array);
      die();
    }
    
    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun))->result();
    if(!empty($data_akun_tahun))
    {
      foreach($data_akun_tahun as $idx => $akun_tahun)
      {
        if(empty($bulan)){
          if(is_null($akun_tahun->saldoawaljan))
            $data_akun_tahun[$idx]->calc_saldoawal = $akun_tahun->saldoawal;
          else
            $data_akun_tahun[$idx]->calc_saldoawal = $akun_tahun->saldoawaljan;

        }else{
          //bulan & tahun awal penjurnalan
          $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
          $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

          if(is_null($akun_tahun->$fieldbulanSaldoAwal) && $bulan == $bulanawal && $tahun == $tahunawal){
            $data_akun_tahun[$idx]->calc_saldoawal = $akun_tahun->saldoawal;
          }else{
            $data_akun_tahun[$idx]->calc_saldoawal = $akun_tahun->$fieldbulanSaldoAwal;
          }

        }

        /*
          @menghitung total transaksi debit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoDebit)){
         $data_akun_tahun[$idx]->totaldebit = $this->count_total_debit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $data_akun_tahun[$idx]->totaldebit = $akun_tahun->$fieldbulanSaldoDebit;
        }
        
        /*
          @menghitung total transaksi kredit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoCredit)){
          $data_akun_tahun[$idx]->totalkredit = $this->count_total_credit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $data_akun_tahun[$idx]->totalkredit = $akun_tahun->$fieldbulanSaldoCredit;
        }


        /*
          @menghitung total saldo akhir
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoAkhir)){
          $data_akun_tahun[$idx]->saldoakhir = $data_akun_tahun[$idx]->calc_saldoawal + 
                                               $data_akun_tahun[$idx]->totaldebit -
                                               $data_akun_tahun[$idx]->totalkredit;
        }else{
          $data_akun_tahun[$idx]->saldoakhir = $akun_tahun->$fieldbulanSaldoAkhir;
        }

      }
    }

    $build_array = array ("success"=>true,"results"=> count($data_akun_tahun),"data"=>$data_akun_tahun);
    echo json_encode($build_array);  
  }

  function get_bukujurnal(){
    $tahun    = $this->input->post("tahun");
    $bulan    = $this->input->post("bulan");
    $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    $idakun   = $this->input->post("idakun");

    if(empty($tahun) || empty($bulan) || empty($idakun)){
      $build_array = array ("success"=>true,"results"=>0,"data"=>array());
      echo json_encode($build_array);
      die();
    }

    $this->db->select('*');
    $this->db->from('v_bukujurnal');
    $this->db->orderby('tgljurnal', 'ASC');
    $this->db->where('status_posting', '1');
    $this->db->where('idakun', $idakun);
    $this->db->where('tahun', $tahun);
    if($bulan != '00')
      $this->db->where('bulan', $bulan);
      
    $query = $this->db->get();

    $data = $query->result();
    $total = $query->num_rows();

    $build_array = array ("success"=>true,"results"=> $total,"data"=>$data);
    echo json_encode($build_array);  
  }

  function count_total_debit($idakun = '', $tahun = '', $bulan = '')
  {
    $qbulan = ($bulan != '00') ? " jurnaldet.bulan = '".$bulan."' AND " : '';
    $get_calc_debit  = $this->db->query("
                       SELECT ifnull(sum(jurnaldet.debit), 0) as totaldebit
                       FROM
                         jurnaldet, jurnal
                       WHERE
                         jurnal.kdjurnal = jurnaldet.kdjurnal AND
                         jurnaldet.idakun = '". $idakun ."' AND
                         jurnaldet.tahun = '".$tahun."' AND
                         ".$qbulan."              
                         jurnal.status_posting = '1'
                      ")->row();
    $return = $get_calc_debit->totaldebit;
    return $return;
  }


  function count_total_credit($idakun = '', $tahun = '', $bulan = '')
  {
    $qbulan = ($bulan != '00') ? " jurnaldet.bulan = '".$bulan."' AND " : '';
    $get_calc_credit  = $this->db->query("
                       SELECT ifnull(sum(jurnaldet.kredit), 0) as totalkredit
                       FROM
                         jurnaldet, jurnal
                       WHERE
                         jurnal.kdjurnal = jurnaldet.kdjurnal AND
                         jurnaldet.idakun = '". $idakun ."' AND
                         jurnaldet.tahun = '".$tahun."' AND
                         ".$qbulan."              
                         jurnal.status_posting = '1'
                      ")->row();
    
    $return = $get_calc_credit->totalkredit;
    return $return;
  }

  /* ========================== LAPORAN PDF ========================== */
  
  function lap_buku_besar_pdf($tahun = '', $bulan = ''){
    $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    $fieldbulanSaldoAwal = ($bulan != '00') ? $this->bulanSaldoAwal[$bulan] : '';
    $fieldbulanSaldoDebit = ($bulan != '00') ? $this->bulanSaldoDebit[$bulan] : '';
    $fieldbulanSaldoCredit = ($bulan != '00') ? $this->bulanSaldoCredit[$bulan] : '';
    $fieldbulanSaldoAkhir = ($bulan != '00') ? $this->bulanSaldoAkhir[$bulan] : '';

    $this->load->library('pdf_lap');
    $isi = '<br/><font size=\"7\" face=\"Helvetica\">';       
    
    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun))->result();
        
    // add a page
    $page_format = array(
      'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      'Rotate' => 0,
      'PZ' => 1,
    );
    
    //Set Footer
    $this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
    $this->pdf_lap->setPrintFooter(true); // enabled ? true
    $this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $this->pdf_lap->SetAutoPageBreak(TRUE, '15');
    
    $this->pdf_lap->SetPrintHeader(false);
    $this->pdf_lap->AddPage('L', $page_format, false, false);
    $this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
    $this->pdf_lap->SetFont('helvetica', '', 14);
    
    $x=0;$y=10;
    $this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
    $this->pdf_lap->Cell(0, 0, 'Buku Besar', 0, 1, 'C', 0, '', 0);

    $this->pdf_lap->SetFont('helvetica', '', 10);
    if($bulan != '00'){
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $this->bulan[$bulan] .' '. $tahun, 0, 1, 'C', 0, '', 0);
    }else{
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $tahun, 0, 1, 'C', 0, '', 0);  
    }
    
    $no = 1;
    $isi .='
        <table border="1">
        <tr>
          <td align="center" width="15%"><strong>Kode Akun</strong></td>
          <td align="center" width="25%"><strong>Nama Akun</strong></td>
          <td align="center" width="15%"><strong>Saldo Awal</strong></td>
          <td align="center" width="15%"><strong>Total Debit</strong></td>
          <td align="center" width="15%"><strong>Total Kredit</strong></td>
          <td align="center" width="15%"><strong>Saldo Akhir</strong></td>
        </tr>';

    if(!empty($data_akun_tahun)){
      foreach($data_akun_tahun as $idx => $akun_tahun){
        
        if($bulan == '00'){
          if(is_null($akun_tahun->saldoawaljan))
            $calc_saldoawal = $akun_tahun->saldoawal;
          else
            $calc_saldoawal = $akun_tahun->saldoawaljan;

        }else{
          //bulan & tahun awal penjurnalan
          $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
          $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

          if(is_null($akun_tahun->$fieldbulanSaldoAwal) && $bulan == $bulanawal && $tahun == $tahunawal){
            $calc_saldoawal = $akun_tahun->saldoawal;
          }else{
            $calc_saldoawal = $akun_tahun->$fieldbulanSaldoAwal;
          }

        }

        /*
          @menghitung total transaksi debit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoDebit)){
          $totaldebit = $this->count_total_debit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $totaldebit = $akun_tahun->$fieldbulanSaldoDebit;
        }
        
        /*
          @menghitung total transaksi kredit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoCredit)){
          $totalkredit = $this->count_total_credit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $totalkredit = $akun_tahun->$fieldbulanSaldoCredit;
        }


        /*
          @menghitung total saldo akhir
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoAkhir)){
          $saldoakhir = $calc_saldoawal + $totaldebit - $totalkredit;
        }else{
          $saldoakhir = $akun_tahun->$fieldbulanSaldoAkhir;
        }

        $isi .='
        <tr>
          <td align="center">&nbsp; '. $akun_tahun->kdakun .' </td>
          <td align="left">&nbsp; '. $akun_tahun->nmakun .' </td>
          <td align="right">&nbsp; Rp.'. number_format($calc_saldoawal,2,',','.') .' &nbsp;</td>
          <td align="right">&nbsp; Rp.'. number_format($totaldebit,2,',','.') .' &nbsp;</td>
          <td align="right">&nbsp; Rp.'. number_format($totalkredit,2,',','.') .' &nbsp;</td>
          <td align="right">&nbsp; Rp.'. number_format($saldoakhir,2,',','.') .' &nbsp;</td>
        </tr>
        ';      
      }
    }

    $isi .= "</table></font>";
    $this->pdf_lap->writeHTML($isi,true,false,false,false);

    //Close and output PDF document
    $this->pdf_lap->Output('lap_buku_besar_'. $tahun .'_'. (@$this->bulan[$bulan]) .'.pdf', 'I');
  }

  function lap_buku_besar_detail_pdf($tahun = '', $bulan = ''){
    $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    $fieldbulanSaldoAwal = ($bulan != '00') ? $this->bulanSaldoAwal[$bulan] : '';
    $fieldbulanSaldoDebit = ($bulan != '00') ? $this->bulanSaldoDebit[$bulan] : '';
    $fieldbulanSaldoCredit = ($bulan != '00') ? $this->bulanSaldoCredit[$bulan] : '';
    $fieldbulanSaldoAkhir = ($bulan != '00') ? $this->bulanSaldoAkhir[$bulan] : '';

    $this->load->library('pdf_lap');
    $isi = '<br/><font size=\"7\" face=\"Helvetica\">';       
    
    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun))->result();
        
    // add a page
    $page_format = array(
      'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      'Rotate' => 0,
      'PZ' => 1,
    );
    
    //Set Footer
    $this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
    $this->pdf_lap->setPrintFooter(true); // enabled ? true
    $this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $this->pdf_lap->SetAutoPageBreak(TRUE, '15');
    
    $this->pdf_lap->SetPrintHeader(false);
    $this->pdf_lap->AddPage('L', $page_format, false, false);
    $this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
    $this->pdf_lap->SetFont('helvetica', '', 14);
    
    $x=0;$y=10;
    $this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
    $this->pdf_lap->Cell(0, 0, 'Buku Besar', 0, 1, 'C', 0, '', 0);

    $this->pdf_lap->SetFont('helvetica', '', 10);
    if($bulan != '00'){
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $this->bulan[$bulan] .' '.  $tahun, 0, 1, 'C', 0, '', 0);
    }else{
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $tahun, 0, 1, 'C', 0, '', 0);  
    }
    
    $no = 1;
    
    if(!empty($data_akun_tahun)){
      foreach($data_akun_tahun as $idx => $akun_tahun){
        
        $idakun  = $akun_tahun->idakun;   
        //get saldo awal
        if($bulan == '00'){
          if(is_null($akun_tahun->saldoawaljan))
            $calc_saldoawal = $akun_tahun->saldoawal;
          else
            $calc_saldoawal = $akun_tahun->saldoawaljan;

        }else{
          //bulan & tahun awal penjurnalan
          $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
          $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

          if(is_null($akun_tahun->$fieldbulanSaldoAwal) && $bulan == $bulanawal && $tahun == $tahunawal){
            $calc_saldoawal = $akun_tahun->saldoawal;
          }else{
            $calc_saldoawal = $akun_tahun->$fieldbulanSaldoAwal;
          }

        }
        
        //get detail jurnal
        $this->db->select('*');
        $this->db->from('v_bukujurnal');
        $this->db->orderby('tgljurnal', 'ASC');
        $this->db->where('idakun', $idakun);
        $this->db->where('status_posting', '1');
        $this->db->where('tahun', $tahun);
        if($bulan != '00')
         $this->db->where('bulan', $bulan);
      
        $query = $this->db->get();

        $data = $query->result();
        $total = $query->num_rows();

        $total_debit = 0;
        $total_kredit = 0;
        $content = '';
        $content .= '
          <table width="100%" border="1">
            <tr>
              <td width="10%" align="center"><strong>Kode Jurnal</strong></td>
              <td width="10%" align="center"><strong>Tanggal<br>Transaksi</strong></td>
              <td width="10%;" align="center"><strong>Tanggal<br>Jurnal</strong></td>     
              <td width="15%" align="center"><strong>No. Reff / No. Bon</strong></td>
              <td width="30%" align="center"><strong>Keterangan</strong></td>
              <td width="12%" align="center"><strong>Debit</strong></td>
              <td width="12%;" align="center"><strong>Kredit</strong></td>
            </tr>
          ';
        if(!empty($data))
        {
          
          foreach($data as $idx => $dt){
            $total_debit += $dt->debit;
            $total_kredit += $dt->kredit; 

            $content .= '
            <tr>
              <td align="center"> &nbsp;'. $dt->kdjurnal .' &nbsp;</td>
              <td align="center"> &nbsp;'. date("d/m/Y", strtotime($dt->tgltransaksi)) .' &nbsp;</td>
              <td align="center"> &nbsp;'. date("d/m/Y", strtotime($dt->tgljurnal)) .' &nbsp;</td>     
              <td align="center"> &nbsp;'. $dt->noreff_jurnaldet .' &nbsp;</td>
              <td align="left"> &nbsp;'. $dt->keterangan .' &nbsp;</td>
              <td align="right"> &nbsp;Rp.'. number_format($dt->debit,2,',','.') .' &nbsp;</td>
              <td align="right"> &nbsp;Rp.'. number_format($dt->kredit,2,',','.') .' &nbsp;</td>
            </tr>';
          }

        }
        $content .='
          <tr>
            <td align="right" colspan="5"> &nbsp;<strong>Total </strong>&nbsp;</td>
            <td align="right"> &nbsp;<strong>Rp.'. number_format($total_debit,2,',','.') .'</strong> &nbsp;</td>
            <td align="right"> &nbsp;<strong>Rp.'. number_format($total_kredit,2,',','.') .'</strong> &nbsp;</td>
          </tr>
        </table>';

        $saldoakhir = $calc_saldoawal + $total_debit - $total_kredit;
        $isi .= '
          <br><br><br>
          <table>
            <tr>
              <td width="15%">Akun</td>
              <td width="10px">:</td>
              <td><strong>'. $akun_tahun->nmakun .' ('. $akun_tahun->kdakun .')</strong></td>
            </tr>
            <tr>
              <td>Saldo Awal</td>
              <td>:</td>
              <td><strong>Rp.'. number_format($calc_saldoawal,2,',','.') .'</strong></td>
            </tr>
            <tr>
              <td>Saldo Akhir</td>
              <td>:</td>
              <td><strong>Rp.'. number_format($saldoakhir,2,',','.') .'</strong></td>
            </tr>
          </table>
          <br/>
        '.$content;

      }
    }

    $isi .= "</font>";
    $this->pdf_lap->writeHTML($isi,true,false,false,false);

    //Close and output PDF document
    $this->pdf_lap->Output('lap_buku_besar_detail_'. $tahun .'_'. (@$this->bulan[$bulan]) .'.pdf', 'I');
  }

  function lap_buku_jurnal_pdf($tahun = '', $bulan = '00', $idakun = ''){
    $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    $fieldbulanSaldoAwal = ($bulan != '00') ? $this->bulanSaldoAwal[$bulan] : '';
    $fieldbulanSaldoDebit = ($bulan != '00') ? $this->bulanSaldoDebit[$bulan] : '';
    $fieldbulanSaldoCredit = ($bulan != '00') ? $this->bulanSaldoCredit[$bulan] : '';
    $fieldbulanSaldoAkhir = ($bulan != '00') ? $this->bulanSaldoAkhir[$bulan] : '';
    //bulan & tahun awal penjurnalan
    $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
    $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

    $this->load->library('pdf_lap');
    $isi = '<br/><font size=\"7\" face=\"Helvetica\">';       

    // add a page
    $page_format = array(
      'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      'Rotate' => 0,
      'PZ' => 1,
    );
    
    //Set Footer
    $this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
    $this->pdf_lap->setPrintFooter(true); // enabled ? true
    $this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $this->pdf_lap->SetAutoPageBreak(TRUE, '15');
    
    $this->pdf_lap->SetPrintHeader(false);
    $this->pdf_lap->AddPage('L', $page_format, false, false);
    $this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
    $this->pdf_lap->SetFont('helvetica', '', 14);
    
    $x=0;$y=10;
    $this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
    $this->pdf_lap->Cell(0, 0, 'Detail Buku Besar', 0, 1, 'C', 0, '', 0);
    
    $this->pdf_lap->SetFont('helvetica', '', 10);
    if($bulan != '00'){
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $this->bulan[$bulan] .' '. $tahun, 0, 1, 'C', 0, '', 0);
    }else{
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $tahun, 0, 1, 'C', 0, '', 0);  
    }
    
    //get all akun tahun
    $akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun, 'idakun' => $idakun))->row();
    
    //get saldo awal
    if($bulan == '00'){
      if(is_null($akun_tahun->saldoawaljan))
        $calc_saldoawal = $akun_tahun->saldoawal;
      else
        $calc_saldoawal = $akun_tahun->saldoawaljan;

    }else{
      //bulan & tahun awal penjurnalan
      $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
      $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

      if(is_null($akun_tahun->$fieldbulanSaldoAwal) && $bulan == $bulanawal && $tahun == $tahunawal){
        $calc_saldoawal = $akun_tahun->saldoawal;
      }else{
        $calc_saldoawal = $akun_tahun->$fieldbulanSaldoAwal;
      }

    }
    
    //get detail jurnal
    $this->db->select('*');
    $this->db->from('v_bukujurnal');
    $this->db->orderby('tgljurnal', 'ASC');
    $this->db->where('idakun', $idakun);
    $this->db->where('status_posting', '1');
    $this->db->where('tahun', $tahun);
    if($bulan != '00')
     $this->db->where('bulan', $bulan);
  
    $query = $this->db->get();

    $data = $query->result();
    $total = $query->num_rows();

    $total_debit = 0;
    $total_kredit = 0;
    $content = '';
    if(!empty($data))
    {
      $content .= '
      <table width="100%" border="1">
        <tr>
          <td width="10%" align="center"><strong>Kode Jurnal</strong></td>
          <td width="10%" align="center"><strong>Tanggal<br>Transaksi</strong></td>
          <td width="10%;" align="center"><strong>Tanggal<br>Jurnal</strong></td>     
          <td width="15%" align="center"><strong>No. Reff / No. Bon</strong></td>
          <td width="30%" align="center"><strong>Keterangan</strong></td>
          <td width="12%" align="center"><strong>Debit</strong></td>
          <td width="12%;" align="center"><strong>Kredit</strong></td>
        </tr>
      ';

      foreach($data as $idx => $dt){
        $total_debit += $dt->debit;
        $total_kredit += $dt->kredit; 

        $content .= '
        <tr>
          <td align="center"> &nbsp;'. $dt->kdjurnal .' &nbsp;</td>
          <td align="center"> &nbsp;'. date("d/m/Y", strtotime($dt->tgltransaksi)) .' &nbsp;</td>
          <td align="center"> &nbsp;'. date("d/m/Y", strtotime($dt->tgljurnal)) .' &nbsp;</td>     
          <td align="center"> &nbsp;'. $dt->noreff_jurnaldet .' &nbsp;</td>
          <td align="left"> &nbsp;'. $dt->keterangan .' &nbsp;</td>
          <td align="right"> &nbsp;Rp.'. number_format($dt->debit,2,',','.') .' &nbsp;</td>
          <td align="right"> &nbsp;Rp.'. number_format($dt->kredit,2,',','.') .' &nbsp;</td>
        </tr>';
      }

      $content .='
        <tr>
          <td align="right" colspan="5"> &nbsp;<strong>Total </strong>&nbsp;</td>
          <td align="right"> &nbsp;<strong>Rp.'. number_format($total_debit,2,',','.') .'</strong> &nbsp;</td>
          <td align="right"> &nbsp;<strong>Rp.'. number_format($total_kredit,2,',','.') .'</strong> &nbsp;</td>
        </tr>
        </table>';
    }

    $saldoakhir = $calc_saldoawal + $total_debit - $total_kredit;
    $isi .= '
      <br><br>
      <table>
        <tr>
          <td width="15%">Akun</td>
          <td width="10px">:</td>
          <td><strong>'. $akun_tahun->nmakun .' ('. $akun_tahun->kdakun .')</strong></td>
        </tr>
        <tr>
          <td>Saldo Awal</td>
          <td>:</td>
          <td><strong>Rp.'. number_format($calc_saldoawal,2,',','.') .'</strong></td>
        </tr>
        <tr>
          <td>Saldo Akhir</td>
          <td>:</td>
          <td><strong>Rp.'. number_format($saldoakhir,2,',','.') .'</strong></td>
        </tr>
      </table>
      <br/><br/>
    '.$content;

    $isi .= "</font>";


    $this->pdf_lap->writeHTML($isi,true,false,false,false);

    //Close and output PDF document
    $this->pdf_lap->Output('lap_detail_buku_besar.pdf', 'I');
  }


  /* ========================== LAPORAN EXCEL ========================== */

  function lap_buku_besar_excel($tahun = '', $bulan = '')
  {
    $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    $fieldbulanSaldoAwal = ($bulan != '00') ? $this->bulanSaldoAwal[$bulan] : '';
    $fieldbulanSaldoDebit = ($bulan != '00') ? $this->bulanSaldoDebit[$bulan] : '';
    $fieldbulanSaldoCredit = ($bulan != '00') ? $this->bulanSaldoCredit[$bulan] : '';
    $fieldbulanSaldoAkhir = ($bulan != '00') ? $this->bulanSaldoAkhir[$bulan] : '';
    $subject_file = ($bulan != '00') ? 'Periode : '. $this->bulan[$bulan] .' '. $tahun : 'Periode : '. $tahun;
    
    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun))->result();
        
    
    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
        'font'  => array('bold'  => true)
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Buku Besar")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Buku Besar'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:G1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Buku Besar');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:G2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(38);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);

    //set header grid
    $this->lib_phpexcel->getActiveSheet()->getStyle("B4:G4")->applyFromArray($headergrid);
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B4", 'Kode Akun')
         ->setCellValue("C4", 'Nama Akun')
         ->setCellValue("D4", 'Saldo Awal')
         ->setCellValue("E4", 'Total Debit')
         ->setCellValue("F4", 'Total Kredit')
         ->setCellValue("G4", 'Saldo Akhir');
    

    if(!empty($data_akun_tahun)){
      $active_row = 5;
      foreach($data_akun_tahun as $idx => $akun_tahun){
        
        if($bulan == '00'){
          if(is_null($akun_tahun->saldoawaljan))
            $calc_saldoawal = $akun_tahun->saldoawal;
          else
            $calc_saldoawal = $akun_tahun->saldoawaljan;

        }else{
          //bulan & tahun awal penjurnalan
          $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
          $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

          if(is_null($akun_tahun->$fieldbulanSaldoAwal) && $bulan == $bulanawal && $tahun == $tahunawal){
            $calc_saldoawal = $akun_tahun->saldoawal;
          }else{
            $calc_saldoawal = $akun_tahun->$fieldbulanSaldoAwal;
          }

        }

        /*
          @menghitung total transaksi debit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoDebit)){
          $totaldebit = $this->count_total_debit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $totaldebit = $akun_tahun->$fieldbulanSaldoDebit;
        }
        
        /*
          @menghitung total transaksi kredit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoCredit)){
          $totalkredit = $this->count_total_credit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $totalkredit = $akun_tahun->$fieldbulanSaldoCredit;
        }


        /*
          @menghitung total saldo akhir
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoAkhir)){
          $saldoakhir = $calc_saldoawal + $totaldebit - $totalkredit;
        }else{
          $saldoakhir = $akun_tahun->$fieldbulanSaldoAkhir;
        }

        //set content grid
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:G{$active_row}")->applyFromArray($contentgrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", $akun_tahun->kdakun)
             ->setCellValue("C{$active_row}", $akun_tahun->nmakun)
             ->setCellValue("D{$active_row}", $calc_saldoawal)
             ->setCellValue("E{$active_row}", $totaldebit)
             ->setCellValue("F{$active_row}", $totalkredit)
             ->setCellValue("G{$active_row}", $saldoakhir);

        $active_row += 1;
      }
    }


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'lap_buku_besar_'. $tahun .'_'. (@$this->bulan[$bulan]);

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

  function lap_buku_besar_detail_excel($tahun = '', $bulan = ''){
    $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    $fieldbulanSaldoAwal = ($bulan != '00') ? $this->bulanSaldoAwal[$bulan] : '';
    $fieldbulanSaldoDebit = ($bulan != '00') ? $this->bulanSaldoDebit[$bulan] : '';
    $fieldbulanSaldoCredit = ($bulan != '00') ? $this->bulanSaldoCredit[$bulan] : '';
    $fieldbulanSaldoAkhir = ($bulan != '00') ? $this->bulanSaldoAkhir[$bulan] : '';

    $subject_file = ($bulan != '00') ? 'Periode : '. $this->bulan[$bulan] .' '.  $tahun : 'Periode : '. $tahun;

    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun))->result();

    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
        'font'  => array('bold'  => true)
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    $summarygrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT),
        'font'  => array('bold'  => true)
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Buku Besar")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Buku Besar'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:H1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Buku Besar');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:H2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);

    $active_row = 4;
    if(!empty($data_akun_tahun)){
      foreach($data_akun_tahun as $idx => $akun_tahun){
        
        $idakun  = $akun_tahun->idakun;   
        //get saldo awal
        if($bulan == '00'){
          if(is_null($akun_tahun->saldoawaljan))
            $calc_saldoawal = $akun_tahun->saldoawal;
          else
            $calc_saldoawal = $akun_tahun->saldoawaljan;

        }else{
          //bulan & tahun awal penjurnalan
          $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
          $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

          if(is_null($akun_tahun->$fieldbulanSaldoAwal) && $bulan == $bulanawal && $tahun == $tahunawal){
            $calc_saldoawal = $akun_tahun->saldoawal;
          }else{
            $calc_saldoawal = $akun_tahun->$fieldbulanSaldoAwal;
          }

        }

        /*
          @menghitung total transaksi debit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoDebit)){
          $totaldebit = $this->count_total_debit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $totaldebit = $akun_tahun->$fieldbulanSaldoDebit;
        }
        
        /*
          @menghitung total transaksi kredit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoCredit)){
          $totalkredit = $this->count_total_credit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $totalkredit = $akun_tahun->$fieldbulanSaldoCredit;
        }

        /*
          @menghitung total saldo akhir
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoAkhir)){
          $saldoakhir = $calc_saldoawal + $totaldebit - $totalkredit;
        }else{
          $saldoakhir = $akun_tahun->$fieldbulanSaldoAkhir;
        }

        //set summary info
        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Akun')
             ->setCellValue("C{$active_row}", ': '. $akun_tahun->nmakun .' ('. $akun_tahun->kdakun .')');
        $active_row += 1; 

        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Saldo Awal')
             ->setCellValue("C{$active_row}", ': Rp.'. number_format($calc_saldoawal,2,',','.') );
        $active_row += 1; 

        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Saldo Akhir')
             ->setCellValue("C{$active_row}", ': Rp.'. number_format($saldoakhir,2,',','.') );
        $active_row += 1; 
        
        //set header grid
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($headergrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Kode Jurnal')
             ->setCellValue("C{$active_row}", 'Tanggal Transaksi')
             ->setCellValue("D{$active_row}", 'Tanggal Jurnal')
             ->setCellValue("E{$active_row}", 'No. Reff / No. Bon')
             ->setCellValue("F{$active_row}", 'Keterangan')
             ->setCellValue("G{$active_row}", 'Debit')
             ->setCellValue("H{$active_row}", 'Kredit');
        $active_row += 1; // set active row to the next row
        
        //get detail jurnal
        $this->db->select('*');
        $this->db->from('v_bukujurnal');
        $this->db->orderby('tgljurnal', 'ASC');
        $this->db->where('idakun', $idakun);
        $this->db->where('status_posting', '1');
        $this->db->where('tahun', $tahun);
        if($bulan != '00')
         $this->db->where('bulan', $bulan);
      
        $query = $this->db->get();

        $data = $query->result();
        $total = $query->num_rows();

        $total_debit = 0;
        $total_kredit = 0;
        if(!empty($data))
        {
          
          foreach($data as $idx => $dt){
            $total_debit += $dt->debit;
            $total_kredit += $dt->kredit; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("B{$active_row}", $dt->kdjurnal)
                 ->setCellValue("C{$active_row}", date("d/m/Y", strtotime($dt->tgltransaksi)))
                 ->setCellValue("D{$active_row}", date("d/m/Y", strtotime($dt->tgljurnal)))
                 ->setCellValue("E{$active_row}", $dt->noreff_jurnaldet)
                 ->setCellValue("F{$active_row}", $dt->keterangan)
                 ->setCellValue("G{$active_row}", $dt->debit)
                 ->setCellValue("H{$active_row}", $dt->kredit);
            $active_row += 1; // set active row to the next row
          
          }

        }

        //row total debit & credit
        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:F{$active_row}");
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($summarygrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Total')
             ->setCellValue("G{$active_row}", $totaldebit)
             ->setCellValue("H{$active_row}", $totalkredit);

        $active_row += 2;

      }
    }

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'lap_buku_besar_detail_'. $tahun .'_'. (@$this->bulan[$bulan]);

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

  function lap_buku_jurnal_excel($tahun = '', $bulan = '00', $idakun = ''){
    $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    $fieldbulanSaldoAwal = ($bulan != '00') ? $this->bulanSaldoAwal[$bulan] : '';
    $fieldbulanSaldoDebit = ($bulan != '00') ? $this->bulanSaldoDebit[$bulan] : '';
    $fieldbulanSaldoCredit = ($bulan != '00') ? $this->bulanSaldoCredit[$bulan] : '';
    $fieldbulanSaldoAkhir = ($bulan != '00') ? $this->bulanSaldoAkhir[$bulan] : '';
    //bulan & tahun awal penjurnalan
    $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
    $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

    $this->load->library('pdf_lap');
    $isi = '<br/><font size=\"7\" face=\"Helvetica\">';       

    // add a page
    $page_format = array(
      'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      'Rotate' => 0,
      'PZ' => 1,
    );
    
    //Set Footer
    $this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
    $this->pdf_lap->setPrintFooter(true); // enabled ? true
    $this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $this->pdf_lap->SetAutoPageBreak(TRUE, '15');
    
    $this->pdf_lap->SetPrintHeader(false);
    $this->pdf_lap->AddPage('L', $page_format, false, false);
    $this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
    $this->pdf_lap->SetFont('helvetica', '', 14);
    
    $x=0;$y=10;
    $this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
    $this->pdf_lap->Cell(0, 0, 'Detail Buku Besar', 0, 1, 'C', 0, '', 0);
    
    $this->pdf_lap->SetFont('helvetica', '', 10);
    if($bulan != '00'){
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $this->bulan[$bulan] .' '. $tahun, 0, 1, 'C', 0, '', 0);
    }else{
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $tahun, 0, 1, 'C', 0, '', 0);  
    }
    
    //get all akun tahun
    $akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun, 'idakun' => $idakun))->row();
    
    //get saldo awal
    if($bulan == '00'){
      if(is_null($akun_tahun->saldoawaljan))
        $calc_saldoawal = $akun_tahun->saldoawal;
      else
        $calc_saldoawal = $akun_tahun->saldoawaljan;

    }else{
      //bulan & tahun awal penjurnalan
      $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
      $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

      if(is_null($akun_tahun->$fieldbulanSaldoAwal) && $bulan == $bulanawal && $tahun == $tahunawal){
        $calc_saldoawal = $akun_tahun->saldoawal;
      }else{
        $calc_saldoawal = $akun_tahun->$fieldbulanSaldoAwal;
      }

    }
    
    //get detail jurnal
    $this->db->select('*');
    $this->db->from('v_bukujurnal');
    $this->db->orderby('tgljurnal', 'ASC');
    $this->db->where('idakun', $idakun);
    $this->db->where('status_posting', '1');
    $this->db->where('tahun', $tahun);
    if($bulan != '00')
     $this->db->where('bulan', $bulan);
  
    $query = $this->db->get();

    $data = $query->result();
    $total = $query->num_rows();

    $total_debit = 0;
    $total_kredit = 0;
    $content = '';
    if(!empty($data))
    {
      $content .= '
      <table width="100%" border="1">
        <tr>
          <td width="10%" align="center"><strong>Kode Jurnal</strong></td>
          <td width="10%" align="center"><strong>Tanggal<br>Transaksi</strong></td>
          <td width="10%;" align="center"><strong>Tanggal<br>Jurnal</strong></td>     
          <td width="15%" align="center"><strong>No. Reff / No. Bon</strong></td>
          <td width="30%" align="center"><strong>Keterangan</strong></td>
          <td width="12%" align="center"><strong>Debit</strong></td>
          <td width="12%;" align="center"><strong>Kredit</strong></td>
        </tr>
      ';

      foreach($data as $idx => $dt){
        $total_debit += $dt->debit;
        $total_kredit += $dt->kredit; 

        $content .= '
        <tr>
          <td align="center"> &nbsp;'. $dt->kdjurnal .' &nbsp;</td>
          <td align="center"> &nbsp;'. date("d/m/Y", strtotime($dt->tgltransaksi)) .' &nbsp;</td>
          <td align="center"> &nbsp;'. date("d/m/Y", strtotime($dt->tgljurnal)) .' &nbsp;</td>     
          <td align="center"> &nbsp;'. $dt->noreff_jurnaldet .' &nbsp;</td>
          <td align="left"> &nbsp;'. $dt->keterangan .' &nbsp;</td>
          <td align="right"> &nbsp;Rp.'. number_format($dt->debit,2,',','.') .' &nbsp;</td>
          <td align="right"> &nbsp;Rp.'. number_format($dt->kredit,2,',','.') .' &nbsp;</td>
        </tr>';
      }

      $content .='
        <tr>
          <td align="right" colspan="5"> &nbsp;<strong>Total </strong>&nbsp;</td>
          <td align="right"> &nbsp;<strong>Rp.'. number_format($total_debit,2,',','.') .'</strong> &nbsp;</td>
          <td align="right"> &nbsp;<strong>Rp.'. number_format($total_kredit,2,',','.') .'</strong> &nbsp;</td>
        </tr>
        </table>';
    }

    $saldoakhir = $calc_saldoawal + $total_debit - $total_kredit;
    $isi .= '
      <br><br>
      <table>
        <tr>
          <td width="15%">Akun</td>
          <td width="10px">:</td>
          <td><strong>'. $akun_tahun->nmakun .' ('. $akun_tahun->kdakun .')</strong></td>
        </tr>
        <tr>
          <td>Saldo Awal</td>
          <td>:</td>
          <td><strong>Rp.'. number_format($calc_saldoawal,2,',','.') .'</strong></td>
        </tr>
        <tr>
          <td>Saldo Akhir</td>
          <td>:</td>
          <td><strong>Rp.'. number_format($saldoakhir,2,',','.') .'</strong></td>
        </tr>
      </table>
      <br/><br/>
    '.$content;

    $isi .= "</font>";


    $this->pdf_lap->writeHTML($isi,true,false,false,false);

    //Close and output PDF document
    $this->pdf_lap->Output('lap_detail_buku_besar.pdf', 'I');
  }
  
}
