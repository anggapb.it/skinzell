<?php

class Pengguna_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_pengguna(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*, date(pengguna.tgldaftar) tgldaftar");
        $this->db->from("pengguna");
        $this->db->join("jpengguna",
				"jpengguna.idjnspengguna = pengguna.idjnspengguna", "left"
		);
        $this->db->join("klppengguna",
				"klppengguna.idklppengguna = pengguna.idklppengguna", "left"
		);
        $this->db->join("status",
				"status.idstatus = pengguna.idstatus", "left"
		);
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select("*");
        $this->db->from("pengguna");
        $this->db->join("jpengguna",
				"jpengguna.idjnspengguna = pengguna.idjnspengguna", "left"
		);
        $this->db->join("klppengguna",
				"klppengguna.idklppengguna = pengguna.idklppengguna", "left"
		);
        $this->db->join("status",
				"status.idstatus = pengguna.idstatus", "left"
		);
    
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }
	
	function ganti_password(){
		if ($this->my_usession->logged_in) {
			 $oldpass =  $this->input->post('passlama');
			 $newpass =  $this->input->post('passbaru');
			 $user = $this->my_usession->userdata('user_id');
			 $cekpass = $this->id_field('userid', 'pengguna', 'password', base64_encode($oldpass));
  
			 if ($cekpass) {
				$data = array(
					 'password'=> base64_encode($newpass),
					 
					 );
				
				$this->db->trans_begin();
				
				$where['userid']=$user;
				
				$this->db->where($where);
				$this->db->update("pengguna", $data);

				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
					$return="Ganti Password gagal";
				}
				else
				{
					$this->db->trans_commit();
					$return="Ganti Password Berhasil";
				}
				echo $return;
				
			 } else {
				$return = "Password Lama Tidak Sama";
				echo $return;
			 }
		} else {
			$this->my_usession->set_userdata('login_res', 0);
			redirect('auth/login');
		}
    }
}
