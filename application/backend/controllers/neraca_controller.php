<?php

class Neraca_controller extends Controller {
  var $nmbulan = array();
  public function __construct()
  {
    parent::Controller();
    $this->load->library('pdf_lap');
    $this->load->library('session');
    $this->load->library('lib_phpexcel');
	
	$this->nmbulan = array(
		'0'  => 'jan',
		'1'  => 'jan',
		'2'  => 'feb',
		'3'  => 'mar',
		'4'  => 'apr',
		'5'  => 'mei',
		'6'  => 'jun',
		'7'  => 'jul',
		'8'  => 'ags',
		'9'  => 'sep',
		'10' => 'okt',
		'11' => 'nov',
		'12' => 'des',
	);

  }
  
  /*
  function get_aktiva_lancar(){
    $tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    if(empty($bulan)) $bulan = 0;

    if(empty($tahun)){
      $build_array = array ("success"=>true, "results"=>0, "data"=> array());
      echo json_encode($build_array);
      die;
    }

    $this->db->order_by('kdakun', 'ASC');
    $dafakun = $this->db->get('v_aktiva_lancar')->result_array();

    foreach($dafakun as $idx => $akun)
    {
        $q = "SELECT getSaldoAktiva('".$tahun."', '".$bulan."', '".$akun['idakun']."') as saldo;";
        $query  = $this->db->query($q)->row_array();
        $dafakun[$idx]['saldo'] = $query['saldo'];
    }

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }
  */

  function get_aktiva_lancar(){
	$tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    if(empty($bulan)) $bulan = 0;
	$nmbulan = $this->nmbulan[$bulan];

	//filter jurnaldet
	$bulanawal = $bulan - 1;
	$tahunawal = $tahun;
	if($bulanawal < 0){
		$bulanawal = 12;
		$tahunawal = $tahunawal - 1;
	}
	$tglawal = $tahunawal . '-'. str_pad($bulanawal, 2, 0) . '-21';
	$tglakhir = $tahun . '-'. str_pad($bulan, 2, 0) . '-20';
	$wherejurnal = "WHERE
	   jurnal.kdjurnal = jurnaldet.kdjurnal AND
	   jurnaldet.idakun = akun.idakun AND
	   jurnaldet.tahun = '{$tahun}' AND
	   jurnal.status_posting = 1 AND
       jurnal.tgljurnal >= '{$tglawal}'
	   ";
	if($bulan > 0){
		$wherejurnal .= " AND jurnal.tgljurnal <= '{$tglakhir}'";
	}
	
	$query = "SELECT 
	`akun`.`idakunparent` AS `idakunparent`
     , `akun`.`idakun` AS `idakun`
     , `akun`.`kdakun` AS `kdakun`
     , `akun`.`nmakun` AS `nmakun`
	 , (ifnull(akuntahun.saldodebit".$nmbulan.", 0) - 
	   ifnull(akuntahun.saldocredit".$nmbulan.", 0) +
	   (SELECT ifnull(sum(jurnaldet.debit), 0) - ifnull(sum(jurnaldet.kredit), 0)
	   FROM jurnaldet, jurnal {$wherejurnal}
       )) as saldo
     FROM `akun`
	 LEFT JOIN akuntahun
	 ON akuntahun.idakun = akun.idakun AND akuntahun.tahun = '{$tahun}'
	 WHERE
	 `akun`.`idstatus` = 1 AND
	 ((left(`akun`.`kdakun`, 2) = '11') OR 
	 (left(`akun`.`kdakun`, 2) = '12') OR 
	 (left(`akun`.`kdakun`, 2) = '13') OR 
	 (left(`akun`.`kdakun`, 2) = '14'))
	 ORDER BY akun.kdakun ASC
	 ";
	
    $dafakun = $this->db->query($query)->result_array();

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
	
  }
  
  /*
  function get_aktiva_tetap(){
    $tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    if(empty($bulan)) $bulan = 0;

    if(empty($tahun)){
      $build_array = array ("success"=>true, "results"=>0, "data"=> array());
      echo json_encode($build_array);
      die;
    }

    $this->db->order_by('kdakun', 'ASC');
    $dafakun = $this->db->get('v_aktiva_tetap')->result_array();

    foreach($dafakun as $idx => $akun)
    {
      $q = "SELECT getSaldoAktiva('".$tahun."', '".$bulan."', '".$akun['idakun']."') as saldo;";
      $query  = $this->db->query($q)->row_array();
      $dafakun[$idx]['saldo'] = $query['saldo'];
    }

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }
  */
  
  
  function get_aktiva_tetap(){
	$tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    
    if(empty($bulan)) $bulan = 0;
	$nmbulan = $this->nmbulan[$bulan];

	//filter jurnaldet
	$bulanawal = $bulan - 1;
	$tahunawal = $tahun;
	if($bulanawal < 0){
		$bulanawal = 12;
		$tahunawal = $tahunawal - 1;
	}
	$tglawal = $tahunawal . '-'. str_pad($bulanawal, 2, 0) . '-21';
	$tglakhir = $tahun . '-'. str_pad($bulan, 2, 0) . '-20';
	$wherejurnal = "WHERE
	   jurnal.kdjurnal = jurnaldet.kdjurnal AND
	   jurnaldet.idakun = akun.idakun AND
	   jurnaldet.tahun = '{$tahun}' AND
	   jurnal.status_posting = 1 AND
       jurnal.tgljurnal >= '{$tglawal}'
	   ";
	if($bulan > 0){
		$wherejurnal .= " AND jurnal.tgljurnal <= '{$tglakhir}'";
	}
	
	$query = "SELECT 
	`akun`.`idakunparent` AS `idakunparent`
     , `akun`.`idakun` AS `idakun`
     , `akun`.`kdakun` AS `kdakun`
     , `akun`.`nmakun` AS `nmakun`
	 , (ifnull(akuntahun.saldodebit".$nmbulan.", 0) - 
	   ifnull(akuntahun.saldocredit".$nmbulan.", 0) +
	   (SELECT ifnull(sum(jurnaldet.debit), 0) - ifnull(sum(jurnaldet.kredit), 0)
	   FROM jurnaldet, jurnal {$wherejurnal}
       )) as saldo
     FROM `akun`
	 LEFT JOIN akuntahun
	 ON akuntahun.idakun = akun.idakun AND akuntahun.tahun = '{$tahun}'
	 WHERE
	 `akun`.`idstatus` = 1 AND
	   ((left(`akun`.`kdakun`, 2) = '15') OR 
	   (left(`akun`.`kdakun`, 2) = '16'))
	 ORDER BY akun.kdakun ASC
	 ";
	
    $dafakun = $this->db->query($query)->result_array();

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }
  
  /*
  function get_pasiva(){
    $tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    if(empty($bulan)) $bulan = 0;

    if(empty($tahun)){
      $build_array = array ("success"=>true, "results"=>0, "data"=> array());
      echo json_encode($build_array);
      die;
    }

    $this->db->order_by('kdakun', 'ASC');
    $dafakun = $this->db->get('v_pasiva')->result_array();

    foreach($dafakun as $idx => $akun)
    {
        $q = "SELECT getSaldoPasiva('".$tahun."', '".$bulan."', '".$akun['idakun']."') as saldo;";
        $query  = $this->db->query($q)->row_array();
        $dafakun[$idx]['saldo'] = $query['saldo'];
    }

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }
  */

  function get_pasiva(){
    $tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    
    if(empty($bulan)) $bulan = 0;
	$nmbulan = $this->nmbulan[$bulan];

	//filter jurnaldet
	$bulanawal = $bulan - 1;
	$tahunawal = $tahun;
	if($bulanawal < 0){
		$bulanawal = 12;
		$tahunawal = $tahunawal - 1;
	}
	$tglawal = $tahunawal . '-'. str_pad($bulanawal, 2, 0) . '-21';
	$tglakhir = $tahun . '-'. str_pad($bulan, 2, 0) . '-20';
	$wherejurnal = "WHERE
	   jurnal.kdjurnal = jurnaldet.kdjurnal AND
	   jurnaldet.idakun = akun.idakun AND
	   jurnaldet.tahun = '{$tahun}' AND
	   jurnal.status_posting = 1 AND
       jurnal.tgljurnal >= '{$tglawal}'
	   ";
	if($bulan > 0){
		$wherejurnal .= " AND jurnal.tgljurnal <= '{$tglakhir}'";
	}
	
	$query = "SELECT 
	  `akun`.`idakun` AS `idakun`
     , `akun`.`kdakun` AS `kdakun`
     , `akun`.`nmakun` AS `nmakun`
	 , (ifnull(akuntahun.saldocredit".$nmbulan.", 0) - 
	   ifnull(akuntahun.saldodebit".$nmbulan.", 0) +
	   (SELECT ifnull(sum(jurnaldet.kredit), 0) - ifnull(sum(jurnaldet.debit), 0)
	   FROM jurnaldet, jurnal {$wherejurnal}
       )) as saldo
     FROM `akun`
	 LEFT JOIN akuntahun
	 ON akuntahun.idakun = akun.idakun AND akuntahun.tahun = '{$tahun}'
	 WHERE
	 `akun`.`idstatus` = 1 AND
	   (left(`akun`.`kdakun`, 1) = '2')
	 ORDER BY akun.kdakun ASC
	 ";
	
    $dafakun = $this->db->query($query)->result_array();

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }
  
  /*
  function get_modal(){
    $tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    if(empty($bulan)) $bulan = 0;

    if(empty($tahun)){
      $build_array = array ("success"=>true, "results"=>0, "data"=> array());
      echo json_encode($build_array);
      die;
    }

    $this->db->order_by('kdakun', 'ASC');
    $dafakun = $this->db->get('v_modal')->result_array();

    foreach($dafakun as $idx => $akun)
    {
        $q = "SELECT getSaldoAktiva('".$tahun."', '".$bulan."', '".$akun['idakun']."') as saldo;";
        $query  = $this->db->query($q)->row_array();
        $dafakun[$idx]['saldo'] = $query['saldo'];
    }

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }
  */
  
  function get_modal(){
    $tahun    = $this->input->post("tahun");
    $bulan   = $this->input->post("bulan");
    
    if(empty($bulan)) $bulan = 0;
	$nmbulan = $this->nmbulan[$bulan];

	//filter jurnaldet
	$bulanawal = $bulan - 1;
	$tahunawal = $tahun;
	if($bulanawal < 0){
		$bulanawal = 12;
		$tahunawal = $tahunawal - 1;
	}
	$tglawal = $tahunawal . '-'. str_pad($bulanawal, 2, 0) . '-21';
	$tglakhir = $tahun . '-'. str_pad($bulan, 2, 0) . '-20';
	$wherejurnal = "WHERE
	   jurnal.kdjurnal = jurnaldet.kdjurnal AND
	   jurnaldet.idakun = akun.idakun AND
	   jurnaldet.tahun = '{$tahun}' AND
	   jurnal.status_posting = 1 AND
       jurnal.tgljurnal >= '{$tglawal}'
	   ";
	if($bulan > 0){
		$wherejurnal .= " AND jurnal.tgljurnal <= '{$tglakhir}'";
	}
	
	$query = "SELECT 
	  `akun`.`idakun` AS `idakun`
     , `akun`.`kdakun` AS `kdakun`
     , `akun`.`nmakun` AS `nmakun`
	 , (ifnull(akuntahun.saldocredit".$nmbulan.", 0) - 
	   ifnull(akuntahun.saldodebit".$nmbulan.", 0) +
	   (SELECT ifnull(sum(jurnaldet.kredit), 0) - ifnull(sum(jurnaldet.debit), 0)
	   FROM jurnaldet, jurnal {$wherejurnal}
       )) as saldo
     FROM `akun`
	 LEFT JOIN akuntahun
	 ON akuntahun.idakun = akun.idakun AND akuntahun.tahun = '{$tahun}'
	 WHERE
	 `akun`.`idstatus` = 1 AND
	   (left(`akun`.`kdakun`, 1) = 3)
	 ORDER BY akun.kdakun ASC
	 ";
	
    $dafakun = $this->db->query($query)->result_array();

    $build_array = array ("success"=>true, "results"=>count($dafakun), "data"=> $dafakun);

    echo json_encode($build_array);
  }


	/*
  function lap_neraca_excel($tahun = '', $bulan = ''){
    
    $subject_file = (!empty($bulan) && $bulan != '0') ? 'Periode : '. str_pad($bulan, 2, "0", STR_PAD_LEFT) .' '.  $tahun : 'Periode : '. $tahun;

    //akun aktiva lancar
    $this->db->order_by('kdakun', 'ASC');
    $dafakun_aktiva_lancar = $this->db->get('v_aktiva_lancar')->result_array();
    foreach($dafakun_aktiva_lancar as $idx => $akun)
    {
      $query  = $this->db->query("SELECT getSaldoAktiva('".$tahun."', '".$bulan."', '".$akun['idakun']."') as saldo")->row_array();
      $dafakun_aktiva_lancar[$idx]['saldo'] = $query['saldo'];
    }

    //akun aktiva tetap
    $this->db->order_by('kdakun', 'ASC');
    $dafakun_aktiva_tetap = $this->db->get('v_aktiva_tetap')->result_array();
    foreach($dafakun_aktiva_tetap as $idx => $akun)
    {
      $query  = $this->db->query("SELECT getSaldoAktiva('".$tahun."', '".$bulan."', '".$akun['idakun']."') as saldo")->row_array();
      $dafakun_aktiva_tetap[$idx]['saldo'] = $query['saldo'];
    }

    //akun pasiva
    $this->db->order_by('kdakun', 'ASC');
    $dafakun_pasiva = $this->db->get('v_pasiva')->result_array();
    foreach($dafakun_pasiva as $idx => $akun)
    {
      $query  = $this->db->query("SELECT getSaldoPasiva('".$tahun."', '".$bulan."', '".$akun['idakun']."') as saldo")->row_array();
      $dafakun_pasiva[$idx]['saldo'] = $query['saldo'];
    }

    //akun modal
    $this->db->order_by('kdakun', 'ASC');
    $dafakun_modal = $this->db->get('v_modal')->result_array();
    foreach($dafakun_modal as $idx => $akun)
    {
      $query  = $this->db->query("SELECT getSaldoAktiva('".$tahun."', '".$bulan."', '".$akun['idakun']."') as saldo")->row_array();
      $dafakun_modal[$idx]['saldo'] = $query['saldo'];
    }

    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
        'font'  => array('bold'  => true)
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    $summarygrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT),
        'font'  => array('bold'  => true)
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Neraca")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Neraca'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:H1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Neraca');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:H2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(37);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(37);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

    $active_row_left = 4;
    if(!empty($dafakun_aktiva_lancar)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:D{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Aktiva Lancar');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Kode')
           ->setCellValue("C{$active_row_left}", 'Nama Akun')
           ->setCellValue("D{$active_row_left}", 'Saldo');
      $active_row_left += 1; 
      
      $total_aktiva_lancar = 0;
      foreach($dafakun_aktiva_lancar as $idx => $dakun){
            
        $total_aktiva_lancar += $dakun['saldo']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("B{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("C{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("D{$active_row_left}", $dakun['saldo']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:C{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Total')
           ->setCellValue("D{$active_row_left}", $total_aktiva_lancar);
      $active_row_left += 2; // set active row to the next row
     
    }

    if(!empty($dafakun_aktiva_tetap)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:D{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Aktiva Tetap');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Kode')
           ->setCellValue("C{$active_row_left}", 'Nama Akun')
           ->setCellValue("D{$active_row_left}", 'Saldo');
      $active_row_left += 1; 
      
      $total_aktiva_tetap = 0;
      foreach($dafakun_aktiva_tetap as $idx => $dakun){
            
        $total_aktiva_tetap += $dakun['saldo']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("B{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("C{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("D{$active_row_left}", $dakun['saldo']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:C{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Total')
           ->setCellValue("D{$active_row_left}", $total_aktiva_tetap);
      $active_row_left += 2; // set active row to the next row
     
    }

    //reset active row
    $active_row_left = 4;
    if(!empty($dafakun_pasiva)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("F{$active_row_left}:H{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Pasiva');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Kode')
           ->setCellValue("G{$active_row_left}", 'Nama Akun')
           ->setCellValue("H{$active_row_left}", 'Saldo');
      $active_row_left += 1; 
      
      $total_pasiva = 0;
      foreach($dafakun_pasiva as $idx => $dakun){
            
        $total_pasiva += $dakun['saldo']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("F{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("G{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("H{$active_row_left}", $dakun['saldo']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("F{$active_row_left}:G{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Total')
           ->setCellValue("H{$active_row_left}", $total_pasiva);
      $active_row_left += 2; // set active row to the next row
     
    }

    if(!empty($dafakun_modal)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("F{$active_row_left}:H{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Modal');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Kode')
           ->setCellValue("G{$active_row_left}", 'Nama Akun')
           ->setCellValue("H{$active_row_left}", 'Saldo');
      $active_row_left += 1; 
      
      $total_modal = 0;
      foreach($dafakun_modal as $idx => $dakun){
            
        $total_modal += $dakun['saldo']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("F{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("G{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("H{$active_row_left}", $dakun['saldo']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("F{$active_row_left}:G{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Total')
           ->setCellValue("H{$active_row_left}", $total_modal);
      $active_row_left += 2; // set active row to the next row
     
    }

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'neraca_'. $tahun .'_'. (@$bulan);

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }
	*/
  
  function lap_neraca_excel($tahun = '', $bulan = ''){
    
    if(empty($bulan) || $bulan == '00')  $bulan = 0;
	$nmbulan = $this->nmbulan[$bulan];

    $subject_file = (!empty($bulan) && $bulan != '0') ? 'Periode : '. str_pad($bulan, 2, "0", STR_PAD_LEFT) .' '.  $tahun : 'Periode : '. $tahun;

	
	//filter jurnaldet
	$bulanawal = $bulan - 1;
	$tahunawal = $tahun;
	if($bulanawal < 0){
		$bulanawal = 12;
		$tahunawal = $tahunawal - 1;
	}
	$tglawal = $tahunawal . '-'. str_pad($bulanawal, 2, 0) . '-21';
	$tglakhir = $tahun . '-'. str_pad($bulan, 2, 0) . '-20';
	$wherejurnal = "WHERE
	   jurnal.kdjurnal = jurnaldet.kdjurnal AND
	   jurnaldet.idakun = akun.idakun AND
	   jurnaldet.tahun = '{$tahun}' AND
	   jurnal.status_posting = 1 AND
       jurnal.tgljurnal >= '{$tglawal}'
	   ";
	if($bulan > 0){
		$wherejurnal .= " AND jurnal.tgljurnal <= '{$tglakhir}'";
	}
	
    //akun aktiva lancar
	$query = "SELECT 
	`akun`.`idakunparent` AS `idakunparent`
     , `akun`.`idakun` AS `idakun`
     , `akun`.`kdakun` AS `kdakun`
     , `akun`.`nmakun` AS `nmakun`
	 , (ifnull(akuntahun.saldodebit".$nmbulan.", 0) - 
	   ifnull(akuntahun.saldocredit".$nmbulan.", 0) +
	   (SELECT ifnull(sum(jurnaldet.debit), 0) - ifnull(sum(jurnaldet.kredit), 0)
	   FROM jurnaldet, jurnal {$wherejurnal}
       )) as saldo
     FROM `akun`
	 LEFT JOIN akuntahun
	 ON akuntahun.idakun = akun.idakun AND akuntahun.tahun = '{$tahun}'
	 WHERE
	 `akun`.`idstatus` = 1 AND
	 ((left(`akun`.`kdakun`, 2) = '11') OR 
	 (left(`akun`.`kdakun`, 2) = '12') OR 
	 (left(`akun`.`kdakun`, 2) = '13') OR 
	 (left(`akun`.`kdakun`, 2) = '14'))
	 ORDER BY akun.kdakun ASC
	 ";
    $dafakun_aktiva_lancar = $this->db->query($query)->result_array();

    //akun aktiva tetap
	$query = "SELECT 
	`akun`.`idakunparent` AS `idakunparent`
     , `akun`.`idakun` AS `idakun`
     , `akun`.`kdakun` AS `kdakun`
     , `akun`.`nmakun` AS `nmakun`
	 , (ifnull(akuntahun.saldodebit".$nmbulan.", 0) - 
	   ifnull(akuntahun.saldocredit".$nmbulan.", 0) +
	   (SELECT ifnull(sum(jurnaldet.debit), 0) - ifnull(sum(jurnaldet.kredit), 0)
	   FROM jurnaldet, jurnal {$wherejurnal}
       )) as saldo
     FROM `akun`
	 LEFT JOIN akuntahun
	 ON akuntahun.idakun = akun.idakun AND akuntahun.tahun = '{$tahun}'
	 WHERE
	 `akun`.`idstatus` = 1 AND
	   ((left(`akun`.`kdakun`, 2) = '15') OR 
	   (left(`akun`.`kdakun`, 2) = '16'))
	 ORDER BY akun.kdakun ASC
	 ";
    $dafakun_aktiva_tetap = $this->db->query($query)->result_array();


    //akun pasiva
	$query = "SELECT 
	  `akun`.`idakun` AS `idakun`
     , `akun`.`kdakun` AS `kdakun`
     , `akun`.`nmakun` AS `nmakun`
	 , (ifnull(akuntahun.saldocredit".$nmbulan.", 0) - 
	   ifnull(akuntahun.saldodebit".$nmbulan.", 0) +
	   (SELECT ifnull(sum(jurnaldet.kredit), 0) - ifnull(sum(jurnaldet.debit), 0)
	   FROM jurnaldet, jurnal {$wherejurnal}
       )) as saldo
     FROM `akun`
	 LEFT JOIN akuntahun
	 ON akuntahun.idakun = akun.idakun AND akuntahun.tahun = '{$tahun}'
	 WHERE
	 `akun`.`idstatus` = 1 AND
	   (left(`akun`.`kdakun`, 1) = '2')
	 ORDER BY akun.kdakun ASC
	 ";
    $dafakun_pasiva = $this->db->query($query)->result_array();


    //akun modal
	$query = "SELECT 
	  `akun`.`idakun` AS `idakun`
     , `akun`.`kdakun` AS `kdakun`
     , `akun`.`nmakun` AS `nmakun`
	 , (ifnull(akuntahun.saldocredit".$nmbulan.", 0) - 
	   ifnull(akuntahun.saldodebit".$nmbulan.", 0) +
	   (SELECT ifnull(sum(jurnaldet.kredit), 0) - ifnull(sum(jurnaldet.debit), 0)
	   FROM jurnaldet, jurnal {$wherejurnal}
       )) as saldo
     FROM `akun`
	 LEFT JOIN akuntahun
	 ON akuntahun.idakun = akun.idakun AND akuntahun.tahun = '{$tahun}'
	 WHERE
	 `akun`.`idstatus` = 1 AND
	   (left(`akun`.`kdakun`, 1) = 3)
	 ORDER BY akun.kdakun ASC
	 ";
    $dafakun_modal = $this->db->query($query)->result_array();


    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
        'font'  => array('bold'  => true)
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    $summarygrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT),
        'font'  => array('bold'  => true)
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Neraca")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Neraca'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:H1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Neraca');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:H2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(37);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(37);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

    $active_row_left = 4;
    if(!empty($dafakun_aktiva_lancar)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:D{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Aktiva Lancar');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Kode')
           ->setCellValue("C{$active_row_left}", 'Nama Akun')
           ->setCellValue("D{$active_row_left}", 'Saldo');
      $active_row_left += 1; 
      
      $total_aktiva_lancar = 0;
      foreach($dafakun_aktiva_lancar as $idx => $dakun){
            
        $total_aktiva_lancar += $dakun['saldo']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("B{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("C{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("D{$active_row_left}", $dakun['saldo']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:C{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Total')
           ->setCellValue("D{$active_row_left}", $total_aktiva_lancar);
      $active_row_left += 2; // set active row to the next row
     
    }

    if(!empty($dafakun_aktiva_tetap)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:D{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Aktiva Tetap');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Kode')
           ->setCellValue("C{$active_row_left}", 'Nama Akun')
           ->setCellValue("D{$active_row_left}", 'Saldo');
      $active_row_left += 1; 
      
      $total_aktiva_tetap = 0;
      foreach($dafakun_aktiva_tetap as $idx => $dakun){
            
        $total_aktiva_tetap += $dakun['saldo']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("B{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("C{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("D{$active_row_left}", $dakun['saldo']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row_left}:C{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row_left}:D{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("B{$active_row_left}", 'Total')
           ->setCellValue("D{$active_row_left}", $total_aktiva_tetap);
      $active_row_left += 2; // set active row to the next row
     
    }

    //reset active row
    $active_row_left = 4;
    if(!empty($dafakun_pasiva)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("F{$active_row_left}:H{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Pasiva');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Kode')
           ->setCellValue("G{$active_row_left}", 'Nama Akun')
           ->setCellValue("H{$active_row_left}", 'Saldo');
      $active_row_left += 1; 
      
      $total_pasiva = 0;
      foreach($dafakun_pasiva as $idx => $dakun){
            
        $total_pasiva += $dakun['saldo']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("F{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("G{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("H{$active_row_left}", $dakun['saldo']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("F{$active_row_left}:G{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Total')
           ->setCellValue("H{$active_row_left}", $total_pasiva);
      $active_row_left += 2; // set active row to the next row
     
    }

    if(!empty($dafakun_modal)){
      //set header grid
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("F{$active_row_left}:H{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Modal');
      $active_row_left += 1; 
      
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($headergrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Kode')
           ->setCellValue("G{$active_row_left}", 'Nama Akun')
           ->setCellValue("H{$active_row_left}", 'Saldo');
      $active_row_left += 1; 
      
      $total_modal = 0;
      foreach($dafakun_modal as $idx => $dakun){
            
        $total_modal += $dakun['saldo']; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("F{$active_row_left}", $dakun['kdakun'])
                 ->setCellValue("G{$active_row_left}", $dakun['nmakun'])
                 ->setCellValue("H{$active_row_left}", $dakun['saldo']);
            $active_row_left += 1; // set active row to the next row
        
      }

      //set footer total
      $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("F{$active_row_left}:G{$active_row_left}"); //merge cell
      $this->lib_phpexcel->getActiveSheet()->getStyle("F{$active_row_left}:H{$active_row_left}")->applyFromArray($summarygrid);
      $this->lib_phpexcel->setActiveSheetIndex(0)
           ->setCellValue("F{$active_row_left}", 'Total')
           ->setCellValue("H{$active_row_left}", $total_modal);
      $active_row_left += 2; // set active row to the next row
     
    }

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'neraca_'. $tahun .'_'. (@$bulan);

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

}
