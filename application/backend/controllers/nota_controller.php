<?php

class Nota_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
			$this->rhlib->secure($this->my_usession);
    }
	
	function get_nota(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        
        $fields			= $this->input->post("fields");
        $query			= $this->input->post("query");
        
        $idregdet		= $this->input->post("idregdet");
        $nonota			= $this->input->post("nonota");
        $koder			= $this->input->post("koder");
        $jpel			= $this->input->post("jpel");
        $idbagian		= $this->input->post("idbagian");
        $grupby			= $this->input->post("grupby");
        $khri			= $this->input->post("khri");
        $tgl			= $this->input->post("ctglnota");
        $idklstarif		= $this->input->post("idklstarif");
		
		$this->db->select("*, nota.nonota AS nonota, nota.catatan AS catatann,  nota.catatandiskon AS catatand,
				if(returfarmasidet.qty, returfarmasidet.qty, 0) AS qtyretur, notadet.qty AS qty,
				(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) tarif,
				(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) tarif2,
				(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) diskon,
				nota.diskon AS diskonr, nota.uangr AS uangr, notadet.dijamin AS dijaminrp, notadet.dijamin AS dijaminrpf,
				((notadet.dijamin/((notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp)))*100) AS dijaminprsn,
				((notadet.dijamin/(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)))*100) AS dijaminprsnf,
				ROUND((notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - notadet.dijamin) AS selisih,
				(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - notadet.dijamin AS selisihf,
				notadet.stdijamin AS dijamin, nota.stplafond AS stplafond,
				registrasidet.idklstarif as idklstarif
		", false);
        $this->db->from("nota");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("v_tarifall",
				"v_tarifall.kditem = notadet.kditem", "left");
        $this->db->join("dokter",
				"dokter.iddokter = notadet.iddokter", "left");
        $this->db->join("perawat",
				"perawat.idperawat = notadet.idperawat", "left");
        $this->db->join("jsatuan",
				"jsatuan.idsatuan = notadet.idsatuan", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
		if($tgl){
			$this->db->where("nota.tglnota = '".$tgl."'");
		}
		if(!$khri){
			if($idregdet)$this->db->where("nota.idregdet", $idregdet);
			else $this->db->where("nota.idregdet", null);
		}
		if($nonota)$this->db->where("nota.nonota", $nonota);
		if($jpel)$this->db->where("bagian.idjnspelayanan", $jpel);
		if($idbagian)$this->db->where("nota.idbagian", $idbagian);
		if($koder == 0){
			$this->db->where("notadet.koder", null);
			$this->db->group_by("notadet.kditem");
		} else if($koder == 1) {
			$this->db->where("notadet.koder !=", '');
			$this->db->group_by("notadet.kditem, notadet.koder");
			$this->db->orderby("notadet.koder");
		}
		if($grupby)$this->db->group_by("nota.nonota");
		$this->db->where("nota.idsttransaksi !=", 2);
		//if($idklstarif)$this->db->where("v_tarifall.klstarif", $idklstarif);
		if($idklstarif){
			$klstarif = array($idklstarif, '', 5);
			//$this->db->where_in('klstarif', $klstarif);
		}
		$this->db->orderby("nota.nonota desc");
		//var_dump($this->db);
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->where("(nota.nonota LIKE '%". $val ."%' OR kuitansi.atasnama LIKE '%". $val ."%')", null, false);
				//$this->db->like('kuitansi.atasnama', $val);
			}
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query, $tgl);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query, $tgl){
        $idregdet		= $this->input->post("idregdet");
        $nonota			= $this->input->post("nonota");
        $koder			= $this->input->post("koder");
        $jpel			= $this->input->post("jpel");
        $idbagian		= $this->input->post("idbagian");
        $grupby			= $this->input->post("grupby");
        $khri			= $this->input->post("khri");
		
		$this->db->select("*, nota.catatan AS catatann, 
				if(returfarmasidet.qty, returfarmasidet.qty, 0) AS qtyretur, notadet.qty AS qty,
				(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) tarif,
				(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) tarif2,
				(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) diskon
		", false);
        $this->db->from("nota");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("v_tarifall",
				"v_tarifall.kditem = notadet.kditem", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("perawat",
				"perawat.idperawat = notadet.idperawat", "left");
        $this->db->join("jsatuan",
				"jsatuan.idsatuan = notadet.idsatuan", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
		if($tgl){
			$this->db->where("nota.tglnota = '".$tgl."'");
		}
				
		if(!$khri){
			if($idregdet)$this->db->where("nota.idregdet", $idregdet);
			else $this->db->where("nota.idregdet", null);
		}
		if($nonota)$this->db->where("nota.nonota", $nonota);
		if($jpel)$this->db->where("bagian.idjnspelayanan", $jpel);
		if($idbagian)$this->db->where("nota.idbagian", $idbagian);
		if($koder == 0){
			$this->db->where("notadet.koder", null);
			$this->db->group_by("notadet.kditem");
		} else if($koder == 1) {
			$this->db->where("notadet.koder !=", '');
			$this->db->group_by("notadet.kditem, notadet.koder");
			$this->db->orderby("notadet.koder");
		}
		if($grupby)$this->db->group_by("nota.nonota");
		$this->db->orderby("nota.nonota desc");
			
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->where("(nota.nonota LIKE '%". $val ."%' OR kuitansi.atasnama LIKE '%". $val ."%')", null, false);
			}
        }
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_notax(){
        $start				= $this->input->post("start");
        $limit				= $this->input->post("limit");
		
        $idregdet			= $this->input->post("idregdet");
        $noreg				= $this->input->post("noreg");
        $jpel				= $this->input->post("jpel");
        $batal				= $this->input->post("batal");
        $ctglnota			= $this->input->post("ctglnota");
        $cnmjnspelayanan	= $this->input->post("cnmjnspelayanan");
        $cdokter			= $this->input->post("cdokter");
        $cnorm				= $this->input->post("cnorm");
        $cnoreg				= $this->input->post("cnoreg");
        $cnmpasien			= $this->input->post("cnmpasien");
        $batal				= $this->input->post("batal");
		
		$this->db->select("*,
				(select SUM((nd.tarifjs*nd.qty)+(nd.tarifjm*nd.qty)+(nd.tarifjp*nd.qty)+(nd.tarifbhp*nd.qty)) as ttlnota
					from notadet nd
					where nd.nonota = nota.nonota) as ttlnota
		");
        $this->db->from("nota");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg = registrasidet.noreg", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
		
		if($idregdet)$this->db->where("nota.idregdet", $idregdet);
		if($noreg)$this->db->where("registrasi.noreg", $noreg);
		if($jpel)$this->db->where("bagian.idjnspelayanan", $jpel);
		if($batal)$this->db->where("nota.idsttransaksi !=", $batal);
		
		if($ctglnota)$this->db->like("nota.tglnota !=", $ctglnota);
		if($cnmjnspelayanan)$this->db->like("nota.idbagian !=", $cnmjnspelayanan);
		if($cdokter)$this->db->like("nota.iddokter !=", $cdokter);
		if($cnorm)$this->db->like("pasien.norm !=", $cnorm);
		if($cnmpasien)$this->db->like("pasien.nmpasien !=", $cnmpasien);
		if($cnoreg)$this->db->like("registrasi.noreg !=", $cnoreg);
		
		$this->db->orderby("tglnota desc");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrowx();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrowx(){
        $start				= $this->input->post("start");
        $limit				= $this->input->post("limit");
		
        $idregdet			= $this->input->post("idregdet");
        $noreg				= $this->input->post("noreg");
        $jpel				= $this->input->post("jpel");
        $batal				= $this->input->post("batal");
        $ctglnota			= $this->input->post("ctglnota");
        $cnmjnspelayanan	= $this->input->post("cnmjnspelayanan");
        $cdokter			= $this->input->post("cdokter");
        $cnorm				= $this->input->post("cnorm");
        $cnoreg				= $this->input->post("cnoreg");
        $cnmpasien			= $this->input->post("cnmpasien");
        $batal				= $this->input->post("batal");
		
		$this->db->select("*,
				(select SUM((nd.tarifjs*nd.qty)+(nd.tarifjm*nd.qty)+(nd.tarifjp*nd.qty)+(nd.tarifbhp*nd.qty)) as ttlnota
					from notadet nd
					where nd.nonota = nota.nonota) as ttlnota
		");
        $this->db->from("nota");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg = registrasidet.noreg", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
		
		if($idregdet)$this->db->where("nota.idregdet", $idregdet);
		if($noreg)$this->db->where("registrasi.noreg", $noreg);
		if($jpel)$this->db->where("bagian.idjnspelayanan", $jpel);
		if($batal)$this->db->where("nota.idsttransaksi !=", $batal);
		
		if($ctglnota)$this->db->like("nota.tglnota !=", $ctglnota);
		if($cnmjnspelayanan)$this->db->like("nota.idbagian !=", $cnmjnspelayanan);
		if($cdokter)$this->db->like("nota.iddokter !=", $cdokter);
		if($cnorm)$this->db->like("pasien.norm !=", $cnorm);
		if($cnmpasien)$this->db->like("pasien.nmpasien !=", $cnmpasien);
		if($cnoreg)$this->db->like("registrasi.noreg !=", $cnoreg);
		
		$this->db->orderby("tglnota desc");
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_notatransri(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        
        $fields			= $this->input->post("fields");
        $query			= $this->input->post("query");
        
        $idregdet		= $this->input->post("idregdet");
        $noreg			= $this->input->post("noreg");
        $nonota			= $this->input->post("nonota");
        $koder			= $this->input->post("koder");
        $okoder			= $this->input->post("okoder");
        $jpel			= $this->input->post("jpel");
        $idbagian		= $this->input->post("idbagian");
        $grupby			= $this->input->post("grupby");
		
		$this->db->select("*, nota.catatan AS catatann,
				(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) tarif,
				(SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) AS qty,
				
				(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty)) - SUM(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - if((
						select if(rfd.qty, rfd.qty, 0)
						from returfarmasidet rfd
						where notadet.idnotadet = rfd.idnotadet
					)>0,SUM((notadet.tarifjs+notadet.tarifjm+notadet.tarifjp+notadet.tarifbhp)*
					(
						select if(rfd.qty, rfd.qty, 0)
						from returfarmasidet rfd
						where notadet.idnotadet = rfd.idnotadet
					)
				), 0) tarif2,
				
				SUM(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) diskon,
				SUM(nota.uangr) AS uangr,
				SUM(nota.diskon) AS diskonr,
				nota.uangr AS uangr,
				IFNULL((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem), 0) AS dijaminrp,
				IFNULL((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem), 0) AS dijaminrpf,
				IFNULL((((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem)/(((SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp)))*100), 0) AS dijaminprsn,
				IFNULL((((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem)/((SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)))*100), 0) AS dijaminprsnf,
				(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - IFNULL((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem), 0) AS selisih,
				
				
				(SUM(notadet.tarifjs*notadet.qty) + SUM(notadet.tarifjm*notadet.qty) + SUM(notadet.tarifjp*notadet.qty) + SUM(notadet.tarifbhp*notadet.qty)) - SUM(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - if((
						select if(rfd.qty, rfd.qty, 0)
						from returfarmasidet rfd
						where notadet.idnotadet = rfd.idnotadet
					)>0,SUM((notadet.tarifjs+notadet.tarifjm+notadet.tarifjp+notadet.tarifbhp)*
					(
						select if(rfd.qty, rfd.qty, 0)
						from returfarmasidet rfd
						where notadet.idnotadet = rfd.idnotadet
					)
				), 0) - IFNULL((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem), 0) AS selisihf,
				
				
				(SELECT nad.stdijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem) AS dijamin,
				(SELECT na.stplafond FROM notaakhir na WHERE na.nona = nota.nona) AS stplafond
		", false);
        $this->db->from("notadet");
        $this->db->join("nota",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg = registrasidet.noreg", "left");
        $this->db->join("v_brgpel",
				"v_brgpel.kditem = notadet.kditem", "left");
        $this->db->join("jsatuan",
				"jsatuan.idsatuan = notadet.idsatuan", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
		/* if($idregdet)$this->db->where("nota.idregdet", $idregdet);
		else $this->db->where("nota.idregdet", null); */
		if($noreg)$this->db->where("registrasi.noreg", $noreg);
		else $this->db->where("registrasi.noreg", null);
		if($nonota)$this->db->where("nota.nonota", $nonota);
		if($idbagian)$this->db->where("nota.idbagian", $idbagian);
		if($koder == 0){
			$this->db->where("notadet.koder", null);
			$this->db->group_by("notadet.kditem");
		} else if($koder == 1) {
			$this->db->where("notadet.koder !=", '');
			$this->db->group_by("notadet.kditem");
		}
		
		if($okoder){
			$this->db->orderby("notadet.koder");
		}
		
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nota.nonota', $val);
				$this->db->or_like('atasnama', $val);
			}
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		$ttl = $q->num_rows();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }

    function get_notatransri_with_bhp(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        
        $fields			= $this->input->post("fields");
        $query			= $this->input->post("query");
        
        $idregdet		= $this->input->post("idregdet");
        $noreg			= $this->input->post("noreg");
        $nonota			= $this->input->post("nonota");
        $koder			= $this->input->post("koder");
        $okoder			= $this->input->post("okoder");
        $jpel			= $this->input->post("jpel");
        $idbagian		= $this->input->post("idbagian");
        $grupby			= $this->input->post("grupby");
		
		$this->db->select("
				(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) tarif,
				(case when notadet.koder <> '' then
				    v_brgpel.nmitem
				  else 
				    pba.nmbrg
				  end) as nmitem,
				   (case when notadet.koder <> '' then
					    notadet.qty
					  else 
					    pba.qty * notadet.qty 
					  end) as qty
				,(case when notadet.koder <> '' then
					    jsatuan.nmsatuan
					  else 
					    pba.nmsatuan
					  end) as nmsatuan
				
			
		", false);
        $this->db->from("notadet");
        $this->db->join("nota",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg = registrasidet.noreg", "left");
        $this->db->join("v_brgpel",
				"v_brgpel.kditem = notadet.kditem", "left");
        $this->db->join("jsatuan",
				"jsatuan.idsatuan = notadet.idsatuan", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
        $this->db->join("v_pelbhpalkes pba",
				"pba.kdpelayanan = notadet.kditem", "left");
        
		/* if($idregdet)$this->db->where("nota.idregdet", $idregdet);
		else $this->db->where("nota.idregdet", null); */
		if($noreg)$this->db->where("registrasi.noreg", $noreg);
		else $this->db->where("registrasi.noreg", null);
		if($nonota)$this->db->where("nota.nonota", $nonota);
		if($idbagian)$this->db->where("nota.idbagian", $idbagian);
		//$this->db->where("v_brgpel.nmitem <>",null);
		// if($koder == 0){
		// 	$this->db->where("notadet.koder", null);
		// 	$this->db->group_by("notadet.kditem");
		// } else if($koder == 1) {
		// 	$this->db->where("notadet.koder !=", '');
		// 	$this->db->group_by("notadet.kditem");
		// }
		
		if($okoder){
			$this->db->orderby("notadet.koder");
		}
		
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nota.nonota', $val);
				$this->db->or_like('atasnama', $val);
			}
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        // print_r($this->db->last_query());
        // die;
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		$ttl = $q->num_rows();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_kuinota(){
        $this->db->select("*,
				SUM((tarifjs*qty)+(tarifjm*qty)+(tarifjp*qty)+(tarifbhp*qty)) as ttlnota
		", false);
        $this->db->from("notadet");
		$this->db->where("nonota", $_POST['nonota']);
		//$this->db->where("idstbypass", 1);
        
        $q = $this->db->get();
        $data = $q->row_array();
		
        echo json_encode($data);
    }
	
	function delete_notadet(){
		$where['nonota'] = $_POST['nonota'];
		$where['kditem'] = $_POST['kditem'];
		$where['idjnstarif'] = $_POST['idjnstarif'];
		$del = $this->rhlib->deleteRecord('notadet',$where);
		
		if($del)
		{
			$ret["success"] = true;
            $ret["nonota"]=$_POST['nonota'];
		}else{
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function delete_bnotadet(){
		
        $k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arr']);
		$b=explode(',', $r);
		foreach($b AS $x){
			$y=explode('-', $x);
			$where['nonota'] = $_POST['nonota'];
			$where['kditem'] = $y[0];
			$where['idjnstarif'] = $y[1];
			$del = $this->rhlib->deleteRecord('notadet',$where);
		}
		
		if($del)
		{
			$ret["success"] = true;
            $ret["nonota"]=$_POST['nonota'];
		}else{
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function batal_nota(){
		$dataArray = array(
             'idsttransaksi'=> 2
        );		
		$this->db->where('nonota', $_POST['nonota']);
		$z =$this->db->update('nota', $dataArray);
		
		if($z)
		{
			$ret["success"] = true;
            $ret["nonota"]=$_POST['nonota'];
		}else{
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
		
	function insorupd_nota(){
        $this->db->trans_begin();
		$kui = 0;
		if(isset($_POST['kui'])) $kui = $_POST['kui'];
		$arrnota = $this->input->post("arrnota");
		
		$this->db->select("
			r.noreg AS noreg,
			r.keluhan AS keluhan,
			r.nmkerabat AS nmkerabat,
			r.notelpkerabat AS notelpkerabat,
			r.catatan AS catatanr,
			trim(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			p.alamat AS alamatp,
			p.nmibu AS nmibu,
			p.alergi AS alergi,
			p.tptlahir AS tptlahirp,
			p.noidentitas AS noidentitas,
			p.tgllahir AS tgllahirp,
			p.notelp AS notelpp,
			p.nohp AS nohpp,
			p.tgldaftar AS tgldaftar,
			p.negara AS negara,
			rd.idregdet AS idregdet,
			rd.tglreg AS tglreg,
			rd.jamreg AS jamreg,
			rd.tglmasuk AS tglmasuk,
			rd.jammasuk AS jammasuk,
			rd.tglkeluar AS tglkeluar,
			rd.jamkeluar AS jamkeluar,
			rd.catatankeluar AS catatankeluar,
			rd.tglrencanakeluar AS tglrencanakeluar,
			rd.catatanrencanakeluar AS catatanrencanakeluar,
			rd.umurtahun AS umurtahun,
			rd.umurbulan AS umurbulan,
			rd.umurhari AS umurhari,
			rd.userbatal AS userbatal,
			rd.nmdokterkirim AS nmdokterkirim,
			rd.userinput AS userinput,
			rd.idstkeluar AS idstkeluar,
			rd.idcarakeluar AS idcarakeluar,
			r.idpenjamin AS idpenjamin,
			r.idjnspelayanan AS idjnspelayanan,
			r.idstpasien AS idstpasien,
			r.idhubkeluarga AS idhubkeluarga,
			p.idjnskelamin AS idjnskelamin,
			rd.idcaradatang AS idcaradatang,
			rd.idbagian AS idbagian,
			rd.idbagiankirim AS idbagiankirim,
			rd.idkamar AS idkamar,
			rd.iddokter AS iddokter,
			rd.iddokterkirim AS iddokterkirim,
			rd.idklsrawat AS idklsrawat,
			rd.idklstarif AS idklstarif,
			rd.idshift AS idshift,
			rd.idbed AS idbed,
			bed.idstbed AS idstbed,
			bed.nmbed AS nmbed,
			jkel.nmjnskelamin AS nmjnskelamin,
			jkel.kdjnskelamin AS kdjnskelamin,
			jpel.nmjnspelayanan AS nmjnspelayanan,
			dae.nmdaerah AS nmdaerah,
			sp.idstpelayanan AS idstpelayanan,
			sp.nmstpelayanan AS nmstpelayanan,
			pnj.nmpenjamin AS nmpenjamin,
			kr.nmklsrawat AS nmklsrawat,
			bag.nmbagian AS nmbagian,
			bagkrm.nmbagian AS nmbagiankirim,
			kmr.nmkamar AS nmkamar,
			kmr.kdkamar AS kdkamar,
			nt.nonota AS nonota,
			nt.nona AS nona,
			nt.tglnota AS tglnota,
			nt.jamnota AS jamnota,
			nt.catatan AS catatannota,
			nt.catatandiskon AS catatand,
			nt.diskon AS diskonr,
			nt.uangr AS uangr,
			nt.idsttransaksi AS idsttransaksi,
			nt.idregdettransfer AS idregdettransfer,
			nt.stplafond AS stplafond,
			na.stplafond AS stplafondna,
			na.catatandiskon AS catatands,
			d.nmdokter AS nmdokter,
			d.nmdoktergelar AS nmdoktergelar,
			dkrm.nmdoktergelar AS nmdokterkirimdlm,
			res.noantrian AS noantrian,
			cd.nmcaradatang AS nmcaradatang,
			pospas.nmstposisipasien AS nmstposisipasien,
			pospas.idstposisipasien AS idstposisipasien,
			kui.nokuitansi AS nokuitansi,
			kui.atasnama AS atasnama,
			kui.total AS total,
			kui.nokasir AS nokasir,
			kui.tglkuitansi AS tglkuitansi,
			kui.jamkuitansi AS jamkuitansi,
			sbt.kdsbtnm AS kdsbtnm,
			kt.nmklstarif AS nmklstarif,
			stkawin.nmstkawin AS nmstkawin,
			(
				SELECT sum(kui2.total) AS deposit
				FROM
				  (kuitansi kui2
				JOIN registrasidet rd2)
				WHERE
				  ((kui2.idregdet = rd2.idregdet)
				  AND (kui2.idstkuitansi = 1)
				  AND (rd2.noreg = r.noreg))) AS deposit,
			daerah.nmdaerah AS kelurahan,
			kec.nmdaerah AS kec,
			kota.nmdaerah AS kota,
			agama.nmagama AS nmagama,
			goldarah.nmgoldarah AS nmgoldarah,
			pendidikan.nmpendidikan AS nmpendidikan,
			pekerjaan.nmpekerjaan AS nmpekerjaan,
			sukubangsa.nmsukubangsa AS nmsukubangsa
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('pasien p',
                'p.norm = r.norm', 'left');
		$this->db->join('jkelamin jkel',
                'jkel.idjnskelamin = p.idjnskelamin', 'left');
		$this->db->join('daerah dae',
                'dae.iddaerah = p.iddaerah', 'left');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('penjamin pnj',
                'pnj.idpenjamin = r.idpenjamin', 'left');
		$this->db->join('jpelayanan jpel',
                'jpel.idjnspelayanan = r.idjnspelayanan', 'left');
		$this->db->join('stpasien stp',
                'stp.idstpasien = r.idstpasien', 'left');
		$this->db->join('hubkeluarga hk',
                'hk.idhubkeluarga = r.idhubkeluarga', 'left');
		$this->db->join('shift sh',
                'sh.idshift = rd.idshift', 'left');
		$this->db->join('bagian bag',
                'bag.idbagian = rd.idbagian', 'left');
		$this->db->join('bagian bagkrm',
                'bagkrm.idbagian = rd.idbagiankirim', 'left');
		$this->db->join('dokter d',
                'd.iddokter = rd.iddokter', 'left');
		$this->db->join('dokter dkrm',
                'dkrm.iddokter = rd.iddokterkirim', 'left');
		$this->db->join('caradatang cd',
                'cd.idcaradatang = rd.idcaradatang', 'left');
		$this->db->join('bed',
                'bed.idbed = rd.idbed', 'left');
		$this->db->join('klstarif kt',
                'kt.idklstarif = rd.idklstarif', 'left');
		$this->db->join('klsrawat kr',
                'kr.idklsrawat = rd.idklsrawat', 'left');
		$this->db->join('kamar kmr',
                'kmr.idkamar = rd.idkamar', 'left');
		$this->db->join('stkeluar sk',
                'sk.idstkeluar = rd.idstkeluar', 'left');
		$this->db->join('stregistrasi sr',
                'sr.idstregistrasi = rd.idstregistrasi', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
		$this->db->join('notaakhir na',
                'na.nona = nt.nona', 'left');
		$this->db->join('stpelayanan sp',
                'sp.idstpelayanan = nt.idstpelayanan', 'left');
		$this->db->join('kuitansi kui',
                'kui.nokuitansi = nt.nokuitansi', 'left');
		$this->db->join('jsbtnm sbt',
                'sbt.idsbtnm = kui.idsbtnm', 'left');
		$this->db->join('reservasi res',
                'res.idregdet = rd.idregdet', 'left');
		$this->db->join('stposisipasien pospas',
                'pospas.idstposisipasien = res.idstposisipasien', 'left');
		$this->db->join('stkawin',
                'p.idstkawin = stkawin.idstkawin', 'left');
		$this->db->join('daerah',
                'p.iddaerah = daerah.iddaerah', 'left');
		$this->db->join('daerah kec',
                'daerah.dae_iddaerah = kec.iddaerah', 'left');
		$this->db->join('daerah kota',
                'kec.dae_iddaerah = kota.iddaerah', 'left');
		$this->db->join('agama',
                'p.idagama = agama.idagama', 'left');
		$this->db->join('goldarah',
                'p.idgoldarah = goldarah.idgoldarah', 'left');
		$this->db->join('pendidikan',
                'p.idpendidikan = pendidikan.idpendidikan', 'left');
		$this->db->join('pekerjaan',
                'p.idpekerjaan = pekerjaan.idpekerjaan', 'left');
		$this->db->join('sukubangsa',
                'p.idsukubangsa = sukubangsa.idsukubangsa', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $_POST['noreg']);
		$this->db->order_by('rd.idregdet DESC');
		$q = $this->db->get();
		$reg = $q->row_array();
		
		if(isset($_POST['fri'])) $reg['idbagian'] = $_POST['fri'];
		
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['nonota']));
		$notanr = $query->num_rows();
		$nota = $query->row_array();
		
		if($kui == 0 && $_POST['arrcarabayar'] != "[]"){
			$kque = $this->insert_kuitansi($nota);
			$kui = $kque['nokuitansi'];
			
			$dataArray = array(
				'tglkeluar' => $_POST['tglnota'],
				'jamkeluar' => $_POST['jamnota'],
				'cttdiagnosa' => $_POST['cttdiagnosa']
			);
			$this->db->where('noreg', $_POST['noreg']);
			$regdet = $this->db->update('registrasidet', $dataArray);
			$this->update_posisispasien($reg['idregdet']);
		} else {
			$kui = $reg['nokuitansi'];
			$dataArray = array(
				'cttdiagnosa' => $_POST['cttdiagnosa']
			);
			$this->db->where('noreg', $_POST['noreg']);
			$regdet = $this->db->update('registrasidet', $dataArray);
		}
		
		$_POST['noresep'] = null;
		if($notanr == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			//$nota = $query->row_array();
			$vnota = $nota['nonota'];
		}
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $nota['idbagian']);
		
		if($nota && $notadet)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$vnota;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insorupd_notari(){
        $this->db->trans_begin();
		if(!isset($_POST['stpelayanan']))$stpelayanan=null;
		else $stpelayanan=$this->searchId('idstpelayanan','stpelayanan','nmstpelayanan',$_POST['stpelayanan']);
		
		if(!isset($_POST['tgltrans'])) $tgltrans = date('Y-m-d');
		else $tgltrans = $_POST['tgltrans'];
		
		if(!isset($_POST['jamtrans'])) $jamtrans = date('H:i:s');
		else $jamtrans = $_POST['jamtrans'];
		
		$this->db->select("
			r.noreg AS noreg,
			rd.idregdet AS idregdet,
			nt.nona AS nona,
			rd.idbed AS idbed
		", false);
		
        $this->db->from('registrasi r');
		$this->db->join('registrasidet rd',
                'rd.noreg = r.noreg', 'left');
		$this->db->join('nota nt',
                'nt.idregdet = rd.idregdet', 'left');
        $this->db->where('rd.userbatal', null);
        $this->db->where('r.noreg', $_POST['noreg']);
		$this->db->order_by('rd.idregdet DESC');
		$query = $this->db->get();
		$reg = $query->row_array();
		$regarr = $query->result();
		if($this->input->post("cekpembayaran") == 1){
			$this->db->select("nt.nokuitansi AS nokuitansi", false);
			$this->db->from('registrasi r');
			$this->db->join('registrasidet rd',
					'rd.noreg = r.noreg', 'left');
			$this->db->join('nota nt',
					'nt.idregdet = rd.idregdet', 'left');
			$this->db->where('rd.userbatal', null);
			$this->db->where('nt.nokuitansi IS NOT NULL', null, false);
			$this->db->where('r.noreg', $_POST['noreg']);
			$this->db->order_by('rd.idregdet DESC');
			$query1 = $this->db->get();
			$reg1 = $query1->row_array();
			$na = $reg['nona'];
			$where['nokuitansi'] = $reg1['nokuitansi'];
			$this->update_kuitansi($where);
			$kuidet = $this->update_kuitansidet($where);			
			$dataArrayn = array('total1'=> $_POST['total'],'catatandiskon'=> $_POST['catatandskn']);
			$this->db->where('nona', $na);				
			$notaakhir = $this->db->update('notaakhir', $dataArrayn);
			if($kuidet)
			{
				$this->db->trans_commit();
				$ret["success"] = true;
				$ret["nona"]=$na;
			}else{
				$this->db->trans_rollback();
				$ret["success"] = false;
			}
		} else {
			$this->db->select("nt.nokuitansi AS nokuitansi", false);
			$this->db->from('registrasi r');
			$this->db->join('registrasidet rd',
					'rd.noreg = r.noreg', 'left');
			$this->db->join('nota nt',
					'nt.idregdet = rd.idregdet', 'left');
			$this->db->where('rd.userbatal', null);
			$this->db->where('nt.nokuitansi IS NOT NULL', null, false);
			$this->db->where('r.noreg', $_POST['noreg']);
			$this->db->order_by('rd.idregdet DESC');
			$query2 = $this->db->get();
			$reg2 = $query2->row_array();				
			if($query2->num_rows() == 0){
				$kque = $this->insert_kuitansi(false);
				$kui = $kque['nokuitansi'];
				
				$naque = $this->insert_notaakhir($kui);
				$na = $naque['nona'];
				foreach($regarr AS $i=>$val){
					$dataArray = array('nokuitansi'=> $kui, 'nona'=>$na, 'idstpelayanan'=>$stpelayanan, 'diskon'=>$_POST['diskonf'], 'uangr'=>$_POST['uangr']);
					$this->db->where('idregdet', $val->idregdet);
					$z = $this->db->update('nota', $dataArray);
					
					$this->update_posisispasien($val->idregdet);
				}
				$dataArray = array(
					'idstbed' => 1
				);
				$this->db->where('idbed', $reg['idbed']);
				$bed = $this->db->update('bed', $dataArray);
				
				$k=array('[',']','"');
				$r=str_replace($k, '', $_POST['arrnota']);
				$b=explode(',', $r);
				foreach($b AS $val){
					$vale = explode('-',$val);
					
					if($vale[1]>0)$stdijamin = 1;
					else $stdijamin = 0;
					
					$dataArrayx = array(
						'nona'=> $na,
						'kditem'=> $vale[0],
						'dijamin'=> $vale[1],
						'stdijamin'=> $stdijamin,
					);
					$y = $this->db->insert('notaakhirdet', $dataArrayx);
				}
				
				$dataArray = array(
					'tglkeluar' => $tgltrans,
					'jamkeluar' => $jamtrans
				);
				$this->db->where('noreg', $_POST['noreg']);
				$regdet = $this->db->update('registrasidet', $dataArray);
				
				if($kui && $z && $na && $regdet && $bed)
				{
					$this->db->trans_commit();
					$ret["success"] = true;
					$ret["nona"]=$na;
				}else{
					$this->db->trans_rollback();
					$ret["success"] = false;
				}
			} else {
				//update kuitansi
				if(!isset($_POST['pembayaran'])) $_POST['pembayaran'] = $_POST['total'];
				
				if(!isset($_POST['tgltrans'])) $tgltrans = $_POST['tglnota'];
				else $tgltrans = $_POST['tgltrans'];
				
				if(!isset($_POST['jamtrans'])) $jamtrans = $_POST['jamnota'];
				else $jamtrans = $_POST['jamtrans'];
				
				$dataArray = array(
					 'nokuitansi'=> $reg2['nokuitansi'],
					 'tglkuitansi'=> $tgltrans,
					 'jamkuitansi'=> $jamtrans,
					 'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
					 'idsbtnm'=> $this->searchSbtnm('idsbtnm','jsbtnm',$_POST['tahun'],$_POST['jkel']),
					 'atasnama'=> $_POST['atasnama'],
					 'total'=> $_POST['total'],
					 'pembayaran'=> $_POST['pembayaran'],
				);
				$this->db->where('nokuitansi', $reg2['nokuitansi']);
				$kui = $this->db->update('kuitansi', $dataArray);
				
				//inser kuidet baru
				$where['nokuitansi'] = $reg2['nokuitansi'];
				$this->db->delete('kuitansidet',$where);
				
				$k=array('[',']','"');
				$r=str_replace($k, '', $_POST['arrcarabayar']);
				$b=explode(',', $r);
				foreach($b AS $val){
					$vale = explode('-', $val);
					$dataArray = $this->getFieldsAndValuesKuiDetup($reg2['nokuitansi'], $vale[0], $vale[1], $vale[2], $vale[3]);
					$kdt =$this->db->insert('kuitansidet',$dataArray);
				}				
				
				//update notaakhir
				$na = $reg['nona'];
				$dataArrayn = array('total1'=> $_POST['total'],'catatandiskon'=> $_POST['catatandskn']);
				$this->db->where('nona', $na);				
				$notaakhir = $this->db->update('notaakhir', $dataArrayn);
				
				if($kui && $kdt)
				{
					$this->db->trans_commit();
					$ret["success"] = true;
					$ret["nona"]=$na;
				}else{
					$this->db->trans_rollback();
					$ret["success"] = false;
				}
			}			
		} 
		echo json_encode($ret);
    }
	
	function insorupd_paket($idtarifpaket, $nota){
		$query = $this->db->getwhere('tarifpaketdet',array('idtarifpaket'=>$idtarifpaket));
		$paketdet = $query->result();
		
		$q = $this->db->getwhere('notadet',array('kditem'=>$idtarifpaket, 'nonota'=>$nota));
		$nd = $q->row_array();
		
		foreach($paketdet AS $z=>$valz){
			$strbrb = substr($valz->kdtarif, 0, 1);
			if($strbrb == 'B'){
				$query = $this->db->getwhere('barang',array('kdbrg'=>$valz->kdtarif));
				$brg = $query->row_array();
			//	$idsatuan = $brg->idsatuankcl;
				$idsatuan = null;
			} else {
				$idsatuan = null;
			}
			
			$dataArray = array(
				'idnotadet'=> $nd['idnotadet'],
				'kditem'=> $valz->kdtarif,
				'idjnstarif'=> $valz->idjnstarif,
				'idsatuan'=> $idsatuan,
				'qty'=> $valz->qty,
				'tarifjs'=> $valz->tarifjs,
				'tarifjm'=> $valz->tarifjm,
				'tarifjp'=> $valz->tarifjp,
				'tarifbhp'=> $valz->tarifbhp,
			);
			
			$w =$this->db->insert('notapaket',$dataArray);
		}
		return true;
    }
	
	function insert_notaakhir($kui){
		$dataArray = $this->getFieldsAndValuesNona($kui);
		$notaakhir = $this->db->insert('notaakhir',$dataArray);
		
		if($notaakhir){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }

	function insorupd_deposit(){
        $this->db->trans_begin();
		
		$_POST['idjnskuitansi'] = 5;
		$_POST['tglnota'] = date('Y-m-d');
		$_POST['jamnota'] = date('H:i:s');
		/* $query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg']));
		$reg = $query->row_array(); */
		
		$kque = $this->insert_kuitansi(false);
		$kui = $kque['nokuitansi'];
		if($kui)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nokui"]=$kui;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_kuitansi($nota){
		if(!$nota){
			$dataArray = $this->getFieldsAndValuesKui($nota);
			$kuitansi = $this->db->insert('kuitansi',$dataArray);
		} else {
			$dataArray = $this->update_kuitansi($nota);
		}
		
		$query = $this->db->getwhere('kuitansidet',array('nokuitansi'=>$dataArray['nokuitansi']));
		$kuitansidet = $query->row_array();
		if($query->num_rows() == 0){
			$kuitansidet = $this->insert_kuitansidet($dataArray);
		} else {
			$kuitansidet = $this->update_kuitansidet($dataArray);
		}
		
		if($kuitansidet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
		
	function insert_kuitansidet($kuitansi){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrcarabayar']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesKuiDet($kuitansi, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('kuitansidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
		
	function insert_nota($reg, $kui){
		$dataArray = $this->getFieldsAndValues($reg, $kui);
		$z =$this->db->insert('nota',$dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
		
	function insert_notadet($nota, $reg, $arrnota, $idbagian){
		$where['nonota'] = $nota;
		//update brgbagian
		$query = $this->db->getwhere('notadet',$where);
		$ceknd = $query->row_array();
		if($ceknd && !in_array($_POST['jtransaksi'],array(4,7)))$stok = $this->tambahStok($where, $idbagian);
		if($ceknd)$this->db->delete('notapaket',array('idnotadet'=>$ceknd['idnotadet']));
		$this->db->delete('notadet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $arrnota);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-',$val);
			if($val != '' && $vale[0] != "null") {
				if(!isset($vale[9]))$vale[9]=0;
				
				if ($_POST['ureg'] != 'FA' && (!isset($vale[8]) || $vale[8]=='')) $dokter=null;
				else if(!isset($vale[8]) || $vale[8]=='null') $dokter=$reg['iddokter'];
				else $dokter=$this->searchIdkhususdr('iddokter','dokter','nmdoktergelar',$vale[8]);
				//if((!isset($vale[8]) || !$dokter) && ($vale[8]=='' || !$dokter) )$dokter=$this->searchIdkhususdr('iddokter','dokter','nmdoktergelar',$vale[8]);
				
				$dataArray = $this->getFieldsAndValuesDet($nota, $reg, $vale[0], $vale[1], $vale[2], $vale[3], $vale[4], $vale[5], $vale[6], $vale[7], $dokter, $vale[9]);
				$z =$this->db->insert('notadet',$dataArray);
				if($ceknd && !in_array($_POST['jtransaksi'],array(4,7)))$stok = $this->kurangStok($idbagian, $vale[0], $vale[1], $vale[2]);
				
				$strbrp = substr($vale[0], 0, 1);
				if($strbrp == 'P'){
					$this->insorupd_paket($vale[0], $nota);
				}
			}
		}
        return true;
    }
	
	function insert_kartustok(){
		/* $stkeluar			= $this->input->post("stkeluar");
		$carakeluar			= $this->input->post("carakeluar");
		if(!$stkeluar) $stkeluar = 1;
		if(!$carakeluar) $carakeluar = 1; */
		
		$query = $this->db->getwhere('kartustok',array('noref'=>$_POST['noref']));
		$cekks = $query->num_rows();
		
		if($cekks < 1){
			$k=array('[',']','"');
			$r=str_replace($k, '', $_POST['arrnota']);
			$b=explode(',', $r);
			foreach($b AS $val){
				$vale = explode('-',$val);
				
				$query = $this->db->getwhere('bagian',array('nmbagian'=>$vale[4]));
				$rec = $query->row_array();
					
				$where = $vale[5].' ORDER BY ';
				$this->db->select("saldoakhir", false);
				$this->db->from("kartustok");
				$this->db->where("idbagian", $rec['idbagian']);
				$this->db->where("kdbrg", $vale[5]);
				$this->db->order_by("tglinput DESC");
				$query = $this->db->get();
				$recks = $query->row_array();
				/*var_dump($recks);
				exit;
				*/if(!$recks) $recks['saldoakhir'] = 0;
				$saldoakhir = $recks['saldoakhir'] - $vale[3];
				$tgltransaksi = date_format(date_create($vale[1]), 'Y-m-d');
				
				
				$query = $this->db->getwhere('barang',array('kdbrg'=>$vale[5]));
				$hrgbrg = $query->row_array();
				
				/* $qry = $this->db->getwhere('barangbagian',array('kdbrg'=>$vale[5], 'idbagian'=>$rec['idbagian']));
				$rec1 = $qry->row_array();
				
				$saldoakhir = $rec1['stoknowbagian'] - $vale[3]; */
				
				if($query->num_rows() > 0){
					$this->db->select("SUM(jmlkeluar) AS jmlkeluar", false);
					$this->db->from("kartustok");
					$this->db->where("idbagian", $rec['idbagian']);
					$this->db->where("kdbrg", $vale[5]);
					$this->db->order_by("tglinput DESC");
					$qry = $this->db->get();
					$jmlk = $qry->row_array();

					$qq = $this->db->orderby('tglinput ASC')->getwhere('kartustok',array('kdbrg'=>$vale[5], 'idbagian'=>$rec['idbagian'], 'jmlmasuk > 0'=>null));
					$ks = $qq->result();
					$hrgbeli = 0;
					$c1 = 0;
					if($qq->num_rows()>0){
						$jmlkeluar = $jmlk['jmlkeluar'];
						foreach($ks AS $val1){
							if($c1 == 0){
								$qw = $this->db->select('SUM(jmlkeluar) AS sumj')->getwhere('kartustok',array('kdbrg'=>$vale[5], 'idbagian'=>$rec['idbagian'], 'tglinput < '=>$val1->tglinput));
								$sumj = $qw->row_array();
								$val1->jmlmasuk += $sumj['sumj'];
								$c1 = 1;
							}
							$jmlkeluar -= $val1->jmlmasuk;
							if($jmlkeluar<1 && $hrgbeli==0) $hrgbeli=$val1->hrgbeli;
						}
						if($hrgbeli == 0) $hrgbeli = $hrgbrg['hrgbeli'];
					} else {
						$hrgbeli = $hrgbrg['hrgbeli'];
					}
					/* var_dump($jmlkeluar);
					exit; */ #kartustock
					
					$this->db->query("CALL SP_insert_transaksi (?,?,?,?,?,?,?,?,?,?,?,?,?)",
								array(
									$vale[0],
									$tgltransaksi,
									$vale[2],
									$vale[3],
									$saldoakhir,
									$this->session->userdata['user_id'],
									$rec['idbagian'],
									$_POST['noref'],
									$rec['kdbagian'],
									$rec['nmbagian'],
									$vale[5],
									$hrgbeli,
									$hrgbrg['hrgjual']
								));
				}
			//	if($this->input->post("ri")){
					$kdr = 11;
					//if($rec['idbagian'] != 11) $kdr = 'null';
					$stok = $this->kurangStok($rec['idbagian'], $vale[5], $vale[3], $kdr);
					$stokbhp = $this->kurangStokBhp($rec['idbagian'], $vale[5], $vale[3], $kdr,$vale[1],$vale[2]);
			//	}
			}
		
			//UPDATE
			/* if(isset($_POST['regis'])){
				$query = $this->db->getwhere('registrasidet',array('noreg'=>$_POST['noreg']));
				$reg = $query->row_array();
				if(is_null($reg['tglkeluar'])){
					$dataArray = array(
						'tglkeluar' => date('Y-m-d'),
						'jamkeluar' => date('H:i:s'),
						/* 'idstkeluar' => $stkeluar,
						'idcarakeluar' => $carakeluar, */
					/* );
					$this->db->where('noreg', $_POST['noreg']);
					$z = $this->db->update('registrasidet', $dataArray);
					$this->update_posisispasien($reg['idregdet']);
				}
			} */
		}
		
        return true;
    }
	
	
	function insert_pasienluar(){
        $this->db->trans_begin();
		$_POST['jamnota'] = date('H:i:s');
		if($_POST['tf_dokter'] != '') $reg['iddokter'] = $this->searchId('iddokter','dokter','nmdoktergelar',$_POST['tf_dokter']);
		else $reg['iddokter'] = null;
		$reg['idbagian'] = 11;
		$reg['idregdet'] = null;
		$_POST['nonota'] = $_POST['tf_nonota'];
		$_POST['nmshift'] = $_POST['tf_waktushift'];
		$_POST['catatan'] = $_POST['tf_catatannf'];
		$_POST['catatandskn'] = $_POST['tf_catatandsknnf'];
        //$_POST['diskon'] = $_POST['diskonr'];
        //$_POST['uangr'] = $_POST['uangr'];
        $_POST['noresep'] = $_POST['tf_noresep'];
        $_POST['stpelayanan'] = 'Umum';
		$_POST['tahun'] = 0;
		$_POST['jkel'] = null;
		$_POST['atasnama'] = $_POST['tf_nmpasien'];
		$total = str_replace(',', '', $_POST['tf_total2']);
		$_POST['total'] = $total;
		//$_POST['arrcarabayar'] = '["Tunai-undefined-undefined-'. $total .'"]';
	
	//kuitansi dan kuitansi detail
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['tf_nonota']));
		$nota = $query->row_array();
		
		$kque = $this->insert_kuitansi($nota);
		$kui = $kque['nokuitansi'];
	
	//nota
		if($query->num_rows() == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			//$nota = $query->row_array();
			$vnota = $nota['nonota'];
		}
		$arrnota = $_POST['arrnota'];
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $reg['idbagian']);
		
		if($kui && $nota && $notadet)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$vnota;
            $ret["nokuitansi"]=$kui;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
	
		/* $dataArray = $this->getFieldsAndValuesPLuar($kui);
		$n =$this->db->insert('nota',$dataArray);
		$nota = $dataArray['nonota'];
	
	//notadet
		$where['nonota'] = $nota;
		$this->db->delete('notadet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrnota']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-',$val);
			$dataArray = $this->getFieldsAndValuesDet($nota, $reg, $vale[0], $vale[1], $vale[2], $vale[3]);
			$ndet =$this->db->insert('notadet',$dataArray);
		}
		
        if($n && $ndet && $kui)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$nota;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret); */
    }
	
	function insert_pelayanantambahan(){
        $this->db->trans_begin();
		$_POST['jamnota'] = date('H:i:s');
		if($_POST['tf_dokter'] != '') $reg['iddokter'] = $this->searchId('iddokter','dokter','nmdoktergelar',$_POST['tf_dokter']);
		else $reg['iddokter'] = null;
		$reg['idbagian'] = 35;
		$reg['idregdet'] = null;
		$_POST['nonota'] = $_POST['tf_nonota'];
		$_POST['nmshift'] = $_POST['tf_waktushift'];
		$_POST['catatan'] = $_POST['tf_catatan'];
		$_POST['catatandskn'] = $_POST['tf_catatandskn'];
        $_POST['diskon'] = null;
        $_POST['uangr'] = null;
        $_POST['noresep'] = null;
        $_POST['stpelayanan'] = 'Umum';
		$_POST['tahun'] = 0;
		$_POST['jkel'] = null;
		$_POST['atasnama'] = $_POST['tf_nmpasien'];
		$total = str_replace(',', '', $_POST['tf_total']);
		$_POST['total'] = $total;
		//$_POST['arrcarabayar'] = '["Tunai-undefined-undefined-'. $total .'"]';
	
	//kuitansi dan kuitansi detail
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['tf_nonota']));
		$nota = $query->row_array();
		
		$kque = $this->insert_kuitansi($nota);
		$kui = $kque['nokuitansi'];
	//nota
		if($query->num_rows() == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			//$nota = $query->row_array();
			$vnota = $nota['nonota'];
		}
		$arrnota = $_POST['arrnota'];
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $reg['idbagian']);
		
		if($kui && $nota && $notadet)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$vnota;
            $ret["nokuitansi"]=$kui;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function test(){
		$query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg']));
		$reg = $query->row_array();
		
		$query = $this->db->getwhere('nota',array('idregdet'=>$reg['idregdet']));
		$nota = $query->row_array();
		
		$ret["success"] = true;
		$ret["nonota"]=$nota['nonota'];
		
		echo json_encode($ret);
    }

	function update_nota($reg, $kui){
		$dataArray = $this->getFieldsAndValues($reg, $kui);
		
		//UPDATE
		$this->db->where('nonota', $_POST['nonota']);
		$z = $this->db->update('nota', $dataArray); 
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function update_kuitansi($nota){
		$dataArray = $this->getFieldsAndValuesKui($nota);
		
		//UPDATE
		$this->db->where('nokuitansi', $dataArray['nokuitansi']);
		$kuitansi = $this->db->update('kuitansi', $dataArray);
		
		return $dataArray;
    }
	
	function update_kuitansidet($kuitansi){
		$where['nokuitansi'] = $kuitansi['nokuitansi'];
		$this->db->delete('kuitansidet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrcarabayar']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesKuiDet($kuitansi, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('kuitansidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function update_transfer(){
		$dataArray = array(
			'idregdettransfer' => $_POST['transfer']
		);
		$this->db->where('nonota', $_POST['nonota']);
		$z = $this->db->update('nota', $dataArray);
        if($z){
			$query = $this->db->getwhere('nota',array('nonota'=>$_POST['nonota']));
			$nokui = $query->row_array();
			
			$dataArrayx = array(
				'idcarabayar' => 9
			);
			$this->db->where('nokuitansi', $nokui['nokuitansi']);
			$y = $this->db->update('kuitansidet', $dataArrayx);
			
			$this->update_posisispasien($_POST['idtransfer']);
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function update_posisispasien($idregdet){
		$dataArray = array(
			'idstposisipasien' => $_POST['stposisipasien']
		);
		$this->db->where('idregdet', $idregdet);
		$z = $this->db->update('reservasi', $dataArray); 
        if($z){
            $ret=$z;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function getFieldsAndValuesNona($kui){
		$nr = $this->getNonotaakhir();
		$nostart = 'NA';
		$nomid = date('y');
		$noend = str_pad($nr, 8, "0", STR_PAD_LEFT);
		$nona = $nostart.$nomid.$noend;
		
		if(!isset($_POST['plafond'])) $_POST['plafond'] = 'false';
		if($_POST['plafond']=='true'){
			$plafond = $_POST['ttlplafond'];
			$stplafond = 1;
		} else {
			$plafond = 0;
			$stplafond = 0;
		}
		
		if(!isset($_POST['tgltrans'])) $tgltrans = date('Y-m-d');
		else $tgltrans = $_POST['tgltrans'];
		
		if(!isset($_POST['jamtrans'])) $jamtrans = date('H:i:s');
		else $jamtrans = $_POST['jamtrans'];
			
		$dataArray = array(
             'nona'=> $nona,
             'tglna'=> $tgltrans,
             'jamna'=> $jamtrans,
             'idsttransaksi'=> 1,
             'nokuitansi'=> $kui,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'total1'=> $_POST['total'],
             'userid'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d H:i:s'),
             'plafond'=> $plafond,
             'stplafond'=> $stplafond,
             'catatandiskon'=> $_POST['catatandskn']
        );
		return $dataArray;
	}
	
	function getFieldsAndValuesKui($nota){
		if(!$nota){
			$nr = $this->getNokuitansi();
			$nostart = 'NK';
			$nomid = date('y');
			$noend = str_pad($nr, 8, "0", STR_PAD_LEFT);
			$nokuitansi = $nostart.$nomid.$noend;
		} else{
			$nokuitansi = $nota['nokuitansi'];
		}
		
		if(!isset($_POST['idregdet']))$idregdet=null;
		else $idregdet=$_POST['idregdet'];
		
		if(!isset($_POST['idjnskuitansi']))$idjnskuitansi=1;
		else $idjnskuitansi=$_POST['idjnskuitansi'];
		
		if(!isset($_POST['pembayaran'])) $_POST['pembayaran'] = $_POST['total'];
		
		if(!isset($_POST['tgltrans'])) $tgltrans = $_POST['tglnota'];
		else $tgltrans = $_POST['tgltrans'];
		
		if(!isset($_POST['jamtrans'])) $jamtrans = $_POST['jamnota'];
		else $jamtrans = $_POST['jamtrans'];
		
		$dataArray = array(
             'nokuitansi'=> $nokuitansi,
             'tglkuitansi'=> $tgltrans,
             'jamkuitansi'=> $jamtrans,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'idstkuitansi'=> 1,
             'idsbtnm'=> $this->searchSbtnm('idsbtnm','jsbtnm',$_POST['tahun'],$_POST['jkel']),
             'atasnama'=> $_POST['atasnama'],
             'idjnskuitansi'=> $idjnskuitansi,
             'pembulatan'=> 0,
             'total'=> $_POST['total'],
             'pembayaran'=> $_POST['pembayaran'],
             'idregdet'=> $idregdet,
             //'ketina'=> 0,
             //'keteng'=> 0
        );
		return $dataArray;
	}
			
	function getFieldsAndValuesKuiDet($kuitansi, $val0, $val1, $val2, $val3){
		if($val1 == 'undefined')$idbank = null;
		else $idbank = $this->searchId('idbank','bank','nmbank',$val1);
		if($val2 == 'undefined' || $val2 == ''|| $val2 == 'null')$nokartu = null;
		else $nokartu = $val2;
		$dataArray = array(
			 'nokuitansi'=> $kuitansi['nokuitansi'],
			 'idbank'=> $idbank,
			 'idcarabayar'=> $this->searchId('idcarabayar','carabayar','nmcarabayar',$val0),
			 'nokartu'=> $nokartu,
			 'jumlah'=> $val3
		);
		return $dataArray;
	}
	
	function getFieldsAndValuesKuiDetup($kuitansi, $val0, $val1, $val2, $val3){
		if($val1 == 'undefined')$idbank = null;
		else $idbank = $this->searchId('idbank','bank','nmbank',$val1);
		if($val2 == 'undefined' || $val2 == ''|| $val2 == 'null')$nokartu = null;
		else $nokartu = $val2;
		$dataArray = array(
			 'nokuitansi'=> $kuitansi,
			 'idbank'=> $idbank,
			 'idcarabayar'=> $this->searchId('idcarabayar','carabayar','nmcarabayar',$val0),
			 'nokartu'=> $nokartu,
			 'jumlah'=> $val3
		);
		return $dataArray;
	}
	
	function getFieldsAndValues($reg, $kui){
		if(is_null($_POST['nonota']) || $_POST['nonota'] == '')
		{
			$nr = $this->getNonota();
			$nostart = 'N'.$_POST['ureg'];
			$nomid = date('y');
			$nombln = date('m');
			$noend = str_pad($nr, 5, "0", STR_PAD_LEFT);
			$nonota = $nostart.$nomid.$nombln.$noend;
		} else{
			$nonota = $_POST['nonota'];
		}
		
		if(!isset($_POST['diskon']))$_POST['diskon']=0;
		
		if(!isset($_POST['uangr']))$_POST['uangr']=0;
		
		if(!isset($_POST['stpelayanan']))$stpelayanan=null;
		else $stpelayanan=$this->searchId('idstpelayanan','stpelayanan','nmstpelayanan',$_POST['stpelayanan']);
		
		if(!isset($_POST['plafond'])) $_POST['plafond'] = 'false';
		
		if($_POST['plafond']=='true'){
			$plafond = $_POST['ttlplafond'];
			$stplafond = 1;
		} else {
			$plafond = 0;
			$stplafond = 0;
		}
		
		if(!isset($_POST['catatandskn']))$_POST['catatandskn']=null;
		
		if(!isset($_POST['idbagianfar']))$idbagianfar=null;
		else $idbagianfar=$_POST['idbagianfar'];
		$dataArray = array(
             'nonota'=> $nonota,
             //'nona'=> $nonota,
             'tglnota'=> $_POST['tglnota'],
             'jamnota'=> $_POST['jamnota'],
             'idbagian'=> $reg['idbagian'],
             'idbagianfar'=> $idbagianfar,
             'iddokter'=> $reg['iddokter'],
             'idjnstransaksi'=> $_POST['jtransaksi'],
             'idsttransaksi'=> 1,
             'nokuitansi'=> $kui,
             'idregdet'=> $reg['idregdet'],
             'noorder'=> 1,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'noresep'=> $_POST['noresep'],
             'catatan'=> $_POST['catatan'],
             'catatandiskon'=> $_POST['catatandskn'],
             'diskon'=> $_POST['diskon'],
             'uangr'=> $_POST['uangr'],
             'userinput'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d'),
			 'idregdettransfer'=> null,
			 'idstpelayanan'=> $stpelayanan,
			 'plafond'=> $plafond,
			 'stplafond'=> $stplafond
        );
		return $dataArray;
	}
	
	function getFieldsAndValuesDet($nota, $reg, $val0, $val1, $val2, $val3, $val4, $val5, $val6, $val7, $val8, $val9){
		if ($val2 == 'null') $val2 = null;
		if(!(int)$val4)$val4 = 0;
		if(!(int)$val5)$val5 = 0;
		if(!(int)$val6)$val6 = 0;
		if(!(int)$val7)$val7 = 0;
		
		if($val9>0)$stdijamin = 1;
		else $stdijamin = 0;
		
		$query = $this->db->getwhere('barang',array('kdbrg'=>$val0));
		$barang = $query->row_array();
		if($barang){
			$hrgjual = $barang['hrgjual'];
			$hrgbeli = $barang['hrgbeli'];
		}else{
			$hrgjual = null;
			$hrgbeli = null;
		}
		
		if($val2){
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'idbagian'=>11));
		} else {
			$strbr = substr($val0, 0, 1);
			if($strbr == 'B'){
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'idbagian'=>$reg['idbagian']));
			} elseif($strbr == 'T'){
				/* if($reg['idjnspelayanan'] != 2){
					$reg['idklstarif'] = '';
				} */
				if(!isset($reg['idklstarif'])) $reg['idklstarif'] = 5;
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'klstarif'=>$reg['idklstarif']));
				if(!$query->row_array()){
					$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'klstarif'=>5));
				}
			} else { 
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0));
			}
		}
		$item = $query->row_array();
		/* if(!$item){
			$query = $this->db->getwhere('tarif',array('kdpelayanan'=>$val0));
			$item = $query->row_array();
			$item['satuankcl'] = null;
		} */
		if(!isset($item['tarifjs']) || $item['tarifjs']=='')$item['tarifjs'] = 0;
		if(!isset($item['tarifjm']) || $item['tarifjm']=='')$item['tarifjm'] = 0;
		if(!isset($item['tarifjp']) || $item['tarifjp']=='')$item['tarifjp'] = 0;
		if(!isset($item['tarifbhp']) || $item['tarifbhp']=='')$item['tarifbhp'] = 0;
		
		$diskon = ($val1*((int)$item['tarifjs']+(int)$item['tarifjm']+(int)$item['tarifjp']+(int)$item['tarifbhp'])) - $val3;
		if(!isset($_POST['plafond'])) $_POST['plafond'] = 'false';
		if($_POST['plafond']=='true'){
			$val9 = ($item['tarifjs']+$item['tarifjm']+$item['tarifjp']+$item['tarifbhp']-$val4-$val5-$val6-$val7)*$val1;
			$stdijamin = 1;
		}
		
		if(isset($item['satuankcl'])) $item['satuankcl'] = $this->searchId('idsatuan','jsatuan','nmsatuan',$item['satuankcl']);
		else $item['satuankcl'] = null;
		$dataArray = array(
             'nonota'=> $nota,
             'kditem'=> $val0,
             'koder'=> $val2,
             //'idjnstarif'=> 0,
             //'kdresep'=> $_POST['kdnota'],
             'idsatuan'=> $item['satuankcl'],
             'qty'=> $val1,
             'tarifjs'=> $item['tarifjs'],
             'tarifjm'=> $item['tarifjm'],
             'tarifjp'=> $item['tarifjp'],
             'tarifbhp'=> $item['tarifbhp'],
             'diskonjs'=> $val4,
             'diskonjm'=> $val5,
             'diskonjp'=> $val6,
             'diskonbhp'=> $val7,
             //'uangr'=> 0,
             'iddokter'=> $val8,
             //'idperawat'=> 1,
             'hrgjual'=> $hrgjual,
             'hrgbeli'=> $hrgbeli,
             'dijamin'=> $val9,
             'stdijamin'=> $stdijamin,
			 //'idstbypass'=> 1
        );
		return $dataArray;
	}
	
	function getFieldsAndValuesPLuar($kui){
		if(is_null($_POST['tf_nonota']) || $_POST['tf_nonota'] == '')
		{
			$nr = $this->getNonota();
			$nostart = 'N'.$_POST['ureg'];
			$nomid = date('y');
			$nombln = date('m');
			$noend = str_pad($nr, 5, "0", STR_PAD_LEFT);
			$nonota = $nostart.$nomid.$nombln.$noend;
		} else {
			$nonota = $_POST['tf_nonota'];
		}
		$dataArray = array(
             'nonota'=> $nonota,
             'nona'=> $nonota,
             'tglnota'=> $_POST['tglnota'],
             'jamnota'=> date('H:i:s'),
             'idbagian'=> 11,
             'iddokter'=> $this->searchId('idshift','shift','nmshift',$_POST['tf_waktushift']),
             'idjnstransaksi'=> 8,
             'idsttransaksi'=> 1,
             'nokuitansi'=> $kui,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['tf_waktushift']),
             'noresep'=> $_POST['tf_noresep'],
             'catatan'=> $_POST['tf_catatannf'],
             'diskon'=> $_POST['tf_diskonf'],
             'uangr'=> $_POST['tf_uangr'],
             'userinput'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d')
        );		
		return $dataArray;
	}
	
	function getNonota(){
       /* $this->db->select("count(nonota) AS max_np");
        $this->db->from("nota");
        $this->db->where('SUBSTRING(nonota,2,2)', $_POST['ureg']);
        $this->db->where('SUBSTRING(nonota,4,2)', date('y')); 
		$q = $this->db->get();
		$data = $q->row_array(); */
		$q = "SELECT SUBSTRING(max(nonota), 8, 5) AS max_np
		FROM (`nota`)
		WHERE SUBSTRING(nonota,2,2) = '".$_POST['ureg']."'
		AND SUBSTRING(nonota,4,2) = '".date('y')."'";
		$query = $this->db->query($q);

		$query->result();
		$data = $query->row_array();

		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function getNokuitansi(){
        $this->db->select("count(nokuitansi) AS max_np");
        $this->db->from("kuitansi");
        $this->db->where('SUBSTRING(nokuitansi,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function getNonotaakhir(){
        $this->db->select("count(nona) AS max_np");
        $this->db->from("notaakhir");
        $this->db->where('SUBSTRING(nona,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}

	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function searchIdkhususdr($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->like($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function searchSbtnm($select, $tbl, $val, $val2){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($val .' BETWEEN ', 'dariumur AND sampaiumur', false);
        $this->db->where('idjnskelamin', $val2);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function tambahStok($where, $idbagian){
        $query = $this->db->getwhere('notadet',$where);
		$ndold = $query->result_array();
		for($i=0;$i<$query->num_rows();$i++){
			$strbr = substr($ndold[$i]['kditem'], 0, 1);
			if($strbr == 'B'){
				($ndold[$i]['koder']) ? $idbag = 11 : $idbag = $idbagian;
				$q = $this->db->getwhere('barangbagian',array('kdbrg'=>$ndold[$i]['kditem'], 'idbagian'=>$idbag));
				$brgbagian = $q->row_array();
				$qty = $brgbagian['stoknowbagian'] + $ndold[$i]['qty'];
				
				$dataArray = array('stoknowbagian'=> $qty);
				$this->db->where('idbagian', $idbag);
				$this->db->where('kdbrg', $ndold[$i]['kditem']);
				$z = $this->db->update('barangbagian', $dataArray);
			}
		}
		return true;
    }
	
	function kurangStok($idbagian, $kdbrg, $qtynow, $koder){
		$strbr = substr($kdbrg, 0, 1);
		if($strbr == 'B'){
			$idbag = 11;
			$query = $this->db->getwhere('barangbagian',array('kdbrg'=>$kdbrg, 'idbagian'=>$idbag));
			$brgbagian = $query->row_array();
			$qty = $brgbagian['stoknowbagian'] - $qtynow;
			
			$dataArray = array('stoknowbagian'=> $qty);
			$this->db->where('idbagian', $idbag);
			$this->db->where('kdbrg', $kdbrg);
			$z = $this->db->update('barangbagian', $dataArray);
		}
		return true;
    }
    function kurangStokBhp($idbagian, $kdbrg, $qtynow, $koder, $tglkartustok, $jamkartustok){

    	// $query = $this->db->getwhere('kartustok',array('noref'=>$_POST['noref']));
		// $cekks = $query->num_rows();

	//	if($cekks < 1){				
			

			$sqlpeldet = "select * from pelayanandet_bhp_alkes where kdpelayanan = '".$kdbrg."'";
	    	$qpeldet = $this->db->query($sqlpeldet);
	    	$result_peldet = $qpeldet->result();
	    	$idbag = 11;

	    			
	    		//kurangi stok bhp yang ada di pelayanan
	    	if($qpeldet->num_rows() > 0){
	    		foreach ($result_peldet as $i => $val) {
	    			$query = $this->db->getwhere('bagian',array('idbagian'=>$idbag));
					$rec = $query->row_array();

					$where = $val->kdtarif.' ORDER BY ';
					$this->db->select("saldoakhir", false);
					$this->db->from("kartustok");
					$this->db->where("idbagian", $rec['idbagian']);
					$this->db->where("kdbrg", $val->kdtarif);
					$this->db->order_by("tglinput DESC");
					$query = $this->db->get();
					$recks = $query->row_array();
					/*var_dump($recks);
					exit;
					*/if(!$recks) $recks['saldoakhir'] = 0;
					$saldoakhir = $recks['saldoakhir'] - ($val->qty * $qtynow);
					$tgltransaksi = date_format(date_create($tglkartustok), 'Y-m-d');
	    				
	    			$query = $this->db->getwhere('barang',array('kdbrg'=>$val->kdtarif));
					$hrgbrg = $query->row_array();	
					
	    			$this->db->select("SUM(jmlkeluar) AS jmlkeluar", false);
					$this->db->from("kartustok");
					$this->db->where("idbagian", $idbagian);
					$this->db->where("kdbrg", $val->kdtarif);
					$this->db->order_by("tglinput DESC");
					$qry = $this->db->get();
					$jmlk = $qry->row_array();

					$qq = $this->db->orderby('tglinput ASC')->getwhere('kartustok',array('kdbrg'=>$val->kdtarif, 'idbagian'=>$idbagian, 'jmlmasuk > 0'=>null));
					$ks = $qq->result();
					$hrgbeli = 0;
					$c1 = 0;
					if($qq->num_rows()>0){
						$jmlkeluar = $jmlk['jmlkeluar'];
						foreach($ks AS $val1){
							if($c1 == 0){
								$qw = $this->db->select('SUM(jmlkeluar) AS sumj')->getwhere('kartustok',array('kdbrg'=>$val->kdtarif, 'idbagian'=>$idbagian, 'tglinput < '=>$val1->tglinput));
								$sumj = $qw->row_array();
								$val1->jmlmasuk += $sumj['sumj'];
								$c1 = 1;
							}
							$jmlkeluar -= $val1->jmlmasuk;
							if($jmlkeluar<1 && $hrgbeli==0) $hrgbeli=$val1->hrgbeli;
						}
						if($hrgbeli == 0) $hrgbeli = $hrgbrg['hrgbeli'];
					} else {
						$hrgbeli = $hrgbrg['hrgbeli'];
					}
					$tgltransaksi = date_format(date_create($tglkartustok), 'Y-m-d');
					$qty_terpotong = $val->qty * $qtynow;
					$this->db->query("CALL SP_insert_transaksi (?,?,?,?,?,?,?,?,?,?,?,?,?)",
								array(
									20, // Jenis Kartu Stok
									$tgltransaksi,
									$jamkartustok,
									$qty_terpotong,
									$saldoakhir,
									$this->session->userdata['user_id'],
									11, // DEFAULT FARMASI
									$_POST['noref'],
									$rec['kdbagian'],
									$rec['nmbagian'],
									$val->kdtarif,
									$hrgbeli,
									$hrgbrg['hrgjual']
								));
	    		

	    		$query_brgbagian = $this->db->getwhere('barangbagian',array('kdbrg'=>$val->kdtarif));
					$arrbrgbagian = $query_brgbagian->row_array();
				
					$qtypeldet = $val->qty * $qtynow;
					$qtybrgbagian = $arrbrgbagian['stoknowbagian'] - $qtypeldet;
					
					$dataArrayBrgbagian = array('stoknowbagian'=> $qtybrgbagian);
				//	$this->db->where('idbagian', 11);
					$this->db->where('kdbrg', $val->kdtarif);
					$z = $this->db->update('barangbagian', $dataArrayBrgbagian);
	    		}
	    	}
	//	}

    	
    	return true;
    }
	
	function getCekTransfer(){
		$this->db->select('kuitansi.total, nota.idregdet');
        $this->db->from('nota');
		$this->db->join('kuitansi',
                'kuitansi.nokuitansi = nota.nokuitansi', 'left');
        $this->db->where('idregdettransfer', $_POST['noreg']);
		$q = $this->db->get();
		$total = $q->row_array();
		
		if(!$total){
			$total['total']=0;
			$total['idregdet']=null;
		}
		
		echo json_encode($total);
    }
	
	function get_counnonota(){
		$noreg   = $this->input->post("noreg");
		
		$q = ("SELECT count(nonota) AS cnonota
				FROM
				  registrasidet rd
				LEFT JOIN nota nt
				ON nt.idregdet = rd.idregdet
				WHERE
				  rd.noreg = '".$noreg."'
				  AND
				  nt.idjnstransaksi = '7'");		
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_counnonotat(){
		$noreg   = $this->input->post("noreg");
		
		$q = ("SELECT count(nonota) AS cnonota
				FROM
				  registrasidet rd
				LEFT JOIN nota nt
				ON nt.idregdet = rd.idregdet
				WHERE
				  rd.noreg = '".$noreg."'
				  AND
				  nt.idjnstransaksi = '4'");		
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_tgl_svr() {
		$data['date'] = date('d-m-Y');
		echo json_encode($data);
    }
	
	function batal_transfarluar(){
		$nonota =  $this->input->post('nonota');
		$nokui =  $this->input->post('nokui');
	//	$passbatal =  $this->input->post('passbatal');
		$tglnota =  $this->input->post('tglnota');
		#$user = 'Batal';
	//	$cekpass = $this->id_field('userid', 'pengguna', 'password', base64_encode($passbatal));
		 
	//	if ($cekpass) {
			$data1 = array(
				 'idsttransaksi'=> 2,				 
				 );
			$data2 = array(
				 'idstkuitansi'=> 2,				 
				 );
			
			$this->db->trans_begin();
			
			$where1['nonota']=$nonota;			
			$this->db->where($where1);
			$this->db->update("nota", $data1);
			
			$where2['nokuitansi']=$nokui;
			$this->db->where($where2);
			$this->db->update("kuitansi", $data2);
			
			$arrnota = $_POST['arrnota'];
			$jam = date('H:i:s');
			$user = $this->my_usession->userdata('user_id');
			$k=array('[',']','"');
			$r=str_replace($k, '', $arrnota);
			$b=explode(',', $r);
			foreach($b AS $val){
				$vale = explode('-', $val);
				$this->db->query("CALL SP_update_brgbagian_dari_farpasluar (?,?,?,?,?,?,?)",
							 array(
								 $nonota,
								 $tglnota,
								 $jam,
								 $user,
								 11,
								 $vale[0],
								 $vale[1]
								));
			}
			
				if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				$return="Batal gagal";
			}
			else
			{
				$this->db->trans_commit();
				$return="Batal Berhasil";
			}
			echo $return;
			
	/*	} else {
			$return = "Password Salah";
			echo $return;
		}*/
    }
	
	function batal_transpeltambahan(){
		$nonota =  $this->input->post('nonota');
		$nokui =  $this->input->post('nokui');
		$passbatal =  $this->input->post('passbatal');
		#$user = 'Batal';
		$cekpass = $this->id_field('userid', 'pengguna', 'password', base64_encode($passbatal));
		 
		if ($cekpass) {
			$data1 = array(
				 'idsttransaksi'=> 2,				 
				 );
			$data2 = array(
				 'idstkuitansi'=> 2,				 
				 );
			
			$this->db->trans_begin();
			
			$where1['nonota']=$nonota;			
			$this->db->where($where1);
			$this->db->update("nota", $data1);
			
			$where2['nokuitansi']=$nokui;
			$this->db->where($where2);
			$this->db->update("kuitansi", $data2);
			
				if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				$return="Batal gagal";
			}
			else
			{
				$this->db->trans_commit();
				$return="Batal Berhasil";
			}
			echo $return;
			
		} else {
			$return = "Password Salah";
			echo $return;
		}
    }
	
	function batal_transdeposit(){
		$nokui =  $this->input->post('nokui');
		$passbatal =  $this->input->post('passbatal');
		#$user = 'Batal';
		$cekpass = $this->id_field('userid', 'pengguna', 'password', base64_encode($passbatal));
		 
		if ($cekpass) {
			$data2 = array(
				 'idstkuitansi'=> 2,				 
				 );
			
			$this->db->trans_begin();
			
			$where2['nokuitansi']=$nokui;
			$this->db->where($where2);
			$this->db->update("kuitansi", $data2);
			
				if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				$return="Batal gagal";
			}
			else
			{
				$this->db->trans_commit();
				$return="Batal Berhasil";
			}
			echo $return;
			
		} else {
			$return = "Password Salah";
			echo $return;
		}
    }
	
	function id_field($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where userid = 'Batal' AND ".$whereb." = '".$wherea."' " ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }
}
