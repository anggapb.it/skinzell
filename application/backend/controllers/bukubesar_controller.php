<?php
/*
  Saldo Awal Akun per tahun = saldoawal akun tahun + (total debit awal tahun hingga tglawal) - (total kredit)
*/

class Bukubesar_controller extends Controller {
  var $bulan = array();
  var $bulanSaldoAwal = array();
  var $bulanSaldoDebit = array();
  var $bulanSaldoCredit = array();
  var $bulanSaldoAkhir = array();

  public function __construct()
  {
    parent::Controller();
    $this->load->library('pdf_lap');
    $this->load->library('session');
    $this->load->library('lib_phpexcel');

  }
  
  function get_bukubesar(){
    $tglawal   = $this->input->post("tglawal");
    $tglakhir  = $this->input->post("tglakhir");

    if(empty($tglawal) || $tglakhir == ''){
      $build_array = array ("success"=>true,"results"=>0,"data"=>array());
      echo json_encode($build_array);
      die();
    }
    
    $tahun = date('Y',strtotime($tglawal));
    $bulan = date('m',strtotime($tglawal));

    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun, 'idstatus' => 1))->result();
    if(!empty($data_akun_tahun))
    {
      foreach($data_akun_tahun as $idx => $akun_tahun)
      {

        $data_akun_tahun[$idx]->calc_saldoawal = $this->calc_saldo_awal($akun_tahun->idakun, $tglawal, $tglakhir);
        
        /*
          @menghitung total transaksi debit
        */
        $data_akun_tahun[$idx]->totaldebit = $this->count_total_debit($akun_tahun->idakun, $tglawal, $tglakhir);

        /*
          @menghitung total transaksi kredit
        */
        $data_akun_tahun[$idx]->totalkredit = $this->count_total_credit($akun_tahun->idakun, $tglawal, $tglakhir);
        
        /*
          @menghitung total saldo akhir
        */
          $data_akun_tahun[$idx]->saldoakhir = $data_akun_tahun[$idx]->calc_saldoawal + 
                                               $data_akun_tahun[$idx]->totaldebit -
                                               $data_akun_tahun[$idx]->totalkredit;
      }
    }

    $build_array = array ("success"=>true,"results"=> count($data_akun_tahun),"data"=>$data_akun_tahun);
    echo json_encode($build_array);  
  }

  function get_bukubesar_bank(){
    $tglawal    = $this->input->post("tglawal");
    $tglakhir    = $this->input->post("tglakhir");
    $idakun = 135; //akun kas di bank

    if(empty($tglawal) || $tglawal == ''){
      $build_array = array ("success"=>true,"results"=>0,"data"=>array());
      echo json_encode($build_array);
      die();
    }

    $query = "
    SELECT 
      jurnaldet.noreff as idbank
      , ifnull((select bank.nmbank as nmbank from bank where bank.idbank = jurnaldet.noreff), '') as nmbank
      , sum(debit) AS tot_debit
      , sum(kredit) AS tot_kredit
      FROM
        jurnaldet, jurnal
      WHERE
        jurnal.kdjurnal = jurnaldet.kdjurnal
        AND jurnal.status_posting = 1
        AND jurnal.tgltransaksi >= '".$tglawal."'
        AND jurnal.tgltransaksi <= '".$tglakhir."'
        AND jurnaldet.idakun = '".$idakun."'
      GROUP BY
        jurnaldet.noreff
    ";
    $data = $this->db->query($query)->result_array();
    $build_array = array ("success"=>true,"results"=> count($data),"data"=>$data);
    echo json_encode($build_array);  
  }

  function get_bukujurnal(){
    $tglawal   = $this->input->post("tglawal");
    $tglakhir  = $this->input->post("tglakhir");
    $idakun    = $this->input->post("idakun");

    if(empty($tglawal) || empty($tglakhir)){
      $build_array = array ("success"=>true,"results"=>0,"data"=>array());
      echo json_encode($build_array);
      die();
    }

    $this->db->select('*');
    $this->db->from('v_bukujurnal');
    $this->db->orderby('tgltransaksi', 'ASC');
    $this->db->where('status_posting', '1');
    $this->db->where('idakun', $idakun);
    $this->db->where('tgltransaksi >=', $tglawal);
    $this->db->where('tgltransaksi <=', $tglakhir);
      
    $query = $this->db->get();

    $data = $query->result();
    $total = $query->num_rows();

    $build_array = array ("success"=>true,"results"=> $total,"data"=>$data);
    echo json_encode($build_array);  
  }

  function get_bukujurnal_bank(){
    $tglawal     = $this->input->post("tglawal");
    $tglakhir    = $this->input->post("tglakhir");
    $idbank      = $this->input->post("idbank");
    $idakun      = 135; //akun kas di bank


    if(empty($tglawal) || $tglakhir == ''){
      $build_array = array ("success"=>true,"results"=>0,"data"=>array());
      echo json_encode($build_array);
      die();
    }

    $this->db->select('*');
    $this->db->from('v_bukujurnal');
    $this->db->orderby('tgltransaksi', 'ASC');
    $this->db->where('status_posting', '1');
    $this->db->where('idakun', $idakun);
    $this->db->where('noreff_jurnaldet', $idbank);
    $this->db->where('tgltransaksi >=', $tglawal);
    $this->db->where('tgltransaksi <=', $tglakhir);
      
    $query = $this->db->get();

    $data = $query->result();
    $total = $query->num_rows();

    $build_array = array ("success"=>true,"results"=> $total,"data"=>$data);
    echo json_encode($build_array);  
  }

  function calc_saldo_awal($idakun = '', $tglawal = '', $tglakhir = '')
  { 
    /*
      Metode Penghitungan Saldo Awal di Buku Besar (Menggunakan periode tanggal)
      jika kode akun 1, 2, 3 (Harta, Kewajiban & Hutang dan Modal) maka saldo awal mengambil saldo pada bulan inputan tanggal awal
      contoh : 
      periode 23 April 2015 - 4 mei 2015
      karena awal periode adalah 23 april maka saldo awal mengambil saldo awal mei (periode bulan mei = 21 april - 20 mei).
      kemudian saldo awal mei dikalkulasi dengan saldo debit & kredit dari tanggal 21 april sampai 23 april. maka didapatlah saldo awal 23 april 2015.
    */

    $tahun = date('Y',strtotime($tglawal));
    $bulan = date('m',strtotime($tglawal));
    $tgl = date('d',strtotime($tglawal));
    $akun      = $this->db->get_where('akun', array('idakun' => $idakun))->row_array();
    $matching_bulan = array(
      '1'   => 'jan',   '2'   => 'feb',    '3'   => 'mar',
      '4'   => 'apr',   '5'   => 'mei',    '6'   => 'jun',
      '7'   => 'jul',   '8'   => 'ags',    '9'   => 'sep',
      '10'  => 'okt',   '11'  => 'nov',    '12'  => 'des',
    );

    #if($akun['idklpakun'] == 1 || $akun['idklpakun'] == 2 || $akun['idklpakun'] == 3){
      if($tgl > 20){
        $bulan = $bulan + 1;

      }else{
        $bulan = $bulan + 0; //it's a tricky code        
      }

      if($bulan > 12){
        $bulan = 1;
        $tahun = $tahun + 1; // it's a tricky too        
      }

      $akuntahun = $this->db->get_where('akuntahun', array('idakun' => $idakun, 'tahun' => $tahun))->row_array();
      $field_saldo_awal_debit = 'saldodebit'.$matching_bulan[$bulan];
      $field_saldo_awal_kredit = 'saldocredit'.$matching_bulan[$bulan]; 
      
      if($bulan == 1){
        $tgl_hitung = ($tahun-1).'-12-21';
      }else{
        $tgl_hitung = $tahun.'-'.($bulan-1).'-21';
      }

      //pengambilan mutasi dari saldo awal (tanggal 21) sampai tanggal tglawal
      $query = $this->db->query("SELECT (sum(debit) - sum(kredit)) AS mutasi
                FROM
                  jurnaldet, jurnal
                WHERE
                  jurnaldet.kdjurnal = jurnal.kdjurnal AND
                  jurnaldet.idakun = '{$idakun}' AND 
                  jurnal.status_posting = '1' AND 
                  jurnal.tgltransaksi >= '{$tgl_hitung}' AND 
                  jurnal.tgltransaksi < '{$tglawal}' 
      ")->row_array();
      $mutasi = $query['mutasi'];

      $saldoawal = $akuntahun[$field_saldo_awal_debit] - $akuntahun[$field_saldo_awal_kredit] + $mutasi;
      return $saldoawal;

    #}else{
    #  return 0;
    #}

  }

  function count_total_debit($idakun = '', $tglawal = '', $tglakhir = '')
  {
    $get_calc_debit  = $this->db->query("
                       SELECT ifnull(sum(jurnaldet.debit), 0) as totaldebit
                       FROM
                         jurnaldet, jurnal
                       WHERE
                         jurnal.kdjurnal = jurnaldet.kdjurnal AND
                         jurnaldet.idakun = '". $idakun ."' AND
                         jurnal.tgltransaksi  >= '".$tglawal."' AND
                         jurnal.tgltransaksi  <= '".$tglakhir."' AND
                         jurnal.status_posting = '1'
                      ")->row();
    $return = $get_calc_debit->totaldebit;
    return $return;
  }


  function count_total_credit($idakun = '', $tglawal = '', $tglakhir = '')
  {
    $get_calc_credit  = $this->db->query("
                       SELECT ifnull(sum(jurnaldet.kredit), 0) as totalkredit
                       FROM
                         jurnaldet, jurnal
                       WHERE
                         jurnal.kdjurnal = jurnaldet.kdjurnal AND
                         jurnaldet.idakun = '". $idakun ."' AND
                         jurnal.tgltransaksi >= '".$tglawal."' AND
                         jurnal.tgltransaksi <= '".$tglakhir."' AND
                         jurnal.status_posting = '1'
                      ")->row();
    
    $return = $get_calc_credit->totalkredit;
    return $return;
  }

  /* ========================== LAPORAN PDF ========================== */
  
  function lap_buku_besar_pdf($tahun = '', $bulan = ''){
    $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
    $fieldbulanSaldoAwal = ($bulan != '00') ? $this->bulanSaldoAwal[$bulan] : '';
    $fieldbulanSaldoDebit = ($bulan != '00') ? $this->bulanSaldoDebit[$bulan] : '';
    $fieldbulanSaldoCredit = ($bulan != '00') ? $this->bulanSaldoCredit[$bulan] : '';
    $fieldbulanSaldoAkhir = ($bulan != '00') ? $this->bulanSaldoAkhir[$bulan] : '';

    $this->load->library('pdf_lap');
    $isi = '<br/><font size=\"7\" face=\"Helvetica\">';       
    
    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun))->result();
        
    // add a page
    $page_format = array(
      'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 320),
      'Dur' => 3,
      'trans' => array(
        'D' => 1.5,
        'S' => 'Split',
        'Dm' => 'V',
        'M' => 'O'
      ),
      'Rotate' => 0,
      'PZ' => 1,
    );
    
    //Set Footer
    $this->pdf_lap->SetFooterMargin(8); // margin footer 1 CM
    $this->pdf_lap->setPrintFooter(true); // enabled ? true
    $this->pdf_lap->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $this->pdf_lap->SetAutoPageBreak(TRUE, '15');
    
    $this->pdf_lap->SetPrintHeader(false);
    $this->pdf_lap->AddPage('L', $page_format, false, false);
    $this->pdf_lap->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-35, PDF_MARGIN_RIGHT);
    $this->pdf_lap->SetFont('helvetica', '', 14);
    
    $x=0;$y=10;
    $this->pdf_lap->writeHTMLCell(0, 10, $y, $x, '', '', 1, 0, true, 'C', true);
    $this->pdf_lap->Cell(0, 0, 'Buku Besar', 0, 1, 'C', 0, '', 0);

    $this->pdf_lap->SetFont('helvetica', '', 10);
    if($bulan != '00'){
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $this->bulan[$bulan] .' '. $tahun, 0, 1, 'C', 0, '', 0);
    }else{
      $this->pdf_lap->Cell(0, 0, 'Periode : '. $tahun, 0, 1, 'C', 0, '', 0);  
    }
    
    $no = 1;
    $isi .='
        <table border="1">
        <tr>
          <td align="center" width="15%"><strong>Kode Akun</strong></td>
          <td align="center" width="25%"><strong>Nama Akun</strong></td>
          <td align="center" width="15%"><strong>Saldo Awal</strong></td>
          <td align="center" width="15%"><strong>Total Debit</strong></td>
          <td align="center" width="15%"><strong>Total Kredit</strong></td>
          <td align="center" width="15%"><strong>Saldo Akhir</strong></td>
        </tr>';

    if(!empty($data_akun_tahun)){
      foreach($data_akun_tahun as $idx => $akun_tahun){
        
        if($bulan == '00'){
          if(is_null($akun_tahun->saldoawaljan))
            $calc_saldoawal = $akun_tahun->saldoawal;
          else
            $calc_saldoawal = $akun_tahun->saldoawaljan;

        }else{
          //bulan & tahun awal penjurnalan
          $bulanawal = $this->Bulanaktif->get_bulan_awal_jurnal();
          $tahunawal = $this->Bulanaktif->get_tahun_awal_jurnal();

          if(is_null($akun_tahun->$fieldbulanSaldoAwal) && $bulan == $bulanawal && $tahun == $tahunawal){
            $calc_saldoawal = $akun_tahun->saldoawal;
          }else{
            $calc_saldoawal = $akun_tahun->$fieldbulanSaldoAwal;
          }

        }

        /*
          @menghitung total transaksi debit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoDebit)){
          $totaldebit = $this->count_total_debit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $totaldebit = $akun_tahun->$fieldbulanSaldoDebit;
        }
        
        /*
          @menghitung total transaksi kredit
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoCredit)){
          $totalkredit = $this->count_total_credit($akun_tahun->idakun, $tahun, $bulan);
        }else{
          $totalkredit = $akun_tahun->$fieldbulanSaldoCredit;
        }


        /*
          @menghitung total saldo akhir
        */
        if($bulan == '00' || is_null($akun_tahun->$fieldbulanSaldoAkhir)){
          $saldoakhir = $calc_saldoawal + $totaldebit - $totalkredit;
        }else{
          $saldoakhir = $akun_tahun->$fieldbulanSaldoAkhir;
        }

        $isi .='
        <tr>
          <td align="center">&nbsp; '. $akun_tahun->kdakun .' </td>
          <td align="left">&nbsp; '. $akun_tahun->nmakun .' </td>
          <td align="right">&nbsp; Rp.'. number_format($calc_saldoawal,2,',','.') .' &nbsp;</td>
          <td align="right">&nbsp; Rp.'. number_format($totaldebit,2,',','.') .' &nbsp;</td>
          <td align="right">&nbsp; Rp.'. number_format($totalkredit,2,',','.') .' &nbsp;</td>
          <td align="right">&nbsp; Rp.'. number_format($saldoakhir,2,',','.') .' &nbsp;</td>
        </tr>
        ';      
      }
    }

    $isi .= "</table></font>";
    $this->pdf_lap->writeHTML($isi,true,false,false,false);

    //Close and output PDF document
    $this->pdf_lap->Output('lap_buku_besar_'. $tahun .'_'. (@$this->bulan[$bulan]) .'.pdf', 'I');
  }

  /* ========================== LAPORAN EXCEL ========================== */

  function lap_buku_besar_excel($tglawal = '', $tglakhir = '')
  {
    $tahun = date('Y',strtotime($tglawal));
    $bulan = date('m',strtotime($tglawal));

    $subject_file = 'Periode : '. date("d/m/Y", strtotime($tglawal)). ' - '. date("d/m/Y", strtotime($tglakhir));

    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun, 'idstatus' => 1))->result();
    
    //list of style
    $headergrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
        'font'  => array('bold'  => true)
    );

    $contentgrid = array(
        'borders' => array(
            'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        )
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Buku Besar")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Buku Besar'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:G1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Buku Besar');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:G2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(38);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);

    //set header grid
    $this->lib_phpexcel->getActiveSheet()->getStyle("B4:G4")->applyFromArray($headergrid);
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B4", 'Kode Akun')
         ->setCellValue("C4", 'Nama Akun')
         ->setCellValue("D4", 'Saldo Awal')
         ->setCellValue("E4", 'Total Debit')
         ->setCellValue("F4", 'Total Kredit')
         ->setCellValue("G4", 'Saldo Akhir');
    
    if(!empty($data_akun_tahun)){
      $active_row = 5;
      foreach($data_akun_tahun as $idx => $akun_tahun){
        
        /*
          @menghitung saldo awal
        */
        $calc_saldoawal = $this->calc_saldo_awal($akun_tahun->idakun, $tglawal, $tglakhir);
        
        /*
          @menghitung total transaksi debit
        */
        $totaldebit = $this->count_total_debit($akun_tahun->idakun, $tglawal, $tglakhir);
        
        /*
          @menghitung total transaksi kredit
        */
        $totalkredit = $this->count_total_credit($akun_tahun->idakun, $tglawal, $tglakhir);
        
        /*
          @menghitung total saldo akhir
        */
        $saldoakhir = $calc_saldoawal + $totaldebit - $totalkredit;
        
        //set content grid
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:G{$active_row}")->applyFromArray($contentgrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", $akun_tahun->kdakun)
             ->setCellValue("C{$active_row}", $akun_tahun->nmakun)
             ->setCellValue("D{$active_row}", $calc_saldoawal)
             ->setCellValue("E{$active_row}", $totaldebit)
             ->setCellValue("F{$active_row}", $totalkredit)
             ->setCellValue("G{$active_row}", $saldoakhir);

        $active_row += 1;
      }
    }


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'lap_buku_besar_'. date("d-m-Y", strtotime($tglawal)). '_'. date("d_m_Y", strtotime($tglakhir));

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

  function lap_buku_besar_detail_excel($tglawal = '', $tglakhir = ''){
    $tahun = date('Y',strtotime($tglawal));
    $bulan = date('m',strtotime($tglawal));

    $subject_file = 'Periode : '. date("d/m/Y", strtotime($tglawal)). ' - '. date("d/m/Y", strtotime($tglakhir));

    //get all akun tahun
    $this->db->orderby('kdakun', 'ASC');
    $data_akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun, 'idstatus' => 1))->result();

    //list of style
    $headergrid = array(
      'borders' => array(
        'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
      ),
      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
      'font'  => array('bold'  => true)
    );

    $contentgrid = array(
      'borders' => array(
        'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
      )
    );

    $summarygrid = array(
      'borders' => array(
        'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
      ),
      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT),
      'font'  => array('bold'  => true)
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Buku Besar")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Buku Besar'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:H1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Buku Besar');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:H2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);

    $active_row = 4;
    if(!empty($data_akun_tahun)){
      foreach($data_akun_tahun as $idx => $akun_tahun){
        
        $idakun  = $akun_tahun->idakun;   
        
        //kalkulasi saldo (saldo awal, adjustment, saldo akhir)
        $calc_saldoawal = $this->calc_saldo_awal($idakun, $tglawal, $tglakhir);
        
        $totaldebit     = $this->count_total_debit($idakun, $tglawal, $tglakhir);        
        $totalkredit    = $this->count_total_credit($idakun, $tglawal, $tglakhir);
        $saldoakhir     = $calc_saldoawal + $totaldebit - $totalkredit;

        //set summary info
        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Akun')
             ->setCellValue("C{$active_row}", ': '. $akun_tahun->nmakun .' ('. $akun_tahun->kdakun .')');
        $active_row += 1; 

        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Saldo Awal')
             ->setCellValue("C{$active_row}", ': Rp.'. number_format($calc_saldoawal,2,',','.') );
        $active_row += 1; 

        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Saldo Akhir')
             ->setCellValue("C{$active_row}", ': Rp.'. number_format($saldoakhir,2,',','.') );
        $active_row += 1; 
        
        //set header grid
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($headergrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Kode Jurnal')
             ->setCellValue("C{$active_row}", 'Tanggal Transaksi')
             ->setCellValue("D{$active_row}", 'Tanggal Jurnal')
             ->setCellValue("E{$active_row}", 'No. Reff / No. Bon')
             ->setCellValue("F{$active_row}", 'Keterangan')
             ->setCellValue("G{$active_row}", 'Debit')
             ->setCellValue("H{$active_row}", 'Kredit');
        $active_row += 1; // set active row to the next row
        
        //get detail jurnal
        $this->db->select('*');
        $this->db->from('v_bukujurnal');
        $this->db->orderby('tgltransaksi', 'ASC');
        $this->db->where('idakun', $idakun);
        $this->db->where('status_posting', '1');
        $this->db->where('tgltransaksi >=', $tglawal);
        $this->db->where('tgltransaksi <=', $tglakhir);
        
        $query = $this->db->get();

        $data = $query->result();
        $total = $query->num_rows();

        $total_debit = 0;
        $total_kredit = 0;
        if(!empty($data))
        {
          
          foreach($data as $idx => $dt){
            $total_debit += $dt->debit;
            $total_kredit += $dt->kredit; 

            //set content grid
            $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($contentgrid);
            $this->lib_phpexcel->setActiveSheetIndex(0)
                 ->setCellValue("B{$active_row}", $dt->kdjurnal)
                 ->setCellValue("C{$active_row}", date("d/m/Y", strtotime($dt->tgltransaksi)))
                 ->setCellValue("D{$active_row}", date("d/m/Y", strtotime($dt->tgljurnal)))
                 ->setCellValue("E{$active_row}", $dt->noreff_jurnaldet)
                 ->setCellValue("F{$active_row}", $dt->keterangan)
                 ->setCellValue("G{$active_row}", $dt->debit)
                 ->setCellValue("H{$active_row}", $dt->kredit);
            $active_row += 1; // set active row to the next row
          
          }

        }

        //row total debit & credit
        $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:F{$active_row}");
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($summarygrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", 'Total')
             ->setCellValue("G{$active_row}", $totaldebit)
             ->setCellValue("H{$active_row}", $totalkredit);

        $active_row += 2;

      }
    }

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'lap_buku_besar_detail_'. date("d-m-Y", strtotime($tglawal)). '_'. date("d_m_Y", strtotime($tglakhir));

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

  function lap_buku_jurnal_excel($tglawal = '', $tglakhir = '', $idakun = ''){
    $tahun = date('Y',strtotime($tglawal));
    $bulan = date('m',strtotime($tglawal));

    $subject_file = 'Periode : '. date("d/m/Y", strtotime($tglawal)). ' - '. date("d/m/Y", strtotime($tglakhir));

    //list of style
    $headergrid = array(
      'borders' => array(
        'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
      ),
      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
      'font'  => array('bold'  => true)
    );

    $contentgrid = array(
      'borders' => array(
        'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
      )
    );

    $summarygrid = array(
      'borders' => array(
        'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
      ),
      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT),
      'font'  => array('bold'  => true)
    );

    // Set document properties
    $this->lib_phpexcel->getProperties()->setCreator("Aplikasi RSIA Harapan Bunda Bandung")
         ->setLastModifiedBy("Aplikasi RSIA Harapan Bunda Bandung")
         ->setTitle("Buku Jurnal")
         ->setSubject($subject_file);

    $this->lib_phpexcel->getActiveSheet()->setTitle('Buku Besar'); // set worksheet name

    //set title
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B1:H1'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true)->setSize(20); //set font to bold
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B1', 'Buku Jurnal');
  
    //set subtitle
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells('B2:H2'); //merge cell
    $this->lib_phpexcel->getActiveSheet()->getStyle("B2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //align center
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue('B2', $subject_file);

    //set width of column
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
    $this->lib_phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);    

    //get all akun tahun
    $akun_tahun = $this->db->get_where('v_bukubesar', array('tahun' => $tahun, 'idakun' => $idakun))->row();
    
    //kalkulasi saldo (saldo awal, adjustment, saldo akhir)
    $calc_saldoawal = $this->calc_saldo_awal($idakun, $tglawal, $tglakhir);

    $totaldebit     = $this->count_total_debit($idakun, $tglawal, $tglakhir);        
    $totalkredit    = $this->count_total_credit($idakun, $tglawal, $tglakhir);
    $saldoakhir     = $calc_saldoawal + $totaldebit - $totalkredit;

    $active_row = 5;
    //set summary info
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B{$active_row}", 'Akun')
         ->setCellValue("C{$active_row}", ': '. $akun_tahun->nmakun .' ('. $akun_tahun->kdakun .')');
    $active_row += 1; 

    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B{$active_row}", 'Saldo Awal')
         ->setCellValue("C{$active_row}", ': Rp.'. number_format($calc_saldoawal,2,',','.') );
    $active_row += 1; 

    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("C{$active_row}:D{$active_row}"); //merge cell
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B{$active_row}", 'Saldo Akhir')
         ->setCellValue("C{$active_row}", ': Rp.'. number_format($saldoakhir,2,',','.') );
    $active_row += 1; 
    
    //set header grid
    $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($headergrid);
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B{$active_row}", 'Kode Jurnal')
         ->setCellValue("C{$active_row}", 'Tanggal Transaksi')
         ->setCellValue("D{$active_row}", 'Tanggal Jurnal')
         ->setCellValue("E{$active_row}", 'No. Reff / No. Bon')
         ->setCellValue("F{$active_row}", 'Keterangan')
         ->setCellValue("G{$active_row}", 'Debit')
         ->setCellValue("H{$active_row}", 'Kredit');
    $active_row += 1; // set active row to the next row

    //get detail jurnal
    $this->db->select('*');
    $this->db->from('v_bukujurnal');
    $this->db->orderby('tgltransaksi', 'ASC');
    $this->db->where('idakun', $idakun);
    $this->db->where('status_posting', '1');
    $this->db->where('tgltransaksi >=', $tglawal);
    $this->db->where('tgltransaksi <=', $tglakhir);
    
    $query = $this->db->get();

    $data = $query->result();
    $total = $query->num_rows();

    $total_debit = 0;
    $total_kredit = 0;
    if(!empty($data))
    {
      
      foreach($data as $idx => $dt){
        $total_debit += $dt->debit;
        $total_kredit += $dt->kredit; 

        //set content grid
        $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($contentgrid);
        $this->lib_phpexcel->setActiveSheetIndex(0)
             ->setCellValue("B{$active_row}", $dt->kdjurnal)
             ->setCellValue("C{$active_row}", date("d/m/Y", strtotime($dt->tgltransaksi)))
             ->setCellValue("D{$active_row}", date("d/m/Y", strtotime($dt->tgljurnal)))
             ->setCellValue("E{$active_row}", $dt->noreff_jurnaldet)
             ->setCellValue("F{$active_row}", $dt->keterangan)
             ->setCellValue("G{$active_row}", $dt->debit)
             ->setCellValue("H{$active_row}", $dt->kredit);
        $active_row += 1; // set active row to the next row
      
      }

    }

    //row total debit & credit
    $this->lib_phpexcel->setActiveSheetIndex(0)->mergeCells("B{$active_row}:F{$active_row}");
    $this->lib_phpexcel->getActiveSheet()->getStyle("B{$active_row}:H{$active_row}")->applyFromArray($summarygrid);
    $this->lib_phpexcel->setActiveSheetIndex(0)
         ->setCellValue("B{$active_row}", 'Total')
         ->setCellValue("G{$active_row}", $totaldebit)
         ->setCellValue("H{$active_row}", $totalkredit);

    $active_row += 2;

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->lib_phpexcel->setActiveSheetIndex(0);

    $filename = 'lap_buku_jurnal_'.$akun_tahun->kdakun.'_'. date("d-m-Y", strtotime($tglawal)). '_'. date("d_m_Y", strtotime($tglakhir));

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->lib_phpexcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }
}
