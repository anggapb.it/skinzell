<?php

class Pengeluaranbrg_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_pengeluaranbrg(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
        $userid            		= $this->input->post("userid");
		
        $this->db->select("*");
        $this->db->from("v_keluarbrg");
		$this->db->order_by('v_keluarbrg.nokeluarbrg DESC');
        		
		if($userid){		
			$this->db->where("v_keluarbrg.idbagianuntuk IN (SELECT idbagian from penggunabagian where userid='".$userid."')");
        }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->numrow($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($key, $userid){
    
        $this->db->select("*");
        $this->db->from("v_keluarbrg"); 
		
        if($userid){		
			$this->db->where("v_keluarbrg.idbagianuntuk IN (SELECT idbagian from penggunabagian where userid = '".$userid."')");
        }
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        $q = $this->db->get();
        return $q->num_rows();
    }
	
	function get_pengeluaranbrgdet(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$nokeluarbrg			= $this->input->post("nokeluarbrg");
		
        $this->db->select("*");
        $this->db->from("v_keluarbrgdet");
		
		//if($nokeluarbrg != null)$this->db->where("v_keluarbrgdet.nokeluarbrg", $nokeluarbrg);
        $where = array();
        $where['v_keluarbrgdet.nokeluarbrg']=$nokeluarbrg;
		$this->db->where($where);
		
        /* if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
				
		
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        } */
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all("v_keluarbrgdet");
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	/* function nw($fields, $query){
    
        $this->db->select("*");
        $this->db->from("v_keluarbrgdet");
		
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
        $q = $this->db->get();
        return $q->num_rows();
    } */
	
	function get_brgmedisdipengeluaranbrg(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $this->input->post("key");
		$idbagian				= $this->input->post("idbagian");
		
        $this->db->select('*');
		$this->db->from('v_barangbagian');
		$this->db->order_by('kdbrg');
        
		$where = array();
		$where['v_barangbagian.idbagian']=$idbagian;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrw($key, $idbagian);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrw($key, $idbagian){
      
        $this->db->select('*');
		$this->db->from('v_barangbagian');
		
		$where = array();
		$where['v_barangbagian.idbagian']=$idbagian;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_pengeluaranbrg(){     
		$where['nokeluarbrg'] = $_POST['nokeluarbrg'];
		$del = $this->rhlib->deleteRecord('keluarbrgdet',$where);
		$del = $this->rhlib->deleteRecord('keluarbrg',$where);
        return $del;
    }
	
	function id_bagian($where, $val){
		$query = $this->db->getwhere('bagian',array($where=>$val));
		$id = $query->row_array();
		return $id['idbagian'];
    }
	
	function getNmbagian(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }
		
	function getKdbagian(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['kdbagian']);
    }
	
	function getNmdaribagian(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }
	
	function getKdbagianuntuk(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['kdbagian']);
    }
	
	function insorupd_pengbrg(){
        $this->db->trans_begin();
		$nokeluarbrg = $this->input->post("nokeluarbrg");
		$query 	= $this->db->getwhere('keluarbrg',array('nokeluarbrg'=>$nokeluarbrg));
		$nokeluarbrg = $query->row_array();
				
		$keluarbrg = $this->insert_tblkeluarbrg($nokeluarbrg);
		$nokeluarbrg = $keluarbrg['nokeluarbrg'];
				
		if($nokeluarbrg)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nokeluarbrg"]=$nokeluarbrg;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_tblkeluarbrg($nokeluarbrg){
		if(!$nokeluarbrg){
			$dataArray = $this->getFieldsAndValues($nokeluarbrg);
			$keluarbrg = $this->db->insert('keluarbrg',$dataArray);
		} else {
			$dataArray = $this->update_tblkeluarbrg($nokeluarbrg);
		}
		
		$query = $this->db->getwhere('keluarbrgdet',array('nokeluarbrg'=>$dataArray['nokeluarbrg']));
		$keluarbrgdet = $query->row_array();
		if($query->num_rows() == 0){
			$keluarbrgdet = $this->insert_tblkeluarbrgdet($dataArray);
		} else {
			$keluarbrgdet = $this->update_tblkeluarbrgdet($dataArray);
		}
		
		if($keluarbrgdet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function insert_tblkeluarbrgdet($keluarbrg){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrpengbrg']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($keluarbrg, $vale[0], $vale[1], $vale[2]);
			$z =$this->db->insert('keluarbrgdet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function update_tblkeluarbrg($nokeluarbrg){
		$dataArray = $this->getFieldsAndValues($nokeluarbrg);
		
		//UPDATE
		$this->db->where('nokeluarbrg', $dataArray['nokeluarbrg']);
		$keluarbrg = $this->db->update('keluarbrg', $dataArray);
		
		return $dataArray;
    }
	
	function update_tblkeluarbrgdet($keluarbrg){
		$where['nokeluarbrg'] = $keluarbrg['nokeluarbrg'];
		$this->db->delete('keluarbrgdet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrpengbrg']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($keluarbrg, $vale[0], $vale[1], $vale[2]);
			$z =$this->db->insert('keluarbrgdet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function getFieldsAndValues(){
			$nokeluarbrg = $this->getNoPengbrg();
			$nokbrg   	= $this->input->post("nokeluarbrg");
			$ctgl   	= $this->input->post("ctgl");
			if($ctgl == 1){
				$vtgl = $_POST['tglkeluar'];
			}else{
				$vtgl = date('Y-m-d');
			}			
		
		$dataArray = array(
			 'nokeluarbrg'		=> ($nokbrg) ? $nokbrg: $nokeluarbrg,
			 'tglkeluar'		=> $vtgl,
             'jamkeluar'		=> $_POST['jamkeluar'],
             'idbagiandari'		=> $_POST['idbagiandari'],
             'idbagianuntuk'	=> $_POST['idbagianuntuk'],
             'idsttransaksi'	=> $_POST['idsttransaksi'],
             'idstsetuju'		=> $_POST['idstsetuju'],
			 'userid'			=> $_POST['userid'],
			 'penerima'			=> $_POST['penerima'],
			 'keterangan'		=> $_POST['keterangan']			
		);
		/* var_dump($dataArray);
		exit; */
		return $dataArray;
	}
	
	function getNoPengbrg(){
		$q = "SELECT getOtoNopengeluaranbrg(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}

	function getFieldsAndValuesDet($keluarbrg,$val1,$val2,$val3){
		$dataArray = array(
			'nokeluarbrg' => $keluarbrg['nokeluarbrg'],
			'kdbrg'		 => $val1,
			'qty' 		 => $val2,
			'catatan'    => $val3,			
		);
		return $dataArray;
	}
	
	function update_qty_tambah(){
		//$nokeluarbrg = $this->getNoPengbrg();
		$date = date("Y-m-d", strtotime($_POST['tglkeluar']));
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_insorupd_brgbagian_dari_pengbrg (?,'".$date."',?,?,?,?,?,?,?,?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3],
							 $vale[4],
							 $vale[5],
							 $vale[6],
							 $vale[7],
							 $vale[8],
							 $vale[9],
							 $vale[10],
							 $vale[11],
							));
		}		
        return;
    }
	
	function update_qty_batal(){
		//$nokeluarbrg = $this->getNoPengbrg();
		$date = date("Y-m-d", strtotime($_POST['tglkeluar']));
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_update_brgbagian_dari_pengbrg (?,'".$date."',?,?,?,?,?,?,?,?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3],
							 $vale[4],
							 $vale[5],
							 $vale[6],
							 $vale[7],
							 $vale[8],
							 $vale[9],
							 $vale[10],
							 $vale[11]
							));
		}
        return;
    }
	
	/* function getNmField(){
		$this->db->select("*");
        $this->db->from("setting");
		$this->db->where("idklpsetting = 6");
		$q = $this->db->get();
		$idklpsetting = $q->row_array();
		echo json_encode($idklpsetting);
	} */
	
	function get_pengbagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$userid 				= $this->input->post("userid");
      
        $this->db->select("penggunabagian.idbagian, bagian.nmbagian, pengguna.userid, pengguna.nmlengkap");
        $this->db->from("penggunabagian");		
		$this->db->join('bagian',
                'bagian.idbagian = penggunabagian.idbagian', 'left');
		$this->db->join('pengguna',
                'pengguna.userid = penggunabagian.userid', 'left');
		$this->db->where("penggunabagian.idbagian = '11'");
		
        $where = array();
		$where['penggunabagian.userid']=$userid;
		$this->db->where($where);
		
		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_pengunabagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
		$userid 				= $this->input->post("userid");
      
        $this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrww($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrww($key, $userid){      
       
		$this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
}