<?php

class Dokter_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_dokter_combo(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
	  
	  
        $this->db->select('dokter.*, bank.nmbank ,jkelamin.nmjnskelamin, spesialisasi.nmspesialisasi, status.nmstatus, stdokter.nmstdokter, bagian.nmbagian, jtenagamedis.nmjnstenagamedis');
        $this->db->from('dokter');
		$this->db->join('jkelamin',
                'jkelamin.idjnskelamin = dokter.idjnskelamin', 'left');		
 		$this->db->join('spesialisasi',
                'spesialisasi.idspesialisasi = dokter.idspesialisasi', 'left');
		$this->db->join('status',
                'status.idstatus = dokter.idstatus', 'left');
		$this->db->join('stdokter',
                'stdokter.idstdokter = dokter.idstdokter', 'left');
		$this->db->join('dokterbagian',
                'dokter.iddokter = dokterbagian.iddokter', 'left');
		$this->db->join('bagian',
                'bagian.idbagian = dokterbagian.idbagian', 'left');
		$this->db->join('bank',
                'bank.idbank = dokter.idbank', 'left');
		$this->db->join('jtenagamedis',
                'jtenagamedis.idjnstenagamedis = dokter.idjnstenagamedis', 'left');
		/* $this->db->join('dokterbagian',
                'dokterbagian.idbagian = dokter.iddokter', 'left'); */
		$this->db->where("dokter.idjnstenagamedis","1");
		
		$this->db->order_by('kddokter');
		$this->db->group_by('iddokter');
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = count($data);

        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_dokter(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
	  
	  
        $this->db->select('dokter.*, bank.nmbank ,jkelamin.nmjnskelamin, spesialisasi.nmspesialisasi, status.nmstatus, stdokter.nmstdokter, bagian.nmbagian, jtenagamedis.nmjnstenagamedis');
        $this->db->from('dokter');
		$this->db->join('jkelamin',
                'jkelamin.idjnskelamin = dokter.idjnskelamin', 'left');		
 		$this->db->join('spesialisasi',
                'spesialisasi.idspesialisasi = dokter.idspesialisasi', 'left');
		$this->db->join('status',
                'status.idstatus = dokter.idstatus', 'left');
		$this->db->join('stdokter',
                'stdokter.idstdokter = dokter.idstdokter', 'left');
		$this->db->join('dokterbagian',
                'dokter.iddokter = dokterbagian.iddokter', 'left');
		$this->db->join('bagian',
                'bagian.idbagian = dokterbagian.idbagian', 'left');
		$this->db->join('bank',
                'bank.idbank = dokter.idbank', 'left');
		$this->db->join('jtenagamedis',
                'jtenagamedis.idjnstenagamedis = dokter.idjnstenagamedis', 'left');
		/* $this->db->join('dokterbagian',
                'dokterbagian.idbagian = dokter.iddokter', 'left'); */
				
		$this->db->order_by('kddokter');
		$this->db->group_by('iddokter');
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
         
		if ($start!=null){
				$this->db->limit($limit,$start);
		}else{
				$this->db->limit(18,0);
		}
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->numrow($fields, $query);

        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select('dokter.*, bank.nmbank ,jkelamin.nmjnskelamin, spesialisasi.nmspesialisasi, status.nmstatus, stdokter.nmstdokter, bagian.nmbagian, jtenagamedis.nmjnstenagamedis');
		$this->db->from('dokter');
		$this->db->join('jkelamin',
                'jkelamin.idjnskelamin = dokter.idjnskelamin', 'left');		
 		$this->db->join('spesialisasi',
                'spesialisasi.idspesialisasi = dokter.idspesialisasi', 'left');
		$this->db->join('status',
                'status.idstatus = dokter.idstatus', 'left');
		$this->db->join('stdokter',
                'stdokter.idstdokter = dokter.idstdokter', 'left');
		$this->db->join('dokterbagian',
                'dokter.iddokter = dokterbagian.iddokter', 'left');
		$this->db->join('bagian',
                'bagian.idbagian = dokterbagian.idbagian', 'left');
		$this->db->join('bank',
                'bank.idbank = dokter.idbank', 'left');
		$this->db->join('jtenagamedis',
                'jtenagamedis.idjnstenagamedis = dokter.idjnstenagamedis', 'left');

        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_dokter(){     
		$where['iddokter'] = $_POST['iddokter'];
		$del = $this->rhlib->deleteRecord('dokter',$where);
		
		$where['iddokter'] = $_POST['iddokter'];
		$del = $this->rhlib->deleteRecord('dokterbagian',$where);
        return $del;
    }
		
	function insert_dokter(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('dokter',$dataArray);	
		
		$dataArray2 = $this->getFieldsAndValues2();
		$ret = $this->rhlib->insertRecord('dokterbagian',$dataArray2);
        return $ret;
    }

	function update_dokter(){
		//update->dokter
		$dataArray = $this->getFieldsAndValues();
		
		$this->db->where('iddokter', $_POST['iddokter']);
		$this->db->update('dokter', $dataArray); 
		
		//delete->dokterbagian		
		$where['iddokter'] = $_POST['iddokter'];
		$del = $this->rhlib->deleteRecord('dokterbagian',$where);
		
		//insert->dokterbagian		
		$dataArrayiup = $this->getFieldsAndValuesiup();
		$ret = $this->rhlib->insertRecord('dokterbagian',$dataArrayiup);
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
	
	function update_rek_dokter(){
		//update->dokter
		$dataArray = $this->getFieldsAndValues_rekdok();
		
		$this->db->where('iddokter', $_POST['iddokter']);
		$this->db->update('dokter', $dataArray); 
		
		
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
		$kddokter = $this->getKdDokter();
		
		$dataArray = array(
             'iddokter'			=> $_POST['iddokter'],
			 'kddokter'			=> ($_POST['kddokter']) ? $_POST['kddokter']:$kddokter,
             'nmdokter'			=> $_POST['nmdokter'],
			 'nmdoktergelar'	=> $_POST['nmdoktergelar'],
			 'idjnskelamin'		=> $_POST['idjnskelamin'],
			 'tptlahir'			=> $_POST['tptlahir'],
			 'tgllahir'			=> ($_POST['tgllahir']) ? $_POST['tgllahir']:null,
             'alamat'			=> $_POST['alamat'],
			 'notelp'			=> $_POST['notelp'],
			 'nohp'				=> $_POST['nohp'],
			 'idspesialisasi'	=> ($_POST['idspesialisasi']) ? $this->id_spsialisasi('nmspesialisasi',$_POST['idspesialisasi']) : null,
             'idstatus'			=> $_POST['idstatus'],
			 'idstdokter'		=> ($_POST['idstdokter']) ? $_POST['idstdokter']:null,
			 'catatan'           => $_POST['catatan'],
             'keterangan'          => $_POST['keterangan'],
             'idjnstenagamedis'	=> $_POST['idjnstenagamedis']
        );
		return $dataArray;
	}
	
	function getFieldsAndValues_rekdok(){
		
		$dataArray = array(
             'iddokter'			=> $_POST['iddokter'],
			 'idbank'			=> $_POST['idbank'],
			 'norek'			=> $_POST['norek'],
			 'atasnama'			=> $_POST['atasnama'],
			 'cabang'			=> $_POST['cabang'],
        );
		return $dataArray;
	}
	
	function getFieldsAndValues2(){
		$hasil = $this->db->query("SELECT iddokter FROM dokter ORDER BY iddokter DESC LIMIT 1");
			foreach($hasil->result() as $row)
				$iddokter= $row->iddokter;
	
		$dataArray2 = array(
			 'idbagian'			=> ($_POST['idbagian']) ? $this->id_bagian('nmbagian',$_POST['idbagian']) : null,
             'iddokter'			=> $iddokter,
        );
		return $dataArray2;
	}
	
	function getFieldsAndValuesiup(){		
		$dataArrayiup = array(
			 'idbagian'			=> ($_POST['idbagian']) ? $this->id_bagian('nmbagian',$_POST['idbagian']) : null,
             'iddokter'			=> $_POST['iddokter'],
        );
		return $dataArrayiup;
	}
	
	function getNmspsialisasi(){
		$query = $this->db->getwhere('spesialisasi',array('idspesialisasi'=>$_POST['idspesialisasi']));
		$nm = $query->row_array();
		echo json_encode($nm['nmspesialisasi']);
    }
	
	function id_spsialisasi($where, $val){
		$query = $this->db->getwhere('spesialisasi',array($where=>$val));
		$id = $query->row_array();
		return $id['idspesialisasi'];
    }
	
	function getNmbagian(){
		$query = $this->db->getwhere('dokterbagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }
	
	function id_bagian($where, $val){
		$query = $this->db->getwhere('bagian',array($where=>$val));
		$id = $query->row_array();
		return $id['idbagian'];
    }
	
	function getKdDokter(){
		$q = "SELECT getotokddokter() as kode;";
        $query  = $this->db->query($q);
        $kode= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $kode=$row->kode;
        }
        return $kode;
	}
	
	function get_dokterri(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select('dokter.*, jkelamin.nmjnskelamin, spesialisasi.nmspesialisasi, status.nmstatus, stdokter.nmstdokter');
        $this->db->from('dokter');
		$this->db->join('jkelamin',
                'jkelamin.idjnskelamin = dokter.idjnskelamin', 'left');		
 		$this->db->join('spesialisasi',
                'spesialisasi.idspesialisasi = dokter.idspesialisasi', 'left');
		$this->db->join('status',
                'status.idstatus = dokter.idstatus', 'left');
		$this->db->join('stdokter',
                'stdokter.idstdokter = dokter.idstdokter', 'left');
		$this->db->join('dokterbagian',
                'dokterbagian.iddokter = dokter.iddokter', 'left');
		$this->db->where('idbagian', 27);
		//$this->db->order_by('kddokter');
        
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}
