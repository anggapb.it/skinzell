<?php

class Tarif_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_tarif(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        
        $fields			= $this->input->post("fields");
        $query			= $this->input->post("query");
        $idklstarif		= $this->input->post("idklstarif");
        $idpenjamin		= $this->input->post("idpenjamin");
      
        $this->db->select("*, (tarif.tarifjs+tarif.tarifjm+tarif.tarifjp+tarif.tarifbhp) AS ttltarif");
        $this->db->from("tarif");
        $this->db->join("pelayanan",
				"pelayanan.kdpelayanan = tarif.kdpelayanan", "left");
        if($idklstarif)$this->db->where_in("tarif.idklstarif", array($idklstarif, 5));
        if($idpenjamin)$this->db->where("tarif.idpenjamin", $idpenjamin);
		
		if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nmpelayanan', $val);
			}
        }
		
        /* if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<($c-1);$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        } */
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
        $idklstarif		= $this->input->post("idklstarif");
      
        $this->db->select("*");
        $this->db->from("tarif");
        $this->db->join("pelayanan",
				"pelayanan.kdpelayanan = tarif.kdpelayanan", "left");
        if($idklstarif)$this->db->where_in("tarif.idklstarif", array($idklstarif, 5));
        
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nmpelayanan', $val);
			}
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
}
