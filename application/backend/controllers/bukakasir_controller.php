<?php

class Bukakasir_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_bukakasir(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $this->input->post("key");
		$userid					= $_POST['userid'];
      
        $this->db->select('*');
		$this->db->from('v_bukakasir');        
        
		$where = array();
		$where['v_bukakasir.userid'] = $userid;
		$this->db->where($where);
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }		
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
				
        $ttl = $this->numrow($userid, $key);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($userid, $key){
      
        $this->db->select('*');
		$this->db->from('v_bukakasir');
		
		$where = array();
		$where['v_bukakasir.userid'] = $userid;
		$this->db->where($where);
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_stkasir(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select('*');
        $this->db->from('stkasir');
		
		$this->db->order_by('idstkasir');
                
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_cbbagiandikasir(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*");
        $this->db->from("bagian");
		
		$this->db->where('idjnspelayanan = 7');
                
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function delete_bukakasir(){     
		$where['nokasir'] = $_POST['nokasir'];
		$del = $this->rhlib->deleteRecord('kasir',$where);
        return $del;
    }
		
	function insert_bukakasir(){
		$dataArray = $this->getFieldsAndValues();
		$this->rhlib->insertRecord('kasir',$dataArray);
        return $dataArray;
    }

	function update_bukakasir(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('nokasir', $_POST['nokasir']);
		$this->db->update('kasir', $dataArray);
        return $dataArray;
    }
	
	function getFieldsAndValues(){
	
		$nokasir			= $this->getNoKasir();		
		$idshiftbuka		= (isset($_POST['idshiftbuka']))? $_POST['idshiftbuka'] : null;
		
		$dataArray = array(
			 'nokasir'		=> ($_POST['nokasir']) ? $_POST['nokasir']: $nokasir,
			 'tglbuka'		=> $_POST['tglbuka'],
             'jambuka'		=> $_POST['jambuka'],
             'idshiftbuka'	=> $idshiftbuka,
			 'userid'		=> $_POST['userid'],
			 'idbagian'		=> $_POST['idbagian'],
			 'saldoawal'	=> $_POST['saldoawal'],
			 'catatanbuka'	=> $_POST['catatanbuka'],		
			 'idstkasir'	=> $_POST['idstkasir'],
			 'selisih'		=> 0
		);
		return $dataArray;
	}
	
	function getNoKasir(){
		$q = "SELECT getotoNokasir(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
	
	function getcekstkasir(){
		$this->db->select("*");
        $this->db->from("v_kasir");
		$this->db->where("idstkasir != 2");
		$q = $this->db->get();
		$idstkasir = $q->row_array();
		echo json_encode($idstkasir);
	}
	
	function cekuserid(){
        $q = "SELECT count(*) as iduser FROM kasir where userid='".$_POST['userid']."' AND idstkasir= '1'";
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->iduser;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
	
	function cekbagian(){
        $q = "SELECT count(*) as idbagian FROM kasir where idbagian='".$_POST['idbagian']."' AND idstkasir= '1'";
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->idbagian;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
}
