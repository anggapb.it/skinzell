<?php 
class Laporan_rmkiaanak2_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_laprmkiaanak2(){

		$tglawal = $this->input->post("tglawal");
        $tglakhir = $this->input->post("tglakhir");
		
		$q = "SELECT rd.idregdet
					 , n.nonota
					 , rd.tglmasuk
					 , k.tglkuitansi
					 , r.noreg
					 , s.noreganak
					 , trim(LEADING '0' FROM `p`.`norm`) AS `norm`
					 , p.nmpasien
					 , s.idcaralahir
					 , clhr.nmcaralahir
					 , if((ifnull((SELECT sum(notadet.qty) AS kditem
								   FROM
									 notadet
								   LEFT JOIN nota
								   ON nota.nonota = notadet.nonota
								   LEFT JOIN registrasidet
								   ON registrasidet.idregdet = nota.idregdet
								   WHERE
									 registrasidet.noreg = rd.noreg
									 AND
									 notadet.kditem = 'T000000212'
								   GROUP BY
									 notadet.kditem
					   ), '0') +
					   ifnull((SELECT sum(notadet.qty) AS kditem
							   FROM
								 notadet
							   LEFT JOIN nota
							   ON nota.nonota = notadet.nonota
							   LEFT JOIN registrasidet
							   ON registrasidet.idregdet = nota.idregdet
							   WHERE
								 registrasidet.noreg = rd.noreg
								 AND
								 notadet.kditem = 'T000000895'
							   GROUP BY
								 notadet.kditem
					   ), '0') +
					   ifnull((SELECT sum(notadet.qty) AS kditem
							   FROM
								 notadet
							   LEFT JOIN nota
							   ON nota.nonota = notadet.nonota
							   LEFT JOIN registrasidet
							   ON registrasidet.idregdet = nota.idregdet
							   WHERE
								 registrasidet.noreg = rd.noreg
								 AND
								 notadet.kditem = 'T000000899'
							   GROUP BY
								 notadet.kditem
					   ), '0')
					   ) >= '1', '1', NULL) AS kn1

					 , if((ifnull((SELECT sum(notadet.qty) AS kditem
								   FROM
									 notadet
								   LEFT JOIN nota
								   ON nota.nonota = notadet.nonota
								   LEFT JOIN registrasidet
								   ON registrasidet.idregdet = nota.idregdet
								   WHERE
									 registrasidet.noreg = rd.noreg
									 AND
									 notadet.kditem = 'T000000212'
								   GROUP BY
									 notadet.kditem
					   ), '0') +
					   ifnull((SELECT sum(notadet.qty) AS kditem
							   FROM
								 notadet
							   LEFT JOIN nota
							   ON nota.nonota = notadet.nonota
							   LEFT JOIN registrasidet
							   ON registrasidet.idregdet = nota.idregdet
							   WHERE
								 registrasidet.noreg = rd.noreg
								 AND
								 notadet.kditem = 'T000000895'
							   GROUP BY
								 notadet.kditem
					   ), '0') +
					   ifnull((SELECT sum(notadet.qty) AS kditem
							   FROM
								 notadet
							   LEFT JOIN nota
							   ON nota.nonota = notadet.nonota
							   LEFT JOIN registrasidet
							   ON registrasidet.idregdet = nota.idregdet
							   WHERE
								 registrasidet.noreg = rd.noreg
								 AND
								 notadet.kditem = 'T000000899'
							   GROUP BY
								 notadet.kditem
					   ), '0')
					   ) >= '2', '1', NULL) AS kn2

					 , if((ifnull((SELECT sum(notadet.qty) AS kditem
								   FROM
									 notadet
								   LEFT JOIN nota
								   ON nota.nonota = notadet.nonota
								   LEFT JOIN registrasidet
								   ON registrasidet.idregdet = nota.idregdet
								   WHERE
									 registrasidet.noreg = rd.noreg
									 AND
									 notadet.kditem = 'T000000212'
								   GROUP BY
									 notadet.kditem
					   ), '0') +
					   ifnull((SELECT sum(notadet.qty) AS kditem
							   FROM
								 notadet
							   LEFT JOIN nota
							   ON nota.nonota = notadet.nonota
							   LEFT JOIN registrasidet
							   ON registrasidet.idregdet = nota.idregdet
							   WHERE
								 registrasidet.noreg = rd.noreg
								 AND
								 notadet.kditem = 'T000000895'
							   GROUP BY
								 notadet.kditem
					   ), '0') +
					   ifnull((SELECT sum(notadet.qty) AS kditem
							   FROM
								 notadet
							   LEFT JOIN nota
							   ON nota.nonota = notadet.nonota
							   LEFT JOIN registrasidet
							   ON registrasidet.idregdet = nota.idregdet
							   WHERE
								 registrasidet.noreg = rd.noreg
								 AND
								 notadet.kditem = 'T000000899'
							   GROUP BY
								 notadet.kditem
					   ), '0')
					   ) >= '3', '1', NULL) AS kn3
					 , jk.nmjnskelamin AS jkelamin

					 , (SELECT group_concat(penyakit.nmpenyakit SEPARATOR ',<br>') AS nmpenyakiteng
						FROM
						  kodifikasidet
						LEFT JOIN kodifikasi
						ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
						LEFT JOIN skl
						ON skl.noreganak = kodifikasi.noreg
						LEFT JOIN penyakit
						ON penyakit.idpenyakit = kodifikasidet.idpenyakit
						WHERE
						  skl.noreganak = s.noreganak
					   ) AS nmpenyakiteng

					 , kd.idstkeluar
					 , stklr.nmstkeluar
					 , kd.idkematian
					 , sbmati.nmkematian
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , cdatang.nmcaradatang AS nmcaradatang
					 , r.idpenjamin
					 , pj.nmpenjamin

				FROM
				  skl s
				LEFT JOIN caralahir clhr
				ON clhr.idcaralahir = s.idcaralahir
				LEFT JOIN pasien p
				ON p.norm = s.normanak
				LEFT JOIN jkelamin jk
				ON jk.idjnskelamin = p.idjnskelamin
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = p.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN registrasi r
				ON r.noreg = s.noreg
				LEFT JOIN penjamin pj
				ON pj.idpenjamin = r.idpenjamin
				LEFT JOIN registrasidet rd
				ON rd.noreg = r.noreg
				LEFT JOIN caradatang cdatang
				ON cdatang.idcaradatang = rd.idcaradatang
				LEFT JOIN nota n
				ON n.idregdet = rd.idregdet
				LEFT JOIN notadet nd
				ON nd.nonota = n.nonota
				LEFT JOIN kuitansi k
				ON k.nokuitansi = n.nokuitansi
				LEFT JOIN kodifikasi kd
				ON kd.noreg = s.noreganak
				LEFT JOIN kodifikasidet kdt
				ON kdt.idkodifikasi = kd.idkodifikasi
				LEFT JOIN penyakit peny
				ON peny.idpenyakit = kdt.idpenyakit
				LEFT JOIN stkeluar stklr
				ON stklr.idstkeluar = kd.idstkeluar
				LEFT JOIN sebabkematian sbmati
				ON sbmati.idkematian = kd.idkematian
				WHERE
				  rd.userbatal IS NULL
				  AND
				  k.idstkuitansi = 1
				  AND
				  nd.kditem LIKE '%T%'
				  AND
				  k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'
				GROUP BY
				  s.noreganak
				ORDER BY
				  s.noreganak";
		$query = $this->db->query($q);
		
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
		
		$ttl = count($data);
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
        if($ttl>0){
            $build_array["data"]=$data;
        }
		
		echo json_encode($build_array);
	}

}
