<?php

class Baganperkiraantahunan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_baganperkiraantahunan(){
		$key					= $_POST["key"]; 
		
		$this->db->select("*");
		$this->db->from("v_baganperkiraantahunan");		
		$this->db->order_by('v_baganperkiraantahunan.kdakun');
		
		$where = array();   
        $where['v_baganperkiraantahunan.tahun']= $_POST['tahun'];          
		$this->db->where($where);
		
       if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}		
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all("v_baganperkiraantahunan");
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
		/* foreach($data as $row) {
            array_push($build_array["data"],array(
				'tahun'				=>$row->tahun,
				'idakun'			=>$row->idakun,
				'idklpakun'			=>$row->idklpakun,
				'nmklpakun'			=>$row->nmklpakun,
				'kdakun'			=>$row->kdakun,
				'nmakun'			=>$row->nmakun,
				'idjnsakun'			=>$row->idjnsakun,
				'kdjnsakun'			=>$row->kdjnsakun,
				'nmparent'			=>$row->nmparent,
                'tglinput'			=>date("Y-m-d",strtotime($row->tglinput)),
				'userid'			=>$row->userid,
				'nmlengkap'			=>$row->nmlengkap,
            ));
        } */
		
        echo json_encode($build_array);
    }
	
	function delete_data(){ 
		$where['tahun'] 	= $_POST['tahun'];
		$where['idakun'] 	= $_POST['idakun'];
		$del = $this->rhlib->deleteRecord('akuntahun',$where);
        return $del;
    }
		
	function insert_akun(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('akuntahun',$dataArray);
        return $ret;
    }
	
	function getFieldsAndValues(){
		
		$dataArray = array(
		     'tahun'	=> $_POST['tahun'], 
			 'idakun' 	=> $_POST['idakun'],
             'userid'	=> $this->session->userdata['user_id'],
			 'tglinput'	=> date('Y-m-d H:i:s'),
        );
		
		return $dataArray;
	}
	
	function cekidakun(){
        $q = "SELECT count(*) as idakun FROM akuntahun where idakun='".$_POST['idakun']."' AND tahun='".$_POST['tahun']."'" ;
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->idakun;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
	
	function copy_data(){
		$where['tahun'] 	= $_POST['tahun_baru'];
		$this->rhlib->deleteRecord('akuntahun',$where);
				
		$tahun_lama = $_POST['tahun_lama'];
		$tahun_baru = $_POST['tahun_baru'];
		$sql = "INSERT INTO akuntahun (tahun, idakun, userid, tglinput)
				SELECT 
					".$tahun_baru."
					, idakun
					, userid
					, tglinput
				FROM
				  akuntahun
				WHERE
				  tahun = '".$tahun_lama."'";
		$ret = $this->db->query($sql);
        return $ret;
    }

    function simpan_saldo_awal()
    {
      $arr_akuntahun      = $this->input->post('arr_akuntahun');
      $arr_akuntahun      = json_decode($arr_akuntahun);
      $update_success     = true;
      if(!empty($arr_akuntahun))
      {
        foreach($arr_akuntahun as $idx => $akuntahun){
          $exp_data = explode('-', $akuntahun);
          /*
            $exp_data[0] = tahun;
            $exp_data[1] = idakun;
            $exp_data[2] = saldoawal;
          */

          $update = $this->db->query("UPDATE akuntahun set saldoawal = '".$exp_data[2]."' WHERE tahun = '".$exp_data[0]."' AND idakun = '". $exp_data[1] ."' ");
          
          if(!$update)
            $update_success = false;

        }

      }

      if($update_success)
        $return = array('success' => true);
      else
        $return = array('success' => false);
       
      echo json_encode($return);die;
    }
}
