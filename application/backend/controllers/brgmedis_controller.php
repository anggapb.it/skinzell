<?php

class Brgmedis_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_brgmedis(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$val                  = $this->input->post("val");
		
        $this->db->select('*,(hrgbeli / rasio) as hargajual, (hrgbeli / rasio) as hargajualtemp');
		$this->db->from('v_barang');
		$this->db->order_by('kdbrg');        
        
		if($val){
			$x=array('[',']','"');
            $y=str_replace($x, '', $val);
            $z=explode(',', $y);
			$this->db->where_not_in('kdbrg',$z);
		}
				
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select('*');
		$this->db->from('v_barang');
		
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_brgmedis(){     
		$where['kdbrg'] = $_POST['kdbrg'];
		$del = $this->rhlib->deleteRecord('barang',$where);
        return $del;
    }
		
	function insert_brgmedis(){
		$dataArray = $this->getFieldsAndValues();
		$this->rhlib->insertRecord('barang',$dataArray);
        return $dataArray;
    }
	
	function update_brgmedis(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('kdbrg', $_POST['kdbrg']);
		$this->db->update('barang', $dataArray);
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
	
	function getFieldsAndValues(){
		$kdbrg = $this->getNobrgmedis();
		
		$dataArray = array(
			 'kdbrg'		=> ($_POST['kdbrg']) ? $_POST['kdbrg']: $kdbrg,
			 'idklpbrg'		=> $_POST['idklpbrg'],
             'idjnsbrg'		=> ($_POST['idjnsbrg'])? $this->id_jnsbrg('nmjnsbrg',$_POST['idjnsbrg']) : null,
             'nmbrg'		=> $_POST['nmbrg'],
			 'idsatuankcl'	=> ($_POST['idsatuankcl'])? $this->id_jsatuan('nmsatuan',$_POST['idsatuankcl']) : null,
			 'rasio'		=> 1,//$_POST['rasio'],
			 'idsatuanbsr'	=> ($_POST['idsatuanbsr'])? $this->id_jsatuan('nmsatuan',$_POST['idsatuanbsr']) : null,
			 'hrgavg'		=> $_POST['hrgavg'],
			 'hrgbeli'		=> $_POST['hrgbeli'],
			 'hrgjual'		=> $_POST['hrgjual'],
			 'idpabrik'		=> ($_POST['idpabrik']) ? $this->id_pabrik('nmpabrik',$_POST['idpabrik']) : null,
			 'idstatus'		=> $_POST['idstatus'],
			 'kdsupplier'	=> ($_POST['kdsupplier']) ? $this->kd_supplier('nmsupplier',$_POST['kdsupplier']) : null,
			 'stokmin'		=> $_POST['stokmin'],			 
			 'stokmax'		=> $_POST['stokmax'],
			 'gambar'		=> $_POST['gambar'],
			 'keterangan'	=> $_POST['keterangan'],
			 'tglinput'		=> $_POST['tglinput'],
             'userinput'	=> $_POST['userinput'],
			 'idstgenerik'	=> $_POST['idstgenerik'],
			 'idstformularium'	=> $_POST['idstformularium'],
			
		);
		return $dataArray;
	}
	
	function getNmjnsbrg(){
		$query = $this->db->getwhere('jbarang',array('idjnsbrg'=>$_POST['idjnsbrg']));
		$nm = $query->row_array();
		echo json_encode($nm['nmjnsbrg']);
    }
	
	function id_jnsbrg($where, $val){
		$query = $this->db->getwhere('jbarang',array($where=>$val));
		$id = $query->row_array();
		return $id['idjnsbrg'];
    }
	
	function getNmsatuan(){
		$query = $this->db->getwhere('jsatuan',array('idsatuan'=>$_POST['idsatuan']));
		$nm = $query->row_array();
		echo json_encode($nm['nmsatuan']);
    }
	
	function id_jsatuan($where, $val){
		$query = $this->db->getwhere('jsatuan',array($where=>$val));
		$id = $query->row_array();
		return $id['idsatuan'];
    }
	
	function getNmsatuanbsr(){
		$query = $this->db->getwhere('jsatuan',array('idsatuan'=>$_POST['idsatuan']));
		$nm = $query->row_array();
		echo json_encode($nm['nmsatuan']);
    }
	
	function id_jsatuanbsr($where, $val){
		$query = $this->db->getwhere('jsatuan',array($where=>$val));
		$id = $query->row_array();
		return $id['idsatuan'];
    }
	
	function getNmpabrik(){
		$query = $this->db->getwhere('pabrik',array('idpabrik'=>$_POST['idpabrik']));
		$nm = $query->row_array();
		echo json_encode($nm['nmpabrik']);
    }
	
	function id_pabrik($where, $val){
		$query = $this->db->getwhere('pabrik',array($where=>$val));
		$id = $query->row_array();
		return $id['idpabrik'];
    }
	
	function getNobrgmedis(){
		$q = "SELECT getotobrgmedis() as kode;";
        $query  = $this->db->query($q);
        $kode= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $kode=$row->kode;
        }
        return $kode;
	}
	
	function getNmsupplier(){
		$query = $this->db->getwhere('supplier',array('kdsupplier'=>$_POST['kdsupplier']));
		$nm = $query->row_array();
		echo json_encode($nm['nmsupplier']);
    }
	
	function kd_supplier($where, $val){
		$query = $this->db->getwhere('supplier',array($where=>$val));
		$id = $query->row_array();
		return $id['kdsupplier'];
    }
		
	function get_stgenerik(){      
        $this->db->select("*");
        $this->db->from("setting");
		$this->db->where("idklpsetting = '20'");
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_stformularium(){      
        $this->db->select("*");
        $this->db->from("setting");
		$this->db->where("idklpsetting = '21'");
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}
