<?php

class Perpembelian_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_perpembelian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
      
        $this->db->select("*, bagian.nmbagian, stsetuju.nmstsetuju, stpo.nmstpo, supplier.nmsupplier, pengguna.userid, pengguna.nmlengkap, pp.keterangan");
        $this->db->from("pp");
		$this->db->join('bagian',
				'bagian.idbagian = pp.idbagian', 'left');
		$this->db->join('stsetuju',
				'stsetuju.idstsetuju = pp.idstsetuju', 'left');
		$this->db->join('pengguna',
				'pengguna.userid = pp.userid', 'left');
		$this->db->join('supplier',
                'supplier.kdsupplier = pp.kdsupplier', 'left');
		$this->db->join('stpo',
                'stpo.idstpo = pp.idstpo', 'left');
				
		$this->db->order_by('nopp DESC');       
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select("*, bagian.nmbagian, stsetuju.nmstsetuju, stpo.nmstpo, supplier.nmsupplier, pengguna.userid, pengguna.nmlengkap, pp.keterangan");
        $this->db->from("pp");
		$this->db->join('bagian',
				'bagian.idbagian = pp.idbagian', 'left');
		$this->db->join('stsetuju',
				'stsetuju.idstsetuju = pp.idstsetuju', 'left');
		$this->db->join('pengguna',
				'pengguna.userid = pp.userid', 'left');
		$this->db->join('supplier',
                'supplier.kdsupplier = pp.kdsupplier', 'left');
		$this->db->join('stpo',
                'stpo.idstpo = pp.idstpo', 'left');    
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_perpembeliandet(){
		$start = $this->input->post("start");
        $limit = $this->input->post("limit");
        
        $fields = $this->input->post("fields");
        $query = $this->input->post("query");
		$nopp = $_POST["nopp"];
      
        $this->db->select("*");
        $this->db->from("v_ppdetail");
		//$this->db->order_by('');
		
		$where = array();
        $where['nopp']=$nopp;
                
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
		$this->db->where($where);
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_ppdetail');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function delete_perpembelian(){     
		$where['nopp'] = $_POST['nopp'];
		$del = $this->rhlib->deleteRecord('pp',$where);
        return $del;
    }
			
	/* function insert_perpembelian(){
		$dataArray = $this->getFieldsAndValues();
		$this->rhlib->insertRecord('pp',$dataArray);
		$ret["nopp"]=$dataArray['nopp']; 
        echo json_encode($ret);
    }

	function update_perpembelian(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('nopp', $_POST['nopp']);
		$this->db->update('pp', $dataArray);
        $ret["nopp"]=$_POST['nopp']; 
        echo json_encode($ret);
    } */
	
	function getFieldsAndValues(){
			$pp   = $this->input->post("nopp");
			$nopp = $this->getNoPp();
		
		$dataArray = array(
			 'nopp'			=> ($pp) ? $pp: $nopp,
			 'tglpp'		=> $_POST['tglpp'],
             'idbagian'		=> $_POST['idbagian'],
             'idstsetuju'	=> $_POST['idstsetuju'],
             'idstpo'		=> $_POST['idstpo'],
             'kdsupplier'	=> ($_POST['kdsupplier'])? $this->kd_supplier('nmsupplier',$_POST['kdsupplier']) : null,
			 'keterangan'	=> $_POST['keterangan'],
			 'userid'		=> $_POST['userid']			
		);
		return $dataArray;
	}
	
	function getNoPp(){
		$q = "SELECT getOtoNopp(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
	
	function getNmsupplier(){
		$query = $this->db->getwhere('supplier',array('kdsupplier'=>$_POST['kdsupplier']));
		$nm = $query->row_array();
		echo json_encode($nm['nmsupplier']);
    }
	
	function kd_supplier($where, $val){
		$query = $this->db->getwhere('supplier',array($where=>$val));
		$id = $query->row_array();
		return $id['kdsupplier'];
    }
		
	function cekbarang(){
        $q = "SELECT count(nopp) as nopp FROM ppdet where nopp='".$_POST['nopp']."' AND kdbrg='".$_POST['kdbrg']."'";
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->nopp;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
		
	function get_perpembelianx(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$nopp = $this->input->post("nopp");
      
        /* $this->db->select("*, bagian.nmbagian, stsetuju.nmstsetuju, pengguna.nmlengkap, (qty*harga) as subtotal,(qty*harga) as hsubtotal, ppdet.kdbrg as kdbrg"); */
        $this->db->select("*, bagian.nmbagian, stsetuju.nmstsetuju, pengguna.nmlengkap");
        $this->db->from("pp");
		$this->db->join('bagian',
				'bagian.idbagian = pp.idbagian', 'left');
		$this->db->join('stsetuju',
				'stsetuju.idstsetuju = pp.idstsetuju', 'left');
		$this->db->join('pengguna',
				'pengguna.userid = pp.userid', 'left');
		/* $this->db->join('ppdet',
				'pp.nopp = ppdet.nopp', 'left' 
		); */
		/* $this->db->join('barang',
				'barang.kdbrg = ppdet.kdbrg', 'left'
		);
		$this->db->join('jsatuan',
				'jsatuan.idsatuan = ppdet.idsatuan', 'left'
		);
		$this->db->join('hrgbrgsup',
				'hrgbrgsup.idhrgbrgsup = ppdet.idhrgbrgsup', 'left'
		);
		$this->db->join('matauang', 
				'matauang.idmatauang = hrgbrgsup.idmatauang', 'left'
		); */
		$this->db->join('supplier',
        'supplier.kdsupplier = pp.kdsupplier', 'left');
		$this->db->groupby('pp.nopp');
		$this->db->where('pp.idstsetuju',2);
	//	$this->db->where('pp.idstpo',1);
		$this->db->where_in('pp.idstpo',array(1,3));
	//	$this->db->or_like('pp.idstpo',3);
		if($nopp)$this->db->where("pp.nopp",$nopp);
		  
		$this->db->order_by('pp.nopp DESC');       
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_perpembelianxx(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$nopp = $this->input->post("nopp");
		$nopo = $this->input->post("nopo");
		$kdbrg = $this->input->post("kdbrg");
		$this->db->select("*");
		$this->db->group_by('kdbrg');
		
		if($nopo) {    
			$this->db->from("v_podet");  
		} else {
			$this->db->from("v_podetpp");
		}

	//	var_dump($kdbrg);
        /* $this->db->select("*, bagian.nmbagian, stsetuju.nmstsetuju,pengguna.nmlengkap, (qty*harga) as subtotal, (qty*harga) as hsubtotal, ppdet.kdbrg as kdbrg, hrgbrgsup.kdsupplier as kdsupplier"); */
       /*  $this->db->select("*, bagian.nmbagian, stsetuju.nmstsetuju,pengguna.nmlengkap, ppdet.kdbrg as kdbrg, (qty*hrgbeli) as subtotal,(qty*hrgbeli) as hsubtotal,(qty*hrgbeli) as htsubtotal,(hrgbeli / rasio) as hargajual, (hrgbeli / rasio) as hargajualtemp, jsatuan.nmsatuan as nmsatuanbsr ");
        $this->db->from("pp");
		$this->db->join('ppdet',
				'pp.nopp = ppdet.nopp', 'left' 
		);
		$this->db->join('bagian',
				'bagian.idbagian = pp.idbagian', 'left');
		$this->db->join('stsetuju',
				'stsetuju.idstsetuju = pp.idstsetuju', 'left');
		$this->db->join('pengguna',
				'pengguna.userid = pp.userid', 'left');
		
		$this->db->join('barang',
				'barang.kdbrg = ppdet.kdbrg', 'left'
		);
		$this->db->join('barangbagian',
				'barangbagian.kdbrg = ppdet.kdbrg', 'left'
		);
		$this->db->join('jsatuan',
				'jsatuan.idsatuan = barang.idsatuanbsr', 'left'
		);
		 $this->db->join('hrgbrgsup',
				'hrgbrgsup.idhrgbrgsup = ppdet.idhrgbrgsup', 'left'
		);
		$this->db->join('matauang', 
				'matauang.idmatauang = hrgbrgsup.idmatauang', 'left'
		); 
		$this->db->join('supplier',
        'supplier.kdsupplier = pp.kdsupplier', 'left'
		);
		$this->db->join('marginbrg',
		'marginbrg.kdbrg = barang.kdbrg', 'left'
		); */
		
		if($nopo) {    
			$this->db->where("nopo", $nopo);  
		} else {
			$this->db->where("nopp", $nopp);
			//$this->db->where('idbagian', 11);
		}
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_headpodiperpembelian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$nopp 					= $_POST["nopp"];
		
        $this->db->select("*, sypembayaran.nmsypembayaran, matauang.nmmatauang, pengguna.nmlengkap");
		$this->db->from("po");
		$this->db->join("sypembayaran", "sypembayaran.idsypembayaran = po.idsypembayaran","left");
		$this->db->join("matauang", "matauang.idmatauang = po.idmatauang","left");
		$this->db->join("pengguna", "pengguna.userid = po.userid","left");     
        
		$where = array();
		$where['po.nopp']=$nopp;
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(6,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrw($nopp);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrw($nopp){
      
        $this->db->select("*, sypembayaran.nmsypembayaran, matauang.nmmatauang, pengguna.nmlengkap");
		$this->db->from("po");
		$this->db->join("sypembayaran", "sypembayaran.idsypembayaran = po.idsypembayaran","left");
		$this->db->join("matauang", "matauang.idmatauang = po.idmatauang","left");
		$this->db->join("pengguna", "pengguna.userid = po.userid","left");
        
		$where = array();
		$where['po.nopp']=$nopp;		
		
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_detpodiperpembelian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$nopo 					= $_POST["nopo"];
		
        $this->db->select("*, barang.kdbrg, jsatuan.nmsatuan");
		$this->db->from("podet");
		$this->db->join("barang", "barang.kdbrg = podet.kdbrg","left");
		$this->db->join("jsatuan","jsatuan.idsatuan = podet.idsatuan","left");
        if($nopo != "") $this->db->where("podet.nopo",$nopo);
		
		//$where = array();
		//$where['podet.nopo']=$nopo;
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(6,0);
        }
		
		//$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->rw($nopo);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function rw($nopo){
      
		$this->db->select("*, barang.kdbrg, jsatuan.nmsatuan");
		$this->db->from("podet");
		$this->db->join("barang", "barang.kdbrg = podet.kdbrg","left");
		$this->db->join("jsatuan","jsatuan.idsatuan = podet.idsatuan","left");
        if($nopo != "") $this->db->where("podet.nopo",$nopo);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function insorupd_pp(){
        $this->db->trans_begin();
		$nopp = $this->input->post("nopp");
		$query = $this->db->getwhere('pp',array('nopp'=>$nopp));
		$nopp = $query->row_array();
				
		$pp = $this->insert_tblpp($nopp);
		$nopp = $pp['nopp'];
				
		if($nopp)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nopp"]=$nopp;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_tblpp($nopp){
		if(!$nopp){
			$dataArray = $this->getFieldsAndValues($nopp);
			$pp = $this->db->insert('pp',$dataArray);
		} else {
			$dataArray = $this->update_tblpp($nopp);
		}
		
		$query = $this->db->getwhere('ppdet',array('nopp'=>$dataArray['nopp']));
		$ppdet = $query->row_array();
		if($query->num_rows() == 0){
			$ppdet = $this->insert_tblppdet($dataArray);
		} else {
			$ppdet = $this->update_tblppdet($dataArray);
		}
		
		if($ppdet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function insert_tblppdet($pp){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrpp']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDett($pp, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('ppdet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function update_tblpp($nopp){
		$dataArray = $this->getFieldsAndValues($nopp);
		
		//UPDATE
		$this->db->where('nopp', $dataArray['nopp']);
		$pp = $this->db->update('pp', $dataArray);
		
		return $dataArray;
    }
	
	function update_tblppdet($pp){
		$where['nopp'] = $pp['nopp'];
		$this->db->delete('ppdet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrpp']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDett($pp, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('ppdet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }

	function getFieldsAndValuesDett($pp,$val1,$val2,$val3,$val4){
		$dataArray = array(
			'nopp' => $pp['nopp'],
			'kdbrg' => $val1,
			'idsatuan' => $val2,
			'qty' => $val3,
			'catatan' => $val4,			
		);
		return $dataArray;
	}
	
	function get_brgmedisdipp(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $this->input->post("key");
		$kdsupplier				= $this->input->post("kdsupplier");
		
        $this->db->select('*');
		$this->db->from('v_barangbagian');
		$this->db->order_by('kdbrg');
        $this->db->where("v_barangbagian.idbagian",11);
		
		$where = array();
		$where['v_barangbagian.kdsupplier']=$kdsupplier;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrow($key,$kdsupplier);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrow($key,$kdsupplier){
      
        $this->db->select('*');
		$this->db->from('v_barangbagian');
		$this->db->order_by('kdbrg');
        $this->db->where("v_barangbagian.idbagian",11);
		
		$where = array();
		$where['v_barangbagian.kdsupplier']=$kdsupplier;		
		$this->db->where($where);
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
}
