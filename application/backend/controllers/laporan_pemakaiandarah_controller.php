<?php 
class Laporan_pemakaiandarah_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_pemakaiandarah(){

		$tglawal = $this->input->post("tglawal");
        $tglakhir = $this->input->post("tglakhir");
		
		/* $start = $this->input->post("start");
        $limit = $this->input->post("limit");
		
		if ($start==null){
            $start = 0;
			$limit = 20;
		}
		
		$fields = $this->input->post("fields");
		$query = $this->input->post("query");
		
		$qlike = ""; // add query like
		if($fields!="" || $query !=""){
			$k=array('[',']','"');
			$r=str_replace($k, '', $fields);
			$b=explode(',', $r);
			$c=count($b);
			for($i=0;$i<$c;$i++){
				if ($i==0) {
					$qlike .= " WHERE ".$b[$i]." LIKE '%".$query."%'";
				} else {
					$qlike .= " OR ".$b[$i]." LIKE '%".$query."%'";
				}
			}
		}
			
		$q = "SELECT * FROM (SELECT * FROM v_pemakaiandarah WHERE tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."') Q".$qlike."";
		$query = $this->db->query($q)." LIMIT ".$start.", ".$limit); */
		
		$q = "SELECT  kw . tglkuitansi  AS  tglkuitansi 
					 ,  r . noreg  AS  noreg 
					 ,  p . nmpasien  AS  nmpasien 
					 ,  daekot . iddaerah  AS  iddaerah 
					 ,  daekot . nmdaerah  AS  nmdaerah 
					 ,  pj . nmpenjamin  AS  nmpenjamin 
					 , group_concat( py . nmpenyakit  SEPARATOR ',<br>') AS  nmpenyakit 
					 ,  g . nmgoldarah  AS  nmgoldarah 
					 ,  pl . nmpelayanan  AS  nmpelayanan 
					 , cast(sum( nd . qty ) AS SIGNED) AS  sumqty 
				FROM
				  (((((((((((((( registrasi   r 
				LEFT JOIN  pasien   p 
				ON (( r . norm  =  p . norm )))
				LEFT JOIN  goldarah   g 
				ON (( p . idgoldarah  =  g . idgoldarah )))
				LEFT JOIN  daerah   dae 
				ON (( dae . iddaerah  =  p . iddaerah )))
				LEFT JOIN  daerah   hdae 
				ON (( hdae . iddaerah  =  dae . dae_iddaerah )))
				LEFT JOIN  daerah   daekot 
				ON (( daekot . iddaerah  =  hdae . dae_iddaerah )))
				LEFT JOIN  penjamin   pj 
				ON (( pj . idpenjamin  =  r . idpenjamin )))
				LEFT JOIN  registrasidet   rd 
				ON (( r . noreg  =  rd . noreg )))
				LEFT JOIN  kodifikasi   k 
				ON (( rd . noreg  =  k . noreg )))
				LEFT JOIN  kodifikasidet   kd 
				ON (( k . idkodifikasi  =  kd . idkodifikasi )))
				LEFT JOIN  penyakit   py 
				ON (( kd . idpenyakit  =  py . idpenyakit )))
				LEFT JOIN  nota   n 
				ON (( rd . idregdet  =  n . idregdet )))
				LEFT JOIN  notadet   nd 
				ON (( n . nonota  =  nd . nonota )))
				LEFT JOIN  pelayanan   pl 
				ON (( nd . kditem  =  pl . kdpelayanan )))
				LEFT JOIN  kuitansi   kw 
				ON (( n . nokuitansi  =  kw . nokuitansi )))
				WHERE
				  (isnull( rd . tglbatal )
				  AND ( kw . idstkuitansi  = 1)
				  AND ( pl . kdpelayanan  IN ('T000000910', 'T000000985', 'T000000986', 'T000000987', 'T000000988', 'T000000989', 'T000000990', 'T000000991'))
				  AND  kw . tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."')
				GROUP BY
				   n . nonota ";
		$query = $this->db->query($q);
		
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
			
		$queryall  = $this->db->query($q);
			
		$ttl = $queryall->num_rows();
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

		if($ttl>0){
			$build_array["data"]=$data;
		}
		
		echo json_encode($build_array);
	}

}
