<?php

class Lapreturfarmasi_controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	//data retur farmasi pasien luar
	function get_lapreturfarmasi_pl(){
		$tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		
        $this->db->select('*');
        $this->db->from('v_returfarmasihed');
		
		if($tglakhir){
			$this->db->where('`tglreturfarmasi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}else{
			//kalau ngga di filter tanggal, gunakan tanggal hari ini.
			$this->db->where('`tglreturfarmasi`', date('Y-m-d'));
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result_array();
        }
		
		//get retur farmasi detail
		foreach($data as $idx => $dt){
			$data[$idx]['returfarmasi_detail'] = $this->get_returfarmasi_detail($dt['noreturfarmasi']);
		}
		
        $ttl = $this->db->count_all('v_returfarmasihed');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    
	}
	
	//data retur farmasi pasien RI / RJ
	function get_lapreturfarmasi_rirj(){
		$tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		
        $this->db->select('*');
        $this->db->from('v_returfarmasihedrjri');
		
		if($tglakhir){
			$this->db->where('`tglreturfarmasi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}else{
			$this->db->where('`tglreturfarmasi`', date('Y-m-d'));
		}
		
		$q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result_array();
        }
		
		//get retur farmasi detail
		foreach($data as $idx => $dt){
			$data[$idx]['returfarmasi_detail'] = $this->get_returfarmasi_detail($dt['noreturfarmasi']);
		}
		
        $ttl = $this->db->count_all('v_returfarmasihedrjri');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    
		
    }

	
	function get_returfarmasi_detail($noreturfarmasi)
	{
		$data = $this->db->get_where('v_returfarmasidet', array('noreturfarmasi' => $noreturfarmasi))->result_array();
		
		$total = 0;
		if(!empty($data)){
			foreach($data as $idx => $dt){
				$total += $dt['subtotal'];
			}
		}
		
		return array(
			'total' => $total,
			'data' => $data,
		);
	}
	
}
