<?php 

class Purchaseorder_Controller extends Controller {
	public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_PO(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$po					    = $this->input->post("idbagian");
		$tahun 					= $this->input->post("tahun");
		$bulan 					= $this->input->post("bulan");
		$stpo 					= $this->input->post("statuspo");
		$setuju					= $this->input->post("setuju");
		
		$tgl1                   = $this->input->post("tglawal");
		$tgl2                   = $this->input->post("tglakhir");
		$key					= $_POST["key"]; 
	//	$nopp = $this->input->post("nopp");
		$this->db->select("*");
		//$this->db->from("po");
		$this->db->from("v_po");
		/* //$this->db->join("podet", "po.nopo = podet.nopo", "left");
		$this->db->join("pp", "po.nopp = pp.nopp", "left");
		$this->db->join("supplier", "supplier.kdsupplier = po.kdsupplier","left");
		$this->db->join("stsetuju", "stsetuju.idstsetuju = po.idstsetuju","left");
		$this->db->join("jpembayaran", "jpembayaran.idjnspembayaran = po.idjnspembayaran","left");
		$this->db->join("pengguna", "po.userid = pengguna.userid", "left");
		$this->db->join("bayar", "bayar.nopo = po.nopo", "left");
		
		$this->db->join("stpo","stpo.idstpo = pp.idstpo","left"); */
	//	$this->db->where("nopo", $_POST['tf_nopo']);
		$this->db->order_by('nopo DESC');
		//$this->db->group_by('nopo');
	//	if($nopp)$this->db->where("nopp", $nopp);
		if($tahun != '')$this->db->like('tglpo', $tahun);
		if($bulan != '')$this->db->like('month(tglpo)', $bulan);
		
		if($stpo != '')$this->db->where('po.idstpo',$stpo);
		if($setuju != '')$this->db->where('po.idstsetuju',$setuju);
		if($this->input->post('chb_nopo')=='true'){	
			if ($key=='1'){
				$id     = $_POST["id"];
				$name   = $_POST["name"];
				$this->db->or_like($id, $name);
			}  
	    }
		
		if(($tgl1 && $tgl2 != ''))
			$this->db->where("date(tglpo) between '". $tgl1 ."' and '". $tgl2."'" );
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		} 
		
		/* if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        } */
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($key, $tgl1, $tgl2);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function numrow($key, $tgl1, $tgl2){
      
        $this->db->select("*");
		$this->db->from("v_po");
		
		if(($tgl1 && $tgl2 != ''))
			$this->db->where("date(tglpo) between '". $tgl1 ."' and '". $tgl2."'" );
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		} 
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	
	function insorupd_po(){
		$this->db->trans_begin();
		$arrpo = $this->input->post("arrpo");
		$nopo = $this->input->post("nopo");
		
		$query = $this->db->getwhere('po',array('nopo'=>$nopo));
		$po = $query->row_array();
		
		if($query->num_rows() == 0){
			$pos = $this->insert_po($po);
			$vpo = $pos['nopo'];
		}else{
			$pos = $query->row_array();
			$vpo = $pos['nopo'];
		}
		
		$podet = $this->insert_podet($vpo, $arrpo);
		
		if($vpo && $podet)
		{
            $this->db->trans_commit();
			$this->update_pp();
			$ret["success"] = true;
            $ret["nopo"]=$vpo;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		echo json_encode($ret);
  
		
	}
	
	function insert_po(){
		$dataArray = $this->getFieldsAndValues();
		$z = $this->db->insert('po',$dataArray);
		if($z){
			$ret = $dataArray;
		}else{
			$ret = false;
		}
		return $ret;
	}
	
	function insert_podet($nopo, $arrpo){
		//$where['nopol'] = $nopol;
	//	$this->db->delete('podet', $where);
		
		$nmsupplier = $this->input->post('nmsupplier');
		$kdsupplier = $this->input->post('kdsupplier');
		$tgl        = $this->input->post('tglpo');
		$time       = date('H:i:s');
		
		$stpo=($_POST['idstpo']) ? $_POST['idstpo']:null;
		$k=array('[',']', '"');
		$r= str_replace($k, '', $arrpo);
		$b = explode(',',$r);
		$no = 1;
		foreach($b as $val){
			$norut = $no++;
			$vale = explode('-', $val);
				$dataArray = $this->getFieldsAndValuesDet($vale[0],$vale[1],$vale[2],$vale[3],$vale[4],$vale[5],$vale[6],$vale[9],$vale[10],$vale[11],$vale[7],$nopo,$norut);
				$z = $this->db->insert('podet',$dataArray);
				/* $this->db->query("CALL SP_stokfrompo (?,?,?,?)",array(
					$vale[2]*$vale[7],
					$vale[0],
					$vale[3],
					$vale[8]
				)); */
				$this->db->query("Call in_pokartustok(?,?,?,?,?,?,?,?,?,?,?,?,?)",array(
							$vale[0],
							$vale[2]*$vale[7],
							$vale[3],
							$vale[8],
							$vale[2],
							$this->session->userdata['user_id'],
							$nopo,
							$kdsupplier,
							$nmsupplier,
							$tgl,
							$time,
							$vale[13],
							$vale[6],
				));
				
				$this->db->query("CALL sp_updatebrgfrompodet (?,?,?,?)",array(
					$vale[0],
					$vale[13],
					$vale[6],
					$stpo
				));
		}
		return true;
	}
	
	function update_stok(){
		$k = array('[',']', '"');
		$r = str_replace($k, '', $_POST['arrstok']);
	}
	
	function update_pp(){
		$dataArray = $this->getValueEdit();
		
		$this->db->where("nopp", $dataArray['nopp']);
		$this->db->update("pp",$dataArray);
		
		 if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
            $ret=$dataArray;
        }
        return $ret;
	}
	
// update
	function updateAll(){
		
		$this->db->trans_begin();
		$arrpo = $this->input->post("arrpo");
		$nopo = $this->input->post("nopo");
		
		$query = $this->db->getwhere('po',array('nopo'=>$nopo));
		$po = $query->row_array();
		
	/* 	if($query->num_rows() == 0){
			$pos = $this->update_po($nopo);
			$vpo = $pos['nopo'];
		}else{
			$pos = $query->row_array();
			$vpo = $pos['nopo'];
		} */
		$pos = $this->update_po($nopo);
		$podet = $this->update_podet($pos, $arrpo);
		$pp = $this->update_pp();
		
		if($pos && $podet && $pp)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
        //    $ret["nopo"]=$vpo;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		echo json_encode($ret);
  
	}
	function update_po($nopo){
		$dataArray = $this->getFieldsAndValues();
		$this->db->where('nopo',$dataArray['nopo']);
		$z = $this->db->update('po',$dataArray);
		if($z){
			$ret = $dataArray;
		}else{
			$ret = false;
		}
		return $ret;
	}
	function update_podet($nopo, $arrpo){
		$k=array('[',']', '"');
		$r= str_replace($k, '', $arrpo);
		$b = explode(',',$r);
		foreach($b as $val){
			$vale = explode('-', $val);			
				$dataArray = $this->getFieldsAndValuesDetForUpdate($vale[0],$vale[1],$vale[2],$vale[3],$vale[4],$vale[5],$vale[6],$vale[9],$vale[10],$vale[11]);
				$this->db->where('nopo',$_POST['nopo']);
				$this->db->where('kdbrg',$vale[0]);
				$z = $this->db->update('podet',$dataArray);
				
				/* $this->db->query("CALL sp_updatebrgfrompodet (?,?,?,?)",array(
					$vale[0],
					$vale[13],
					$vale[14],
					$stpo
				)); */
				
		}
		return true;
	}
	function getFieldsAndValues(){
		if(is_null($_POST['nopo']) || $_POST['nopo'] == ''){
			$nopo = $this->getNoPO($_POST['tglpo']);
		}else{
			$nopo = $_POST['nopo'];
		}
		$dataArray = array(
			'nopo' => $nopo,
			'tglpo' => $_POST['tglpo'],//date('Y-m-d'),
			'idjnspp' => $_POST['idjnspp'],
			'nopp' => ($_POST['nopp'] != '')?$_POST['nopp']:null,
			'tglpp' => $_POST['tglpp'],
			'idbagian' => 11,
			'kdsupplier' => $_POST['kdsupplier'],
			'idsypembayaran' => ($_POST['idsypembayaran']) ? $_POST['idsypembayaran']:null,
			'idjnspembayaran' => ($_POST['idjnspembayaran']) ? $_POST['idjnspembayaran']:null,
			'tglpengiriman' => date('Y-m-d'),
		//	'idstpo' => $_POST['idstpo'],
			'idstsetuju' => 2,
			'idmatauang' => 1,
			'bpb' => $_POST['bpb'],
			'ketpo' => $_POST['keterangan'],
		//	'idstdiskon' => $_POST['idstdiskon'],
		//	'diskon' => $_POST['diskon'],
		//	'ppnrp' => '',
			'totalpo' => $_POST['totalpo'],
			'userid'=> $this->session->userdata['user_id'],
       //     'tglinput'=> date('Y-m-d'),
			'approval1' => $_POST['approval1'],
		//	'approval2' => $_POST['approval2'], 
			'tgljatuhtempo' => $_POST['tgljatuhtempo'],
			'idstkontrabon' => $_POST['idstkontrabon'],
		);
		return $dataArray;
	}
	
	function getFieldsAndValuesDet($val1,$val2,$val3,$val4,$val5,$val6,$val7,$val8,$val9,$val12,$val13,$nopo,$norut){
	$query = $this->db->getwhere('hrgbrgsup',array('kdbrg'=>$val1));
	$item = $query = $query->row_array();
		$dataArray = array(
			'nopo' => $nopo,
			'kdbrg' => $val1,
			'idsatuan' => $val2,
			'qty' => $val3,
			'rasio' => $val13,
			'qtybonus' => $val4,
			'hargabeli' => $val5,
			'diskon' => $val6,
			'ppn' => $val9,
			'hargajual' => $val7,
			'margin' => $val8,
			'diskonrp' => $val12,
			'nourut' => $norut
		);
		// var_dump($dataArray);
		// exit;
		return $dataArray;
	}
	function getFieldsAndValuesDetForUpdate($val1,$val2,$val3,$val4,$val5,$val6,$val7,$val8,$val9,$val10){
		
		$dataArray = array(
		
			'idsatuan' => $val2,
			'qty' => $val3,
			'qtybonus' => $val4,
			'hargabeli' => $val5,
			'diskon' => $val6,
			'hargajual' => $val7,
			'margin' => $val8,
			'ppn' => $val9,
			'diskonrp' => $val10,
			
		);
		return $dataArray;
	}
	
	function getValueEdit(){
		$dataArray = array(
			'nopp' => $_POST['nopp'],
			'idstpo' => ($_POST['idstpo']) ? $_POST['idstpo']:null,
		);
		return $dataArray;
	}
	
	function getNoPO($tglpo){
		$q = "SELECT getOtoNopo('".$tglpo."') as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
	
	function app1(){
	$sql =	$this->db->query("select * from setting where kdset = 'ACCPO1'");
	$num = $sql->num_rows();
			if($num>0){
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);			
			}else{
				return 0;
			} 
	
	}
	function app2(){
	$sql =	$this->db->query("select * from setting where kdset = 'ACCPO2'");
	$num = $sql->num_rows();
			if($num>0){
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);			
			}else{
				return 0;
			} 
	
	}
	function getDataPo(){
		$nopo = str_pad($_POST['nopo'], 10, "0", STR_PAD_LEFT);
        $this->db->select("*");
        //$this->db->from("v_ambilpo");
		//$this->db->where('nopo1',$nopo);
		$this->db->from("v_po"); //dhy15
		$this->db->where('nopo',$nopo); //dhy15
		$q = $this->db->get();
		$po = $q->row_array();
		echo json_encode($po);
    }
	function getPodet(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$nopo 					= $this->input->post("nopo");
		$this->db->select("*");
		$this->db->from("podet");
		$this->db->where("nopo",$nopo);
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('podet');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	function getDataPP(){
		$nopp = str_pad($_POST['nopp'], 10, "0", STR_PAD_LEFT);
        $this->db->select("*, bagian.nmbagian, stsetuju.nmstsetuju, stpo.nmstpo, supplier.nmsupplier, pengguna.userid, pengguna.nmlengkap, pp.keterangan");
        $this->db->from("pp");
		$this->db->join('bagian',
				'bagian.idbagian = pp.idbagian', 'left');
		$this->db->join('stsetuju',
				'stsetuju.idstsetuju = pp.idstsetuju', 'left');
		$this->db->join('pengguna',
				'pengguna.userid = pp.userid', 'left');
		$this->db->join('supplier',
                'supplier.kdsupplier = pp.kdsupplier', 'left');
		$this->db->join('stpo',
                'stpo.idstpo = pp.idstpo', 'left');
				
		$this->db->order_by('nopp DESC');      
		$this->db->where('nopp',$nopp); //dhy15
		$q = $this->db->get();
		$pp = $q->row_array();
		echo json_encode($pp);
    }
	
	function get_podet_ppdet(){

		$nopp = $this->input->post("nopp");
		$nopo = $this->input->post("nopo");

		$isupdate = $this->input->post("isupdate");
		
		$countpp = $this->getcountpp($nopp);
		$maxpp = $this->getmaxtglpo($nopp);
		
		//echo '<script> alert('.$this->getqtypp($nopp,'B000000001').'); </script>';
		
		if ($isupdate=='1') {
			$q = $this->db->query("SELECT `podet`.`nopo` AS `nopo`
										 , `podet`.`kdbrg` AS `kdbrg`
										 , `barang`.`nmbrg` AS `nmbrg`
										 , `podet`.`idsatuan` AS `idsatuanbsr`
										 , `jsatuan`.`nmsatuan` AS `nmsatuanbsr`
										 , `podet`.`qty` AS `qty`
										 , `podet`.`qtybonus` AS `qtyb`
										 , (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
											FROM
											  `retursupplierdet`
											WHERE
											  ((`retursupplierdet`.`nopo` = `podet`.`nopo`)
											  AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
											  AND (`retursupplierdet`.`idstbayar` <> 2))) AS `qtyretur`

										 , (`podet`.`qty` - (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
															 FROM
															   `retursupplierdet`
															 WHERE
															   ((`retursupplierdet`.`nopo` = `podet`.`nopo`)
															   AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
															   AND (`retursupplierdet`.`idstbayar` <> 2)))) qty_qtyret

										 , `podet`.`hargabeli` AS `hrgbeli`
										 , `podet`.`diskon` AS `diskon`
										 , `podet`.`ppn` AS `ppn`
										 , `podet`.`hargajual` AS `hargajual1`
										 , if(((`podet`.`qty` - (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
																 FROM
																   `retursupplierdet`
																 WHERE
																   ((`retursupplierdet`.`nopo` = `podet`.`nopo`)
																   AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
																   AND (`retursupplierdet`.`idstbayar` <> 2)))) = '0'), `podet`.`diskonrp`, ((`podet`.`hargabeli` * (`podet`.`qty` - (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
																																													  FROM
																																														`retursupplierdet`
																																													  WHERE
																																														((`retursupplierdet`.`nopo` = `podet`.`nopo`)
																																														AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
																																														AND (`retursupplierdet`.`idstbayar` <> 2))))) * `podet`.diskon / 100)) AS `diskonrp`

										 , if(((`podet`.`qty` - (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
																 FROM
																   `retursupplierdet`
																 WHERE
																   ((`retursupplierdet`.`nopo` = `podet`.`nopo`)
																   AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
																   AND (`retursupplierdet`.`idstbayar` <> 2)))) = '0'), (`podet`.`qty` * `barang`.`hrgbeli`), (((`podet`.`hargabeli` * (`podet`.`qty` - (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
																																																		   FROM
																																																			 `retursupplierdet`
																																																		   WHERE
																																																			 ((`retursupplierdet`.`nopo` = `podet`.`nopo`)
																																																			 AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
																																																			 AND (`retursupplierdet`.`idstbayar` <> 2))))) - ((`podet`.`hargabeli` * (`podet`.`qty` - (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
																																																																									   FROM
																																																																										 `retursupplierdet`
																																																																									   WHERE
																																																																										 ((`retursupplierdet`.`nopo` = `podet`.`nopo`)
																																																																										 AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
																																																																										 AND (`retursupplierdet`.`idstbayar` <> 2))))) * `podet`.diskon / 100)) + (((`podet`.`hargabeli` * (`podet`.`qty` - (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
																																																																																																							 FROM
																																																																																																							   `retursupplierdet`
																																																																																																							 WHERE
																																																																																																							   ((`retursupplierdet`.`nopo` = `podet`.`nopo`)
																																																																																																							   AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
																																																																																																							   AND (`retursupplierdet`.`idstbayar` <> 2))))) - ((`podet`.`hargabeli` * (`podet`.`qty` - (SELECT ifnull(sum(`retursupplierdet`.`qty`), 0) AS jumlah
																																																																																																																														 FROM
																																																																																																																														   `retursupplierdet`
																																																																																																																														 WHERE
																																																																																																																														   ((`retursupplierdet`.`nopo` = `podet`.`nopo`)
																																																																																																																														   AND (`retursupplierdet`.`kdbrg` = `podet`.`kdbrg`)
																																																																																																																														   AND (`retursupplierdet`.`idstbayar` <> 2))))) * `podet`.diskon / 100)) * 0.1))) `subtotal`

										 , `podet`.`margin` AS `margin`
										 , (`barang`.`hrgbeli` / `barang`.`rasio`) AS `hargajual`
										 , (`barang`.`hrgbeli` / `barang`.`rasio`) AS `hargajualtemp`
										 , `podet`.`rasio` AS `rasio`
										 , (SELECT `po`.`tglpo` AS `tglpo`
											FROM
											  `po`
											WHERE
											  (`po`.`nopo` = `podet`.`nopo`)) AS `tglpo`
										 , (SELECT `po`.`nopp` AS `nopp`
											FROM
											  `po`
											WHERE
											  (`po`.`nopo` = `podet`.`nopo`)) AS `nopp`
									FROM
									  ((`podet`
									LEFT JOIN `barang`
									ON ((`barang`.`kdbrg` = `podet`.`kdbrg`)))
									LEFT JOIN `jsatuan`
									ON ((`jsatuan`.`idsatuan` = `podet`.`idsatuan`)))
									where `podet`.nopo='".$nopo."'
									ORDER BY podet.nopo, podet.nourut ASC");
		} else {
			if($countpp==0) {
				$q = $this->db->query("select * from v_podetpp where nopp='".$nopp."'");
			} else {
				$q = $this->db->query("select * from v_podet where nopp='".$nopp."' and tglpo='".$maxpp."' order by nopo desc limit 1");
			}
		}

        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){

			if ($isupdate=='1') {
					
					$ppnn = 0;
					$s1 = 0;
					$subtotal = 0;
					$hbelisatuankcl = 0;
					$hj1 = 0;
					$hj2 = 0;
					$hjual = 0;
					
					foreach($data as $row) {
					
						$ppn 	= ($row->ppn==10.00) ? 0.1:0;
						//$qtynol = ($row->qty_qtyret==0) ? 0:($s1 + ($s1*$ppn));						
						
						//untuk memnentukan subtotal
						$s1 = (($row->hrgbeli * $row->qty_qtyret)-$row->diskonrp);
						$subtotal = ($row->qty_qtyret==0) ? 0:($s1 + ($s1*$ppn));
						
						//untuk memnentukan harga belisatuan kecil
						$hbelisatuankcl = ($row->qty_qtyret==0) ? 0:($subtotal / ($row->qty_qtyret * $row->rasio));
						
						//untuk memnentukan hrgjual
						$hj1 = $row->hargajual;
						$hj2 = ($hj1 + ($hj1 * $ppn));				
						$hjual = ($hj2 + ($hj2 *($row->margin / 100)));	
						
						array_push($build_array["data"],array(
							'nopo'=>$row->nopo,
							'kdbrg'=>$row->kdbrg,
							'nmbrg'=>$row->nmbrg,
							'idsatuanbsr'=>$row->idsatuanbsr,
							'nmsatuanbsr'=>$row->nmsatuanbsr,
							'qty'=>$row->qty,
							'qtyb'=>$row->qtyb,
							'qtyretur'=>$row->qtyretur,
							'qty_qtyret'=>$row->qty_qtyret,
							'hrgbeli'=>$row->hrgbeli,
							'diskon'=>($row->qty_qtyret==0) ? 0:$row->diskon,
							'tamppn'=>$row->ppn,
							'ppn'=>($row->ppn==10.00) ? '1':'0',
							'hargajual'=>($row->qty_qtyret==0) ? 0:$row->hargajual1,
							'diskonrp'=>($row->qty_qtyret==0) ? 0:$row->diskonrp,
							'margin'=>$row->margin,
							'subtotal'=>($row->qty_qtyret==0) ? 0:(($row->qtyretur ==0) ? ($s1 + ($s1 * $ppn)) : $row->subtotal), //($s1 + ($s1 * $ppn)),
							'subtotaltemp'=>$row->subtotal,
							'hsubtotal'=>$row->subtotal,
							'htsubtotal'=>$row->subtotal,
							'hargajual1'=>$hjual,
							'hargajualtemp'=>$row->hargajualtemp,
							'rasio'=>$row->rasio,
							'hrgbelikcl'=>$hbelisatuankcl,
							'tglpo'=>$row->tglpo,
							'nopp'=>$row->nopp,
							
						));
					}  
			} else {
				if($countpp==0) {
					foreach($data as $row) {	
						
						$subtot = 0;
						$subtot = ($this->getitembrg('hrgbeli','barang','kdbrg',$row->kdbrg) * $row->qty);						
						
						//untuk memnentukan harga belisatuan kecil
						$hbelisatuankcl = 0;
						$hbelisatuankcl = ($subtot / ($row->qty * $row->rasio));
												
						$hj1 = 0;
						$hj2 = 0;
						$hjual = 0;
						//untuk memnentukan hrgjual
						$hj1 = ($this->getitembrg('hrgbeli','barang','kdbrg',$row->kdbrg) / $row->rasio);
						$hj2 = ($hj1 + ($hj1 * 0));				
						$hjual = ($hj2 + ($hj2 *($row->margin / 100)));
						
						array_push($build_array["data"],array(
							'nopp'=>$row->nopp,
							'idsatuanbsr'=>$row->idsatuanbsr,
							'nmsatuanbsr'=>$row->nmsatuanbsr,
							'qty'=>$row->qty,
							'catatan'=>$row->catatan,
							'idhrgbrgsup'=>$row->idhrgbrgsup,
							'kdbrg'=>$row->kdbrg,
							'idstpp'=>$row->idstpp,
							'nmstpp'=>$row->nmstpp,
							'margin'=>$row->margin,							
							'nopo'=>$row->nopo,
							'nmbrg'=>$row->nmbrg,
							'subtotal'=>$row->hrgbeli,	
							'hrgbeli'=>$this->getitembrg('hrgbeli','barang','kdbrg',$row->kdbrg),//$row->hrgbeli,
							'hrgjual'=>$row->hrgjual,
							'harga'=>$row->harga,	
							'rasio'=> $row->rasio,
							'diskonrp'=>0,
							'diskon'=>0,
							'ppn'=>'',
							'subtotal'=>$subtot,
							'hrgbelikcl'=>$hbelisatuankcl,
							'hargajual'=>$hjual,

						));
					}
				} else {
					foreach($data as $row) {
						array_push($build_array["data"],array(
							'nopo'=>$row->nopo,
							'kdbrg'=>$row->kdbrg,
							'nmbrg'=>$row->nmbrg,
							'idsatuanbsr'=>$row->idsatuanbsr,
							'nmsatuanbsr'=>$row->nmsatuanbsr,
							'qty'=>$this->getqtypp($row->nopp,$row->kdbrg) - $this->getqtysum($row->nopp,$row->kdbrg),
							'qtyb'=>$row->qtyb,
							'hrgbeli'=>$this->getitembrg('hrgbeli','barang','kdbrg',$row->kdbrg),//$row->hrgbeli,
							'diskon'=>'',//$row->diskon,
							'tamppn'=>$row->ppn,
							'ppn'=>'',//($row->ppn==10.00) ? '1':'0',
							'hargajual1'=>$row->hargajual1,
							'diskonrp'=>'',//$row->diskonrp,
							'margin'=>$row->margin,
							'subtotal'=>$this->getitembrg('hrgbeli','barang','kdbrg',$row->kdbrg),//$row->subtotal,
							'hsubtotal'=>$row->hsubtotal,
							'htsubtotal'=>$row->htsubtotal,
							'hargajual'=>$this->getitembrg('hrgjual','barang','kdbrg',$row->kdbrg),//$row->hargajual,
							'hargajualtemp'=>$row->hargajualtemp,
							'rasio'=>$row->rasio,
							'tglpo'=>$row->tglpo,
							'nopp'=>$row->nopp,

						));
					}
				}
			}
			
        }
		
        echo json_encode($build_array);
    }
	
	function getcountpp($nopp){
		$this->db->select("count(nopp) AS countnopp");
        $this->db->from("po");
        $this->db->where('nopp',$nopp);
		$q = $this->db->get();
		$data = $q->row_array();
		return $data['countnopp'];
	}
	
	function getmaxtglpo($nopp){
		$this->db->select("max(tglpo) AS maxtglpo");
        $this->db->from("v_podet");
        $this->db->where('nopp',$nopp);
		$q = $this->db->get();
		$data = $q->row_array();
		return $data['maxtglpo'];
	}
	
	function getqtypp($nopp,$kdbrg){
		$q = $this->db->query("select * from v_podetpp where nopp='".$nopp."' and kdbrg='".$kdbrg."'");
		$data = $q->row_array();
		return $data['qty'];
	}
	
	function getqtysum($nopp,$kdbrg){
		$q = $this->db->query("SELECT sum(qty) as sumqty
		FROM
		  v_podet
		WHERE
		  nopp = '".$nopp."'
		  AND kdbrg = '".$kdbrg."'");
		$data = $q->row_array();
		return $data['sumqty'];
	}
	
	function getitembrg($fields,$table,$fieldw,$id){
		$q = $this->db->query("select $fields as fieldnya from $table where $fieldw='".$id."'");
		$data = $q->row_array();
		return $data['fieldnya'];
	}
	
	function get_brgmedisdipo(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
		$key					= $_POST["key"]; 
		
		$val                  = $this->input->post("val");
		
        $this->db->select('*,(hrgbeli / rasio) as hargajual, (hrgbeli / rasio) as hargajualtemp');
		$this->db->from('v_barang');
		$this->db->order_by('kdbrg');        
        
		if($val){
			$x=array('[',']','"');
            $y=str_replace($x, '', $val);
            $z=explode(',', $y);
			$this->db->where_not_in('kdbrg',$z);
		}     
        
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$arrnm = explode(' ', $name);
			foreach($arrnm AS $vall){
				$this->db->like($id, $vall);
			}
		}
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->rw($key);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function rw($key){
      
        $this->db->select('*');
		$this->db->from('v_barang');
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$arrnm = explode(' ', $name);
			foreach($arrnm AS $vall){
				$this->db->like($id, $vall);
			}
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function insert_bayar(){
		$dataArray = $this->getFieldsAndValuesb();
		$ret = $this->db->insert('bayar',$dataArray);
		$no = $dataArray['nobayar'];
		
		$dataArrayy = $this->getFieldsAndValuesbdet($no);
		$ret = $this->db->insert('bayardet',$dataArrayy);
        return $ret;
    }
			
	function getFieldsAndValuesb(){
		$getNoBayar = $this->getNoBayar();
			
		$dataArray = array(
			 'nobayar'			=> $getNoBayar,
			 'tglbayar'			=> date('Y-m-d'),
			 'nopo'				=> $_POST['nopo'],
			 'userid'			=> $this->session->userdata['user_id'],
			 'approval'			=> $_POST['approval'],
		);		
		return $dataArray;
	}
	
	function getFieldsAndValuesbdet($no){
		$dataArrayy = array(
			 'nobayar'			=> $no,
			 'idcarabayar'		=> $_POST['idsypembayaran'],
			 'nominal'			=> $_POST['totalpo'],
		);
		return $dataArrayy;
	}
	
	function getNoBayar(){
		$q = "SELECT getOtoNobayar(now()) as no;";
        $query  = $this->db->query($q);
        $no= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $no=$row->no;
        }
        return $no;
	}
	
	function get_tgl(){
		$jmlhari 	= $this->input->post("jmlhari");		
		$tglppo 	= $this->input->post("tglpo");	
		$n = date("n", strtotime($tglppo));
		$j = date("j", strtotime($tglppo));
		$y = date("Y", strtotime($tglppo));
		
		if($jmlhari != 0){			
			$hariberikutnya = mktime(0,0,0,$n,$j+$jmlhari,$y);				
			echo date("d-m-Y", $hariberikutnya);
		}else{			
			echo date("d-m-Y", strtotime($tglppo));			
		}
	}
}