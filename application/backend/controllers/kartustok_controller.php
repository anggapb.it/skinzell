<?php 
class Kartustok_Controller extends Controller {
    public function __construct(){
        parent::Controller();
            $this->load->library('session');
            $this->load->library('rhlib');
    }
    
    function get_bagian_krtstok(){
        $start = $this->input->post("start");
        $limit = $this->input->post("limit");
        
        $fields = $this->input->post("fields");
        $query = $this->input->post("query");
        $jpel = $this->input->post("jpel");
        $val = $this->input->post("val");
      
        $this->db->select('bagian.*, lvlbagian.nmlvlbagian, jhirarki.nmjnshirarki, jpelayanan.nmjnspelayanan, bdgrawat.nmbdgrawat, penggunabagian.userid');
        $this->db->from('bagian');
        $this->db->join('lvlbagian',
                'lvlbagian.idlvlbagian = bagian.idlvlbagian', 'left');
        $this->db->join('jhirarki',
                'jhirarki.idjnshirarki = bagian.idjnshirarki', 'left');
        $this->db->join('jpelayanan',
                'jpelayanan.idjnspelayanan = bagian.idjnspelayanan', 'left');
        $this->db->join('bdgrawat',
                'bdgrawat.idbdgrawat = bagian.idbdgrawat', 'left');
        $this->db->join('penggunabagian',
                'bagian.idbagian = penggunabagian.idbagian', 'left');
        $this->db->where('kdbagian !=','null');
        $this->db->where('penggunabagian.userid',$this->session->userdata("user_id"));
        //$this->db->where('bagian.idlvlbagian <>',1);
        
        if($jpel == 1){
            $this->db->where('bagian.idjnspelayanan',$jpel);
        } else if($jpel == 2){
            $this->db->where('bagian.idjnspelayanan',$jpel);
        } else if($jpel == 3){
            $this->db->where('bagian.idjnspelayanan',$jpel);
        }
        
        if($val != ''){
            $x=array('[',']','"');
            $y=str_replace($x, '', $val);
            $z=explode(',', $y);
            $this->db->where_not_in('bagian.idbagian',$z);
        }
        
        $this->db->order_by('kdbagian');
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        
        echo json_encode($build_array);
    }
    
    function get_brgbagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $key                    = $this->input->post("key");
        $idbagian               = $this->input->post("idbagian");
        $this->db->select("*");
        $this->db->from("v_barangbagian");
        if($idbagian)$this->db->where('idbagian', $idbagian);
        if ($key=='1'){
                $id     = $_POST["id"];
                $name   = $_POST["name"];
                $this->db->or_like($id, $name);
        } 
         if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
         
        
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(10,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

       if($ttl>0){
            $build_array["data"]=$data;
        } 
        
        echo json_encode($build_array);
    }
    
    function numrow($fields, $query){
     //   $cidbagian                = $this->input->post("cidbagian");
        
        $this->db->select('*');
        $this->db->from('v_barangbagian');
    //  if($cidbagian != '')$this->db->where('idbagian', $cidbagian);

        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
     
    function get_kartustok(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
        $kdbrg                  = $this->input->post("kdbrg");
        $idbagian               = $this->input->post("idbagian");
        $tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
        $this->db->select("*");
        $this->db->from("v_kartustok");
        //$this->db->join("jkartustok","jkartustok.idjnskartustok = kartustok.idjnskartustok", "left");
        
        if($idbagian)$this->db->where('v_kartustok.idbagian',$idbagian);
        if($kdbrg)$this->db->where('v_kartustok.kdbrg', $kdbrg);
    
        if($tglawal){
        
        $this->db->where('v_kartustok.tglkartustok BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
        }
         if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
       /*  if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(10,0);
        } */
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        
        $ttl = $this->numrow2($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

       if($ttl>0){
            $build_array["data"]=$data;
        } 
        
        echo json_encode($build_array);
    }
    
    function numrow2($fields, $query){
        $kdbrg                  = $this->input->post("kdbrg");
        $tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
        $this->db->select("*");
        $this->db->from("v_kartustok");
        $this->db->where('v_kartustok.kdbrg', $kdbrg);
        if($tglawal){
        
        $this->db->where('v_kartustok.`tglkartustok` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
        }
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
    
    function getCekRJ(){
        $this->db->select("*");
        $this->db->from("kartustok");
        $this->db->where('noref',$_POST['nonota']);
        $q = $this->db->get();
        $kartustok['cek'] = $q->num_rows();
        echo json_encode($kartustok);
    }
    
    function getTglstokbrg(){
        $kdbrg = $this->input->post("kdbrg");
        $query = "select DATE_FORMAT(so.tglso,'%d-%m-%Y') as tglso, DATE_FORMAT(so.tglso,'%Y-%m-%d') as tglsoval, sodet.kdbrg
                    from sodet left join so on so.noso=sodet.noso where sodet.kdbrg='".$kdbrg."' and so.idstso <> '1' ORDER BY so.tglso DESC";
        $q = $this->db->query($query);
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
        
        $ttl = $this->db->count_all('sodet');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        
        echo json_encode($build_array);
    }
    
}
?>