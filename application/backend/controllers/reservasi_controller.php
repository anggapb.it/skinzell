<?php

class Reservasi_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_reservasi(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
		
        $tglreservasi           = $this->input->post("tglreservasi");
        $norm             		= $this->input->post("norm");
        $nmpasien             		= $this->input->post("nmpasien");
        $upel             		= $this->input->post("upel");
        $upel2             		= $this->input->post("upel2");
        $nmdokter                 = $this->input->post("nmdokter");
        $dokter                 = $this->input->post("dokter");
        $email                 = $this->input->post("email");
        $notelp                 = $this->input->post("notelp");
        $streservasi         	= $this->input->post("streservasi");
        $idreservasi         	= $this->input->post("idreservasi");
        $jpelayanan         	= $this->input->post("jpelayanan");
      
        $this->db->select("*, reservasi.norm as rnorm");
        $this->db->from("reservasi");
        $this->db->join("dokter", 
					"dokter.iddokter = reservasi.iddokter", "left"
		);
        $this->db->join("pasien", 
					"pasien.norm = reservasi.norm", "left"
		);
        $this->db->join("jkelamin", 
					"jkelamin.idjnskelamin = pasien.idjnskelamin", "left"
		);
        $this->db->join("registrasidet", 
					"registrasidet.idregdet = reservasi.idregdet", "left"
		);
        $this->db->join("stregistrasi", 
					"stregistrasi.idstregistrasi = registrasidet.idstregistrasi", "left"
		);
        $this->db->join("registrasi", 
					"registrasi.noreg = registrasidet.noreg", "left"
		);
        $this->db->join("stpasien", 
					"stpasien.idstpasien = registrasi.idstpasien", "left"
		);
        $this->db->join("stposisipasien", 
					"stposisipasien.idstposisipasien = reservasi.idstposisipasien", "left"
		);
        $this->db->join("bagian", 
					"bagian.idbagian = reservasi.idbagian", "left"
		);
				
		if($streservasi)$this->db->where('reservasi.idstreservasi',$streservasi);
		if($idreservasi)$this->db->where('reservasi.idreservasi',$idreservasi);
		if($jpelayanan)$this->db->where('bagian.idjnspelayanan',$jpelayanan);
		
		$this->db->order_by('tglreservasi desc, CAST(noantrian AS UNSIGNED) asc');
		
		if($norm)$this->db->like('pasien.norm',$norm);
		if($nmpasien)$this->db->like('pasien.nmpasien',$nmpasien);
		if($tglreservasi)$this->db->where('reservasi.tglreservasi',$tglreservasi);
		if($upel)$this->db->where('registrasidet.idbagian',$upel);
		if($upel2)$this->db->where('reservasi.idbagian',$upel2);
		if($dokter)$this->db->where('reservasi.iddokter',$dokter);
		if($nmdokter)$this->db->like('dokter.nmdoktergelar',$nmdokter);
		if($email)$this->db->like('reservasi.email',$email);
		if($notelp){
			$this->db->like('reservasi.nohp',$notelp);
			$this->db->like('reservasi.notelp',$notelp);
		}
        
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(500,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow(){
        $tglreservasi           = $this->input->post("tglreservasi");
        $norm             		= $this->input->post("norm");
        $nmpasien             		= $this->input->post("nmpasien");
        $upel             		= $this->input->post("upel");
        $upel2             		= $this->input->post("upel2");
        $nmdokter                 = $this->input->post("nmdokter");
        $dokter                 = $this->input->post("dokter");
        $email                 = $this->input->post("email");
        $notelp                 = $this->input->post("notelp");
        $streservasi         	= $this->input->post("streservasi");
        $idreservasi         	= $this->input->post("idreservasi");
        $jpelayanan         	= $this->input->post("jpelayanan");
      
        $this->db->select("*, reservasi.norm as rnorm");
        $this->db->from("reservasi");
        $this->db->join("dokter", 
					"dokter.iddokter = reservasi.iddokter", "left"
		);
        $this->db->join("pasien", 
					"pasien.norm = reservasi.norm", "left"
		);
        $this->db->join("jkelamin", 
					"jkelamin.idjnskelamin = pasien.idjnskelamin", "left"
		);
        $this->db->join("registrasidet", 
					"registrasidet.idregdet = reservasi.idregdet", "left"
		);
        $this->db->join("stregistrasi", 
					"stregistrasi.idstregistrasi = registrasidet.idstregistrasi", "left"
		);
        $this->db->join("registrasi", 
					"registrasi.noreg = registrasidet.noreg", "left"
		);
        $this->db->join("stpasien", 
					"stpasien.idstpasien = registrasi.idstpasien", "left"
		);
        $this->db->join("stposisipasien", 
					"stposisipasien.idstposisipasien = reservasi.idstposisipasien", "left"
		);
        $this->db->join("bagian", 
					"bagian.idbagian = reservasi.idbagian", "left"
		);
		
		if($streservasi)$this->db->where('reservasi.idstreservasi',$streservasi);
		if($idreservasi)$this->db->where('reservasi.idreservasi',$idreservasi);
		if($jpelayanan)$this->db->where('bagian.idjnspelayanan',$jpelayanan);
		
		$this->db->order_by('tglreservasi desc, noantrian asc');
		
		if($norm)$this->db->like('pasien.norm',$norm);
		if($nmpasien)$this->db->like('pasien.nmpasien',$nmpasien);
		if($tglreservasi)$this->db->where('reservasi.tglreservasi',$tglreservasi);
		if($upel)$this->db->where('registrasidet.idbagian',$upel);
		if($upel2)$this->db->where('reservasi.idbagian',$upel2);
		if($dokter)$this->db->where('reservasi.iddokter',$dokter);
		if($nmdokter)$this->db->like('dokter.nmdoktergelar',$nmdokter);
		if($email)$this->db->like('reservasi.email',$email);
		if($notelp){
			$this->db->like('reservasi.nohp',$notelp);
			$this->db->like('reservasi.notelp',$notelp);
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function insorupd_reservasi(){
	 /*  //registrasi
		$query = $this->db->getwhere('registrasi',array('noreg'=>$_POST['tf_noreg']));
		if($query->num_rows() > 0) $reg = $this->update_registrasi();
		else $reg = $this->insert_registrasi();
		
	  //registrasi detail
		$regdet = $this->insert_registrasidet($reg);
		
	  //reservasi
		$res = $this->insert_reservasi($reg, $regdet);
		
		if($reg && $regdet && $res)
		{
			$ret["success"] = true;
            $ret["noreg"]=$regdet['noreg'];
		}else{
			$ret["success"] = false;
		}
		
		echo json_encode($ret); */
    }
		
	function insert_reservasi(){
		$dataArray = $this->getFieldsAndValuesRes($reg, $regdet);
		$this->rhlib->insertRecord('reservasi',$dataArray);
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret=$dataArray;
        }else{
            $ret["success"]=false;
        }
        return $ret;
    }

	function update_reservasi(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('noreg', $dataArray['noreg']);
		$this->db->update('registrasi', $dataArray);

        if ($this->db->trans_status() === FALSE)
        {
            $ret["success"]=false;
        }
        else
        {
            $ret["success"]=true;
            $ret=$dataArray;
        }
        return $ret;
    }

	function getFieldsAndValues(){
		$this->db->select('max(idregdet) as max');
		$this->db->from('registrasidet');
		$q = $this->db->get();
		$dataregdet = $q->row_array();
		$dataArray = array(
             'tglreservasi'=> date_format(date_create($regdet['tglreg']), 'Y-m-d'),
             'jamreservasi'=> date_format(date_create($regdet['jamreg']), 'H:i:s'),
             'idshift'=> $regdet['idshift'],
             'norm'=> $reg['norm'],
             'nmpasien'=> $_POST['tf_nmpasien'],
             'alamat'=> $_POST['alamat'],
             'notelp'=> $_POST['notelp'],
             'nohp'=> $_POST['nohp'],
             'iddokter'=> $regdet['iddokter'],
             'idbagian'=> $regdet['idbagian'],
             'idstreservasi'=> 1,
             'idregdet'=> $dataregdet['max'],
             //'noantrian'=> $this->idKamar('nmkamar',$_POST['tf_nmkamar']),
             'userinput'=> $this->session->userdata['username'],
             'tglinput'=> date('Y-m-d')
        );		
		return $dataArray;
	}
	
	function getNoantrian(){
        $nmbagian                  = $this->input->post("nmbagian");
        $nmdokter                  = $this->input->post("nmdokter");
		$idbagian = $this->searchId('idbagian','bagian','nmbagian',$nmbagian);
		$iddokter = $this->searchId('iddokter','dokter','nmdoktergelar',$nmdokter);
		
        $this->db->select("count(idreservasi) AS max_np");
        $this->db->from("reservasi");
        $this->db->where('tglreservasi', date('Y-m-d'));
		if($idbagian)$this->db->where('idbagian', $idbagian);
		if($iddokter)$this->db->where('iddokter', $iddokter);
        
		$q = $this->db->get();
		$data = $q->row_array();
        $ret["antrian"]=$data['max_np'];
		
		echo json_encode($ret);
	}
	
	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function updateStposisipasien(){
        $idreservasi		= $this->input->post("idreservasi");
        $idstposisipasien	= $this->input->post("idstposisipasien");
	
		$this->db->where('idreservasi', $idreservasi);
		$data = array(
             'idstposisipasien'=> $idstposisipasien
        );
		$z = $this->db->update('reservasi', $data);
		if($z){
			$ret = true;
		} else {
			$ret = false;
		}
		
		return json_encode($ret);
	}
	
	function getRegres(){
        $idreservasi		= $this->input->post("idreservasi");
		
        $this->db->select("*");
        $this->db->from("reservasi");
        $this->db->join("bagian", 
					"bagian.idbagian = reservasi.idbagian", "left"
		);
        $this->db->join("dokter", 
					"dokter.iddokter = reservasi.iddokter", "left"
		);
        $this->db->where('idreservasi', $idreservasi);
		$q = $this->db->get();
		$data = $q->row_array();
        //$ret["antrian"]=$data['idbagian'];
		
		echo json_encode($data);
	}
	
	//=====================================BARU=====================================================
	
	function get_date_server() {
		$data['date'] = date('d-m-Y');
		echo json_encode($data);
    }
	
	function autoNumber($column,$tbl){
        $q = "SELECT max(".$column.")+1 as max FROM ".$tbl."" ;
        $query  = $this->db->query($q);
        $max = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $max=$row->max;
        }
        if ($max == null){
            $max=0;
        }
        return $max;
    } 
	
	function get_by_like($column,$tbl,$whereb, $wherea){
        $q = "SELECT ".$column." as id FROM ".$tbl." where ".$whereb." LIKE '%".$wherea."%' LIMIT 1" ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->id;
        }
        return $id;
    }
	
	function get_bagian_rj() {
		$q = $this->db->query("SELECT * FROM bagian WHERE idjnspelayanan=1");
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_dokter_bagian_rj() {
		$idbagian = $this->input->post('idbagian');
		$q = $this->db->query("SELECT dokterbagian.iddokter
				 , dokterbagian.idbagian
				 , dokter.kddokter
				 , dokter.nmdokter
				 , dokter.nmdoktergelar
			FROM
			  dokterbagian
			LEFT JOIN dokter
			ON dokter.iddokter = dokterbagian.iddokter
			  WHERE idbagian='$idbagian'");
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_shift($time){
        $q = "SELECT idshift
			FROM
			  shift
			WHERE
			  cast('$time' AS TIME) 
			  BETWEEN darijam AND sampaijam LIMIT 1" ;
        $query  = $this->db->query($q);
        $id = ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $id=$row->idshift;
        }
        return $id;
    }
	
	function get_noantrian($idbagian,$iddokter,$tgl){
        $q = "SELECT count(idreservasi) AS countrv
				FROM
				  reservasi
				WHERE
				  tglreservasi = '".$tgl."'
				  AND idbagian = '$idbagian'
				  AND iddokter = '$iddokter'" ;
        $query  = $this->db->query($q);

		$row = $query->row();
        $countrv = ($row->countrv == 0) ? 1:$row->countrv + 1;

        return $countrv;
    }
	
	function simpan_data(){
		
		if($_POST['tf_idres'] != ''){
			$dataArray = array(
				 'tglreservasi'=> date('Y-m-d', strtotime($_POST['df_tglrev'])),
				 'jamreservasi'=> date('H:i:s'),
				 'idshift'=> $this->get_shift(date('H:i:s')),
				 'norm'=> $this->get_by_like('norm','pasien','norm', $_POST['tf_norm']),
				 'nmpasien'=> $_POST['tf_nmpasien'],
				 'nohp'=> $_POST['tf_nohp'],
				 'idstreservasi'=> 1,
				 'userinput'=> $this->session->userdata['username'],
				 'tglinput'=> date('Y-m-d'),
				 'idstposisipasien'=> 1
			);
			$this->db->where('idreservasi', $_POST['tf_idres']);
			$this->db->update('reservasi',$dataArray);
		}else{
			$dataArray = array(
				 'idreservasi'=>$this->autoNumber('idreservasi','reservasi'),
				 'tglreservasi'=> date('Y-m-d', strtotime($_POST['df_tglrev'])),
				 'jamreservasi'=> date('H:i:s'),
				 'idshift'=> $this->get_shift(date('H:i:s')),
				 'idbagian'=> $_POST['h_bagian'],
				 'iddokter'=> $_POST['h_dokter'],
				 'noantrian'=> $this->get_noantrian($_POST['h_bagian'],$_POST['h_dokter'],date('Y-m-d', strtotime($_POST['df_tglrev']))),
				 'norm'=> $this->get_by_like('norm','pasien','norm', $_POST['tf_norm']),
				 'nmpasien'=> $_POST['tf_nmpasien'],
				 'nohp'=> $_POST['tf_nohp'],
				 'idstreservasi'=> 1,
				 'userinput'=> $this->session->userdata['username'],
				 'tglinput'=> date('Y-m-d'),
				 'idstposisipasien'=> 1
			);
			$this->rhlib->insertRecord('reservasi',$dataArray);
		}
		
        if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret=$dataArray;
        }else{
            $ret["success"]=false;
        }
        return $ret;
    }
	
	function batal_reservasi(){ 				
		$this->db->where('idreservasi', $_POST['idreservasi']);
		$this->db->set('idstreservasi',2); 
		$this->db->update('reservasi'); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
	
	function get_data_reservasi(){
		
		$tglreservasi = ($this->input->post("tglreservasi")) ? $this->input->post("tglreservasi"):date("Y-m-d");
        $idbagian = $this->input->post("idbagian");
		$iddokter = $this->input->post("iddokter");
		
		$optbagian = ($idbagian) ? "=":"<>";
		$optdokter = ($iddokter) ? "=":"<>";
		
        $start = $this->input->post("start");
		$limit = $this->input->post("limit");
		
		if ($start==null){
            $start = 0;
			$limit = 25;
		}
		
		$fields = $this->input->post("fields");
		$query = $this->input->post("query");
		
		$qlike = ""; // add query like
		if($fields!="" || $query !=""){
			$k=array('[',']','"');
			$r=str_replace($k, '', $fields);
			$b=explode(',', $r);
			$c=count($b);
			for($i=0;$i<$c;$i++){
				if ($i==0) {
					$qlike .= " WHERE ".$b[$i]." LIKE '%".$query."%'";
				} else {
					$qlike .= " OR ".$b[$i]." LIKE '%".$query."%'";
				}
			}
		}
			
		$q = "SELECT * FROM (SELECT r.idreservasi
				 , DATE_FORMAT(r.tglreservasi , '%d-%m-%Y') AS tglreservasi
				 , DATE_FORMAT(r.tglinput , '%d-%m-%Y') AS tglinput
				 , r.idbagian
				 , b.nmbagian
				 , r.iddokter
				 , d.nmdoktergelar
				 , r.noantrian
				 , trim(LEADING '0' FROM r.norm ) AS norm
				 , r.nmpasien
				 , r.nohp
				 , rd.noreg
				 , r.idstreservasi
				 , s.nmstreservasi
				 , ifnull(r.stdatang,0) AS stdatang
				 , if(idstposisipasien = '4', 'Sudah di periksa', ' ') as stpasien
			FROM
			  reservasi r
			LEFT JOIN bagian b
			ON b.idbagian = r.idbagian
			LEFT JOIN dokter d
			ON d.iddokter = r.iddokter
			LEFT JOIN registrasidet rd
			ON rd.idregdet = r.idregdet
			LEFT JOIN registrasi reg
			ON rd.noreg = reg.noreg
			LEFT JOIN streservasi s
			ON s.idstreservasi = r.idstreservasi
			  WHERE r.idstposisipasien NOT IN('5','6') -- AND r.tglreservasi = '".$tglreservasi."'
			  AND r.idbagian ".$optbagian." '".$idbagian."'
			  AND r.iddokter ".$optdokter." '".$iddokter."'
			  AND rd.userbatal IS NULL
			ORDER BY
			  r.noantrian DESC
			  ) A".$qlike."";

		$query = $this->db->query($q." LIMIT ".$start.", ".$limit);
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
			
		$queryall  = $this->db->query($q);
			
		$ttl = $queryall->num_rows();
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

		if($ttl>0){
			$build_array["data"]=$data;
		}
		
		echo json_encode($build_array);
    }
	
	function get_cdata_reservasi(){
		$tgl = $this->input->post("tgl");
		
		$q = "SELECT r.idreservasi
				 , r.tglreservasi AS tglreservasi
				 , r.tglinput AS tglinput
				 , r.idbagian
				 , b.nmbagian
				 , r.iddokter
				 , d.nmdoktergelar
				 , r.noantrian
				 , r.norm
				 , r.nmpasien
				 , r.nohp
				 , rd.noreg
				 , r.idstreservasi
				 , s.nmstreservasi
				 , ifnull(r.stdatang,0) AS stdatang
			FROM
			  reservasi r
			LEFT JOIN bagian b
			ON b.idbagian = r.idbagian
			LEFT JOIN dokter d
			ON d.iddokter = r.iddokter
			LEFT JOIN registrasidet rd
			ON rd.idregdet = r.idregdet
			LEFT JOIN streservasi s
			ON s.idstreservasi = r.idstreservasi
			WHERE r.tglreservasi = '".$tgl."' AND r.idstposisipasien = '1' AND r.idstreservasi = '1'
			  AND rd.userbatal IS NULL
			ORDER BY
			  r.noantrian DESC
			  ";

		$query = $this->db->query($q);
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
			
		$ttl = count($data);
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

		if($ttl>0){
			$build_array["data"]=$data;
		}
		
		echo json_encode($build_array);
    }
	
	function eksport_reservasi($tgl,$idbagian,$iddokter){	
		$optbagian = ($idbagian!="null") ? "=":"<>";
		$optdokter = ($iddokter!="null") ? "=":"<>";
		
		$nmbagian = $this->searchId("nmbagian", "bagian", "idbagian", $idbagian);
		$nmdokter = $this->searchId("nmdoktergelar", "dokter", "iddokter", $iddokter);
		
		$head['tgl'] = 'Tanggal: '.date("d-m-Y", strtotime($tgl));
		if ($nmbagian) {
			$head['nmbagian'] = 'Unit Pelayanan: '.$nmbagian;
		}
		if ($nmdokter) {
			$head['nmdokter'] = 'Dokter: '.$nmdokter;
		}
		$data['filter'] = $head;
		
		$data['fieldname'] = array('No. Antrian'
									,'Tanggal Reservasi'
									,'Unit Pelayanan'
									,'Dokter'
									,'No. RM'
									,'Nama Pasien'
									,'No. Handphone'
									,'No. Registrasi'
									,'Status'
									,'Datang');

		$data['data'] = $this->db->query("SELECT r.noantrian
				 , DATE_FORMAT(r.tglreservasi , '%d-%m-%Y') AS tglreservasi
				 , b.nmbagian
				 , d.nmdoktergelar
				 , r.norm
				 , r.nmpasien
				 , r.nohp
				 , rd.noreg
				 , s.nmstreservasi
				 , if(r.stdatang=1,'Ya','Tidak') AS stdatang
			FROM
			  reservasi r
			LEFT JOIN bagian b
			ON b.idbagian = r.idbagian
			LEFT JOIN dokter d
			ON d.iddokter = r.iddokter
			LEFT JOIN registrasidet rd
			ON rd.idregdet = r.idregdet
			LEFT JOIN streservasi s
			ON s.idstreservasi = r.idstreservasi
			WHERE r.tglreservasi = '".$tgl."'
			  AND r.idbagian ".$optbagian." '".$idbagian."'
			  AND r.iddokter ".$optdokter." '".$iddokter."'
			  AND rd.userbatal IS NULL
			ORDER BY
			  r.noantrian DESC")->result();
	
		$data['filename'] = 'reservasi-'.date("d-m-Y", strtotime($tgl));
		$this->load->view('exportexcelfilterarray', $data); 	
			
	}
}
