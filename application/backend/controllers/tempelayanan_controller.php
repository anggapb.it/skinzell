<?php

class Tempelayanan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_tempelayanan(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                 	= $this->input->post("query");
		$idtemplayanan           = $this->input->post("idtemplayanan");
				
		$this->db->select("*");
		$this->db->from("v_tempelayanandetail");
		$this->db->where("idstatus", 1);
		
		$where = array();
        $where['idtemplayanan']=$idtemplayanan;
        
        $this->db->where($where);
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_tempelayananri(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                 	= $this->input->post("query");
		$idtemplayanan           = $this->input->post("idtemplayanan");
		$idklstarif           = $this->input->post("idklstarif");
				
		$this->db->select("*");
		$this->db->from("v_tempelayanandet");
		$this->db->where("idstatus", 1);
		
		$where = array();
        $where['idtemplayanan']=$idtemplayanan;
        $where['klstarif']=$idklstarif;
        
        $this->db->where($where);
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function delete_tempelayanan(){     
		$where['iditempelayanantemp'] = $_POST['iditempelayanantemp'];
		$del = $this->rhlib->deleteRecord('templatepelayanandet',$where);
        return $del;
    }
	
	function delete_win_pelayanan(){     
		$where['idtemplayanan'] = $_POST['idtemplayanan'];
		$where['kdpelayanan'] = $_POST['kdpelayanan'];
		$del = $this->rhlib->deleteRecord('templatepelayanandet',$where);
        return $del;
    }
		
	function insert_win_pelayanan(){
		$dataArray = $this->getFieldsAndValuesPelayanan();
		$ret = $this->rhlib->insertRecord('templatepelayanandet',$dataArray);
		return $ret;
    }
	
	function getFieldsAndValuesPelayanan(){				
		$dataArray = array(
		     'iditempelayanantemp'		=> $_POST['iditempelayanantemp'],
		     'kdpelayanan'				=> $_POST['kdpelayanan'],
			 'idtemplayanan'			=> $_POST['idtemplayanan']
        );
		
		return $dataArray;
	}
	
	function cekkdpelayanan(){
        $q = "SELECT count(kdpelayanan) as kdpelayanan FROM templatepelayanandet where kdpelayanan='".$_POST['kdpelayanan']."' AND idtemplayanan='".$_POST['idtemplayanan']."'";
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->kdpelayanan;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
	
	function get_parent_mastertempelayanan(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
		
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_mtempelayanan");
        $this->db->where("v_mtempelayanan.idjnshirarki = 1");
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
	
		$this->db->select("*");
		$this->db->from("v_mtempelayanan");
        $this->db->where("v_mtempelayanan.idjnshirarki = 1");
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }	
}
