<?php 
class Laporan_rmpenyakitterbanyakrj_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_laprmpenyakitterbanyakrj(){

		$tglawal 	= $this->input->post("tglawal");
        $tglakhir 	= $this->input->post("tglakhir");
        $vdkot 		= $this->input->post("vdkot");
        $vdkott		= $this->input->post("vdkott");
        $vpenj 		= $this->input->post("vpenj");
        $idpenjamin	= $this->input->post("idpenjamin");
		
		$q = "SELECT kodifikasi.idkodifikasi
					 , kodifikasidet.idpenyakit
					 , penyakit.dtd
					 , penyakit.kdpenyakit
					 , penyakit.nmpenyakit
					 , penyakit.nmpenyakit as nmpenyakiteng
					 , kodifikasi.noreg
					 , kuitansi.tglkuitansi
					 , pasien.norm
					 , pasien.nmpasien
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , registrasi.idpenjamin
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if((rd.umurtahun = '0' 
						  AND rd.umurbulan = '0'), rd.umurhari <= 6, '0')
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd6haril
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if((rd.umurtahun = '0'
						  AND rd.umurbulan = '0'), rd.umurhari <= 6, '0')
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd6harip

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if((rd.umurtahun = '0'
						  AND rd.umurbulan = '0'), rd.umurhari > 6, '0')
						  AND if((rd.umurtahun = '0'
						  AND rd.umurbulan = '0'), rd.umurhari <= 28, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd28haril
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if((rd.umurtahun = '0'
						  AND rd.umurbulan = '0'), rd.umurhari > 6, '0')
						  AND if((rd.umurtahun = '0'
						  AND rd.umurbulan = '0'), rd.umurhari <= 28, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd28harip

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if((rd.umurtahun = '0'), if(rd.umurbulan = '0', rd.umurhari > 28, rd.umurbulan >= 1), rd.umurtahun = 1)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd1thnl
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if((rd.umurtahun = '0'), if(rd.umurbulan = '0', rd.umurhari > 28, rd.umurbulan >= 1), rd.umurtahun = 1)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd1thnp

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 4, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd4thnl
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 4, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd4thnp

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if(rd.umurtahun > 1, rd.umurtahun > 4, 0)
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 14, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd14thnl
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if(rd.umurtahun > 1, rd.umurtahun > 4, 0)
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 14, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd14thnp

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if(rd.umurtahun > 1, rd.umurtahun > 14, 0)
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 24, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd24thnl
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if(rd.umurtahun > 1, rd.umurtahun > 14, 0)
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 24, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd24thnp

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if(rd.umurtahun > 1, rd.umurtahun > 24, 0)
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 44, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd44thnl
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if(rd.umurtahun > 1, rd.umurtahun > 24, 0)
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 44, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd44thnp

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if(rd.umurtahun > 1, rd.umurtahun > 44, 0)
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 64, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd64thnl
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if(rd.umurtahun > 1, rd.umurtahun > 44, 0)
						  AND if(rd.umurtahun > 1, rd.umurtahun <= 64, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS ukd64thnp

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 1
						  AND if(rd.umurtahun > 1, rd.umurtahun > 64, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS uld64thnl
					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND p.idjnskelamin = 2
						  AND if(rd.umurtahun > 1, rd.umurtahun > 64, 0)
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS uld64thnp					   
					
					 , if((SELECT count(kdt.idpenyakit)
						   FROM
							 kodifikasidet kdt
						   LEFT JOIN kodifikasi kd
						   ON kd.idkodifikasi = kdt.idkodifikasi
						   LEFT JOIN registrasi r
						   ON r.noreg = kd.noreg
						   LEFT JOIN registrasidet rd
						   ON rd.noreg = r.noreg
						   LEFT JOIN nota n
						   ON n.idregdet = rd.idregdet
						   LEFT JOIN kuitansi k
						   ON k.nokuitansi = n.nokuitansi
						   LEFT JOIN pasien p
						   ON p.norm = r.norm
						   LEFT JOIN daerah dkell
						   ON dkell.iddaerah = p.iddaerah
						   LEFT JOIN daerah dkecc
						   ON dkecc.iddaerah = dkell.dae_iddaerah
						   LEFT JOIN daerah dkott
						   ON dkott.iddaerah = dkecc.dae_iddaerah
						   WHERE
							 r.idjnspelayanan IN(1,3)
							 AND kdt.idpenyakit = kodifikasidet.idpenyakit
							 AND kdt.idkodifikasi IS NOT NULL
							 AND p.idjnskelamin = 1) > 1, '0', if((SELECT count(kdt.idpenyakit)
																   FROM
																	 kodifikasidet kdt
																   LEFT JOIN kodifikasi kd
																   ON kd.idkodifikasi = kdt.idkodifikasi
																   LEFT JOIN registrasi r
																   ON r.noreg = kd.noreg
																   LEFT JOIN registrasidet rd
																   ON rd.noreg = r.noreg
																   LEFT JOIN nota n
																   ON n.idregdet = rd.idregdet
																   LEFT JOIN kuitansi k
																   ON k.nokuitansi = n.nokuitansi
																   LEFT JOIN pasien p
																   ON p.norm = r.norm
																   LEFT JOIN daerah dkell
																   ON dkell.iddaerah = p.iddaerah
																   LEFT JOIN daerah dkecc
																   ON dkecc.iddaerah = dkell.dae_iddaerah
																   LEFT JOIN daerah dkott
																   ON dkott.iddaerah = dkecc.dae_iddaerah
																   WHERE
																	 r.idjnspelayanan IN(1,3)
																	 AND kdt.idpenyakit = kodifikasidet.idpenyakit
																	 AND kdt.idkodifikasi IS NOT NULL
																	 AND p.idjnskelamin = 1) < 1, '0', '1')
					   ) AS kbl
					 , if((SELECT count(kdt.idpenyakit)
						   FROM
							 kodifikasidet kdt
						   LEFT JOIN kodifikasi kd
						   ON kd.idkodifikasi = kdt.idkodifikasi
						   LEFT JOIN registrasi r
						   ON r.noreg = kd.noreg
						   LEFT JOIN registrasidet rd
						   ON rd.noreg = r.noreg
						   LEFT JOIN nota n
						   ON n.idregdet = rd.idregdet
						   LEFT JOIN kuitansi k
						   ON k.nokuitansi = n.nokuitansi
						   LEFT JOIN pasien p
						   ON p.norm = r.norm
						   LEFT JOIN daerah dkell
						   ON dkell.iddaerah = p.iddaerah
						   LEFT JOIN daerah dkecc
						   ON dkecc.iddaerah = dkell.dae_iddaerah
						   LEFT JOIN daerah dkott
						   ON dkott.iddaerah = dkecc.dae_iddaerah
						   WHERE
							 r.idjnspelayanan IN(1,3)
							 AND kdt.idpenyakit = kodifikasidet.idpenyakit
							 AND kdt.idkodifikasi IS NOT NULL
							 AND p.idjnskelamin = 2) > 1, '0', if((SELECT count(kdt.idpenyakit)
																   FROM
																	 kodifikasidet kdt
																   LEFT JOIN kodifikasi kd
																   ON kd.idkodifikasi = kdt.idkodifikasi
																   LEFT JOIN registrasi r
																   ON r.noreg = kd.noreg
																   LEFT JOIN registrasidet rd
																   ON rd.noreg = r.noreg
																   LEFT JOIN nota n
																   ON n.idregdet = rd.idregdet
																   LEFT JOIN kuitansi k
																   ON k.nokuitansi = n.nokuitansi
																   LEFT JOIN pasien p
																   ON p.norm = r.norm
																   LEFT JOIN daerah dkell
																   ON dkell.iddaerah = p.iddaerah
																   LEFT JOIN daerah dkecc
																   ON dkecc.iddaerah = dkell.dae_iddaerah
																   LEFT JOIN daerah dkott
																   ON dkott.iddaerah = dkecc.dae_iddaerah
																   WHERE
																	 r.idjnspelayanan IN(1,3)
																	 AND kdt.idpenyakit = kodifikasidet.idpenyakit
																	 AND kdt.idkodifikasi IS NOT NULL
																	 AND p.idjnskelamin = 2) < 1, '0', '1')
					   ) AS kbp

					 , (if((SELECT count(kdt.idpenyakit)
							FROM
							  kodifikasidet kdt
							LEFT JOIN kodifikasi kd
							ON kd.idkodifikasi = kdt.idkodifikasi
							LEFT JOIN registrasi r
							ON r.noreg = kd.noreg
							LEFT JOIN registrasidet rd
							ON rd.noreg = r.noreg
							LEFT JOIN nota n
							ON n.idregdet = rd.idregdet
							LEFT JOIN kuitansi k
							ON k.nokuitansi = n.nokuitansi
							LEFT JOIN pasien p
							ON p.norm = r.norm
							LEFT JOIN daerah dkell
							ON dkell.iddaerah = p.iddaerah
							LEFT JOIN daerah dkecc
							ON dkecc.iddaerah = dkell.dae_iddaerah
							LEFT JOIN daerah dkott
							ON dkott.iddaerah = dkecc.dae_iddaerah
							WHERE
							  r.idjnspelayanan IN(1,3)
							  AND kdt.idpenyakit = kodifikasidet.idpenyakit
							  AND kdt.idkodifikasi IS NOT NULL
							  AND p.idjnskelamin = 1) > 1, '0', if((SELECT count(kdt.idpenyakit)
																	FROM
																	  kodifikasidet kdt
																	LEFT JOIN kodifikasi kd
																	ON kd.idkodifikasi = kdt.idkodifikasi
																	LEFT JOIN registrasi r
																	ON r.noreg = kd.noreg
																	LEFT JOIN registrasidet rd
																	ON rd.noreg = r.noreg
																	LEFT JOIN nota n
																	ON n.idregdet = rd.idregdet
																	LEFT JOIN kuitansi k
																	ON k.nokuitansi = n.nokuitansi
																	LEFT JOIN pasien p
																	ON p.norm = r.norm
																	LEFT JOIN daerah dkell
																	ON dkell.iddaerah = p.iddaerah
																	LEFT JOIN daerah dkecc
																	ON dkecc.iddaerah = dkell.dae_iddaerah
																	LEFT JOIN daerah dkott
																	ON dkott.iddaerah = dkecc.dae_iddaerah
																	WHERE
																	  r.idjnspelayanan IN(1,3)
																	  AND kdt.idpenyakit = kodifikasidet.idpenyakit
																	  AND kdt.idkodifikasi IS NOT NULL
																	  AND p.idjnskelamin = 1) < 1, '0', '1')
					   )) + (if((SELECT count(kdt.idpenyakit)
								 FROM
								   kodifikasidet kdt
								 LEFT JOIN kodifikasi kd
								 ON kd.idkodifikasi = kdt.idkodifikasi
								 LEFT JOIN registrasi r
								 ON r.noreg = kd.noreg
								 LEFT JOIN registrasidet rd
								 ON rd.noreg = r.noreg
								 LEFT JOIN nota n
								 ON n.idregdet = rd.idregdet
								 LEFT JOIN kuitansi k
								 ON k.nokuitansi = n.nokuitansi
								 LEFT JOIN pasien p
								 ON p.norm = r.norm
								 LEFT JOIN daerah dkell
								 ON dkell.iddaerah = p.iddaerah
								 LEFT JOIN daerah dkecc
								 ON dkecc.iddaerah = dkell.dae_iddaerah
								 LEFT JOIN daerah dkott
								 ON dkott.iddaerah = dkecc.dae_iddaerah
								 WHERE
								   r.idjnspelayanan IN(1,3)
								   AND kdt.idpenyakit = kodifikasidet.idpenyakit
								   AND kdt.idkodifikasi IS NOT NULL
								   AND p.idjnskelamin = 2) > 1, '0', if((SELECT count(kdt.idpenyakit)
																		 FROM
																		   kodifikasidet kdt
																		 LEFT JOIN kodifikasi kd
																		 ON kd.idkodifikasi = kdt.idkodifikasi
																		 LEFT JOIN registrasi r
																		 ON r.noreg = kd.noreg
																		 LEFT JOIN registrasidet rd
																		 ON rd.noreg = r.noreg
																		 LEFT JOIN nota n
																		 ON n.idregdet = rd.idregdet
																		 LEFT JOIN kuitansi k
																		 ON k.nokuitansi = n.nokuitansi
																		 LEFT JOIN pasien p
																		 ON p.norm = r.norm
																		 LEFT JOIN daerah dkell
																		 ON dkell.iddaerah = p.iddaerah
																		 LEFT JOIN daerah dkecc
																		 ON dkecc.iddaerah = dkell.dae_iddaerah
																		 LEFT JOIN daerah dkott
																		 ON dkott.iddaerah = dkecc.dae_iddaerah
																		 WHERE
																		   r.idjnspelayanan IN(1,3)
																		   AND kdt.idpenyakit = kodifikasidet.idpenyakit
																		   AND kdt.idkodifikasi IS NOT NULL
																		   AND p.idjnskelamin = 2) < 1, '0', '1')
					   )) as jmlkb

					 , (SELECT count(kdt.idpenyakit)
						FROM
						  kodifikasidet kdt
						LEFT JOIN kodifikasi kd
						ON kd.idkodifikasi = kdt.idkodifikasi
						LEFT JOIN registrasi r
						ON r.noreg = kd.noreg
						LEFT JOIN registrasidet rd
						ON rd.noreg = r.noreg
						LEFT JOIN nota n
						ON n.idregdet = rd.idregdet
						LEFT JOIN kuitansi k
						ON k.nokuitansi = n.nokuitansi
						LEFT JOIN pasien p
						ON p.norm = r.norm
						LEFT JOIN daerah dkell
						ON dkell.iddaerah = p.iddaerah
						LEFT JOIN daerah dkecc
						ON dkecc.iddaerah = dkell.dae_iddaerah
						LEFT JOIN daerah dkott
						ON dkott.iddaerah = dkecc.dae_iddaerah
						WHERE
						  r.idjnspelayanan IN(1,3)
						  AND kdt.idkodifikasi IS NOT NULL
						  AND kdt.idpenyakit = kodifikasidet.idpenyakit
						  AND if(1 = '".$vdkot."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$vdkott."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

						  AND if(1 = '".$vpenj."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
						  if('' = '".$idpenjamin."', k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
						  AND k.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
					   ) AS jmlkunjungan

				FROM
				  kodifikasidet
				LEFT JOIN kodifikasi
				ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
				LEFT JOIN penyakit
				ON penyakit.idpenyakit = kodifikasidet.idpenyakit
				LEFT JOIN registrasi
				ON registrasi.noreg = kodifikasi.noreg
				LEFT JOIN registrasidet
				ON registrasidet.noreg = registrasi.noreg
				LEFT JOIN nota
				ON nota.idregdet = registrasidet.idregdet
				LEFT JOIN kuitansi
				ON kuitansi.nokuitansi = nota.nokuitansi
				LEFT JOIN pasien
				ON pasien.norm = registrasi.norm
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = pasien.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				WHERE
				  registrasi.idjnspelayanan IN(1,3)
				  AND kodifikasidet.idkodifikasi IS NOT NULL
				  AND if(1 = '".$vdkot."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
				  if('26005' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
				  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
				  if('30457' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
				  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
				  if('30754' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
				  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
				  if('' = '".$vdkott."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkot.iddaerah NOT IN (26005, 30457, 30754)
				  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

				  AND if(1 = '".$vpenj."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
				  if('' = '".$idpenjamin."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', registrasi.idpenjamin = '".$idpenjamin."'
				  AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
				GROUP BY
				  kodifikasidet.idpenyakit";
		$query = $this->db->query($q);
		
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
					
		$ttl = count($data);
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

		if($ttl>0){
			$build_array["data"]=$data;
		}
		
		echo json_encode($build_array);
	}
	
	function get_lrmpenjamin(){      
        $this->db->select('*');			
        $this->db->from('penjamin');			
		$this->db->order_by('kdpenjamin');
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	

}
