<?php

class Pengeluarankas_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_pengeluarankas(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$tgl1                   = $this->input->post("tglawal");
		$tgl2                   = $this->input->post("tglakhir");
		$key					= $_POST["key"]; 
      
        $this->db->select("*");
        $this->db->from("v_jurnal");
		$this->db->order_by('kdjurnal DESC');
        	   
	    if($this->input->post('chb_periode')=='true'){
			$this->db->where("date(tgltransaksi) between '". $tgl1 ."' and '". $tgl2."'" );	   
	    }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		}  
        //======================================================================        
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($key, $tgl1, $tgl2);
        
        //======================================================================
         $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($key, $tgl1, $tgl2){
      
        $this->db->select("*");
        $this->db->from("v_jurnal");		
        	   
	    if($this->input->post('chb_periode')=='true'){
			$this->db->where("date(tgltransaksi) between '". $tgl1 ."' and '". $tgl2."'" );	   
	    }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}        
                
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_pengeluarankasdet(){
	
		$kdjurnal = $_POST["kdjurnal"];
      
        $this->db->select("*");
        $this->db->from("v_jurnaldet");
		$this->db->order_by("kdakun");
		
		$where = array();
        $where['kdjurnal']=$kdjurnal;
		
		$this->db->where($where);
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_jurnaldet');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_pengeluarankasdetail(){
	
		$kdjurnal = $_POST["kdjurnal"];
      
        $this->db->select("*");
        $this->db->from("v_jurnaldet");
		$this->db->order_by("kdakun");
		
		$where = array();
        $where['kdjurnal']=$kdjurnal;
		
		$this->db->where($where);
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $datax = $this->db->count_all('v_jurnaldet');
        $ttl = $datax;
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function delete_pengeluarankas(){     
		$where['kdjurnal'] = $_POST['kdjurnal'];
		$del = $this->rhlib->deleteRecord('jurnaldet',$where);
		$del = $this->rhlib->deleteRecord('jurnal',$where);
        return $del;
    }
	
	function insorupd_pengeluarankas(){
        $this->db->trans_begin();
		$kdjurnal = $this->input->post("kdjurnal");
		$query 	= $this->db->getwhere('jurnal',array('kdjurnal'=>$kdjurnal));
		$kdjurnal = $query->row_array();
				
		$jurnal = $this->insert_tbljurnal($kdjurnal);
		$kdjurnal = $jurnal['kdjurnal'];
				
		if($kdjurnal)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["kdjurnal"]=$kdjurnal;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_tbljurnal($kdjurnal){
		if(!$kdjurnal){
			$dataArray = $this->getFieldsAndValues($kdjurnal);
			$jurnal = $this->db->insert('jurnal',$dataArray);
		} else {
			$dataArray = $this->update_tbljurnal($kdjurnal);
		}
		
		$query = $this->db->getwhere('jurnaldet',array('kdjurnal'=>$dataArray['kdjurnal']));
		$jurnaldet = $query->row_array();
		if($query->num_rows() == 0){
			$jurnaldet = $this->insert_tbljurnaldet($dataArray);
		} else {
			$jurnaldet = $this->update_tbljurnaldet($dataArray);
		}
		
		if($jurnaldet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function insert_tbljurnaldet($jurnal){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrjurnal']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($jurnal, $vale[0], $vale[1], $vale[2]);
			$z =$this->db->insert('jurnaldet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function update_tbljurnal($kdjurnal){
		$dataArray = $this->getFieldsAndValues($kdjurnal);
		
		//UPDATE
		$this->db->where('kdjurnal', $dataArray['kdjurnal']);
		$jurnal = $this->db->update('jurnal', $dataArray);
		
		return $dataArray;
    }
	
	function update_tbljurnaldet($jurnal){
		$where['kdjurnal'] = $jurnal['kdjurnal'];
		$this->db->delete('jurnaldet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrjurnal']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($jurnal, $vale[0], $vale[1], $vale[2]);
			$z =$this->db->insert('jurnaldet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function getFieldsAndValues(){
			$kdjur   	= $this->input->post("kdjurnal");
			$kdjurnal = $this->getKdjurnal();
		
		$dataArray = array(
			 'kdjurnal'			=> ($kdjur) ? $kdjur : $kdjurnal,
			 'idbagian'			=> $_POST['idbagian'],
			 'idjnstransaksi'	=> $_POST['idjnstransaksi'],
             'tgltransaksi'		=> $_POST['tgltransaksi'],
             'tgljurnal'		=> $_POST['tgltransaksi'],
             'keterangan'		=> $_POST['keterangan'],
             'noreff'			=> $_POST['noreff'],
             'userid'			=> $_POST['userid'],
             //'tglinput'			=> $_POST['userid'],
			 'nokasir'			=> $_POST['nokasir'],	
			 'nominal'			=> $_POST['nominal']			
		);
		/* var_dump($dataArray);
		exit; */
		return $dataArray;
	}
	
	function getKdjurnal(){
		$q = "SELECT getOtoNojurnal(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}

	function getFieldsAndValuesDet($jurnal,$val1,$val2,$val3){
		$dataArray = array(
			'kdjurnal'	 => $jurnal['kdjurnal'],
			'idakun'	 => $val1,
			'debit' 	 => $val2,
			'kredit'   	 => $val3,			
		);
		return $dataArray;
	}
	
	function getNmbagiann(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }
	
	function get_bagtahunan(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
		
		$key					= $_POST['key'];
      
        $this->db->select("*");
        $this->db->from("v_bagantahunandijurnal");        
        
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		} 
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nuw($key);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nuw($key){
      
        $this->db->select("*");
        $this->db->from("v_bagantahunandijurnal");

		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->like($id, $name);
		} 
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function getCekstkasir(){
		$this->db->select("*");
        $this->db->from("v_kasir");
		$this->db->where("idstkasir != 2");
		$q = $this->db->get();
		$idstkasir = $q->row_array();
		echo json_encode($idstkasir);
	}
	
	function get_jnstransaksidijurnal(){
      
        $this->db->select('jtransaksi.*');
        $this->db->from('jtransaksi');
		$this->db->where("idjnskas = '2'");
		$this->db->order_by('idjnstransaksi');
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function searchsum(){
        
		$search = ($this->input->post("nmcombo")) ? "".$this->input->post("idcombo")." like'%".$this->input->post("nmcombo")."%'":"";
		$tgl = ($this->input->post("cbtgl")=='true') ? "date(tgltransaksi) between '". $this->input->post("tglawal") ."' and '". $this->input->post("tglakhir") ."'":"";
        $where = "";
		$and = "";
		
		if ($search && $tgl) {
			$where = " where ";
			$and = " and ";
		}
		
		if ($search && !$tgl) {
			$where = " where ";
			$and = "";
		}
		
		if (!$search && $tgl) {
			$where = " where ";
			$and = "";
		}
		
		$q = $this->db->query("select sum(nominal) as nom from v_jurnal".$where.$search.$and.$tgl);
        $data = array();
		$row['sum'] = "";
        if ($q->num_rows() > 0) {
            $row['sum'] = $q->row()->nom;
        }
		
      
		
        echo json_encode($row);
    }
	
	
	
}
