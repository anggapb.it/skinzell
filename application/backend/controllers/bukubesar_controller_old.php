<?php

class Bukubesar_controller extends Controller {
    public function __construct()
    {
        parent::Controller();
      $this->load->library('session');
      $this->load->library('rhlib');
    }
  
  function get_bukubesar(){
    $tglawal_transaksi     = $this->input->post("tglawal_transaksi");
    $tglakhir_transaksi    = $this->input->post("tglakhir_transaksi");
    $tglawal_input         = $this->input->post("tglawal_input");
    $tglakhir_input        = $this->input->post("tglakhir_input");
    
    $this->db->select('*');
    $this->db->from('v_lap_bukubesar');
    
    if(!empty($tglawal_transaksi) && !empty($tglakhir_transaksi)){
      if($tglawal_transaksi == $tglakhir_transaksi){
        $this->db->where('tgltransaksi', $tglawal_transaksi);
      }else{
        $this->db->where('tgltransaksi >=', $tglawal_transaksi);
        $this->db->where('tgltransaksi <=', $tglakhir_transaksi);
      }
    }
    
    if(!empty($tglawal_input) && !empty($tglakhir_input)){
      if($tglawal_input == $tglakhir_input){
        $this->db->where('tglinput', $tglawal_input);
      }else{
        $this->db->where('tglinput >=', $tglawal_input);
        $this->db->where('tglinput <=', $tglakhir_input);
      }
    }
    
    
    $q = $this->db->get();
    $data = array();
    if ($q->num_rows() > 0) {
        $data = $q->result_array();
    }

    $ttl = $this->db->count_all('v_lap_bukubesar');
    $build_array = array ("success"=>true,"results"=>$ttl,"data"=>$data);
    
    if($ttl>0){
        $build_array["data"]=$data;
    }
  
    echo json_encode($build_array);
     
  }
  
  
}
