<?php

class Dokumenpasien_Controller extends Controller {
    var $dokumenpath = './resources/img/camphoto/berkaspasien/';
    var $dokumenurl = '/resources/img/camphoto/berkaspasien/';
    var $digits = 3;
    public function __construct()
    {
        parent::Controller();
		$this->load->library('session');
    }
	
	function get_dokumen(){
        $norm      = $this->input->post("norm");
        $norm      = $norm;
        if(empty($norm)){
            $build_array = array ("success"=>true,"results"=>0,"data"=>array());
            echo json_encode($build_array);
            die;            
        }

        $medrekdir = $this->dokumenpath.$norm;
        $medrekurl = base_url().$this->dokumenurl.$norm;
        
        if (!file_exists($medrekdir)) {
            $build_array = array ("success"=>true,"results"=>0,"data"=>array());
            echo json_encode($build_array);
            die;
        }
        
        $files1 = scandir($medrekdir);
        $dokumens = array();
        if(!empty($files1)){
            foreach($files1 as $idx => $file){
                if($file == '.' || $file == '..') continue;
                if(is_dir($medrekdir.'/'.$file)) continue;

                $filemtime = Date ("Y-m-d H:i:s", filemtime($medrekdir.'/'.$file));
                $records = array(
                    'nmdokumen' => $file,
                    'pathdokumen' => $medrekdir.'/'.$file,
                    'urldokumen' => $medrekurl.'/'.$file,
                    'tglupload' => $filemtime,
                );

                $dokumens[] = $records;
            }
        }


        $build_array = array ("success"=>true,"results"=>count($dokumens),"data"=>$dokumens);
        echo json_encode($build_array);

    }
	
	
	function delete_dokumen(){
        $pathdokumen = $this->input->post('pathdokumen');
        if(empty($pathdokumen)){
            return false;
        }

        if(file_exists($pathdokumen)){
            unlink($pathdokumen);
            return true;
        }

        return false;
    }
	
	function upload_dokumen(){
        $this->load->library('upload');

        $norm     = $this->input->post('tf_norm');
        $norm     = $norm;
        $nmpasien = $this->input->post('tf_nmpasien');
        $nmpasien = str_replace(array(',','.',' ',';'), '_', $nmpasien);
        $nmpasien = strtolower($nmpasien);

        if(empty($norm)){
            $ret["success"]=false;
            $ret["msg"]= 'Upload Data Gagal';

            echo json_encode($ret);
            die;
        }

        $medrekdir = $this->dokumenpath.$norm;
        
        //check, jika folder tidak ada. create
        if (!file_exists($medrekdir)) {
            if (!mkdir($medrekdir, 0777, true)) {
                $ret["success"]=false;
                $ret["msg"]= 'Gagal membuat folder';
                echo json_encode($ret);
                die;
            }
        }

        //upload dokumen 1
        $config = array();
        if(isset($_FILES['upload_dokumen1']['name']) && !empty($_FILES['upload_dokumen1']['name'])){
            $config['upload_path'] = $medrekdir;
            $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $new_name = $norm.'_'.$nmpasien.'_'.time().'_'.str_pad(rand(0, pow(10, $this->digits)-1), $this->digits, '0', STR_PAD_LEFT);
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('upload_dokumen1')) {
                echo json_encode(array('success' => false, 'msg' => $this->upload->display_errors()));
                die;
            }

        }

        //upload dokumen 2
        $config = array();
        if(isset($_FILES['upload_dokumen2']['name']) && !empty($_FILES['upload_dokumen2']['name'])){
            $config['upload_path'] = $medrekdir;
            $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $new_name = $norm.'_'.$nmpasien.'_'.time().'_'.str_pad(rand(0, pow(10, $this->digits)-1), $this->digits, '0', STR_PAD_LEFT);
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('upload_dokumen2')) {
                echo json_encode(array('success' => false, 'msg' => $this->upload->display_errors()));
                die;
            }

        }

        //upload dokumen 3
        $config = array();
        if(isset($_FILES['upload_dokumen3']['name']) && !empty($_FILES['upload_dokumen3']['name'])){
            $config['upload_path'] = $medrekdir;
            $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $new_name = $norm.'_'.$nmpasien.'_'.time().'_'.str_pad(rand(0, pow(10, $this->digits)-1), $this->digits, '0', STR_PAD_LEFT);
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('upload_dokumen3')) {
                echo json_encode(array('success' => false, 'msg' => $this->upload->display_errors()));
                die;
            }

        }

        //upload dokumen 4
        $config = array();
        if(isset($_FILES['upload_dokumen4']['name']) && !empty($_FILES['upload_dokumen4']['name'])){
            $config['upload_path'] = $medrekdir;
            $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $new_name = $norm.'_'.$nmpasien.'_'.time().'_'.str_pad(rand(0, pow(10, $this->digits)-1), $this->digits, '0', STR_PAD_LEFT);
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('upload_dokumen4')) {
                echo json_encode(array('success' => false, 'msg' => $this->upload->display_errors()));
                die;
            }

        }

        //upload dokumen 5
        $config = array();
        if(isset($_FILES['upload_dokumen5']['name']) && !empty($_FILES['upload_dokumen5']['name'])){
            $config['upload_path'] = $medrekdir;
            $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $new_name = $norm.'_'.$nmpasien.'_'.time().'_'.str_pad(rand(0, pow(10, $this->digits)-1), $this->digits, '0', STR_PAD_LEFT);
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('upload_dokumen5')) {
                echo json_encode(array('success' => false, 'msg' => $this->upload->display_errors()));
                die;
            }

        }

        $ret["success"] = true;
        $ret["msg"]='Berhasil mengupload dokumen.';
        echo json_encode($ret);
    }
	
}
