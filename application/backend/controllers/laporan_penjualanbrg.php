 <?php 
class Laporan_penjualanbrg extends Controller{
	public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	
	function get_data(){
		$start 					= $this->input->post("start");
        $limit 					= $this->input->post("limit");
        
        $fields				    = $this->input->post("fields");
        $query					= $this->input->post("query");
		$tglawal				= $this->input->post("tglawal");
		$tglakhir				= $this->input->post("tglakhir");
		
		$q = "SELECT tglreg
     , nmpasien
     , nmitem
     , userbatal
     , qty
     , diskon_brg / pembagi_diskon_brg AS diskon_brg
     , penjualan - (diskon_brg / pembagi_diskon_brg) AS penjualan
     , diskon
     , modal
     , (
       CASE
       WHEN penjualan - diskon = 0 THEN
         0
       ELSE
         ((penjualan - (diskon_brg / pembagi_diskon_brg)) - diskon) - modal
       END) AS keuntungan
     , (
       CASE
       WHEN penjualan - diskon = 0 THEN
         0
       ELSE
         concat(round((((penjualan - (diskon_brg / pembagi_diskon_brg) - diskon) - modal) / penjualan) * 100, 2), ' %')
       END) AS persentase_margin


FROM
  (SELECT rd.tglreg
        , p.nmpasien
        , t.nmitem
        , rd.userbatal
        , nd.qty
        , (nd.qty * ifnull(nd.tarifjs + nd.tarifjp + nd.tarifjm + nd.tarifbhp, 0)) AS penjualan
        , ifnull(nd.diskonjs + nd.diskonjp + nd.diskonjm + nd.diskonbhp, 0) AS diskon
        , n.diskon AS diskon_brg
        , nd.qty * t.hrgbeli AS modal
        , (SELECT count(nonota)
           FROM
             notadet
           WHERE
             nonota = nd.nonota) AS pembagi_diskon_brg

   FROM
     notadet nd
   LEFT JOIN v_tarifall t
   ON t.kditem = nd.kditem
   LEFT JOIN nota n
   ON n.nonota = nd.nonota
   LEFT JOIN registrasidet rd
   ON rd.idregdet = n.idregdet
   LEFT JOIN registrasi r
   ON r.noreg = rd.noreg
   LEFT JOIN pasien p
   ON p.norm = r.norm
   WHERE
     n.nokuitansi IS NOT NULL) A where tglreg BETWEEN '".$tglawal."' AND '".$tglakhir."' AND userbatal IS NULL";
  		$query = $this->db->query($q);
        $data = $query->result();		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	

	
}