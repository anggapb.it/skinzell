<?php 
class Tagihan_penjamin_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('pdf');
    }
	
	function get_tagihan_penjamin(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
        $this->db->from("v_tagihanpenjamin");
		
		if ($this->input->post("cbx")=='true') {
			$this->db->where('tgltagihan BETWEEN ', "'". $this->input->post("tglawalan") ."' AND '". $this->input->post("tglakhiran") ."'", false);
		}
		if ($this->input->post("value")){
			$this->db->or_like($this->input->post("key"), $this->input->post("value"));
		}
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(25,0);
        } 
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function appkeu(){
		$sql =	$this->db->query("select * from setting where idklpsetting = '14'");
		$num = $sql->num_rows();
			if($num>0){
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);			
			}else{
				return 0;
			} 
	
	}
	
	function hapus_tagihan_penjamin(){     
		$where['notagihan'] = $this->input->post("notagihan");
		$delkw = $this->rhlib->deleteRecord('kuitansipenjamin',$where);
		$delri = $this->rhlib->deleteRecord('tagihanri',$where);
		$delrj = $this->rhlib->deleteRecord('tagihanrj',$where);
		if ($delkw && $delri && $delrj) {
			$del = $this->rhlib->deleteRecord('tagihan',$where);
		}
        return $del;
    }
	
	function get_transaksi_ugd(){
		$idpenjamin = $this->input->post("idpenjamin");
		$notagihan = $this->input->post("notagihan");

        $q = $this->db->query("SELECT n.nonota AS nonota
			 , r.noreg AS noreg
			 , rd.tglreg AS tglreg
			 , trim(LEADING '0' FROM p.norm) AS norm
			 , p.nmpasien AS nmpasien
			 , p.idjnskelamin AS idjnskelamin
			 , j.kdjnskelamin AS kdjnskelamin
			 , j.nmjnskelamin AS nmjnskelamin
			 , p.tgllahir AS tgllahir
			 , b.idbagian AS idbagian
			 , b.nmbagian AS nmbagian
			 , ifnull(sum(nd.tarifjs * nd.qty)
			   + sum(nd.tarifjm * nd.qty)
			   + sum(nd.tarifjp * nd.qty)
			   + sum(nd.tarifbhp * nd.qty)
			   + n.uangr, 0) - ifnull(n.diskon, 0) AS total
			 , (
			   SELECT ifnull(sum(kd.jumlah), 0)
			   FROM
				 kuitansidet kd
			   WHERE
				 kd.nokuitansi = k.nokuitansi
				 AND kd.idcarabayar = 1
			   ) AS dibayarpasien
			 , (ifnull(sum(nd.tarifjs * nd.qty)
			   + sum(nd.tarifjm * nd.qty)
			   + sum(nd.tarifjp * nd.qty)
			   + sum(nd.tarifbhp * nd.qty)
			   + n.uangr, 0) - ifnull(n.diskon, 0))
				-
			   (
			   SELECT ifnull(sum(kd.jumlah), 0)
			   FROM
				 kuitansidet kd
			   WHERE
				 kd.nokuitansi = k.nokuitansi
				 AND kd.idcarabayar = 1
			   ) as jumlahtagihan
			 , if((SELECT nonota
				   FROM
					 tagihanrj
				   WHERE
					 nonota = n.nonota
					 AND notagihan = '$notagihan') IS NOT NULL, 1, 0) AS pilih
			FROM
			  registrasi r
			LEFT JOIN pasien p
			ON p.norm = r.norm
			LEFT JOIN jkelamin j
			ON p.idjnskelamin = j.idjnskelamin
			LEFT JOIN registrasidet rd
			ON rd.noreg = r.noreg
			LEFT JOIN bagian b
			ON b.idbagian = rd.idbagian
			LEFT JOIN dokter d
			ON d.iddokter = rd.iddokter
			LEFT JOIN nota n
			ON n.idregdet = rd.idregdet
			LEFT JOIN notadet nd
			ON nd.nonota = n.nonota
			LEFT JOIN kuitansi k
			ON k.nokuitansi = n.nokuitansi
		WHERE rd.userbatal IS NULL
		AND b.idjnspelayanan = 3
		AND n.nonota IS NOT NULL
		AND r.idpenjamin = '$idpenjamin'
			GROUP BY r.noreg");
			
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function get_transaksi_ri(){
		$idpenjamin = $this->input->post("idpenjamin");
		$notagihan = $this->input->post("notagihan");

        $q = $this->db->query("SELECT nota.nona
			 , registrasi.noreg
			 , registrasidet.tglreg
			 , registrasidet.tglmasuk
			 , registrasidet.tglkeluar
			 , trim(LEADING '0' FROM pasien.norm) as norm
			 , pasien.nmpasien
			 , pasien.idjnskelamin
			 , jkelamin.kdjnskelamin
			 , pasien.tgllahir
			 , registrasidet.idbagian
			 , bagian.nmbagian
			 , ifnull(sum(notadet.tarifjs * notadet.qty)
			   + sum(notadet.tarifjm * notadet.qty)
			   + sum(notadet.tarifjp * notadet.qty)
			   + sum(notadet.tarifbhp * notadet.qty)
			   + nota.uangr, 0) - ifnull(nota.diskon, 0) AS total
			 , (
			   SELECT ifnull(sum(kd.jumlah), 0)
			   FROM
				 kuitansidet kd
			   WHERE
				 kd.nokuitansi = kuitansi.nokuitansi
				 AND kd.idcarabayar = 1
			   ) AS dibayarpasien
			 , (ifnull(sum(notadet.tarifjs * notadet.qty)
			   + sum(notadet.tarifjm * notadet.qty)
			   + sum(notadet.tarifjp * notadet.qty)
			   + sum(notadet.tarifbhp * notadet.qty)
			   + nota.uangr, 0) - ifnull(nota.diskon, 0))
				-
			   (
			   SELECT ifnull(sum(kd.jumlah), 0)
			   FROM
				 kuitansidet kd
			   WHERE
				 kd.nokuitansi = kuitansi.nokuitansi
				 AND kd.idcarabayar = 1
			   ) as jumlahtagihan
			 , if((SELECT nona FROM tagihanri WHERE nona=nota.nona AND notagihan='$notagihan') IS NOT NULL,1,0) as pilih
		FROM
		  registrasi
		LEFT JOIN registrasidet
		ON registrasidet.noreg = registrasi.noreg
		LEFT JOIN nota
		ON nota.idregdet = registrasidet.idregdet
		LEFT JOIN notadet
		ON notadet.nonota = nota.nonota
		LEFT JOIN kuitansi
		ON kuitansi.nokuitansi = nota.nokuitansi
		LEFT JOIN pasien
		ON pasien.norm = registrasi.norm
		LEFT JOIN jkelamin
		ON pasien.idjnskelamin = jkelamin.idjnskelamin
		LEFT JOIN penjamin
		ON penjamin.idpenjamin = registrasi.idpenjamin
		LEFT JOIN dokter
		ON dokter.iddokter = registrasidet.iddokter
		LEFT JOIN bagian
		ON bagian.idbagian = registrasidet.idbagian
		WHERE
		  registrasi.idjnspelayanan = 2
		  AND nota.nona IS NOT NULL
		  AND registrasi.idpenjamin = '$idpenjamin'
		GROUP BY
		  registrasi.noreg");
			
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function getrekbank(){
		$q = $this->db->query("select * from setting 
		where idklpsetting = ".$this->input->post("idklpsetting")." 
		and idset = ".$this->input->post("idset")."");
		
		$data = $q->row_array();
		
		echo $data['ketset'];
	}
	
	function getnotagihan(){
		$initial = 'NT';
		$q = $this->db->query("SELECT 
			max(notagihan) AS max_no,
			max(cast(right(notagihan,6) AS UNSIGNED)) AS max_count,
			6 - length(max(cast(right(notagihan,6) AS UNSIGNED))) AS length_count
		FROM
			tagihan 
		WHERE 
			SUBSTRING(notagihan,1,2)='".$initial."' 
			AND SUBSTRING(notagihan,3,2)='".date('y')."'");
		$data = $q->row_array();
		
		if(is_null($data['max_no'])) {
			$max = $initial.strval(date('y'))."000001";
		} else {
			$par = "";
			$add = "";
			for($i=1;$i <= $data['length_count'];$i++) {
				$par .= '0';
			}
			$add = $data['max_count'] + 1;
			$max = $initial.strval(date('y')).$par.$add;
		}
		
		return $max;
	}
	
	function insert_tagihan(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('tagihan',$dataArray);
		
        if ($ret) {
			$this->exec_tagihan_det($dataArray['notagihan']);
		}
    }
	
	function update_tagihan(){
		$dataArray = $this->getFieldsAndValues();
		$this->db->update('tagihan',$dataArray);
		$this->db->where('notagihan',$dataArray['notagihan']);
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
	
	function getFieldsAndValues(){
		$dataArray = array(
             'notagihan'=> ($_POST['notagihan']) ? $_POST['notagihan']:$this->getnotagihan(),
             'tgltagihan'=> $_POST['tgltagihan'],
			 'idpenjamin'=> $_POST['idpenjamin'],
			 'perihal'=> $_POST['perihal'],
			 'lampiran'=> $_POST['lampiran'],
			 'tgljatuhtempo'=> $_POST['tgljatuhtempo'],
			 'approval'=> $_POST['approval'],
			 'userid'=> $_POST['userid'],
        );		
		return $dataArray;
	}
	
	function exec_tagihan_det($notagihan){      // ISTRA
        $dataugd=$this->input->post("dataugd");
		$datari=$this->input->post("datari");
		
        $this->db->trans_begin();
        
		$rowsugd = explode(";",$dataugd);
        $row_countugd = count($rowsugd);
        for($riugd=0;$riugd<$row_countugd;$riugd++){
            $rows2ugd = explode(":",$rowsugd[$riugd]);
					
			$this->db->query("CALL sp_simpan_tagihan_penjamin (?,?,?,?,?,?)",
			array('UGD',$notagihan,$rows2ugd[0],$rows2ugd[1],$rows2ugd[2],$rows2ugd[3]));
     
        }
		
		$rowsri = explode(";",$datari);
        $row_countri = count($rowsri);
        for($riri=0;$riri<$row_countri;$riri++){
            $rows2ri = explode(":",$rowsri[$riri]);
					
			$this->db->query("CALL sp_simpan_tagihan_penjamin (?,?,?,?,?,?)",
			array('RI',$notagihan,$rows2ri[0],$rows2ri[1],$rows2ri[2],$rows2ri[3]));
     
        }
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            //$return["success"]=false;
            //$return["message"]="Eksekusi Data gagal";
			echo $notagihan;
        }
        else
        {
            $this->db->trans_commit();
            //$return["success"]=true;
           // $return["message"]="Eksekusi Data Berhasil";
		   echo $notagihan;
        }
        //return $return["success"];
    }
	
	function get_kuitansipenjamin(){
		$notagihan = $this->input->post("notagihan");

        $q = $this->db->query("SELECT * FROM v_kuitansipenjamin where notagihan ='$notagihan'");
			
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
	
	function getnokuitansipenjamin(){
		$initial = 'KP';
		$q = $this->db->query("SELECT 
			max(nokuitansipenjamin) AS max_no,
			max(cast(right(nokuitansipenjamin,6) AS UNSIGNED)) AS max_count,
			6 - length(max(cast(right(nokuitansipenjamin,6) AS UNSIGNED))) AS length_count
		FROM
			kuitansipenjamin 
		WHERE 
			SUBSTRING(nokuitansipenjamin,1,2)='".$initial."' 
			AND SUBSTRING(nokuitansipenjamin,3,2)='".date('y')."'");
		$data = $q->row_array();
		
		if(is_null($data['max_no'])) {
			$max = $initial.strval(date('y'))."000001";
		} else {
			$par = "";
			$add = "";
			for($i=1;$i <= $data['length_count'];$i++) {
				$par .= '0';
			}
			$add = $data['max_count'] + 1;
			$max = $initial.strval(date('y')).$par.$add;
		}
		
		return $max;
	}
	
	function insert_kuitansipenjamin(){
		$dataArray = array(
             'nokuitansipenjamin'=> ($_POST['nokuitansipenjamin']) ? $_POST['nokuitansipenjamin']:$this->getnokuitansipenjamin(),
             'tglkuitansipenjamin'=> $_POST['tglkuitansipenjamin'],
			 'userid'=> $_POST['userid'],
			 'notagihan'=> $_POST['notagihan'],
			 'idcarabayar'=> $_POST['idcarabayar'],
			 'idstkuitansi'=> 1,
			 'jumlah'=> $_POST['jumlah'],
			 'tgljaminput'=> date("Y-m-d H:i:s"),
        );
		$ret = $this->rhlib->insertRecord('kuitansipenjamin',$dataArray);
		
        if ($ret) {
			 echo $dataArray['nokuitansipenjamin'];
		} else {
			echo null;
		}
    }
	
	function hapus_kuitansipenjamin() {
		$rowsdelete = explode(";",$_POST['pardelete']);
        $row_countdelete = count($rowsdelete);
        for($ridelete=0;$ridelete<$row_countdelete;$ridelete++){
            $rows2delete = explode(":",$rowsdelete[$ridelete]);
			
			$where['nokuitansipenjamin'] = $rows2delete[0];
			$this->rhlib->deleteRecord('kuitansipenjamin',$where);
     
        }
	}

  function get_tagihan_jurnaling(){
    $nokuitansipenjamin     = $this->input->post("nokuitansipenjamin");
    $kdakun_kredit    = '12120'; // piutang Penjamin
    $kdakun_debit     = ''; // tergantung cara bayar (cash / transfer)
    
    if(empty($nokuitansipenjamin)) $nokuitansipenjamin = '0';
    
    $data_kuitansi = $this->db->get_where('kuitansipenjamin', array('nokuitansipenjamin' => $nokuitansipenjamin))->row_array();
    if(isset($data_kuitansi['idcarabayar'])){
      if($data_kuitansi['idcarabayar'] == '1')
        $kdakun_debit     = '11000'; // Kas
      elseif($data_kuitansi['idcarabayar'] == '2')
        $kdakun_debit     = '11200'; // Kas di Bank  
    }
    
    $akun_kredit = $this->db->get_where('akun', array('kdakun' => $kdakun_kredit))->row_array();
    $akun_debit = $this->db->get_where('akun', array('kdakun' => $kdakun_debit))->row_array(); 

    $breakdown_jurnal = array();

    $breakdown_jurnal[] = array(
      'idakun' => $akun_debit['idakun'],
      'kdakun' => $akun_debit['kdakun'],
      'nmakun' => $akun_debit['nmakun'],
      'noreff' => '',
      'debit' => $data_kuitansi['jumlah'], 
      'kredit'=> 0
    );

    $breakdown_jurnal[] = array(
      'idakun' => $akun_kredit['idakun'], 
      'kdakun' => $akun_kredit['kdakun'], 
      'nmakun' => $akun_kredit['nmakun'], 
      'noreff' => '',
      'debit' => 0, 
      'kredit'=> $data_kuitansi['jumlah']
    );

    $build_array = array ("success"=>true, "results"=>count($breakdown_jurnal), "data"=> $breakdown_jurnal);

    echo json_encode($build_array);
  }

  function posting_tagihan_penjamin(){

    $nokuitansipenjamin  = $this->input->post("nokuitansipenjamin_posting");
    $tglkuitansipenjamin  = $this->input->post("tglkuitansipenjamin_posting");
    $kdpenjamin  = $this->input->post("kdpenjamin");
    $nmpenjamin  = $this->input->post("nmpenjamin");
    $nominal_jurnal  = $this->input->post("nominal_jurnal");
    $post_grid  = $this->input->post("post_grid");
    $userid    = $this->input->post("userid");
    $kdjurnal = $this->get_nojurnal_khusus();

    if(empty($nokuitansipenjamin) || empty($nominal_jurnal) || empty($post_grid) || empty($userid))
    {
      $return = array('success' => false, 'message' => 'nokuitansipg dan user id tidak boleh kosong');
      echo json_encode($return);
      die();
    }

    $dataJurnal = array(
      'kdjurnal' => $kdjurnal,
      'idjnsjurnal' => 2, // jurnal khusus
      'tgltransaksi' => $tglkuitansipenjamin,
      'tgljurnal' => date('Y-m-d'),
      'keterangan' => 'Pembayaran Penjamin : '.$nmpenjamin.' ('.$kdpenjamin.')',
      'noreff' => $nokuitansipenjamin,
      'userid' => $userid,
      'nominal' => $nominal_jurnal,
      'idjnstransaksi' => 14, // 14 = Tagihan Penjamin
      'status_posting' => 1,
    );

    $continue = true;
    $this->db->trans_start();
    
    //insert jurnal
    if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
    //insert jurnal det
    $jdet_rows = explode(';', $post_grid);
    if(!empty($jdet_rows) AND $continue === true)
    {
      foreach($jdet_rows as $idx => $jdet)
      {
        $item_jdet = explode('^', $jdet);
        if(isset($item_jdet[4]) && isset($item_jdet[5]))
        {
          /*
            item_jdet[0] = idakun   
            item_jdet[1] = kdakun 
            item_jdet[2] = nmakun 
            item_jdet[3] = noreff 
            item_jdet[4] = debit 
            item_jdet[5] = kredit
          */
          $ins_jdet = array(
            'kdjurnal' => $kdjurnal,
            'tahun' => date('Y'),
            'idakun' => $item_jdet[0],  
            'noreff' => $item_jdet[3],
            'debit' => $item_jdet[4],
            'kredit' => $item_jdet[5],
          );
          
          if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;

        }

      }
    }

    if($continue){
      $this->db->trans_complete();
      $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
      echo json_encode($return);
      die();
    
    }else{
      $this->db->trans_rollback();
      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
      echo json_encode($return);
      die();
    }
  
  }
	
  function get_nojurnal_khusus(){
    $q = "SELECT getOtoNoJurnalKhusus(now()) as nm;";
    $query  = $this->db->query($q);
    $nm= ''; 
                
    if ($query->num_rows() != 0)
    {
        $row = $query->row();
        $nm=$row->nm;
    }
    return $nm;
  }

	function cetak_tagihan_penjamin($idpenjamin = '', $dataugd = '', $datari = '')
	{
		$arr_ugd = explode('-', $dataugd);
		$arr_ri = explode('-', $datari);
		$this->pdf->SetAutoPageBreak(true);
		$this->_generate_notaugd($idpenjamin,$arr_ugd);
		
		foreach($arr_ri as $nona){
			if(empty($nona)) continue;
			$this->_generate_notari($nona);
		}
		
		//Close and output PDF document
		$this->pdf->Output('tagihan_penjamin.pdf', 'I');
		//============================================================+
		// END OF FILE
		//============================================================+
		
	}
	
	function _generate_notaugd($idpenjamin = '', $arr_ugd = array())
	{
		$this->pdf->SetPrintHeader(false);
		
		if(empty($arr_ugd)) return;
		
		$get_penjamin = $this->db->get_where('penjamin', array('idpenjamin' => $idpenjamin))->row_array();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 400, 'ury' => 345),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		//$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-20);
		$this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNotaRI();
		$this->pdf->SetFont('helvetica', 'B', 12);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">TAGIHAN PENJAMIN RJ & UGD</div>', '', 1, 0, true, 'C', true);
		
		$x+=10;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Penjamin');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.htmlentities($get_penjamin['nmpenjamin']));
		
		
		$x+=5;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');

        $this->pdf->Line(3, $x+2, 390, $x+2, $style);

		$isi = '';
		$total = 0;
		$header = 0;
		$no = 1;
		
		$isi .="<tr>
						<td width=\"10%\" align=\"center\">No.</td>
						<td width=\"40%\" >Pasien</td>
						<td width=\"25%\" >Item</td>
						<td width=\"25%\" align=\"right\">Tagihan</td>
					</tr>";
		foreach($arr_ugd as $idx => $nonota){
			if(empty($nonota)) continue;
			
			//get detail pasien
			$q_pasien = "
			SELECT registrasi.noreg, pasien.norm, pasien.nmpasien, registrasidet.tglreg
			FROM nota
			left join registrasidet on registrasidet.idregdet = nota.idregdet 
			left join registrasi on registrasi.noreg = registrasidet.noreg
			left join pasien on pasien.norm = registrasi.norm
			WHERE nota.nonota = '".$nonota."'
			group by nota.nonota";
			$detail_pasien = $this->db->query($q_pasien)->row_array();
			
			//get tindakan dijamin
			$q_tindakan = "
			SELECT notadet.nonota, notadet.kditem, notadet.dijamin, notadet.stdijamin,pelayanan.nmpelayanan
			FROM notadet
			LEFT JOIN pelayanan ON notadet.kditem = pelayanan.kdpelayanan
			WHERE
				notadet.nonota = '".$nonota."' AND 
				stdijamin = 1 AND 
				left(kditem, 1) = 'T'
			";
			$detail_tindakan = $this->db->query($q_tindakan)->result_array();
			
			//get obat dijamin
			$q_obat = "
			SELECT notadet.nonota, notadet.kditem, notadet.dijamin, notadet.stdijamin, barang.nmbrg
			FROM notadet
			LEFT JOIN barang ON notadet.kditem = barang.kdbrg
			WHERE 
				notadet.nonota = '".$nonota."' AND
				stdijamin = 1 AND
				left(kditem, 1) = 'B'
			";
			$detail_obat = $this->db->query($q_obat)->result_array();
			
			if(empty($detail_tindakan) && empty($detail_obat)) continue;
			
			$item_pasien = '';
			$tagihan_pasien = '';
			$total_pasien = 0;
			foreach($detail_tindakan as $tindakan){
				$item_pasien .= htmlentities($tindakan['nmpelayanan'])."<br>";
				$tagihan_pasien .= number_format($tindakan['dijamin'],0,',','.')."<br>";
				$total_pasien += $tindakan['dijamin'];
			}
			foreach($detail_obat as $obat){
				$item_pasien .= htmlentities($obat['nmbrg'])."<br>";
				$tagihan_pasien .= number_format($obat['dijamin'],0,',','.')."<br>";
				$total_pasien += $obat['dijamin'];
			}
			
			$isi .= "<tr>
						<td width=\"10%\" align=\"center\">". $no ."</td>
						<td width=\"40%\" >(".$detail_pasien['norm'].") ".htmlentities($detail_pasien['nmpasien'])."</td>
						<td width=\"25%\" >".$item_pasien."</td>
						<td width=\"25%\" align=\"right\">".$tagihan_pasien."</td>
					</tr>
					<tr>
						<td colspan=\"3\" align=\"right\">Total Tagihan Pasien</td>
						<td width=\"25%\" align=\"right\">".number_format($total_pasien,0,',','.')."</td>
					</tr>
					<tr>
						<td colspan=\"4\" align=\"right\"></td>
					</tr>
					";
					
			$total += $total_pasien;
			$no++;
		
		}
		
		
		$x+=6;
		$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
			  <tbody>
			  ". $isi ."
			  </tbody>
				<tr>
					<td width=\"75%\" colspan=\"3\"><p align=\"right\"><strong>Total</strong></p></td>
					<td width=\"25%\" align=\"right\"><strong>". number_format((int)$total,0,',','.') ."</strong></td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
		
	}
  
	function _generate_notari($nona)
	{
		$this->pdf->SetPrintHeader(false);
		
		//get detail pasien
		$q_pasien = "SELECT registrasi.noreg, pasien.norm, pasien.nmpasien, registrasidet.tglreg, penjamin.nmpenjamin
		FROM notaakhir
		LEFT JOIN nota ON notaakhir.nona = nota.nona 
		left join registrasidet on registrasidet.idregdet = nota.idregdet 
		left join registrasi on registrasi.noreg = registrasidet.noreg
		left join pasien on pasien.norm = registrasi.norm
		left join penjamin on penjamin.idpenjamin = registrasi.idpenjamin
		WHERE notaakhir.nona = '".$nona."'
		group by notaakhir.nona";
		$detail_pasien = $this->db->query($q_pasien)->row_array();
		
		//get tindakan dijamin
		$q_tindakan = "
		SELECT notaakhirdet.nona, notaakhirdet.kditem,notaakhirdet.dijamin,notaakhirdet.stdijamin,pelayanan.nmpelayanan
		FROM notaakhirdet
		LEFT JOIN pelayanan ON notaakhirdet.kditem = pelayanan.kdpelayanan
		WHERE
			nona = '".$nona."' AND 
			stdijamin = 1 AND 
			left(kditem, 1) = 'T'
		";
		$detail_tindakan = $this->db->query($q_tindakan)->result_array();
		
		//get obat dijamin
		$q_obat = "
		SELECT notaakhirdet.nona, notaakhirdet.kditem, notaakhirdet.dijamin, notaakhirdet.stdijamin, barang.nmbrg
		FROM notaakhirdet
		LEFT JOIN barang ON notaakhirdet.kditem = barang.kdbrg
		WHERE 
			nona = '".$nona."' AND
			stdijamin = 1 AND
			left(kditem, 1) = 'B'
		";
		$detail_obat = $this->db->query($q_obat)->result_array();
		
		$style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'phase' => 0, 'color' => array(0, 0, 0));
		$page_format = array(
			'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 400, 'ury' => 345),
			'Dur' => 3,
			'trans' => array(
				'D' => 1.5,
				'S' => 'Split',
				'Dm' => 'V',
				'M' => 'O'
			),
			'Rotate' => 0,
			'PZ' => 1,
		);
		$this->pdf->AddPage('L', $page_format, false, false);
		$this->pdf->SetMargins(PDF_MARGIN_LEFT-13, PDF_MARGIN_TOP-5, PDF_MARGIN_RIGHT-10);
		//$this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM-20);
		$this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM-20);
		$this->pdf->headerNotaRI();
		$this->pdf->SetFont('helvetica', 'B', 12);
		$x=25;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">TAGIHAN PENJAMIN RI</div>', '', 1, 0, true, 'C', true);
		
		$this->pdf->SetFont('helvetica', '', 10);
		$x+=5;$y+=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. Nota');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$detail_pasien['noreg']);
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Tgl. Reg');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.date_format(date_create($detail_pasien['tglreg']), 'd-m-Y'));
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Pasien');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.htmlentities($detail_pasien['nmpasien']));
		
		$x+=0;$y=320;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">No. RM');
		$x+=0;$y+=25;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.$detail_pasien['norm']);
		
		
		$x+=5;$y=3;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:5;">Penjamin');
		$x+=0;$y+=40;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, ':');
		$x+=0;$y+=2;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '<div style="letter-spacing:4;">'.htmlentities($detail_pasien['nmpenjamin']));
		
		$x+=3;$y=0;
		$this->pdf->writeHTMLCell(0, 10, $y, $x, '');

        $this->pdf->Line(3, $x+2, 390, $x+2, $style);

		$isi = '';
		$total = 0;
		$header = 0;
		$no = 1;
		$isi .= "<tr>
			<td width=\"10%\" align=\"center\">No.</td>
			<td width=\"70%\">Item</td>
			<td width=\"20%\" align=\"right\">Tagihan</td>
		</tr>";
		
		if(!empty($detail_tindakan)){
			$isi .= "
			<tr>
				<td width=\"100%\" colspan=\"3\"></td>
			</tr>
			<tr>
				<td width=\"100%\" colspan=\"3\">Tindakan</td>
			</tr>";
		}
		foreach($detail_tindakan AS $i=>$val){
			if($i > 10) continue;
			$total += $val['dijamin'];
			
			$isi .= "<tr>
				<td width=\"10%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">". htmlentities($val['nmpelayanan']) ."</td>
				<td width=\"20%\" align=\"right\">". number_format($val['dijamin'],0,',','.') ."</td>
			</tr>";
			$no++;
		}
		
		if(!empty($detail_obat)){
			$isi .= "
			<tr>
				<td width=\"100%\" colspan=\"3\" ></td>
			</tr>
			<tr>
				<td width=\"100%\" colspan=\"3\" >Obat / Alkes</td>
			</tr>";
		}
		foreach($detail_obat AS $i =>$val){
			
			$total += $val['dijamin'];
			
			$isi .= "<tr>
				<td width=\"10%\" align=\"center\">". $no ."</td>
				<td width=\"70%\">". htmlentities($val['nmbrg']) ."</td>
				<td width=\"20%\" align=\"right\">". number_format($val['dijamin'],0,',','.') ."</td>
			</tr>";
			$no++;
		}
		
		$html = "<div style=\"letter-spacing:5;\"><br/><br/><font size=\"10\" face=\"helvetica\">
			<table border=\"0px\" cellpadding=\"0\" width=\"97%\">
			  <tbody>
			  ". $isi ."
			  </tbody>
				<tr>
					<td width=\"75%\"><p align=\"right\"><strong> Total </strong></p></td>
					<td width=\"25%\" align=\"right\"><strong>". number_format((int)$total,0,',','.') ."</strong></td>
				</tr>
			</table></font></div>
		";
		$this->pdf->writeHTML($html,true,false,false,false);
	

	}
	
  
  
  }

?>