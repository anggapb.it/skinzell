<?php

class Mtemobat_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_mtemobat(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
              
		$this->db->select("*");
		$this->db->from("v_mtemobat");
		$this->db->where('v_mtemobat.nmjnshirarki !=','null');
		
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
      
        $this->db->select("*");
		$this->db->from("v_mtemobat");

        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_mtemobat(){     
		$where['idtempobat'] = $_POST['idtempobat'];
		$del = $this->rhlib->deleteRecord('templateobat',$where);
        return $del;
    }
		
	function insert_mtemobat(){
		$dataArray = $this->getFieldsAndValues();
		$ret = $this->rhlib->insertRecord('templateobat',$dataArray);
        return $ret;
    }

	function update_mtemobat(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('idtempobat', $_POST['idtempobat']);
		$this->db->update('templateobat', $dataArray); 
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
    }
			
	function getFieldsAndValues(){
	
	$tem_idtempobat =  $_POST['tem_idtempobat']; 

	if($tem_idtempobat=="") $tem_idtempobat=null;
	
		$dataArray = array(
             'idtempobat'		=> $_POST['idtempobat'],
			 'idjnshirarki'		=> $_POST['idjnshirarki'],
			 'idstatus'			=> $_POST['idstatus'],
			 'tem_idtempobat'	=> $this->id_tempobat('nmtempobat',$tem_idtempobat),
             'nmtempobat'		=> $_POST['nmtempobat'],
        );		
/* 	var_dump($dataArray);
	exit; */
		return $dataArray;
	}
	
	function get_parent_tempobat(){ 
		$query = $this->db->getwhere('templateobat',array('idjnshirarki'=>'0'));
		$parent = $query->result();
		$ttl = count($parent);
        $arrPenyakit = array ("success"=>true,"results"=>$ttl,"data"=>$parent);
		echo json_encode($arrPenyakit);
    }
	
	function id_tempobat($where, $val){
		$query = $this->db->getwhere('templateobat',array($where=>$val));
		$id = $query->row_array();
		return  $id['idtempobat'];
    }
	
	function getNmtempobat(){
		$query = $this->db->getwhere('templateobat',array('idtempobat'=>$_POST['tem_idtempobat']));
		$nm = $query->row_array();
		echo json_encode($nm['nmtempobat']);
    }	
}
