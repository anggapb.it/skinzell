<?php

class Lapprive_controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_lap_prive(){
		$tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		
        $this->db->select('*');
        $this->db->from('v_lapprive');
		
		if($tglakhir){
			$this->db->where('`tglkuitansi` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}else{
			$this->db->where('`tglkuitansi`', date('Y-m-d'));
		}
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result_array();
        }
		
        $ttl = $this->db->count_all('v_lapprive');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    
	}
	
	
}
