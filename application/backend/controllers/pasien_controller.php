<?php

class Pasien_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_pasien(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        
        $fields		= $this->input->post("fields");
        $query		= $this->input->post("query");
		
        $norm		= $this->input->post("norm");
        $normlama	= $this->input->post("normlama");
        $nmpasien	= $this->input->post("nmpasien");
        $tgllahir	= $this->input->post("tgllahir");
        $tptlahir	= $this->input->post("tptlahir");
        $alamat		= $this->input->post("alamat");
        $nmibu		= $this->input->post("nmibu");
        $notelp		= $this->input->post("notelp");
      
        $this->db->select("*,pasien.norm as normnya");
        $this->db->from("pasien");
		$this->db->join('jkelamin',
                'jkelamin.idjnskelamin = pasien.idjnskelamin', 'left');
		$this->db->join('daerah',
                'daerah.iddaerah = pasien.iddaerah', 'left');
		$this->db->order_by('norm desc');
		
        if($norm != '')$this->db->like('norm', $norm);
        if($normlama != '')$this->db->like('normlama', $normlama);
        if($nmpasien != ''){
			$arrnmpasien = explode(' ', $nmpasien);
			foreach($arrnmpasien AS $val){
				$this->db->like('nmpasien', $val);
			}
		}
        if($tgllahir != '')$this->db->where('tgllahir', $tgllahir);
        if($alamat != ''){
			$arralamat = explode(' ', $alamat);
			foreach($arralamat AS $val){
				$this->db->like('alamat', $val);
			}
		}
        if($tptlahir != '')$this->db->like('tptlahir', $tptlahir);
        if($nmibu != '')$this->db->like('nmibu', $nmibu);
        if($notelp != ''){
			$this->db->like('notelp', $notelp);
			$this->db->like('nohp', $notelp);
		}
		
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow(){
        $start		= $this->input->post("start");
        $limit		= $this->input->post("limit");
        
        $fields		= $this->input->post("fields");
        $query		= $this->input->post("query");
		
        $norm		= $this->input->post("norm");
        $normlama	= $this->input->post("normlama");
        $nmpasien	= $this->input->post("nmpasien");
        $tgllahir	= $this->input->post("tgllahir");
        $tptlahir	= $this->input->post("tptlahir");
        $alamat		= $this->input->post("alamat");
        $nmibu		= $this->input->post("nmibu");
        $notelp		= $this->input->post("notelp");
    
        $this->db->select("*");
        $this->db->from("pasien");
		$this->db->join('jkelamin',
                'jkelamin.idjnskelamin = pasien.idjnskelamin', 'left');
		$this->db->join('daerah',
                'daerah.iddaerah = pasien.iddaerah', 'left');
		$this->db->order_by('norm desc');
		
        if($norm != '')$this->db->like('norm', $norm);
        if($normlama != '')$this->db->like('normlama', $normlama);
        if($nmpasien != ''){
			$arrnmpasien = explode(' ', $nmpasien);
			foreach($arrnmpasien AS $val){
				$this->db->like('nmpasien', $val);
			}
		}
        if($tgllahir != '')$this->db->where('tgllahir', $tgllahir);
        if($alamat != ''){
			$arralamat = explode(' ', $alamat);
			foreach($arralamat AS $val){
				$this->db->like('alamat', $val);
			}
		}
        if($tptlahir != '')$this->db->like('tptlahir', $tptlahir);
        if($nmibu != '')$this->db->like('nmibu', $nmibu);
        if($notelp != ''){
			$this->db->like('notelp', $notelp);
			$this->db->like('nohp', $notelp);
		}
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
		
	function insorupd_pasien(){
        $this->db->trans_begin();
		$query = $this->db->getwhere('pasien',array('norm'=>str_pad($_POST['tf_norm'], 10, "0", STR_PAD_LEFT)));
		if($query->num_rows() > 0) $pas = $this->update_pasien();
		else $pas = $this->insert_pasien();
		
        if($pas){
            $this->db->trans_commit();
            $ret["success"]=true;
            $ret["norm"]=$pas['norm'];
        }else{
            $this->db->trans_rollback();
            $ret["success"]=false;
        }
        echo json_encode($ret);
    }
	
	function delete_pasien(){     
		$where['idpasien'] = $_POST['idpasien'];
		$del = $this->rhlib->deleteRecord('pasien',$where);
        return $del;
    }
		
	function insert_pasien(){
		$_POST['tf_norm'] = $this->getnorm();
		$dataArray = $this->getFieldsAndValues();
		$z =$this->db->insert('pasien',$dataArray);
		
		if($_POST['tfidres'] != ""){
			$this->db->where('idreservasi', $_POST['tfidres']);
			$this->db->update('reservasi', array('norm'=>$dataArray['norm']));
		}
		
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }

	function update_pasien(){ 				
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('norm', str_pad($_POST['tf_norm'], 10, "0", STR_PAD_LEFT));
		$this->db->update('pasien', $dataArray);
        return $dataArray;
    }
			
	function getFieldsAndValues(){
		if(is_null($_POST['tf_norm']) || $_POST['tf_norm'] == '') $np = $this->getnorm();
		else $np = $_POST['tf_norm'];
		$norm = str_pad($np, 10, "0", STR_PAD_LEFT);
		$dataArray = array(
             'norm'=> $norm,
             'nmpasien'=> $_POST['tf_nmpasien'],
             'idjnskelamin'=> $this->searchId('idjnskelamin','jkelamin','nmjnskelamin',$_POST['cb_jkelamin']),
             'idstkawin'=> $this->searchId('idstkawin','stkawin','nmstkawin',$_POST['cb_stkawin']),
             'alamat'=> $_POST['tf_alamat'],
             'idwn'=> $this->searchId('idwn','wn','kdwn',$_POST['cb_wn']),
             'iddaerah'=> ($_POST['tf_iddaerah']) ? $_POST['tf_iddaerah']:null,//$_POST['tf_iddaerah'],
             'notelp'=> $_POST['tf_notelp'],
             'nohp'=> $_POST['tf_nohp'],
             'tptlahir'=> $_POST['tf_tptlahir'],
             'tgllahir'=> date_format(date_create($_POST['df_tgllahir']), 'Y-m-d'),
             'tgllahirortu'=> date_format(date_create($_POST['df_tgllahirortu']), 'Y-m-d'),
             'tgllahirpasangan'=> date_format(date_create($_POST['df_tgllahirpasangan']), 'Y-m-d'),
             'nmibu'=> $_POST['tf_nmibu'],
             'nmpasangan'=> $_POST['tf_nmpasangan'],
             'idpekerjaan'=> $this->searchId('idpekerjaan','pekerjaan','nmpekerjaan',$_POST['cb_pekerjaan']),
             'idagama'=> $this->searchId('idagama','agama','nmagama',$_POST['cb_agama']),
             //'idjnsidentitas'=> $_POST['idjnsidentitas'],
             //'idstpelayanan'=> $this->searchId('idstpelayanan','stpelayanan','nmstpelayanan',$_POST['cb_stpelayanan']),
             'noidentitas'=> $_POST['tf_noidentitas'],
             'idgoldarah'=> $this->searchId('idgoldarah','goldarah','nmgoldarah',$_POST['cb_goldarah']),
             'idpendidikan'=> $this->searchId('idpendidikan','pendidikan','nmpendidikan',$_POST['cb_pendidikan']),
             'idsukubangsa'=> $this->searchId('idsukubangsa','sukubangsa','nmsukubangsa',$_POST['cb_sukubangsa']),
             'catatan'=> $_POST['ta_catatan'],
             'negara'=> $_POST['tf_negara'],
             'alergi'=> $_POST['ta_alergiobat'],
			 'tgldaftar'=> DATE('Y-m-d'),
			 'foto_pasien'=> $_POST['photocam'],
        );		
		return $dataArray;
	}
	
	function getnorm(){
        $this->db->select("max(norm) AS max_np");
        $this->db->from("pasien");
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function getDataPasien(){
        $cnorm		= $this->input->post("norm");
        $cnormlama	= $this->input->post("normlama");
		if($cnorm != '' || $cnormlama != ''){
			$this->db->select("*");
			$this->db->from("pasien");
			$this->db->join('daerah',
					'daerah.iddaerah = pasien.iddaerah', 'left');
			$this->db->join('jkelamin',
					'jkelamin.idjnskelamin = pasien.idjnskelamin', 'left');
			
			if($cnorm != ''){
				$norm = str_pad($cnorm, 10, "0", STR_PAD_LEFT);
				$this->db->where('norm', $norm);
			}
			if($cnormlama != '')$this->db->where('normlama', $cnormlama);
			$q = $this->db->get();
			$pasien = $q->row_array();
		} else {
			$pasien = false;
		}
		echo json_encode($pasien);
    }
	
	function getCekPasien(){
        $cnorm		= $this->input->post("norm");
		if($cnorm != ''){
			$this->db->select("*");
			$this->db->from("pasien");
			
			$norm = str_pad($cnorm, 10, "0", STR_PAD_LEFT);
			$this->db->where('norm', $norm);
			$this->db->where('idjnskelamin IS NOT NULL', null, false);
			$this->db->where('tgllahir IS NOT NULL', null, false);
			$q = $this->db->get();
			$pasien = $q->row_array();
			if($pasien) $ret['cek'] = true;
			else $ret['cek'] = false;
		} else {
			$ret['cek'] = false;
		}
		echo json_encode($ret);
    }
	
	function getCekPasienres(){
        $cnorm		= $this->input->post("norm");
		$date = date('Y-m-d');
		if($cnorm != ''){
			$this->db->select("*");
			$this->db->from("reservasi");
			
			$norm = str_pad($cnorm, 10, "0", STR_PAD_LEFT);
			$this->db->where('norm', $norm);
			$this->db->where("idstposisipasien = '1'");
			$this->db->where("tglreservasi", $date);
			$this->db->where("idstreservasi = '1'");
			$q = $this->db->get();
			$pasien = $q->row_array();
		} else {
			$pasien = false;
		}
		echo json_encode($pasien);
    }
	
	function getCekPasienresreg(){
        $cnorm		= $this->input->post("norm");
		$date = date('Y-m-d');
		if($cnorm != ''){
			$this->db->select("*");
			$this->db->from("reservasi");
			$this->db->join('dokter',
					'dokter.iddokter = reservasi.iddokter', 'left');
			$this->db->join('bagian',
					'bagian.idbagian = reservasi.idbagian', 'left');
			
			$norm = str_pad($cnorm, 10, "0", STR_PAD_LEFT);
			$this->db->where('norm', $norm);
			$this->db->where("idstposisipasien = '1'");
			$this->db->where("tglreservasi", $date);
			$this->db->where("idstreservasi = '1'");
			$q = $this->db->get();
			$pasien = $q->row_array();
		} else {
			$pasien = false;
		}
		echo json_encode($pasien);
    }
	
	function getNoRmPL(){
        $this->db->select("*");
        $this->db->from("pasien");
		$this->db->where('nmpasien',$_POST['nama']);
		$q = $this->db->get();
		$pasien = $q->row_array();
		echo json_encode($pasien);
    }

}