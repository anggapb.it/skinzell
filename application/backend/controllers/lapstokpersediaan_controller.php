<?php

class Lapstokpersediaan_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_lapstokpersediaan(){
        $tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		$idbagian				= $_POST['idbagian'];
	
		$q = ("SELECT barang.kdbrg
					 , barang.nmbrg
					 , ifnull((SELECT sum(kartustok.saldoawal) AS sa
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) AS sa

					 , ifnull((SELECT sum(kartustok.jmlmasuk) - (ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																		 FROM
																		   kartustok
																		 WHERE
																		   ((kartustok.`kdbrg` = barang.`kdbrg`)
																		   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																		   AND (kartustok.idbagian = bbagian.idbagian)
																		   AND (kartustok.`idjnskartustok` = 2))
																		   AND
																		   kartustok.noref = (SELECT rsdet.noretursupplier AS noretursupplier
																							  FROM
																								retursupplierdet rsdet
																							  WHERE
																								rsdet.idstbayar <> '2'
																								AND
																								rsdet.noretursupplier = kartustok.noref
																								AND
																								rsdet.kdbrg = kartustok.kdbrg
																							  GROUP BY
																								kartustok.kdbrg DESC
																							  LIMIT
																								1)
																		 ORDER BY
																		   kartustok.kdbrg DESC
																		 LIMIT
																		   1), 0)) AS jml
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								 AND
								 kartustok.idjnskartustok = 1
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) AS beli

					 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
							   FROM
								 kartustok
							   WHERE
								 ((kartustok.`kdbrg` = barang.`kdbrg`)
								 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
								 AND (kartustok.idbagian = bbagian.idbagian)
								 AND (kartustok.`idjnskartustok` = 2))
								 AND
								 kartustok.noref = (SELECT rsdet.noretursupplier AS noretursupplier
													FROM
													  retursupplierdet rsdet
													WHERE
													  rsdet.idstbayar = '2'
													  AND
													  rsdet.noretursupplier = kartustok.noref
													  AND
													  rsdet.kdbrg = kartustok.kdbrg
													GROUP BY
													  kartustok.kdbrg DESC
													LIMIT
													  1)
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) AS jmlretbeli

					 , ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
							   FROM
								 kartustok
							   WHERE
								 ((kartustok.`kdbrg` = barang.`kdbrg`)
								 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
								 AND (kartustok.idbagian = bbagian.idbagian)
								 AND (kartustok.`idjnskartustok` = 18))
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) AS jmltbl

					 , ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
							   FROM
								 kartustok
							   WHERE
								 ((kartustok.`kdbrg` = barang.`kdbrg`)
								 AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
								 AND (kartustok.idbagian = bbagian.idbagian)
								 AND (kartustok.`idjnskartustok` = 5))
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) AS `jmldistribusibrg`

					 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idjnskartustok = 10
								 AND
								 kartustok.kode = '0306'
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC), 0) AS `jmldistribusimasukbrg`

					 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idjnskartustok = 11
								 AND
								 kartustok.kode = '0306'
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC), 0) AS `jmlrkbb`

					 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idjnskartustok = 6
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC), 0) AS `jmlrmbg`

					 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idjnskartustok = 7
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC), 0) AS `jmlbhp`

					 , ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								 AND
								 kartustok.`idjnskartustok` IN (3, 16, 17)
								 AND
								 kartustok.noref NOT IN (SELECT kstok.noref AS kd
													 FROM
													   kartustok kstok
													 WHERE
													   kstok.kdbrg = barang.kdbrg
													   AND
													   kstok.idbagian = bbagian.idbagian
													   AND
													   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
													   AND
													   kstok.`idjnskartustok` = '19')
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) AS `jmljualbrg`

					 , ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idjnskartustok IN (14, 15)
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC), 0) AS `jmlretjualbrg`

					 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
                                                                  FROM
                                                                    kartustok
                                                                  WHERE
                                                                    kartustok.kdbrg = barang.kdbrg
                                                                    AND
                                                                    kartustok.idjnskartustok = 10
                                                                    AND
                                                                    kartustok.kode = '0306'
                                                                    AND
                                                                    kartustok.idbagian = bbagian.idbagian
                                                                    AND
                                                                    kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                  ORDER BY
                                                                    kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
                                                                                                         FROM
                                                                                                           kartustok
                                                                                                         WHERE
                                                                                                           kartustok.kdbrg = barang.kdbrg
                                                                                                           AND
                                                                                                           kartustok.idjnskartustok = 11
                                                                                                           AND
                                                                                                           kartustok.kode = '0306'
                                                                                                           AND
                                                                                                           kartustok.idbagian = bbagian.idbagian
                                                                                                           AND
                                                                                                           kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                                                         ORDER BY
                                                                                                           kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
                                                                                                                                                FROM
                                                                                                                                                  kartustok
                                                                                                                                                WHERE
                                                                                                                                                  kartustok.kdbrg = barang.kdbrg
                                                                                                                                                  AND
                                                                                                                                                  kartustok.idjnskartustok = 7
                                                                                                                                                  AND
                                                                                                                                                  kartustok.idbagian = bbagian.idbagian
                                                                                                                                                  AND
                                                                                                                                                  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                                                                                                ORDER BY
                                                                                                                                                  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
                                                                                                                                                                                              FROM
                                                                                                                                                                                                kartustok
                                                                                                                                                                                              WHERE
                                                                                                                                                                                                kartustok.kdbrg = barang.kdbrg
                                                                                                                                                                                                AND
                                                                                                                                                                                                kartustok.idbagian = bbagian.idbagian
                                                                                                                                                                                                AND
                                                                                                                                                                                                kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                                                                                                                                                AND
                                                                                                                                                                                                kartustok.idjnskartustok = 1
                                                                                                                                                                                              ORDER BY
                                                                                                                                                                                                kartustok.kdbrg DESC
                                                                                                                                                                                              LIMIT
                                                                                                                                                                                                1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
                                                                                                                                                                                                                 FROM
                                                                                                                                                                                                                   kartustok
                                                                                                                                                                                                                 WHERE
                                                                                                                                                                                                                   ((kartustok.`kdbrg` = barang.`kdbrg`)
                                                                                                                                                                                                                   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
                                                                                                                                                                                                                   AND (kartustok.idbagian = bbagian.idbagian)
                                                                                                                                                                                                                   AND (kartustok.`idjnskartustok` = 2))
                                                                                                                                                                                                                 ORDER BY
                                                                                                                                                                                                                   kartustok.kdbrg DESC
                                                                                                                                                                                                                 LIMIT
                                                                                                                                                                                                                   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
                                                                                                                                                                                                                                     FROM
                                                                                                                                                                                                                                       kartustok
                                                                                                                                                                                                                                     WHERE
                                                                                                                                                                                                                                       ((kartustok.`kdbrg` = barang.`kdbrg`)
                                                                                                                                                                                                                                       AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
                                                                                                                                                                                                                                       AND (kartustok.idbagian = bbagian.idbagian)
                                                                                                                                                                                                                                       AND (kartustok.`idjnskartustok` = 18))
                                                                                                                                                                                                                                     ORDER BY
                                                                                                                                                                                                                                       kartustok.kdbrg DESC
                                                                                                                                                                                                                                     LIMIT
                                                                                                                                                                                                                                       1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
                                                                                                                                                                                                                                                         FROM
                                                                                                                                                                                                                                                           kartustok
                                                                                                                                                                                                                                                         WHERE
                                                                                                                                                                                                                                                           ((kartustok.`kdbrg` = barang.`kdbrg`)
                                                                                                                                                                                                                                                           AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
                                                                                                                                                                                                                                                           AND (kartustok.idbagian = bbagian.idbagian)
                                                                                                                                                                                                                                                           AND (kartustok.`idjnskartustok` = 5))
                                                                                                                                                                                                                                                         ORDER BY
                                                                                                                                                                                                                                                           kartustok.kdbrg DESC
                                                                                                                                                                                                                                                         LIMIT
                                                                                                                                                                                                                                                           1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
                                                                                                                                                                                                                                                                             FROM
                                                                                                                                                                                                                                                                               kartustok
                                                                                                                                                                                                                                                                             WHERE
                                                                                                                                                                                                                                                                               kartustok.kdbrg = barang.kdbrg
                                                                                                                                                                                                                                                                               AND
                                                                                                                                                                                                                                                                               kartustok.idjnskartustok = 6
                                                                                                                                                                                                                                                                               AND
                                                                                                                                                                                                                                                                               kartustok.idbagian = bbagian.idbagian
                                                                                                                                                                                                                                                                               AND
                                                                                                                                                                                                                                                                               kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                                                                                                                                                                                                                             ORDER BY
                                                                                                                                                                                                                                                                               kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
                                                                                                                                                                                                                                                                                                                    FROM
                                                                                                                                                                                                                                                                                                                      kartustok
                                                                                                                                                                                                                                                                                                                    WHERE
                                                                                                                                                                                                                                                                                                                      kartustok.kdbrg = barang.kdbrg
                                                                                                                                                                                                                                                                                                                      AND
                                                                                                                                                                                                                                                                                                                      kartustok.idjnskartustok = 7
                                                                                                                                                                                                                                                                                                                      AND
                                                                                                                                                                                                                                                                                                                      kartustok.idbagian = bbagian.idbagian
                                                                                                                                                                                                                                                                                                                      AND
                                                                                                                                                                                                                                                                                                                      kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                                                                                                                                                                                                                                                                    ORDER BY
                                                                                                                                                                                                                                                                                                                      kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
                                                                                                                                                                                                                                                                                                                                                           FROM
                                                                                                                                                                                                                                                                                                                                                             kartustok
                                                                                                                                                                                                                                                                                                                                                           WHERE
                                                                                                                                                                                                                                                                                                                                                             kartustok.kdbrg = barang.kdbrg
                                                                                                                                                                                                                                                                                                                                                             AND
                                                                                                                                                                                                                                                                                                                                                             kartustok.idbagian = bbagian.idbagian
                                                                                                                                                                                                                                                                                                                                                             AND
                                                                                                                                                                                                                                                                                                                                                             kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                                                                                                                                                                                                                                                                                                             AND
                                                                                                                                                                                                                                                                                                                                                             kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																							 AND
																																																																																							 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																												 FROM
																																																																																												   kartustok kstok
																																																																																												 WHERE
																																																																																												   kstok.kdbrg = barang.kdbrg
																																																																																												   AND
																																																																																												   kstok.idbagian = bbagian.idbagian
																																																																																												   AND
																																																																																												   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																												   AND
																																																																																												   kstok.`idjnskartustok` = '19')
                                                                                                                                                                                                                                                                                                                                                           ORDER BY
                                                                                                                                                                                                                                                                                                                                                             kartustok.kdbrg DESC
                                                                                                                                                                                                                                                                                                                                                           LIMIT
                                                                                                                                                                                                                                                                                                                                                             1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
                                                                                                                                                                                                                                                                                                                                                                               FROM
                                                                                                                                                                                                                                                                                                                                                                                 kartustok
                                                                                                                                                                                                                                                                                                                                                                               WHERE
                                                                                                                                                                                                                                                                                                                                                                                 kartustok.kdbrg = barang.kdbrg
                                                                                                                                                                                                                                                                                                                                                                                 AND
                                                                                                                                                                                                                                                                                                                                                                                 kartustok.idjnskartustok IN (14, 15)
                                                                                                                                                                                                                                                                                                                                                                                 AND
                                                                                                                                                                                                                                                                                                                                                                                 kartustok.idbagian = bbagian.idbagian
                                                                                                                                                                                                                                                                                                                                                                                 AND
                                                                                                                                                                                                                                                                                                                                                                                 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                                                                                                                                                                                                                                                                                                                               ORDER BY
                                                                                                                                                                                                                                                                                                                                                                                 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
                                                                                                                                                                                                                                                                                                                                                                                                                      FROM
                                                                                                                                                                                                                                                                                                                                                                                                                        kartustok
                                                                                                                                                                                                                                                                                                                                                                                                                      WHERE
                                                                                                                                                                                                                                                                                                                                                                                                                        kartustok.kdbrg = barang.kdbrg
                                                                                                                                                                                                                                                                                                                                                                                                                        AND
                                                                                                                                                                                                                                                                                                                                                                                                                        kartustok.idbagian = bbagian.idbagian
                                                                                                                                                                                                                                                                                                                                                                                                                        AND
                                                                                                                                                                                                                                                                                                                                                                                                                        kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
                                                                                                                                                                                                                                                                                                                                                                                                                      ORDER BY
                                                                                                                                                                                                                                                                                                                                                                                                                        kartustok.kdbrg DESC
                                                                                                                                                                                                                                                                                                                                                                                                                      LIMIT
                                                                                                                                                                                                                                                                                                                                                                                                                        1), 0))) AS sa
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) AS sisa							 

					 , ifnull((SELECT if(kartustok.`idjnskartustok` = 3, sum(kartustok.jmlkeluar * kartustok.hrgbeli), 0) AS jml
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								 AND
								 kartustok.`idjnskartustok` IN (3)
								 AND
								 kartustok.noref NOT IN (SELECT kstok.noref AS kd
													 FROM
													   kartustok kstok
													 WHERE
													   kstok.kdbrg = barang.kdbrg
													   AND
													   kstok.idbagian = bbagian.idbagian
													   AND
													   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
													   AND
													   kstok.`idjnskartustok` = '19')
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) - ifnull((SELECT sum(kartustok.jmlmasuk * kartustok.hrgbeli) AS jmlmasuk
												  FROM
													kartustok
												  WHERE
													kartustok.kdbrg = barang.kdbrg
													AND
													kartustok.idjnskartustok IN (14, 15)
													AND
													kartustok.idbagian = bbagian.idbagian
													AND
													kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
												  ORDER BY
													kartustok.kdbrg DESC), 0) AS nbli

					 , ifnull((SELECT if(kartustok.`idjnskartustok` = 3, sum(kartustok.jmlkeluar * kartustok.hrgjual), 0) AS jml
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
								 AND
								 kartustok.`idjnskartustok` IN (3)
								 AND
								 kartustok.noref NOT IN (SELECT kstok.noref AS kd
													 FROM
													   kartustok kstok
													 WHERE
													   kstok.kdbrg = barang.kdbrg
													   AND
													   kstok.idbagian = bbagian.idbagian
													   AND
													   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
													   AND
													   kstok.`idjnskartustok` = '19')
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) - ifnull((SELECT sum(kartustok.jmlmasuk * kartustok.hrgjual) AS jmlmasuk
												  FROM
													kartustok
												  WHERE
													kartustok.kdbrg = barang.kdbrg
													AND
													kartustok.idjnskartustok IN (14, 15)
													AND
													kartustok.idbagian = bbagian.idbagian
													AND
													kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
												  ORDER BY
													kartustok.kdbrg DESC), 0) AS njual
				
					 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																				  FROM
																					kartustok
																				  WHERE
																					kartustok.kdbrg = barang.kdbrg
																					AND
																					kartustok.idjnskartustok = 10
																					AND
																					kartustok.kode = '0306'
																					AND
																					kartustok.idbagian = bbagian.idbagian
																					AND
																					kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																				  ORDER BY
																					kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																														 FROM
																														   kartustok
																														 WHERE
																														   kartustok.kdbrg = barang.kdbrg
																														   AND
																														   kartustok.idjnskartustok = 11
																														   AND
																														   kartustok.kode = '0306'
																														   AND
																														   kartustok.idbagian = bbagian.idbagian
																														   AND
																														   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																														 ORDER BY
																														   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																								FROM
																																								  kartustok
																																								WHERE
																																								  kartustok.kdbrg = barang.kdbrg
																																								  AND
																																								  kartustok.idjnskartustok = 7
																																								  AND
																																								  kartustok.idbagian = bbagian.idbagian
																																								  AND
																																								  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																								ORDER BY
																																								  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
																																																			  FROM
																																																				kartustok
																																																			  WHERE
																																																				kartustok.kdbrg = barang.kdbrg
																																																				AND
																																																				kartustok.idbagian = bbagian.idbagian
																																																				AND
																																																				kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																				AND
																																																				kartustok.idjnskartustok = 1
																																																			  ORDER BY
																																																				kartustok.kdbrg DESC
																																																			  LIMIT
																																																				1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																								 FROM
																																																								   kartustok
																																																								 WHERE
																																																								   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																								   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																								   AND (kartustok.idbagian = bbagian.idbagian)
																																																								   AND (kartustok.`idjnskartustok` = 2))
																																																								 ORDER BY
																																																								   kartustok.kdbrg DESC
																																																								 LIMIT
																																																								   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
																																																													 FROM
																																																													   kartustok
																																																													 WHERE
																																																													   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																													   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																													   AND (kartustok.idbagian = bbagian.idbagian)
																																																													   AND (kartustok.`idjnskartustok` = 18))
																																																													 ORDER BY
																																																													   kartustok.kdbrg DESC
																																																													 LIMIT
																																																													   1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
																																																																		 FROM
																																																																		   kartustok
																																																																		 WHERE
																																																																		   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																																		   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																																		   AND (kartustok.idbagian = bbagian.idbagian)
																																																																		   AND (kartustok.`idjnskartustok` = 5))
																																																																		 ORDER BY
																																																																		   kartustok.kdbrg DESC
																																																																		 LIMIT
																																																																		   1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																							 FROM
																																																																							   kartustok
																																																																							 WHERE
																																																																							   kartustok.kdbrg = barang.kdbrg
																																																																							   AND
																																																																							   kartustok.idjnskartustok = 6
																																																																							   AND
																																																																							   kartustok.idbagian = bbagian.idbagian
																																																																							   AND
																																																																							   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																							 ORDER BY
																																																																							   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																																																	FROM
																																																																																	  kartustok
																																																																																	WHERE
																																																																																	  kartustok.kdbrg = barang.kdbrg
																																																																																	  AND
																																																																																	  kartustok.idjnskartustok = 7
																																																																																	  AND
																																																																																	  kartustok.idbagian = bbagian.idbagian
																																																																																	  AND
																																																																																	  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																	ORDER BY
																																																																																	  kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
																																																																																										   FROM
																																																																																											 kartustok
																																																																																										   WHERE
																																																																																											 kartustok.kdbrg = barang.kdbrg
																																																																																											 AND
																																																																																											 kartustok.idbagian = bbagian.idbagian
																																																																																											 AND
																																																																																											 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																											 AND
																																																																																											 kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																											 AND
																																																																																											 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																																	 FROM
																																																																																																	   kartustok kstok
																																																																																																	 WHERE
																																																																																																	   kstok.kdbrg = barang.kdbrg
																																																																																																	   AND
																																																																																																	   kstok.idbagian = bbagian.idbagian
																																																																																																	   AND
																																																																																																	   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																	   AND
																																																																																																	   kstok.`idjnskartustok` = '19')
																																																																																										   ORDER BY
																																																																																											 kartustok.kdbrg DESC
																																																																																										   LIMIT
																																																																																											 1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																																															   FROM
																																																																																																 kartustok
																																																																																															   WHERE
																																																																																																 kartustok.kdbrg = barang.kdbrg
																																																																																																 AND
																																																																																																 kartustok.idjnskartustok IN (14, 15)
																																																																																																 AND
																																																																																																 kartustok.idbagian = bbagian.idbagian
																																																																																																 AND
																																																																																																 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																															   ORDER BY
																																																																																																 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
																																																																																																									  FROM
																																																																																																										kartustok
																																																																																																									  WHERE
																																																																																																										kartustok.kdbrg = barang.kdbrg
																																																																																																										AND
																																																																																																										kartustok.idbagian = bbagian.idbagian
																																																																																																										AND
																																																																																																										kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																									  ORDER BY
																																																																																																										kartustok.kdbrg DESC
																																																																																																									  LIMIT
																																																																																																										1), 0))) AS sa
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0) * ifnull((SELECT b.hrgbeli AS hbli
												  FROM
													barang b
												  WHERE
													b.kdbrg = barang.kdbrg), 0) AS hrgbeli
								 
					 , ifnull((SELECT if((kartustok.idbagian <> '11'), (((ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																				  FROM
																					kartustok
																				  WHERE
																					kartustok.kdbrg = barang.kdbrg
																					AND
																					kartustok.idjnskartustok = 10
																					AND
																					kartustok.kode = '0306'
																					AND
																					kartustok.idbagian = bbagian.idbagian
																					AND
																					kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																				  ORDER BY
																					kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																														 FROM
																														   kartustok
																														 WHERE
																														   kartustok.kdbrg = barang.kdbrg
																														   AND
																														   kartustok.idjnskartustok = 11
																														   AND
																														   kartustok.kode = '0306'
																														   AND
																														   kartustok.idbagian = bbagian.idbagian
																														   AND
																														   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																														 ORDER BY
																														   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																								FROM
																																								  kartustok
																																								WHERE
																																								  kartustok.kdbrg = barang.kdbrg
																																								  AND
																																								  kartustok.idjnskartustok = 7
																																								  AND
																																								  kartustok.idbagian = bbagian.idbagian
																																								  AND
																																								  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																								ORDER BY
																																								  kartustok.kdbrg DESC), 0)), ((((((((ifnull((SELECT sum(kartustok.jmlmasuk) AS jml
																																																			  FROM
																																																				kartustok
																																																			  WHERE
																																																				kartustok.kdbrg = barang.kdbrg
																																																				AND
																																																				kartustok.idbagian = bbagian.idbagian
																																																				AND
																																																				kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																				AND
																																																				kartustok.idjnskartustok = 1
																																																			  ORDER BY
																																																				kartustok.kdbrg DESC
																																																			  LIMIT
																																																				1), 0) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																								 FROM
																																																								   kartustok
																																																								 WHERE
																																																								   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																								   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																								   AND (kartustok.idbagian = bbagian.idbagian)
																																																								   AND (kartustok.`idjnskartustok` = 2))
																																																								 ORDER BY
																																																								   kartustok.kdbrg DESC
																																																								 LIMIT
																																																								   1), 0)) + ifnull((SELECT sum(kartustok.`jmlmasuk`) AS `jmlmasuk`
																																																													 FROM
																																																													   kartustok
																																																													 WHERE
																																																													   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																													   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																													   AND (kartustok.idbagian = bbagian.idbagian)
																																																													   AND (kartustok.`idjnskartustok` = 18))
																																																													 ORDER BY
																																																													   kartustok.kdbrg DESC
																																																													 LIMIT
																																																													   1), 0)) - ifnull((SELECT sum(kartustok.`jmlkeluar`) AS `jmlkeluar`
																																																																		 FROM
																																																																		   kartustok
																																																																		 WHERE
																																																																		   ((kartustok.`kdbrg` = barang.`kdbrg`)
																																																																		   AND (kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."')
																																																																		   AND (kartustok.idbagian = bbagian.idbagian)
																																																																		   AND (kartustok.`idjnskartustok` = 5))
																																																																		 ORDER BY
																																																																		   kartustok.kdbrg DESC
																																																																		 LIMIT
																																																																		   1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																							 FROM
																																																																							   kartustok
																																																																							 WHERE
																																																																							   kartustok.kdbrg = barang.kdbrg
																																																																							   AND
																																																																							   kartustok.idjnskartustok = 6
																																																																							   AND
																																																																							   kartustok.idbagian = bbagian.idbagian
																																																																							   AND
																																																																							   kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																							 ORDER BY
																																																																							   kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jmlkeluar
																																																																																	FROM
																																																																																	  kartustok
																																																																																	WHERE
																																																																																	  kartustok.kdbrg = barang.kdbrg
																																																																																	  AND
																																																																																	  kartustok.idjnskartustok = 7
																																																																																	  AND
																																																																																	  kartustok.idbagian = bbagian.idbagian
																																																																																	  AND
																																																																																	  kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																	ORDER BY
																																																																																	  kartustok.kdbrg DESC), 0)) - ifnull((SELECT sum(kartustok.jmlkeluar) AS jml
																																																																																										   FROM
																																																																																											 kartustok
																																																																																										   WHERE
																																																																																											 kartustok.kdbrg = barang.kdbrg
																																																																																											 AND
																																																																																											 kartustok.idbagian = bbagian.idbagian
																																																																																											 AND
																																																																																											 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																											 AND
																																																																																											 kartustok.`idjnskartustok` IN (3, 16, 17)
																																																																																											 AND
																																																																																											 kartustok.noref NOT IN (SELECT kstok.noref AS kd
																																																																																																	 FROM
																																																																																																	   kartustok kstok
																																																																																																	 WHERE
																																																																																																	   kstok.kdbrg = barang.kdbrg
																																																																																																	   AND
																																																																																																	   kstok.idbagian = bbagian.idbagian
																																																																																																	   AND
																																																																																																	   kstok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																	   AND
																																																																																																	   kstok.`idjnskartustok` = '19')
																																																																																										   ORDER BY
																																																																																											 kartustok.kdbrg DESC
																																																																																										   LIMIT
																																																																																											 1), 0)) + ifnull((SELECT sum(kartustok.jmlmasuk) AS jmlmasuk
																																																																																															   FROM
																																																																																																 kartustok
																																																																																															   WHERE
																																																																																																 kartustok.kdbrg = barang.kdbrg
																																																																																																 AND
																																																																																																 kartustok.idjnskartustok IN (14, 15)
																																																																																																 AND
																																																																																																 kartustok.idbagian = bbagian.idbagian
																																																																																																 AND
																																																																																																 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																															   ORDER BY
																																																																																																 kartustok.kdbrg DESC), 0)) + ifnull((SELECT sum(kartustok.saldoawal) AS sa
																																																																																																									  FROM
																																																																																																										kartustok
																																																																																																									  WHERE
																																																																																																										kartustok.kdbrg = barang.kdbrg
																																																																																																										AND
																																																																																																										kartustok.idbagian = bbagian.idbagian
																																																																																																										AND
																																																																																																										kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
																																																																																																									  ORDER BY
																																																																																																										kartustok.kdbrg DESC
																																																																																																									  LIMIT
																																																																																																										1), 0))) AS sa
							   FROM
								 kartustok
							   WHERE
								 kartustok.kdbrg = barang.kdbrg
								 AND
								 kartustok.idbagian = bbagian.idbagian
								 AND
								 kartustok.tglkartustok BETWEEN '".$tglawal."' AND '".$tglakhir."'
							   ORDER BY
								 kartustok.kdbrg DESC
							   LIMIT
								 1), 0)  * ifnull((SELECT b.hrgjual AS hjual
												  FROM
													barang b
												  WHERE
													b.kdbrg = barang.kdbrg), 0) AS hrgjual
				
				FROM
				  barangbagian bbagian
				LEFT JOIN barang
				ON bbagian.kdbrg = barang.kdbrg
				WHERE
				  barang.idstatus <> '0'
				  AND
				  bbagian.idbagian = '".$idbagian."'
				GROUP BY
				  barang.kdbrg
				ORDER BY
				  barang.nmbrg");
				
		$query  = $this->db->query($q);
        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }        
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
        echo json_encode($build_array);
    }
	
	function get_pengbagian(){		
		$userid 				= $this->input->post("userid");
      
        $this->db->select("penggunabagian.idbagian, bagian.nmbagian, pengguna.userid, pengguna.nmlengkap");
        $this->db->from("penggunabagian");		
		$this->db->join('bagian',
                'bagian.idbagian = penggunabagian.idbagian', 'left');
		$this->db->join('pengguna',
                'pengguna.userid = penggunabagian.userid', 'left');
		
		$this->db->order_by('penggunabagian.idbagian DESC');
		
        $where = array();
		$where['penggunabagian.userid']=$userid;
		$this->db->where($where);
		
		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}
