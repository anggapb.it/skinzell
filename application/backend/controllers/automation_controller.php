<?php

class Automation_controller extends Controller {
  public function __construct()
  {
    parent::Controller();
    $this->load->library('session');
    $this->load->library('rhlib');
  }
  
  /* ============================= TRANSAKSI SUPPLIER ============================= */
  function posting_transaksi_beli_supplier(){
    $tglawal = $this->input->post('tglawal');
    $tglakhir = $this->input->post('tglakhir');

    if(empty($tglawal) || empty($tglakhir)) return false;
    
    $this->db->select("*");
    $query = "SELECT 
      `po`.`nopo` AS `nopo`
       , `po`.`tglpo` AS `tglpo`
       , `po`.`kdsupplier` AS `kdsupplier`
       , `po`.`idstsetuju` AS `idstsetuju`
       , ifnull(`jurnal`.`kdjurnal`, '-') AS `kdjurnal`
       , (SELECT sum(truncate((((`podet`.`hargabeli` * `podet`.`qty`) * ((100 - `podet`.`diskon`) / 100)) * ((`podet`.`ppn` + 100) / 100)), 0)) AS `sum3`
          FROM
            `podet`
          WHERE
            (`podet`.`nopo` = `po`.`nopo`)) AS `total_bayar`
       , `supplier`.`nmsupplier` AS `nmsupplier`
       FROM
        ((`po`
       LEFT JOIN `supplier`
       ON ((`po`.`kdsupplier` = `supplier`.`kdsupplier`)))
       LEFT JOIN `jurnal`
       ON (((`po`.`nopo` = `jurnal`.`noreff`) AND (`jurnal`.`idjnstransaksi` = 1))))
       HAVING
       tglpo >= '".$tglawal."' AND
       tglpo <= '".$tglakhir."' AND
       kdjurnal =  '-'
    ";
    $q = $this->db->query($query);  
    $ttl = $q->num_rows();
    $data = ($ttl > 0) ? $q->result() : array();
    
    if(!empty($data)){
      foreach($data as $idx => $dt){
        $nopo  = $dt->nopo;
        $tglpo_posting  = $dt->tglpo;
        $nmsupplier  = $dt->nmsupplier;
        $nominal_jurnal  = $dt->total_bayar;
        $userid    = 'admin';
        $kdjurnal = $this->get_nojurnal_khusus();    
        $post_grid_persediaan = $this->get_jtransaksi_supplier_beli_jurnaling_persediaan($dt->nopo);
        $post_grid_biaya = $this->get_jtransaksi_supplier_beli_jurnaling_biaya($dt->nopo);

        $dataJurnal = array(
          'kdjurnal' => $kdjurnal,
          'idjnsjurnal' => 2, // jurnal khusus
          'tgltransaksi' => $tglpo_posting,
          'tgljurnal' => date('Y-m-d'),
          'keterangan' => 'Pembelian Pada: '.$nmsupplier,
          'noreff' => $nopo,
          'userid' => $userid,
          'nominal' => $nominal_jurnal,
          'idjnstransaksi' => 1, // 1 = pembelian obat supplier
          'status_posting' => 1,
        );

        $continue = true;
        $this->db->trans_start();
    
        //insert jurnal
        if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
    
        //insert jurnal det persediaan
        if(!empty($post_grid_persediaan) AND $continue === true)
        {
          foreach($post_grid_persediaan as $idx => $item_jdet)
          {
            if(isset($item_jdet['debit']) && isset($item_jdet['kredit']))
            {
              $ins_jdet = array(
                'kdjurnal' => $kdjurnal,
                'tahun' => date('Y'),
                'idakun' => $item_jdet['idakun'],  
                'noreff' => $item_jdet['noreff'],
                'debit' => $item_jdet['debit'],
                'kredit' => $item_jdet['kredit'],
                'klpjurnal' => '3', //3 = jurnal persediaan
              );
              
              if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;
            }

          }
        }

        //insert jurnal det biaya
        if(!empty($post_grid_biaya) AND $continue === true)
        {
          foreach($post_grid_biaya as $idx => $item_jdet)
          {
            if(isset($item_jdet['debit']) && isset($item_jdet['kredit'])){
              $ins_jdet = array(
                'kdjurnal' => $kdjurnal,
                'tahun' => date('Y'),
                'idakun' => $item_jdet['idakun'],  
                'noreff' => $item_jdet['noreff'],
                'debit' => $item_jdet['debit'],
                'kredit' => $item_jdet['kredit'],
                'klpjurnal' => '2', //2 = jurnal biaya
              );
              
              if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;
            }

          }
        }

        if($continue)
          $this->db->trans_complete();
        else
          $this->db->trans_rollback();
  
      }
    }

    $return = array('success' => true, 'message' => 'berhasil memposting jurnal');
    echo json_encode($return);die;

  }


  function get_jtransaksi_supplier_beli_jurnaling_persediaan($nopo = ''){
    $akun_hutang = $this->db->get_where('akun', array('kdakun' => '21200'))->row_array();
    $akun_diskon = $this->db->get_where('akun', array('kdakun' => '51500'))->row_array();
    
    if(empty($nopo)) $nopo = '0';

    $this->db->select("
      nopo
     , sum(totbeli_diskon) AS totbeli_diskon
     , sum(totbeli + totbonus) AS totbeli_bonus
     , sum(totdiskon + totbonus) AS bonus_diskon
     , idakunbeli
     , kdakunbeli
     , nmakunbeli
     , kdsupplier
    ");
    $this->db->from("v_jurn_podet");
    $this->db->where("nopo", $nopo);
    $this->db->group_by('kdakunbeli');
    $data = $this->db->get()->result_array();

    $total_hutang = 0;
    $total_diskon = 0;
    $breakdown_jurnal = array();    
    if(!empty($data))
    {
      foreach($data as $idx => $dt)
      {
        $total_hutang += $dt['totbeli_diskon']; //total beli sudah dikurangi diskon
        if($dt['bonus_diskon'] > 0) $total_diskon += $dt['bonus_diskon']; 
        
        $breakdown_jurnal[] = array(
          'idakun' => $dt['idakunbeli'], 
          'kdakun' => $dt['kdakunbeli'], 
          'nmakun' => $dt['nmakunbeli'], 
          'noreff' => '', 
          'debit' => $dt['totbeli_bonus'], 
          'kredit'=> 0
        );
      }

      if($total_diskon > 0){
        //tambah akun diskon
        $breakdown_jurnal[] = array(
          'idakun' => $akun_diskon['idakun'], 
          'kdakun' => $akun_diskon['kdakun'], 
          'nmakun' => $akun_diskon['nmakun'], 
          'noreff' => '', 
          'debit' => 0, 
          'kredit'=> $total_diskon
        );        
      }

      //tambah akun hutang
      $breakdown_jurnal[] = array(
        'idakun' => $akun_hutang['idakun'], 
        'kdakun' => $akun_hutang['kdakun'], 
        'nmakun' => $akun_hutang['nmakun'], 
        'noreff' => $dt['kdsupplier'], 
        'debit' => 0, 
        'kredit'=> $total_hutang
      );
    }

    return $breakdown_jurnal;
  }
  
  function get_jtransaksi_supplier_beli_jurnaling_biaya($nopo = ''){
    $akun_hutang_ppn = $this->db->get_where('akun', array('kdakun' => '21610'))->row_array();
    $akun_ppn = $this->db->get_where('akun', array('kdakun' => '51710'))->row_array();

    if(empty($nopo)) $nopo = '0';

    $this->db->select("
      nopo
     , sum(beban_ppn) AS beban_ppn
     , idakunbeli
     , kdakunbeli
     , nmakunbeli
     , kdsupplier
    ");
    $this->db->from("v_jurn_podet");
    $this->db->where("nopo", $nopo);
    $this->db->group_by('kdakunbeli');
    $data = $this->db->get()->result_array();

    $total_ppn = 0;
    $breakdown_jurnal = array();    
    if(!empty($data))
    {
      foreach($data as $idx => $dt)
      {
        if($dt['beban_ppn'] > 0) $total_ppn += $dt['beban_ppn'];
      }

      if($total_ppn > 0){

        $breakdown_jurnal[] = array(
          'idakun' => $akun_ppn['idakun'], 
          'kdakun' => $akun_ppn['kdakun'], 
          'nmakun' => $akun_ppn['nmakun'], 
          'noreff' => '', 
          'debit' => $total_ppn, 
          'kredit'=> 0
        );

        $breakdown_jurnal[] = array(
          'idakun' => $akun_hutang_ppn['idakun'], 
          'kdakun' => $akun_hutang_ppn['kdakun'], 
          'nmakun' => $akun_hutang_ppn['nmakun'], 
          'noreff' => '', 
          'debit'  => 0,
          'kredit' => $total_ppn
        );

      }

    }

    return $breakdown_jurnal;
  }


  /* ============================= TRANSAKSI PASIEN ============================= */

  function posting_transaksi_pasien(){
    $tglawal = $this->input->post('tglawal');
    $tglakhir = $this->input->post('tglakhir');

    if(empty($tglawal) || empty($tglakhir)) return false;

    $main_query = "
      SELECT `kuitansi`.`nokuitansi` AS `nokuitansi`
       , `kuitansi`.`tglkuitansi` AS `tglkuitansi`
       , `kuitansi`.`idstkuitansi` AS `idstkuitansi`
       , `kuitansi`.`atasnama` AS `atasnama`
       , `kuitansi`.`idjnskuitansi` AS `idjnskuitansi`
       , `kuitansi`.`total` AS `total`
       , `kuitansi`.`idregdet` AS `idregdet`
       , `kuitansi`.`pembayaran` AS `pembayaran`
       , ifnull((SELECT `jurnal`.`kdjurnal` AS `kdjurnal`
                 FROM
                   `jurnal`
                 WHERE
                   ((`kuitansi`.`nokuitansi` = `jurnal`.`noreff`)
                   AND (`jurnal`.`idjnsjurnal` = 2)
                   AND ((`jurnal`.`idjnstransaksi` = 6)
                   OR (`jurnal`.`idjnstransaksi` = 7)
                   OR (`jurnal`.`idjnstransaksi` = 8)
                   OR (`jurnal`.`idjnstransaksi` = 10)
                   OR (`jurnal`.`idjnstransaksi` = 11)))), '-') AS `kdjurnal`
       , if(isnull((SELECT `jurnal`.`kdjurnal` AS `kdjurnal`
                    FROM
                      `jurnal`
                    WHERE
                      ((`kuitansi`.`nokuitansi` = `jurnal`.`noreff`)
                      AND (`jurnal`.`idjnsjurnal` = 2)
                      AND ((`jurnal`.`idjnstransaksi` = 6)
                      OR (`jurnal`.`idjnstransaksi` = 7)
                      OR (`jurnal`.`idjnstransaksi` = 8)
                      OR (`jurnal`.`idjnstransaksi` = 10)
                      OR (`jurnal`.`idjnstransaksi` = 11))))), 'belum', 'sudah') AS `status_posting`
       , if(isnull((SELECT `jurnal`.`kdjurnal` AS `kdjurnal`
                    FROM
                      `jurnal`
                    WHERE
                      ((`kuitansi`.`nokuitansi` = `jurnal`.`noreff`)
                      AND (`jurnal`.`idjnsjurnal` = 2)
                      AND ((`jurnal`.`idjnstransaksi` = 6)
                      OR (`jurnal`.`idjnstransaksi` = 7)
                      OR (`jurnal`.`idjnstransaksi` = 8)
                      OR (`jurnal`.`idjnstransaksi` = 10)
                      OR (`jurnal`.`idjnstransaksi` = 11))))), false, true) AS `status_posting2`
       , `nota`.`nonota` AS `nonota`
       , (SELECT `registrasidet`.`noreg` AS `noreg`
          FROM
            `registrasidet`
          WHERE
            (`registrasidet`.`idregdet` = `nota`.`idregdet`)) AS `noreg`
       , if((`kuitansi`.`idjnskuitansi` = 2), 'Rawat Inap', if((`kuitansi`.`idjnskuitansi` = 4), 'UGD', if(((`kuitansi`.`idjnskuitansi` = 1) AND (`nota`.`idbagian` = 11)), 'Farmasi Pasien Luar', if(((`kuitansi`.`idjnskuitansi` = 1) AND (`nota`.`idbagian` = 35)), 'Pelayanan Tambahan', 'Rawat Jalan')))) AS `jenis_transaksi`
       , if((`kuitansi`.`idjnskuitansi` = 2), '7', if((`kuitansi`.`idjnskuitansi` = 4), '8', if(((`kuitansi`.`idjnskuitansi` = 1) AND (`nota`.`idbagian` = 11)), '10', if(((`kuitansi`.`idjnskuitansi` = 1) AND (`nota`.`idbagian` = 35)), '11', '6')))) AS `jtransjurnal`
      FROM
      (`kuitansi`
      LEFT JOIN `nota`
      ON ((`kuitansi`.`nokuitansi` = `nota`.`nokuitansi`)))
      WHERE
        `kuitansi`.`idstkuitansi` = 1 AND 
        (
          `kuitansi`.`idjnskuitansi` = 1 OR 
          `kuitansi`.`idjnskuitansi` = 2 OR 
          `kuitansi`.`idjnskuitansi` = 4
        ) AND
        tglkuitansi >= '".$tglawal."' AND
        tglkuitansi <= '".$tglakhir."'
      GROUP BY
        `kuitansi`.`nokuitansi`
        HAVING 
          status_posting = 'belum'
      ";

    $q = $this->db->query($main_query);  
    $ttl = $q->num_rows();

    $data = ($ttl > 0) ? $q->result() : array();
    if(!empty($data)){
      foreach($data as $idx => $dt){
        
        $nokuitansi  = $dt->nokuitansi;
        $tglkuitansi_posting  = $dt->tglkuitansi;
        $jtransaksi_posting  = $dt->jenis_transaksi;
        $idjtransaksi = $dt->jtransjurnal;
        $atasnama  = $dt->atasnama;
        $nominal_jurnal  = $dt->total;
        $post_grid  = $this->get_jtransaksi_pasien_pendapatan($dt->nokuitansi);
        $post_grid_persediaan  = $this->get_jtransaksi_pasien_persediaan($dt->nokuitansi);
        $balance = true;

        //check apakah post_grid balance
        $debit_grid = 0;
        $kredit_grid = 0;
        if(!empty($post_grid)){
          foreach($post_grid as $idxpg => $pg){

            $debit_grid += $pg['debit'];
            $kredit_grid += $pg['kredit'];
          }          
        }
        if($debit_grid != $kredit_grid) $balance = false;

        //check apakah post_grid_persediaan balance
        $debit_grid = 0;
        $kredit_grid = 0;
        if(!empty($post_grid_persediaan)){
          foreach($post_grid_persediaan as $idxpg => $pg){
            $debit_grid += $pg['debit'];
            $kredit_grid += $pg['kredit'];
          }          
        }
        if($debit_grid != $kredit_grid) $balance = false;

        if($balance)
        {
          $userid    = 'admin';
          $kdjurnal = $this->get_nojurnal_khusus($tglkuitansi_posting);
          $dataJurnal = array(
            'kdjurnal' => $kdjurnal,
            'idjnsjurnal' => 2, // jurnal khusus
            'tgltransaksi' => $tglkuitansi_posting,
            'tgljurnal' => date('Y-m-d'),
            'keterangan' => 'Transaksi '. $jtransaksi_posting .': '.$atasnama,
            'noreff' => $nokuitansi,
            'userid' => $userid,
            'nominal' => $nominal_jurnal,
            'idjnstransaksi' => $idjtransaksi,
            'status_posting' => 1,
          );

          $continue = true;
          $this->db->trans_start();
          
          //insert jurnal
          if( ! $this->db->insert('jurnal', $dataJurnal) ) $continue = false;
      
          //jurnal pendapatan
          if(!empty($post_grid) AND $continue === true)
          {
            foreach($post_grid as $idx => $item_jdet)
            {
              if(isset($item_jdet['noreff'])){
                $ins_jdet = array(
                  'kdjurnal' => $kdjurnal,
                  'tahun' => date('Y'),
                  'idakun' => $item_jdet['idakun'],  
                  'noreff' => $item_jdet['noreff'],
                  'debit' => $item_jdet['debit'],
                  'kredit' => $item_jdet['kredit'],
                  'klpjurnal' => '1', // 1 = jurnal pendapatan
                );
                
                if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
              }

            }
          }

          //jurnal persediaan
          if(!empty($post_grid_persediaan) AND $continue === true)
          {
            foreach($post_grid_persediaan as $idx => $item_jdet)
            {
              /*
                item_jdet[0] = idakun   
                item_jdet[1] = kdakun 
                item_jdet[2] = nmakun 
                item_jdet[3] = noreff 
                item_jdet[4] = debit 
                item_jdet[5] = kredit
              */
              if(isset($item_jdet['noreff'])){
                $ins_jdet = array(
                  'kdjurnal' => $kdjurnal,
                  'tahun' => date('Y'),
                  'idakun' => $item_jdet['idakun'],  
                  'noreff' => $item_jdet['noreff'],
                  'debit' => $item_jdet['debit'],
                  'kredit' => $item_jdet['kredit'],
                  'klpjurnal' => '3', // 3 = jurnal persediaan
                );
                
                if( ! $this->db->insert('jurnaldet', $ins_jdet) ) $continue = false;          
              }

            }
          }

          if($continue)
            $this->db->trans_complete();
          else
            $this->db->trans_rollback();

        }

      }
    }

  }


  function get_jtransaksi_pasien_pendapatan($nokuitansi = ''){
    $akun_deposit = $this->db->get_where('akun', array('kdakun' => '21760'))->row_array();
    $akun_profesi = $this->db->get_where('akun', array('kdakun' => '41103'))->row_array();
    $akun_diskon_js = $this->db->get_where('akun', array('kdakun' => '41260'))->row_array();
    $akun_diskon_jm = $this->db->get_where('akun', array('kdakun' => '41250'))->row_array();
    $akun_diskon_jp = $this->db->get_where('akun', array('kdakun' => '41270'))->row_array();
    $akun_diskon_bhp = $this->db->get_where('akun', array('kdakun' => '41280'))->row_array();
    
    if(empty($nokuitansi)) $nokuitansi = '0';

    $dafakun  = array();    
    $breakdown_jurnal = array();

    $get_akun_bayar = $this->db->query("SELECT 
      ifnull(`kuitansidet`.`idbank`, '') AS `idbank`
      , sum(`kuitansidet`.`jumlah`) AS `jumlah`
      , (SELECT if(((`carabayar`.`kdakun` IS NOT NULL) AND (`carabayar`.`kdakun` <> '')), `carabayar`.`kdakun`, 
          if((`carabayar`.`idcarabayar` = '2'), ifnull((SELECT `bank`.`kdakun` AS `kdakun` FROM `bank` 
          WHERE (`bank`.`idbank` = `kuitansidet`.`idbank`)), '11000'), '11000')) AS `select1`) AS `kdakun`
      FROM
      (`kuitansidet` LEFT JOIN `carabayar`
      ON (`kuitansidet`.`idcarabayar` = `carabayar`.`idcarabayar`))
      WHERE nokuitansi = '".$nokuitansi."'
      GROUP BY kdakun,idbank
    ")->result_array();

    if(!empty($get_akun_bayar)){
      foreach($get_akun_bayar as $akunbayar)
      {
        $gt_akun = $this->db->get_where('akun', array('kdakun' => $akunbayar['kdakun']))->row_array();
        $breakdown_jurnal[] = array(
          'idakun' => $gt_akun['idakun'], 
          'kdakun' => $gt_akun['kdakun'], 
          'nmakun' => $gt_akun['nmakun'], 
          'noreff' => $akunbayar['idbank'], 
          'debit' => $akunbayar['jumlah'], 
          'kredit'=> 0
        );
      }
    }

    //check apakah ada deposit yang memiliki regdet yang sama
    $get_nota = $this->db->get_where('nota', array('nokuitansi' => $nokuitansi))->row_array();
    if(!empty($get_nota))
    {

       $total_deposit = 0;

      //cari nomor registrasi dari nota
      $get_registrasi = $this->db->get_where('registrasidet', array('idregdet' => $get_nota['idregdet']))->row_array();
      if(!empty($get_registrasi)){
        $noreg = $get_registrasi['noreg'];

        //hitung total kuitansi yang memiliki jenis kuitansi deposit & noreg = $noreg
        $get_total_deposit = $this->db->query("
          SELECT sum(kuitansi.total) AS deposit
          FROM
            (kuitansi
          JOIN registrasidet)
          WHERE
            kuitansi.idregdet = registrasidet.idregdet
            AND kuitansi.idjnskuitansi = 5
            AND kuitansi.idstkuitansi = 1
            AND registrasidet.noreg = '".$noreg."'")->row_array();
        $total_deposit = $get_total_deposit['deposit'];

      }

      if($total_deposit > 0)
      {
        $q_totaltagihan = $this->db->query("
          SELECT sum(
            (qty * (tarifjs + tarifjm + tarifjp + tarifbhp) ) - 
            (`notadet`.`diskonjs` + `notadet`.`diskonjm` + `notadet`.`diskonjp` + `notadet`.`diskonbhp`)
          )  AS tot_tagihan
          FROM (notadet left join nota on (notadet.nonota = nota.nonota))
          WHERE nota.nokuitansi = '".$nokuitansi."'
        ")->row_array();

        //jika total deposit > total tagihan. deposit diperkecil sampai pas dengan tagihan
        if($total_deposit > $q_totaltagihan['tot_tagihan']){
          $total_deposit = $q_totaltagihan['tot_tagihan'];
        }
        
        $breakdown_jurnal[] = array(
          'idakun' => $akun_deposit['idakun'], 
          'kdakun' => $akun_deposit['kdakun'], 
          'nmakun' => $akun_deposit['nmakun'], 
          'noreff' => '', 
          'debit' => $total_deposit, 
          'kredit'=> 0
        );
      }

    }

    //get akun kredit obat (pendapatan obat / alkes)
    $get_pendapatan_obat = $this->db->query("SELECT 
      sum(`notadet`.`tarifbhp` * `notadet`.`qty`) AS `jumlah`
      , `setakunklpbarang`.`idakunjual` AS `idakun`
      , `akun`.`kdakun` AS `kdakun`
      , `akun`.`nmakun` AS `nmakun`
      FROM
      (((((`notadet`
      LEFT JOIN `nota` ON ((`notadet`.`nonota` = `nota`.`nonota`)))
      LEFT JOIN `barang` ON ((`barang`.`kdbrg` = `notadet`.`kditem`)))
      LEFT JOIN `setakunklpbarang` ON (((`setakunklpbarang`.`idklpbrg` = `barang`.`idklpbrg`) AND (`setakunklpbarang`.`tahun` = year(`nota`.`tglnota`)))))
      LEFT JOIN `akun` ON ((`setakunklpbarang`.`idakunjual` = `akun`.`idakun`)))
      LEFT JOIN `akun` `akunbeli` ON ((`setakunklpbarang`.`idakun` = `akunbeli`.`idakun`)))
      WHERE
      `nota`.`idsttransaksi` = 1 AND 
      (left(`notadet`.`kditem`, 1) = 'B') AND 
      nokuitansi = '".$nokuitansi."' GROUP BY kdakun
    ")->result_array();

    if(!empty($get_pendapatan_obat)){
      foreach($get_pendapatan_obat as $pendapatan_obat)
      {
        $dafakun[$pendapatan_obat['kdakun'] .'_'] = array(
          'idakun' => $pendapatan_obat['idakun'], 
          'nmakun' => $pendapatan_obat['nmakun'], 
          'idreff' => '', 
          'noreff' => '', 
          'jumlah'=> $pendapatan_obat['jumlah']
        );
      }
    }


    //get akun kredit pelayanan
    $get_pendapatan_pelayanan = $this->db->query("SELECT 
      `notadet`.`idnotadet` AS `idnotadet`
       , `nota`.`nokuitansi` AS `nokuitansi`
       , `notadet`.`nonota` AS `nonota`
       , `notadet`.`kditem` AS `kditem`
       , `notadet`.`qty` AS `qty`
       , `notadet`.`tarifjs` AS `tarifjs`
       , `notadet`.`tarifjm` AS `tarifjm`
       , `notadet`.`tarifjp` AS `tarifjp`
       , `notadet`.`tarifbhp` AS `tarifbhp`
       , (`notadet`.`tarifjs` * `notadet`.`qty`) AS `totaljs`
       , (`notadet`.`tarifjm` * `notadet`.`qty`) AS `totaljm`
       , (`notadet`.`tarifjp` * `notadet`.`qty`) AS `totaljp`
       , (`notadet`.`tarifbhp` * `notadet`.`qty`) AS `totalbhp`
       , `notadet`.`diskonjs` AS `diskonjs`
       , `notadet`.`diskonjm` AS `diskonjm`
       , `notadet`.`diskonjp` AS `diskonjp`
       , `notadet`.`diskonbhp` AS `diskonbhp`
       , `notadet`.`hrgjual` AS `hrgjual`
       , `notadet`.`hrgbeli` AS `hrgbeli`
       , `notadet`.`iddokter` AS `iddokter`
       , `notadet`.`idperawat` AS `idperawat`
       , `notadet`.`dijamin` AS `dijamin`
       , `setakunpelayanan`.`kdakunjs` AS `kdakunjs`
       , `setakunpelayanan`.`kdakunjm` AS `kdakunjm`
       , `setakunpelayanan`.`kdakunjp` AS `kdakunjp`
       , `setakunpelayanan`.`kdakunbhp` AS `kdakunbhp`
      FROM
        ((`notadet`
      LEFT JOIN `nota`
      ON ((`notadet`.`nonota` = `nota`.`nonota`)))
      LEFT JOIN `setakunpelayanan`
      ON (((`setakunpelayanan`.`kdpelayanan` = `notadet`.`kditem`) AND (`setakunpelayanan`.`tahun` = year(`nota`.`tglnota`)))))
      WHERE
        `nota`.`idsttransaksi` = 1 AND 
        (left(`notadet`.`kditem`, 1) = 'T') AND
        nokuitansi = '".$nokuitansi."'
    ")->result_array();
    
    if(!empty($get_pendapatan_pelayanan)){
      foreach($get_pendapatan_pelayanan as $pendapatan_pelayanan)
      {
        //jasa sarana
        if($pendapatan_pelayanan['totaljs'] > 0){
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjs'] .'_']))
          {
            $dafakun[$pendapatan_pelayanan['kdakunjs'] .'_']['jumlah'] += $pendapatan_pelayanan['totaljs'];
          }else{
            $getakunjs = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjs']))->row_array();
            if(!empty($getakunjs)){
              $dafakun[$pendapatan_pelayanan['kdakunjs'] .'_'] = array(
                'jumlah' => $pendapatan_pelayanan['totaljs'],
                'idakun' => $getakunjs['idakun'],
                'nmakun' => $getakunjs['nmakun'],
                'idnoreff' => '',
                'noreff' => '',
              );
            }
          }
        }

        //jasa medis
        if($pendapatan_pelayanan['totaljm'] > 0){
          //jika akun jasa profesi sudah ada & id dokternya sama maka tambah nominal
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjm'] .'_'. $pendapatan_pelayanan['iddokter']]) AND 
             $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']]['idnoreff'] == $pendapatan_pelayanan['iddokter'])
          {
            $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']]['jumlah'] += $pendapatan_pelayanan['totaljm'];
          }else{
            $getakunjm = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjm']))->row_array();
            $getdokter = $this->db->get_where('dokter', array('iddokter' => $pendapatan_pelayanan['iddokter']))->row_array();
            if(!empty($getakunjm)){
              $dafakun[$pendapatan_pelayanan['kdakunjm'].'_'. $pendapatan_pelayanan['iddokter']] = array(
                'jumlah' => $pendapatan_pelayanan['totaljm'],
                'idakun' => $getakunjm['idakun'],
                'nmakun' => $getakunjm['nmakun'],
                'idnoreff' => $pendapatan_pelayanan['iddokter'],
                'noreff' => (isset($getdokter['kddokter'])) ? $getdokter['kddokter'] : '',
              );
            }
          }
        }

        //jasa profesi
        if($pendapatan_pelayanan['totaljp'] > 0){
          //jika akun jasa profesi sudah ada & id tenaga medis sama maka tambah nominal
          if(isset($dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]) AND 
             $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]['idnoreff'] == $pendapatan_pelayanan['idperawat'])
          {
            $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']]['jumlah'] += $pendapatan_pelayanan['totaljp'];
          }else{
            $getakunjp = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunjp']))->row_array();
            $getperawat = $this->db->get_where('dokter', array('iddokter' => $pendapatan_pelayanan['idperawat']))->row_array();
            if(!empty($getakunjp)){
              $dafakun[$pendapatan_pelayanan['kdakunjp'] .'_'. $pendapatan_pelayanan['idperawat']] = array(
                'jumlah' => $pendapatan_pelayanan['totaljp'],
                'idakun' => $getakunjp['idakun'],
                'nmakun' => $getakunjp['nmakun'],
                'idnoreff' => $pendapatan_pelayanan['idperawat'],
                'noreff' => (isset($getperawat['kddokter'])) ? $getperawat['kddokter'] : '',
              );
            }
          }
        }

        //BHP
        if($pendapatan_pelayanan['totalbhp'] > 0){
          if(isset($dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_']))
          {
            $dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_']['jumlah'] += $pendapatan_pelayanan['totalbhp'];
          }else{
            $getakunbhp = $this->db->get_where('akun', array('kdakun' => $pendapatan_pelayanan['kdakunbhp']))->row_array();
            if(!empty($getakunbhp)){
              $dafakun[$pendapatan_pelayanan['kdakunbhp'] .'_'] = array(
                'jumlah' => $pendapatan_pelayanan['totalbhp'],
                'idakun' => $getakunbhp['idakun'],
                'nmakun' => $getakunbhp['nmakun'],
                'idnoreff' => '',
                'noreff' => '',
              );
            }
          }
        }
      }
    }

    if(!empty($dafakun))
    {
      foreach($dafakun as $kdakun => $dtjurnal)
      {
        $kdakun_exp = explode('_', $kdakun);

        $breakdown_jurnal[] = array(
          'idakun' => $dtjurnal['idakun'], 
          'kdakun' => $kdakun_exp[0], 
          'nmakun' => $dtjurnal['nmakun'], 
          'noreff' => $dtjurnal['noreff'], 
          'debit'  => 0, 
          'kredit' => $dtjurnal['jumlah']
        );
      }
    }

    //get pendapatan jasa profesi farmasi (uang racik) & diskon barang dagangan
    $diskon_barang = 0;
    $q_nota = $this->db->query("
      SELECT 
      SUM(uangr) as tot_uangr,
      diskon as diskon_obat from nota where nokuitansi = '".$nokuitansi."'
    ")->row_array();

    $diskon_barang += $q_nota['diskon_obat'];

    $q_notadet = $this->db->query("
      SELECT 
        sum(`notadet`.`diskonjs`) AS `diskonjs`
        , sum(`notadet`.`diskonjm`) AS `diskonjm`
        , sum(`notadet`.`diskonjp`) AS `diskonjp`
        , sum(`notadet`.`diskonbhp`) AS `diskonbhp`
        , sum(`notadet`.`dijamin`) AS `tot_dijamin`
      FROM
      (
        (`nota` JOIN `notadet`) 
        LEFT JOIN `registrasidet` 
        ON ((`nota`.`idregdet` = `registrasidet`.`idregdet`))
      ) 
      WHERE 
      (
        (`nota`.`nokuitansi` = '".$nokuitansi."') AND 
        (`nota`.`idsttransaksi` = 1) AND 
        (`nota`.`nonota` = `notadet`.`nonota`)
      )
    ")->row_array();

    $diskon_barang += $q_notadet['diskonbhp'];
    $diskonjs = $q_notadet['diskonjs'];
    $diskonjm = $q_notadet['diskonjm'];
    $diskonjp = $q_notadet['diskonjp'];

    if($q_nota['tot_uangr'] > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_profesi['idakun'], 
        'kdakun' => $akun_profesi['kdakun'], 
        'nmakun' => $akun_profesi['nmakun'], 
        'noreff' => '0306', 
        'debit'  => 0, 
        'kredit' => $q_nota['tot_uangr']
      );
    }

    if($diskon_barang > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_bhp['idakun'], 
        'kdakun' => $akun_diskon_bhp['kdakun'], 
        'nmakun' => $akun_diskon_bhp['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskon_barang, 
        'kredit' => 0
      );
    }

    if($diskonjs > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_js['idakun'], 
        'kdakun' => $akun_diskon_js['kdakun'], 
        'nmakun' => $akun_diskon_js['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjs, 
        'kredit' => 0
      );
    }

    if($diskonjm > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_jm['idakun'], 
        'kdakun' => $akun_diskon_jm['kdakun'], 
        'nmakun' => $akun_diskon_jm['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjm, 
        'kredit' => 0
      );
    }

    if($diskonjp > 0)
    {
      $breakdown_jurnal[] = array(
        'idakun' => $akun_diskon_jp['idakun'], 
        'kdakun' => $akun_diskon_jp['kdakun'], 
        'nmakun' => $akun_diskon_jp['nmakun'], 
        'noreff' => '', 
        'debit'  => $diskonjp, 
        'kredit' => 0
      );
    }

    return $breakdown_jurnal;

  }

  function get_jtransaksi_pasien_persediaan($nokuitansi = ''){
    if(empty($nokuitansi)) $nokuitansi = '0';

    $dafakun  = array();    
    $breakdown_jurnal = array();

    $get_akun_persediaan = $this->db->query("
      SELECT 
        sum(hrgjual * qty) as penjualanbarang,
        sum(hrgbeli * qty) as nominalpersediaan, 
        idakun as idakunjual, 
        kdakun as kdakunjual, 
        nmakun as nmakunjual,
        idakunbeli as idakunbeli, 
        kdakunbeli as kdakunbeli, 
        nmakunbeli as nmakunbeli
      FROM v_jurn_trans_rjugd_akunbrg
      WHERE nokuitansi = '".$nokuitansi."'
      GROUP BY kdakun")->result_array();
    
    if(!empty($get_akun_persediaan)){
      foreach($get_akun_persediaan as $akun_persediaan)
      {
        //pendapatan barang
        $breakdown_jurnal[] = array(
          'idakun' => $akun_persediaan['idakunjual'], 
          'kdakun' => $akun_persediaan['kdakunjual'], 
          'nmakun' => $akun_persediaan['nmakunjual'], 
          'noreff' => '', 
          'debit' => $akun_persediaan['nominalpersediaan'], 
          'kredit'=> 0
        );

        //pengurangan stok
        $breakdown_jurnal[] = array(
          'idakun' => $akun_persediaan['idakunbeli'], 
          'kdakun' => $akun_persediaan['kdakunbeli'], 
          'nmakun' => $akun_persediaan['nmakunbeli'], 
          'noreff' => '', 
          'debit' => 0,
          'kredit'=> $akun_persediaan['nominalpersediaan']
        );
      }
    }

    return $breakdown_jurnal;
  }



  /* lain lain */

  function get_nojurnal_khusus($date = ''){

    if(empty($date))
      $q = "SELECT getOtoNoJurnalKhusus(now()) as nm;";
    else
      $q = "SELECT getOtoNoJurnalKhusus('".$date."') as nm;";
      
    $query  = $this->db->query($q);
    $nm= ''; 
                
    if ($query->num_rows() != 0)
    {
        $row = $query->row();
        $nm=$row->nm;
    }
    return $nm;
  }
}
