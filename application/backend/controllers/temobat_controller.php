<?php

class Temobat_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_temobat(){
        $start                = $this->input->post("start");
        $limit                = $this->input->post("limit");
        
        $fields               = $this->input->post("fields");
        $query                = $this->input->post("query");
		$idtempobat           = $this->input->post("idtempobat");
				
		$this->db->select("*");
		$this->db->from("v_temobatdet");	
		$this->db->where("idstatus", 1);
		
		$where = array();
        $where['idtempobat']=$idtempobat;
        
        $this->db->where($where);
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function delete_temobat(){     
		$where['iditemobattemp'] = $_POST['iditemobattemp'];
		$del = $this->rhlib->deleteRecord('templateobatdet',$where);
        return $del;
    }
		
	function delete_win_Barang(){     
		$where['idtempobat'] = $_POST['idtempobat'];
		$where['kdbrg'] = $_POST['kdbrg'];
		$del = $this->rhlib->deleteRecord('templateobatdet',$where);
        return $del;
    }
	
	function insert_win_barang(){
		$dataArray = $this->getFieldsAndValuesBarang();
		$ret = $this->rhlib->insertRecord('templateobatdet',$dataArray);
		return $ret;
    }
	
	function update_qtyitem(){
		//UPDATE
		$this->db->where('iditemobattemp', $_POST['iditemobattemp']);
		$this->db->set('qtyitem', $_POST['qtyitem']);
		$this->db->update('templateobatdet'); 
        
		if($this->db->affected_rows()){
            $ret["success"]=true;
            $ret["msg"]='Update Data Berhasil';
        }else{
            $ret["success"]=false;
            $ret["msg"]= 'Update Data Gagal';
        }
        return $ret;
	}	
		
	function getFieldsAndValuesBarang(){		
		$dataArray = array(
		     'idtempobat'		=> $_POST['idtempobat'],
		     'kdbrg'			=> $_POST['kdbrg'],
			 'qtyitem'			=> 0,
        );
		
		return $dataArray;
	}
	
	function cekkdbarang(){
        $q = "SELECT count(kdbrg) as kdbrg FROM templateobatdet where kdbrg='".$_POST['kdbrg']."' AND idtempobat='".$_POST['idtempobat']."'";
        $query  = $this->db->query($q);
        $jum = '';
        if ($query->num_rows() != 0)
        {
			$row = $query->row();
            $jum=$row->kdbrg;
        }
        if ($jum == null){
            $jum=0;
        }
        echo $jum;
    }
	
	function get_parent_mastertemobat(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
		
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_mtemobat");
        $this->db->where("v_mtemobat.idjnshirarki = 1");
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
	
		$this->db->select("*");
		$this->db->from("v_mtemobat");
        $this->db->where("v_mtemobat.idjnshirarki = 1");
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_parent_mastertbrg(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
		
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$this->db->select("*");
		$this->db->from("v_tembarang");
		
		if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numroww($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numroww($fields, $query){
	
		$this->db->select("*");
		$this->db->from("v_tembarang");
        
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
}
