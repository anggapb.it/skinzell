<?php 
class Laporan_rmpenyakitterbanyakri_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_laprmpenyakitterbanyakri(){

		$tglawal 	= $this->input->post("tglawal");
        $tglakhir 	= $this->input->post("tglakhir");
        $vdkot 		= $this->input->post("vdkot");
        $vdkott		= $this->input->post("vdkott");
        $vpenj 		= $this->input->post("vpenj");
        $idpenjamin	= $this->input->post("idpenjamin");
		
		$q = "SELECT idkodifikasi
					 , idpenyakit
					 , dtd
					 , kdpenyakit
					 , nmpenyakit
					 , nmpenyakiteng
					 , kot
					 , idpenjamin
					 , sum(ukd6haril) AS ukd6haril
					 , sum(ukd6harip) AS ukd6harip
					 , sum(ukd28haril) AS ukd28haril
					 , sum(ukd28harip) AS ukd28harip
					 , sum(ukd1thnl) AS ukd1thnl
					 , sum(ukd1thnp) AS ukd1thnp
					 , sum(ukd4thnl) AS ukd4thnl
					 , sum(ukd4thnp) AS ukd4thnp
					 , sum(ukd14thnl) AS ukd14thnl
					 , sum(ukd14thnp) AS ukd14thnp
					 , sum(ukd24thnl) AS ukd24thnl
					 , sum(ukd24thnp) AS ukd24thnp
					 , sum(ukd44thnl) AS ukd44thnl
					 , sum(ukd44thnp) AS ukd44thnp
					 , sum(ukd64thnl) AS ukd64thnl
					 , sum(ukd64thnp) AS ukd64thnp
					 , sum(uld64thnl) AS uld64thnl
					 , sum(uld64thnp) AS uld64thnp
					 , sum(jmlkhidupl) AS jmlkhidupl
					 , sum(jmlkhidupp) AS jmlkhidupp
					 , sum(jmlkmatil) AS jmlkmatil
					 , sum(jmlkmatip) AS jmlkmatip
					 , sum(jumlah) AS jumlah

				FROM
				  (
				  SELECT kodifikasi.idkodifikasi
					   , kodifikasidet.idpenyakit
					   , penyakit.dtd
					   , penyakit.kdpenyakit
					   , penyakit.nmpenyakit
					   , penyakit.nmpenyakit as nmpenyakiteng
					   , kodifikasi.noreg
					   , kuitansi.tglkuitansi
					   , pasien.norm
					   , pasien.nmpasien
					   , dkel.nmdaerah AS kel
					   , dkec.nmdaerah AS kec
					   , dkot.nmdaerah AS kot
					   , registrasi.idpenjamin
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari <= 6, '0')
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd6haril
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari <= 6, '0')
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd6harip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari > 6, '0')
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari <= 28, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd28haril
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari > 6, '0')
							AND if((rd.umurtahun = '0'
							AND rd.umurbulan = '0'), rd.umurhari <= 28, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd28harip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((rd.umurtahun = '0'), if(rd.umurbulan = '0', rd.umurhari > 28, rd.umurbulan >= 1), rd.umurtahun = 1)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd1thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((rd.umurtahun = '0'), if(rd.umurbulan = '0', rd.umurhari > 28, rd.umurbulan >= 1), rd.umurtahun = 1)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd1thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun <= 4, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd4thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun <= 4, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd4thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND rd.tglkeluar BETWEEN '2015-06-01' AND '2015-06-30'
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 4, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 14, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd14thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 4, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 14, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd14thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 14, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 24, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd24thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 14, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 24, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd24thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 24, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 44, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd44thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 24, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 44, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd44thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 44, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 64, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd64thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 44, 0)
							AND if(rd.umurtahun > 1, rd.umurtahun <= 64, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd64thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if(rd.umurtahun > 1, rd.umurtahun > 64, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS uld64thnl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if(rd.umurtahun > 1, rd.umurtahun > 64, 0)
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS uld64thnp

					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkhidupl
					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkhidupp

					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut = 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkmatil
					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND kd.idtindaklanjut = 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkmatip

					   , (SELECT count(r.noreg)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN registrasi r
						  ON r.noreg = kd.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = r.norm
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(r.noreg)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN registrasi r
							  ON r.noreg = kd.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = r.norm
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 2
								AND kd.idtindaklanjut <> 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(r.noreg)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN registrasi r
							  ON r.noreg = kd.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = r.norm
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 1
								AND kd.idtindaklanjut = 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(r.noreg)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN registrasi r
							  ON r.noreg = kd.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = r.norm
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 2
								AND kd.idtindaklanjut = 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jumlah

				  FROM
					kodifikasidet
				  LEFT JOIN kodifikasi
				  ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
				  LEFT JOIN penyakit
				  ON penyakit.idpenyakit = kodifikasidet.idpenyakit
				  LEFT JOIN registrasi
				  ON registrasi.noreg = kodifikasi.noreg
				  LEFT JOIN registrasidet
				  ON registrasidet.noreg = registrasi.noreg
				  LEFT JOIN (
				  SELECT idregdet
					   , nokuitansi
				  FROM
					nota
				  GROUP BY
					idregdet
				  )nota
				  ON nota.idregdet = registrasidet.idregdet
				  LEFT JOIN kuitansi
				  ON kuitansi.nokuitansi = nota.nokuitansi
				  LEFT JOIN pasien
				  ON pasien.norm = registrasi.norm
				  LEFT JOIN daerah dkel
				  ON dkel.iddaerah = pasien.iddaerah
				  LEFT JOIN daerah dkec
				  ON dkec.iddaerah = dkel.dae_iddaerah
				  LEFT JOIN daerah dkot
				  ON dkot.iddaerah = dkec.dae_iddaerah
				  WHERE
					registrasi.idjnspelayanan = 2
					AND kodifikasidet.idkodifikasi IS NOT NULL
					AND if(1 = '".$vdkot."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('26005' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('30457' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('30754' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkot.iddaerah NOT IN (26005, 30457, 30754)
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

					AND if(1 = '".$vpenj."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', registrasi.idpenjamin = '".$idpenjamin."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
				  GROUP BY
					kodifikasidet.idpenyakit

				  UNION

				  SELECT kodifikasi.idkodifikasi
					   , kodifikasidet.idpenyakit
					   , penyakit.dtd
					   , penyakit.kdpenyakit
					   , penyakit.nmpenyakit
					   , penyakit.nmpenyakit as nmpenyakiteng
					   , kodifikasi.noreg
					   , kuitansi.tglkuitansi
					   , pasien.norm
					   , pasien.nmpasien
					   , dkel.nmdaerah AS kel
					   , dkec.nmdaerah AS kec
					   , dkot.nmdaerah AS kot
					   , registrasi.idpenjamin
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 6, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd6haril
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 6, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd6harip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) > 6, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 28, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd28haril
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) > 6, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 28, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd28harip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) > 28, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 365, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd1thn1
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) > 28, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')
							AND if((
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END) <= 365, (
							CASE
							WHEN isnull(s.tglkelahiran) THEN
							  ((to_days(curdate()) - to_days(s.tglkelahiran)) + 1)
							WHEN (s.tglkelahiran = rd.tglkeluar) THEN
							  '1'
							ELSE
							  ((to_days(rd.tglkeluar) - to_days(s.tglkelahiran)) + 1)
							END), '0')

							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS ukd1thnp
					   , '0' AS ukd4thnl
					   , '0' AS ukd4thnp
					   , '0' AS ukd14thnl
					   , '0' AS ukd14thnp
					   , '0' AS ukd24thnl
					   , '0' AS ukd24thnp
					   , '0' AS ukd44thnl
					   , '0' AS ukd44thnp
					   , '0' AS ukd64thnl
					   , '0' AS ukd64thnp
					   , '0' AS uld64thnl
					   , '0' AS uld64thnp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkhidupl
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkhidupp

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut = 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkmatil
					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 2
							AND kd.idtindaklanjut = 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jmlkmatip

					   , (SELECT count(kdt.idpenyakit)
						  FROM
							kodifikasidet kdt
						  LEFT JOIN kodifikasi kd
						  ON kd.idkodifikasi = kdt.idkodifikasi
						  LEFT JOIN skl s
						  ON s.noreganak = kd.noreg
						  LEFT JOIN registrasi r
						  ON r.noreg = s.noreg
						  LEFT JOIN registrasidet rd
						  ON rd.noreg = r.noreg
						  LEFT JOIN pasien p
						  ON p.norm = s.normanak
						  LEFT JOIN daerah dkell
						  ON dkell.iddaerah = p.iddaerah
						  LEFT JOIN daerah dkecc
						  ON dkecc.iddaerah = dkell.dae_iddaerah
						  LEFT JOIN daerah dkott
						  ON dkott.iddaerah = dkecc.dae_iddaerah
						  WHERE
							r.idjnspelayanan = 2
							AND kdt.idkodifikasi IS NOT NULL
							AND kdt.idpenyakit = kodifikasidet.idpenyakit
							AND p.idjnskelamin = 1
							AND kd.idtindaklanjut <> 5
							AND ((rd.idcarakeluar <> 6)
							OR isnull(rd.idcarakeluar))
							AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
							if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

							AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
							AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(kdt.idpenyakit)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN skl s
							  ON s.noreganak = kd.noreg
							  LEFT JOIN registrasi r
							  ON r.noreg = s.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = s.normanak
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 2
								AND kd.idtindaklanjut <> 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(kdt.idpenyakit)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN skl s
							  ON s.noreganak = kd.noreg
							  LEFT JOIN registrasi r
							  ON r.noreg = s.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = s.normanak
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 1
								AND kd.idtindaklanjut = 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) + (SELECT count(kdt.idpenyakit)
							  FROM
								kodifikasidet kdt
							  LEFT JOIN kodifikasi kd
							  ON kd.idkodifikasi = kdt.idkodifikasi
							  LEFT JOIN skl s
							  ON s.noreganak = kd.noreg
							  LEFT JOIN registrasi r
							  ON r.noreg = s.noreg
							  LEFT JOIN registrasidet rd
							  ON rd.noreg = r.noreg
							  LEFT JOIN (
							  SELECT idregdet
								   , nokuitansi
							  FROM
								nota
							  GROUP BY
								idregdet
							  ) n
							  ON n.idregdet = rd.idregdet
							  LEFT JOIN kuitansi k
							  ON k.nokuitansi = n.nokuitansi
							  LEFT JOIN pasien p
							  ON p.norm = s.normanak
							  LEFT JOIN daerah dkell
							  ON dkell.iddaerah = p.iddaerah
							  LEFT JOIN daerah dkecc
							  ON dkecc.iddaerah = dkell.dae_iddaerah
							  LEFT JOIN daerah dkott
							  ON dkott.iddaerah = dkecc.dae_iddaerah
							  WHERE
								r.idjnspelayanan = 2
								AND kdt.idkodifikasi IS NOT NULL
								AND kdt.idpenyakit = kodifikasidet.idpenyakit
								AND p.idjnskelamin = 2
								AND kd.idtindaklanjut = 5
								AND ((rd.idcarakeluar <> 6)
								OR isnull(rd.idcarakeluar))
								AND if(1 = '".$vdkot."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('26005' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30457' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
								if('30754' = '".$vdkott."', dkott.iddaerah = '".$vdkott."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkott.iddaerah NOT IN (26005, 30457, 30754)
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

								AND if(1 = '".$vpenj."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."', r.idpenjamin = '".$idpenjamin."'
								AND rd.tglkeluar BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
						 ) AS jumlah

				  FROM
					kodifikasidet
				  LEFT JOIN kodifikasi
				  ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
				  LEFT JOIN penyakit
				  ON penyakit.idpenyakit = kodifikasidet.idpenyakit
				  LEFT JOIN skl
				  ON skl.noreganak = kodifikasi.noreg
				  LEFT JOIN registrasi
				  ON registrasi.noreg = skl.noreg
				  LEFT JOIN registrasidet
				  ON registrasidet.noreg = registrasi.noreg
				  LEFT JOIN (
				  SELECT idregdet
					   , nokuitansi
				  FROM
					nota
				  GROUP BY
					idregdet
				  )nota
				  ON nota.idregdet = registrasidet.idregdet
				  LEFT JOIN kuitansi
				  ON kuitansi.nokuitansi = nota.nokuitansi
				  LEFT JOIN pasien
				  ON pasien.norm = skl.normanak
				  LEFT JOIN daerah dkel
				  ON dkel.iddaerah = pasien.iddaerah
				  LEFT JOIN daerah dkec
				  ON dkec.iddaerah = dkel.dae_iddaerah
				  LEFT JOIN daerah dkot
				  ON dkot.iddaerah = dkec.dae_iddaerah
				  WHERE
					registrasi.idjnspelayanan = 2
					AND kodifikasidet.idkodifikasi IS NOT NULL
					AND if(1 = '".$vdkot."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('26005' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('30457' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."',
					if('30754' = '".$vdkott."', dkot.iddaerah = '".$vdkott."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$vdkott."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', dkot.iddaerah NOT IN (26005, 30457, 30754)
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."')))))

					AND if(1 = '".$vpenj."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', if('' = '".$idpenjamin."', kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."', registrasi.idpenjamin = '".$idpenjamin."'
					AND kuitansi.tglkuitansi BETWEEN '". $tglawal ."' AND '". $tglakhir ."'))
				  GROUP BY
					kodifikasidet.idpenyakit
				  ) laprm
				GROUP BY
				  idpenyakit
				ORDER BY
				  dtd ASC";
		$query = $this->db->query($q);
		
		$data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
		
		$ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
}
