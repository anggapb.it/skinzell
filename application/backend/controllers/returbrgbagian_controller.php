<?php

class Returbrgbagian_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_returbrgbagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
        $userid            		= $this->input->post("userid");
      
        $this->db->select("*");
        $this->db->from("v_returbagian");
		$this->db->order_by('v_returbagian.noreturbagian DESC');
        
        if($userid){		
			$this->db->where("v_returbagian.idbagiandari IN (SELECT idbagian from penggunabagian where userid='".$userid."')");
        }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->numrow($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($key, $userid){
    
        $this->db->select("*");
        $this->db->from("v_returbagian"); 
		
        if($userid){		
			$this->db->where("v_returbagian.idbagiandari IN (SELECT idbagian from penggunabagian where userid = '".$userid."')");
        }
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        $q = $this->db->get();
        return $q->num_rows();
    }
	
	function get_returbrgbagiandet(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$noreturbagian			= $this->input->post("noreturbagian");
		
        $this->db->select("*");
        $this->db->from("v_returbagiandet");
		
		//if($noreturbagian != null)$this->db->where("v_returbagiandet.noreturbagian", $noreturbagian);
        $where = array();
        $where['v_returbagiandet.noreturbagian']=$noreturbagian;
		$this->db->where($where);
		
        /* if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
				
		
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        } */
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all("v_returbagiandet");
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	/* function nw($fields, $query){
    
        $this->db->select("*");
        $this->db->from("v_returbagiandet");
		
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
        $q = $this->db->get();
        return $q->num_rows();
    } */
	
	function get_pengbrgdireturbagian(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $this->input->post("key");
		$idbagian				= $this->input->post("idbagian");
		
        $this->db->select('*');
		$this->db->from('v_keluarbrgdireturbagian');
		$this->db->order_by('nokeluarbrg');
        
		$where = array();
		$where['v_keluarbrgdireturbagian.idbagiandari']=$idbagian;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrw($key, $idbagian);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrw($key, $idbagian){
      
        $this->db->select('*');
		$this->db->from('v_keluarbrgdireturbagian');
		
		$where = array();
		$where['v_keluarbrgdireturbagian.idbagiandari']=$idbagian;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_returbagian(){     
		$where['noreturbagian'] = $_POST['noreturbagian'];
		$del = $this->rhlib->deleteRecord('returbagiandet',$where);
		$del = $this->rhlib->deleteRecord('returbagian',$where);
        return $del;
    }
	
	function id_bagian($where, $val){
		$query = $this->db->getwhere('bagian',array($where=>$val));
		$id = $query->row_array();
		return $id['idbagian'];
    }
	
	function getNmbagian(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }
	
	function insorupd_retbagian(){
        $this->db->trans_begin();
		$noreturbagian = $this->input->post("noreturbagian");
		$query 	= $this->db->getwhere('returbagian',array('noreturbagian'=>$noreturbagian));
		$noreturbagian = $query->row_array();
				
		$returbagian = $this->insert_tblreturbagian($noreturbagian);
		$noreturbagian = $returbagian['noreturbagian'];
				
		if($noreturbagian)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["noreturbagian"]=$noreturbagian;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_tblreturbagian($noreturbagian){
		if(!$noreturbagian){
			$dataArray = $this->getFieldsAndValues($noreturbagian);
			$returbagian = $this->db->insert('returbagian',$dataArray);
		} else {
			$dataArray = $this->update_tblreturbagian($noreturbagian);
		}
		
		$query = $this->db->getwhere('returbagiandet',array('noreturbagian'=>$dataArray['noreturbagian']));
		$returbagiandet = $query->row_array();
		if($query->num_rows() == 0){
			$returbagiandet = $this->insert_tblreturbagiandet($dataArray);
		} else {
			$returbagiandet = $this->update_tblreturbagiandet($dataArray);
		}
		
		if($returbagiandet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function insert_tblreturbagiandet($returbagian){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrretbagian']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($returbagian, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('returbagiandet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function update_tblreturbagian($noreturbagian){
		$dataArray = $this->getFieldsAndValues($noreturbagian);
		
		//UPDATE
		$this->db->where('noreturbagian', $dataArray['noreturbagian']);
		$returbagian = $this->db->update('returbagian', $dataArray);
		
		return $dataArray;
    }
	
	function update_tblreturbagiandet($returbagian){
		$where['noreturbagian'] = $returbagian['noreturbagian'];
		$this->db->delete('returbagiandet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrretbagian']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($returbagian, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('returbagiandet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function getFieldsAndValues(){
			$norb   		= $this->input->post("noreturbagian");
			$Noreturbagian 	= $this->getNoreturbagian();
		
		$dataArray = array(
			 'noreturbagian'	=> ($norb) ? $norb: $Noreturbagian,
			 'tglreturbagian'	=> $_POST['tglreturbagian'],
             'jamreturbagian'	=> $_POST['jamreturbagian'],
             'idbagiandari'		=> $_POST['idbagiandari'],
             'idbagianuntuk'	=> $_POST['idbagianuntuk'],
             'idsttransaksi'	=> $_POST['idsttransaksi'],
             'idstsetuju'		=> $_POST['idstsetuju'],
			 'userid'			=> $_POST['userid'],
			 'keterangan'		=> $_POST['keterangan']			
		);
		/* var_dump($dataArray);
		exit; */
		return $dataArray;
	}
	
	function getNoreturbagian(){
		$q = "SELECT getOtoNoreturbagian(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}

	function getFieldsAndValuesDet($returbagian,$val1,$val2,$val3,$val4){
		$dataArray = array(
			'noreturbagian'  => $returbagian['noreturbagian'],
			'nokeluarbrg' 	 => $val1,
			'kdbrg'			 => $val2,
			'qty' 		 	 => $val3,
			'catatan'   	 => $val4,			
		);
		return $dataArray;
	}
	
	function update_qty_tambah(){
		//$Noreturbagian 	= $this->getNoreturbagian();
		$date = date("Y-m-d", strtotime($_POST['tglreturbagian']));
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_insorupd_brgbagian_dari_retbagian (?,'".$date."',?,?,?,?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3],
							 $vale[4],
							 $vale[5],
							 $vale[6],
							 $vale[7]
							));
		}		
        return;
    }
	
	function update_qty_batal(){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_update_brgbagian_dari_pengbrg (?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3]
							));
		}
        return;
    }
	
	function get_pengbagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
		$userid 				= $this->input->post("userid");
      
        $this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrww($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrww($key, $userid){
      
       /*  $this->db->select("penggunabagian.idbagian, bagian.nmbagian, bagian.jpelayanan.nmjnspelayanan, bdgrawat.nmbdgrawat pengguna.userid, pengguna.nmlengkap");
        $this->db->from("penggunabagian");		
		$this->db->join('bagian',
                'bagian.idbagian = penggunabagian.idbagian', 'left');
		$this->db->join('jpelayanan',
                'jpelayanan.idjnspelayanan = bagian.idjnspelayanan', 'left');
		$this->db->join('bdgrawat',
                'bdgrawat.idbdgrawat = bagian.idbdgrawat', 'left');
		$this->db->join('pengguna',
                'pengguna.userid = penggunabagian.userid', 'left'); */
		$this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
}