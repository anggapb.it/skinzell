<?php

class Pettycash_Controller extends Controller {
  private $idjnsjurnal = 4; // 4= petty cash

  public function __construct()
  {
    parent::Controller();
		$this->load->library('session');
		#$this->load->library('rhlib');
		$this->load->helper('terbilang');
  }

	function get_petty_cash()
	{
		$start = $this->input->post("start");
    $limit = $this->input->post("limit");
		$orderby = $this->input->post("orderby");
		$order = $this->input->post("order");
		$fields = $this->input->post("fields");
    $query = $this->input->post("query");
		$tglawal = $this->input->post("tglawal");
    $tglakhir = $this->input->post("tglakhir");
    $searchkey = $this->input->post("searchkey");
    $searchvalue = $this->input->post("searchvalue");

		$start = (!empty($start)) ? $start : 0;
		$limit = (!empty($limit)) ? $limit : 50;
		$orderby = (!empty($orderby)) ? $orderby : 'kdjurnal';
		$order = (!empty($order)) ? $order : 'DESC';
		
		$this->db->select('*');
		$this->db->from('v_pettycash');
		
		$this->db->limit($limit, $start);
		$this->db->orderby($orderby, $order);
		
		if(!empty($tglawal) && !empty($tglakhir))
		{
			if($tglawal == $tglakhir){
				$this->db->where('tgltransaksi', $tglawal);
			}else{
				$this->db->where('tgltransaksi >=', $tglawal);
				$this->db->where('tgltransaksi <=', $tglakhir); 
			}
		}
		
		if(!empty($searchkey) && $searchvalue != ''){
			if($searchkey == 'tgljurnal' && strpos($searchvalue,'/') !== false){
				//change date format
				$exp_date = explode('/', $searchvalue);
				$searchvalue = $exp_date[2].'-'.$exp_date[1].'-'.$exp_date[0];
			}
		
			$this->db->like($searchkey, $searchvalue);
		}
		
		$get = $this->db->get();
		$data = array();
    if ($get->num_rows() > 0) {
        $data = $get->result();
    }
    $total = $this->numrow_petty_cash($fields, $query, $tglawal, $tglakhir, $searchkey, $searchvalue);
    $build_array = array ("success" => true, "results" => $total, "data" => $data);
		
    echo json_encode($build_array);
		die();
	}
	
	function insert_petty_cash()
	{
		$kdjurnal     = $this->getKdPettyCash();
		$idjnsjurnal  = $this->idjnsjurnal;
		$tgltransaksi	= $this->input->post('tgltransaksi');
		$tgljurnal		= $this->input->post('tgljurnal');
		$keterangan		= $this->input->post('keterangan');
		$noreff				= $this->input->post('noreff');
		$userid				= $this->input->post('userid');
		$akun_debit		= $this->input->post('akun_debit');
		$akun_kredit	= $this->input->post('akun_kredit');
		$nominal			= $this->input->post('nominal');
		
		$ins_jurnal = true;
		$ins_jurnal_det = true;
		
		//get tahun
		$exp_tgljurnal = explode('-', $tgljurnal);
		$tahun = $exp_tgljurnal[0];
		
		//start transaction
		$this->db->trans_begin();
		
		//insert jurnal 
		$do_insert = $this->db->insert('jurnal', array(
			'kdjurnal'      => $kdjurnal,
			'idjnsjurnal'   => $idjnsjurnal,
			'tgltransaksi'  => $tgltransaksi,
			'tgljurnal'     => $tgljurnal,
			'keterangan'    => $keterangan,
			'noreff'        => $noreff,
			'userid'        => $userid,
			'nominal'       => $nominal,
			'status_posting' => 0
		));
		if($this->db->affected_rows() != 1) $ins_jurnal = false; //if insert failed, set to false
		
		//insert jurnal det debit		
		$data_debit = array(
			'kdjurnal' => $kdjurnal,
			'tahun' => $tahun,
      'idakun' => $akun_debit,
			'noreff' => $noreff,
			'debit' => $nominal,
			'kredit' => 0,
		);
		$this->db->insert('jurnaldet', $data_debit);
		if($this->db->affected_rows() != 1) $ins_jurnal_det = false; //if insert failed, set to false
		
		//insert jurnal det kredit		
		$data_kredit = array(
			'kdjurnal' => $kdjurnal,
			'tahun' => $tahun,
      'idakun' => $akun_kredit,
			'noreff' => $noreff,
			'debit' => 0,
			'kredit' => $nominal,
		);
		$this->db->insert('jurnaldet', $data_kredit);
		if($this->db->affected_rows() != 1) $ins_jurnal_det = false; //if insert failed, set to false

		if($ins_jurnal && $ins_jurnal_det){
			$this->db->trans_commit();
			$ret["success"] = true;
            $ret["kdjurnal"]=$kdjurnal;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
		die;
	}
	
	function update_petty_cash()
	{
		$kdjurnal			= $this->input->post('kdjurnal');
		$idjnsjurnal        = $this->idjnsjurnal;
		$tgltransaksi		= $this->input->post('tgltransaksi');
		$tgljurnal			= $this->input->post('tgljurnal');
		$keterangan			= $this->input->post('keterangan');
		$noreff				= $this->input->post('noreff');
		$userid				= $this->input->post('userid');
		$akun_debit			= $this->input->post('akun_debit');
		$akun_kredit		= $this->input->post('akun_kredit');
		$nominal			= $this->input->post('nominal');
		$update_jurnal_det  = true; 
		
		//get tahun
		$exp_tgljurnal = explode('-', $tgljurnal);
		$tahun = $exp_tgljurnal[0];
		
		//start transaction
		$this->db->trans_begin();
		
		//update jurnal 
		$this->db->update('jurnal', array(
			'idjnsjurnal'   => $idjnsjurnal,
			'tgltransaksi'  => $tgltransaksi,
			'tgljurnal'     => $tgljurnal,
			'keterangan'    => $keterangan,
			'noreff'        => $noreff,
			'userid'        => $userid,
			'nominal'       => $nominal,
		), array('kdjurnal' => $kdjurnal));
				
		//delete old detail jurnal
		$delete_old_data = $this->db->delete('jurnaldet', array('kdjurnal' => $kdjurnal)); 
		if(! $delete_old_data) $update_jurnal_det = false; 
		
		//insert jurnal det debit		
		$data_debit = array(
			'kdjurnal' => $kdjurnal,
			'tahun' => $tahun,
      'idakun' => $akun_debit,
			'noreff' => $noreff,
			'debit' => $nominal,
			'kredit' => 0,
		);
		$this->db->insert('jurnaldet', $data_debit);
		if($this->db->affected_rows() != 1) $update_jurnal_det = false; 
		
		//insert jurnal det kredit		
		$data_kredit = array(
			'kdjurnal' => $kdjurnal,
			'tahun' => $tahun,
      'idakun' => $akun_kredit,
			'noreff' => $noreff,
			'debit' => 0,
			'kredit' => $nominal,
		);
		$this->db->insert('jurnaldet', $data_kredit);
		if($this->db->affected_rows() != 1) $update_jurnal_det = false; 
		
		if($update_jurnal_det){
			$this->db->trans_commit();
			$ret["success"] = true;
            $ret["kdjurnal"]=$kdjurnal;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
		die;
	}
	
	function delete_petty_cash()
	{
		$kdjurnal			= $this->input->post('kdjurnal');
		$delete_jurnal      = true;
		$delete_jurnal_det  = true;
		
		//start transaction
		$this->db->trans_begin();
		
		$delete_detail = $this->db->delete('jurnaldet', array('kdjurnal' => $kdjurnal)); 
		if(! $delete_detail) $delete_jurnal_det = false; 
		
		$delete = $this->db->delete('jurnal', array('kdjurnal' => $kdjurnal)); 
		if(! $delete) $delete_jurnal = false; 
		
		
		if($delete && $delete_detail){
			$this->db->trans_commit();
			$ret["success"] = true;
            $ret["kdjurnal"] = $kdjurnal;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
		die;
	}
	
	function bulk_posting_pettycash()
	{
		$pc_ids = $this->input->post('pc_ids');
		if(empty($pc_ids)){
			$return = array('success' => false);
			echo json_encode($return);
			die;
		}

		$pc_ids = explode('^',$pc_ids);
		foreach($pc_ids as $pc_id){
			if(!empty($pc_id)){
				$this->db->update('jurnal', array('status_posting' => 1), array('kdjurnal' => $pc_id));
			}
		}

		$return = array('success' => true);
		echo json_encode($return);
		die;
		
	}

	function numrow_petty_cash($fields, $query, $tglawal, $tglakhir, $searchkey, $searchvalue){
		
		$this->db->select('*');
		$this->db->from('v_pettycash');
		
		if(!empty($tglawal) && !empty($tglakhir))
		{
			if($tglawal == $tglakhir){
				$this->db->where('tgltransaksi', $tglawal);
			}else{
				$this->db->where('tgltransaksi >=', $tglawal);
				$this->db->where('tgltransaksi <=', $tglakhir); 
			}
		}
		
		if(!empty($searchkey) && $searchvalue != ''){
			$this->db->like($searchkey, $searchvalue);
		}
		
		$get = $this->db->get();
		return $get->num_rows();
	}

	function get_ket_petty_cash()
	{
  	$query = $this->input->post("query");    
		$this->db->select('*');
		$this->db->from('v_keterangan_jurnal');
		$this->db->orderby('keterangan', 'ASC');
		$this->db->limit(30);
		
    if($query !=""){
        $this->db->like('keterangan', $query);
    }
		
		$get = $this->db->get();
    $data = array();
    if ($get->num_rows() > 0) {
        $data = $get->result();
    }

    $total = count($data);
    $build_array = array ("success" => true, "results" => $total, "data" => $data);
		
    echo json_encode($build_array);
		die();
	}

	function save_ket_petty_cash()
	{
		$s_keterangan = $this->input->post('s_keterangan');
		if(empty($s_keterangan)){
			echo json_encode(array('success' => false, 'msg' => 'keterangan kosong'));
			die;
		}

		$check = $this->db->get_where('ket_jurnal', array('ket' => $s_keterangan))->num_rows();
		if($check > 0){
			echo json_encode(array('success' => false, 'msg' => 'keterangan sudah ada'));
			die;
		}

		if($this->db->insert('ket_jurnal', array('ket' => $s_keterangan))){
			echo json_encode(array('success' => true));
			die;
		}else{
			echo json_encode(array('success' => false, 'msg' => 'gagal menyimpan'));
			die;
		
		}

	}
	
	function getKdPettyCash(){
		$q = "SELECT getOtoNoPettyCash(now()) as nm;";
        $query  = $this->db->query($q);
		$nm= ''; 
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
	
	function get_terbilang(){
		$nominal = $this->input->post('nominal');
		if(!is_numeric($nominal)) return 'data yang dimasukkan bukan angka';
		
		$terbilang = convert_terbilang($nominal);
		echo $terbilang;
		die;
	}

	function get_pairingakun(){
		$keterangan = $this->input->post('keterangan');
		$get_data = $this->db->query("
			SELECT 
				jurnal.kdjurnal as kdjurnal, 
				jurnal.keterangan as keterangan, 
				jurnaldet.idakun as idakun, 
				akun.nmakun AS nmakun
			FROM 
				(((jurnaldet
			JOIN jurnal
				ON ( 
						jurnal.kdjurnal = jurnaldet.kdjurnal AND 
						jurnaldet.debit > 0 AND 
						jurnal.idjnsjurnal = 4 AND
						jurnal.keterangan like '%".$keterangan."%'
						)
		     )
			LEFT JOIN akun
			on (jurnaldet.idakun = akun.idakun)))
		")->row_array();
		if(!empty($get_data)){
			echo json_encode($get_data);			
		}else{
			echo json_encode(array());
		}
		die;
	}

	function get_petty_cash_posting(){
		$tglawal   = $this->input->post("tglawal");
    $tglakhir  = $this->input->post("tglakhir");
		$orderby   = 'kdjurnal';
		$order     = 'DESC';

		$this->db->select('*');
		$this->db->from('jurnal');
		$this->db->orderby($orderby, $order);
		$this->db->where('idjnsjurnal', $this->idjnsjurnal);

		if(!empty($tglawal) && !empty($tglakhir))
		{
			if($tglawal == $tglakhir){
				$this->db->where('tgltransaksi', $tglawal);
			}else{
				$this->db->where('tgltransaksi >=', $tglawal);
				$this->db->where('tgltransaksi <=', $tglakhir); 
			}
		}else{
				$this->db->where('tgltransaksi', date('Y-m-d'));
		}
		
		$get = $this->db->get();
    $data = $get->result();
    if(!empty($data))
    {
    	foreach($data as $idx => $dt)
    	{
    		$data[$idx]->terbilang = convert_terbilang($dt->nominal);
    	}
    }

    $build_array = array ("success" => true, "results" => count($data), "data" => $data);
		
    echo json_encode($build_array);
		die();
	}

	function get_pettycash_postingdet(){
		$kdjurnal = $this->input->post('kdjurnal');
		$this->db->orderby('debit', 'DESC');
		$data = $this->db->get_where('v_jurnaldet', array('kdjurnal' => $kdjurnal))->result();
		$build_array = array ("success"=>true, "results"=>count($data), "data"=> $data);
		echo json_encode($build_array);
		die();
	}

	function posting_pettycash(){
		$kdjurnal = $this->input->post('kdjurnal_posting');
		$userid = $this->input->post('userid');
		
		if(!empty($kdjurnal)){
	    $this->db->trans_start();

			$update = array('status_posting' => 1,'userid' => $userid);
			$this->db->where('kdjurnal', $kdjurnal);

	    if($this->db->update('jurnal', $update)){
	  		$this->db->trans_complete();
	  		$return = array('success' => true, 'message' => 'berhasil memposting jurnal');
	      echo json_encode($return);
	      die();
	    
	    }else{
	      $this->db->trans_rollback();
	      $return = array('success' => false, 'message' => 'gagal memposting jurnal');
	      echo json_encode($return);
	      die();

	    }

		}
		
		$return = array('success' => false, 'message' => 'gagal memposting jurnal karena kdjurnal kosong');
		echo json_encode($return);
		die();

	}

	function get_akun_biaya()
	{
		#$this->db->orderby('nmakun', 'ASC');
		#$this->db->where('idklpakun', 5);
		#$this->db->where('idklpakun', 5);
		#$this->db->or_where('idklpakun', 6);
		$data = $this->db->query('SELECT * FROM akun WHERE (idklpakun = 5 OR idklpakun = 6) AND idstatus = 1 ORDER BY nmakun ASC ')->result_array();
		
    $build_array = array ("success" => true, "results" => count($data), "data" => $data);
    echo json_encode($build_array);

	}

	function get_akun_biaya_w_inventaris()
	{
		$data = $this->db->query('
			SELECT * FROM akun 
			WHERE (idklpakun = 5 OR idklpakun = 6) AND idstatus = 1 AND kdakun not in (61710,61720,61730,61740)
			UNION
			SELECT idakunparent, idakun, kdakun, concat(\'Invent. \', nmakun) AS nmakun, keterangan, idklpakun, idjnsakun, idstatus, userid, tglinput
			FROM akun
			WHERE kdakun = 15100 OR kdakun = 15200 OR kdakun = 15300 OR kdakun = 15400 OR kdakun = 15600
			ORDER BY nmakun ASC ')->result_array();
		
    $build_array = array ("success" => true, "results" => count($data), "data" => $data);
    echo json_encode($build_array);

	}

}
