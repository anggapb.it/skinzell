<?php

class Returbrgsupplier_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_returbrgsupplier(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
        $userid            		= $this->input->post("userid");
		
        $this->db->select("*");
        $this->db->from("v_retbrgsupplier");
		$this->db->order_by('v_retbrgsupplier.noretursupplier DESC');
        
        if($userid){		
			$this->db->where("v_retbrgsupplier.idbagian IN (SELECT idbagian from penggunabagian where userid='".$userid."')");
        }
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = $this->numrow($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($key, $userid){
    
        $this->db->select("*");
        $this->db->from("v_retbrgsupplier"); 
		
        if($userid){		
			$this->db->where("v_retbrgsupplier.idbagian IN (SELECT idbagian from penggunabagian where userid = '".$userid."')");
        }
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        $q = $this->db->get();
        return $q->num_rows();
    }
	
	function get_returbrgsupplierdet(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$noretursupplier			= $this->input->post("noretursupplier");
		
        $this->db->select("*");
        $this->db->from("v_retbrgsupplierdet");
		
		//if($noretursupplier != null)$this->db->where("v_retbrgsupplierdet.noretursupplier", $noretursupplier);
        $where = array();
        $where['v_retbrgsupplierdet.noretursupplier']=$noretursupplier;
		$this->db->where($where);
		
        /* if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
				
		
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        } */
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all("v_retbrgsupplierdet");
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	/* function nw($fields, $query){
    
        $this->db->select("*");
        $this->db->from("v_retbrgsupplierdet");
		
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
        $q = $this->db->get();
        return $q->num_rows();
    } */
	
	function get_supdiretsupplier(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		//$key					= $this->input->post("key");
		//$idbagian				= $this->input->post("idbagian");
		
        $this->db->select('*');
		$this->db->from('v_supdiretsupplier');
		$this->db->order_by('kdsupplier');
        
		 if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrow($fields, $query){
    
        $this->db->select("*");
        $this->db->from("v_supdiretsupplier");
		
        if($fields!="" || $query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
        $q = $this->db->get();
        return $q->num_rows();
    }
	
	function get_brgdiretursupplier(){
		$start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                  = $this->input->post("fields");
        $query                  = $this->input->post("query");
		$key					= $this->input->post("key");
		$kdsupplier					= $this->input->post("kdsupplier");
		
        $this->db->select('*');
		$this->db->from('v_brgdiretursupplier');
		$this->db->order_by('v_brgdiretursupplier.nopo DESC');
        
		$where = array();
		$where['v_brgdiretursupplier.kdsupplier']=$kdsupplier;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrw($key, $kdsupplier);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrw($key, $kdsupplier){
      
        $this->db->select('*');
		$this->db->from('v_brgdiretursupplier');
		
		$where = array();
		$where['v_brgdiretursupplier.kdsupplier']=$kdsupplier;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_returbrgsupplier(){     
		$where['noretursupplier'] = $_POST['noretursupplier'];
		$del = $this->rhlib->deleteRecord('retursupplierdet',$where);
		$del = $this->rhlib->deleteRecord('retursupplier',$where);
        return $del;
    }
	
	function getNmsupplier(){
		$query = $this->db->getwhere('supplier',array('kdsupplier'=>$_POST['kdsupplier']));
		$nm = $query->row_array();
		echo json_encode($nm['nmsupplier']);
    }
	
	function kd_supplier($where, $val){
		$query = $this->db->getwhere('supplier',array($where=>$val));
		$id = $query->row_array();
		return $id['kdsupplier'];
    }
	
	function id_bagian($where, $val){
		$query = $this->db->getwhere('bagian',array($where=>$val));
		$id = $query->row_array();
		return $id['idbagian'];
    }
	
	function getNmbagian(){
		$query = $this->db->getwhere('bagian',array('idbagian'=>$_POST['idbagian']));
		$nm = $query->row_array();
		echo json_encode($nm['nmbagian']);
    }
	
	function id_sttransaksi($where, $val){
		$query = $this->db->getwhere('sttransaksi',array($where=>$val));
		$id = $query->row_array();
		return $id['idsttransaksi'];
    }
	
	function getNmsttransaksi(){
		$query = $this->db->getwhere('sttransaksi',array('idsttransaksi'=>$_POST['idsttransaksi']));
		$nm = $query->row_array();
		echo json_encode($nm['nmsttransaksi']);
    }
	
	function insorupd_retsupplier(){
        $this->db->trans_begin();
		$noretursupplier = $this->input->post("noretursupplier");
		$query 	= $this->db->getwhere('retursupplier',array('noretursupplier'=>$noretursupplier));
		$noretursupplier = $query->row_array();
				
		$retursupplier = $this->insert_tblretursupplier($noretursupplier);
		$noretursupplier = $retursupplier['noretursupplier'];
				
		if($noretursupplier)
		{
      $this->db->trans_commit();
			$ret["success"] = true;
      $ret["noretursupplier"]=$noretursupplier;

     //tambah data ke jurnal
     //$this->db->query("CALL JURN_save_rs (?,?)",array($noretursupplier, $this->session->userdata['user_id']));

		}else{
      $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_tblretursupplier($noretursupplier){
		if(!$noretursupplier){
			$dataArray = $this->getFieldsAndValues($noretursupplier);
			$retursupplier = $this->db->insert('retursupplier',$dataArray);
		} else {
			$dataArray = $this->update_tblretursupplier($noretursupplier);
		}
		
		$query = $this->db->getwhere('retursupplierdet',array('noretursupplier'=>$dataArray['noretursupplier']));
		$retursupplierdet = $query->row_array();
		if($query->num_rows() == 0){
			$retursupplierdet = $this->insert_tblretursupplierdet($dataArray);
		} else {
			$retursupplierdet = $this->update_tblretursupplierdet($dataArray);
		}
		
		if($retursupplierdet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function insert_tblretursupplierdet($retursupplier){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrretsupplier']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($retursupplier, $vale[0], $vale[1], $vale[2], $vale[3], $vale[5]);
			$z =$this->db->insert('retursupplierdet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function update_tblretursupplier($noretursupplier){
		$dataArray = $this->getFieldsAndValues($noretursupplier);
		
		//UPDATE
		$this->db->where('noretursupplier', $dataArray['noretursupplier']);
		$retursupplier = $this->db->update('retursupplier', $dataArray);
		
		return $dataArray;
    }
	
	function update_tblretursupplierdet($retursupplier){
		$where['noretursupplier'] = $retursupplier['noretursupplier'];
		$this->db->delete('retursupplierdet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrretsupplier']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($retursupplier, $vale[0], $vale[1], $vale[2], $vale[3], $vale[5]);
			$z =$this->db->insert('retursupplierdet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function getFieldsAndValues(){
			$nors		  		= $this->input->post("noretursupplier");
			$Noretursupplier 	= $this->getNoretursupplier();
		
		$dataArray = array(
			 'noretursupplier'	=> ($nors) ? $nors: $Noretursupplier,
			 'tglretursupplier'	=> $_POST['tglretursupplier'],
             'jamretursupplier'	=> $_POST['jamretursupplier'],
             'idbagian'			=> $_POST['idbagian'],
             'kdsupplier'		=> $_POST['kdsupplier'],
             'idsttransaksi'	=> $_POST['idsttransaksi'],
             'idstsetuju'		=> $_POST['idstsetuju'],
			 'userid'			=> $_POST['userid'],
			 'keterangan'		=> $_POST['keterangan']			
		);
		/* var_dump($dataArray);
		exit; */
		return $dataArray;
	}
	
	function getNoretursupplier(){
		$q = "SELECT getOtoNoretursupplier(now()) as nm;";
        $query  = $this->db->query($q);
        $nm= ''; 
                    
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}

	function getFieldsAndValuesDet($retursupplier,$val1,$val2,$val3,$val4,$val5){
		$dataArray = array(
			'noretursupplier'  	=> $retursupplier['noretursupplier'],
			'nopo' 				=> $val1,
			'kdbrg'				=> $val2,
			'qty' 		 	 	=> $val3,
			'catatan'   	 	=> $val4,			
			'idstbayar'   	 	=> $val5,			
		);
		return $dataArray;
	}
	
	function update_qty_retur(){
		//$Noretursupplier 	= $this->getNoretursupplier();
		$date = date("Y-m-d", strtotime($_POST['tglretursupplier']));
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_insorupd_brgbagian_dari_retsupplier (?,'".$date."',?,?,?,?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3],
							 $vale[4],
							 $vale[5],
							 $vale[6],
							 $vale[7],
							));
		}		
        return;
    }
	
	function update_qty_batal(){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arruqty']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$this->db->query("CALL SP_update_brgbagian_dari_pengbrg (?,?,?,?)",
						 array(
							 $vale[0],
							 $vale[1],
							 $vale[2],
							 $vale[3]
							));
		}
        return;
    }
	
	function get_pengbagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$key					= $this->input->post("key");
		$userid 				= $this->input->post("userid");
      
        $this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
		if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
		
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(18,0);
        }
		
		$this->db->where($where);
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->nrww($key, $userid);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function nrww($key, $userid){
      
       /*  $this->db->select("penggunabagian.idbagian, bagian.nmbagian, bagian.jpelayanan.nmjnspelayanan, bdgrawat.nmbdgrawat pengguna.userid, pengguna.nmlengkap");
        $this->db->from("penggunabagian");		
		$this->db->join('bagian',
                'bagian.idbagian = penggunabagian.idbagian', 'left');
		$this->db->join('jpelayanan',
                'jpelayanan.idjnspelayanan = bagian.idjnspelayanan', 'left');
		$this->db->join('bdgrawat',
                'bdgrawat.idbdgrawat = bagian.idbdgrawat', 'left');
		$this->db->join('pengguna',
                'pengguna.userid = penggunabagian.userid', 'left'); */
		$this->db->select("*");
		$this->db->from("v_bagiandilogistik");
		
        $where = array();
		$where['v_bagiandilogistik.userid']=$userid;
		
        if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		}
        
		$this->db->where($where);
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_pengunabagian(){
        $start                  = $this->input->post("start");
        $limit                  = $this->input->post("limit");
        
        $fields                 = $this->input->post("fields");
        $query                  = $this->input->post("query");
		
		$userid 				= $this->input->post("userid");
      
        $this->db->select("penggunabagian.idbagian, bagian.nmbagian, pengguna.userid, pengguna.nmlengkap");
        $this->db->from("penggunabagian");		
		$this->db->join('bagian',
                'bagian.idbagian = penggunabagian.idbagian', 'left');
		$this->db->join('pengguna',
                'pengguna.userid = penggunabagian.userid', 'left');
		$this->db->where("penggunabagian.idbagian = '11'");
		
        $where = array();
		$where['penggunabagian.userid']=$userid;
		$this->db->where($where);
		
		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}