<?php

class Nota_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
			$this->rhlib->secure($this->my_usession);
    }
	
	function get_nota(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        
        $fields			= $this->input->post("fields");
        $query			= $this->input->post("query");
        
        $idregdet		= $this->input->post("idregdet");
        $nonota			= $this->input->post("nonota");
        $koder			= $this->input->post("koder");
        $jpel			= $this->input->post("jpel");
        $idbagian		= $this->input->post("idbagian");
        $grupby			= $this->input->post("grupby");
        $khri			= $this->input->post("khri");
		
		$this->db->select("*, nota.nonota AS nonota, nota.catatan AS catatann, 
				if(returfarmasidet.qty, returfarmasidet.qty, 0) AS qtyretur, notadet.qty AS qty,
				(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) tarif,
				(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) tarif2,
				(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) diskon,
				nota.diskon AS diskonr, nota.uangr AS uangr, notadet.dijamin AS dijaminrp, notadet.dijamin AS dijaminrpf,
				((notadet.dijamin/((notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp)))*100) AS dijaminprsn,
				((notadet.dijamin/(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)))*100) AS dijaminprsnf,
				(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - notadet.dijamin AS selisih,
				(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - notadet.dijamin AS selisihf,
				notadet.stdijamin AS dijamin, nota.stplafond AS stplafond
		", false);
        $this->db->from("nota");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("v_tarifall",
				"v_tarifall.kditem = notadet.kditem", "left");
        $this->db->join("dokter",
				"dokter.iddokter = notadet.iddokter", "left");
        $this->db->join("perawat",
				"perawat.idperawat = notadet.idperawat", "left");
        $this->db->join("jsatuan",
				"jsatuan.idsatuan = notadet.idsatuan", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
		if(!$khri){
			if($idregdet)$this->db->where("nota.idregdet", $idregdet);
			else $this->db->where("nota.idregdet", null);
		}
		if($nonota)$this->db->where("nota.nonota", $nonota);
		if($jpel)$this->db->where("bagian.idjnspelayanan", $jpel);
		if($idbagian)$this->db->where("nota.idbagian", $idbagian);
		if($koder == 0){
			$this->db->where("notadet.koder", null);
			$this->db->group_by("notadet.kditem");
		} else if($koder == 1) {
			$this->db->where("notadet.koder !=", '');
			$this->db->group_by("notadet.kditem, notadet.koder");
			$this->db->orderby("notadet.koder");
		}
		if($grupby)$this->db->group_by("nota.nonota");
		$this->db->where("nota.idsttransaksi !=", 2);
		$this->db->orderby("nota.nonota desc");
		//var_dump($this->db);
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nota.nonota', $val);
				$this->db->or_like('atasnama', $val);
			}
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
        $idregdet		= $this->input->post("idregdet");
        $nonota			= $this->input->post("nonota");
        $koder			= $this->input->post("koder");
        $jpel			= $this->input->post("jpel");
        $idbagian		= $this->input->post("idbagian");
        $grupby			= $this->input->post("grupby");
        $khri			= $this->input->post("khri");
		
		$this->db->select("*, nota.catatan AS catatann, 
				if(returfarmasidet.qty, returfarmasidet.qty, 0) AS qtyretur, notadet.qty AS qty,
				(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) tarif,
				(notadet.qty * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) tarif2,
				(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) diskon
		", false);
        $this->db->from("nota");
        $this->db->join("notadet",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("v_tarifall",
				"v_tarifall.kditem = notadet.kditem", "left");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("perawat",
				"perawat.idperawat = notadet.idperawat", "left");
        $this->db->join("jsatuan",
				"jsatuan.idsatuan = notadet.idsatuan", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("kuitansi",
				"kuitansi.nokuitansi = nota.nokuitansi", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
				
		if(!$khri){
			if($idregdet)$this->db->where("nota.idregdet", $idregdet);
			else $this->db->where("nota.idregdet", null);
		}
		if($nonota)$this->db->where("nota.nonota", $nonota);
		if($jpel)$this->db->where("bagian.idjnspelayanan", $jpel);
		if($idbagian)$this->db->where("nota.idbagian", $idbagian);
		if($koder == 0){
			$this->db->where("notadet.koder", null);
			$this->db->group_by("notadet.kditem");
		} else if($koder == 1) {
			$this->db->where("notadet.koder !=", '');
			$this->db->group_by("notadet.kditem, notadet.koder");
			$this->db->orderby("notadet.koder");
		}
		if($grupby)$this->db->group_by("nota.nonota");
		$this->db->orderby("nota.nonota desc");
			
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nota.nonota', $val);
				$this->db->or_like('atasnama', $val);
			}
        }
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_notax(){
        $start				= $this->input->post("start");
        $limit				= $this->input->post("limit");
		
        $idregdet			= $this->input->post("idregdet");
        $noreg				= $this->input->post("noreg");
        $jpel				= $this->input->post("jpel");
        $batal				= $this->input->post("batal");
        $ctglnota			= $this->input->post("ctglnota");
        $cnmjnspelayanan	= $this->input->post("cnmjnspelayanan");
        $cdokter			= $this->input->post("cdokter");
        $cnorm				= $this->input->post("cnorm");
        $cnoreg				= $this->input->post("cnoreg");
        $cnmpasien			= $this->input->post("cnmpasien");
        $batal				= $this->input->post("batal");
		
		$this->db->select("*,
				(select SUM((nd.tarifjs*nd.qty)+(nd.tarifjm*nd.qty)+(nd.tarifjp*nd.qty)+(nd.tarifbhp*nd.qty)) as ttlnota
					from notadet nd
					where nd.nonota = nota.nonota) as ttlnota
		");
        $this->db->from("nota");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg = registrasidet.noreg", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
		
		if($idregdet)$this->db->where("nota.idregdet", $idregdet);
		if($noreg)$this->db->where("registrasi.noreg", $noreg);
		if($jpel)$this->db->where("bagian.idjnspelayanan", $jpel);
		if($batal)$this->db->where("nota.idsttransaksi !=", $batal);
		
		if($ctglnota)$this->db->like("nota.tglnota !=", $ctglnota);
		if($cnmjnspelayanan)$this->db->like("nota.idbagian !=", $cnmjnspelayanan);
		if($cdokter)$this->db->like("nota.iddokter !=", $cdokter);
		if($cnorm)$this->db->like("pasien.norm !=", $cnorm);
		if($cnmpasien)$this->db->like("pasien.nmpasien !=", $cnmpasien);
		if($cnoreg)$this->db->like("registrasi.noreg !=", $cnoreg);
		
		$this->db->orderby("tglnota desc");
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrowx();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrowx(){
        $start				= $this->input->post("start");
        $limit				= $this->input->post("limit");
		
        $idregdet			= $this->input->post("idregdet");
        $noreg				= $this->input->post("noreg");
        $jpel				= $this->input->post("jpel");
        $batal				= $this->input->post("batal");
        $ctglnota			= $this->input->post("ctglnota");
        $cnmjnspelayanan	= $this->input->post("cnmjnspelayanan");
        $cdokter			= $this->input->post("cdokter");
        $cnorm				= $this->input->post("cnorm");
        $cnoreg				= $this->input->post("cnoreg");
        $cnmpasien			= $this->input->post("cnmpasien");
        $batal				= $this->input->post("batal");
		
		$this->db->select("*,
				(select SUM((nd.tarifjs*nd.qty)+(nd.tarifjm*nd.qty)+(nd.tarifjp*nd.qty)+(nd.tarifbhp*nd.qty)) as ttlnota
					from notadet nd
					where nd.nonota = nota.nonota) as ttlnota
		");
        $this->db->from("nota");
        $this->db->join("dokter",
				"dokter.iddokter = nota.iddokter", "left");
        $this->db->join("bagian",
				"bagian.idbagian = nota.idbagian", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg = registrasidet.noreg", "left");
        $this->db->join("pasien",
				"pasien.norm = registrasi.norm", "left");
        $this->db->join("penjamin",
				"penjamin.idpenjamin = registrasi.idpenjamin", "left");
		
		if($idregdet)$this->db->where("nota.idregdet", $idregdet);
		if($noreg)$this->db->where("registrasi.noreg", $noreg);
		if($jpel)$this->db->where("bagian.idjnspelayanan", $jpel);
		if($batal)$this->db->where("nota.idsttransaksi !=", $batal);
		
		if($ctglnota)$this->db->like("nota.tglnota !=", $ctglnota);
		if($cnmjnspelayanan)$this->db->like("nota.idbagian !=", $cnmjnspelayanan);
		if($cdokter)$this->db->like("nota.iddokter !=", $cdokter);
		if($cnorm)$this->db->like("pasien.norm !=", $cnorm);
		if($cnmpasien)$this->db->like("pasien.nmpasien !=", $cnmpasien);
		if($cnoreg)$this->db->like("registrasi.noreg !=", $cnoreg);
		
		$this->db->orderby("tglnota desc");
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function get_notatransri(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        
        $fields			= $this->input->post("fields");
        $query			= $this->input->post("query");
        
        $idregdet		= $this->input->post("idregdet");
        $noreg			= $this->input->post("noreg");
        $nonota			= $this->input->post("nonota");
        $koder			= $this->input->post("koder");
        $okoder			= $this->input->post("okoder");
        $jpel			= $this->input->post("jpel");
        $idbagian		= $this->input->post("idbagian");
        $grupby			= $this->input->post("grupby");
		
		$this->db->select("*, nota.catatan AS catatann,
				(notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) tarif,
				(SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) AS qty,
				(SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp) - SUM(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) tarif2,
				SUM(notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) diskon,
				SUM(nota.uangr) AS uangr, SUM(nota.diskon) AS diskonr, nota.uangr AS uangr,IFNULL((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem), 0) AS dijaminrp,IFNULL((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem), 0) AS dijaminrpf,IFNULL((((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem)/(((SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp)))*100), 0) AS dijaminprsn,IFNULL((((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem)/((SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)))*100), 0) AS dijaminprsnf,((SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - IFNULL((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem), 0) AS selisih, ((SUM(notadet.qty) - if(SUM(returfarmasidet.qty), SUM(returfarmasidet.qty), 0)) * (notadet.tarifjs + notadet.tarifjm + notadet.tarifjp + notadet.tarifbhp)) - (notadet.diskonjs + notadet.diskonjm + notadet.diskonjp + notadet.diskonbhp) - IFNULL((SELECT nad.dijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem), 0) AS selisihf,(SELECT nad.stdijamin FROM notaakhirdet nad WHERE nad.nona = nota.nona AND nad.kditem = notadet.kditem) AS dijamin, (SELECT na.stplafond FROM notaakhir na WHERE na.nona = nota.nona) AS stplafond
		", false);
        $this->db->from("notadet");
        $this->db->join("nota",
				"notadet.nonota = nota.nonota", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = nota.idregdet", "left");
        $this->db->join("registrasi",
				"registrasi.noreg = registrasidet.noreg", "left");
        $this->db->join("v_brgpel",
				"v_brgpel.kditem = notadet.kditem", "left");
        $this->db->join("jsatuan",
				"jsatuan.idsatuan = notadet.idsatuan", "left");
        $this->db->join("returfarmasidet",
				"returfarmasidet.idnotadet = notadet.idnotadet AND returfarmasidet.idstposisipasien = 5", "left");
		/* if($idregdet)$this->db->where("nota.idregdet", $idregdet);
		else $this->db->where("nota.idregdet", null); */
		if($noreg)$this->db->where("registrasi.noreg", $noreg);
		else $this->db->where("registrasi.noreg", null);
		if($nonota)$this->db->where("nota.nonota", $nonota);
		if($idbagian)$this->db->where("nota.idbagian", $idbagian);
		if($koder == 0){
			$this->db->where("notadet.koder", null);
			$this->db->group_by("notadet.kditem");
		} else if($koder == 1) {
			$this->db->where("notadet.koder !=", '');
			$this->db->group_by("notadet.kditem");
		}
		
		if($okoder){
			$this->db->orderby("notadet.koder");
		}
		
        if($query !=""){
			$arrquery = explode(' ', $query);
			foreach($arrquery AS $val){
				$this->db->like('nota.nonota', $val);
				$this->db->or_like('atasnama', $val);
			}
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		$ttl = $q->num_rows();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_kuinota(){
        $this->db->select("*,
				SUM((tarifjs*qty)+(tarifjm*qty)+(tarifjp*qty)+(tarifbhp*qty)) as ttlnota
		", false);
        $this->db->from("notadet");
		$this->db->where("nonota", $_POST['nonota']);
		//$this->db->where("idstbypass", 1);
        
        $q = $this->db->get();
        $data = $q->row_array();
		
        echo json_encode($data);
    }
	
	function delete_notadet(){
		$where['nonota'] = $_POST['nonota'];
		$where['kditem'] = $_POST['kditem'];
		$where['idjnstarif'] = $_POST['idjnstarif'];
		$del = $this->rhlib->deleteRecord('notadet',$where);
		
		if($del)
		{
			$ret["success"] = true;
            $ret["nonota"]=$_POST['nonota'];
		}else{
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function delete_bnotadet(){
		
        $k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arr']);
		$b=explode(',', $r);
		foreach($b AS $x){
			$y=explode('-', $x);
			$where['nonota'] = $_POST['nonota'];
			$where['kditem'] = $y[0];
			$where['idjnstarif'] = $y[1];
			$del = $this->rhlib->deleteRecord('notadet',$where);
		}
		
		if($del)
		{
			$ret["success"] = true;
            $ret["nonota"]=$_POST['nonota'];
		}else{
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function batal_nota(){
		$dataArray = array(
             'idsttransaksi'=> 2
        );		
		$this->db->where('nonota', $_POST['nonota']);
		$z =$this->db->update('nota', $dataArray);
		
		if($z)
		{
			$ret["success"] = true;
            $ret["nonota"]=$_POST['nonota'];
		}else{
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
		
	function insorupd_nota(){
        $this->db->trans_begin();
		$kui = 0;
		if(isset($_POST['kui'])) $kui = $_POST['kui'];
		$arrnota = $this->input->post("arrnota");
		$query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg']));
		$reg = $query->row_array();
		if(isset($_POST['fri'])) $reg['idbagian'] = $_POST['fri'];
		
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['nonota']));
		$notanr = $query->num_rows();
		$nota = $query->row_array();
		
		if($kui == 0 && $_POST['arrcarabayar'] != "[]"){
			$kque = $this->insert_kuitansi($nota);
			$kui = $kque['nokuitansi'];
		} else {
			$kui = null;
		}
		
		$_POST['noresep'] = null;
		if($notanr == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			//$nota = $query->row_array();
			$vnota = $nota['nonota'];
		}
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $nota['idbagian']);
		
		if($nota && $notadet)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$vnota;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insorupd_notari(){
        $this->db->trans_begin();
		if(!isset($_POST['stpelayanan']))$stpelayanan=null;
		else $stpelayanan=$this->searchId('idstpelayanan','stpelayanan','nmstpelayanan',$_POST['stpelayanan']);
		
		$query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg']));
		$reg = $query->row_array();
		$regarr = $query->result();
		if($this->input->post("cekpembayaran") == 1){
			$query1 = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg'], 'nokuitansi IS NOT NULL'=>null));
			$reg1 = $query1->row_array();
			$na = $reg['nona'];
			$where['nokuitansi'] = $reg1['nokuitansi'];
			$kuidet = $this->update_kuitansidet($where);
			if($kuidet)
			{
				$this->db->trans_commit();
				$ret["success"] = true;
				$ret["nona"]=$na;
			}else{
				$this->db->trans_rollback();
				$ret["success"] = false;
			}
		} else {
			
			$kque = $this->insert_kuitansi(false);
			$kui = $kque['nokuitansi'];
			
			$naque = $this->insert_notaakhir($kui);
			$na = $naque['nona'];
			foreach($regarr AS $i=>$val){
				$dataArray = array('nokuitansi'=> $kui, 'nona'=>$na, 'idstpelayanan'=>$stpelayanan, 'diskon'=>$_POST['diskonf'], 'uangr'=>$_POST['uangr']);
				$this->db->where('idregdet', $val->idregdet);
				$z = $this->db->update('nota', $dataArray);
				
				$this->update_posisispasien($val->idregdet);
			}
			$dataArray = array(
				'idstbed' => 1
			);
			$this->db->where('idbed', $reg['idbed']);
			$bed = $this->db->update('bed', $dataArray);
			
			$k=array('[',']','"');
			$r=str_replace($k, '', $_POST['arrnota']);
			$b=explode(',', $r);
			foreach($b AS $val){
				$vale = explode('-',$val);
				
				if($vale[1]>0)$stdijamin = 1;
				else $stdijamin = 0;
				
				$dataArrayx = array(
					'nona'=> $na,
					'kditem'=> $vale[0],
					'dijamin'=> $vale[1],
					'stdijamin'=> $stdijamin,
				);
				$y = $this->db->insert('notaakhirdet', $dataArrayx);
			}
			
			$dataArray = array(
				'tglkeluar' => date('Y-m-d'),
				'jamkeluar' => date('H:i:s')
			);
			$this->db->where('noreg', $_POST['noreg']);
			$regdet = $this->db->update('registrasidet', $dataArray);
			
			if($kui && $z && $na && $regdet && $bed)
			{
				$this->db->trans_commit();
				$ret["success"] = true;
				$ret["nona"]=$na;
			}else{
				$this->db->trans_rollback();
				$ret["success"] = false;
			}
		} 
		echo json_encode($ret);
    }
	
	function insorupd_paket($idtarifpaket, $nota){
		$query = $this->db->getwhere('tarifpaketdet',array('idtarifpaket'=>$idtarifpaket));
		$paketdet = $query->result();
		
		$q = $this->db->getwhere('notadet',array('kditem'=>$idtarifpaket, 'nonota'=>$nota));
		$nd = $q->row_array();
		
		foreach($paketdet AS $z=>$valz){
			$strbrb = substr($valz->kdtarif, 0, 1);
			if($strbrb == 'B'){
				$query = $this->db->getwhere('barang',array('kdbrg'=>$valz->kdtarif));
				$brg = $query->row_array();
				$idsatuan = $brg->idsatuankcl;
			} else {
				$idsatuan = null;
			}
			
			$dataArray = array(
				'idnotadet'=> $nd['idnotadet'],
				'kditem'=> $valz->kdtarif,
				'idjnstarif'=> $valz->idjnstarif,
				'idsatuan'=> $idsatuan,
				'qty'=> $valz->qty,
				'tarifjs'=> $valz->tarifjs,
				'tarifjm'=> $valz->tarifjm,
				'tarifjp'=> $valz->tarifjp,
				'tarifbhp'=> $valz->tarifbhp,
			);
			
			$w =$this->db->insert('notapaket',$dataArray);
		}
		return true;
    }
	
	function insert_notaakhir($kui){
		$dataArray = $this->getFieldsAndValuesNona($kui);
		$notaakhir = $this->db->insert('notaakhir',$dataArray);
		
		if($notaakhir){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }

	function insorupd_deposit(){
        $this->db->trans_begin();
		
		$_POST['idjnskuitansi'] = 5;
		$query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg']));
		$reg = $query->row_array();
		
		$kque = $this->insert_kuitansi(false);
		$kui = $kque['nokuitansi'];
		if($kui)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nokui"]=$kui;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function insert_kuitansi($nota){
		if(!$nota){
			$dataArray = $this->getFieldsAndValuesKui($nota);
			$kuitansi = $this->db->insert('kuitansi',$dataArray);
		} else {
			$dataArray = $this->update_kuitansi($nota);
		}
		
		$query = $this->db->getwhere('kuitansidet',array('nokuitansi'=>$dataArray['nokuitansi']));
		$kuitansidet = $query->row_array();
		if($query->num_rows() == 0){
			$kuitansidet = $this->insert_kuitansidet($dataArray);
		} else {
			$kuitansidet = $this->update_kuitansidet($dataArray);
		}
		
		if($kuitansidet){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
		
	function insert_kuitansidet($kuitansi){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrcarabayar']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesKuiDet($kuitansi, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('kuitansidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
		
	function insert_nota($reg, $kui){
		$dataArray = $this->getFieldsAndValues($reg, $kui);
		$z =$this->db->insert('nota',$dataArray);
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
		
	function insert_notadet($nota, $reg, $arrnota, $idbagian){
		$where['nonota'] = $nota;
		//update brgbagian
		$query = $this->db->getwhere('notadet',$where);
		$ceknd = $query->row_array();
		if($ceknd)$stok = $this->tambahStok($where, $idbagian);
		if($ceknd)$this->db->delete('notapaket',array('idnotadet'=>$ceknd['idnotadet']));
		$this->db->delete('notadet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $arrnota);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-',$val);
			if($val != '' && $vale[0] != "null") {
				if(!isset($vale[9]))$vale[9]=0;
				
				if ($_POST['ureg'] != 'FA' && (!isset($vale[8]) || $vale[8]=='')) $dokter=null;
				else if(!isset($vale[8]) || $vale[8]=='null') $dokter=$reg['iddokter'];
				else $dokter=$this->searchIdkhususdr('iddokter','dokter','nmdoktergelar',$vale[8]);
				//if((!isset($vale[8]) || !$dokter) && ($vale[8]=='' || !$dokter) )$dokter=$this->searchIdkhususdr('iddokter','dokter','nmdoktergelar',$vale[8]);
				
				$dataArray = $this->getFieldsAndValuesDet($nota, $reg, $vale[0], $vale[1], $vale[2], $vale[3], $vale[4], $vale[5], $vale[6], $vale[7], $dokter, $vale[9]);
				$z =$this->db->insert('notadet',$dataArray);
				$stok = $this->kurangStok($idbagian, $vale[0], $vale[1], $vale[2]);
				
				$strbrp = substr($vale[0], 0, 1);
				if($strbrp == 'P'){
					$this->insorupd_paket($vale[0], $nota);
				}
			}
		}
        return true;
    }
	
	function insert_kartustok(){
		/* $stkeluar			= $this->input->post("stkeluar");
		$carakeluar			= $this->input->post("carakeluar");
		if(!$stkeluar) $stkeluar = 1;
		if(!$carakeluar) $carakeluar = 1; */
		
		$query = $this->db->getwhere('kartustok',array('noref'=>$_POST['noref']));
		$cekks = $query->num_rows();
		
		if($cekks < 1){
			$k=array('[',']','"');
			$r=str_replace($k, '', $_POST['arrnota']);
			$b=explode(',', $r);
			foreach($b AS $val){
				$vale = explode('-',$val);
				
				$query = $this->db->getwhere('bagian',array('nmbagian'=>$vale[4]));
				$rec = $query->row_array();
				
				$where = $vale[5].' ORDER BY ';
				$this->db->select("saldoakhir", false);
				$this->db->from("kartustok");
				$this->db->where("idbagian", $rec['idbagian']);
				$this->db->where("kdbrg", $vale[5]);
				$this->db->order_by("tglinput DESC");
				$query = $this->db->get();
				$recks = $query->row_array();
				if(!$recks) $recks['saldoakhir'] = 0;
				$saldoakhir = $recks['saldoakhir'] - $vale[3];
				$tgltransaksi = date_format(date_create($vale[1]), 'Y-m-d');
				
				$query = $this->db->getwhere('barang',array('kdbrg'=>$vale[5]));
				$hrgbrg = $query->row_array();
				
				/* $qry = $this->db->getwhere('barangbagian',array('kdbrg'=>$vale[5], 'idbagian'=>$rec['idbagian']));
				$rec1 = $qry->row_array();
				
				$saldoakhir = $rec1['stoknowbagian'] - $vale[3]; */
				
				if($query->num_rows() > 0){
					$hrgbeli = $hrgbrg['hrgbeli'];
					$this->db->query("CALL SP_insert_transaksi (?,?,?,?,?,?,?,?,?,?,?,?,?)",
								array(
									$vale[0],
									$tgltransaksi,
									$vale[2],
									$vale[3],
									$saldoakhir,
									$this->session->userdata['user_id'],
									$rec['idbagian'],
									$_POST['noref'],
									$rec['kdbagian'],
									$rec['nmbagian'],
									$vale[5],
									$hrgbeli,
									$hrgbrg['hrgjual']
								));
				}
			}
		
			//UPDATE
			if(isset($_POST['regis'])){
				$dataArray = array(
					'tglkeluar' => date('Y-m-d'),
					'jamkeluar' => date('H:i:s'),
					/* 'idstkeluar' => $stkeluar,
					'idcarakeluar' => $carakeluar, */
				);
				$this->db->where('noreg', $_POST['noreg']);
				$z = $this->db->update('registrasidet', $dataArray);
		
				$query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg']));
				$reg = $query->row_array();
				$this->update_posisispasien($reg['idregdet']);
			}
		}
		
        return true;
    }
	
	function insert_pasienluar(){
        $this->db->trans_begin();
		if($_POST['tf_dokter'] != '') $reg['iddokter'] = $this->searchId('iddokter','dokter','nmdoktergelar',$_POST['tf_dokter']);
		else $reg['iddokter'] = null;
		$reg['idbagian'] = 11;
		$reg['idregdet'] = null;
		$_POST['nonota'] = $_POST['tf_nonota'];
		$_POST['nmshift'] = $_POST['tf_waktushift'];
		$_POST['catatan'] = $_POST['tf_catatannf'];
        //$_POST['diskon'] = $_POST['diskonr'];
        //$_POST['uangr'] = $_POST['uangr'];
        $_POST['noresep'] = $_POST['tf_noresep'];
        $_POST['stpelayanan'] = 'Umum';
		$_POST['tahun'] = 0;
		$_POST['jkel'] = null;
		$_POST['atasnama'] = $_POST['tf_nmpasien'];
		$total = str_replace(',', '', $_POST['tf_total2']);
		$_POST['total'] = $total;
		//$_POST['arrcarabayar'] = '["Tunai-undefined-undefined-'. $total .'"]';
	
	//kuitansi dan kuitansi detail
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['tf_nonota']));
		$nota = $query->row_array();
		
		$kque = $this->insert_kuitansi($nota);
		$kui = $kque['nokuitansi'];
	
	//nota
		if($query->num_rows() == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			//$nota = $query->row_array();
			$vnota = $nota['nonota'];
		}
		$arrnota = $_POST['arrnota'];
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $reg['idbagian']);
		
		if($kui && $nota && $notadet)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$vnota;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
	
		/* $dataArray = $this->getFieldsAndValuesPLuar($kui);
		$n =$this->db->insert('nota',$dataArray);
		$nota = $dataArray['nonota'];
	
	//notadet
		$where['nonota'] = $nota;
		$this->db->delete('notadet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrnota']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-',$val);
			$dataArray = $this->getFieldsAndValuesDet($nota, $reg, $vale[0], $vale[1], $vale[2], $vale[3]);
			$ndet =$this->db->insert('notadet',$dataArray);
		}
		
        if($n && $ndet && $kui)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$nota;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret); */
    }
	
	function insert_pelayanantambahan(){
        $this->db->trans_begin();
		if($_POST['tf_dokter'] != '') $reg['iddokter'] = $this->searchId('iddokter','dokter','nmdoktergelar',$_POST['tf_dokter']);
		else $reg['iddokter'] = null;
		$reg['idbagian'] = 35;
		$reg['idregdet'] = null;
		$_POST['nonota'] = $_POST['tf_nonota'];
		$_POST['nmshift'] = $_POST['tf_waktushift'];
		$_POST['catatan'] = $_POST['tf_catatan'];
        $_POST['diskon'] = null;
        $_POST['uangr'] = null;
        $_POST['noresep'] = null;
        $_POST['stpelayanan'] = 'Umum';
		$_POST['tahun'] = 0;
		$_POST['jkel'] = null;
		$_POST['atasnama'] = $_POST['tf_nmpasien'];
		$total = str_replace(',', '', $_POST['tf_total']);
		$_POST['total'] = $total;
		//$_POST['arrcarabayar'] = '["Tunai-undefined-undefined-'. $total .'"]';
	
	//kuitansi dan kuitansi detail
		$query = $this->db->getwhere('nota',array('nonota'=>$_POST['tf_nonota']));
		$nota = $query->row_array();
		
		$kque = $this->insert_kuitansi($nota);
		$kui = $kque['nokuitansi'];
	//nota
		if($query->num_rows() == 0){
			$nota = $this->insert_nota($reg, $kui);
			$vnota = $nota['nonota'];
		} else {
			$nota = $this->update_nota($reg, $kui);
			//$nota = $query->row_array();
			$vnota = $nota['nonota'];
		}
		$arrnota = $_POST['arrnota'];
		$notadet = $this->insert_notadet($vnota, $reg, $arrnota, $reg['idbagian']);
		
		if($kui && $nota && $notadet)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nonota"]=$vnota;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
	
	function test(){
		$query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg']));
		$reg = $query->row_array();
		
		$query = $this->db->getwhere('nota',array('idregdet'=>$reg['idregdet']));
		$nota = $query->row_array();
		
		$ret["success"] = true;
		$ret["nonota"]=$nota['nonota'];
		
		echo json_encode($ret);
    }

	function update_nota($reg, $kui){
		$dataArray = $this->getFieldsAndValues($reg, $kui);
		
		//UPDATE
		$this->db->where('nonota', $_POST['nonota']);
		$z = $this->db->update('nota', $dataArray); 
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function update_kuitansi($nota){
		$dataArray = $this->getFieldsAndValuesKui($nota);
		
		//UPDATE
		$this->db->where('nokuitansi', $dataArray['nokuitansi']);
		$kuitansi = $this->db->update('kuitansi', $dataArray);
		
		return $dataArray;
    }
	
	function update_kuitansidet($kuitansi){
		$where['nokuitansi'] = $kuitansi['nokuitansi'];
		$this->db->delete('kuitansidet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrcarabayar']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesKuiDet($kuitansi, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('kuitansidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function update_transfer(){
		$dataArray = array(
			'idregdettransfer' => $_POST['transfer']
		);
		$this->db->where('nonota', $_POST['nonota']);
		$z = $this->db->update('nota', $dataArray);
        if($z){
			$query = $this->db->getwhere('nota',array('nonota'=>$_POST['nonota']));
			$nokui = $query->row_array();
			
			$dataArrayx = array(
				'idcarabayar' => 9
			);
			$this->db->where('nokuitansi', $nokui['nokuitansi']);
			$y = $this->db->update('kuitansidet', $dataArrayx);
			
			$this->update_posisispasien($_POST['idtransfer']);
            $ret=$dataArray;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function update_posisispasien($idregdet){
		$dataArray = array(
			'idstposisipasien' => $_POST['stposisipasien']
		);
		$this->db->where('idregdet', $idregdet);
		$z = $this->db->update('reservasi', $dataArray); 
        if($z){
            $ret=$z;
        }else{
            $ret=false;
        }
        return $ret;
    }
	
	function getFieldsAndValuesNona($kui){
		$nr = $this->getNonotaakhir();
		$nostart = 'NA';
		$nomid = date('y');
		$noend = str_pad($nr, 8, "0", STR_PAD_LEFT);
		$nona = $nostart.$nomid.$noend;
		
		if(!isset($_POST['plafond'])) $_POST['plafond'] = 'false';
		if($_POST['plafond']=='true'){
			$plafond = $_POST['ttlplafond'];
			$stplafond = 1;
		} else {
			$plafond = 0;
			$stplafond = 0;
		}
			
		$dataArray = array(
             'nona'=> $nona,
             'tglna'=> date('Y-m-d'),
             'jamna'=> date('H:i:s'),
             'idsttransaksi'=> 1,
             'nokuitansi'=> $kui,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'total1'=> $_POST['total'],
             'userid'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d H:i:s'),
             'plafond'=> $plafond,
             'stplafond'=> $stplafond
        );		
		return $dataArray;
	}
	
	function getFieldsAndValuesKui($nota){
		if(!$nota){
			$nr = $this->getNokuitansi();
			$nostart = 'NK';
			$nomid = date('y');
			$noend = str_pad($nr, 8, "0", STR_PAD_LEFT);
			$nokuitansi = $nostart.$nomid.$noend;
		} else{
			$nokuitansi = $nota['nokuitansi'];
		}
		
		if(!isset($_POST['idregdet']))$idregdet=null;
		else $idregdet=$_POST['idregdet'];
		
		if(!isset($_POST['idjnskuitansi']))$idjnskuitansi=1;
		else $idjnskuitansi=$_POST['idjnskuitansi'];
		
		if(!isset($_POST['pembayaran'])) $_POST['pembayaran'] = $_POST['total'];
		
		$dataArray = array(
             'nokuitansi'=> $nokuitansi,
             'tglkuitansi'=> date('Y-m-d'),
             'jamkuitansi'=> date('H:i:s'),
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'idstkuitansi'=> 1,
             'idsbtnm'=> $this->searchSbtnm('idsbtnm','jsbtnm',$_POST['tahun'],$_POST['jkel']),
             'atasnama'=> $_POST['atasnama'],
             'idjnskuitansi'=> $idjnskuitansi,
             'pembulatan'=> 0,
             'total'=> $_POST['total'],
             'pembayaran'=> $_POST['pembayaran'],
             'idregdet'=> $idregdet,
             //'ketina'=> 0,
             //'keteng'=> 0
        );
		return $dataArray;
	}
			
	function getFieldsAndValuesKuiDet($kuitansi, $val0, $val1, $val2, $val3){
		if($val1 == 'undefined')$idbank = null;
		else $idbank = $this->searchId('idbank','bank','nmbank',$val1);
		if($val2 == 'undefined' || $val2 == ''|| $val2 == 'null')$nokartu = null;
		else $nokartu = $val2;
		$dataArray = array(
			 'nokuitansi'=> $kuitansi['nokuitansi'],
			 'idbank'=> $idbank,
			 'idcarabayar'=> $this->searchId('idcarabayar','carabayar','nmcarabayar',$val0),
			 'nokartu'=> $nokartu,
			 'jumlah'=> $val3
		);
		return $dataArray;
	}
	
	function getFieldsAndValues($reg, $kui){
		if(is_null($_POST['nonota']) || $_POST['nonota'] == '')
		{
			$nr = $this->getNonota();
			$nostart = 'N'.$_POST['ureg'];
			$nomid = date('y');
			$nombln = date('m');
			$noend = str_pad($nr, 5, "0", STR_PAD_LEFT);
			$nonota = $nostart.$nomid.$nombln.$noend;
		} else{
			$nonota = $_POST['nonota'];
		}
		
		if(!isset($_POST['diskon']))$_POST['diskon']=0;
		if(!isset($_POST['uangr']))$_POST['uangr']=0;
		if(!isset($_POST['stpelayanan']))$stpelayanan=null;
		else $stpelayanan=$this->searchId('idstpelayanan','stpelayanan','nmstpelayanan',$_POST['stpelayanan']);
		if(!isset($_POST['plafond'])) $_POST['plafond'] = 'false';
		if($_POST['plafond']=='true'){
			$plafond = $_POST['ttlplafond'];
			$stplafond = 1;
		} else {
			$plafond = 0;
			$stplafond = 0;
		}
		
		if(!isset($_POST['idbagianfar']))$idbagianfar=null;
		else $idbagianfar=$_POST['idbagianfar'];
		$dataArray = array(
             'nonota'=> $nonota,
             //'nona'=> $nonota,
             'tglnota'=> $_POST['tglnota'],
             'jamnota'=> date('H:i:s'),
             'idbagian'=> $reg['idbagian'],
             'idbagianfar'=> $idbagianfar,
             'iddokter'=> $reg['iddokter'],
             'idjnstransaksi'=> $_POST['jtransaksi'],
             'idsttransaksi'=> 1,
             'nokuitansi'=> $kui,
             'idregdet'=> $reg['idregdet'],
             'noorder'=> 1,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'noresep'=> $_POST['noresep'],
             'catatan'=> $_POST['catatan'],
             'diskon'=> $_POST['diskon'],
             'uangr'=> $_POST['uangr'],
             'userinput'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d'),
			 'idregdettransfer'=> null,
			 'idstpelayanan'=> $stpelayanan,
			 'plafond'=> $plafond,
			 'stplafond'=> $stplafond
        );
		//var_dump($_POST['catatan']);
		//exit;
		return $dataArray;
	}
	
	function getFieldsAndValuesDet($nota, $reg, $val0, $val1, $val2, $val3, $val4, $val5, $val6, $val7, $val8, $val9){
		if ($val2 == 'null') $val2 = null;
		if(!(int)$val4)$val4 = 0;
		if(!(int)$val5)$val5 = 0;
		if(!(int)$val6)$val6 = 0;
		if(!(int)$val7)$val7 = 0;
		
		if($val9>0)$stdijamin = 1;
		else $stdijamin = 0;
		
		$query = $this->db->getwhere('barang',array('kdbrg'=>$val0));
		$barang = $query->row_array();
		if($barang){
			$hrgjual = $barang['hrgjual'];
			$hrgbeli = $barang['hrgbeli'];
		}else{
			$hrgjual = null;
			$hrgbeli = null;
		}
		
		if($val2){
			$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'idbagian'=>11));
		} else {
			$strbr = substr($val0, 0, 1);
			if($strbr == 'B'){
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'idbagian'=>$reg['idbagian']));
			} elseif($strbr == 'T'){
				/* if($reg['idjnspelayanan'] != 2){
					$reg['idklstarif'] = '';
				} */
				if(!isset($reg['idklstarif'])) $reg['idklstarif'] = $_POST['idklstarif'];
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'klstarif'=>$reg['idklstarif']));
				if(!$query->row_array()){
					$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0, 'klstarif'=>5));
				}
			} else { 
				$query = $this->db->getwhere('v_tarifall',array('kditem'=>$val0));
			}
		}
		$item = $query->row_array();
		/* if(!$item){
			$query = $this->db->getwhere('tarif',array('kdpelayanan'=>$val0));
			$item = $query->row_array();
			$item['satuankcl'] = null;
		} */
		if(!isset($item['tarifjs']) || $item['tarifjs']=='')$item['tarifjs'] = 0;
		if(!isset($item['tarifjm']) || $item['tarifjm']=='')$item['tarifjm'] = 0;
		if(!isset($item['tarifjp']) || $item['tarifjp']=='')$item['tarifjp'] = 0;
		if(!isset($item['tarifbhp']) || $item['tarifbhp']=='')$item['tarifbhp'] = 0;
		
		$diskon = ($val1*((int)$item['tarifjs']+(int)$item['tarifjm']+(int)$item['tarifjp']+(int)$item['tarifbhp'])) - $val3;
		if(!isset($_POST['plafond'])) $_POST['plafond'] = 'false';
		if($_POST['plafond']=='true'){
			$val9 = ($item['tarifjs']+$item['tarifjm']+$item['tarifjp']+$item['tarifbhp']-$val4-$val5-$val6-$val7)*$val1;
			$stdijamin = 1;
		}
		
		if(isset($item['satuankcl'])) $item['satuankcl'] = $this->searchId('idsatuan','jsatuan','nmsatuan',$item['satuankcl']);
		else $item['satuankcl'] = null;
		$dataArray = array(
             'nonota'=> $nota,
             'kditem'=> $val0,
             'koder'=> $val2,
             //'idjnstarif'=> 0,
             //'kdresep'=> $_POST['kdnota'],
             'idsatuan'=> $item['satuankcl'],
             'qty'=> $val1,
             'tarifjs'=> $item['tarifjs'],
             'tarifjm'=> $item['tarifjm'],
             'tarifjp'=> $item['tarifjp'],
             'tarifbhp'=> $item['tarifbhp'],
             'diskonjs'=> $val4,
             'diskonjm'=> $val5,
             'diskonjp'=> $val6,
             'diskonbhp'=> $val7,
             //'uangr'=> 0,
             'iddokter'=> $val8,
             //'idperawat'=> 1,
             'hrgjual'=> $hrgjual,
             'hrgbeli'=> $hrgbeli,
             'dijamin'=> $val9,
             'stdijamin'=> $stdijamin,
			 //'idstbypass'=> 1
        );
		return $dataArray;
	}
	
	function getFieldsAndValuesPLuar($kui){
		if(is_null($_POST['tf_nonota']) || $_POST['tf_nonota'] == '')
		{
			$nr = $this->getNonota();
			$nostart = 'N'.$_POST['ureg'];
			$nomid = date('y');
			$nombln = date('m');
			$noend = str_pad($nr, 5, "0", STR_PAD_LEFT);
			$nonota = $nostart.$nomid.$nombln.$noend;
		} else {
			$nonota = $_POST['tf_nonota'];
		}
		$dataArray = array(
             'nonota'=> $nonota,
             'nona'=> $nonota,
             'tglnota'=> $_POST['tglnota'],
             'jamnota'=> date('H:i:s'),
             'idbagian'=> 11,
             'iddokter'=> $this->searchId('idshift','shift','nmshift',$_POST['tf_waktushift']),
             'idjnstransaksi'=> 8,
             'idsttransaksi'=> 1,
             'nokuitansi'=> $kui,
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['tf_waktushift']),
             'noresep'=> $_POST['tf_noresep'],
             'catatan'=> $_POST['tf_catatannf'],
             'diskon'=> $_POST['tf_diskonf'],
             'uangr'=> $_POST['tf_uangr'],
             'userinput'=> $this->session->userdata['user_id'],
             'tglinput'=> date('Y-m-d')
        );		
		return $dataArray;
	}
	
	function getNonota(){
        $this->db->select("count(nonota) AS max_np");
        $this->db->from("nota");
        $this->db->where('SUBSTRING(nonota,2,2)', $_POST['ureg']);
        $this->db->where('SUBSTRING(nonota,4,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function getNokuitansi(){
        $this->db->select("count(nokuitansi) AS max_np");
        $this->db->from("kuitansi");
        $this->db->where('SUBSTRING(nokuitansi,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}
	
	function getNonotaakhir(){
        $this->db->select("count(nona) AS max_np");
        $this->db->from("notaakhir");
        $this->db->where('SUBSTRING(nona,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}

	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function searchIdkhususdr($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->like($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function searchSbtnm($select, $tbl, $val, $val2){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($val .' BETWEEN ', 'dariumur AND sampaiumur', false);
        $this->db->where('idjnskelamin', $val2);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
	
	function tambahStok($where, $idbagian){
        $query = $this->db->getwhere('notadet',$where);
		$ndold = $query->result_array();
		for($i=0;$i<$query->num_rows();$i++){
			$strbr = substr($ndold[$i]['kditem'], 0, 1);
			if($strbr == 'B'){
				($ndold[$i]['koder']) ? $idbag = 11 : $idbag = $idbagian;
				$query = $this->db->getwhere('barangbagian',array('kdbrg'=>$ndold[$i]['kditem'], 'idbagian'=>$idbag));
				$brgbagian = $query->row_array();
				$qty = $brgbagian['stoknowbagian'] + $ndold[$i]['qty'];
				
				$dataArray = array('stoknowbagian'=> $qty);
				$this->db->where('idbagian', $idbag);
				$this->db->where('kdbrg', $ndold[$i]['kditem']);
				$z = $this->db->update('barangbagian', $dataArray);
			}
		}
		return true;
    }
	
	function kurangStok($idbagian, $kdbrg, $qtynow, $koder){
		$strbr = substr($kdbrg, 0, 1);
		if($strbr == 'B'){
			($koder != 'null') ? $idbag = 11 : $idbag = $idbagian;
			$query = $this->db->getwhere('barangbagian',array('kdbrg'=>$kdbrg, 'idbagian'=>$idbag));
			$brgbagian = $query->row_array();
			$qty = $brgbagian['stoknowbagian'] - $qtynow;
			
			$dataArray = array('stoknowbagian'=> $qty);
			$this->db->where('idbagian', $idbag);
			$this->db->where('kdbrg', $kdbrg);
			$z = $this->db->update('barangbagian', $dataArray);
		}
		return true;
    }
	
	function getCekTransfer(){
		$this->db->select('kuitansi.total, nota.idregdet');
        $this->db->from('nota');
		$this->db->join('kuitansi',
                'kuitansi.nokuitansi = nota.nokuitansi', 'left');
        $this->db->where('idregdettransfer', $_POST['noreg']);
		$q = $this->db->get();
		$total = $q->row_array();
		
		if(!$total){
			$total['total']=0;
			$total['idregdet']=null;
		}
		
		echo json_encode($total);
    }
}
