<?php

class Vlaprj_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_vlaprjperiode(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        $iddokter		= $this->input->post("iddokter");
        $bagian			= $this->input->post("bagian");
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
      
        /* $this->db->select("*, (ucc+utotal) AS total");
        $this->db->from("v_laprjperiode"); */
		$this->db->select("
			r.noreg AS noreg,
			rd.tglreg AS tglreg,
			TRIM(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			(
				SELECT sum(t.tarifbhp*t.qty)
				FROM
				  notadet t
				WHERE
				  ((t.nonota = n.nonota)
				  AND (t.kditem LIKE 'B%'))
			) AS uobat,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS upemeriksaan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000001'
				  AND t.nonota = n.nonota
			) AS upemeriksaan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS utindakan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000048'
				  AND t.nonota = n.nonota
			) AS utindakan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS uimun
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000009'
				  AND t.nonota = n.nonota
			) AS uimun,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulab
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000037'
				  AND t.nonota = n.nonota
			) AS ulab,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulain
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000073'
				  AND t.nonota = n.nonota
			) AS ulain,
			n.uangr AS uracik,
			sum(nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp) + n.diskon AS udiskon,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar <> 1
			) AS ucc,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar = 1
			) AS utotal,
			b.nmbagian AS nmbagian,
			b.idbagian AS idbagian,
			d.nmdoktergelar AS nmdoktergelar,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
			) AS total
		", false);
        $this->db->from("registrasi r", false);
		$this->db->join('pasien p',
					'p.norm = r.norm', 'left', false);
		$this->db->join('registrasidet rd',
					'rd.noreg = r.noreg', 'left', false);
		$this->db->join('bagian b',
					'b.idbagian = rd.idbagian', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = rd.iddokter', 'left', false);
		$this->db->join('nota n',
					'n.idregdet = rd.idregdet', 'left', false);
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->where('rd.userbatal IS NULL', null, false);
		$this->db->where('b.idjnspelayanan', 1);
		$this->db->groupby('r.noreg');
		
		if($bagian)$this->db->where('rd.idbagian', $bagian);
		if($iddokter)$this->db->where('rd.iddokter', $iddokter);
		if($tglakhir){
			$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow_vlaprjperiode();
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow_vlaprjperiode(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
		
        $iddokter		= $this->input->post("iddokter");
        $bagian			= $this->input->post("bagian");
        $tglawal		= $this->input->post("tglawal");
        $tglakhir		= $this->input->post("tglakhir");
      
        $this->db->select("
			r.noreg AS noreg,
			rd.tglreg AS tglreg,
			TRIM(LEADING '0' FROM p.norm) AS norm,
			p.nmpasien AS nmpasien,
			(
				SELECT sum(t.tarifbhp*t.qty)
				FROM
				  notadet t
				WHERE
				  ((t.nonota = n.nonota)
				  AND (t.kditem LIKE 'B%'))
			) AS uobat,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS upemeriksaan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000001'
				  AND t.nonota = n.nonota
			) AS upemeriksaan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS utindakan
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000048'
				  AND t.nonota = n.nonota
			) AS utindakan,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS uimun
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000009'
				  AND t.nonota = n.nonota
			) AS uimun,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulab
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000037'
				  AND t.nonota = n.nonota
			) AS ulab,
			(
				SELECT sum((t.tarifjs+t.tarifjm+t.tarifjp+t.tarifbhp)*t.qty) AS ulain
				FROM
				  notadet t, pelayanan p
				WHERE
				  t.kditem = p.kdpelayanan
				  AND p.pel_kdpelayanan = 'T000000073'
				  AND t.nonota = n.nonota
			) AS ulain,
			n.uangr AS uracik,
			sum(nd.diskonjs + nd.diskonjm + nd.diskonjp + nd.diskonbhp + n.diskon) AS udiskon,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar <> 1
			) AS ucc,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
				  AND kd.idcarabayar = 1
			) AS utotal,
			b.nmbagian AS nmbagian,
			b.idbagian AS idbagian,
			d.nmdoktergelar AS nmdoktergelar,
			(
				SELECT sum(kd.jumlah)
				FROM
				  kuitansidet kd
				WHERE
				  kd.nokuitansi = k.nokuitansi
			) AS total
		", false);
        $this->db->from("registrasi r", false);
		$this->db->join('pasien p',
					'p.norm = r.norm', 'left', false);
		$this->db->join('registrasidet rd',
					'rd.noreg = r.noreg', 'left', false);
		$this->db->join('bagian b',
					'b.idbagian = rd.idbagian', 'left', false);
		$this->db->join('dokter d',
					'd.iddokter = rd.iddokter', 'left', false);
		$this->db->join('nota n',
					'n.idregdet = rd.idregdet', 'left', false);
		$this->db->join('notadet nd',
					'nd.nonota = n.nonota', 'left', false);
		$this->db->join('kuitansi k',
					'k.nokuitansi = n.nokuitansi', 'left', false);
		$this->db->where('rd.userbatal IS NULL', null, false);
		$this->db->where('b.idjnspelayanan', 1);
		$this->db->groupby('r.noreg');
		
		if($bagian) $this->db->where('rd.idbagian', $bagian);
		if($iddokter)$this->db->where('rd.iddokter', $iddokter);
		if($tglakhir){
			$this->db->where('k.tglkuitansi BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function cekdktr_lapkeurj(){
        $idbagian		= $this->input->post("bagian");
		
		$this->db->select("*");
        $this->db->from("jadwalpraktek");
		$this->db->join('bagian',
                'bagian.idbagian = jadwalpraktek.idbagian', 'left');
 		$this->db->join('dokter',
                'dokter.iddokter = jadwalpraktek.iddokter', 'left');
		$this->db->join('hari',
                'hari.idhari = jadwalpraktek.idhari', 'left');
		$this->db->join('spesialisasi',
                'spesialisasi.idspesialisasi = dokter.idspesialisasi', 'left');		
		if($idbagian)$this->db->where("jadwalpraktek.idbagian",$idbagian);
		$this->db->groupby('nmdoktergelar');
		$this->db->order_by('nmdoktergelar');
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
	}
}
