<?php

class Kuitansi_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_kuitansi(){
        $start			= $this->input->post("start");
        $limit			= $this->input->post("limit");
        
        $fields			= $this->input->post("fields");
        $query			= $this->input->post("query");
        $idregdet		= $this->input->post("idregdet");
        $noreg			= $this->input->post("noreg");
      
        $this->db->select("*");
        $this->db->from("kuitansi");
        $this->db->join("kuitansidet",
				"kuitansidet.nokuitansi = kuitansi.nokuitansi", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = kuitansi.idregdet", "left");
        $this->db->join("stkuitansi",
				"stkuitansi.idstkuitansi = kuitansi.idstkuitansi", "left");
		if($idregdet)$this->db->where("kuitansi.idregdet", $idregdet);
		if($noreg)$this->db->where("registrasidet.noreg", $noreg);
		$this->db->where("kuitansi.idstkuitansi", 1);
		$this->db->group_by("kuitansi.nokuitansi,kuitansi.tglkuitansi");
		$this->db->orderby("tglkuitansi desc");
		
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
                
        if ($start!=null){
            $this->db->limit($limit,$start);
        }else{
            $this->db->limit(50,0);
        }
        
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->numrow($fields, $query);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function numrow($fields, $query){
        $idregdet		= $this->input->post("idregdet");
        $noreg			= $this->input->post("noreg");
      
        $this->db->select("*");
        $this->db->from("kuitansi");
        $this->db->join("kuitansidet",
				"kuitansidet.nokuitansi = kuitansi.nokuitansi", "left");
        $this->db->join("registrasidet",
				"registrasidet.idregdet = kuitansi.idregdet", "left");
        $this->db->join("stkuitansi",
				"stkuitansi.idstkuitansi = kuitansi.idstkuitansi", "left");
		if($idregdet)$this->db->where("kuitansi.idregdet", $idregdet);
		if($noreg)$this->db->where("registrasidet.noreg", $noreg);
		$this->db->where("kuitansi.idstkuitansi", 1);
		$this->db->group_by("kuitansi.nokuitansi,kuitansi.tglkuitansi");
		$this->db->orderby("tglkuitansi desc");
        
        if($query !=""){
            $k=array('[',']','"');
            $r=str_replace($k, '', $fields);
            $b=explode(',', $r);
            $c=count($b);
            for($i=0;$i<$c;$i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
        
        $q = $this->db->get();
        
        return $q->num_rows();
    }
	
	function delete_kuitansi(){
        $this->db->trans_begin();
		
		$dataArray = array(
			'idstkuitansi' => 2
		);
		$this->db->where('nokuitansi', $_POST['nokuitansi']);
		$z = $this->db->update('kuitansi', $dataArray);
		
		
		if($z)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
        return $ret;
    }
		
	function insert_kuitansi(){
        $this->db->trans_begin();
		$query = $this->db->getwhere('kuitansi',array('nokuitansi'=>$_POST['nokuitansi']));
		$kuitansi = $query->row_array();
		if($query->num_rows() == 0){
			$dataArray = $this->getFieldsAndValues();
			$kuitansi = $this->db->insert('kuitansi',$dataArray);
		} else {
			$kuitansi = $this->update_kuitansi();
			$dataArray = $kuitansi;
		}
		
		$query = $this->db->getwhere('kuitansidet',array('nokuitansi'=>$kuitansi['nokuitansi']));
		$kuitansidet = $query->row_array();
		if($query->num_rows() == 0){
			$kuitansidet = $this->insert_kuitansidet($dataArray);
		} else {
			$kuitansidet = $this->update_kuitansidet($kuitansi);
		}
		
		$nota = $this->update_nota($dataArray);
		
		if($kuitansi && $kuitansidet && $nota)
		{
            $this->db->trans_commit();
			$ret["success"] = true;
            $ret["nokuitansi"]=$dataArray['nokuitansi'];
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
    }
		
	function insert_kuitansidet($kuitansi){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrcarabayar']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($kuitansi, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('kuitansidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
        return $ret;
    }
	
	function test(){
		$query = $this->db->getwhere('v_registrasi',array('noreg'=>$_POST['noreg']));
		$reg = $query->row_array();
		
		$query = $this->db->getwhere('kuitansi',array('idregdet'=>$reg['idregdet']));
		$kuitansi = $query->row_array();
		
		$ret["success"] = true;
		$ret["nokuitansi"]=$kuitansi['nokuitansi'];
		
		echo json_encode($ret);
    }

	function update_kuitansi(){
		$dataArray = $this->getFieldsAndValues();
		
		//UPDATE
		$this->db->where('nokuitansi', $dataArray['nokuitansi']);
		$kuitansi = $this->db->update('kuitansi', $dataArray);
		
		return $dataArray;
    }
	
	function update_kuitansidet($kuitansi){
		$where['nokuitansi'] = $kuitansi['nokuitansi'];
		$this->db->delete('kuitansidet',$where);
		
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrcarabayar']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$vale = explode('-', $val);
			$dataArray = $this->getFieldsAndValuesDet($kuitansi, $vale[0], $vale[1], $vale[2], $vale[3]);
			$z =$this->db->insert('kuitansidet',$dataArray);
		}
        if($z){
            $ret=$dataArray;
        }else{
            $ret=false;
        }
		
		return $ret;
    }
	
	function update_nota($kuitansi){
		$k=array('[',']','"');
        $r=str_replace($k, '', $_POST['arrnota']);
		$b=explode(',', $r);
		foreach($b AS $val){
			$this->db->where('nonota', $val);
			$z =$this->db->update('nota', array('nokuitansi'=>$kuitansi['nokuitansi']));
		}
		
		if($z){
            $ret=true;
        }else{
            $ret=false;
        }
		return $ret;
    }

	function getFieldsAndValues(){
		if($_POST['nokuitansi'] == '')
		{
			$nr = $this->getNokuitansi();
			$nostart = 'NK';
			$nomid = date('y');
			$noend = str_pad($nr, 8, "0", STR_PAD_LEFT);
			$nokuitansi = $nostart.$nomid.$noend;
		} else{
			$nokuitansi = $_POST['nokuitansi'];
		}
		
		$dataArray = array(
             'nokuitansi'=> $nokuitansi,
             'tglkuitansi'=> date('Y-m-d'),
             'jamkuitansi'=> date('H:i:s'),
             'idshift'=> $this->searchId('idshift','shift','nmshift',$_POST['nmshift']),
             'idstkuitansi'=> 1,
             'idsbtnm'=> $this->searchId('idsbtnm','jsbtnm','kdsbtnm',$_POST['kdsbtnm']),
             'atasnama'=> $_POST['atasnama'],
             'idjnskuitansi'=> 1,
             'pembulatan'=> $_POST['pembulatan'],
             'total'=> $_POST['total'],
             //'ketina'=> 0,
             //'keteng'=> 0
        );		
		return $dataArray;
	}
			
	function getFieldsAndValuesDet($kuitansi, $val0, $val1, $val2, $val3){
		if($val1 == 'undefined')$idbank = null;
		else $idbank = $this->searchId('idbank','bank','nmbank',$val1);
		$dataArray = array(
			 'nokuitansi'=> $kuitansi['nokuitansi'],
			 'idbank'=> $idbank,
			 'idcarabayar'=> $this->searchId('idcarabayar','carabayar','nmcarabayar',$val0),
			 'jumlah'=> $val3
		);
		return $dataArray;
	}
	
	function getNokuitansi(){
        $this->db->select("count(nokuitansi) AS max_np");
        $this->db->from("kuitansi");
        $this->db->where('SUBSTRING(nokuitansi,3,2)', date('y'));
		$q = $this->db->get();
		$data = $q->row_array();
		if(is_null($data['max_np'])) $max = 1;
		else $max = $data['max_np'] + 1;
		return $max;
	}

	function searchId($select, $tbl, $where, $val){
        $this->db->select($select);
        $this->db->from($tbl);
        $this->db->where($where,$val);
		$q = $this->db->get();
		$id = $q->row_array();
		if($q->num_rows()<1){
			$id[$select] = null;
		}
		return $id[$select];
    }
}
