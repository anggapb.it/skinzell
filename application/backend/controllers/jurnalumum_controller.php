<?php

class Jurnalumum_Controller extends Controller {
  private $idjnsjurnal = 1; // 1 = jurnal umum
	private $idbagian = 47; // 47 = idbagian keuangan
	
  public function __construct()
  {
    parent::Controller();
		$this->load->library('session');
		#$this->load->library('rhlib');
  }

	/* ======================= Jurnal Umum ================================= */
	function get_jurnal_umum()
	{
		$start = $this->input->post("start");
    $limit = $this->input->post("limit");
		$orderby = $this->input->post("orderby");
		$order = $this->input->post("order");
		$fields = $this->input->post("fields");
    $query = $this->input->post("query");
    $tglawal = $this->input->post("tglawal");
    $tglakhir = $this->input->post("tglakhir");
    $searchkey = $this->input->post("searchkey");
    $searchvalue = $this->input->post("searchvalue");

		$start = (!empty($start)) ? $start : 0;
		$limit = (!empty($limit)) ? $limit : 50;
		$orderby = (!empty($orderby)) ? $orderby : 'kdjurnal';
		$order = (!empty($order)) ? $order : 'DESC';
		
		$this->db->select('*');
		$this->db->from('v_jurnalumum');
		
		$this->db->limit($limit, $start);
		$this->db->orderby($orderby, $order);
		
		
		if(!empty($tglawal) && !empty($tglakhir))
		{
			if($tglawal == $tglakhir){
				$this->db->where('tgltransaksi', $tglawal);
			}else{
				$this->db->where('tgltransaksi >=', $tglawal);
				$this->db->where('tgltransaksi <=', $tglakhir); 
			}
		}
		
		if(!empty($searchkey) && !empty($searchvalue)){
			if($searchkey == 'tgljurnal' && strpos($searchvalue,'/') !== false){
				//change date format
				$exp_date = explode('/', $searchvalue);
				$searchvalue = $exp_date[2].'-'.$exp_date[1].'-'.$exp_date[0];
			}
		
			$this->db->like($searchkey, $searchvalue);
		}
		
		//standard filter 
        /*
		if($fields!="" || $query !=""){
            $k = array('[',']','"');
            $r = str_replace($k, '', $fields);
            $b = explode(',', $r);
            $c = count($b);
            for($i = 0; $i < $c; $i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		*/
		
		$get = $this->db->get();
        $data = array();
        if ($get->num_rows() > 0) {
            $data = $get->result();
        }
		
        $total = $this->numrow($fields, $query, $tglawal, $tglakhir, $searchkey, $searchvalue);
        $build_array = array ("success" => true, "results" => $total, "data" => $data);
		
        echo json_encode($build_array);
		die();
	}
	
	//hitung jumlah data di  v_jurnalumum
	function numrow($fields, $query, $tglawal, $tglakhir, $searchkey, $searchvalue)
  {
		
		$this->db->select('*');
		$this->db->from('v_jurnalumum');
		
		if(!empty($tglawal) && !empty($tglakhir))
		{
			if($tglawal == $tglakhir){
				$this->db->where('tgltransaksi', $tglawal);
			}else{
				$this->db->where('tgltransaksi >=', $tglawal);
				$this->db->where('tgltransaksi <=', $tglakhir); 
			}
		}
		
		if(!empty($searchkey) && !empty($searchvalue)){
			$this->db->like($searchkey, $searchvalue);
		}
		
		//standard filter 
        /*
		if($fields!="" || $query !=""){
            $k = array('[',']','"');
            $r = str_replace($k, '', $fields);
            $b = explode(',', $r);
            $c = count($b);
            for($i = 0; $i < $c; $i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		*/
		
		$get = $this->db->get();
		return $get->num_rows();
	}

	function insert_jurnal_umum()
	{
		$kdjurnal           = $this->getKdjurnalUmum();
		$idjnsjurnal        = $this->idjnsjurnal;
		#$idbagian			= $this->input->post('idbagian');
		$idbagian			= $this->idbagian; // idbagian keuangan
		$tgltransaksi		= $this->input->post('tgltransaksi');
		$tgljurnal			= $this->input->post('tgljurnal');
		$keterangan			= $this->input->post('keterangan');
		$noreff				= $this->input->post('noreff');
		$userid				= $this->input->post('userid');
		$nokasir			= $this->input->post('nokasir');
		$nominal			= $this->input->post('nominal');
		$arrjurnal			= $this->input->post('arrjurnal');
		$arrjurnal 			= json_decode($arrjurnal);
		
		$ins_jurnal = true;
		$ins_jurnal_det = true;
		
		//get tahun
		$exp_tgljurnal = explode('-', $tgljurnal);
		$tahun = $exp_tgljurnal[0];

		//start transaction
		$this->db->trans_begin();
		
		//insert jurnal 
		$do_insert = $this->db->insert('jurnal', array(
			'kdjurnal'      => $kdjurnal,
			'idjnsjurnal'   => $idjnsjurnal,
			'idbagian'      => $idbagian,
			'tgltransaksi'  => $tgltransaksi,
			'tgljurnal'     => $tgljurnal,
			'keterangan'    => $keterangan,
			'noreff'        => $noreff,
			'userid'        => $userid,
      'nominal'       => $nominal,
      'status_posting' => '1',
		));
		if($this->db->affected_rows() != 1) $ins_jurnal = false; //if insert failed, set to false
		
		//insert jurnal det
		if(!empty($arrjurnal))
		{
			foreach($arrjurnal as $idx => $jurnaldet)
			{
				/*
					$jdet_data[0] = idakun;
          $jdet_data[1] = noreff;
					$jdet_data[2] = debit;
					$jdet_data[3] = kredit;
				*/
				$jdet_data = explode('-', $jurnaldet);
				
				$data_ju_det = array(
					'kdjurnal' => $kdjurnal,
          'tahun' => $tahun,
          'idakun' => $jdet_data[0],
					'noreff' => $jdet_data[1],
					'debit' => $jdet_data[2],
					'kredit' => $jdet_data[3],
				);
				$this->db->insert('jurnaldet', $data_ju_det);
				if($this->db->affected_rows() != 1) $ins_jurnal_det = false; //if insert failed, set to false
				
			}
		}else{
			$ins_jurnal_det = false;
		}
		
		if($ins_jurnal && $ins_jurnal_det){
			$this->db->trans_commit();
			$ret["success"] = true;
      $ret["kdjurnal"]=$kdjurnal;
		}else{
      $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
		die;
	}
	
	function update_jurnal_umum()
	{
		$kdjurnal			  = $this->input->post('kdjurnal');
		$idjnsjurnal    = $this->idjnsjurnal;
		$idbagian			  = $this->idbagian; // idbagian keuangan
		$tgltransaksi		= $this->input->post('tgltransaksi');
		$tgljurnal			= $this->input->post('tgljurnal');
		$keterangan			= $this->input->post('keterangan');
		$noreff				  = $this->input->post('noreff');
		$userid				  = $this->input->post('userid');
		$nokasir			  = $this->input->post('nokasir');
		$nominal			  = $this->input->post('nominal');
		$arrjurnal			= $this->input->post('arrjurnal');
		$arrjurnal 			= json_decode($arrjurnal);
		
		$update_jurnal_det = true;
		
		//get tahun
		$exp_tgljurnal = explode('-', $tgljurnal);
		$tahun = $exp_tgljurnal[0];
		
		//start transaction
		$this->db->trans_begin();
		
		//insert jurnal 
		$do_update = $this->db->update('jurnal', array(
			'idjnsjurnal'   => $idjnsjurnal,
			'idbagian'      => $idbagian,
			'tgltransaksi'  => $tgltransaksi,
			'tgljurnal'     => $tgljurnal,
			'keterangan'    => $keterangan,
			'noreff'        => $noreff,
			'userid'        => $userid,
			'nominal'       => $nominal,
		), array('kdjurnal' => $kdjurnal));
		
		//delete the old jurnal det
		$delete_old_data = $this->db->delete('jurnaldet', array('kdjurnal' => $kdjurnal)); 
		if(! $delete_old_data) $update_jurnal_det = false; 
		
		//insert jurnal det
		if(!empty($arrjurnal))
		{
			foreach($arrjurnal as $idx => $jurnaldet)
			{
				/*
					$jdet_data[0] = idakun;
					$jdet_data[1] = debit;
					$jdet_data[2] = kredit;
				*/
				$jdet_data = explode('-', $jurnaldet);
				
				$data_ju_det = array(
					'kdjurnal' => $kdjurnal,
					'tahun' => $tahun,
          'idakun' => $jdet_data[0],
					'noreff' => $noreff,
					'debit' => $jdet_data[1],
					'kredit' => $jdet_data[2],
				);
				$this->db->insert('jurnaldet', $data_ju_det);
				if($this->db->affected_rows() != 1) $update_jurnal_det = false; //if insert failed, set to false
				
			}
		}else{
			$update_jurnal_det = false;
		}
		
		if($update_jurnal_det){
			$this->db->trans_commit();
			$ret["success"] = true;
            $ret["kdjurnal"]=$kdjurnal;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
		die;
	}
	
	function delete_jurnal_umum()
	{
		$kdjurnal			= $this->input->post('kdjurnal');
		$delete_jurnal      = true;
		$delete_jurnal_det  = true;
		
		//start transaction
		$this->db->trans_begin();
		
		$delete_detail = $this->db->delete('jurnaldet', array('kdjurnal' => $kdjurnal)); 
		if(! $delete_detail) $delete_jurnal_det = false; 
		
		$delete = $this->db->delete('jurnal', array('kdjurnal' => $kdjurnal)); 
		if(! $delete) $delete_jurnal = false; 
		
		
		if($delete && $delete_detail){
			$this->db->trans_commit();
			$ret["success"] = true;
            $ret["kdjurnal"] = $kdjurnal;
		}else{
            $this->db->trans_rollback();
			$ret["success"] = false;
		}
		
		echo json_encode($ret);
		die;
	}
	
	/* ===================== Detail Jurnal Umum ============================ */
	function get_jurnal_umum_det()
	{
		$kdjurnal = $this->input->post("kdjurnal");
		$start = $this->input->post("start");
    $limit = $this->input->post("limit");
		$orderby = $this->input->post("orderby");
		$order = $this->input->post("order");
		$fields = $this->input->post("fields");
    $query = $this->input->post("query");

		$kdjurnal = (!empty($kdjurnal)) ? $kdjurnal : '';
		$start = (!empty($start)) ? $start : 0;
		$limit = (!empty($limit)) ? $limit : 50;
		$orderby = (!empty($orderby)) ? $orderby : 'kdjurnal';
		$order = (!empty($order)) ? $order : 'DESC';
		
		
		$this->db->select('*');
		$this->db->from('v_jurnalumum_det');
		
		$this->db->where('kdjurnal', $kdjurnal);
		$this->db->limit($limit, $start);
		$this->db->orderby($orderby, $order);
		
		//standard filter 
        if($fields!="" || $query !=""){
            $k = array('[',']','"');
            $r = str_replace($k, '', $fields);
            $b = explode(',', $r);
            $c = count($b);
            for($i = 0; $i < $c; $i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
		$get = $this->db->get();
        $data = array();
        if ($get->num_rows() > 0) {
            $data = $get->result();
        }
		
        $total = $this->numrow_det($fields, $query, $kdjurnal);
        $build_array = array ("success" => true, "results" => $total, "data" => $data);
		
        echo json_encode($build_array);
		die();
	}
	
	function numrow_det($fields, $query, $kdjurnal)
  {
		
		$this->db->select('*');
		$this->db->from('v_jurnalumum_det');
		
		$this->db->where('kdjurnal', $kdjurnal);
		
		//standard filter 
        if($fields!="" || $query !=""){
            $k = array('[',']','"');
            $r = str_replace($k, '', $fields);
            $b = explode(',', $r);
            $c = count($b);
            for($i = 0; $i < $c; $i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
		$get = $this->db->get();
		return $get->num_rows();
	}

	
	/* ===================== Akun Jurnal ============================ */
  function akun_jurnal()
  {
    $start = $this->input->post("start");
    $limit = $this->input->post("limit");
    $orderby = $this->input->post("orderby");
    $order = $this->input->post("order");
    $fields = $this->input->post("fields");
    $query = $this->input->post("query");

    $kdjurnal = (!empty($kdjurnal)) ? $kdjurnal : '';
    $start = (!empty($start)) ? $start : 0;
    $limit = (!empty($limit)) ? $limit : 20;
    $orderby = (!empty($orderby)) ? $orderby : 'idakun';
    $order = (!empty($order)) ? $order : 'ASC';
    
    $this->db->select('*');
    $this->db->from('v_akun_jurnal');
    
    //$this->db->limit($limit, $start);
    $this->db->orderby($orderby, $order);
    
    //standard filter 
        if($fields!="" || $query !=""){
            $k = array('[',']','"');
            $r = str_replace($k, '', $fields);
            $b = explode(',', $r);
            $c = count($b);
            for($i = 0; $i < $c; $i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
    
    $get = $this->db->get();
        $data = array();
        if ($get->num_rows() > 0) {
            $data = $get->result();
        }
    
        $total = $this->numrow_akun($fields, $query);
        $build_array = array ("success" => true, "results" => $total, "data" => $data);
    
        echo json_encode($build_array);
    die();
  }

  function akun_pembayaran_supplier()
  {
    //$get = $this->db->query("SELECT * FROM v_akun_jurnal WHERE kdakun like '11%' order by kdakun asc ");
    $get = $this->db->query("SELECT * FROM v_akun_jurnal WHERE kdakun like '11%' AND kdakun not in('11000', '11350', '11200') order by kdakun asc ");
    $data = array();
    if ($get->num_rows() > 0) {
      $data = $get->result();
    }
    
    $total = count($data);
    $build_array = array ("success" => true, "results" => $total, "data" => $data);
  
    echo json_encode($build_array);
    die();
  }
	
	function numrow_akun($fields, $query){
		
		$this->db->select('*');
		$this->db->from('v_akun_jurnal');
		
		//standard filter 
        if($fields!="" || $query !=""){
            $k = array('[',']','"');
            $r = str_replace($k, '', $fields);
            $b = explode(',', $r);
            $c = count($b);
            for($i = 0; $i < $c; $i++){
                $d[$b[$i]]=$query;
            }
            $this->db->or_like($d, $query);
        }
		
		$get = $this->db->get();
		return $get->num_rows();
		
	}

	function getKdjurnalUmum(){
		$q = "SELECT getOtoNojurnalUmum(now()) as nm;";
        $query  = $this->db->query($q);
		$nm= ''; 
        if ($query->num_rows() != 0)
        {
            $row = $query->row();
            $nm=$row->nm;
        }
        return $nm;
	}
}
