<?php

class Lappengeluaranbrg_Controller extends Controller {
    public function __construct()
    {
        parent::Controller();
			$this->load->library('session');
			$this->load->library('rhlib');
    }
	
	function get_lappengeluaranbrg(){
        $tglawal                = $this->input->post("tglawal");
        $tglakhir               = $this->input->post("tglakhir");
		$idbagian				= $_POST['idbagian'];
		
        $this->db->select('*');
        $this->db->from('v_lappengeluaranbrg');
		
		if($tglakhir){
			$this->db->where('`tglkeluar` BETWEEN ', "'". $tglawal ."' AND '". $tglakhir ."'", false);
		}
		
		$where = array();
		$where['v_lappengeluaranbrg.idbagiandari']=$idbagian;
		
		/* if ($key=='1'){
			$id     = $_POST["id"];
			$name   = $_POST["name"];
			$this->db->or_like($id, $name);
		} */
		
		$this->db->where($where);
		
        $q = $this->db->get();
        $data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
        $ttl = $this->db->count_all('v_lappengeluaranbrg');
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
	
	function get_pengbagian(){		
		$userid 				= $this->input->post("userid");
      
        $this->db->select("penggunabagian.idbagian, bagian.nmbagian, pengguna.userid, pengguna.nmlengkap");
        $this->db->from("penggunabagian");		
		$this->db->join('bagian',
                'bagian.idbagian = penggunabagian.idbagian', 'left');
		$this->db->join('pengguna',
                'pengguna.userid = penggunabagian.userid', 'left');
		
		$this->db->order_by('penggunabagian.idbagian DESC');
		
        $where = array();
		$where['penggunabagian.userid']=$userid;
		$this->db->where($where);
		
		$q = $this->db->get();
		$data = array();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }
		
		$ttl = count($data);
        $build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());

        if($ttl>0){
            $build_array["data"]=$data;
        }
		
        echo json_encode($build_array);
    }
}
