<?php  
	class Dashboard_Controller extends Controller{
		public function __construct()
		{
			parent::Controller();
				$this->load->library('session');
				$this->load->library('rhlib');
		}
		
		function count_pasien(){
			$this->db->select("count(norm) as norm");
			$this->db->from("pasien");
			$q = $this->db->get();
			$norm = $q->row_array();
			echo json_encode($norm);
		}
		function count_dokter(){
			$this->db->select("count(iddokter) as iddokter");
			$this->db->from("dokter");
			$q = $this->db->get();
			$dokt = $q->row_array();
			echo json_encode($dokt);
		}
		function count_perawat(){
			$this->db->select("count(idperawat) as idperawat");
			$this->db->from("perawat");
			$q = $this->db->get();
			$perawat = $q->row_array();
			echo json_encode($perawat);
		}
		function count_bed(){
			$this->db->select("count(idbed) as idbed");
			$this->db->from("bed");
			$q = $this->db->get();
			$bed = $q->row_array();
			echo json_encode($bed);
		}
		function count_bed_isi(){
			$this->db->select("v_bed_isi.idbed");
			$this->db->from("v_bed_isi");
			//$this->db->where("bed.idstbed",2);
			$q = $this->db->get();
			$bed = $q->row_array();
			echo json_encode($bed);
		}
		function count_bed_kosong(){
			$this->db->select("v_bed_kosong.idbed as idbedkosong");
			$this->db->from("v_bed_kosong");
			//$this->db->where("bed.idstbed",2);
			$q = $this->db->get();
			$bed = $q->row_array();
			echo json_encode($bed);
		}
		function pie_pasien(){
			$sql = $this->db->query("SELECT DISTINCT (EXTRACT( YEAR FROM tgldaftar )) as tahundaftar, count(norm) as norm from pasien GROUP BY tahundaftar");
			$num = $sql->num_rows();
			
			if($num>0){
			//	return $sql->result();
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);
			}else{
				return 0;
			}
		}
		function pie_tmedis(){
			$sql = $this->db->query("SELECT DISTINCT count(dokter.kddokter) as kddokter, jtenagamedis.nmjnstenagamedis from dokter 
									left join jtenagamedis
									on jtenagamedis.idjnstenagamedis = dokter.idjnstenagamedis
									where dokter.idjnstenagamedis <> ''
									AND dokter.idstatus = 1
									GROUP BY jtenagamedis.nmjnstenagamedis
									ORDER BY jtenagamedis.idjnstenagamedis");
			$num = $sql->num_rows();
			
			if($num>0){
			//	return $sql->result();
				$arr = array('data'=>$sql->result());
				echo json_encode($arr);
			}else{
				return 0;
			}
		}
		function chart_pasien_rj(){
			$tahun = $_POST['tahun'];
			$query = $this->db->query("SELECT DISTINCT (
                extract(YEAR
                FROM nota.tglnota)
                ) AS tahunreg
              , bagian.nmbagian
			  , count(rd.noreg) AS noreg
				FROM
				  registrasidet rd
				left join bagian
				on bagian.idbagian = rd.idbagian
				left join registrasi
				on registrasi.noreg = rd.noreg	
				left join nota
				on nota.idregdet = rd.idregdet	
				where bagian.idjnspelayanan IN (1,3) 
				AND nota.idsttransaksi = 1
				AND substr(nota.tglnota, 1, 4) = '".$tahun."'
				AND (rd.userbatal is null or rd.userbatal = '')
				AND (rd.tglbatal is null or rd.tglbatal = '')
				group by bagian.nmbagian"
				);
				
			$num = $query->num_rows();
			if($num>0){
				$arr = array('data'=>$query->result());
				echo json_encode($arr);
			}else{
				return 0;
			}
		}
		
		function chart_pasien_ri(){
			$tahun = $_POST['tahun'];
			$query = $this->db->query("SELECT DISTINCT 
                bagian.nmbagian
			  , count(rd.noreg) AS noreg
				FROM
				  registrasidet rd
				left join bagian
				on bagian.idbagian = rd.idbagian			
				left join registrasi
				on registrasi.noreg = rd.noreg	
				left join nota
				on nota.idregdet = rd.idregdet		
				where bagian.idjnspelayanan = 2
				AND nota.idsttransaksi = 1
				AND substr(rd.tglmasuk, 1, 4) = '".$tahun."'
				AND (rd.userbatal is null or rd.userbatal = '')
				AND (rd.tglbatal is null or rd.tglbatal = '')
				group by bagian.nmbagian"
				);
				
			$num = $query->num_rows();
			if($num>0){
				$arr = array('data'=>$query->result());
				echo json_encode($arr);
			}else{
				return 0;
			}
		}
		
		function count_poli(){
			
			$query = $this->db->query("SELECT DISTINCT (
                extract(YEAR
                FROM rd.tglreg)
                ) AS tahunreg
              , 
                rd.idbagian as idbagianb
              ,(
                SELECT count(rd.idbagian)
                FROM
                  registrasidet rd, registrasi r, bagian b
                WHERE
                  r.noreg = rd.noreg
                  AND
                  r.idstpasien = 1
                  AND
                  rd.idbagian = b.idbagian
                  AND
                  b.idbagian = idbagianb
                  and
                  extract(YEAR
                  FROM rd.tglreg) = tahunreg
                ) AS polibaru
              , (
                SELECT count(rd.idbagian)
                FROM
                  registrasidet rd, registrasi r, bagian b
                WHERE
                  r.noreg = rd.noreg
                  AND
                  r.idstpasien = 2
                  AND
                  rd.idbagian = b.idbagian
                  AND
                  b.idbagian = idbagianb
                  AND
                  extract(YEAR
                  FROM rd.tglreg) = tahunreg
                ) AS polilama,
                b.nmbagian 
				FROM
				  registrasidet rd, registrasi r, bagian b
				WHERE
				  r.noreg = rd.noreg
				  and
				  rd.idbagian = b.idbagian
				  AND
				  rd.tglreg LIKE '%2014%'
				GROUP BY
				  tahunreg, rd.idbagian"
  );
		$num = $query->num_rows();
			if($num>0){
				$arr = array('data'=>$query->result());
				echo json_encode($arr);
			}else{
				return 0;
			}
		}
		
	function Informasi(){
	$sql =	$this->db->query("select deskripsi from informasi where idstpublish = 2 order by idinfo DESC");
	$num = $sql->num_rows();
	$echoarr=array();
	$echoarr['data']='';
			if($num>0){
				$arr =$sql->result();
				foreach ($arr as $data) {
					$echoarr['data'] .= $data->deskripsi;
				}
				echo json_encode($echoarr);			
			}else{
				echo json_encode($echoarr);
			} 
	}
	
	function thn(){
	$this->db->select("max(tahun.tahun) AS tahun");
			$this->db->from("tahun");
			//$this->db->where("bed.idstbed",2);
			$q = $this->db->get();
			$thn = $q->row_array();
			echo json_encode($thn); 
	}
	function test(){
		$this->db->select("deskripsi");
			$this->db->from("informasi");
			$q = $this->db->get();
			$informasi = $q->row_array();
			echo json_encode($informasi);
	}
}