<?php 
class Laporan_rmkiaibu2_Controller extends Controller{
		public function __construct(){
			parent::Controller();  
			$this->load->library('session');
		}

	function get_laprmkiaibu2(){

		$tglawal = $this->input->post("tglawal");
        $tglakhir = $this->input->post("tglakhir");
		
		$q = "SELECT  rd.tglmasuk
					 , k.tglkuitansi
					 , r.noreg
					 , trim(LEADING '0' FROM `p`.`norm`) AS `norm`
					 , p.nmpasien
					 , (SELECT notadet.kditem AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						LEFT JOIN pelayanan
						ON pelayanan.kdpelayanan = notadet.kditem
						WHERE
						  registrasidet.idregdet = rd.idregdet
						  AND
						  (notadet.kditem = 'T000000210'
						  OR
						  notadet.kditem = 'T000000217')
						ORDER BY
						  notadet.kditem DESC
						LIMIT
						  1
					   ) AS clahirn
					 , (SELECT notadet.kditem AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						LEFT JOIN pelayanan
						ON pelayanan.kdpelayanan = notadet.kditem
						WHERE
						  registrasidet.idregdet = rd.idregdet
						  AND
						  notadet.kditem = 'T000000218'
					   ) AS clahirsc
					 , (SELECT notadet.kditem AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						LEFT JOIN pelayanan
						ON pelayanan.kdpelayanan = notadet.kditem
						WHERE
						  registrasidet.idregdet = rd.idregdet
						  AND
						  notadet.kditem = 'T000000223'
					   ) AS clahirll
					 , (SELECT group_concat(pelayanan.nmpelayanan SEPARATOR ',<br>') AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						LEFT JOIN pelayanan
						ON pelayanan.kdpelayanan = notadet.kditem
						WHERE
						  registrasidet.idregdet = rd.idregdet
						  AND
						  (notadet.kditem = 'T000000210'
						  OR
						  notadet.kditem = 'T000000217'
						  OR
						  notadet.kditem = 'T000000218'
						  OR
						  notadet.kditem = 'T000000223')
					   ) AS nakes
					 , (SELECT if(sum(notadet.qty) >= 1, notadet.kditem, NULL) AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						WHERE
						  registrasidet.noreg = rd.noreg
						  AND
						  notadet.kditem = 'T000000213'
						GROUP BY
						  notadet.kditem
					   ) AS kf1
					 , (SELECT if(sum(notadet.qty) > 1, notadet.kditem, NULL) AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						WHERE
						  registrasidet.noreg = rd.noreg
						  AND
						  notadet.kditem = 'T000000213'
						GROUP BY
						  notadet.kditem
					   ) AS kf2
					 , (SELECT if(sum(notadet.qty) > 2, notadet.kditem, NULL) AS kditem
						FROM
						  notadet
						LEFT JOIN nota
						ON nota.nonota = notadet.nonota
						LEFT JOIN registrasidet
						ON registrasidet.idregdet = nota.idregdet
						WHERE
						  registrasidet.noreg = rd.noreg
						  AND
						  notadet.kditem = 'T000000213'
						GROUP BY
						  notadet.kditem
					   ) AS kf3
					 , (SELECT group_concat(kondisilahir.nmkonlhr SEPARATOR ',<br>') AS nmkonlhr
						FROM
						  skl
						LEFT JOIN kondisilahir
						ON kondisilahir.idkonlhr = skl.idkonlhr
						WHERE
						  skl.noreg = r.noreg
					   ) AS konhir
					 , (SELECT group_concat(jkelamin.nmjnskelamin SEPARATOR ',<br>') AS nmjnskelamin
						FROM
						  skl
						LEFT JOIN jkelamin
						ON jkelamin.idjnskelamin = skl.idjnskelamin
						WHERE
						  skl.noreg = r.noreg
					   ) AS jkelamin
					 , rd.idstkeluar
					 , stklr.nmstkeluar
					 , (SELECT nmkematian AS nmkematian
						FROM
						  kodifikasi
								LEFT JOIN sebabkematian sbmati
								ON sbmati.idkematian = kodifikasi.idkematian
						WHERE
						  kodifikasi.noreg = r.noreg
					   ) AS nmkematian
					 , (SELECT group_concat(penyakit.nmpenyakit SEPARATOR ',<br>') AS nmpenyakiteng
						FROM
						  kodifikasidet
						LEFT JOIN kodifikasi
						ON kodifikasi.idkodifikasi = kodifikasidet.idkodifikasi
						LEFT JOIN registrasi
						ON registrasi.noreg = kodifikasi.noreg
						LEFT JOIN penyakit
						ON penyakit.idpenyakit = kodifikasidet.idpenyakit
						WHERE
						  registrasi.noreg = r.noreg
					   ) AS nmpenyakiteng
					 , dkel.nmdaerah AS kel
					 , dkec.nmdaerah AS kec
					 , dkot.nmdaerah AS kot
					 , cdatang.nmcaradatang AS nmcaradatang
					 , r.idpenjamin
					 , pj.nmpenjamin

				FROM
				  registrasi r
				LEFT JOIN registrasidet rd
				ON r.noreg = rd.noreg
				LEFT JOIN caradatang cdatang
				ON cdatang.idcaradatang = rd.idcaradatang
				LEFT JOIN pasien p
				ON p.norm = r.norm
				LEFT JOIN daerah dkel
				ON dkel.iddaerah = p.iddaerah
				LEFT JOIN daerah dkec
				ON dkec.iddaerah = dkel.dae_iddaerah
				LEFT JOIN daerah dkot
				ON dkot.iddaerah = dkec.dae_iddaerah
				LEFT JOIN penjamin pj
				ON pj.idpenjamin = r.idpenjamin
				LEFT JOIN nota n
				ON n.idregdet = rd.idregdet
				LEFT JOIN notadet nd
				ON nd.nonota = n.nonota
				LEFT JOIN pelayanan pel
				ON pel.kdpelayanan = nd.kditem
				LEFT JOIN kuitansi k
				ON k.nokuitansi = n.nokuitansi
				LEFT JOIN stkeluar stklr
				ON stklr.idstkeluar = rd.idstkeluar
				WHERE
				  r.idjnspelayanan = 2
				  AND
				  rd.userbatal IS NULL
				  AND
				  k.idstkuitansi = 1
				  AND
				  nd.kditem LIKE '%T%'
				  AND
				  k.tglkuitansi BETWEEN '".$tglawal."' AND '".$tglakhir."'
				GROUP BY
				  r.noreg
				ORDER BY
				  r.noreg";
		$query = $this->db->query($q);
		
		$data = array();
		if ($query->num_rows() > 0) {
			$data = $query->result();
		}
		
		$ttl = count($data);
		$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
		
        if($ttl>0){
            $build_array["data"]=$data;
        }
		
		echo json_encode($build_array);
	}

}
