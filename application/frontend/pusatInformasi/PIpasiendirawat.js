function PIpasiendirawat(){
Ext.form.Field.prototype.msgTarget = 'side';
	var ds_pipasiendirawat = dm_pipasiendirawat();
	
	var arr_cari = [['noreg', 'No. Registrasi'],['norm', 'No. RM'],['nmpasien', 'Nama Pasien'],['nmdokter', 'Nama Dokter']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	var arr_cari2 = [['1', 'Semua'],['2', 'Dirawat'],['3', 'Sudah Pulang']];
	
	var ds_cari2 = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari2 
	});
	
	function fnSearchgrid(){		
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
			ds_pipasiendirawat.setBaseParam('key',  '1');
			ds_pipasiendirawat.setBaseParam('id',  idcombo);
			ds_pipasiendirawat.setBaseParam('name',  nmcombo);
		ds_pipasiendirawat.load();
	}		
	
	var row_gridnya = new Ext.grid.RowSelectionModel({
		singleSelect: true
	});
	
	var vw_grid_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gp.grid_pasiendirawat',
		store: ds_pipasiendirawat,
		sm: row_gridnya,
		view: vw_grid_nya,
		tbar: [{
			xtype: 'tbtext',
			style: 'marginLeft: 18px',
			text: 'Status Pasien :'
		},{
			xtype: 'combo', 
			id: 'cb.cari', width: 100, 
			store: ds_cari2, 
			valueField: 'id', displayField: 'nama',
			editable: false, triggerAction: 'all',
			forceSelection: true, submitValue: true, mode: 'local',
			emptyText:'Pilih...',
			listeners: {
				select: function() {					
					ds_pipasiendirawat.setBaseParam('cekid', Ext.getCmp('cb.cari').getValue());
					Ext.getCmp('cek').setValue('');
					ds_pipasiendirawat.load();
				}
			}
		},'-',{
			xtype: 'tbtext',
			style: 'marginLeft: 20px',
			text: 'Search :'
		},{
			xtype: 'combo', 
			id: 'cb.search', width: 150, 
			store: ds_cari, 
			valueField: 'id', displayField: 'nama',
			editable: false, triggerAction: 'all',
			forceSelection: true, submitValue: true, mode: 'local',
			emptyText:'Pilih...',
			listeners: {
				select: function() {
					var cbsearchh = Ext.getCmp('cb.search').getValue();
						if(cbsearchh != ''){
							Ext.getCmp('cek').enable();
							Ext.getCmp('btn_cari').enable();
							Ext.getCmp('cek').focus();
						}
						return;
				}
			}
		},{
			xtype: 'textfield',
			style: 'marginLeft: 7px',
			id: 'cek',
			width: 185,
			disabled: true,
			validator: function(){
				var cek = Ext.getCmp('cek').getValue();
				if(cek == ''){
					fnSearchgrid();
				}
				return;
			}
		},{ 
			text: 'Cari',
			id:'btn_cari',
			iconCls: 'silk-find',
			style: 'marginLeft: 7px',
			disabled: true,
			handler: function() {
				var btncari = Ext.getCmp('cek').getValue();
					if(btncari != ''){
						fnSearchgrid();
					}else{
						Ext.MessageBox.alert('Message', 'Tidak ada data yang di cari');
					}
				return;
			}
		}],
		autoScroll: true,
		height: 480,
		columnLines: true,
		forceFit: true,		
        loadMask: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'No. Registrasi',
			width: 80,
			dataIndex: 'noreg',
			sortable: true,
		},{
			header: 'No. RM',
			width: 70,
			dataIndex: 'norm',
			sortable: true
		},
		{
			header: 'Nama Pasien',
			width: 235,
			dataIndex: 'nmpasien',
			sortable: true
		},
		{
			header: 'Ruangan / Kamar / Bed',
			width: 165,
			dataIndex: 'ruangan_kamar_bed',
			sortable: true
		},
		{
			header: 'Dokter Jaga',
			width: 195,
			dataIndex: 'nmdokter',
			sortable: true,
		},
		{
			header: 'Tgl. Masuk',
			width: 72,
			dataIndex: 'tglmasuk',
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},
		{
			header: 'Jam Masuk',
			width: 69,
			dataIndex: 'jammasuk',
			sortable: true,
		},
		{
			header: 'Tgl. Keluar',
			width: 72,
			dataIndex: 'tglkeluar',
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},
		{
			header: 'Jam Keluar',
			width: 69,
			dataIndex: 'jamkeluar',
			sortable: true,
		},
		{
			header: 'Lama Rawat',
			width: 71,
			dataIndex: 'lamarawat',
			sortable: true,
			align: 'center'
		}],
		//bbar: paging,
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Informasi Pasien Dirawat', iconCls:'silk-user',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}],		
		listeners: {
			afterrender: mulai
		}
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadTpumum(){
		ds_pipasiendirawat.reload();
	}
	
	function mulai(){
		Ext.getCmp('cb.cari').setValue('1');
	}
}