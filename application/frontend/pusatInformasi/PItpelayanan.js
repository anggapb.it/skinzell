function PItpelayanan(){
Ext.form.Field.prototype.msgTarget = 'side';
	var ds_pitpelayanan = dm_pitpelayanan();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
		
	var row_gridnya = new Ext.grid.RowSelectionModel({
		singleSelect: true
	});
	
	var vw_grid_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gp.grid_tpelayanan',
		store: ds_pitpelayanan,
		sm: row_gridnya,
		view: vw_grid_nya,
		tbar: [],
		plugins: cari_data,
		autoScroll: true,
		height: 480,
		columnLines: true,
		forceFit: true,		
        loadMask: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 70,
			dataIndex: 'kdpelayanan',
			sortable: true
		},
		{
			header: 'Nama Pelayanan',
			width: 450,
			dataIndex: 'nmpelayanan',
			sortable: true
		},
		{
			header: 'Jenis Pelayanan',
			width: 95,
			dataIndex: 'nmjnspelayanan',
			sortable: true
		},{
			header: 'Parent',
			width: 130,
			dataIndex: 'nmparent',
			sortable: true,
			renderer: function(value, p, r){
				var parent = '';
					if(r.data['nmparent'] == 0) parent = '';
					if(r.data['nmparent'] != 0) parent = r.data['nmparent'];
				return parent ;
			}
		},
		{
			header: 'Kelas Tarif',
			width: 61,
			dataIndex: 'nmklstarif',
			sortable: true,
		},
		{
			header: 'Total Tarif',
			width: 100,
			dataIndex: 'total',
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000'),
		}]
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Informasi Tarif Pelayanan', iconCls:'silk-user',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadTpumum(){
		ds_pitpelayanan.reload();
	}
}