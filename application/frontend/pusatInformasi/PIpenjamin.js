function PIpenjamin(){
	var pageSize = 20;
	var ds_penjamin = dm_penjamin();
	var ds_jpenjamin = dm_jpenjamin();
	var ds_status = dm_status();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_penjamin,
		displayInfo: true,
		displayMsg: 'Data Info. Penjamin Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_penjamin',
		store: ds_penjamin,		
		autoScroll: true,
		autoHeight: true,//height: 560,
		columnLines: true,
		plugins: cari_data,
		tbar: [],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 70,
			dataIndex: 'kdpenjamin',
			sortable: true
		},
		{
			header: 'Nama',
			width: 150,
			dataIndex: 'nmpenjamin',
			sortable: true
		},
		{
			header: 'Jenis Penjamin',
			width: 100,
			dataIndex: 'nmjnspenjamin',
			sortable: true
		},
		{
			header: 'Alamat',
			width: 200,
			dataIndex: 'alamat',
			sortable: true
		},
		{
			header: 'No. Telepon',
			width: 100,
			dataIndex: 'notelp',
			sortable: true
		},
		{
			header: 'Contact Person',
			width: 150,
			dataIndex: 'nmcp',
			sortable: true
		},
		{
			header: 'Tanggal Awal',
			width: 100,
			dataIndex: 'tglawal',
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},
		{
			header: 'Tanggal Akhir',
			width: 100,
			dataIndex: 'tglakhir',
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},
		{
			header: 'Status',
			width: 80,
			dataIndex: 'nmstatus',
			sortable: true
		},
		{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Lihat',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/eye.png',
					tooltip: 'Lihat record',
                    handler: function(grid, rowIndex) {
						fnEditePenjamin(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Info. Penjamin', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadPenjamin(){
		ds_penjamin.reload();
	}
	
	function fnAddPenjamin(){
		var grid = grid_nya;
		wEntryPenjamin(false, grid, null);	
	}
	
	function fnEditePenjamin(grid, record){
		var record = ds_penjamin.getAt(record);
		wEntryPenjamin(true, grid, record);		
	}
	
	function fnDeletePenjamin(grid, record){
		var record = ds_penjamin.getAt(record);
		var url = BASE_URL + 'penjamin_controller/delete_penjamin';
		var params = new Object({
						idpenjamin	: record.data['idpenjamin']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryPenjamin(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Info. Penjamin':'Penjamin (Entry)';
		var penjamin_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.penjamin',
			buttonAlign: 'left',
			labelWidth: 120, labelAlign: 'right',
			bodyStyle: 'padding:5px 4px 2px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 459, width: 890,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: false,
			items: [{
				xtype: 'panel', title: '', layout: 'column', height: 407,
				frame: true, 
				items: [{
					columnWidth: 0.50, border: false, layout: 'form', style: 'marginTop: 7px',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'Kode',
						id:'tf.frm.kdpenjamin',
						width: 100,
						readOnly: true, 
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Nama penjamin',
						id:'tf.frm.nmpenjamin',
						width: 300,
						readOnly: true, 
					},
					{
						xtype: 'combo', id: 'cb.frm.jpenjamin', 
						fieldLabel: 'Jenis penjamin',
						store: ds_jpenjamin, triggerAction: 'all',
						valueField: 'idjnspenjamin', displayField: 'nmjnspenjamin',
						forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', width: 150,
						editable: false, allowBlank: false,
						readOnly: true, 
					},		
					{
						xtype: 'textarea',
						fieldLabel: 'Alamat',
						id: 'ta.frm.alamat',
						width: 300,
						readOnly: true, 
					},
					{
						xtype: 'textfield',
						fieldLabel: 'No. Telepon',
						id:'tf.frm.notelp',
						width: 300,
						readOnly: true, 
					},		
					{
						xtype: 'textfield',
						fieldLabel: 'No. Fax',
						id: 'tf.frm.nofax',
						width: 300,
						readOnly: true, 
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Email',
						id: 'tf.frm.email',
						width: 300,
						vtype: 'email',
						readOnly: true, 
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Web Site',
						id: 'tf.frm.website',
						width: 300,
						readOnly: true, 
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Contact Person',
						id: 'tf.frm.nmcp',
						width: 300,
						readOnly: true, 
					},
					{
						xtype: 'textfield',
						fieldLabel: 'No. Handphone',
						id:'tf.frm.nohp',
						width: 300,
						readOnly: true, 
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal Awal',
						id: 'df.frm.tglawal',
						name: 'df.frm.tglawal',
						format: "d/m/Y",
						width: 100,
						editable: false,
						readOnly: true, 
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal Akhir',
						id: 'df.frm.tglakhir',
						name: 'df.frm.tglakhir',
						format: "d/m/Y",
						width: 100,
						editable: false,
						readOnly: true, 
					},
					{
						xtype: 'combo', id: 'cb.frm.status', 
						fieldLabel: 'Status',
						store: ds_status, triggerAction: 'all',
						valueField: 'idstatus', displayField: 'nmstatus',
						forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', width: 150,
						editable: false, allowBlank: false,
						readOnly: true, 
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form', style: 'marginTop: 7px',
					labelWidth: 110, labelAlign: 'right',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Info Umum',
						id:'tf.frm.infoumum',
						width: 300,
						height: 120,
						readOnly: true, 
					},
					{
						xtype: 'textarea',
						fieldLabel: 'Info RJ',
						id:'tf.frm.inforj',
						width: 300,
						height: 120,
						readOnly: true, 
					},
					{
						xtype: 'textarea',
						fieldLabel: 'Info RI',
						id:'tf.frm.infori',
						width: 300,
						height: 125,
						readOnly: true,
					}]
				}]
			}],
			buttons: [{
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wPenjamin.close();
				}
			}]
		});
			
		var wPenjamin = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [penjamin_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setPenjaminForm(isUpdate, record);
		wPenjamin.show();

	/**
	FORM FUNCTIONS
	*/	
		function setPenjaminForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					RH.setCompValue('tf.frm.kdpenjamin', record.get('kdpenjamin'));
					RH.setCompValue('tf.frm.nmpenjamin', record.get('nmpenjamin'));
					RH.setCompValue('cb.frm.jpenjamin', record.data['idjnspenjamin']);
					RH.setCompValue('ta.frm.alamat', record.get('alamat'));
					RH.setCompValue('tf.frm.notelp', record.get('notelp'));
					RH.setCompValue('tf.frm.nofax', record.get('nofax'));
					RH.setCompValue('tf.frm.email', record.get('email'));
					RH.setCompValue('tf.frm.website', record.get('website'));
					RH.setCompValue('tf.frm.nmcp', record.get('nmcp'));				
					RH.setCompValue('tf.frm.nohp', record.get('nohp'));
					RH.setCompValue('df.frm.tglawal', record.get('tglawal'));
					RH.setCompValue('df.frm.tglakhir', record.get('tglakhir'));
					RH.setCompValue('cb.frm.status', record.data['idstatus']);
					RH.setCompValue('tf.frm.infoumum', record.get('infoumum'));
					RH.setCompValue('tf.frm.inforj', record.get('inforj'));
					RH.setCompValue('tf.frm.infori', record.get('infori'));
					return;
				}
			}
		}					
	}
}
