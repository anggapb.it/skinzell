function PPperpembelian(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_perpembelian = dm_perpembelian();
	var ds_perpembeliandet = dm_perpembeliandet();
		ds_perpembeliandet.setBaseParam('nopp','null');
	var ds_bagian = dm_bagian();
	var ds_stsetuju = dm_stsetuju();
	var ds_pengguna = dm_pengguna();
	var ds_brgmedisdipp = dm_brgmedisdipp();
	var ds_supplier = dm_supplier();
	var ds_podiperpembelian = dm_podiperpembelian();
	var ds_detpodiperpembelian = dm_detpodiperpembelian();
	var ds_stpo = dm_stpo();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,		
		disableIndexes:['keterangan'],
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_perpembelian,
		displayInfo: true,
		displayMsg: 'Data Daftar Surat Pemesanan Barang Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var vw_grid_nya = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	function keyToView_po(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat PO" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_perpembelian',
		store: ds_perpembelian,
		view: vw_grid_nya,
		frame: true,
		height: 250, //autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddPerpembelian();
				//Ext.getCmp('btn_tmb').disable();
				Ext.getCmp('btn_simpan').enable();
				Ext.getCmp('tf.searchppdet').setValue();
				Ext.getCmp('tf.reckdbrg').setValue();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'No Pesanan',
			width: 82,
			dataIndex: 'nopp',
			sortable: true,
			renderer: keyToView_po
		},
		{
			header: 'Tanggal Kirim',
			width: 74,
			dataIndex: 'tglpp',
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},
		{
			header: 'Bagian',
			width: 82,
			dataIndex: 'nmbagian',
			sortable: true
		},
		{
			header: 'Status Pesanan',
			width: 90,
			dataIndex: 'nmstsetuju',
			sortable: true
		},
		{
			header: 'Status Pembelian',
			width: 92,
			dataIndex: 'nmstpo',
			sortable: true
		},{
			header: 'Supplier',
			width: 210,
			dataIndex: 'nmsupplier',
			sortable: true
		},
		{
			header: 'Pengguan',
			width: 97,
			dataIndex: 'nmlengkap',
			sortable: true,
		},
		{
			header: 'Keterangan',
			width: 128,
			dataIndex: 'keterangan',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditPerpembelian(grid, rowIndex);
						var searchppdet = RH.getCompValue('tf.nopp', true);
						if(searchppdet != ''){
							RH.setCompValue('tf.searchppdet', searchppdet);
							RH.setCompValue('tf.reckdbrg', searchppdet);
						}
						var setuju = RH.getCompValue('cb.setuju', true);
						if(setuju != 1){
							Ext.getCmp('btn_tmb').disable();
							Ext.getCmp('btn_simpan').disable();
						}
						return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeletePerpembelian(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Cetak',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetakpp(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			cellclick: onCellClickviewpo
		}
	});
	
	function onCellClickviewpo(grid, rowIndex, columnIndex, event) {
		var t = event.getTarget();
		var record = grid.getStore().getAt(rowIndex);
		
		if (t.className == 'keyMasterDetail'){
				var rec_nopp = record.data['nopp'];
				Ext.getCmp('tf.tamnopp').setValue(rec_nopp);
				if(rec_nopp != "null"){
					//ds_podiperpembelian.setBaseParam('nopp', rec_nopp);
					ds_podiperpembelian.reload({
						params: { 
							nopp: rec_nopp
						}
					});
					//ds_detpodiperpembelian.setBaseParam('nopo', "null");
					ds_detpodiperpembelian.reload({
						params: { 
							nopo: 'null'
						}
					});
				}else if(rec_nopp == "null"){
					//ds_detpodiperpembelian.setBaseParam('nopo', "null");
					//ds_detpodiperpembelian.reload();
					ds_detpodiperpembelian.reload({
						params: { 
							nopo: 'null'
						}
					});
				}				
			return true;
		}
		return true;
	} 
		
	var paging_head_po = new Ext.PagingToolbar({
		pageSize: 6,
		store: ds_podiperpembelian,
		displayInfo: true,
		displayMsg: 'Data {0} - {1} of {2}',
		emptyMsg: 'No data to display',
		listeners: {
			beforechange : function(){
				ds_podiperpembelian.setBaseParam('nopp', Ext.getCmp('tf.tamnopp').getValue());
				ds_podiperpembelian.reload();
			}
		}
	});
	
	var vw_grid_head_po = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil_po(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail No. Pembelian" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
		
	var grid_head_po = new Ext.grid.GridPanel({
		id: 'grid_head_po',
		store: ds_podiperpembelian,		
		autoScroll: true,
		view: vw_grid_head_po,
		columnLines: true,
		tbar: [{
			xtype: 'textfield',
			id: 'tf.tamnopp',
			hidden: true
		}],
		height: 200,
		//sm: sm_nya,
		frame: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'No. Pembelian',
			dataIndex: 'nopo',			
			sortable: true,
			width: 90,			
			renderer: keyToDetil_po
		},{
			header: 'Tanggal Terima',
			dataIndex: 'tglpo',
			width: 90,
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: 'Syarat Pembayaran',
			dataIndex: 'nmsypembayaran',
			width: 120,
			sortable: true,
		},{
			header: 'Mata Uang',
			dataIndex: 'nmmatauang',
			width: 90,
			sortable: true,
		},{
			header: 'No. Reff',
			dataIndex: 'bpb',
			width: 100,
			sortable: true,
		},{
			header: 'Total Pembelian',
			dataIndex: 'totalpo',
			width: 120,
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000'),
		},{
			header: 'Pengguna',
			dataIndex: 'nmlengkap',
			width: 253,
			sortable: true,
		}],
		bbar: paging_head_po,
		listeners: {
			cellclick: onCellClickpodet
		}
	});
	
	function onCellClickpodet(grid, rowIndex, columnIndex, event) {
		var t = event.getTarget();
		var record = grid.getStore().getAt(rowIndex);
		
		if (t.className == 'keyMasterDetail'){
				var rec_nopo = record.data['nopo'];
				Ext.getCmp('tf.tamnopo').setValue(rec_nopo);				
				//ds_detpodiperpembelian.setBaseParam('nopo', rec_nopo);
				//ds_detpodiperpembelian.reload();
				ds_detpodiperpembelian.reload({
					params: { 
						nopo: rec_nopo
					}
				});
			return true;
		}
		return true;
	}
	
	var paging_det_po = new Ext.PagingToolbar({
		pageSize: 6,
		store: ds_detpodiperpembelian,
		displayInfo: true,
		displayMsg: 'Data {0} - {1} of {2}',
		emptyMsg: 'No data to display',
		listeners: {
			beforechange : function(){
				ds_detpodiperpembelian.setBaseParam('nopo', Ext.getCmp('tf.tamnopo').getValue());
				ds_detpodiperpembelian.reload();
			}
		}
	});	
			
	var grid_det_po = new Ext.grid.GridPanel({
		id: 'grid_det_po',
		store: ds_detpodiperpembelian,	
		tbar: [{
			xtype: 'textfield',
			id: 'tf.tamnopo',
			hidden: true
		}],		
		autoScroll: true,
		//autoHeight: true,
		columnLines: true,
		height: 200,
		//sm: sm_nya,
		frame: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Nama Barang',
			dataIndex: 'nmbrg',
			width: 390,
			sortable: true,
		},{
			header: 'Satuan(Besar)',
			dataIndex: 'nmsatuan',
			width: 90,
			sortable: true,
		},{
			header: 'Qty',
			dataIndex: 'qty',
			width: 66,
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},{
			header: 'Qty Bonus',
			dataIndex: 'qtybonus',
			width: 76,
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},{
			header: 'Harga Beli',
			dataIndex: 'hargabeli',
			width: 100,
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000'),
		},{
			header: 'Diskon(%)',
			dataIndex: 'diskon',
			width: 76,
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},{
			header: 'Diskon(Rp)',
			dataIndex: 'diskonrp',
			width: 76,
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},{
			header: 'PPN(%)',
			dataIndex: 'ppn',
			width: 60,
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},{
			header: 'Harga Jual',
			dataIndex: 'hargajual',
			width: 90,
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000'),
		}],
		bbar: paging_det_po,
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Surat Pemesanan Barang', iconCls:'silk-calendar',
		width: 900, Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		//tbar: [],
		items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 1,
			//defaults: { labelWidth: 87, labelAlign: 'right'},
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Daftar Surat Pemesanan Barang',
				layout: 'form',
				height: 287,
				boxMaxHeight:292,
				items: [grid_nya]
			}]
		},
		{
			xtype: 'container',
			style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Pembelian Barang',
				layout: 'form',
				height: 225,
				boxMaxHeight:240,
				items: [grid_head_po]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 1,
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Detail Pembelian Barang',
				layout: 'form',
				height: 230,
				boxMaxHeight:245,
				items: [grid_det_po]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadPerpembelian(){
		ds_perpembelian.reload();
	}
	
	function fnAddPerpembelian(){
		var grid = grid_nya;
		wEntryPerpembelian(false, grid, null);
		var userid = RH.getCompValue('tf.userid', true);
			if(userid != ''){
				RH.setCompValue('cb.user', userid);
			}
			return;
	}
	
	function fnEditPerpembelian(grid, record){
		var record = ds_perpembelian.getAt(record);
		wEntryPerpembelian(true, grid, record);		
	}
	
	function fnDeletePerpembelian(grid, record){
		var record = ds_perpembelian.getAt(record);
		var url = BASE_URL + 'perpembelian_controller/delete_perpembelian';
		var params = new Object({
						nopp	: record.data['nopp']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
	function cetakpp(grid, record){
		var record = ds_perpembelian.getAt(record);
		var nopp = record.data['nopp'] 
		RH.ShowReport(BASE_URL + 'print/print_pp/pp_pdf/' + nopp);
	}

	function wEntryPerpembelian(isUpdate, grid, record){					
		var winTitle = (isUpdate)?'Daftar Surat Pemesanan Barang (Edit)':'Daftar Surat Pemesanan Barang(Entry)';
		
		var paging_brg = new Ext.PagingToolbar({
			pageSize: pageSize,
			store: ds_perpembeliandet,
			displayInfo: true,
			displayMsg: 'Data Permintaan Pembelian (PP) Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var vw_grid_perpembeliandet = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_perpembeliandet = new Ext.grid.EditorGridPanel({
			id: 'gp.grid_brg',
			store: ds_perpembeliandet,			
			view: vw_grid_perpembeliandet,
			columnLines: true,
			height: 274,
			pageSize: pageSize,
			clicksToEdit: 1,
			frame: true,
			tbar: [{
				text: 'Tambah',
				id: 'btn_tmb',
				iconCls: 'silk-add',
				handler: function() {
					fncariBarang();
				}
			},{
				xtype: 'textfield',
				id:'tf.searchppdet',
				width: 100,
				hidden: true,
				validator: function(){
					ds_perpembeliandet.setBaseParam('nopp', Ext.getCmp('tf.searchppdet').getValue());
					//Ext.getCmp('gp.grid_brg').store.reload();
					ds_perpembeliandet.reload();
				}				
			},{
				xtype: 'textfield',
				id:'tf.reckdbrg',
				width: 70,
				hidden: true,				
			},{
				xtype: 'textfield',
				id:'tf.updatekdbrg',
				width: 100,
				hidden: true,				
			}],
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: '',
				width: 100,
				dataIndex: 'kdbrg',
				sortable: true,
				hidden: true
			},
			{
				header: 'Nama Barang',
				width: 300,
				dataIndex: 'nmbrg',
				sortable: true
			},
			{
				header: 'idsatuan',
				width: 100,
				dataIndex: 'idsatuan',
				sortable: true,
				hidden: true
			},
			{
				header: 'Satuan',
				width: 92,
				dataIndex: 'nmsatuan',
				sortable: true
			},
			{
				header: 'Qty',
				width: 100,
				dataIndex: 'qty',
				sortable: true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
				editor: new Ext.form.TextField({
					id: 'qty',
					enableKeyEvents: true,
					listeners: {
						change: function(){
							//fnEditQty(); 
						}
					}
				})
			},
			{
				header: 'Catatan',
				width: 250,
				dataIndex: 'catatan',
				sortable: true,
				editor: new Ext.form.TextField({
					id: 'catatan',
					enableKeyEvents: true,
					listeners: {
						change: function(){
							//fnEditCatatan(); 
						}
					}
				})
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							//Ext.getCmp('btn_simpan').disable();
							//Ext.getCmp('btn_tmb').disable();
							ds_perpembeliandet.removeAt(rowIndex);
						}
					}]
			}],
			//bbar: paging_brg,
			listeners: {
				rowclick: Addrecord
			}
		});
		
		function Addrecord(grid, rowIndex, columnIndex){
			var record = grid.getStore().getAt(rowIndex);
		}
		
		function fnHapusPerpembeliandet(grid, record){
			var record = ds_perpembeliandet.getAt(record);
			Ext.Msg.show({
				title: 'Konfirmasi',
				msg: 'Hapus data yang dipilih..?',
				buttons: Ext.Msg.YESNO,
				icon: Ext.MessageBox.QUESTION,
				fn: function (response) {
					if ('yes' !== response) {
						return;
					}
					Ext.Ajax.request({
					url: BASE_URL + 'perpembelian_controller/delete_perpembeliandet',
					params: {				
						nopp	: record.data['nopp'], //Ext.getCmp('tf.searchppdet').getValue(),
						kdbrg	: record.data['kdbrg']
					},
					success: function(response){
						Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
						ds_perpembeliandet.reload();
					},
					failure: function() {
						//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
					}
				});
				
				}            
			});	
		}
						
		var perpembelian_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.perpembelian',
			buttonAlign: 'left',
			//autoScroll: true,
			labelWidth: 150, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 549, width: 930,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			tbar: [{
				id:'btn_simpan',text: 'Simpan', iconCls:'silk-save', style: 'marginLeft: 5px',
				handler: function() {
						//fnSavePerpembelian();	
						simpanPP();
					}
				}, 
				{
					text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
					handler: function() {
						Ext.getCmp('tf.searchppdet').setValue();
						wPerpembelian.close();
						ds_perpembelian.reload();
						ds_perpembeliandet.reload();
				}
			}],
			frame: false,	
			items: [{
				xtype: 'panel', layout:'fit', height: 190,
				title:'', id:'fp.wpp', frame:true,
				items: [{
					xtype: 'fieldset', title: 'Surat Pemesanan Barang', layout: 'column',
					items: [{
						columnWidth: 0.45, border: false, layout: 'form',					
						items: [{
							xtype: 'textfield',
							fieldLabel: 'No. Pesanan',
							id: 'tf.nopp', 
							width: 200,
							readOnly: true,
							style : 'opacity:0.6',							
						},{
							xtype: 'datefield',
							fieldLabel: 'Tanggal Kirim',
							id: 'df.tglpp',
							value: new Date(),
							format: 'd-m-Y',
							width: 200
						},{
							xtype: 'combo', id: 'cb.bagian', fieldLabel: 'Bagian',
							store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
							triggerAction: 'all', forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
							width: 200, allowBlank: false, editable: false,
							value: 11, readOnly: true,
							style : 'opacity:0.6',							
						},{
							xtype: 'combo', id: 'cb.setuju', fieldLabel: 'Disetujui',
							store: ds_stsetuju, valueField: 'idstsetuju', displayField: 'nmstsetuju',
							triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih...', selectOnFocus:false,
							width: 200, allowBlank: false, editable: false,
							value: 2,
						},{
							xtype: 'combo', id: 'cb.stpo', fieldLabel: 'Status Pesanan',
							store: ds_stpo, valueField: 'idstpo', displayField: 'nmstpo',
							triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih...', selectOnFocus:false,
							width: 200, allowBlank: false, editable: false, value: 1,
							readOnly: true, style : 'opacity:0.6',
						}]
					},
					{
						columnWidth: 0.55, border: false, layout: 'form',
						items: [{
							xtype: 'compositefield',
							fieldLabel: 'Supplier',
							width: 290,
							items: [{
								xtype: 'textfield', id: 'tf.supplier',
								width: 260,
								emptyText:'Pilih...',	
								allowBlank: false,
								readOnly: true
							},{
								xtype: 'textfield', id: 'tf.kdsupplier',
								width: 50,
								readOnly: true,
								hidden: true
							},{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn.supplier',
								width: 4,
								handler: function() {
									fnwSupplier();
								}
							}]
						},{
							xtype: 'textarea',
							fieldLabel: 'Keterangan',
							id  : 'ta.keterangan',
							width : 260,
							height: 70,
						},{
							xtype: 'combo', id: 'cb.user', 
							fieldLabel: 'User Input',
							store: ds_pengguna, triggerAction: 'all',
							valueField: 'userid', displayField: 'nmlengkap',
							forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih...', width: 260,
							editable: false,
							readOnly: true,
							style : 'opacity:0.6',
						},{
							xtype: 'textfield',
							fieldLabel: 'User Id',
							id:'tf.userid',
							width: 150,
							value: USERID,
							hidden: true,
							
						}]
					}]
				}],
				/* bbar: [{
					id:'btn_simpan',text: 'Simpan', iconCls:'silk-save',
					handler: function() {
							//fnSavePerpembelian();	
							simpanPP();
						}
					}, 
					{
						text: 'Kembali', iconCls:'silk-arrow-undo',
						handler: function() {
							Ext.getCmp('tf.searchppdet').setValue();
							wPerpembelian.close();
							ds_perpembelian.reload();
							ds_perpembeliandet.reload();
					}
				}] */
				
			},{
				xtype: 'fieldset',
				title: 'Pemesanan Barang',
				layout: 'form',
				items: [grid_perpembeliandet]
			}]
		});
			
		var wPerpembelian = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [perpembelian_form]
		});
				
		function fnEditQty(){
			var qty = Ext.getCmp('qty').getValue();
			var letters = /^[a-zA-Z]+$/;
			var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
			if(qty.match(letters)){
				alert('Masukan Angka');
				ds_perpembeliandet.reload();
			} 
			else if(qty.match(simbol)){
				alert('Masukan Angka');
				ds_perpembeliandet.reload();
			} 
			else {           
				fnQty(qty);   
			}      
		}
		
		function fnQty(qty){
			Ext.Ajax.request({
				url: BASE_URL + 'perpembelian_controller/update_qty',
				params: {
					nopp	: Ext.getCmp('tf.searchppdet').getValue(),
					kdbrg	: Ext.getCmp('tf.updatekdbrg').getValue(),
					qty    	: qty
				},
				success: function() {
					//Ext.Msg.alert("Info", "Ubah Berhasil");
					Ext.getCmp('gp.grid_brg').store.reload();
				},
				failure: function() {
					Ext.Msg.alert("Info", "Ubah Data Gagal");
				}
			});
		}
		
		function fnEditCatatan(){
			var catatan = Ext.getCmp('catatan').getValue();
			Ext.Ajax.request({
				url: BASE_URL + 'perpembelian_controller/update_catatan',
				params: {
					nopp		: Ext.getCmp('tf.searchppdet').getValue(),
					kdbrg		: Ext.getCmp('tf.updatekdbrg').getValue(),
					catatan    	: catatan
				},
				success: function() {
					//Ext.Msg.alert("Info", "Ubah Berhasil");
					Ext.getCmp('gp.grid_brg').store.reload();
				},
				failure: function() {
					Ext.Msg.alert("Info", "Ubah Data Gagal");
				}
			});
		}

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setPerpembelianForm(isUpdate, record);
		wPerpembelian.show();

	/**
	FORM FUNCTIONS
	*/	
		function setPerpembelianForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'perpembelian_controller/getNmsupplier',
						params:{
							kdsupplier : record.get('kdsupplier')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.supplier', r);
						}
					});
					
					RH.setCompValue('tf.nopp', record.get('nopp'));
					RH.setCompValue('df.tglpp', record.get('tglpp'));
					RH.setCompValue('cb.bagian', record.get('idbagian'));
					RH.setCompValue('cb.setuju', record.get('idstsetuju'));
					RH.setCompValue('cb.stpo', record.get('idstpo'));
					RH.setCompValue('tf.supplier', record.get('kdsupplier'));
					RH.setCompValue('cb.user', record.get('userid'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					return;
				}
			}
		}
		
		function fnSavePerpembelian(){
			var idForm = 'frm.perpembelian';
			var sUrl = BASE_URL +'perpembelian_controller/insert_perpembelian';
			var sParams = new Object({
				nopp		:	RH.getCompValue('tf.nopp'),
				tglpp		:	RH.getCompValue('df.tglpp'),
				idbagian	:	RH.getCompValue('cb.bagian'),
				idstsetuju	:	RH.getCompValue('cb.setuju'),
				idstpo		:	RH.getCompValue('cb.stpo'),
				kdsupplier	:	RH.getCompValue('tf.supplier'),
				keterangan	:	RH.getCompValue('ta.keterangan'),
				userid		:	RH.getCompValue('cb.user'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate || RH.getCompValue('tf.nopp') != ''){
				sUrl = BASE_URL +'perpembelian_controller/update_perpembelian';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			submitGridForm(idForm, sUrl, sParams, grid, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
		
		function simpanPP(){
			var cekstpo = Ext.getCmp('cb.stpo').getValue();
			var ceksup = Ext.getCmp('tf.supplier').getValue();
			
			if(cekstpo && ceksup && reckdbrg != ""){
				var reckdbrg = Ext.getCmp('tf.reckdbrg').getValue();
				if(reckdbrg != ""){
					var arrpp = [];
					for(var zx = 0; zx < ds_perpembeliandet.data.items.length; zx++){
						var record = ds_perpembeliandet.data.items[zx].data;
						zkdbrg = record.kdbrg;
						zidsatuan = record.idsatuan;
						zqty = record.qty;
						zcatatan = record.catatan;
						arrpp[zx] = zkdbrg + '-' + zidsatuan + '-' + zqty + '-' + zcatatan ;
					}
					Ext.Ajax.request({
						url: BASE_URL + 'perpembelian_controller/insorupd_pp',
						params: {
							nopp		:	RH.getCompValue('tf.nopp'),
							tglpp		:	RH.getCompValue('df.tglpp'),
							idbagian	:	RH.getCompValue('cb.bagian'),
							idstsetuju	:	RH.getCompValue('cb.setuju'),
							idstpo		:	RH.getCompValue('cb.stpo'),
							kdsupplier	:	RH.getCompValue('tf.supplier'),
							keterangan	:	RH.getCompValue('ta.keterangan'),
							userid		:	RH.getCompValue('cb.user'),
							
							arrpp : Ext.encode(arrpp)
							
						},
						success: function(response){
							Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
							obj = Ext.util.JSON.decode(response.responseText);
							console.log(obj);
							Ext.getCmp("tf.nopp").setValue(obj.nopp);
							ds_perpembelian.reload();
						},
						failure : function(){
							Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
						}
					});
				}else{
					Ext.MessageBox.alert('Informasi','Pesanan barang belum ada..');
				}
			}else{
				Ext.MessageBox.alert('Informasi','Isi data terlebih dahulu..');
			}
		}
					
	}
	
	function getForm(idform){
		var form;
		if(Ext.getCmp(idform)){
			var comp = Ext.getCmp(idform);
			if(comp.getForm()){
				return comp.getForm();
			}
		}
	}

	function submitGridForm (idForm, sUrl, sParams, grid, msgWait, msgSuccess, msgFail, msgInvalid){
		var form = getForm(idForm);
		if(form.isValid()){
			Ext.Ajax.request({
				url: sUrl,
				method: 'POST',
				params: sParams, 		
				//waitMsg: msgWait,				
				/* success: function(){
					Ext.Msg.alert("Info:", msgSuccess);	
					//grid.getStore().reload();
					//fTotal();
					//win.close();
					Ext.getCmp('btn_tmb').enable();
					Ext.getCmp('btn_simpan').disable();				
				}, */
				success: function(response) {
					//if(o.result.success == true) {
					var arrjson = Ext.decode(response.responseText);
						Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
						Ext.getCmp('tf.nopp').setValue(arrjson.nopp);
						Ext.getCmp('tf.searchppdet').setValue(arrjson.nopp);
						Ext.getCmp('btn_tmb').enable();
						ds_perpembelian.reload();
					//}
				},
				failure: function(){
					Ext.Msg.alert("Info:", msgFail);
				}
			});
		} else {
			Ext.Msg.alert("Info:", msgInvalid);
		}	
	}
	
	function fnwSupplier(){
		var ds_supplier = dm_supplier();
		
		function fnkeyAddsup(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_supplier = new Ext.grid.ColumnModel([
			{
				dataIndex: 'kdsupplier',
				width: 100,
				hidden: true
			},{
				header: 'Nama Supplier',
				dataIndex: 'nmsupplier',
				width: 224,
				renderer: fnkeyAddsup
			},{
				header: 'Alamat',
				dataIndex: 'alamat',
				width: 230, 
			},{
				header: 'No. Telepon',
				dataIndex: 'notelp',
				width: 95, 
			},{
				header: 'No. Fax',
				dataIndex: 'nofax',
				width: 70, 
			},{
				header: 'Kontak Person',
				dataIndex: 'kontakperson',
				width: 110, 
			},{
				header: 'NO. Hp',
				dataIndex: 'nohp',
				width: 95, 
			}
		]);
		
		var sm_supplier = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_supplier = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_supplier = new Ext.PagingToolbar({
			pageSize: 20,
			store: ds_supplier,
			displayInfo: true,
			displayMsg: 'Data Supplier Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_supplier = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_supplier = new Ext.grid.GridPanel({
			ds: ds_supplier,
			cm: cm_supplier,
			sm: sm_supplier,
			view: vw_supplier,
			height: 460,
			width: 835,
			plugins: cari_supplier,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_supplier,
			listeners: {
				cellclick: onCellClickaddsup
			}
		});
		var win_find_cari_supplier = new Ext.Window({
			title: 'Cari Supplier',
			modal: true,
			items: [grid_find_cari_supplier]
		}).show();
		
		function onCellClickaddsup(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_kdsupplier = record.data["kdsupplier"];
					var var_cari_nmsupplier = record.data["nmsupplier"];
					Ext.getCmp("tf.kdsupplier").setValue(var_cari_kdsupplier);
					Ext.getCmp("tf.supplier").setValue(var_cari_nmsupplier);
								win_find_cari_supplier.close();
				return true;
			}
			return true;
		}
	}
	
	function fncariBarang(){
		var ds_brgmedisdipp = dm_brgmedisdipp();		
		ds_brgmedisdipp.setBaseParam('kdsupplier', Ext.getCmp("tf.kdsupplier").getValue());
		ds_brgmedisdipp.reload();
		
		function keyToAddbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_barang = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header: headerGerid('Kode'),
				width: 75,
				dataIndex: 'kdbrg',
				sortable: true,
				renderer: keyToAddbrg
			},
			{
				header: headerGerid('Nama Barang'),
				width: 280,
				dataIndex: 'nmbrg',
				sortable: true
			},
			{
				header: headerGerid('Jenis Barang'),
				width: 100,
				dataIndex: 'nmjnsbrg',
				sortable: true,
			},
			{
				header: headerGerid('Satuan <br> (Besar)'),
				width: 100,
				dataIndex: 'nmsatuanbsr',
				sortable: true,
				align:'center',
			},
			{
				header: headerGerid('Jumlah'),
				width: 52,
				dataIndex: 'jmlsatuanbsr',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
			}
		]);
		
		var sm_barang = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_barang = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_barang = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_brgmedisdipp,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_barang = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_barang = new Ext.grid.GridPanel({
			ds: ds_brgmedisdipp,
			cm: cm_barang,
			sm: sm_barang,
			view: vw_barang,
			height: 477,
			width: 655,
			//plugins: cari_barang,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_brgmedisdipp.setBaseParam('key',  '1');
							ds_brgmedisdipp.setBaseParam('id',  'nmbrg');
							ds_brgmedisdipp.setBaseParam('name',  nmcombo);
						ds_brgmedisdipp.load();
					}
				}]
			}],
			bbar: paging_barang,
			listeners: {
				//rowdblclick: klik_cari_barang
				cellclick: onCellClicAddbrg
			}
		});
		var win_find_cari_barang = new Ext.Window({
			title: 'Cari Barang',
			modal: true,
			items: [grid_find_cari_barang]
		}).show();
		
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var rec_record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var cek = true;
					var obj = ds_brgmedisdipp.getAt(rowIndex);
					var skdbrg		= obj.data["kdbrg"];
					var snmbrg		= obj.data["nmbrg"];
					var sidsatuan	= obj.data["idsatuanbsr"];
					var snmsatuan	= obj.data["nmsatuanbsr"];
					
					Ext.getCmp('tf.reckdbrg').setValue(skdbrg);
					
					ds_perpembeliandet.each(function(rec){
						if(rec.get('kdbrg') == skdbrg) {
							Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
							cek = false;
						}
					});
				
				if(cek){
					var orgaListRecord = new Ext.data.Record.create([
						{
							name: 'kdbrg',
							name: 'nmbrg',
							name: 'idsatuan',
							name: 'nmsatuan',
							name: 'qty',
							name: 'catatan'
						}
					]);
					
					ds_perpembeliandet.add([
						new orgaListRecord({
							'kdbrg'	  	: skdbrg,
							'nmbrg'	  	: snmbrg,
							'idsatuan'	: sidsatuan,
							'nmsatuan'	: snmsatuan,
							'qty'		: 1,
							'catatan'	: "-"
						})
					]);
					win_find_cari_barang.close();
				}
			}
			return true;
		}
	}
}