function PDaftarPO(isUpdate, recordpo, storesObj){
	
var ds_supplier = storesObj.ds_supplier1;	
var ds_jperpembelian = storesObj.ds_jperpembelian1;
var ds_bagian = storesObj.ds_bagian1;
var ds_brgmedis = storesObj.ds_brgmedis1;
var ds_sypembayaran = storesObj.ds_sypembayaran1;
var ds_jpembayaran = storesObj.ds_jpembayaran1;
var ds_stpo = storesObj.ds_stpo1;
var	ds_pp = storesObj.ds_pp1;
var ds_stsetuju = storesObj.ds_stsetuju1;
var ds_app1 = storesObj.ds_app11;

var tempstpo;

var	ds_ppx = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'purchaseorder_controller/get_podet_ppdet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields: [{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'tglpp',
				mapping: 'tglpp'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'harga',
				mapping: 'harga'
			},{
				name: 'idhrgbrgsup',
				mapping: 'idhrgbrgsup'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'idstpp',
				mapping: 'idstpp'
			},{
				name: 'nmstpp',
				mapping: 'nmstpp'
			},{
				name: 'nopo',
				mapping: 'nopo'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'nmmatauang',
				mapping: 'nmmatauang'
			},{
				name: 'kdmatauang',
				mapping: 'kdmatauang'
			},{
				name: 'subtotal',
				mapping: 'subtotal'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'margin',
				mapping: 'margin'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'idsatuanbsr',
				mapping: 'idsatuanbsr'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'hsubtotal',
				mapping : 'hsubtotal'
			},{
				name: 'hargajualtemp',
				mapping: 'hargajualtemp'
			},{
				name: 'hargajual',
				mapping: 'hargajual'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'htsubtotal',
				mapping: 'htsubtotal'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'diskonrp',
				mapping: 'diskonrp'
			},{
				name: 'qtypo',
				mapping: 'qtypo'
			},{
				name: 'idstpp',
				mapping: 'idstpp'
			},{
				name: 'ppn',
				mapping: 'ppn',
				type:'bool'
			},{
				name: 'tamppn',
				mapping: 'tamppn',
			},{
				name: 'qtytemp',
				mapping: 'qty',
			},{
				name: 'hrgbelikcl',
				mapping: 'hrgbelikcl',
			},{
				name: 'tmptotppn',
				mapping: 'tmptotppn',
			}],
			listeners: {
				load: function(store, records, options) {
						ftotal();
				}
			}
		});
	
var subtotal = 0 ,xxx = 0, hit = 0,nmmatauang = '', xkdmatauang = '', idmatauang = '', rownota = 0;
var arr = [];



/* GRID */
var row_gridnya = new Ext.grid.RowSelectionModel({
		singleSelect: true
	});

	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
var grid_po = new Ext.grid.EditorGridPanel({
		store: ds_ppx,
		frame: false,
		height: 220,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_po',
		forceFit: true,
		//sm: cbGrid,
		tbar: [{
							text: 'Tambah',
							id: 'btn_add',
							iconCls: 'silk-add', 
							margins: '5',
							disabled:true,
							width: 100,
							handler: function() {
			//					fPel();
								fbarang();
							}
					},{
							text: 'Syarat Pembayaran',
					},{
							xtype: 'combo', fieldLabel: 'Syarat Pembayaran',
							id:'cb.syaratbayar', width: 100, store: ds_sypembayaran,
							valueField: 'idsypembayaran', displayField: 'nmsypembayaran', editable: false,
							triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih....', allowBlank: false
					},{
							text: 'Jenis Pembayaran',
					},{
							xtype: 'combo', fieldLabel: 'Jenis Pembayaran',
							id:'cb.jnsbayar', width: 100, store: ds_jpembayaran,
							valueField: 'idjnspembayaran', displayField: 'nmjnspembayaran', editable: false,
							triggerAction: 'all',forceSelection: true, selectOnFocus:false, submitValue: true, mode: 'local',
							emptyText:'Pilih....', allowBlank: false

					},{
							text: 'Status Pesanan',
					},{
							xtype: 'combo', fieldLabel: 'Status Pesanan',
							id:'cb.stspo', width: 100, store: ds_stpo,
							valueField: 'idstpo', displayField: 'nmstpo', editable: false,
							triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih....', allowBlank: false
					},{
							text: 'No. Reff',
					},{
						xtype: 'textfield', fieldLabel: 'No. Reff', id:'tf.reff', width: 202
					}],
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota = rowIndex;
            }
        },
		columns: [
		{
			header: headerGerid('Kode Barang'),
			dataIndex: 'kdbrg',
			width: 80
		},{
			header: headerGerid('Nama Barang'),
			dataIndex: 'nmbrg',
			width: 160
		},{
			header: headerGerid('Satuan'),
			dataIndex: 'nmsatuanbsr',
			width: 60,
		},{
			header: 'qtytemp',
			dataIndex: 'qtytemp',
			width: 83,
			hidden: true,
		},{
			header: headerGerid('Qty'),
			dataIndex: 'qty',
			width: 50,
			align: 'right',
			xtype: 'numbercolumn', format:'0,000.00',
			editor: {
				xtype: 'numberfield',
				id: 'tf.qty', width: 150,
				enableKeyEvents: true,
				listeners:{
					/* specialkey: function(field, e){
						if (e.getKey() == e.ENTER) {
							var record = ds_ppx.getAt(rownota);
							var subtotal=0, ppnn= 0;
							var s1 = 0;
							if (record.data.ppn==true) {
								ppnn = 0.1;
							} else if (record.data.ppn==false) {
								ppnn = 0;
							}
							
							var cek = Ext.getCmp('tf.nopp').getValue();
							if(cek !=''){
								s1 = ((record.data.hrgbeli * Ext.getCmp('tf.qty').getValue()) - record.data.diskonrp);
								subtotal = (s1 + (s1 * ppnn));
								record.set('subtotal',subtotal);
								alert(subtotal);
							}else if(cek ==''){
								subtotalhrgjual();	
								diskon();
								diskonrp();
								if (name == 'mousedown') {
									record.set(this.dataIndex, !record.data[this.dataIndex]);
									subtotalhrgjualppn(rowIndex);							
									subtotalhrgjual();
								}
							}
						}
					} *//* ,
					change :function(field, newVal, oldVal){
						var record = ds_ppx.getAt(rownota);
						
						if (newVal <= 0) {
							record.set('qty',oldVal);
							if (oldVal <= 0) {
								record.set('qty',1);
							}
						 } else {
							if (record.data.qtytemp > 0) {
								if (newVal > record.data.qtytemp && isUpdate) {
									Ext.MessageBox.alert('Errors', 'Jumlah Qty Tidak Boleh Lebih dari Sisa Qty yang ada');
									record.set('qty',record.data.qtytemp);
									return;
								}
								return;
							}						 
						 }
						subtotalhrgjual();	
						diskon();
						diskonrp();
						if (name == 'mousedown') {
							record.set(this.dataIndex, !record.data[this.dataIndex]);
							subtotalhrgjualppn(rowIndex);							
							subtotalhrgjual();
						}
					}*/
					change :function(field, newVal, oldVal){						
						var cek = Ext.getCmp('tf.nopp').getValue();
						var record = ds_ppx.getAt(rownota);
						if(cek !=''){
							if (newVal <= 0) {
								record.set('qty',oldVal);
								if (oldVal <= 0) {
									record.set('qty',1);
								}
							 } else {
								if (record.data.qtytemp > 0) {
									if (newVal > record.data.qtytemp && isUpdate) {
										Ext.MessageBox.alert('Errors', 'Jumlah Qty Tidak Boleh Lebih dari Sisa Qty: ' + record.data.qtytemp + '');
										record.set('qty',record.data.qtytemp);
										return;
									}else if(newVal > record.data.qtytemp){
										Ext.MessageBox.alert('Errors', 'Jumlah Qty Tidak Boleh Lebih dari Sisa Qty: ' + record.data.qtytemp + '');
										record.set('qty',record.data.qtytemp);
										return;
									}
								}
							 }
							var subtotal=0, ppnn= 0;
							var s1 = 0;
							if (record.data.ppn==true) {
								ppnn = 0.1;
							} else if (record.data.ppn==false) {
								ppnn = 0;
							}
							s1 = ((record.data.hrgbeli * Ext.getCmp('tf.qty').getValue()) - record.data.diskonrp);
							subtotal = (s1 + (s1 * ppnn));
							record.set('subtotal',subtotal);
							diskon();
							diskonrp();
							if (name == 'mousedown') {
								record.set(this.dataIndex, !record.data[this.dataIndex]);
								subtotalhrgjualppn(rowIndex);						
								subtotalhrgjual();
							}
							//alert(subtotal);
						}else if(cek ==''){
							if (newVal <= 0) {
								record.set('qty',oldVal);
								if (oldVal <= 0) {
									record.set('qty',1);
								}
							 } else {
								if (record.data.qtytemp > 0) {
									if (newVal > record.data.qtytemp && isUpdate) {
										Ext.MessageBox.alert('Errors', 'Jumlah Qty Tidak Boleh Lebih dari Sisa Qty: ' + record.data.qtytemp + '');
										record.set('qty',record.data.qtytemp);
										return;
									}else if(newVal > record.data.qtytemp){
										Ext.MessageBox.alert('Errors', 'Jumlah Qty Tidak Boleh Lebih dari Sisa Qty: ' + record.data.qtytemp + '');
										record.set('qty',record.data.qtytemp);
										return;
									}
								}
							 }
							subtotalhrgjual();	
							diskon();
							diskonrp();
							if (name == 'mousedown') {
								record.set(this.dataIndex, !record.data[this.dataIndex]);
								subtotalhrgjualppn(rowIndex);							
								subtotalhrgjual();
							}
						}
					}
				}
			}
			
		},{
			header: headerGerid('Qty<br>Bonus'),
			dataIndex: 'qtyb',
			width: 50,
			xtype: 'numbercolumn', format:'0,000.00',
			editor: new Ext.form.TextField({
					id: 'tf.qtyb',
				enableKeyEvents: true,
			listeners : {
					change : function(){
			
					}
			}
			}),
		},{
			header: headerGerid('@Harga Beli'),
			dataIndex: 'hrgbeli',
			width: 90,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			editor: new Ext.form.TextField({
				enableKeyEvents: true,
				id: 'tf.hargabeli',
				listeners : {
					'change' : function(){
						subtotalhrgjual();
					}
				}
			}),
		},{
			header: headerGerid('PPN<br>(10%)'),
			dataIndex: 'ppn',
			width: 65,
			xtype: 'checkcolumn',
			id: 'cc.ppn',
			processEvent: function(name, e, grid, rowIndex, colIndex){
					var record = grid.store.getAt(rowIndex);		
					if (name == 'mousedown') {
						record.set(this.dataIndex, !record.data[this.dataIndex]);
						subtotalhrgjualppn(rowIndex);
					}

				}
		},{
			header: headerGerid('tamppn'),
			dataIndex: 'tamppn',
			width: 65,
			hidden: true
		},{
			header: headerGerid('Diskon<br>(%)'),
			dataIndex: 'diskon',
			width: 45,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00',
			editor: new Ext.form.TextField({
				id: 'tf.tdiskon',
					listeners : {
					change :function(field, newVal, oldVal){
							var record = ds_ppx.getAt(rownota);
							if (newVal < 0) {
								record.set('diskon',oldVal);
								if (oldVal < 0) {
									record.set('diskon',0);
								}
							 }
						diskon();
					}
				}
			}),
		},{
			header: headerGerid('Diskon<br>(Rp.)'),
			dataIndex: 'diskonrp',
			width: 80,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			editor: new Ext.form.TextField({
				id: 'tf.tdiskonrp',
					listeners : {
						change :function(field, newVal, oldVal){
							var record = ds_ppx.getAt(rownota);
							if (newVal < 0) {
								record.set('diskonrp',oldVal);
								if (oldVal < 0) {
									record.set('diskonrp',0);
								}
							 }
							diskonrp();
							subtotalhrgjual();
					}
				}
			}),
		},{
			header: headerGerid('Subtotal'),
			dataIndex: 'subtotal',
			width: 90,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: headerGerid('@Harga Beli<br>(Satuan Kecil)'),
			dataIndex: 'hrgbelikcl',
			width: 90,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			enableKeyEvents: true,
			editor: new Ext.form.TextField({
					id: 'tf.hrgbelikcl',
					listeners : {
					change : function(){
				}
			}
			}),
		},{
			header: headerGerid('Margin<br>(%)'),
			dataIndex: 'margin',
			width: 45,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00',
			editor: new Ext.form.TextField({
				id: 'tf.margin',
				listeners : {
						change :function(field, newVal, oldVal){
							var record = ds_ppx.getAt(rownota);
							if (newVal < 0) {
								record.set('margin',oldVal);
								if (oldVal < 0) {
									record.set('margin',0);
								}
							 }
							subtotalhrgjual();
					}
				}
			}),
		},{
			header: headerGerid('@Harga Jual'),
			dataIndex: 'hargajual',
			width: 100,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			enableKeyEvents: true,
			editor: new Ext.form.TextField({
					id: 'tf.qtyb',
					listeners : {
					change : function(){
				}
			}
			}),
		},{
			header: 'rasio',
			dataIndex: 'rasio',
			width: 100,
			hidden: true
		},{
			header: 'tampungtotppn',
			dataIndex: 'tmptotppn',
			width: 100,
			hidden: true,			
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			xtype: 'actioncolumn',
			width: 43,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					ds_ppx.removeAt(rowIndex);

				}
			}]
		}],
		
	});
/* end GRID */


/* BUAT PO FORM */
var po_form = new Ext.form.FormPanel({
			id: 'fp.daftarPO',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Buat Pembelian',
			autoScroll: true,
			tbar: [
				{text: 'Simpan', id: 'btn.simpan', iconCls: 'silk-save', 
				handler: function(){simpanPO();}},
				{text: 'Cetak', id:'btn.cetak', iconCls:'silk-printer',disabled: true, handler: function(){cetakPO();}},
				{text: 'Kembali', id: 'id.kembali', iconCls: 'silk-house', handler: function(){page_controller('120104');}},
				{xtype: 'tbfill'},
			],
			items: [{
				xtype: 'fieldset', //title: 'Order Pembelian',
				id: 'fs.orderpo', layout: 'column',
				defaults: { labelWidth: 150, labelAlign: 'right' }, 
				items:[{
					//column 1 left
					layout: 'form', columnWidth: 0.50,
					items: [{
						xtype: 'textfield',
						id: 'tf.nopo',
						width: 150,
						disabled: true,
						fieldLabel: 'No. Pembelian',
						style: 'font-weight: bold;text-align: center'
					},{
						xtype: 'datefield', 
						id: 'df.tglpo', 
						value: new Date(), 
						width: 100,
						format: 'd-m-Y',
						fieldLabel: 'Tanggal Pembelian'
					},{
						xtype: 'combo', fieldLabel: 'Jenis Pesanan',
						id:'cb.jnspp', width: 200, store: ds_jperpembelian,
						valueField: 'idjnspp', displayField: 'nmjnspp', editable: false,
						triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih....', //value: 1,
						listeners: {
									select : function(combo, records, eOpts){
									var obj = records.data;
									
										if(obj.idjnspp == 2){
											Ext.getCmp('tf.nopp').disable();
											Ext.getCmp('df.tglpp').disable();
											Ext.getCmp('btn.pp').disable();
											Ext.getCmp('tf.nopp').setValue(null);
											Ext.getCmp('tf.supplier').enable();
											Ext.getCmp('btn.sup').enable();

											Ext.getCmp('cb.stspo').disable();
											Ext.getCmp('cb.stspo').setValue('2');
											Ext.getCmp('df.tglpp').setValue(null);
											
											ds_ppx.setBaseParam('nopo','-');
											ds_ppx.reload();

										} else if(obj.idjnspp == 1){
											Ext.getCmp('tf.nopp').enable();
											Ext.getCmp('tf.supplier').disable();
											Ext.getCmp('tf.kdsupplier').disable();
											Ext.getCmp('btn.pp').enable();
											Ext.getCmp('btn.sup').disable();
											
											Ext.getCmp('cb.stspo').enable();
			
										}
									}
								}
					},{
						xtype: 'compositefield',
						fieldLabel: 'No. Pesanan',
						items:[{
							xtype: 'textfield', id: 'tf.nopp', width: 100, disabled: true
						},{
							xtype:'textfield', id: 'tf.idpp', width: 250, hidden: true
						},{
							xtype: 'button', text: ' .... ',
							width: 30,id: 'btn.pp',disabled: true,
							handler: function(){
								dftPP();
							}
						}]
					},{
						xtype: 'datefield', 
						id: 'df.tglpp', 
						value: new Date(), 
						width: 100,
						format: 'd-m-Y',
						fieldLabel: 'Tanggal Pesanan',
						disabled: true,
					}]
				},{
				//column 2 right
					layout: 'form', columnWidth: 0.50,
					items:[{
						xtype: 'compositefield',
						fieldLabel: 'Supplier',
						items:[{
							xtype: 'textfield', id: 'tf.supplier', width: 230,disabled: true
						},{
							xtype:'textfield', id: 'tf.kdsupplier', width: 250, hidden: true
						},{
							xtype: 'button', text: ' .... ',
							width: 30,id: 'btn.sup',disabled: true,
							handler: function(){
								dftSupplier();
							}
						}]
						
					},{
							xtype: 'textfield', id: 'tf.tlp', width: 230, readOnly: true, style:'opacity: 0.6', fieldLabel: 'No. Telepon'
					},{
							xtype: 'textfield', id: 'tf.fax', width: 230, readOnly: true, style:'opacity: 0.6', fieldLabel: 'Fax.'
					},{
							xtype:'textfield', id:'tf.npwp', width: 230, readOnly: true, style:'opacity: 0.6', fieldLabel: 'NPWP'	
					},{
							xtype: 'datefield', 
							id: 'df.tglkirim', 
							value: new Date(), 
							width: 100,
							format: 'd-m-Y',
							fieldLabel: 'Tanggal Terima',
							disabled: true,
							hidden: true,
					},{
							xtype: 'combo', id: 'cb.bagian', fieldLabel: 'Bagian',
							store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
							triggerAction: 'all', forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
							width: 230, allowBlank: false, editable: false,
							value: 'Farmasi/Apotik', readOnly: true,
							style : 'opacity:0.6',
							
					}]
				}]
			},{
				xtype: 'fieldset', title:'Rincian Pembelian', 
				items: [grid_po]
			},{
				xtype: 'fieldset', title: '',
				layout: 'column',
				defaults: { labelWidth: 150, labelAlign: 'right' },
				items:[{
				//column 2 left
					layout: 'form', columnWidth: 0.50,
					items: [{
							xtype:'textarea', id:'ta.ket', width: 350, height: 75, fieldLabel: 'Keterangan'
					},{
							xtype: 'compositefield',
							items:[{
								xtype: 'combo', fieldLabel: 'Approval',
								id:'cb.app1', width: 350, store: ds_app1,
								valueField: 'nmset', displayField: 'nmset', editable: false,allowBlank: false,
								triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
								emptyText:'Pilih....',value: 'Wenny'
							}]
						}]
				},{
					layout: 'form', columnWidth: 0.50,
					items: [{
							xtype: 'numericfield', id:'tf.jumlah', width: 220,
							disabled: true, fieldLabel: 'Jumlah'
							
					},{
							xtype: 'numericfield', id:'tf.totdiskon', width: 220,
							disabled: true, fieldLabel: 'Total Diskon'
							
					},{
						
							xtype: 'numericfield', id: 'tf.totppn', fieldLabel: 'Total PPN (10%)', width: 220, disabled: true
					},{
							xtype:'numericfield', id: 'tf.total', width: 220, disabled: true, fieldLabel: 'Total', style: 'font-weight:bold;text-align:right'
					}]
				}]
			}],
			listeners:{
				afterrender: module_afterrender
			}
}); SET_PAGE_CONTENT(po_form);
/*end PO FORM*/

	function module_afterrender () {
		
		
		if (isUpdate) {
		
			Ext.getCmp('tf.nopo').setValue(recordpo.data.nopo);
			Ext.getCmp('df.tglpo').setValue(recordpo.data.tglpo);
			Ext.getCmp('tf.nopp').setValue(recordpo.data.nopp);
			Ext.getCmp('cb.jnspp').setValue(recordpo.data.idjnspp);
			Ext.getCmp('cb.bagian').getValue(recordpo.data.idbagian);
			
			Ext.getCmp('tf.kdsupplier').setValue(recordpo.data.kdsupplier);
			Ext.getCmp('tf.supplier').setValue(RH.getRecordFieldValue(ds_supplier, 'nmsupplier', 'kdsupplier', recordpo.data.kdsupplier));
			Ext.getCmp("tf.tlp").setValue(RH.getRecordFieldValue(ds_supplier, 'notelp', 'kdsupplier', recordpo.data.kdsupplier));
			Ext.getCmp("tf.fax").setValue(RH.getRecordFieldValue(ds_supplier, 'nofax', 'kdsupplier', recordpo.data.kdsupplier));
			Ext.getCmp("tf.npwp").setValue(RH.getRecordFieldValue(ds_supplier, 'npwp', 'kdsupplier', recordpo.data.kdsupplier));
						
			Ext.getCmp('tf.reff').setValue(recordpo.data.bpb);
			Ext.getCmp('ta.ket').setValue(recordpo.data.ketpo);
			
			var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
			
			Ext.Ajax.request({
				url:BASE_URL + 'purchaseorder_controller/getDataPP',
				method:'POST',
				params: {
					nopp		: recordpo.data.nopp
				},
				success: function(response){
					obj = Ext.util.JSON.decode(response.responseText);
					
					
					if(recordpo.data.idjnspp == 1){
						Ext.getCmp('tf.nopp').enable();
						Ext.getCmp('tf.supplier').disable();
						Ext.getCmp('tf.kdsupplier').disable();
						Ext.getCmp('btn.pp').enable();
						Ext.getCmp('btn.sup').disable();
					}else{
						Ext.getCmp('tf.nopp').disable();
						Ext.getCmp('df.tglpp').disable();
						Ext.getCmp('btn.pp').disable();
						Ext.getCmp('cb.stspo').disable();
						Ext.getCmp('tf.supplier').enable();
						Ext.getCmp('btn.sup').enable();
					}
					Ext.getCmp('df.tglpp').setValue(obj.tglpp);
					Ext.getCmp('cb.syaratbayar').setValue(recordpo.data.idsypembayaran);
					Ext.getCmp('cb.jnsbayar').setValue(recordpo.data.idjnspembayaran);
					Ext.getCmp('cb.stspo').setValue(obj.idstpo);
					
					tempstpo = obj.idstpo;
					
					waitmsg.hide();
					
					if(recordpo.data.idjnspp == 1){
						ds_ppx.setBaseParam('nopp',recordpo.data.nopp);
						ds_ppx.setBaseParam('nopo',null);
					} else {
						ds_ppx.setBaseParam('nopo',recordpo.data.nopo);
						ds_ppx.setBaseParam('nopp',obj.null);
						Ext.getCmp('cb.stspo').setValue(2);
					}
						ds_ppx.reload();
						//ftotal();
				}
			});
			
		}
	}
	
function dftSupplier(){
		var cm_cari_supplier = new Ext.grid.ColumnModel([
            {
                header: 'Kode Supplier',
                dataIndex: 'kdsupplier',
                width: 100,
				renderer: keyToDetil_supplier
            }, {
                header: 'Nama Supplier',
                dataIndex: 'nmsupplier',
                width: 150
            }, {
                header: 'Alamat',
                dataIndex: 'alamat',
                width: 200
            }, {
                header: 'Tanggal Daftar',
                dataIndex: 'tgldaftar',
                width: 100
            }, {
                header: 'Email',
                dataIndex: 'email',
                width: 100
            }, {
                header: 'No. Telp./HP',
                dataIndex: 'notelp',
                width: 150
            }
        ]);
		 var sm_cari_supplier = new Ext.grid.RowSelectionModel({
            singleSelect: true
        });
        var vw_cari_supplier = new Ext.grid.GridView({
            emptyText: '< Belum ada Data >'
        });
		var paging_cari_supplier = new Ext.PagingToolbar({
            pageSize: 50,
            store: ds_supplier,
            displayInfo: true,
            displayMsg: 'Data Supplier Dari {0} - {1} of {2}',
            emptyMsg: 'No data to display'
        });
		var cari_supplier = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_cari_suppilier = new Ext.grid.GridPanel({
			id: 'gp.supplier',
            ds: ds_supplier,
            cm: cm_cari_supplier,
            sm: sm_cari_supplier,
            view: vw_cari_supplier,
            height: 400,
            width: 825,
			plugins: cari_supplier,
            autoSizeColumns: true,
            enableColumnResize: true,
            enableColumnHide: false,
            enableColumnMove: false,
            enableHdaccess: false,
            columnLines: true,
            loadMask: true,
            buttonAlign: 'left',
            layout: 'anchor',
            anchorSize: {
                width: 400,
                height: 400
            },
			tbar: [],
            bbar: paging_cari_supplier,
            listeners: {
            //    rowdblclick: klik_cari_supplier
				cellclick: onCellSupplier
            }
        });
		var win_supplier = new Ext.Window({
			title: 'Cari Supplier',
			modal: true,
			items: [grid_find_cari_suppilier]
		}).show();
	
	function onCellSupplier(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var rec_record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var rec_fsupplier = ds_supplier.getAt(rowIndex);
				var fkdsupplier = rec_fsupplier.data["kdsupplier"];
			var fnmsupplier = rec_fsupplier.data["nmsupplier"];
			var fnotelp = rec_fsupplier.data["notelp"];
			var fnofax = rec_fsupplier.data["nofax"];
			var fnpwp = rec_fsupplier.data["npwp"];
		
			
			Ext.getCmp("tf.kdsupplier").setValue(fkdsupplier);
			Ext.getCmp("tf.supplier").setValue(fnmsupplier);
			Ext.getCmp("tf.tlp").setValue(fnotelp);
			Ext.getCmp("tf.fax").setValue(fnofax);
			Ext.getCmp("tf.npwp").setValue(fnpwp);
			Ext.getCmp("btn_add").enable();
				win_supplier.close();
			}
			return true;
		}
	
	}

function dftPP(){
	var cm_fpp = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'nopp'
			},{
				header: 'No. SPB',
				dataIndex: 'nopp',
				width: 100,
				renderer: keyToDetil
			},{
				header: 'Tanggal SPB',
				dataIndex: 'tglpp',
				renderer: Ext.util.Format.dateRenderer('d-m-Y'),
				width: 100
			},{
				header: 'Bagian',
				dataIndex: 'nmbagian',
				width: 100
			},{
				header: 'Status Pesanan',
				dataIndex: 'nmstsetuju',
				width: 100
			},{
				header: 'User Input',
				dataIndex: 'nmlengkap',
				width: 150
			}
		]);
		 var sm_cari_pp = new Ext.grid.RowSelectionModel({
            singleSelect: true
        });
        var vw_cari_pp = new Ext.grid.GridView({
            emptyText: '< Belum ada Data >'
        });
		var paging_cari_pp = new Ext.PagingToolbar({
            pageSize: 50,
            store: ds_pp,
            displayInfo: true,
            displayMsg: 'Data Supplier Dari {0} - {1} of {2}',
            emptyMsg: 'No data to display'
        });
		var cari_pp = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_cari_pp = new Ext.grid.GridPanel({
			id: 'gp.pp',
            ds: ds_pp,
            cm: cm_fpp,
            sm: sm_cari_pp,
            view: vw_cari_pp,
            height: 400,
            width: 600,
			plugins: cari_pp,
            autoSizeColumns: true,
            enableColumnResize: true,
            enableColumnHide: false,
            enableColumnMove: false,
            enableHdaccess: false,
            columnLines: true,
            loadMask: true,
            buttonAlign: 'left',
            layout: 'anchor',
            anchorSize: {
                width: 400,
                height: 400
            },
			tbar: [],
            bbar: paging_cari_pp,
            listeners: {
            //    rowdblclick: klik_cari_pp
			cellclick: onCellClicAddbrg
            }
        });
		var win_pp = new Ext.Window({
			title: 'Cari Surat Pemesanan Barang (SPB)',
			modal: true,
			items: [grid_find_cari_pp]
		}).show();

function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var rec_record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var rec_fpp = ds_pp.getAt(rowIndex);
				var fpp_nopp = rec_fpp.data["nopp"];
				var fmatauang = rec_fpp.data["idmatauang"];
				var fkdmatauang = rec_fpp.data["kdmatauang"];
				var fnmmatauang = rec_fpp.data["nmmatauang"];
				var fkdsupplier = rec_fpp.data["kdsupplier"];
				var fnmsupplier = rec_fpp.data["nmsupplier"];
				var ftelp = rec_fpp.data["notelp"];
				var ffax = rec_fpp.data["nofax"];
				var fnpwp = rec_fpp.data["npwp"];
				var fkdbrg= rec_fpp.data["kdbrg"];
				var ftglpp = rec_fpp.data["tglpp"];
			
				Ext.getCmp("tf.nopp").setValue(fpp_nopp);
				Ext.getCmp("tf.kdsupplier").setValue(fkdsupplier);
				Ext.getCmp("tf.supplier").setValue(fnmsupplier);
				Ext.getCmp("tf.tlp").setValue(ftelp);
				Ext.getCmp("tf.fax").setValue(ffax);
				Ext.getCmp("tf.npwp").setValue(fnpwp);
				Ext.getCmp("df.tglpp").setValue(ftglpp);
				Ext.getCmp('cb.stspo').setValue('');
				Ext.getCmp('cb.syaratbayar').setValue('');
				Ext.getCmp('cb.jnsbayar').setValue('');
				Ext.getCmp('tf.totdiskon').setValue(0);
				Ext.getCmp('tf.totppn').setValue(0);
				Ext.getCmp('tf.total').setValue(0);
				
				Ext.getCmp("btn_add").disable();
				ds_ppx.setBaseParam('nopp',fpp_nopp);
				ds_ppx.setBaseParam('nopo',null);
				ds_ppx.reload();
				//ftotal();
				win_pp.close();
			}
			return true;
		}

}

function fbarang(){
	tpd();
		
	var cbGridPel = new Ext.grid.CheckboxSelectionModel({
			listeners: {
				rowselect : function( selectionModel, rowIndex, record){
					var skdbrg		= record.get("kdbrg");
					var snmbrg      = record.get("nmbrg");
					var sidsatuan	= record.get("nmsatuanbsr");
					var idsatuanbsr	= record.get("idsatuanbsr");
					var sharga		= record.get("harga");
					var sqty		= '1';//record.get("qty");
					var snmmatauang = record.get("nmmatauang");
					var skdmatauang = record.get("kdmatauang");
					var sidmatauang = record.get('idmatauang');
					//var sdiskon 	= record.get('diskon');
					var shtotal  	= record.get('hsubtotal');
					var shargabeli 	= record.get('hrgsatuanbsr');
					var shargajual 	= record.get('hrgjual');
					var shargajualtemp 	= record.get('hargajualtemp');
					var srasio 		= record.get('rasio');
					var smargin 	= record.get('margin');
					var starif		= parseInt(sqty) * parseInt(sharga) ;
					var shrgbeli	= parseInt(shargabeli) / parseInt(srasio) ;
					
					var orgaListRecord = new Ext.data.Record.create([
						{
							name: 'kdbrg',
							name: 'nmbrg',
							name: 'nmsatuanbsr',
							name: 'harga',
							name: 'subtotal',
							name: 'qty',
							name: 'nmmatauang',
							name: 'kdmatauang',
							name: 'idmatauang',
							name: 'diskon',
							name: 'diskonrp',
							name: 'hsubtotal',
							name: 'hrgbeli',
							name: 'idsatuanbsr',
							name: 'rasio',
							name: 'margin',
							name: 'hargajual',
							name: 'hargajualtemp',
							name: 'tmptotppn',
							name: 'tamppn',
						}
					]);
					
					ds_ppx.add([
						new orgaListRecord({
							'kdbrg': skdbrg,
							'nmbrg': snmbrg,
							'nmsatuanbsr': sidsatuan,
							'harga': sharga,
							'subtotal': shargabeli,
							'qty': sqty,
							'nmmatauang': snmmatauang,
							'kdmatauang': skdmatauang,
							'idmatauang': sidmatauang,
							'diskon' : 0,
							'diskonrp' : 0,
							'hsubtotal' : shtotal,
							'hrgbeli' : shargabeli,
							'idsatuanbsr' : idsatuanbsr,
							'hrgbelikcl' : shrgbeli,
							'rasio' : srasio,
							'margin' : smargin,
							'hargajual' : shargajual,
							'hargajualtemp' : shargajualtemp,
							'tmptotppn' : 0,
							'tamppn' : 0,
						})
					]);
					tpd();
					
				},
				rowdeselect : function( selectionModel, rowIndex, record){
					
				},
				beforerowselect : function (sm, rowIndex, keep, rec) {
					if (this.deselectingFlag && this.grid.enableDragDrop){
						this.deselectingFlag = false;
						this.deselectRow(rowIndex);
						return this.deselectingFlag;
					}
					//return keep;
				}
			}
		});
	
	var cm_fpel = new Ext.grid.ColumnModel([
			cbGridPel,
			{
				header: headerGerid('Kode barang'),
				width: 80,
				dataIndex: 'kdbrg'
			},{
				header: headerGerid('Nama Barang'),
				dataIndex: 'nmbrg',
				width: 200
			},{
				header: headerGerid('Jenis Barang'),
				dataIndex: 'nmjnsbrg',
				width: 100
			},{
				header: headerGerid('Satuan<br>Besar'),
				dataIndex: 'nmsatuanbsr',
				width: 60
			},{
				header: headerGerid('@Harga Satuan<br>Besar'),
				dataIndex: 'hrgsatuanbsr',
				align:'right',
				xtype: 'numbercolumn', format:'0,000',
				width: 90
			},{
				header: headerGerid('Rasio'),
				dataIndex: 'rasio',
				align:'right',
				xtype: 'numbercolumn', format:'0,000',
				width: 40
			},{
				header: headerGerid('Satuan<br>Kecil'),
				dataIndex: 'nmsatuankcl',
				width: 60
			},{
				header: headerGerid('@Harga Satuan<br>Kecil'),
				dataIndex: 'hrgbeli',
				align:'right',
				xtype: 'numbercolumn', format:'0,000',
				width: 90
			},{
				header: headerGerid('@Harga<br>Jual'),
				dataIndex: 'hrgjual',
				align:'right',
				xtype: 'numbercolumn', format:'0,000',
				width: 90
			},{
				header: headerGerid('Supplier'),
				dataIndex: 'nmsupplier',
				width: 200
			}
		]);
		var vw_fpel = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var page_fpel = new Ext.PagingToolbar({
			store: ds_brgmedis,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_fpel = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_fpel= new Ext.grid.GridPanel({
			id: 'gp.find_fpel',
			ds: ds_brgmedis,
			cm: cm_fpel,
			sm: cbGridPel,
			view: vw_fpel,
			height: 500,
			width: 1050,
			plugins: cari_fpel,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: page_fpel
		});
		var win_fpel = new Ext.Window({
			title: 'Cari Barang',
			modal: true,
			items: [grid_fpel]
		}).show();
		
	
	function tpd(){
			var arr = [];
			
			for (var zxc = 0; zxc <ds_ppx.data.items.length; zxc++) {
				var record = ds_ppx.data.items[zxc].data;
				zkditem = record.kdbrg;
				nmmatauang = record.nmmatauang;
				xkdmatauang = record.kdmatauang;
				idmatauang = record.idmatauang;
				xxx += record.harga;
				arr[zxc] = zkditem;
				
			}
			Ext.getCmp("tf.total").setValue(xxx);
			ds_brgmedis.setBaseParam('val',Ext.encode(arr));
			ds_brgmedis.reload();
		}
}	

//simpan
function simpanPO(){
var validas = Ext.getCmp('cb.app1').getValue();
var syaratbayar = Ext.getCmp('cb.syaratbayar').getValue();
var jnsbayar = Ext.getCmp('cb.jnsbayar').getValue();
var stspo = Ext.getCmp('cb.stspo').getValue();

if (tempstpo==2 && Ext.getCmp('cb.jnspp').getValue()==1) {
	Ext.MessageBox.alert('Errors', 'Pesanan Sudah Complete!');
} else {

	if(validas && syaratbayar && jnsbayar && stspo == ''){
		Ext.MessageBox.alert('Errors', 'Data Belum Valid (data primer belum terisi)!');
	}else{
		var arrpo = [];
		var arrstok = [];
		for(var zx = 0; zx < ds_ppx.data.items.length; zx++){
			var record = ds_ppx.data.items[zx].data;
			zkdbrg = record.kdbrg;
			zidsatuan = record.idsatuanbsr;
			zidstpp = record.idstpp;
		/* 	if(zidstpp == 1){
			
			zqty = record.qty - 1;
			}else{
			zqty = record.qty;
			
			} */
			zqty = record.qty;
			zqtyb = record.qtyb;
			zhargabeli = record.hrgbeli;
			zdiskon = record.diskon;
			zdiskonrp = record.diskonrp;
			zhargajual = record.hargajual;
			zrasio = record.rasio;
			zstoknow = record.stoknowbagian;
			
			zmargin = record.margin;
			zppn = record.tamppn;
			arrpo[zx] = zkdbrg + '-' + zidsatuan + '-' + zqty + '-' + zqtyb + '-' + zhargabeli + '-' + zdiskon + '-' + zhargajual  + '-' + zrasio + '-' + zstoknow + '-' + zmargin + '-' + zppn+ '-' + zdiskonrp;
		}
		if(Ext.getCmp('tf.nopo').getValue() != ''){
			Ext.Ajax.request({
			url: BASE_URL + 'purchaseorder_controller/updateAll',
			params: {
				nopo : Ext.getCmp('tf.nopo').getValue(),
				tglpo : Ext.getCmp('df.tglpo').getValue(),
				idjnspp : Ext.getCmp('cb.jnspp').getValue(),
				idbagian : Ext.getCmp('cb.bagian').getValue(),
				kdsupplier : Ext.getCmp('tf.kdsupplier').getValue(),
				idsypembayaran : Ext.getCmp('cb.syaratbayar').getValue(),
				idjnspembayaran : Ext.getCmp('cb.jnsbayar').getValue(),
				tglpengiriman : Ext.getCmp('df.tglkirim').getValue(),
				idstpo : Ext.getCmp('cb.stspo').getValue(),
				tglpp : Ext.getCmp('df.tglpp').getValue(),
				bpb : Ext.getCmp('tf.reff').getValue(),
				nopp : Ext.getCmp('tf.nopp').getValue(),
				keterangan : Ext.getCmp('ta.ket').getValue(),
				totalpo : Ext.getCmp('tf.total').getValue(),
				approval1 : Ext.getCmp('cb.app1').getValue(),
				
				arrpo : Ext.encode(arrpo)
				
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi','Update Data Berhasil');
				obj = Ext.util.JSON.decode(response.responseText);
				console.log(obj);
				//Ext.getCmp("tf.nopo").setValue(nopo);
				},
			failure : function(){
				Ext.MessageBox.alert('Informasi','Update Data Gagal');
			}
		});
		}else{
		Ext.Ajax.request({
			url: BASE_URL + 'purchaseorder_controller/insorupd_po',
			params: {
				nopo : Ext.getCmp('tf.nopo').getValue(),
				tglpo : Ext.getCmp('df.tglpo').getValue(),
				idjnspp : Ext.getCmp('cb.jnspp').getValue(),
				idbagian : Ext.getCmp('cb.bagian').getValue(),
				kdsupplier : Ext.getCmp('tf.kdsupplier').getValue(),
				idsypembayaran : Ext.getCmp('cb.syaratbayar').getValue(),
				idjnspembayaran : Ext.getCmp('cb.jnsbayar').getValue(),
				tglpengiriman : Ext.getCmp('df.tglkirim').getValue(),
				idstpo : Ext.getCmp('cb.stspo').getValue(),
				tglpp : Ext.getCmp('df.tglpp').getValue(),
				nmsupplier : Ext.getCmp('tf.supplier').getValue(),
				bpb : Ext.getCmp('tf.reff').getValue(),
				nopp : Ext.getCmp('tf.nopp').getValue(),
				keterangan : Ext.getCmp('ta.ket').getValue(),
				totalpo : Ext.getCmp('tf.total').getValue(),
				approval1 : Ext.getCmp('cb.app1').getValue(),
				arrpo : Ext.encode(arrpo)
				
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
				obj = Ext.util.JSON.decode(response.responseText);
				console.log(obj);
				Ext.getCmp("tf.nopo").setValue(obj.nopo);
				Ext.getCmp("btn.cetak").enable();
				Ext.getCmp("btn.simpan").disable();
			},
			failure : function(){
				Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
			}
		});
	}
		
	}
}
}

function counttotal() {
	var jumlah = 0, diskonrp1 = 0, tamtotppn = 0; total = 0;
			ds_ppx.each(function(rec){
				jumlah += (parseFloat(rec.get('hrgbeli')) * parseFloat(rec.get('qty'))); 
				diskonrp1 += parseFloat(rec.get('diskonrp'));
				if (rec.get('ppn')=='0') {
					var ppnn = 0;
					var tamppn = 0;
					ppnn += 0; //jika ppn di ceklist hitungan ppn= 0
					rec.set('tmptotppn',ppnn);
					rec.set('tamppn',tamppn);	
				} else if (rec.get('ppn')=='1') {
					var ppnn = 0;
					var tamppn = 10;
					ppnn += (rec.get('hrgbeli') * rec.get('qty') - rec.get('diskonrp')) * 0.1; //jika ppn di ceklist hitungan ppn= ((hrgbeli * qty) - diskonrp) * 0.1
					rec.set('tmptotppn',ppnn);
					rec.set('tamppn',tamppn);
				}
				tamtotppn += parseFloat(rec.get('tmptotppn'));				
			});

			total = jumlah - diskonrp1 + tamtotppn;
			//alert(jumlah + ' ? ' + diskonrp + ' ? ' + ppn + ' ? ' + total);
			Ext.getCmp("tf.jumlah").setValue(jumlah);
			Ext.getCmp("tf.totdiskon").setValue(diskonrp1);
			Ext.getCmp("tf.totppn").setValue(tamtotppn);
			Ext.getCmp("tf.total").setValue(total);

}

function ftotal(){
	/* ds_ppx.reload({
		scope : this,
		callback: function(records, operation, success){ */
			/* var jumlah = 0, diskonrp = 0, ppn = 0, total = 0;
			ds_ppx.each(function(rec){ 
				jumlah += (parseFloat(rec.get('hrgbeli')) * parseFloat(rec.get('qty'))); 
				diskonrp += parseFloat(rec.get('diskonrp'));
				if (rec.get('ppn')=='1') {
					ppn += (rec.get('hrgbeli') * rec.get('qty') - diskonrp) * 0.1; //jika ppn di ceklist hitungan ppn= ((hrgbeli * qty) - diskonrp) * 0.1
					//alert('CCC');
				} else if (rec.get('ppn')=='0') {
					ppn += 0; //jika ppn di ceklist hitungan ppn= 0
					//alert('DDD');
				}
				
			});
			total = jumlah - diskonrp + ppn;
			//alert(jumlah + ' ? ' + diskonrp + ' ? ' + ppn + ' ? ' + total);
			Ext.getCmp("tf.jumlah").setValue(jumlah);
			Ext.getCmp("tf.totdiskon").setValue(diskonrp);
			Ext.getCmp("tf.totppn").setValue(ppn);
			Ext.getCmp("tf.total").setValue(total); */
			
			var jumlah = 0, diskonrp1 = 0, tamtotppn = 0; total = 0;
			ds_ppx.each(function(rec){
				jumlah += (parseFloat(rec.get('hrgbeli')) * parseFloat(rec.get('qty'))); 
				diskonrp1 += parseFloat(rec.get('diskonrp'));
				if (rec.get('ppn')=='0') {
					var ppnn = 0;
					ppnn += 0; //jika ppn di ceklist hitungan ppn= 0
					rec.set('tmptotppn',ppnn);
				} else if (rec.get('ppn')=='1') {
					var ppnn = 0;
					ppnn += (rec.get('hrgbeli') * rec.get('qty') - rec.get('diskonrp')) * 0.1; //jika ppn di ceklist hitungan ppn= ((hrgbeli * qty) - diskonrp) * 0.1
					rec.set('tmptotppn',ppnn);
				}
				tamtotppn += parseFloat(rec.get('tmptotppn'));				
			});			
			
			//subtotalhrgjual();
			total = jumlah - diskonrp1 + tamtotppn;
			//alert(jumlah + ' ? ' + diskonrp + ' ? ' + ppn + ' ? ' + total);
			Ext.getCmp("tf.jumlah").setValue(jumlah);
			Ext.getCmp("tf.totdiskon").setValue(diskonrp1);
			Ext.getCmp("tf.totppn").setValue(tamtotppn);
			Ext.getCmp("tf.total").setValue(total);
		/* }
	}); */
}

function diskonrp(){
	var record = ds_ppx.getAt(rownota);
	
	if (!record.data.qty) return; //jika qty kosong return
	
	var calcdiskon = (record.data.diskonrp /(record.data.hrgbeli * record.data.qty)) * 100; //isi diskonpersen = (diskonrp / (hrgbeli * qty)) * 100
	record.set('diskon',calcdiskon);
	
	counttotal();
}

function diskon(){
	var record = ds_ppx.getAt(rownota);
	
	if (!record.data.qty) return; //jika qty kosong return
	
	var calcdiskonrp = (record.data.diskon / 100) * (record.data.hrgbeli * record.data.qty); //isi diskonrp = (diskonrp / 100) * (hrgbeli * qty)
	record.set('diskonrp',calcdiskonrp);
	
	subtotalhrgjual(); //langsung hitung subtotal lewat isi diskonrp
	
	counttotal();
}

function subtotalhrgjual(){
	var record = ds_ppx.getAt(rownota);
	
	var ppn=0;
	var ppnn=0;
	var diskonrp=0;
	var subtotal=0;
	var hargajual=0;
	var margin=0;
	var hsatuankecil = 0;
	//var hb1 = 0;
	//var hb2 = 0;
	var hj1 = 0;
	var hj2 = 0;
	
	var s1 = 0;
	
	if (!record.data.qty) { return; } //jika qty kosong return
	if (!record.data.diskonrp) { diskonrp=0; } //jika diskonrp kosong set default 0
	
	if (record.data.ppn==true) {
		ppn = (record.data.hrgbeli * record.data.qty) * 0.1; //jika ppn di ceklist hitungan ppn= ((hrgbeli * qty) - diskonrp) * 0.1
		ppnn = 0.1;
	} else if (record.data.ppn==false) {
		ppn = 0; //jika ppn di ceklist hitungan ppn= 0
		ppnn = 0;
	}
	
	if (record.data.margin) {
		margin=parseFloat(record.data.margin);
	} else {
		margin=0;
	}

	//subtotal = ((record.data.hrgbeli * record.data.qty) - record.data.diskonrp)+ppn; //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
	s1 = ((record.data.hrgbeli * record.data.qty) - record.data.diskonrp); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
	subtotal = (s1 + (s1 * ppnn));
	record.set('subtotal',subtotal);
	
	hsatuankecil = (parseFloat(record.data.subtotal) / (parseFloat(record.data.qty) * parseFloat(record.data.rasio))); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
	record.set('hrgbelikcl',hsatuankecil);
	//var hb1 = (record.data.hrgbeli / record.data.rasio); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
	//var hb2 = (hb1 * (record.data.diskon / 100));
	//var hsatuankecil = (hb1 - hb2);
	//record.set('hrgbelikcl',hsatuankecil);
	//alert(hsatuankecil);
	
	/* var t = Ext.getCmp("tf.totdiskon").getValue();
	if(t !='0'){
		hb2 = (parseInt(record.data.hrgbelikcl) - ((parseInt(record.data.hrgbelikcl) * parseInt(record.data.diskonrp)))); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
		hsatuankecil = (parseInt(hb2) + ((parseInt(hb2) * 0.1))); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
		record.set('hrgbelikcl',hsatuankecil);
		alert('ZZZ');
	}else if(t == '0'){
		hb1 = (parseInt(record.data.hrgbeli) / parseInt(record.data.rasio));
		hb2 = (parseInt(hb1) - ((parseInt(hb1) * parseInt(record.data.diskonrp)))); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
		//hsatuankecil = (parseInt(hb2) / (parseInt(record.data.qty) * parseInt(record.data.rasio))); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
		record.set('hrgbelikcl',hb2);
		alert(hb2);
	} */
	
	//hargajual = ((record.data.hrgbeli * record.data.qty) + ppn) * (margin / 100); //hargajual = ((hrgbeli * qty) + ppn) * margin
	//hargajual = ((record.data.hrgbeli * (margin / 100)) + parseInt(record.data.hrgbeli)); //hargajual = ((hrgbeli * qty) + ppn) * margin
	
	hj1 = (record.data.hrgbeli / record.data.rasio);
	hj2 = parseInt(hj1) + (parseInt(hj1) * ppnn);
	hargajual = parseInt(hj2) + (parseInt(hj2) * (parseInt(record.data.margin) / 100));
	record.set('hargajual',hargajual);
	
	counttotal();
	
	//catatan: diskonrp dan ppn tidak berpengaruh terhadap penjumlahan dan pengurangan jika nilainya 0
}

function subtotalhrgjualppn(rowIndex){
	var record = ds_ppx.getAt(rowIndex);
	
	var ppn=0;
	var ppnn=0;
	var subtotal=0;
	var hargajual=0;
	var margin=0;
	var s1 = 0;
	var hj1 = 0;
	var hj2 = 0;
	
	if (record.data.ppn==true) {
		ppn = (record.data.hrgbeli * record.data.qty) * 0.1; //jika ppn di ceklist hitungan ppn= ((hrgbeli * qty) - diskonrp) * 0.1
		ppnn = 0.1;
	} else if (record.data.ppn==false) {
		ppn = 0; //jika ppn di ceklist hitungan ppn= 0
		ppnn = 0;
	}
	
	if (record.data.margin) {
		margin=parseFloat(record.data.margin);
	} else {
		margin=0;
	}
	
	s1 = ((record.data.hrgbeli * record.data.qty) - record.data.diskonrp); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
	subtotal = (s1 + (s1 * ppnn));
	record.set('subtotal',subtotal);
	
	hsatuankecil = (parseFloat(record.data.subtotal) / (parseFloat(record.data.qty) * parseFloat(record.data.rasio))); //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
	record.set('hrgbelikcl',hsatuankecil);
	
	//subtotal = (record.data.hrgbeli * record.data.qty) + ppn; //subtotal = ((hrgbeli * qty) - diskonrp) + ppn
	//record.set('subtotal',subtotal);
	//record.set('hrgbelikcl',subtotal);
	//hargajual = ((record.data.hrgbeli * record.data.qty) + ppn) * (margin / 100); //hargajual = ((hrgbeli * qty) + ppn) * margin
	//record.set('hargajual',hargajual);
	
	hj1 = (record.data.hrgbeli / record.data.rasio);
	hj2 = parseInt(hj1) + (parseInt(hj1) * ppnn);
	hargajual = parseInt(hj2) + (parseInt(hj2) * (parseInt(record.data.margin) / 100));
	record.set('hargajual',hargajual);
	
	counttotal();
	
	//catatan: diskonrp dan ppn tidak berpengaruh terhadap penjumlahan dan pengurangan jika nilainya 0
}

function cetakPO(){
	var nopo = Ext.getCmp('tf.nopo').getValue();
	RH.ShowReport(BASE_URL + 'print/print_po/po_pdf/' + nopo);
}

function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:12px;text-decoration:underline;">'
			+ value +'</div>';
	}

function keyToDetil_supplier(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:12px;text-decoration:underline;">'
			+ value +'</div>';
}


}