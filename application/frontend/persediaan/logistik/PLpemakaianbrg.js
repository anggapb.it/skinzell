function PLpemakaianbrg(){
	var pageSize = 18;
	//var ds_bagian = dm_bagian();
	var ds_stsetuju = dm_stsetuju();
	var ds_sttransaksi = dm_sttransaksi();
	var ds_pemakaianbrg = dm_pemakaianbrg();
	var ds_pemakaianbrgdet = dm_pemakaianbrgdet();
		ds_pemakaianbrgdet.setBaseParam('nopakaibrg','null');
	var row = '';
	
	var ds_bagian = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'pemakaianbrg_controller/getNmbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "idjnspelayanan",
			mapping: "idjnspelayanan"
		},{
			name: "nmjnspelayanan",
			mapping: "nmjnspelayanan"
		},{
			name: "idbdgrawat",
			mapping: "idbdgrawat"
		},{
			name: "nmbdgrawat",
			mapping: "nmbdgrawat"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_bagian.setBaseParam('userid', USERID);
	
	var arr_cari = [['nopakaibrg', 'No. Pakai Barang'],['nmlengkap', 'User Input'],['nmbagian', 'Bagian'],['nmstsetuju', 'Status Persetujuan'],['nmsttransaksi', 'Status Transaksi']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	function fnSearchgrid(){
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
			ds_pemakaianbrg.setBaseParam('key',  '1');
			ds_pemakaianbrg.setBaseParam('id',  idcombo);
			ds_pemakaianbrg.setBaseParam('name',  nmcombo);
		ds_pemakaianbrg.load(); 
	}
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_pemakaianbrg,
		displayInfo: true,
		displayMsg: 'Data Pemakaian Barang Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var vw_pemakaianbrg = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail kasir" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var grid_pemakaianbrg = new Ext.grid.GridPanel({
		id: 'grid_pemakaianbrg',
		store: ds_pemakaianbrg,
		view: vw_pemakaianbrg,
		autoScroll: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddPemakainabrg();
				Ext.getCmp('tf.carinopbdet').setValue();
				Ext.getCmp('df.tglpakaibrg').setValue(new Date());
				Ext.getCmp('tf.reckdbrg').setValue();
				Ext.getCmp('bt.batal').disable();
				Ext.getCmp('bt.cetak').disable();
			}
		},'-',{
			xtype: 'compositefield',
			width: 455,
			items: [{
				xtype: 'label', text: 'Search :', margins: '4 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.search',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				margins: '2 5 0 0',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.search').getValue();
							if(cbsearchh != ''){
								Ext.getCmp('cek').enable();
								Ext.getCmp('cek').focus();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'cek',
				width: 200,
				margins: '2 5 0 0',
				disabled: true,
				validator: function(){
					fnSearchgrid();
				}
			}]
		},{
			xtype: 'textfield',
			id:'tf.userid',
			width: 60,
			value: USERID,
			hidden: true,
			validator: function(){
				ds_pemakaianbrg.setBaseParam('userid', Ext.getCmp('tf.userid').getValue());
				ds_pemakaianbrg.reload();
			}
		}],
		autoHeight: true,
		columnLines: true,
		//plugins: cari_data,
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. <br> Pemakaian'),
			width: 85,
			dataIndex: 'nopakaibrg',
			sortable: true,
			align:'center'
		},
		{
			header: headerGerid('Tgl <br> Pemakaian'),
			width: 80,
			dataIndex: 'tglpakaibrg',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Jam'),
			width: 60,
			dataIndex: 'jampakaibrg',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('User Input'),
			width: 140,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Bagian'),
			width: 160,
			dataIndex: 'nmbagian',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Status <br> Persetujuan'),
			width: 86,
			dataIndex: 'nmstsetuju',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Status <br> Transaksi'),
			width: 159,
			dataIndex: 'nmsttransaksi',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Keterangan'),
			width: 160,
			dataIndex: 'keterangan',
			sortable: true,
			align:'center',
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditPemakaianbrg(grid, rowIndex);
						var nopakaibrg = RH.getCompValue('tf.nopakaibrg', true);
						if(nopakaibrg != ''){
							RH.setCompValue('tf.carinopbdet', nopakaibrg);
							Ext.getCmp('tf.reckdbrg').setValue(nopakaibrg);
						}
						var setuju = RH.getCompValue('cb.stsetuju', true);
						if(setuju != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('cb.stsetuju').setReadOnly(true);
						}else{							
							Ext.getCmp('bt.batal').disable();
						}
						var sttransaksi = RH.getCompValue('cb.sttransaksi', true);
						if(sttransaksi != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('bt.batal').disable();
						}
						Ext.getCmp('btn_data_pen_bgn').disable();
						return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeletePemakaianbrg(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Cetak',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetakPembrg(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging		
	});	
	       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Daftar Pemakaian Barang', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_pemakaianbrg]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadPemakaianbrg(){
		ds_pemakaianbrg.reload();
	}
	
	function fnAddPemakainabrg(){
		var grid = grid_pemakaianbrg;
		wEntryPemakaianbrg(false, grid, null);	
	}
	
	function fnEditPemakaianbrg(grid, record){
		var record = ds_pemakaianbrg.getAt(record);
		wEntryPemakaianbrg(true, grid, record);		
	}
	
	function fnDeletePemakaianbrg(grid, record){
		var record = ds_pemakaianbrg.getAt(record);
		var cekstatus = record.data['idstsetuju'];
		if(cekstatus!=1){
			Ext.MessageBox.alert('Informasi','Data sudah tidak bisa dihapus..');
		}else{
			var url = BASE_URL + 'pemakaianbrg_controller/delete_pemakaianbrg';
			var params = new Object({
							nopakaibrg	: record.data['nopakaibrg']
						});
			RH.deleteGridRecord(url, params, grid );
		}
		return;
	}
	
	function cetakPembrg(grid, record){
		var record = ds_pemakaianbrg.getAt(record);
		var nopakaibrg = record.data['nopakaibrg'] 
		RH.ShowReport(BASE_URL + 'print/print_pakaibrg/pakaibrg_pdf/' + nopakaibrg);
	}
	
	/**
WIN - FORM ENTRY/EDIT 
*/

	function wEntryPemakaianbrg(isUpdate, grid, record){
		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.jampakaibrg"))
					RH.setCompValue("tf.jampakaibrg",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
				
		var paging_daftar_pemakaianbrg = new Ext.PagingToolbar({
			pageSize: pageSize,
			store: ds_pemakaianbrgdet,
			displayInfo: true,
			displayMsg: 'Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var vw_daftar_pemakaianbrg = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_daftar_pemakaianbrg = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_pemakaianbrg',
			store: ds_pemakaianbrgdet,
			view: vw_daftar_pemakaianbrg,
			tbar: [{
				text: 'Tambah',
				id: 'btn_tmbh',
				iconCls: 'silk-add',
				handler: function() {
					var cekbagian = Ext.getCmp('tf.bagian').getValue();
					if(cekbagian != ""){
						fncariBarang();
					}else{
						Ext.MessageBox.alert('Informasi','Bagian belum dipilih');
					}
				}
			},{
				xtype: 'compositefield',
				width: 515,
				items: [{
					xtype: 'label', text: 'Status Transaksi :', margins: '3 5 0 235px',
				},{
					xtype: 'combo',
					id: 'cb.sttransaksi',
					store: ds_sttransaksi, 
					valueField: 'idsttransaksi', displayField: 'nmsttransaksi',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 165, editable: false, value: 1,
					readOnly: true, style: 'opacity: 0.6'
				}]
			},{
				xtype: 'compositefield',
				width: 240,
				items: [{
					xtype: 'label', text: 'Status Persetujuan :', margins: '3 5 0 0',
				},{
					xtype: 'combo',
					id: 'cb.stsetuju',
					store: ds_stsetuju, 
					valueField: 'idstsetuju', displayField: 'nmstsetuju',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 130, allowBlank: false, editable: false, value: 2
				}]
			},{
				xtype: 'textfield',
				id:'tf.carinopbdet',
				width: 60,
				hidden: true,
				validator: function(){
					ds_pemakaianbrgdet.setBaseParam('nopakaibrg', Ext.getCmp('tf.carinopbdet').getValue());
					ds_pemakaianbrgdet.reload();					
				}
			},{
				xtype: 'textfield',
				id:'tf.reckdbrg',
				width: 60,
				hidden: true,							
			}],
			autoScroll: true,
			height: 360, //autoHeight: true,
			columnLines: true,
			frame: true,
			clicksToEdit: 1,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('Kode Barang'),
				width: 85,
				dataIndex: 'kdbrg',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Nama Barang'),
				width: 237,
				dataIndex: 'nmbrg',
				sortable: true,
				align:'center',
			},
			{
				header: headerGerid(' ID Jenis Barang'),
				width: 100,
				dataIndex: 'idjnsbrg',
				sortable: true,
				hidden: true,
			},
			{
				header: headerGerid('Jenis Barang'),
				width: 97,
				dataIndex: 'nmjnsbrg',
				sortable: true,
				align:'center',
			},{
				header: headerGerid('id Satuan'),
				width: 100,
				dataIndex: 'idsatuankcl',
				sortable: true,
				hidden: true
			},{
				header: headerGerid('Satuan'),
				width: 97,
				dataIndex: 'nmsatuan',
				sortable: true,
				align:'center',
			},{
				header: headerGerid('Stok <br> Sekarang'),
				width: 79,
				dataIndex: 'stoknowbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			},{
				header: headerGerid('Qty'),
				width: 55,
				dataIndex: 'qty',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				editor: {
					xtype: 'numberfield',
					id: 'tfgp.qty',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							var record = ds_pemakaianbrgdet.getAt(row);
							var stoknowbagian = record.data.stoknowbagian;
							var qty = Ext.getCmp('tfgp.qty').getValue();
							if(qty == stoknowbagian){
								//Ext.MessageBox.alert('Informasi','Anda yakin akan meretur semua Qty Sisa..?');
							}else if(qty >= stoknowbagian){
								Ext.MessageBox.alert('Informasi','Qty tidak boleh lebih dari Stok Sekarang');
								Ext.getCmp('tfgp.qty').setValue();
							}
							return;
						}
					}
				}
			},{
				header: headerGerid('Catatan'),
				width: 135,
				dataIndex: 'catatan',
				sortable: true,
				align:'center',editor: {
					xtype: 'textfield',
					id: 'tfgp.catatan', 
					enableKeyEvents: true,
				}
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							/* var winTitle = (isUpdate)?'Pemakaian Barang (Edit)':'Pemakaian Barang (Entry)';
							if(winTitle == 'Pemakaian Barang (Edit)'){								
								Ext.getCmp('btn_simpan').disable();
								Ext.getCmp('btn_tmbh').disable();
							}else{
								Ext.getCmp('btn_simpan').disable();
							}*/
							ds_pemakaianbrgdet.removeAt(rowIndex);
							//return; 
						}
					}]
			}],
			listeners	: {
				rowclick : function(grid, rowIndex, e){
					row = rowIndex;
				}
			},
			//bbar: paging_daftar_pemakaianbrg
		});
		
		var winTitle = (isUpdate)?'Pemakaian Barang (Edit)':'Pemakaian Barang (Entry)';
		var pemakaianbrg_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.pemakaianbrg',
			buttonAlign: 'left',
			labelWidth: 165, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 555, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					var stso = Ext.getCmp('cb.stsetuju').getValue();
					if (stso != 1){
						Ext.getCmp('df.tglpakaibrg').setReadOnly(true);
						Ext.getCmp('cb.stsetuju').setReadOnly(true);
					}else{
						Ext.getCmp('df.tglpakaibrg').setReadOnly(false);
					}
					simpanPbrg();
					return;	
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', id: 'bt.cetak', style: 'marginLeft: 5px',
				handler: function() {
					cetak();
				}
			},{
				text: 'Batal', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px',
				handler: function() {
					batalPbrg();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					Ext.getCmp('tf.carinopbdet').setValue();
					ds_pemakaianbrg.reload();
					wPemakaianbrg.close();
			}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 105, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.49, border: false, layout: 'form',					
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. Pemakaian',
						id:'tf.nopakaibrg',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tgl/Jam Pemakaian',
						items:[{
							xtype: 'datefield',
							id: 'df.tglpakaibrg',
							format: 'd-m-Y',
							//value: new Date(),
							width: 120,
						},{
							xtype: 'label', id: 'lb.time', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.jampakaibrg',
							width: 60,
						}]
					},{
						xtype: 'compositefield',
						style: 'marginLeft: -48px;',
						width: 280,
						items: [{
							xtype: 'label', id: 'lb.bgn', text: 'Bagian:', margins: '3 5 0 4',
						},{
							xtype: 'textfield',
							id: 'tf.bagian',
							emptyText:'Nama Bagian..',
							width: 205,
							readOnly: true,
							allowBlank: false
						},
						{
							xtype: 'button',
							iconCls: 'silk-find',
							id: 'btn_data_pen_bgn',
							width: 3,
							handler: function() {
								fnwBagian();
							}
						},{
							xtype: 'textfield',
							id: 'tf.tamidbagian',
							width: 20,
							hidden: true,							
						}]
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					items: [/* {
						xtype: 'compositefield',
						style: 'marginLeft: -48px;',
						width: 275,
						items: [{
							xtype: 'label', id: 'lb.bgn', text: 'Bagian :', margins: '3 5 0 0',
						},{
							xtype: 'textfield',
							id: 'tf.bagian',
							emptyText:'Nama Bagian..',
							width: 200,
							readOnly: true,
							allowBlank: false
						},
						{
							xtype: 'button',
							iconCls: 'silk-find',
							id: 'btn_data_pen_bgn',
							width: 3,
							handler: function() {
								fnwBagian();
							}
						},{
							xtype: 'textfield',
							id: 'tf.tamidbagian',
							width: 20,
							hidden: true,							
						}]
					}, */
					{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						id  : 'ta.keterangan',
						width : 200,
						height: 45,
					},{
						xtype: 'textfield',
						fieldLabel: 'User Input',
						id: 'tf.nmpenerima',
						width: 200,
						readOnly: true,
						value: USERNAME,
						style : 'opacity:0.6'
						
					}]
				}]
			},{
				xtype: 'fieldset',
				title: 'Daftar Pemakaian Barang',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 400,
				items: [grid_daftar_pemakaianbrg]
			}]
		});
			
		var wPemakaianbrg = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [pemakaianbrg_form]
		});
		
	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setPemakaianbrgForm(isUpdate, record);
		wPemakaianbrg.show();

	/**
	FORM FUNCTIONS
	*/	
		function setPemakaianbrgForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'pemakaianbrg_controller/getNmbagian',
						params:{
							idbagian : record.get('idbagian')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.bagian', r);
						}
					});
					
					RH.setCompValue('tf.nopakaibrg', record.get('nopakaibrg'));
					RH.setCompValue('df.tglpakaibrg', record.get('tglpakaibrg'));
					//RH.setCompValue('tf.jampakaibrg', record.get('jampakaibrg'));
					RH.setCompValue('tf.bagian', record.get('idbagian'));
					RH.setCompValue('tf.tamidbagian', record.get('idbagian'));
					RH.setCompValue('cb.sttransaksi', record.get('idsttransaksi'));
					RH.setCompValue('cb.stsetuju', record.get('idstsetuju'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					return;
				}
			}
		}
		
		function fnSavePemakaianbrg(){
			var idForm = 'frm.pemakaianbrg';
			var sUrl = BASE_URL +'pemakaianbrg_controller/insert_update_pemakaianbrg';
			var sParams = new Object({
				pembelianbrg	:	RH.getCompValue('tf.pemakaian'),
				tgltutup		:	RH.getCompValue('df.pemakaian'),
				jamtutup		:	RH.getCompValue('tf.bagian'),
				idshifttutup	:	RH.getCompValue('tf.idsifttutup'),
				saldoakhir		:	RH.getCompValue('nf.saldoakhirinput'),
				selisih			:	RH.getCompValue('nf.selisih'),
				catatantutup	:	RH.getCompValue('ta.catatantutup'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'pemakaianbrg_controller/insert_update_pemakaianbrg';
				msgSuccess = 'Tutup kasir berhasil';
				msgFail = 'Tutup kasir gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wPemakaianbrg, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
		
		function simpanPbrg(){
			var cekbagian = Ext.getCmp('tf.tamidbagian').getValue();
			
			if(cekbagian != ""){
				var reckdbrg = Ext.getCmp('tf.reckdbrg').getValue();
				if(reckdbrg != ""){
					var recstsetuju = Ext.getCmp('cb.stsetuju').getValue();
					if(recstsetuju != 1){
						var arrpbrg = [];
						for(var zx = 0; zx < ds_pemakaianbrgdet.data.items.length; zx++){
							var record = ds_pemakaianbrgdet.data.items[zx].data;
							zkdbrg = record.kdbrg;
							zqty = record.qty;
							zcatatan = record.catatan;
							arrpbrg[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
						}
						Ext.Ajax.request({
							url: BASE_URL + 'pemakaianbrg_controller/insorupd_pbrg',
							params: {
								nopakaibrg		:	RH.getCompValue('tf.nopakaibrg'),
								tglpakaibrg		:	RH.getCompValue('df.tglpakaibrg'),
								jampakaibrg		:	RH.getCompValue('tf.jampakaibrg'),
								idbagian		:	RH.getCompValue('tf.tamidbagian'),
								idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
								idstsetuju		:	RH.getCompValue('cb.stsetuju'),
								userid			:	USERID,
								keterangan		:	RH.getCompValue('ta.keterangan'),
								
								arrpbrg : Ext.encode(arrpbrg)
								
							},
							success: function(response){
									Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
									obj = Ext.util.JSON.decode(response.responseText);
									console.log(obj);
									Ext.getCmp("tf.nopakaibrg").setValue(obj.nopakaibrg);
									Ext.getCmp("tf.carinopbdet").setValue(obj.nopakaibrg);
									ds_pemakaianbrgdet.setBaseParam('nopakaibrg', Ext.getCmp("tf.carinopbdet").getValue(obj.nopakaibrg));
									ds_pemakaianbrg.reload();
									ds_pemakaianbrgdet.reload();
									Ext.getCmp('btn_tmbh').disable();
									Ext.getCmp('btn_simpan').disable();
									Ext.getCmp('bt.batal').enable();
									Ext.getCmp('bt.cetak').enable();
									var idstsetuju = Ext.getCmp('cb.stsetuju').getValue();
										if(idstsetuju != 1){
											Ext.getCmp('cb.stsetuju').setReadOnly(true);
										}
									var arruqty = [];
									for(var zx = 0; zx < ds_pemakaianbrgdet.data.items.length; zx++){
										var record = ds_pemakaianbrgdet.data.items[zx].data;
										ukdbrg = record.kdbrg;
										uqty = record.qty;
										arruqty[zx] = Ext.getCmp('tf.nopakaibrg').getValue(obj.nopakaibrg) + '-' + Ext.getCmp('tf.jampakaibrg').getValue() + '-' + Ext.getCmp('tf.tamidbagian').getValue() + '-' + USERID + '-' + ukdbrg + '-' + uqty;
									}
									Ext.Ajax.request({
										url: BASE_URL + 'pemakaianbrg_controller/update_qty_tambah',
										params: {
											tglpakaibrg		: RH.getCompValue('df.tglpakaibrg'),
											arruqty 		: Ext.encode(arruqty)
											
										},
										success: function(response){								
											ds_pemakaianbrgdet.reload();
											//Ext.getCmp('grid_daftar_pemakaianbrg').store.reload();
										},
										failure : function(){
											Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
										}
									});
									return;
								}
						});
					}else{
						var arrpbrg = [];
						for(var zx = 0; zx < ds_pemakaianbrgdet.data.items.length; zx++){
							var record = ds_pemakaianbrgdet.data.items[zx].data;
							zkdbrg = record.kdbrg;
							zqty = record.qty;
							zcatatan = record.catatan;
							arrpbrg[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
						}
						Ext.Ajax.request({
							url: BASE_URL + 'pemakaianbrg_controller/insorupd_pbrg',
							params: {
								nopakaibrg		:	RH.getCompValue('tf.nopakaibrg'),
								tglpakaibrg		:	RH.getCompValue('df.tglpakaibrg'),
								jampakaibrg		:	RH.getCompValue('tf.jampakaibrg'),
								idbagian		:	RH.getCompValue('tf.tamidbagian'),
								idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
								idstsetuju		:	RH.getCompValue('cb.stsetuju'),
								userid			:	USERID,
								keterangan		:	RH.getCompValue('ta.keterangan'),
								
								arrpbrg : Ext.encode(arrpbrg)
								
							},
							success: function(response){
								Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
								obj = Ext.util.JSON.decode(response.responseText);
								console.log(obj);
								Ext.getCmp("tf.nopakaibrg").setValue(obj.nopakaibrg);
								ds_pemakaianbrg.reload();
							},
							failure : function(){
								Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
							}
						});
					}
				}else{
					Ext.MessageBox.alert('Informasi','Isi data barang..');
				}
			}else{
				Ext.MessageBox.alert('Informasi','Isi data bagian terlebih dahulu..');
			}
		}
		
		function batalPbrg(){
			Ext.getCmp('cb.sttransaksi').setValue('2');
			var arrpbrg = [];
			for(var zx = 0; zx < ds_pemakaianbrgdet.data.items.length; zx++){
				var record = ds_pemakaianbrgdet.data.items[zx].data;
				zkdbrg = record.kdbrg;
				zqty = record.qty;
				zcatatan = record.catatan;
				arrpbrg[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'pemakaianbrg_controller/insorupd_pbrg',
				params: {
					nopakaibrg		:	RH.getCompValue('tf.nopakaibrg'),
					tglpakaibrg		:	RH.getCompValue('df.tglpakaibrg'),
					jampakaibrg		:	RH.getCompValue('tf.jampakaibrg'),
					idbagian		:	RH.getCompValue('tf.tamidbagian'),
					idsttransaksi	:	2, //RH.getCompValue('cb.sttransaksi'),
					idstsetuju		:	RH.getCompValue('cb.stsetuju'),
					userid			:	USERID,
					keterangan		:	RH.getCompValue('ta.keterangan'),
					
					arrpbrg : Ext.encode(arrpbrg)
					
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi','Pemakaian barang dibatalkan..');
					obj = Ext.util.JSON.decode(response.responseText);
					console.log(obj);
					Ext.getCmp("tf.nopakaibrg").setValue(obj.nopakaibrg);
					Ext.getCmp("tf.carinopbdet").setValue(obj.nopakaibrg);
					ds_pemakaianbrgdet.setBaseParam('nopakaibrg', Ext.getCmp("tf.carinopbdet").getValue(obj.nopakaibrg));
					Ext.getCmp('btn_tmbh').disable();
					Ext.getCmp('btn_simpan').disable();
					Ext.getCmp('bt.batal').disable();
					
					var arruqty = [];
					for(var zx = 0; zx < ds_pemakaianbrgdet.data.items.length; zx++){
						var record = ds_pemakaianbrgdet.data.items[zx].data;
						ukdbrg = record.kdbrg;
						uqty = record.qty;
						arruqty[zx] = Ext.getCmp('tf.nopakaibrg').getValue(obj.nopakaibrg) + '-' + Ext.getCmp('tf.jampakaibrg').getValue() + '-' + Ext.getCmp('tf.tamidbagian').getValue() + '-' + USERID + '-' + ukdbrg + '-' + uqty;
					}
					Ext.Ajax.request({
						url: BASE_URL + 'pemakaianbrg_controller/update_qty_batal',
						params: {							
							tglpakaibrg		: RH.getCompValue('df.tglpakaibrg'),
							arruqty			: Ext.encode(arruqty)
							
						},							
						failure : function(){
							Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
						}
					});
					ds_pemakaianbrg.reload();
					ds_pemakaianbrgdet.reload();
				}
			});
		}

		function cetak(){
			var nopakaibrg	= Ext.getCmp('tf.nopakaibrg').getValue();			
			RH.ShowReport(BASE_URL + 'print/print_pakaibrg/pakaibrg_pdf/' + nopakaibrg);
		}		
	}
	
	function fncariBarang(){
		var ds_brgmedisdipemakaianbrg = dm_brgmedisdipemakaianbrg();		
		ds_brgmedisdipemakaianbrg.setBaseParam('idbagian', Ext.getCmp('tf.tamidbagian').getValue());
		ds_brgmedisdipemakaianbrg.reload();
		
		function keyToAddbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_barang = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: 'Kode Barang',
			width: 73,
			dataIndex: 'kdbrg',
			sortable: true,
			renderer: keyToAddbrg
		},
		{
			header: 'Nama Barang',
			width: 352,
			dataIndex: 'nmbrg',
			sortable: true
		},
		{
			header: 'Jenis Barang',
			width: 100,
			dataIndex: 'nmjnsbrg',
			sortable: true,
			align:'center',
		},
		{
			header: 'Satuan',
			width: 97,
			dataIndex: 'nmsatuankcl',
			sortable: true,
			align:'center',
		},
		{
			header: 'Stok Sekarang',
			width: 94,
			dataIndex: 'stoknowbagian',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',				
		}
		]);
		
		var sm_barang = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_barang = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_barang = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_brgmedisdipemakaianbrg,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_barang = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_barang = new Ext.grid.GridPanel({
			ds: ds_brgmedisdipemakaianbrg,
			cm: cm_barang,
			sm: sm_barang,
			view: vw_barang,
			height: 460,
			width: 750,
			//plugins: cari_barang,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_brgmedisdipemakaianbrg.setBaseParam('key',  '1');
							ds_brgmedisdipemakaianbrg.setBaseParam('id',  'nmbrg');
							ds_brgmedisdipemakaianbrg.setBaseParam('name',  nmcombo);
						ds_brgmedisdipemakaianbrg.load();
					}
				}]
			}],
			bbar: paging_barang,
			listeners: {
				//rowdblclick: klik_cari_barang
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_barang = new Ext.Window({
			title: 'Cari Barang Bagian',
			modal: true,
			items: [grid_find_cari_barang]
		}).show();
				
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var cek = true;
					var obj = ds_brgmedisdipemakaianbrg.getAt(rowIndex);
					var skdbrg			= obj.get("kdbrg");
					var snmbrg    	 	= obj.get("nmbrg");
					var sjnsbrg    	 	= obj.get("nmjnsbrg");
					var sidsatuan		= obj.get("nmsatuankcl");
					var stoknowbagian	= obj.get("stoknowbagian");
					
					Ext.getCmp("tf.reckdbrg").setValue(skdbrg);
					Ext.getCmp('btn_simpan').enable();					
					Ext.getCmp('btn_data_pen_bgn').disable();
					
					ds_pemakaianbrgdet.each(function(rec){
						if(rec.get('kdbrg') == skdbrg) {
							Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
							cek = false;
						}
					});
					
					if(cek){
						var orgaListRecord = new Ext.data.Record.create([
							{
								name: 'kdbrg',
								name: 'nmbrg',
								name: 'nmjnsbrg',
								name: 'nmsatuan',
								name: 'stoknowbagian',
								name: 'qty',
								name: 'catatan'
							}
						]);
						
						ds_pemakaianbrgdet.add([
							new orgaListRecord({
								'kdbrg'			: skdbrg,
								'nmbrg'			: snmbrg,
								'nmjnsbrg'		: sjnsbrg,
								'nmsatuan'		: sidsatuan,
								'stoknowbagian'	: stoknowbagian,
								'qty'			: 1,
								'catatan'		: "-"
							})
						]);
						win_find_cari_barang.close();
					}
			}
			return true;
		}
	}
	
	function fnwBagian(){
		var ds_bagian = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'pemakaianbrg_controller/get_pengbagian',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "idbagian",
				mapping: "idbagian"
			},{
				name: "nmbagian",
				mapping: "nmbagian"
			},{
				name: "idjnspelayanan",
				mapping: "idjnspelayanan"
			},{
				name: "nmjnspelayanan",
				mapping: "nmjnspelayanan"
			},{
				name: "idbdgrawat",
				mapping: "idbdgrawat"
			},{
				name: "nmbdgrawat",
				mapping: "nmbdgrawat"
			},{
				name: "userid",
				mapping: "userid"
			},{
				name: "nmlengkap",
				mapping: "nmlengkap"
			}]
		});
		
		ds_bagian.setBaseParam('userid', USERID);
		
		function fnkeyAddBagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bagian = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200,
				renderer: fnkeyAddBagian
			},{
				header: 'Jenis Pelayanan',
				dataIndex: 'nmjnspelayanan',
				width: 150
			},{
				header: 'Bidang Perawatan',
				dataIndex: 'nmbdgrawat',
				width: 220
			}
		]);
		
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagian,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bagian = new Ext.grid.GridPanel({
			ds: ds_bagian,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 620,
			//plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_bagian.setBaseParam('key',  '1');
							ds_bagian.setBaseParam('id',  'nmbagian');
							ds_bagian.setBaseParam('name',  nmcombo);
						ds_bagian.load();
					}
				}]
			}],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Pengguna Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbagian = record.data["idbagian"];
					var var_cari_nmbagian = record.data["nmbagian"];
								
					//Ext.getCmp('tf.bagian').focus();
					//Ext.getCmp("tf.bagian").setValue(var_cari_bagian);
					Ext.getCmp("tf.bagian").setValue(var_cari_nmbagian);
					Ext.getCmp("tf.tamidbagian").setValue(var_cari_idbagian);
					Ext.getCmp('btn_add').enable();
					//Ext.getCmp('grid_brgbagian').store.reload();
								win_find_cari_bagian.close();
				return true;
			}
			return true;
		}
	}
}
