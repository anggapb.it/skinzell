function PLstokopname(){
	var pageSize = 18;
	var ds_ststokopname = dm_ststokopname();
	var ds_stokopname = dm_stokopname();
	var ds_stokopnamedet = dm_stokopnamedet();
		ds_stokopnamedet.setBaseParam('noso','null');
	var rowdata= '';
			
	var ds_pengbagian = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'returbrgsupplier_controller/get_pengunabagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_pengbagian.setBaseParam('userid', USERID);
	
	var ds_bagian = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'returbrgbagian_controller/get_pengbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "idjnspelayanan",
			mapping: "idjnspelayanan"
		},{
			name: "nmjnspelayanan",
			mapping: "nmjnspelayanan"
		},{
			name: "idbdgrawat",
			mapping: "idbdgrawat"
		},{
			name: "nmbdgrawat",
			mapping: "nmbdgrawat"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_bagian.setBaseParam('userid', USERID);
	
	var arr_cari = [['noso', 'No. Stock Opname'],['nmbagian', 'Bagian'],['nmstso', 'Status Stock Opname'],['nmlengkap', 'Penanggung Jawab']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	function fnSearchgrid(){
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.searchh').getValue();
		nmcombo= Ext.getCmp('cekk').getValue();
			ds_stokopname.setBaseParam('key',  '1');
			ds_stokopname.setBaseParam('id',  idcombo);
			ds_stokopname.setBaseParam('name',  nmcombo);
		ds_stokopname.load(); 
	}
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_stokopname,
		displayInfo: true,
		displayMsg: 'Data Stock Opname Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var vw_stokopname = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail kasir" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var grid_stokopname = new Ext.grid.GridPanel({
		id: 'grid_stokopname',
		store: ds_stokopname,
		view: vw_stokopname,
		autoScroll: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddStokopname();
				Ext.getCmp('tf.carinosodet').setValue();
				Ext.getCmp('df.tglso').setValue(new Date());
				Ext.getCmp('tf.reckdbrg').setValue();
				Ext.getCmp('btn_cetak').disable();
				Ext.getCmp('btn_cetakexcel').disable();
				Ext.getCmp('cb.ststokopname').setReadOnly(true);
				getId_nama();
			}
		},'-',{
			xtype: 'compositefield',
			width: 455,
			items: [{
				xtype: 'label', text: 'Search :', margins: '4 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.searchh',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				margins: '2 5 0 0',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.searchh').getValue();
							if(cbsearchh != ''){
								Ext.getCmp('cekk').enable();
								Ext.getCmp('cekk').focus();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'cekk',
				width: 200,
				margins: '2 5 0 0',
				disabled: true,
				validator: function(){
					fnSearchgrid();
				}
			}]
		},{
			xtype: 'textfield',
			id:'tf.userid',
			width: 60,
			value: USERID,
			hidden: true,
			validator: function(){
				ds_stokopname.setBaseParam('userid', Ext.getCmp('tf.userid').getValue());
				ds_stokopname.reload();
			}
		}],
		autoHeight: true,
		columnLines: true,
		//plugins: cari_data,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. <br> Stock Opname'),
			width: 90,
			dataIndex: 'noso',
			sortable: true,
			align:'center'
		},
		{
			header: headerGerid('Tgl <br> Stock Opname'),
			width: 90,
			dataIndex: 'tglso',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Jam'),
			width: 60,
			dataIndex: 'jamso',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Bagian'),
			width: 170,
			dataIndex: 'nmbagian',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Status <br> Stock Opname'),
			width: 150,
			dataIndex: 'nmstso',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Penanggung Jawab'),
			width: 140,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Keterangan'),
			width: 160,
			dataIndex: 'keterangan',
			sortable: true,
			align:'center',
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditStokopname(grid, rowIndex);
						var record = grid.getStore().getAt(rowIndex);
						Ext.getCmp('tf.cekstso').setValue(record.data['idstso']);
						var cekstso = Ext.getCmp('tf.cekstso').getValue();
						if(cekstso == 2){
							Ext.getCmp('cb.ststokopname').disable();
							Ext.getCmp('df.tglso').disable();
							Ext.getCmp('btn_simpan').disable();
						}					
						var nostokopname = RH.getCompValue('tf.noso', true);
						if(nostokopname != ''){
							RH.setCompValue('tf.carinosodet', nostokopname);
							Ext.getCmp('tf.reckdbrg').setValue(nostokopname);
						}
						Ext.getCmp('btn_data_pen_bgn').disable();
						return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteStokopname(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Cetak',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetakSo(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 70,
				header: 'Cetak Excel',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetakSoexcel(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging		
	});	
	       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Daftar Stock Opname', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_stokopname]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadStokopname(){
		ds_stokopname.reload();
	}
	
	function fnAddStokopname(){
		var grid = grid_stokopname;
		wEntryStokopname(false, grid, null);	
	}
	
	function fnEditStokopname(grid, record){
		var record = ds_stokopname.getAt(record);
		wEntryStokopname(true, grid, record);		
	}
	
	function fnDeleteStokopname(grid, record){
		var record = ds_stokopname.getAt(record);
		var cekstatus = record.data['idstso'];
		if(cekstatus!=1){
			Ext.MessageBox.alert('Informasi','Data sudah tidak bisa dihapus..');
		}else{
			var url = BASE_URL + 'stokopname_controller/delete_stokopname';
			var params = new Object({
							noso	: record.data['noso']
						});
			RH.deleteGridRecord(url, params, grid );
		}
		return;
	}
	
	function cetakSo(grid, record){
		var record = ds_stokopname.getAt(record);
		var noso = record.data['noso'] 
		RH.ShowReport(BASE_URL + 'print/print_stokopname/stokopname_pdf/' + noso);
	}
	function cetakSoexcel(grid, record){
		var record = ds_stokopname.getAt(record);
		var noso = record.data['noso'] 
		var idbagian = record.data['idbagian'] 
		window.location =(BASE_URL + 'print/print_stokopname/excelso/' + noso +'/'+idbagian);
	}
	
	function getId_nama(){
		ds_pengbagian.reload({
			scope   : this,
			callback: function(records, operation, success) {
				var nmlkp = '';

				 ds_pengbagian.each(function (rec) { 
						nmlkp = rec.get('nmlengkap');
					});                          
				Ext.getCmp("tf.userinput").setValue(nmlkp);
				var cekuser = Ext.getCmp('tf.userinput').getValue();
				if(cekuser != ""){
					Ext.getCmp('btn_simpan').enable();
				}
				else{
					Ext.getCmp('btn_simpan').disable();
					Ext.getCmp('btn_cetak').disable();
					Ext.getCmp('btn_cetakexcel').disable();
					Ext.getCmp('btn_data_pen_bgn').disable();
				}
				return;
			}
		});
		
		Ext.Ajax.request({
			url:BASE_URL + 'returbrgsupplier_controller/getNmbagian',
			params:{
				idbagian : Ext.getCmp('tf.bagian').getValue()
			},
			method:'POST',
			success: function(response){
				var r = Ext.decode(response.responseText);
				RH.setCompValue('tf.bagian', r);
			}
		});
	}
	
	/**
WIN - FORM ENTRY/EDIT 
*/

	function wEntryStokopname(isUpdate, grid, record){	
		// var ds_stokopnamedet = dm_stokopnamedet();
		// ds_stokopnamedet.removeAll();

		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.jamso"))
					RH.setCompValue("tf.jamso",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
		
		var vw_daftar_stokopname = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		/*var arr_cari2 = [['kdbrg', 'Kode Barang'],['nmbrg', 'Nama Barang'],['nmjnsbrg', 'Jenis Barang'],['jmlkomputer', 'Stok Komputer'],['jmlfisik', 'Stok Fisik']];
	
		var ds_cari2 = new Ext.data.ArrayStore({
			fields: ['id', 'nama'],
			data : arr_cari2
		});*/
		
		/*function fnSearchgrid2(){
			var idcombo, nmcombo;
			idcombo= Ext.getCmp('cb.search2').getValue();
			nmcombo= Ext.getCmp('cek2').getValue();
				ds_stokopnamedet.setBaseParam('key',  '1');
				ds_stokopnamedet.setBaseParam('id',  idcombo);
				ds_stokopnamedet.setBaseParam('name',  nmcombo);
			ds_stokopnamedet.load(); 
		}*/

		var cari_data = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
				
		var grid_daftar_stokopname = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_stokopname',
			store: ds_stokopnamedet,
			view: vw_daftar_stokopname,
			plugins: cari_data,
			tbar: [{
				xtype: 'compositefield',
				width: 515,
				items: [{
					xtype: 'textfield',
					id:'tf.carinosodet',
					width: 60,
					hidden: true,
					validator: function(){
						ds_stokopnamedet.setBaseParam('noso', Ext.getCmp('tf.carinosodet').getValue());
						ds_stokopnamedet.reload();
					}
				},{
					xtype: 'textfield',
					id:'tf.reckdbrg',
					width: 60,
					hidden: true,							
				},{
					xtype: 'textfield',
					id:'tf.updatekdbrg',
					width: 60,
					hidden: true,							
				},{
					xtype: 'textfield',
					id:'tf.cekstso',
					width: 60,
					hidden: true,							
				}]
			},'->'],
			autoScroll: true,
			height: 333,
			columnLines: true,
			frame: true,
			clicksToEdit: 1,
			listeners: {
				rowclick : function(grid, rowIndex, columnIndex){
					var record = grid.getStore().getAt(rowIndex);
					rowdata = rowIndex;
					Ext.getCmp('tf.updatekdbrg').setValue(record.data['kdbrg']);
				}
			},
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('Kode <br> Barang'),
				width: 87,
				dataIndex: 'kdbrg',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Nama Barang'),
				width: 200,
				dataIndex: 'nmbrg',
				sortable: true,
			},
			{
				header: headerGerid('Jenis Barang'),
				width: 87,
				dataIndex: 'nmjnsbrg',
				sortable: true,
				align:'center',
			},{
				header: headerGerid('Satuan <br> Kecil'),
				width: 87,
				dataIndex: 'nmsatuankcl',
				sortable: true,
				align:'center',
			},{
				header: headerGerid('Stok <br> Komputer'),
				width: 70,
				dataIndex: 'jmlkomputer',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',				
			},{
				header: headerGerid('Stok <br> Fisik'),
				width: 60,
				dataIndex: 'jmlfisik',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				editor: {
					xtype: 'textfield',
					id: 'tfgp.stokfisik', 
					enableKeyEvents: true,
					listeners: {
						specialkey: function(field, e){
							if (e.getKey() == e.ENTER) {
								var record = ds_stokopnamedet.getAt(rowdata);
								var cekk = Ext.getCmp('tf.cekstso').getValue();
								if(cekk == 2){
									Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di Edit..');
									Ext.getCmp('grid_daftar_stokopname').store.reload();
								}else{
									//fnEditStokfisik();
									//var selisih = Ext.getCmp('tfgp.stokfisik').getValue() - record.data.jmlkomputer;
									//record.set('selisih',selisih);
								}																
								var cekstokfisik = Ext.getCmp('tfgp.stokfisik').getValue();
								if(cekstokfisik !='0'){									
									//fnEditStokfisik();
									//var selisih = Ext.getCmp('tfgp.stokfisik').getValue() - record.data.jmlkomputer;
									//record.set('selisih',selisih);
								}else{
									Ext.MessageBox.alert('Informasi','Jml Fisik tidak boleh "0"');
									Ext.getCmp('tfgp.stokfisik').setValue();
								}
								return;
							}
						}
					}
				}				
			},{
				header: headerGerid('Selisih'),
				width: 70,
				dataIndex: 'selisih',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
			},
			{
				header: headerGerid('Catatan'),
				width: 130,
				dataIndex: 'catatan',
				sortable: true,
				align:'center',
				editor: {
					xtype: 'textfield',
					id: 'tfgp.catatan',
					enableKeyEvents: true,
					listeners: {
						specialkey: function(field, e){
							if (e.getKey() == e.ENTER) {
								var cekk = Ext.getCmp('tf.cekstso').getValue();
								if(cekk == 2){
									Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di Edit..');
									Ext.getCmp('grid_daftar_stokopname').store.reload();
								}else{
									//fnEditCatatan();
								}
								return;
							}
						}
					}
				}
			},{
				xtype: 'actioncolumn',
				width: 50,
				header: 'Hapus',
				align:'center',
				items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
						if(Ext.getCmp('cb.ststokopname').getValue() == 2){
							meta.attr = "style='pointer-events: none;'";
						}else{
						  	meta.css = 'x-grid-icon';
						}
					},
					icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
					handler: function(grid, rowIndex) {
						// fnHapusSodet(grid, rowIndex);
						ds_stokopnamedet.removeAt(rowIndex);
					}
				}]
			}]
		});
		
		var winTitle = (isUpdate)?'Stock Opname (Edit)':'Stock Opname (Entry)';		
		var stokopname_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.stokopname',
			buttonAlign: 'left',
			labelWidth: 140, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 555, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan Edit', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					var cekidstatus = Ext.getCmp('cb.ststokopname').getValue();
					if(cekidstatus !=1){
						Ext.Msg.show({
							title: 'Konfirmasi',
							msg: 'Apakah Stock Opname Selesai..?',
							buttons: Ext.Msg.YESNO,
							icon: Ext.MessageBox.QUESTION,
							fn: function (response) {
								if ('yes' != response) {	
									Ext.getCmp('cb.ststokopname').setValue('1');
									// fnSaveStokopname();
									fnSimpan();
									return;
								}else{
									var arruqty = [];
									var arrso = [];
									for(var zx = 0; zx < ds_stokopnamedet.data.items.length; zx++){
										var record = ds_stokopnamedet.data.items[zx].data;
										zkdbrg 		 = record.kdbrg;
										zjmlkomputer = record.jmlkomputer;
										zjmlfisik 	 = record.jmlfisik;
										zhrg_beli 	 = record.hrg_beli;
										 zhrg_jual 	 = record.hrg_jual;
										 ztglexp 	 = Ext.util.Format.date(record.tgl_exp, 'Y_m_d');
										 zcatatan	 = record.catatan;
										arruqty[zx] = zkdbrg + '-' + zjmlfisik + '-' + zhrg_beli + '-' + zhrg_jual + '-' + ztglexp;
										arrso[zx] = zkdbrg + '-' + zjmlkomputer + '-' + zjmlfisik + '-' + zhrg_beli + '-' + zhrg_jual + '-' + ztglexp + '-' + zcatatan;
									}

								
										Ext.Ajax.request({
										url: BASE_URL + 'stokopname_controller/update_qty_so',
										params: {
											noso 		: 	Ext.getCmp('tf.noso').getValue(),
											tglso		:	RH.getCompValue('df.tglso'),
											idbagian	:	RH.getCompValue('tf.idbagian'),
											arruqty 	: 	Ext.encode(arruqty),											
											arrso 	: 	Ext.encode(arrso),											
										},
										success: function(response) {
											Ext.getCmp('cb.ststokopname').disable();
											Ext.getCmp('df.tglso').disable();
											Ext.getCmp('btn_simpan').disable();
											fnSimpan();
										},
										failure : function(){
											Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
										}
									});
								}
							}
						});
					}else{
						fnSimpan();
					}
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', id: 'btn_cetak', style: 'marginLeft: 5px',
				handler: function() {
					cetak();
				}
			},{
				text: 'Cetak Excel', iconCls:'silk-printer', id: 'btn_cetakexcel', style: 'marginLeft: 5px',
				handler: function() {
					cetakexcel();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					Ext.getCmp('tf.carinosodet').setValue();
					ds_stokopname.reload();
					wStokopname.close();
			}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 130, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.49, border: false, layout: 'form',					
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. SO',
						id:'tf.noso',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tgl/Jam SO',
						items:[{
							xtype: 'datefield',
							id: 'df.tglso',
							format: 'd-m-Y',
							//value: new Date(),
							width: 120,
						},{
							xtype: 'label', id: 'lb.time', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.jamso',
							width: 60, 
							readOnly: true,
						}]
					},{
						xtype: 'compositefield',
						style: 'marginLeft: -48px;',
						width: 300,
						items: [{
							xtype: 'label', id: 'lb.bgn', text: 'Bagian:', margins: '3 5 0 4',
						},{
							xtype: 'textfield',
							id: 'tf.bagian',
							emptyText:'Nama Bagian..',
							width: 205,
							readOnly: true,
							style: 'opacity: 0.6',
						},
						{
							xtype: 'button',
							iconCls: 'silk-find',
							id: 'btn_data_pen_bgn',
							width: 3,
							handler: function() {
								fnwBagian();
							}
						},{
							xtype: 'textfield',
							id: 'tf.idbagian',
							width: 20,
							hidden: true,							
						}]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Status',
						items:[{
							xtype: 'combo',
							id: 'cb.ststokopname',
							store: ds_ststokopname, 
							valueField: 'idstso', displayField: 'nmstso',
							triggerAction: 'all', forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
							width: 205, editable: false, value: 1,
						}]
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					labelWidth: 150, labelAlign: 'right',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'Penanggung Jawab',
						id: 'tf.userinput',
						width: 250,
						readOnly: true,
						value: USERNAME,
						style : 'opacity:0.6',
					},{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						id  : 'ta.keterangan',
						width : 250,
						height: 70,
					}]
				}]
			},{
				xtype: 'fieldset',
				title: 'Daftar Barang Bagian :',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 373,
				items: [grid_daftar_stokopname]
			}]
		});
			
		var wStokopname = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [stokopname_form]
		});
		
		function fnEditStokfisik(){
			var stokfisik = Ext.getCmp('tfgp.stokfisik').getValue();
			var letters = /^[a-zA-Z]+$/;
			var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
			if(stokfisik.match(letters)){
				Ext.Msg.alert("Info", "Masukan Angka");
				ds_stokopnamedet.reload();
			} 
			else if(stokfisik.match(simbol)){
				Ext.Msg.alert("Info", "Masukan Angka");
				ds_stokopnamedet.reload();
			} 
			else {           
				fnSfisik(stokfisik);   
			}      
		}
		
		function fnSfisik(stokfisik){
			Ext.Ajax.request({
				url: BASE_URL + 'stokopname_controller/update_jmlfisik',
				params: {
					noso		: Ext.getCmp('tf.carinosodet').getValue(),
					kdbrg		: Ext.getCmp('tf.updatekdbrg').getValue(),
					jmlfisik	: stokfisik
				},
				success: function() {
					//Ext.Msg.alert("Info", "Ubah Berhasil");
					Ext.getCmp('grid_daftar_stokopname').store.reload();
				},
				failure: function() {
					Ext.Msg.alert("Info", "Ubah Data Gagal");
				}
			});
		}
		
		function fnEditCatatan(){
			var catatan = Ext.getCmp('tfgp.catatan').getValue();
			Ext.Ajax.request({
				url: BASE_URL + 'stokopname_controller/update_catatan',
				params: {
					noso		: Ext.getCmp('tf.carinosodet').getValue(),
					kdbrg		: Ext.getCmp('tf.updatekdbrg').getValue(),
					catatan    	: catatan
				},
				success: function() {
					//Ext.Msg.alert("Info", "Ubah Berhasil");
					Ext.getCmp('grid_daftar_stokopname').store.reload();
				},
				failure: function() {
					Ext.Msg.alert("Info", "Ubah Data Gagal");
				}
			});
		}
		
		function cetak(){
			var noso = Ext.getCmp('tf.noso').getValue();			
			RH.ShowReport(BASE_URL + 'print/print_stokopname/stokopname_pdf/' + noso);
		}
		
		function cetakexcel(){		
			var noso = Ext.getCmp('tf.noso').getValue();
			var idbagian = Ext.getCmp('tf.idbagian').getValue();
		window.location =(BASE_URL + 'print/print_stokopname/excelso/' + noso +'/'+idbagian);
		}
		
	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setStokopnameForm(isUpdate, record);
		wStokopname.show();

	/**
	FORM FUNCTIONS
	*/	
		function setStokopnameForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'returbrgbagian_controller/getNmbagian',
						params:{
							idbagian : record.get('idbagian')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.bagian', r);
						}
					});
					
					RH.setCompValue('tf.noso', record.get('noso'));
					RH.setCompValue('df.tglso', record.get('tglso'));
					RH.setCompValue('tf.bagian', record.get('idbagian'));
					RH.setCompValue('tf.idbagian', record.get('idbagian'));
					RH.setCompValue('cb.ststokopname', record.get('idstso'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					return;
				}
			}
		}
		
		function fnSimpan(){
			var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
			var arrso = [];
			for(var zx = 0; zx < ds_stokopnamedet.data.items.length; zx++){
				var record = ds_stokopnamedet.data.items[zx].data;
				zkdbrg 		 = record.kdbrg;
				zjmlkomputer = record.jmlkomputer;
				zjmlfisik 	 = record.jmlfisik;
				zhrg_beli 	 = record.hrg_beli;
				zhrg_jual 	 = record.hrg_jual;
				ztglexp 	 = Ext.util.Format.date(record.tgl_exp, 'Y_m_d');				
				zcatatan 	 = record.catatan;				
				arrso[zx] = zkdbrg + '-' + zjmlkomputer + '-' + zjmlfisik + '-' + zhrg_beli + '-' + zhrg_jual + '-' + ztglexp + '-' + zcatatan;
			}
				Ext.Ajax.request({
				url: BASE_URL + 'stokopname_controller/insorupd_data',
				params: {
					noso 			: 	Ext.getCmp('tf.noso').getValue(),
					tglso			:	RH.getCompValue('df.tglso'),
					jamso			:	RH.getCompValue('tf.jamso'),
					idstso			:	RH.getCompValue('cb.ststokopname'),
					idbagian		:	RH.getCompValue('tf.idbagian'),
					keterangan		:	RH.getCompValue('ta.keterangan'),
					arrso 			: 	Ext.encode(arrso)
					
				},
				success: function(response){
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
					obj = Ext.util.JSON.decode(response.responseText);
					console.log(obj);
					Ext.getCmp('tf.noso').setValue(obj.noso);
					Ext.getCmp('tf.carinosodet').setValue(obj.noso);
					Ext.getCmp('btn_cetak').enable();
					Ext.getCmp('btn_cetakexcel').enable();
					ds_stokopname.reload();
					var cbstso = Ext.getCmp('cb.ststokopname').getValue();
					Ext.getCmp('tf.cekstso').setValue(cbstso);
					Ext.getCmp('cb.ststokopname').setReadOnly(false);
					Ext.getCmp('btn_data_pen_bgn').disable();
				},
				failure : function(){
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
				}
			});

		}
	}
		
	function fnwBagian(){		
		var ds_brgbagian =	dm_brgbagian1();
		var ds_bagian = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'returbrgbagian_controller/get_pengbagian',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "idbagian",
				mapping: "idbagian"
			},{
				name: "nmbagian",
				mapping: "nmbagian"
			},{
				name: "idjnspelayanan",
				mapping: "idjnspelayanan"
			},{
				name: "nmjnspelayanan",
				mapping: "nmjnspelayanan"
			},{
				name: "idbdgrawat",
				mapping: "idbdgrawat"
			},{
				name: "nmbdgrawat",
				mapping: "nmbdgrawat"
			},{
				name: "userid",
				mapping: "userid"
			},{
				name: "nmlengkap",
				mapping: "nmlengkap"
			}]
		});
		
		ds_bagian.setBaseParam('userid', USERID);
		
		function fnkeyAddBagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bagian = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200,
				renderer: fnkeyAddBagian
			},{
				header: 'Jenis Pelayanan',
				dataIndex: 'nmjnspelayanan',
				width: 150
			},{
				header: 'Bidang Perawatan',
				dataIndex: 'nmbdgrawat',
				width: 220
			}
		]);
		
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagian,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bagian = new Ext.grid.GridPanel({
			ds: ds_bagian,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 620,
			//plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_bagian.setBaseParam('key',  '1');
							ds_bagian.setBaseParam('id',  'nmbagian');
							ds_bagian.setBaseParam('name',  nmcombo);
						ds_bagian.load();
					}
				}]
			}],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Pengguna Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbagian = record.data["idbagian"];
					var var_cari_nmbagian = record.data["nmbagian"];
								
					Ext.getCmp("tf.bagian").setValue(var_cari_nmbagian);
					Ext.getCmp("tf.idbagian").setValue(var_cari_idbagian);
					Ext.getCmp('btn_add').enable();
					ds_stokopnamedet.removeAll();

					ds_brgbagian.setBaseParam('idbagian', var_cari_idbagian);
					ds_brgbagian.reload({
						scope   : this,
						callback: function(records, operation, success) {
							ds_brgbagian.each(function (rec) {
								var skdbrg			= rec.get("kdbrg");
								var snmbrg			= rec.get("nmbrg");
								var snmjnsbrg		= rec.get("nmjnsbrg");
								var snmsatuankcl	= rec.get("nmsatuankcl");
								var sstoknowbagian	= rec.get("stoknowbagian");
								var sjmlfisik		= rec.get("stoknowbagian");
								var shrg_beli		= rec.get("hrg_beli");
								var shrg_jual		= rec.get("hrg_jual");
								var stgl_exp		= rec.get("tgl_exp");

								var orgaListRecord = new Ext.data.Record.create([
									{
										name: 'kdbrg',
										name: 'nmbrg',
										name: 'nmjnsbrg',
										name: 'nmsatuankcl',
										name: 'jmlkomputer',
										name: 'jmlfisik',
										name: 'selisih',
										name: 'hrg_beli',
										name: 'hrg_jual',
										name: 'tgl_exp',
										name: 'catatan'
									}
								]);
								
								ds_stokopnamedet.add([
									new orgaListRecord({
										'kdbrg'			: skdbrg,
										'nmbrg'			: snmbrg,
										'nmjnsbrg'		: snmjnsbrg,
										'nmsatuankcl'	: snmsatuankcl,
										'jmlkomputer'	: sstoknowbagian,
										'jmlfisik'		: sjmlfisik,
										'selisih'		: 0,
										'hrg_beli'		: shrg_beli,
										'hrg_jual'		: shrg_jual,
										'tgl_exp'		: stgl_exp,
										'catatan'		: '-'
									})
								]);
							});
						}
					});
					
					win_find_cari_bagian.close();
			}
			return true;
		}
	}
}
