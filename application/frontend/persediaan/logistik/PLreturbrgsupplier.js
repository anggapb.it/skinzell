function PLreturbrgsupplier(){
	var pageSize = 18;
	//var ds_bagian = dm_bagian();
	var ds_stsetuju = dm_stsetuju();
	var ds_sttransaksi = dm_sttransaksi();
	var ds_returbrgsupplier = dm_returbrgsupplier();
	var ds_returbrgsupplierdet = dm_returbrgsupplierdet();
		ds_returbrgsupplierdet.setBaseParam('noretursupplier','null');
	var row = '';
	var ds_stbayar = dm_stbayar();
	
	var ds_pengbagian = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'returbrgsupplier_controller/get_pengunabagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_pengbagian.setBaseParam('userid', USERID);
	
	var ds_bagian = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'returbrgsupplier_controller/get_pengbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "idjnspelayanan",
			mapping: "idjnspelayanan"
		},{
			name: "nmjnspelayanan",
			mapping: "nmjnspelayanan"
		},{
			name: "idbdgrawat",
			mapping: "idbdgrawat"
		},{
			name: "nmbdgrawat",
			mapping: "nmbdgrawat"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_bagian.setBaseParam('userid', USERID);
	
	var arr_cari = [['noretursupplier', 'No. Retur Supplier'],['nmlengkap', 'User Input'],['nmbagian', 'Dari Bagian'],['nmsupplier', 'Retur Ke Supplier'],['nmstsetuju', 'Status Persetujuan'],['nmsttransaksi', 'Status Transaksi']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});			
	
	function render_stbayar(value) {
		var val = RH.getRecordFieldValue(ds_stbayar, 'nmstbayar', 'idstbayar', value);
		return val;
	}
	
	function fnSearchgrid(){
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
			ds_returbrgsupplier.setBaseParam('key',  '1');
			ds_returbrgsupplier.setBaseParam('id',  idcombo);
			ds_returbrgsupplier.setBaseParam('name',  nmcombo);
		ds_returbrgsupplier.load(); 
	}
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_returbrgsupplier,
		displayInfo: true,
		displayMsg: 'Data Retur Barang Supplier Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var vw_returbrgsupplier = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail kasir" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var grid_returbrgsupplier = new Ext.grid.GridPanel({
		id: 'grid_returbrgsupplier',
		store: ds_returbrgsupplier,
		view: vw_returbrgsupplier,
		autoScroll: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddReturbrgsupplier();
				Ext.getCmp('tf.carinoreturdet').setValue();
				Ext.getCmp('tf.reckdbrg').setValue();
				Ext.getCmp('bt.batal').disable();
				Ext.getCmp('btn_cetak').disable();
				getId_nama();				
			}
		},'-',{
			xtype: 'compositefield',
			width: 455,
			items: [{
				xtype: 'label', text: 'Search :', margins: '4 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.search',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				margins: '2 5 0 0',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.search').getValue();
							if(cbsearchh != ''){
								Ext.getCmp('cek').enable();
								Ext.getCmp('cek').focus();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'cek',
				width: 200,
				margins: '2 5 0 0',
				disabled: true,
				validator: function(){
					fnSearchgrid();
				}
			}]
		},{
			xtype: 'textfield',
			id:'tf.userid',
			width: 60,
			value: USERID,
			hidden: true,
			validator: function(){
				ds_returbrgsupplier.setBaseParam('userid', Ext.getCmp('tf.userid').getValue());
				ds_returbrgsupplier.reload();
			}
		}],
		autoHeight: true,
		//height: 530,
		columnLines: true,
		//plugins: cari_data,
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. <br> Retur Supplier'),
			width: 90,
			dataIndex: 'noretursupplier',
			sortable: true,
			align:'center'
		},
		{
			header: headerGerid('Tgl <br> Retur Supplier'),
			width: 90,
			dataIndex: 'tglretursupplier',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Jam'),
			width: 60,
			dataIndex: 'jamretursupplier',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('User Input'),
			width: 134,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Dari Bagian'),
			width: 120,
			dataIndex: 'nmbagian',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Retur Ke Supplier'),
			width: 147,
			dataIndex: 'nmsupplier',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Status <br> Persetujuan'),
			width: 86,
			dataIndex: 'nmstsetuju',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Status <br> Transaksi'),
			width: 76,
			dataIndex: 'nmsttransaksi',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Keterangan'),
			width: 150,
			dataIndex: 'keterangan',
			sortable: true,
			align:'center',
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditReturbrgsupplier(grid, rowIndex);
						var noretursupplier = RH.getCompValue('tf.noretursupplier', true);
						if(noretursupplier != ''){
							RH.setCompValue('tf.carinoreturdet', noretursupplier);
							Ext.getCmp('tf.reckdbrg').setValue(noretursupplier);
						}
						var setuju = RH.getCompValue('cb.stsetuju', true);
						if(setuju != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('cb.stsetuju').setReadOnly(true);
						}else{							
							Ext.getCmp('bt.batal').disable();
						}
						var sttransaksi = RH.getCompValue('cb.sttransaksi', true);
						if(sttransaksi != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('bt.batal').disable();
						}
						Ext.getCmp('btn.supplier').disable();
						Ext.getCmp('idstbayar').setReadOnly(true);
						return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteReturbrgsupplier(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Cetak',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetakRetsupplier(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging		
	});	
	       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Daftar Retur Barang Supplier', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_returbrgsupplier]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadreturbrgsupplier(){
		ds_returbrgsupplier.reload();
	}
	
	function fnAddReturbrgsupplier(){
		var grid = grid_returbrgsupplier;
		wEntryReturbrgsupplier(false, grid, null);	
	}
	
	function fnEditReturbrgsupplier(grid, record){
		var record = ds_returbrgsupplier.getAt(record);
		wEntryReturbrgsupplier(true, grid, record);		
	}
	
	function fnDeleteReturbrgsupplier(grid, record){
		var record = ds_returbrgsupplier.getAt(record);
		var cekstatus = record.data['idstsetuju'];
		if(cekstatus!=1){
			Ext.MessageBox.alert('Informasi','Data sudah tidak bisa dihapus..');
		}else{
			var url = BASE_URL + 'returbrgsupplier_controller/delete_returbrgsupplier';
			var params = new Object({
							noretursupplier	: record.data['noretursupplier']
						});
			RH.deleteGridRecord(url, params, grid );
		}
		return;
	}
	
	function cetakRetsupplier(grid, record){
		var record = ds_returbrgsupplier.getAt(record);
		var noretursupplier = record.data['noretursupplier'] 
		RH.ShowReport(BASE_URL + 'print/print_retursupplier/retursupplier_pdf/' + noretursupplier);
	}
	
	function getId_nama(){		
		ds_pengbagian.reload({
			scope   : this,
			callback: function(records, operation, success) {
				var nmlkp = '';

				 ds_pengbagian.each(function (rec) { 
						nmlkp = rec.get('nmlengkap');
					});                          
				Ext.getCmp("tf.userinput").setValue(nmlkp);
				var cekuser = Ext.getCmp('tf.userinput').getValue();
				if(cekuser != ""){
					Ext.getCmp('btn_simpan').enable();
				}
				else{
					//Ext.MessageBox.alert('Informasi','Login ');
					Ext.getCmp('btn_tmbh').disable();
					Ext.getCmp('btn_simpan').disable();
					Ext.getCmp('btn_cetak').disable();
					Ext.getCmp('bt.batal').disable();
				}
				return;
			}
		});	
		
		Ext.getCmp('tf.idbagian').setValue('11');
		Ext.Ajax.request({
			url:BASE_URL + 'returbrgsupplier_controller/getNmbagian',
			params:{
				idbagian : Ext.getCmp('tf.idbagian').getValue()
			},
			method:'POST',
			success: function(response){
				var r = Ext.decode(response.responseText);
				RH.setCompValue('tf.idbagian', r);
			}
		});
	}
	
	/**
WIN - FORM ENTRY/EDIT 
*/

	function wEntryReturbrgsupplier(isUpdate, grid, record){		
		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.jamretursupplier"))
					RH.setCompValue("tf.jamretursupplier",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
				
		var paging_daftar_returbrgsupplier = new Ext.PagingToolbar({
			pageSize: pageSize,
			store: ds_returbrgsupplierdet,
			displayInfo: true,
			displayMsg: 'Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var vw_daftar_returbrgsupplier = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_daftar_returbrgsupplier = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_returbrgsupplier',
			store: ds_returbrgsupplierdet,
			view: vw_daftar_returbrgsupplier,
			tbar: [{
				text: 'Tambah',
				id: 'btn_tmbh',
				iconCls: 'silk-add',
				handler: function() {
					var cekkdsupplier = Ext.getCmp('tf.kdsupplier').getValue();
					if(cekkdsupplier != ""){
						fncariNopo();
						//Ext.getCmp('btn_data_pen_bgn').disable();
					}else{
						Ext.MessageBox.alert('Informasi','Supplier belum dipilih');
					}
				}
			},{
				xtype: 'compositefield',
				width: 515,
				items: [{
					xtype: 'label', text: 'Status Transaksi :', margins: '3 5 0 235px',
				},{
					xtype: 'combo',
					id: 'cb.sttransaksi',
					store: ds_sttransaksi, 
					valueField: 'idsttransaksi', displayField: 'nmsttransaksi',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 165, editable: false, value: 1,
					readOnly: true, style: 'opacity: 0.6'
				}
				/* {
					xtype: 'textfield',
					id: 'tf.sttransaksi',
					width: 165,
					//value: 1,
					readOnly: true,
					style: 'opacity: 0.6',
				} */]
			},{
				xtype: 'compositefield',
				width: 240,
				items: [{
					xtype: 'label', text: 'Status Persetujuan :', margins: '3 5 0 0',
				},{
					xtype: 'combo',
					id: 'cb.stsetuju',
					store: ds_stsetuju, 
					valueField: 'idstsetuju', displayField: 'nmstsetuju',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 140, allowBlank: false, editable: false, value: 2
				}]
			},{
				xtype: 'textfield',
				id:'tf.carinoreturdet',
				width: 60,
				hidden: true,
				validator: function(){
					ds_returbrgsupplierdet.setBaseParam('noretursupplier', Ext.getCmp('tf.carinoreturdet').getValue());
					ds_returbrgsupplierdet.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.reckdbrg',
				width: 60,
				hidden: true,							
			}],
			autoScroll: true,
			height: 333, //autoHeight: true,
			columnLines: true,
			frame: true,
			clicksToEdit: 1,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('No. <br> Pembelian'),
				width: 85,
				dataIndex: 'nopo',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Tgl <br> Pembelian'),
				width: 85,
				dataIndex: 'tglpo',
				sortable: true,
				align:'center',
				renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			},{
				header: headerGerid('Kode Barang'),
				width: 85,
				dataIndex: 'kdbrg',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Nama Barang'),
				width: 182,
				dataIndex: 'nmbrg',
				sortable: true,
			},
			{
				header: headerGerid('Satuan<br>(Besar)'),
				width: 90,
				dataIndex: 'nmsatuanbsr',
				sortable: true,
				align: 'center'
			},
			/* {
				header: headerGerid(' ID Jenis Barang'),
				width: 100,
				dataIndex: 'idjnsbrg',
				sortable: true,
				hidden: true,
			},
			{
				header: headerGerid('Jenis Barang'),
				width: 97,
				dataIndex: 'nmjnsbrg',
				sortable: true,
				align:'center',
			},{
				header: headerGerid('id Satuan'),
				width: 100,
				dataIndex: 'idsatuankcl',
				sortable: true,
				hidden: true
			},
			{
				header: headerGerid('Satuan'),
				width: 97,
				dataIndex: 'nmsatuan',
				sortable: true,
				align:'center',
			}, */
			{
				header: headerGerid('Qty <br> Terima'),
				width: 55,
				dataIndex: 'qtyterima',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',				
			},{
				header: headerGerid('qtysisa'),
				width: 55,
				dataIndex: 'qtysisa',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			},{
				header: headerGerid('Jml <br> Retur'),
				width: 55,
				dataIndex: 'jmlretur',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',				
			},{
				header: headerGerid('Qty <br> Sisa tam'),
				width: 55,
				dataIndex: 'tamqtysisa',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true				
			},{
				header: headerGerid('stoknowbagian'),
				width: 55,
				dataIndex: 'stoknowbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			},{
				header: headerGerid('Qty <br> Retur'),
				width: 55,
				dataIndex: 'qtyretur',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				editor: {
					xtype: 'numberfield',
					id: 'tfgp.qty',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){							
							var record = ds_returbrgsupplierdet.getAt(row);
							var cekqty = Ext.getCmp('tfgp.qty').getValue();
							if(cekqty !='0'){									
								//jumlah();
								var tmpqtyxrasio = record.data.rasio * Ext.getCmp('tfgp.qty').getValue();
								record.set('tmpqtyxrasio',tmpqtyxrasio);
								//alert(tmpqtyxrasio);
							}else{								
								Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh "0"');
								Ext.getCmp('tfgp.qty').setValue('1');
								//jumlah();
							}
							
							var stoknowbagian = record.data.stoknowbagian;
							var qtyterima = record.data.qtyterima;
							var tamqtysisa = record.data.tamqtysisa;
							var qtyretur = Ext.getCmp('tfgp.qty').getValue();						
							
							if(qtyretur == stoknowbagian){}
							else if(qtyretur >= stoknowbagian){
								 Ext.MessageBox.alert('Informasi', 'Jumlah retur melebihi Stok..');
								 Ext.getCmp('tfgp.qty').setValue();
								 record.set('tmpqtyxrasio','');
								 
								//var tmpqtyxrasio = record.data.qtyretur * record.data.rasio;
								//record.set('tmpqtyxrasio',tmpqtyxrasio);
								// return;
							}
							var cekstsetuju = Ext.getCmp('cb.stsetuju').getValue();
							if(cekstsetuju != 1){
								if(tamqtysisa == qtyretur){}
								else if(qtyretur >= qtyterima){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Terima..');
									Ext.getCmp('tfgp.qty').setValue();
									record.set('tmpqtyxrasio','');
								}else if(tamqtysisa <= qtyretur){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Sisa..');
									Ext.getCmp('tfgp.qty').setValue();
									record.set('tmpqtyxrasio','');
									
									//var tmpqtyxrasio = record.data.qtyretur * record.data.rasio;
									//record.set('tmpqtyxrasio',tmpqtyxrasio);
								}
								//return;
							}else{
								if(tamqtysisa == qtyretur){}
								else if(qtyretur >= qtyterima){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Terima..');
									Ext.getCmp('tfgp.qty').setValue();
									record.set('tmpqtyxrasio','');									
									//var tmpqtyxrasio = record.data.qtyretur * record.data.rasio;
									//record.set('tmpqtyxrasio',tmpqtyxrasio);
								}
								//return;
							}
							
							//var tmpqtyxrasio = record.data.qtyretur * record.data.rasio;
							//record.set('tmpqtyxrasio',tmpqtyxrasio);							
								//alert(tmpqtyxrasio);
							return;
						}
					}
				}
			},
			/* ,{
				header: headerGerid('Stok <br> Minimal'),
				width: 79,
				dataIndex: 'stokminbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			},{
				header: headerGerid('Stok <br> Maksimal'),
				width: 79,
				dataIndex: 'stokmaxbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			}, */
			{
				header: headerGerid('Catatan'),
				width: 100,
				dataIndex: 'catatan',
				sortable: true,
				align:'center',editor: {
					xtype: 'textfield',
					id: 'tfgp.catatan', 
					enableKeyEvents: true,
				}
			},{
				header: headerGerid('rasio'),
				width: 79,
				dataIndex: 'rasio',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			},{
				header: headerGerid('tmpqtyxrasio'),
				width: 79,
				dataIndex: 'tmpqtyxrasio',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			},{
				header: headerGerid('tmpidstbayar'),
				width: 79,
				dataIndex: 'tmpidstbayar',
				sortable: true,
				align: 'center',
				hidden: true
			}/* ,{
				header: headerGerid('Status Bayar'),
				width: 100,
				dataIndex: 'nmstbayar',
				sortable: true,
				align: 'center',
			} */,{
				header: headerGerid('Status Bayar'),
				width: 100,
				dataIndex: 'idstbayar',
				renderer: render_stbayar,
				sortable: true,
				editor :new Ext.form.ComboBox({
						id: 'idstbayar',
						store: ds_stbayar,
						triggerAction: 'all',
						valueField: 'idstbayar',
						displayField: 'nmstbayar',
						forceSelection: true,
						submitValue: true, 
						mode: 'local',
						typeAhead: false,
						selectOnFocus: true,
						editable: false,
						listeners: {
                            select: function(combo, record){
								/* if (record.data.idstbayar != '2'){
								}else{																						
									//Ext.MessageBox.alert('Informasi','Maaf Barang Belum Dibayarkan');
									//Ext.getCmp('gpcb.idstbayar').setValue('Belum Dibayarkan');
									if(Ext.getCmp('tf.carinoreturdet').getValue() != ''){
									}else{
										record.set('idstbayar','1');
									}
								} */
								//Ext.getCmp('tmpidstbayar').setValue(record.data.idstbayar);
								
								var recordx = ds_returbrgsupplierdet.getAt(row);
								if (record.data.idstbayar == record.data.tmpidstbayar || record.data.idstbayar == '3'){
								}else if(record.data.idstbayar == '2'){
									//record.set('idstbayar',record.data.tmpidstbayar);
									Ext.getCmp('idstbayar').setValue(recordx.data.tmpidstbayar);
								}
							}
						}
					})
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							ds_returbrgsupplierdet.removeAt(rowIndex);
						}
					}]
			}],
			listeners	: {
				rowclick : function(grid, rowIndex, e){
					row = rowIndex;
				}
			},
			//bbar: paging_daftar_returbrgsupplier
		});
		
		var winTitle = (isUpdate)?'Retur Barang Supplier (Edit)':'Retur Barang Supplier (Entry)';
		/* Ext.Ajax.request({
			url:BASE_URL + 'returbrgbagian_controller/getNmField',
			method:'POST',
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.idbagian").setValue(obj.nilai);
			}
		}); */
		
		var returbrgsupplier_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.returbrgsupplier',
			buttonAlign: 'left',
			labelWidth: 170, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 555, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					var stso = Ext.getCmp('cb.stsetuju').getValue();
					if (stso != 1){
						Ext.getCmp('df.tglretursupplier').setReadOnly(true);
						Ext.getCmp('cb.stsetuju').setReadOnly(true);
					}else{
						Ext.getCmp('df.tglretursupplier').setReadOnly(false);
					}
					simpanRetsup(); 
					return;                          
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', id: 'btn_cetak', style: 'marginLeft: 5px',
				handler: function() {
					cetak();					
				}
			},{
				text: 'Batal', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px',
				hidden: true, handler: function() {
					//batalPengbrg();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					Ext.getCmp('tf.carinoreturdet').setValue();
					ds_returbrgsupplier.reload();
					wReturbrgsupplier.close();
			}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 130, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.49, border: false, layout: 'form',					
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. Retur Supplier',
						id:'tf.noretursupplier',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tgl/Jam Retur Supplier',
						items:[{
							xtype: 'datefield',
							id: 'df.tglretursupplier',
							format: 'd-m-Y',
							value: new Date(),
							width: 120,
						},{
							xtype: 'label', id: 'lb.time', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.jamretursupplier',
							width: 60,
						}]
					},{
						xtype: 'compositefield',
						style: 'marginLeft: -108px;',
						width: 340,
						items: [{
							xtype: 'label', id: 'lb.bgn', text: 'Retur Ke Supplier:', margins: '3 5 0 4',
						},{
							xtype: 'textfield',
							id: 'tf.nmsupplier',
							width: 205,
							emptyText:'Pilih...',	
							allowBlank: false,
							readOnly: true
						},{
							xtype: 'button',
							iconCls: 'silk-find',
							id: 'btn.supplier',
							width: 3,
							handler: function(){
								fnwSupplier();
							}
						},{
							xtype: 'textfield',
							id: 'tf.kdsupplier',
							width: 20,
							hidden: true,							
						}]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Dari Bagian',
						items:[/* {
							xtype: 'combo',
							id: 'cb.bagianuntuk',
							store: ds_bagian, 
							valueField: 'idbagian', displayField: 'nmbagian',
							triggerAction: 'all', forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
							width: 205, editable: false,
							readOnly: true, style: 'opacity: 0.6'
						}, */{
							xtype: 'textfield',
							id: 'tf.idbagian',
							width: 205,
							readOnly: true,
							style: 'opacity: 0.6'
						}]
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						id  : 'ta.keterangan',
						width : 205,
						height: 70,
					},{
						xtype: 'textfield',
						fieldLabel: 'User Input',
						id: 'tf.userinput',
						width: 205,
						readOnly: true,
						value: USERNAME,
						style : 'opacity:0.6',
					}]
				}]
			},{
				xtype: 'fieldset',
				title: 'Daftar Barang Yang Diretur :',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 373,
				items: [grid_daftar_returbrgsupplier]
			}]
		});
			
		var wReturbrgsupplier = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [returbrgsupplier_form]
		});
		
	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setReturbrgsupplierForm(isUpdate, record);
		wReturbrgsupplier.show();

	/**
	FORM FUNCTIONS
	*/	
		function setReturbrgsupplierForm(isUpdate, record){
			if(isUpdate){
				if(record != null){						
					Ext.Ajax.request({
						url:BASE_URL + 'returbrgsupplier_controller/getNmsupplier',
						params:{
							kdsupplier : record.get('kdsupplier')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.nmsupplier', r);
						}
					});
					
					Ext.Ajax.request({
						url:BASE_URL + 'returbrgsupplier_controller/getNmbagian',
						params:{
							idbagian : record.get('idbagian')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.idbagian', r);
						}
					});
					
					RH.setCompValue('tf.noretursupplier', record.get('noretursupplier'));
					RH.setCompValue('df.tglretursupplier', record.get('tglretursupplier'));
					//RH.setCompValue('tf.jamkeluar', record.get('jampakaibrg'));
					RH.setCompValue('tf.nmsupplier', record.get('kdsupplier'));
					RH.setCompValue('tf.kdsupplier', record.get('kdsupplier'));
					RH.setCompValue('tf.idbagian', record.get('idbagian'));
					RH.setCompValue('cb.sttransaksi', record.get('idsttransaksi'));
					RH.setCompValue('cb.stsetuju', record.get('idstsetuju'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					return;
				}
			}
		}
		
		function fnSaveReturbrgsupplier(){
			var idForm = 'frm.returbrgsupplier';
			var sUrl = BASE_URL +'returbrgbagian_controller/insert_update_returbrgsupplier';
			var sParams = new Object({
				pembelianbrg	:	RH.getCompValue('tf.pemakaian'),
				tgltutup		:	RH.getCompValue('df.pemakaian'),
				jamtutup		:	RH.getCompValue('tf.bagian'),
				idshifttutup	:	RH.getCompValue('tf.idsifttutup'),
				saldoakhir		:	RH.getCompValue('nf.saldoakhirinput'),
				selisih			:	RH.getCompValue('nf.selisih'),
				catatantutup	:	RH.getCompValue('ta.catatantutup'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'returbrgbagian_controller/insert_update_returbrgsupplier';
				msgSuccess = 'Tutup kasir berhasil';
				msgFail = 'Tutup kasir gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wReturbrgsupplier, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
		
		function simpanRetsup(){
			var qty = Ext.getCmp('tfgp.qty').getValue();
			if(qty !='0'){
				var cekkdsupplier = Ext.getCmp('tf.kdsupplier').getValue();
				if(cekkdsupplier != ""){
					var reckdbrg = Ext.getCmp('tf.reckdbrg').getValue();
					if(reckdbrg != ""){
						var recstsetuju = Ext.getCmp('cb.stsetuju').getValue();
						if(recstsetuju != 1){
							var arrretsupplier = [];
							for(var zx = 0; zx < ds_returbrgsupplierdet.data.items.length; zx++){
								var record = ds_returbrgsupplierdet.data.items[zx].data;
								znopo = record.nopo;
								zkdbrg = record.kdbrg;
								zqty = record.qtyretur;
								zcatatan = record.catatan;
								zidstbayar = record.idstbayar;
								arrretsupplier[zx] = znopo + '-' + zkdbrg + '-' + zqty + '-' + zcatatan+ '-' + zidstbayar ;
							}
							Ext.Ajax.request({
								url: BASE_URL + 'returbrgsupplier_controller/insorupd_retsupplier',
								params: {
									noretursupplier		:	RH.getCompValue('tf.noretursupplier'),
									tglretursupplier	:	RH.getCompValue('df.tglretursupplier'),
									jamretursupplier	:	RH.getCompValue('tf.jamretursupplier'),
									idbagian			:	11, //RH.getCompValue('tf.idbagian'),
									kdsupplier			:	RH.getCompValue('tf.kdsupplier'),
									idsttransaksi		:	RH.getCompValue('cb.sttransaksi'),
									idstsetuju			:	RH.getCompValue('cb.stsetuju'),
									userid				:	USERID,
									keterangan			:	RH.getCompValue('ta.keterangan'),
									
									arrretsupplier : Ext.encode(arrretsupplier)
									
								},
								success: function(response){
									Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
									obj = Ext.util.JSON.decode(response.responseText);
									console.log(obj);
									Ext.getCmp("tf.noretursupplier").setValue(obj.noretursupplier);
									Ext.getCmp("tf.carinoreturdet").setValue(obj.noretursupplier);
									ds_returbrgsupplierdet.setBaseParam('noretursupplier', Ext.getCmp("tf.carinoreturdet").getValue(obj.noretursupplier));
									ds_returbrgsupplier.reload();
									ds_returbrgsupplierdet.reload();
									//Ext.getCmp('btn_tmbh').disable();
									Ext.getCmp('btn_simpan').disable();
									Ext.getCmp('btn_tmbh').disable();
									Ext.getCmp('bt.batal').enable();
									Ext.getCmp('btn_cetak').enable();
									var idstsetuju = Ext.getCmp('cb.stsetuju').getValue();
										if(idstsetuju != 1){
											Ext.getCmp('cb.stsetuju').setReadOnly(true);
										}
									var arruqty = [];
									for(var zx = 0; zx < ds_returbrgsupplierdet.data.items.length; zx++){
										var record = ds_returbrgsupplierdet.data.items[zx].data;
										unopo = record.nopo;
										uidbagian = 11;
										ukdbrg = record.kdbrg;
										uqtyretur = record.tmpqtyxrasio;
										arruqty[zx] = Ext.getCmp('tf.noretursupplier').getValue(obj.noretursupplier) + '-' + Ext.getCmp('tf.jamretursupplier').getValue() + '-' + Ext.getCmp('tf.kdsupplier').getValue() + '-' + uidbagian + '-' + USERID + '-' + ukdbrg + '-' + uqtyretur+ '-' + unopo;
									}
									Ext.Ajax.request({
										url: BASE_URL + 'returbrgsupplier_controller/update_qty_retur',
										params: {
											tglretursupplier	: RH.getCompValue('df.tglretursupplier'),
											arruqty 			: Ext.encode(arruqty)
											
										},							
										failure : function(){
											Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
										}
									});
									return;
								}
							});
						}else{
							var arrretsupplier = [];
							for(var zx = 0; zx < ds_returbrgsupplierdet.data.items.length; zx++){
								var record = ds_returbrgsupplierdet.data.items[zx].data;
								znopo = record.nopo;
								zkdbrg = record.kdbrg;
								zqty = record.qtyretur;
								zcatatan = record.catatan;
								zidstbayar = record.idstbayar;
								arrretsupplier[zx] = znopo + '-' + zkdbrg + '-' + zqty + '-' + zcatatan+ '-' + zidstbayar ;
							}
							Ext.Ajax.request({
								url: BASE_URL + 'returbrgsupplier_controller/insorupd_retsupplier',
								params: {
									noretursupplier		:	RH.getCompValue('tf.noretursupplier'),
									tglretursupplier	:	RH.getCompValue('df.tglretursupplier'),
									jamretursupplier	:	RH.getCompValue('tf.jamretursupplier'),
									idbagian			:	11, //RH.getCompValue('tf.idbagian'),
									kdsupplier			:	RH.getCompValue('tf.kdsupplier'),
									idsttransaksi		:	RH.getCompValue('cb.sttransaksi'),
									idstsetuju			:	RH.getCompValue('cb.stsetuju'),
									userid				:	USERID,
									keterangan			:	RH.getCompValue('ta.keterangan'),
									
									arrretsupplier : Ext.encode(arrretsupplier)
									
								},
								success: function(response){
									Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
									obj = Ext.util.JSON.decode(response.responseText);
									console.log(obj);
									Ext.getCmp("tf.noretursupplier").setValue(obj.noretursupplier);
									ds_returbrgsupplier.reload();
								},
								failure : function(){
									Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
								}
							});
						}
					}else{
						Ext.MessageBox.alert('Informasi','Isi data barang..');
					}
				}else{
					Ext.MessageBox.alert('Informasi','Isi data bagian terlebih dahulu..');
				}
			}else{
				Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh "0"');
			}
		}
		
		function batalPengbrg(){
			Ext.getCmp('cb.sttransaksi').setValue('2');
			var arrretsupplier = [];
			for(var zx = 0; zx < ds_returbrgsupplierdet.data.items.length; zx++){
				var record = ds_returbrgsupplierdet.data.items[zx].data;
				zkdbrg = record.kdbrg;
				zqty = record.qtyretur;
				zcatatan = record.catatan;
				arrretsupplier[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'returbrgbagian_controller/insorupd_pengbrg',
				params: {
					nokeluarbrg		:	RH.getCompValue('tf.noretursupplier'),
					tglkeluar		:	RH.getCompValue('df.tglkeluar'),
					jamkeluar		:	RH.getCompValue('tf.jamkeluar'),
					idbagiandari	:	11,//RH.getCompValue('tf.idbagian'),
					idbagianuntuk	:	RH.getCompValue('tf.idbagian'),
					idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
					idstsetuju		:	RH.getCompValue('cb.stsetuju'),
					userid			:	USERID,
					penerima		:	RH.getCompValue('tf.nmpenerima'),
					keterangan		:	RH.getCompValue('ta.keterangan'),
					
					arrretsupplier : Ext.encode(arrretsupplier)
					
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi','Pemakaian barang dibatalkan..');
					obj = Ext.util.JSON.decode(response.responseText);
					console.log(obj);
					Ext.getCmp("tf.noretursupplier").setValue(obj.noretursupplier);
					Ext.getCmp("tf.carinoreturdet").setValue(obj.noretursupplier);
					ds_returbrgsupplierdet.setBaseParam('noretursupplier', Ext.getCmp("tf.carinoreturdet").getValue(obj.noretursupplier));
					ds_returbrgsupplier.reload();
					ds_returbrgsupplierdet.reload();
					//Ext.getCmp('btn_tmbh').disable();
					Ext.getCmp('btn_simpan').disable();
					Ext.getCmp('bt.batal').disable();
				}
			});
			
			var arruqty = [];
			for(var zx = 0; zx < ds_returbrgsupplierdet.data.items.length; zx++){
				var record = ds_returbrgsupplierdet.data.items[zx].data;
				ukdbrg = record.kdbrg;
				uqty = record.qtyretur;
				arruqty[zx] = Ext.getCmp('cb.bagianuntuk').getValue() + '-' + Ext.getCmp('tf.idbagian').getValue() + '-' + ukdbrg + '-' + uqty;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'returbrgbagian_controller/update_qty_batal',
				params: {								
					arruqty : Ext.encode(arruqty)
					
				},							
				failure : function(){
					Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
				}
			});
		}
		
		function cetak(){
			var noretursupplier	= Ext.getCmp('tf.noretursupplier').getValue();			
			RH.ShowReport(BASE_URL + 'print/print_retursupplier/retursupplier_pdf/' + noretursupplier);
		}
	}
	
	function fncariNopo(){
		var ds_brgdiretursupplier = dm_brgdiretursupplier();		
		ds_brgdiretursupplier.setBaseParam('kdsupplier', Ext.getCmp('tf.kdsupplier').getValue());
		ds_brgdiretursupplier.reload();
		
		function keyToAddnokeluarbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_pengbrg = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. <br> Pengeluaran'),
			width: 85,
			dataIndex: 'nopo',
			sortable: true,
			align:'center',
			renderer: keyToAddnokeluarbrg
		},
		{
			header: headerGerid('Tgl <br> Pengeluaran'),
			width: 85,
			dataIndex: 'tglpo',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Kode'),
			width: 73,
			dataIndex: 'kdbrg',
			sortable: true,
		},
		{
			header: headerGerid('Nama Barang'),
			width: 200,
			dataIndex: 'nmbrg',
			sortable: true
		},
		{
			header: headerGerid('Satuan<br>(Kecil)'),
			width: 90,
			dataIndex: 'nmsatuanbsr',
			sortable: true,
			align: 'center'
		},{
			header: headerGerid('Qty <br> Terima'),
			width: 55,
			dataIndex: 'qtyterima',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Jml <br> Retur'),
			width: 55,
			dataIndex: 'jmlretur',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('qtysisa'),
			width: 55,
			dataIndex: 'qtysisa',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			hidden: true
		},{
			header: headerGerid('tamqtysisa'),
			width: 55,
			dataIndex: 'tamqtysisa',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			hidden: true
		},{
			header: headerGerid('stoknowbagian'),
			width: 55,
			dataIndex: 'stoknowbagian',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			hidden: true
		},{
			header: headerGerid('User Input'),
			width: 140,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},{
			//header: headerGerid('User Input'),
			width: 140,
			dataIndex: 'userid',
			sortable: true,
			align:'center',
			hidden: true
		},{
			header: headerGerid('ID Status Bayar'),
			width: 100,
			dataIndex: 'idstbayar',
			sortable: true,
			align:'center',
			hidden: true
		},{
			header: headerGerid('Status Bayar'),
			width: 100,
			dataIndex: 'nmstbayar',
			sortable: true,
			align:'center', 
		}
		]);
		
		var sm_pengbrg = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_pengbrg = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_pengbrg = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_brgdiretursupplier,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_pengbrg = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_pengbrg = new Ext.grid.GridPanel({
			ds: ds_brgdiretursupplier,
			cm: cm_pengbrg,
			sm: sm_pengbrg,
			view: vw_pengbrg,
			height: 460,
			width: 816,
			//plugins: cari_pengbrg,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_brgdiretursupplier.setBaseParam('key',  '1');
							ds_brgdiretursupplier.setBaseParam('id',  'nmbrg');
							ds_brgdiretursupplier.setBaseParam('name',  nmcombo);
						ds_brgdiretursupplier.load();
					}
				}]
			}],
			bbar: paging_pengbrg,
			listeners: {
				//rowdblclick: klik_cari_pengbrg
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_pengbrg = new Ext.Window({
			title: 'Tambah Barang Yang Diretur',
			modal: true,
			items: [grid_find_cari_pengbrg]
		}).show();
				
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			var qtyterima = record.data["qtyterima"];			
			var jmlretur = record.data["jmlretur"];
			
			//if(qtysisa == stoknowbagian || stoknowbagian != '0'){
				if(qtyterima != jmlretur){
					if (t.className == 'keyMasterDetail'){					
							var cek = true;
							var obj = ds_brgdiretursupplier.getAt(rowIndex);
							var snopo			= obj.get("nopo");
							var stglpo			= obj.get("tglpo");
							var skdbrg			= obj.get("kdbrg");
							var snmbrg    	 	= obj.get("nmbrg");
							var snmsatuanbsr 	= obj.get("nmsatuanbsr");
							var sqtyterima 		= obj.get("qtyterima");
							var sjmlretur 		= obj.get("jmlretur");
							var sqtysisa		= obj.get("qtysisa");
							var stamqtysisa		= obj.get("tamqtysisa");
							var sstoknowbagian	= obj.get("stoknowbagian");			
							var srasio			= obj.get("rasio");			
							var sidstbayar		= obj.get("idstbayar");			
							//var snmstbayar		= obj.get("nmstbayar");			
														
							Ext.getCmp("tf.reckdbrg").setValue(snopo);
							Ext.getCmp('btn_simpan').enable();
							
							ds_returbrgsupplierdet.each(function(rec){
								if(rec.get('nopo') == snopo && rec.get('kdbrg') == skdbrg) {
									Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
									cek = false;
								}
							});
							
							if(cek){
								var orgaListRecord = new Ext.data.Record.create([
									{
										name: 'nopo',
										name: 'tglpo',
										name: 'kdbrg',
										name: 'nmbrg',
										name: 'nmsatuanbsr',
										name: 'qtyterima',
										name: 'jmlretur',
										name: 'qtysisa',
										name: 'tamqtysisa',
										name: 'stoknowbagian',
										name: 'catatan',
										name: 'rasio',
										name: 'idstbayar',
										name: 'tmpidstbayar',
									}
								]);
								
								ds_returbrgsupplierdet.add([
									new orgaListRecord({
										'nopo'			: snopo,
										'tglpo'			: stglpo,
										'kdbrg'			: skdbrg,
										'nmbrg'			: snmbrg,
										'nmsatuanbsr'	: snmsatuanbsr,
										'qtyterima'		: sqtyterima,
										'jmlretur'		: sjmlretur,
										'qtysisa'		: sqtysisa,
										'tamqtysisa'	: stamqtysisa,
										'stoknowbagian'	: sstoknowbagian,
										'catatan'		: '-',
										'rasio'			: srasio,
										'idstbayar'		: sidstbayar,
										'tmpidstbayar'	: sidstbayar,
									})
								]);
								Ext.getCmp('btn.supplier').disable();
								ds_returbrgsupplierdet.each(function(rec){
								if(rec.get('idstbayar') == '2') {
									//Ext.MessageBox.alert('Informasi', '2');
									Ext.getCmp('idstbayar').setReadOnly(true);
								}
							});
								win_find_cari_pengbrg.close();
							}
					}
					return true;				
				}else{
					Ext.MessageBox.alert('Informasi', 'Barang sudah habis diretur..');
				}
			/* }else{
				Ext.MessageBox.alert('Informasi', 'Stok kurang untuk melakukan retur..');
			} */
		}
	}
	
	function fnwSupplier(){
		var ds_supdiretsupplier = dm_supdiretsupplier();
		
		function fnkeyAddsup(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_supplier = new Ext.grid.ColumnModel([
			{
				dataIndex: 'kdsupplier',
				width: 100,
				hidden: true
			},{
				header: 'Nama Supplier',
				dataIndex: 'nmsupplier',
				width: 224,
				renderer: fnkeyAddsup
			},{
				header: 'Alamat',
				dataIndex: 'alamat',
				width: 230, 
			},{
				header: 'No. Telepon',
				dataIndex: 'notelp',
				width: 95, 
			},{
				header: 'No. Fax',
				dataIndex: 'nofax',
				width: 70, 
			},{
				header: 'Kontak Person',
				dataIndex: 'kontakperson',
				width: 110, 
			},{
				header: 'NO. Hp',
				dataIndex: 'nohp',
				width: 95, 
			}
		]);
		
		var sm_supplier = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_supplier = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_supplier = new Ext.PagingToolbar({
			pageSize: 20,
			store: ds_supdiretsupplier,
			displayInfo: true,
			displayMsg: 'Data Supplier Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_supplier = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_supplier = new Ext.grid.GridPanel({
			ds: ds_supdiretsupplier,
			cm: cm_supplier,
			sm: sm_supplier,
			view: vw_supplier,
			height: 460,
			width: 835,
			plugins: cari_supplier,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_supplier,
			listeners: {
				cellclick: onCellClickaddsup
			}
		});
		var win_find_cari_supplier = new Ext.Window({
			title: 'Cari Supplier',
			modal: true,
			items: [grid_find_cari_supplier]
		}).show();
		
		function onCellClickaddsup(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_kdsupplier = record.data["kdsupplier"];
					var var_cari_nmsupplier = record.data["nmsupplier"];
					
					Ext.getCmp("tf.kdsupplier").setValue(var_cari_kdsupplier);
					Ext.getCmp("tf.nmsupplier").setValue(var_cari_nmsupplier);
								win_find_cari_supplier.close();
				return true;
			}
			return true;
		}
	}
}
