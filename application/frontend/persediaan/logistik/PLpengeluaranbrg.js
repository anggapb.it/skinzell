function PLpengeluaranbrg(){
	var pageSize = 18;
	var ds_bagian = dm_bagian();
	var ds_stsetuju = dm_stsetuju();
	var ds_sttransaksi = dm_sttransaksi();
	var ds_pengeluaranbrg = dm_pengeluaranbrg();
	var ds_pengeluaranbrgdet = dm_pengeluaranbrgdet();
		ds_pengeluaranbrgdet.setBaseParam('nokeluarbrg','null');	
	var ds_bagiandipengeluaranbrg = dm_bagiandipengeluaranbrg();
	ds_bagiandipengeluaranbrg.setBaseParam('userid', USERID);
	
	var row = '';
	
	var ds_pengbagian = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'pengeluaranbrg_controller/get_pengbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_pengbagian.setBaseParam('userid', USERID);
	
	var arr_cari = [['nokeluarbrg', 'No. Keluar Barang'],['nmlengkap', 'User Input'],['nmbagiandari', 'Dari Bagian'],['nmbagianuntuk', 'Untuk Bagian'],['nmstsetuju', 'Status Persetujuan'],['nmsttransaksi', 'Status Transaksi']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	function fnSearchgrid(){
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
			ds_pengeluaranbrg.setBaseParam('key',  '1');
			ds_pengeluaranbrg.setBaseParam('id',  idcombo);
			ds_pengeluaranbrg.setBaseParam('name',  nmcombo);
		ds_pengeluaranbrg.load(); 
	}
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_pengeluaranbrg,
		displayInfo: true,
		displayMsg: 'Data Pengeluaran Barang Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var vw_pengeluaranbrg = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail kasir" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var grid_pengeluaranbrg = new Ext.grid.GridPanel({
		id: 'grid_pengeluaranbrg',
		store: ds_pengeluaranbrg,
		view: vw_pengeluaranbrg,
		autoScroll: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddPengeluaranbrg();
				Ext.getCmp('tf.carinokeluarbrgdet').setValue();
				Ext.getCmp('tf.reckdbrg').setValue();
				Ext.getCmp('df.tglkeluar').setValue(new Date());
				Ext.getCmp('bt.batal').disable();
				Ext.getCmp('btn_cetak').disable();
				//Ext.getCmp('tf.bagianuntuk').setValue('11');				
				//ds_pengbagian.setBaseParam('userid', USERID);
				getId_nama();
				ds_pengbagian.reload({
					scope   : this,
					callback: function(records, operation, success) {
						var nmlkp = '';

						 ds_pengbagian.each(function (rec) { 
								nmlkp = rec.get('nmlengkap');
							});                          
						Ext.getCmp("tf.userinput").setValue(nmlkp);
						var cekuser = Ext.getCmp('tf.userinput').getValue();
						if(cekuser != ""){
							Ext.getCmp('btn_simpan').enable();
						}
						else{
							//Ext.MessageBox.alert('Informasi','Login ');
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('btn_cetak').disable();
							Ext.getCmp('bt.batal').disable();
						}
						return;
					}
				});	
			}
		},'-',{
			xtype: 'compositefield',
			width: 455,
			items: [{
				xtype: 'label', text: 'Search :', margins: '4 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.search',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				margins: '2 5 0 0',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.search').getValue();
							if(cbsearchh != ''){
								Ext.getCmp('cek').enable();
								Ext.getCmp('cek').focus();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'cek',
				width: 200,
				margins: '2 5 0 0',
				disabled: true,
				validator: function(){
					fnSearchgrid();
				}
			}/* ,{
				xtype: 'button',
				text: 'Cari',
				id: 'btn_data',
				width: 50,
				disabled: true,
				style: { 
					background:'-webkit-gradient(linear, left top, left bottom, color-stop(0.5, #fff), color-stop(0.8, #dddddd))',
					background:'-moz-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-webkit-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-o-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-ms-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'linear-gradient(to bottom, #fff 50%, #dddddd 80%)',
					'-moz-border-radius':'15px',
					'-webkit-border-radius':'15px',
					'border-radius':'5px',
					border:'1px solid #d6bcd6' 
				},
				handler: function() {
					var cbsearch = Ext.getCmp('cb.search').getValue();
					var cek = Ext.getCmp('cek').getValue();
						if(cbsearch != ''){
							if(cek != ''){
								fnSearchgrid();
							}else if(cek == ''){
								Ext.MessageBox.alert('Message', 'Isi Data Yang Akan Di Cari..!');
							}
						}else if(cbsearch == ''){
							Ext.MessageBox.alert('Message', 'Cari Berdasarkan Belum Di Pilih..!');
						}
						return;
				}
			} */]
		},{
			xtype: 'textfield',
			id:'tf.userid',
			width: 60,
			value: USERID,
			hidden: true,
			validator: function(){
				ds_pengeluaranbrg.setBaseParam('userid', Ext.getCmp('tf.userid').getValue());
				ds_pengeluaranbrg.reload();
			}
		}],
		autoHeight: true,
		//height: 530,
		columnLines: true,
		//plugins: cari_data,
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. <br> Keluar Barang'),
			width: 85,
			dataIndex: 'nokeluarbrg',
			sortable: true,
			align:'center'
		},
		{
			header: headerGerid('Tgl Keluar'),
			width: 79,
			dataIndex: 'tglkeluar',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Jam'),
			width: 53,
			dataIndex: 'jamkeluar',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('User Input'),
			width: 95,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Dari Bagian'),
			width: 110,
			dataIndex: 'nmbagiandari',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Untuk Bagian'),
			width: 110,
			dataIndex: 'nmbagianuntuk',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Penerima'),
			width: 102,
			dataIndex: 'penerima',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Status <br> Persetujuan'),
			width: 86,
			dataIndex: 'nmstsetuju',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Status <br> Transaksi'),
			width: 76,
			dataIndex: 'nmsttransaksi',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Keterangan'),
			width: 145,
			dataIndex: 'keterangan',
			sortable: true,
			align:'center',
		}/* ,{
			header: headerGerid('noretbgn'),
			width: 100,
			dataIndex: 'noreturbagian',
			sortable: true,
			align:'center',
		} */,{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditPengeluaranbrg(grid, rowIndex);
						var nokeluarbrg = RH.getCompValue('tf.nokeluarbrg', true);
						if(nokeluarbrg != ''){
							RH.setCompValue('tf.carinokeluarbrgdet', nokeluarbrg);
							Ext.getCmp('tf.reckdbrg').setValue(nokeluarbrg);
						}
						var setuju = RH.getCompValue('cb.stsetuju', true);
						if(setuju != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('cb.stsetuju').setReadOnly(true);							
							Ext.getCmp('tfgp.qty').setReadOnly(true);
							Ext.getCmp('tfgp.catatan').setReadOnly(true);
						}else{							
							//Ext.getCmp('bt.batal').disable();
						}
						var sttransaksi = RH.getCompValue('cb.sttransaksi', true);
						if(sttransaksi != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('bt.batal').disable();
						}
						var ceknoret = RH.getCompValue('tf.tamnoreturbagian', true);
						if(ceknoret != '' ){
							Ext.getCmp('bt.batal').disable();
							Ext.getCmp('btn_simpan').disable();
						}else if(ceknoret == '' ){
							//Ext.getCmp('bt.batal').enable();
						}
						ds_pengbagian.reload({
							scope   : this,
							callback: function(records, operation, success) {
								var nmlkp = '';

								 ds_pengbagian.each(function (rec) { 
										nmlkp = rec.get('nmlengkap');
									});                          
								Ext.getCmp("tf.userinput").setValue(nmlkp);
								var cekuser = Ext.getCmp('tf.userinput').getValue();
								if(cekuser != ""){
									//Ext.getCmp('btn_simpan').enable();
								}
								else{
									//Ext.MessageBox.alert('Informasi','Login ');
									Ext.getCmp('btn_tmbh').disable();
									Ext.getCmp('btn_simpan').disable();
									Ext.getCmp('btn_cetak').disable();
									Ext.getCmp('bt.batal').disable();
								}
								return;
							}
						});
						Ext.getCmp('btn_data_pen_bgn').disable();
						return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeletePengeluaranbrg(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Cetak',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetakKeluarbrg(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging		
	});	
	       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Daftar Pengeluaran Barang (Distribusi Barang)', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_pengeluaranbrg]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadPengeluaranbrg(){
		ds_pengeluaranbrg.reload();
	}
	
	function fnAddPengeluaranbrg(){
		var grid = grid_pengeluaranbrg;
		wEntryPengeluaranbrg(false, grid, null);	
	}
	
	function fnEditPengeluaranbrg(grid, record){
		var record = ds_pengeluaranbrg.getAt(record);
		wEntryPengeluaranbrg(true, grid, record);		
	}
	
	function fnDeletePengeluaranbrg(grid, record){
		var record = ds_pengeluaranbrg.getAt(record);
		var cekstatus = record.data['idstsetuju'];
		if(cekstatus!=1){
			Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di hapus..');
		}else{
			var url = BASE_URL + 'pengeluaranbrg_controller/delete_pengeluaranbrg';
			var params = new Object({
							nokeluarbrg	: record.data['nokeluarbrg']
						});
			RH.deleteGridRecord(url, params, grid );
		}
		return;
	}
	
	function cetakKeluarbrg(grid, record){
		var record = ds_pengeluaranbrg.getAt(record);
		var nokeluarbrg = record.data['nokeluarbrg'] 
		RH.ShowReport(BASE_URL + 'print/print_pengbrg/pengbrg_pdf/' + nokeluarbrg);
	}
	
	function getId_nama(){		
		Ext.getCmp('tf.kddaribagian').setValue('11');
		Ext.getCmp('tf.nmdaribagian').setValue('11');
		Ext.Ajax.request({
			url:BASE_URL + 'pengeluaranbrg_controller/getKdbagian',
			params:{
				idbagian : Ext.getCmp('tf.kddaribagian').getValue()
			},
			method:'POST',
			success: function(response){
				var r = Ext.decode(response.responseText);
				RH.setCompValue('tf.kddaribagian', r);
			}
		});
		
		Ext.Ajax.request({
			url:BASE_URL + 'pengeluaranbrg_controller/getNmdaribagian',
			params:{
				idbagian : Ext.getCmp('tf.nmdaribagian').getValue()
			},
			method:'POST',
			success: function(response){
				var r = Ext.decode(response.responseText);
				RH.setCompValue('tf.nmdaribagian', r);
			}
		});
	}
	
	/**
WIN - FORM ENTRY/EDIT 
*/

	function wEntryPengeluaranbrg(isUpdate, grid, record){
		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.jamkeluar"))
					RH.setCompValue("tf.jamkeluar",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
				
		var paging_daftar_pengeluaranbrg = new Ext.PagingToolbar({
			pageSize: pageSize,
			store: ds_pengeluaranbrgdet,
			displayInfo: true,
			displayMsg: 'Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var vw_daftar_pengeluaranbrg = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_daftar_pengeluaranbrg = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_pengeluaranbrg',
			store: ds_pengeluaranbrgdet,
			view: vw_daftar_pengeluaranbrg,
			tbar: [{
				text: 'Tambah',
				id: 'btn_tmbh',
				iconCls: 'silk-add',
				handler: function() {
					var cekbagianuntuk = Ext.getCmp('tf.bagian').getValue();
					if(cekbagianuntuk != ""){
						fncariBarang();
						Ext.getCmp('btn_data_pen_bgn').disable();
					}else{
						Ext.MessageBox.alert('Informasi','Bagian belum dipilih');
					}
				}
			},{
				xtype: 'compositefield',
				width: 515,
				items: [{
					xtype: 'label', text: 'Status Transaksi :', margins: '3 5 0 235px',
				},{
					xtype: 'combo',
					id: 'cb.sttransaksi',
					store: ds_sttransaksi, 
					valueField: 'idsttransaksi', displayField: 'nmsttransaksi',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 165, editable: false, value: 1,
					readOnly: true, style: 'opacity: 0.6',
				}]
			},{
				xtype: 'compositefield',
				width: 240,
				items: [{
					xtype: 'label', text: 'Status Persetujuan :', margins: '3 5 0 0',
				},{
					xtype: 'combo',
					id: 'cb.stsetuju',
					store: ds_stsetuju, 
					valueField: 'idstsetuju', displayField: 'nmstsetuju',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 140, allowBlank: false, editable: false, value: 2
				}]
			},{
				xtype: 'textfield',
				id:'tf.carinokeluarbrgdet',
				width: 60,
				hidden: true,
				validator: function(){
					ds_pengeluaranbrgdet.setBaseParam('nokeluarbrg', Ext.getCmp('tf.carinokeluarbrgdet').getValue());
					ds_pengeluaranbrgdet.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.reckdbrg',
				width: 60,
				hidden: true,							
			},{
				xtype: 'textfield',
				id:'tf.stsetuju',
				width: 30,
				hidden: true,							
			},{
				xtype: 'textfield',
				id:'tf.sttransaksi',
				width: 30,
				hidden: true,
				validator: function(){
					var stt = Ext.getCmp('tf.sttransaksi').getValue();
					if (stt!=1){
						Ext.getCmp('bt.batal').disable();
					}else if(stt==2){
						Ext.getCmp('bt.batal').enable();							
					}
				}
			}],
			autoScroll: true,
			height: 333, //autoHeight: true,
			columnLines: true,
			frame: true,
			clicksToEdit: 1,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('Kode Barang'),
				width: 85,
				dataIndex: 'kdbrg',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Nama Barang'),
				width: 237,
				dataIndex: 'nmbrg',
				sortable: true,
			},
			{
				header: headerGerid(' ID Jenis Barang'),
				width: 100,
				dataIndex: 'idjnsbrg',
				sortable: true,
				hidden: true,
			},
			{
				header: headerGerid('Jenis Barang'),
				width: 97,
				dataIndex: 'nmjnsbrg',
				sortable: true,
				align:'center',
			},{
				header: headerGerid('id Satuan'),
				width: 100,
				dataIndex: 'idsatuankcl',
				sortable: true,
				hidden: true
			},{
				header: headerGerid('Satuan'),
				width: 97,
				dataIndex: 'nmsatuan',
				sortable: true,
				align:'center',
			},{
				header: headerGerid('Qty'),
				width: 55,
				dataIndex: 'qty',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				editor: {
					xtype: 'numberfield',
					id: 'tfgp.qty',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							var record = ds_pengeluaranbrgdet.getAt(row);
							var stoknowbagian = record.data.stoknowbagian;
							var qty = Ext.getCmp('tfgp.qty').getValue();
							if(qty == stoknowbagian){
								//Ext.MessageBox.alert('Informasi','Anda yakin akan meretur semua Qty Sisa..?');
							}else if(qty >= stoknowbagian){
								Ext.MessageBox.alert('Informasi','Qty tidak boleh lebih dari Stok Sekarang');
								Ext.getCmp('tfgp.qty').setValue();
							}
							var cekk = Ext.getCmp('tf.stsetuju').getValue();
							if(cekk == '' ||cekk == 1){}
							else if(cekk != 1 || cekk !=''){
								Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di Edit..');
							}
							return;
						}
					}
				}
			},{
				header: headerGerid('Stok <br> Sekarang'),
				width: 79,
				dataIndex: 'stoknowbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			},{
				header: headerGerid('Stok <br> Minimal'),
				width: 79,
				dataIndex: 'stokminbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			},{
				header: headerGerid('Stok <br> Maksimal'),
				width: 79,
				dataIndex: 'stokmaxbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			},{
				header: headerGerid('Catatan'),
				width: 143,
				dataIndex: 'catatan',
				sortable: true,
				align:'center',editor: {
					xtype: 'textfield',
					id: 'tfgp.catatan',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							var cekk = Ext.getCmp('tf.stsetuju').getValue();
							if(cekk == '' ||cekk == 1){}
							else if(cekk != 1 || cekk !=''){
								Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di Edit..');
							}
						}
					}
				}
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							/* var winTitle = (isUpdate)?'Pemakaian Barang (Edit)':'Pemakaian Barang (Entry)';
							if(winTitle == 'Pemakaian Barang (Edit)'){								
								Ext.getCmp('btn_simpan').disable();
								Ext.getCmp('btn_tmbh').disable();
							}else{
								Ext.getCmp('btn_simpan').disable();
							}*/
							//return; 
							var cekk = Ext.getCmp('tf.stsetuju').getValue();
							if(cekk == '' ||cekk == 1){
								ds_pengeluaranbrgdet.removeAt(rowIndex);
							}else if(cekk != 1 || cekk !=''){
								Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di hapus..');
							}
						}
					}]
			}],
			listeners	: {
				rowclick : function(grid, rowIndex, e){
					row = rowIndex;
				}
			},
			//bbar: paging_daftar_pengeluaranbrg
		});
		
		var winTitle = (isUpdate)?'Daftar Pengeluaran Barang (Edit)':'Daftar Pengeluaran Barang (Entry)';
		/* Ext.Ajax.request({
			url:BASE_URL + 'pengeluaranbrg_controller/getNmField',
			method:'POST',
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.bagianuntuk").setValue(obj.nilai);
			}
		}); */
		
		var pengeluaranbrg_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.pengeluaranbrg',
			buttonAlign: 'left',
			labelWidth: 170, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 555, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					var stso = Ext.getCmp('cb.stsetuju').getValue();
					if (stso != 1){
						Ext.getCmp('df.tglkeluar').setReadOnly(true);
						Ext.getCmp('cb.stsetuju').setReadOnly(true);
						Ext.getCmp('tf.stsetuju').setValue(stso);
						Ext.getCmp('tfgp.qty').setReadOnly(true);
						Ext.getCmp('tfgp.catatan').setReadOnly(true);
					}else{
						Ext.getCmp('df.tglkeluar').setReadOnly(false);
					}
					simpanPengbrg();
					return;					
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', id: 'btn_cetak', style: 'marginLeft: 5px',
				handler: function() {
					cetak();
				}
			},{
				text: 'Batal', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px',
				handler: function() {
					batalPengbrg();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					Ext.getCmp('tf.carinokeluarbrgdet').setValue();
					ds_pengeluaranbrg.reload();
					wPengeluaranbrg.close();
			}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 130, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.49, border: false, layout: 'form',					
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. Pengeluaran',
						id:'tf.nokeluarbrg',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tgl/Jam Pengeluaran',
						items:[{
							xtype: 'datefield',
							id: 'df.tglkeluar',
							format: 'd-m-Y',
							//value: new Date(),
							width: 120,
							listeners:{
								select: function(field, newValue){
									Ext.getCmp('tf.ctgl').setValue('1');
								},
								change : function(field, newValue){									
									Ext.getCmp('tf.ctgl').setValue('1');
								}
							}
						},{
							xtype: 'label', id: 'lb.time', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.ctgl',
							width: 20,
							value: '0',
							hidden: true
						},{ 	
							xtype: 'textfield',
							id: 'tf.jamkeluar',
							width: 60, 
							readOnly: true,
						},{
							xtype: 'textfield',
							id: 'tf.tamnoreturbagian',
							width: 60,
							hidden: true,
							validator: function(){
								var ceknoret = RH.getCompValue('tf.tamnoreturbagian', true);
								if(ceknoret != '' ){
									Ext.getCmp('bt.batal').disable();
									Ext.getCmp('btn_simpan').disable();
								}else if(ceknoret == '' ){
									//Ext.getCmp('bt.batal').enable();
								}
							}
						}]
					},{
						xtype: 'textfield',
						fieldLabel: 'User Input',
						id: 'tf.userinput',
						width: 205,
						readOnly: true,
						style : 'opacity:0.6'						
					},{
						xtype: 'textfield',
						fieldLabel: 'Penerima',
						id: 'tf.nmpenerima',
						width: 205,						
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					items: [{
						xtype: 'compositefield',
						fieldLabel: 'Dari Bagian',
						width: 270,
						items:[{
							xtype: 'combo',
							id: 'cb.daribagian',
							store: ds_bagian, 
							valueField: 'idbagian', displayField: 'nmbagian',
							triggerAction: 'all', forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
							width: 205, editable: false, value: 'Farmasi/Apotik',
							readOnly: true, style: 'opacity: 0.6'
						},{
							xtype: 'textfield',
							id: 'tf.kddaribagian',
							width: 30,
							hidden: true
						},{
							xtype: 'textfield',
							id: 'tf.nmdaribagian',
							width: 50,
							hidden: true
						}]
					},{
						xtype: 'compositefield',
						style: 'marginLeft: -85px;',
						width: 318,
						items: [{
							xtype: 'label', id: 'lb.bgn', text: 'Untuk Bagian:', margins: '3 5 0 4',
						},{
							xtype: 'textfield',
							id: 'tf.bagian',
							emptyText:'Nama Bagian..',
							width: 205,
							readOnly: true,
							allowBlank: false
						},
						{
							xtype: 'button',
							iconCls: 'silk-find',
							id: 'btn_data_pen_bgn',
							width: 3,
							handler: function() {
								fnwBagian();
							}
						},{
							xtype: 'textfield',
							id: 'tf.bagianuntuk',
							width: 20,
							hidden: true,							
						},{
							xtype: 'textfield',
							id: 'tf.kdbagianuntuk',
							width: 50,
							hidden: true
						}]
					},
					{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						id  : 'ta.keterangan',
						width : 205,
						height: 45,
					}]
				}]
			},{
				xtype: 'fieldset',
				title: 'Daftar Barang Bagian',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 373,
				items: [grid_daftar_pengeluaranbrg]
			}]
		});
			
		var wPengeluaranbrg = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [pengeluaranbrg_form]
		});
		
	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setPengeluaranbrgForm(isUpdate, record);
		wPengeluaranbrg.show();

	/**
	FORM FUNCTIONS
	*/	
		function setPengeluaranbrgForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'pengeluaranbrg_controller/getNmbagian',
						params:{
							idbagian : record.get('idbagianuntuk')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.bagian', r);
						}
					});
					
					RH.setCompValue('tf.kddaribagian', record.get('idbagiandari'));
					Ext.Ajax.request({
						url:BASE_URL + 'pengeluaranbrg_controller/getKdbagian',
						params:{
							idbagian : Ext.getCmp('tf.kddaribagian').getValue()
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.kddaribagian', r);
						}
					});
					
					RH.setCompValue('tf.nmdaribagian', record.get('idbagiandari'));
					Ext.Ajax.request({
						url:BASE_URL + 'pengeluaranbrg_controller/getNmdaribagian',
						params:{
							idbagian : Ext.getCmp('tf.nmdaribagian').getValue()
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.nmdaribagian', r);
						}
					});
					
					RH.setCompValue('tf.kdbagianuntuk', record.get('idbagianuntuk'));
					Ext.Ajax.request({
						url:BASE_URL + 'pengeluaranbrg_controller/getKdbagianuntuk',
						params:{
							idbagian : Ext.getCmp('tf.kdbagianuntuk').getValue()
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.kdbagianuntuk', r);
						}
					});
					
					RH.setCompValue('tf.nokeluarbrg', record.get('nokeluarbrg'));
					RH.setCompValue('df.tglkeluar', record.get('tglkeluar'));
					Ext.getCmp('tf.ctgl').setValue('1');
					//RH.setCompValue('tf.jamkeluar', record.get('jampakaibrg'));
					RH.setCompValue('tf.bagian', record.get('idbagianuntuk'));
					RH.setCompValue('tf.bagianuntuk', record.get('idbagianuntuk'));
					RH.setCompValue('cb.sttransaksi', record.get('idsttransaksi'));
					RH.setCompValue('tf.sttransaksi', record.get('idsttransaksi'));
					RH.setCompValue('cb.stsetuju', record.get('idstsetuju'));
					RH.setCompValue('tf.stsetuju', record.get('idstsetuju'));
					RH.setCompValue('tf.nmpenerima', record.get('penerima'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					RH.setCompValue('tf.tamnoreturbagian', record.get('noreturbagian'));
					return;
				}
			}
		}
		
		function fnSavePengeluaranbrg(){
			var idForm = 'frm.pengeluaranbrg';
			var sUrl = BASE_URL +'pengeluaranbrg_controller/insert_update_pengeluaranbrg';
			var sParams = new Object({
				pembelianbrg	:	RH.getCompValue('tf.pemakaian'),
				tgltutup		:	RH.getCompValue('df.pemakaian'),
				jamtutup		:	RH.getCompValue('tf.bagian'),
				idshifttutup	:	RH.getCompValue('tf.idsifttutup'),
				saldoakhir		:	RH.getCompValue('nf.saldoakhirinput'),
				selisih			:	RH.getCompValue('nf.selisih'),
				catatantutup	:	RH.getCompValue('ta.catatantutup'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'pengeluaranbrg_controller/insert_update_pengeluaranbrg';
				msgSuccess = 'Tutup kasir berhasil';
				msgFail = 'Tutup kasir gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wPengeluaranbrg, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
		
		function simpanPengbrg(){
			var cekbagianuntuk = Ext.getCmp('tf.bagianuntuk').getValue();
			
			if(cekbagianuntuk != ""){
				var reckdbrg = Ext.getCmp('tf.reckdbrg').getValue();
				if(reckdbrg != ""){
					var recstsetuju = Ext.getCmp('cb.stsetuju').getValue();
					if(recstsetuju != 1){
						var arrpengbrg = [];
						for(var zx = 0; zx < ds_pengeluaranbrgdet.data.items.length; zx++){
							var record = ds_pengeluaranbrgdet.data.items[zx].data;
							zkdbrg = record.kdbrg;
							zqty = record.qty;
							zcatatan = record.catatan;
							arrpengbrg[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
						}
						Ext.Ajax.request({
							url: BASE_URL + 'pengeluaranbrg_controller/insorupd_pengbrg',
							params: {
								nokeluarbrg		:	RH.getCompValue('tf.nokeluarbrg'),
								tglkeluar		:	RH.getCompValue('df.tglkeluar'),
								jamkeluar		:	RH.getCompValue('tf.jamkeluar'),
								idbagiandari	:	11,//RH.getCompValue('tf.bagianuntuk'),
								idbagianuntuk	:	RH.getCompValue('tf.bagianuntuk'),
								idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
								idstsetuju		:	RH.getCompValue('cb.stsetuju'),
								userid			:	USERID,
								penerima		:	RH.getCompValue('tf.nmpenerima'),
								keterangan		:	RH.getCompValue('ta.keterangan'),
								ctgl		:	RH.getCompValue('tf.ctgl'),
								
								arrpengbrg : Ext.encode(arrpengbrg)
								
							},
							success: function(response){
								Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
								obj = Ext.util.JSON.decode(response.responseText);
								console.log(obj);
								Ext.getCmp("tf.nokeluarbrg").setValue(obj.nokeluarbrg);
								Ext.getCmp("tf.carinokeluarbrgdet").setValue(obj.nokeluarbrg);
								ds_pengeluaranbrgdet.setBaseParam('nokeluarbrg', Ext.getCmp("tf.carinokeluarbrgdet").getValue(obj.nokeluarbrg));
								ds_pengeluaranbrg.reload();
								ds_pengeluaranbrgdet.reload();
								Ext.getCmp('btn_tmbh').disable();
								Ext.getCmp('btn_simpan').disable();
								Ext.getCmp('bt.batal').enable();
								Ext.getCmp('btn_cetak').enable();
								var idstsetuju = Ext.getCmp('cb.stsetuju').getValue();
									if(idstsetuju != 1){
										Ext.getCmp('cb.stsetuju').setReadOnly(true);
									}
								var arruqty = [];
								for(var zx = 0; zx < ds_pengeluaranbrgdet.data.items.length; zx++){
									var record = ds_pengeluaranbrgdet.data.items[zx].data;
									ukdbrg = record.kdbrg;
									uqty = record.qty;
									ustoknow = record.stoknowbagian;
									arruqty[zx] =  Ext.getCmp('tf.nokeluarbrg').getValue(obj.nokeluarbrg) + '-' + Ext.getCmp('tf.jamkeluar').getValue() + '-' + USERID + '-' + 11 + '-' + Ext.getCmp('tf.kddaribagian').getValue() + '-' + Ext.getCmp('tf.nmdaribagian').getValue() + '-' + Ext.getCmp('tf.bagianuntuk').getValue() + '-' + Ext.getCmp('tf.kdbagianuntuk').getValue() + '-' + Ext.getCmp('tf.bagian').getValue() + '-' + ukdbrg + '-' + uqty + '-' + ustoknow;
								}
								Ext.Ajax.request({
									url: BASE_URL + 'pengeluaranbrg_controller/update_qty_tambah',
									params: {										
										tglkeluar	: RH.getCompValue('df.tglkeluar'),
										arruqty 	: Ext.encode(arruqty)
										
									},
									success: function(response){								
										ds_pengeluaranbrgdet.reload();
									},						
									failure : function(){
										Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
									}
								});
								return;
							}							
						});
					}else{
						var arrpengbrg = [];
						for(var zx = 0; zx < ds_pengeluaranbrgdet.data.items.length; zx++){
							var record = ds_pengeluaranbrgdet.data.items[zx].data;
							zkdbrg = record.kdbrg;
							zqty = record.qty;
							zcatatan = record.catatan;
							arrpengbrg[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
						}
						Ext.Ajax.request({
							url: BASE_URL + 'pengeluaranbrg_controller/insorupd_pengbrg',
							params: {
								nokeluarbrg		:	RH.getCompValue('tf.nokeluarbrg'),
								tglkeluar		:	RH.getCompValue('df.tglkeluar'),
								jamkeluar		:	RH.getCompValue('tf.jamkeluar'),
								idbagiandari	:	11,//RH.getCompValue('tf.bagianuntuk'),
								idbagianuntuk	:	RH.getCompValue('tf.bagianuntuk'),
								idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
								idstsetuju		:	RH.getCompValue('cb.stsetuju'),
								userid			:	USERID,
								penerima		:	RH.getCompValue('tf.nmpenerima'),
								keterangan		:	RH.getCompValue('ta.keterangan'),
								ctgl		:	RH.getCompValue('tf.ctgl'),
								
								arrpengbrg : Ext.encode(arrpengbrg)
								
							},
							success: function(response){
								Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
								obj = Ext.util.JSON.decode(response.responseText);
								console.log(obj);
								Ext.getCmp("tf.nokeluarbrg").setValue(obj.nokeluarbrg);
								ds_pengeluaranbrg.reload();
							},
							failure : function(){
								Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
							}
						});
					}
				}else{
					Ext.MessageBox.alert('Informasi','Isi data barang..');
				}
			}else{
				Ext.MessageBox.alert('Informasi','Isi data bagian terlebih dahulu..');
			}
		}
		
		function batalPengbrg(){
			Ext.getCmp('cb.sttransaksi').setValue('2');
			Ext.getCmp('tf.sttransaksi').setValue('2');
			var arrpengbrg = [];
			for(var zx = 0; zx < ds_pengeluaranbrgdet.data.items.length; zx++){
				var record = ds_pengeluaranbrgdet.data.items[zx].data;
				zkdbrg = record.kdbrg;
				zqty = record.qty;
				zcatatan = record.catatan;
				arrpengbrg[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'pengeluaranbrg_controller/insorupd_pengbrg',
				params: {
					nokeluarbrg		:	RH.getCompValue('tf.nokeluarbrg'),
					tglkeluar		:	RH.getCompValue('df.tglkeluar'),
					jamkeluar		:	RH.getCompValue('tf.jamkeluar'),
					idbagiandari	:	11,//RH.getCompValue('tf.bagianuntuk'),
					idbagianuntuk	:	RH.getCompValue('tf.bagianuntuk'),
					idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
					idstsetuju		:	RH.getCompValue('cb.stsetuju'),
					userid			:	USERID,
					penerima		:	RH.getCompValue('tf.nmpenerima'),
					keterangan		:	RH.getCompValue('ta.keterangan'),
					ctgl			:	RH.getCompValue('tf.ctgl'),
					
					arrpengbrg : Ext.encode(arrpengbrg)
					
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi','Pengeluaran barang dibatalkan..');
					obj = Ext.util.JSON.decode(response.responseText);
					console.log(obj);
					Ext.getCmp("tf.nokeluarbrg").setValue(obj.nokeluarbrg);
					Ext.getCmp("tf.carinokeluarbrgdet").setValue(obj.nokeluarbrg);
					ds_pengeluaranbrgdet.setBaseParam('nokeluarbrg', Ext.getCmp("tf.carinokeluarbrgdet").getValue(obj.nokeluarbrg));
					Ext.getCmp('btn_tmbh').disable();
					Ext.getCmp('btn_simpan').disable();
					
					var arruqty = [];
					for(var zx = 0; zx < ds_pengeluaranbrgdet.data.items.length; zx++){
						var record = ds_pengeluaranbrgdet.data.items[zx].data;
						ukdbrg = record.kdbrg;
						uqty = record.qty;
						ustoknow = record.stoknowbagian;
						arruqty[zx] = Ext.getCmp('tf.nokeluarbrg').getValue(obj.nokeluarbrg) + '-' + Ext.getCmp('tf.jamkeluar').getValue() + '-' + USERID + '-' + 11 + '-' + Ext.getCmp('tf.kddaribagian').getValue() + '-' + Ext.getCmp('tf.nmdaribagian').getValue() + '-' + Ext.getCmp('tf.bagianuntuk').getValue() + '-' + Ext.getCmp('tf.kdbagianuntuk').getValue() + '-' + Ext.getCmp('tf.bagian').getValue() + '-' + ukdbrg + '-' + uqty + '-' + ustoknow;
					}
					Ext.Ajax.request({
						url: BASE_URL + 'pengeluaranbrg_controller/update_qty_batal',
						params: {
							tglkeluar	: RH.getCompValue('df.tglkeluar'),
							arruqty		: Ext.encode(arruqty)
							
						},							
						failure : function(){
							Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
						}
					});
					ds_pengeluaranbrg.reload();
					ds_pengeluaranbrgdet.reload();
				}
			});	
		}

		function cetak(){
			var nokeluarbrg		= Ext.getCmp('tf.nokeluarbrg').getValue();			
			RH.ShowReport(BASE_URL + 'print/print_pengbrg/pengbrg_pdf/' + nokeluarbrg);
		}		
	}
	
	function fncariBarang(){
		var ds_brgmedisdipengeluaranbrg = dm_brgmedisdipengeluaranbrg();		
		ds_brgmedisdipengeluaranbrg.setBaseParam('idbagian', '11');
		ds_brgmedisdipengeluaranbrg.reload();
		
		function keyToAddbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_barang = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: 'Kode Barang',
			width: 73,
			dataIndex: 'kdbrg',
			sortable: true,
			renderer: keyToAddbrg
		},
		{
			header: 'Nama Barang',
			width: 352,
			dataIndex: 'nmbrg',
			sortable: true
		},
		{
			header: 'Jenis Barang',
			width: 100,
			dataIndex: 'nmjnsbrg',
			sortable: true,
			align:'center',
		},
		{
			header: 'Satuan',
			width: 97,
			dataIndex: 'nmsatuankcl',
			sortable: true,
			align:'center',
		},
		{
			header: 'Stok Sekarang',
			width: 94,
			dataIndex: 'stoknowbagian',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',				
		}
		]);
		
		var sm_barang = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_barang = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_barang = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_brgmedisdipengeluaranbrg,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_barang = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_barang = new Ext.grid.GridPanel({
			ds: ds_brgmedisdipengeluaranbrg,
			cm: cm_barang,
			sm: sm_barang,
			view: vw_barang,
			height: 460,
			width: 750,
			//plugins: cari_barang,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_brgmedisdipengeluaranbrg.setBaseParam('key',  '1');
							ds_brgmedisdipengeluaranbrg.setBaseParam('id',  'nmbrg');
							ds_brgmedisdipengeluaranbrg.setBaseParam('name',  nmcombo);
						ds_brgmedisdipengeluaranbrg.load();
					}
				}]
			}],
			bbar: paging_barang,
			listeners: {
				//rowdblclick: klik_cari_barang
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_barang = new Ext.Window({
			title: 'Cari Barang Bagian',
			modal: true,
			items: [grid_find_cari_barang]
		}).show();
				
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var cek = true;
					var obj = ds_brgmedisdipengeluaranbrg.getAt(rowIndex);
					var skdbrg			= obj.get("kdbrg");
					var snmbrg    	 	= obj.get("nmbrg");
					var sjnsbrg    	 	= obj.get("nmjnsbrg");
					var sidsatuan		= obj.get("nmsatuankcl");
					var stoknowbagian	= obj.get("stoknowbagian");
					var stokminbagian	= obj.get("stokminbagian");
					var stokmaxbagian	= obj.get("stokmaxbagian");
					
					Ext.getCmp("tf.reckdbrg").setValue(skdbrg);
					Ext.getCmp('btn_simpan').enable();
					
					ds_pengeluaranbrgdet.each(function(rec){
						if(rec.get('kdbrg') == skdbrg) {
							Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
							cek = false;
						}
					});
					
					if(cek){
						var orgaListRecord = new Ext.data.Record.create([
							{
								name: 'kdbrg',
								name: 'nmbrg',
								name: 'nmjnsbrg',
								name: 'nmsatuan',
								name: 'qty',
								name: 'stoknowbagian',
								name: 'stokminbagian',
								name: 'stokmaxbagian',
								name: 'catatan'
							}
						]);
						
						ds_pengeluaranbrgdet.add([
							new orgaListRecord({
								'kdbrg'			: skdbrg,
								'nmbrg'			: snmbrg,
								'nmjnsbrg'		: sjnsbrg,
								'nmsatuan'		: sidsatuan,
								'qty'			: 1,
								'stoknowbagian'	: stoknowbagian,
								'stokminbagian'	: stokminbagian,
								'stokmaxbagian'	: stokmaxbagian,
								'catatan'		: "-"
							})
						]);
						win_find_cari_barang.close();
					}
			}
			return true;
		}
	}
	
	function fnwBagian(){
		var ds_bagiandipengeluaranbrgdipengeluaranbrg = dm_bagiandipengeluaranbrg();
		
		function fnkeyAddBagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bagian = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200,
				renderer: fnkeyAddBagian
			},{
				header: 'Jenis Pelayanan',
				dataIndex: 'nmjnspelayanan',
				width: 150
			},{
				header: 'Bidang Perawatan',
				dataIndex: 'nmbdgrawat',
				width: 220
			}
		]);
		
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagiandipengeluaranbrg,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bagian = new Ext.grid.GridPanel({
			ds: ds_bagiandipengeluaranbrg,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 620,
			//plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_bagiandipengeluaranbrg.setBaseParam('key',  '1');
							ds_bagiandipengeluaranbrg.setBaseParam('id',  'nmbagian');
							ds_bagiandipengeluaranbrg.setBaseParam('name',  nmcombo);
						ds_bagiandipengeluaranbrg.load();
					}
				}]
			}],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Pengguna Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbagian = record.data["idbagian"];
					var var_cari_kdbagian = record.data["kdbagian"];
					var var_cari_nmbagian = record.data["nmbagian"];
								
					//Ext.getCmp('tf.bagian').focus();
					//Ext.getCmp("tf.bagian").setValue(var_cari_bagian);
					Ext.getCmp("tf.bagian").setValue(var_cari_nmbagian);
					Ext.getCmp("tf.kdbagianuntuk").setValue(var_cari_kdbagian);
					Ext.getCmp("tf.bagianuntuk").setValue(var_cari_idbagian);
					Ext.getCmp('btn_add').enable();
					//Ext.getCmp('grid_brgbagian').store.reload();
								win_find_cari_bagian.close();
				return true;
			}
			return true;
		}
	}
}
