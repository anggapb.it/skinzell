function PLreturbrgbagian(){
	var pageSize = 18;
	var ds_bagian = dm_bagian();
	var ds_stsetuju = dm_stsetuju();
	var ds_sttransaksi = dm_sttransaksi();
	var ds_returbrgbagian = dm_returbrgbagian();
	var ds_returbrgbagiandet = dm_returbrgbagiandet();
		ds_returbrgbagiandet.setBaseParam('noreturbagian','null');
		
	var row = '';
	
	var ds_bagianpeng = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'returbrgbagian_controller/get_pengbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "idjnspelayanan",
			mapping: "idjnspelayanan"
		},{
			name: "nmjnspelayanan",
			mapping: "nmjnspelayanan"
		},{
			name: "idbdgrawat",
			mapping: "idbdgrawat"
		},{
			name: "nmbdgrawat",
			mapping: "nmbdgrawat"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_bagianpeng.setBaseParam('userid', USERID);
	
	var arr_cari = [['noreturbagian', 'No. Retur Bagian'],['nmlengkap', 'User Input'],['nmbagiandari', 'Dari Bagian'],['nmbagianuntuk', 'Retur Ke Bagian'],['nmstsetuju', 'Status Persetujuan'],['nmsttransaksi', 'Status Transaksi']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	function fnSearchgrid(){
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
			ds_returbrgbagian.setBaseParam('key',  '1');
			ds_returbrgbagian.setBaseParam('id',  idcombo);
			ds_returbrgbagian.setBaseParam('name',  nmcombo);
		ds_returbrgbagian.load(); 
	}
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_returbrgbagian,
		displayInfo: true,
		displayMsg: 'Data Retur Barang Bagian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var vw_returbagian = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail kasir" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var grid_returbagian = new Ext.grid.GridPanel({
		id: 'grid_returbagian',
		store: ds_returbrgbagian,
		view: vw_returbagian,
		autoScroll: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddReturbagian();
				Ext.getCmp('tf.carinoreturdet').setValue();
				Ext.getCmp('tf.reckdbrg').setValue();
				Ext.getCmp('df.tglreturbagian').setValue(new Date());
				Ext.getCmp('bt.batal').disable();
				Ext.getCmp('btn_cetak').disable();
				//Ext.getCmp('tf.daribagian').setValue('11');	
			}
		},'-',{
			xtype: 'compositefield',
			width: 455,
			items: [{
				xtype: 'label', text: 'Search :', margins: '4 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.search',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				margins: '2 5 0 0',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.search').getValue();
							if(cbsearchh != ''){
								Ext.getCmp('cek').enable();
								Ext.getCmp('cek').focus();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'cek',
				width: 200,
				margins: '2 5 0 0',
				disabled: true,
				validator: function(){
					fnSearchgrid();
				}
			}]
		},{
			xtype: 'textfield',
			id:'tf.userid',
			width: 60,
			value: USERID,
			hidden: true,
			validator: function(){
				ds_returbrgbagian.setBaseParam('userid', Ext.getCmp('tf.userid').getValue());
				ds_returbrgbagian.reload();
			}
		}],
		autoHeight: true,
		//height: 530,
		columnLines: true,
		//plugins: cari_data,
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. <br> Retur Bagian'),
			width: 85,
			dataIndex: 'noreturbagian',
			sortable: true,
			align:'center'
		},
		{
			header: headerGerid('Tgl <br> Retur Bagian'),
			width: 85,
			dataIndex: 'tglreturbagian',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Jam'),
			width: 60,
			dataIndex: 'jamreturbagian',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('User Input'),
			width: 140,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Dari Bagian'),
			width: 125,
			dataIndex: 'nmbagiandari',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Retur Ke Bagian'),
			width: 125,
			dataIndex: 'nmbagianuntuk',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Status <br> Persetujuan'),
			width: 86,
			dataIndex: 'nmstsetuju',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Status <br> Transaksi'),
			width: 76,
			dataIndex: 'nmsttransaksi',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Keterangan'),
			width: 160,
			dataIndex: 'keterangan',
			sortable: true,
			align:'center',
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditReturbagian(grid, rowIndex);
						var noreturbagian = RH.getCompValue('tf.noreturbagian', true);
						if(noreturbagian != ''){
							RH.setCompValue('tf.carinoreturdet', noreturbagian);
							Ext.getCmp('tf.reckdbrg').setValue(noreturbagian);
						}
						var setuju = RH.getCompValue('cb.stsetuju', true);
						if(setuju != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('cb.stsetuju').setReadOnly(true);
						}else{							
							Ext.getCmp('bt.batal').disable();
						}
						var sttransaksi = RH.getCompValue('cb.sttransaksi', true);
						if(sttransaksi != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('bt.batal').disable();
						}
						Ext.getCmp('btn_data_pen_bgn').disable();
						return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteReturbagian(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Cetak',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetakRetbrgbagian(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging		
	});	
	       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Daftar Retur Barang Bagian', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_returbagian]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadReturbagian(){
		ds_returbrgbagian.reload();
	}
	
	function fnAddReturbagian(){
		var grid = grid_returbagian;
		wEntryReturbagian(false, grid, null);	
	}
	
	function fnEditReturbagian(grid, record){
		var record = ds_returbrgbagian.getAt(record);
		wEntryReturbagian(true, grid, record);		
	}
	
	function fnDeleteReturbagian(grid, record){
		var record = ds_returbrgbagian.getAt(record);
		var cekstatus = record.data['idstsetuju'];
		if(cekstatus!=1){
			Ext.MessageBox.alert('Informasi','Data sudah tidak bisa dihapus..');
		}else{
			var url = BASE_URL + 'returbrgbagian_controller/delete_returbagian';
			var params = new Object({
							noreturbagian	: record.data['noreturbagian']
						});
			RH.deleteGridRecord(url, params, grid );
		}
		return;
	}
	
	function cetakRetbrgbagian(grid, record){
		var record = ds_returbrgbagian.getAt(record);
		var noreturbagian = record.data['noreturbagian'] 
		RH.ShowReport(BASE_URL + 'print/print_returbagian/returbagian_pdf/' + noreturbagian);
	}
	
	/**
WIN - FORM ENTRY/EDIT 
*/

	function wEntryReturbagian(isUpdate, grid, record){	
		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.jamreturbagian"))
					RH.setCompValue("tf.jamreturbagian",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
				
		var paging_daftar_returbagian = new Ext.PagingToolbar({
			pageSize: pageSize,
			store: ds_returbrgbagiandet,
			displayInfo: true,
			displayMsg: 'Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var vw_daftar_returbagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_daftar_returbagian = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_returbagian',
			store: ds_returbrgbagiandet,
			view: vw_daftar_returbagian,
			tbar: [{
				text: 'Tambah',
				id: 'btn_tmbh',
				iconCls: 'silk-add',
				handler: function() {
					var cekbagianuntuk = Ext.getCmp('tf.bagian').getValue();
					if(cekbagianuntuk != ""){
						fncariPengbrg();
						//Ext.getCmp('btn_data_pen_bgn').disable();
					}else{
						Ext.MessageBox.alert('Informasi','Bagian belum dipilih');
					}
				}
			},{
				xtype: 'compositefield',
				width: 515,
				items: [{
					xtype: 'label', text: 'Status Transaksi :', margins: '3 5 0 235px',
				},{
					xtype: 'combo',
					id: 'cb.sttransaksi',
					store: ds_sttransaksi, 
					valueField: 'idsttransaksi', displayField: 'nmsttransaksi',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 165, editable: false, value: 1,
					readOnly: true, style: 'opacity: 0.6'
				}]
			},{
				xtype: 'compositefield',
				width: 240,
				items: [{
					xtype: 'label', text: 'Status Persetujuan :', margins: '3 5 0 0',
				},{
					xtype: 'combo',
					id: 'cb.stsetuju',
					store: ds_stsetuju, 
					valueField: 'idstsetuju', displayField: 'nmstsetuju',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 140, allowBlank: false, editable: false, value: 2
				}]
			},{
				xtype: 'textfield',
				id:'tf.carinoreturdet',
				width: 60,
				hidden: true,
				validator: function(){
					ds_returbrgbagiandet.setBaseParam('noreturbagian', Ext.getCmp('tf.carinoreturdet').getValue());
					ds_returbrgbagiandet.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.reckdbrg',
				width: 60,
				hidden: true,							
			}],
			autoScroll: true,
			height: 333, //autoHeight: true,
			columnLines: true,
			frame: true,
			clicksToEdit: 1,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('No. <br> Pengeluaran'),
				width: 85,
				dataIndex: 'nokeluarbrg',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Tgl <br> Pengeluaran'),
				width: 85,
				dataIndex: 'tglkeluar',
				sortable: true,
				align:'center',
				renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			},{
				header: headerGerid('Kode Barang'),
				width: 85,
				dataIndex: 'kdbrg',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Nama Barang'),
				width: 237,
				dataIndex: 'nmbrg',
				sortable: true,
			},
			/* {
				header: headerGerid(' ID Jenis Barang'),
				width: 100,
				dataIndex: 'idjnsbrg',
				sortable: true,
				hidden: true,
			},
			{
				header: headerGerid('Jenis Barang'),
				width: 97,
				dataIndex: 'nmjnsbrg',
				sortable: true,
				align:'center',
			},{
				header: headerGerid('id Satuan'),
				width: 100,
				dataIndex: 'idsatuankcl',
				sortable: true,
				hidden: true
			},
			{
				header: headerGerid('Satuan'),
				width: 97,
				dataIndex: 'nmsatuan',
				sortable: true,
				align:'center',
			}, */
			{
				header: headerGerid('Qty <br> Terima'),
				width: 55,
				dataIndex: 'qtyterima',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',				
			},{
				header: headerGerid('qtysisa'),
				width: 55,
				dataIndex: 'qtysisa',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			},{
				header: headerGerid('Jml <br> Retur'),
				width: 55,
				dataIndex: 'jmlretur',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',				
			},{
				header: headerGerid('Qty <br> Sisa tam'),
				width: 55,
				dataIndex: 'tamqtysisa',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true				
			},{
				header: headerGerid('stoknowbagiandari'),
				width: 55,
				dataIndex: 'stoknowbagiandari',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			},{
				header: headerGerid('stoknowbagianuntuk'),
				width: 55,
				dataIndex: 'stoknowbagianuntuk',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			},{
				header: headerGerid('Qty <br> Retur'),
				width: 55,
				dataIndex: 'qtyretur',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				editor: {
					xtype: 'numberfield',
					id: 'tfgp.qty',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							var record = ds_returbrgbagiandet.getAt(row);
							var stoknowbagian = record.data.stoknowbagiandari;
							var qtyterima = record.data.qtyterima;
							var tamqtysisa = record.data.tamqtysisa;
							var qtyretur = Ext.getCmp('tfgp.qty').getValue();	
							
							if(qtyretur == stoknowbagian){}
							else if(qtyretur >= stoknowbagian){
								 Ext.MessageBox.alert('Informasi', 'Jumlah retur melebihi Stok..');
								 Ext.getCmp('tfgp.qty').setValue();
								 return;
							}
							if(tamqtysisa == qtyretur){}
							else if(qtyretur >= qtyterima){								
								Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Terima..');
								Ext.getCmp('tfgp.qty').setValue();
							}else if(tamqtysisa <= qtyretur){								
								Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Sisa..');
								Ext.getCmp('tfgp.qty').setValue();
							}
							return;
						}
					}
				}
			},
			/* ,{
				header: headerGerid('Stok <br> Sekarang'),
				width: 79,
				dataIndex: 'stoknowbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			},{
				header: headerGerid('Stok <br> Minimal'),
				width: 79,
				dataIndex: 'stokminbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			},{
				header: headerGerid('Stok <br> Maksimal'),
				width: 79,
				dataIndex: 'stokmaxbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right'
			}, */
			{
				header: headerGerid('Catatan'),
				width: 130,
				dataIndex: 'catatan',
				sortable: true,
				align:'center',editor: {
					xtype: 'textfield',
					id: 'tfgp.catatan', 
					enableKeyEvents: true,
				}
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							ds_returbrgbagiandet.removeAt(rowIndex);
						}
					}]
			}],
			listeners	: {
				rowclick : function(grid, rowIndex, e){
					row = rowIndex;
				}
			},
			//bbar: paging_daftar_returbagian
		});
		
		var winTitle = (isUpdate)?'Retur Barang Bagian (Edit)':'Retur Barang Bagian (Entry)';
		/* Ext.Ajax.request({
			url:BASE_URL + 'returbrgbagian_controller/getNmField',
			method:'POST',
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.daribagian").setValue(obj.nilai);
			}
		}); */
		
		var returbagian_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.returbagian',
			buttonAlign: 'left',
			labelWidth: 170, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 555, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					var stso = Ext.getCmp('cb.stsetuju').getValue();
					if (stso != 1){
						Ext.getCmp('df.tglreturbagian').setReadOnly(true);
						Ext.getCmp('cb.stsetuju').setReadOnly(true);
					}else{
						Ext.getCmp('df.tglreturbagian').setReadOnly(false);
					}
					simpanRetbagian();
					return;						
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', id: 'btn_cetak', style: 'marginLeft: 5px',
				handler: function() {
					cetak();					
				}
			},{
				text: 'Batal', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px',
				hidden: true,handler: function() {
					//batalPengbrg();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					Ext.getCmp('tf.carinoreturdet').setValue();
					ds_returbrgbagian.reload();
					wReturbagian.close();
			}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 130, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.49, border: false, layout: 'form',					
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. Retur Bagian',
						id:'tf.noreturbagian',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tgl/Jam Retur Bagian',
						items:[{
							xtype: 'datefield',
							id: 'df.tglreturbagian',
							format: 'd-m-Y',
							//value: new Date(),
							width: 120
						},{
							xtype: 'label', id: 'lb.time', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.jamreturbagian',
							width: 60,
						}]
					},{
						xtype: 'compositefield',
						style: 'marginLeft: -72px;',
						width: 318,
						items: [{
							xtype: 'label', id: 'lb.bgn', text: 'Dari Bagian:', margins: '3 5 0 4',
						},{
							xtype: 'textfield',
							id: 'tf.bagian',
							emptyText:'Nama Bagian..',
							width: 205,
							readOnly: true,
							allowBlank: false
						},
						{
							xtype: 'button',
							iconCls: 'silk-find',
							id: 'btn_data_pen_bgn',
							width: 3,
							handler: function() {
								fnwBagian();
							}
						},{
							xtype: 'textfield',
							id: 'tf.daribagian',
							width: 20,
							hidden: true,							
						}]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Retur Ke Bagian',
						items:[{
							xtype: 'combo',
							id: 'cb.bagianuntuk',
							store: ds_bagian, 
							valueField: 'idbagian', displayField: 'nmbagian',
							triggerAction: 'all', forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
							width: 205, editable: false, value: 'INSTALASI FARMASI',
							readOnly: true, style: 'opacity: 0.6'
						}/* ,{
							xtype: 'textfield',
							id: 'tf.daribagian',
							width: 50,
							hidden: true,
							validator: function(){
								var tam = Ext.getCmp('tf.daribagian').getValue();
								if(tam !=""){
									Ext.getCmp('cb.bagianuntuk').setValue(tam);
								}
							}
						} */]
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						id  : 'ta.keterangan',
						width : 205,
						height: 70,
					},{
						xtype: 'textfield',
						fieldLabel: 'User Input',
						id: 'tf.userinput',
						width: 205,
						readOnly: true,
						value: USERNAME,
						style : 'opacity:0.6',
					}]
				}]
			},{
				xtype: 'fieldset',
				title: 'Daftar Barang Bagian',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 373,
				items: [grid_daftar_returbagian]
			}]
		});
			
		var wReturbagian = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [returbagian_form]
		});
		
	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setReturbagianForm(isUpdate, record);
		wReturbagian.show();

	/**
	FORM FUNCTIONS
	*/	
		function setReturbagianForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'returbrgbagian_controller/getNmbagian',
						params:{
							idbagian : record.get('idbagiandari')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.bagian', r);
						}
					});
					
					RH.setCompValue('tf.noreturbagian', record.get('noreturbagian'));
					RH.setCompValue('df.tglreturbagian', record.get('tglreturbagian'));
					//RH.setCompValue('tf.jamkeluar', record.get('jampakaibrg'));
					RH.setCompValue('tf.bagian', record.get('idbagiandari'));
					RH.setCompValue('tf.daribagian', record.get('idbagiandari'));
					RH.setCompValue('cb.sttransaksi', record.get('idsttransaksi'));
					RH.setCompValue('cb.stsetuju', record.get('idstsetuju'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					return;
				}
			}
		}
		
		function fnSaveReturbagian(){
			var idForm = 'frm.returbagian';
			var sUrl = BASE_URL +'returbrgbagian_controller/insert_update_returbagian';
			var sParams = new Object({
				pembelianbrg	:	RH.getCompValue('tf.pemakaian'),
				tgltutup		:	RH.getCompValue('df.pemakaian'),
				jamtutup		:	RH.getCompValue('tf.bagian'),
				idshifttutup	:	RH.getCompValue('tf.idsifttutup'),
				saldoakhir		:	RH.getCompValue('nf.saldoakhirinput'),
				selisih			:	RH.getCompValue('nf.selisih'),
				catatantutup	:	RH.getCompValue('ta.catatantutup'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'returbrgbagian_controller/insert_update_returbagian';
				msgSuccess = 'Tutup kasir berhasil';
				msgFail = 'Tutup kasir gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wReturbagian, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
		
		function simpanRetbagian(){
			var qty = Ext.getCmp('tfgp.qty').getValue();
			if(qty !='0'){
				var cekdaribagian = Ext.getCmp('tf.daribagian').getValue();
				if(cekdaribagian != ""){
					var reckdbrg = Ext.getCmp('tf.reckdbrg').getValue();
					if(reckdbrg != ""){
						var recstsetuju = Ext.getCmp('cb.stsetuju').getValue();
						if(recstsetuju != 1){
							var arrretbagian = [];
							for(var zx = 0; zx < ds_returbrgbagiandet.data.items.length; zx++){
								var record = ds_returbrgbagiandet.data.items[zx].data;
								znokeluarbrg = record.nokeluarbrg;
								zkdbrg = record.kdbrg;
								zqty = record.qtyretur;
								zcatatan = record.catatan;
								arrretbagian[zx] = znokeluarbrg + '-' + zkdbrg + '-' + zqty + '-' + zcatatan ;
							}
							Ext.Ajax.request({
								url: BASE_URL + 'returbrgbagian_controller/insorupd_retbagian',
								params: {
									noreturbagian	:	RH.getCompValue('tf.noreturbagian'),
									tglreturbagian	:	RH.getCompValue('df.tglreturbagian'),
									jamreturbagian	:	RH.getCompValue('tf.jamreturbagian'),
									idbagiandari	:	RH.getCompValue('tf.daribagian'),
									idbagianuntuk	:	11, //RH.getCompValue('tf.daribagian'),
									idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
									idstsetuju		:	RH.getCompValue('cb.stsetuju'),
									userid			:	USERID,
									keterangan		:	RH.getCompValue('ta.keterangan'),
									
									arrretbagian : Ext.encode(arrretbagian)
									
								},
								success: function(response){	
									Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
									obj = Ext.util.JSON.decode(response.responseText);
									console.log(obj);
									Ext.getCmp("tf.noreturbagian").setValue(obj.noreturbagian);
									Ext.getCmp("tf.carinoreturdet").setValue(obj.noreturbagian);
									ds_returbrgbagiandet.setBaseParam('noreturbagian', Ext.getCmp("tf.carinoreturdet").getValue(obj.noreturbagian));
									ds_returbrgbagian.reload();
									ds_returbrgbagiandet.reload();
									//Ext.getCmp('btn_tmbh').disable();
									Ext.getCmp('btn_simpan').disable();
									Ext.getCmp('bt.batal').enable();
									Ext.getCmp('btn_cetak').enable();
									var idstsetuju = Ext.getCmp('cb.stsetuju').getValue();
										if(idstsetuju != 1){
											Ext.getCmp('cb.stsetuju').setReadOnly(true);
										}
									var arruqty = [];
									for(var zx = 0; zx < ds_returbrgbagiandet.data.items.length; zx++){
										var record = ds_returbrgbagiandet.data.items[zx].data;
										ukdbrg = record.kdbrg;
										uqtyretur = record.qtyretur;
										ustoknow = record.stoknowbagianuntuk;
										arruqty[zx] = Ext.getCmp('tf.noreturbagian').getValue(obj.noreturbagian) + '-' + Ext.getCmp('tf.jamreturbagian').getValue() + '-' + Ext.getCmp('tf.daribagian').getValue() + '-' + 11 + '-' + USERID + '-' + ukdbrg + '-' + uqtyretur + '-' + ustoknow;
									}
									Ext.Ajax.request({
										url: BASE_URL + 'returbrgbagian_controller/update_qty_tambah',
										params: {
											tglreturbagian	: RH.getCompValue('df.tglreturbagian'),
											arruqty 		: Ext.encode(arruqty)
											
										},							
										failure : function(){
											Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
										}
									});
									return;
								}
							});
						}else{
							var arrretbagian = [];
							for(var zx = 0; zx < ds_returbrgbagiandet.data.items.length; zx++){
								var record = ds_returbrgbagiandet.data.items[zx].data;
								znokeluarbrg = record.nokeluarbrg;
								zkdbrg = record.kdbrg;
								zqty = record.qtyretur;
								zcatatan = record.catatan;
								arrretbagian[zx] = znokeluarbrg + '-' + zkdbrg + '-' + zqty + '-' + zcatatan ;
							}
							Ext.Ajax.request({
								url: BASE_URL + 'returbrgbagian_controller/insorupd_retbagian',
								params: {
									noreturbagian	:	RH.getCompValue('tf.noreturbagian'),
									tglreturbagian	:	RH.getCompValue('df.tglreturbagian'),
									jamreturbagian	:	RH.getCompValue('tf.jamreturbagian'),
									idbagiandari	:	RH.getCompValue('tf.daribagian'),
									idbagianuntuk	:	11, //RH.getCompValue('tf.daribagian'),
									idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
									idstsetuju		:	RH.getCompValue('cb.stsetuju'),
									userid			:	USERID,
									keterangan		:	RH.getCompValue('ta.keterangan'),
									
									arrretbagian : Ext.encode(arrretbagian)
									
								},
								success: function(response){
									Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
									obj = Ext.util.JSON.decode(response.responseText);
									console.log(obj);
									Ext.getCmp("tf.noreturbagian").setValue(obj.noreturbagian);
									ds_returbrgbagian.reload();
								},
								failure : function(){
									Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
								}
							});
						}
					}else{
						Ext.MessageBox.alert('Informasi','Isi data barang..');
					}
				}else{
					Ext.MessageBox.alert('Informasi','Isi data bagian terlebih dahulu..');
				}
			}else{
				Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh "0"');
			}
		}
		
		function batalPengbrg(){
			Ext.getCmp('cb.sttransaksi').setValue('2');
			var arrretbagian = [];
			for(var zx = 0; zx < ds_returbrgbagiandet.data.items.length; zx++){
				var record = ds_returbrgbagiandet.data.items[zx].data;
				zkdbrg = record.kdbrg;
				zqty = record.qtyretur;
				zcatatan = record.catatan;
				arrretbagian[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'returbrgbagian_controller/insorupd_pengbrg',
				params: {
					nokeluarbrg		:	RH.getCompValue('tf.noreturbagian'),
					tglkeluar		:	RH.getCompValue('df.tglkeluar'),
					jamkeluar		:	RH.getCompValue('tf.jamkeluar'),
					idbagiandari	:	11,//RH.getCompValue('tf.daribagian'),
					idbagianuntuk	:	RH.getCompValue('tf.daribagian'),
					idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
					idstsetuju		:	RH.getCompValue('cb.stsetuju'),
					userid			:	USERID,
					penerima		:	RH.getCompValue('tf.nmpenerima'),
					keterangan		:	RH.getCompValue('ta.keterangan'),
					
					arrretbagian : Ext.encode(arrretbagian)
					
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi','Pemakaian barang dibatalkan..');
					obj = Ext.util.JSON.decode(response.responseText);
					console.log(obj);
					Ext.getCmp("tf.noreturbagian").setValue(obj.nokeluarbrg);
					Ext.getCmp("tf.carinoreturdet").setValue(obj.nokeluarbrg);
					ds_returbrgbagiandet.setBaseParam('nokeluarbrg', Ext.getCmp("tf.carinoreturdet").getValue(obj.nokeluarbrg));
					ds_returbrgbagian.reload();
					ds_returbrgbagiandet.reload();
					//Ext.getCmp('btn_tmbh').disable();
					Ext.getCmp('btn_simpan').disable();
					Ext.getCmp('bt.batal').disable();
				}
			});
			
			var arruqty = [];
			for(var zx = 0; zx < ds_returbrgbagiandet.data.items.length; zx++){
				var record = ds_returbrgbagiandet.data.items[zx].data;
				ukdbrg = record.kdbrg;
				uqty = record.qtyretur;
				arruqty[zx] = 11 + '-' + Ext.getCmp('tf.daribagian').getValue() + '-' + ukdbrg + '-' + uqty;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'returbrgbagian_controller/update_qty_batal',
				params: {								
					arruqty : Ext.encode(arruqty)
					
				},							
				failure : function(){
					Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
				}
			});
		}
		
		function cetak(){
			var noreturbagian	= Ext.getCmp('tf.noreturbagian').getValue();			
			RH.ShowReport(BASE_URL + 'print/print_returbagian/returbagian_pdf/' + noreturbagian);
		}
	}
	
	function fncariPengbrg(){
		var ds_pengbrgdireturbagian = dm_pengbrgdireturbagian();		
		ds_pengbrgdireturbagian.setBaseParam('idbagian', Ext.getCmp('tf.daribagian').getValue());
		ds_pengbrgdireturbagian.reload();
		
		function keyToAddnokeluarbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_pengbrg = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. <br> Pengeluaran'),
			width: 85,
			dataIndex: 'nokeluarbrg',
			sortable: true,
			align:'center',
			renderer: keyToAddnokeluarbrg
		},
		{
			header: headerGerid('Tgl <br> Pengeluaran'),
			width: 85,
			dataIndex: 'tglkeluar',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Kode'),
			width: 73,
			dataIndex: 'kdbrg',
			sortable: true,
		},
		{
			header: headerGerid('Nama Barang'),
			width: 200,
			dataIndex: 'nmbrg',
			sortable: true
		},{
			header: headerGerid('Qty <br> Terima'),
			width: 55,
			dataIndex: 'qtyterima',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Jml <br> Retur'),
			width: 55,
			dataIndex: 'jmlretur',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('qtysisa'),
			width: 55,
			dataIndex: 'qtysisa',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			hidden: true
		},{
			header: headerGerid('tamqtysisa'),
			width: 55,
			dataIndex: 'tamqtysisa',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			hidden: true
		},{
			header: headerGerid('stoknowbagian'),
			width: 55,
			dataIndex: 'stoknowbagian',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			hidden: true
		},{
			header: headerGerid('Catatan'),
			width: 160,
			dataIndex: 'catatan',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('User Input'),
			width: 140,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},{
			//header: headerGerid('User Input'),
			width: 140,
			dataIndex: 'userid',
			sortable: true,
			align:'center',
			hidden: true
		}
		]);
		
		var sm_pengbrg = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_pengbrg = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_pengbrg = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_pengbrgdireturbagian,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_pengbrg = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_pengbrg = new Ext.grid.GridPanel({
			ds: ds_pengbrgdireturbagian,
			cm: cm_pengbrg,
			sm: sm_pengbrg,
			view: vw_pengbrg,
			height: 460,
			width: 886,
			//plugins: cari_pengbrg,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_pengbrgdireturbagian.setBaseParam('key',  '1');
							ds_pengbrgdireturbagian.setBaseParam('id',  'nmbrg');
							ds_pengbrgdireturbagian.setBaseParam('name',  nmcombo);
						ds_pengbrgdireturbagian.load();
					}
				}]
			}],
			bbar: paging_pengbrg,
			listeners: {
				//rowdblclick: klik_cari_pengbrg
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_pengbrg = new Ext.Window({
			title: 'Tambah Barang Yang Diretur',
			modal: true,
			items: [grid_find_cari_pengbrg]
		}).show();
				
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			var qtyterima = record.data["qtyterima"];			
			var jmlretur = record.data["jmlretur"];
			
			//if(qtysisa == stoknowbagian || stoknowbagian != '0'){
				if(qtyterima != jmlretur){
					if (t.className == 'keyMasterDetail'){					
							var cek = true;
							var obj = ds_pengbrgdireturbagian.getAt(rowIndex);
							var snokeluarbrg	= obj.get("nokeluarbrg");
							var stglkeluar		= obj.get("tglkeluar");
							var skdbrg			= obj.get("kdbrg");
							var snmbrg    	 	= obj.get("nmbrg");
							var sqtyterima 		= obj.get("qtyterima");
							var sjmlretur 		= obj.get("jmlretur");
							var sqtysisa		= obj.get("qtysisa");
							var stamqtysisa		= obj.get("tamqtysisa");
							var sstoknowbagiandari	= obj.get("stoknowbagiandari");
							var sstoknowbagianuntuk	= obj.get("stoknowbagianuntuk");
							var scatatan 		= obj.get("catatan");
							var suserid 		= obj.get("userid");
							var snmlengkap 		= obj.get("nmlengkap");					
							
							
							Ext.getCmp("tf.reckdbrg").setValue(snokeluarbrg);
							Ext.getCmp('btn_simpan').enable();
							
							ds_returbrgbagiandet.each(function(rec){
								if(rec.get('nokeluarbrg') == snokeluarbrg && rec.get('kdbrg') == skdbrg) {
									Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
									cek = false;
								}
							});
							
							if(cek){
								var orgaListRecord = new Ext.data.Record.create([
									{
										name: 'nokeluarbrg',
										name: 'tglkeluar',
										name: 'kdbrg',
										name: 'nmbrg',
										name: 'qtyterima',
										name: 'jmlretur',
										name: 'qtysisa',
										name: 'tamqtysisa',
										name: 'stoknowbagiandari',
										name: 'stoknowbagianuntuk',
										name: 'catatan',
										name: 'userid',
										name: 'nmlengkap'
									}
								]);
								
								ds_returbrgbagiandet.add([
									new orgaListRecord({
										'nokeluarbrg'	: snokeluarbrg,
										'tglkeluar'		: stglkeluar,
										'kdbrg'			: skdbrg,
										'nmbrg'			: snmbrg,
										'qtyterima'		: sqtyterima,
										'jmlretur'		: sjmlretur,
										'qtysisa'		: sqtysisa,
										'tamqtysisa'	: stamqtysisa,
										'stoknowbagiandari'	: sstoknowbagiandari,
										'stoknowbagianuntuk': sstoknowbagianuntuk,
										'catatan'		: scatatan,
										'userid'		: suserid,
										'nmlengkap'		: snmlengkap
									})
								]);
								Ext.getCmp('btn_data_pen_bgn').disable();
								win_find_cari_pengbrg.close();
							}
					}
					return true;				
				}else{
					Ext.MessageBox.alert('Informasi', 'Barang sudah habis diretur..');
				}
			/* }else{
				Ext.MessageBox.alert('Informasi', 'Stok kurang untuk melakukan retur..');
			} */
		}
	}
	
	function fnwBagian(){
		var ds_bagianpeng = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'returbrgbagian_controller/get_pengbagian',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "idbagian",
				mapping: "idbagian"
			},{
				name: "nmbagian",
				mapping: "nmbagian"
			},{
				name: "idjnspelayanan",
				mapping: "idjnspelayanan"
			},{
				name: "nmjnspelayanan",
				mapping: "nmjnspelayanan"
			},{
				name: "idbdgrawat",
				mapping: "idbdgrawat"
			},{
				name: "nmbdgrawat",
				mapping: "nmbdgrawat"
			},{
				name: "userid",
				mapping: "userid"
			},{
				name: "nmlengkap",
				mapping: "nmlengkap"
			}]
		});
		
		ds_bagianpeng.setBaseParam('userid', USERID);
		
		function fnkeyAddBagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bagian = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200,
				renderer: fnkeyAddBagian
			},{
				header: 'Jenis Pelayanan',
				dataIndex: 'nmjnspelayanan',
				width: 150
			},{
				header: 'Bidang Perawatan',
				dataIndex: 'nmbdgrawat',
				width: 220
			}
		]);
		
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagianpeng,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bagian = new Ext.grid.GridPanel({
			ds: ds_bagianpeng,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 620,
			//plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_bagianpeng.setBaseParam('key',  '1');
							ds_bagianpeng.setBaseParam('id',  'nmbagian');
							ds_bagianpeng.setBaseParam('name',  nmcombo);
						ds_bagianpeng.load();
					}
				}]
			}],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Pengguna Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbagian = record.data["idbagian"];
					var var_cari_nmbagian = record.data["nmbagian"];
								
					//Ext.getCmp('tf.bagian').focus();
					//Ext.getCmp("tf.bagian").setValue(var_cari_bagian);
					Ext.getCmp("tf.bagian").setValue(var_cari_nmbagian);
					Ext.getCmp("tf.daribagian").setValue(var_cari_idbagian);
					Ext.getCmp('btn_add').enable();
					//Ext.getCmp('grid_brgbagian').store.reload();
								win_find_cari_bagian.close();
				return true;
			}
			return true;
		}
	}
}
