function honor_dokter(){
	var ds_hondok = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_hondok',
			method: 'POST'
		}),
		baseParams: {
			cbx : false
		},
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
				name: 'nohondok', 
				mapping:'nohondok'
			},{
				name: 'tglhondok', 
				mapping:'tglhondok'
			},{
				name: 'tglawal', 
				mapping:'tglawal'
			},{
				name: 'jasadrjaga', 
				mapping:'jasadrjaga'
			},{
				name: 'potonganlain', 
				mapping:'potonganlain'
			},{
				name: 'userid', 
				mapping:'userid'
			},{
				name: 'nmlengkap', 
				mapping:'nmlengkap'
			},{
				name: 'iddokter', 
				mapping:'iddokter'
			},{
				name: 'nmdoktergelar', 
				mapping:'nmdoktergelar'
			},{
				name: 'nmstatus', 
				mapping:'nmstatus'
			},{
				name: 'nmstdokter', 
				mapping:'nmstdokter'
			},{
				name: 'approval', 
				mapping:'approval'
			},{
				name: 'idstbayar', 
				mapping:'idstbayar'
			},{
				name: 'nmstbayar', 
				mapping:'nmstbayar'
			},{
				name: 'idjnspembayaran', 
				mapping:'idjnspembayaran'
			},{
				name: 'nmjnspembayaran', 
				mapping:'nmjnspembayaran'
			},{
				name: 'tglinput', 
				mapping:'tglinput'
			},{
				name: 'catatan', 
				mapping:'catatan'
			},{
				name: 'jumlah', 
				mapping:'jumlah'
			},{
				name: 'kdjurnal', 
				mapping:'kdjurnal'
			},{
				name: 'tglakhir', 
				mapping:'tglakhir'
			},{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			},{
				name: 'periode',
				mapping: 'periode'
			},{
				name: 'norek',
				mapping: 'norek'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'tgldikeluarkan',
				mapping: 'tgldikeluarkan'
			},{
				name: 'nmjnspembayaran',
				mapping: 'nmjnspembayaran'
			}]
		});
		
	var ds_stbayar = dm_stbayar();
	var ds_jpembayaran = dm_jpembayaran();
	var ds_app1 = dm_apphondok();
	var storeObj = {stbayar:ds_stbayar,jpembayaran:ds_jpembayaran,app1:ds_app1};
	
	var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
			header: '<center>No. Pembayaran</center>',
			width: 90,
			dataIndex: 'nohondok',
		},{
			header: '<center>Tgl. Pembayaran</center>',
			width: 90,
			//dataIndex: 'tglhondok',
			dataIndex: 'tgldikeluarkan',
			align:'center',	
			xtype: 'datecolumn',
			format: 'd/m/Y',
		},{
			header: '<center>Posting</center>',
			width: 90,
			dataIndex: 'kdjurnal',
			align:'center',	
			renderer: fnKeyPostingJurnal,
		},{
			header: '<center>Nama Dokter</center>',
			width: 200,
			dataIndex: 'nmdoktergelar',
		},{
			header: '<center>Spesialisasi</center>',
			width: 150,
			dataIndex: 'nmspesialisasi',
		},{
			header: '<center>Tgl. Awal</center>',
			width: 85,
			dataIndex: 'tglawal',
			xtype: 'datecolumn',
			format: 'd/m/Y',
		},{
			header: '<center>Tgl. Akhir</center>',
			width: 85,
			dataIndex: 'tglakhir',		
			xtype: 'datecolumn',
			format: 'd/m/Y',			
		},{
			header: '<center>Jenis Pembayaran</center>',
			width: 100,
			dataIndex: 'nmjnspembayaran',			
		},{
			header: '<center>Status Bayar</center>',
			width: 100,
			dataIndex: 'nmstbayar',			
		},{
			header: '<center>Jumlah</center>',
			width: 100,
			dataIndex: 'jumlah',
			align: 'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: '<center>User Input</center>',
			width: 100,
			dataIndex: 'nmlengkap',
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						var records = ds_hondok.getAt(rowIndex);
						honor_dokter_form(ds_hondok, records, storeObj);
						
						//return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						var records = ds_hondok.getAt(rowIndex);
						var url = BASE_URL + 'honor_controller/hapus';
						var params = new Object({
										nohondok	: records.data['nohondok']
									});
						RH.deleteGridRecord(url, params, grid );

                    }
                }]
        }]
    });
	
    var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});
	
	var paging = new Ext.PagingToolbar({
		pageSize: 18,
		store: ds_hondok,
		displayInfo: true,
		displayMsg: 'Data Pembelian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_hondok',
		store: ds_hondok,
    vw:vw,
		cm:cm,
		tbar:[{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			width: 100,
			handler: function() {
				//fPel();
				honor_dokter_form(ds_hondok,null,storeObj);
			}
		},{
			xtype: 'checkbox',
			id:'chb.pelayanan',
			margins: '3 10 0 5',
			listeners: {
				check : function(cb, value) {
					cari();
				}
			}						
		},{
			xtype: 'label', text: 'Tgl. Pembayaran', 
		},{
			xtype: 'datefield',
			id: 'df.tglawalan',
			format: 'd-m-Y',
			value: new Date(),
			width: 100,
			listeners:{
				select: function(field, newValue){
					cari();
				}
			}
				
		},{
			xtype: 'label',	text: 's.d',
		},{
			xtype: 'datefield',
			id: 'df.tglakhiran',
			format: 'd-m-Y',
			value: new Date(),
			width: 100,
			listeners:{
				select: function(field, newValue){
					cari();
				}
			}
		},{
							
	}],
		frame: true,
		height: 530,
		//autoHeight: true,
		autoWidth: false,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		autoScroll: true,
		bbar: paging,
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
				/* var header = grid_nya.getColumnModel().getColumnHeader(columnIdx).replace('<center>','').replace('</center>','');
				var index = grid_nya.getColumnModel().getDataIndex(columnIdx);
				alert(header + ' ' + index + ' ' + grid_nya.getStore().getTotalCount()); */
				var t = e.getTarget();
    
		    if (t.className == 'keyPostingJurnal')
		    {
  	      var obj             = ds_hondok.getAt(rowIdx);
		      var nohondok        = obj.get("nohondok");
		      var tgldikeluarkan  = obj.get("tgldikeluarkan");
		      var nmdoktergelar   = obj.get("nmdoktergelar");
		      var kdjurnal        = obj.get("kdjurnal");
		      
					var ds_jurnal_hondok = new Ext.data.JsonStore({
						proxy: new Ext.data.HttpProxy({
							url: BASE_URL + 'jurn_trans_hondok_controller/get_honor_dokter_jurnaling',
							method: 'POST'
						}),
						root: 'data', 
						totalProperty: 'results',
						autoLoad: true,
						baseParams: {
							nohondok: nohondok
						},
						fields:[{
						    name: 'idakun', mapping: 'idakun',
						}, {
						    name: 'kdakun', mapping: 'kdakun',
						}, {
						    name: 'nmakun', mapping: 'nmakun',
						}, {
						    name: 'noreff', mapping: 'noreff',
						}, {
						    name: 'debit', mapping: 'debit',
						}, {
						    name: 'kredit', mapping: 'kredit',
						}],
					});

					the_debit = 0;
					the_kredit = 0;
					ds_jurnal_hondok.load({
	          scope   : this,
	          callback: function(records, operation, success) {
	            //hitung total debit & kredit
	            total_debit = 0;
	            total_kredit = 0;
	            ds_jurnal_hondok.each(function (rec) { 
	              total_debit += parseFloat(rec.get('debit')); 
	              total_kredit += parseFloat(rec.get('kredit')); 
	            });
	            Ext.getCmp("info.total_debit").setValue(total_debit);            
	            Ext.getCmp("info.total_kredit").setValue(total_kredit);
	          }
	        });

				  var grid_detail_jurnal = new Ext.grid.GridPanel({
				    id: 'grid_detail_jurnal',
				    store: ds_jurnal_hondok,
				    view: new Ext.grid.GridView({emptyText: '< Tidak ada Data >'}),
				    frame: true,
				    loadMask: true,
				    height: 200,
				    layout: 'anchor',
				    bbar: [
				      '->',
				      {
				        xtype: 'numericfield',
				        thousandSeparator:',',
				        id: 'info.total_debit',
				        width:100,
				        readOnly:true,
				        value:0,
				        align:'right',
				        decimalPrecision:0,
				    },{
				        xtype: 'numericfield',
				        thousandSeparator:',',
				        id: 'info.total_kredit',
				        width:100,
				        readOnly:true,
				        value:0,
				        align:'right',
				        decimalPrecision:0,
				    }],
				    columns: [new Ext.grid.RowNumberer(),
				    {
				      header: '<center>Kode</center>',
				      width: 100,
				      dataIndex: 'kdakun',
				      sortable: true,
				      align:'center',
				    },{
				      header: '<center>Nama Akun</center>',
				      width: 180,
				      dataIndex: 'nmakun',
				      sortable: true,
				      align:'left',
				    },{
				      header: '<center>No.Reff</center>',
				      width: 100,
				      dataIndex: 'noreff',
				      sortable: true,
				      align:'center',
				    },{
				      header: '<center>Debit</center>',
				      width: 100,
				      dataIndex: 'debit',
				      sortable: true,
				      xtype: 'numbercolumn', format:'0,000', align:'right',
				    },{
				      header: '<center>Kredit</center>',
				      width: 100,
				      dataIndex: 'kredit',
				      sortable: true,
				      xtype: 'numbercolumn', format:'0,000', align:'right',
				    }]
				  });

					var posting_form = new Ext.form.FormPanel({
						xtype:'form',
						id: 'frm.posting',
						buttonAlign: 'left',
						autoScroll: true,
						labelWidth: 165, labelAlign: 'right',
						monitorValid: true,
						height: 250, width: 650,
						layout: {
							type: 'form',
							pack: 'center',
							align: 'center'
						},
						frame: true,
						tbar: [{
							text: 'Posting', iconCls:'silk-save', id: 'btn.posting', style: 'marginLeft: 5px',
							handler: function() {
                  var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
                  var nohondok_posting = nohondok;
                  var tgldikeluarkan_posting = tgldikeluarkan;
                  var nmdoktergelar = nmdoktergelar;
                  var nominal_jurnal = 0; //nominal debit
                  var nominal_jurnal_kredit = 0; //nominal kredit
                  var post_grid ='';
                  var count= 1;
                  var endpar = ';';
                    
                  grid_detail_jurnal.getStore().each(function(rec){ // ambil seluruh grid
                    var rowData = rec.data; 
                    if (count == grid_detail_jurnal.getStore().getCount()) {
                      endpar = ''
                    }
                    post_grid += rowData['idakun'] + '^' + 
                           rowData['kdakun'] + '^' + 
                           rowData['nmakun']  + '^' + 
                           rowData['noreff']  + '^' + 
                           rowData['debit'] + '^' + 
                           rowData['kredit']  + '^' +
                        endpar;
                    
                     nominal_jurnal += rowData['debit'];
                     nominal_jurnal_kredit += rowData['kredit'];

                    count = count+1;
                  });

                  if(nohondok_posting == '' || tgldikeluarkan_posting == ''){
                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
                  }
                  else if(nominal_jurnal != nominal_jurnal_kredit){
                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
                  }
                  else{
                    
                    Ext.Ajax.request({
                      url: BASE_URL + 'jurn_trans_hondok_controller/posting_honor_dokter',
                      params: {
                        nohondok_posting        : nohondok_posting,
                        tgldikeluarkan_posting  : tgldikeluarkan_posting,
                        nmdoktergelar           : nmdoktergelar,
                        nominal_jurnal          : nominal_jurnal,
                        post_grid               : post_grid,
                        userid                  : USERID,
                      },
                      success: function(response){
                        waitmsg.hide();

                        obj = Ext.util.JSON.decode(response.responseText);
                        if(obj.success === true){
                          Ext.getCmp('btn.posting').disable();
                        }else{
                          Ext.getCmp('btn.posting').enable();  
                        }

                        Ext.MessageBox.alert('Informasi', obj.message);
                        ds_hondok.reload();
                        wPostingForm.close();
                      }
                    });
                   
                  }

                }
						},{
							text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
							handler: function() {
								wPostingForm.close();
							}
						}],	
						items: [
							grid_detail_jurnal,
						]
					});

					if(kdjurnal == null)
	        {
	          Ext.getCmp('btn.posting').enable();
	        }else{
	          Ext.getCmp('btn.posting').disable();
	        }
	
					var wPostingForm = new Ext.Window({
						title: 'Posting Honor Dokter',
						modal: true, closable:false,
						items: [posting_form]
					});
				
					wPostingForm.show();

		    }
			},
		}
	});	
	                    
	var form_hondok = new Ext.form.FormPanel({
		id: 'form_hondok',
		title: 'Daftar Honor Dokter',
		bodyStyle: 'padding: 5px',
		border: true,  
		autoScroll: true,
		layout: 'form',
		items: [grid_nya],
		listeners: {
			afterrender: function () {
			
			},
			beforerender: function () {				
				
			}
		}
	});
	
	SET_PAGE_CONTENT(form_hondok);
	
	function cari(){
		//if (Ext.getCmp('chb.pelayanan').getValue()) {
			ds_hondok.reload({
				params: { 
					cbx: Ext.getCmp('chb.pelayanan').getValue(),
					tglawalan: Ext.getCmp('df.tglawalan').getValue(),
					tglakhiran: Ext.getCmp('df.tglakhiran').getValue(),
				}
			});	
		//}
	}


  function fnKeyPostingJurnal(value, value2, value3){
  	
  	var val3 = value3.data;
  	var nohondok = val3.nohondok;
  	var idstbayar = val3.idstbayar;
  	var kdjurnal = val3.kdjurnal;

  	if(idstbayar == 1){
  		return 'belum dibayarkan';
  	}

  	if(kdjurnal != null){
  		return 'sudah posting';
  	}

  	Ext.QuickTips.init();
    return '<div class="keyPostingJurnal" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">posting</div>';
  }

}