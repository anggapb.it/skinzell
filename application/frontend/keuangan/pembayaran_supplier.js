//===================LIST PEMBAYARAN SUPPLIER===========================//{
function pembayaran_supplier() {
	var tgl_byr = new Date();
	var ds_search = new Ext.data.JsonStore({ //static data
		autoDestroy: true,
		fields: [ 'field', 'value' ]
	});
	
	var ds_status = new Ext.data.JsonStore({ //static data
		autoDestroy: true,
		fields: [ 'field', 'value' ],
		data: [
		{	"field":"Semua","value":"Semua"},
		{	"field":"Lunas","value":"Lunas"},
		{	"field":"Belum Bayar","value":"Belum Bayar"},
		{	"field":"Belum Lunas","value":"Belum Lunas"},
		{	"field":"N/A","value":"N/A"},
		]
	});
	
	var ds_app1 = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'tagihan_penjamin_controller/appkeu',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmset', mapping:'nmset'
		}]
	});	
	
	var ds_jpembayaran = dm_jpembayaran();
	var storesObj = {app1:ds_app1,jpembayaran:ds_jpembayaran};
	
	var ds_grid_po_supp = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pembayaran_supplier_controller/get_po_supp',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			baseParams: {
				cbxbeli : true,
				tglbeli1 : Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglbeli2 : Ext.util.Format.date(new Date(), 'Y-m-d'),
			},
			autoLoad: false,
			fields: [{
				name: 'nopo',
				mapping: 'nopo'
			},{
				name: 'tglpo',
				mapping: 'tglpo'
			},{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'tglpp',
				mapping: 'tglpp'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'npwp',
				mapping: 'npwp'
			},{
				name: 'bpb',
				mapping: 'bpb'
			},{
				name: 'idjnspembayaran',
				mapping: 'idjnspembayaran'
			},{
				name: 'kdjnspembayaran',
				mapping: 'kdjnspembayaran'
			},{
				name: 'nmjnspembayaran',
				mapping: 'nmjnspembayaran'
			},{
				name: 'idsypembayaran',
				mapping: 'idsypembayaran'
			},{
				name: 'kdsypembayaran',
				mapping: 'kdsypembayaran'
			},{
				name: 'nmsypembayaran',
				mapping: 'nmsypembayaran'
			},{
				name: 'tgljatuhtempo',
				mapping: 'tgljatuhtempo'
			},{
				name: 'bli',
				mapping: 'bli'
			},{
				name: 'byr',
				mapping: 'byr'
			},{
				name: 'selisih',
				mapping: 'selisih'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nobayar',
				mapping: 'nobayar'
			},{
				name: 'stlunas',
				mapping: 'stlunas'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'nmstpo',
				mapping: 'nmstpo'
			},{
				name: 'totdiskon',
				mapping: 'totdiskon'
			},{
				name: 'totppn',
				mapping: 'totppn'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'approval1',
				mapping: 'approval1'
			},{
				name: 'nmjnspp',
				mapping: 'nmjnspp'
			},{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'tglpp',
				mapping: 'tglpp'
			},{
				name: 'ketpo',
				mapping: 'ketpo'
			},{
				name: 'tglbayar',
				mapping: 'tglbayar'
			},{
				name: 'nobayar',
				mapping: 'nobayar'
			},{
				name: 'kdjurnal',
				mapping: 'kdjurnal'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'idstkontrabon',
				mapping: 'idstkontrabon'
			},{
				name: 'nmstkontrabon',
				mapping: 'nmstkontrabon'
			},{
				name: 'jumlahret',
				mapping: 'jumlahret'
			},{
				name: 'totppnret',
				mapping: 'totppnret'
			},{
				name: 'bliret',
				mapping: 'bliret'
			},{
				name: 'selisihret',
				mapping: 'selisihret'
			},{
				name: 'stlunasret',
				mapping: 'stlunasret'
			}]
		});

	ds_grid_po_supp.on('load', function(){
		var sumjumlah = 0,sumdiskon = 0,sumppn = 0,bli = 0,byr = 0,selisih = 0;
		ds_grid_po_supp.each(function(rec){
			sumjumlah += parseFloat(rec.get('jumlahret')); 
			sumdiskon += parseFloat(rec.get('totdiskon')); 
			sumppn += parseFloat(rec.get('totppnret')); 
			bli += parseFloat(rec.get('bliret')); 
			byr += parseFloat(rec.get('byr')); 
			selisih += parseFloat(rec.get('selisihret')); 
		});
		Ext.getCmp("tf.sumjumlah").setValue(sumjumlah);
		Ext.getCmp("tf.sumdiskon").setValue(sumdiskon);
		Ext.getCmp("tf.sumppn").setValue(sumppn);
		Ext.getCmp("tf.bli").setValue(bli);
		Ext.getCmp("tf.byr").setValue(byr);
		Ext.getCmp("tf.selisih").setValue(selisih);
	});
	
	var sm = new Ext.grid.RowSelectionModel({
		singleSelect: true,
		listeners: {}
	});
	
	var cm = new Ext.grid.ColumnModel({
	  // specify any defaults for each column
	  defaults: {
      sortable: true // columns are not sortable by default           
	  },
    columns: [
			new Ext.grid.RowNumberer(),
			{
        xtype: 'actioncolumn',
        width: 50,
				header: 'Bayar',
				align:'center',
        items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
          icon : 'resources/img/icons/fam/money.png',
					tooltip: 'Bayar',
          handler: function(grid, rowIndex) {
						var records = ds_grid_po_supp.getAt(rowIndex);
						pembayaran_supplier_form(ds_grid_po_supp, records, storesObj);
          }
        }]
      },
			{
				header: '<center>Status Bayar</center>',
				width: 93,
				dataIndex: 'stlunasret',
				align: 'center',
			},{
				header: '<center>No. Pembelian</center>',
				width: 87,
				dataIndex: 'nopo',
				align:'center',	
			},{
				header: '<center>Posting</center>',
				width: 87,
				dataIndex: 'kdjurnal',
				align:'center',	
	      renderer: fnkeyPostingJurnal
			},{
				header: '<center>Tgl. Pembelian</center>',
				width: 81,
				dataIndex: 'tglpo',
				align:'center',	
				xtype:'datecolumn',
				format:'d/m/Y'
			},{
				header: '<center>Tgl. Pembayaran</center>',
				width: 90,
				dataIndex: 'tglbayar',
				align:'center',	
				xtype:'datecolumn',
				format:'d/m/Y'
			},{
				header: '<center>Supplier</center>',
				width: 230,
				dataIndex: 'nmsupplier',
			},{
				header: '<center>No. Reff/SJ<br>/No. Faktur</center>',
				width: 70,
				dataIndex: 'bpb',
				align:'center',	
			},{
				header: '<center>Jenis<br>Pembayaran</center>',
				width: 90,
				dataIndex: 'nmjnspembayaran',
				align:'center',				
			},{
				header: '<center>Syarat<br>Pembayaran</center>',
				width: 85,
				dataIndex: 'nmsypembayaran',
				align:'center',				
			},{
				header: '<center>Kontra Bon</center>',
				width: 70,
				dataIndex: 'nmstkontrabon',
				align:'left',	
			},{
				header: '<center>Tgl. Jatuh<br>Tempo</center>',
				width: 72,
				dataIndex: 'tgljatuhtempo',
				align:'center',	
				xtype:'datecolumn',
				format:'d/m/Y'
			},{
				header: '<center>Status<br>Pesanan</center>',
				width: 77,
				dataIndex: 'nmstpo',
				align:'center',	
			},{
				header: '<center>Jumlah</center>',
				width: 100,
				dataIndex: 'jumlahret',
				align:'right',
				xtype: 'numbercolumn', format:'0,000',
			},{
				header: '<center>Total<br>Diskon</center>',
				width: 63,
				dataIndex: 'totdiskon',
				align:'right',
				xtype: 'numbercolumn', format:'0,000',
			},{
				header: '<center>Total<br>PPN (10%)</center>',
				width: 68,
				dataIndex: 'totppnret',
				align:'center',
				xtype: 'numbercolumn', format:'0,000',
			},{
				header: '<center>Total<br>Tagihan</center>',
				width: 100,
				dataIndex: 'bliret',
				align:'right',
				xtype: 'numbercolumn',// format:'0,000',
			},{
				header: '<center>Sudah<br>Dibayar</center>',
				width: 100,
				dataIndex: 'byr',
				align:'right',
				xtype: 'numbercolumn',// format:'0,000',
			},{
				header: '<center>Sisa<br>Tagihan</center>',
				width: 100,
				dataIndex: 'selisihret',
				align:'right',
				xtype: 'numbercolumn',// format:'0,000',
			},{
				header: '<center>User<br>Input</center>',
				width: 100,
				dataIndex: 'nmlengkap',
			},{
				header: '<center>Approval</center>',
				width: 77,
				dataIndex: 'approval1',
			},{
				header: '<center>Jenis Pesanan</center>',
				width: 119,
				dataIndex: 'nmjnspp',
			},{
				header: '<center>No.<br>Pesanan</center>',
				width: 84,
				dataIndex: 'nopp',
				align:'center',	
			},{
				header: '<center>Tgl.<br>Pesanan</center>',
				width: 71,
				dataIndex: 'tglpp',
				align:'center',	
			},{
				header: '<center>Keterangan</center>',
				width: 100,
				dataIndex: 'ketpo',
			}]
    });
	
    var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});
	
	var paging = new Ext.PagingToolbar({
		pageSize: 25,
		store: ds_grid_po_supp,
		displayInfo: true,
		displayMsg: 'Data Pembelian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya_po_supp = new Ext.grid.EditorGridPanel({
		id: 'grid_posupp',
		store: ds_grid_po_supp,
        vw:vw,
		cm:cm,
		sm:sm,
		tbar: [
    {
					text: 'Eksport Excel',
					id: 'btn_save_excel',
					iconCls: 'silk-save',
					width: 100, disabled: true,
					handler: function() {
						
						var cbxstbayar= Ext.getCmp('cbxstbayar').getValue();
						var cbxsearch= Ext.getCmp('cbxsearch').getValue();
						var cbxbeli= Ext.getCmp('cbxbeli').getValue();
						var cbxpembayaran= Ext.getCmp('cbxpembayaran').getValue();
						var cbxjthtempo= Ext.getCmp('cbxjthtempo').getValue();

						var status= Ext.getCmp('cb.status').getValue();
						var key= Ext.getCmp('cb.search').getValue();
						var value= (Ext.getCmp('tf.search').getValue()) ? Ext.getCmp('tf.search').getValue():'-';
						var tglbeli1= Ext.getCmp('df.tglbeli1').getValue().format('Y-m-d');
						var tglbeli2= Ext.getCmp('df.tglbeli2').getValue().format('Y-m-d');
						var jthtempo1= Ext.getCmp('df.jthtempo1').getValue().format('Y-m-d');
						var jthtempo2= Ext.getCmp('df.jthtempo2').getValue().format('Y-m-d');
						var pembayaran1= Ext.getCmp('df.pembayaran1').getValue().format('Y-m-d');
						var pembayaran2= Ext.getCmp('df.pembayaran2').getValue().format('Y-m-d');

						window.location = BASE_URL + 'print/print_pembayaran_supplier/export_pembayaran_supplier/'
								+cbxstbayar+'/'+cbxsearch+'/'+cbxbeli+'/'+cbxjthtempo+'/'+cbxpembayaran+'/'+status+'/'+key+'/'+value+'/'
								+tglbeli1+'/'+tglbeli2+'/'+jthtempo1+'/'+jthtempo2+'/'+pembayaran1+'/'+pembayaran2;
					}
		},
    {
					text: 'Rekap Pembelian Excel',
					id: 'btn_rekap_pembelian_excel',
					iconCls: 'silk-save',
					width: 100, disabled: true,
					handler: function() {
						
						var cbxstbayar= Ext.getCmp('cbxstbayar').getValue();
						var cbxsearch= Ext.getCmp('cbxsearch').getValue();
						var cbxbeli= Ext.getCmp('cbxbeli').getValue();
						var cbxpembayaran= Ext.getCmp('cbxpembayaran').getValue();
						var cbxjthtempo= Ext.getCmp('cbxjthtempo').getValue();

						var status= Ext.getCmp('cb.status').getValue();
						var key= Ext.getCmp('cb.search').getValue();
						var value= (Ext.getCmp('tf.search').getValue()) ? Ext.getCmp('tf.search').getValue():'-';
						var tglbeli1= Ext.getCmp('df.tglbeli1').getValue().format('Y-m-d');
						var tglbeli2= Ext.getCmp('df.tglbeli2').getValue().format('Y-m-d');
						var jthtempo1= Ext.getCmp('df.jthtempo1').getValue().format('Y-m-d');
						var jthtempo2= Ext.getCmp('df.jthtempo2').getValue().format('Y-m-d');
						var pembayaran1= Ext.getCmp('df.pembayaran1').getValue().format('Y-m-d');
						var pembayaran2= Ext.getCmp('df.pembayaran2').getValue().format('Y-m-d');

						RH.ShowReport(BASE_URL + 'print/print_pembayaran_supplier/rekap_pembelian_excel/'
								+cbxstbayar+'/'+cbxsearch+'/'+cbxbeli+'/'+cbxjthtempo+'/'+cbxpembayaran+'/'+status+'/'+key+'/'+value+'/'
								+tglbeli1+'/'+tglbeli2+'/'+jthtempo1+'/'+jthtempo2+'/'+pembayaran1+'/'+pembayaran2);
					}
		},{
					text: 'Detail Rekap Pembelian Excel',
					id: 'btn_detail_rekap_pembelian_excel',
					iconCls: 'silk-save',
					width: 100, disabled: true,
					handler: function() {
						
						var cbxstbayar= Ext.getCmp('cbxstbayar').getValue();
						var cbxsearch= Ext.getCmp('cbxsearch').getValue();
						var cbxbeli= Ext.getCmp('cbxbeli').getValue();
						var cbxpembayaran= Ext.getCmp('cbxpembayaran').getValue();
						var cbxjthtempo= Ext.getCmp('cbxjthtempo').getValue();

						var status= Ext.getCmp('cb.status').getValue();
						var key= Ext.getCmp('cb.search').getValue();
						var value= (Ext.getCmp('tf.search').getValue()) ? Ext.getCmp('tf.search').getValue():'-';
						var tglbeli1= Ext.getCmp('df.tglbeli1').getValue().format('Y-m-d');
						var tglbeli2= Ext.getCmp('df.tglbeli2').getValue().format('Y-m-d');
						var jthtempo1= Ext.getCmp('df.jthtempo1').getValue().format('Y-m-d');
						var jthtempo2= Ext.getCmp('df.jthtempo2').getValue().format('Y-m-d');
						var pembayaran1= Ext.getCmp('df.pembayaran1').getValue().format('Y-m-d');
						var pembayaran2= Ext.getCmp('df.pembayaran2').getValue().format('Y-m-d');

						RH.ShowReport(BASE_URL + 'print/print_pembayaran_supplier/detail_rekap_pembelian_excel/'
								+cbxstbayar+'/'+cbxsearch+'/'+cbxbeli+'/'+cbxjthtempo+'/'+cbxpembayaran+'/'+status+'/'+key+'/'+value+'/'
								+tglbeli1+'/'+tglbeli2+'/'+jthtempo1+'/'+jthtempo2+'/'+pembayaran1+'/'+pembayaran2);
					}
		},{
					text: 'Rekap Pembelian',
					id: 'btn_rekap_pembelian',
					iconCls: 'silk-save',
					width: 100, disabled: true,
					handler: function() {
						
						var cbxstbayar= Ext.getCmp('cbxstbayar').getValue();
						var cbxsearch= Ext.getCmp('cbxsearch').getValue();
						var cbxbeli= Ext.getCmp('cbxbeli').getValue();
						var cbxpembayaran= Ext.getCmp('cbxpembayaran').getValue();
						var cbxjthtempo= Ext.getCmp('cbxjthtempo').getValue();

						var status= Ext.getCmp('cb.status').getValue();
						var key= Ext.getCmp('cb.search').getValue();
						var value= (Ext.getCmp('tf.search').getValue()) ? Ext.getCmp('tf.search').getValue():'-';
						var tglbeli1= Ext.getCmp('df.tglbeli1').getValue().format('Y-m-d');
						var tglbeli2= Ext.getCmp('df.tglbeli2').getValue().format('Y-m-d');
						var jthtempo1= Ext.getCmp('df.jthtempo1').getValue().format('Y-m-d');
						var jthtempo2= Ext.getCmp('df.jthtempo2').getValue().format('Y-m-d');
						var pembayaran1= Ext.getCmp('df.pembayaran1').getValue().format('Y-m-d');
						var pembayaran2= Ext.getCmp('df.pembayaran2').getValue().format('Y-m-d');

						RH.ShowReport(BASE_URL + 'print/print_pembayaran_supplier/rekap_pembelian/'
								+cbxstbayar+'/'+cbxsearch+'/'+cbxbeli+'/'+cbxjthtempo+'/'+cbxpembayaran+'/'+status+'/'+key+'/'+value+'/'
								+tglbeli1+'/'+tglbeli2+'/'+jthtempo1+'/'+jthtempo2+'/'+pembayaran1+'/'+pembayaran2);
					}
		},{
					text: 'Detail Rekap Pembelian',
					id: 'btn_detail_rekap_pembelian',
					iconCls: 'silk-save',
					width: 100, disabled: true,
					handler: function() {
						
						var cbxstbayar= Ext.getCmp('cbxstbayar').getValue();
						var cbxsearch= Ext.getCmp('cbxsearch').getValue();
						var cbxbeli= Ext.getCmp('cbxbeli').getValue();
						var cbxpembayaran= Ext.getCmp('cbxpembayaran').getValue();
						var cbxjthtempo= Ext.getCmp('cbxjthtempo').getValue();

						var status= Ext.getCmp('cb.status').getValue();
						var key= Ext.getCmp('cb.search').getValue();
						var value= (Ext.getCmp('tf.search').getValue()) ? Ext.getCmp('tf.search').getValue():'-';
						var tglbeli1= Ext.getCmp('df.tglbeli1').getValue().format('Y-m-d');
						var tglbeli2= Ext.getCmp('df.tglbeli2').getValue().format('Y-m-d');
						var jthtempo1= Ext.getCmp('df.jthtempo1').getValue().format('Y-m-d');
						var jthtempo2= Ext.getCmp('df.jthtempo2').getValue().format('Y-m-d');
						var pembayaran1= Ext.getCmp('df.pembayaran1').getValue().format('Y-m-d');
						var pembayaran2= Ext.getCmp('df.pembayaran2').getValue().format('Y-m-d');

						RH.ShowReport(BASE_URL + 'print/print_pembayaran_supplier/detail_rekap_pembelian/'
								+cbxstbayar+'/'+cbxsearch+'/'+cbxbeli+'/'+cbxjthtempo+'/'+cbxpembayaran+'/'+status+'/'+key+'/'+value+'/'
								+tglbeli1+'/'+tglbeli2+'/'+jthtempo1+'/'+jthtempo2+'/'+pembayaran1+'/'+pembayaran2);
					}
		}],
		frame: true,
		height: 450,
		autoScroll: true,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		bbar: [{
			text: '<span style="margin-left:0px;font-size:12px;">Jumlah:</span>',
		},{
			xtype: 'numericfield',
			id: 'tf.sumjumlah',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true
		},{
			text: '<span style="margin-left:15px;font-size:12px;">Total Diskon:</span>',
		},{
			xtype: 'numericfield',
			id: 'tf.sumdiskon',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true
		},{
			text: '<span style="margin-left:15px;font-size:12px;">Total PPN (10%):</span>',
		},{
			xtype: 'numericfield',
			id: 'tf.sumppn',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true
		},{
			text: '<span style="margin-left:15px;font-size:12px;">Total Tagihan:</span>',
		},{
			xtype: 'numericfield',
			id: 'tf.bli',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true
		},{
			text: '<span style="margin-left:15px;font-size:12px;">Sudah Di Bayar:</span>',
		},{
			xtype: 'numericfield',
			id: 'tf.byr',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true
		},{
			text: '<span style="margin-left:15px;font-size:12px;">Sisa Tagihan:</span>',
		},{
			xtype: 'numericfield',
			id: 'tf.selisih',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true
		}],
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {

				//showPostingForm
		    var t = e.getTarget();
    
		    if (t.className == 'showPostingForm')
		    {
    	      var obj     = ds_grid_po_supp.getAt(rowIdx);
			      var nobayar  = obj.get("nobayar");
			      var tglbayar  = obj.get("tglbayar");
			      var nmsupplier  = obj.get("nmsupplier");
			      var kdjurnal  = obj.get("kdjurnal");
		    	  var ds_detail_pembayaran = dm_jurnsupplier_bayardet();
					  var ds_detail_jurnal = dm_jurnsupplier_bayar_jurnaling();

					  //Ext.getCmp("info.nmsupplier").setValue(nmsupplier);
				        
      			ds_detail_jurnal.setBaseParam('nobayar',nobayar);
      			ds_detail_jurnal.load({
				      scope   : this,
				      callback: function(records, operation, success) {
				        total_debit = 0;
				        total_kredit = 0;
				        ds_detail_jurnal.each(function (rec) { 
				          total_debit += parseFloat(rec.get('debit')); 
				          total_kredit += parseFloat(rec.get('kredit')); 
				        });

				        Ext.getCmp("info.totaldebit").setValue(total_debit);
				        Ext.getCmp("info.totalkredit").setValue(total_kredit);

				      }
				    });
		        ds_detail_pembayaran.setBaseParam('nobayar', nobayar);


					  var cm_detail_pembayaran = new Ext.grid.ColumnModel({
					    columns: [new Ext.grid.RowNumberer(),
					    {
					      header: '<center>Jenis Pembayaran</center>',
					      width: 105,
					      dataIndex: 'nmjnspembayaran',
					      align:'center',
					    },{
					      header: '<center>No. Kartu/No. Reff</center>',
					      width: 140,
					      dataIndex: 'nokartu',
					      align:'center',
					    },{
					      header: '<center>Akun</center>',
					      width: 140,
					      dataIndex: 'nmakun',
					      align:'center',
					    },{
					      header: '<center>Nominal</center>',
					      width: 90,
					      dataIndex: 'nominal',
					      xtype: 'numbercolumn', format:'0,000', align:'right',
					    }]
					  });

					  var cm_detail_jurnal = new Ext.grid.ColumnModel({
					    columns: [new Ext.grid.RowNumberer(),
					    {
					      header: '<center>Kode</center>',
					      width: 80,
					      dataIndex: 'kdakun',
					      sortable: true,
					      align:'center',
					    },{
					      header: '<center>Nama Akun</center>',
					      width: 130,
					      dataIndex: 'nmakun',
					      sortable: true,
					      align:'left',
					    },{
					      header: '<center>No.Reff</center>',
					      width: 90,
					      dataIndex: 'noreff',
					      sortable: true,
					      align:'center',
					    },{
					      header: '<center>Debit</center>',
					      width: 90,
					      dataIndex: 'debit',
					      sortable: true,
					      xtype: 'numbercolumn', format:'0,000', align:'right',
					    },{
					      header: '<center>Kredit</center>',
					      width: 90,
					      dataIndex: 'kredit',
					      sortable: true,
					      xtype: 'numbercolumn', format:'0,000', align:'right',
					    }]
					  });

					  var grid_detail_pembayaran = new Ext.grid.GridPanel({
					    id: 'grid_detail_pembayaran',
					    store: ds_detail_pembayaran,
					    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
					    cm: cm_detail_pembayaran,
					    frame: true,
					    loadMask: true,
					    height: 150,
					    layout: 'anchor',
					    style: 'padding-bottom:10px',
					  });

					  var grid_detail_jurnal = new Ext.grid.GridPanel({
					    id: 'grid_detail_jurnal',
					    store: ds_detail_jurnal,
					    title: 'Detail Jurnal',
					    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
					    cm: cm_detail_jurnal,
					    frame: true,
					    loadMask: true,
					    height: 200,
					    layout: 'anchor',
					  });

						var posting_form = new Ext.form.FormPanel({
							xtype:'form',
							id: 'frm.posting',
							buttonAlign: 'left',
							autoScroll: true,
							labelWidth: 165, labelAlign: 'right',
							monitorValid: true,
							height: 400, width: 650,
							layout: {
								type: 'form',
								pack: 'center',
								align: 'center'
							},
							frame: true,
							tbar: [{
								text: 'Posting', iconCls:'silk-save', id: 'btn.posting', style: 'marginLeft: 5px',
								handler: function() {
	                  var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
	                  var nobayar_posting = nobayar;
	                  var tglbayar_posting = tglbayar;
	                  var post_grid ='';
                    var count= 1;
                    var endpar = ';';
	                  var nominal_jurnal = Ext.getCmp("info.totaldebit").getValue();
	                  var nominal_jurnal_kredit = Ext.getCmp("info.totalkredit").getValue();
	                  var nmsupplier = obj.get("nmsupplier");
  
                    grid_detail_jurnal.getStore().each(function(rec){ // ambil seluruh grid
                      var rowData = rec.data; 
                      if (count == grid_detail_jurnal.getStore().getCount()) {
                        endpar = ''
                      }
                      post_grid += rowData['idakun'] + '^' + 
                             rowData['kdakun'] + '^' + 
                             rowData['nmakun']  + '^' + 
                             rowData['noreff']  + '^' + 
                             rowData['debit'] + '^' + 
                             rowData['kredit']  + '^' +
                          endpar;
                      
                      count = count+1;
                    });

	                  if(nobayar_posting == '' || tglbayar_posting == ''){
	                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
	                  }
	                  else if(nominal_jurnal != nominal_jurnal_kredit){
	                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
	                  }
	                  else{
	                    
	                    Ext.Ajax.request({
	                      url: BASE_URL + 'jurn_transaksi_supplier_controller/posting_jurnal_supplier_bayar',
	                      params: {
	                        nobayar_posting    : nobayar_posting,
	                        tglbayar_posting   : tglbayar_posting,
	                        nmsupplier      : nmsupplier,
	                        nominal_jurnal  : nominal_jurnal,
	                        post_grid       : post_grid,
	                        userid          : USERID,
	                      },
	                      success: function(response){
	                        waitmsg.hide();

	                        obj = Ext.util.JSON.decode(response.responseText);
	                        if(obj.success === true){
	                          Ext.getCmp('btn.posting').disable();
	                        }else{
	                          Ext.getCmp('btn.posting').enable();  
	                        }

	                        Ext.MessageBox.alert('Informasi', obj.message);
	                        ds_grid_po_supp.reload();
	                        wPostingForm.close();
	                      }
	                    });
	                   
	                  }

	                }
							},{
								text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
								handler: function() {
									wPostingForm.close();
								}
							}],	
							items: [
								grid_detail_pembayaran, 
								grid_detail_jurnal,
								{
									xtype: 'numericfield',
					        thousandSeparator:',',
					        id: 'info.totaldebit',
					        width:100,
					        readOnly:true,
					        value:'0',
					        hidden: true,
					        align:'right',
					        decimalPrecision:0,
								},
								{
									xtype: 'numericfield',
					        thousandSeparator:',',
					        id: 'info.totalkredit',
					        width:100,
					        readOnly:true,
					        value:'0',
					        hidden: true,
					        align:'right',
					        decimalPrecision:0,
								}
							]
						});

						if(kdjurnal == null)
		        {
		          Ext.getCmp('btn.posting').enable();
		        }else{
		          Ext.getCmp('btn.posting').disable();
		        }
		
						var wPostingForm = new Ext.Window({
							title: 'Posting Pembayaran Supplier',
							modal: true, closable:false,
							items: [posting_form]
						});
					
						//setJurnalUmumForm(isUpdate, record);
						wPostingForm.show();
				}

				/* var header = grid_nya_po_supp.getColumnModel().getColumnHeader(columnIdx).replace('<center>','').replace('</center>','');
				var index = grid_nya_po_supp.getColumnModel().getDataIndex(columnIdx);
				alert(header + ' ' + index + ' ' + grid_nya_po_supp.getStore().getTotalCount()); */				
			},
		}
	});	
	                    
	var form_po_supplier = new Ext.form.FormPanel({
		id: 'form_po_supplier',
		title: 'Pembayaran Supplier',
		bodyStyle: 'padding: 5px',
		border: true,  
		autoScroll: true,
		layout: 'form',
		items: [{
					xtype: 'fieldset',
					id: 'fs.cariposupp', layout: 'column',
					defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[{
							layout: 'form', columnWidth: 0.50, border: false,  
							items: [{
									xtype: 'compositefield', fieldLabel: 'Status Bayar',
									items:[{
												xtype:'checkbox',  
												id: 'cbxstbayar',
												listeners: {
													check : function(cb, value) {
														//if(value){
															//cariPo();
														//}
													}
												}
											},{
												xtype: 'combo', fieldLabel: '', width: 150,
												id:'cb.status', autoWidth: true, store: ds_status,
												valueField: 'value', displayField: 'field', editable: false,allowBlank: true,
												triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
												emptyText:'Pilih....',
												listeners: {
													afterrender: function () {
														Ext.getCmp('cb.status').setValue("Semua");
													},
													select: function () {
														//cariPo();
													}
												}
											}]
								},{
									xtype: 'compositefield', fieldLabel: 'Cari Berdasarkan',
									items:[{
												xtype:'checkbox',  
												id: 'cbxsearch',
												listeners: {
													check : function(cb, value) {
														//cariPo();
														if(!value){
															Ext.getCmp('tf.search').setValue(null);
														}
													}
												}
											},{
												xtype: 'combo', fieldLabel: '', width: 150,
												id:'cb.search', autoWidth: true, store: ds_search,
												valueField: 'value', displayField: 'field', editable: false,allowBlank: true,
												triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
												emptyText:'Pilih....',
												listeners: {
													afterrender: function () {
														var a, rec, header, index;
															for (a=2;a<=23;a++) {
																header = grid_nya_po_supp.getColumnModel().getColumnHeader(a).replace('<center>','').replace('</center>','').replace('<br>',' ');
																index = grid_nya_po_supp.getColumnModel().getDataIndex(a);
															
																rec = new ds_search.recordType({field:header, value:index});
																rec.commit();
																ds_search.add(rec);
																
																if (a==2) { Ext.getCmp('cb.search').setValue(index) }
															}
													}
												}
											},{
												xtype: 'textfield',
												id: 'tf.search',
												width: 150,
												allowBlank: true,
												listeners: {
													specialkey: function(f,e){
														if (e.getKey() == e.ENTER) {
															//cariPo();
														}
													}
												}
											}/* ,{
												xtype: 'button',
												id: 'bsearch',
												align:'left',
												text: 'Cari',
												iconCls: 'silk-find', //align : '0 0 0 15',
												handler: function() {
													cariPo();
												}
											} */]
								},{
									xtype: 'compositefield', fieldLabel: '',
									items:[{
												xtype:'checkbox',  
												id: 'cbxjthtempo132',
												hidden: true,
												listeners: {
													check : function(cb, value) {
														//if(value){
															//cariPo();
														//}
													}
												}
										},{
											xtype: 'button',
											id: 'bsearch',
											//align:'left',
											text: 'Cari',
											iconCls: 'silk-find', margins: '0 0 0 25',
											handler: function() {
												cariPo();
											}
										}]
								}]
						},{
							layout: 'form', columnWidth: 0.50, border: false,  
							items: [{
									xtype: 'compositefield', fieldLabel: 'Tgl. Pembelian',
									items:[{
												xtype:'checkbox',  
												id: 'cbxbeli',
												//checked: true,
												//disabled: true,
												listeners: {
													check : function(cb, value) {
														//if(value){
															//cariPo();
														//}
													}
												}
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.tglbeli1',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													//cariPo();
												}
											}
										},{
											xtype: 'label', text: 's.d',
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.tglbeli2',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													//cariPo();
												}
											}
										}]
								},{
									xtype: 'compositefield', fieldLabel: 'Tgl. Pembayaran',
									items:[{
												xtype:'checkbox',  
												id: 'cbxpembayaran',
												listeners: {
													check : function(cb, value) {
															//cariPo();
													}
												}
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.pembayaran1',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													//cariPo();
												}
											}
										},{
											xtype: 'label', text: 's.d',
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.pembayaran2',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													//cariPo();
												}
											}
										}]
								},{
									xtype: 'compositefield', fieldLabel: 'Tgl. Jatuh Tempo',
									items:[{
												xtype:'checkbox',  
												id: 'cbxjthtempo',
												listeners: {
													check : function(cb, value) {
														//if(value){
															//cariPo();
														//}
													}
												}
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.jthtempo1',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													//cariPo();
												}
											}
										},{
											xtype: 'label', text: 's.d',
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.jthtempo2',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													//cariPo();
												}
											}
										}]
								}]
						}]
				},
				{
					xtype: 'fieldset', title:'Daftar Pembelian',
					id: 'fs.gridposupp',
					items:[grid_nya_po_supp]
				}
		],
		listeners: {
			afterrender: function () {
				
			},
			beforerender: function () {				
				
			}
		}
	});
	
	SET_PAGE_CONTENT(form_po_supplier);
	
	function cariPo(){
		var cbxstbayar = Ext.getCmp('cbxstbayar').getValue();
		var cbxsearch = Ext.getCmp('cbxsearch').getValue();
		var cbxbeli = Ext.getCmp('cbxbeli').getValue();
		var cbxjthtempo = Ext.getCmp('cbxjthtempo').getValue();
		var cbxpembayaran = Ext.getCmp('cbxpembayaran').getValue();
		
		if 	(cbxstbayar || cbxsearch || cbxbeli || cbxjthtempo || cbxpembayaran) {
			Ext.getCmp('btn_save_excel').enable();
			Ext.getCmp('btn_rekap_pembelian_excel').enable();
			Ext.getCmp('btn_detail_rekap_pembelian_excel').enable();
			Ext.getCmp('btn_rekap_pembelian').enable();
			Ext.getCmp('btn_detail_rekap_pembelian').enable();

		} else {
			Ext.getCmp('btn_save_excel').disable();
			Ext.getCmp('btn_rekap_pembelian_excel').disable();
			Ext.getCmp('btn_detail_rekap_pembelian_excel').disable();
			Ext.getCmp('btn_rekap_pembelian').disable();
			Ext.getCmp('btn_detail_rekap_pembelian').disable();
		}
		
		if (!Ext.getCmp('cbxstbayar').getValue() && !Ext.getCmp('cbxsearch').getValue() && !Ext.getCmp('cbxbeli').getValue() && !Ext.getCmp('cbxjthtempo').getValue() && !Ext.getCmp('cbxpembayaran').getValue())
		{
			grid_nya_po_supp.getStore().removeAll();
		} else {
			if (Ext.getCmp('cb.search').getValue()) {
				ds_grid_po_supp.reload({
					params: { 
						cbxstbayar: cbxstbayar,
						cbxsearch: cbxsearch,
						cbxbeli: cbxbeli,
						cbxjthtempo: cbxjthtempo,
						cbxpembayaran: cbxpembayaran,
						
						key: Ext.getCmp('cb.search').getValue(),
						value: Ext.getCmp('tf.search').getValue(),
						status: Ext.getCmp('cb.status').getValue(),
						tglbeli1: Ext.getCmp('df.tglbeli1').getValue(),
						tglbeli2: Ext.getCmp('df.tglbeli2').getValue(),
						jthtempo1: Ext.getCmp('df.jthtempo1').getValue(),
						jthtempo2: Ext.getCmp('df.jthtempo2').getValue(),
						pembayaran1: Ext.getCmp('df.pembayaran1').getValue(),
						pembayaran2: Ext.getCmp('df.pembayaran2').getValue(),
					}
				});	
			}
		}
		
	}
	
	//=============**********************
	
	//===================FORM PEMBAYARAN SUPPLIER===========================//{
	function pembayaran_supplier_form(ds_gridmain, records, storesObj){
		
		var ds_app1 = storesObj.app1;
		var ds_jpembayaran = storesObj.jpembayaran;
		//var ds_akun = dm_akun_jurnal();
		var ds_akun = dm_akun_pembayaran_supplier();		
		
		//====================FORM UTAMA
		var byr_supp_form = new Ext.form.FormPanel({
					id: 'fp.byrSupp',
					region: 'center',
					bodyStyle: 'padding: 5px;',		
					border: false, frame: true,
					height: 550,
					width: 1070,
					items: [{
						xtype: 'fieldset',
						id: 'fs.orderpo', layout: 'column',
						defaults: { labelWidth: 150, labelAlign: 'right', }, 
						items:[{
							//column 1 left
							layout: 'form', columnWidth: 0.50,
							items: [{
								xtype: 'textfield',
								id: 'tf.nopo',
								width: 150,
								fieldLabel: 'No. Pembelian',
								disabled: true, 
								style: 'font-weight: bold;',
								value: (records) ? records.data.nopo:""
							},{
								xtype: 'datefield', 
								id: 'df.tglpo', 
								value: (records) ? records.data.tglpo:new Date(), 
								width: 100,
								format: 'd-m-Y',
								disabled: true, 
								fieldLabel: 'Tanggal Pembelian'
							},{
								xtype: 'textfield', 
								id: 'tf.jpp', 
								fieldLabel: 'Jenis Pesanan', 
								value: (records) ? records.data.nmjnspp:"", 
								disabled: true, 
								width: 100,
							},{
								xtype: 'textfield', 
								id: 'tf.nopp',
								value: (records) ? records.data.nopp:"", 							
								fieldLabel: 'No. Pesanan', 
								disabled: true, 
								width: 100,
							},{
								xtype: 'datefield', 
								id: 'df.tglpp', 
								value: (records) ? records.data.tglpp:new Date(), 
								width: 100,
								format: 'd-m-Y',
								disabled: true, 
								fieldLabel: 'Tanggal Pesanan',
							}]
						},{
						//column 2 right
							layout: 'form', columnWidth: 0.50,
							items:[{
								xtype: 'compositefield',
								fieldLabel: 'Supplier',
								items:[{
									xtype: 'textfield', id: 'tf.supplier', width: 230, disabled: true, value: (records) ? records.data.nmsupplier:"", 
								},{
									xtype:'textfield', id: 'tf.kdsupplier', width: 250, disabled: true, hidden: true, value: (records) ? records.data.kdsupplier:"", 
								}]
								
							},{
									xtype: 'textfield', id: 'tf.tlp', width: 230, style:'opacity: 0.6', fieldLabel: 'No. Telepon', disabled: true,
									value: (records) ? records.data.notelp:"",
							},{
									xtype: 'textfield', id: 'tf.fax', width: 230, style:'opacity: 0.6', fieldLabel: 'Fax', disabled: true,
									value: (records) ? records.data.nofax:"",
							},{
									xtype:'textfield', id:'tf.npwp', width: 230, style:'opacity: 0.6', fieldLabel: 'NPWP', disabled: true,
									value: (records) ? records.data.npwp:"",
							},{
								xtype: 'compositefield',
								fieldLabel: 'Bagian',
								items:[{
									xtype: 'textfield', id: 'tf.bagian', width: 230, disabled: true, value: (records) ? records.data.nmbagian:"", 
								},{
									xtype:'textfield', id: 'tf.idbagian', width: 250, disabled: true, hidden: true, value: (records) ? records.data.idbagian:"",
								}]
								
							}]
						}],
						bbar: [{
									xtype: 'label', text: 'Syarat Pembayaran:', style:'font-size: 12px;margin-left:10px;margin-right:5px',
								},{
									xtype: 'textfield', id: 'tf.nmsypembayaran', width: 70, disabled: true, style:'font-size: 12px;opacity: 0.6', value: (records) ? records.data.nmsypembayaran:"", 
								},{
									xtype: 'label', text: 'Tgl. Jatuh Tempo:', style:'font-size: 12px;margin-left:10px;margin-right:5px',
								},{
									xtype: 'datefield', id: 'df.tgljatuhtempo', width: 100, format: 'd-m-Y', disabled: true, readOnly: true, style:'font-size: 12px;opacity: 0.6', value: (records) ? records.data.tgljatuhtempo:"",
								},{
									xtype: 'label', text: 'Jenis Pembayaran:', style:'font-size: 12px;margin-left:10px;margin-right:5px',
								},{
									xtype: 'textfield', id: 'tf.nmjnspembayaran', width: 100, disabled: true, style:'font-size: 12px;opacity: 0.6', value: (records) ? records.data.nmjnspembayaran:"",
								},{
									xtype: 'label', text: 'Status Pesanan:', style:'font-size: 12px;margin-left:10px;margin-right:5px',
								},{
									xtype: 'textfield', id: 'tf.nmstpo', width: 100, disabled: true, style:'font-size: 12px;opacity: 0.6', value: (records) ? records.data.nmstpo:"",
								},{
									xtype: 'label', text: 'No. Reff:', style:'font-size: 12px;margin-left:10px;margin-right:5px',
								},{
									xtype: 'textfield', id: 'tf.bpb', width: 100, disabled: true, style:'font-size: 12px;opacity: 0.6', value: (records) ? records.data.bpb:"",
								}]
					},getTabs()],
					listeners:{
						afterrender: module_afterrender
					}
		}); 
		
		//====================BUAT TAB
		function getTabs(){
			var Tabs = { //new Ext.TabPanel({ //{    //Ext.widget('tabpanel', { //for Extjs-4
				xtype: 'tabpanel',
				id: 'tabpanel',
				height:365,
				defaults :{
					bodyPadding: 10, 
					autoScroll: true,
					frame: true
				},
				activeTab: 0,
				items: [{
					title: 'Pembayaran', id: 'tab.bayar', autoScroll: true,
					items: [getBayar()]
				},{            
					title: 'Detail Pembelian',	id: 'tab.detbeli', autoScroll: true,
					items: [getBeli()]
				}]
				
			};
			
			return Tabs;
		}
		
		//====================TAB BAYAR
		function getBayar() {
		
		//====================GRID BAYAR
			var ds_grid = new Ext.data.JsonStore({
					proxy: new Ext.data.HttpProxy({
					url : BASE_URL + 'pembayaran_supplier_controller/get_bayardet',
						method: 'POST'
					}),
					totalProperty: 'results',
					root: 'data',
					baseParams: {
						nobayar:""
					},
					autoLoad: true,
					fields: [{
						name: 'nobayar',
						mapping: 'nobayar'
					},{
						name: 'idjnspembayaran',
						mapping: 'idjnspembayaran'
					},{
						name: 'nmjnspembayaran',
						mapping: 'nmjnspembayaran'
					},{
						name: 'nominal',
						mapping: 'nominal'
					},{
						name: 'nokartu',
						mapping: 'nokartu'
					},{
						name: 'tahun',
						mapping: 'tahun'
					},{
						name: 'idakun',
						mapping: 'idakun'
					}]
				});
				
			var sm_nya = new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners: {}
			});
				
			var cm = new Ext.grid.ColumnModel({
				// specify any defaults for each column
				defaults: {
					sortable: true // columns are not sortable by default           
				},
				columns: [
				new Ext.grid.RowNumberer(),
				{
					header: '<center>Jenis Pembayaran</center>',
					width: 120,
					dataIndex: 'nmjnspembayaran',
					align:'center',	
				},{
					header: '<center>Akun</center>',
					width: 150,
					dataIndex: 'akun',
					align:'right',
				},{
					header: '<center>Nominal</center>',
					width: 150,
					dataIndex: 'nominal',
					align:'right',
					xtype: 'numbercolumn',// format:'0,000',
				},{
					header: '<center>No. Kartu/No. Reff</center>',
					width: 130,
					dataIndex: 'nokartu',
				},{
						xtype: 'actioncolumn',
						width: 50,
						header: 'Hapus',
						align:'center',
						items: [{
							getClass: function(v, meta, record) {
								meta.attr = "style='cursor:pointer;'";
							},
							icon   : 'resources/img/icons/fam/delete.gif',
							tooltip: 'Hapus',
							handler: function(grid, rowIndex) {
								ds_grid.removeAt(rowIndex);
								counttotal();

							}
						}]
				}]
			});
			
			var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});

			var grid_nya = new Ext.grid.EditorGridPanel({
				id: 'grid_bayardet',
				store: ds_grid,
				vw:vw,
				cm:cm,
				sm:sm_nya,
				tbar: [{
						text: 'Tambah',
						id: 'btn_add_form',
						iconCls: 'silk-add', 
						margins: '5',
						disabled: (records.data.stlunas=='Lunas') ? true:false,
						width: 70,
						handler: function() {
							if(Ext.getCmp("tf.sisa").getValue()==0){
								Ext.Msg.alert("Info", "Pembayaran Sudah Cukup"); return;
							}
							addBayar();
						}
					},'->',
					{
						text: 'Total/Sisa Tagihan',
					},
					{
						xtype: 'numericfield', 							
						fieldLabel: 'Total/Sisa Tagihan', 
						id: 'tf.sisa',
						width: 125,
						/* maskRe: /[0-9]/,
						decimalSeparator: ',', 
						decimalPrecision: 0, 
						alwaysDisplayDecimals: true,
						useThousandSeparator: true, 
						thousandSeparator: '.',
						disabled: true */
				}],
				bbar: ['->',
					{
						text: 'Jumlah',
					},
					{
						xtype: 'numericfield', 							
						fieldLabel: 'Jumlah', 
						id: 'tf.jml',
						width: 125,
						/* maskRe: /[0-9]/,
						decimalSeparator: ',', 
						decimalPrecision: 0, 
						alwaysDisplayDecimals: true,
						useThousandSeparator: true, 
						thousandSeparator: '.',
						disabled: true,  */
				}],
				frame: true,
				height: 135,
				autoWidth: true,
				loadMask: true,
				forceFit: true,
				buttonAlign: 'left',
				autoScroll: true,
				listeners: {
					cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
					
					},
				}
			});
			//====================
			
			//====================GRID HISTORI BAYAR
			var ds_grid_histori = new Ext.data.JsonStore({
					proxy: new Ext.data.HttpProxy({
					url : BASE_URL + 'pembayaran_supplier_controller/get_bayarhistori',
						method: 'POST'
					}),
					totalProperty: 'results',
					root: 'data',
					baseParams: {
						nopo:records.data.nopo
					},
					autoLoad: true,
					fields: [{
						name: 'nobayar',
						mapping: 'nobayar'
					},{
						name: 'tglbayar',
						mapping: 'tglbayar'
					},{
						name: 'jumlah',
						mapping: 'jumlah'
					},{
						name: 'catatan',
						mapping: 'catatan'
					},{
						name: 'nmlengkap',
						mapping: 'nmlengkap'
					},{
						name: 'approval',
						mapping: 'approval'
					}]
				});
				
			ds_grid_histori.on('load', function(){
				var sbayar = 0;
				ds_grid_histori.each(function(rec){
					sbayar += rec.get('jumlah'); 
				
				});
				Ext.getCmp("tf.sbayar").setValue(sbayar);
				Ext.getCmp("tf.stagih").setValue(Ext.getCmp("tf.tbayar").getValue() - sbayar);
				Ext.getCmp("tf.sisa").setValue(Ext.getCmp("tf.stagih").getValue());
			});
			
			var sm_histori = new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners: {}
			});
				
			var cm_histori = new Ext.grid.ColumnModel({
				// specify any defaults for each column
				defaults: {
					sortable: true // columns are not sortable by default           
				},
				columns: [
				new Ext.grid.RowNumberer(),
				{
					header: '<center>No. Pembayaran</center>',
					width: 120,
					dataIndex: 'nobayar',
					align:'center',	
				},{
					header: '<center>Tgl. Pembayaran</center>',
					width: 150,
					dataIndex: 'tglbayar',
				},{
					header: '<center>Jumlah</center>',
					width: 100,
					dataIndex: 'jumlah',
					align:'right',
					xtype: 'numbercolumn',// format:'0,000',
				},{
					header: '<center>Catatan</center>',
					width: 100,
					dataIndex: 'catatan',
				},{
					header: '<center>User Input</center>',
					width: 100,
					dataIndex: 'nmlengkap',
				},{
					header: '<center>Approval</center>',
					width: 100,
					dataIndex: 'approval',
				},{
						xtype: 'actioncolumn',
						width: 50,
						header: 'Hapus',
						align:'center',
						items: [{
							getClass: function(v, meta, record) {
								meta.attr = "style='cursor:pointer;'";
							},
							icon   : 'resources/img/icons/fam/delete.gif',
							tooltip: 'Hapus',
							handler: function(grid, rowIndex) {
								var recordh = ds_grid_histori.getAt(rowIndex);
								
								Ext.Msg.show({
									title: 'Konfirmasi',
									msg: 'Hapus data yang dipilih?',
									buttons: Ext.Msg.YESNO,
									icon: Ext.MessageBox.QUESTION,
									fn: function (response) {
										if ('yes' !== response) {
											return;
										}
										
										var waitmsghapushistori = Ext.MessageBox.wait('Menghapus Data...', 'Info');
										
										Ext.Ajax.request({
											url: BASE_URL + 'pembayaran_supplier_controller/delete_bayar',
											method: 'POST',
											params: {
												nobayar	: recordh.data.nobayar
											},
											success: function() {
												waitmsghapushistori.hide();
												Ext.Msg.alert("Info", "Hapus Data Berhasil");
												grid_nya_histori.getStore().reload();
												ds_gridmain.reload();
												grid_nya.getStore().removeAll();
												counttotal();
											},
											failure: function(result){
												waitmsghapushistori.hide();
												Ext.MessageBox.alert("Info", "Hapus Data Gagal");
											}					
										});
									}            
								});

							}
						}]
				}]
			});
			
			var vw_histori = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});

			var grid_nya_histori = new Ext.grid.EditorGridPanel({
				id: 'grid_bayar_histori',
				store: ds_grid_histori,
				vw:vw_histori,
				cm:cm_histori,
				sm:sm_histori,
				//tbar: [],
				bbar: [{
						text: 'Total Bayar',
					},
					{
						xtype: 'numericfield', 							
						fieldLabel: 'Total Bayar', 
						id: 'tf.tbayar',
						width: 125,
						/* maskRe: /[0-9]/,
						decimalSeparator: ',', 
						decimalPrecision: 0, 
						alwaysDisplayDecimals: true,
						useThousandSeparator: true, 
						thousandSeparator: '.',
						disabled: true,  */
						value: records.data.bliret, 
					},
					{
						text: 'Sudah Di Bayar',
					},
					{
						xtype: 'numericfield', 							
						fieldLabel: 'Sudah Di Bayar', 
						id: 'tf.sbayar',
						width: 125,
						/* maskRe: /[0-9]/,
						decimalSeparator: ',', 
						decimalPrecision: 0, 
						alwaysDisplayDecimals: true,
						useThousandSeparator: true, 
						thousandSeparator: '.',
						disabled: true, */
					},
					{
						text: 'Sisa Tagihan',
					},
					{
						xtype: 'numericfield', 							
						fieldLabel: 'Sisa Tagihan', 
						id: 'tf.stagih',
						width: 125,
						/* maskRe: /[0-9]/,
						decimalSeparator: ',', 
						decimalPrecision: 0, 
						alwaysDisplayDecimals: true,
						useThousandSeparator: true, 
						thousandSeparator: '.',
						disabled: true, */ 
				}],
				frame: true,
				height: 150,
				autoWidth: true,
				loadMask: true,
				forceFit: true,
				buttonAlign: 'left',
				autoScroll: true,
				listeners: {
					cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
					
					},
				}
			});
			//====================
			
			//====================FORM TAB BAYAR
			var byr_form = {
					xtype: 'panel',
					id: 'fp.byrtab',
					layout: 'form',
					margin: '0 0 10',
					tbar: [{
								text: 'Baru',
								id: 'btn_baru_bayarsupp',
								iconCls: 'silk-arrow-refresh', 
								margins: '5',
								width: 70,
								disabled: true,
								handler: function() {
									Ext.getCmp('btn_save_bayarsupp').enable();
									Ext.getCmp('btn_add_form').enable();
									Ext.getCmp('btn_baru_bayarsupp').disable();
									
									Ext.getCmp('tf.nobayar').setValue("");
									Ext.getCmp('df.tglbayar').setValue(new Date());
									Ext.getCmp('cb.app1').setValue("Wenny");
									Ext.getCmp('ta.catatan').setValue("");
									
									grid_nya.getStore().removeAll();
								}
						},{
								text: 'Simpan',
								id: 'btn_save_bayarsupp',
								iconCls: 'silk-save', 
								margins: '5',
								width: 70,
								disabled: (records.data.stlunas=='Lunas') ? true:false,
								handler: function() {
									//jika sisa tagihan tidak 0, maka muncul alert
									var sisa_tagihan = Ext.getCmp("tf.sisa").getValue();
									if(sisa_tagihan != 0){

										Ext.Msg.alert("Info", "Total bayar tidak sesuai dengan total tagihan "); return;
									
									}else{
										
										if (records.data.nobayar) {
											simpan_update(records.data.nobayar,true);
										} else {
											simpan_update(null,false);
										}

									}
								}
						},{
								text: 'Cetak',
								id: 'btn_cetak_bayarsupp',
								iconCls: 'silk-printer', 
								margins: '5',
								width: 70,
								handler: function() {
									var win = window.open(); 
									win.location.reload();
									win.location = BASE_URL + 'print/print_pembayaran_supplier/pdf_bayarsupp/'+RH.getCompValue('tf.nopo');

								}
						},{
								text: 'Kembali',
								id: 'btn_back_bayarsupp',
								iconCls: 'silk-arrow-undo', 
								margins: '5',
								width: 70,
								handler: function() {
									win_byr_supp_form.close();
								}
						}],
					items: [{
						xtype: 'fieldset',
						flex: 1,
						id: 'fs.bayar', layout: 'column',
						defaults: { labelWidth: 150, labelAlign: 'right', }, 
						items:[{
							//column 1 left
							layout: 'form', columnWidth: 0.50,
							items: [{
								xtype: 'textfield',
								id: 'tf.nobayar',
								width: 150,
								fieldLabel: 'No. Pembayaran',
								disabled: true, 
								style: 'font-weight: bold;',
								value: (records.data.nobayar) ? records.data.nobayar:"",
							},{
								xtype: 'datefield', 
								id: 'df.tglbayar', 
								width: 100,
								format: 'd-m-Y',
								//disabled: true, 
								fieldLabel: 'Tgl. Pembayaran',
								//value: (records.data.nobayar) ? records.data.tglbayar:new Date(),
								value: (records.data.nobayar) ? records.data.tglbayar : tgl_byr,
								listeners:{
                  select: function(field, newValue){
                    tgl_byr = Ext.getCmp('df.tglbayar').getValue();
                  },
                  change : function(field, newValue){
                    tgl_byr = Ext.getCmp('df.tglbayar').getValue();
                  }
                }
							},{
								xtype: 'textfield', 
								id: 'tf.userid', 
								fieldLabel: '',
								disabled: true, 
								hidden: true, 
								width: 100,
								value: (records.data.nobayar) ? records.data.userid:USERID,
							},{
								xtype: 'textfield', 
								id: 'tf.username', 
								fieldLabel: 'User Input',
								disabled: true, 
								width: 100,
								value: (records.data.nobayar) ? records.data.nmlengkap:USERNAME,
							}]
						},{
						//column 2 right
							layout: 'form', columnWidth: 0.50,
							items:[{
									xtype: 'combo', fieldLabel: 'Approval',
									id:'cb.app1', width: 200, store: ds_app1,
									valueField: 'nmset', displayField: 'nmset', editable: false,allowBlank: false,
									triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
									emptyText:'Pilih....',
								},{
									xtype: 'textarea', 
									id: 'ta.catatan', 
									fieldLabel: 'Catatan',
									width: 200, height: 45,
									value: (records.data.nobayar) ? records.data.catatan:"",
								}]
						}]
					},{
						xtype: 'fieldset',
						flex: 1,
						title: 'Cara Bayar',
						defaults: {
							labelWidth: 130,
							xtype: 'textfield',
							labelAlign: 'right'
						},
						items: [
						grid_nya
						]

					},{
						xtype: 'fieldset',
						flex: 1,
						title: 'Daftar Pembayaran',
						defaults: {
							labelWidth: 130,
							xtype: 'textfield',
							labelAlign: 'right'
						},
						items: [
						grid_nya_histori
						]

					}],
			};
			
			return byr_form;
			
			//====================POPUP BAYAR
			function addBayar() {
				var add_byr_form = new Ext.form.FormPanel({
						id: 'fp.addbyr',
						region: 'center',
						bodyStyle: 'padding: 5px;',		
						border: false, frame: true,
						height: 200,
						width: 400,
						labelWidth: 125,
						autoScroll: true,
						bbar: [{
								text: 'Tambah',
								id: 'btn_add_bayar',
								iconCls: 'silk-add', 
								margins: '5',
								disabled: (records.data.stlunas=='Lunas') ? true:false,
								width: 70,
								handler: function() {
									tambah();
								}
						},{
								text: 'Kembali',
								id: 'btn_back_bayar',
								iconCls: 'silk-arrow-undo', 
								margins: '5',
								width: 70,
								handler: function() {
									win_byr_add.close();
								}
						}],
						items: [{
									xtype: 'combo', fieldLabel: 'Jenis Pembayaran',
									id:'cb.jpembayaran', width: 200, store: ds_jpembayaran,
									valueField: 'idjnspembayaran', displayField: 'nmjnspembayaran', editable: false,allowBlank: false,
									triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
									emptyText:'Pilih....',
								},{
									xtype: 'combo',
									id:'cb.akun',
									store: ds_akun,
									valueField: 'idakun', 
									displayField: 'nmakun', 
									editable: true,
									allowBlank: false,
									triggerAction: 'all',
									forceSelection: true, 
									submitValue: true, 
									mode: 'local',
									emptyText:'Pilih....',
									fieldLabel: 'Akun',
									width: 200,
								},{
									xtype: 'numericfield',
									fieldLabel: 'Nominal',
									id: 'tf.nominal',
									width: 200,
									/* maskRe: /[0-9]/,
									decimalSeparator: ',', 
									decimalPrecision: 0, 
									alwaysDisplayDecimals: true,
									useThousandSeparator: true, 
									thousandSeparator: '.', */
									value:Ext.getCmp('tf.sisa').getValue()
								},{
									xtype: 'textfield', 
									id: 'tf.nokartu',							
									fieldLabel: 'No. Kartu/No. Reff', 
									width: 100,
								}]
				});	
				
				var win_byr_add = new Ext.Window({
					title: 'Tambah Bayar',
					modal: true,
					items: [add_byr_form]
				}).show();
				
				function tambah(){
				
					if(!Ext.getCmp('cb.jpembayaran').getValue() || !Ext.getCmp('tf.nominal').getValue()/*  || !Ext.getCmp('tf.nokartu').getValue() */){
						Ext.Msg.alert("Info", "Lengkapi"); return;
					}
					
					if(!Ext.getCmp('cb.akun').getValue() || !Ext.getCmp('cb.akun').getValue()/*  || !Ext.getCmp('tf.nokartu').getValue() */){
						Ext.Msg.alert("Info", "Lengkapi"); return;
					}
					
					if(Ext.getCmp('tf.nominal').getValue() > Ext.getCmp("tf.sisa").getValue()){
						Ext.Msg.alert("Info", "Pembayaran Tidak Boleh Melebihi Tagihan"); return;
					}

					// data akun
					var value_akun  = Ext.getCmp('cb.akun').getValue();
					var record_akun = Ext.getCmp('cb.akun').findRecord(Ext.getCmp('cb.akun').valueField || Ext.getCmp('cb.akun').displayField, value_akun);
					var sidakun			= record_akun.get("idakun");
					var snmakun    	= record_akun.get("nmakun");
					
					var orgaListRecord = new Ext.data.Record.create([
								{
									name: 'nobayar',
									name: 'idjnspembayaran',
									name: 'nmjnspembayaran',
									name: 'nominal',
									name: 'nokartu',
									name: 'idakun',
								}
							]);
							
							ds_grid.add([
								new orgaListRecord({
									'nobayar': "",
									'idjnspembayaran': Ext.getCmp('cb.jpembayaran').getValue(),
									'nmjnspembayaran': Ext.getCmp('cb.jpembayaran').getRawValue(),
									'nominal': Ext.getCmp('tf.nominal').getValue(),
									'nokartu': Ext.getCmp('tf.nokartu').getValue(),
									'idakun': sidakun,
									'akun': snmakun,
								})
							]);
							
							counttotal();
				}
			}
			
			function counttotal() {
				var jumlah = 0;
				ds_grid.each(function(rec){
					jumlah += parseFloat(rec.get('nominal')); 
				
				});
				Ext.getCmp("tf.jml").setValue(jumlah);
				Ext.getCmp("tf.sisa").setValue(Ext.getCmp("tf.stagih").getValue() - jumlah);
				if (Ext.getCmp('tf.nominal')) {
					Ext.getCmp('tf.nominal').setValue(Ext.getCmp("tf.stagih").getValue() - jumlah);
				}

			}
			
			//====================SIMPAN BAYAR
			function simpan_update(nobayar,isUpdate){
			
				if (grid_nya.getStore().getCount()==0) {
					Ext.Msg.alert("Info", "Lengkapi Data");
					return;
				}
				
				var waitmsgsimpan = Ext.MessageBox.wait('Menyimpan Data...', 'Info');
				
				var par='';
				var count= 1;
				var endpar = ';';
			
				grid_nya.getStore().each(function(rec){ // ambil seluruh grid prodi
					var rowData = rec.data; 
					if (count == grid_nya.getStore().getCount()) {
						endpar = ''
					}
				
					par +=  nobayar + '&' +
							rowData['idjnspembayaran'] + '&' +
							rowData['nominal'] + '&' +
							rowData['nokartu'] + '&' +
							rowData['idakun'] + '&' +
							endpar;
											
					count = count+1;
				});
				
				Ext.Ajax.request({
					url: BASE_URL + 'pembayaran_supplier_controller/simpan_update',
					method: 'POST',
					params: {
						par			: par,
						nobayar		: Ext.getCmp('tf.nobayar').getValue(),
						tglbayar	: Ext.getCmp('df.tglbayar').getValue(),
						nopo		: Ext.getCmp('tf.nopo').getValue(),
						userid		: Ext.getCmp('tf.userid').getValue(),
						app1		: Ext.getCmp('cb.app1').getValue(),
						catatan		: Ext.getCmp('ta.catatan').getValue()
					},
					success: function(response) {
						var jsonData = Ext.decode(response.responseText);
						
						waitmsgsimpan.hide();
						Ext.Msg.alert("Info", "Simpan Data Berhasil");
						
						Ext.getCmp('tf.nobayar').setValue(jsonData.nobayar);
						Ext.getCmp('btn_save_bayarsupp').disable();
						Ext.getCmp('btn_add_form').disable();
						Ext.getCmp('btn_baru_bayarsupp').enable();
						ds_grid_histori.reload();
						ds_gridmain.reload();
						grid_nya.getStore().removeAll();
						counttotal();
						
					},
					failure: function(result){
						waitmsgsimpan.hide();
						Ext.MessageBox.alert("Info", "Simpan Data Gagal");
						win_byr_supp_form.close();
					}					
				});

			}

		}
		
		//====================TAB DETAIL PEMBELIAN
		function getBeli() {
		
			var	ds_ppx = new Ext.data.JsonStore({
				proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'pembayaran_supplier_controller/get_detbeli',
					method: 'POST'
				}),
				totalProperty: 'results',
				root: 'data',
				autoLoad: true,
				baseParams: {
						nopo:records.data.nopo
				},
				fields: [{
					name: 'nopp',
					mapping: 'nopp'
				},{
					name: 'tglpp',
					mapping: 'tglpp'
				},{
					name: 'idbagian',
					mapping: 'idbagian'
				},{
					name: 'nmbagian',
					mapping: 'nmbagian'
				},{
					name: 'idstsetuju',
					mapping: 'idstsetuju'
				},{
					name: 'nmstsetuju',
					mapping: 'nmstsetuju'
				},{
					name: 'keterangan',
					mapping: 'keterangan'
				},{
					name: 'userid',
					mapping: 'userid'
				},{
					name: 'nmlengkap',
					mapping: 'nmlengkap'
				},{
					name: 'kdbrg',
					mapping: 'kdbrg'
				},{
					name: 'nmbrg',
					mapping: 'nmbrg'
				},{
					name: 'idsatuan',
					mapping: 'idsatuan'
				},{
					name: 'nmsatuan',
					mapping: 'nmsatuan'
				},{
					name: 'qty',
					mapping: 'qty'
				},{
					name: 'catatan',
					mapping: 'catatan'
				},{
					name: 'harga',
					mapping: 'harga'
				},{
					name: 'idhrgbrgsup',
					mapping: 'idhrgbrgsup'
				},{
					name: 'kdsupplier',
					mapping: 'kdsupplier'
				},{
					name: 'idstpp',
					mapping: 'idstpp'
				},{
					name: 'nmstpp',
					mapping: 'nmstpp'
				},{
					name: 'nopo',
					mapping: 'nopo'
				},{
					name: 'idmatauang',
					mapping: 'idmatauang'
				},{
					name: 'nmmatauang',
					mapping: 'nmmatauang'
				},{
					name: 'kdmatauang',
					mapping: 'kdmatauang'
				},{
					name: 'subtotal',
					mapping: 'subtotal'
				},{
					name: 'nmsupplier',
					mapping: 'nmsupplier'
				},{
					name: 'nofax',
					mapping: 'nofax'
				},{
					name: 'notelp',
					mapping: 'notelp'
				},{
					name: 'nofax',
					mapping: 'nofax'
				},{
					name: 'margin',
					mapping: 'margin'
				},{
					name: 'rasio',
					mapping: 'rasio'
				},{
					name: 'idsatuanbsr',
					mapping: 'idsatuanbsr'
				},{
					name: 'hrgbeli',
					mapping: 'hrgbeli'
				},{
					name: 'hsubtotal',
					mapping : 'hsubtotal'
				},{
					name: 'hargajualtemp',
					mapping: 'hargajualtemp'
				},{
					name: 'hargajual',
					mapping: 'hargajual'
				},{
					name: 'nmsatuanbsr',
					mapping: 'nmsatuanbsr'
				},{
					name: 'htsubtotal',
					mapping: 'htsubtotal'
				},{
					name: 'stoknowbagian',
					mapping: 'stoknowbagian'
				},{
					name: 'diskon',
					mapping: 'diskon'
				},{
					name: 'diskonrp',
					mapping: 'diskonrp'
				},{
					name: 'qtypo',
					mapping: 'qtypo'
				},{
					name: 'idstpp',
					mapping: 'idstpp'
				},{
					name: 'ppn',
					mapping: 'ppn',
					type:'bool'
				},{
					name: 'tamppn',
					mapping: 'tamppn',
				},{
					name: 'qtytemp',
					mapping: 'qty',
				},{
					name: 'hrgbelikcl',
					mapping: 'hrgbelikcl',
				},{
					name: 'tmptotppn',
					mapping: 'tmptotppn',
				},{
					name: 'qtyb',
					mapping: 'qtyb',
				},{
					name: 'qtyretur',
					mapping: 'qtyretur',
				}],
				listeners: {
					load: function(store, records, options) {

					}
				}
			});
		
		var row_gridnya = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});

		function headerGerid(text, align){
			var hAlign = 'center';	
			if(align =='c') hAlign = 'center';
			if(align =='l') hAlign = 'left';
			if(align =='r') hAlign = 'right';
			return "<span align='"+hAlign+"'>"+text+"</span>";
		}
		
		var sm_po = new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {}
		});
		
		var grid_po = new Ext.grid.EditorGridPanel({
				store: ds_ppx,
				frame: true,
				border: true,
				height: 220,
				bodyStyle: 'padding:3px 3px 3px 3px',
				id: 'grid_po',
				forceFit: true,
				autoScroll: true,
				columnLines: true,
				sm:sm_po,
				loadMask: true,
				clicksToEdit: 1,
				listeners	: {
					rowclick : function(grid, rowIndex, e){
						rownota = rowIndex;
					}
				},
				columns: [
				new Ext.grid.RowNumberer(),
				{
					header: headerGerid('Kode Barang'),
					dataIndex: 'kdbrg',
					width: 80
				},{
					header: headerGerid('Nama Barang'),
					dataIndex: 'nmbrg',
					width: 160
				},{
					header: headerGerid('Satuan'),
					dataIndex: 'nmsatuanbsr',
					width: 60,
				},{
					header: headerGerid('Qty'),
					dataIndex: 'qty',
					width: 50,
					align: 'right',
					xtype: 'numbercolumn', format:'0,000.00',		
				},{
					header: headerGerid('Qty<br>Bonus'),
					dataIndex: 'qtyb',
					width: 50,
					xtype: 'numbercolumn', format:'0,000.00',
				},{
					header: headerGerid('Qty<br>Retur'),
					dataIndex: 'qtyretur',
					width: 50,
					align: 'right',
					xtype: 'numbercolumn', format:'0,000.00',
				},{
					header: headerGerid('@Harga Beli'),
					dataIndex: 'hrgbeli',
					width: 90,
					align:'right',
					xtype: 'numbercolumn', format:'0,000',
				},{
					header: headerGerid('PPN<br>(10%)'),
					dataIndex: 'ppn',
					width: 65,
					xtype: 'checkcolumn',
					id: 'cc.ppn',
					processEvent: function(name, e, grid, rowIndex, colIndex){
						return false;
					}
				},{
					header: headerGerid('Diskon<br>(%)'),
					dataIndex: 'diskon',
					width: 55,
					align:'right',
					xtype: 'numbercolumn', format:'0,000.00',
				},{
					header: headerGerid('Diskon<br>(Rp.)'),
					dataIndex: 'diskonrp',
					width: 80,
					align:'right',
					xtype: 'numbercolumn', format:'0,000',
				},{
					header: headerGerid('Subtotal'),
					dataIndex: 'subtotal',
					width: 90,
					align:'right',
					xtype: 'numbercolumn', format:'0,000',
				},{
					header: headerGerid('@Harga Beli<br>(Satuan Kecil)'),
					dataIndex: 'hrgbelikcl',
					width: 90,
					align:'right',
					xtype: 'numbercolumn', format:'0,000',
				},{
					header: headerGerid('Margin<br>(%)'),
					dataIndex: 'margin',
					width: 55,
					align:'right',
					xtype: 'numbercolumn', format:'0,000.00',
				},{
					header: headerGerid('@Harga Jual'),
					dataIndex: 'hargajual',
					width: 100,
					align:'right',
					xtype: 'numbercolumn', format:'0,000',
				}]
				
			});
			
			var form_po = {
				xtype: 'panel',
				id: 'fp.byrtab',
				layout: 'form',
				margin: '0 0 10',
				items: [{
					xtype: 'fieldset', title:'Rincian Pembelian', 
					items: [grid_po]
				},{
					xtype: 'fieldset', title: '',
					layout: 'column',
					defaults: { labelWidth: 150, labelAlign: 'right' },
					items:[{
					//column 2 left
						layout: 'form', columnWidth: 0.70,
						items: [{
								xtype: 'textfield', id:'comp.hidden', style:'opacity: 0'
								
						}]
					},{
						layout: 'form', columnWidth: 0.30,
						items: [{
								xtype: 'numericfield', id:'tf.jumlah', width: 120,
								disabled: true, fieldLabel: 'Jumlah'
								
						},{
								xtype: 'numericfield', id:'tf.totdiskon', width: 120,
								disabled: true, fieldLabel: 'Total Diskon'
								
						},{
							
								xtype: 'numericfield', id: 'tf.totppn', fieldLabel: 'Total PPN (10%)', width: 120, disabled: true
						},{
								xtype:'numericfield', id: 'tf.total', width: 120, disabled: true, fieldLabel: 'Total', style: 'font-weight:bold;text-align:right'
						}]
					}]
				}]
			};
			
			ds_ppx.on('load', function(){
				var jumlah = 0, diskonrp1 = 0, tamtotppn = 0; total = 0;
					ds_ppx.each(function(rec){
						jumlah += (parseFloat(rec.get('hrgbeli')) * (parseFloat(rec.get('qty')) - parseFloat(rec.get('qtyretur')))); 
						diskonrp1 += parseFloat(rec.get('diskonrp'));
						if (rec.get('ppn')=='0') {
							var ppnn = 0;
							ppnn += 0; //jika ppn di ceklist hitungan ppn= 0
							rec.set('tmptotppn',ppnn);
						} else if (rec.get('ppn')=='1') {
							var ppnn = 0;
							ppnn += (parseFloat(rec.get('hrgbeli')) * (parseFloat(rec.get('qty')) - parseFloat(rec.get('qtyretur'))) - parseFloat(rec.get('diskonrp'))) * 0.1; //jika ppn di ceklist hitungan ppn= ((hrgbeli * qty) - diskonrp) * 0.1
							rec.set('tmptotppn',ppnn);
						}
						tamtotppn += parseFloat(rec.get('tmptotppn'));				
					});			

					total = jumlah - diskonrp1 + tamtotppn;
					Ext.getCmp("tf.jumlah").setValue(jumlah);
					Ext.getCmp("tf.totdiskon").setValue(diskonrp1);
					Ext.getCmp("tf.totppn").setValue(tamtotppn);
					Ext.getCmp("tf.total").setValue(total);
					//Ext.getCmp("tf.totppn").setValue(Math.round(tamtotppn));
					//Ext.getCmp("tf.total").setValue(Math.round(total));
			});
			
			return form_po;
		}
		
		var win_byr_supp_form = new Ext.Window({
				title: 'Pembayaran Supplier Form',
				modal: true,
				items: [byr_supp_form]
			}).show();

		function module_afterrender () {
		
				
		}

	}
	//===================END OF FORM PEMBAYARAN SUPPLIER===========================// }

	//======================== FORM POSTING ========================//
	function fnkeyPostingJurnal(value, b, record){
    //Ext.QuickTips.init();
    //return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
    //  + value +'</div>';
    
    var rec = record.data;
    var stlunas = rec.stlunas;
    var kdjurnal = rec.kdjurnal;

    if(stlunas != 'Lunas'){
    	return 'belum lunas';
    }else if(kdjurnal == null){
			Ext.QuickTips.init();
	    return '<div class="showPostingForm" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">Posting</div>';
    }else{
    	return 'sudah posting';
    }

	}


}
//===================END OF LIST PEMBAYARAN SUPPLIER===========================//}


	
