function tagihan_penjamin(){
//========================GRID TAGIHAN PENJAMIN===================================
	var ds_tagihanpenjamin = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'tagihan_penjamin_controller/get_tagihan_penjamin',
			method: 'POST'
		}),
		baseParams: {
			cbx : false
		},
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
				name: 'notagihan', 
				mapping:'notagihan'
			},{
				name: 'tgltagihan', 
				mapping:'tgltagihan'
			},{
				name: 'tgljatuhtempo', 
				mapping:'tgljatuhtempo'
			},{
				name: 'perihal', 
				mapping:'perihal'
			},{
				name: 'lampiran', 
				mapping:'lampiran'
			},{
				name: 'approval', 
				mapping:'approval'
			},{
				name: 'idpenjamin', 
				mapping:'idpenjamin'
			},{
				name: 'nmpenjamin', 
				mapping:'nmpenjamin'
			},{
				name: 'nmjnspenjamin', 
				mapping:'nmjnspenjamin'
			},{
				name: 'alamat', 
				mapping:'alamat'
			},{
				name: 'notelp', 
				mapping:'notelp'
			},{
				name: 'nohp', 
				mapping:'nohp'
			},{
				name: 'nmcp', 
				mapping:'nmcp'
			},{
				name: 'totaltagihan', 
				mapping:'totaltagihan'
			},{
				name: 'dibayarpasien', 
				mapping:'dibayarpasien'
			},{
				name: 'jmltagihan', 
				mapping:'jmltagihan'
			},{
				name: 'sttagihan', 
				mapping:'sttagihan'
			},{
				name: 'jmlbyrpenjamin', 
				mapping:'jmlbyrpenjamin'
			}]
		});
		
	var ds_search = new Ext.data.JsonStore({ //static data
		autoDestroy: true,
		fields: [ 'field', 'value' ]
	});
	
	var ds_appkeu = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'tagihan_penjamin_controller/appkeu',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmset', mapping:'nmset'
		}]
	});	
	
	//var ds_carabayar = dm_carabayar();
	var ds_carabayar = dm_carabayar2();
	//var storeObj = {stbayar:ds_stbayar,jpembayaran:ds_jpembayaran};
	
	var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Bayar',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'resources/img/icons/fam/money.png',
					tooltip: 'Bayar',
                    handler: function(grid, rowIndex) {
						var records = ds_tagihanpenjamin.getAt(rowIndex);
						bayar_penjamin_form(ds_tagihanpenjamin,records)

                    }
                }]
        },{
			header: '<center>No. Tagihan</center>',
			width: 90,
			dataIndex: 'notagihan',
		},{
			header: '<center>Tgl. Tagihan</center>',
			width: 90,
			dataIndex: 'tgltagihan',
			align:'center',	
		},{
			header: '<center>Tgl. Jatuh Tempo</center>',
			width: 90,
			dataIndex: 'tgljatuhtempo',
			align:'center',	
		},{
			header: '<center>Nama Penjamin</center>',
			width: 200,
			dataIndex: 'nmpenjamin',
		},{
			header: '<center>Total Transaksi</center>',
			width: 100,
			dataIndex: 'totaltagihan',
			align: 'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: '<center>Dibayar Pasien</center>',
			width: 100,
			dataIndex: 'dibayarpasien',
			align: 'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: '<center>Jumlah Tagihan</center>',
			width: 100,
			dataIndex: 'jmltagihan',
			align: 'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: '<center>Status Tagihan</center>',
			width: 90,
			dataIndex: 'sttagihan',
			align:'center',	
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						var records = ds_tagihanpenjamin.getAt(rowIndex);
						tagihan_penjamin_form(ds_tagihanpenjamin,records);

                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						var records = ds_tagihanpenjamin.getAt(rowIndex);
						var url = BASE_URL + 'tagihan_penjamin_controller/hapus_tagihan_penjamin';
						var params = new Object({
										notagihan	: records.data['notagihan']
									});
						RH.deleteGridRecord(url, params, grid );

                    }
                }]
        }]
    });
	
	var sm = new Ext.grid.RowSelectionModel({
		singleSelect: true,
		listeners: {}
	});
	
    var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});
	
	var paging = new Ext.PagingToolbar({
		pageSize: 25,
		store: ds_tagihanpenjamin,
		displayInfo: true,
		displayMsg: 'Data Tagihan Penjamin Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.EditorGridPanel({
		id: 'grid_tagihanpenjamin',
		store: ds_tagihanpenjamin,
        vw:vw,
		cm:cm,
		sm:sm,
		tbar:[{
							text: 'Tambah',
							id: 'btn_add',
							iconCls: 'silk-add',
							width: 100,
							handler: function() {
								tagihan_penjamin_form(ds_tagihanpenjamin,null);
							}
					},{
						xtype: 'checkbox',
						id:'chb.pelayanan',
						margins: '3 10 0 5',
						listeners: {
							check : function(cb, value) {
								cari();
							}
						}						
					},{
						xtype: 'label', text: 'Tgl. Tagihan :', 
					},{
							xtype: 'datefield',
							id: 'df.tglawalan',
							format: 'd-m-Y',
							value: new Date(),
							width: 100,
							listeners:{
								select: function(field, newValue){
									cari();
								}
							}
							
					},{
						xtype: 'label',	text: 's.d',
					},{
							xtype: 'datefield',
							id: 'df.tglakhiran',
							format: 'd-m-Y',
							value: new Date(),
							width: 100,
							listeners:{
								select: function(field, newValue){
									cari();
								}
							}
					},'->',{
						xtype: 'label',	text: 'Cari Berdasarkan :',
					},{
							xtype: 'combo', fieldLabel: '', width: 150,
							id:'cb.search', autoWidth: true, store: ds_search,
							valueField: 'value', displayField: 'field', editable: false,allowBlank: true,
							triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih....',
								listeners: {
									afterrender: function () {
										var a, rec, header, index;
										for (a=2;a<=9;a++) {
											header = grid_nya.getColumnModel().getColumnHeader(a).replace('<center>','').replace('</center>','').replace('<br>','');
											index = grid_nya.getColumnModel().getDataIndex(a);
															
											rec = new ds_search.recordType({field:header, value:index});
											rec.commit();
											ds_search.add(rec);
																
											if (a==5) { Ext.getCmp('cb.search').setValue(index) }
										}
									},
									select: function () {
										cari();
									}
								}
					},{
							xtype: 'textfield',
							id: 'tf.search',
							width: 150,
							allowBlank: true,
							listeners: {
								specialkey: function(f,e){
									if (e.getKey() == e.ENTER) {
										cari();
									}
								}
							}
					},{
							xtype: 'button',
							id: 'bsearch',
							align:'left',
							iconCls: 'silk-find',
							handler: function() {
								cari();
							}
		}],
		frame: true,
		autoHeight: true,
		autoWidth: true,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		autoScroll: true,
		bbar: paging,
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
				/* var header = grid_nya.getColumnModel().getColumnHeader(columnIdx).replace('<center>','').replace('</center>','');
				var index = grid_nya.getColumnModel().getDataIndex(columnIdx);
				alert(header + ' ' + index + ' ' + grid_nya.getStore().getTotalCount()); */				
			},
		}
	});	
	                    
	var form_tagihanpenjamin = new Ext.form.FormPanel({
		id: 'form_tagihanpenjamin',
		title: 'Daftar Tagihan Penjamin',
		bodyStyle: 'padding: 5px',
		border: true,  
		autoScroll: true,
		layout: 'form',
		items: [grid_nya],
		listeners: {
			afterrender: function () {
				
			},
			beforerender: function () {				
				
			}
		}
	});
	
	SET_PAGE_CONTENT(form_tagihanpenjamin);
	
	function cari(){
			ds_tagihanpenjamin.reload({
				params: { 
					cbx: Ext.getCmp('chb.pelayanan').getValue(),
					tglawalan: Ext.getCmp('df.tglawalan').getValue(),
					tglakhiran: Ext.getCmp('df.tglakhiran').getValue(),
					key: Ext.getCmp('cb.search').getValue(),
					value: Ext.getCmp('tf.search').getValue(),
				}
			});	
	}
//========================END GRID TAGIHAN PENJAMIN===================================

//========================FORM TAGIHAN PENJAMIN===================================
	function tagihan_penjamin_form(ds_gridmain,records){	
		
		function dftPenjamin(){
			var ds_penjamin = dm_penjamin();
			
			function keyToView_penjamin(value){
				Ext.QuickTips.init();
				return '<div class="keyMasterPenjamin" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
					+ value +'</div>';
			}
			var cm_cari_penjamin = new Ext.grid.ColumnModel([
				{
					header: 'Nama Penjamin',
					dataIndex: 'nmpenjamin',
					width: 200,
					renderer: keyToView_penjamin
				},{
					header: 'Jenis Penjamin',
					dataIndex: 'nmjnspenjamin',
					width: 200
				},{
					header: 'Alamat',
					dataIndex: 'alamat',
					width: 300
				},{
					header: 'No. Telp',
					dataIndex: 'notelp',
					width: 150
				},{
					header: 'No. HP',
					dataIndex: 'nohp',
					width: 150
				},{
					header: 'Contact Person',
					dataIndex: 'nmcp',
					width: 200
				}
			]);
			var sm_cari_penjamin = new Ext.grid.RowSelectionModel({
				singleSelect: true
			});
			var vw_cari_penjamin = new Ext.grid.GridView({
				emptyText: '< Belum ada Data >'
			});
			var paging_cari_penjamin = new Ext.PagingToolbar({
				pageSize: 18,
				store: ds_penjamin,
				displayInfo: true,
				displayMsg: 'Data Dari {0} - {1} of {2}',
				emptyMsg: 'No data to display'
			});
			var cari_cari_penjamin = [new Ext.ux.grid.Search({
				iconCls: 'btn_search',
				minChars: 1,
				autoFocus: true,
				position: 'top',
				mode: 'remote',
				width: 200
			})];
			var grid_find_cari_penjamin= new Ext.grid.GridPanel({
				ds: ds_penjamin,
				cm: cm_cari_penjamin,
				sm: sm_cari_penjamin,
				view: vw_cari_penjamin,
				height: 460,
				width: 680,
				plugins: cari_cari_penjamin,
				autoSizeColumns: true,
				enableColumnResize: true,
				enableColumnHide: false,
				enableColumnMove: false,
				enableHdaccess: false,
				columnLines: true,
				loadMask: true,
				buttonAlign: 'left',
				layout: 'anchor',
				anchorSize: {
					width: 400,
					height: 400
				},
				tbar: [],
				bbar: paging_cari_penjamin,
				listeners: {
					cellclick: klik_cari_penjamin
				}
			});
			var win_find_cari_penjamin = new Ext.Window({
				title: 'Cari Penjamin',
				modal: true,
				items: [grid_find_cari_penjamin]
			}).show();

			function klik_cari_penjamin(grid, rowIdx, columnIndex, event){
				var t = event.getTarget();
				if (t.className == 'keyMasterPenjamin'){
					var rec_penjamin = ds_penjamin.getAt(rowIdx);
					
					Ext.getCmp("hf.idpenjamin").setValue(rec_penjamin.data["idpenjamin"]);
					Ext.getCmp("tf.nmpenjamin").setValue(rec_penjamin.data["nmpenjamin"]);
					Ext.getCmp("tf.nmjnspenjamin").setValue(rec_penjamin.data["nmjnspenjamin"]);
					Ext.getCmp("ta.alamat").setValue(rec_penjamin.data["alamat"]);
					Ext.getCmp("tf.notelp").setValue(rec_penjamin.data["notelp"]);
					Ext.getCmp("tf.nohp").setValue(rec_penjamin.data["nohp"]);
					Ext.getCmp("tf.nmcp").setValue(rec_penjamin.data["nmcp"]);
					
					reload_transaksi(rec_penjamin.data["idpenjamin"],null);
					
					win_find_cari_penjamin.close();
				}
			}
		}
		
		//=======transaksi ugd
		var ds_ugd = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'tagihan_penjamin_controller/get_transaksi_ugd',
				method: 'POST'
			}),
			baseParams: {
				idpenjamin : '-'
			},
			root: 'data', 
			totalProperty: 'results',
			autoLoad: false,
			fields:[{
					name: 'nonota', 
					mapping:'nonota'
				},{
					name: 'noreg', 
					mapping:'noreg'
				},{
					name: 'tglreg', 
					mapping:'tglreg'
				},{
					name: 'norm', 
					mapping:'norm'
				},{
					name: 'nmpasien', 
					mapping:'nmpasien'
				},{
					name: 'idjnskelamin', 
					mapping:'idjnskelamin'
				},{
					name: 'kdjnskelamin', 
					mapping:'kdjnskelamin'
				},{
					name: 'tgllahir', 
					mapping:'tgllahir'
				},{
					name: 'idbagian', 
					mapping:'idbagian'
				},{
					name: 'nmbagian', 
					mapping:'nmbagian'
				},{
					name: 'total', 
					mapping:'total'
				},{
					name: 'dibayarpasien', 
					mapping:'dibayarpasien'
				},{
					name: 'jumlahtagihan', 
					mapping:'jumlahtagihan'
				},{
					name: 'pilih', 
					mapping:'pilih',
					type: 'bool'
				}]
			});
		
		var vw_ugd = new Ext.grid.GridView({
			emptyText: '<Belum ada Data>'
		});
		var sm_ugd = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var cm_ugd = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header: 'No Nota',
				width: 90,
				dataIndex: 'nonota',
				sortable: true,
			},{
				header: 'No Registrasi',
				width: 90,
				dataIndex: 'noreg',
				sortable: true,
			},{
				header: 'Tgl. Registrasi',
				width: 80,
				dataIndex: 'tglreg',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y'
			},{
				header: 'No. RM',
				width: 60,
				dataIndex: 'norm',
				sortable: true,
			},{
				header: 'Nama Pasien',
				width: 150,
				dataIndex: 'nmpasien',
				sortable: true,
			},{
				header: '(L/P)',
				width: 50,
				dataIndex: 'kdjnskelamin',
				sortable: true,
			},{
				header: 'Tgl. Lahir',
				width: 80,
				dataIndex: 'tgllahir',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y'
			},{
				header: 'Unit Pelayanan',
				width: 105,
				dataIndex: 'nmbagian',
				sortable: true,
			},{	
				header: 'Total<br>Transaksi',
				width: 93,
				dataIndex: 'total',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			},{	
				header: 'Dibayar<br>Pasien',
				width: 93,
				dataIndex: 'dibayarpasien',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			},{	
				header: 'Jumlah<br>Tagihan',
				width: 93,
				dataIndex: 'jumlahtagihan',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			},{
				xtype: 'checkcolumn',
				header: '<center>Pilih</center>',
				width: 50,
				dataIndex: 'pilih',
				sortable: true,
				processEvent : function(name, e, grid, rowIndex, colIndex){
						 if (name == 'mousedown') {
							if (records) {
								return;
							}
							var record = grid.store.getAt(rowIndex);
							record.set(this.dataIndex, !record.data[this.dataIndex]);
							pilih_transaksi('ugd');
						}
				}
			},
		]);
		var grid_ugd = new Ext.grid.EditorGridPanel({
			ds: ds_ugd,
			cm: cm_ugd,
			sm: sm_ugd,
			view: vw_ugd,
			height: 150,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: [{
			text: '<span style="margin-left:0px;font-size:12px;">Jumlah 1:</span>', style: 'margin-left: 570px',
		},{
			xtype: 'numericfield',
			id: 'tf.sumtotalugd',
			width:90,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			xtype: 'numericfield',
			id: 'tf.sumdibayarugd',
			width:90,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			xtype: 'numericfield',
			id: 'tf.sumtagihanugd',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		}],
		}); 
		//=======eof transaksi ugd
		
		//=======transaksi ri
		var ds_ri = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'tagihan_penjamin_controller/get_transaksi_ri',
				method: 'POST'
			}),
			baseParams: {
				idpenjamin : '-'
			},
			root: 'data', 
			totalProperty: 'results',
			autoLoad: false,
			fields:[{
					name: 'nona', 
					mapping:'nona'
				},{
					name: 'noreg', 
					mapping:'noreg'
				},{
					name: 'tglmasuk', 
					mapping:'tglmasuk'
				},{
					name: 'tglkeluar', 
					mapping:'tglkeluar'
				},{
					name: 'norm', 
					mapping:'norm'
				},{
					name: 'nmpasien', 
					mapping:'nmpasien'
				},{
					name: 'idjnskelamin', 
					mapping:'idjnskelamin'
				},{
					name: 'kdjnskelamin', 
					mapping:'kdjnskelamin'
				},{
					name: 'tgllahir', 
					mapping:'tgllahir'
				},{
					name: 'idbagian', 
					mapping:'idbagian'
				},{
					name: 'nmbagian', 
					mapping:'nmbagian'
				},{
					name: 'total', 
					mapping:'total'
				},{
					name: 'dibayarpasien', 
					mapping:'dibayarpasien'
				},{
					name: 'jumlahtagihan', 
					mapping:'jumlahtagihan'
				},{
					name: 'pilih', 
					mapping:'pilih',
					type: 'bool'
				}]
			});
		
		var vw_ri = new Ext.grid.GridView({
			emptyText: '<Belum ada Data>'
		});
		var sm_ri = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var cm_ri = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header: 'No Nota Akhir',
				width: 90,
				dataIndex: 'nona',
				sortable: true,
			},{
				header: 'No Registrasi',
				width: 90,
				dataIndex: 'noreg',
				sortable: true,
			},{
				header: 'Tgl. Masuk',
				width: 80,
				dataIndex: 'tglmasuk',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y'
			},{
				header: 'Tgl. Keluar',
				width: 80,
				dataIndex: 'tglkeluar',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y'
			},{
				header: 'No. RM',
				width: 60,
				dataIndex: 'norm',
				sortable: true,
			},{
				header: 'Nama Pasien',
				width: 150,
				dataIndex: 'nmpasien',
				sortable: true,
			},{
				header: '(L/P)',
				width: 50,
				dataIndex: 'kdjnskelamin',
				sortable: true,
			},{
				header: 'Tgl. Lahir',
				width: 80,
				dataIndex: 'tgllahir',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y'
			},{
				header: 'Ruangan',
				width: 105,
				dataIndex: 'nmbagian',
				sortable: true,
			},{	
				header: 'Total<br>Transaksi',
				width: 93,
				dataIndex: 'total',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			},{	
				header: 'Dibayar<br>Pasien',
				width: 93,
				dataIndex: 'dibayarpasien',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			},{	
				header: 'Jumlah<br>Tagihan',
				width: 93,
				dataIndex: 'jumlahtagihan',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			},{
				xtype: 'checkcolumn',
				header: '<center>Pilih</center>',
				width: 50,
				dataIndex: 'pilih',
				sortable: true,
				processEvent : function(name, e, grid, rowIndex, colIndex){
						 if (name == 'mousedown') {
							if (records) {
								return;
							}
							var record = grid.store.getAt(rowIndex);
							record.set(this.dataIndex, !record.data[this.dataIndex]);
							pilih_transaksi('ri');
						}
				}
			},
		]);
		var grid_ri = new Ext.grid.EditorGridPanel({
			ds: ds_ri,
			cm: cm_ri,
			sm: sm_ri,
			view: vw_ri,
			height: 150,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: [{
			text: '<span style="margin-left:0px;font-size:12px;">Jumlah 1:</span>', style: 'margin-left: 570px',
		},{
			xtype: 'numericfield',
			id: 'tf.sumtotalri',
			width:90,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			xtype: 'numericfield',
			id: 'tf.sumdibayarri',
			width:90,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			xtype: 'numericfield',
			id: 'tf.sumtagihanri',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		}],
		}); 
		//=======eof transaksi ri

		var f_tagihan_penjamin = new Ext.form.FormPanel({
					id: 'fp.tgpenjamin',
					region: 'center',
					bodyStyle: 'padding: 10px;',		
					border: false, frame: true,
					height: 550,
					width: 1250,
					autoScroll: true,
					tbar: [{
					text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
					handler: function() {
						simpan_tagihan();
					}
				},{
					text: 'Hapus', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px',
					hidden: true, handler: function() {

					}
				},{
					text: 'Cetak', id: 'bt.cetak',  iconCls: 'silk-printer', style: 'marginLeft: 10px',
					handler: function() {
						cetak_tagihan();
					}
				},{
					text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
					handler: function() {
						w_tagihan_penjamin.close();
				}
				}],
				items:[{
					xtype: 'fieldset', title: 'Data Tagihan', layout: 'column', border: true,
					defaults: { labelWidth: 170, labelAlign: 'right', }, 
					items:[{
						columnWidth: 0.50, border: true, layout: 'form',
							items:[{
									xtype: 'compositefield',
									id: 'cf.nmpenjamin',
									fieldLabel: 'Nama Penjamin',
									items:[{
										xtype: 'hidden', id:'hf.idpenjamin',
										value: (records) ? records.data.idpenjamin:null
									},{
										xtype: 'textfield',
										id: 'tf.nmpenjamin',
										width: 250,
										disabled: true,
										style: 'opacity: 0.5',
										value: (records) ? records.data.nmpenjamin:null
									},{
										xtype: 'button', iconCls: 'silk-find',
										width: 30,id: 'btn.penjamin',
										handler: function(){
											dftPenjamin();
										}
									}]
								},{
									xtype: 'textfield', id: 'tf.nmjnspenjamin',width: 250, fieldLabel: 'Jenis Penjamin',disabled: true,
									style: 'opacity: 0.5', value: (records) ? records.data.nmjnspenjamin:null
								},{
									xtype: 'textarea', id: 'ta.alamat',width: 250, height: 50, fieldLabel: 'Alamat',disabled: true,
									style: 'opacity: 0.5', value: (records) ? records.data.alamat:null
								},{
									xtype: 'compositefield',
									fieldLabel: 'No. Telepon / HP',
									items:[{
										xtype: 'textfield', id: 'tf.notelp', width: 117, disabled: true,
										style: 'opacity: 0.5', value: (records) ? records.data.notelp:null
									},{
										xtype: 'label', text: '/', style:'font-size: 12px',
									},{
										xtype: 'textfield', id: 'tf.nohp', width: 117,disabled: true,
										style: 'opacity: 0.5', value: (records) ? records.data.nohp:null
									}]
								},{
									xtype: 'textfield', id: 'tf.nmcp',width: 250, fieldLabel: 'Contact Person',disabled: true,
									style: 'opacity: 0.5', value: (records) ? records.data.nmcp:null
								}]
					},{
						columnWidth: 0.50, border: true, layout: 'form',
							items:[{
								xtype: 'textfield',
								id: 'tf.notagihan',
								width: 250,
								fieldLabel: 'No. Tagihan',
								disabled: true, 
								emptyText:'Otomatis',
								style: 'font-weight: bold;opacity: 0.5',
								value: (records) ? records.data.notagihan:null
							},{
								xtype: 'datefield', 
								id: 'df.tgltagihan', 
								width: 100,
								value: (records) ? records.data.tgltagihan:new Date(),
								format: 'd-m-Y',
								fieldLabel: 'Tgl. Tagihan'
							},{
								xtype: 'textfield',
								id: 'tf.perihal',
								width: 250,
								fieldLabel: 'Perihal',
								value: (records) ? records.data.perihal:null
							},{
								xtype: 'textfield',
								id: 'tf.lampiran',
								width: 250,
								fieldLabel: 'Lampiran',
								value: (records) ? records.data.lampiran:null
							},{
								xtype: 'datefield', 
								id: 'df.tgljatuhtempo', 
								width: 100,
								value: (records) ? records.data.tgljatuhtempo:new Date(),
								format: 'd-m-Y',
								fieldLabel: 'Tgl. Jatuh Tempo'
							},{
								xtype: 'combo', fieldLabel: 'Approval (Keuangan)',
								id:'cb.app', width: 200, store: ds_appkeu,
								valueField: 'nmset', displayField: 'nmset', editable: false,allowBlank: true,
								triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
								emptyText:'Pilih....', value: (records) ? records.data.approval:null
							}]
					}]
				},{
					xtype: 'fieldset', title: 'Transaksi Rawat Jalan / UGD', layout: 'form', border: true,
					items:[grid_ugd]
				},{
					xtype: 'fieldset', title: 'Transaksi Rawat Inap', layout: 'form', border: true,
					items:[grid_ri]
				},{
					xtype: 'fieldset', title: 'Summary', layout: 'column', border: true,
					defaults: { labelWidth: 170, labelAlign: 'right', }, 
					items:[{
						columnWidth: 0.50, border: true, layout: 'form',
							items:[{
								xtype: 'textarea',
								id: 'ta.infobankharbun',
								width: 250,
								height: 75,
								fieldLabel: 'Dibayar Ke Rekening Bank',
								disabled: true, 
								style: 'font-weight: bold;opacity: 0.5',
							}]
						},{
						columnWidth: 0.50, border: true, layout: 'form',
							items:[{
								xtype: 'numericfield',
								id: 'tf.total12',
								width: 250,
								fieldLabel: 'Total (Jumlah 1 + Jumlah 2)',
								decimalSeparator: ',', 
								decimalPrecision: 0, 
								alwaysDisplayDecimals: true,
								useThousandSeparator: true, 
								thousandSeparator: '.',
								style: 'font-weight: bold;opacity: 0.5',
							},{
								xtype: 'textfield',
								id: 'tf.statustagihan',
								width: 250,
								fieldLabel: 'Status Tagihan',
								disabled: true, 
								style: 'font-weight: bold;opacity: 0.5',
							},{
								xtype: 'textfield',
								id: 'tf.username',
								width: 250,
								fieldLabel: 'User Input',
								disabled: true, 
								style: 'font-weight: bold;opacity: 0.5',
								value: USERNAME
							},{
								xtype: 'hidden',
								id: 'tf.userid',
								width: 250,
								value: USERID
							}]
						}]
				}],
				listeners: {
					afterrender: function () {
						var rec_tagihan = (records) ? records.data.notagihan:null;
						var rec_idpenjamin = (records) ? records.data.idpenjamin:null;
						reload_transaksi(rec_idpenjamin,rec_tagihan);
						
						Ext.Ajax.request({
							url: BASE_URL + 'tagihan_penjamin_controller/getrekbank',
							params: { 
								idklpsetting: 18,
								idset: 36
							},
							success: function(response){
								Ext.getCmp('ta.infobankharbun').setValue(response.responseText)
							},
							failure : function(){
							
							}
						});
					},
					beforerender: function () {				
						
					}
				}
		});
		
		var w_tagihan_penjamin = new Ext.Window({
				title: 'Tagihan Penjamin',
				modal: true,
				items: [f_tagihan_penjamin]
			}).show();
			
		function reload_transaksi(idpenjamin,records_notagihan){
			ds_ugd.setBaseParam("idpenjamin",idpenjamin);
			ds_ugd.setBaseParam("notagihan",records_notagihan);
			ds_ugd.reload({
				/* params: { 
					idpenjamin: idpenjamin,
				}, */
				scope   : this,
				callback: function(record, operation, success) {
					var sumtotal = 0, sumdibayar = 0, sumtagihan = 0;
					ds_ugd.each(function(rec){
						if (rec.get('pilih') == true){
							sumtotal += parseFloat(rec.get('total'));
							sumdibayar += parseFloat(rec.get('dibayarpasien'));
							sumtagihan += parseFloat(rec.get('jumlahtagihan'));
						}
					});
					Ext.getCmp('tf.sumtotalugd').setValue(sumtotal);
					Ext.getCmp('tf.sumdibayarugd').setValue(sumdibayar);
					Ext.getCmp('tf.sumtagihanugd').setValue(sumtagihan);
					Ext.getCmp('tf.total12').setValue(Ext.getCmp('tf.sumtagihanugd').getValue() + Ext.getCmp('tf.sumtagihanri').getValue());
					
					var i = ds_ugd.getCount()-1; 
					for (i; i>=0; i--) {
						if (ds_ugd.getAt(i).get('pilih') == false) {
							if (records) {
								ds_ugd.removeAt(i);
							}
						}
					}
				}
			});
			
			ds_ri.setBaseParam("idpenjamin",idpenjamin);
			ds_ri.setBaseParam("notagihan",records_notagihan);
			ds_ri.reload({
				/* params: { 
					idpenjamin: idpenjamin,
				}, */
				scope   : this,
				callback: function(record, operation, success) {
					var sumtotal = 0, sumdibayar = 0, sumtagihan = 0;
					ds_ri.each(function(rec){
						if (rec.get('pilih') == true){
							sumtotal += parseFloat(rec.get('total'));
							sumdibayar += parseFloat(rec.get('dibayarpasien'));
							sumtagihan += parseFloat(rec.get('jumlahtagihan'));
						}
					});
					Ext.getCmp('tf.sumtotalri').setValue(sumtotal);
					Ext.getCmp('tf.sumdibayarri').setValue(sumdibayar);
					Ext.getCmp('tf.sumtagihanri').setValue(sumtagihan);
					Ext.getCmp('tf.total12').setValue(Ext.getCmp('tf.sumtagihanugd').getValue() + Ext.getCmp('tf.sumtagihanri').getValue());
				
					var i = ds_ri.getCount()-1; 
					for (i; i>=0; i--) {
						if (ds_ri.getAt(i).get('pilih') == false) {
							if (records) {
								ds_ri.removeAt(i);
							}
						}
					}
				}
			});
		}
		
		function pilih_transaksi(type){

			var sumtotal = 0, sumdibayar = 0, sumtagihan = 0;
			if (type=='ugd') {
				tfsumtotal = 'tf.sumtotalugd';
				tfsumdibayar = 'tf.sumdibayarugd';
				tfsumtagihan = 'tf.sumtagihanugd';
				grid_ugd.getStore().each(function(rec){
					rowData = rec.data;
					if (rowData['pilih']) {
						sumtotal += parseFloat(rowData['total']);
						sumdibayar += parseFloat(rowData['dibayarpasien']);
						sumtagihan += parseFloat(rowData['jumlahtagihan']);
					}
				});
			} else if (type=='ri') {
				tfsumtotal = 'tf.sumtotalri';
				tfsumdibayar = 'tf.sumdibayarri';
				tfsumtagihan = 'tf.sumtagihanri';
				grid_ri.getStore().each(function(rec){
					rowData = rec.data;
					if (rowData['pilih']) {
						sumtotal += parseFloat(rowData['total']);
						sumdibayar += parseFloat(rowData['dibayarpasien']);
						sumtagihan += parseFloat(rowData['jumlahtagihan']);
					}
				});
			}
			
			Ext.getCmp(tfsumtotal).setValue(sumtotal);
			Ext.getCmp(tfsumdibayar).setValue(sumdibayar);
			Ext.getCmp(tfsumtagihan).setValue(sumtagihan);
			Ext.getCmp('tf.total12').setValue(Ext.getCmp('tf.sumtagihanugd').getValue() + Ext.getCmp('tf.sumtagihanri').getValue());

		}
		
		function get_tagihan_ugd() {
			var checkcount = 0;
			var par='';
			var count= 1;
			var endpar = ';';

			grid_ugd.getStore().each(function(rec){ // ambil seluruh grid ugd
				var rowcheck = rec.data; 
				if (rowcheck['pilih']) {
					checkcount = checkcount + 1;
				}

			});
		
			grid_ugd.getStore().each(function(rec){ // ambil seluruh grid ugd
				var rowData = rec.data; 
				if (count == checkcount) {
					endpar = ''
				}
				var pilih = (rowData['pilih']) ? 1 : 0;	
				if (pilih) {
					par +=  rowData['nonota'] + ':' +
							rowData['total'] + ':' +
							rowData['dibayarpasien'] + ':' +
							pilih + ':' + // pilih
							endpar;
											
					count = count+1;
				}
			});
			
			return par;
		}
		
		function get_tagihan_ri() {
			var checkcount = 0;
			var par='';
			var count= 1;
			var endpar = ';';
			
			grid_ri.getStore().each(function(rec){ // ambil seluruh grid ugd
				var rowcheck = rec.data; 
				if (rowcheck['pilih']) {
					checkcount = checkcount + 1;
				}

			});
		
			grid_ri.getStore().each(function(rec){ // ambil seluruh grid ri
				var rowData = rec.data; 
				if (count == checkcount) {
					endpar = ''
				}
				var pilih = (rowData['pilih']) ? 1 : 0;			
				if (pilih) {
					par +=  rowData['nona'] + ':' +
							rowData['total'] + ':' +
							rowData['dibayarpasien'] + ':' +
							pilih + ':' + // pilih
							endpar;
											
					count = count+1;
				}
			});
			
			return par;
		}
		
		function simpan_tagihan(){
				if (!RH.getCompValue('hf.idpenjamin')) {
					Ext.MessageBox.alert('Informasi','Lengkapi ' + Ext.getCmp('cf.nmpenjamin').fieldLabel);
					return;
				}
				if (!RH.getCompValue('cb.app')) {
					Ext.MessageBox.alert('Informasi','Lengkapi ' + Ext.getCmp('cb.app').fieldLabel);
					return;
				}
				
				var waitmsgsimpan = Ext.MessageBox.wait('Menyimpan Data...', 'Informasi');
				
				var dataugd = get_tagihan_ugd();
				var datari = get_tagihan_ri();
				
				var functionphp, alert;
				
				if (records) {
					functionphp = 'update_tagihan'; alert = 'Ubah';
				} else {
					functionphp = 'insert_tagihan'; alert = 'Simpan';
				}
				
				Ext.Ajax.request({
					url: BASE_URL + 'tagihan_penjamin_controller/' + functionphp,
					params: {
						notagihan 	  : RH.getCompValue('tf.notagihan'),
						tgltagihan 	  : RH.getCompValue('df.tgltagihan'),
						idpenjamin 	  : RH.getCompValue('hf.idpenjamin'),
						perihal  	  : RH.getCompValue('tf.perihal'),
						lampiran	  : RH.getCompValue('tf.lampiran'),
						tgljatuhtempo : RH.getCompValue('df.tgljatuhtempo'),
						approval	  : RH.getCompValue('cb.app'),
						userid	  	  : RH.getCompValue('tf.userid'),
						
						dataugd		  : dataugd,
						datari		  : datari,
					},
					success: function(response){
						waitmsgsimpan.hide();
						Ext.MessageBox.alert('Informasi',alert + ' Data Berhasil');
						RH.setCompValue('tf.notagihan',(records) ? records.data.notagihan:response.responseText);
						ds_gridmain.reload();
						
						if (alert=='Simpan') {
							w_tagihan_penjamin.close();
						}
					},
					failure : function(){
						waitmsgsimpan.hide();
						Ext.MessageBox.alert('Informasi', alert + ' Data Gagal');
					}
				});
		
		}

		
		function cetak_tagihan(){
			if (!RH.getCompValue('hf.idpenjamin')) {
				Ext.MessageBox.alert('Informasi','Lengkapi ' + Ext.getCmp('cf.nmpenjamin').fieldLabel);
				return;
			}
			if (!RH.getCompValue('cb.app')) {
				Ext.MessageBox.alert('Informasi','Lengkapi ' + Ext.getCmp('cb.app').fieldLabel);
				return;
			}
			var idpenjamin = Ext.getCmp('hf.idpenjamin').getValue();
			var dataugd = '';
			var datari = '';

			grid_ugd.getStore().each(function(rec){ // ambil seluruh grid ugd
				var rowData = rec.data; 
				var pilih = (rowData['pilih']) ? 1 : 0;	
				if (pilih) {
					dataugd +=  rowData['nonota'] + '-';
				}
			});
			
			grid_ri.getStore().each(function(rec){ // ambil seluruh grid ri
				var rowData = rec.data; 
				var pilih = (rowData['pilih']) ? 1 : 0;			
				if (pilih) {
					datari +=  rowData['nona'] + '-';
				}
			});
			
			//jika ugd dan ri kosong
			if(dataugd == '' && datari == '')
			{
				Ext.MessageBox.alert('Informasi','tidak ada tagihan yang dipilih ');
				return;
			}
			
			if(dataugd == '') dataugd = '-';
			if(datari == '') datari = '-';
			
			RH.ShowReport( BASE_URL + 'tagihan_penjamin_controller/cetak_tagihan_penjamin/'+ idpenjamin + '/' + dataugd + '/' + datari);
			
			
		}

		
	}
//========================END FORM TAGIHAN PENJAMIN===================================

//========================FORM BAYAR PENJAMIN===================================
	function bayar_penjamin_form(ds_gridmain,records){
	  function fnKeyPostingJurnal(value, value2, value3){
  	
	  	var val3 = value3.data;
	  	var nokuitansipg = val3.nokuitansipg;
	  	var kdjurnal = val3.kdjurnal;

	  	if(kdjurnal != null){
	  		return 'sudah posting';
	  	}

	  	Ext.QuickTips.init();
	    return '<div class="keyPostingJurnal" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">posting</div>';
	  }

		//====================GRID HISTORI BAYAR PENJAMIN
			var ds_grid_histori = new Ext.data.JsonStore({
					proxy: new Ext.data.HttpProxy({
					url : BASE_URL + 'tagihan_penjamin_controller/get_kuitansipenjamin',
						method: 'POST'
					}),
					totalProperty: 'results',
					root: 'data',
					baseParams: {
						notagihan:records.data.notagihan
					},
					autoLoad: true,
					fields: [{
						name: 'nokuitansipenjamin',
						mapping: 'nokuitansipenjamin'
					},{
						name: 'tglkuitansipenjamin',
						mapping: 'tglkuitansipenjamin'
					},{
						name: 'userid',
						mapping: 'userid'
					},{
						name: 'notagihan',
						mapping: 'notagihan'
					},{
						name: 'idcarabayar',
						mapping: 'idcarabayar'
					},{
						name: 'idstkuitansi',
						mapping: 'idstkuitansi'
					},{
						name: 'jumlah',
						mapping: 'jumlah'
					},{
						name: 'tgljaminput',
						mapping: 'tgljaminput'
					},{
						name: 'nmlengkap',
						mapping: 'nmlengkap'
					},{
						name: 'nmcarabayar',
						mapping: 'nmcarabayar'
					},{
						name: 'nmstkuitansi',
						mapping: 'nmstkuitansi'
					},{
						name: 'nmpenjamin',
						mapping: 'nmpenjamin'
					},{
						name: 'kdpenjamin',
						mapping: 'kdpenjamin'
					},{
						name: 'kdjurnal',
						mapping: 'kdjurnal'
					}]
				});
				
			ds_grid_histori.on('load', function(){
				var jmlbayar = 0;
				ds_grid_histori.each(function(rec){
					jmlbayar += parseFloat(rec.get('jumlah')); 
				
				});
				Ext.getCmp("tf.totbayar").setValue(jmlbayar);
			});
			
			var sm_histori = new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners: {}
			});
				
			var cm_histori = new Ext.grid.ColumnModel({
				// specify any defaults for each column
				defaults: {
					sortable: true // columns are not sortable by default           
				},
				columns: [
				new Ext.grid.RowNumberer(),
				{
					header: '<center>No. Pembayaran</center>',
					width: 120,
					dataIndex: 'nokuitansipenjamin',
					align:'center',	
				},{
					header: '<center>Tgl. Pembayaran</center>',
					width: 150,
					dataIndex: 'tglkuitansipenjamin',
				},{
					header: '<center>Posting</center>',
					width: 90,
					dataIndex: 'kdjurnal',
					sortable: true,
					renderer: fnKeyPostingJurnal
				}
				,{
					header: '<center>User Input</center>',
					width: 100,
					dataIndex: 'nmlengkap',
				},{
					header: '<center>Cara Bayar</center>',
					width: 120,
					dataIndex: 'nmcarabayar',
					align:'center',	
				},{
					header: '<center>Jumlah Bayar</center>',
					width: 100,
					dataIndex: 'jumlah',
					align:'right',
					xtype: 'numbercolumn', format:'0,000',
				},{
					xtype: 'checkcolumn',
					header: '<center>Pilih</center>',
					width: 50,
					dataIndex: 'pilih',
					sortable: true,
					processEvent : function(name, e, grid, rowIndex, colIndex){
							 if (name == 'mousedown') {
								var record = grid.store.getAt(rowIndex);
								record.set(this.dataIndex, !record.data[this.dataIndex]);
							}
					}
				}]
			});
			
			var vw_histori = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});

			var grid_nya_histori = new Ext.grid.EditorGridPanel({
				id: 'grid_bayar_histori',
				store: ds_grid_histori,
				vw:vw_histori,
				cm:cm_histori,
				sm:sm_histori,
				bbar: [{
						text: 'Total Bayar',
					},
					{
						xtype: 'numericfield', 							
						fieldLabel: 'Total Bayar', 
						id: 'tf.totbayar',
						width: 125,
						maskRe: /[0-9]/,
						decimalSeparator: ',', 
						decimalPrecision: 0, 
						alwaysDisplayDecimals: true,
						useThousandSeparator: true, 
						thousandSeparator: '.',
						disabled: true, 
					}],
				frame: true,
				height: 145,
				autoWidth: true,
				loadMask: true,
				forceFit: true,
				buttonAlign: 'left',
				autoScroll: true,
				listeners: {
					cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
						var t = e.getTarget();
		    
				    if (t.className == 'keyPostingJurnal')
				    {
		  	      var obj                	= ds_grid_histori.getAt(rowIdx);
				      var nokuitansipenjamin 	= obj.get("nokuitansipenjamin");
				      var tglkuitansipenjamin = obj.get("tglkuitansipenjamin");
				      var kdpenjamin      		= obj.get("kdpenjamin");
				      var nmpenjamin      		= obj.get("nmpenjamin");
				      var kdjurnal            = obj.get("kdjurnal");
							var ds_jurnal_tp        = new Ext.data.JsonStore({
								proxy: new Ext.data.HttpProxy({
									url: BASE_URL + 'tagihan_penjamin_controller/get_tagihan_jurnaling',
									method: 'POST'
								}),
								root: 'data', 
								totalProperty: 'results',
								autoLoad: true,
								baseParams: {
									nokuitansipenjamin: nokuitansipenjamin
								},
								fields:[
								{ name: 'idakun', mapping: 'idakun', }, 
								{ name: 'kdakun', mapping: 'kdakun', }, 
								{ name: 'nmakun', mapping: 'nmakun', }, 
								{ name: 'noreff', mapping: 'noreff', }, 
								{ name: 'debit', mapping: 'debit', }, 
								{ name: 'kredit', mapping: 'kredit', }
								],
							});

							the_debit = 0;
							the_kredit = 0;
							ds_jurnal_tp.load({
			          scope   : this,
			          callback: function(records, operation, success) {
			            //hitung total debit & kredit
			            total_debit = 0;
			            total_kredit = 0;
			            ds_jurnal_tp.each(function (rec) { 
			              total_debit += parseFloat(rec.get('debit')); 
			              total_kredit += parseFloat(rec.get('kredit')); 
			            });
			            Ext.getCmp("info.total_debit").setValue(total_debit);            
			            Ext.getCmp("info.total_kredit").setValue(total_kredit);
			          }
			        });

						  var grid_detail_jurnal = new Ext.grid.GridPanel({
						    id: 'grid_detail_jurnal',
						    store: ds_jurnal_tp,
						    view: new Ext.grid.GridView({emptyText: '< Tidak ada Data >'}),
						    frame: true,
						    loadMask: true,
						    height: 200,
						    layout: 'anchor',
						    bbar: [
						      '->',
						      {
						        xtype: 'numericfield',
						        thousandSeparator:',',
						        id: 'info.total_debit',
						        width:100,
						        readOnly:true,
						        value:0,
						        align:'right',
						        decimalPrecision:0,
						    },{
						        xtype: 'numericfield',
						        thousandSeparator:',',
						        id: 'info.total_kredit',
						        width:100,
						        readOnly:true,
						        value:0,
						        align:'right',
						        decimalPrecision:0,
						    }],
						    columns: [new Ext.grid.RowNumberer(),
						    {
						      header: '<center>Kode</center>',
						      width: 100,
						      dataIndex: 'kdakun',
						      sortable: true,
						      align:'center',
						    },{
						      header: '<center>Nama Akun</center>',
						      width: 180,
						      dataIndex: 'nmakun',
						      sortable: true,
						      align:'left',
						    },{
						      header: '<center>No.Reff</center>',
						      width: 100,
						      dataIndex: 'noreff',
						      sortable: true,
						      align:'center',
						    },{
						      header: '<center>Debit</center>',
						      width: 100,
						      dataIndex: 'debit',
						      sortable: true,
						      xtype: 'numbercolumn', format:'0,000', align:'right',
						    },{
						      header: '<center>Kredit</center>',
						      width: 100,
						      dataIndex: 'kredit',
						      sortable: true,
						      xtype: 'numbercolumn', format:'0,000', align:'right',
						    }]
						  });

							var posting_form = new Ext.form.FormPanel({
								xtype:'form',
								id: 'frm.posting',
								buttonAlign: 'left',
								autoScroll: true,
								labelWidth: 165, labelAlign: 'right',
								monitorValid: true,
								height: 250, width: 650,
								layout: {
									type: 'form',
									pack: 'center',
									align: 'center'
								},
								frame: true,
								tbar: [{
									text: 'Posting', iconCls:'silk-save', id: 'btn.posting', style: 'marginLeft: 5px',
									handler: function() {
		                  var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
		                  var nokuitansipenjamin_posting = nokuitansipenjamin;
		                  var tglkuitansipenjamin_posting = tglkuitansipenjamin;
		                  var kdpenjamin_posting = kdpenjamin;
		                  var nmpenjamin_posting = nmpenjamin;
		                  var nominal_jurnal = Ext.getCmp("info.total_debit").getValue(); //nominal debit
					            var nominal_jurnal_kredit = Ext.getCmp("info.total_kredit").getValue(); //nominal kredit		          
		                  var post_grid ='';
		                  var count= 1;
		                  var endpar = ';';
		                    
		                  grid_detail_jurnal.getStore().each(function(rec){ // ambil seluruh grid
		                    var rowData = rec.data; 
		                    if (count == grid_detail_jurnal.getStore().getCount()) {
		                      endpar = ''
		                    }
		                    post_grid += rowData['idakun'] + '^' + 
		                           rowData['kdakun'] + '^' + 
		                           rowData['nmakun']  + '^' + 
		                           rowData['noreff']  + '^' + 
		                           rowData['debit'] + '^' + 
		                           rowData['kredit']  + '^' +
		                        endpar;
		                    count = count+1;
		                  });

		                  if(nokuitansipenjamin_posting == '' || tglkuitansipenjamin_posting == ''){
		                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
		                  }
		                  else if(nominal_jurnal != nominal_jurnal_kredit){
		                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
		                  }
		                  else{
		                    
		                    Ext.Ajax.request({
		                      url: BASE_URL + 'tagihan_penjamin_controller/posting_tagihan_penjamin',
		                      params: {
		                        nokuitansipenjamin_posting   : nokuitansipenjamin_posting,
		                        tglkuitansipenjamin_posting  : tglkuitansipenjamin_posting,
		                        kdpenjamin           				 : kdpenjamin_posting,
		                        nmpenjamin           				 : nmpenjamin_posting,
		                        nominal_jurnal               : nominal_jurnal,
		                        post_grid                    : post_grid,
		                        userid                       : USERID,
		                      },
		                      success: function(response){
		                        waitmsg.hide();

		                        obj = Ext.util.JSON.decode(response.responseText);
		                        if(obj.success === true){
		                          Ext.getCmp('btn.posting').disable();
		                        }else{
		                          Ext.getCmp('btn.posting').enable();  
		                        }

		                        Ext.MessageBox.alert('Informasi', obj.message);
		                        ds_grid_histori.reload();
		                        wPostingForm.close();
		                      }
		                    });
		                   
		                  }

		                }
								},{
									text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
									handler: function() {
										wPostingForm.close();
									}
								}],	
								items: [
									grid_detail_jurnal,
								]
							});

							if(kdjurnal == null)
			          Ext.getCmp('btn.posting').enable();
			        else
			          Ext.getCmp('btn.posting').disable();
			        
							var wPostingForm = new Ext.Window({
								title: 'Posting Pembayaran Penjamin',
								modal: true, closable:false,
								items: [posting_form]
							});
						
							wPostingForm.show();

				    }
					}
				}
			});
			//====================
			
		var f_bayar_penjamin = new Ext.form.FormPanel({
					id: 'f_bayar_penjamin',
					region: 'center',
					bodyStyle: 'padding: 5px;',		
					border: false, frame: true,
					height: 550,
					width: 1000,
					autoScroll: true,
					tbar: [{
					text: 'Baru', iconCls:'silk-add', id: 'btn_baru', style: 'marginLeft: 5px',
					handler: function() {
						RH.setCompValue('tf.nokuitansipenjamin',null);
						RH.setCompValue('df.tglkuitansipenjamin',new Date());
						RH.setCompValue('cb.app',null);
						RH.setCompValue('cb.carabayar',null);
						RH.setCompValue('tf.jmlbayar',null);
						
						Ext.getCmp('btn_simpan').enable();
					}
				},{
					text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
					handler: function() {
						simpan_bayar_penjamin();
					}
				},{
					text: 'Cetak', id: 'bt.cetak',  iconCls: 'silk-printer', style: 'marginLeft: 10px',
					disabled: (records) ? false:true,
					handler: function() {
						
					}
				},{
					text: 'Hapus', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px',
					disabled: (records) ? false:true,
					handler: function() {
						hapus_bayar_penjamin();
					}
				},{
					text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
					handler: function() {
						win_bayar_penjamin.close();
					}
				}],
				items:[{
					xtype: 'fieldset', title: '', layout: 'column', border: false,
					defaults: { labelWidth: 150, labelAlign: 'right', }, 
					items:[{
						columnWidth: 0.50, border: true, layout: 'form',
						items:[{
							xtype: 'fieldset',
							id: 'fs.datatagihan',
							title: 'Data Tagihan',
							layout: 'form',
							items: [{
								xtype: 'textfield',
								id: 'tf.notagihan',
								width: 270,
								fieldLabel: 'No. Tagihan',
								disabled: true, 
								style: 'font-weight: bold;opacity: 0.5',
								value: (records) ? records.data.notagihan:null
							},{
								xtype: 'datefield', 
								id: 'df.tgltagihan', 
								width: 100,
								value: (records) ? records.data.tgltagihan:new Date(),
								format: 'd-m-Y',
								fieldLabel: 'Tgl. Tagihan',
								disabled: true,
							},{
								xtype: 'datefield', 
								id: 'df.tgljatuhtempo', 
								width: 100,
								value: (records) ? records.data.tgljatuhtempo:new Date(),
								format: 'd-m-Y',
								fieldLabel: 'Tgl. Jatuh Tempo',
								disabled: true,
							},{
								xtype: 'textfield',
								id: 'tf.nmpenjamin',
								fieldLabel: 'Nama Penjamin',
								width: 270,
								disabled: true,
								style: 'opacity: 0.5',
								value: (records) ? records.data.nmpenjamin:null
							},{
								xtype: 'textfield', id: 'tf.nmjnspenjamin',width: 270, fieldLabel: 'Jenis Penjamin',disabled: true,
								style: 'opacity: 0.5', value: (records) ? records.data.nmjnspenjamin:null
							},{
								xtype: 'textarea', id: 'ta.alamat',width: 270, height: 74, fieldLabel: 'Alamat',disabled: true,
								style: 'opacity: 0.5', value: (records) ? records.data.alamat:null
							},{
								xtype: 'compositefield',
								fieldLabel: 'No. Telepon / HP',
								items:[{
									xtype: 'textfield', id: 'tf.notelp', width: 117, disabled: true,
									style: 'opacity: 0.5', value: (records) ? records.data.notelp:null
								},{
									xtype: 'label', text: '/', style:'font-size: 12px',
								},{
									xtype: 'textfield', id: 'tf.nohp', width: 117,disabled: true,
									style: 'opacity: 0.5', value: (records) ? records.data.nohp:null
								}]
							},{
									xtype: 'textfield', id: 'tf.nmcp',width: 270, fieldLabel: 'Contact Person',disabled: true,
									style: 'opacity: 0.5', value: (records) ? records.data.nmcp:null
							},{
									xtype: 'textfield', id: 'tf.sttagihan',width: 270, fieldLabel: 'Status Tagihan',disabled: true,
									style: 'opacity: 0.5', value: (records) ? records.data.sttagihan:null
							}] 
						}]
					},{
						columnWidth: 0.50, border: true, layout: 'form',
						 items:[{
							xtype: 'fieldset',
							id: 'fs.databayar',
							title: 'Data Pembayaran',
							layout: 'form',
							items: [{
								xtype: 'textfield',
								id: 'tf.nokuitansipenjamin',
								width: 270,
								fieldLabel: 'No. Pembayaran',
								disabled: true, 
								emptyText:'Otomatis',
								style: 'font-weight: bold;opacity: 0.5',
							},{
								xtype: 'datefield', 
								id: 'df.tglkuitansipenjamin', 
								width: 100,
								format: 'd-m-Y',
								value: new Date(),
								fieldLabel: 'Tgl. Pembayaran',
								disabled: false,
							},{
								xtype: 'textfield', 
								id: 'tf.userid', 
								fieldLabel: '',
								disabled: true, 
								hidden: true, 
								value: USERID,
								width: 100,
							},{
								xtype: 'textfield', 
								id: 'tf.username', 
								fieldLabel: 'User Input',
								disabled: true, 
								value: USERNAME,
								width: 270,
							},{
								xtype: 'combo', fieldLabel: 'Approval',
								id:'cb.app', width: 270, store: ds_appkeu,
								valueField: 'nmset', displayField: 'nmset', editable: false,allowBlank: true,
								triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
								emptyText:'Pilih....',
							},{
								xtype: 'numericfield', 
								id: 'tf.tottrans', 
								fieldLabel: 'Total Transaksi',
								disabled: true, 
								value: (records) ? records.data.totaltagihan:null,
								width: 150,
							},{
								xtype: 'numericfield', 
								id: 'tf.dibayarpasien', 
								fieldLabel: 'Di Bayar Pasien',
								disabled: true, 
								value: (records) ? records.data.dibayarpasien:null,
								width: 150,
							},{
								xtype: 'numericfield', 
								id: 'tf.jmltagih', 
								fieldLabel: 'Jumlah Tagihan',
								disabled: true, 
								value: (records) ? records.data.jmltagihan:null,
								width: 150,
							},{
								xtype: 'numericfield', 
								id: 'tf.totalbayar', 
								fieldLabel: 'Total Dibayar Penjamin',
								disabled: true, 
								value: (records) ? records.data.jmlbyrpenjamin:null,
								width: 150,
							},{
								xtype: 'numericfield', 
								id: 'tf.sisatagih', 
								fieldLabel: 'Sisa Tagihan',
								disabled: true, 
								value: null,
								width: 150,
							},{
								xtype: 'numericfield', 
								id: 'tf.jmlbayar', 
								fieldLabel: 'Jumlah Bayar',
								disabled: false, 
								value: null,
								width: 150,
							},{
								xtype: 'combo', fieldLabel: 'Cara Bayar',
								id:'cb.carabayar', width: 150, store: ds_carabayar,
								valueField: 'idcarabayar', displayField: 'nmcarabayar', editable: false,allowBlank: true,
								triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
								emptyText:'Pilih....',
							}]
						}]
					},{

						columnWidth: 1, border: true, layout: 'form',
						 items:[{
							xtype: 'fieldset',
							id: 'fd.riwayatbayar',
							title: 'Riwayat Pembayaran',
							layout: 'form',
							items: [grid_nya_histori]
						}]

					}]
				}],
				listeners: {
					afterrender: function () {

					},
					beforerender: function () {				
						
					}
				}
		});
		
		var win_bayar_penjamin = new Ext.Window({
				title: 'Pembayaran Penjamin',
				modal: true,
				items: [f_bayar_penjamin]
			}).show();
			
		function simpan_bayar_penjamin(){
				if (!RH.getCompValue('cb.carabayar')) {
					Ext.MessageBox.alert('Informasi','Lengkapi ' + Ext.getCmp('cb.carabayar').fieldLabel);
					return;
				}
				if (!RH.getCompValue('cb.app')) {
					Ext.MessageBox.alert('Informasi','Lengkapi ' + Ext.getCmp('cb.app').fieldLabel);
					return;
				}
				
				var waitmsgsimpan = Ext.MessageBox.wait('Menyimpan Data...', 'Informasi');
				
				Ext.Ajax.request({
					url: BASE_URL + 'tagihan_penjamin_controller/insert_kuitansipenjamin',
					params: {
						nokuitansipenjamin 	  : RH.getCompValue('tf.nokuitansipenjamin'),
						tglkuitansipenjamin   : RH.getCompValue('df.tglkuitansipenjamin'),
						userid 	  			  : RH.getCompValue('tf.userid'),
						notagihan  	  		  : RH.getCompValue('tf.notagihan'),
						idcarabayar	  		  : RH.getCompValue('cb.carabayar'),
						jumlah	  			  : RH.getCompValue('tf.jmlbayar'),
					},
					success: function(response){
						waitmsgsimpan.hide();
						Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
						RH.setCompValue('tf.nokuitansipenjamin', response.responseText);
						Ext.getCmp('btn_simpan').disable();
						Ext.getCmp('btn_baru').enable();
						ds_grid_histori.reload();
					},
					failure : function(){
						waitmsgsimpan.hide();
						Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
					}
				});
		
		}
		
		function hapus_bayar_penjamin() {
			var checkcount = 0;
			var pardelete='';
			var count= 1;
			var endpar = ';';

			grid_nya_histori.getStore().each(function(rec){ // ambil seluruh grid ugd
				var rowcheck = rec.data; 
				if (rowcheck['pilih']) {
					checkcount = checkcount + 1;
				}

			});
			
			if (checkcount == 0) {
				Ext.MessageBox.alert('Informasi','Silahkan Pilih Data Yang Akan Dihapus');
				return;
			}
		
			grid_nya_histori.getStore().each(function(rec){ // ambil seluruh grid ugd
				var rowData = rec.data; 
				if (count == checkcount) {
					endpar = ''
				}
				var pilih = (rowData['pilih']) ? 1 : 0;	
				if (pilih) {
					pardelete +=  rowData['nokuitansipenjamin'] + ':' + endpar;
											
					count = count+1;
				}
			});
			
			Ext.Msg.show({
				title: 'Konfirmasi',
				msg: 'Hapus Data Yang Dipilih?',
				buttons: Ext.Msg.YESNO,
				icon: Ext.MessageBox.QUESTION,
				fn: function (response) {
					if ('yes' !== response) {
						return;
					}
					var waitmsghapus = Ext.MessageBox.wait('Menghapus Data...', 'Info');
					Ext.Ajax.request({
						url: BASE_URL + 'tagihan_penjamin_controller/hapus_kuitansipenjamin',
						method: 'POST',
						params: {
							pardelete: pardelete
						},
						success: function() {
							waitmsghapus.hide();
							Ext.MessageBox.alert("Info", "Hapus Data Berhasil");
							ds_grid_histori.reload();
						},
						failure: function(result){
							waitmsghapus.hide();
							Ext.MessageBox.alert("Info", "Hapus Data Gagal");
						}					
					});
				}            
			});

		}
	
	}
	//========================END FORM BAYAR PENJAMIN===================================
}