function perhitungan_jasamedis_form(grids, records, storesObj){
	
	var ds_jtm = dm_jtm();
	ds_jtm.setBaseParam('idjtm',records.data.idjtm);

	ds_jtm.load({
		callback: function () {
			var sjm = 0, sdisk = 0,sjml = 0;

			ds_jtm.each(function(rec){
				sjm += parseFloat(rec.get('tarifjm'));
				sdisk += parseFloat(rec.get('diskonrp'));
				sjml += parseFloat(rec.get('jumlah'));
			});
							
			Ext.getCmp('tf.sumtarifjm').setValue(sjm);
			Ext.getCmp('tf.sumdiskon').setValue(sdisk);
			Ext.getCmp('tf.sumjumlah').setValue(sjml);
		}
	});
							
	var ds_stdoktertim = storesObj.stdoktertim;   
	
	function render_stdoktertim(value) {
		var val = RH.getRecordFieldValue(ds_stdoktertim, 'nmstdoktertim', 'idstdoktertim', value);
		return val;
    }
	
	var rownya = 0, row='';
	//==================== Grid Detail
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	var vw_daftar_pemakaianbrg = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
	var grid_daftar_pemakaianbrg = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_pemakaianbrg',
			store: ds_jtm,
			view: vw_daftar_pemakaianbrg,
			tbar: [{
				text: 'Tambah Tim Medis',
				id: 'btn_tmbh',
				iconCls: 'silk-add',
				handler: function() {
				addDokter(records)
				/* //	var cekbagian = Ext.getCmp('tf.bagian').getValue();
					if(cekbagian != ""){
						fncariBarang();
					}else{
						Ext.MessageBox.alert('Informasi','Bagian belum dipilih');
					} */
				}
			}],
			autoScroll: true,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			height: 200, 
			frame: true,
			clicksToEdit: 1,
			//sm: sm_nya,
			listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownya = rowIndex;
            }
        },
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('Nama Tim Medis'),
				width: 150,
				dataIndex: 'nmdoktergelar',
				sortable: true,
				align:'center'
			},{
				header: '',
				dataIndex: 'iddokter',
				sortable: true,
				hidden:true,
			},
			{
				header: headerGerid('Spesialisasi'),
				width: 170,
				dataIndex: 'nmspesialisasi',
				sortable: true,
				align:'center',
			},
			{
				header: headerGerid('Status'),
				width: 77,
				dataIndex: 'idstdoktertim',
				renderer: render_stdoktertim,
				sortable: true,
				editor :new Ext.form.ComboBox({
						id: 'idstdoktertim',
						store: ds_stdoktertim,
						triggerAction: 'all',
						valueField: 'idstdoktertim',
						displayField: 'nmstdoktertim',
						forceSelection: true,
						submitValue: true, 
						mode: 'local',
						typeAhead: false,
						selectOnFocus: true,
						editable: false,
                        listeners: {
                            select: function(combo, record){
								var recordx = ds_jtm.getAt(row);
								sumjasamedis(recordx,records,record.data.idstdoktertim);	
							}
						}
					})
			},
			{
				header: headerGerid('%'),
				width: 40,
				dataIndex: 'diskonjm',
				sortable: true,
				align:'right',
				xtype: 'numbercolumn', format:'0,000',
				editor:	{
					xtype: 'numberfield',
					id: 'tf.diskonjm',
					enableKeyEvents: true,
					listeners: {
						change: function(field, newVal, oldVal){
							var record = ds_jtm.getAt(row);
							var a1 = parseFloat(Ext.getCmp('tf.tarif').getValue());
							var a2 = parseFloat(record.get('diskonjm')) / 100;
							
							record.set('diskon',0);
							record.set('diskonrp',0);
							record.set('tarifjm',a1 * a2);
							record.set('jumlah',parseFloat(record.get('tarifjm')) - parseFloat(record.get('diskonrp')));

							sumjasamedis(record,records,null);		
						
						}
					}
				}
			},{
				header: headerGerid('Tarif<br>Jasa Medis'),
				width: 88,
				dataIndex: 'tarifjm',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
				editor:	{
					xtype: 'numberfield',
					id: 'tf.tarifjm',
					enableKeyEvents: true,
					listeners: {
						change: function(field, newVal, oldVal){
							var record = ds_jtm.getAt(row);
							var a1 = parseFloat(Ext.getCmp('tf.tarif').getValue());
							var a2 = parseFloat(record.get('tarifjm'));
							
							if (!record.get('tarifjm')) {
								Ext.MessageBox.alert('Informasi', 'Isi Tarif Jasa Medis'); 
								record.set('diskonrp',0);
								return;
							}
							
							record.set('diskonjm',(a2 / a1) * 100);
							record.set('jumlah',parseFloat(record.get('tarifjm')) - parseFloat(record.get('diskonrp')));
							
							sumjasamedis(record,records,null);							
						}
					}
				}
				
			},{
				header: headerGerid('%'),
				width: 40,
				dataIndex: 'diskon',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
				editor:	{
					xtype: 'numberfield',
					id: 'tf.diskon1',
					enableKeyEvents: true,
					listeners: {
						change: function(field, newVal, oldVal){
							var record = ds_jtm.getAt(row);
							var a1 = parseFloat(record.get('tarifjm'));
							var a2 = parseFloat(record.get('diskon')) / 100;
							
							if (!record.get('tarifjm')) {
								Ext.MessageBox.alert('Informasi', 'Isi Persen Tarif Jasa Medis'); 
								record.set('diskon',0);
								return;
							}
							
							record.set('diskonrp',a1 * a2);
							record.set('jumlah',parseFloat(record.get('tarifjm')) - parseFloat(record.get('diskonrp')));

							sumjasamedis(record,records,null);		
						}
					}
				}
			},{
				header: headerGerid('Diskon'),
				width: 66,
				dataIndex: 'diskonrp',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
				editor:	{
					xtype: 'numberfield',
					id: 'tf.diskonrp',
					enableKeyEvents: true,
					listeners: {
						change: function(field, newVal, oldVal){
							var record = ds_jtm.getAt(row);
							var a1 = parseFloat(record.get('tarifjm'));
							var a2 = parseFloat(record.get('diskonrp'));
							
							if (!record.get('tarifjm')) {
								Ext.MessageBox.alert('Informasi', 'Isi Persen Tarif Jasa Medis'); 
								record.set('diskonrp',0);
								return;
							}
							
							record.set('diskon',(a2 / a1) * 100);
							record.set('jumlah',parseFloat(record.get('tarifjm')) - parseFloat(record.get('diskonrp')));

							sumjasamedis(record,records,null);							
						}
					}
				}
			},{
				header: headerGerid('Jumlah'),
				width: 90,
				dataIndex: 'jumlah',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							
							var sjm = 0, sdisk = 0,sjml = 0;
							var rechapus = ds_jtm.getAt(rowIndex);
							
							if (records.data.idjtm) {
								Ext.Ajax.request({
									url: BASE_URL + 'timjasamedis_controller/hapusdet',
									params: {
										iddokter : rechapus.get("iddokter"),
										idjtm : rechapus.get("idjtm"),
										},
									success: function(response){
											Ext.MessageBox.alert('Informasi','Hapus Data Berhasil');
											
											ds_jtm.removeAt(rowIndex);
											
											ds_jtm.each(function(rec){
												sjm += parseFloat(rec.get('tarifjm'));
												sdisk += parseFloat(rec.get('diskonrp'));
												sjml += parseFloat(rec.get('jumlah'));
											});
											
											Ext.getCmp('tf.sumtarifjm').setValue(sjm);
											Ext.getCmp('tf.sumdiskon').setValue(sdisk);
											Ext.getCmp('tf.sumjumlah').setValue(sjml);
									},
									failure : function(){
										Ext.MessageBox.alert('Informasi', 'Hapus Data Gagal');
									}
								});
							}
							
							ds_jtm.removeAt(rowIndex);
							
							sumjasamedis(rechapus,records,null);
						}
					}]
			}],
			listeners	: {
				rowclick : function(grid, rowIndex, e){
					row = rowIndex;
				}
			},
			//bbar: paging_daftar_pemakaianbrg
		});
		
	function sumjasamedis(rekord,rekords,combodata){
		var sjm = 0, sdisk = 0,sjml = 0;
		ds_jtm.each(function(rec){
			sjm += parseFloat(rec.get('tarifjm'));
			sdisk += parseFloat(rec.get('diskonrp'));
			sjml += parseFloat(rec.get('jumlah'));
		});
											
		Ext.getCmp('tf.sumtarifjm').setValue(sjm);
		Ext.getCmp('tf.sumdiskon').setValue(sdisk);
		Ext.getCmp('tf.sumjumlah').setValue(sjml);
		
		Ext.getCmp('tf.jasasarana').setValue(parseFloat(Ext.getCmp('tf.tarif').getValue()) - sjml);
		
		if (rekords.data.idjtm) {
				Ext.Ajax.request({
							url: BASE_URL + 'timjasamedis_controller/simpanupdatedetedit',
							params: {
								idjtm			: rekords.data.idjtm,
								iddokter		: rekord.data.iddokter,
								idstdoktertim	: (rekord.data.idstdoktertim) ? rekord.data.idstdoktertim:combodata,
								jumlah			: (rekord.data.jumlah) ? rekord.data.jumlah:0,
								diskon			: (rekord.data.diskon) ? rekord.data.diskon:0,
								tarifjm			: (rekords.data.tarifdiskon) ? rekords.data.tarifdiskon:0,
								
								js				:  Ext.getCmp('tf.jasasarana').getValue()
							},
							success: function(response){
							
							},
							failure : function(){
								Ext.MessageBox.alert('Informasi', 'Ubah Data Gagal');
							}
				});
		}
	}
		
	//====================FORM UTAMA
	var byr_supp_form = new Ext.form.FormPanel({
				id: 'fp.byrSupp',
				region: 'center',
				bodyStyle: 'padding: 10px;',		
				border: false, frame: true,
				height: 500,
				width: 900,
				//title: 'Pembayaran Supplier Form',
				autoScroll: true,
				tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px', disabled:(records.data.idjtm) ? true:false,
				handler: function() {
					simpan();
					return;	
				}
			},{
				text: 'Cetak', id: 'bt.print',  iconCls: 'silk-printer', style: 'marginLeft: 10px', disabled:(records.data.idjtm) ? false:true,
				handler: function() {
					var win = window.open(); 
					win.location.reload();
					win.location = BASE_URL + 'print/print_jasatimmedis/pdf_jasatimmedis/'+records.data.idjtm;
				}
			},{
				text: 'Hapus', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px', disabled:(records.data.idjtm) ? false:true,
				handler: function() {
					hapus();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					win_byr_supp_form.close();
			}
			}],
				items: [{
					
				xtype: 'fieldset', title: '', layout: 'column',
				defaults: { labelWidth: 150, labelAlign: 'right', }, 
				items: [{
					columnWidth: 0.53, border: false, layout: 'form',					
					items: [{
						xtype: 'compositefield',
						fieldLabel: 'No./Tgl Registrasi',
						items:[{
							xtype: 'textfield',
							id: 'tf.noreg',
							width: 100,
							value: (records) ? records.data.noreg:'',
							readOnly: true,
							style: 'opacity: 0.6',
						},{
							xtype: 'label', id: 'lb.time', text: '/',
						},{ 	
							xtype: 'datefield',
							id: 'df.tglreg',
							format: 'd-m-Y',
							value: new Date(),
							width: 100,
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.tglreg:'',
						}]
					},{
							xtype: 'textfield',
							id: 'tf.idjtm',
							width: 100,
							hidden: true,
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.idjtm:'',
							
					},{
							xtype: 'textfield',
							id: 'tf.norm',
							width: 100,
							fieldLabel: 'No. RM',
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.norm:'',
							
						
					},{
							xtype: 'textfield',
							id: 'tf.nmpasien',
							width: 200,
							fieldLabel: 'Nama Pasien',
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.nmpasien:'',
					},{
							xtype: 'textfield',
							id: 'tf.idnotadet',
							width: 200,
						//	fieldLabel: 'Nama Pasien',
							readOnly: true,
							hidden: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.idnotadet:'',
					},{
							xtype: 'textfield',
							id: 'tf.tindakan',
							width: 200,
							fieldLabel: 'Tindakan/Pelayanan',
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.nmpelayanan:'',
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tarif Jasa Medis',
						items:[{
							xtype: 'numericfield',
							id: 'tf.tarif',
							width: 100,
							align: 'right',
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.tarifjasa:'',
						},{
							xtype: 'label', id: 'lb.disk', text: 'Diskon:', margins: '0 5 0 27',
						},{ 	
							xtype: 'numericfield',
							id: 'tf.diskon',
							width: 100,
							align: 'right',
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.diskon:'',
						}]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tarif Jasa Medis - Diskon',
						items:[{
							xtype: 'numericfield',
							id: 'tf.tarifdiskon',
							width: 100,
							align: 'right',
							fieldLabel: 'Tarif Jasa Medis - Diskon',
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.data.tarifdiskon:'',
						},{
							xtype: 'label', id: 'lb.js', text: 'Jasa Sarana:', margins: '0 5 0 0',
						},{ 	
							xtype: 'numericfield',
							id: 'tf.jasasarana',
							width: 100,
							align: 'right',
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records.data.js) ? records.data.js:0,
						}]
					}]
				},
				{
					columnWidth: 0.47, border: false, layout: 'form',
					items: [{
						xtype: 'datefield',
							id: 'df.tgl',
							format: 'd-m-Y',
							value: new Date(),
							width: 120,
							fieldLabel: 'Tanggal',
							readOnly: true,
							style: 'opacity: 0.6',
					},{
						xtype: 'textfield',
						fieldLabel: 'User Input',
						id: 'tf.nmpenerima',
						width: 200,
						readOnly: true,
						value: (records.data.nmlengkap) ? records.data.nmlengkap:USERNAME,
						style : 'opacity:0.6'
						
					},{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						id  : 'ta.keterangan',
						width : 200,
						value: (records) ? records.data.catatan:'',
						height: 45,
					}]
			}]
		},{
				xtype: 'fieldset',
				title: 'Daftar Tim Medis Yang Terlibat',
				layout: 'form',
				style: 'marginTop: -5px',
				items: [grid_daftar_pemakaianbrg]
			}],
		bbar: [{
			text: '<span style="margin-left:0px;font-size:12px;">Total:</span>', style: 'margin-left: 450px',
		},{
			xtype: 'numericfield',
			id: 'tf.sumtarifjm',
			width:90,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6'
		},{
			text: '<span style="margin-left:37px;font-size:12px;"></span>',
		},{
			xtype: 'numericfield',
			id: 'tf.sumdiskon',
			width:66,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true
		},{
			text: '<span style="margin-left:-5px;font-size:12px;"></span>',
		},{
			xtype: 'numericfield',
			id: 'tf.sumjumlah',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true
		}],
		listeners:{
            afterrender:function(){
							
			}
        }
	}); 
	
	var win_byr_supp_form = new Ext.Window({
			title: 'Perhitungan Jasa Tim Medis',
			modal: true,
			items: [byr_supp_form]
		}).show();
		
	function addDokter(recordsx){
		var ds_vdokter = dm_vdokter();		
		//ds_vdokter.setBaseParam('idjtm', recordsx.data.idjtm);
		ds_vdokter.reload();
		
	
		function keyToAddbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_barang = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 73,
			dataIndex: 'kddokter',
			sortable: true,
			renderer: keyToAddbrg
		},{
			header: 'Nama',
			width: 200,
			dataIndex: 'nmdoktergelar',
			sortable: true,
			renderer: keyToAddbrg
		},
		{
			header: 'Spesialisasi',
			width: 200,
			dataIndex: 'nmspesialisasi',
			sortable: true
		}]);
		
		var sm_barang = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_barang = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_barang = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_vdokter,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_barang = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_barang = new Ext.grid.GridPanel({
			ds: ds_vdokter,
			cm: cm_barang,
			sm: sm_barang,
			view: vw_barang,
			height: 350,
			width: 500,
			plugins: cari_barang,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [/* {
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_vdokter.setBaseParam('key',  '1');
							ds_vdokter.setBaseParam('id',  'nmbrg');
							ds_vdokter.setBaseParam('name',  nmcombo);
						ds_vdokter.load();
					}
				}]
			} */],
			bbar: paging_barang,
			listeners: {
				//rowdblclick: klik_cari_barang
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_barang = new Ext.Window({
			title: 'Cari Tim Medis',
			modal: true,
			items: [grid_find_cari_barang]
		}).show();
		
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var cek = true;
					var obj = ds_vdokter.getAt(rowIndex);
					var snmdoktergelar	= obj.get("nmdoktergelar");
					var snmspesialisasi	= obj.get("nmspesialisasi");
					var siddokter	= obj.get("iddokter");
					var sdiskonjm  = records.data.diskonjm;
					var sjumlah  = records.data.tarifdiskon;
					
					
					
					
					ds_jtm.each(function(rec){
						if(rec.get('iddokter') == siddokter) {
							Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
							cek = false;
						}
					});
					
					if (recordsx.data.idjtm) {
						Ext.Ajax.request({
							url: BASE_URL + 'timjasamedis_controller/simpanupdatedet',
							params: {
								iddokter		: siddokter,
								idjtm			: recordsx.data.idjtm,
							},
							success: function(response){
							
							},
							failure : function(){
								Ext.MessageBox.alert('Informasi', 'Tambah Data Gagal');
							}
						});
					}
					
					if(cek){
						var orgaListRecord = new Ext.data.Record.create([
							{
								name: 'nmdoktergelar',
								name: 'iddokter',
								name: 'nmspesialisasi',
								name: 'nmstatus',
								name: 'diskonjm',
								name: 'diskon',
								name: 'diskonrp',
								name: 'jumlahjm',
								
							}
						]);
						
						ds_jtm.add([
							new orgaListRecord({
								'nmdoktergelar'	: snmdoktergelar,
								'nmspesialisasi': snmspesialisasi,
								'iddokter'		: siddokter,
								'diskonjm'		: 0,
								'diskon'		: 0,
								'diskonrp'		: 0,
								'jumlahjm'		: sjumlah,
							//	'nmstatus'		: sjnsbrg,
								
							})
						]);
						win_find_cari_barang.close();
					}
			}
			return true;
		}
	}
	
	function hapus(){
		Ext.Msg.show({
				title: 'Konfirmasi',
				msg: 'Hapus Data Ini?',
				buttons: Ext.Msg.YESNO,
				icon: Ext.MessageBox.QUESTION,
				fn: function (response) {
					if ('yes' !== response) {
						return;
					}
					Ext.Ajax.request({
						url: BASE_URL + 'timjasamedis_controller/hapus',
						params: {
						//	idjtm : RH.getCompValue('tf.idjtm'),
							idjtm : RH.getCompValue('tf.idjtm'),
							
							},
						success: function(response){
								Ext.MessageBox.alert('Informasi','Hapus Data Berhasil');
								obj = Ext.util.JSON.decode(response.responseText);	
								grids.getStore().reload();					
								win_byr_supp_form.close();
								ds_grid.reload();
						},
						failure : function(){
							Ext.MessageBox.alert('Informasi', 'Hapus Data Gagal');
						}
					});
				}            
		});
	}
	
	
	function simpan(){
		var sumjm = parseFloat(Ext.getCmp("tf.sumtarifjm").getValue());
		var jmjm = parseFloat(Ext.getCmp("tf.tarif").getValue());
		var sumdiskon = parseFloat(Ext.getCmp("tf.sumdiskon").getValue());
		var disk = parseFloat(Ext.getCmp("tf.diskon").getValue());
		var sumjml = parseFloat(Ext.getCmp("tf.sumjumlah").getValue());
		var tarifdiskon = parseFloat(Ext.getCmp("tf.tarifdiskon").getValue());
		
		if(sumjm > jmjm){
			Ext.MessageBox.alert("Informasi", "Pembagian Tarif Jasa Medis Melebihi Jumlah Total Tarif Jasa Medis");  return;
		}
		
		if(sumdiskon > disk){
			Ext.MessageBox.alert("Informasi", "Pembagian Diskon Jasa Medis Melebihi Jumlah Diskon Jasa Medis");  return;
		}
		
		/* if(sumjml != tarifdiskon){
			Ext.MessageBox.alert("Informasi", "Perhitungan Tarif Jasa Medis - Diskon belum Balance");  return;
		} */

			var arrdok = [];
			for(var zx = 0; zx < ds_jtm.data.items.length; zx++){
				var record = ds_jtm.data.items[zx].data;
				ziddokter = record.iddokter;
				zidstdoktertim = record.idstdoktertim;
				zjumlah = record.jumlah;
				zdiskon = record.diskon;
				zjumlahjm = record.jumlahjm;
				arrdok[zx] = ziddokter + '-' + zidstdoktertim + '-' + zjumlah + '-' + zdiskon + '-' + zjumlahjm;

			}
			Ext.Ajax.request({
				url: BASE_URL + 'timjasamedis_controller/simpan',
				params: {
				//	idjtm : RH.getCompValue('tf.idjtm'),
					idnotadet : RH.getCompValue('tf.idnotadet'),
					tgljtm 	  : RH.getCompValue('df.tgl'),
					userid	  : USERID,
					catatan   : RH.getCompValue('ta.keterangan'),
					js   	  : RH.getCompValue('tf.jasasarana'),
					
					arrdok : Ext.encode(arrdok)
				},
				success: function(response){
						Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
						obj = Ext.util.JSON.decode(response.responseText);
						grids.getStore().reload();					
						win_byr_supp_form.close();
				},
				failure : function(){
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			});

	}

}