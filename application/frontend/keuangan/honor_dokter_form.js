function honor_dokter_form(ds_hondokmain,records,storeObj){	
	var ds_stbayar = storeObj.stbayar;
	var ds_jpembayaran = storeObj.jpembayaran;
	var ds_app1 = storeObj.app1;
	
	var ds_hondok = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_hondok',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: false,
		fields:[{
				name: 'nohondok', 
				mapping:'nohondok'
			},{
				name: 'tglhondok', 
				mapping:'tglhondok'
			},{
				name: 'periode', 
				mapping:'periode'
			},{
				name: 'jumlah', 
				mapping:'jumlah'
			}]
		});
		
	var ds_honor_ri = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_honor_ri',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: false,
		fields: [{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'tglreg',
			mapping: 'tglreg'
		},{
			name: 'tglkuitansi',
			mapping: 'tglkuitansi'
		},{
			name: 'norm',
			mapping: 'norm'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'tarifjm',
			mapping: 'tarifjm'
		},{
			name: 'diskonjm',
			mapping: 'diskonjm'
		},{
			name: 'zis',
			mapping: 'zis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk'
		},{
			name: 'tglkeluar',
			mapping: 'tglkeluar'
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		},{
			name: 'pemeriksaan',
			mapping: 'pemeriksaan'
		},{
			name: 'tindakan',
			mapping: 'tindakan'
		},{
			name: 'jasamedis',
			mapping: 'jasamedis'
		},{
			name: 'diskonmedis',
			mapping: 'diskonmedis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		}]
	});
	
	var ds_honor_rj = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_honor_rj',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: false,
		fields: [{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'tglreg',
			mapping: 'tglreg'
		},{
			name: 'tglkuitansi',
			mapping: 'tglkuitansi'
		},{
			name: 'norm',
			mapping: 'norm'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'tarifjm',
			mapping: 'tarifjm'
		},{
			name: 'diskonjm',
			mapping: 'diskonjm'
		},{
			name: 'pemeriksaan',
			mapping: 'pemeriksaan'
		},{
			name: 'tindakan',
			mapping: 'tindakan'
		},{
			name: 'jasamedis',
			mapping: 'jasamedis'
		},{
			name: 'diskonmedis',
			mapping: 'diskonmedis'
		},{
			name: 'zis',
			mapping: 'zis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		}]
	});
	
	var ds_honor_jm = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_honor_jm',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: false,
		fields: [{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'tglreg',
			mapping: 'tglreg'
		},{
			name: 'tglkuitansi',
			mapping: 'tglkuitansi'
		},{
			name: 'norm',
			mapping: 'norm'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'tarifjm',
			mapping: 'tarifjm'
		},{
			name: 'diskonjm',
			mapping: 'diskonjm'
		},{
			name: 'zis',
			mapping: 'zis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		},{
			name: 'jasamedis',
			mapping: 'jasamedis'
		},{
			name: 'diskonmedis',
			mapping: 'diskonmedis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		}]
	});
	
	var ds_honor_peltam = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_honor_peltam',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: false,
		fields: [{
			name: 'nonota',
			mapping: 'nonota'
		},{
			name: 'tglkuitansi',
			mapping: 'tglkuitansi'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'tarifjm',
			mapping: 'tarifjm'
		},{
			name: 'diskonjm',
			mapping: 'diskonjm'
		},{
			name: 'zis',
			mapping: 'zis'
		},{
			name: 'jasamedis',
			mapping: 'jasamedis'
		},{
			name: 'diskonmedis',
			mapping: 'diskonmedis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		}]
	});
	
	var row = '';
	//Riwayat
		var sumjml = 0;
		function keyToAddbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetails" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
	    var vw_riwayat = new Ext.grid.GridView({
			emptyText: '<Belum ada Data>'
		});
		var sm_riwayat = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var cm_riwayat = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header: 'Nomor',
				width: 90,
				dataIndex: 'nohondok',
				sortable: true,
				renderer: keyToAddbrg,
			},{
				header: 'Tgl Pembayaran',
				width: 90,
				dataIndex: 'tglhondok',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y',
			},{
				header: 'Periode',
				width: 150,
				dataIndex: 'periode',
				sortable: true,
			},{
				header: 'Jumlah',
				width: 90,
				dataIndex: 'jumlah',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			}
		]);
		var grid_riwayat = new Ext.grid.GridPanel({
			ds: ds_hondok,
			cm: cm_riwayat,
			sm: sm_riwayat,
			view: vw_riwayat,
			height: 150,
			width: 500,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: [{
					text: 'No. Pembayaran Dipilih',
				},{
					xtype: 'textfield', 							
					fieldLabel: 'No. Pembayaran Dipilih',
					id: 'tf.nopilih',
					width: 125,
					disabled: true, 
				},
				{
					text: 'Jumlah',
				},{
					xtype: 'numericfield', 							
					fieldLabel: 'Jumlah', 
					id: 'tf.jmlriw',
					width: 125,
					maskRe: /[0-9]/,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					disabled: true, 
			}],
		listeners: {
				//rowdblclick: klik_cari_barang
				cellclick: klik_cari
			}
		}); 
		
		function klik_cari(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterDetails'){
				var rec = ds_hondok.getAt(rowIdx);
				var nohondok = rec.data["nohondok"];
				Ext.getCmp("tf.nopilih").setValue(nohondok);
				Ext.getCmp('bt.batal').enable();				
			}
		}
	//end Riwayat
	//start ugd
		var vw_ugd = new Ext.grid.GridView({
			emptyText: '<Belum ada Data>'
		});
		var sm_ugd = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var cm_ugd = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({width: 30}),
			{
				header: 'No Registrasi',
				width: 90,
				dataIndex: 'noreg',
				sortable: true,
			},{
				header: 'Tgl Kuitansi',
				width: 90,
				dataIndex: 'tglkuitansi',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y',
			},{
				header: 'No. RM',
				width: 90,
				dataIndex: 'norm',
				sortable: true,
			},{
				header: 'Nama Pasien',
				width: 220,
				dataIndex: 'nmpasien',
				sortable: true,
			//	align: 'right',
				//xtype: 'numbercolumn', format:'0,000',
			},{
				header: 'Pemeriksaan',
				width: 87,
				dataIndex: 'pemeriksaan',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'Tindakan',
				width: 87,
				dataIndex: 'tindakan',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'Jasa Medis',
				width: 87,
				dataIndex: 'jasamedis',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'Diskon',
				width: 80,
				dataIndex: 'diskonmedis',
				sortable: true,
				align: 'right',
			 	xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'ZIS',
				width: 86,
				dataIndex: 'zis',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{	
				header: 'Jumlah',
				width: 93,
				dataIndex: 'jumlah',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			}
		]);
		var grid_ugd = new Ext.grid.EditorGridPanel({
			ds: ds_honor_rj,
			cm: cm_ugd,
			sm: sm_ugd,
			view: vw_ugd,
			height: 150,
			//width: 900,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: [{
			text: '<span style="margin-left:0px;font-size:12px;">Total 1:</span>', style: 'margin-left: 470px',
		},{
			xtype: 'numericfield',
			id: 'tf.periksajm1',
			width:87,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>', style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.tindakanjm1',
			width:87,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>', style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.jumlahjm1',
			width:87,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>', style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.diskonjm1',
			width:80,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>',style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.zis1',
			width:86,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>',style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.jumlah1',
			width:93,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		}],
		}); 
	//end ugd
	//start RI
		var vw_ri = new Ext.grid.GridView({
			emptyText: '<Belum ada Data>'
		});
		var sm_ri = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var cm_ri = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({width: 30}),
			{
				header: 'No Registrasi',
				width: 78,
				dataIndex: 'noreg',
				sortable: true,
			}/* ,{
				header: 'Tgl Registrasi',
				width: 78,
				dataIndex: 'tglreg',
				sortable: true,
			} */,{
				header: 'Tgl Masuk',
				width: 68,
				dataIndex: 'tglmasuk',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y',
			},{
				header: 'Tgl Keluar',
				width: 68,
				dataIndex: 'tglkuitansi',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y',
			},{
				header: 'No. RM',
				width: 60,
				dataIndex: 'norm',
				sortable: true,
			},{
				header: 'Nama Pasien',
				width: 132,
				dataIndex: 'nmpasien',
				sortable: true,
			//	align: 'right',
				//xtype: 'numbercolumn', format:'0,000',
			},{
				header: 'Ruangan',
				width: 80,
				dataIndex: 'nmbagian',
				sortable: true,
			},{
				header: 'Visite',
				width: 87,
				dataIndex: 'pemeriksaan',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'Tindakan',
				width: 87,
				dataIndex: 'tindakan',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'Jasa Medis',
				width: 87,
				dataIndex: 'jasamedis',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'Diskon',
				width: 87,
				dataIndex: 'diskonmedis',
				sortable: true,
				align: 'right',
			 	xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'ZIS',
				width: 85,
				dataIndex: 'zis',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{	
				header: 'Jumlah',
				width: 90,
				dataIndex: 'jumlah',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			}
		]);
		var grid_ri = new Ext.grid.EditorGridPanel({
			ds: ds_honor_ri,
			cm: cm_ri,
			sm: sm_ri,
			view: vw_ri,
			height: 150,
			//width: 900,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: [{
			text: '<span style="margin-left:0px;font-size:12px;">Total 2:</span>', style: 'margin-left: 470px',
		},{
			xtype: 'numericfield',
			id: 'tf.periksajm2',
			width:87,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>', style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.tindakanjm2',
			width:87,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>', style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.jumlahjm2',
			width:87,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>', style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.diskonjm2',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>',style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.zis2',
			width:85,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>',style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.jumlah2',
			width:90,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		}],
		}); 
	//end RI
	//start Tim Medis
		var vw_tm = new Ext.grid.GridView({
			emptyText: '<Belum ada Data>'
		});
		var sm_tm = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var cm_tm = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({width: 30}),
			{
				header: 'No Registrasi',
				width: 110,
				dataIndex: 'noreg',
				sortable: true,
			},{
				header: 'Tgl Kuitansi',
				width: 110,
				dataIndex: 'tglkuitansi',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y',
			},{
				header: 'No. RM',
				width: 110,
				dataIndex: 'norm',
				sortable: true,
			},{
				header: 'Nama Pasien',
				width: 325,
				dataIndex: 'nmpasien',
				sortable: true,
			//	align: 'right',
				//xtype: 'numbercolumn', format:'0,000',
			},{
				header: 'Jasa Medis',
				width: 93,
				dataIndex: 'jasamedis',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'Diskon',
				width: 75,
				dataIndex: 'diskonmedis',
				sortable: true,
				align: 'right',
			 	xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'ZIS',
				width: 93,
				dataIndex: 'zis',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{	
				header: 'Jumlah',
				width: 93,
				dataIndex: 'jumlah',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			}
		]);
		var grid_tm = new Ext.grid.EditorGridPanel({
			ds: ds_honor_jm,
			cm: cm_tm,
			sm: sm_tm,
			view: vw_tm,
			height: 150,
			//width: 900,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: [{
			text: '<span style="margin-left:0px;font-size:12px;">Total 3:</span>', style: 'margin-left: 637px',
		},{
			xtype: 'numericfield',
			id: 'tf.jumlahjm3',
			width:93,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>', style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.diskonjm3',
			width:75,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>',style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.zis3',
			width:93,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>',style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.jumlah3',
			width:93,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		}],
	}); 
	//end Tim Medis
	//start Pel Tambahan
		var vw_peltam = new Ext.grid.GridView({
			emptyText: '<Belum ada Data>'
		});
		var sm_peltam = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var cm_peltam = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({width: 30}),
			{
				header: 'No Nota',
				width: 110,
				dataIndex: 'nonota',
				sortable: true,
			},{
				header: 'Tgl Kuitansi',
				width: 110,
				dataIndex: 'tglkuitansi',
				sortable: true,
				xtype: 'datecolumn',
				format: 'd/m/Y',
			},{
				header: 'Nama Pasien',
				width: 433,
				dataIndex: 'nmpasien',
				sortable: true,
			//	align: 'right',
				//xtype: 'numbercolumn', format:'0,000',
			},{
				header: 'Jasa Medis',
				width: 93,
				dataIndex: 'jasamedis',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'Diskon',
				width: 75,
				dataIndex: 'diskonmedis',
				sortable: true,
				align: 'right',
			 	xtype: 'numbercolumn', format:'0,000',
			
			},{
				header: 'ZIS',
				width: 93,
				dataIndex: 'zis',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			
			},{	
				header: 'Jumlah',
				width: 93,
				dataIndex: 'jumlah',
				sortable: true,
				align: 'right',
				xtype: 'numbercolumn', format:'0,000',
			}
		]);
		var grid_peltam = new Ext.grid.EditorGridPanel({
			ds: ds_honor_peltam,
			cm: cm_peltam,
			sm: sm_peltam,
			view: vw_peltam,
			height: 150,
			//width: 900,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: [{
			text: '<span style="margin-left:0px;font-size:12px;">Total 4:</span>', style: 'margin-left: 637px',
		},{
			xtype: 'numericfield',
			id: 'tf.jumlahjm4',
			width:93,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>', style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.diskonjm4',
			width:75,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>',style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.zis4',
			width:93,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		},{
			text: '<span style="margin-left:0px;font-size:12px;"></span>',style: 'margin-left: -10px',
		},{
			xtype: 'numericfield',
			id: 'tf.jumlah4',
			width:93,
			decimalSeparator: ',', 
			decimalPrecision: 0, 
			alwaysDisplayDecimals: true,
			useThousandSeparator: true, 
			thousandSeparator: '.',
			readOnly: true,
			style: 'opacity: 0.6',
		}],
	}); 
	//end Tim Medis
	//Main Form
	var honor_dokter_form = new Ext.form.FormPanel({
				id: 'fp.byrSupp',
				region: 'center',
				bodyStyle: 'padding: 10px;',		
				border: false, frame: true,
				height: 550,
				width: 1150,
				autoScroll: true,
				tbar: [{
					text: 'Baru', iconCls:'silk-add', id: 'btn_baru', style: 'marginLeft: 5px',
					handler: function() {
						Ext.getCmp('tf.nmdokter').setValue();
						Ext.getCmp('tf.iddokter').setValue();
						Ext.getCmp('tf.spesialisasi').setValue();
						Ext.getCmp('tf.stdokter').setValue();
						Ext.getCmp('tf.staktif').setValue();
						Ext.getCmp('tf.nohondok').setValue('Otomatis');
						ds_hondok.setBaseParam('cbx','false');
						ds_hondok.setBaseParam('iddokter','-');
						ds_hondok.reload();
						Ext.getCmp('tf.nopilih').setValue();
						Ext.getCmp('tf.jmlriw').setValue('0');
						
						ds_honor_rj.setBaseParam('iddokter',null);
						ds_honor_rj.setBaseParam('nohondok',null);
						ds_honor_rj.setBaseParam('tglakhir',null);
						ds_honor_rj.setBaseParam('tglawal',null);
						ds_honor_rj.reload();
						Ext.getCmp('tf.periksajm1').setValue('0');
						Ext.getCmp('tf.tindakanjm1').setValue('0');
						Ext.getCmp('tf.jumlahjm1').setValue('0');
						Ext.getCmp('tf.diskonjm1').setValue('0');
						Ext.getCmp('tf.zis1').setValue('0');
						Ext.getCmp('tf.jumlah1').setValue('0');
						
						ds_honor_ri.setBaseParam('iddokter',null);
						ds_honor_ri.setBaseParam('nohondok',null);
						ds_honor_ri.setBaseParam('tglakhir',null);
						ds_honor_ri.setBaseParam('tglawal',null);
						ds_honor_ri.reload();
						Ext.getCmp('tf.periksajm2').setValue('0');
						Ext.getCmp('tf.tindakanjm2').setValue('0');
						Ext.getCmp('tf.jumlahjm2').setValue('0');
						Ext.getCmp('tf.diskonjm2').setValue('0');
						Ext.getCmp('tf.zis2').setValue('0');
						Ext.getCmp('tf.jumlah2').setValue('0');
						
						ds_honor_jm.setBaseParam('iddokter',null);
						ds_honor_jm.setBaseParam('nohondok',null);
						ds_honor_jm.setBaseParam('tglakhir',null);
						ds_honor_jm.setBaseParam('tglawal',null);
						ds_honor_jm.reload();
						Ext.getCmp('tf.jumlahjm3').setValue('0');
						Ext.getCmp('tf.diskonjm3').setValue('0');
						Ext.getCmp('tf.zis3').setValue('0');
						Ext.getCmp('tf.jumlah3').setValue('0');	
						
						Ext.getCmp('tf.norek').setValue();
						Ext.getCmp('tf.bank').setValue();
						Ext.getCmp('tf.atasnama').setValue();
						Ext.getCmp('tf.telp').setValue();
						Ext.getCmp('tf.hp').setValue();
						Ext.getCmp('tf.totalnya').setValue('0');
						Ext.getCmp('tf.jasadok').setValue('0');
						Ext.getCmp('tf.potongan').setValue('0');
						Ext.getCmp('tf.jumlah').setValue('0');
						Ext.getCmp('ta.catatan').setValue();
						Ext.getCmp('btn_simpan').enable();						
						Ext.getCmp('bt.cetakrekap').disable();
						Ext.getCmp('bt.cetakdetail').disable();
						Ext.getCmp('bt.eksportrek').disable();
						Ext.getCmp('bt.eksportdet').disable();
						Ext.getCmp('bt.batal').disable();
					}
				},{
					text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
					handler: function() {
						simpan();
						return;	
					}
				},{
					text: 'Cetak Rekap', id: 'bt.cetakrekap',  iconCls: 'silk-printer', style: 'marginLeft: 10px',
					disabled: (records) ? false:true,
					handler: function() {
						var win = window.open(); 
						win.location.reload();
						win.location = BASE_URL + 'print/print_honordokter/pdf_honordokter/'+RH.getCompValue('tf.nohondok');
					}
				},{
					text: 'Cetak Detail', id: 'bt.cetakdetail',  iconCls: 'silk-printer', style: 'marginLeft: 10px',
					disabled: (records) ? false:true,
					handler: function() {
						var win = window.open(); 
						win.location.reload();
						win.location = BASE_URL + 'print/print_honordokterdetail/pdf_honordokterdetail/'+RH.getCompValue('tf.nohondok');
					}
				},{
					text: 'Excel Rekap', id: 'bt.eksportrek',  iconCls: 'silk-save', style: 'marginLeft: 10px',
					disabled: (records) ? false:true,
					handler: function() {
						window.location = BASE_URL + 'print/print_honordokterdetail/eksport_honordokterrekap/'+RH.getCompValue('tf.nohondok');
					}
				},{
					text: 'Excel Detail', id: 'bt.eksportdet',  iconCls: 'silk-save', style: 'marginLeft: 10px',
					disabled: (records) ? false:true,
					handler: function() {
						window.location = BASE_URL + 'print/print_honordokterdetail/eksport_honordokterdetail/'+RH.getCompValue('tf.nohondok');
					}
				},{
					text: 'Hapus', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px',
					disabled: (records) ? false:true,
					handler: function() {
						hapus();
					}
				},{
					text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
					handler: function() {
						win_honor.close();
				}
			}],
			items:[{
				xtype: 'fieldset', title: '', layout: 'column', border: false,
				defaults: { labelWidth: 100, labelAlign: 'right', }, 
				items:[{
					columnWidth: 0.50, border: false, layout: 'form',
					height: 190,
					items:[{
						xtype: 'fieldset',
						id: 'fd.haha',
						title: '',
						layout: 'form',
						style: 'marginTop: 7px',
					items: [{
						xtype: 'textfield',
						id: 'tf.nohondok',
						width: 110,
						fieldLabel: 'No. Pembayaran',
						disabled: true,
						emptyText:'Otomatis',
						value: (records) ? records.get("nohondok"):"",
						style: 'font-weight: bold;opacity: 0.6',
					},{
						xtype: 'datefield',
						fieldLabel: 'Tgl. Pembayaran',
						id: 'df.tglbayar',
						format: 'd-m-Y',
						width: 100,
						disabled: (records) ? true:false,
						value: (records) ? records.get("tglhondok"):new Date(),
					},{
						xtype: 'compositefield',
						fieldLabel: 'Nama',
						items:[{
							xtype: 'textfield',
							id: 'tf.nmdokter',
							width: 250,
							readOnly: true,
							value: (records) ? records.get("nmdoktergelar"):"",
							style: 'opacity: 0.6',
						},{
						xtype: 'textfield', id:'tf.iddokter', hidden: true, value: (records) ? records.get("iddokter"):"",
						},{
							xtype: 'button', text: ' .... ',
							width: 30,id: 'btn.dok', disabled: (records) ? true:false,
							handler: function(){
								addDokter();
							}
						}]
					},{
						xtype: 'textfield', id: 'tf.spesialisasi',width: 285, fieldLabel: 'Spesialisasi',readOnly: true,
						style: 'opacity: 0.6', value: (records) ? records.get("nmspesialisasi"):"",
					},{
						xtype: 'compositefield',
						fieldLabel: 'Status',
						items:[{
							xtype: 'textfield',
							id: 'tf.stdokter',
							width: 100,
							readOnly: true,
							style: 'opacity: 0.6',
							value: (records) ? records.get("nmstdokter"):"",
						},{
							xtype: 'label', text: 'Status Aktif', id:'lb.staktif'
						},{
							xtype: 'textfield', id: 'tf.staktif', width: 110,readOnly: true,
							style: 'opacity: 0.6', value: (records) ? records.get("nmstatus"):"",
						}]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Periode',
						items:[{
							xtype: 'datefield',
							id: 'df.tglawal',
							format: 'd-m-Y',
							disabled: (records) ? true:false,
							value: (records) ? records.get("tglawal"):Ext.Ajax.request({
																			url: BASE_URL + 'honor_controller/get_tglawal_',
																			method: 'POST',
																			params: {},
																			success: function(response){
																				obj = Ext.util.JSON.decode(response.responseText);
																				Ext.getCmp('df.tglawal').setValue(obj.tglawal);
																			},
																			failure : function(){
																				Ext.MessageBox.alert('Informasi', 'xxx');
																			}
																		}),
							width: 100,
							listeners:{
								select: function(field, newValue){
									cari(false);
								}
							}
						},{
							xtype: 'label', text: 's.d', id: 'lb.sd'
						},{
							xtype: 'datefield',
							id: 'df.tglakhir',
							format: 'd-m-Y',
							disabled: (records) ? true:false,
							value: (records) ? records.get("tglakhir"):Ext.Ajax.request({
																			url: BASE_URL + 'honor_controller/get_tglakhir_',
																			method: 'POST',
																			params: {},
																			success: function(response){
																				obj = Ext.util.JSON.decode(response.responseText);
																				Ext.getCmp('df.tglakhir').setValue(obj.tglakhir);
																			},
																			failure : function(){
																				Ext.MessageBox.alert('Informasi', 'xxx');
																			}
																		}),
							width: 100,
							listeners:{
								select: function(field, newValue){
									cari(false);
									Ext.getCmp('df.tgldikeluarkan').setValue(getnextmonth(newValue));
								}
							}
						}]
					}]
					}]
				},{
					columnWidth: 0.50, border: false, layout: 'form',
					 items:[{
						xtype: 'fieldset',
						id: 'fd.riwayat',
						title: 'Riwayat Pembayaran',
						layout: 'form',
					//	style: 'marginTop: -5px',
						items: [grid_riwayat] 
					}]
				},{

					columnWidth: 1, border: false, layout: 'form',
					 items:[{
						xtype: 'fieldset',
						id: 'fd.satu',
						title: 'Penerimaan Rawat Jalan / UGD',
						layout: 'form',
					//	style: 'marginTop: -5px',
						items: [grid_ugd]
					}]

				},{
					columnWidth: 1, border: false, layout: 'form',
					 items:[{
						xtype: 'fieldset',
						id: 'fd.dua',
						title: 'Penerimaan Rawat Inap / RI',
						layout: 'form',
					//	style: 'marginTop: -5px',
						items: [grid_ri]
					}]
				},{
					columnWidth: 1, border: false, layout: 'form',
					 items:[{
						xtype: 'fieldset',
						id: 'fd.tiga',
						title: 'Penerimaan Tindakan Tim Medis',
						layout: 'form',
					//	style: 'marginTop: -5px',
						items: [grid_tm]
					}]
				},{
					columnWidth: 1, border: false, layout: 'form',
					 items:[{
						xtype: 'fieldset',
						id: 'fd.empat',
						title: 'Penerimaan Pelayanan Tambahan',
						layout: 'form',
					//	style: 'marginTop: -5px',
						items: [grid_peltam]
					}]
				},{
					columnWidth: 1, border: false, layout: 'form',
					items:[{
						xtype: 'fieldset',
						id: 'fd.info',
						title: 'Informasi Lainnya',
						layout: 'form',
					//	style: 'marginTop: -5px',
						items: [{
							xtype: 'fieldset', title: '', layout: 'column', border: false,
							defaults: { labelWidth: 100, labelAlign: 'right', }, 
						items:[{
								columnWidth: 0.65, border: false, layout: 'form',
						items:[{
								xtype: 'textfield', id:'tf.norek', fieldLabel:'No. Rekening', width: 350,readOnly: true,
									style: 'opacity: 0.6', value: (records) ? records.get("norek"):"",
								},{
								xtype: 'textfield', id:'tf.bank', fieldLabel:'Bank', width: 350,readOnly: true,
									style: 'opacity: 0.6', value: (records) ? records.get("nmbank"):"",
							},{
								xtype: 'textfield', id:'tf.atasnama', fieldLabel:'Atas Nama', width: 350,readOnly: true,
									style: 'opacity: 0.6', value: (records) ? records.get("atasnama"):"",
							},{
								
							xtype: 'compositefield',fieldLabel: 'No. Telepon',
								items:[{
									xtype: 'textfield',
									id: 'tf.telp',
									width: 125,
									readOnly: true,
									style: 'opacity: 0.6',
									value: (records) ? records.get("notelp"):"",
								},{
									xtype: 'label', text: 'No. Handphone', id:'lb.hp'
								},{
									xtype: 'textfield', id: 'tf.hp', width: 125,readOnly: true,
									style: 'opacity: 0.6',
									value: (records) ? records.get("nohp"):"",
								}]
							},{
								xtype: 'compositefield',fieldLabel: 'User Input',
								items:[{
									xtype: 'textfield',
									id: 'tf.username',
									width: 125,
									readOnly: true,
									style: 'opacity: 0.6',
									value: (records) ? records.get("nmlengkap"):USERNAME,
								},{
									xtype: 'textfield',
									id: 'tf.user',
									width: 125,
									hidden: true,
									style: 'opacity: 0.6',
									value: (records) ? records.get("userid"):USERID,
								},{
									xtype: 'label', text: 'Tgl. Input', id:'lb.tglinput'
								},{
									xtype: 'datefield',
								//	fieldLabel: 'Tgl. Pembayaran',
									id: 'tf.tglinput',
									format: 'd-m-Y',
									value: (records) ? records.get("tglinput"):new Date(),
									width: 125,
									readOnly: true,
									style: 'opacity: 0.6',
								}]
							},{
								xtype: 'compositefield',fieldLabel: 'Approval',
								items:[{
									xtype: 'combo', fieldLabel: 'Approval',
									id:'cb.app1', width: 120, store: ds_app1,
									valueField: 'nmset', displayField: 'nmset', editable: false,allowBlank: false,
									triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
									emptyText:'Pilih....',//value: (records) ? records.get("approval"):null,
									},{
										xtype: 'label', text: 'Jenis Pembayaran', id:'lb.stbayar', width: 100
									},{
										xtype: 'combo', fieldLabel: 'Jenis Pembayaran',
										id:'cb.jpembayaran', width: 150, store: ds_jpembayaran,
										valueField: 'idjnspembayaran', displayField: 'nmjnspembayaran', editable: false,allowBlank: true,
										triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
										emptyText:'Pilih....', value: (records) ? records.get("idjnspembayaran"):null,
									}]
							},{
								xtype: 'compositefield',fieldLabel: 'Status Bayar',
								items:[{
										xtype: 'combo',
										fieldLabel: 'Status Bayar',
										store: ds_stbayar,
										id: 'cb.stbayar',
										triggerAction: 'all',
										editable: false,
										valueField: 'idstbayar',
										displayField: 'nmstbayar',
										forceSelection: true,
										submitValue: true,
										typeAhead: true,
										mode: 'local',
										value: (records) ? records.get("idstbayar"):null,
										emptyText:'Pilih...',
										selectOnFocus:true,
										width: 150,
										listeners: {
											afterrender: function () {
												if (!records) { Ext.getCmp('cb.stbayar').setValue(1); }
											}
										}
									},{
										xtype: 'label', text: 'Tgl. Dikeluarkan', id:'lb.stbayar'
									},{
										xtype: 'datefield',
										fieldLabel: 'Tgl. Dikeluarkan',
										id: 'df.tgldikeluarkan',
										format: 'd-m-Y',
										width: 100,
										value: (records) ? records.get("tgldikeluarkan"):getnextmonth(new Date()),
									}]
							}]
						},{
							   columnWidth: 0.35, border: false, layout: 'form',
						items:[{
								xtype: 'numericfield', id:'tf.totalnya', fieldLabel:'Total 1+2+3+4', width: 100,disabled: true,
								decimalSeparator: ',', 
								decimalPrecision: 0, 
								alwaysDisplayDecimals: true,
								useThousandSeparator: true, 
								thousandSeparator: '.',
								listeners:{
									change: function(){
										var tot = Ext.getCmp('tf.totalnya').getValue();
										var jasdok = Ext.getCmp('tf.jasadok').getValue();
										var potongan = Ext.getCmp('tf.potongan').getValue();
										var hasil = parseFloat(tot) + parseFloat(jasdok) - parseFloat(potongan);
										Ext.getCmp('tf.jumlah').setValue(hasil);
									}
								}
								},{
								xtype: 'numericfield', id:'tf.jasadok', fieldLabel:'Jasa Piket', width: 100, 
								decimalSeparator: ',', 
								decimalPrecision: 0, 
								alwaysDisplayDecimals: true,
								useThousandSeparator: true, 
								thousandSeparator: '.',
								value: (records) ? records.get("jasadrjaga"):0,
								enableKeyEvents: true,
								listeners:{
									change: function(){
										var tot = Ext.getCmp('tf.totalnya').getValue();
										var jasdok = Number(Ext.getCmp('tf.jasadok').getValue());
										var potongan = Ext.getCmp('tf.potongan').getValue();
										var hasil = parseFloat(tot) + parseFloat(jasdok) - parseFloat(potongan);
										Ext.getCmp('tf.jumlah').setValue(hasil);
										Ext.getCmp('tf.jasadok').setValue(Number(Ext.getCmp('tf.jasadok').getValue()));
									},
									specialkey: function(f,e){
										if (e.getKey() == e.ENTER) {
											var tot = Ext.getCmp('tf.totalnya').getValue();
											var jasdok = Number(Ext.getCmp('tf.jasadok').getValue());
											var potongan = Ext.getCmp('tf.potongan').getValue();
											var hasil = parseFloat(tot) + parseFloat(jasdok) - parseFloat(potongan);
											Ext.getCmp('tf.jumlah').setValue(hasil);
											Ext.getCmp('tf.jasadok').setValue(Number(Ext.getCmp('tf.jasadok').getValue()));
										}
									}
								}
								
								},{
								xtype: 'numericfield', id:'tf.potongan', fieldLabel:'Potongan Lainnya', width: 100,
								decimalSeparator: ',', 
								decimalPrecision: 0, 
								alwaysDisplayDecimals: true,
								useThousandSeparator: true, 
								thousandSeparator: '.',
								enableKeyEvents: true,
								value: (records) ? records.get("potonganlain"):0,
								listeners: {
									change: function(){
										var tot = Ext.getCmp('tf.totalnya').getValue();
										var jasdok = Ext.getCmp('tf.jasadok').getValue();
										var potongan = Number(Ext.getCmp('tf.potongan').getValue());
										var hasil = parseFloat(tot) + parseFloat(jasdok) - parseFloat(potongan);
										Ext.getCmp('tf.jumlah').setValue(hasil);
										Ext.getCmp('tf.potongan').setValue(Number(Ext.getCmp('tf.potongan').getValue()));
									},
									specialkey: function(f,e){
										if (e.getKey() == e.ENTER) {
											var tot = Ext.getCmp('tf.totalnya').getValue();
											var jasdok = Ext.getCmp('tf.jasadok').getValue();
											var potongan = Number(Ext.getCmp('tf.potongan').getValue());
											var hasil = parseFloat(tot) + parseFloat(jasdok) - parseFloat(potongan);
											Ext.getCmp('tf.jumlah').setValue(hasil);
											Ext.getCmp('tf.potongan').setValue(Number(Ext.getCmp('tf.potongan').getValue()));
										}
									}
								}
								
								},{
								xtype: 'numericfield', id:'tf.jumlah', fieldLabel:'Jumlah Yang Diterima', width: 100,disabled: true,
								decimalSeparator: ',', 
								decimalPrecision: 0, 
								alwaysDisplayDecimals: true,
								useThousandSeparator: true, 
								thousandSeparator: '.',
								},{
								xtype: 'textarea',
								fieldLabel: 'Catatan',
								id  : 'ta.catatan',
								value: (records) ? records.get("catatan"):"",
								width : 200,
								height: 45,
								}]	
							}]
						}]

					}]
				}]
			}],
			listeners: {
				afterrender: function () {
					cari(true);
				},
				beforerender: function () {				
					
				}
			}
	});
	
	var win_honor = new Ext.Window({
			title: 'Honor Dokter',
			modal: true,
			items: [honor_dokter_form]
		}).show();

/* 
Function Dialog Dokter
*/
	function addDokter(){	
		var ds_dokter = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'dokter_controller/get_dokter',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'kddokter',
				mapping: 'kddokter'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'tptlahir',
				mapping: 'tptlahir'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'idspesialisasi',
				mapping: 'idspesialisasi'
			},{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'idstdokter',
				mapping: 'idstdokter'
			},{
				name: 'nmstdokter',
				mapping: 'nmstdokter'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idbank',
				mapping: 'idbank'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'norek',
				mapping: 'norek'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'cabang',
				mapping: 'cabang'
			}]
		});
		
	
		function keyToAddbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_barang = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: 'Kode Dokter',
			width: 73,
			dataIndex: 'kddokter',
			sortable: true,
			renderer: keyToAddbrg
		},{
			header: 'Nama Dokter',
			width: 200,
			dataIndex: 'nmdoktergelar',
			sortable: true,
			renderer: keyToAddbrg
		},
		{
			header: 'Spesialisasi',
			width: 200,
			dataIndex: 'nmspesialisasi',
			sortable: true
		}]);
		
		var sm_barang = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_barang = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_barang = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_dokter,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_barang = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_barang = new Ext.grid.GridPanel({
			ds: ds_dokter,
			cm: cm_barang,
			sm: sm_barang,
			view: vw_barang,
			height: 350,
			width: 500,
			plugins: cari_barang,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [/* {
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
							nmcombo = Ext.getCmp('cek').getValue();
							ds_dokter.setBaseParam('key',  '1');
							ds_dokter.setBaseParam('id',  'nmbrg');
							ds_dokter.setBaseParam('name',  nmcombo);
						ds_dokter.load();
					}
				}]
			} */],
			bbar: paging_barang,
			listeners: {
				//rowdblclick: klik_cari_barang
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_dokter = new Ext.Window({
			title: 'Cari Dokter',
			modal: true,
			items: [grid_find_cari_barang]
		}).show();
		
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			if (t.className == 'keyMasterDetail'){
	
				var rec_cari = ds_dokter.getAt(rowIndex);
				var var_cari_iddok = rec_cari.data["iddokter"];
				var var_cari_nmdokter = rec_cari.data["nmdoktergelar"];
				var var_cari_nmspesial = rec_cari.data["nmspesialisasi"];
				var var_cari_nmstdokter = rec_cari.data["nmstdokter"];
				var var_cari_nmststatus= rec_cari.data["nmstatus"];
				var var_cari_bank= rec_cari.data["nmbank"];
				var var_cari_atasnama= rec_cari.data["atasnama"];
				var var_cari_norek= rec_cari.data["norek"];
				var var_cari_notelp= rec_cari.data["notelp"];
				var var_cari_nohp= rec_cari.data["nohp"];
					
				Ext.getCmp('tf.iddokter').focus();
				Ext.getCmp('tf.iddokter').setValue(var_cari_iddok);
				Ext.getCmp('tf.nmdokter').setValue(var_cari_nmdokter);
				Ext.getCmp('tf.spesialisasi').setValue(var_cari_nmspesial);
				Ext.getCmp('tf.stdokter').setValue(var_cari_nmstdokter);
				Ext.getCmp('tf.staktif').setValue(var_cari_nmststatus);
				Ext.getCmp('tf.bank').setValue(var_cari_bank);
				Ext.getCmp('tf.atasnama').setValue(var_cari_atasnama);
				Ext.getCmp('tf.norek').setValue(var_cari_norek);
				Ext.getCmp('tf.telp').setValue(var_cari_notelp);
				Ext.getCmp('tf.hp').setValue(var_cari_nohp);
				

			win_find_cari_dokter.close();
			
			cari(true);
	
			}
		}
	}
		function cari(tf){
			if (tf) {
				ds_hondok.setBaseParam("cbx",false);
				if (records) {
					ds_hondok.setBaseParam("iddokter",records.get("iddokter"));
				} else {
					if (Ext.getCmp('tf.iddokter').getValue()) {
						ds_hondok.setBaseParam("iddokter",Ext.getCmp('tf.iddokter').getValue());
					} else {
						ds_hondok.setBaseParam("iddokter","-");
					}
				}
				ds_hondok.reload({
                        scope   : this,
                        callback: function(records, operation, success) {
                            var jmlriw = 0;
							ds_hondok.each(function(rec){
								jmlriw += parseFloat(rec.get('jumlah'));
							});
							Ext.getCmp('tf.jmlriw').setValue(jmlriw);

                        }
				});
			}
				
			ds_honor_rj.setBaseParam("nohondok",Ext.getCmp("tf.nohondok").getValue());
			ds_honor_rj.setBaseParam("iddokter",Ext.getCmp("tf.iddokter").getValue());
			ds_honor_rj.setBaseParam("tglawal",Ext.getCmp("df.tglawal").getValue());
			ds_honor_rj.setBaseParam("tglakhir",Ext.getCmp("df.tglakhir").getValue());
			ds_honor_rj.reload({
                        scope   : this,
                        callback: function(records, operation, success) {
                            var sumperiksa1 = 0, sumtindakan1 = 0, sumtarif1 = 0, sumdiskon1 = 0, zis1 = 0, jumlah1=0;
							ds_honor_rj.each(function(rec){
												sumperiksa1 += parseFloat(rec.get('pemeriksaan'));
												sumtindakan1 += parseFloat(rec.get('tindakan'));
												sumtarif1 += parseFloat(rec.get('jasamedis'));
												sumdiskon1 += parseFloat(rec.get('diskonmedis'));
												zis1 += parseFloat(rec.get('zis'));
												jumlah1 += parseFloat(rec.get('jumlah'));
											//	sumjumlah += parseFloat(rec.get('diskonjm'));
											});
							Ext.getCmp('tf.periksajm1').setValue(sumperiksa1);
							Ext.getCmp('tf.tindakanjm1').setValue(sumtindakan1);
							Ext.getCmp('tf.jumlahjm1').setValue(sumtarif1);
							Ext.getCmp('tf.diskonjm1').setValue(sumdiskon1);
							Ext.getCmp('tf.zis1').setValue(zis1);
							Ext.getCmp('tf.jumlah1').setValue(jumlah1);

							var total = parseFloat(Ext.getCmp('tf.jumlah1').getValue()) + parseFloat(Ext.getCmp('tf.jumlah2').getValue()) + parseFloat(Ext.getCmp('tf.jumlah3').getValue()) + parseFloat(Ext.getCmp('tf.jumlah4').getValue());
							Ext.getCmp('tf.totalnya').setValue(total);
							
							var jasdok = Ext.getCmp('tf.jasadok').getValue();
							var potongan = Ext.getCmp('tf.potongan').getValue();
							var hasil = parseFloat(total) + parseFloat(jasdok) - parseFloat(potongan);
							Ext.getCmp('tf.jumlah').setValue(hasil);

                        }
            });
			
			ds_honor_ri.setBaseParam("nohondok",Ext.getCmp("tf.nohondok").getValue());
			ds_honor_ri.setBaseParam("iddokter",Ext.getCmp("tf.iddokter").getValue());
			ds_honor_ri.setBaseParam("tglawal",Ext.getCmp("df.tglawal").getValue());
			ds_honor_ri.setBaseParam("tglakhir",Ext.getCmp("df.tglakhir").getValue());
			ds_honor_ri.reload({
                        scope   : this,
                        callback: function(records, operation, success) {
                            var sumperiksa = 0, sumtindakan = 0, sumtarif = 0, sumdiskon = 0, zis = 0, jumlah=0;
							ds_honor_ri.each(function(rec){
												sumperiksa += parseFloat(rec.get('pemeriksaan'));
												sumtindakan += parseFloat(rec.get('tindakan'));
												sumtarif += parseFloat(rec.get('jasamedis'));
												sumdiskon += parseFloat(rec.get('diskonmedis'));
												zis += parseFloat(rec.get('zis'));
												jumlah += parseFloat(rec.get('jumlah'));
											//	sumjumlah += parseFloat(rec.get('diskonjm'));
											});
							Ext.getCmp('tf.periksajm2').setValue(sumperiksa);
							Ext.getCmp('tf.tindakanjm2').setValue(sumtindakan);
							Ext.getCmp('tf.jumlahjm2').setValue(sumtarif);
							Ext.getCmp('tf.diskonjm2').setValue(sumdiskon);
							Ext.getCmp('tf.zis2').setValue(zis);
							Ext.getCmp('tf.jumlah2').setValue(jumlah);

							var total = parseFloat(Ext.getCmp('tf.jumlah1').getValue()) + parseFloat(Ext.getCmp('tf.jumlah2').getValue()) + parseFloat(Ext.getCmp('tf.jumlah3').getValue()) + parseFloat(Ext.getCmp('tf.jumlah4').getValue());
							Ext.getCmp('tf.totalnya').setValue(total);
							
							var jasdok = Ext.getCmp('tf.jasadok').getValue();
							var potongan = Ext.getCmp('tf.potongan').getValue();
							var hasil = parseFloat(total) + parseFloat(jasdok) - parseFloat(potongan);
							Ext.getCmp('tf.jumlah').setValue(hasil);

                        }
            });
			
			ds_honor_jm.setBaseParam("nohondok",Ext.getCmp("tf.nohondok").getValue());
			ds_honor_jm.setBaseParam("iddokter",Ext.getCmp("tf.iddokter").getValue());
			ds_honor_jm.setBaseParam("tglawal",Ext.getCmp("df.tglawal").getValue());
			ds_honor_jm.setBaseParam("tglakhir",Ext.getCmp("df.tglakhir").getValue());
			ds_honor_jm.reload({
                        scope   : this,
                        callback: function(records, operation, success) {
                            var sumtarif3 = 0, sumdiskon3 = 0, zis3 = 0, jumlah3=0;
							ds_honor_jm.each(function(rec){
												sumtarif3 += parseFloat(rec.get('jasamedis'));
												sumdiskon3 += parseFloat(rec.get('diskonmedis'));
												zis3 += parseFloat(rec.get('zis'));
												jumlah3 += parseFloat(rec.get('jumlah'));
											//	sumjumlah += parseFloat(rec.get('diskonjm'));
											});
							Ext.getCmp('tf.jumlahjm3').setValue(sumtarif3);
							Ext.getCmp('tf.diskonjm3').setValue(sumdiskon3);
							Ext.getCmp('tf.zis3').setValue(zis3);
							Ext.getCmp('tf.jumlah3').setValue(jumlah3);
							
							var total = parseFloat(Ext.getCmp('tf.jumlah1').getValue()) + parseFloat(Ext.getCmp('tf.jumlah2').getValue()) + parseFloat(Ext.getCmp('tf.jumlah3').getValue()) + parseFloat(Ext.getCmp('tf.jumlah4').getValue());
							Ext.getCmp('tf.totalnya').setValue(total);
							
							var jasdok = Ext.getCmp('tf.jasadok').getValue();
							var potongan = Ext.getCmp('tf.potongan').getValue();
							var hasil = parseFloat(total) + parseFloat(jasdok) - parseFloat(potongan);
							Ext.getCmp('tf.jumlah').setValue(hasil);

                        }
            });
			
			ds_honor_peltam.setBaseParam("nohondok",Ext.getCmp("tf.nohondok").getValue());
			ds_honor_peltam.setBaseParam("iddokter",Ext.getCmp("tf.iddokter").getValue());
			ds_honor_peltam.setBaseParam("tglawal",Ext.getCmp("df.tglawal").getValue());
			ds_honor_peltam.setBaseParam("tglakhir",Ext.getCmp("df.tglakhir").getValue());
			ds_honor_peltam.reload({
                        scope   : this,
                        callback: function(records, operation, success) {
                            var sumtarif4 = 0, sumdiskon4 = 0, zis4 = 0, jumlah4=0;
							ds_honor_peltam.each(function(rec){
												sumtarif4 += parseFloat(rec.get('jasamedis'));
												sumdiskon4 += parseFloat(rec.get('diskonmedis'));
												zis4 += parseFloat(rec.get('zis'));
												jumlah4 += parseFloat(rec.get('jumlah'));
											//	sumjumlah += parseFloat(rec.get('diskonjm'));
											});
							Ext.getCmp('tf.jumlahjm4').setValue(sumtarif4);
							Ext.getCmp('tf.diskonjm4').setValue(sumdiskon4);
							Ext.getCmp('tf.zis4').setValue(zis4);
							Ext.getCmp('tf.jumlah4').setValue(jumlah4);
							
							var total = parseFloat(Ext.getCmp('tf.jumlah1').getValue()) + parseFloat(Ext.getCmp('tf.jumlah2').getValue()) + parseFloat(Ext.getCmp('tf.jumlah3').getValue()) + parseFloat(Ext.getCmp('tf.jumlah4').getValue());
							Ext.getCmp('tf.totalnya').setValue(total);
							
							var jasdok = Ext.getCmp('tf.jasadok').getValue();
							var potongan = Ext.getCmp('tf.potongan').getValue();
							var hasil = parseFloat(total) + parseFloat(jasdok) - parseFloat(potongan);
							Ext.getCmp('tf.jumlah').setValue(hasil);

                        }
            });
			
			var apprec = ds_app1.getAt(0);
			
			if (records) {
				Ext.getCmp('cb.app1').setValue(records.get("approval"));
			} else {
				Ext.getCmp('cb.app1').setValue(apprec.get('nmset'));
			}

				
		}
		
	//Simpan
	function simpan(){
	// validasi //	
		
		var arrpenerimaan1 = [];
		var arrpenerimaan2 = [];
		var arrpenerimaan3 = [];
		var arrpenerimaan4 = [];
		
		var countarr1 = ds_honor_rj.getCount();
		var countarr2 = ds_honor_ri.getCount();
		var countarr3 = ds_honor_jm.getCount();
		var countarr4 = ds_honor_peltam.getCount();
		
		for(var zx = 0; zx < ds_honor_rj.data.items.length; zx++){
			var record = ds_honor_rj.data.items[zx].data;
			znoreg = record.noreg;
			zjnshondok = 1;
			ztotjm = record.jasamedis;
			zdiskon = record.diskonmedis;
			zzis = record.zis;

			arrpenerimaan1[zx] = znoreg + '-' + zjnshondok + '-' + ztotjm + '-' + zdiskon + '-' + zzis;

		}
		for(var zx = 0; zx < ds_honor_ri.data.items.length; zx++){
			var record = ds_honor_ri.data.items[zx].data;
			znoreg = record.noreg;
			zjnshondok = 2;
			ztotjm = record.jasamedis;
			zdiskon = record.diskonmedis;
			zzis = record.zis;
			arrpenerimaan2[zx] = znoreg + '-' + zjnshondok + '-' + ztotjm + '-' + zdiskon + '-' + zzis;

		}
		for(var zx = 0; zx < ds_honor_jm.data.items.length; zx++){
			var record = ds_honor_jm.data.items[zx].data;
			znoreg = record.noreg;
			zjnshondok = 3;
			ztotjm = record.jasamedis;
			zdiskon = record.diskonmedis;
			zzis = record.zis;
			arrpenerimaan3[zx] = znoreg + '-' + zjnshondok + '-' + ztotjm + '-' + zdiskon + '-' + zzis;

		}
		for(var zx = 0; zx < ds_honor_peltam.data.items.length; zx++){
			var record = ds_honor_peltam.data.items[zx].data;
			znoreg = record.nonota;
			zjnshondok = 4;
			ztotjm = record.jasamedis;
			zdiskon = record.diskonmedis;
			zzis = record.zis;
			arrpenerimaan4[zx] = znoreg + '-' + zjnshondok + '-' + ztotjm + '-' + zdiskon + '-' + zzis;
		}
		
		if (!RH.getCompValue('tf.iddokter')) {
			Ext.MessageBox.alert('Informasi','Lengkapi Dokter'); return;
		}
		if (!RH.getCompValue('cb.app1')) {
			Ext.MessageBox.alert('Informasi','Lengkapi Approval'); return;
		}
		if (!RH.getCompValue('cb.jpembayaran')) {
			Ext.MessageBox.alert('Informasi','Lengkapi Jenis Pembayaran'); return;
		}
		if (!RH.getCompValue('cb.stbayar')) {
			Ext.MessageBox.alert('Informasi','Lengkapi Status Bayar'); return;
		}
		
		var waitmsgsimpan = Ext.MessageBox.wait('Menyimpan Data...', 'Informasi');
		Ext.Ajax.request({
			url: (records) ? BASE_URL + 'honor_controller/update':BASE_URL + 'honor_controller/simpan',
			params: {
				nohondok : RH.getCompValue('tf.nohondok'),
				tglhondok : RH.getCompValue('df.tglbayar'),
				
				tglawal 	  : RH.getCompValue('df.tglawal'),
				tglakhir 	  : RH.getCompValue('df.tglakhir'),
				jasadrjaga 	  : RH.getCompValue('tf.jasadok'),
				potonganlain  : RH.getCompValue('tf.potongan'),
				userid	  	  : RH.getCompValue('tf.user'),
				iddokter  	  : RH.getCompValue('tf.iddokter'),
				approval	  : RH.getCompValue('cb.app1'),
				idjnspembayaran	  : RH.getCompValue('cb.jpembayaran'),
				idstbayar	  : RH.getCompValue('cb.stbayar'),
				catatan   	  : RH.getCompValue('ta.catatan'),
				
				tgldikeluarkan : RH.getCompValue('df.tgldikeluarkan'),
				
				arrpenerimaan1 : Ext.encode(arrpenerimaan1),
				arrpenerimaan2 : Ext.encode(arrpenerimaan2),
				arrpenerimaan3 : Ext.encode(arrpenerimaan3),
				arrpenerimaan4 : Ext.encode(arrpenerimaan4),
				
				countarr1		: countarr1,
				countarr2		: countarr2,
				countarr3		: countarr3,
				countarr4		: countarr4,
			},
			success: function(response){
				waitmsgsimpan.hide();
				if (records) {
					Ext.MessageBox.alert('Informasi','Update Data Berhasil');
					ds_hondok.setBaseParam("cbx",false);
					ds_hondok.setBaseParam("iddokter",RH.getCompValue('tf.iddokter'));
					ds_hondok.reload({
                        scope   : this,
                        callback: function(records, operation, success) {
                            var jmlriw = 0;
							ds_hondok.each(function(rec){
								jmlriw += parseFloat(rec.get('jumlah'));
							});
							Ext.getCmp('tf.jmlriw').setValue(jmlriw);

                        }
					});
					ds_hondokmain.reload();
				} else {
					Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
					obj = Ext.util.JSON.decode(response.responseText);
					Ext.getCmp('tf.nohondok').setValue(obj.autonum);
					Ext.getCmp('btn_simpan').disable();
					Ext.getCmp('bt.cetakrekap').enable();
					Ext.getCmp('bt.cetakdetail').enable();
					Ext.getCmp('bt.eksportrek').enable();
					Ext.getCmp('bt.eksportdet').enable();
					Ext.getCmp('bt.batal').enable();
					ds_hondok.setBaseParam("cbx",false);
					ds_hondok.setBaseParam("iddokter",RH.getCompValue('tf.iddokter'));
					ds_hondok.reload({
                        scope   : this,
                        callback: function(records, operation, success) {
                            var jmlriw = 0;
							ds_hondok.each(function(rec){
								jmlriw += parseFloat(rec.get('jumlah'));
							});
							Ext.getCmp('tf.jmlriw').setValue(jmlriw);

                        }
					});
					ds_hondokmain.reload();
				}
			},
			failure : function(){
				waitmsgsimpan.hide();
				if (records) {
					Ext.MessageBox.alert('Informasi', 'Update Data Gagal');
				} else {
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			}
		});
		
	}
	
	function hapus(){
		if (!Ext.getCmp("tf.nopilih").getValue()) {
			Ext.MessageBox.alert('Informasi', 'Pilih Data Riwayat Pembayaran'); return;
		}
		var nohondok = Ext.getCmp('tf.nohondok').getValue();
		var nohondokpilih = Ext.getCmp('tf.nopilih').getValue();
		var message = "Hapus Data Ini?";

		if (records) {
			if (nohondok==nohondokpilih) {
				message = "Hapus Data Yang Sedang Di Edit?";
			} else {
				message = "Hapus Data Ini?";
			}
		}
		Ext.Msg.show({
				title: 'Konfirmasi',
				msg: message,
				buttons: Ext.Msg.YESNO,
				icon: Ext.MessageBox.QUESTION,
				fn: function (response) {
					if ('yes' !== response) {
						return;
					}
					var waitmsghapus = Ext.MessageBox.wait('Menghapus Data...', 'Informasi');
					Ext.Ajax.request({
						url: BASE_URL + 'honor_controller/hapus',
						params: {
							nohondok : Ext.getCmp('tf.nopilih').getValue(),
						},
						
						success: function(response){
								waitmsghapus.hide();
								Ext.MessageBox.alert('Informasi','Hapus Data Berhasil');
								Ext.getCmp('tf.nopilih').setValue(null);
								ds_hondok.setBaseParam("cbx",false);
								ds_hondok.setBaseParam("iddokter",RH.getCompValue('tf.iddokter'));
								ds_hondok.reload({
									scope   : this,
									callback: function(records, operation, success) {
										var jmlriw = 0;
										ds_hondok.each(function(rec){
											jmlriw += parseFloat(rec.get('jumlah'));
										});
										Ext.getCmp('tf.jmlriw').setValue(jmlriw);

									}
								});
								ds_hondokmain.reload();
								if (records) {
									if (nohondok==nohondokpilih) {
										win_honor.close();
									}
								}
						},
						failure : function(){
							waitmsghapus.hide();
							Ext.MessageBox.alert('Informasi', 'Hapus Data Gagal');
						}
					});
				}            
		});

	}
	
	function getnextmonth(lastdate) {
		return new Date(lastdate.getFullYear(), lastdate.getMonth()+1, 1);
	}
	
}