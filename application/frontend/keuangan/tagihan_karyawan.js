//===================LIST PEMBAYARAN KARYAWAN===========================//{
function tagihan_karyawan() {
	
	var arr_cari = [['nokartu', 'Nama Karyawan'],['noreg', 'No. Registrasi'],['tglreg', 'Tgl Registrasi'],['nmpasien', 'Nama Pasien']
					,['norm', 'No Rm']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	var ds_status = new Ext.data.JsonStore({ //static data
		autoDestroy: true,
		fields: [ 'field', 'value' ],
		data: [
		{	"field":"Semua","value":"Semua"},
		{	"field":"Lunas","value":"Lunas"},
		{	"field":"Belum Bayar","value":"Belum Bayar"},
		{	"field":"Belum Lunas","value":"Belum Lunas"},
		{	"field":"N/A","value":"N/A"},
		]
	});
	
	var ds_app1 = dm_app_pg();
	var ds_carabayar = dm_carabayar();
	var storesObj = {app1:ds_app1,carabayar:ds_carabayar};
	
	var ds_gridtkar = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 't_karyawan_controller/get_tkar',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			baseParams: {
				cbxreg : true,
				tglkuitansi1 : Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglkuitansi2 : Ext.util.Format.date(new Date(), 'Y-m-d'),
			},
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'stlunas',
				mapping: 'stlunas'
			},{
				name: 'idkuitansidet',
				mapping: 'idkuitansidet'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nominalugd',
				mapping: 'nominalugd'
			},{
				name: 'total_transaksi',
				mapping: 'total_transaksi'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_bayar',
				mapping: 'total_bayar'
			},{
				name: 'jml_tagihan',
				mapping: 'jml_tagihan'
			},{
				name: 'jml_byr',
				mapping: 'jml_byr'
			},{
				name: 'sisa',
				mapping: 'sisa'
			},{
				name: 'nokartu',
				mapping: 'nokartu'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			}],
			listeners: {
			load: function(store, records, options) {
					var jumlahbiaya = 0;
					var byr_pasien = 0;
					var jml_tagihan = 0;
					var jml_byr = 0;
					var sisa = 0;
                    ds_gridtkar.each(function (rec) { 
                        jumlahbiaya += (parseFloat(rec.get('total_transaksi'))) ? parseFloat(rec.get('total_transaksi')):0;
                        byr_pasien += (parseFloat(rec.get('total_bayar'))) ? parseFloat(rec.get('total_bayar')):0;
                        jml_tagihan += (parseFloat(rec.get('jml_tagihan'))) ? parseFloat(rec.get('jml_tagihan')):0;
                        jml_byr += (parseFloat(rec.get('jml_byr'))) ? parseFloat(rec.get('jml_byr')):0;
                        sisa += (parseFloat(rec.get('sisa'))) ? parseFloat(rec.get('sisa')):0;
                    });

                    Ext.getCmp('tf.all_transaksi').setValue(jumlahbiaya);
                    Ext.getCmp('tf.all_jmlbayar').setValue(byr_pasien);
                    Ext.getCmp('tf.all_tagihan').setValue(jml_tagihan);
                    Ext.getCmp('tf.all_byrkar').setValue(jml_byr);
                    Ext.getCmp('tf.all_sisatagihan').setValue(sisa);
			}
		}
		});
	
	
	var ds_gridtfarmasi = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 't_karyawan_controller/get_tfarmasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			baseParams: {
				cbxreg2 : true,
				tglkuitansi3 : Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglkuitansi4 : Ext.util.Format.date(new Date(), 'Y-m-d'),
			},
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'idkuitansidet',
				mapping: 'idkuitansidet'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'total_transaksi',
				mapping: 'total_transaksi'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'stlunas',
				mapping: 'stlunas'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_bayar',
				mapping: 'total_bayar'
			},{
				name: 'jml_tagihan',
				mapping: 'jml_tagihan'
			},{
				name: 'jml_byr',
				mapping: 'jml_byr'
			},{
				name: 'sisa',
				mapping: 'sisa'
			},{
				name: 'tunai',
				mapping: 'tunai'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'nokartu',
				mapping: 'nokartu'
			}],
			listeners: {
			load: function(store, records, options) {
					var jumlahbiaya = 0;
					var byr_pasien = 0;
					var jml_tagihan = 0;
					var jml_byr = 0;
					var sisa = 0;
                    ds_gridtfarmasi.each(function (rec) { 
                        jumlahbiaya += (parseFloat(rec.get('total_transaksi'))) ? parseFloat(rec.get('total_transaksi')):0;
                        byr_pasien += (parseFloat(rec.get('total_bayar'))) ? parseFloat(rec.get('total_bayar')):0;
                        jml_tagihan += (parseFloat(rec.get('jml_tagihan'))) ? parseFloat(rec.get('jml_tagihan')):0;
                        jml_byr += (parseFloat(rec.get('jml_byr'))) ? parseFloat(rec.get('jml_byr')):0;
                        sisa += (parseFloat(rec.get('sisa'))) ? parseFloat(rec.get('sisa')):0;
                    });

                    Ext.getCmp('tf.all_transaksi2').setValue(jumlahbiaya);
                    Ext.getCmp('tf.all_jmlbayar2').setValue(byr_pasien);
                    Ext.getCmp('tf.all_tagihan2').setValue(jml_tagihan);
                    Ext.getCmp('tf.all_byrkar2').setValue(jml_byr);
                    Ext.getCmp('tf.all_sisatagihan2').setValue(sisa); 
			}
		}
		});
		
	
	
	var sm = new Ext.grid.RowSelectionModel({
		singleSelect: true,
		listeners: {}
	});
	
	var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Bayar',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'resources/img/icons/fam/money.png',
					tooltip: 'Bayar',
                    handler: function(grid, rowIndex) {
						var records = ds_gridtkar.getAt(rowIndex);
						bayar_tkaryawan(ds_gridtkar, records, storesObj);

                    }
                }]
        },
		{
			header: '<center>Status</center>',
			width: 87,
			dataIndex: 'stlunas',
			hidden: true,
			align:'center',	
		},{
			header: '<center>No. Registrasi</center>',
			width: 87,
			dataIndex: 'noreg',
			align:'center',	
		},{
			header: '<center>Tgl. Kuitansi</center>',
			width: 81,
			dataIndex: 'tglkuitansi',
			align:'center',	
			//format: 'd-m-Y',
		},{
			header: '',
			dataIndex: 'idkuitansidet',
			hidden: true
		},{
			header: '',
			dataIndex: 'idjnspelayanan',
			hidden: true
		},{
			header: '',
			dataIndex: 'nmjnspelayanan',
			hidden: true
		},{
			header: '<center>No. RM</center>',
			width: 80,
			dataIndex: 'norm',
			//align:'center',	
		},{
			header: '<center>Nama Pasien</center>',
			width: 100,
			dataIndex: 'nmpasien',
			//align:'center',				
		},{
			header: '<center>Nama Karyawan</center>',
			width: 100,
			dataIndex: 'nokartu',
			//align:'center',				
		},{
			header: '<center>(L/P)</center>',
			width: 50,
			dataIndex: 'kdjnskelamin',
			align:'center',	
			hidden: true	
		},{
			header: '<center>Tgl. Lahir</center>',
			width: 72,
			dataIndex: 'tgllahir',
			align:'center',	
			hidden: true
		},{
			header: '<center>Total <br> Transaksi</center>',
			width: 100,
			dataIndex: 'total_transaksi',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			/* renderer: function(value, p, r){
				if(r.data['idjnspelayanan'] == 2) {
					var jumlah = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']) - parseFloat(r.data['deposit']);
				} else {
					var jumlah = parseFloat(r.data['total']);
				}
				return Ext.util.Format.number(jumlah, '0,000');
			} */	
		},{
			header: '<center>Dibayar <br> Pasien</center>',
			width: 100,
			dataIndex: 'total_bayar',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			//hidden: true
		},{
			header: '<center>Jumlah<br>Potong Gaji</center>',
			width: 100,
			dataIndex: 'jml_tagihan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			hidden: false
		},{
			header: '<center>Jumlah<br>Dibayar</center>',
			width: 100,
			dataIndex: 'jml_byr',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: '<center>Sisa<br>Tagihan</center>',
			width: 100,
			dataIndex: 'sisa',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		}]
    });
	
	var cm2 = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Bayar',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'resources/img/icons/fam/money.png',
					tooltip: 'Bayar',
                    handler: function(grid, rowIndex) {
						var records = ds_gridtfarmasi.getAt(rowIndex);
						bayar_tfarmasi(ds_gridtfarmasi, records, storesObj);

                    }
                }]
        },
		{
			header: '<center>No. Nota</center>',
			width: 87,
			dataIndex: 'nonota',
			align:'center',	
		},{
			header: '<center>Tgl. Kuitansi</center>',
			width: 81,
			dataIndex: 'tglkuitansi',
			align:'center',	
			format: 'd-m-Y',
		},{
			header: '',
			dataIndex: 'idkuitansidet',
			hidden: true
		},{
			header: '',
			dataIndex: 'idjnspelayanan',
			hidden: true
		},{
			header: '',
			dataIndex: 'nmjnspelayanan',
			hidden: true
		},{
			header: '<center>Nama Pasien</center>',
			width: 150,
			dataIndex: 'atasnama',
			//align:'center',				
		},{
			header: '<center>Nama <br /> Karyawan</center>',
			width: 100,
			dataIndex: 'nokartu',
			//align:'center',	
		},{
			header: '<center>Total <br> Transaksi</center>',
			width: 100,
			dataIndex: 'total_transaksi',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			/* renderer: function(value, p, r){
				if(r.data['idjnspelayanan'] == 2) {
					var jumlah = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']) - parseFloat(r.data['deposit']);
				} else {
					var jumlah = parseFloat(r.data['total']);
				}
				return Ext.util.Format.number(jumlah, '0,000');
			} */	
		},{
			header: '<center>Dibayar <br> Pasien</center>',
			width: 100,
			dataIndex: 'total_bayar',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			//hidden: true
		},{
			header: '<center>Jumlah<br>Potong Gaji</center>',
			width: 100,
			dataIndex: 'jml_tagihan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			hidden: false
		},{
			header: '<center>Jumlah<br>Dibayar</center>',
			width: 100,
			dataIndex: 'jml_byr',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: '<center>Sisa<br>Tagihan</center>',
			width: 100,
			dataIndex: 'sisa',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		}]
    });
	
    var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});
	
	var paging = new Ext.PagingToolbar({
		pageSize: 25,
		store: ds_gridtkar,
		displayInfo: true,
		displayMsg: 'Data Pembelian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_tkar = new Ext.grid.EditorGridPanel({
		id: 'id_gridtkar',
		store: ds_gridtkar,
        vw:vw,
		cm:cm,
		sm:sm,
		//tbar: 				[],
		frame: true,
		height: 200,
		autoScroll: true,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		bbar: [{
					text: '<span style="margin-left:0px;font-size:12px;">Total :</span>', style: 'margin-left: 480px',
				},{
					xtype: 'numericfield',
					id: 'tf.all_transaksi',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_jmlbayar',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_tagihan',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_byrkar',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_sisatagihan',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				}],
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
			
			},
		}
	});	
	
	
	var grid_tfarmasi = new Ext.grid.EditorGridPanel({
		id: 'id_gridtfarmasi',
		store: ds_gridtfarmasi,
        vw:vw,
		cm:cm2,
		sm:sm,
		//tbar: 				[],
		frame: true,
		height: 200,
		autoScroll: true,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		bbar: [{
					text: '<span style="margin-left:0px;font-size:12px;">Total :</span>', style: 'margin-left: 450px',
				},{
					xtype: 'numericfield',
					id: 'tf.all_transaksi2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_jmlbayar2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_tagihan2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_byrkar2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_sisatagihan2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				}],
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
			
			},
		}
	});	
	
	
	                    
	var form_t_karyawan = new Ext.form.FormPanel({
		id: 'form_t_karyawan',
		title: 'Daftar Potong Gaji',
		bodyStyle: 'padding: 5px',
		border: true,  
		frame: true,
		autoScroll: true,
		layout: 'form',
		items: [{
					xtype: 'fieldset', title: 'Transaksi RJ / RI / UGD',
					id: 'fs.cariposupp', layout: 'column',
					defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[{
							layout: 'form', columnWidth: 0.50, border: false,  
							items: [{
									xtype: 'compositefield', fieldLabel: 'Status Tagihan',
									items:[{
												xtype:'checkbox',  
												id: 'cbxstbayar',
												listeners: {
													check : function(cb, value) {
														if(value){
															cari_tkar();
														}
													}
												}
											},{
												xtype: 'combo', fieldLabel: '',
												id:'cb.status', autoWidth: true, store: ds_status,
												valueField: 'value', displayField: 'field', editable: false,allowBlank: true,
												triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
												emptyText:'Pilih....',
												listeners: {
													afterrender: function () {
														Ext.getCmp('cb.status').setValue("Semua");
													},
													select: function () {
														cari_tkar();
													}
												}
											}]
								},{
									xtype: 'compositefield', fieldLabel: 'Tgl. Kuitansi',
									items:[{
												xtype:'checkbox',  
												id: 'cbxreg',
												checked: false,
												disabled: false,
												listeners: {
													check : function(cb, value) {
														if(value){
															cari_tkar();
														}
													}
												}
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.tglkuitansi1',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													cari_tkar();
												}
											}
										},{
											xtype: 'label', text: 's.d',
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.tglkuitansi2',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													cari_tkar();
												}
											}
										}]
								},
								{
									xtype: 'compositefield', fieldLabel: 'Cari Berdasarkan',
									items:[{
												xtype:'checkbox',  
												id: 'cbxsearch',
												listeners: {
													check : function(cb, value) {
														if(value){
															Ext.getCmp('cb.search').enable();
															Ext.getCmp('cek').enable();
															//cari_tkar();
														} else {
															Ext.getCmp('cb.search').disable();
															Ext.getCmp('cek').disable();
															
														
														}
													}
												}
											},{
													xtype: 'combo',
													store: ds_cari,
													id: 'cb.search',
													triggerAction: 'all',
													editable: false,
													valueField: 'id',
													displayField: 'nama',
													forceSelection: true,
													submitValue: true,
													typeAhead: true,
													mode: 'local',
													emptyText:'Pilih...',
													selectOnFocus:true,
													disabled: true,
													width: 130,
													//margins: '2 5 0 0',
													listeners: {
														select: function() {
															var cbsearchh = Ext.getCmp('cb.search').getValue();
																if(cbsearchh != ''){
																	Ext.getCmp('cek').focus();
																}
																return;
														}
													}
												},{
													xtype: 'textfield',
													id: 'cek',
													disabled: true,
													width: 200,
													//margins: '2 5 0 0',
													validator: function(){
														var cek = Ext.getCmp('cek').getValue();
														if(cek == ''){
															cari_tkar();
														}
														return;
													}
								},{
										xtype: 'button',
										text: 'Cari',
										id: 'btn_data',
										width: 50,
										style: { 
											background:'-webkit-gradient(linear, left top, left bottom, color-stop(0.5, #fff), color-stop(0.8, #dddddd))',
											background:'-moz-linear-gradient(top, #fff 50%, #dddddd 80%)',
											background:'-webkit-linear-gradient(top, #fff 50%, #dddddd 80%)',
											background:'-o-linear-gradient(top, #fff 50%, #dddddd 80%)',
											background:'-ms-linear-gradient(top, #fff 50%, #dddddd 80%)',
											background:'linear-gradient(to bottom, #fff 50%, #dddddd 80%)',
											'-moz-border-radius':'15px',
											'-webkit-border-radius':'15px',
											'border-radius':'5px',
											border:'1px solid #d6bcd6' 
										},
										handler: function() {
											var cbsearch = Ext.getCmp('cb.search').getValue();
											var cek = Ext.getCmp('cek').getValue();
											if(cbsearch != ''){
												if(cek != ''){
													cari_tkar();
												}else if(cek == ''){
													Ext.MessageBox.alert('Message', 'Isi Data Yang Akan Di Cari..!');
												}
											}else if(cbsearch == ''){
												Ext.MessageBox.alert('Message', 'Cari Berdasarkan Belum Di Pilih..!');
											}
											return;
										}
									}]
								}]
						}]
						
				},
				{
					xtype: 'fieldset', title:'',
					id: 'fs.gridposupp',
					items:[grid_tkar]
				},
				{
					xtype: 'fieldset', title: 'Transaksi Pelayanan Tambahan / Farmasi Pasien Luar',
					id: 'fs.cariposupp_bawah', layout: 'column',
					defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[{
							layout: 'form', columnWidth: 0.50, border: false,  
							items: [{
									xtype: 'compositefield', fieldLabel: 'Status Tagihan',
									items:[{
												xtype:'checkbox',  
												id: 'cbxstbayar2',
												listeners: {
													check : function(cb, value) {
														if(value){
															cari_tfarmasi();
														}
													}
												}
											},{
												xtype: 'combo', fieldLabel: '',
												id:'cb.status2', autoWidth: true, store: ds_status,
												valueField: 'value', displayField: 'field', editable: false,allowBlank: true,
												triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
												emptyText:'Pilih....',
												listeners: {
													afterrender: function () {
														Ext.getCmp('cb.status2').setValue("Semua");
													},
													select: function () {
														cari_tfarmasi();
													}
												}
											}]
								},{
									xtype: 'compositefield', fieldLabel: 'Tgl. Kuitansi',
									items:[{
												xtype:'checkbox',  
												id: 'cbxreg2',
												//checked: true,
												//disabled: true,
												listeners: {
													check : function(cb, value) {
														if(value){
															cari_tfarmasi();
														}
													}
												}
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.tglkuitansi3',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													cari_tfarmasi();
												}
											}
										},{
											xtype: 'label', text: 's.d',
										},{
											xtype: 'datefield', fieldLabel:'', id: 'df.tglkuitansi4',
											width: 100, value: new Date(),
											format: 'd-m-Y',
											listeners:{
												select : function(field, newValue){
													cari_tfarmasi();
												}
											}
										}]
								},
								{
									xtype: 'compositefield', fieldLabel: 'Cari Berdasarkan',
									items:[{
												xtype:'checkbox',  
												id: 'cbxsearch2',
												listeners: {
													check : function(cb, value) {
														if(value){
															Ext.getCmp('cb.search2').enable();
															Ext.getCmp('cek_bawah').enable();
															//cari_tkar();
														} else {
															Ext.getCmp('cb.search2').disable();
															Ext.getCmp('cek_bawah').disable();
															
														
														}
													}
												}
											},{
													xtype: 'combo',
													store: ds_cari,
													id: 'cb.search2',
													triggerAction: 'all',
													editable: false,
													valueField: 'id',
													displayField: 'nama',
													forceSelection: true,
													submitValue: true,
													typeAhead: true,
													mode: 'local',
													emptyText:'Pilih...',
													selectOnFocus:true,
													disabled: true,
													width: 130,
													//margins: '2 5 0 0',
													listeners: {
														select: function() {
															var cbsearchh = Ext.getCmp('cb.search2').getValue();
																if(cbsearchh != ''){
																	Ext.getCmp('cek_bawah').focus();
																}
																return;
														}
													}
												},{
													xtype: 'textfield',
													id: 'cek_bawah',
													disabled: true,
													width: 200,
													//margins: '2 5 0 0',
													validator: function(){
														var cek_bawah = Ext.getCmp('cek_bawah').getValue();
														if(cek_bawah == ''){
															cari_tfarmasi();
														}
														return;
													}
								},{
										xtype: 'button',
										text: 'Cari',
										id: 'btn_data2',
										width: 50,
										style: { 
											background:'-webkit-gradient(linear, left top, left bottom, color-stop(0.5, #fff), color-stop(0.8, #dddddd))',
											background:'-moz-linear-gradient(top, #fff 50%, #dddddd 80%)',
											background:'-webkit-linear-gradient(top, #fff 50%, #dddddd 80%)',
											background:'-o-linear-gradient(top, #fff 50%, #dddddd 80%)',
											background:'-ms-linear-gradient(top, #fff 50%, #dddddd 80%)',
											background:'linear-gradient(to bottom, #fff 50%, #dddddd 80%)',
											'-moz-border-radius':'15px',
											'-webkit-border-radius':'15px',
											'border-radius':'5px',
											border:'1px solid #d6bcd6' 
										},
										handler: function() {
											var cbsearch2 = Ext.getCmp('cb.search2').getValue();
											var cek_bawah = Ext.getCmp('cek_bawah').getValue();
											if(cbsearch2 != ''){
												if(cek_bawah != ''){
													cari_tfarmasi();
												}else if(cek_bawah == ''){
													Ext.MessageBox.alert('Message', 'Isi Data Yang Akan Di Cari..!');
												}
											}else if(cbsearch2 == ''){
												Ext.MessageBox.alert('Message', 'Cari Berdasarkan Belum Di Pilih..!');
											}
											return;
										}
									}]
								}]
						}]
						
				},{
					xtype: 'fieldset', title:'',
					id: 'fs.gridposupp2',
					items:[grid_tfarmasi]
				}
				
		],
		listeners: {
			afterrender: function () {
				
			},
			beforerender: function () {				
				
			}
		}
	});
	
	SET_PAGE_CONTENT(form_t_karyawan);
	
	function cari_tkar(){
		//if (Ext.getCmp('cb.search').getValue()) {
			ds_gridtkar.reload({
				params: { 
					cbxstbayar: Ext.getCmp('cbxstbayar').getValue(),
					cbxsearch: Ext.getCmp('cbxsearch').getValue(),
					cbxreg: Ext.getCmp('cbxreg').getValue(),
					
					key: Ext.getCmp('cb.search').getValue(),
					value: Ext.getCmp('cek').getValue(),
					status: Ext.getCmp('cb.status').getValue(),
					tglkuitansi1: Ext.getCmp('df.tglkuitansi1').getValue(),
					tglkuitansi2: Ext.getCmp('df.tglkuitansi2').getValue(),
				}
			});	
		//}
	}
	
	function cari_tfarmasi(){
		//if (Ext.getCmp('cb.search').getValue()) {
			ds_gridtfarmasi.reload({
				params: { 
					cbxstbayar2: Ext.getCmp('cbxstbayar2').getValue(),
					cbxsearch2: Ext.getCmp('cbxsearch2').getValue(),
					cbxreg2: Ext.getCmp('cbxreg2').getValue(),
					
					key: Ext.getCmp('cb.search2').getValue(),
					value: Ext.getCmp('cek_bawah').getValue(),
					status2: Ext.getCmp('cb.status2').getValue(),
					tglkuitansi3: Ext.getCmp('df.tglkuitansi3').getValue(),
					tglkuitansi4: Ext.getCmp('df.tglkuitansi4').getValue(),
				}
			});	
		//}
	}





  function fnKeyPostingJurnal(value, value2, value3){
  	
  	var val3 = value3.data;
  	var nokuitansipg = val3.nokuitansipg;
  	var kdjurnal = val3.kdjurnal;

  	if(kdjurnal != null){
  		return 'sudah posting';
  	}

  	Ext.QuickTips.init();
    return '<div class="keyPostingJurnal" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">posting</div>';
  }

function bayar_tkaryawan(ds_gridmain, records, storesObj){
		
		var ds_app1 = dm_app1();
		var ds_riwayatbayar = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 't_karyawan_controller/get_kuitansipg',
			method: 'POST'
		}),
		root: 'data', 
		baseParams: {
						idkuitansidet:records.data.idkuitansidet
					},
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: 'nokuitansipg',
			mapping: 'nokuitansipg'
		},{
			name: 'tglkuitansipgreg',
			mapping: 'tglkuitansipgreg'
		},{
			name: 'idkuitansidet',
			mapping: 'idkuitansidet'
		},{
			name: 'nmcarabayar',
			mapping: 'nmcarabayar'
		},{
			name: 'userid',
			mapping: 'userid'
		},{
			name: 'approval',
			mapping: 'approval'
		},{
			name: 'jml_pg',
			mapping: 'jml_pg'
		},{
			name: 'tgljaminput',
			mapping: 'tgljaminput'
		},{
			name: 'catatan',
			mapping: 'catatan'
		},{
			name: 'nama_karyawan',
			mapping: 'nama_karyawan'
		},{
			name: 'nmlengkap',
			mapping: 'nmlengkap'
		},{
			name: 'kdjurnal',
			mapping: 'kdjurnal'
		}],
	/*	load: function(store, records, options) {
					var sbayar = 0;
					ds_riwayatbayar.each(function(rec){
						sbayar += parseFloat(rec.get('jml_pg')); 
					
					});
					Ext.getCmp("tf.tot_all").setValue(sbayar);
					Ext.getCmp("tf.byrpasien").setValue(sbayar);
					Ext.getCmp("tf.tottagihanbyr").setValue(sbayar);
					Ext.getCmp("tf.jmltagihan").setValue(parseFloat(Ext.getCmp("tf.total").getValue()) - sbayar);
					Ext.getCmp("tf.sisa").setValue(Ext.getCmp("tf.jmltagihan").getValue());
			} */
	});
	
		
		ds_riwayatbayar.on('load', function(){
				var sbayar = 0;
				ds_riwayatbayar.each(function(rec){
					sbayar += parseFloat(rec.get('jml_pg')); 
				
				});
				Ext.getCmp("tf.tot_all").setValue(sbayar);
				//Ext.getCmp("tf.byrpasien").setValue(sbayar);
				Ext.getCmp("tf.tottagihanbyr").setValue(sbayar);
				//Ext.getCmp("tf.jmltagihan").setValue(parseFloat(Ext.getCmp("tf.total").getValue()) - sbayar);
				Ext.getCmp("tf.sisa").setValue(parseFloat(Ext.getCmp("tf.jmltagihan").getValue()) - parseFloat(Ext.getCmp("tf.tottagihanbyr").getValue()));
			});
	
		
		
		var vw_rbyr = new Ext.grid.GridView({
			emptyText: '<Belum ada Data>'
		});
		var sm_byr = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var cm_byr = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header: 'No Pembayaran',
				width: 90,
				dataIndex: 'nokuitansipg',
				sortable: true,
			},{
				header: 'Tgl Pembayaran',
				width: 90,
				dataIndex: 'tglkuitansipgreg',
				sortable: true,
			},{
				header: 'Posting',
				width: 90,
				dataIndex: 'kdjurnal',
				sortable: true,
				renderer: fnKeyPostingJurnal
			},{
				header: 'User Input',
				width: 150,
				dataIndex: 'nmlengkap',
				sortable: true,
			//	align: 'right',
				//xtype: 'numbercolumn', format:'0,000',
			},{
				header: 'Approval',
				width: 100,
				dataIndex: 'approval',
				sortable: true,			
			},{
				header: 'Cara Bayar',
				width: 110,
				dataIndex: 'nmcarabayar',
				sortable: true,			
			},{
				header: 'Jumlah Bayar',
				width: 100,
				dataIndex: 'jml_pg',
				sortable: true,
				align: 'right',
			 	xtype: 'numbercolumn', format:'0,000',
			
			},
			{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteKuitansiPG(grid, rowIndex);
                    }
                }]
        }
		]);
		
		var grid_rbyr = new Ext.grid.EditorGridPanel({
			store: ds_riwayatbayar,
			cm: cm_byr,
			sm: sm_byr,
			view: vw_rbyr,
			height: 150,
			width: 900,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: [{
					text: '<span style="margin-left:0px;font-size:12px;">Total :</span>', style: 'margin-left: 560px',
				},{
					xtype: 'numericfield',
					id: 'tf.tot_all',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6',
				}],
				listeners: {
					cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
					var t = e.getTarget();
	    
			    if (t.className == 'keyPostingJurnal')
			    {
	  	      var obj               = ds_riwayatbayar.getAt(rowIdx);
			      var nokuitansipg      = obj.get("nokuitansipg");
			      var tglkuitansipgreg  = obj.get("tglkuitansipgreg");
			      var nama_karyawan     = obj.get("nama_karyawan");
			      //var nama_karyawan     = obj.get("nokartu");
			      var kdjurnal          = obj.get("kdjurnal");
						var ds_jurnal_pg      = new Ext.data.JsonStore({
							proxy: new Ext.data.HttpProxy({
								url: BASE_URL + 't_karyawan_controller/get_pg_jurnaling',
								method: 'POST'
							}),
							root: 'data', 
							totalProperty: 'results',
							autoLoad: true,
							baseParams: {
								nokuitansipg: nokuitansipg
							},
							fields:[
							{ name: 'idakun', mapping: 'idakun', }, 
							{ name: 'kdakun', mapping: 'kdakun', }, 
							{ name: 'nmakun', mapping: 'nmakun', }, 
							{ name: 'noreff', mapping: 'noreff', }, 
							{ name: 'debit', mapping: 'debit', }, 
							{ name: 'kredit', mapping: 'kredit', }
							],
						});

						the_debit = 0;
						the_kredit = 0;
						ds_jurnal_pg.load({
		          scope   : this,
		          callback: function(records, operation, success) {
		            //hitung total debit & kredit
		            total_debit = 0;
		            total_kredit = 0;
		            ds_jurnal_pg.each(function (rec) { 
		              total_debit += parseFloat(rec.get('debit')); 
		              total_kredit += parseFloat(rec.get('kredit')); 
		            });
		            Ext.getCmp("info.total_debit").setValue(total_debit);            
		            Ext.getCmp("info.total_kredit").setValue(total_kredit);
		          }
		        });

					  var grid_detail_jurnal = new Ext.grid.GridPanel({
					    id: 'grid_detail_jurnal',
					    store: ds_jurnal_pg,
					    view: new Ext.grid.GridView({emptyText: '< Tidak ada Data >'}),
					    frame: true,
					    loadMask: true,
					    height: 200,
					    layout: 'anchor',
					    bbar: [
					      '->',
					      {
					        xtype: 'numericfield',
					        thousandSeparator:',',
					        id: 'info.total_debit',
					        width:100,
					        readOnly:true,
					        value:0,
					        align:'right',
					        decimalPrecision:0,
					    },{
					        xtype: 'numericfield',
					        thousandSeparator:',',
					        id: 'info.total_kredit',
					        width:100,
					        readOnly:true,
					        value:0,
					        align:'right',
					        decimalPrecision:0,
					    }],
					    columns: [new Ext.grid.RowNumberer(),
					    {
					      header: '<center>Kode</center>',
					      width: 100,
					      dataIndex: 'kdakun',
					      sortable: true,
					      align:'center',
					    },{
					      header: '<center>Nama Akun</center>',
					      width: 180,
					      dataIndex: 'nmakun',
					      sortable: true,
					      align:'left',
					    },{
					      header: '<center>No.Reff</center>',
					      width: 100,
					      dataIndex: 'noreff',
					      sortable: true,
					      align:'center',
					    },{
					      header: '<center>Debit</center>',
					      width: 100,
					      dataIndex: 'debit',
					      sortable: true,
					      xtype: 'numbercolumn', format:'0,000', align:'right',
					    },{
					      header: '<center>Kredit</center>',
					      width: 100,
					      dataIndex: 'kredit',
					      sortable: true,
					      xtype: 'numbercolumn', format:'0,000', align:'right',
					    }]
					  });

						var posting_form = new Ext.form.FormPanel({
							xtype:'form',
							id: 'frm.posting',
							buttonAlign: 'left',
							autoScroll: true,
							labelWidth: 165, labelAlign: 'right',
							monitorValid: true,
							height: 250, width: 650,
							layout: {
								type: 'form',
								pack: 'center',
								align: 'center'
							},
							frame: true,
							tbar: [{
								text: 'Posting', iconCls:'silk-save', id: 'btn.posting', style: 'marginLeft: 5px',
								handler: function() {
	                  var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
	                  var nokuitansipg_posting = nokuitansipg;
	                  var tglkuitansipgreg_posting = tglkuitansipgreg;
	                  var nama_karyawan_posting = nama_karyawan;
	                  var nominal_jurnal = Ext.getCmp("info.total_debit").getValue(); //nominal debit
				            var nominal_jurnal_kredit = Ext.getCmp("info.total_kredit").getValue(); //nominal kredit		          
	                  var post_grid ='';
	                  var count= 1;
	                  var endpar = ';';
	                    
	                  grid_detail_jurnal.getStore().each(function(rec){ // ambil seluruh grid
	                    var rowData = rec.data; 
	                    if (count == grid_detail_jurnal.getStore().getCount()) {
	                      endpar = ''
	                    }
	                    post_grid += rowData['idakun'] + '^' + 
	                           rowData['kdakun'] + '^' + 
	                           rowData['nmakun']  + '^' + 
	                           rowData['noreff']  + '^' + 
	                           rowData['debit'] + '^' + 
	                           rowData['kredit']  + '^' +
	                        endpar;
	                    count = count+1;
	                  });

	                  if(nokuitansipg_posting == '' || tglkuitansipgreg_posting == ''){
	                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
	                  }
	                  else if(nominal_jurnal != nominal_jurnal_kredit){
	                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
	                  }
	                  else{
	                    
	                    Ext.Ajax.request({
	                      url: BASE_URL + 't_karyawan_controller/posting_pg_karyawan',
	                      params: {
	                        nokuitansipg_posting        : nokuitansipg_posting,
	                        tglkuitansipgreg_posting  : tglkuitansipgreg_posting,
	                        nama_karyawan           : nama_karyawan_posting,
	                        nominal_jurnal          : nominal_jurnal,
	                        post_grid               : post_grid,
	                        userid                  : USERID,
	                      },
	                      success: function(response){
	                        waitmsg.hide();

	                        obj = Ext.util.JSON.decode(response.responseText);
	                        if(obj.success === true){
	                          Ext.getCmp('btn.posting').disable();
	                        }else{
	                          Ext.getCmp('btn.posting').enable();  
	                        }

	                        Ext.MessageBox.alert('Informasi', obj.message);
	                        ds_riwayatbayar.reload();
	                        wPostingForm.close();
	                      }
	                    });
	                   
	                  }

	                }
							},{
								text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
								handler: function() {
									wPostingForm.close();
								}
							}],	
							items: [
								grid_detail_jurnal,
							]
						});

						if(kdjurnal == null)
		        {
		          Ext.getCmp('btn.posting').enable();
		        }else{
		          Ext.getCmp('btn.posting').disable();
		        }
		
						var wPostingForm = new Ext.Window({
							title: 'Posting Potong Gaji Karyawan',
							modal: true, closable:false,
							items: [posting_form]
						});
					
						wPostingForm.show();

			    }
				},
			}	
			}); 
	
		
		
		
		var ds_app1 = storesObj.app1;
		
		//====================FORM UTAMA
		var byr_tkar = new Ext.form.FormPanel({
					id: 'fp.byrtkar',
					region: 'center',
					bodyStyle: 'padding: 5px;',		
					border: false, frame: true,
					height: 550,
					width: 900,
					tbar: [{
						text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
						handler: function() {
									//if (records.data.nobayar) {
										//simpan_update(records.data.nobayar,true);
									//} else {
										simpan_update();
									//}
								}
					},{
						text: 'Cetak', id: 'bt.cetak',  iconCls: 'silk-printer', style: 'marginLeft: 10px',
						disabled: (records) ? false:true,
						hidden: true,
						handler: function() {
							var win = window.open(); 
							win.location.reload();
							win.location = BASE_URL + 'print/print_honordokter/pdf_honordokter/'+RH.getCompValue('tf.nohondok');
						}
					},{
						text: 'Hapus', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px',
						disabled: (records) ? false:true,
						handler: function() {
							//hapus();
						}
						, hidden: true
					},{
						text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
						handler: function() {
							win_byr_tkar.close();
					}
					}],
					items: [
						{
							xtype: 'fieldset', title: '', layout: 'column', border: false,
							defaults: { labelWidth: 100, labelAlign: 'right', }, 
							items:[{
								columnWidth: 0.50, border: false, layout: 'form',
								height: 300,
								items:[
									{
										xtype: 'fieldset',
										id: 'fd.formbayar',
										title: 'Data Tagihan',
										layout: 'form',
										style: 'marginTop: 7px',
										items: [{
												xtype: 'textfield',
												id: 'tf.nama_karyawan',
												width: 150,
												fieldLabel: 'Nama Karyawan',
												labelStyle: 'width:220px',
												value: (records) ? records.get("nokartu"):"",
												
												//style: 'opacity: 0.6; font-weight: bold;',
											},{
												xtype: 'textfield',
												id: 'tf.noreg',
												width: 100,
												fieldLabel: 'No. Registrasi',
												readOnly: true,
												disabled: true,
												value: (records) ? records.get("noreg"):"",
												style: 'opacity: 0.6; font-weight: bold;',
												labelStyle: 'width:220px',
											},{
												xtype: 'textfield',
												id: 'tf.idkuitansidet',
												width: 100,
												fieldLabel: '',
												readOnly: true,
												hidden: true,
												value: (records) ? records.get("idkuitansidet"):"",
												
												
											},{
												xtype: 'datefield',
												id: 'df.tglreg',
												fieldLabel: 'Tgl. Kuitansi',
												format: 'd-m-Y',
												disabled: (records) ? true:false,
												value: (records) ? records.get("tglkuitansi"):new Date(),
												width: 100,
												labelStyle: 'width:220px',
											},{
												xtype: 'textfield',
												id: 'tf.unit',
												width: 150,
												fieldLabel: 'Unit Pelayanan',
												readOnly: true,
												disabled: true,
												labelStyle: 'width:220px',
												value: (records) ? records.get("nmjnspelayanan"):"",
												hidden: true
											},{
												xtype: 'numericfield',
												id: 'tf.total',
												width: 150,
												fieldLabel: 'Total Transaksi',
												readOnly: true,
												disabled: true,
												decimalSeparator: ',', decimalPrecision: 2, alwaysDisplayDecimals: true,
												useThousandSeparator: true, thousandSeparator: '.', value: 0,
												value: (records) ? records.get("total_transaksi"):"",
												labelStyle: 'width:220px',
											},{
												xtype: 'numericfield', id: 'tf.byrpasien', 
												fieldLabel: 'Dibayar Pasien',
												readOnly: true, disabled: true, width: 150,
												decimalSeparator: ',', decimalPrecision: 2, alwaysDisplayDecimals: true,
												useThousandSeparator: true, thousandSeparator: '.', value: 0,
												value: (records) ? records.get("total_bayar"):"",
												
												labelStyle: 'width:220px',
												hidden: false
											},{
												xtype: 'numericfield', id: 'tf.jmltagihan', 
												fieldLabel: 'Jumlah Tagihan',
												readOnly: true, disabled: true, width: 150,
												decimalSeparator: ',', decimalPrecision: 2, alwaysDisplayDecimals: true,
												useThousandSeparator: true, thousandSeparator: '.', value: 0,
												labelStyle: 'width:220px',
												value: (records) ? records.get("jml_tagihan"):"",
												
												hidden: false
											},{
												xtype: 'numericfield', id: 'tf.tottagihanbyr', 
												fieldLabel: 'Jumlah Tagihan Yang Sudah Dibayar',
												readOnly: true, disabled: true, width: 150,
												decimalSeparator: ',', decimalPrecision: 2, alwaysDisplayDecimals: true,
												useThousandSeparator: true, thousandSeparator: '.', value: 0,
												labelStyle: 'width:220px',
											},{
												xtype: 'numericfield',
												id: 'tf.sisa',
												width: 150,
												fieldLabel: 'Sisa Tagihan',
												readOnly: true,
												disabled: true,
												decimalSeparator: ',', decimalPrecision: 2, alwaysDisplayDecimals: true,
												useThousandSeparator: true, thousandSeparator: '.', value: 0,
												//value: (records) ? records.get("noreg"):"",
												labelStyle: 'width:220px',
											},{
												xtype: 'numericfield', id: 'tf.jumlah',
												fieldLabel: 'Jumlah Bayar / Potong Gaji',	
												readOnly: false, disabled: false, width: 150,
												decimalSeparator: ',', decimalPrecision: 2, alwaysDisplayDecimals: true,
												useThousandSeparator: true, thousandSeparator: '.', //value: 0,
												labelStyle: 'width:220px',
											}
										]	
									}
								]
							
							},{
							columnWidth: 0.50, border: false, layout: 'form',
							items:[{
							xtype: 'fieldset',
							id: 'fd.riwayat',
							height: 270,
							title: 'Data Pembayaran',
							layout: 'form',
							style: 'marginTop: 6px',
							items: [/*{
												xtype: 'textfield',
												id: 'tf.nama_karyawan',
												width: 100,
												fieldLabel: 'Nama Karyawan',
												hidden: true,
												value: (records) ? records.get("nmpasien"):"",
												//style: 'opacity: 0.6; font-weight: bold;',
									}, */{
												xtype: 'textfield',
												id: 'tf.nobayar',
												width: 200,
												fieldLabel: 'No. Pembayaran',
												readOnly: true,
												emptyText: 'Otomatis',
												//value: (records) ? records.get("noreg"):"",
												style: 'opacity: 0.6; font-weight: bold;',
									},{
												xtype: 'datefield',
												id: 'df.tglbyr',
												fieldLabel: 'Tgl. Pembayaran',
												format: 'd-m-Y',
												//disabled: (records) ? true:false,
												value: new Date(),
												width: 100,
									},{
												xtype: 'textfield',
												id: 'tf.userid',
												width: 200,
												fieldLabel: 'User Input',
												readOnly: true,
												disabled: true,
												value: USERNAME,
									},{
											xtype: 'combo', fieldLabel: 'Approval',
											id:'cb.approval', width: 200, store: ds_app1,
											valueField: 'nmset', displayField: 'nmset', editable: false,allowBlank: false,
											triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
											emptyText:'Pilih....',value: (records.data.nobayar) ? records.data.app1:"Novi",
									},{
												xtype: 'textarea',
												fieldLabel: 'Catatan',
												id  : 'ta.catatan',
												//value: (records) ? records.get("catatan"):"",
												width : 200,
												height: 45,
									}] 
								}]
						
						},{

							columnWidth: 1, border: false, layout: 'form',
							style: 'marginTop: -20px',
							defaults: { labelWidth: 100, labelAlign: 'right', }, 
							 items:[{
								xtype: 'fieldset',
								id: 'fd.satu',
								title: 'Riwayat Pembayaran',
								layout: 'form',
							//	style: 'marginTop: -5px',
								items: [grid_rbyr]
							}]

						}]
						},
					
					
					
					],
					listeners:{
						afterrender: module_afterrender
					}
		}); 
			
		var win_byr_tkar = new Ext.Window({
				title: 'Pembayaran Karyawan Form',
				modal: true,
				items: [byr_tkar]
			}).show();

		function module_afterrender () {
		
				
		}
		
		
		function fnDeleteKuitansiPG(grid, record){
		var record = ds_riwayatbayar.getAt(record);
		var url = BASE_URL + 't_karyawan_controller/delete_kuitansipg';
		var params = new Object({
						nokuitansipg	: record.data['nokuitansipg']
					});
		RH.deleteGridRecord(url, params, grid );
	}
		
		
		function simpan_update(){
					
				var waitmsg_getotokui = Ext.MessageBox.wait('No. Pembayaran sedang diproses...', 'Info');
				var waitmsgsimpan = Ext.MessageBox.wait('Menyimpan Data...', 'Info');
				var sisa_tagihan = Ext.getCmp('tf.sisa').getValue();
				var jml_tagihan = Ext.getCmp('tf.jumlah').getValue();
				
				if(jml_tagihan > sisa_tagihan){ RH.warning('Inputan Melebihi Sisa Tagihan'); return; };	
				if(sisa_tagihan == 0){ RH.warning('Sudah Lunas'); return; };	
				if(jml_tagihan == ''){ RH.warning('Jumlah Bayar Harus di isi'); return; };	
	
				Ext.Ajax.request({
					url: BASE_URL + 't_karyawan_controller/get_autoNOKWI',
					method: 'POST',
					//params :{ prefix : prefixNOKWI },
					params :{ tglkuitansipgreg: Ext.getCmp('df.tglbyr').getValue() },
					scope: this, async:false,
					waitMsg: "No. Pembayaran sedang diproses...",
					success:  function(result){
						var jsonData = Ext.decode(result.responseText); //bisa utk Ext3 dan Ext4			
						var nokuitansipg = jsonData.nokuitansipg;
						RH.setCompValue('tf.nobayar', nokuitansipg);
						
						if(RH.getCompValue('tf.nobayar')=='0') 
						{ RH.warning('No. Kuitansi gagal dibuat'); waitmsg_getotokui.hide(); return; }
						else {
			//// INSERT NEW KUITANSI-BAYAR ###############################	
							var form = RH.getForm('fp.byrtkar');
							if(form.isValid()){
								waitmsg_getotokui.hide();
								form.submit({
									url: BASE_URL +'t_karyawan_controller/simpan',
									method: 'POST',
									params: {
										nokuitansipg						: Ext.getCmp('tf.nobayar').getValue(),
										tglkuitansipgreg					: Ext.getCmp('df.tglbyr').getValue(),
										idkuitansidet						: Ext.getCmp('tf.idkuitansidet').getValue(),
										userid								: USERID,//Ext.getCmp('tf.userid').getValue(),
										approval							: Ext.getCmp('cb.approval').getValue(),
										jumlah								: Ext.getCmp('tf.jumlah').getValue(),
										catatan								: Ext.getCmp('ta.catatan').getValue(),
										nama_karyawan						: Ext.getCmp('tf.nama_karyawan').getValue()
									},
									waitMsg: 'Tunggu, sedang proses menyimpan...',			
									success: function(response) {
										var jsonData = Ext.decode(response.responseText);
										
										waitmsgsimpan.hide();
										Ext.Msg.alert("Info", "Simpan Data Berhasil");
										
										Ext.getCmp('tf.nobayar').setValue(jsonData.nokuitansipg);
										//Ext.getCmp('btn_save_bayarpg').disable();
										//Ext.getCmp('btn_add_form').disable();
										//Ext.getCmp('btn_baru_bayarpg').enable();
										ds_riwayatbayar.reload();
										ds_gridmain.reload();
										//grid_nya.getStore().removeAll();
										//counttotal();
										
									},
									failure: function(result){
										waitmsgsimpan.hide();
										Ext.MessageBox.alert("Info", "Simpan Data Gagal");
										win_byr_tkar.close();
									}	
								});
							} else { waitmsg_getotokui.hide(); Ext.Msg.alert("Info:", msgSaveInvalid); }
						}
					}
				});	
				/*			
					
				var waitmsgsimpan = Ext.MessageBox.wait('Menyimpan Data...', 'Info');
				
				Ext.Ajax.request({
					url: BASE_URL + 't_karyawan_controller/simpan',
					method: 'POST',
					params: {
						nokuitansipg						: Ext.getCmp('tf.nobayar').getValue(),
						tglkuitansipgreg					: Ext.getCmp('df.tglbyr').getValue(),
						idkuitansidet						: Ext.getCmp('tf.idkuitansidet').getValue(),
						userid								: USERID,//Ext.getCmp('tf.userid').getValue(),
						approval							: Ext.getCmp('cb.approval').getValue(),
						jumlah								: Ext.getCmp('tf.jumlah').getValue(),
						catatan								: Ext.getCmp('ta.catatan').getValue(),
						nama_karyawan						: Ext.getCmp('tf.nama_karyawan').getValue()
					},
					success: function(response) {
						var jsonData = Ext.decode(response.responseText);
						
						waitmsgsimpan.hide();
						Ext.Msg.alert("Info", "Simpan Data Berhasil");
						
						Ext.getCmp('tf.nobayar').setValue(jsonData.nokuitansipg);
						//Ext.getCmp('btn_save_bayarpg').disable();
						//Ext.getCmp('btn_add_form').disable();
						//Ext.getCmp('btn_baru_bayarpg').enable();
						ds_riwayatbayar.reload();
						ds_gridmain.reload();
						//grid_nya.getStore().removeAll();
						//counttotal();
						
					},
					failure: function(result){
						waitmsgsimpan.hide();
						Ext.MessageBox.alert("Info", "Simpan Data Gagal");
						win_byr_tkar.close();
					}					
				});

			}

		
		*/

	
		}


	}
	
}


