function rek_dokter(){
Ext.form.Field.prototype.msgTarget = 'side';
	//var SYS_DATE = new Date();
	var pageSize = 18;
	var ds_dokter = dm_dokter();
	var ds_jkelamin = dm_jkelamin();
	var ds_spdokter = dm_spdokter();
	var ds_status = dm_status();
	var ds_stdokter = dm_stdokter();
	var ds_bagian = dm_bagian();
	var ds_bagianklsperawatan = dm_bagianklsperawatan();
	var ds_bank = dm_bank();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_dokter,
		displayInfo: true,
		displayMsg: 'Data Dokter Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_dokter',
		store: ds_dokter,		
		autoScroll: true,
		height: 530,//autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddDokter();
				//Ext.getCmp('tf.frm.kddokter').setReadOnly(false);
			},
			hidden:true
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 70,
			dataIndex: 'kddokter',
			sortable: true
		},
		{
			header: 'Nama Dokter Tanpa Gelar',
			width: 170,
			dataIndex: 'nmdokter',
			sortable: true
		},
		{
			header: 'Nama Dokter Dengan Gelar',
			width: 220,
			dataIndex: 'nmdoktergelar',
			sortable: true
		},
		{
			header: 'Jenis Kelamin',
			width: 100,
			dataIndex: 'nmjnskelamin',
			sortable: true
		},
		{
			header: '',
			width: 100,
			dataIndex: 'idbank',
			sortable: true,
			hidden: true,
		},{
			header: 'Bank',
			width: 100,
			dataIndex: 'nmbank',
			sortable: true
		},
		{
			header: 'No. Rekening',
			width: 150,
			dataIndex: 'norek',
			sortable: true,
		},
		{
			header: 'Atas Nama',
			width: 150,
			dataIndex: 'atasnama',
			sortable: true
		},
		{
			header: 'Cabang',
			width: 100,
			dataIndex: 'cabang',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditDokter(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Rekening Bank Dokter', iconCls:'silk-money',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadDokter(){
		ds_dokter.reload();
	}
	
	function fnEditDokter(grid, record){
		var record = ds_dokter.getAt(record);
		wEntryDokter(true, grid, record);		
	}
	
	
	/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryDokter(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Rekening Bank Dokter (Edit)':'';
		var dokter_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.dokter',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 220, width: 500,
			layout: 'form', 
			frame: false, 
			defaultType:'textfield',		
			items: [ 
			{
				id: 'tf.frm.iddokter', 
				hidden: true,
			},
			{
				xtype: 'combo', id: 'cb.frm.bank', 
				fieldLabel: 'Bank',
				store: ds_bank, triggerAction: 'all',
				valueField: 'idbank', displayField: 'nmbank',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},
			{
				fieldLabel: 'No. Rek',
				id:'tf.frm.norek',
				width: 300,
			},{
				fieldLabel: 'Atas Nama',
				id:'tf.frm.atasnama',
				width: 300,
			},{
				fieldLabel: 'Cabang',
				id:'tf.frm.cabang',
				width: 150,
			}],
			buttons: [{
				text: 'Simpan', iconCls:'silk-save',
				handler: function() {
					fnSaveDokter();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wDokter.close();
				}
			}]
		});
			
		var wDokter = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [dokter_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setDokterForm(isUpdate, record);
		wDokter.show();

	/**
	FORM FUNCTIONS
	*/	
		function setDokterForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					
					
					Ext.Ajax.request({
						url:BASE_URL + 'dokter_controller/getNmbank',
						params:{
							idbank : record.get('idbank')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							//RH.setCompValue('tf.bagian', r);
						}
					});
					
					RH.setCompValue('tf.frm.iddokter', record.get('iddokter'));
					RH.setCompValue('cb.frm.bank', record.get('idbank'));
					RH.setCompValue('tf.frm.norek', record.get('norek'));
					RH.setCompValue('tf.frm.atasnama', record.data['atasnama']);
					RH.setCompValue('tf.frm.cabang', record.get('cabang'));
				
					return;
				}
			}
		}
		
		function fnSaveDokter(){
			var sParams = new Object({
				iddokter			:	RH.getCompValue('tf.frm.iddokter'),
				idbank				:	RH.getCompValue('cb.frm.bank'),
				norek				:	RH.getCompValue('tf.frm.norek'),
				atasnama			:	RH.getCompValue('tf.frm.atasnama'),
				cabang				:	RH.getCompValue('tf.frm.cabang'),
				
			});
			if(isUpdate){
				var idForm = 'frm.dokter';
				sUrl = BASE_URL +'dokter_controller/update_rek_dokter';
				var msgWait = 'Tunggu, sedang proses menyimpan...';
				var msgSuccess = 'Tambah data berhasil';
				var msgFail = 'Tambah data gagal';
				var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wDokter, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
					
	}
	
}