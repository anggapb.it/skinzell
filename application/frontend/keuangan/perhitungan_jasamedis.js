function perhitungan_jasamedis(){	
	var ds_app1 = dm_app1();
	var ds_carabayar = dm_carabayar();
	var storesObj = {app1:ds_app1,carabayar:ds_carabayar};
	
	var ds_stdoktertim = dm_stdoktertim();
	var storesObj = {stdoktertim:ds_stdoktertim};
	
	var ds_grid = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'timjasamedis_controller/get_jasamedis',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			baseParams: {				
				tglawal	:Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglakhir:Ext.util.Format.date(new Date(), 'Y-m-d')
			},
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'tarifjasa',
				mapping: 'tarifjasa'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idnotadet',
				mapping: 'idnotadet'
			},{
				name: 'idjtm',
				mapping: 'idjtm'
			},{
				name: 'diskonjm',
				mapping: 'diskonjm'
			},{
				name: 'status',
				mapping: 'status'
			},{
				name: 'tarifdiskon',
				mapping: 'tarifdiskon'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'js',
				mapping: 'js'
			}]
		});

	ds_grid.on('beforeload', function(){

	});
	
	var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Pilih',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'resources/img/icons/fam/money.png',
					tooltip: 'Pilih',
                    handler: function(grid, rowIndex) {
						var records = ds_grid.getAt(rowIndex);
						perhitungan_jasamedis_form(grid_nya, records, storesObj);

                    }
                }]
        },
		{
			header: '<center>No. Registrasi</center>',
			width: 88,
			dataIndex: 'noreg',
		},{
			header: '<center>Tgl. Registrasi</center>',
			width: 88,
			dataIndex: 'tglreg',
			align:'center',	
		},{
			header: '<center>No. RM</center>',
			width: 88,
			dataIndex: 'norm',
		},{
			header: '<center>Nama Pasien</center>',
			width: 150,
			dataIndex: 'nmpasien',
		},{
			header: '<center>Bagian</center>',
			width: 95,
			dataIndex: 'nmbagian',
		}/* ,{
			header: '<center>Posisi Pasien</center>',
			width: 92,
			dataIndex: 'nmjnspembayaran',			
		} */,{
			header: '<center>Tindakan/Pelayanan</center>',
			width: 150,
			dataIndex: 'nmpelayanan',			
		},{
			header: '<center>Tarif<br>Jasa Medis</center>',
			width: 100,
			dataIndex: 'tarifjasa',
			xtype: 'numbercolumn', format:'0,000',
			align: 'right'
		},{
			header: '<center>Status</center>',
			width: 100,
			dataIndex: 'status',
		},{
			header: '',
			dataIndex: 'idjtm',
			hidden: true
		},{
			header: '',
			dataIndex: 'diskonjm',
			hidden: true
		},{
			header: '',
			dataIndex: 'tarifdiskon',
			hidden: true
		},{
			header: '',
			dataIndex: 'diskon',
			hidden: true
		}]
    });
	
    var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: 25,
		store: ds_grid,
		displayInfo: true,
		displayMsg: 'Data Pembelian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.EditorGridPanel({
		id: 'grid_posupp',
		store: ds_grid,
        vw:vw,
		cm:cm,
		plugins: cari_data,
		tbar: [{
			xtype: 'compositefield',
			width: 500,
			items: [{
				xtype: 'label', id: 'lb.lb', text: 'Tgl. Registrasi :', margins: '4 10 0 20',
			},{
				xtype: 'datefield',
				id: 'tglawal',
				value: new Date(),
				format: "d-m-Y",
				width: 100,
				listeners:{
					select: function(field, newValue){
						cAdvance();
					}
				}
			},
			{
				xtype: 'label', id: 'lb.lbt', text: 's/d', margins: '4 4 0 0',
			},
			{
				xtype: 'datefield',
				id: 'tglakhir',
				value: new Date(),
				format: "d-m-Y",
				width: 100,
				listeners:{
					select: function(field, newValue){
						cAdvance();
					}
				}
			}]
		},'->'],
		frame: true,
		//autoHeight: true,
		//autoWidth: true,
		height: 530,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		autoScroll: true,
		//bbar: paging,
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
				/* var header = grid_nya.getColumnModel().getColumnHeader(columnIdx).replace('<center>','').replace('</center>','');
				var index = grid_nya.getColumnModel().getDataIndex(columnIdx);
				alert(header + ' ' + index + ' ' + grid_nya.getStore().getTotalCount()); */				
			},
		}
	});	
	                    
	var form_jasamedis = new Ext.form.FormPanel({
		id: 'form_jasamedis',
		title: 'Daftar Tindakan oleh Tim Medis',
		bodyStyle: 'padding: 5px',
		border: true,  
		autoScroll: true,
		layout: 'form',
		items: [grid_nya],
		listeners: {
			afterrender: function () {
				
			},
			beforerender: function () {				
				
			}
		}
	});
	
	SET_PAGE_CONTENT(form_jasamedis);
	
	function cariPo(){
		if (Ext.getCmp('cb.search').getValue()) {
			ds_grid.reload({
				params: { 
					key: Ext.getCmp('cb.search').getValue(),
					value: Ext.getCmp('tf.search').getValue(),
				}
			});	
		}
	}
	
	function cAdvance(){
		ds_grid.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('tglawal').getValue(), 'Y-m-d'));
		ds_grid.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('tglakhir').getValue(), 'Y-m-d'));
		ds_grid.reload();
	}

}