function Mkelbaganperkiraan(){
	var pageSize = 18;
	var ds_klpakun = dm_klpakun();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_klpakun,
		displayInfo: true,
		displayMsg: 'Data Kelompok Bagan Perkiraan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_hari',
		store: ds_klpakun,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddKlpakun();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdklpakun',
			sortable: true
		},
		{
			header: 'Nama',
			width: 300,
			dataIndex: 'nmklpakun',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditKlpakun(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteKlpakun(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Kelompok Bagan Perkiraan', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadKlpakun(){
		ds_klpakun.reload();
	}
	
	function fnAddKlpakun(){
		var grid = grid_nya;
		wEntryKlpakun(false, grid, null);	
	}
	
	function fnEditKlpakun(grid, record){
		var record = ds_klpakun.getAt(record);
		wEntryKlpakun(true, grid, record);		
	}
	
	function fnDeleteKlpakun(grid, record){
		var record = ds_klpakun.getAt(record);
		var url = BASE_URL + 'kelbaganperkiraan_controller/delete_klpakun';
		var params = new Object({
						idklpakun	: record.data['idklpakun']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryKlpakun(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Kelompok Bagan Perkiraan (Edit)':'Kelompok Bagan Perkiraan (Entry)';
	var klpakun_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.klpakun',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idklpakun', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdklpakun', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmklpakun', 
            fieldLabel: 'Nama',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveKlpakun();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wKlpakun.close();
            }
        }]
    });
		
    var wKlpakun = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [klpakun_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setKlpakunForm(isUpdate, record);
	wKlpakun.show();

/**
FORM FUNCTIONS
*/	
	function setKlpakunForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idklpakun'));
				RH.setCompValue('tf.frm.idklpakun', record.get('idklpakun'));
				RH.setCompValue('tf.frm.kdklpakun', record.get('kdklpakun'));
				RH.setCompValue('tf.frm.nmklpakun', record.get('nmklpakun'));
				return;
			}
		}
	}
	
	function fnSaveKlpakun(){
		var idForm = 'frm.klpakun';
		var sUrl = BASE_URL +'kelbaganperkiraan_controller/insert_klpakun';
		var sParams = new Object({
			idklpakun		:	RH.getCompValue('tf.frm.idklpakun'),
			kdklpakun		:	RH.getCompValue('tf.frm.kdklpakun'),
			nmklpakun		:	RH.getCompValue('tf.frm.nmklpakun'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'kelbaganperkiraan_controller/update_klpakun';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wKlpakun, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}