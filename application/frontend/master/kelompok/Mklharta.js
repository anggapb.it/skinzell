function Mklharta(){
  Ext.form.Field.prototype.msgTarget = 'side';

  var pageSize = 18;
  var ds_kelharta = dm_kelharta();
  
  var cari_data = [new Ext.ux.grid.Search({
    iconCls: 'btn_search',
    minChars: 1,
    autoFocus: true,
    autoHeight: true,
    position: 'top',
    mode: 'remote',
    width: 200
  })];
  
  var paging = new Ext.PagingToolbar({
    pageSize: pageSize,
    store: ds_kelharta,
    displayInfo: true,
    displayMsg: 'Data Kelompok Harta Dari {0} - {1} of {2}',
    emptyMsg: 'No data to display'
  });
  
  var grid_nya = new Ext.grid.GridPanel({
    id: 'grid_kelharta',
    store: ds_kelharta,
    autoHeight: true,
    columnLines: true,
    plugins: cari_data,
    pageSize: pageSize,
    tbar: [
    {
      text: 'Tambah',
      id: 'btn_add',
      iconCls: 'silk-add',
      handler: function() {
        fnAddKelharta();
      }
    }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: 'Kode',
      width: 100,
      dataIndex: 'kdkelharta',
      sortable: true
    },{
      header: 'Kelompok Harta',
      width: 250,
      dataIndex: 'nmkelharta',
      sortable: true
    },{
      header: 'Jenis Harta',
      width: 170,
      dataIndex: 'jenisharta',
      renderer : fnJenisHarta,
      sortable: true
    },{
      header: 'Akun Harta',
      width: 150,
      dataIndex: 'akunharta',
      sortable: true
    },{
      header: 'Akun Akumulasi Penyusutan',
      width: 150,
      dataIndex: 'akunakumulasipenyusutan',
      sortable: true
    },{
      header: 'Akun Biaya Penyusutan',
      width: 150,
      dataIndex: 'akunbiayapenyusutan',
      sortable: true
    },{
      xtype: 'actioncolumn',
      width: 50,
      header: 'Edit',
      align:'center',
      items: [{
        getClass: function(v, meta, record) {
          meta.attr = "style='cursor:pointer;'";
        },
        icon   : 'application/framework/img/rh_edit.png',
        tooltip: 'Edit record',
        handler: function(grid, rowIndex) {
          fnEditKelharta(grid, rowIndex);
        }
      }]
    },{
      xtype: 'actioncolumn',
      width: 50,
      header: 'Hapus',
      align:'center',
      items: [{
        getClass: function(v, meta, record) {
          meta.attr = "style='cursor:pointer;'";
        },
        icon   : 'application/framework/img/rh_delete.gif',
        tooltip: 'Hapus record',
        handler: function(grid, rowIndex) {
          fnDeleteKelharta(grid, rowIndex);
        }
      }]
    }],
    bbar: paging
  });
       
  var form_bp_general = new Ext.form.FormPanel({
    id: 'form_bp_general',
    title: 'Kelompok Harta', iconCls:'silk-money',
    layout: 'fit',
    items: [
    {
      xtype: 'panel',
      border: false,
      items: [{
        layout: 'form',
        border: false,
        items: [grid_nya]
      }]
    }]
  });
  SET_PAGE_CONTENT(form_bp_general);
  
  function reloadKelharta(){
    ds_kelharta.reload();
  }
  
  function fnAddKelharta(){
    var grid = grid_nya;
    wEntryKelharta(false, grid, null); 
  }
  
  function fnEditKelharta(grid, record){
    var record = ds_kelharta.getAt(record);
    wEntryKelharta(true, grid, record);    
  }
  
  function fnDeleteKelharta(grid, record){
    var record = ds_kelharta.getAt(record);
    var url = BASE_URL + 'kelharta_controller/delete_kelharta';
    var params = new Object({
      idkelharta  : record.data['idkelharta']
    });
    RH.deleteGridRecord(url, params, grid );
  }

  function fnJenisHarta(value){
    Ext.QuickTips.init();
    if(value == '0'){
      return 'Tidak Disusutkan';
    }else if(value == '1'){
      return 'Disusutkan';  
    }else{
      return value;
    }
  }
}

function wEntryKelharta(isUpdate, grid, record){
  var winTitle = (isUpdate)?'Kelompok Harta (Edit)':'Kelompok Harta (Entry)';
  var ds_jharta = new Ext.data.JsonStore({
      fields: [ 'id', 'name',],
      autoLoad: true,
      data: [
        {"id":"0", "name":"Tidak Disusutkan"},
        {"id":"1", "name":"Disusutkan"},
      ]
    });   

  var kelharta_form = new Ext.form.FormPanel({
    xtype:'form',
    id: 'frm.kelharta',
    buttonAlign: 'left',
    labelWidth: 175, labelAlign: 'right',
    bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
    monitorValid: true,
    height: 250, width: 500,
    layout: 'form', 
    frame: false, 
    defaultType:'textfield',    
    items: [{
      id: 'tf.frm.idkelharta', 
      hidden: true,
    },{
      id: 'tf.frm.kdkelharta', 
      fieldLabel: 'Kode',
      width: 150, allowBlank: false,
    },{
      id: 'tf.frm.nmkelharta', 
      fieldLabel: 'Kelompok Harta',
      width: 300, allowBlank: false,        
    },{
      xtype: 'combo',
      id: 'tf.frm.jenisharta', 
      fieldLabel: 'Jenis Harta',
      width: 300, allowBlank: false, 
      store: ds_jharta,
      triggerAction: 'all',
      editable: false,
      valueField: 'id',
      displayField: 'name',
      forceSelection: true,
      submitValue: true,
      mode: 'local',
      emptyText:'Pilih...',       
    },{
      id: 'tf.frm.akunharta', 
      fieldLabel: 'Akun Harta',
      width: 300, allowBlank: true,        
    },{
      id: 'tf.frm.akunakumulasipenyusutan', 
      fieldLabel: 'Akun Akumulasi Penyusutan',
      width: 300, allowBlank: true,        
    },{
      id: 'tf.frm.akunbiayapenyusutan', 
      fieldLabel: 'Akun Biaya Penyusutan',
      width: 300, allowBlank: true,        
    }],
    buttons: [{
      text: 'Simpan', iconCls:'silk-save',
      handler: function() {
          fnSaveKelharta();
      }
    },{
      text: 'Kembali', iconCls:'silk-arrow-undo',
      handler: function() {
          wKelharta.close();
      }
    }]
  });
    
  var wKelharta = new Ext.Window({
      title: winTitle,
      modal: true, closable:false,
      items: [kelharta_form]
  });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
  setKelhartaForm(isUpdate, record);
  wKelharta.show();

/**
FORM FUNCTIONS
*/  
  function setKelhartaForm(isUpdate, record){
    if(isUpdate){
      if(record != null){
        RH.setCompValue('tf.frm.idkelharta', record.get('idkelharta'));
        RH.setCompValue('tf.frm.kdkelharta', record.get('kdkelharta'));
        RH.setCompValue('tf.frm.jenisharta', record.get('jenisharta'));
        RH.setCompValue('tf.frm.nmkelharta', record.get('nmkelharta'));
        RH.setCompValue('tf.frm.akunharta', record.get('akunharta'));
        RH.setCompValue('tf.frm.akunakumulasipenyusutan', record.get('akunakumulasipenyusutan'));
        RH.setCompValue('tf.frm.akunbiayapenyusutan', record.get('akunbiayapenyusutan'));
        return;
      }
    }
  }
  
  function fnSaveKelharta(){
    var idForm = 'frm.kelharta';
    var sUrl = BASE_URL +'kelharta_controller/insert_kelharta';
    var sParams = new Object({
      idkelharta               : RH.getCompValue('tf.frm.idkelharta'),
      kdkelharta               : RH.getCompValue('tf.frm.kdkelharta'),
      jenisharta               : RH.getCompValue('tf.frm.jenisharta'),
      nmkelharta               : RH.getCompValue('tf.frm.nmkelharta'),
      akunharta                : RH.getCompValue('tf.frm.akunharta'),
      akunakumulasipenyusutan  : RH.getCompValue('tf.frm.akunakumulasipenyusutan'),
      akunbiayapenyusutan      : RH.getCompValue('tf.frm.akunbiayapenyusutan'),
    });
    var msgWait = 'Tunggu, sedang proses menyimpan...';
    var msgSuccess = 'Tambah data berhasil';
    var msgFail = 'Tambah data gagal';
    var msgInvalid = 'Data belum valid (data primer belum terisi)!';
    
    if(isUpdate){
      sUrl = BASE_URL +'kelharta_controller/update_kelharta';
      msgSuccess = 'Update data berhasil';
      msgFail = 'Update data gagal';
    }
    
    //call form grid submit function (common function by RH)
    RH.submitGridForm(idForm, sUrl, sParams, grid, wKelharta, 
      msgWait, msgSuccess, msgFail, msgInvalid);
  }
        
}