function Mcarabayar(){
Ext.form.Field.prototype.msgTarget = 'side';

	var pageSize = 20;
	var ds_carabayar = dm_carabayar();
	//var ds_akunall = dm_akunall();
	var ds_akunall = dm_baganperkiraan();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_carabayar,
		displayInfo: true,
		displayMsg: 'Data Cara Bayar Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_carabayar',
		store: ds_carabayar,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddCarabayar();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdcarabayar',
			sortable: true
		},
		{
			header: 'Cara Bayar',
			width: 300,
			dataIndex: 'nmcarabayar',
			sortable: true
		},{
			header: 'Kode Akun',
			width: 100,
			dataIndex: 'kdakun',
			sortable: true
		},{
			header: 'Nama Akun',
			width: 200,
			dataIndex: 'nmakun',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditCarabayar(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteCarabayar(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Cara Bayar', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadCarabayar(){
		ds_carabayar.reload();
	}
	
	function fnAddCarabayar(){
		var grid = grid_nya;
		wEntryCarabayar(false, grid, null);	
	}
	
	function fnEditCarabayar(grid, record){
		var record = ds_carabayar.getAt(record);
		wEntryCarabayar(true, grid, record);		
	}
	
	function fnDeleteCarabayar(grid, record){
		var record = ds_carabayar.getAt(record);
		var url = BASE_URL + 'carabayar_controller/delete_carabayar';
		var params = new Object({
						idcarabayar	: record.data['idcarabayar']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryCarabayar(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Cara Bayar (Edit)':'Cara Bayar (Entry)';
	var carabayar_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.carabayar',
        buttonAlign: 'left',
		labelWidth: 120, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idcarabayar', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdcarabayar', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmcarabayar', 
            fieldLabel: 'Cara Bayar',
            width: 300,       
        },{
								xtype: 'textfield',
								id: 'tf.kdakun',
								width: 50,
								readOnly: true,
								allowBlank: false,
								hidden: true
								
		},{
							xtype: 'compositefield',
						//	style: 'padding: 5px; marginLeft: 10px; marginBottom: -6px',
						//	width:395,
							items: [{
								xtype: 'textfield',
								fieldLabel: 'Akun',
								id: 'tf.akun',
								emptyText:'Nama Akun..',
								width: 200,
								readOnly: true,
								allowBlank: false
							},
							{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn_data_pen_bgn',
								width: 3,
								handler: function() {
									fnwAkun();
									//ds_akunall.reload();
								}
							}]
		}],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveCarabayar();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wCarabayar.close();
            }
        }]
    });
		
    var wCarabayar = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [carabayar_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setCarabayarForm(isUpdate, record);
	wCarabayar.show();

/**
FORM FUNCTIONS
*/	
	function setCarabayarForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idcarabayar'));
				RH.setCompValue('tf.frm.idcarabayar', record.get('idcarabayar'));
				RH.setCompValue('tf.frm.kdcarabayar', record.get('kdcarabayar'));
				RH.setCompValue('tf.frm.nmcarabayar', record.get('nmcarabayar'));
				RH.setCompValue('tf.akun', record.get('nmakun'));
				RH.setCompValue('tf.kdakun', record.get('kdakun'));
				return;
			}
		}
	}
	
	function fnSaveCarabayar(){
		var idForm = 'frm.carabayar';
		var sUrl = BASE_URL +'carabayar_controller/insert_carabayar';
		var sParams = new Object({
			idcarabayar		:	RH.getCompValue('tf.frm.idcarabayar'),
			kdcarabayar		:	RH.getCompValue('tf.frm.kdcarabayar'),
			nmcarabayar		:	RH.getCompValue('tf.frm.nmcarabayar'),
			kdakun			:	RH.getCompValue('tf.kdakun'),
		});
		var kdakun = RH.getCompValue('tf.kdakun');
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'carabayar_controller/update_carabayar';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wCarabayar, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
	
	
	function fnwAkun(){
		//var ds_akunall = dm_akunall();
		var ds_akunall = dm_baganperkiraan();
	
		function fnkeyAddAkun(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_akun = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Kode Akun',
				dataIndex: 'kdakun',
				width: 100,
				renderer: fnkeyAddAkun
			},{
				header: 'Nama Akun',
				dataIndex: 'nmakun',
				width: 200
			}
		]);
		
		var sm_akun = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_akun = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_akun = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_akunall,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_akun = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_akun = new Ext.grid.GridPanel({
			ds: ds_akunall,
			cm: cm_akun,
			sm: sm_akun,
			view: vw_akun,
			height: 460,
			width: 400,
			plugins: cari_akun,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_akun,
			listeners: {
				cellclick: onCellClickaddakun
			}
		});
		var win_find_cari_akun = new Ext.Window({
			title: 'Cari Akun',
			modal: true,
			items: [grid_find_cari_akun]
		}).show();

		function onCellClickaddakun(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_kdakun = record.data["kdakun"];
					var var_cari_akun = record.data["nmakun"];
								
					//Ext.getCmp('tf.bagian').focus();
					Ext.getCmp("tf.akun").setValue(var_cari_akun);
					Ext.getCmp("tf.kdakun").setValue(var_cari_kdakun);
					
								win_find_cari_akun.close();
				return true;
			}
			return true;
		}
	}

				
}