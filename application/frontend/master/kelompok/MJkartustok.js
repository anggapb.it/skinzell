function MJkartustok(){
	var pageSize = 18;
	var ds_jkartustok = dm_jkartustok();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_jkartustok,
		displayInfo: true,
		displayMsg: 'Data Jenis Kartu Stok Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_kartu',
		store: ds_jkartustok,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddKartu();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdjnskartustok',
			sortable: true
		},
		{
			header: 'Nama Jenis Kartu Stok',
			width: 300,
			dataIndex: 'nmjnskartustok',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditKartu(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteKartu(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Jenis Kartu Stok', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadKartu(){
		ds_jkartustok.reload();
	}
	
	function fnAddKartu(){
		var grid = grid_nya;
		wEntryKartu(false, grid, null);	
	}
	
	function fnEditKartu(grid, record){
		var record = ds_jkartustok.getAt(record);
		wEntryKartu(true, grid, record);		
	}
	
	function fnDeleteKartu(grid, record){
		var record = ds_jkartustok.getAt(record);
		var url = BASE_URL + 'jkartustok_controller/delete_jkartustok';
		var params = new Object({
						idjnskartustok	: record.data['idjnskartustok']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryKartu(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Jenis Kartu Stok (Edit)':'Jenis Kartu Stok (Entry)';
	var jkartu_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.kartu',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idjnskartustok', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdjnskartustok', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmjnskartustok', 
            fieldLabel: 'Nama Jenis Kartu Stok',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveKartu();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wKartu.close();
            }
        }]
    });
		
    var wKartu = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [jkartu_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setKartuForm(isUpdate, record);
	wKartu.show();

/**
FORM FUNCTIONS
*/	
	function setKartuForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				RH.setCompValue('tf.frm.idjnskartustok', record.get('idjnskartustok'));
				RH.setCompValue('tf.frm.kdjnskartustok', record.get('kdjnskartustok'));
				RH.setCompValue('tf.frm.nmjnskartustok', record.get('nmjnskartustok'));
				return;
			}
		}
	}
	
	function fnSaveKartu(){
		var idForm = 'frm.kartu';
		var sUrl = BASE_URL +'jkartustok_controller/insert_jkartustok';
		var sParams = new Object({
			idjnskartustok		:	RH.getCompValue('tf.frm.idjnskartustok'),
			kdjnskartustok		:	RH.getCompValue('tf.frm.kdjnskartustok'),
			nmjnskartustok		:	RH.getCompValue('tf.frm.nmjnskartustok'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'jkartustok_controller/update_jkartustok';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wKartu, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
}