function Mstkasir(){
Ext.form.Field.prototype.msgTarget = 'side';

	var pageSize = 18;
	var ds_stkasir = dm_stkasirr();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_stkasir,
		displayInfo: true,
		displayMsg: 'Data Status Kasir Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_stkasir',
		store: ds_stkasir,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddStkasir();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdstkasir',
			sortable: true
		},
		{
			header: 'Status',
			width: 300,
			dataIndex: 'nmstkasir',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditStkasir(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteStkasir(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Status Kasir', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadStkasir(){
		ds_stkasir.reload();
	}
	
	function fnAddStkasir(){
		var grid = grid_nya;
		wEntryStkasir(false, grid, null);	
	}
	
	function fnEditStkasir(grid, record){
		var record = ds_stkasir.getAt(record);
		wEntryStkasir(true, grid, record);		
	}
	
	function fnDeleteStkasir(grid, record){
		var record = ds_stkasir.getAt(record);
		var url = BASE_URL + 'stkasir_controller/delete_stkasir';
		var params = new Object({
						idstkasir	: record.data['idstkasir']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryStkasir(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Status Kasir (Edit)':'Status Kasir (Entry)';
	var stkasir_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.stkasir',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idstkasir', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdstkasir', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmstkasir', 
            fieldLabel: 'Status',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveStkasir();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wStkasir.close();
            }
        }]
    });
		
    var wStkasir = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [stkasir_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setStkasirForm(isUpdate, record);
	wStkasir.show();

/**
FORM FUNCTIONS
*/	
	function setStkasirForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idstkasir'));
				RH.setCompValue('tf.frm.idstkasir', record.get('idstkasir'));
				RH.setCompValue('tf.frm.kdstkasir', record.get('kdstkasir'));
				RH.setCompValue('tf.frm.nmstkasir', record.get('nmstkasir'));
				return;
			}
		}
	}
	
	function fnSaveStkasir(){
		var idForm = 'frm.stkasir';
		var sUrl = BASE_URL +'stkasir_controller/insert_stkasir';
		var sParams = new Object({
			idstkasir		:	RH.getCompValue('tf.frm.idstkasir'),
			kdstkasir		:	RH.getCompValue('tf.frm.kdstkasir'),
			nmstkasir		:	RH.getCompValue('tf.frm.nmstkasir'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'stkasir_controller/update_stkasir';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wStkasir, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}