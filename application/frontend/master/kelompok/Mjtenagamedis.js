function Mjtenagamedis(){
	var pageSize = 18;
	var ds_jtenagamedis = dm_jtenagamedis();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_jtenagamedis,
		displayInfo: true,
		displayMsg: 'Data Jenis Tenaga Medis Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_jtenagamedis',
		store: ds_jtenagamedis,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddJtenagamedis();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdjnstenagamedis',
			sortable: true
		},
		{
			header: 'Nama',
			width: 300,
			dataIndex: 'nmjnstenagamedis',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditJtenagamedis(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteJtenagamedis(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Jenis Tenaga Medis', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadJtenagamedis(){
		ds_jtenagamedis.reload();
	}
	
	function fnAddJtenagamedis(){
		var grid = grid_nya;
		wEntryJtenagamedis(false, grid, null);	
	}
	
	function fnEditJtenagamedis(grid, record){
		var record = ds_jtenagamedis.getAt(record);
		wEntryJtenagamedis(true, grid, record);		
	}
	
	function fnDeleteJtenagamedis(grid, record){
		var record = ds_jtenagamedis.getAt(record);
		var url = BASE_URL + 'jtenagamedis_controller/delete_jtenagamedis';
		var params = new Object({
						idjnstenagamedis	: record.data['idjnstenagamedis']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryJtenagamedis(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Jenis Tenaga Medis (Edit)':'Jenis Tenaga Medis (Entry)';
	var jtenagamedis_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.jtenagamedis',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idjnstenagamedis', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdjnstenagamedis', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmjnstenagamedis', 
            fieldLabel: 'Nama',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveJtenagamedis();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wJtenagamedis.close();
            }
        }]
    });
		
    var wJtenagamedis = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [jtenagamedis_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setJtenagamedisForm(isUpdate, record);
	wJtenagamedis.show();

/**
FORM FUNCTIONS
*/	
	function setJtenagamedisForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idjnstenagamedis'));
				RH.setCompValue('tf.frm.idjnstenagamedis', record.get('idjnstenagamedis'));
				RH.setCompValue('tf.frm.kdjnstenagamedis', record.get('kdjnstenagamedis'));
				RH.setCompValue('tf.frm.nmjnstenagamedis', record.get('nmjnstenagamedis'));
				return;
			}
		}
	}
	
	function fnSaveJtenagamedis(){
		var idForm = 'frm.jtenagamedis';
		var sUrl = BASE_URL +'jtenagamedis_controller/insert_jtenagamedis';
		var sParams = new Object({
			idjnstenagamedis		:	RH.getCompValue('tf.frm.idjnstenagamedis'),
			kdjnstenagamedis		:	RH.getCompValue('tf.frm.kdjnstenagamedis'),
			nmjnstenagamedis		:	RH.getCompValue('tf.frm.nmjnstenagamedis'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'jtenagamedis_controller/update_jtenagamedis';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wJtenagamedis, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}