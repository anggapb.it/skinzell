function Mjbaganperkiraan(){
	var pageSize = 18;
	var ds_jnsakun = dm_jnsakun();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_jnsakun,
		displayInfo: true,
		displayMsg: 'Data Jenis Bagan Perkiraan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_jakun',
		store: ds_jnsakun,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddJnsakun();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdjnsakun',
			sortable: true
		},
		{
			header: 'Nama',
			width: 300,
			dataIndex: 'nmjnsakun',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditJnsakun(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteJnsakun(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Jenis Bagan Perkiraan', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadJnsakun(){
		ds_jnsakun.reload();
	}
	
	function fnAddJnsakun(){
		var grid = grid_nya;
		wEntryJnsakun(false, grid, null);	
	}
	
	function fnEditJnsakun(grid, record){
		var record = ds_jnsakun.getAt(record);
		wEntryJnsakun(true, grid, record);		
	}
	
	function fnDeleteJnsakun(grid, record){
		var record = ds_jnsakun.getAt(record);
		var url = BASE_URL + 'jbaganperkiraan_controller/delete_jnsakun';
		var params = new Object({
						idjnsakun	: record.data['idjnsakun']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryJnsakun(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Jenis Bagan Perkiraan (Edit)':'Jenis Bagan Perkiraan (Entry)';
	var jnsakun_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.jnsakun',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idjnsakun', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdjnsakun', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmjnsakun', 
            fieldLabel: 'Nama',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveJnsakun();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wJnsakun.close();
            }
        }]
    });
		
    var wJnsakun = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [jnsakun_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setJnsakunForm(isUpdate, record);
	wJnsakun.show();

/**
FORM FUNCTIONS
*/	
	function setJnsakunForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idjnsakun'));
				RH.setCompValue('tf.frm.idjnsakun', record.get('idjnsakun'));
				RH.setCompValue('tf.frm.kdjnsakun', record.get('kdjnsakun'));
				RH.setCompValue('tf.frm.nmjnsakun', record.get('nmjnsakun'));
				return;
			}
		}
	}
	
	function fnSaveJnsakun(){
		var idForm = 'frm.jnsakun';
		var sUrl = BASE_URL +'jbaganperkiraan_controller/insert_jnsakun';
		var sParams = new Object({
			idjnsakun		:	RH.getCompValue('tf.frm.idjnsakun'),
			kdjnsakun		:	RH.getCompValue('tf.frm.kdjnsakun'),
			nmjnsakun		:	RH.getCompValue('tf.frm.nmjnsakun'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'jbaganperkiraan_controller/update_jnsakun';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wJnsakun, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}