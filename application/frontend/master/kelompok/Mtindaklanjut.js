function Mtindaklanjut(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_tinlanjut = dm_tinlanjut();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_tinlanjut,
		displayInfo: true,
		displayMsg: 'Data Tindak Lanjut Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_tinlanjut',
		store: ds_tinlanjut,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddTinlanjut();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 200,
			dataIndex: 'kdtindaklanjut',
			sortable: true 
		},{
			header: 'Nama Tindak Lanjut',
			width: 200,
			dataIndex: 'nmtindaklanjut',
			sortable: true 
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditTinlanjut(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteTinlanjut(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Tindak Lanjut', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadTinlanjut(){
		ds_tinlanjut.reload();
	}
	
	function fnAddTinlanjut(){
		var grid = grid_nya;
		wEntryTinlanjut(false, grid, null);	
	}
	
	function fnEditTinlanjut(grid, record){
		var record = ds_tinlanjut.getAt(record);
		wEntryTinlanjut(true, grid, record);		
	}
	
	function fnDeleteTinlanjut(grid, record){
		var record = ds_tinlanjut.getAt(record);
		var url = BASE_URL + 'tinlanjut_controller/delete_tindaklanjut';
		var params = new Object({
						idtindaklanjut	: record.data['idtindaklanjut']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryTinlanjut(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Tindak Lanjut (Edit)':'Tindak Lanjut (Entry)';
	var tinlanjut_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.tinlanjut',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 180, width: 400,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [    
		{
			id: 'tf.frm.idtindaklanjut',
			hidden: true
            
        },{
			id: 'tf.frm.kdtindaklanjut',
        	fieldLabel: 'Kode',
        	width: 120,
            
        },{
        	id: 'tf.frm.nmtindaklanjut', 
            fieldLabel: 'Nama Tindak Lanjut',
            width: 200,
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveTinlanjut();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wTinlanjut.close();
            }
        }]
    });
		
    var wTinlanjut = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [tinlanjut_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setTinlanjutForm(isUpdate, record);
	wTinlanjut.show();

/**
FORM FUNCTIONS
*/	
	function setTinlanjutForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				RH.setCompValue('tf.frm.idtindaklanjut', record.get('idtindaklanjut'));
				RH.setCompValue('tf.frm.kdtindaklanjut', record.get('kdtindaklanjut'));
				RH.setCompValue('tf.frm.nmtindaklanjut', record.get('nmtindaklanjut'));
				return;
			}
		}
	}
	
	function fnSaveTinlanjut(){
		var idForm = 'frm.tinlanjut';
		var sUrl = BASE_URL +'tinlanjut_controller/insert_tindaklanjut';
		var sParams = new Object({
			idtindaklanjut	:	RH.getCompValue('tf.frm.idtindaklanjut'),
			kdtindaklanjut	:	RH.getCompValue('tf.frm.kdtindaklanjut'),
			nmtindaklanjut	:	RH.getCompValue('tf.frm.nmtindaklanjut')
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'tinlanjut_controller/update_tindaklanjut';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wTinlanjut, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}