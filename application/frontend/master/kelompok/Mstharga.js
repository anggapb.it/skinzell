function Mstharga(){
	var pageSize = 18;
	var ds_stharga = dm_stharga();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_stharga,
		displayInfo: true,
		displayMsg: 'Data Status Harga Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_stharga',
		store: ds_stharga,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddStharga();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdstharga',
			sortable: true
		},
		{
			header: 'Nama',
			width: 300,
			dataIndex: 'nmstharga',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditStharga(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteStharga(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Status Harga', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadStharga(){
		ds_stharga.reload();
	}
	
	function fnAddStharga(){
		var grid = grid_nya;
		wEntryStharga(false, grid, null);	
	}
	
	function fnEditStharga(grid, record){
		var record = ds_stharga.getAt(record);
		wEntryStharga(true, grid, record);		
	}
	
	function fnDeleteStharga(grid, record){
		var record = ds_stharga.getAt(record);
		var url = BASE_URL + 'stharga_controller/delete_stharga';
		var params = new Object({
						idstharga	: record.data['idstharga']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryStharga(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Status Harga (Edit)':'Status Harga (Entry)';
	var stharga_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.stharga',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idstharga', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdstharga', 
            fieldLabel: 'Kode',
            width: 150,
        },{
            id: 'tf.frm.nmstharga', 
            fieldLabel: 'Nama',
            width: 300,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveStharga();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wStharga.close();
            }
        }]
    });
		
    var wStharga = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [stharga_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setSthargaForm(isUpdate, record);
	wStharga.show();

/**
FORM FUNCTIONS
*/	
	function setSthargaForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idStharga'));
				RH.setCompValue('tf.frm.idstharga', record.get('idstharga'));
				RH.setCompValue('tf.frm.kdstharga', record.get('kdstharga'));
				RH.setCompValue('tf.frm.nmstharga', record.get('nmstharga'));
				return;
			}
		}
	}
	
	function fnSaveStharga(){
		var idForm = 'frm.stharga';
		var sUrl = BASE_URL +'stharga_controller/insert_stharga';
		var sParams = new Object({
			idstharga		:	RH.getCompValue('tf.frm.idstharga'),
			kdstharga		:	RH.getCompValue('tf.frm.kdstharga'),
			nmstharga		:	RH.getCompValue('tf.frm.nmstharga'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'stharga_controller/update_stharga';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wStharga, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}