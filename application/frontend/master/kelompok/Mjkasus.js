function Mjkasus(){
	var pageSize = 18;
	var ds_jkasus = dm_jkasus();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_jkasus,
		displayInfo: true,
		displayMsg: 'Data Jenis Kasus Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_jkasus',
		store: ds_jkasus,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddJkasus();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdjnskasus',
			sortable: true
		},
		{
			header: 'Nama',
			width: 300,
			dataIndex: 'nmjnskasus',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditJkasus(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteJkasus(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Jenis Kasus', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadJkasus(){
		ds_jkasus.reload();
	}
	
	function fnAddJkasus(){
		var grid = grid_nya;
		wEntryJkasus(false, grid, null);	
	}
	
	function fnEditJkasus(grid, record){
		var record = ds_jkasus.getAt(record);
		wEntryJkasus(true, grid, record);		
	}
	
	function fnDeleteJkasus(grid, record){
		var record = ds_jkasus.getAt(record);
		var url = BASE_URL + 'jkasus_controller/delete_jkasus';
		var params = new Object({
						idjnskasus	: record.data['idjnskasus']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryJkasus(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Jenis Kasus (Edit)':'Jenis Kasus (Entry)';
	var jkasus_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.jkasus',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idjnskasus', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdjnskasus', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmjnskasus', 
            fieldLabel: 'Nama',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveJkasus();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wJkasus.close();
            }
        }]
    });
		
    var wJkasus = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [jkasus_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setJkasusForm(isUpdate, record);
	wJkasus.show();

/**
FORM FUNCTIONS
*/	
	function setJkasusForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idjnskasus'));
				RH.setCompValue('tf.frm.idjnskasus', record.get('idjnskasus'));
				RH.setCompValue('tf.frm.kdjnskasus', record.get('kdjnskasus'));
				RH.setCompValue('tf.frm.nmjnskasus', record.get('nmjnskasus'));
				return;
			}
		}
	}
	
	function fnSaveJkasus(){
		var idForm = 'frm.jkasus';
		var sUrl = BASE_URL +'jkasus_controller/insert_jkasus';
		var sParams = new Object({
			idjnskasus		:	RH.getCompValue('tf.frm.idjnskasus'),
			kdjnskasus		:	RH.getCompValue('tf.frm.kdjnskasus'),
			nmjnskasus		:	RH.getCompValue('tf.frm.nmjnskasus'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'jkasus_controller/update_jkasus';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wJkasus, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}