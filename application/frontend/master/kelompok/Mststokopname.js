function Mststokopname(){
	var pageSize = 18;
	var ds_ststokopname = dm_ststokopname();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_ststokopname,
		displayInfo: true,
		displayMsg: 'Data Status Stok Opname Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_stso',
		store: ds_ststokopname,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddStso();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdstso',
			sortable: true
		},
		{
			header: 'Nama',
			width: 300,
			dataIndex: 'nmstso',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditStso(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteStso(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Status Stok Opname', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadStso(){
		ds_ststokopname.reload();
	}
	
	function fnAddStso(){
		var grid = grid_nya;
		wEntryStso(false, grid, null);	
	}
	
	function fnEditStso(grid, record){
		var record = ds_ststokopname.getAt(record);
		wEntryStso(true, grid, record);		
	}
	
	function fnDeleteStso(grid, record){
		var record = ds_ststokopname.getAt(record);
		var url = BASE_URL + 'ststokopname_controller/delete_ststokopname';
		var params = new Object({
						idstso	: record.data['idstso']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryStso(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Status Stok Opname (Edit)':'Status Stok Opname (Entry)';
	var stso_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.stso',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idstso', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdstso', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmstso', 
            fieldLabel: 'Nama',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveStso();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wStso.close();
            }
        }]
    });
		
    var wStso = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [stso_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setStsoForm(isUpdate, record);
	wStso.show();

/**
FORM FUNCTIONS
*/	
	function setStsoForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idStso'));
				RH.setCompValue('tf.frm.idstso', record.get('idstso'));
				RH.setCompValue('tf.frm.kdstso', record.get('kdstso'));
				RH.setCompValue('tf.frm.nmstso', record.get('nmstso'));
				return;
			}
		}
	}
	
	function fnSaveStso(){
		var idForm = 'frm.stso';
		var sUrl = BASE_URL +'ststokopname_controller/insert_ststokopname';
		var sParams = new Object({
			idstso		:	RH.getCompValue('tf.frm.idstso'),
			kdstso		:	RH.getCompValue('tf.frm.kdstso'),
			nmstso		:	RH.getCompValue('tf.frm.nmstso'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'ststokopname_controller/update_ststokopname';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wStso, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}