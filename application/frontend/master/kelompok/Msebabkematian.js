function Msebabkematian(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_sebabkematian = dm_penkematian();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_sebabkematian,
		displayInfo: true,
		displayMsg: 'Data Sebab Kematian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_kematian',
		store: ds_sebabkematian,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddKematian();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode Sebab Kematian',
			width: 200,
			dataIndex: 'kdkematian',
			sortable: true 
		},{
			header: 'Nama Sebab Kematian',
			width: 200,
			dataIndex: 'nmkematian',
			sortable: true 
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditKematian(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteKematian(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Sebab Kematian', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadKematian(){
		ds_sebabkematian.reload();
	}
	
	function fnAddKematian(){
		var grid = grid_nya;
		wEntryKematian(false, grid, null);	
	}
	
	function fnEditKematian(grid, record){
		var record = ds_sebabkematian.getAt(record);
		wEntryKematian(true, grid, record);		
	}
	
	function fnDeleteKematian(grid, record){
		var record = ds_sebabkematian.getAt(record);
		var url = BASE_URL + 'penkematian_controller/delete_penkematian';
		var params = new Object({
						idkematian	: record.data['idkematian']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryKematian(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Sebab Kematian (Edit)':'Sebab Kematian (Entry)';
	var kematian_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.kematian',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 180, width: 400,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [    
		{
			id: 'tf.frm.idkematian',
			hidden: true
            
        },{
			id: 'tf.frm.kdkematian',
        	fieldLabel: 'Kode Sebab Kematian',
        	width: 120,
            
        },{
        	id: 'tf.frm.nmkematian', 
            fieldLabel: 'Nama Sebab Kematian',
            width: 200,
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveKematian();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wKematian.close();
            }
        }]
    });
		
    var wKematian = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [kematian_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setKematianForm(isUpdate, record);
	wKematian.show();

/**
FORM FUNCTIONS
*/	
	function setKematianForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				RH.setCompValue('tf.frm.idkematian', record.get('idkematian'));
				RH.setCompValue('tf.frm.kdkematian', record.get('kdkematian'));
				RH.setCompValue('tf.frm.nmkematian', record.get('nmkematian'));
				return;
			}
		}
	}
	
	function fnSaveKematian(){
		var idForm = 'frm.kematian';
		var sUrl = BASE_URL +'penkematian_controller/insert_penkematian';
		var sParams = new Object({
			idkematian	:	RH.getCompValue('tf.frm.idkematian'),
			kdkematian	:	RH.getCompValue('tf.frm.kdkematian'),
			nmkematian	:	RH.getCompValue('tf.frm.nmkematian')
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'penkematian_controller/update_penkematian';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wKematian, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}