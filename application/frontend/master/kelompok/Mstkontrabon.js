function Mstkontrabon(){
	var pageSize = 18;
	var ds_stkontrabon = dm_stkontrabon();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_stkontrabon,
		displayInfo: true,
		displayMsg: 'Data Kontra Bon Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_stkontrabon',
		store: ds_stkontrabon,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddStkontrabon();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdstkontrabon',
			sortable: true
		},
		{
			header: 'Nama',
			width: 300,
			dataIndex: 'nmstkontrabon',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditStkontrabon(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteStkontrabon(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Kontra Bon', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadStkontrabon(){
		ds_stkontrabon.reload();
	}
	
	function fnAddStkontrabon(){
		var grid = grid_nya;
		wEntryStkontrabon(false, grid, null);	
	}
	
	function fnEditStkontrabon(grid, record){
		var record = ds_stkontrabon.getAt(record);
		wEntryStkontrabon(true, grid, record);		
	}
	
	function fnDeleteStkontrabon(grid, record){
		var record = ds_stkontrabon.getAt(record);
		var url = BASE_URL + 'stkontrabon_controller/delete_stkontrabon';
		var params = new Object({
						idstkontrabon	: record.data['idstkontrabon']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryStkontrabon(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Kontra Bon (Edit)':'Kontra Bon (Entry)';
	var stkontrabon_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.stkontrabon',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idstkontrabon', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdstkontrabon', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmstkontrabon', 
            fieldLabel: 'Nama',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSaveStkontrabon();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wStkontrabon.close();
            }
        }]
    });
		
    var wStkontrabon = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [stkontrabon_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setStkontrabonForm(isUpdate, record);
	wStkontrabon.show();

/**
FORM FUNCTIONS
*/	
	function setStkontrabonForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idstkontrabon'));
				RH.setCompValue('tf.frm.idstkontrabon', record.get('idstkontrabon'));
				RH.setCompValue('tf.frm.kdstkontrabon', record.get('kdstkontrabon'));
				RH.setCompValue('tf.frm.nmstkontrabon', record.get('nmstkontrabon'));
				return;
			}
		}
	}
	
	function fnSaveStkontrabon(){
		var idForm = 'frm.stkontrabon';
		var sUrl = BASE_URL +'stkontrabon_controller/insert_stkontrabon';
		var sParams = new Object({
			idstkontrabon		:	RH.getCompValue('tf.frm.idstkontrabon'),
			kdstkontrabon		:	RH.getCompValue('tf.frm.kdstkontrabon'),
			nmstkontrabon		:	RH.getCompValue('tf.frm.nmstkontrabon'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'stkontrabon_controller/update_stkontrabon';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wStkontrabon, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}