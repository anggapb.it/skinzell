function Mpenkematian(){
	var pageSize = 18;
	var ds_penkematian = dm_penkematian();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_penkematian,
		displayInfo: true,
		displayMsg: 'Data Penyebab Kematian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_penkematian',
		store: ds_penkematian,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		pageSize: pageSize,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddPenkematian();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 100,
			dataIndex: 'kdkematian',
			sortable: true
		},
		{
			header: 'Nama',
			width: 300,
			dataIndex: 'nmkematian',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditPenkematian(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeletePenkematian(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Penyebab Kematian', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadPenkematian(){
		ds_penkematian.reload();
	}
	
	function fnAddPenkematian(){
		var grid = grid_nya;
		wEntryPenkematian(false, grid, null);	
	}
	
	function fnEditPenkematian(grid, record){
		var record = ds_penkematian.getAt(record);
		wEntryPenkematian(true, grid, record);		
	}
	
	function fnDeletePenkematian(grid, record){
		var record = ds_penkematian.getAt(record);
		var url = BASE_URL + 'penkematian_controller/delete_penkematian';
		var params = new Object({
						idkematian	: record.data['idkematian']
					});
		RH.deleteGridRecord(url, params, grid );
	}
}


function wEntryPenkematian(isUpdate, grid, record){
	var winTitle = (isUpdate)?'Penyebab Kematian (Edit)':'Penyebab Kematian (Entry)';
	var penkematian_form = new Ext.form.FormPanel({
		xtype:'form',
        id: 'frm.penkematian',
        buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
        bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
        monitorValid: true,
        height: 200, width: 500,
        layout: 'form', 
		frame: false, 
		defaultType:'textfield',		
		items: [     
		{
            id: 'tf.frm.idkematian', 
            hidden: true,
        },    
		{
            id: 'tf.frm.kdkematian', 
            fieldLabel: 'Kode',
            width: 150, allowBlank: false,
        },{
            id: 'tf.frm.nmkematian', 
            fieldLabel: 'Nama',
            width: 300, allowBlank: false,        
        }],
        buttons: [{
            text: 'Simpan', iconCls:'silk-save',
            handler: function() {
                fnSavePenkematian();                           
            }
        }, {
            text: 'Kembali', iconCls:'silk-arrow-undo',
            handler: function() {
                wPenkematian.close();
            }
        }]
    });
		
    var wPenkematian = new Ext.Window({
        title: winTitle,
        modal: true, closable:false,
        items: [penkematian_form]
    });

/**
CALL SET FORM AND SHOW THE FORM (WINDOW)
*/
	setPenkematianForm(isUpdate, record);
	wPenkematian.show();

/**
FORM FUNCTIONS
*/	
	function setPenkematianForm(isUpdate, record){
		if(isUpdate){
			if(record != null){
				//alert(record.get('idkematian'));
				RH.setCompValue('tf.frm.idkematian', record.get('idkematian'));
				RH.setCompValue('tf.frm.kdkematian', record.get('kdkematian'));
				RH.setCompValue('tf.frm.nmkematian', record.get('nmkematian'));
				return;
			}
		}
	}
	
	function fnSavePenkematian(){
		var idForm = 'frm.penkematian';
		var sUrl = BASE_URL +'penkematian_controller/insert_penkematian';
		var sParams = new Object({
			idkematian		:	RH.getCompValue('tf.frm.idkematian'),
			kdkematian		:	RH.getCompValue('tf.frm.kdkematian'),
			nmkematian		:	RH.getCompValue('tf.frm.nmkematian'),
		});
		var msgWait = 'Tunggu, sedang proses menyimpan...';
		var msgSuccess = 'Tambah data berhasil';
		var msgFail = 'Tambah data gagal';
		var msgInvalid = 'Data belum valid (data primer belum terisi)!';
		
		if(isUpdate){
			sUrl = BASE_URL +'penkematian_controller/update_penkematian';
			msgSuccess = 'Update data berhasil';
			msgFail = 'Update data gagal';
		}
		
		//call form grid submit function (common function by RH)
		RH.submitGridForm(idForm, sUrl, sParams, grid, wPenkematian, 
			msgWait, msgSuccess, msgFail, msgInvalid);
	}
				
}