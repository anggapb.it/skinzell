function Msetpelmaks(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_setmakspel = dm_setmakspel();
	//var ds_jnsbrg = dm_jnsbrg();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_setmakspel,
		displayInfo: true,
		displayMsg: 'Data Setting Maks. Treatment Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_setmakspel',
		store: ds_setmakspel,		
		autoScroll: true,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddTbmedis();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Pelayanan',
			width: 250,
			dataIndex: 'nmpelayanan',
			sortable: true
		},
		{
			header: 'Maksimal',
			width: 100,
			dataIndex: 'maksimal',
			sortable: true,
			align:'right',
			//renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditTbmedis(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteTbmedis(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Setting Maks. Treatment', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadTbmedis(){
		ds_setmakspel.reload();
	}
	
	function fnAddTbmedis(){
		var grid = grid_nya;
		wEntryTbmedis(false, grid, null);	
	}
	
	function fnEditTbmedis(grid, record){
		var record = ds_setmakspel.getAt(record);
		wEntryTbmedis(true, grid, record);		
	}
	
	function fnDeleteTbmedis(grid, record){
		var record = ds_setmakspel.getAt(record);
		var url = BASE_URL + 'setmakspel_controller/delete_setmakspel';
		var params = new Object({
						id	: record.data['id']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryTbmedis(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Setting Maks. Treatment (Edit)':'Setting Maks. Treatment (Entry)';
		var setmakspel_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.setmakspel',
			buttonAlign: 'left',
			labelWidth: 130, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 180, width: 500,
			layout: 'form', 
			frame: false, 
			defaultType:'textfield',		
			items: [ 
			{
				id: 'tf.frm.id', 
				hidden: true,
			},
			
			{
				xtype: 'compositefield',
				fieldLabel: 'Nama Pelayanan',
				width: 278,
				items: [{
					xtype: 'textfield', 
					id: 'tf.pelayanan', 
					emptyText:'Pilih...',
					width: 250,
					editable: false,
					allowBlank: false,
					readOnly: true
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.brg',
					width: 4,
					handler: function() {
						fnwpelayanan();
					}
				}]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Maksimal',
				id:'com_maksimal',
				items: [
					{
					xtype: 'numericfield',
					fieldLabel: '',
					id:'tf.frm.maksimal',
					width: 100,
					// decimalPrecision: 2,		
					// decimalSeparator: ',',						
					// thousandSeparator: '.',
					// alwaysDisplayDecimals: true,
					// useThousandSeparator: true,
				}]				
			}],
			buttons: [{
				text: 'Simpan', iconCls:'silk-save',
				handler: function() {
					fnSaveTbsetmakspel();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wTbsetmakspel.close();
				}
			}]
		});
			
		var wTbsetmakspel = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [setmakspel_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setTbsetmakspelForm(isUpdate, record);
		wTbsetmakspel.show();

	/**
	FORM FUNCTIONS
	*/	
		function setTbsetmakspelForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'setmakspel_controller/getNmpelayanan',
						params:{
							kdpelayanan : record.get('kdpelayanan')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.pelayanan', r);
						}
					});
					
					RH.setCompValue('tf.frm.id', record.get('id'));
					//RH.setCompValue('cb.jbarang', record.data['idjnsbrg']);
					RH.setCompValue('tf.pelayanan', record.data['kdpelayanan']);
					RH.setCompValue('tf.frm.maksimal', record.get('maksimal'));
					return;
				}
			}
		}
		
		function fnSaveTbsetmakspel(){
			var idForm = 'frm.setmakspel';
			var sUrl = BASE_URL +'setmakspel_controller/insert_setmakspel';
			var sParams = new Object({
				id		:	RH.getCompValue('tf.frm.id'),
				//idjnsbrg		:	RH.getCompValue('cb.jbarang'),
				kdpelayanan			:	RH.getCompValue('tf.pelayanan'),
				maksimal			:	RH.getCompValue('tf.frm.maksimal'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'setmakspel_controller/update_setmakspel';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wTbsetmakspel, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}					
	}
	
	function fnwpelayanan(){
		var ds_pelayanan = dm_pelayanan();
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_pelayanan = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header: 'Pelayanan',
				dataIndex: 'nmpelayanan',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		
		var sm_pelayanan = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_pelayanan = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_pelayanan = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_pelayanan,
			displayInfo: true,
			displayMsg: 'Data Pelayanan Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_pelayanan = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_pelayanan = new Ext.grid.GridPanel({
			ds: ds_pelayanan,
			cm: cm_pelayanan,
			sm: sm_pelayanan,
			view: vw_pelayanan,
			height: 460,
			width: 430,
			plugins: cari_pelayanan,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_pelayanan,
			listeners: {
				//rowdblclick: klik_cari_pelayanan
				cellclick: onCellClickaddpel
			}
		});
		var win_find_cari_pelayanan = new Ext.Window({
			title: 'Cari Pelayanan',
			modal: true,
			items: [grid_find_cari_pelayanan]
		}).show();
		
		function onCellClickaddpel(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idpelayanan = record.data["nmpelayanan"];
								
					Ext.getCmp('tf.pelayanan').focus();
					Ext.getCmp("tf.pelayanan").setValue(var_cari_idpelayanan);
								win_find_cari_pelayanan.close();
				return true;
			}
			return true;
		}
	}
}
