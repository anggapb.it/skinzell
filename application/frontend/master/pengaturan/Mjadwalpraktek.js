function Mjadwalpraktek(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_jadwalpraktek = dm_jadwalpraktek();
	var ds_dokter = dm_dokter();
	var ds_cbdijpdokter = dm_cbdijpdokter();
	var ds_hari = dm_hari();
	var ds_shift = dm_shift();
	var ds_jampraktek = dm_jampraktek();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_jadwalpraktek,
		displayInfo: true,
		displayMsg: 'Data Jadwal Praktek Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_jadwalpraktek',
		store: ds_jadwalpraktek,		
		autoScroll: true,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddJadwalpraktek();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Bagian',
			width: 150,
			dataIndex: 'nmbagian',
			sortable: true
		},
		{
			header: 'Dokter',
			width: 230,
			dataIndex: 'nmdoktergelar',
			sortable: true
		},
		{
			header: 'Hari',
			width: 80,
			dataIndex: 'nmhari',
			sortable: true
		},
		{
			header: 'Shift',
			width: 80,
			dataIndex: 'nmshift',
			sortable: true
		},
		{
			header: 'Jam Praktek',
			width: 100,
			dataIndex: 'jampraktek',
			sortable: true
		},
		{
			header: 'Keterangan',
			width: 200,
			dataIndex: 'keterangan',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditJadwalpraktek(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteJadwalpraktek(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Jadwal Praktek Dokter', iconCls:'silk-user',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadJadwalpraktek(){
		ds_jadwalpraktek.reload();
	}
	
	function fnAddJadwalpraktek(){
		var grid = grid_nya;
		wEntryJadwalpraktek(false, grid, null);	
	}
	
	function fnEditJadwalpraktek(grid, record){
		var record = ds_jadwalpraktek.getAt(record);
		wEntryJadwalpraktek(true, grid, record);		
	}
	
	function fnDeleteJadwalpraktek(grid, record){
		var record = ds_jadwalpraktek.getAt(record);
		var url = BASE_URL + 'jadwalpraktek_controller/delete_jadwalpraktek';
		var params = new Object({
						idjadwalpraktek	: record.data['idjadwalpraktek']
					});
		RH.deleteGridRecord(url, params, grid );
	}

	/**
	WIN - FORM ENTRY/EDIT 
	*/
	function wEntryJadwalpraktek(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Jadwal Praktek Dokter (Edit)':'Jadwal Praktek Dokter (Entry)';
		var jadwalpraktek_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.jadwalpraktek',
			buttonAlign: 'left',
			labelWidth: 130, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 300, width: 450,
			layout: 'form', 
			frame: false, 
			defaultType:'textfield',		
			items: [ 
			{
				id: 'tf.frm.idjadwalpraktek', 
				hidden: true,
			},
			{
				xtype: 'combo', id: 'cb.frm.bagian', 
				fieldLabel: 'Bagian',
				store: ds_cbdijpdokter, triggerAction: 'all',
				valueField: 'idbagian', displayField: 'nmbagian',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 250,
				editable: false,
				allowBlank: false
			},{
				xtype: 'compositefield',
				fieldLabel: 'Dokter',
				id: 'comp_dtr',
				width: 278,
				items: [{
					xtype: 'textfield',
					id: 'tf.frm.dokter',
					emptyText:'Pilih...',
					width: 250,
					readOnly: true,
					allowBlank: false
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.dtr',
					width: 4,
					handler: function() {
						fncaridokter();
					}
				}]
			},
			{
				xtype: 'combo', id: 'cb.frm.hari', 
				fieldLabel: 'Hari',
				store: ds_hari, triggerAction: 'all',
				valueField: 'idhari', displayField: 'nmhari',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},
			{
				xtype: 'combo', id: 'cb.frm.shift', 
				fieldLabel: 'Shift',
				store: ds_shift, triggerAction: 'all',
				valueField: 'idshift', displayField: 'nmshift',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
			},{
				xtype: 'combo', id: 'cb.frm.jampraktek', 
				fieldLabel: 'Jam Praktek',
				store: ds_jampraktek, triggerAction: 'all',
				valueField: 'nmjampraktek', displayField: 'nmjampraktek',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
			},
			{
				xtype: 'textarea',
				fieldLabel: 'Keterangan',
				id:'ta.frm.keterangan',
				width: 250,
				sortable: true,
			}],
			buttons: [{
				text: 'Simpan', iconCls:'silk-save',
				handler: function() {
					fnSaveJadwalpraktek();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wJadwalpraktek.close();
				}
			}]
		});
			
		var wJadwalpraktek = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [jadwalpraktek_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setJadwalpraktekForm(isUpdate, record);
		wJadwalpraktek.show();

	/**
	FORM FUNCTIONS
	*/	
		function setJadwalpraktekForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'jadwalpraktek_controller/getNmdokter',
						params:{
							iddokter : record.get('iddokter')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.frm.dokter', r);
						}
					});
					
					RH.setCompValue('tf.frm.idjadwalpraktek', record.get('idjadwalpraktek'));
					RH.setCompValue('cb.frm.bagian', record.data['idbagian']);	
					RH.setCompValue('tf.frm.dokter', record.get('iddokter'));
					RH.setCompValue('cb.frm.hari', record.data['idhari']);
					RH.setCompValue('cb.frm.shift', record.get('idshift'));
					RH.setCompValue('cb.frm.jampraktek', record.get('jampraktek'));
					RH.setCompValue('ta.frm.keterangan', record.get('keterangan'));
					return;
				}
			}
		}
		
		function fnSaveJadwalpraktek(){
			var idForm = 'frm.jadwalpraktek';
			var sUrl = BASE_URL +'jadwalpraktek_controller/insert_jadwalpraktek';
			var sParams = new Object({
				idjadwalpraktek		:	RH.getCompValue('tf.frm.idjadwalpraktek'),
				idbagian			:	RH.getCompValue('cb.frm.bagian'),
				iddokter			:	RH.getCompValue('tf.frm.dokter'),
				idhari				:	RH.getCompValue('cb.frm.hari'),
				idshift				:	RH.getCompValue('cb.frm.shift'),
				jampraktek			:	RH.getCompValue('cb.frm.jampraktek'),
				keterangan			:	RH.getCompValue('ta.frm.keterangan'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'jadwalpraktek_controller/update_jadwalpraktek';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wJadwalpraktek, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
					
	}
	
	function fncaridokter(){
		var ds_dokter = dm_dokter();

		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_dokter = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header: 'Dokter',
				dataIndex: 'nmdoktergelar',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		
		var sm_dokter = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_dokter = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_dokter = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_dokter,
			displayInfo: true,
			displayMsg: 'Data Dokter Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_dokter = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_dokter = new Ext.grid.GridPanel({
			ds: ds_dokter,
			cm: cm_dokter,
			sm: sm_dokter,
			view: vw_dokter,
			height: 460,
			width: 430,
			plugins: cari_dokter,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_dokter,
			listeners: {
				cellclick: onCellClickadddokter
			}
		});
		var win_find_cari_dokter = new Ext.Window({
			title: 'Cari Dokter',
			modal: true,
			items: [grid_find_cari_dokter]
		}).show();
		
		function onCellClickadddokter(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_iddokter = record.data["nmdoktergelar"];
								
					Ext.getCmp("tf.frm.dokter").setValue(var_cari_iddokter);
								win_find_cari_dokter.close();
				return true;
			}
			return true;
		}
	}
}