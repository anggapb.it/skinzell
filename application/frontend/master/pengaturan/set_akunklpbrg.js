function set_akunklpbrg(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;		
	var ds_tahunakun = dm_tahunakun();
	var ds_klpbarang = dm_klpbarang();
	var ds_akunklpbrg = dm_akunklpbrg();	
	var ds_akunbeli = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
		url : BASE_URL + 'baganperkiraan_controller/get_akun_perkelompok',
			method: 'POST'
		}),
		baseParams:{
			idklpakun: '1' // 4 = harta
		},
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
                  name: 'idakunparent',
                  mapping: 'idakunparent'
             },{
                  name: 'idakun',
                  mapping: 'idakun'
             },{
                  name: 'kdakun',
                  mapping: 'kdakun'
             },{
                  name: 'nmakun',
                  mapping: 'nmakun'
             },{
                  name: 'keterangan',
                  mapping: 'keterangan'
             },{
                  name: 'idklpakun',
                  mapping: 'idklpakun'
             },{
                  name: 'idjnsakun',
                  mapping: 'idjnsakun'
             },{
                  name: 'idstatus',
                  mapping: 'idstatus'
             },{
                  name: 'userid',
                  mapping: 'userid'
             },{
                  name: 'tglinput',
                  mapping: 'tglinput'
             },]
	});
  var ds_akunjual = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
		url : BASE_URL + 'baganperkiraan_controller/get_akun_perkelompok',
			method: 'POST'
		}),
		baseParams:{
			idklpakun: '4' // 4 = pendapatan
		},
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
                  name: 'idakunparent',
                  mapping: 'idakunparent'
             },{
                  name: 'idakun',
                  mapping: 'idakun'
             },{
                  name: 'kdakun',
                  mapping: 'kdakun'
             },{
                  name: 'nmakun',
                  mapping: 'nmakun'
             },{
                  name: 'keterangan',
                  mapping: 'keterangan'
             },{
                  name: 'idklpakun',
                  mapping: 'idklpakun'
             },{
                  name: 'idjnsakun',
                  mapping: 'idjnsakun'
             },{
                  name: 'idstatus',
                  mapping: 'idstatus'
             },{
                  name: 'userid',
                  mapping: 'userid'
             },{
                  name: 'tglinput',
                  mapping: 'tglinput'
             },]
	});

	var arr_cari = [['nmklpbarang', 'Kelompok Barang'],['tahun', 'Tahun']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	var cari_baganperkiraan = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];

	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var editor = new Ext.ux.grid.RowEditor({
    saveText: 'Update',
		clicksToEdit: 1
  });
	
	var row_gridnya = new Ext.grid.RowSelectionModel({
		singleSelect: true
	});
	
	var vw_grid_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	var grid_nya = new Ext.grid.EditorGridPanel({
		id: 'gp.akunbrg',
		store: ds_akunklpbrg,
		sm: row_gridnya,
		view: vw_grid_nya,
		tbar: [{
			xtype: 'tbtext',
			style: 'marginLeft: 10px',
			text: 'Search :'
		},{
			xtype: 'combo', 
			id: 'cb.search', width: 150, 
			store: ds_cari, 
			valueField: 'id', displayField: 'nama',
			editable: false, triggerAction: 'all',
			forceSelection: true, submitValue: true, mode: 'local',
			emptyText:'Pilih...', disabled: true,
			listeners: {
				select: function() {
					var cbsearchh = Ext.getCmp('cb.search').getValue();
						if(cbsearchh != ''){
							Ext.getCmp('cek').enable();
							Ext.getCmp('btn_cari').enable();
							Ext.getCmp('cek').focus();
						}
						return;
				}
			}
		},{
			xtype: 'textfield',
			style: 'marginLeft: 7px',
			id: 'cek',
			width: 185,
			disabled: true,
			validator: function(){
				var cek = Ext.getCmp('cek').getValue();
				if(cek == ''){
					fnSearchgrid();
				}
				return;
			}
		},{ 
			text: 'Cari',
			id:'btn_cari',
			iconCls: 'silk-find',
			style: 'marginLeft: 7px',
			disabled: true,
			handler: function() {
				var btncari = Ext.getCmp('cek').getValue();
					if(btncari != ''){
						fnSearchgrid();
					}else{
						Ext.MessageBox.alert('Message', 'Tidak ada data yang di cari');
					}
				return;
			}
		}],
		//plugins: editor,//cari_data,
		autoScroll: true,
		columnLines: true,
		height: 260, 
		forceFit: true,		
        loadMask: true,
		frame: true,
		clicksToEdit: 1,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Tahun',
			width: 60,
			dataIndex: 'tahun',
			sortable: true,
			hidden: true
		},
		{
			header: '',
			width: 60,
			dataIndex: 'idklpbrg',
			sortable: true,
			hidden: true
		},
		{
			header: 'Kelompok Barang',
			width: 250,
			dataIndex: 'nmklpbarang',
			sortable: true,
			align: 'left',
		},{
			header: 'Akun Debit Pembelian',
			width: 200,
			dataIndex: 'akunbeli',
			sortable: true,
			align: 'left',
		},{
			header: 'Akun Kredit Penjualan',
			width: 200,
			dataIndex: 'akunjual',
			sortable: true,
			align: 'left',
		},
		{
      xtype: 'actioncolumn',
      width: 45,
			header: 'Hapus',
			align:'center',
      items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
        icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
        handler: function(grid, rowIndex) {
					fnHapusData(rowIndex);
        }
      }]
    }]
	});
	
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Setting Akun Kelompok Barang (Kelompok Akun : PENDAPATAN)', iconCls:'silk-user',
		width: 900, Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		//defaults: { labelWidth: 150, labelAlign: 'right', width: 910, },
		defaults: { labelWidth: 150, labelAlign: 'right'},
		items: [{
			layout: 'form', columnWidth: 0.5,
			items: [{
				xtype: 'fieldset', 
				title: 'Setting Akun Kelompok Barang',
				layout: 'form',
				id: 'f_akunbrg',
				height: 170,
				boxMaxHeight:250,
				items: [{
							xtype: 'combo', id: 'cb_tahun', 
							fieldLabel: 'Tahun', 
							store: ds_tahunakun, triggerAction: 'all',
							valueField: 'tahun', displayField: 'tahun',
							forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih Tahun...', width: 90,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var isi = Ext.getCmp('cb_tahun').getValue();
									if (isi){
			
										ds_akunklpbrg.setBaseParam('tahun', isi);
										reload_grid();
								
									} else {
										
									}
								}
							}
						},{
					xtype: 'container', fieldLabel: 'Kelompok Barang',
					layout: 'hbox',
					items: [{
									xtype: 'textfield', //margins: '0 0 0 4',
									id: 'tf_kdklpbrg',
									width: 100, readOnly: true,
									style : 'opacity:0.6',
									hidden: true
					},{
							xtype: 'combo', id: 'cb_klpbarang', //fieldLabel: '',
							store: ds_klpbarang, triggerAction: 'all',
							valueField: 'idklpbrg', displayField: 'nmklpbarang',
							forceSelection: true, submitValue: true, 
							//margins: '0 0 0 4',
							mode: 'local', emptyText:'Pilih Kelompok Barang..', width: 200,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var kdklpbrg = Ext.getCmp('cb_klpbarang').getValue();
									if (kdklpbrg){
											Ext.getCmp('tf_kdklpbrg').setValue(kdklpbrg);
									} 
								}
							}
						}]
				},{
					xtype: 'container', fieldLabel: 'Akun Debit Pembelian',
					layout: 'hbox',
					items: [{
									xtype: 'textfield', //margins: '0 0 0 4',
									id: 'tf_kdakunbrg',
									width: 100, readOnly: true,
									style : 'opacity:0.6',
									hidden: true
					},{
							xtype: 'combo', id: 'cb_akun', fieldLabel: 'Akun Debit Pembelian',
							store: ds_akunbeli, triggerAction: 'all',
							valueField: 'idakun', displayField: 'nmakun',
							forceSelection: true, submitValue: true, 
							//margins: '0 0 0 4',
							mode: 'local', emptyText:'Pilih Akun..', width: 200,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var kdakunbrg = Ext.getCmp('cb_akun').getValue();
									if (kdakunbrg){
											Ext.getCmp('tf_kdakunbrg').setValue(kdakunbrg);
									} 
								}
							}
						}]
				},{
					xtype: 'container', fieldLabel: 'Akun Kredit Penjualan',
					layout: 'hbox',
					items: [{
									xtype: 'textfield', //margins: '0 0 0 4',
									id: 'tf_kdakunbrgjual',
									width: 100, readOnly: true,
									style : 'opacity:0.6',
									hidden: true
					},{
							xtype: 'combo', id: 'cb_akunjual', fieldLabel: 'Akun Kredit Penjualan',
							store: ds_akunjual, triggerAction: 'all',
							valueField: 'idakun', displayField: 'nmakun',
							forceSelection: true, submitValue: true, 
							//margins: '0 0 0 4',
							mode: 'local', emptyText:'Pilih Akun..', width: 200,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var kdakunbrg = Ext.getCmp('cb_akunjual').getValue();
									if (kdakunbrg){
											Ext.getCmp('tf_kdakunbrgjual').setValue(kdakunbrg);
									} 
								}
							}
						}]
				},
			{
				xtype: 'button',
				text: 'Simpan',
				id: 'bt.cpy',
				iconCls:'silk-save',
				style: 'margin: 5px 0 0 10px',
				width: 72,
				handler: function(){
					save_akun_brg();
				}					
			},{
				xtype: 'button',
				text: 'Salin Setting Akun Pelayanan Dari Tahun Sebelumnnya',
				id: 'bt_cpdata',
				iconCls: 'silk-save',
				style: 'marginLeft: 190px; marginTop: -22px; marginBottom: 0px',
				width: 72,
				handler: function(){								
								var cbtahun = Ext.getCmp("cb_tahun").getValue();
								if(cbtahun != ""){
									fncopydata();
									Ext.getCmp("cb.tahunlama").setValue(cbtahun);
								} else {
									alert('Tahun Harus Di Pilih');
								}
								return;
						}						
			}]
			}]
		},{
			layout: 'form', columnWidth: 0.5,
			items: [{
					xtype: 'fieldset', 
					title: 'Informasi',
					layout: 'form',
					id: 'f_akunbrg2',
					height: 170,
					boxMaxHeight:250,
					items:[{
						xtype: 'compositefield',
						items: [{
						xtype: 'label', id: 'lb.klpbrg', text: 'Jumlah Kelompok Barang:', margins: '0 0 0 1',
									},{
									xtype: 'textfield', margins: '0 0 0 4',
									id: 'tf_jmlklpbrg',
									width: 50, readOnly: true,
									//style : 'opacity:0.6'
								}]
						
						},{
					
						xtype: 'compositefield',
						items: [{
						xtype: 'label', id: 'lb.klpbrg', text: 'Jumlah Kelompok Barang Sudah di Setting:', margins: '0 0 0 1',
									},{
									xtype: 'textfield', margins: '0 0 0 4',
									id: 'tf_jmlset',
									width: 50, readOnly: true,
									//style : 'opacity:0.6'
								}]
						
						},{
					
						xtype: 'compositefield',
						items: [{
						xtype: 'label', id: 'lb.klpbrg', text: 'Jumlah Kelompok Barang Belum di Setting:', margins: '0 0 0 0',
									},{
									xtype: 'textfield', margins: '0 0 0 4',
									id: 'tf_jmlnotset',
									width: 50, readOnly: true,
									//style : 'opacity:0.6'
								}]
						
						}]
			}]
		
		},
		{
			xtype: 'container',
			style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Daftar Setting Akun Pelayanan',
				layout: 'form',
				height: 310,
				boxMaxHeight:325,
				items: [{
					xtype: 'panel', layout:'form', columnWidth: 0.99,
					id:'fp.master',
					items: [grid_nya],
					
				}]
			}]
		}],		
		listeners: {
			afterrender: mulai
		}
	});
	
	
	function reload_grid(){
		
		ds_akunklpbrg.reload({
			params: { 
				tahun: RH.getCompValue('cb_tahun', true),
			},
			callback: function(results){
				if(results == 0) return;
				var count1 = 0;
				var count2 = 0;
				var count3 = 0;
				
				ds_akunklpbrg.each(function (rec) { 
					if (rec.data['nmklpbarang'] != '') {
						count1 = count1 + 1; 
					}
					if (rec.data['kdakunbeli'] != '' && rec.data['kdakunjual'] != '') {
						count2 = count2 + 1; 
					}
					if (rec.data['kdakunbeli'] == '' || rec.data['kdakunjual'] == '') {
						count3 = count3 + 1; 
					}
				});
				
				RH.setCompValue('tf_jmlklpbrg', count1);
				RH.setCompValue('tf_jmlset', count2);
				RH.setCompValue('tf_jmlnotset', count3);
				
				
			}
		});
	
		
	
	}
	
	function save_akun_brg(){
	
		if(RH.isEmpty('cb_tahun')){ RH.warning('Tahun harus di pilih'); return; };
		if(RH.isEmpty('cb_klpbarang')){ RH.warning('Kelompok Barang harus di pilih'); return; };
		if(RH.isEmpty('cb_akun')){ RH.warning('Akun harus debit di pilih'); return; };
		if(RH.isEmpty('cb_akunjual')){ RH.warning('Akun harus kredit di pilih'); return; };
		
		
		var form = RH.getForm('form_bp_general');
				if(form.isValid()){
					form.submit({
						url: BASE_URL +'setakunklpbrg_controller/insert_akunbrg',
						method: 'POST',
						params: get_insertAkunbrg(), 
						waitMsg: 'Tunggu, sedang proses menyimpan...',			
						success: function(){
							//Ext.getCmp('gp.akunbrg').store.reload();	
							reload_grid();
							Ext.Msg.alert("Info:", msgSaveSuccess);	
										
						},
						failure: function(){ Ext.Msg.alert("Info:", msgSaveFail); },
					});
				} else { Ext.Msg.alert("Info:", msgSaveInvalid); }
	}
	
	function get_insertAkunbrg(){		
		return new Object({
			tahun			:	RH.getCompValue('cb_tahun'),
			idklpbrg		:	RH.getCompValue('cb_klpbarang'),
			idakun			:	RH.getCompValue('cb_akun'),
			idakunjual			:	RH.getCompValue('cb_akunjual'),
		});
	}
	
	
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	
	function mulai(){
		//fTotal();
		//Ext.getCmp('btn_add').setDisabled(true);
		Ext.getCmp('cb.search').setDisabled(false);
		ds_akunklpbrg.reload();
	}
	
	function fnHapusData(record){
		var record = ds_akunklpbrg.getAt(record);
		Ext.Msg.show({
			title: 'Konfirmasi',
			msg: 'Hapus data yang dipilih..?',
			buttons: Ext.Msg.YESNO,
			icon: Ext.MessageBox.QUESTION,
			fn: function (response) {
				if ('yes' !== response) {
					return;
				}
				Ext.Ajax.request({
				url: BASE_URL + 'setakunklpbrg_controller/delete_data',
				params: {				
					tahun		: record.data['tahun'],
					idklpbrg	: record.data['idklpbrg']
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
					//Ext.getCmp('gp.akunbrg').store.reload();
					reload_grid();
					//fTotal();
				},
				failure: function() {
					//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
				}
			});
			
			}            
		});	
	}
	
	
	
	
	function fncopydata(){		
		var form_copy = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.copy',
			labelWidth: 120, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 235, width: 350,
			layout: 'form',
			frame: true,	
			items: [{
				xtype: 'fieldset', layout:'form', height: 80,
				frame:true,
				items: [{
					xtype: 'combo', id: 'cb.tahunlama', fieldLabel: 'Tahun Asal',
					store: ds_tahunakun, triggerAction: 'all',
					valueField: 'tahun', displayField: 'tahun',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih Tahun..', width: 170,
					editable: false, //readOnly: true, style: 'opacitty: 0.6',
				},{
					xtype: 'combo', id: 'cb.tahunbaru', fieldLabel: 'Tahun Tujuan',
					store: ds_tahunakun, triggerAction: 'all',
					valueField: 'tahun', displayField: 'tahun',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih Tahun..', width: 170,
					editable: false,
					allowBlank: false,
				}]
			},{
				xtype: 'button',
				text: 'Simpan',
				id: 'bt.cpy',
				iconCls:'silk-save',
				style: 'margin: -5px 0 0 10px',
				width: 72,
				handler: function(){
					copydata();
				}						
			},{
				xtype: 'button',
				text: 'Kembali',
				id: 'bt.kemb',
				iconCls:'silk-arrow-undo',
				style: 'marginLeft: 90px; marginTop: -22px; marginBottom: 15px',
				width: 72,
				handler: function(){
					win_salin_dta.close();
				}						
			},{
				xtype: 'fieldset', title: 'Informasi :',
				items: [{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -13px 20px 0 -5px; font-size: 13px', width: 320,
					items: [{
						xtype: 'label',
						text: '1. Tahun asal akan menyailin dan menimpa',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -22px 20px 0 10px; font-size: 13px', width: 350,
					items: [{
						xtype: 'label',
						text: ' (replace) tahun tujuan',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -18px 20px 0 -5px; font-size: 13px', width: 350,
					items: [{
						xtype: 'label',
						text: '2. Tahun asal tidak boleh sama dengan tahun tujuan',
					}]
				}]
			}] 
		});
				
		function copydata(){
			var cekvaltariflama = Ext.getCmp("cb.tahunlama").getValue();
			var cekvaltarifbaru = Ext.getCmp("cb.tahunbaru").getValue();
			if (cekvaltariflama != cekvaltarifbaru){
				Ext.Msg.show({
					title: 'Konfirmasi',
					msg: 'Salin Data..?',
					buttons: Ext.Msg.YESNO,
					icon: Ext.MessageBox.QUESTION,
					fn: function (response) {
						if ('yes' !== response) {
							return;
						}
						Ext.Ajax.request({
						url: BASE_URL + 'setakunklpbrg_controller/copy_data',
						params: {
							tahun_lama	: Ext.getCmp('cb.tahunlama').getValue(),
							tahun_baru	: Ext.getCmp('cb.tahunbaru').getValue()
						},
						success: function(response){
							//Ext.MessageBox.alert('Informasi', 'Copy Berhasil..');
							Ext.getCmp('gp.akunbrg').store.reload();
							win_salin_dta.close();
						},
						failure: function() {
							//Ext.Msg.alert("Informasi", "Copy Data Gagal");
						}
					});
					
					}            
				});	
			}else{
				Ext.Msg.alert("Informasi", "Tahun tidak boleh sama..");
			}
		}
	
		var win_salin_dta = new Ext.Window({
			title: 'Salin Setting Akun Pelayanan',
			modal: true, closable: false,
			items: [form_copy]
		}).show();
	}
}