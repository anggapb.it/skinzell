function set_akun_pelayanan(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 50;	
	
	var ds_pelayanan = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'akunpelayanan_controller/get_pelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nourut',
				mapping: 'nourut'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'pel_kdpelayanan',
				mapping: 'pel_kdpelayanan'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});	
	var ds_tahunakun = dm_tahunakun();	
	var ds_akunpelayanan = dm_akunpelayanan();	
	
	var arr_cari = [['nmpelayanan', 'Pelayanan'],['klpnmpelayanan', 'Parent Pelayanan'],['nmjnspelayanan', 'Jenis Pelayanan']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	var cari_baganperkiraan = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];

	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_pelayanan,
		displayInfo: true,
		displayMsg: 'Data Daftar Pelayanan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	//Checkbox pada Grid
	var cbGrid = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			beforerowselect : function (sm, rowIndex, keep, rec) {
				if (this.deselectingFlag && this.grid.enableDragDrop){
					this.deselectingFlag = false;
					this.deselectRow(rowIndex);
					return this.deselectingFlag;
				}
				return keep;
			}
		}	
	});

var cbGrid2 = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			beforerowselect : function (sm, rowIndex, keep, rec) {
				if (this.deselectingFlag && this.grid.enableDragDrop){
					this.deselectingFlag = false;
					this.deselectRow(rowIndex);
					return this.deselectingFlag;
				}
				return keep;
			}
		}	
	});

	
	var grid_akunpel = new Ext.grid.GridPanel({
		id: 'grid_akunpel',
		sm: cbGrid,
		store: ds_pelayanan,		
		autoScroll: true,
		//autoHeight: true,
		columnLines: true,
		height: 230,
		plugins: cari_baganperkiraan,
		tbar: [],
		//sm: sm_nya,
		frame: true,
		columns: [new Ext.grid.RowNumberer(),
		cbGrid,
		
		{
			header: 'Kode',
			width: 70,
			dataIndex: 'kdpelayanan',
			sortable: true
		},
		{
			header: 'Nama Pelayanan',
			width: 470,
			dataIndex: 'nmpelayanan',
			sortable: true
		},
		{
			header: 'Jenis Pelayanan',
			width: 120,
			dataIndex: 'nmjnspelayanan',
			sortable: true
		},
		{
			header: 'Parent',
			width: 130,
			dataIndex: 'nmparent',
			sortable: true,
			renderer: function(value, p, r){
				var parent = '';
					if(r.data['nmparent'] == 0) parent = '';
					if(r.data['nmparent'] != 0) parent = r.data['nmparent'];
				return parent ;
			}
		}],
		bbar: paging
	});
	
	function fncekkdakun(){
		var m = grid_akunpel.getSelectionModel().getSelections();
		for(var i=0; i < m.length; i++){
			var rec = m[i];
			console.log(rec);
			if(rec){
				console.log(rec.get("kdpelayanan"));
				var kdpelayanan = rec.data['kdpelayanan'];
				Ext.Ajax.request({
					url: BASE_URL + 'akunpelayanan_controller/cekidakunpel',
					method: 'POST',
					params: {
						tahun : Ext.getCmp('cb.frm.tahun').getValue(),
						kdpelayanan : kdpelayanan
					},
					success: function(response){
						akun = response.responseText;
						if (akun =='1') {
							Ext.MessageBox.alert('Message', 'Data Sudah Ada...');
						} else {
							//Ext.MessageBox.confirm('Message', 'Tambah Data Yang Di Pilih..?' , add);
							add();
						}
					}
				});
			}
		}
	}
	
	function add(btn){
		/* console.log(btn);
		if(btn == 'yes')
		{ */
			var m = grid_akunpel.getSelectionModel().getSelections();
			var store = grid_akunpel.getStore();			
			for(var i=0; i< m.length; i++){
				var rec = m[i];
				console.log(rec);
				if(rec){
					//console.log(rec.get("kdpelayanan"));
					var kdpelayanan = rec.data['kdpelayanan'];
					Ext.Ajax.request({
						url: BASE_URL +'akunpelayanan_controller/insert_akun',
						method: 'POST',
						params: {
							kdpelayanan 	: kdpelayanan,
							tahun 			: Ext.getCmp('cb.frm.tahun').getValue()
						},
						success: function(){
							Ext.getCmp('gp.akunpel').store.reload();
						}
					});
				}
			}			
		//}
	}
	
	
	function fnSearchgrid(){		
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
		ds_akunpelayanan.setBaseParam('key',  '1');
		ds_akunpelayanan.setBaseParam('id',  idcombo);
		ds_akunpelayanan.setBaseParam('name',  nmcombo);
		ds_akunpelayanan.load();
	}
	
	var editor = new Ext.ux.grid.RowEditor({
    saveText: 'Update',
		clicksToEdit: 1
  });
	
	var row_gridnya = new Ext.grid.RowSelectionModel({
		singleSelect: true
	});
	
	var vw_grid_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	var grid_nya = new Ext.grid.EditorGridPanel({
		id: 'gp.akunpel',
		store: ds_akunpelayanan,
		//sm: row_gridnya,
		sm: cbGrid2,
		view: vw_grid_nya,
		tbar: [{
			xtype: 'tbtext',
			style: 'marginLeft: 10px',
			text: 'Search :'
		},{
			xtype: 'combo', 
			id: 'cb.search', width: 150, 
			store: ds_cari, 
			valueField: 'id', displayField: 'nama',
			editable: false, triggerAction: 'all',
			forceSelection: true, submitValue: true, mode: 'local',
			emptyText:'Pilih...', disabled: true,
			listeners: {
				select: function() {
					var cbsearchh = Ext.getCmp('cb.search').getValue();
						if(cbsearchh != ''){
							Ext.getCmp('cek').enable();
							Ext.getCmp('btn_cari').enable();
							Ext.getCmp('cek').focus();
						}
						return;
				}
			}
		},{
			xtype: 'textfield',
			style: 'marginLeft: 7px',
			id: 'cek',
			width: 185,
			disabled: true,
			validator: function(){
				var cek = Ext.getCmp('cek').getValue();
				if(cek == ''){
					fnSearchgrid();
				}
				return;
			}
		},{ 
			text: 'Cari',
			id:'btn_cari',
			iconCls: 'silk-find',
			style: 'marginLeft: 7px',
			disabled: true,
			handler: function() {
				var btncari = Ext.getCmp('cek').getValue();
					if(btncari != ''){
						fnSearchgrid();
					}else{
						Ext.MessageBox.alert('Message', 'Tidak ada data yang di cari');
					}
				return;
			}
		}],
		//plugins: editor,//cari_data,
		autoScroll: true,
		columnLines: true,
		height: 260, 
		forceFit: true,		
    loadMask: true,
		frame: true,
		clicksToEdit: 1,
		columns: [new Ext.grid.RowNumberer(),cbGrid2,
		{
			header: 'Tahun',
			width: 60,
			dataIndex: 'tahun',
			sortable: true,
			hidden: true
		},{
			header: 'Kode',
			width: 60,
			dataIndex: 'kdpelayanan',
			sortable: true,
			hidden: true
		},{
			header: 'Parent',
			width: 120,
			dataIndex: 'klpnmpelayanan',
			sortable: true,
			align: 'left',
		},{
			header: 'Jenis Pelayanan',
			width: 120,
			dataIndex: 'nmjnspelayanan',
			sortable: true,
			align: 'left',
		},{
			header: 'Pelayanan',
			width: 280,
			dataIndex: 'nmpelayanan',
			sortable: true,
			align: 'left',
		},{
			header: 'Akun <br />Sarana RS',
			width: 100,
			dataIndex: 'kdakunjs',
			sortable: true,
			align: 'center',
		},
		{
			header: 'Akun <br />Jasa Medis',
			width: 100,
			dataIndex: 'kdakunjm',
			sortable: true,
			align: 'center',
		},
		{
			header: 'Akun <br />Jasa Profesi',
			width: 100,
			dataIndex: 'kdakunjp',
			sortable: true,
			align: 'center',
		},
		{
			header: 'Akun <br />BHP',
			width: 100,
			dataIndex: 'kdakunbhp',
			sortable: true,
			align: 'center',
		},{
      xtype: 'actioncolumn',
      width: 45,
			header: 'Hapus',
			align:'center',
      items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
        icon: 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
        handler: function(grid, rowIndex){
					fnHapusData(rowIndex);
        }
      }]
    }],

	});
	
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Setting Akun Pelayanan', iconCls:'silk-user',
		width: 900, Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		//defaults: { labelWidth: 150, labelAlign: 'right', width: 910, },
		items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 1,
			//defaults: { labelWidth: 87, labelAlign: 'right'},
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Daftar Pelayanan',
				layout: 'form',
				height: 260,
				boxMaxHeight:270,
				items: [grid_akunpel]
			}]
		},
		{
			xtype: 'container',
			style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Daftar Setting Akun Pelayanan',
				layout: 'form',
				height: 310,
				boxMaxHeight:325,
				items: [{
					xtype: 'panel', layout:'form', columnWidth: 0.99,
					id:'fp.master',
					items: [grid_nya],
					tbar: [
						{
							text: 'Tambah',
							id: 'btn_add',
							iconCls: 'silk-add',
							handler: function()
							{
								var m = grid_akunpel.getSelectionModel().getSelections();
								if(m.length > 0)
								{
									fncekkdakun(); 
								}
								else
								{
									Ext.MessageBox.alert('Message', 'Data Belum Di Pilih...!');
								}
							}
						},'-',
						{
							xtype: 'tbtext',
							text: '<div style="margin: 0 4px 0 6px">Tahun :</div>'
						},
						{
							xtype: 'combo', id: 'cb.frm.tahun', 
							store: ds_tahunakun, triggerAction: 'all',
							valueField: 'tahun', displayField: 'tahun',
							forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih Tahun...', width: 90,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var isi = Ext.getCmp('cb.frm.tahun').getValue();
									if (isi){
										Ext.getCmp('btn_add').enable();
										Ext.getCmp('cb.search').enable();
										Ext.getCmp('bt_cpdata').enable();
										Ext.getCmp('btn_settingakun').enable();
																
										Ext.getCmp("df.blnawal").setValue(RH.getRecordFieldValue(ds_tahunakun, 'tglawal', 'tahun', isi));
										Ext.getCmp("df.blnakhir").setValue(RH.getRecordFieldValue(ds_tahunakun, 'tglakhir', 'tahun', isi));
										
										ds_akunpelayanan.setBaseParam('tahun', isi);
										Ext.getCmp('gp.akunpel').store.reload();
								
									} else {
										Ext.getCmp('btn_add').disable(); 
										Ext.getCmp('cb.search').disable();
										Ext.getCmp('bt_cpdata').disable();
									}
								}
							}
						},'-',
						{
							xtype: 'tbtext',
							text: '<div style="margin: 0 4px 0 16px">Periode :</div>',
							hidden: true
						},
						{	
							xtype: 'datefield',
							id: 'df.blnawal',
							width: 120,
							format: 'd-F-Y',
							readOnly: true,
							style: 'opacity: 0.6',
							hidden: true
						},
						{
							xtype: 'tbtext',
							text: '<div style="margin: 0 4px 0 6px"> s.d </div>',
							hidden: true
						},
						{	
							xtype: 'datefield',
							id: 'df.blnakhir',
							width: 120,
							format: 'd-F-Y',
							readOnly: true,
							style: 'opacity: 0.6',
							hidden: true
						},
						{
							text: 'Setting Pelayanan Terpilih',
							id: 'btn_settingakun',
							iconCls: 'silk-save',
							disabled: true,
							style: 'marginLeft: 10px',
							handler: function(){
								
								//var grid_akun = grid_nya.getStore().getCount();
								var countSelected = grid_nya.getSelectionModel().getSelections();
                
								var cbtahun = Ext.getCmp("cb.frm.tahun").getValue();
								if(cbtahun != "" && countSelected.length > 0){
									update_setting();
								} else if(cbtahun != "" && countSelected.length == 0){
									Ext.MessageBox.alert("Informasi", "Data belum dipilih");
								}
								return;
							}
							
						},{
							text: 'Salin Setting Akun Pelayanan Dari Tahun Sebelumnnya',
							id: 'bt_cpdata',
							iconCls: 'silk-save',
							disabled: true,
							style: 'marginLeft: 10px',
							handler: function(){								
								var cbtahun = Ext.getCmp("cb.frm.tahun").getValue();
								if(cbtahun != ""){
									fncopydata();
									Ext.getCmp("cb.tahunlama").setValue(cbtahun);
								}
								return;
							}
						}
					]
				}]
			}]
		}],		
		listeners: {
			afterrender: mulai
		}
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	function mulai(){
		//fTotal();
		Ext.getCmp('btn_add').setDisabled(true);
		Ext.getCmp('cb.search').setDisabled(true);
		ds_akunpelayanan.reload();
	}
	
	/* function fTotal(){
		ds_tpumum.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				ds_tpumum.each(function (rec) { sum += parseFloat(rec.get('total')); });
				Ext.getCmp("tf.total").setValue(sum);
			}
		});
	} */
	
	
	function fnHapusData(record){
		var record = ds_akunpelayanan.getAt(record);
		Ext.Msg.show({
			title: 'Konfirmasi',
			msg: 'Hapus data yang dipilih..?',
			buttons: Ext.Msg.YESNO,
			icon: Ext.MessageBox.QUESTION,
			fn: function (response) {
				if ('yes' !== response) {
					return;
				}
				Ext.Ajax.request({
				url: BASE_URL + 'akunpelayanan_controller/delete_data',
				params: {				
					tahun	: record.data['tahun'],
					kdpelayanan	: record.data['kdpelayanan']
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
					Ext.getCmp('gp.akunpel').store.reload();
					//fTotal();
				},
				failure: function() {
					//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
				}
			});
			
			}            
		});	
	}
	
	
	
	function update_setting(){ 
	
		var ds_klppelayanan = dm_klppelayanan();
		
		var ds_akun = dm_akun();
	
		var form_updateset = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.updateset',
			labelWidth: 120, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 200, width: 500,
			layout: 'form',
			frame: true,	
			items: [{
				xtype: 'fieldset', layout:'form', height: 150,
				frame:true,
				items: [{
					xtype: 'container', fieldLabel: 'Akun Sarana / RS',
					layout: 'hbox',
					items: [{
									xtype: 'textfield', //margins: '0 0 0 4',
									id: 'tf_akunjs',
									width: 100, readOnly: true,
									style : 'opacity:0.6',
									
					},{
							xtype: 'combo', id: 'cb.kdakunjs', //fieldLabel: '',
							store: ds_akun, triggerAction: 'all',
							valueField: 'kdakun', displayField: 'kd_nm_akun',
							forceSelection: true, submitValue: true, 
							margins: '0 0 0 4',
							mode: 'local', emptyText:'Pilih Kelompok Akun..', width: 200,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var kdakun = Ext.getCmp('cb.kdakunjs').getValue();
									if (kdakun){
											Ext.getCmp('tf_akunjs').setValue(kdakun);
									} 
								}
							}
						}]
				},{
					xtype: 'container', fieldLabel: 'Akun Jasa Medis',
					layout: 'hbox',
					items: [{
									xtype: 'textfield', //margins: '0 0 0 4',
									id: 'tf_akunjm',
									width: 100, readOnly: true,
									style : 'opacity:0.6',
									
					},{
							xtype: 'combo', id: 'cb.kdakunjm', //fieldLabel: '',
							store: ds_akun, triggerAction: 'all',
							valueField: 'kdakun', displayField: 'kd_nm_akun',
							forceSelection: true, submitValue: true, 
							margins: '0 0 0 4',
							mode: 'local', emptyText:'Pilih Kelompok Akun..', width: 200,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var kdakun = Ext.getCmp('cb.kdakunjm').getValue();
									if (kdakun){
											Ext.getCmp('tf_akunjm').setValue(kdakun);
									} 
								}
							}
						}]
				},{
					xtype: 'container', fieldLabel: 'Akun Jasa Profesi',
					layout: 'hbox',
					items: [{
									xtype: 'textfield', //margins: '0 0 0 4',
									id: 'tf_akunjp',
									width: 100, readOnly: true,
									style : 'opacity:0.6',
									
					},{
							xtype: 'combo', id: 'cb.kdakunjp', //fieldLabel: '',
							store: ds_akun, triggerAction: 'all',
							valueField: 'kdakun', displayField: 'kd_nm_akun',
							forceSelection: true, submitValue: true, 
							margins: '0 0 0 4',
							mode: 'local', emptyText:'Pilih Kelompok Akun..', width: 200,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var kdakun = Ext.getCmp('cb.kdakunjp').getValue();
									if (kdakun){
											Ext.getCmp('tf_akunjp').setValue(kdakun);
									} 
								}
							}
						}]
				},{
					xtype: 'container', fieldLabel: 'Akun BHP',
					layout: 'hbox',
					items: [{
									xtype: 'textfield', //margins: '0 0 0 4',
									id: 'tf_akunbhp',
									width: 100, readOnly: true,
									style : 'opacity:0.6',
									
					},{
							xtype: 'combo', id: 'cb.kdakunbhp', //fieldLabel: '',
							store: ds_akun, triggerAction: 'all',
							valueField: 'kdakun', displayField: 'kd_nm_akun',
							forceSelection: true, submitValue: true, 
							margins: '0 0 0 4',
							mode: 'local', emptyText:'Pilih Kelompok Akun..', width: 200,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var kdakun = Ext.getCmp('cb.kdakunbhp').getValue();
									if (kdakun){
											Ext.getCmp('tf_akunbhp').setValue(kdakun);
									} 
								}
							}
						}]
				}]
			},{
				xtype: 'button',
				text: 'Simpan',
				id: 'bt.cpy',
				iconCls:'silk-save',
				style: 'margin: -5px 0 0 10px',
				width: 72,
				handler: function(){
					updateAkun();
				}					
			},{
				xtype: 'button',
				text: 'Kembali',
				id: 'bt.kemb',
				iconCls:'silk-arrow-undo',
				style: 'marginLeft: 90px; marginTop: -22px; marginBottom: 15px',
				width: 72,
				handler: function(){
					win_updateset.close();
				}						
			}] 
		});
		
		function get_par() {
			var par='';
			var rowSeparator = '||';
			var selectedData = grid_nya.getSelectionModel().getSelections();
			var numSelectedData = selectedData.length;
			
			for(var i = 0; i < numSelectedData; i++){
				var rec = selectedData[i];
				var colTahun = rec.get("tahun");
				var colKdpelayanan = rec.get("kdpelayanan");
				
				if ((numSelectedData-1) == i ) {
					rowSeparator = '';
				}

				par += colTahun + ';' + colKdpelayanan + rowSeparator;
					
			}	  

			return par;
		}
	
		function updateAkun() {
			
		  var par = get_par();
			
			Ext.Ajax.request({
				url: BASE_URL + 'akunpelayanan_controller/update_akunpelayanan',
				method: 'POST',
				params: { 			
					postdata: par,
					kdakunjs : Ext.getCmp("cb.kdakunjs").getValue(),
					kdakunjm : Ext.getCmp("cb.kdakunjm").getValue(),
					kdakunjp : Ext.getCmp("cb.kdakunjp").getValue(),
					kdakunbhp : Ext.getCmp("cb.kdakunbhp").getValue(),
				},
				success: function() {
					Ext.Msg.alert("Info:", msgSaveSuccess);
					grid_nya.getStore().reload();
					win_updateset.close();
				},
				failure: function() {
					Ext.MessageBox.alert("Informasi", "SImpan Data Gagal");
				}
			});
				
	  }
	
		var win_updateset = new Ext.Window({
			title: 'Setting Akun Pelayanan',
			modal: true, closable: false,
			items: [form_updateset]
		}).show();
	
	}
		
	function fncopydata(){		
		var form_copy = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.copy',
			labelWidth: 120, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 235, width: 350,
			layout: 'form',
			frame: true,	
			items: [{
				xtype: 'fieldset', layout:'form', height: 80,
				frame:true,
				items: [{
					xtype: 'combo', id: 'cb.tahunlama', fieldLabel: 'Tahun Asal',
					store: ds_tahunakun, triggerAction: 'all',
					valueField: 'tahun', displayField: 'tahun',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih Tahun..', width: 170,
					editable: false, //readOnly: true, style: 'opacitty: 0.6',
				},{
					xtype: 'combo', id: 'cb.tahunbaru', fieldLabel: 'Tahun Tujuan',
					store: ds_tahunakun, triggerAction: 'all',
					valueField: 'tahun', displayField: 'tahun',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih Tahun..', width: 170,
					editable: false,
					allowBlank: false,
				}]
			},{
				xtype: 'button',
				text: 'Simpan',
				id: 'bt.cpy',
				iconCls:'silk-save',
				style: 'margin: -5px 0 0 10px',
				width: 72,
				handler: function(){
					copydata();
				}						
			},{
				xtype: 'button',
				text: 'Kembali',
				id: 'bt.kemb',
				iconCls:'silk-arrow-undo',
				style: 'marginLeft: 90px; marginTop: -22px; marginBottom: 15px',
				width: 72,
				handler: function(){
					win_salin_dta.close();
				}						
			},{
				xtype: 'fieldset', title: 'Informasi :',
				items: [{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -13px 20px 0 -5px; font-size: 13px', width: 320,
					items: [{
						xtype: 'label',
						text: '1. Tahun asal akan menyailin dan menimpa',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -22px 20px 0 10px; font-size: 13px', width: 350,
					items: [{
						xtype: 'label',
						text: ' (repleace) tahun tujuan',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -18px 20px 0 -5px; font-size: 13px', width: 350,
					items: [{
						xtype: 'label',
						text: '2. Tahun asal tidak boleh sama dengan tahun tujuan',
					}]
				}]
			}] 
		});
				
		function copydata(){
			var cekvaltahunlama = Ext.getCmp("cb.tahunlama").getValue();
			var cekvaltahunbaru = Ext.getCmp("cb.tahunbaru").getValue();
			if (cekvaltahunlama != cekvaltahunbaru){
				Ext.Msg.show({
					title: 'Konfirmasi',
					msg: 'Salin Data..?',
					buttons: Ext.Msg.YESNO,
					icon: Ext.MessageBox.QUESTION,
					fn: function (response) {
						if ('yes' !== response) {
							return;
						}
						Ext.Ajax.request({
						url: BASE_URL + 'akunpelayanan_controller/copy_data',
						params: {
							tahun_lama	: Ext.getCmp('cb.tahunlama').getValue(),
							tahun_baru	: Ext.getCmp('cb.tahunbaru').getValue()
						},
						success: function(response){
							//Ext.MessageBox.alert('Informasi', 'Copy Berhasil..');
							Ext.getCmp('gp.akunpel').store.reload();
							win_salin_dta.close();
						},
						failure: function() {
							//Ext.Msg.alert("Informasi", "Copy Data Gagal");
						}
					});
					
					}            
				});	
			}else{
				Ext.Msg.alert("Informasi", "Tahun tidak boleh sama..");
			}
		}
	
		var win_salin_dta = new Ext.Window({
			title: 'Salin Setting Akun Pelayanan',
			modal: true, closable: false,
			items: [form_copy]
		}).show();
	}
}