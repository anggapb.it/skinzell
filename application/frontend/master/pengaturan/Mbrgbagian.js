function Mbrgbagian(){
Ext.form.Field.prototype.msgTarget = 'side';

	var pageSize = 15;
	var ds_brgbagian1 = dm_brgbagian1();
		//ds_brgbagian1.setBaseParam('idbagian','null');
	var ds_bagian = dm_bagian();	
	
	var arr_cari = [['kdbrg', 'Kode Barang'],['nmbrg', 'Nama Barang'],['nmjnsbrg', 'Jenis Barang'],['nmsatuanbsr', 'Satuan Besar'],['rasio', 'Rasio'],['nmsatuankcl', 'Satuan Kecil'],['stoknowbagian', 'Stok Sekarang'],['stokminbagian', 'Stok Minimal'],['stokmaxbagian', 'Stok Maksimal'],['informasi', 'informasi']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	function fnSearchgrid(){
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
			ds_brgbagian1.setBaseParam('key',  '1');
			ds_brgbagian1.setBaseParam('id',  idcombo);
			ds_brgbagian1.setBaseParam('name',  nmcombo);
		ds_brgbagian1.reload({
			params: { 
				idbagian: Ext.getCmp('cb.bagian').getValue()
			}
		});	
	}
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_brgbagian1,
		displayInfo: true,
		displayMsg: 'Data Barang Bagian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display',
		listeners: {
			beforechange : function(){
				/* if (!Ext.getCmp('cb.bagian').getValue){
					ds_brgbagian1.setBaseParam('idbagian', Ext.getCmp('cb.bagian').getValue());					
					Ext.getCmp('grid_brgbagian').store.reload();
				} */
				
				ds_brgbagian1.setBaseParam('idbagian', Ext.getCmp('cb.bagian').getValue());					
					Ext.getCmp('grid_brgbagian').store.reload();
			}
		}
	});
	
	var vw_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
		
	var grid_nya = new Ext.grid.EditorGridPanel({
		id: 'grid_brgbagian',
		store: ds_brgbagian1,
		view: vw_nya,	
		frame: true,		
		autoScroll: true,
		height: 465,
		columnLines: true,
		clicksToEdit: 1,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			disabled: true,
			handler: function() {
				fnwBarang();
				Ext.getCmp('gp.brg').store.reload();
			}
		},
		{
			xtype: 'textfield',
			name: 'txttampunghuruf',
			id: 'id.txttampunghuruf',
			hidden: true,
		},{
			xtype: 'textfield',
			id: 'tf.kdbrg',
			hidden: true,
		},'-',
		{
			xtype: 'compositefield',
			style: 'margins: 0 0 0 7px',
			width: 500,
			items: [{
				xtype: 'label', text: 'Search :', margins: '5 8 0 5',
			},
			{
				xtype: 'combo', 
				id: 'cb.search', width: 125, 
				store: ds_cari, 
				valueField: 'id', displayField: 'nama',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local',
				emptyText:'Pilih...', disabled: true, margins: '2 0 0 0',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.search').getValue();
							if(cbsearchh != ''){
								Ext.getCmp('cek').enable();
								Ext.getCmp('cek').focus();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				style: 'marginLeft: 7px',
				id: 'cek',
				width: 227,
				disabled: true,
				margins: '2 0 0 0',
				validator: function(){
					fnSearchgrid();
				}
			}]
		},{
			xtype: 'textfield',
			id: 'tf.idbagian', 
			width: '50',
			hidden: true,
			validator: function(){
				ds_brgbagian1.reload({
					params: { 
						idbagian: Ext.getCmp('cb.bagian').getValue()
					}
				});				
			}
		}
		],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Nama Bagian',
			width: 150,
			dataIndex: 'nmbagian',
			sortable: true,
			hidden: true
		},{
			header: 'Kode Barang',
			width: 80,
			dataIndex: 'kdbrg',
			sortable: true
		},
		{
			header: 'Nama Barang',
			width: 240,
			dataIndex: 'nmbrg',
			sortable: true
		},{
			header: 'Jenis Barang',
			width: 100,
			dataIndex: 'nmjnsbrg',
			sortable: true
		},{
			header: 'Satuan Besar',
			width: 90,
			dataIndex: 'nmsatuanbsr',
			sortable: true
		},
		{
			header: 'Rasio',
			width: 100,
			dataIndex: 'rasio',
			sortable: true, 
			xtype: 'numbercolumn', format:'0,000.00', align:'right'
		},{
			header: 'Satuan Kecil',
			width: 90,
			dataIndex: 'nmsatuankcl',
			sortable: true
		},{
			header: 'Stok Sekarang',
			width: 100,
			dataIndex: 'stoknowbagian',
			sortable: true, 
			xtype: 'numbercolumn', format:'0,000.00', align:'right'
		},
		{
			header: 'Harga Beli',
			width: 100,
			dataIndex: 'hrgbeli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: 'Subtotal',
			width: 100,
			dataIndex: 'subtotalbeli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},
		{
			header: 'Stok Minimal',
			width: 100,
			dataIndex: 'stokminbagian',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			// editor: new Ext.form.TextField({
			// 			id: 'stokmin',
   //                      enableKeyEvents: true,
   //                      listeners: {
   //                          specialkey: function(field, e){
			// 					if (e.getKey() == e.ENTER) {
			// 						 fnEditstkmin();
			// 					}
			// 				}
			// 			}
			// 		})
		},
		{
			header: 'Stok Maksimal',
			width: 100,
			dataIndex: 'stokmaxbagian',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			// editor: new Ext.form.TextField({
			// 			id: 'stokmax',
   //                      enableKeyEvents: true,                        
			// 			listeners:{
			// 				specialkey: function(field, e){
			// 					if (e.getKey() == e.ENTER) {
			// 						 fnEditstkmax();
			// 					}
			// 				}
			// 			}
			// 		})
		},{
			header: 'Informasi',
			width: 150,
			dataIndex: 'informasi',
			sortable: true,
			renderer: function(value, p, r){
				var info = '';
					if(r.data['stoknowbagian'] || r.data['stoknowbagian'] == null ) info = 'Stok Sekarang Belum Ada';
					if(r.data['stoknowbagian'] || r.data['stoknowbagian'] != null ) info = r.data['informasi'];
				return info ;
			}
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteBrgbagian(grid, rowIndex);
                    }
                }]
        }],
		//bbar: paging,
		bbar: [
			{
				xtype: 'compositefield',
				style: 'padding: 5px; margin: 3px 0 0 800px',
				width:400,
				items:[
					{
						xtype: 'label', text: 'Total : ', id: 'lb.p', margins: '3 5 0 0',
					},{
						xtype: 'numericfield',
						id:'tf.tothrgbeli',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:100,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},
				]
			},
		],
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {
				var rec = ds_brgbagian1.getAt(rowIdx);
				Ext.getCmp("id.txttampunghuruf").setValue(rec.data["idbagian"]);
				Ext.getCmp("tf.kdbrg").setValue(rec.data["kdbrg"]);
			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Barang Bagian', iconCls:'silk-calendar',
		layout: 'fit',
		frame: true,
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .99,
						border: false,
						items: [{
							xtype: 'compositefield',
							style: 'padding: 5px; marginLeft: 10px; marginBottom: -6px',
							width:395,
							items: [{
								xtype: 'label', id: 'lb.bgn', text: 'Bagian :', margins: '3 10 0 5',
							},{
								xtype: 'textfield',
								id: 'tf.bagian',
								emptyText:'Nama Bagian..',
								width: 300,
								readOnly: true,
								allowBlank: false
							},
							{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn_data_pen_bgn',
								width: 3,
								handler: function() {
									fnwBagian();
									Ext.getCmp('cb.search').disable();
									ds_brgbagian1.reload();
								}
							}]
						},{
							xtype: 'combo',
							id: 'cb.bagian',
							store: ds_bagian,
							triggerAction: 'all',
							valueField: 'idbagian',
							displayField: 'nmbagian',
							forceSelection: true,
							submitValue: true, 
							mode: 'local',
							emptyText:'Nama Bagian..',
							width: 300,
							editable: false,
							//readOnly: true,
							//allowBlank: false,
							hidden: true,
							typeAhead: true,
							selectOnFocus: true,
							listeners: {
								select: function(){
									//ds_brgbagian1.setBaseParam('idbagian', Ext.getCmp('cb.bagian').getValue());					
									//Ext.getCmp('grid_brgbagian').store.reload();
									ds_brgbagian1.reload({
										params: { 
											idbagian: Ext.getCmp('cb.bagian').getValue()
										}
									});									
								}
							}
						}]
					}]
				},{
					xtype : 'fieldset',
					title: 'Daftar Barang Bagian',
					items: [grid_nya]
				}]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadBrgbagian(){
		ds_brgbagian1.reload();
	}
			
	function fnDeleteBrgbagian(grid, record){
		var record = ds_brgbagian1.getAt(record);
		var url = BASE_URL + 'brgbagian_controller/delete_brgbagian';
		var params = new Object({
						idbagian	: record.data['idbagian'],
						kdbrg		: record.data['kdbrg']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
	function fnEditstkmin(){
        var stokmin = Ext.getCmp('stokmin').getValue();
        var letters = /^[a-zA-Z]+$/;
		var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
        if(stokmin.match(letters)){
			alert('Masukan Angka');
            ds_brgbagian1.reload();
        } 
		else if(stokmin.match(simbol)){
            alert('Masukan Angka');
            ds_brgbagian1.reload();
        } 
        else {           
            fnstkmin(stokmin);   
        }      
    }
	
	function fnstkmin(stokmin){
		Ext.Ajax.request({
			url: BASE_URL + 'brgbagian_controller/update_stokmin',
			params: {
				idbagian		: Ext.getCmp('cb.bagian').getValue(),
				kdbrg			: Ext.getCmp('tf.kdbrg').getValue(),
                stokminbagian  	: stokmin
			},
			success: function() {
				//Ext.Msg.alert("Info", "Ubah Berhasil");
				Ext.getCmp('grid_brgbagian').store.reload();
			},
			failure: function() {
				Ext.Msg.alert("Info", "Ubah Data Gagal");
			}
		});
	}
	
	function fnEditstkmax(){
        var stokmax = Ext.getCmp('stokmax').getValue();
        var letters = /^[a-zA-Z]+$/;
		var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
        if(stokmax.match(letters)){
			alert('Masukan Angka');
            ds_brgbagian1.reload();
        } 
		else if(stokmax.match(simbol)){
            alert('Masukan Angka');
            ds_brgbagian1.reload();
        } 
        else {           
            fnstkmax(stokmax);   
        }      
    }
	
	function fnstkmax(stokmax){
		Ext.Ajax.request({
			url: BASE_URL + 'brgbagian_controller/update_stokmax',
			params: {
				idbagian		: Ext.getCmp('cb.bagian').getValue(),
				kdbrg			: Ext.getCmp('tf.kdbrg').getValue(),
                stokmaxbagian  	: stokmax
			},
			success: function() {
				//Ext.Msg.alert("Info", "Ubah Berhasil");
				Ext.getCmp('grid_brgbagian').store.reload();
			},
			failure: function() {
				Ext.Msg.alert("Info", "Ubah Data Gagal");
			}
		});
	}

	/**
	WIN - FORM ENTRY/EDIT 
	*/
		
	function fnwBarang(){		
		var ds_brgmedisdibrgbagian = dm_brgmedisdibrgbagian();
		ds_brgmedisdibrgbagian.setBaseParam('idbagian', Ext.getCmp('cb.bagian').getValue());
		ds_brgmedisdibrgbagian.reload();
					
		var sm_cbGridBarang = new Ext.grid.CheckboxSelectionModel({
			listeners: { 
				rowselect : function( selectionModel, rowIndex, record){
					Ext.Ajax.request({
						url: BASE_URL + 'brgbagian_controller/insert_win_barang',
						params: {
							idbagian	: Ext.getCmp('cb.bagian').getValue(),
							kdbrg		: record.get("kdbrg"),
						},
						success: function(){
							ds_brgmedisdibrgbagian.removeAt(rowIndex);
							Ext.getCmp('grid_brgbagian').store.reload();
						},
						failure: function() {
							//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
						}
					});
				},
				rowdeselect : function( selectionModel, rowIndex, record){
					/* Ext.Ajax.request({
						url: BASE_URL + 'tppelayanan_controller/delete_win_Barang',
						params: {
							idtarifpaket	: Ext.getCmp('tf.idtarifpaket').getValue(),
							kdbrg		 	: record.get("kdbrg"),
						},
						success: function(){
							fTotal();
						},
						failure: function() {
							//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
						}
					}); */
				},
				beforerowselect : function (sm, rowIndex, keep, rec) {
					if (this.deselectingFlag && this.grid.enableDragDrop){
						this.deselectingFlag = false;
						this.deselectRow(rowIndex);
						return this.deselectingFlag;
					}
					return keep;
				}
			}
		});
		
		/* function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		} */
		
		var cm_barang = new Ext.grid.ColumnModel([
			sm_cbGridBarang,
			{
				header: 'Kode Barang',
				dataIndex: 'kdbrg',
				width: 80,
				//renderer: fnkeyAdd
			},{
				header: 'Nama Barang',
				dataIndex: 'nmbrg',
				width: 430
			},
			{
				header: 'Jenis Barang',
				width: 100,
				dataIndex: 'nmjnsbrg',
				sortable: true,
				align:'center',
			},
			{
				header: 'Satuan',
				width: 97,
				dataIndex: 'nmsatuankcl',
				sortable: true,
				align:'center',
			}
		]);
		
		/* var sm_barang = new Ext.grid.RowSelectionModel({
			singleSelect: true
		}); */
		
		var vw_barang = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_barang = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_brgmedisdibrgbagian,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_barang = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_barang = new Ext.grid.GridPanel({
			id: 'gp.brg',
			ds: ds_brgmedisdibrgbagian,
			cm: cm_barang,
			sm: sm_cbGridBarang,
			view: vw_barang,
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cekk',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cekk').getValue();
							ds_brgmedisdibrgbagian.setBaseParam('key',  '1');
							ds_brgmedisdibrgbagian.setBaseParam('id',  'nmbrg');
							ds_brgmedisdibrgbagian.setBaseParam('name',  nmcombo);
						ds_brgmedisdibrgbagian.load();
					}
				}]
			}],
			height: 460,
			width: 740,
			//plugins: cari_barang,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: paging_barang,
			listeners: {
				cellclick: onCellClickaddbarang
			}
		});
		var win_find_cari_barang = new Ext.Window({
			title: 'Cari Barang',
			modal: true, //closable:false,
			items: [grid_find_cari_barang]
		}).show();
		
		function onCellClickaddbarang(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_kdbarang = record.data["kdbrg"];
					//var var_cari_idsatuanbsr = record.data["idsatuanbsr"];
								
					//Ext.getCmp('tf.barang').focus();
					Ext.getCmp("cb.brg").setValue(var_cari_kdbarang);
					//Ext.getCmp("cb.satuan").setValue(var_cari_idsatuanbsr);
								win_find_cari_barang.close();
				return true;
			}
			return true;
		}
	}
	
	function fnwBagian(){
		var ds_brgbagian1 = dm_brgbagian1();
		ds_brgbagian1.setBaseParam('idbagian', 'null');
		ds_brgbagian1.reload();
		var ds_bagian = dm_bagian();
		
		function fnkeyAddBagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bagian = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200,
				renderer: fnkeyAddBagian
			},{
				header: 'Jenis Pelayanan',
				dataIndex: 'nmjnspelayanan',
				width: 150
			},{
				header: 'Bidang Perawatan',
				dataIndex: 'nmbdgrawat',
				width: 220
			}
		]);
		
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagian,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bagian = new Ext.grid.GridPanel({
			ds: ds_bagian,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 620,
			plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbagian = record.data["idbagian"];
					var var_cari_bagian = record.data["nmbagian"];
								
					//Ext.getCmp('tf.bagian').focus();
					Ext.getCmp("tf.bagian").setValue(var_cari_bagian);
					Ext.getCmp("cb.bagian").setValue(var_cari_idbagian);
					Ext.getCmp('tf.idbagian').setValue(var_cari_idbagian);
					Ext.getCmp('cb.search').enable();
					Ext.getCmp('cek').setValue();
					Ext.getCmp('btn_add').enable();
					Ext.getCmp('grid_brgbagian').store.reload({
						callback: function(results){
							if(results == 0) return;
							var sum = 0;
							Ext.getCmp('grid_brgbagian').store.each(function (rec) {
								sum += parseFloat(rec.get('subtotalbeli'));
								RH.setCompValue('tf.tothrgbeli', sum); 
							}); 
						}
					});
								win_find_cari_bagian.close();
				return true;
			}
			return true;
		}
	}
}