function Mtempelayanan(){
Ext.form.Field.prototype.msgTarget = 'side';
	var ds_tempelayanan = dm_tempelayanan();
	var ds_mtempelayanan = dm_mtempelayanan();
		
	var cbGrid = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			beforerowselect : function (sm, rowIndex, keep, rec) {
				if (this.deselectingFlag && this.grid.enableDragDrop){
					this.deselectingFlag = false;
					this.deselectRow(rowIndex);
					return this.deselectingFlag;
				}
				return keep;
			}
		}
	});
	
	var vw_tempelayanan = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	var grid_tempelayanan = new Ext.grid.EditorGridPanel({
		id: 'gp.grid_tempelayanan',
		store: ds_tempelayanan,
		view: vw_tempelayanan,
		sm: cbGrid,
		border: false,
		height: 525,
		columnLines: true,
		autoScroll: true,
		//autoHeight: true,
		//plugins: cari_data,
		//sm: sm_nya,
		clicksToEdit: 1,
		tbar: [{
			text: 'Tambah Pelayanan',
			id: 'btn_addb',
			iconCls: 'silk-add',
			handler: function(){
				var pen_nmpaket = RH.getCompValue('tf.frm.pen_nmpaket', true);
				if(pen_nmpaket != ''){
					var grid = grid_tempelayanan;
					fnwinPelayanan(grid);
					Ext.getCmp('cb.frm.mtempelayanan').disable();
					RH.setCompValue('cb.frm.mtempelayanan', pen_nmpaket);
				}else if(pen_nmpaket == ''){
					Ext.MessageBox.alert('Message', 'Nama Template Di isi...!');
				}
				return;
			}
		},'-',
		{
			text: 'Hapus',
			id: 'btn_hapus',
			icon : 'application/framework/img/rh_delete.gif',
			handler: function() {
				var m = grid_tempelayanan.getSelectionModel().getSelections();
					if(m.length > 0)
					{				
						Ext.MessageBox.confirm('Message', 'Hapus Data Yang Di Pilih..?' , del);		 
					}
					else
					{
						Ext.MessageBox.alert('Message', 'Data Belum Di Pilih...!');
					}		
			}
		},{
			xtype: 'textfield',
			id: 'tf.idtemplayanan',
			width: 70,
			emptyText: 'idtempelayanan',
			hidden: true,
			validator: function() {
				ds_tempelayanan.setBaseParam('idtemplayanan', Ext.getCmp('tf.idtemplayanan').getValue());
				Ext.getCmp('gp.grid_tempelayanan').store.reload();
				
			}
		},{
			xtype: 'textfield',
			id: 'tf.frm.iditempelayanantemp',
			emptyText: 'iditempelayanantemp',
			hidden: true,
		}],
		columns: [new Ext.grid.RowNumberer(),
		cbGrid,
		{
			header: '<center>Nama Pelayanan</center>',
			width: 500,
			dataIndex: 'nmpelayanan',
			sortable: true
		}],
		listeners:{
			rowclick: Addrecord
		}
	});
	
	function Addrecord(grid, rowIndex, columnIndex){
		var record = grid.getStore().getAt(rowIndex);
		Ext.getCmp('tf.frm.iditempelayanantemp').setValue(record.data['iditempelayanantemp']);
	}
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Template Pelayanan', iconCls:'silk-calendar',
		layout: 'fit',		
		autoScroll: true,
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				tbar: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .99,
						border: false,
						items: [{
							xtype: 'compositefield',
							style: 'padding: 5px; marginLeft: 15px',
							name: 'comp_pen_nmpaket',
							id: 'comp_pen_nmpaket',
							width:700,
							items: [{
								xtype: 'label', id: 'lb.nmpenyakit', text: 'Nama Template Pelayanan :', margins: '3 10 0 5',
							},{
								xtype: 'textfield',
								id: 'tf.frm.pen_nmpaket',
								width: 370,
								emptyText:'Pilih...',
								readOnly: true,
								
							},
							{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn_data_pen_nmpaket',
								width: 3,
								handler: function() {
									parentMasterpelayanan();
								}
							}]
						}]
					}]
				}],
				items: [grid_tempelayanan]
			}]
		}],
		listeners: {
			afterrender: mulai
		}
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	function mulai(){
		Ext.getCmp('gp.grid_tempelayanan').store.reload();
	}
	
	function reloadTempelayanan(){
		ds_tempelayanan.reload();
	}
	
	function fnAddPenyakit(){
		var grid = grid_tempelayanan;
		wEntryTempelayanan(false, grid, null);	
	}
	
	function fnEditTempelayanan(grid, record){
		var record = ds_tempelayanan.getAt(record);
		wEntryTempelayanan(true, grid, record);		
	}	
	
	function del(btn){
		console.log(btn);
		if(btn == 'yes')
		{			
			var m = grid_tempelayanan.getSelectionModel().getSelections();
			for(var i=0; i< m.length; i++){
				var rec = m[i];
				console.log(rec);
				if(rec){
					console.log(rec.get("iditempelayanantemp"));
					var iditempelayanantemp = rec.data['iditempelayanantemp'];
					Ext.Ajax.request({
						url: BASE_URL +'tempelayanan_controller/delete_tempelayanan',
						method: 'POST',
						params: {
							iditempelayanantemp 	: iditempelayanantemp
						},
						success: function(){
							
							Ext.getCmp('gp.grid_tempelayanan').store.reload();
						}
					});
				}
					Ext.MessageBox.alert('Message', 'Hapus Data Berhasil..');
			}
		}
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function parentMasterpelayanan(){		
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var ds_mastertempelayanan_parent = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'tempelayanan_controller/get_parent_mastertempelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtemplayanan',
				mapping: 'idtemplayanan'
			},{
				name: 'nmtemplayanan',
				mapping: 'nmtemplayanan'
			}]
		});
				
		var cm_mastertempelayanan_parent = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idtemplayanan',
				width: 30
			},{
				header: 'Nama Template Pelayanan',
				dataIndex: 'nmtemplayanan',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		var sm_mastertempelayanan_parent = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_mastertempelayanan_parent = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_mastertempelayanan_parent = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_mastertempelayanan_parent,
			displayInfo: true,
			displayMsg: 'Data Master Paket Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_mastertempelayanan_parent = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200,
			disableIndexes:['idtemplayanan'],
		})];
		var grid_find_mastertempelayanan_parent = new Ext.grid.GridPanel({
			ds: ds_mastertempelayanan_parent,
			cm: cm_mastertempelayanan_parent,
			sm: sm_mastertempelayanan_parent,
			view: vw_mastertempelayanan_parent,
			height: 460,
			width: 422,
			plugins: cari_mastertempelayanan_parent,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			autoScroll: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_mastertempelayanan_parent,
			listeners: {
				cellclick: onCellClickaddparent
			}
		});
		var win_find_mastertempelayanan_parent = new Ext.Window({
			title: 'Master Template Pelayanan',
			modal: true,
			items: [grid_find_mastertempelayanan_parent]
		}).show();
		
		function onCellClickaddparent(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_idtemplayanan_parent = record.data["idtemplayanan"];
					var var_nmtemplayanan_parent = record.data["nmtemplayanan"];
								
					Ext.getCmp('tf.frm.pen_nmpaket').focus()
					Ext.getCmp("tf.idtemplayanan").setValue(var_idtemplayanan_parent);
					Ext.getCmp("tf.frm.pen_nmpaket").setValue(var_nmtemplayanan_parent);
								win_find_mastertempelayanan_parent.close();
				return true;
			}
			return true;
		}
	}
	
	function fnwinPelayanan(grid){
		var ds_kdpelayanan = dm_kdpelayanan();
		/* var sm_Pelayanan = new Ext.grid.RowSelectionModel({
			singleSelect: true
		}); */
		
		var sm_cbGridPelayanan = new Ext.grid.CheckboxSelectionModel({
			listeners: {
				rowselect : function( selectionModel, rowIndex, record){
					Ext.Ajax.request({
						url: BASE_URL + 'tempelayanan_controller/cekkdpelayanan',
						method: 'POST',
						params: {
							idtemplayanan	: Ext.getCmp('tf.idtemplayanan').getValue(),
							kdpelayanan		: record.get("kdpelayanan"),
						},
						success: function(response){
							pelayanan = response.responseText;
							if (pelayanan =='1') {
								Ext.MessageBox.alert('Message', 'Data Sudah Ada Yang Sama...');
								Ext.getCmp('gp.grid_pelayanan').store.reload();
								
							} else {
								Ext.Ajax.request({
									url: BASE_URL + 'tempelayanan_controller/insert_win_pelayanan',
									params: {
										idtemplayanan		: Ext.getCmp('tf.idtemplayanan').getValue(),
										kdpelayanan			: record.get("kdpelayanan"),
									},
									success: function(){
										ds_tempelayanan.reload();
									},
									failure: function() {
										//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
									}
								});
							}
						}
					});
				},
				rowdeselect : function( selectionModel, rowIndex, record){
					Ext.Ajax.request({
						url: BASE_URL + 'tempelayanan_controller/delete_win_pelayanan',
						params: {
							idtemplayanan	: Ext.getCmp('tf.idtemplayanan').getValue(),
							kdpelayanan		: record.get("kdpelayanan"),
						},
						success: function(){
							ds_tempelayanan.reload();							
						},
						failure: function() {
							//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
						}
					});
				},
				beforerowselect : function (sm, rowIndex, keep, rec) {
					if (this.deselectingFlag && this.grid.enableDragDrop){
						this.deselectingFlag = false;
						this.deselectRow(rowIndex);
						return this.deselectingFlag;
					}
					return keep;
				}
			}
		});
		
		var cm_pelayanan = new Ext.grid.ColumnModel([
			sm_cbGridPelayanan,
			{
				//header: 'Id',
				dataIndex: 'kdpelayanan',
				width: 100,
				hidden: true
			},{
				header: 'Nama Pelayanan',
				dataIndex: 'nmpelayanan',
				width: 470
			}
		]);
		
		var paging_pelayanan = new Ext.PagingToolbar({
			pageSize: 15,
			store: ds_kdpelayanan,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_pelayanan = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var vw_pelayanan = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_pelayanan = new Ext.grid.GridPanel({
			id: 'gp.grid_pelayanan',
			ds: ds_kdpelayanan,
			cm: cm_pelayanan,
			sm: sm_cbGridPelayanan, //sm_Pelayanan,
			tbar: [],
			plugins: cari_pelayanan,
			view: vw_pelayanan,
			height: 395,
			width: 515,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			autoScroll: true,
			loadMask: true,
			layout: 'anchor',
			style: 'marginTop: 10px',
			anchorSize: {
				width: 500,
				height: 400
			},
			bbar: paging_pelayanan,
			listeners: {	
				//rowclick: add_pelayanan
			}
		});
		
		var form_input_pelayanan = new Ext.FormPanel({
			xtype:'form',
			id: 'frm.pelayanan',
			buttonAlign: 'left',
			labelWidth: 165, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 470, width: 527,
			layout: 'form',
			frame: false, 
			defaultType:'textfield',
			items: [{
				xtype: 'fieldset',
				columnWidth: .90,
				border: false,
				items: [{
					xtype: 'combo', id: 'cb.frm.mtempelayanan', 
					fieldLabel: 'Nama Template Pelayanan',
					store: ds_mtempelayanan, triggerAction: 'all',
					valueField: 'idtempelayanan', displayField: 'nmtempelayanan',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', width: 250,
					editable: false,
				}]
			},
				grid_pelayanan
			]
		});
		
		var win_pelayanan = new Ext.Window({
			title: 'Pelayanan (Entry)',
			modal: true,
			//closable: false,
			items: [form_input_pelayanan]
		}).show();
		
		function fnSavepelayanan(){
			var idForm = 'frm.pelayanan';
			var sUrl = BASE_URL +'tppelayanan_controller/insert_win_pelayanan';
			var sParams = new Object({
				//idtarifpaketdet	:	RH.getCompValue('tf.frm.idtarifpaketdet'),
				kdpelayanan		:	RH.getCompValue('tf.kdpelayanan'),
				idjnstarif		:	RH.getCompValue('tf.jnstarif'),
				idtarifpaket	:	RH.getCompValue('tf.idtarifpaket')
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			//call form grid submit function (common function by RH)
			submitGridForm(idForm, sUrl, sParams, grid, win_pelayanan, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		} 
	}
	
	function getForm(idform){
		var form;
		if(Ext.getCmp(idform)){
			var comp = Ext.getCmp(idform);
			if(comp.getForm()){
				return comp.getForm();
			}
		}
	}

	function submitGridForm (idForm, sUrl, sParams, grid, win, msgWait, msgSuccess, msgFail, msgInvalid){
		var form = getForm(idForm);
		if(form.isValid()){
			form.submit({
				url: sUrl,
				method: 'POST',
				params: sParams, 		
				waitMsg: msgWait,				
				success: function(){
					Ext.Msg.alert("Info:", msgSuccess);	
					grid.getStore().reload();
					win.close();
				},
				failure: function(){
					Ext.Msg.alert("Info:", msgFail);
				}
			});
		} else {
			Ext.Msg.alert("Info:", msgInvalid);
		}	
	}
	
}
