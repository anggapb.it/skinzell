function Mmtempelayanan(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_mtempelayanan = dm_mtempelayanan();
	var ds_jhirarki = dm_jhirarki();
	var ds_status = dm_status();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_mtempelayanan,
		displayInfo: true,
		displayMsg: 'Data Master Template Pelayanan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_mtempelayanan',
		store: ds_mtempelayanan,		
		autoScroll: true,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddMtempelayanan();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Nama Pelayanan',
			width: 300,
			dataIndex: 'nmtemplayanan',
			sortable: true
		},
		{
			header: 'Jenis Hirarki',
			width: 74,
			dataIndex: 'nmjnshirarki',
			sortable: true
		},{
			header: 'Parent',
			dataIndex: 'nmparent',
			width: 200,
			sortable: true,
			renderer: function(value, p, r){
				var parent = '';
					if(r.data['nmparent'] == 0) parent = '';
					if(r.data['nmparent'] != 0) parent = r.data['nmparent'];
				return parent ;
			}
		},
		{
			header: 'Status',
			width: 100,
			dataIndex: 'nmstatus',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditMtempelayanan(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteMtempelayanan(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Master Template Pelayanan', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadMtempelayanan(){
		ds_mtempelayanan.reload();
	}
	
	function fnAddMtempelayanan(){
		var grid = grid_nya;
		wEntryMtempelayanan(false, grid, null);	
	}
	
	function fnEditMtempelayanan(grid, record){
		var record = ds_mtempelayanan.getAt(record);
		wEntryMtempelayanan(true, grid, record);		
	}
	
	function fnDeleteMtempelayanan(grid, record){
		var record = ds_mtempelayanan.getAt(record);
		var url = BASE_URL + 'mtempelayanan_controller/delete_mtempelayanan';
		var params = new Object({
						idtemplayanan	: record.data['idtemplayanan']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryMtempelayanan(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Master Template Pelayanan (Edit)':'Master Template Pelayanan (Entry)';
		var mtempelayanan_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.mtempelayanan',
			buttonAlign: 'left',
			labelWidth: 160, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 200, width: 500,
			layout: 'form', 
			frame: false, 
			defaultType:'textfield',		
			items: [ 
			{
				id: 'tf.frm.idtemplayanan', 
				hidden: true,
			},
			{
				fieldLabel: 'Nama Template Pelayanan',
				id:'tf.frm.nmtemplayanan',
				width: 300,
				//allowBlank: false
			},
			{
				xtype: 'combo', id: 'cb.frm.jhirarki', 
				fieldLabel: 'Jenis Hirarki',
				store: ds_jhirarki, triggerAction: 'all',
				valueField: 'idjnshirarki', displayField: 'nmjnshirarki',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},{
				xtype: 'combo', id: 'cb.frm.ststatus', 
				fieldLabel: 'Status',
				store: ds_status, triggerAction: 'all',
				valueField: 'idstatus', displayField: 'nmstatus',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},
			/* {
				id:'tf.frm.tem_idtemplayanan',
				width: 100,
				sortable: true,
				hidden: true
			} */
			{
				xtype: 'compositefield',
				fieldLabel: 'Parent',
				items: [{
					xtype: 'textfield',
					id: 'tf.frm.tem_idtemplayanan',				
					fieldLabel: 'Parent',
					width: 270, emptyText:'Pilih...'
				},
				{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn_data_tem_idtemplayanan',
					width: 3,
					handler: function() {
						parentMtempelayanan();
					}
				}]
			}],
			buttons: [{
				text: 'Simpan', iconCls:'silk-save',
				handler: function() {
					fnSaveMtempelayanan();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wMtempelayanan.close();
				}
			}]
		});
			
		var wMtempelayanan = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [mtempelayanan_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setMtempelayananForm(isUpdate, record);
		wMtempelayanan.show();

	/**
	FORM FUNCTIONS
	*/	
		function setMtempelayananForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'mtempelayanan_controller/getNmtempelayanan',
						params:{
							tem_idtemplayanan : record.get('tem_idtemplayanan')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.frm.tem_idtemplayanan', r);
						}
					});
					
					RH.setCompValue('tf.frm.idtemplayanan', record.get('idtemplayanan'));
					RH.setCompValue('tf.frm.nmtemplayanan', record.get('nmtemplayanan'));
					RH.setCompValue('cb.frm.jhirarki', record.data['idjnshirarki']);
					RH.setCompValue('cb.frm.ststatus', record.data['idstatus']);
					RH.setCompValue('tf.frm.tem_idtemplayanan', record.get('tem_idtemplayanan'));
					return;
				}
			}
		}
		
		function fnSaveMtempelayanan(){
			var idForm = 'frm.mtempelayanan';
			var sUrl = BASE_URL +'mtempelayanan_controller/insert_mtempelayanan';
			var sParams = new Object({
				idtemplayanan		:	RH.getCompValue('tf.frm.idtemplayanan'),
				nmtemplayanan		:	RH.getCompValue('tf.frm.nmtemplayanan'),
				idjnshirarki		:	RH.getCompValue('cb.frm.jhirarki'),
				idstatus			:	RH.getCompValue('cb.frm.ststatus'),
				tem_idtemplayanan	:	RH.getCompValue('tf.frm.tem_idtemplayanan'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'mtempelayanan_controller/update_mtempelayanan';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wMtempelayanan, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
					
	}

	function parentMtempelayanan(){
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var ds_mtempelayanan_parent = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
								url: BASE_URL + 'mtempelayanan_controller/get_parent_tempelayanan',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
			fields: [{
				name: 'idtemplayanan',
				mapping: 'idtemplayanan'
			},{
				name: 'nmtemplayanan',
				mapping: 'nmtemplayanan'
			}]
		});
		var cm_Mtempelayanan_parent = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idtemplayanan',
				width: 30
			},{
				header: 'Nama Obat',
				dataIndex: 'nmtemplayanan',
				width: 370,
				renderer: fnkeyAdd
			}
		]);
		var sm_Mtempelayanan_parent = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_Mtempelayanan_parent = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_Mtempelayanan_parent = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_mtempelayanan_parent,
			displayInfo: true,
			displayMsg: 'Data Master Template Pelayanan Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_Mtempelayanan_parent = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200,
			disableIndexes:['idtemplayanan'],
		})];
		var grid_find_Mtempelayanan_parent = new Ext.grid.GridPanel({
			ds: ds_mtempelayanan_parent,
			cm: cm_Mtempelayanan_parent,
			sm: sm_Mtempelayanan_parent,
			view: vw_Mtempelayanan_parent,
			height: 350,
			width: 400,
			plugins: cari_Mtempelayanan_parent,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_Mtempelayanan_parent,
			listeners: {
				cellclick: onCellClickaddparent
			}
		});
		var win_find_Mtempelayanan_parent = new Ext.Window({
			title: 'Sub Pelayanan',
			modal: true,
			items: [grid_find_Mtempelayanan_parent]
		}).show();

		function onCellClickaddparent(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_Mtempelayanan_parent = record.data["nmtemplayanan"];
						
					Ext.getCmp('tf.frm.tem_idtemplayanan').focus()
					Ext.getCmp("tf.frm.tem_idtemplayanan").setValue(var_Mtempelayanan_parent);
								win_find_Mtempelayanan_parent.close();
				return true;
			}
			return true;
		}
	}
}
