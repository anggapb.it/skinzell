function Mbrgmedis(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_brgmedis = dm_brgmedis();
	var ds_klpbarang = dm_klpbarang();
	var ds_jnsbrg = dm_jnsbrg();
	var ds_jsatuan = dm_jsatuan();
	var ds_pabrik = dm_pabrik();
	var ds_status = dm_status();
	var ds_stgenerik = dm_stgenerik();
	var ds_stformularium = dm_stformularium();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_brgmedis,
		displayInfo: true,
		displayMsg: 'Data Barang Medis Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
		//render gambar1
	function renderGbr1(val1) {
		return '<img src="resources/gambar/'+ val1 +'" style="max-height:50%; max-width:50%;">';
	}
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_brgmedis',
		store: ds_brgmedis,		
		autoScroll: true,
		height: 530, //autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddBrgmedis();
				//Ext.getCmp('tf.kdpabrik').getEl().hide();
				//Ext.getCmp('tf.kdpabrik').getEl().show();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 72,
			dataIndex: 'kdbrg',
			sortable: true
		},
		{
			header: 'Nama Barang',
			width: 152,
			dataIndex: 'nmbrg',
			sortable: true
		},
		{
			header: 'Jenis Barang',
			width: 85,
			dataIndex: 'nmjnsbrg',
			sortable: true
		},
		{
			header: 'Satuan kecil',
			width: 78,
			dataIndex: 'nmsatuankcl',
			sortable: true,
		},
		{
			header: 'Rasio',
			hidden:true,
			width: 51,
			dataIndex: 'rasio',
			sortable: true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},
		{
			header: 'Satuan Besar',
			width: 86,
			dataIndex: 'nmsatuanbsr',
			sortable: true,
		},
		/* {
			header: 'Harga Vg',
			width: 80,
			dataIndex: 'hrgavg',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right'
		}, */
		{
			header: 'Harga Beli',
			width: 75,
			dataIndex: 'hrgbeli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right'
		},{
			header: 'Harga Jual',
			width: 78,
			dataIndex: 'hrgjual',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right'
		},
		{
			header: 'Nama Pabrik',
			width: 90,
			dataIndex: 'nmpabrik',
			sortable: true
		},{
			header: 'Nama Supplier',
			width: 90,
			dataIndex: 'nmsupplier',
			sortable: true
		},{
			header: 'Status',
			width: 63,
			dataIndex: 'nmstatus',
			sortable: true
		},
		/* {
			header: 'Stok Min',
			width: 70,
			dataIndex: 'stokmin',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right'
		},
		{
			header: 'Stok Max',
			width: 70,
			dataIndex: 'stokmax',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right'
		}, */		
		{
			header: 'Keterangan',
			width: 69,
			dataIndex: 'keterangan',
			sortable: true
		},
		{
			header: "Gambar", 
			renderer: renderGbr1, 
			align:'center', 
			dataIndex: 'gambar', 
			sortable: true,
			hidden: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditBrgmedis(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteBrgmedis(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Barang Medis', iconCls:'silk-user',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadBrgmedis(){
		ds_brgmedis.reload();
	}
	
	function fnAddBrgmedis(){
		var grid = grid_nya;
		wEntryBrgmedis(false, grid, null);
		var userid = RH.getCompValue('tf.userid', true);
			if(userid != ''){
				RH.setCompValue('cb.user', userid);
			}
			return;
	}
	
	function fnEditBrgmedis(grid, record){
		var record = ds_brgmedis.getAt(record);
		wEntryBrgmedis(true, grid, record);		
	}
	
	function fnDeleteBrgmedis(grid, record){
		var record = ds_brgmedis.getAt(record);
		var url = BASE_URL + 'brgmedis_controller/delete_brgmedis';
		var params = new Object({
						kdbrg	: record.data['kdbrg']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
	/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryBrgmedis(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Barang Medis (Edit)':'Barang Medis (Entry)';
		var brgmedis_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.brgmedis',
			buttonAlign: 'left',
			labelWidth: 150, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 580, width: 450,
			layout: 'form', 
			frame: false,
			autoScroll: true,
			defaultType:'textfield',		
			items: [
			{
				fieldLabel: 'Kode',
				id:'tf.kdbrg',
				width: 100,
				style : 'opacity:0.6',
				readOnly: true
			},
			{
				fieldLabel: 'Nama Barang',
				id:'tf.nmbrg',
				width: 250,
				allowBlank: false
			},
			{
				xtype: 'combo', id: 'cb.klpbarang', 
				fieldLabel: 'Kelompok Barang',
				store: ds_klpbarang, triggerAction: 'all',
				valueField: 'idklpbrg', displayField: 'nmklpbarang',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},{
				xtype: 'compositefield',
				fieldLabel: 'Jenis Barang',
				id: 'comp_jnsbrg',
				width: 178,
				items: [{
					xtype: 'textfield', 
					id: 'tf.jbarang', 
					emptyText:'Pilih...',
					width: 150,
					editable: false,
					allowBlank: false,
					readOnly: true
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.jnsbrg',
					width: 4,
					handler: function() {
						fnwJbarang();
					}
				}]
			},{
				xtype: 'compositefield',
				fieldLabel: 'Satuan Kecil',
				id: 'comp_skcl',
				width: 178,
				items: [{
					xtype: 'textfield',
					id: 'tf.jsatuan',					
					emptyText:'Pilih...',
					width: 150,
					readOnly: true,
					allowBlank: false
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.jnsbrg',
					width: 4,
					handler: function() {
						fnwsatuankcl();
					}
				}]
			},{
				xtype: 'numericfield',
				fieldLabel: 'Rasio',
				hidden:true,
				id: 'tf.rasio',
				width: 150,
				allowBlank: true,
			},{
				xtype: 'compositefield',
				fieldLabel: 'Satuan Besar',
				id: 'comp_sbsr',
				width: 178,
				items: [{
					xtype: 'textfield', 
					id: 'tf.jsatuanbsr',
					emptyText:'Pilih...',
					width: 150,
					readOnly: true,
					allowBlank: false
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.jnsbrg',
					width: 4,
					handler: function() {
						fnwsatuanbsr();
					}
				}]
			},{
				xtype: 'numericfield',
				fieldLabel: 'Harga Avg',
				id:'tf.hrgavg',
				width: 150,
				hidden: true
			},{
				xtype: 'numericfield',
				fieldLabel: 'Harga Beli',
				id:'tf.hrgbeli',
				width: 150,
			},{
				xtype: 'numericfield',
				fieldLabel: 'Harga Jual',
				id:'tf.hrgjual',
				width: 150,
			},{
				xtype: 'compositefield',
				fieldLabel: 'Pabrik',
				id: 'comp_pabrik',
				width: 178,
				items: [{
					xtype: 'textfield',
					id: 'tf.pabrik',
					emptyText:'Pilih...',
					width: 150,
					editable: false,
					readOnly: true
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.pabrik',
					width: 4,
					handler: function() {
						fnwPabrik();
					}
				}]
			},{
				xtype: 'combo', id: 'cb.frm.status', 
				fieldLabel: 'Status',
				store: ds_status, triggerAction: 'all',
				valueField: 'idstatus', displayField: 'nmstatus',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},{
				xtype: 'combo', id: 'cb.frm.stgenerik', 
				fieldLabel: 'Generik',
				store: ds_stgenerik, triggerAction: 'all',
				valueField: 'nilai', displayField: 'nmset',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false
			},{
				xtype: 'combo', id: 'cb.frm.stformularium', 
				fieldLabel: 'Formularium',
				store: ds_stformularium, triggerAction: 'all',
				valueField: 'nilai', displayField: 'nmset',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false
			},{
				xtype: 'numericfield',
				fieldLabel: 'Stok Min',
				id:'tf.stokmin',
				width: 150,				
				hidden: true
			},		
			{
				xtype: 'numericfield',
				fieldLabel: 'Stok Max',
				id:'tf.stokmax',
				width: 150,
				hidden: true
			},{
				xtype: 'compositefield',
				fieldLabel: 'Supplier',
				width: 178,
				items: [{
					xtype: 'textfield', id: 'tf.supplier',
					width: 150,
					emptyText:'Pilih...',
					readOnly: true
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.supplier',
					width: 4,
					handler: function() {
						fnwSupplier();
					}
				}]
			},
			{
				xtype: 'textfield',
				id: 'idimage',
				fieldLabel: 'image',
				hidden: true
			},		
			{
				xtype: 'fileuploadfield',
				id: 'idgambar',
				fieldLabel: 'Gambar',
				emptyText: 'Select an image to upload...',
				width: 200,
				buttonText: 'Browse',
				listeners: {
					valid: function(){
						var idimage = RH.getCompValue('idgambar', true);
						if(idimage != ''){
							RH.setCompValue('idimage', idimage);
						}
						return;
					}
				}
			},
			{	
				xtype: 'textarea',
				fieldLabel: 'Keterangan',
				id:'ta.keterangan',
				width: 250,
			},	
			{
				xtype: 'datefield',
				fieldLabel: 'Tanggal Input',
				id: 'df.tglinput',
				format: "d/m/Y",
				value: new Date(),
				width: 150,
				editable: false,
				style : 'opacity:0.6',
				readOnly: true
			},	
			{
				xtype: 'textfield',
				fieldLabel: 'User Input',
				id:'tf.userinput',
				width: 150,
				value: USERNAME,
				style : 'opacity:0.6',
				readOnly: true
			}],
			buttons: [{
				text: 'Simpan', iconCls:'silk-save',
				handler: function() {
					fnSaveBrgmedis();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wBrgmedis.close();
				}
			}]
		});
			
		var wBrgmedis = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [brgmedis_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setBrgmedisForm(isUpdate, record);
		wBrgmedis.show();

	/**
	FORM FUNCTIONS
	*/	
		function setBrgmedisForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'brgmedis_controller/getNmjnsbrg',
						params:{
							idjnsbrg : record.get('idjnsbrg')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.jbarang', r);
						}
					});
					
					Ext.Ajax.request({
						url:BASE_URL + 'brgmedis_controller/getNmsatuan',
						params:{
							idsatuan : record.get('idsatuankcl')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.jsatuan', r);
						}
					});
					
					Ext.Ajax.request({
						url:BASE_URL + 'brgmedis_controller/getNmsatuanbsr',
						params:{
							idsatuan : record.get('idsatuanbsr')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.jsatuanbsr', r);
						}
					});
					
					Ext.Ajax.request({
						url:BASE_URL + 'brgmedis_controller/getNmpabrik',
						params:{
							idpabrik : record.get('idpabrik')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.pabrik', r);
						}
					});
					
					Ext.Ajax.request({
						url:BASE_URL + 'brgmedis_controller/getNmsupplier',
						params:{
							kdsupplier : record.get('kdsupplier')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.supplier', r);
						}
					});
					
					RH.setCompValue('tf.kdbrg', record.get('kdbrg'));
					RH.setCompValue('cb.klpbarang', record.get('idklpbrg'));
					RH.setCompValue('tf.jbarang', record.get('idjnsbrg'));
					RH.setCompValue('tf.nmbrg', record.get('nmbrg'));
					RH.setCompValue('tf.jsatuan', record.get('idsatuankcl'));
					RH.setCompValue('tf.rasio', record.get('rasio'));
					RH.setCompValue('tf.jsatuanbsr', record.get('idsatuanbsr'));
					RH.setCompValue('tf.hrgavg', record.get('hrgavg'));
					RH.setCompValue('tf.hrgbeli', record.get('hrgbeli'));
					RH.setCompValue('tf.hrgjual', record.get('hrgjual'));
					RH.setCompValue('tf.pabrik', record.get('idpabrik'));
					RH.setCompValue('cb.frm.status', record.get('idstatus'));
					RH.setCompValue('tf.stokmin', record.get('stokmin'));
					RH.setCompValue('tf.stokmax', record.get('stokmax'));
					RH.setCompValue('tf.supplier', record.get('kdsupplier'));
					RH.setCompValue('idimage', record.get('gambar'));
					RH.setCompValue('idgambar', record.get('gambar'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					RH.setCompValue('df.tglinput', record.data['tglinput']);
					RH.setCompValue('tf.userinput', record.get('userinput'));
					RH.setCompValue('cb.frm.stgenerik', record.get('idstgenerik'));
					RH.setCompValue('cb.frm.stformularium', record.get('idstformularium'));
					return;
				}
			}
		}
		
		function fnSaveBrgmedis(){
			var idForm = 'frm.brgmedis';
			var sUrl = BASE_URL +'brgmedis_controller/insert_brgmedis';
			var sParams = new Object({
				kdbrg			:	RH.getCompValue('tf.kdbrg'),
				idklpbrg		:	RH.getCompValue('cb.klpbarang'),
				idjnsbrg		:	RH.getCompValue('tf.jbarang'),
				nmbrg			:	RH.getCompValue('tf.nmbrg'),
				idsatuankcl		:	RH.getCompValue('tf.jsatuan'),
				rasio			:	RH.getCompValue('tf.rasio'),
				idsatuanbsr		:	RH.getCompValue('tf.jsatuanbsr'),
				hrgavg			:	RH.getCompValue('tf.hrgavg'),
				hrgbeli			:	RH.getCompValue('tf.hrgbeli'),
				hrgjual			:	RH.getCompValue('tf.hrgjual'),
				idpabrik		:	RH.getCompValue('tf.pabrik'),
				idstatus		:	RH.getCompValue('cb.frm.status'),
				stokmin			:	RH.getCompValue('tf.stokmin'),
				stokmax			:	RH.getCompValue('tf.stokmax'),
				kdsupplier		:	RH.getCompValue('tf.supplier'),
				gambar			:	RH.getCompValue('idimage'),
				keterangan		:	RH.getCompValue('ta.keterangan'),		
				tglinput		:	RH.getCompValue('df.tglinput'),
				userinput		:	RH.getCompValue('tf.userinput'),
				idstgenerik		:	RH.getCompValue('cb.frm.stgenerik'),
				idstformularium	:	RH.getCompValue('cb.frm.stformularium'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'brgmedis_controller/update_brgmedis';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wBrgmedis, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
					
	}
	
	function fnwPabrik(){		
		var ds_pabrik = dm_pabrik();
		
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_pabrik = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header: 'Nama',
				dataIndex: 'nmpabrik',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		
		var sm_pabrik = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_pabrik = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_pabrik = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_pabrik,
			displayInfo: true,
			displayMsg: 'Data Pabrik Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_pabrik = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_pabrik = new Ext.grid.GridPanel({
			ds: ds_pabrik,
			cm: cm_pabrik,
			sm: sm_pabrik,
			view: vw_pabrik,
			height: 460,
			width: 430,
			plugins: cari_pabrik,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_pabrik,
			listeners: {
				//rowdblclick: klik_cari_pabrik
				cellclick: onCellClickaddpabrik
			}
		});
		var win_find_cari_pabrik = new Ext.Window({
			title: 'Cari Pabrik',
			modal: true,
			items: [grid_find_cari_pabrik]
		}).show();
		
		function onCellClickaddpabrik(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idpabrik = record.data["nmpabrik"];
								
					Ext.getCmp('tf.pabrik').focus();
					Ext.getCmp("tf.pabrik").setValue(var_cari_idpabrik);
								win_find_cari_pabrik.close();
				return true;
			}
			return true;
		}
	}
	
	function fnwJbarang(){
		var ds_jnsbrg = dm_jnsbrg();
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_jnsbrg = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header: 'Jenis Barang',
				dataIndex: 'nmjnsbrg',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		
		var sm_jnsbrg = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_jnsbrg = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_jnsbrg = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_jnsbrg,
			displayInfo: true,
			displayMsg: 'Data Jenis Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_jnsbrg = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_jnsbrg = new Ext.grid.GridPanel({
			ds: ds_jnsbrg,
			cm: cm_jnsbrg,
			sm: sm_jnsbrg,
			view: vw_jnsbrg,
			height: 460,
			width: 430,
			plugins: cari_jnsbrg,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_jnsbrg,
			listeners: {
				//rowdblclick: klik_cari_jnsbrg
				cellclick: onCellClickaddjbrg
			}
		});
		var win_find_cari_jnsbrg = new Ext.Window({
			title: 'Cari Jenis Barang',
			modal: true,
			items: [grid_find_cari_jnsbrg]
		}).show();
		
		function onCellClickaddjbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idjnsbrg = record.data["nmjnsbrg"];
								
					Ext.getCmp('tf.jbarang').focus();
					Ext.getCmp("tf.jbarang").setValue(var_cari_idjnsbrg);
								win_find_cari_jnsbrg.close();
				return true;
			}
			return true;
		}
	}
	
	function fnwsatuankcl(){		
		var ds_jsatuan = dm_jsatuan();
	
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_skcl = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header: 'Satuan Kecil',
				dataIndex: 'nmsatuan',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		
		var sm_skcl = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_skcl = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_skcl = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_jsatuan,
			displayInfo: true,
			displayMsg: 'Data Jenis Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_skcl = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_skcl = new Ext.grid.GridPanel({
			ds: ds_jsatuan,
			cm: cm_skcl,
			sm: sm_skcl,
			view: vw_skcl,
			height: 460,
			width: 430,
			plugins: cari_skcl,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_skcl,
			listeners: {
				//rowdblclick: klik_cari_skcl
				cellclick: onCellClickaddjsatuan
			}
		});
		var win_find_cari_skcl = new Ext.Window({
			title: 'Satuan Kecil',
			modal: true,
			items: [grid_find_cari_skcl]
		}).show();
		
		function onCellClickaddjsatuan(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_satuan = record.data["nmsatuan"];
								
					Ext.getCmp('tf.jsatuan').focus();
					Ext.getCmp("tf.jsatuan").setValue(var_cari_satuan);
								win_find_cari_skcl.close();
				return true;
			}
			return true;
		}
	}
	
	function fnwsatuanbsr(){		
		var ds_jsatuan = dm_jsatuan();
		
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bsr = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header: 'Satuan Besar',
				dataIndex: 'nmsatuan',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		
		var sm_bsr = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bsr = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bsr = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_jsatuan,
			displayInfo: true,
			displayMsg: 'Data Jenis Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bsr = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bsr = new Ext.grid.GridPanel({
			ds: ds_jsatuan,
			cm: cm_bsr,
			sm: sm_bsr,
			view: vw_bsr,
			height: 460,
			width: 430,
			plugins: cari_bsr,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_bsr,
			listeners: {
				//rowdblclick: klik_cari_bsr
				cellclick: onCellClickaddjsatuanbsr
			}
		});
		var win_find_cari_bsr = new Ext.Window({
			title: 'Satuan Besar',
			modal: true,
			items: [grid_find_cari_bsr]
		}).show();
		
		function onCellClickaddjsatuanbsr(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var idsatuan = record.data["nmsatuan"];
								
					Ext.getCmp('tf.jsatuanbsr').focus();
					Ext.getCmp("tf.jsatuanbsr").setValue(idsatuan);
								win_find_cari_bsr.close();
				return true;
			}
			return true;
		}
	}
	
	function fnwSupplier(){
		var ds_supplier = dm_supplier();
		
		function fnkeyAddsup(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_supplier = new Ext.grid.ColumnModel([
			{
				dataIndex: 'kdsupplier',
				width: 100,
				hidden: true
			},{
				header: 'Nama Supplier',
				dataIndex: 'nmsupplier',
				width: 224,
				renderer: fnkeyAddsup
			},{
				header: 'Alamat',
				dataIndex: 'alamat',
				width: 230, 
			},{
				header: 'No. Telepon',
				dataIndex: 'notelp',
				width: 95, 
			},{
				header: 'No. Fax',
				dataIndex: 'nofax',
				width: 70, 
			},{
				header: 'Kontak Person',
				dataIndex: 'kontakperson',
				width: 110, 
			},{
				header: 'NO. Hp',
				dataIndex: 'nohp',
				width: 95, 
			}
		]);
		
		var sm_supplier = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_supplier = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_supplier = new Ext.PagingToolbar({
			pageSize: 20,
			store: ds_supplier,
			displayInfo: true,
			displayMsg: 'Data Supplier Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_supplier = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_supplier = new Ext.grid.GridPanel({
			ds: ds_supplier,
			cm: cm_supplier,
			sm: sm_supplier,
			view: vw_supplier,
			height: 460,
			width: 835,
			plugins: cari_supplier,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_supplier,
			listeners: {
				cellclick: onCellClickaddsup
			}
		});
		var win_find_cari_supplier = new Ext.Window({
			title: 'Cari Supplier',
			modal: true,
			items: [grid_find_cari_supplier]
		}).show();
		
		function onCellClickaddsup(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_kdsupplier = record.data["nmsupplier"];
					Ext.getCmp("tf.supplier").setValue(var_cari_kdsupplier);
								win_find_cari_supplier.close();
				return true;
			}
			return true;
		}
	}
}