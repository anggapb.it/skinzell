function Mbaganperkiraan(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_baganperkiraan = dm_baganperkiraan();	
	var ds_klpakun = dm_klpakun();
	var ds_jnsakun = dm_jnsakun();
	var ds_status = dm_status();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_baganperkiraan,
		displayInfo: true,
		displayMsg: 'Data Bagan Perkiraan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_baganperkiraan',
		store: ds_baganperkiraan,		
		autoScroll: true,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddBaganperkiraan();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 70,
			dataIndex: 'kdakun',
			sortable: true
		},
		{
			header: 'Nama Bagan Perkiraan',
			width: 250,
			dataIndex: 'nmakun',
			sortable: true
		},
		{
			header: 'Kel. Bagan Perkiraan',
			width: 120,
			dataIndex: 'nmklpakun',
			sortable: true,
			align: 'center',
		},
		{
			header: 'Jenis Bagan Perkiraan',
			width: 120,
			dataIndex: 'nmjnsakun',
			sortable: true,
			align: 'center',
		},{
			header: 'Parent',
			width: 130,
			dataIndex: 'nmparent',
			sortable: true,
			renderer: function(value, p, r){
				var parent = '';
					if(r.data['nmparent'] == 0) parent = '';
					if(r.data['nmparent'] != 0) parent = r.data['nmparent'];
				return parent ;
			}
		},
		{
			header: 'Status',
			width: 80,
			dataIndex: 'nmstatus',
			sortable: true,
			align: 'center',
		},{
			header: 'User Input',
			width: 100,
			dataIndex: 'nmlengkap',
			sortable: true,
			align: 'center',
		},{
			header: 'Keterangan',
			width: 120,
			dataIndex: 'keterangan',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditBaganperkiraan(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteBaganperkiraan(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Bagan Perkiraan', iconCls:'silk-user',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadBaganperkiraan(){
		ds_baganperkiraan.reload();
	}
	
	function fnAddBaganperkiraan(){
		var grid = grid_nya;
		wEntryBaganperkiraan(false, grid, null);	
	}
	
	function fnEditBaganperkiraan(grid, record){
		var record = ds_baganperkiraan.getAt(record);
		wEntryBaganperkiraan(true, grid, record);		
	}
	
	function fnDeleteBaganperkiraan(grid, record){
		var record = ds_baganperkiraan.getAt(record);
		var url = BASE_URL + 'baganperkiraan_controller/delete_baganperkiraan';
		var params = new Object({
						idakun	: record.data['idakun']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryBaganperkiraan(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Bagan Perkiraan (Edit)':'Bagan Perkiraan (Entry)';
		var baganperkiraan_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.baganperkiraan',
			buttonAlign: 'left',
			labelWidth: 150, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 350, width: 500,
			layout: 'form', 
			frame: false, 
			defaultType:'textfield',		
			items: [
			{
				fieldLabel: 'idakun',
				id: 'tf.idakun',
				width: 100,
				hidden: true
			},{
				fieldLabel: 'Kode',
				id:'tf.kdbaganperkiraan',
				width: 100,
				//readOnly: true,
				//style : 'opacity:0.6',
			},
			{
				fieldLabel: 'Nama Bagan Perkiraan',
				id:'tf.frm.nmbaganperkiraan',
				width: 300,
				allowBlank: false
			},
			{
				xtype: 'combo', id: 'cb.frm.kelbaganperkiraan', 
				fieldLabel: 'Kel. Bagan Perkiraan',
				store: ds_klpakun, triggerAction: 'all',
				valueField: 'idklpakun', displayField: 'nmklpakun',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},
			{
				xtype: 'combo', id: 'cb.frm.jbaganperkiraan', 
				fieldLabel: 'Jenis Bagan Perkiraan',
				store: ds_jnsakun, triggerAction: 'all',
				valueField: 'idjnsakun', displayField: 'nmjnsakun',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},
			{
				xtype: 'combo', id: 'cb.frm.status', 
				fieldLabel: 'Status',
				store: ds_status, triggerAction: 'all',
				valueField: 'idstatus', displayField: 'nmstatus',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},
			{
				xtype: 'compositefield',
				name: 'comp_pel_baganperkiraan',
				fieldLabel: 'Parent',
				id: 'comp_pel_baganperkiraan',
				items: [{
					xtype: 'textfield',
					id: 'tf.frm.idakunparent',				
					fieldLabel: 'Parent',
					width: 150, emptyText:'Pilih...'
				},
				{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn_data_pel_baganperkiraan',
					width: 3,
					handler: function() {
						parentBaganperkiraan();
					}
				}]
			},{
				xtype: 'textarea',
				fieldLabel: 'Keterangan',
				id:'ta.keterangan',
				width: 300,
			},{
				xtype: 'datefield', 
				fieldLabel: 'Tanggal Input',
				id: 'df.tglpp', 
				value: new Date(), 
				width: 150,
				format: 'd-m-Y',
				readOnly: true,
				style : 'opacity:0.6',
			},{
				fieldLabel: 'User Input',
				id:'tf.userid',
				width: 150,
				value: USERNAME,
				readOnly: true,
				style : 'opacity:0.6',
			}],
			buttons: [{
				text: 'Simpan', iconCls:'silk-save',
				handler: function() {
					fnSaveBaganperkiraan();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wBaganperkiraan.close();
				}
			}]
		});
			
		var wBaganperkiraan = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [baganperkiraan_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setBaganperkiraanForm(isUpdate, record);
		wBaganperkiraan.show();

	/**
	FORM FUNCTIONS
	*/	
		function setBaganperkiraanForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'baganperkiraan_controller/getNmakun',
						params:{
							idakunparent : record.get('idakunparent')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.frm.idakunparent', r);
						}
					});
					
					RH.setCompValue('tf.idakun', record.get('idakun'));
					RH.setCompValue('tf.kdbaganperkiraan', record.get('kdakun'));
					RH.setCompValue('tf.frm.nmbaganperkiraan', record.get('nmakun'));
					RH.setCompValue('cb.frm.kelbaganperkiraan', record.data['idklpakun']);
					RH.setCompValue('cb.frm.jbaganperkiraan', record.get('idjnsakun'));
					RH.setCompValue('cb.frm.status', record.get('idstatus'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					RH.setCompValue('tf.frm.idakunparent', record.get('idakunparent'));
					return;
				}
			}
		}
		
		function fnSaveBaganperkiraan(){
			var idForm = 'frm.baganperkiraan';
			var sUrl = BASE_URL +'baganperkiraan_controller/insert_baganperkiraan';
			var sParams = new Object({		
				idakun			:	RH.getCompValue('tf.idakun'),
				kdakun			:	RH.getCompValue('tf.kdbaganperkiraan'),
				nmakun			:	RH.getCompValue('tf.frm.nmbaganperkiraan'),
				idklpakun		:	RH.getCompValue('cb.frm.kelbaganperkiraan'),
				idjnsakun		:	RH.getCompValue('cb.frm.jbaganperkiraan'),
				idstatus		:	RH.getCompValue('cb.frm.status'),
				keterangan		:	RH.getCompValue('ta.keterangan'),
				idakunparent	:	RH.getCompValue('tf.frm.idakunparent'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'baganperkiraan_controller/update_baganperkiraan';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wBaganperkiraan, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
					
	}
	
	function parentBaganperkiraan(){
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var ds_baganperkiraan_parent = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
								url: BASE_URL + 'baganperkiraan_controller/get_parent_baganperkiraan',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
			fields: [{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			},{
				name: 'nmklpakun',
				mapping: 'nmklpakun'
			},{
				name: 'nmjnsakun',
				mapping: 'nmjnsakun'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});
		var cm_baganperkiraan_parent = new Ext.grid.ColumnModel([
			{
				header: 'Kode',
				width: 70,
				dataIndex: 'kdakun',
				sortable: true,
				renderer: fnkeyAdd
			},
			{
				header: 'Nama Bagan Perkiraan',
				width: 250,
				dataIndex: 'nmakun',
				sortable: true
			},
			{
				header: 'Kel. Bagan Perkiraan',
				width: 120,
				dataIndex: 'nmklpakun',
				sortable: true,
				align: 'center',
			},
			{
				header: 'Jenis Bagan Perkiraan',
				width: 120,
				dataIndex: 'nmjnsakun',
				sortable: true,
				align: 'center',
			},{
				header: 'Parent',
				width: 130,
				dataIndex: 'nmparent',
				sortable: true,
				renderer: function(value, p, r){
					var parent = '';
						if(r.data['nmparent'] == 0) parent = '';
						if(r.data['nmparent'] != 0) parent = r.data['nmparent'];
					return parent ;
				}
			}
		]);
		var sm_baganperkiraan_parent = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_baganperkiraan_parent = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_baganperkiraan_parent = new Ext.PagingToolbar({
			pageSize: 10,
			store: ds_baganperkiraan_parent,
			displayInfo: true,
			displayMsg: 'Data Bagan Perkiraan Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var car_baganperkiraan_parent = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200,
			//disableIndexes:['kdakun'],
		})];
		var grid_find_baganperkiraan_parent = new Ext.grid.GridPanel({
			ds: ds_baganperkiraan_parent,
			cm: cm_baganperkiraan_parent,
			sm: sm_baganperkiraan_parent,
			view: vw_baganperkiraan_parent,
			height: 400,
			width: 720,
			plugins: car_baganperkiraan_parent,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_baganperkiraan_parent,
			listeners: {
				cellclick: onCellClickaddparent
			}
		});
		var win_find_baganperkiraan_parent = new Ext.Window({
			title: 'Sub Bagan Perkiraan',
			modal: true,
			items: [grid_find_baganperkiraan_parent]
		}).show();
		
		function onCellClickaddparent(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_baganperkiraan_parent = record.data["nmakun"];
						
					Ext.getCmp('tf.frm.idakunparent').focus()
					Ext.getCmp("tf.frm.idakunparent").setValue(var_baganperkiraan_parent);
								win_find_baganperkiraan_parent.close();
				return true;
			}
			return true;
		}
	}
	
}
