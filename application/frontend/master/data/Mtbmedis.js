function Mtbmedis(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_tbmedis = dm_tbmedis();
	//var ds_jnsbrg = dm_jnsbrg();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_tbmedis,
		displayInfo: true,
		displayMsg: 'Data Tarif Barang Medis Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_tbmedis',
		store: ds_tbmedis,		
		autoScroll: true,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddTbmedis();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Nama Barang',
			width: 250,
			dataIndex: 'nmbrg',
			sortable: true
		},
		{
			header: 'Margin Barang Medis',
			width: 125,
			dataIndex: 'margin',
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditTbmedis(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteTbmedis(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Tarif Barang Medis', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadTbmedis(){
		ds_tbmedis.reload();
	}
	
	function fnAddTbmedis(){
		var grid = grid_nya;
		wEntryTbmedis(false, grid, null);	
	}
	
	function fnEditTbmedis(grid, record){
		var record = ds_tbmedis.getAt(record);
		wEntryTbmedis(true, grid, record);		
	}
	
	function fnDeleteTbmedis(grid, record){
		var record = ds_tbmedis.getAt(record);
		var url = BASE_URL + 'tbmedis_controller/delete_tbmedis';
		var params = new Object({
						idmarginbrg	: record.data['idmarginbrg']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryTbmedis(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Tarif Barang Medis (Edit)':'Tarif Barang Medis (Entry)';
		var tbmedis_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.tbmedis',
			buttonAlign: 'left',
			labelWidth: 130, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 180, width: 500,
			layout: 'form', 
			frame: false, 
			defaultType:'textfield',		
			items: [ 
			{
				id: 'tf.frm.idmarginbrg', 
				hidden: true,
			},
			/* {
				xtype: 'compositefield',
				fieldLabel: 'Jenis Barang',
				id: 'comp_jnsbrg',
				items: [{
					xtype: 'combo', id: 'cb.jbarang', 
					store: ds_jnsbrg, triggerAction: 'all',
					valueField: 'idjnsbrg', displayField: 'nmjnsbrg',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', width: 150,
					editable: false, allowBlank: false,
					readOnly: true
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.jnsbrg',
					width: 4,
					handler: function() {
						fnwJbarang();
					}
				}]
			}, */
			{
				xtype: 'compositefield',
				fieldLabel: 'Nama Barang',
				width: 278,
				items: [{
					xtype: 'textfield', 
					id: 'tf.barang', 
					emptyText:'Pilih...',
					width: 250,
					editable: false,
					allowBlank: false,
					readOnly: true
				},{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn.brg',
					width: 4,
					handler: function() {
						fnwbarang();
					}
				}]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Margin Barang Medis',
				id:'com_margin',
				items: [
					{
					xtype: 'numericfield',
					fieldLabel: 'Margin Barang Medis',
					id:'tf.frm.margin',
					width: 100,
					decimalPrecision: 2,		
					decimalSeparator: ',',						
					thousandSeparator: '.',
					alwaysDisplayDecimals: true,
					useThousandSeparator: true,
				},{
					xtype: 'label', id: 'lb.lb', text: '%', margins: '3 0 0 0',
				}]				
			}],
			buttons: [{
				text: 'Simpan', iconCls:'silk-save',
				handler: function() {
					fnSaveTbmedis();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wTbmedis.close();
				}
			}]
		});
			
		var wTbmedis = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [tbmedis_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setTbmedisForm(isUpdate, record);
		wTbmedis.show();

	/**
	FORM FUNCTIONS
	*/	
		function setTbmedisForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'tbmedis_controller/getNmbarang',
						params:{
							kdbrg : record.get('kdbrg')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.barang', r);
						}
					});
					
					RH.setCompValue('tf.frm.idmarginbrg', record.get('idmarginbrg'));
					//RH.setCompValue('cb.jbarang', record.data['idjnsbrg']);
					RH.setCompValue('tf.barang', record.data['kdbrg']);
					RH.setCompValue('tf.frm.margin', record.get('margin'));
					return;
				}
			}
		}
		
		function fnSaveTbmedis(){
			var idForm = 'frm.tbmedis';
			var sUrl = BASE_URL +'tbmedis_controller/insert_tbmedis';
			var sParams = new Object({
				idmarginbrg		:	RH.getCompValue('tf.frm.idmarginbrg'),
				//idjnsbrg		:	RH.getCompValue('cb.jbarang'),
				kdbrg			:	RH.getCompValue('tf.barang'),
				margin			:	RH.getCompValue('tf.frm.margin'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'tbmedis_controller/update_tbmedis';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wTbmedis, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}					
	}
	
	function fnwbarang(){
		var ds_brgmedis = dm_brgmedis();
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_brgmedis = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
			{
				header: 'Nama Barang',
				dataIndex: 'nmbrg',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		
		var sm_brgmedis = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_brgmedis = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_brgmedis = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_brgmedis,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_brgmedis = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_brgmedis = new Ext.grid.GridPanel({
			ds: ds_brgmedis,
			cm: cm_brgmedis,
			sm: sm_brgmedis,
			view: vw_brgmedis,
			height: 460,
			width: 430,
			plugins: cari_brgmedis,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_brgmedis,
			listeners: {
				//rowdblclick: klik_cari_brgmedis
				cellclick: onCellClickaddjbrg
			}
		});
		var win_find_cari_brgmedis = new Ext.Window({
			title: 'Cari Barang',
			modal: true,
			items: [grid_find_cari_brgmedis]
		}).show();
		
		function onCellClickaddjbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbrgmedis = record.data["nmbrg"];
								
					Ext.getCmp('tf.barang').focus();
					Ext.getCmp("tf.barang").setValue(var_cari_idbrgmedis);
								win_find_cari_brgmedis.close();
				return true;
			}
			return true;
		}
	}
}
