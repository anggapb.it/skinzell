function Mtpumum(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_tpumum = dm_tpumum();
	var ds_kdpelayanan = dm_kdpelayanan();
	var ds_klstarif = dm_klstarif();	
	var ds_klsrawat = dm_klsrawat();
	
	var arr_cari = [['tarif.kdpelayanan', 'Kode Pelayanan'],['nmpelayanan', 'Nama Pelayanan'],['nmjnspelayanan', 'Jenis Pelayanan'],['nmparent', 'Parent']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	var cari_pelayanan = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];

	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_kdpelayanan,
		displayInfo: true,
		displayMsg: 'Data Daftar Pelayanan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
		//Checkbox pada Grid
	var cbGrid = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			beforerowselect : function (sm, rowIndex, keep, rec) {
				if (this.deselectingFlag && this.grid.enableDragDrop){
					this.deselectingFlag = false;
					this.deselectRow(rowIndex);
					return this.deselectingFlag;
				}
				return keep;
			}
		}	
	});
	
	var grid_pelayanan = new Ext.grid.GridPanel({
		id: 'grid_pelayanan',
		sm: cbGrid,
		store: ds_kdpelayanan,		
		autoScroll: true,
		//autoHeight: true,
		columnLines: true,
		height: 230,
		plugins: cari_pelayanan,
		tbar: [],
		//sm: sm_nya,
		frame: true,
		columns: [new Ext.grid.RowNumberer(),
		cbGrid,
		{
			header: 'Kode',
			width: 80,
			dataIndex: 'kdpelayanan',
			sortable: true
		},
		{
			header: 'Nama Pelayanan',
			width: 320,
			dataIndex: 'nmpelayanan',
			sortable: true
		},
		{
			header: 'Jenis Pelayanan',
			width: 110,
			dataIndex: 'nmjnspelayanan',
			sortable: true
		},
		{
			header: 'Jenis Hirarki',
			width: 75,
			dataIndex: 'nmjnshirarki',
			sortable: true
		},{
			header: 'Parent',
			width: 130,
			dataIndex: 'nmparent',
			sortable: true,
			renderer: function(value, p, r){
				var parent = '';
					if(r.data['nmparent'] == 0) parent = '';
					if(r.data['nmparent'] != 0) parent = r.data['nmparent'];
				return parent ;
			}
		},
		{
			header: 'Status',
			width: 80,
			dataIndex: 'nmstatus',
			sortable: true
		}],
		bbar: paging
	});	
	
	var grid_klsrawat = new Ext.grid.GridPanel({
		id: 'grid_klsrawat',
		store: ds_klsrawat,
		height: 230,
		columnLines: true,
		frame: true,
		pageSize: pageSize,
		//tbar: [],
		//sm: sm_nya,
		columns: [
		{
			header: 'Bagian (Unit/Ruangan)',
			width: 168,
			dataIndex: 'nmbagian',
			sortable: true
		},{
			header: 'Kelas Traif',
			width: 100,
			dataIndex: 'nmklstarif',
			sortable: true
		}],
		//bbar: paging_klsrawat,
		
	});
	
	function add(btn){
		/* console.log(btn);
		if(btn == 'yes')
		{ */
			var m = grid_pelayanan.getSelectionModel().getSelections();
			var store = grid_pelayanan.getStore();			
			for(var i=0; i< m.length; i++){
				var rec = m[i];
				console.log(rec);
				if(rec){
					console.log(rec.get("kdpelayanan"));
					var kdpelayanan = rec.data['kdpelayanan'];
					Ext.Ajax.request({
						url: BASE_URL +'tpumum_controller/insert_tpumum',
						method: 'POST',
						params: {
							kdpelayanan 	: kdpelayanan,
							idklstarif		: RH.getCompValue('cb.frm.klstarif')
						},
						success: function(){
							Ext.getCmp('gp.grid_tpumum').store.reload();
						}
					});
				}
			}			
		//}
	}
	
	function fncekkdpelyanan(){
		var m = grid_pelayanan.getSelectionModel().getSelections();
		for(var i=0; i< m.length; i++){
			var rec = m[i];
			console.log(rec);
			if(rec){
				console.log(rec.get("kdpelayanan"));
				var kdpelayanan = rec.data['kdpelayanan'];
				Ext.Ajax.request({
					url: BASE_URL + 'tpumum_controller/cekkdpelayanan',
					method: 'POST',
					params: {
						idklstarif : Ext.getCmp('cb.frm.klstarif').getValue(),
						kdpelayanan : kdpelayanan
					},
					success: function(response){
						pelayanan = response.responseText;
						if (pelayanan =='1') {
							Ext.MessageBox.alert('Message', 'Data Sudah Ada...');
						} else {
							//Ext.MessageBox.confirm('Message', 'Tambah Data Yang Di Pilih..?' , add);
							add();
						}
					}
				});
			}
		}
	}
	
	function fnSearchgrid(){		
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
			ds_tpumum.setBaseParam('key',  '1');
			ds_tpumum.setBaseParam('id',  idcombo);
			ds_tpumum.setBaseParam('name',  nmcombo);
		ds_tpumum.load();
	}	
	
	var editor = new Ext.ux.grid.RowEditor({
        saveText: 'Update',
		clicksToEdit: 1
    });
	
	var row_gridnya = new Ext.grid.RowSelectionModel({
		singleSelect: true
	});
	
	var vw_grid_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	var grid_nya = new Ext.grid.EditorGridPanel({
		id: 'gp.grid_tpumum',
		store: ds_tpumum,
		sm: row_gridnya,
		view: vw_grid_nya,
		tbar: [{
			xtype: 'tbtext',
			style: 'marginLeft: 10px',
			text: 'Search :'
		},{
			xtype: 'combo', 
			id: 'cb.search', width: 150, 
			store: ds_cari, 
			valueField: 'id', displayField: 'nama',
			editable: false, triggerAction: 'all',
			forceSelection: true, submitValue: true, mode: 'local',
			emptyText:'Pilih...', disabled: true,
			listeners: {
				select: function() {
					var cbsearchh = Ext.getCmp('cb.search').getValue();
						if(cbsearchh != ''){
							Ext.getCmp('cek').enable();
							Ext.getCmp('btn_cari').enable();
							Ext.getCmp('cek').focus();
						}
						return;
				}
			}
		},{
			xtype: 'textfield',
			style: 'marginLeft: 7px',
			id: 'cek',
			width: 185,
			disabled: true,
			validator: function(){
				var cek = Ext.getCmp('cek').getValue();
				if(cek == ''){
					fnSearchgrid();
				}
				return;
			}
		},{ 
			text: 'Cari',
			id:'btn_cari',
			iconCls: 'silk-find',
			style: 'marginLeft: 7px',
			disabled: true,
			handler: function() {
				var btncari = Ext.getCmp('cek').getValue();
					if(btncari != ''){
						fnSearchgrid();
					}else{
						Ext.MessageBox.alert('Message', 'Tidak ada data yang di cari');
					}
				return;
			}
		}],
		//plugins: editor,//cari_data,
		autoScroll: true,
		columnLines: true,
		height: 260, 
		forceFit: true,		
        loadMask: true,
		frame: true,
		clicksToEdit: 1,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'ID',
			width: 60,
			dataIndex: 'idpenjamin',
			sortable: true,
			hidden: true
		},{
			header: 'Kode',
			width: 70,
			dataIndex: 'kdpelayanan',
			sortable: true
		},
		{
			header: 'Nama Pelayanan',
			width: 245,
			dataIndex: 'nmpelayanan',
			sortable: true
		},
		{
			header: 'Jenis Pelayanan',
			width: 95,
			dataIndex: 'nmjnspelayanan',
			sortable: true
		},{
			header: 'Parent',
			width: 130,
			dataIndex: 'nmparent',
			sortable: true,
			renderer: function(value, p, r){
				var parent = '';
					if(r.data['nmparent'] == 0) parent = '';
					if(r.data['nmparent'] != 0) parent = r.data['nmparent'];
				return parent ;
			}
		},
		{
			header: 'Kelas Tarif',
			width: 61,
			dataIndex: 'nmklstarif',
			sortable: true,
		},
		{
			header: 'Tarif JS <br> (Jasa Sarana)',
			width: 110,
			dataIndex: 'tarifjs',
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
			editor: new Ext.form.TextField({
						id: 'tarifjs',
                        enableKeyEvents: true,
                        listeners: {
                            specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									 fnEdiTarifjs();
								}
							}
						}
					})
		},
		{
			header: 'Tarif JM <br> (Komisi Dokter)',
			width: 110,
			dataIndex: 'tarifjm',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
			editor: new Ext.form.TextField({
						id: 'tarifjm',
                        enableKeyEvents: true,
                        listeners: {
                            specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									 fnEdiTarifjm();
								}
							}
						}
					})
		},
		{
			header: 'Tarif JP',
			width: 75,
			dataIndex: 'tarifjp',
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
			editor: new Ext.form.TextField({
						id: 'tarifjp',
                        enableKeyEvents: true,
                        listeners: {
                            specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									 fnEdiTarifjp();
								}
							}
						}
					})
		},
		{
			header: 'Tarif BHP',
			//hidden: true,
			width: 75,
			dataIndex: 'tarifbhpnew',
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
			// editor: new Ext.form.TextField({
			// 			id: 'tarifbhp',
   //                      enableKeyEvents: true,
   //                      listeners: {
   //                          specialkey: function(field, e){
			// 					if (e.getKey() == e.ENTER) {
			// 						 fnEdiTarifbhp();
			// 					}
			// 				}
			// 			}
			// 		})
		},
		{
			header: 'Total Tarif',
			width: 81,
			dataIndex: 'total',
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},{
                xtype: 'actioncolumn',
                width: 45,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnHapusTpumum(rowIndex);
                    }
                }]
        }],
		/* bbar: [
		{
			xtype:'tbfill'	
		},
		{
			xtype: 'fieldset',
			height: 45,
			border: false,
			items: [{
				xtype: 'numericfield', fieldLabel: 'Total Jumlah', 
				id:'tf.total', readOnly: true, disabled: false, width:100,
			}]
		}], */
		listeners: {	
			rowclick: Addrecord
		}
	});
	
	function Addrecord(grid, rowIndex, columnIndex){	
		//RH.setCompValue('tf.total', RH.sumRecVal(ds_tpumum, 'total'));
		
		var record = grid.getStore().getAt(rowIndex);
		RH.setCompValue('tf.frm.kdpelayanan', record.data['kdpelayanan']);
		RH.setCompValue('tf.frm.klstarif', record.data['idklstarif']);
		//RH.setCompValue('tf.frm.idpenjamin', record.data['idpenjamin']);
	}
	
	function fnEdiTarifjs(){
		Ext.getCmp('alertupdate').getEl().setStyle('color', 'black');
		Ext.getCmp('alertupdate').setText("Sedang Mengupdate...");
        var tarifjs = Ext.getCmp('tarifjs').getValue();
        var letters = /^[a-zA-Z]+$/;
		var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
        if(tarifjs.match(letters)){
			alert('Masukan Angka');
            ds_tpumum.reload();
        } 
		else if(tarifjs.match(simbol)){
            alert('Masukan Angka');
            ds_tpumum.reload();
        } 
        else {           
            fnTarifjs(tarifjs);   
        }      
    }
	
	function fnTarifjs(tarifjs){
		Ext.Ajax.request({
			url: BASE_URL + 'tpumum_controller/update_tarifjs',
			params: {
				//idpenjamin	: Ext.getCmp('tf.frm.idpenjamin').getValue(),
				kdpelayanan	: Ext.getCmp('tf.frm.kdpelayanan').getValue(),
				idklstarif	: Ext.getCmp('tf.frm.klstarif').getValue(),
                tarifjs    	: tarifjs
			},
			success: function() {
				Ext.getCmp('alertupdate').getEl().setStyle('color', 'blue');
					Ext.getCmp('alertupdate').setText("Berhasil Di Update");
				//Ext.Msg.alert("Info", "Ubah Berhasil");
				//fTotal();
				//Ext.getCmp('gp.grid_tpumum').store.reload();
			},
			failure: function() {
				Ext.getCmp('alertupdate').getEl().setStyle('color', 'red');
					Ext.getCmp('alertupdate').setText("Update Gagal");
				Ext.Msg.alert("Info", "Ubah Data Gagal");
			}
		});
	}
	
	function fnEdiTarifjm(){
		Ext.getCmp('alertupdate').getEl().setStyle('color', 'black');
		Ext.getCmp('alertupdate').setText("Sedang Mengupdate...");
        var tarifjm = Ext.getCmp('tarifjm').getValue();
        var letters = /^[a-zA-Z]+$/;
		var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
        if(tarifjm.match(letters)){
            alert('Masukan Angka');
            ds_tpumum.reload();
        } 
		else if(tarifjm.match(simbol)){
            alert('Masukan Angka');
            ds_tpumum.reload();
        } 
        else {           
            fnTarifjm(tarifjm);   
        }      
    }
	
	function fnTarifjm(tarifjm){
		Ext.Ajax.request({
			url: BASE_URL + 'tpumum_controller/update_tarifjm',
			params: {
				//idpenjamin	: Ext.getCmp('tf.frm.idpenjamin').getValue(),
				kdpelayanan	: Ext.getCmp('tf.frm.kdpelayanan').getValue(),
				idklstarif	: Ext.getCmp('tf.frm.klstarif').getValue(),
                tarifjm    : tarifjm
			},
			success: function() {
				Ext.getCmp('alertupdate').getEl().setStyle('color', 'blue');
				Ext.getCmp('alertupdate').setText("Berhasil Di Update");
				//Ext.Msg.alert("Info", "Ubah Berhasil");
				//fTotal();
			//	Ext.getCmp('gp.grid_tpumum').store.reload();
			},
			failure: function() {
				Ext.getCmp('alertupdate').getEl().setStyle('color', 'red');
				Ext.getCmp('alertupdate').setText("Update Gagal");
				Ext.Msg.alert("Info", "Ubah Data Gagal");
			}
		});
	}
	
	function fnEdiTarifjp(){
		Ext.getCmp('alertupdate').getEl().setStyle('color', 'black');
		Ext.getCmp('alertupdate').setText("Sedang Mengupdate...");
        var tarifjp = Ext.getCmp('tarifjp').getValue();
        var letters = /^[a-zA-Z]+$/;
		var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
        if(tarifjp.match(letters)){
            alert('Masukan Angka');
            ds_tpumum.reload();
        } 
		else if(tarifjp.match(simbol)){
            alert('Masukan Angka');
            ds_tpumum.reload();
        } 
        else {           
            fnTarifjp(tarifjp);   
        }      
    }
	
	function fnTarifjp(tarifjp){
		Ext.Ajax.request({
			url: BASE_URL + 'tpumum_controller/update_tarifjp',
			params: {
				//idpenjamin	: Ext.getCmp('tf.frm.idpenjamin').getValue(),
				kdpelayanan	: Ext.getCmp('tf.frm.kdpelayanan').getValue(),
				idklstarif	: Ext.getCmp('tf.frm.klstarif').getValue(),
                tarifjp    : tarifjp
			},
			success: function() {
				Ext.getCmp('alertupdate').getEl().setStyle('color', 'blue');
				Ext.getCmp('alertupdate').setText("Berhasil Di Update");
				//Ext.Msg.alert("Info", "Ubah Berhasil");
				//fTotal();
				//Ext.getCmp('gp.grid_tpumum').store.reload();
			},
			failure: function() {
				Ext.Msg.alert("Info", "Ubah Data Gagal");
				Ext.getCmp('alertupdate').getEl().setStyle('color', 'red');
				Ext.getCmp('alertupdate').setText("Update Gagal");
			}
		});
	}
	
	function fnEdiTarifbhp(){
		Ext.getCmp('alertupdate').getEl().setStyle('color', 'black');
		Ext.getCmp('alertupdate').setText("Sedang Mengupdate...");
        var tarifbhp = Ext.getCmp('tarifbhp').getValue();
        var letters = /^[a-zA-Z]+$/;
		var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
        if(tarifbhp.match(letters)){
            alert('Masukan Angka');
            ds_tpumum.reload();
        } 
		else if(tarifbhp.match(simbol)){
            alert('Masukan Angka');
            ds_tpumum.reload();
        } 
        else {           
            fnTarifbhp(tarifbhp);   
        }      
    }
	
	function fnTarifbhp(tarifbhp){
		Ext.Ajax.request({
			url: BASE_URL + 'tpumum_controller/update_tarifbhp',
			params: {
				//idpenjamin	: Ext.getCmp('tf.frm.idpenjamin').getValue(),
				kdpelayanan	: Ext.getCmp('tf.frm.kdpelayanan').getValue(),
				idklstarif	: Ext.getCmp('tf.frm.klstarif').getValue(),
                tarifbhp    : tarifbhp
			},
			success: function() {
				Ext.getCmp('alertupdate').getEl().setStyle('color', 'blue');
				Ext.getCmp('alertupdate').setText("Berhasil Di Update");
				//Ext.Msg.alert("Info", "Ubah Berhasil");
				//fTotal();
				//Ext.getCmp('gp.grid_tpumum').store.reload();
			},
			failure: function() {
				Ext.getCmp('alertupdate').getEl().setStyle('color', 'red');
				Ext.getCmp('alertupdate').setText("Update Gagal");
				Ext.Msg.alert("Info", "Ubah Data Gagal");
			}
		});
	}
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Tarif Pelayanan Umum', iconCls:'silk-user',
		width: 900, Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		//defaults: { labelWidth: 150, labelAlign: 'right', width: 910, },
		items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.70,
			//defaults: { labelWidth: 87, labelAlign: 'right'},
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Daftar Pelayanan',
				layout: 'form',
				height: 250,
				boxMaxHeight:265,
				items: [grid_pelayanan]
			}]
		},
		{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.30,
			layout: 'fit',
			//defaults: { labelWidth: 87, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset', 
				title: 'Informasi Kelas Tarif Bagian',
				layout: 'form',
				height: 250,
				boxMaxHeight:265,
				items: [grid_klsrawat]
			}]
		},
		{
			xtype: 'container',
			style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Daftar Tarif Pelayanan Umum',
				layout: 'form',
				height: 310,
				boxMaxHeight:325,
				items: [{
					xtype: 'panel', layout:'form', columnWidth: 0.99,
					id:'fp.master',
					items: [grid_nya],
					tbar: [
						{
							text: 'Tambah',
							id: 'btn_add',
							iconCls: 'silk-add',
							handler: function()
							{
								var m = grid_pelayanan.getSelectionModel().getSelections();
								if(m.length > 0)
								{
									fncekkdpelyanan(); 
								}
								else
								{
									Ext.MessageBox.alert('Message', 'Data Belum Di Pilih...!');
								}
							}
						},'-' ,
						{
							xtype: 'tbtext',
							text: 'Kelas Tarif :'
						},
						{
							xtype: 'combo', id: 'cb.frm.klstarif', 
							store: ds_klstarif, triggerAction: 'all',
							valueField: 'idklstarif', displayField: 'nmklstarif',
							forceSelection: true, submitValue: true, 
							mode: 'local', emptyText:'Pilih Kelas Tarif...', width: 150,
							editable: false,
							allowBlank: false,
							listeners: {
								select: function() {
									var isi = Ext.getCmp('cb.frm.klstarif').getValue();
									if (isi){
										Ext.getCmp('btn_add').enable();
										Ext.getCmp('cb.search').enable();
										Ext.getCmp("tf.klstarif").setValue(isi);
										Ext.getCmp('bt_cpdata').enable();
									} else {
										Ext.getCmp('btn_add').disable(); 
										Ext.getCmp('cb.search').disable();
										Ext.getCmp('bt_cpdata').disable();
									}
								}
							}
						},'-',
						{
							text: 'Salin Tarif Pelayanan Umum',
							id: 'bt_cpdata',
							iconCls: 'silk-save',
							disabled: true,
							style: 'marginLeft: 10px',
							handler: function(){								
								var cbtrf = Ext.getCmp("cb.frm.klstarif").getValue();
								if(cbtrf != ""){
									fncopydata();
									Ext.getCmp("cb.klstariflama").setValue(cbtrf);
								}
								return;
							}
						},'-',	{
							xtype: 'label', id: 'alertupdate',
						},
						{
							xtype: 'textfield',
							fieldLabel: 'penjamin',
							id:'tf.frm.idpenjamin',					
							width: 50,
							hidden: true
						},		
						{
							xtype: 'textfield',
							fieldLabel: 'Kode',
							id:'tf.frm.kdpelayanan',
							width: 80,
							hidden: true
						},		
						{
							xtype: 'textfield',
							fieldLabel: 'kls',
							id:'tf.frm.klstarif',
							width: 50,
							hidden: true
						},		
						{
							xtype: 'textfield',
							fieldLabel: 'kls',
							id:'tf.klstarif',
							emptyText: 'idklstarif',
							width: 70,
							hidden: true,
							validator: function() {
								ds_tpumum.setBaseParam('klstarif', Ext.getCmp('tf.klstarif').getValue());
								Ext.getCmp('gp.grid_tpumum').store.reload();
								//fTotal();
							}
						}
					]
				}]
			}]
		}],		
		listeners: {
			afterrender: mulai
		}
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	function mulai(){
		//fTotal();
		Ext.getCmp('btn_add').setDisabled(true);
		Ext.getCmp('cb.search').setDisabled(true);
		ds_tpumum.reload();
	}
	
	/* function fTotal(){
		ds_tpumum.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				ds_tpumum.each(function (rec) { sum += parseFloat(rec.get('total')); });
				Ext.getCmp("tf.total").setValue(sum);
			}
		});
	} */
	
	function reloadTpumum(){
		ds_tpumum.reload();
	}
		
	function fnEditTpumum(grid, record){
		var record = ds_tpumum.getAt(record);
		wEntryTpumum(true, grid, record);		
	}
	
	function fnDeleteTpumum(grid, record){
		var record = ds_tpumum.getAt(record);
		var url = BASE_URL + 'tpumum_controller/delete_tpumum';
		var params = new Object({
						//idpenjamin	: record.data['tarif.idpenjamin'],
						kdpelayanan	: record.data['tarif.kdpelayanan'],
						idklstarif	: record.data['tarif.idklstarif']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
	function fnHapusTpumum(record){
		var record = ds_tpumum.getAt(record);
		Ext.Msg.show({
			title: 'Konfirmasi',
			msg: 'Hapus data yang dipilih..?',
			buttons: Ext.Msg.YESNO,
			icon: Ext.MessageBox.QUESTION,
			fn: function (response) {
				if ('yes' !== response) {
					return;
				}
				Ext.Ajax.request({
				url: BASE_URL + 'tpumum_controller/delete_tpumum',
				params: {				
					//idpenjamin	: record.data['idpenjamin'],
					kdpelayanan	: record.data['kdpelayanan'],
					idklstarif	: record.data['idklstarif']
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
					Ext.getCmp('gp.grid_tpumum').store.reload();
					//fTotal();
				},
				failure: function() {
					//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
				}
			});
			
			}            
		});	
	}
	
	function fncopydata(){		
		
		var form_copy = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.copy',
			labelWidth: 120, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 293, width: 350,
			layout: 'form',
			frame: true,	
			items: [{
				xtype: 'fieldset', layout:'form', height: 80,
				frame:true,
				items: [{
					xtype: 'combo', id: 'cb.klstariflama', fieldLabel: 'Kelas Tarif Asal',
					store: ds_klstarif, triggerAction: 'all',
					valueField: 'idklstarif', displayField: 'nmklstarif',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih Kelas Tarif..', width: 170,
					editable: false, //readOnly: true, style: 'opacitty: 0.6',
				},{
					xtype: 'combo', id: 'cb.klstarifbaru', fieldLabel: 'Kelas Tarif Tujuan',
					store: ds_klstarif, triggerAction: 'all',
					valueField: 'idklstarif', displayField: 'nmklstarif',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih Kelas Tarif..', width: 170,
					editable: false,
					allowBlank: false,
				}]
			},{
				xtype: 'button',
				text: 'Simpan',
				id: 'bt.cpy',
				iconCls:'silk-save',
				style: 'margin: -5px 0 0 10px',
				width: 72,
				handler: function(){
					copydata();
				}						
			},{
				xtype: 'button',
				text: 'Kembali',
				id: 'bt.kemb',
				iconCls:'silk-arrow-undo',
				style: 'marginLeft: 90px; marginTop: -22px; marginBottom: 15px',
				width: 72,
				handler: function(){
					win_salin_dta.close();
				}						
			},{
				xtype: 'fieldset', title: 'Informasi :',
				items: [{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -13px 20px 0 -5px; font-size: 13px', width: 330,
					items: [{
						xtype: 'label',
						text: '1. Tarif pelayanan yang disalin adalah tarif pelayanan',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -23px 20px 0 10px; font-size: 13px',
					items: [{
						xtype: 'label',
						text: 'umum',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -18px 20px 0 -5px; font-size: 13px', width: 320,
					items: [{
						xtype: 'label',
						text: '2. Tarif pelayanan asal akan menyailn dan menimpa',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -22px 20px 0 10px; font-size: 13px', width: 350,
					items: [{
						xtype: 'label',
						text: ' (repleace) tarif pelayanan tujuan berdasarkan kelas tarif yang dipilih',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -18px 20px 0 -5px; font-size: 13px', width: 350,
					items: [{
						xtype: 'label',
						text: '3. Kelas tarif asal tidak boleh sama dengan kelas tarif',
					}]
				},{
					xtype: 'fieldset', title: '', border: false, style: 'margin: -23px 20px 0 10px; font-size: 13px',
					items: [{
						xtype: 'label',
						text: 'tujuan',
					}]
				}]
			}] 
		});
				
		function copydata(){
			var cekvaltariflama = Ext.getCmp("cb.klstariflama").getValue();
			var cekvaltarifbaru = Ext.getCmp("cb.klstarifbaru").getValue();
			if (cekvaltariflama != cekvaltarifbaru){
				Ext.Msg.show({
					title: 'Konfirmasi',
					msg: 'Salin Data Tarif..?',
					buttons: Ext.Msg.YESNO,
					icon: Ext.MessageBox.QUESTION,
					fn: function (response) {
						if ('yes' !== response) {
							return;
						}
						Ext.Ajax.request({
						url: BASE_URL + 'tpumum_controller/copy_data',
						params: {
							idklstarif_lama	: Ext.getCmp('cb.klstariflama').getValue(),
							idklstarif_baru	: Ext.getCmp('cb.klstarifbaru').getValue()
						},
						success: function(response){
							//Ext.MessageBox.alert('Informasi', 'Copy Berhasil..');
							Ext.getCmp('gp.grid_tpumum').store.reload();
							win_salin_dta.close();
						},
						failure: function() {
							//Ext.Msg.alert("Informasi", "Copy Data Gagal");
						}
					});
					
					}            
				});	
			}else{
				Ext.Msg.alert("Informasi", "Kelas Tarif tidak boleh sama..");
			}
		}
	
		var win_salin_dta = new Ext.Window({
			title: 'Salin Tarif Pelayanan Umum',
			modal: true, closable: false,
			items: [form_copy]
		}).show();
	}
}