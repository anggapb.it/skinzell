function Mtemobat(){
Ext.form.Field.prototype.msgTarget = 'side';
	var ds_temobat = dm_temobat();
	var ds_mtemobat = dm_mtemobat();
		
	var cbGrid = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			beforerowselect : function (sm, rowIndex, keep, rec) {
				if (this.deselectingFlag && this.grid.enableDragDrop){
					this.deselectingFlag = false;
					this.deselectRow(rowIndex);
					return this.deselectingFlag;
				}
				return keep;
			}
		}
	});
	
	var vw_tempobat = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	var grid_temobat = new Ext.grid.EditorGridPanel({
		id: 'gp.grid_temobat',
		store: ds_temobat,
		view: vw_tempobat,
		sm: cbGrid,
		border: false,
		height: 525,
		columnLines: true,
		autoScroll: true,
		//autoHeight: true,
		//plugins: cari_data,
		//sm: sm_nya,
		clicksToEdit: 1,
		tbar: [{
			text: 'Tambah Barang(Obat/Alkes)',
			id: 'btn_addb',
			iconCls: 'silk-add',
			handler: function(){
				var pen_nmpaket = RH.getCompValue('tf.frm.pen_nmpaket', true);
				if(pen_nmpaket != ''){
					var grid = grid_temobat;
					fnwinBarang(grid);
					Ext.getCmp('cb.frm.mtemobat').disable();
					RH.setCompValue('cb.frm.mtemobat', pen_nmpaket);
				}else if(pen_nmpaket == ''){
					Ext.MessageBox.alert('Message', 'Nama Template Di isi...!');
				}
				return;
			}
		},'-',
		{
			text: 'Hapus',
			id: 'btn_hapus',
			icon : 'application/framework/img/rh_delete.gif',
			handler: function() {
				var m = grid_temobat.getSelectionModel().getSelections();
					if(m.length > 0)
					{				
						Ext.MessageBox.confirm('Message', 'Hapus Data Yang Di Pilih..?' , del);		 
					}
					else
					{
						Ext.MessageBox.alert('Message', 'Data Belum Di Pilih...!');
					}		
			}
		},{
			xtype: 'textfield',
			id: 'tf.idtempobat',
			width: 70,
			emptyText: 'idtempobat',
			hidden: true,
			validator: function() {
				ds_temobat.setBaseParam('idtempobat', Ext.getCmp('tf.idtempobat').getValue());
				Ext.getCmp('gp.grid_temobat').store.reload();
				
			}
		},{
			xtype: 'textfield',
			id: 'tf.frm.iditemobattemp',
			emptyText: 'iditemobattemp',
			hidden: true,
		}],
		columns: [new Ext.grid.RowNumberer(),
		cbGrid,
		{
			header: '<center>Nama Obat</center>',
			width: 476,
			dataIndex: 'nmbrg',
			sortable: true
		},{
			header: 'Qty',
			width: 90,
			dataIndex: 'qtyitem',
			sortable: true,
			align:'right',
			editor: new Ext.form.TextField({
						id: 'tfgp.qtyitem',
                        enableKeyEvents: true,
                        listeners: {
							specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									 fnEditQty();
								}
							}
						}
					})
		}],
		listeners:{
			rowclick: Addrecord
		}
	});
	
	function Addrecord(grid, rowIndex, columnIndex){
		var record = grid.getStore().getAt(rowIndex);
		Ext.getCmp('tf.frm.iditemobattemp').setValue(record.data['iditemobattemp']);
	}
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Template Obat', iconCls:'silk-calendar',
		layout: 'fit',		
		autoScroll: true,
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				tbar: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .99,
						border: false,
						items: [{
							xtype: 'compositefield',
							style: 'padding: 5px; marginLeft: 15px',
							name: 'comp_pen_nmpaket',
							id: 'comp_pen_nmpaket',
							width:550,
							items: [{
								xtype: 'label', id: 'lb.nmpenyakit', text: 'Nama Template Obat :', margins: '3 10 0 5',
							},{
								xtype: 'textfield',
								id: 'tf.frm.pen_nmpaket',
								width: 370,
								emptyText:'Pilih...',
								readOnly: true,
								
							},
							{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn_data_pen_nmpaket',
								width: 3,
								handler: function() {
									parentMasterobat();
								}
							}]
						}]
					}]
				}],
				items: [grid_temobat]
			}]
		}],
		listeners: {
			afterrender: mulai
		}
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	function mulai(){
		Ext.getCmp('gp.grid_temobat').store.reload();
	}
	
	function reloadTemobat(){
		ds_temobat.reload();
	}
	
	function fnAddPenyakit(){
		var grid = grid_temobat;
		wEntryTemobat(false, grid, null);	
	}
	
	function fnEditTemobat(grid, record){
		var record = ds_temobat.getAt(record);
		wEntryTemobat(true, grid, record);		
	}
	
	function fnEditQty(){
        var qty = Ext.getCmp('tfgp.qtyitem').getValue();
        var letters = /^[a-zA-Z]+$/;
		var simbol = /^[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/;
        if(qty.match(letters)){
			alert('Masukan Angka');
            ds_temobat.reload();
        } 
		else if(qty.match(simbol)){
            alert('Masukan Angka');
            ds_temobat.reload();
        } 
        else {           
            fnQty(qty);   
        }      
    }
	
	function fnQty(qty){
		Ext.Ajax.request({
			url: BASE_URL + 'temobat_controller/update_qtyitem',
			params: {
				iditemobattemp	: Ext.getCmp('tf.frm.iditemobattemp').getValue(),
                qtyitem    		: qty
			},
			success: function() {
				//Ext.Msg.alert("Info", "Ubah Berhasil");
				ds_temobat.reload();
			},
			failure: function() {
				Ext.Msg.alert("Info", "Ubah Data Gagal");
			}
		});
	}
	
	function del(btn){
		console.log(btn);
		if(btn == 'yes')
		{			
			var m = grid_temobat.getSelectionModel().getSelections();
			for(var i=0; i< m.length; i++){
				var rec = m[i];
				console.log(rec);
				if(rec){
					console.log(rec.get("iditemobattemp"));
					var iditemobattemp = rec.data['iditemobattemp'];
					Ext.Ajax.request({
						url: BASE_URL +'temobat_controller/delete_temobat',
						method: 'POST',
						params: {
							iditemobattemp 	: iditemobattemp
						},
						success: function(){
							
							Ext.getCmp('gp.grid_temobat').store.reload();
						}
					});
				}
					Ext.MessageBox.alert('Message', 'Hapus Data Berhasil..');
			}
		}
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function parentMasterobat(){		
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var ds_mastertemobat_parent = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'temobat_controller/get_parent_mastertemobat',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtempobat',
				mapping: 'idtempobat'
			},{
				name: 'nmtempobat',
				mapping: 'nmtempobat'
			}]
		});
				
		var cm_masterpaket_parent = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idtempobat',
				width: 30
			},{
				header: 'Nama Template Obat',
				dataIndex: 'nmtempobat',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		var sm_masterpaket_parent = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_masterpaket_parent = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_masterpaket_parent = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_mastertemobat_parent,
			displayInfo: true,
			displayMsg: 'Data Master Paket Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_masterpaket_parent = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200,
			disableIndexes:['idtempobat'],
		})];
		var grid_find_masterpaket_parent = new Ext.grid.GridPanel({
			ds: ds_mastertemobat_parent,
			cm: cm_masterpaket_parent,
			sm: sm_masterpaket_parent,
			view: vw_masterpaket_parent,
			height: 460,
			width: 422,
			plugins: cari_masterpaket_parent,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			autoScroll: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_masterpaket_parent,
			listeners: {
				cellclick: onCellClickaddparent
			}
		});
		var win_find_masterpaket_parent = new Ext.Window({
			title: 'Master Template Obat',
			modal: true,
			items: [grid_find_masterpaket_parent]
		}).show();
		
		function onCellClickaddparent(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_idtempobat_parent = record.data["idtempobat"];
					var var_nmtempobat_parent = record.data["nmtempobat"];
								
					Ext.getCmp('tf.frm.pen_nmpaket').focus()
					Ext.getCmp("tf.idtempobat").setValue(var_idtempobat_parent);
					Ext.getCmp("tf.frm.pen_nmpaket").setValue(var_nmtempobat_parent);
								win_find_masterpaket_parent.close();
				return true;
			}
			return true;
		}
	}
	
	function fnwinBarang(grid){	
		var ds_brgmedis = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'temobat_controller/get_parent_mastertbrg',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			}]
		});
		
		var sm_cbGridBarang = new Ext.grid.CheckboxSelectionModel({
			listeners: {
				rowselect : function( selectionModel, rowIndex, record){
					Ext.Ajax.request({
						url: BASE_URL + 'temobat_controller/cekkdbarang',
						method: 'POST',
						params: {
							idtempobat	: Ext.getCmp('tf.idtempobat').getValue(),
							kdbrg		: record.get("kdbrg"),
						},
						success: function(response){
							pelayanan = response.responseText;
							if (pelayanan =='1') {
								Ext.MessageBox.alert('Message', 'Data Sudah Ada Yang Sama...');
								Ext.getCmp('gp.grid_barang').store.reload();
								
							} else {
								Ext.Ajax.request({
									url: BASE_URL + 'temobat_controller/insert_win_barang',
									params: {
										idtempobat	: Ext.getCmp('tf.idtempobat').getValue(),
										kdbrg		: record.get("kdbrg"),
									},
									success: function(){
										ds_temobat.reload();
									},
									failure: function() {
										//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
									}
								});
							}
						}
					});
				},
				rowdeselect : function( selectionModel, rowIndex, record){
					Ext.Ajax.request({
						url: BASE_URL + 'temobat_controller/delete_win_Barang',
						params: {
							idtempobat	: Ext.getCmp('tf.idtempobat').getValue(),
							kdbrg		: record.get("kdbrg"),
						},
						success: function(){
							ds_temobat.reload();							
						},
						failure: function() {
							//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
						}
					});
				},
				beforerowselect : function (sm, rowIndex, keep, rec) {
					if (this.deselectingFlag && this.grid.enableDragDrop){
						this.deselectingFlag = false;
						this.deselectRow(rowIndex);
						return this.deselectingFlag;
					}
					return keep;
				}
			}
		});
		
		var cm_barang = new Ext.grid.ColumnModel([
			sm_cbGridBarang,
			{
				//header: 'Id',
				dataIndex: 'kdbrg',
				width: 100,
				hidden: true
			},{
				header: 'Nama Barang',
				dataIndex: 'nmbrg',
				width: 400
			}
		]);
		
		var paging_barang = new Ext.PagingToolbar({
			pageSize: 15,
			store: ds_brgmedis,
			displayInfo: true,
			displayMsg: 'Data Barang Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_barang = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var vw_barang = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var grid_barang = new Ext.grid.GridPanel({
			id: 'gp.grid_barang',
			ds: ds_brgmedis,
			cm: cm_barang,
			sm: sm_cbGridBarang, //sm_barang,
			view: vw_barang,
			tbar: [],
			plugins: cari_barang,
			height: 395,
			width: 450,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			autoScroll: true,
			loadMask: true,
			layout: 'anchor',
			style: 'marginTop: 10px',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: paging_barang,
			listeners: {
			}
		});
		
		var form_input_barang = new Ext.FormPanel({
			xtype:'form',
			id: 'frm.barang',
			buttonAlign: 'left',
			labelWidth: 135, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 470, width: 462,
			layout: 'form', 
			frame: false, 
			defaultType:'textfield',
			items: [{
				xtype: 'fieldset',
				columnWidth: .90,
				border: false,
				items: [{
					xtype: 'combo', id: 'cb.frm.mtemobat', 
					fieldLabel: 'Nama Template Obat',
					store: ds_mtemobat, triggerAction: 'all',
					valueField: 'idtempobat', displayField: 'nmtempobat',
					forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', width: 250,
					editable: false,
				}]
			},
				grid_barang
			],
		});
		
		var win_barang = new Ext.Window({
			title: 'Barang (Entry)',
			modal: true,
			//closable: false,
			items: [form_input_barang]
		}).show();
		
		function fnSavebarang(){
			var idForm = 'frm.barang';
			var sUrl = BASE_URL +'temobat_controller/insert_win_pelayanan';
			var sParams = new Object({
				//iditemobattemp	:	RH.getCompValue('tf.frm.iditemobattemp'),
				kdpelayanan		:	RH.getCompValue('tf.kdbrg'),
				idjnstarif		:	RH.getCompValue('tf.jnstarif'),
				idtarifpaket	:	RH.getCompValue('tf.idtempobat'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			//call form grid submit function (common function by RH)
			submitGridForm(idForm, sUrl, sParams, grid, win_barang, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		} 
		
	}
	
	function getForm(idform){
		var form;
		if(Ext.getCmp(idform)){
			var comp = Ext.getCmp(idform);
			if(comp.getForm()){
				return comp.getForm();
			}
		}
	}

	function submitGridForm (idForm, sUrl, sParams, grid, win, msgWait, msgSuccess, msgFail, msgInvalid){
		var form = getForm(idForm);
		if(form.isValid()){
			form.submit({
				url: sUrl,
				method: 'POST',
				params: sParams, 		
				waitMsg: msgWait,				
				success: function(){
					Ext.Msg.alert("Info:", msgSuccess);	
					grid.getStore().reload();
					win.close();
				},
				failure: function(){
					Ext.Msg.alert("Info:", msgFail);
				}
			});
		} else {
			Ext.Msg.alert("Info:", msgInvalid);
		}	
	}
	
}
