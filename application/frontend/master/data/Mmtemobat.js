function Mmtemobat(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var ds_mtemobat = dm_mtemobat();
	var ds_jhirarki = dm_jhirarki();
	var ds_status = dm_status();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_mtemobat,
		displayInfo: true,
		displayMsg: 'Data Master Template Obat Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_mtemobat',
		store: ds_mtemobat,		
		autoScroll: true,
		autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddMtemobat();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Nama Obat',
			width: 300,
			dataIndex: 'nmtempobat',
			sortable: true
		},
		{
			header: 'Jenis Hirarki',
			width: 74,
			dataIndex: 'nmjnshirarki',
			sortable: true
		},{
			header: 'Parent',
			dataIndex: 'nmparent',
			width: 200,
			sortable: true,
			renderer: function(value, p, r){
				var parent = '';
					if(r.data['nmparent'] == 0) parent = '';
					if(r.data['nmparent'] != 0) parent = r.data['nmparent'];
				return parent ;
			}
		},
		{
			header: 'Status',
			width: 100,
			dataIndex: 'nmstatus',
			sortable: true
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditMtemobat(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteMtemobat(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Master Template Obat', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadMtemobat(){
		ds_mtemobat.reload();
	}
	
	function fnAddMtemobat(){
		var grid = grid_nya;
		wEntryMtemobat(false, grid, null);	
	}
	
	function fnEditMtemobat(grid, record){
		var record = ds_mtemobat.getAt(record);
		wEntryMtemobat(true, grid, record);		
	}
	
	function fnDeleteMtemobat(grid, record){
		var record = ds_mtemobat.getAt(record);
		var url = BASE_URL + 'mtemobat_controller/delete_mtemobat';
		var params = new Object({
						idtempobat	: record.data['idtempobat']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryMtemobat(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Master Template Obat (Edit)':'Master Template Obat (Entry)';
		var mtemobat_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.mtemobat',
			buttonAlign: 'left',
			labelWidth: 150, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 200, width: 500,
			layout: 'form', 
			frame: false, 
			defaultType:'textfield',		
			items: [ 
			{
				id: 'tf.frm.idtempobat', 
				hidden: true,
			},
			{
				fieldLabel: 'Nama Template Obat',
				id:'tf.frm.nmtempobat',
				width: 300,
				//allowBlank: false
			},
			{
				xtype: 'combo', id: 'cb.frm.jhirarki', 
				fieldLabel: 'Jenis Hirarki',
				store: ds_jhirarki, triggerAction: 'all',
				valueField: 'idjnshirarki', displayField: 'nmjnshirarki',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},{
				xtype: 'combo', id: 'cb.frm.ststatus', 
				fieldLabel: 'Status',
				store: ds_status, triggerAction: 'all',
				valueField: 'idstatus', displayField: 'nmstatus',
				forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', width: 150,
				editable: false,
				allowBlank: false
			},
			/* {
				id:'tf.frm.tem_idtempobat',
				width: 100,
				sortable: true,
				hidden: true
			} */
			{
				xtype: 'compositefield',
				fieldLabel: 'Parent',
				items: [{
					xtype: 'textfield',
					id: 'tf.frm.tem_idtempobat',				
					fieldLabel: 'Parent',
					width: 270, emptyText:'Pilih...'
				},
				{
					xtype: 'button',
					iconCls: 'silk-find',
					id: 'btn_data_tem_idtempobat',
					width: 3,
					handler: function() {
						parentMtemobat();
					}
				}]
			}],
			buttons: [{
				text: 'Simpan', iconCls:'silk-save',
				handler: function() {
					fnSaveMtemobat();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wMtemobat.close();
				}
			}]
		});
			
		var wMtemobat = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [mtemobat_form]
		});

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setMtemobatForm(isUpdate, record);
		wMtemobat.show();

	/**
	FORM FUNCTIONS
	*/	
		function setMtemobatForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'mtemobat_controller/getNmtempobat',
						params:{
							tem_idtempobat : record.get('tem_idtempobat')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.frm.tem_idtempobat', r);
						}
					});
					
					RH.setCompValue('tf.frm.idtempobat', record.get('idtempobat'));
					RH.setCompValue('tf.frm.nmtempobat', record.get('nmtempobat'));
					RH.setCompValue('cb.frm.jhirarki', record.data['idjnshirarki']);
					RH.setCompValue('cb.frm.ststatus', record.data['idstatus']);
					RH.setCompValue('tf.frm.tem_idtempobat', record.get('tem_idtempobat'));
					return;
				}
			}
		}
		
		function fnSaveMtemobat(){
			var idForm = 'frm.mtemobat';
			var sUrl = BASE_URL +'mtemobat_controller/insert_mtemobat';
			var sParams = new Object({
				idtempobat		:	RH.getCompValue('tf.frm.idtempobat'),
				nmtempobat		:	RH.getCompValue('tf.frm.nmtempobat'),
				idjnshirarki	:	RH.getCompValue('cb.frm.jhirarki'),
				idstatus		:	RH.getCompValue('cb.frm.ststatus'),
				tem_idtempobat	:	RH.getCompValue('tf.frm.tem_idtempobat'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'mtemobat_controller/update_mtemobat';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wMtemobat, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
					
	}

	function parentMtemobat(){
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var ds_mtemobat_parent = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
								url: BASE_URL + 'mtemobat_controller/get_parent_tempobat',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
			fields: [{
				name: 'idtempobat',
				mapping: 'idtempobat'
			},{
				name: 'nmtempobat',
				mapping: 'nmtempobat'
			}]
		});
		var cm_Mtemobat_parent = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idtempobat',
				width: 30
			},{
				header: 'Nama Obat',
				dataIndex: 'nmtempobat',
				width: 370,
				renderer: fnkeyAdd
			}
		]);
		var sm_Mtemobat_parent = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_Mtemobat_parent = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_Mtemobat_parent = new Ext.PagingToolbar({
			pageSize: 50,
			store: ds_mtemobat_parent,
			displayInfo: true,
			displayMsg: 'Data Master Template Obat Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_Mtemobat_parent = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200,
			disableIndexes:['idtempobat'],
		})];
		var grid_find_Mtemobat_parent = new Ext.grid.GridPanel({
			ds: ds_mtemobat_parent,
			cm: cm_Mtemobat_parent,
			sm: sm_Mtemobat_parent,
			view: vw_Mtemobat_parent,
			height: 350,
			width: 400,
			plugins: cari_Mtemobat_parent,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_Mtemobat_parent,
			listeners: {
				cellclick: onCellClickaddparent
			}
		});
		var win_find_Mtemobat_parent = new Ext.Window({
			title: 'Sub Obat',
			modal: true,
			items: [grid_find_Mtemobat_parent]
		}).show();

		function onCellClickaddparent(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_Mtemobat_parent = record.data["nmtempobat"];
						
					Ext.getCmp('tf.frm.tem_idtempobat').focus()
					Ext.getCmp("tf.frm.tem_idtempobat").setValue(var_Mtemobat_parent);
								win_find_Mtemobat_parent.close();
				return true;
			}
			return true;
		}
	}
}
