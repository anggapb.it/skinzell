// auto posting transaksi pasien
function u13_autoposting_pasien(){
  Ext.form.Field.prototype.msgTarget = 'side';
  pageSize = 15;
  
  var form_setting = new Ext.form.FormPanel({
    id: 'fp.form_autuposting',
    title: 'Posting Jurnal Transaksi Pasien', iconCls:'silk-calendar',
    width: 900, Height: 1000,
    layout: {
      type: 'form',
      pack: 'center',
      align: 'center'
    },
    frame: true,
    autoScroll: true,
    items: [{
      xtype: 'fieldset', layout: 'column', style: 'marginTop: 8px',
      defaults: { labelWidth: 150, labelAlign: 'right' },
      items: [{
        xtype : 'compositefield',
        fieldLabel: 'Periode ',
        items : [{
          xtype: 'label',
          text: ' Periode : ',
          style: 'margin-top:4px',
        },{
          xtype: 'datefield',
          id: 'df.tglawal',
          width: 100, 
          value: new Date(), 
          allowBlank: false,
          format: 'd/m/Y',  
        },{
          xtype: 'label',
          text: ' s/d ',
          style: 'margin-top:4px;margin-left:5px;margin-right:5px;',
        },{
          xtype: 'datefield',
          id: 'df.tglakhir',
          width: 100,
          value: new Date(), 
          allowBlank: false,
          format: 'd/m/Y',
        },{
          xtype: 'button',
          text: 'Posting',
          id: 'btn.posting',
          width: 80,
          handler: function() {
            posting_transaksi_pasien();
          }
        }]
      }]
    }]
  });
  SET_PAGE_CONTENT(form_setting);

  function posting_transaksi_pasien(){
    var tglawal  = Ext.getCmp('df.tglawal').getValue().format('Y-m-d'); 
    var tglakhir = Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'); 
    var waitmsg = Ext.MessageBox.wait('Sedang memposting harap tunggu ...', 'Info');
      
      if(tglawal == '' || tglakhir == ''){
        Ext.MessageBox.alert('Informasi','Tanggal belum diisi..');
        return;
      }
      
      Ext.Ajax.request({
        url: BASE_URL + 'automation_controller/posting_transaksi_pasien',
        params: {
          tglawal  : tglawal,
          tglakhir : tglakhir,
        },
        success: function(response){
          waitmsg.hide();
          obj = Ext.util.JSON.decode(response.responseText);
          Ext.MessageBox.alert('Informasi', obj.message);
        }
      });
            
  }
  
}