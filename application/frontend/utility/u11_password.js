function u11_password() {
	               
	var form_back = new Ext.form.FormPanel({
        border: false,
		id: 'form_back',
		labelAlign: 'left',
		buttonAlign: 'right',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
        height: 150,
		width: 325,
        layout: 'form',
        items: []
    });
	
	var form_bp_general = new Ext.form.FormPanel({
        border: false,
		id: 'form_bp_general',
		labelAlign: 'left',
		buttonAlign: 'right',
		bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
		monitorValid: true,
		labelWidth: 155, labelAlign: 'right',
        height: 150,
		width: 475,
        layout: 'form',
        items: [{
								xtype: 'compositefield', 
								fieldLabel: 'Masukkan Password Lama', id: 'idpasslama',
								width: 300, defaultType: 'textfield',
								defaults: { hideLabel: false },
								items: [{
											xtype: 'textfield',
											labelStyle: 'width:160px',
											width:200,
											allowBlank: false,
											inputType: 'password',
											id: 'passlama',
											name: 'passlama'			
										}]
							},
							{
								xtype: 'compositefield',
								fieldLabel: 'Masukkan Password Baru', id: 'idpassbaru',
								width: 300, defaultType: 'textfield',
								defaults: { hideLabel: false },
								items: [{
											xtype: 'textfield',
											labelStyle: 'width:200px',
											width:200,
											allowBlank: false,
											inputType: 'password',
											id: 'passbaru',
											name: 'passbaru'	
										},
										{
											xtype: 'label', margins: '0 0 0 5',
											text: ' min:8, max:100'
										}]
							},
							{
								xtype: 'compositefield',
								fieldLabel: 'Ulangi Pass.Baru', id: 'idulangpassbaru',
								width: 300, defaultType: 'textfield',
								defaults: { hideLabel: false },
								items: [{
											xtype: 'textfield',
											labelStyle: 'width:200px',
											width:200,
											allowBlank: false,
											inputType: 'password',
											id: 'passulang',
											name: 'passulang'		
										},
										{
											xtype: 'label', margins: '0 0 0 5',
											text: ' min:8, max:100'
										}]
				}],
                buttons: [{
                    text: 'Reset',
					iconCls: 'silk-add',
                    handler: function() {
                        reset('form_bp_general');
                    }
                },
				{
                    id:'btn_simpan',
                    text: 'Simpan',
                    iconCls: 'silk-save',
                    handler: function() {
						ubah_password('form_bp_general');
                    }
                }, 
				{
                    text: 'Kembali',
                    handler: function() {
                        win.close();
                    }
                }
          ]
    });
        
    function ubah_password(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		var pbaru = Ext.getCmp('passbaru').getValue();
		var pulang = Ext.getCmp('passulang').getValue();
		
		if (form_nya.getForm().isValid()) {
		
			if (Ext.getCmp('passbaru').getValue() != Ext.getCmp('passulang').getValue()) {
				Ext.MessageBox.alert("Informasi", "Password Baru Tidak Sama");
			}
			else if (pbaru.length < 8 || pulang.length < 8 ) {
				Ext.MessageBox.alert("Informasi", "Password Minimal 8 Karakter");
			} 
			else {
			
				Ext.Ajax.request({
                url: BASE_URL + 'pengguna_controller/ganti_password',
                method: 'POST',
                params: {
					passlama:Ext.getCmp('passlama').getValue(),
					passbaru:Ext.getCmp('passbaru').getValue()
                },
                success: function(response) {
					var r = response.responseText;
					Ext.MessageBox.alert("Informasi", r);
					
					if (r == "Ganti Password Berhasil") {
						reset('form_bp_general');
						win.close();
					}
				},
				failure: function() {
					Ext.MessageBox.alert("Informasi", "Ganti Password Gagal");
				}
				});
			
			}
		} else if (!form_nya.getForm().isValid()) {
			Ext.MessageBox.alert("Informasi", "Lengkapi");
		}
	}
	
	function reset(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().reset();
	}
	
	SET_PAGE_CONTENT(form_back);
	
	var win = new Ext.Window({
			title: 'Ganti Password',
			modal: true,
			items: [form_bp_general],
			resizable: false,
			draggable: false,
		}).show();
    
}