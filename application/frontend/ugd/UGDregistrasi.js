function UGDregistrasi(norm){
	/* var myVar=setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i');
		if(Ext.getCmp("tf.jamshift"))
				Ext.getCmp("tf.jamshift").setValue(formattedValue);
				//RH.setCompValue("tf.jamshift",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	} */
	
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("idshift").setValue(obj.idshift);
			Ext.getCmp("tf.waktushift").setValue(obj.nmshift);
			
			var formattedValue = Ext.util.Format.date(new Date(), 'H:i');
			Ext.getCmp("tf.jamshift").setValue(formattedValue);
		}
	});
	
	Ext.Ajax.request({
		url:BASE_URL + 'vregistrasi_controller/cekRegistrasi',
		method:'POST',
		params: {
			norm		: norm,
			jpel		: 'Unit Gawat Darurat'
		},
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			if(obj.validasi > 0){
				var arr = [];
				var ind = 0;
				Ext.MessageBox.alert('Informasi', 'Pasien tersebut telah melakukan registrasi UGD');
				obj.arr.forEach(function(n) {
					arr[ind] = n;
					ind += 1;
				});
				ds_bagianugd.setBaseParam('val',Ext.encode(arr));
				ds_bagianugd.reload();
			}
		}
	});
	
	Ext.Ajax.request({
		url:BASE_URL + 'pasien_controller/getDataPasien',
		method:'POST',
		params: {
			norm		: norm
		},
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			if(obj.norm != null) obj.norm = parseInt(obj.norm);
			Ext.getCmp('alamat').setValue(obj.alamat);
			Ext.getCmp('notelp').setValue(obj.notelp);
			Ext.getCmp('nohp').setValue(obj.nohp);
			Ext.getCmp('tf.norm').setValue(obj.norm);
			Ext.getCmp('tf.nmpasien').setValue(obj.nmpasien);
			Ext.getCmp('cb.jkelamin').setValue(obj.nmjnskelamin);
			if(obj.tgllahir)umur(new Date(obj.tgllahir));
		}
	});
	
	Ext.Ajax.request({
		url:BASE_URL + 'registrasi_controller/getUpelJadwal',
		method:'POST',
		params: {
			idjnspelayanan		: 3
		},
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('idbagian').setValue(obj.idbagian);
			Ext.getCmp('tf.upelayanan').setValue(obj.nmbagian);
			Ext.getCmp('tf.dokter').setValue(obj.nmdoktergelar);
		}
	});
	
	if(Ext.getCmp('tf.idreservasi').value != null){
		Ext.Ajax.request({
			url:BASE_URL + 'reservasi_controller/getRegres',
			method:'POST',
			params: {
				idreservasi		: Ext.getCmp('tf.idreservasi').value
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.dokter").setValue(obj.nmdoktergelar);
				Ext.getCmp("tf.upelayanan").setValue(obj.nmbagian);
			}
		});
	}
	
	var ds_jkelamin = dm_jkelamin();
	var ds_caradatang = dm_caradatang();
	var ds_klsrawat = dm_klsrawat();
	var ds_kamarbagian = dm_kamarbagian();
	var ds_penjamin = dm_penjamin();
	var ds_hubkeluarga = dm_hubkeluarga();
	var ds_bagianrjriugd = dm_bagianrjriugd();
	var ds_bagianugd = dm_bagian();
	ds_bagianugd.setBaseParam('jpel',3);
	var ds_dokter = dm_dokter();
	var ds_jpelayanan = dm_jpelayanan();
	var ds_jadwalprakteknow = dm_jadwalprakteknow();
	
	var grid_jdwldktr = new Ext.grid.GridPanel({
		store: ds_jadwalprakteknow,
		height: 120,
		width: 300,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_jdwldktr',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				Ext.getCmp("tf.dokter").setValue(record.get('nmdoktergelar'));
            }
        },
		columns: [{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 170,
			sortable: true
		},{
			header: '<div style="text-align:center;">Spesialisasi</div>',
			dataIndex: 'nmspesialisasi',
			width: 105,
			sortable: true
		}]
	});
	
	var registrasiugd_form = new Ext.form.FormPanel({ 
		id: 'fp.registrasiugd',
		title: 'Registasi UGD',
		width: 900, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		tbar: [
			{ text: 'Kembali', iconCls: 'silk-arrow-undo', handler: function(){page_controller('0601');} },'-',
			{ text: 'Simpan', id:'bt.simpan', iconCls: 'silk-save', handler: function(){simpanUGD("fp.registrasiugd");} },'-',
			{ text: 'Batal Registrasi UGD', id:'bt.batal', iconCls: 'silk-cancel', handler: function(){batalUGD("fp.registrasiugd");} },'-',
			{ text: 'Cari Registrasi UGD', iconCls: 'silk-find', handler: function(){cariRegUGD();} },'-',
			{xtype: 'tbfill' }
		],
		defaults: { labelWidth: 150, labelAlign: 'right'},
        items: [{
			xtype: 'fieldset', title: '',
			id:'fs.registrasiugd', layout: 'column', autowidth: true,
			defaults: { labelWidth: 150, labelAlign: 'right' }, 
			items: [{
				//COLUMN 1
				layout: 'form', columnWidth: 0.5,
				items: [{
					xtype: 'textfield',
					id: 'alamat',
					hidden:true
				},{
					xtype: 'textfield',
					id: 'notelp',
					hidden:true
				},{
					xtype: 'textfield',
					id: 'nohp',
					hidden:true
				},{
					xtype: 'textfield', fieldLabel: 'No. RM',
					id: 'tf.norm',
					width: 80, readOnly: true,
					style : 'opacity:0.6'
				},{
					xtype: 'textfield', fieldLabel: 'Nama Pasien',
					id: 'tf.nmpasien',
					width: 300, readOnly: true,
					style : 'opacity:0.6'
				},{
					xtype: 'combo', fieldLabel: 'Jenis Kelamin',
					id: 'cb.jkelamin', width: 150, 
					store: ds_jkelamin, valueField: 'idjnskelamin', displayField: 'nmjnskelamin',
					editable: false, triggerAction: 'all',
					forceSelection: true, submitValue: true, mode: 'local',
					readOnly: true, style : 'opacity:0.6'
				},{
					xtype: 'container', fieldLabel: 'Umur',
					layout: 'hbox',
					items: [{
						xtype: 'textfield', id: 'tf.umurthn',
						width: 30, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'label', id: 'lb.umurthn', text: 'Tahun', margins: '0 10 0 5',
					},{ 	
						xtype: 'textfield', id: 'tf.umurbln', 
						width: 30, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'label', id: 'lb.umurbln', text: 'Bulan', margins: '0 10 0 5',
					},{
						xtype: 'textfield', id: 'tf.umurhari',
						width: 30, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'label', id: 'lb.umurhari', text: 'Hari', margins: '0 10 0 5'
					}]
				},{
					xtype: 'compositefield',
					fieldLabel: 'Penjamin',
					items: [{
						xtype: 'textfield',
						id: 'tf.penjamin', allowBlank: false,
						value:'Pasien Umum', readOnly: true,
						width: 250
					},{
						xtype: 'label', id: 'lb.bintang',
						text: '*', margins: '0 5 0 5',
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.penjamin',
						width: 28,
						handler: function() {
							dftPenjamin();
						}
					}]
				},{
					xtype: 'combo', fieldLabel : 'Cara Datang',
					id: 'cb.caradatang', width: 300, 
					store: ds_caradatang, valueField: 'idcaradatang', displayField: 'nmcaradatang',
					editable: false, triggerAction: 'all', value:'Datang Sendiri',
					forceSelection: true, submitValue: true, mode: 'local',
					emptyText:'Pilih...',
					listeners:{
						select:function(combo, records, eOpts){
							if(records.get('idcaradatang') != 1)
							{
								Ext.getCmp('tf.upengirim').setReadOnly(false);
								Ext.getCmp('tf.upengirim').el.setStyle('opacity', 1);
								Ext.getCmp('btn.upengirim').enable();
								Ext.getCmp('tf.dkirim').setReadOnly(false);
								Ext.getCmp('tf.dkirim').el.setStyle('opacity', 1);
								Ext.getCmp('btn.dkirim').enable();
							} else {
								Ext.getCmp('tf.upengirim').setReadOnly(true);
								Ext.getCmp('tf.upengirim').setValue('');
								Ext.getCmp('tf.upengirim').el.setStyle('opacity', 0.6);
								Ext.getCmp('btn.upengirim').disable();
								Ext.getCmp('tf.dkirim').setReadOnly(true);
								Ext.getCmp('tf.dkirim').setValue('');
								Ext.getCmp('tf.dkirim').el.setStyle('opacity', 0.6);
								Ext.getCmp('btn.dkirim').disable();
							}
						}
					}
				},{
					xtype: 'compositefield',
					fieldLabel: 'Unit Pengirim',
					items: [{
						xtype: 'textfield',
						id: 'tf.upengirim',
						width: 250, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.upengirim',
						width: 45, disabled: true,
						handler: function() {
							dftBagian();
						}
					}]
				},{
					xtype: 'compositefield',
					fieldLabel: 'Dokter Pengirim',
					items: [{
						xtype: 'textfield',
						id: 'tf.dkirim',
						width: 250, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.dkirim',
						width: 45, disabled: true,
						handler: function() {
							dftDokter();
						}
					}]
				},{
					xtype: 'textarea', fieldLabel: 'Catatan Registrasi',
					id  : 'ta.catatan', width : 300
				}]
			}, {
				//COLUMN 2
				layout: 'form', columnWidth: 0.5,
				items: [{
					xtype: 'container', fieldLabel: 'No. Registrasi',
					layout: 'hbox',
					items: [{
						xtype :'textfield',
						id :'tf.noreg',width:120, readOnly: true,
						style : 'opacity:0.6'
					}]
				},{
					xtype: 'container', fieldLabel: 'Tgl./Jam/Shift',
					layout: 'hbox',
					items: [{	
						xtype: 'datefield', id: 'df.tglshift',
						width: 100, value: new Date(),
						format: 'd-m-Y'
					},{
						xtype: 'label', id: 'lb.garing1', text: '/', margins: '0 10 0 10',
					},{ 	
						xtype: 'textfield', id: 'tf.jamshift',
						width: 65
					},{
						xtype: 'label', id: 'lb.garing2', text: '/', margins: '0 10 0 10',
					},{
						xtype: 'textfield', id: 'tf.waktushift', 
						width: 60, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield', id: 'idshift',
						hidden:true
					}]
				},{
					xtype: 'compositefield',
					fieldLabel: 'Unit Pelayanan',
					hidden: true,
					items: [{
						xtype :'textfield',
						id :'idbagian',
						hidden: true, value:'12'
					},{
						xtype :'textfield',
						id :'tf.upelayanan', width:250, readOnly: true,
						hidden: true, value:'UGD'
					}]
				},{
					xtype: 'textfield', id: 'tf.dokter', 
					fieldLabel: 'Dokter Praktek', allowBlank:false,
					width: 300,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							ds_jadwalprakteknow.setBaseParam('cquery', Ext.getCmp('tf.dokter').getValue());
							ds_jadwalprakteknow.reload();
						}
					}
				},{
					xtype: 'container', fieldLabel: ' ',
					items: [grid_jdwldktr]
				},{
					xtype: 'container',
					layout: 'hbox',
					items: [{
						xtype: 'label', id: 'lb.ket',
						html: '<h3 style="font-style:italic;color:red;">Dalam Keadaan Darurat Hubungi :</h3>', margins: '0 0 0 155',
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Nama',
					id  : 'tf.nmkerabat', width : 300
				},{
					xtype: 'textfield', fieldLabel: 'No. Telp./HP',
					id  : 'tf.notelpkerabat', width : 300
				},{
					xtype: 'combo', fieldLabel : 'Hubungan Keluarga',
					id: 'cb.hubkeluarga', width: 300, 
					store: ds_hubkeluarga, valueField: 'idhubkeluarga', displayField: 'nmhubkeluarga',
					editable: false, triggerAction: 'all',
					forceSelection: true, submitValue: true, mode: 'local',
					emptyText:'Pilih...'
				}]
			}]
		}],
		listeners: {
			afterrender: function () {
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/get_tgl_svr',
					method: 'POST',
					params: {
					
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp('df.tglshift').setValue(obj.date);
					},
					failure : function(){
						Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
					}
				});
			}
		}
	}); SET_PAGE_CONTENT(registrasiugd_form);
	
	function umur(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tf.umurthn').setValue(year);
		Ext.getCmp('tf.umurbln').setValue(month);
		Ext.getCmp('tf.umurhari').setValue(day);
	}
	
	function bersihugd() {
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue();
		Ext.getCmp('cb.jkelamin').setValue();
		Ext.getCmp('tf.umurthn').setValue();
		Ext.getCmp('tf.umurbln').setValue();
		Ext.getCmp('tf.umurhari').setValue();
		Ext.getCmp('tf.penjamin').setValue('Pasien Umum');
		Ext.getCmp('cb.caradatang').setValue(1);
		Ext.getCmp('tf.upengirim').setValue();
		Ext.getCmp('tf.dkirim').setValue();
		Ext.getCmp('tf.noreg').setValue(); 
		//Ext.getCmp('df.tglshift').setValue(new Date());
		Ext.getCmp('tf.upelayanan').setValue(' ');
		Ext.getCmp('tf.dokter').setValue(' ');
		Ext.getCmp('ta.catatan').setValue();
		Ext.getCmp('tf.upengirim').disable();
		Ext.getCmp('btn.upengirim').disable();
		Ext.getCmp('tf.dkirim').disable();
		Ext.getCmp('btn.dkirim').disable();
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/get_tgl_svr',
			method: 'POST',
			params: {
			
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp('df.tglshift').setValue(obj.date);
			},
			failure : function(){
				Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
			}
		});
	}
	
	function check(e) {
		var cek = e.checked;
	}

	function simpanUGD(namaForm) {
		var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'registrasi_controller/insorupd_registrasi',
			method: 'POST',
			params: {
				ureg 	: 'UG',
			},
			success: function(form, o) {
				if(o.result.success == true) {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
					Ext.getCmp('tf.noreg').setValue(o.result.noreg);
					Ext.getCmp('bt.simpan').disable();
					//myStopFunction();
				} else {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			},
			failure: function (form, action) {
				waitmsg.hide();
				Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
			}
		});
	}
	
	function batalUGD(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'registrasi_controller/batal_registrasi',
			method: 'POST',
			success: function(registrasiugd_form, o) {
				if (o.result.success == true) {
					Ext.MessageBox.alert('Informasi', 'Registrasi Berhasil Dibatalkan');
					bersihugd();
				} else if (o.result.success == 'false') {
					Ext.MessageBox.alert('Informasi', 'Registrasi Gagal Dibatalkan');
				}
			}
		});
	}
	
	function cariRegUGD(){
		var ds_vregistrasi = dm_vregistrasi();
		ds_vregistrasi.setBaseParam('cek','UG');
		ds_vregistrasi.setBaseParam('groupby','noreg');
		ds_vregistrasi.setBaseParam('nokuitansi',2);
		function keyToView_reg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterReg" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_cari_regugd = new Ext.grid.ColumnModel([
			{
				header: 'No. Registrasi',
				dataIndex: 'noreg',
				width: 80,
				renderer: keyToView_reg
			},{
				header: 'Tgl. Registrasi',
				dataIndex: 'tglreg',
				renderer: Ext.util.Format.dateRenderer('d-M-Y'),
				width: 100
			},{
				header: 'No. RM',
				dataIndex: 'norm',
				width: 80
			},{
				header: 'Nama Pasien',
				dataIndex: 'nmpasien',
				width: 150
			},{
				header: 'Unit Pelayanan',
				dataIndex: 'nmbagian',
				width: 130
			},{
				header: 'Dokter',
				dataIndex: 'nmdoktergelar',
				width: 180
			},{
				header: 'No. Antrian',
				dataIndex: 'noantrian',
				width: 70
			},{
				header: 'Cara Datang',
				dataIndex: 'nmcaradatang',
				width: 130
			}
		]);
		var sm_cari_regugd = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_regugd = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_regugd = new Ext.PagingToolbar({
			store: ds_vregistrasi,
			pageSize:18,
			displayInfo: true,
			displayMsg: 'Data Registrasi Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var grid_find_cari_regugd= new Ext.grid.GridPanel({
			id: 'gp.find_regugd',
			ds: ds_vregistrasi,
			cm: cm_cari_regugd,
			sm: sm_cari_regugd,
			view: vw_cari_regugd,
			height: 300,
			width: 955,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_regugd,
			listeners: {
				cellclick: klik_cari_regugd
			}
		});
		var win_find_cari_regugd = new Ext.Window({
			title: 'Cari Registrasi',
			modal: true,
			items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'button',
						text: 'Cari',
						id: 'btn.cari',
						iconCls:'silk-find',
						style: 'padding: 10px',
						width: 100,
						handler: function() {
							cAdvance();
						}
					},{
						xtype: 'button',
						text: 'Kembali',
						id: 'btn.kembali',
						iconCls:'silk-arrow-undo',
						style: 'padding: 10px',
						width: 110,
						handler: function() {
							win_find_cari_regugd.close();
						}
					}]
				},{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnmibu',
							items:[{
								xtype: 'checkbox',
								id:'chb.cupel',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('cb.cupelayanan').enable();
											Ext.getCmp('cb.cupelayanan').focus();
										} else if(val == false){
											Ext.getCmp('cb.cupelayanan').disable();
											Ext.getCmp('cb.cupelayanan').setValue('');
										}
									}
								}
							},{
								xtype: 'combo',
								id: 'cb.cupelayanan', width: 230, 
								store: ds_bagianugd, valueField: 'idbagian', displayField: 'nmbagian',
								editable: false, triggerAction: 'all',
								forceSelection: true, submitValue: true, mode: 'local',
								emptyText:'Unit Pelayanan', disabled: true
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_cdokter',
							items:[{
								xtype: 'checkbox',
								id:'chb.cdokter',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('cb.cdokter').enable();
											Ext.getCmp('cb.cdokter').focus();
										} else if(val == false){
											Ext.getCmp('cb.cdokter').disable();
											Ext.getCmp('cb.cdokter').setValue('');
										}
									}
								}
							},{
								xtype: 'combo',
								id: 'cb.cdokter', width: 230, 
								store: ds_dokter, valueField: 'iddokter', displayField: 'nmdoktergelar',
								editable: false, triggerAction: 'all',
								forceSelection: true, submitValue: true, mode: 'local',
								emptyText:'Dokter', disabled: true
							}]
						}]
					},{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnorm',
							items:[{
								xtype: 'checkbox',
								id:'chb.crm',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.crm').enable();
											Ext.getCmp('tf.crm').focus();
										} else if(val == false){
											Ext.getCmp('tf.crm').disable();
											Ext.getCmp('tf.crm').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.crm',
								emptyText:'No. RM',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_cnmpasien',
							items:[{
								xtype: 'checkbox',
								id:'chb.cnmpasien',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.cnmpasien').enable();
											Ext.getCmp('tf.cnmpasien').focus();
										} else if(val == false){
											Ext.getCmp('tf.cnmpasien').disable();
											Ext.getCmp('tf.cnmpasien').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.cnmpasien',
								emptyText:'Nama Pasien',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						}]
					},{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnoreg',
							items:[{
								xtype: 'checkbox',
								id:'chb.cnoreg',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.cnoreg').enable();
											Ext.getCmp('tf.cnoreg').focus();
										} else if(val == false){
											Ext.getCmp('tf.cnoreg').disable();
											Ext.getCmp('tf.cnoreg').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.cnoreg',
								emptyText:'No. Registrasi',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_ctglreg',
							items:[{
								xtype: 'checkbox',
								id:'chb.ctglreg',
								checked   : true,
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('df.ctglreg').enable();
											Ext.getCmp('df.ctglreg').focus();
										} else if(val == false){
											Ext.getCmp('df.ctglreg').disable();
											Ext.getCmp('df.ctglreg').setValue(new Date());
										}
									}
								}
							},{
								xtype: 'label', id: 'lb.ctglreg', text: 'Tgl. Reg', margins: '5 5 0 0',
							},{
								xtype: 'datefield',
								id: 'df.ctglreg',
								width: 100, value: new Date(),
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						}]
					}]
				},
				grid_find_cari_regugd
			]
		}).show();
		ds_vregistrasi.setBaseParam('ctglreg',Ext.util.Format.date(new Date(), 'Y-m-d'));
		ds_vregistrasi.reload();
		
		function klik_cari_regugd(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterReg'){
				var rec_cari_regugd = ds_vregistrasi.getAt(rowIdx);
				var regugd_noreg = rec_cari_regugd.data["noreg"];
				var regugd_norm = rec_cari_regugd.data["norm"];
				var regugd_nmpasien = rec_cari_regugd.data["nmpasien"];
				var regugd_jkelamin = rec_cari_regugd.data["idjnskelamin"];
				var regugd_umurtahun = rec_cari_regugd.data["umurtahun"];
				var regugd_umurbulan = rec_cari_regugd.data["umurbulan"];
				var regugd_umurhari = rec_cari_regugd.data["umurhari"];
				var regugd_penjamin = rec_cari_regugd.data["nmpenjamin"];
				var regugd_idcaradatang = rec_cari_regugd.data["idcaradatang"];
				var regugd_nmbagiankirim = rec_cari_regugd.data["nmbagiankirim"];
				var regugd_idjnspelayanan = rec_cari_regugd.data["idjnspelayanan"];
				var regugd_ruangan = rec_cari_regugd.data["nmbagian"];
				var regugd_dokter = rec_cari_regugd.data["nmdoktergelar"];
				var regugd_catatan = rec_cari_regugd.data["catatanr"];
				var regugd_noantrian = rec_cari_regugd.data["noantrian"];
				
				if(rec_cari_regugd.data["nmdokterkirimdlm"]){
					var regugd_dokterkirim = rec_cari_regugd.data["nmdokterkirimdlm"];
				} else {
					var regugd_dokterkirim = rec_cari_regugd.data["nmdokterkirim"];
				}
				
				Ext.getCmp("tf.noreg").setValue(regugd_noreg);
				Ext.getCmp("tf.norm").setValue(regugd_norm);
				Ext.getCmp("tf.nmpasien").setValue(regugd_nmpasien);
				Ext.getCmp("cb.jkelamin").setValue(regugd_jkelamin);
				Ext.getCmp("tf.umurthn").setValue(regugd_umurtahun);
				Ext.getCmp("tf.umurbln").setValue(regugd_umurbulan);
				Ext.getCmp("tf.umurhari").setValue(regugd_umurhari);
				Ext.getCmp("tf.penjamin").setValue(regugd_penjamin);
				Ext.getCmp("cb.caradatang").setValue(regugd_idcaradatang);
				Ext.getCmp("tf.upengirim").setValue(regugd_nmbagiankirim);
				Ext.getCmp("tf.dkirim").setValue(regugd_dokterkirim);
				Ext.getCmp("tf.upelayanan").setValue(regugd_ruangan);
				Ext.getCmp("tf.dokter").setValue(regugd_dokter);
				Ext.getCmp("ta.catatan").setValue(regugd_catatan);
				
				if(regugd_idcaradatang != 1)
				{
					Ext.getCmp('tf.upengirim').enable();
					Ext.getCmp('tf.upengirim').setReadOnly(false);
					Ext.getCmp('tf.upengirim').el.setStyle('opacity', 1);
					Ext.getCmp('btn.upengirim').enable();
					Ext.getCmp('tf.dkirim').setReadOnly(false);
					Ext.getCmp('tf.dkirim').el.setStyle('opacity', 1);
					Ext.getCmp('btn.dkirim').enable();
				} else {
					Ext.getCmp('tf.upengirim').setReadOnly(true);
					Ext.getCmp('tf.upengirim').el.setStyle('opacity', 0.6);
					Ext.getCmp('btn.upengirim').disable();
					Ext.getCmp('tf.dkirim').setReadOnly(true);
					Ext.getCmp('tf.dkirim').el.setStyle('opacity', 0.6);
					Ext.getCmp('btn.dkirim').disable();
				}
							win_find_cari_regugd.close();
			}
		}
		
		function cAdvance(){
			if(Ext.getCmp('chb.ctglreg').getValue() == true){
				ds_vregistrasi.setBaseParam('ctglreg',Ext.util.Format.date(Ext.getCmp('df.ctglreg').getValue(), 'Y-m-d'));
			} else {
				ds_vregistrasi.setBaseParam('ctglreg','');
			}
		
			ds_vregistrasi.setBaseParam('cnmjnspelayanan',Ext.getCmp('cb.cupelayanan').getValue());
			ds_vregistrasi.setBaseParam('cdokter',Ext.getCmp('cb.cdokter').getValue());
			ds_vregistrasi.setBaseParam('cnorm',Ext.getCmp('tf.crm').getValue());
			ds_vregistrasi.setBaseParam('cnmpasien',Ext.getCmp('tf.cnmpasien').getValue());
			ds_vregistrasi.setBaseParam('cnoreg',Ext.getCmp('tf.cnoreg').getValue());
			ds_vregistrasi.reload();
		}
	}
	
	function dftPenjamin(){
		function keyToView_penjamin(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterPenjamin" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_penjamin = new Ext.grid.ColumnModel([
			{
				header: 'Nama Penjamin',
				dataIndex: 'nmpenjamin',
				width: 200,
				renderer: keyToView_penjamin
			},{
				header: 'Alamat',
				dataIndex: 'alamat',
				width: 300
			},{
				header: 'No. Telp',
				dataIndex: 'notelp',
				width: 150
			}
		]);
		var sm_cari_penjamin = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_penjamin = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_penjamin = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_penjamin,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_cari_penjamin = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_cari_penjamin= new Ext.grid.GridPanel({
			ds: ds_penjamin,
			cm: cm_cari_penjamin,
			sm: sm_cari_penjamin,
			view: vw_cari_penjamin,
			height: 460,
			width: 680,
			plugins: cari_cari_penjamin,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_penjamin,
			listeners: {
				cellclick: klik_cari_penjamin
			}
		});
		var win_find_cari_penjamin = new Ext.Window({
			title: 'Cari Penjamin',
			modal: true,
			items: [grid_find_cari_penjamin]
		}).show();

		function klik_cari_penjamin(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterPenjamin'){
				var rec_cari_penjamin = ds_penjamin.getAt(rowIdx);
				var var_cari_nmpenjamin = rec_cari_penjamin.data["nmpenjamin"];
				
				Ext.getCmp("tf.penjamin").setValue(var_cari_nmpenjamin);
							win_find_cari_penjamin.close();
			}
		}
	}
	
	function dftBagian(){
		function keyToView_bagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterBagian" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_bagian = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idbagian'
			},{
				header: 'Kode',
				dataIndex: 'kdbagian',
				width: 100,
				renderer: keyToView_bagian
			},{
				header: 'Unit/Ruangan Pelayanan',
				dataIndex: 'nmbagian',
				width: 245
			}
		]);
		var sm_cari_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagianrjriugd
		});
		var cari_cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_cari_bagian= new Ext.grid.GridPanel({
			ds: ds_bagianrjriugd,
			cm: cm_cari_bagian,
			sm: sm_cari_bagian,
			view: vw_cari_bagian,
			height: 460,
			width: 350,
			plugins: cari_cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_bagian,
			listeners: {
				cellclick: klik_cari_bagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function klik_cari_bagian(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterBagian'){
				var rec_cari_bagian = ds_bagianrjriugd.getAt(rowIdx);
				var var_cari_nmbagian = rec_cari_bagian.data["nmbagian"];
				
				Ext.getCmp("tf.upengirim").setValue(var_cari_nmbagian);
							win_find_cari_bagian.close();
			}
		}
	}
	
	function dftDokter(){
		function keyToView_dokter(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDokter" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_dokter = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'iddokter'
			},{
				header: 'Kode',
				dataIndex: 'kddokter',
				width: 50,
				renderer: keyToView_dokter
			},{
				header: 'Nama Dokter',
				dataIndex: 'nmdoktergelar',
				width: 200
			},{
				header: 'Spesialisasi',
				dataIndex: 'nmspesialisasi',
				width: 155
			}
		]);
		var sm_cari_dokter = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_dokter = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_dokter = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_dokter,
			displayInfo: true,
			displayMsg: 'Data Dokter Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_cari_dokter = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_cari_dokter= new Ext.grid.GridPanel({
			ds: ds_dokter,
			cm: cm_cari_dokter,
			sm: sm_cari_dokter,
			view: vw_cari_dokter,
			height: 460,
			width: 410,
			plugins: cari_cari_dokter,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_dokter,
			listeners: {
				cellclick: klik_cari_dokter
			}
		});
		var win_find_cari_dokter = new Ext.Window({
			title: 'Cari Dokter',
			modal: true,
			items: [grid_find_cari_dokter]
		}).show();

		function klik_cari_dokter(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterDokter'){
				var rec_cari_dokter = ds_dokter.getAt(rowIdx);
				var var_cari_nmdoktergelar = rec_cari_dokter.data["nmdoktergelar"];
				
				Ext.getCmp("tf.dkirim").setValue(var_cari_nmdoktergelar);
							win_find_cari_dokter.close();
			}
		}
	}
	
}