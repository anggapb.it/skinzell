// Utility
//=======================================================
	function dm_kelompoksetting(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'kelompoksetting_controller/get_klpsetting', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idklpsetting',
				mapping: 'idklpsetting'
			},{
				name: 'kdklpsetting',
				mapping: 'kdklpsetting'
			},
			{
				name: 'nmklpsetting',
				mapping: 'nmklpsetting'
			}]
		});
		return master;
	}

	function dm_penjualanbrg(){
	var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'laporan_penjualanbrg/get_data',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'modal',
				mapping: 'modal'
			},{
				name: 'penjualan',
				mapping: 'penjualan'
			},{
				name: 'persentase_margin',
				mapping: 'persentase_margin'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'noref',
				mapping: 'noref'
			},{
				name: 'tglkartustok',
				mapping: 'tglkartustok'
			},{
				name: 'jamkartustok',
				mapping: 'jamkartustok'
			},{
				name: 'saldoawal',
				mapping: 'saldoawal'
			},{
				name: 'jmlmasuk',
				mapping: 'jmlmasuk'
			},{
				name: 'rbm',
				mapping: 'rbm'
			},{
				name: 'jmlkeluar',
				mapping: 'jmlkeluar'
			},{
				name: 'rbk',
				mapping: 'rbk'
			},{
				name: 'saldoakhir',
				mapping: 'saldoakhir'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'nbeli',
				mapping: 'nbeli'
			},{
				name: 'njual',
				mapping: 'njual'
			}]
		});
		return master;

}
	
//=======================================================
	function dm_klpsetting_setting(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'setting_controller/get_klpsetting_setting', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idklpsetting',
				mapping: 'idklpsetting'
			},{
				name: 'kdklpsetting',
				mapping: 'kdklpsetting'
			},
			{
				name: 'nmklpsetting',
				mapping: 'nmklpsetting'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stkontrabon(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'stkontrabon_controller/get_stkontrabon', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstkontrabon',
				mapping: 'idstkontrabon'
			},{
				name: 'kdstkontrabon',
				mapping: 'kdstkontrabon'
			},
			{
				name: 'nmstkontrabon',
				mapping: 'nmstkontrabon'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jtenagamedis(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jtenagamedis_controller/get_jtenagamedis', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnstenagamedis',
				mapping: 'idjnstenagamedis'
			},{
				name: 'kdjnstenagamedis',
				mapping: 'kdjnstenagamedis'
			},
			{
				name: 'nmjnstenagamedis',
				mapping: 'nmjnstenagamedis'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_setting(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setting_controller/get_setting',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idset',
				mapping: 'idset'
			},{
				name: 'idklpsetting',
				mapping: 'idklpsetting'
			},{
				name: 'nmklpsetting',
				mapping: 'nmklpsetting'
			},{
				name: 'kdset',
				mapping: 'kdset'
			},{
				name: 'nmset',
				mapping: 'nmset'
			},{
				name: 'ketset',
				mapping: 'ketset'
			},{
				name: 'nilai',
				mapping: 'nilai'
			},{
				name: 'nourut',
				mapping: 'nourut'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_status(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'status_controller/get_status',
				method: 'POST'
			}),
			autoLoad: true,
			root: 'data',
			fields: [
				{ name: "idstatus", mapping: "idstatus" },
				{ name: "nmstatus", mapping: "nmstatus" }
			]
		});
		return master;
	}

// Master Kelompok
//=======================================================
	function dm_hari(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'hari_controller/get_hari', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idhari',
				mapping: 'idhari'
			},{
				name: 'kdhari',
				mapping: 'kdhari'
			},
			{
				name: 'nmhari',
				mapping: 'nmhari'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jkasus(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jkasus_controller/get_jkasus', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnskasus',
				mapping: 'idjnskasus'
			},{
				name: 'kdjnskasus',
				mapping: 'kdjnskasus'
			},
			{
				name: 'nmjnskasus',
				mapping: 'nmjnskasus'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_cb_jkasus(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jkasus_controller/get_cb_jkasus', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnskasus',
				mapping: 'idjnskasus'
			},{
				name: 'kdjnskasus',
				mapping: 'kdjnskasus'
			},
			{
				name: 'nmjnskasus',
				mapping: 'nmjnskasus'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jkartustok(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jkartustok_controller/get_jkartustok', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnskartustok',
				mapping: 'idjnskartustok'
			},{
				name: 'kdjnskartustok',
				mapping: 'kdjnskartustok'
			},
			{
				name: 'nmjnskartustok',
				mapping: 'nmjnskartustok'
			}]
		});
		return master;
	}

//=======================================================
	function dm_kembar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'kembar_controller/get_kembar', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkembar',
				mapping: 'idkembar'
			},{
				name: 'kdkembar',
				mapping: 'kdkembar'
			},
			{
				name: 'nmkembar',
				mapping: 'nmkembar'
			}]
		});
		return master;
	}

//=======================================================
	function dm_jdiagnosa(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jdiagnosa_controller/get_jdiagnosa', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnsdiagnosa',
				mapping: 'idjnsdiagnosa'
			},{
				name: 'kdjnsdiagnosa',
				mapping: 'kdjnsdiagnosa'
			},
			{
				name: 'nmjnsdiagnosa',
				mapping: 'nmjnsdiagnosa'
			}]
		});
		return master;
	}
	
//=======================================================
function dm_statusKartuRM(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'status_controller/getStatusKartuRM',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
					name: 'idstkrm',
					mapping: 'idstkrm'
				},{
					name: 'nmstkrm',
					mapping: 'nmstkrm'
				}]
		});
		return master;
	}

//=======================================================
	function dm_shift(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'shift_controller/get_shift', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'kdshift',
				mapping: 'kdshift'
			},
			{
				name: 'nmshift',
				mapping: 'nmshift'
			},
			{
				name: 'darijam',
				mapping: 'darijam'
			},
			{
				name: 'sampaijam',
				mapping: 'sampaijam'
			}]
		});
		return master;
	}
	
//======================================================
	function dm_lvlbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'lvlbagian_controller/get_lvlbagian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idlvlbagian',
				mapping: 'idlvlbagian'
			},{
				name: 'kdlvlbagian',
				mapping: 'kdlvlbagian'
			},
			{
				name: 'nmlvlbagian',
				mapping: 'nmlvlbagian'
			}]
		});
		return master;
	}
	
//======================================================
	function dm_bdgrawat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bdgperawatan_controller/get_bdgperawatan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbdgrawat',
				mapping: 'idbdgrawat'
			},{
				name: 'kdbdgrawat',
				mapping: 'kdbdgrawat'
			},
			{
				name: 'nmbdgrawat',
				mapping: 'nmbdgrawat'
			}]
		});
		return master;
	}

//======================================================
	function dm_spdokter(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'spdokter_controller/get_spdokter',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idspesialisasi',
				mapping: 'idspesialisasi'
			},{
				name: 'kdspesialisasi',
				mapping: 'kdspesialisasi'
			},
			{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			}]
		});
		return master;
	}
	
//======================================================
	function dm_stdokter(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stsdokter_controller/get_stdokter',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstdokter',
				mapping: 'idstdokter'
			},{
				name: 'kdstdokter',
				mapping: 'kdstdokter'
			},
			{
				name: 'nmstdokter',
				mapping: 'nmstdokter'
			}]
		});
		return master;
	}
	
//======================================================
	function dm_jampraktek(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jampraktek_controller/get_jampraktek', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjampraktek',
				mapping: 'idjampraktek'
			},{
				name: 'kdjampraktek',
				mapping: 'kdjampraktek'
			},
			{
				name: 'nmjampraktek',
				mapping: 'nmjampraktek'
			}]
		});
		return master;
	}

//=======================================================
	function dm_klstarif(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'klstarif_controller/get_klstarif',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'kdklstarif',
				mapping: 'kdklstarif'
			},
			{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			}]
		});
		return master;
	}
	
//=======================================================	
	function dm_klsrawat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'klsperawatan_controller/get_klsperawatan', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idklsrawat',
				mapping: 'idklsrawat'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'kdklsrawat',
				mapping: 'kdklsrawat'
			},{
				name: 'nmklsrawat',
				mapping: 'nmklsrawat'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_bagianklsperawatan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'klsperawatan_controller/get_bagianklsperawatan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idbdgrawat',
				mapping: 'idbdgrawat'
			},{
				name: 'nmbdgrawat',
				mapping: 'nmbdgrawat'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jpenjamin(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jpenjamin_controller/get_jpenjamin', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnspenjamin',
				mapping: 'idjnspenjamin'
			},{
				name: 'kdjnspenjamin',
				mapping: 'kdjnspenjamin'
			},
			{
				name: 'nmjnspenjamin',
				mapping: 'nmjnspenjamin'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_hubkeluarga(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'hubkeluarga_controller/get_hubkeluarga', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idhubkeluarga',
				mapping: 'idhubkeluarga'
			},{
				name: 'kdhubkeluarga',
				mapping: 'kdhubkeluarga'
			},
			{
				name: 'nmhubkeluarga',
				mapping: 'nmhubkeluarga'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pekerjaan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'pekerjaan_controller/get_pekerjaan', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpekerjaan',
				mapping: 'idpekerjaan'
			},{
				name: 'kdpekerjaan',
				mapping: 'kdpekerjaan'
			},
			{
				name: 'nmpekerjaan',
				mapping: 'nmpekerjaan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pendidikan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'pendidikan_controller/get_pendidikan', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpendidikan',
				mapping: 'idpendidikan'
			},{
				name: 'kdpendidikan',
				mapping: 'kdpendidikan'
			},
			{
				name: 'nmpendidikan',
				mapping: 'nmpendidikan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_agama(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'agama_controller/get_agama', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idagama',
				mapping: 'idagama'
			},{
				name: 'kdagama',
				mapping: 'kdagama'
			},
			{
				name: 'nmagama',
				mapping: 'nmagama'
			}]
		});
		return master;
	}
	
//=======================================================
        function dm_jkas(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jkas_controller/get_jkas', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnskas',
				mapping: 'idjnskas'
			},{
				name: 'kdjnskas',
				mapping: 'kdjnskas'
			},
			{
				name: 'nmjnskas',
				mapping: 'nmjnskas'
			}]
		});
		return master;
	}
//=======================================================
        function dm_jkuitansi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jkuitansi_controller/get_jkuitansi', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnskuitansi',
				mapping: 'idjnskuitansi'
			},{
				name: 'kdjnskuitansi',
				mapping: 'kdjnskuitansi'
			},{
				name: 'nmjnskuitansi',
				mapping: 'nmjnskuitansi'
			},{
				name: 'idjnskas',
				mapping: 'idjnskas'
			},{
				name: 'nmjnskas',
				mapping: 'nmjnskas'
			}]
		});
		return master;
	}
//=======================================================
	function dm_sukubangsa(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'sukubangsa_controller/get_sukubangsa', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idsukubangsa',
				mapping: 'idsukubangsa'
			},{
				name: 'kdsukubangsa',
				mapping: 'kdsukubangsa'
			},
			{
				name: 'nmsukubangsa',
				mapping: 'nmsukubangsa'
			}]
		});
		return master;
	}

//=======================================================
	function dm_caradatang(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'caradatang_controller/get_caradatang', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idcaradatang',
				mapping: 'idcaradatang'
			},{
				name: 'kdcaradatang',
				mapping: 'kdcaradatang'
			},
			{
				name: 'nmcaradatang',
				mapping: 'nmcaradatang'
			}]
		});
		return master;
	}

	function dm_akunklpbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setakunklpbrg_controller/get_akunklpbrg',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tahun',
				mapping: 'tahun'
			},{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'idklpbrg',
				mapping: 'idklpbrg'
			},{
				name: 'kdakunbeli',
				mapping: 'kdakunbeli'
			},{
				name: 'nmakunbeli',
				mapping: 'nmakunbeli'
			},{
				name: 'akunbeli',
				mapping: 'akunbeli'
			},{
				name: 'kdakunjual',
				mapping: 'kdakunjual'
			},{
				name: 'nmakunjual',
				mapping: 'nmakunjual'
			},{
				name: 'akunjual',
				mapping: 'akunjual'
			},{
				name: 'nmklpbarang',
				mapping: 'nmklpbarang'
			}]
		});
		return master;
	}

//=======================================================
	function dm_klpbarang(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'klpbarang_controller/get_klpbarang', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idklpbrg',
				mapping: 'idklpbrg'
			},{
				name: 'kdklpbarang',
				mapping: 'kdklpbarang'
			},
			{
				name: 'nmklpbarang',
				mapping: 'nmklpbarang'
			}]
		});
		return master;
	}
     
	function dm_kelharta(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'kelharta_controller/get_kelharta', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkelharta',
				mapping: 'idkelharta'
			},{
				name: 'kdkelharta',
				mapping: 'kdkelharta'
			},{
				name: 'jenisharta',
				mapping: 'jenisharta'
			},{
				name: 'nmkelharta',
				mapping: 'nmkelharta'
			},{
				name: 'akunharta',
				mapping: 'akunharta'
			},{
				name: 'akunakumulasipenyusutan',
				mapping: 'akunakumulasipenyusutan'
			},{
				name: 'akunbiayapenyusutan',
				mapping: 'akunbiayapenyusutan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_jnsbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jbarang_controller/get_jbarang', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'kdjnsbrg',
				mapping: 'kdjnsbrg'
			},
			{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_dosis(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'dosis_controller/get_dosis', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddosis',
				mapping: 'iddosis'
			},{
				name: 'kddosis',
				mapping: 'kddosis'
			},
			{
				name: 'nmdosis',
				mapping: 'nmdosis'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_aturanpakai(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'aturanpakai_controller/get_aturanpakai', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idaturanpakai',
				mapping: 'idaturanpakai'
			},{
				name: 'kdaturanpakai',
				mapping: 'kdaturanpakai'
			},
			{
				name: 'nmaturanpakai',
				mapping: 'nmaturanpakai'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_goldarah(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'goldarah_controller/get_goldarah', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idgoldarah',
				mapping: 'idgoldarah'
			},{
				name: 'kdgoldarah',
				mapping: 'kdgoldarah'
			},
			{
				name: 'nmgoldarah',
				mapping: 'nmgoldarah'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stkawin(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'stkawin_controller/get_stkawin', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstkawin',
				mapping: 'idstkawin'
			},{
				name: 'kdstkawin',
				mapping: 'kdstkawin'
			},
			{
				name: 'nmstkawin',
				mapping: 'nmstkawin'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jidentitas(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jidentitas_controller/get_jnsidentitas', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnsidentitas',
				mapping: 'idjnsidentitas'
			},{
				name: 'kdjnsidentitas',
				mapping: 'kdjnsidentitas'
			},
			{
				name: 'nmjnsidentitas',
				mapping: 'nmjnsidentitas'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stpasien(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'stspasien_controller/get_stpasien', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstpasien',
				mapping: 'idstpasien'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'kdstpasien',
				mapping: 'kdstpasien'
			},{
				name: 'nmstpasien',
				mapping: 'nmstpasien'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jkelamin(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jkelamin_controller/get_jnskelamin', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},
			{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jpelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jpelayanan_controller/get_jpelayanan', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'kdjnspelayanan',
				mapping: 'kdjnspelayanan'
			},
			{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stpelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'stpelayanan_controller/get_stpelayanan', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstpelayanan',
				mapping: 'idstpelayanan'
			},{
				name: 'kdstpelayanan',
				mapping: 'kdstpelayanan'
			},
			{
				name: 'nmstpelayanan',
				mapping: 'nmstpelayanan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_lvldaerah(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'lvldaerah_controller/get_lvldaerah',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idlvldaerah',
				mapping: 'idlvldaerah'
			},{
				name: 'kdlvldaerah',
				mapping: 'kdlvldaerah'
			},
			{
				name: 'nmlvldaerah',
				mapping: 'nmlvldaerah'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jsatuan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jenissatuannew_controller/get_jsatuan', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'kdsatuan',
				mapping: 'kdsatuan'
			},
			{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			}]
		});
		return master;
	}
	

//=======================================================
	function dm_jpenyakit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jpenyakit_controller/get_jpenyakit', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnspenyakit',
				mapping: 'idjnspenyakit'
			},{
				name: 'kdjnspenyakit',
				mapping: 'kdjnspenyakit'
			},
			{
				name: 'nmjnspenyakit',
				mapping: 'nmjnspenyakit'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_kebangsaan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'kebangsaan_controller/get_kebangsaan', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkebangsaan',
				mapping: 'idkebangsaan'
			},{
				name: 'kdkebangsaan',
				mapping: 'kdkebangsaan'
			},
			{
				name: 'nmkebangsaan',
				mapping: 'nmkebangsaan'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_jorder(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jorder_controller/get_jorder', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnsorder',
				mapping: 'idjnsorder'
			},{
				name: 'kdjnsorder',
				mapping: 'kdjnsorder'
			},
			{
				name: 'nmjnsorder',
				mapping: 'nmjnsorder'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_stpo(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'stpo_controller/get_stpo', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstpo',
				mapping: 'idstpo'
			},{
				name: 'kdstpo',
				mapping: 'kdstpo'
			},
			{
				name: 'nmstpo',
				mapping: 'nmstpo'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_matauang(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'matauang_controller/get_matauang', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'kdmatauang',
				mapping: 'kdmatauang'
			},
			{
				name: 'nmmatauang',
				mapping: 'nmmatauang'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_wnegara(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'wnegara_controller/get_wnegara', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idwn',
				mapping: 'idwn'
			},{
				name: 'kdwn',
				mapping: 'kdwn'
			},
			{
				name: 'nmwn',
				mapping: 'nmwn'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_extrabed(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'extrabed_controller/get_extrabed', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idextrabed',
				mapping: 'idextrabed'
			},{
				name: 'kdextrabed',
				mapping: 'kdextrabed'
			},
			{
				name: 'nmextrabed',
				mapping: 'nmextrabed'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stbed(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'stbed_controller/get_stbed', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstbed',
				mapping: 'idstbed'
			},{
				name: 'kdstbed',
				mapping: 'kdstbed'
			},
			{
				name: 'nmstbed',
				mapping: 'nmstbed'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jpembayaran(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jpembayaran_controller/get_jpembayaran', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnspembayaran',
				mapping: 'idjnspembayaran'
			},{
				name: 'kdjnspembayaran',
				mapping: 'kdjnspembayaran'
			},
			{
				name: 'nmjnspembayaran',
				mapping: 'nmjnspembayaran'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_sypembayaran(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'sypembayaran_controller/get_sypembayaran', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idsypembayaran',
				mapping: 'idsypembayaran'
			},{
				name: 'kdsypembayaran',
				mapping: 'kdsypembayaran'
			},
			{
				name: 'nmsypembayaran',
				mapping: 'nmsypembayaran'
			}]
		});
		return master;
	}

//=======================================================
	function dm_sypembayarandipo(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'sypembayaran_controller/get_sypembayarandipo', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idsypembayaran',
				mapping: 'idsypembayaran'
			},{
				name: 'kdsypembayaran',
				mapping: 'kdsypembayaran'
			},
			{
				name: 'nmsypembayaran',
				mapping: 'nmsypembayaran'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jlibur(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'jlibur_controller/get_jlibur', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnslibur',
				mapping: 'idjnslibur'
			},{
				name: 'kdjnslibur',
				mapping: 'kdjnslibur'
			},
			{
				name: 'nmjnslibur',
				mapping: 'nmjnslibur'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jtarif(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jtarif_controller/get_jtarif',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnstarif',
				mapping: 'idjnstarif'
			},{
				name: 'kdjnstarif',
				mapping: 'kdjnstarif'
			},{
				name: 'nmjnstarif',
				mapping: 'nmjnstarif'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_sttransaksi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'sttransaksi_controller/get_sttransaksi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'kdsttransaksi',
				mapping: 'kdsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_carabayar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'carabayar_controller/get_carabayar',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idcarabayar',
				mapping: 'idcarabayar'
			},{
				name: 'kdcarabayar',
				mapping: 'kdcarabayar'
			},{
				name: 'nmcarabayar',
				mapping: 'nmcarabayar'
			},{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			}]
		});
		return master;
	}
	
	//menampilkan cara bayar khusus tunai & transfer (tanpa dijamin / potong gaji dll)
	function dm_carabayar2(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'carabayar_controller/get_carabayar2',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idcarabayar',
				mapping: 'idcarabayar'
			},{
				name: 'kdcarabayar',
				mapping: 'kdcarabayar'
			},{
				name: 'nmcarabayar',
				mapping: 'nmcarabayar'
			},{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stbypass(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stbypass_controller/get_stbypass',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstbypass',
				mapping: 'idstbypass'
			},{
				name: 'kdstbypass',
				mapping: 'kdstbypass'
			},{
				name: 'nmstbypass',
				mapping: 'nmstbypass'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stdokterrawat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stdokterrawat_controller/get_stdokterrawat',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstdokterrawat',
				mapping: 'idstdokterrawat'
			},{
				name: 'kdstdokterrawat',
				mapping: 'kdstdokterrawat'
			},{
				name: 'nmstdokterrawat',
				mapping: 'nmstdokterrawat'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stdoktertim(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stdoktertim_controller/get_stdoktertim',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstdoktertim',
				mapping: 'idstdoktertim'
			},{
				name: 'kdstdoktertim',
				mapping: 'kdstdoktertim'
			},{
				name: 'nmstdoktertim',
				mapping: 'nmstdoktertim'
			}]
		});
		return master;
	}
//=======================================================
	function dm_stpublish(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stpublish_controller/get_stpublish',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstpublish',
				mapping: 'idstpublish'
			},{
				name: 'kdstpublish',
				mapping: 'kdstpublish'
			},{
				name: 'nmstpublish',
				mapping: 'nmstpublish'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stregistrasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stregistrasi_controller/get_stregistrasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstregistrasi',
				mapping: 'idstregistrasi'
			},{
				name: 'kdstregistrasi',
				mapping: 'kdstregistrasi'
			},{
				name: 'nmstregistrasi',
				mapping: 'nmstregistrasi'
			}]
		});
		return master;
	}	
	
//=======================================================
	function dm_stposisipasien(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stposisipasien_controller/get_stposisipasien',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'kdstposisipasien',
				mapping: 'kdstposisipasien'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			}]
		});
		return master;
	}	
	
//=======================================================
	function dm_stkrm(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stkrm_controller/get_stkrm',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstkrm',
				mapping: 'idstkrm'
			},{
				name: 'kdstkrm',
				mapping: 'kdstkrm'
			},{
				name: 'nmstkrm',
				mapping: 'nmstkrm'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tahun(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tahun_controller/get_tahun',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tahun',
				mapping: 'tahun'
			},{
				name: 'tglawal',
				mapping: 'tglawal'
			},{
				name: 'tglakhir',
				mapping: 'tglakhir'
			}]
		});
		return master;
	}

//=======================================================
	function dm_tahunakun(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tahun_controller/get_tahunakun',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tahun',
				mapping: 'tahun'
			},{
				name: 'tglawal',
				mapping: 'tglawal'
			},{
				name: 'tglakhir',
				mapping: 'tglakhir'
			}]
		});
		return master;
	}

//=======================================================
	function dm_bulan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bulan_controller/get_bulan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbulan',
				mapping: 'idbulan'
			},{
				name: 'nmbulan',
				mapping: 'nmbulan'
			},{
				name:'kdbulan',
				mapping: 'kdbulan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_jnstransaksi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jtransaksi_controller/get_jnstransaksi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnstransaksi',
				mapping: 'idjnstransaksi'
			},{
				name: 'kdjnstransaksi',
				mapping: 'kdjnstransaksi'
			},{
				name: 'nmjnstransaksi',
				mapping: 'nmjnstransaksi'
			}]
		});
		return master;
	}	
	
//=======================================================
	function dm_jnstransaksidijurnal(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pengeluarankas_controller/get_jnstransaksidijurnal',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnstransaksi',
				mapping: 'idjnstransaksi'
			},{
				name: 'kdjnstransaksi',
				mapping: 'kdjnstransaksi'
			},{
				name: 'nmjnstransaksi',
				mapping: 'nmjnstransaksi'
			}]
		});
		return master;
	}	
	
//=======================================================
	function dm_stsetuju(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stpersetujuan_controller/get_stpersetujuan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'kdstsetuju',
				mapping: 'kdstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			}]
		});
		return master;
	}
		
//=======================================================
	function dm_stperpembelian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stperpembelian_controller/get_stperpembelian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstpp',
				mapping: 'idstpp'
			},{
				name: 'kdstpp',
				mapping: 'kdstpp'
			},{
				name: 'nmstpp',
				mapping: 'nmstpp'
			}]
		});
		return master;
	}
		
//=======================================================
	function dm_jperpembelian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jperpembelian_controller/get_jperpembelian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnspp',
				mapping: 'idjnspp'
			},{
				name: 'kdjnspp',
				mapping: 'kdjnspp'
			},{
				name: 'nmjnspp',
				mapping: 'nmjnspp'
			}]
		});
		return master;
	}
			
//=======================================================
	function dm_stdiskon(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stdiskon_controller/get_stdiskon',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstdiskon',
				mapping: 'idstdiskon'
			},{
				name: 'kdstdiskon',
				mapping: 'kdstdiskon'
			},{
				name: 'nmstdiskon',
				mapping: 'nmstdiskon'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stkasir(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bukakasir_controller/get_stkasir',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstkasir',
				mapping: 'idstkasir'
			},{
				name: 'kdstkasir',
				mapping: 'kdstkasir'
			},{
				name: 'nmstkasir',
				mapping: 'nmstkasir'
			}]
		});
		return master;
	}

//=======================================================
	function dm_stkasirr(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stkasir_controller/get_stkasir',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstkasir',
				mapping: 'idstkasir'
			},{
				name: 'kdstkasir',
				mapping: 'kdstkasir'
			},{
				name: 'nmstkasir',
				mapping: 'nmstkasir'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_penkematian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'penkematian_controller/get_penkematian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkematian',
				mapping: 'idkematian'
			},{
				name: 'kdkematian',
				mapping: 'kdkematian'
			},{
				name: 'nmkematian',
				mapping: 'nmkematian'
			}]
		});
		return master;
	}

//=======================================================
	function dm_tinlanjut(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tinlanjut_controller/get_tindaklanjut',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtindaklanjut',
				mapping: 'idtindaklanjut'
			},{
				name: 'kdtindaklanjut',
				mapping: 'kdtindaklanjut'
			},{
				name: 'nmtindaklanjut',
				mapping: 'nmtindaklanjut'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_ststokopname(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'ststokopname_controller/get_ststokopname',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstso',
				mapping: 'idstso'
			},{
				name: 'kdstso',
				mapping: 'kdstso'
			},{
				name: 'nmstso',
				mapping: 'nmstso'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stharga(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stharga_controller/get_stharga',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idstharga',
				mapping: 'idstharga'
			},{
				name: 'kdstharga',
				mapping: 'kdstharga'
			},{
				name: 'nmstharga',
				mapping: 'nmstharga'
			}]
		});
		return master;
	}

//=======================================================
	function dm_jnsakun(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jbaganperkiraan_controller/get_jnsakun',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnsakun',
				mapping: 'idjnsakun'
			},{
				name: 'kdjnsakun',
				mapping: 'kdjnsakun'
			},{
				name: 'nmjnsakun',
				mapping: 'nmjnsakun'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_klpakun(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kelbaganperkiraan_controller/get_klpakun',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idklpakun',
				mapping: 'idklpakun'
			},{
				name: 'kdklpakun',
				mapping: 'kdklpakun'
			},{
				name: 'nmklpakun',
				mapping: 'nmklpakun'
			}]
		});
		return master;
	}
	
// Master Data
//=======================================================
	function dm_daerah(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'daerah_controller/get_daerah', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddaerah',
				mapping: 'iddaerah'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'dae_iddaerah',
				mapping: 'dae_iddaerah'
			},{
				name: 'idlvldaerah',
				mapping: 'idlvldaerah'
			},{
				name: 'nmlvldaerah',
				mapping: 'nmlvldaerah'
			},{
				name: 'kddaerah',
				mapping: 'kddaerah'
			},
			{
				name: 'nmdaerah',
				mapping: 'nmdaerah'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_daerah_pasien(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'daerah_controller/get_daerah_pasien', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddaerah',
				mapping: 'iddaerah'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'dae_iddaerah',
				mapping: 'dae_iddaerah'
			},{
				name: 'idlvldaerah',
				mapping: 'idlvldaerah'
			},{
				name: 'nmlvldaerah',
				mapping: 'nmlvldaerah'
			},{
				name: 'kddaerah',
				mapping: 'kddaerah'
			},
			{
				name: 'nmdaerah',
				mapping: 'nmdaerah'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_bagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bagian_controller/get_bagian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'idlvlbagian',
				mapping: 'idlvlbagian'
			},{
				name: 'nmlvlbagian',
				mapping: 'nmlvlbagian'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idbdgrawat',
				mapping: 'idbdgrawat'
			},{
				name: 'nmbdgrawat',
				mapping: 'nmbdgrawat'
			},{
				name: 'kdbagian',
				mapping: 'kdbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'bag_idbagian',
				mapping: 'bag_idbagian'
			},{
				name: 'alias',
				mapping: 'alias'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_bagianrjriugd(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bagian_controller/get_bagianrjriugd',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'idlvlbagian',
				mapping: 'idlvlbagian'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'idbdgrawat',
				mapping: 'idbdgrawat'
			},{
				name: 'kdbagian',
				mapping: 'kdbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'bag_idbagian',
				mapping: 'bag_idbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_bagianClean(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bagian_controller/getBagianClean',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'idlvlbagian',
				mapping: 'idlvlbagian'
			},{
				name: 'nmlvlbagian',
				mapping: 'nmlvlbagian'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idbdgrawat',
				mapping: 'idbdgrawat'
			},{
				name: 'nmbdgrawat',
				mapping: 'nmbdgrawat'
			},{
				name: 'kdbagian',
				mapping: 'kdbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'bag_idbagian',
				mapping: 'bag_idbagian'
			}]
		});
		return master;
	}
//=======================================================
	function dm_kmrbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bagian_controller/get_kmrbagian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_bagianri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bagian_controller/get_bagianri',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'idlvlbagian',
				mapping: 'idlvlbagian'
			},{
				name: 'nmlvlbagian',
				mapping: 'nmlvlbagian'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idbdgrawat',
				mapping: 'idbdgrawat'
			},{
				name: 'nmbdgrawat',
				mapping: 'nmbdgrawat'
			},{
				name: 'kdbagian',
				mapping: 'kdbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'bag_idbagian',
				mapping: 'bag_idbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_kamar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kamar_controller/get_kamar',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'kdkamar',
				mapping: 'kdkamar'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'fasilitas',
				mapping: 'fasilitas'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_kamarbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kamar_controller/get_kamarbag',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'kdkamar',
				mapping: 'kdkamar'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'fasilitas',
				mapping: 'fasilitas'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idbed',
				mapping: 'idbed'
			}]
		});
		return master;
	}

//=======================================================
	function dm_cbkamardibed(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kamar_controller/get_cbkamardibed',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'kdkamar',
				mapping: 'kdkamar'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'rperawatan_kamar',
				mapping: 'rperawatan_kamar'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_bed(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bed_controller/get_bed',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'rperawatan_kamar',
				mapping: 'rperawatan_kamar'
			},{
				name: 'idbed',
				mapping: 'idbed'
			},{
				name: 'kdbed',
				mapping: 'kdbed'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idextrabed',
				mapping: 'idextrabed'
			},{
				name: 'nmextrabed',
				mapping: 'nmextrabed'
			},{
				name: 'idstbed',
				mapping: 'idstbed'
			},{
				name: 'nmstbed',
				mapping: 'nmstbed'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_dokter(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'dokter_controller/get_dokter',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'kddokter',
				mapping: 'kddokter'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'tptlahir',
				mapping: 'tptlahir'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'idspesialisasi',
				mapping: 'idspesialisasi'
			},{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'idstdokter',
				mapping: 'idstdokter'
			},{
				name: 'nmstdokter',
				mapping: 'nmstdokter'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idbank',
				mapping: 'idbank'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'norek',
				mapping: 'norek'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'cabang',
				mapping: 'cabang'
			},{
				name: 'idjnstenagamedis',
				mapping: 'idjnstenagamedis'
			},{
				name: 'nmjnstenagamedis',
				mapping: 'nmjnstenagamedis'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_dokterri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'dokter_controller/get_dokterri',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'kddokter',
				mapping: 'kddokter'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'tptlahir',
				mapping: 'tptlahir'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'idspesialisasi',
				mapping: 'idspesialisasi'
			},{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'idstdokter',
				mapping: 'idstdokter'
			},{
				name: 'nmstdokter',
				mapping: 'nmstdokter'
			},{
				name: 'catatan',
				mapping: 'catatan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_perawat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'perawat_controller/get_perawat',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idperawat',
				mapping: 'idperawat'
			},{
				name: 'kdperawat',
				mapping: 'kdperawat'
			},{
				name: 'nmperawat',
				mapping: 'nmperawat'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_reservasix(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'reservasi_controller/get_reservasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idreservasi',
				mapping: 'idreservasi'
			},{
				name: 'tglreservasi',
				mapping: 'tglreservasi'
			},{
				name: 'jamreservasi',
				mapping: 'jamreservasi'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'norm',
				mapping: 'rnorm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'email',
				mapping: 'email'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'idstreservasi',
				mapping: 'idstreservasi'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'noantrian',
				mapping: 'noantrian'
			},{
				name: 'userinput',
				mapping: 'userinput'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'jamreg',
				mapping: 'jamreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'nmstpasien',
				mapping: 'nmstpasien'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'tinggibadan',
				mapping: 'tinggibadan'
			},{
				name: 'beratbadan',
				mapping: 'beratbadan'
			},{
				name: 'nmstregistrasi',
				mapping: 'nmstregistrasi'
			},{
				name: 'keluhan',
				mapping: 'keluhan'
			},{
				name: 'umurtahun',
				mapping: 'umurtahun'
			},{
				name: 'umurbulan',
				mapping: 'umurbulan'
			},{
				name: 'umurhari',
				mapping: 'umurhari'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'alergi',
				mapping: 'alergi'
			},{
				name: 'systole',
				mapping: 'systole'
			},{
				name: 'diastole',
				mapping: 'diastole'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'gravidag',
				mapping: 'gravidag'
			},{
				name: 'gravidap',
				mapping: 'gravidap'
			},{
				name: 'gravidaa',
				mapping: 'gravidaa'
			},{
				name: 'gravidai',
				mapping: 'gravidai'
			},{
				name: 'kunjhamil',
				mapping: 'kunjhamil'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_kuitansi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'kuitansi_controller/get_kuitansi', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'jamkuitansi',
				mapping: 'jamkuitansi'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'idstkuitansi',
				mapping: 'idstkuitansi'
			},{
				name: 'nmstkuitansi',
				mapping: 'nmstkuitansi'
			},{
				name: 'idsbtnm',
				mapping: 'idsbtnm'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'idjnskuitansi',
				mapping: 'idjnskuitansi'
			},{
				name: 'pembulatan',
				mapping: 'pembulatan'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'ketina',
				mapping: 'ketina'
			},{
				name: 'keteng',
				mapping: 'keteng'
			},{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_kuitansidet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'kuitansidet_controller/get_kuitansidet', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkuitansidet',
				mapping: 'idkuitansidet'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idbank',
				mapping: 'idbank'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'idcarabayar',
				mapping: 'idcarabayar'
			},{
				name: 'nmcarabayar',
				mapping: 'nmcarabayar'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'nokartu',
				mapping: 'nokartu'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_bank(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'bank_controller/get_bank', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbank',
				mapping: 'idbank'
			},{
				name: 'kdbank',
				mapping: 'kdbank'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_penjamin(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'penjamin_controller/get_penjamin',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'kdpenjamin',
				mapping: 'kdpenjamin'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'idjnspenjamin',
				mapping: 'idjnspenjamin'
			},{
				name: 'nmjnspenjamin',
				mapping: 'nmjnspenjamin'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'email',
				mapping: 'email'
			},{
				name: 'website',
				mapping: 'website'
			},{
				name: 'nmcp',
				mapping: 'nmcp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'tglawal',
				mapping: 'tglawal'
			},{
				name: 'tglakhir',
				mapping: 'tglakhir'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'infoumum',
				mapping: 'infoumum'
			},{
				name: 'inforj',
				mapping: 'inforj'
			},{
				name: 'infori',
				mapping: 'infori'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_penyakit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'penyakit_controller/get_penyakit',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpenyakit',
				mapping: 'idpenyakit'
			},{
				name: 'kdpenyakit',
				mapping: 'kdpenyakit'
			},{
				name: 'nmpenyakit',
				mapping: 'nmpenyakit'
			},{
				name: 'nmpenyakiteng',
				mapping: 'nmpenyakiteng'
			},{
				name: 'kategori',
				mapping: 'kategori'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'pen_idpenyakit',
				mapping: 'pen_idpenyakit'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			},{
				name: 'dtd',
				mapping: 'dtd'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tbmedis(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tbmedis_controller/get_tbmedis',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idmarginbrg',
				mapping: 'idmarginbrg'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'margin',
				mapping: 'margin'
			}]
		});
		return master;
	}

	function dm_setmakspel(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setmakspel_controller/get_setmakspel',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'id',
				mapping: 'id'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'maksimal',
				mapping: 'maksimal'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_pabrik(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'pabrik_controller/get_pabrik', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpabrik',
				mapping: 'idpabrik'
			},{
				name: 'kdpabrik',
				mapping: 'kdpabrik'
			},{
				name: 'nmpabrik',
				mapping: 'nmpabrik'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_brgmedis(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'brgmedis_controller/get_brgmedis',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'idklpbrg',
				mapping: 'idklpbrg'
			},{
				name: 'nmklpbarang',
				mapping: 'nmklpbarang'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'idsatuanbsr',
				mapping: 'idsatuanbsr'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'hrgavg',
				mapping: 'hrgavg'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'idpabrik',
				mapping: 'idpabrik'
			},{
				name: 'nmpabrik',
				mapping: 'nmpabrik'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'stokmin',
				mapping: 'stokmin'
			},{
				name: 'stokmax',
				mapping: 'stokmax'
			},{
				name: 'gambar',
				mapping: 'gambar'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'userinput',
				mapping: 'userinput'
			},{
				name: 'margin',
				mapping: 'margin'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'hargajual',
				mapping: 'hargajual'
			},{
				name: 'hargajualtemp',
				mapping: 'hargajualtemp'
			},{
				name: 'idstgenerik',
				mapping: 'idstgenerik'
			},{
				name: 'idstformularium',
				mapping: 'idstformularium'
			}]
		});
		return master;
	}
		
//=======================================================
	function dm_stgenerik(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'brgmedis_controller/get_stgenerik',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nilai',
				mapping: 'nilai'
			},{
				name: 'nmset',
				mapping: 'nmset'
			}]
		});
		return master;
	}

//=======================================================
	function dm_stformularium(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'brgmedis_controller/get_stformularium',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nilai',
				mapping: 'nilai'
			},{
				name: 'nmset',
				mapping: 'nmset'
			}]
		});
		return master;
	}
	
//=======================================================

	function dm_lokasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'lokasi_controller/get_lokasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idlokasi',
				mapping: 'idlokasi'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'kdlokasi',
				mapping: 'kdlokasi'
			},{
				name: 'nmlokasi',
				mapping: 'nmlokasi'
			}]
		});
		return master;
	}
	
//=========================================================
	function dm_jhirarki(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'jhirarki_controller/gridM_hierarki',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [
			{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},
			{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},
			{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pelayanan_controller/get_pelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nourut',
				mapping: 'nourut'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'pel_kdpelayanan',
				mapping: 'pel_kdpelayanan'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});
		return master;
	}
	
	function dm_akun(){
		
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'akunpelayanan_controller/get_akun',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			},{
				name: 'kd_nm_akun',
				mapping: 'kd_nm_akun'
			}]
		});
		return master;
	
	}
	
	function dm_akunall(){
		
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'akunpelayanan_controller/get_akunall',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			}]
		});
		return master;
	
	}
	
	function dm_akunpelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'akunpelayanan_controller/get_akunpelayanan',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tahun',
				mapping: 'tahun'
			},{
				name: 'idakunjs',
				mapping: 'idakunjs'
			},{
				name: 'idakunjm',
				mapping: 'idakunjm'
			},{
				name: 'idakunjp',
				mapping: 'idakunjp'
			},{
				name: 'idakunbhp',
				mapping: 'idakunbhp'
			},{
				name: 'kdakunjs',
				mapping: 'kdakunjs'
			},{
				name: 'kdakunjm',
				mapping: 'kdakunjm'
			},{
				name: 'kdakunjp',
				mapping: 'kdakunjp'
			},{
				name: 'kdakunbhp',
				mapping: 'kdakunbhp'
			},{
				name: 'nmakunjs',
				mapping: 'nmakunjs'
			},{
				name: 'nmakunjm',
				mapping: 'nmakunjm'
			},{
				name: 'nmakunjp',
				mapping: 'nmakunjp'
			},{
				name: 'nmakunbhp',
				mapping: 'nmakunbhp'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'klpnmpelayanan',
				mapping: 'klpnmpelayanan'
			},{
				name: 'kd_klppelayanan',
				mapping: 'kd_klppelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			}]
		});
		return master;
	}
	
	function dm_klppelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pelayanan_controller/get_parent_pelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nourut',
				mapping: 'nourut'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_kdpelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pelayanan_controller/get_pelayanantotpumum_nonumum',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tpumum(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tpumum_controller/get_tpumum',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [/* {
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			}, */{
				name: 'tarifbhpnew',
				mapping: 'tarifbhpnew'
			},{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'total',
				mapping: 'total'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tpnonumum(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tpnonumum_controller/get_tpnonumum',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'total',
				mapping: 'total'
			}]
		});
		return master;
	}
	
//===========================================================
	function dm_idpenjamin(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'penjamin_controller/get_idpenjamin',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'kdpenjamin',
				mapping: 'kdpenjamin'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			}]
		});
		return master;
	}
	
//=======================================================	
	function dm_masterpaket(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'masterpaket_controller/get_masterpaket', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtarifpaket',
				mapping: 'idtarifpaket'
			},{
				name: 'kdpaket',
				mapping: 'kdpaket'
			},{
				name: 'namapaket',
				mapping: 'namapaket'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'nmpaket',
				mapping: 'nmpaket'
			},,{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'tar_idtarifpaket',
				mapping: 'tar_idtarifpaket'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tppelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'tppelayanan_controller/get_tppelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtarifpaketdet',
				mapping: 'idtarifpaketdet'
			},{
				name: 'kdtarif',
				mapping: 'kdtarif'
			},{
				name: 'nmtarif',
				mapping: 'nmtarif'
			},{
				name: 'idjnstarif',
				mapping: 'idjnstarif'
			},{
				name: 'kdjnstarif',
				mapping: 'kdjnstarif'
			},{
				name: 'idtarifpaket',
				mapping: 'idtarifpaket'
			},{
				name: 'nmpaket',
				mapping: 'nmpaket'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'total',
				mapping: 'total'
			}]
		});
		return master;
	}

	function dm_detail_pelayanan_bhp_alkes(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'tppelayanan_controller/get_detail_bhp_alkes',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpelayanandet',
				mapping: 'idpelayanandet'
			},{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nmtarif',
				mapping: 'nmtarif'
			},{
				name: 'kdtarif',
				mapping: 'kdtarif'
			},{
				name: 'idjnstarif',
				mapping: 'idjnstarif'
			},{
				name: 'tarifbhp_per_treatment',
				mapping: 'tarifbhp_per_treatment'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			}]
		});
		return master;
	}
	
//=======================================================	
	function dm_tarifpaketdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'tarifpaketdet_controller/get_tarifpaketdet', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtarifpaketdet',
				mapping: 'idtarifpaketdet'
			},{
				name: 'nmitem',
				mapping: 'nmitem'
			},{
				name: 'kdtarif',
				mapping: 'kdtarif'
			},{
				name: 'idjnstarif',
				mapping: 'idjnstarif'
			},{
				name: 'idtarifpaket',
				mapping: 'idtarifpaket'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'ttltarif',
				mapping: 'ttltarif'
			}]
		});
		return master;
	}
	
//=======================================================	
	function dm_nota(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'nota_controller/get_nota', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'nmitem',
				mapping: 'nmitem'
			},{
				name: 'nona',
				mapping: 'nona'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'noorder',
				mapping: 'noorder'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'noresep',
				mapping: 'noresep'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'catatann',
				mapping: 'catatann'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'userinput',
				mapping: 'userinput'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'idregdettransfer',
				mapping: 'idregdettransfer'
			},{
				name: 'idnotadet',
				mapping: 'idnotadet'
			},{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'idjnstarif',
				mapping: 'idjnstarif'
			},{
				name: 'kdresep',
				mapping: 'kdresep'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'diskonjs',
				mapping: 'diskonjs'
			},{
				name: 'diskonjm',
				mapping: 'diskonjm'
			},{
				name: 'diskonjp',
				mapping: 'diskonjp'
			},{
				name: 'diskonbhp',
				mapping: 'diskonbhp'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idperawat',
				mapping: 'idperawat'
			},{
				name: 'nmperawat',
				mapping: 'nmperawat'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'idstbypass',
				mapping: 'idstbypass'
			},{
				name: 'tarif',
				mapping: 'tarif'
			},{
				name: 'tarif2',
				mapping: 'tarif2'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'ctotal',
				mapping: 'ctotal'
			},{
				name: 'idtarifpaketdet',
				mapping: 'idtarifpaketdet'
			},{
				name: 'koder',
				mapping: 'koder',
				type: 'int'
			},{
				name: 'drp',
				mapping: 'diskon'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'qtyretur',
				mapping: 'qtyretur'
			},{
				name: 'diskonr',
				mapping: 'diskonr'
			},{
				name: 'dijamin',
				mapping: 'dijamin'
			},{
				name: 'dijaminrp',
				mapping: 'dijaminrp'
			},{
				name: 'dijaminrpf',
				mapping: 'dijaminrpf'
			},{
				name: 'dijaminprsn',
				mapping: 'dijaminprsn'
			},{
				name: 'dijaminprsnf',
				mapping: 'dijaminprsnf'
			},{
				name: 'selisih',
				mapping: 'selisih'
			},{
				name: 'selisihf',
				mapping: 'selisihf'
			},{
				name: 'catatand',
				mapping: 'catatand'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			}]
		});
		return master;
	}
	
//=======================================================	
	function dm_notatransri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'nota_controller/get_notatransri', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'nmitem',
				mapping: 'nmitem'
			},{
				name: 'nona',
				mapping: 'nona'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'noorder',
				mapping: 'noorder'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'noresep',
				mapping: 'noresep'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'catatann',
				mapping: 'catatann'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'diskonr',
				mapping: 'diskonr'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'userinput',
				mapping: 'userinput'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'idregdettransfer',
				mapping: 'idregdettransfer'
			},{
				name: 'idnotadet',
				mapping: 'idnotadet'
			},{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'idjnstarif',
				mapping: 'idjnstarif'
			},{
				name: 'kdresep',
				mapping: 'kdresep'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'diskonjs',
				mapping: 'diskonjs'
			},{
				name: 'diskonjm',
				mapping: 'diskonjm'
			},{
				name: 'diskonjp',
				mapping: 'diskonjp'
			},{
				name: 'diskonbhp',
				mapping: 'diskonbhp'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idperawat',
				mapping: 'idperawat'
			},{
				name: 'nmperawat',
				mapping: 'nmperawat'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'idstbypass',
				mapping: 'idstbypass'
			},{
				name: 'tarif',
				mapping: 'tarif'
			},{
				name: 'tarif2',
				mapping: 'tarif2'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'ctotal',
				mapping: 'ctotal'
			},{
				name: 'idtarifpaketdet',
				mapping: 'idtarifpaketdet'
			},{
				name: 'koder',
				mapping: 'koder',
				type: 'int'
			},{
				name: 'drp',
				mapping: 'diskon'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'qtyretur',
				mapping: 'qtyretur'
			},{
				name: 'dijamin',
				mapping: 'dijamin'
			},{
				name: 'dijaminrp',
				mapping: 'dijaminrp'
			},{
				name: 'dijaminrpf',
				mapping: 'dijaminrpf'
			},{
				name: 'dijaminprsn',
				mapping: 'dijaminprsn'
			},{
				name: 'dijaminprsnf',
				mapping: 'dijaminprsnf'
			},{
				name: 'selisih',
				mapping: 'selisih'
			},{
				name: 'selisihf',
				mapping: 'selisihf'
			}]
		});
		return master;
	}

	function dm_notatransri_with_bhp(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'nota_controller/get_notatransri_with_bhp', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'nmitem',
				mapping: 'nmitem'
			},{
				name: 'nona',
				mapping: 'nona'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'noorder',
				mapping: 'noorder'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'noresep',
				mapping: 'noresep'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'catatann',
				mapping: 'catatann'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'diskonr',
				mapping: 'diskonr'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'userinput',
				mapping: 'userinput'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'idregdettransfer',
				mapping: 'idregdettransfer'
			},{
				name: 'idnotadet',
				mapping: 'idnotadet'
			},{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'idjnstarif',
				mapping: 'idjnstarif'
			},{
				name: 'kdresep',
				mapping: 'kdresep'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'diskonjs',
				mapping: 'diskonjs'
			},{
				name: 'diskonjm',
				mapping: 'diskonjm'
			},{
				name: 'diskonjp',
				mapping: 'diskonjp'
			},{
				name: 'diskonbhp',
				mapping: 'diskonbhp'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idperawat',
				mapping: 'idperawat'
			},{
				name: 'nmperawat',
				mapping: 'nmperawat'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'idstbypass',
				mapping: 'idstbypass'
			},{
				name: 'tarif',
				mapping: 'tarif'
			},{
				name: 'tarif2',
				mapping: 'tarif2'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'ctotal',
				mapping: 'ctotal'
			},{
				name: 'idtarifpaketdet',
				mapping: 'idtarifpaketdet'
			},{
				name: 'koder',
				mapping: 'koder',
				type: 'int'
			},{
				name: 'drp',
				mapping: 'diskon'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'qtyretur',
				mapping: 'qtyretur'
			},{
				name: 'dijamin',
				mapping: 'dijamin'
			},{
				name: 'dijaminrp',
				mapping: 'dijaminrp'
			},{
				name: 'dijaminrpf',
				mapping: 'dijaminrpf'
			},{
				name: 'dijaminprsn',
				mapping: 'dijaminprsn'
			},{
				name: 'dijaminprsnf',
				mapping: 'dijaminprsnf'
			},{
				name: 'selisih',
				mapping: 'selisih'
			},{
				name: 'selisihf',
				mapping: 'selisihf'
			}]
		});
		return master;
	}
	
//=======================================================	
	function dm_notax(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'nota_controller/get_notax', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'nona',
				mapping: 'nona'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'noorder',
				mapping: 'noorder'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'noresep',
				mapping: 'noresep'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'userinput',
				mapping: 'userinput'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'idregdettransfer',
				mapping: 'idregdettransfer'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'ttlnota',
				mapping: 'ttlnota'
			},{
				name: 'koder',
				mapping: 'koder',
				type: 'int'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			}]
		});
		return master;
	}
	
//=======================================================	
	function dm_counnonotat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'nota_controller/get_counnonotat', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'cnonota',
				mapping: 'cnonota'
			}]
		});
		return master;
	}
	
//=======================================================	
	function dm_counnonota(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'nota_controller/get_counnonota', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'cnonota',
				mapping: 'cnonota'
			}]
		});
		return master;
	}
		
//=======================================================
	function dm_supplier(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'supplier_controller/get_supplier',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'tgldaftar',
				mapping: 'tgldaftar'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'email',
				mapping: 'email'
			},{
				name: 'website',
				mapping: 'website'
			},{
				name: 'kontakperson',
				mapping: 'kontakperson'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'npwp',
				mapping: 'npwp'
			},{
				name: 'idbank',
				mapping: 'idbank'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'norek',
				mapping: 'norek'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_mtemobat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'mtemobat_controller/get_mtemobat',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtempobat',
				mapping: 'idtempobat'
			},{
				name: 'nmtempobat',
				mapping: 'nmtempobat'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'tem_idtempobat',
				mapping: 'tem_idtempobat'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_mtempelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'mtempelayanan_controller/get_mtempelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtemplayanan',
				mapping: 'idtemplayanan'
			},{
				name: 'nmtemplayanan',
				mapping: 'nmtemplayanan'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'tem_idtemplayanan',
				mapping: 'tem_idtemplayanan'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});
		return master;
	}

//=======================================================
	function dm_temobat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'temobat_controller/get_temobat',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iditemobattemp',
				mapping: 'iditemobattemp'
			},{
				name: 'idtempobat',
				mapping: 'idtempobat'
			},{
				name: 'nmtempobat',
				mapping: 'nmtempobat'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'qtyitem',
				mapping: 'qtyitem'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			}]
		});
		return master;
	}

//=======================================================
	function dm_mastertemobat_parent(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'temobat_controller/get_parent_mastertemobat',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtempobat',
				mapping: 'idtempobat'
			},{
				name: 'nmtempobat',
				mapping: 'nmtempobat'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tempelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tempelayanan_controller/get_tempelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iditempelayanantemp',
				mapping: 'iditempelayanantemp'
			},{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idtemplayanan',
				mapping: 'idtemplayanan'
			},{
				name: 'nmtemplayanan',
				mapping: 'nmtemplayanan'
			},{
				name: 'klstarif',
				mapping: 'klstarif'
			},{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'ttltarif',
				mapping: 'ttltarif'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tempelayananri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tempelayanan_controller/get_tempelayananri',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iditempelayanantemp',
				mapping: 'iditempelayanantemp'
			},{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idtemplayanan',
				mapping: 'idtemplayanan'
			},{
				name: 'nmtemplayanan',
				mapping: 'nmtemplayanan'
			},{
				name: 'klstarif',
				mapping: 'klstarif'
			},{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'ttltarif',
				mapping: 'ttltarif'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_mastertempelayanan_parent(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'tempelayanan_controller/get_parent_mastertempelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idtemplayanan',
				mapping: 'idtemplayanan'
			},{
				name: 'nmtemplayanan',
				mapping: 'nmtemplayanan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_baganperkiraan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'baganperkiraan_controller/get_baganperkiraan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			},{
				name: 'idklpakun',
				mapping: 'idklpakun'
			},{
				name: 'nmklpakun',
				mapping: 'nmklpakun'
			},{
				name: 'idjnsakun',
				mapping: 'idjnsakun'
			},{
				name: 'kdjnsakun',
				mapping: 'kdjnsakun'
			},{
				name: 'nmjnsakun',
				mapping: 'nmjnsakun'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'idakunparent',
				mapping: 'idakunparent'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_baganperkiraantahunan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'baganperkiraantahunan_controller/get_baganperkiraantahunan',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tahun',
				mapping: 'tahun'
			},{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'saldoawal',
				mapping: 'saldoawal'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			},{
				name: 'idklpakun',
				mapping: 'idklpakun'
			},{
				name: 'nmklpakun',
				mapping: 'nmklpakun'
			},{
				name: 'idjnsakun',
				mapping: 'idjnsakun'
			},{
				name: 'kdjnsakun',
				mapping: 'kdjnsakun'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			}]
		});
		return master;
	}
	
	function dm_baganperkiraanditahunan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'baganperkiraan_controller/get_dataakun',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			},{
				name: 'idklpakun',
				mapping: 'idklpakun'
			},{
				name: 'nmklpakun',
				mapping: 'nmklpakun'
			},{
				name: 'idjnsakun',
				mapping: 'idjnsakun'
			},{
				name: 'kdjnsakun',
				mapping: 'kdjnsakun'
			},{
				name: 'nmjnsakun',
				mapping: 'nmjnsakun'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'idakunparent',
				mapping: 'idakunparent'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});
		return master;
	}
	
//Master Pengaturan
//=======================================================
	function dm_brgbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'brgbagian_controller/get_brgbagian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'stokminbagian',
				mapping: 'stokminbagian'
			},{
				name: 'stokmaxbagian',
				mapping: 'stokmaxbagian'
			}]
		});
		return master;
	}
	
	function dm_brgbagian1(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'brgbagian_controller/get_brgbagian1',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'subtotalbeli',
				mapping: 'subtotalbeli'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuanbsr',
				mapping: 'idsatuanbsr'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'stokminbagian',
				mapping: 'stokminbagian'
			},{
				name: 'stokmaxbagian',
				mapping: 'stokmaxbagian'
			},{
				name: 'informasi',
				mapping: 'informasi'
			}]
		});
		return master;
	}
	
	function dm_brgmedisdibrgbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'brgbagian_controller/get_brgmedisdibrgbagian',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_hlibur(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'hlibur_controller/get_hlibur',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idlibur',
				mapping: 'idlibur'
			},{
				name: 'idjnslibur',
				mapping: 'idjnslibur'
			},{
				name: 'nmjnslibur',
				mapping: 'nmjnslibur'
			},{
				name: 'tgllibur',
				mapping: 'tgllibur'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jadwalpraktek(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jadwalpraktek_controller/get_jadwalpraktek',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjadwalpraktek',
				mapping: 'idjadwalpraktek'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idhari',
				mapping: 'idhari'
			},{
				name: 'nmhari',
				mapping: 'nmhari'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'nmshift',
				mapping: 'nmshift'
			},{
				name: 'jampraktek',
				mapping: 'jampraktek'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
	
	function dm_cbdijpdokter(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jadwalpraktek_controller/get_cbdijpdokter',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idbdgrawat',
				mapping: 'idbdgrawat'
			},{
				name: 'nmbdgrawat',
				mapping: 'nmbdgrawat'
			},{
				name: 'kdbagian',
				mapping: 'kdbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jadwalprakteknow(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jadwalpraktek_controller/getNmHari',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjadwalpraktek',
				mapping: 'idjadwalpraktek'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idhari',
				mapping: 'idhari'
			},{
				name: 'nmhari',
				mapping: 'nmhari'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'jampraktek',
				mapping: 'jampraktek'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			}]
		});
		return master;
	}

//Pusat Informasi
//=======================================================
	function dm_inputinformasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'inputinformasi_controller/get_inputinformasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idinfo',
				mapping: 'idinfo'
			},{
				name: 'tglinfo',
				mapping: 'tglinfo'
			},{
				name: 'judul',
				mapping: 'judul'
			},{
				name: 'deskripsi',
				mapping: 'deskripsi'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'idstpublish',
				mapping: 'idstpublish'
			},{
				name: 'nmstpublish',
				mapping: 'nmstpublish'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_infojpraktekdokter(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'infojpraktekdokter_controller/get_infojpraktekdokter',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjadwalpraktek',
				mapping: 'idjadwalpraktek'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'senin',
				mapping: 'senin'
			},{
				name: 'selasa',
				mapping: 'selasa'
			},{
				name: 'rabu',
				mapping: 'rabu'
			},{
				name: 'kamis',
				mapping: 'kamis'
			},{
				name: 'jumat',
				mapping: 'jumat'
			},{
				name: 'sabtu',
				mapping: 'sabtu'
			},{
				name: 'minggu',
				mapping: 'minggu'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}

//=======================================================
	var reader = new Ext.data.JsonReader({
		root:'data',
		idProperty: '',
		totalProperty: 'results',
		remoteGroup: true,
		fields: [ 
		{ name: 'nmbagian' }
		, { name: 'nmkamar' } 
		, { name: 'nmbed' }
		, { name: 'nmstbed' }
		, { name: 'noreg' }
		, { name: 'tglmasuk' }
		, { name: 'jammasuk' }
		, { name: 'norm' }
		, { name: 'nmpasien' }
		, { name: 'nmkerabat' }
		, { name: 'kdjnskelamin' }
		, { name: 'umur' }
		, { name: 'idstbed' }
		]
	});
	
	function dm_infobed(){
		var master = new Ext.data.GroupingStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'infobed_controller/get_infobed',
				method: 'POST',
			}),
			baseParams:{
				key:'0'
			},			
			reader: reader,
			groupField:'nmbagian',		
			remoteSort: true,
		});
		return master;
	}
	
//=======================================================
	var arr_cari = [
		['nmbagian', 'Ruangan'],
		['nmkamar', 'Kamar'],
		['nmbed', 'Bed'],
		['nmstbed', 'Status Bed'],
		['noreg', 'No. Registrasi (REG)'],
		//['tglmasuk', 'Tanggal Masuk'],
		//['jammasuk', 'Jam Masuk'],
		['norm', 'No. Rekam Medis (RM)'],
		['nmpasien', 'Nama Pasien'],
		['nmkerabat', 'Nama Orang Tua/Pasangan'],
		['kdjnskelamin', 'Jenis Kelamin (L/P)'],
		//['Umur', 'Umur'],
	];
	
	function dm_cmbinfobed(){
		var master = new Ext.data.ArrayStore({
			fields: ['id', 'name'],
			data : arr_cari 
		});
		return master;
	}
	
//=======================================================
	function dm_pitpelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tpumum_controller/get_pitpelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'total',
				mapping: 'total'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pipasiendirawat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'infopasiendirawat_controller/get_pipasiendirawat',
				method: 'POST'
			}),
			baseParams:{
				key : '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'ruangan_kamar_bed',
				mapping: 'ruangan_kamar_bed'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'tglmasuk',
				mapping: 'tglmasuk'
			},{
				name: 'jammasuk',
				mapping: 'jammasuk'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'jamkeluar',
				mapping: 'jamkeluar'
			},{
				name: 'lamarawat',
				mapping: 'lamarawat'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pisupplier(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'supplier_controller/get_pisupplier',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'tgldaftar',
				mapping: 'tgldaftar'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'email',
				mapping: 'email'
			},{
				name: 'website',
				mapping: 'website'
			},{
				name: 'kontakperson',
				mapping: 'kontakperson'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'npwp',
				mapping: 'npwp'
			},{
				name: 'idbank',
				mapping: 'idbank'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'norek',
				mapping: 'norek'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			}]
		});
		return master;
	}

//Transaksi
//=======================================================
	function dm_pasien(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pasien_controller/get_pasien',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'norm',
				mapping: 'norm',
				//type: 'int'
			},{
				name: 'normnya',
				mapping: 'normnya'
			},{
				name: 'normlama',
				mapping: 'normlama'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'idstkawin',
				mapping: 'idstkawin'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'idwn',
				mapping: 'idwn'
			},{
				name: 'iddaerah',
				mapping: 'iddaerah'
			},{
				name: 'nmdaerah',
				mapping: 'nmdaerah'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'tptlahir',
				mapping: 'tptlahir'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'nmibu',
				mapping: 'nmibu'
			},{
				name: 'idpekerjaan',
				mapping: 'idpekerjaan'
			},{
				name: 'idagama',
				mapping: 'idagama'
			},{
				name: 'noidentitas',
				mapping: 'noidentitas'
			},{
				name: 'idgoldarah',
				mapping: 'idgoldarah'
			},{
				name: 'idpendidikan',
				mapping: 'idpendidikan'
			},{
				name: 'idsukubangsa',
				mapping: 'idsukubangsa'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'negara',
				mapping: 'negara'
			},{
				name: 'alergi',
				mapping: 'alergi'
			},{
				name: 'idstpelayanan',
				mapping: 'idstpelayanan'
			},{
				name: 'tgldaftar',
				mapping: 'tgldaftar'
			},{
				name: 'nmpasangan',
				mapping: 'nmpasangan'
			},{
				name: 'tgllahirortu',
				mapping: 'tgllahirortu'
			},{
				name: 'tgllahirpasangan',
				mapping: 'tgllahirpasangan'
			},{
				name: 'foto_pasien',
				mapping: 'foto_pasien'
			}]
		});
		return master;
	}
	
//======================================================= 
	function dm_registrasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'registrasi_controller/get_registrasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'registrasi.noreg',
				mapping: 'zzz'
			},{
				name: 'registrasi.idregdet',
				mapping: 'idregdet'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'pasien.alamat',
				mapping: 'xxx'
			},{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'idstpasien',
				mapping: 'idstpasien'
			},{
				name: 'keluhan',
				mapping: 'keluhan'
			},{
				name: 'tglrencanakeluar',
				mapping: 'tglrencanakeluar'
			},{
				name: 'catatanrencanakeluar',
				mapping: 'catatanrencanakeluar'
			},{
				name: 'nmkerabat',
				mapping: 'nmkerabat'
			},{
				name: 'notelpkerabat',
				mapping: 'notelpkerabat'
			},{
				name: 'umurtahun',
				mapping: 'umurtahun'
			},{
				name: 'umurbulan',
				mapping: 'umurbulan'
			},{
				name: 'umurhari',
				mapping: 'umurhari'
			},{
				name: 'idcaradatang',
				mapping: 'idcaradatang'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'idbagiankirim',
				mapping: 'idbagiankirim'
			},{
				name: 'nmdokterkirim',
				mapping: 'nmdokterkirim'
			},{
				name: 'keluhan',
				mapping: 'keluhan'
			},{
				name: 'nmklsrawat',
				mapping: 'nmklsrawat'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'kdkamar',
				mapping: 'kdkamar'
			},{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idstbed',
				mapping: 'idstbed'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmkerabat',
				mapping: 'nmkerabat'
			},{
				name: 'notelpkerabat',
				mapping: 'notelpkerabat'
			},{
				name: 'idhubkeluarga',
				mapping: 'idhubkeluarga'
			},{
				name: 'catatanreg',
				mapping: 'catatanreg'
			},{
				name:'klsrawat.kdklsrawat',
				mapping:'kelastarif'

			},{
				name:'idklsrawat',
				mapping: 'idklsrawat'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name:'idregdet',
				mapping: 'idregdet'
			}]
		});
		return master;
	}
	
//=======================================================

	function dm_po(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'purchaseorder_controller/get_po',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			baseParams:{
				key:'0'
			},
			fields: [{
				name: 'nopo',
				mapping: 'nopo',
			},{
				name: 'tglpo',
				mapping: 'tglpo'
			},{
				name: 'idjnspp',
				mapping: 'idjnspp'
			},{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'tglpp',
				mapping: 'tglpp'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'idsypembayaran',
				mapping: 'idsypembayaran'
			},{
				name: 'nmsypembayaran',
				mapping: 'nmsypembayaran'
			},{
				name: 'idjnspembayaran',
				mapping: 'idjnspembayaran'
			},{
				name: 'nmjnspembayaran',
				mapping: 'nmjnspembayaran'
			},{
				name: 'tglpengiriman',
				mapping: 'tglpengiriman'
			},{
				name: 'idstpo',
				mapping: 'idstpo'
			},{
				name: 'nmstpo',
				mapping: 'nmstpo'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'bpb',
				mapping: 'bpb'
			},{
				name: 'ketpo',
				mapping: 'ketpo'
			},{
				name: 'iddiskon',
				mapping: 'iddiskon'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'ppnrp',
				mapping: 'ppnrp'
			},{
				name: 'totalpo',
				mapping: 'totalpo'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'approval1',
				mapping: 'approval1'
			},{
				name: 'approval2',
				mapping: 'approval2'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'idhrgbrgsup',
				mapping: 'idhrgbrgsup'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'kdsupplier',
				mapping : 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping : 'nmsupplier'
			},{
				name: 'nmstpo',
				mapping: 'nmstpo'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'nobayar',
				mapping: 'nobayar'
			},{
				name: 'stlunas',
				mapping: 'stlunas'
			},{
				name: 'tgljatuhtempo',
				mapping: 'tgljatuhtempo'
			},{
				name: 'idstkontrabon',
				mapping: 'idstkontrabon'
			},{
				name: 'nmstkontrabon',
				mapping: 'nmstkontrabon'
			}]
		});
		return master;
	}
	
//=======================================================


//======================================================= 
	function dm_vregistrasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'vregistrasi_controller/get_vregistrasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'jamreg',
				mapping: 'jamreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'tgllahirp',
				mapping: 'tgllahirp'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'alamatp',
				mapping: 'alamatp'
			},{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idstpasien',
				mapping: 'idstpasien'
			},{
				name: 'keluhan',
				mapping: 'keluhan'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'jamkeluar',
				mapping: 'jamkeluar'
			},{
				name: 'tglrencanakeluar',
				mapping: 'tglrencanakeluar'
			},{
				name: 'tglrencanakeluar',
				mapping: 'tglrencanakeluar'
			},{
				name: 'catatanrencanakeluar',
				mapping: 'catatanrencanakeluar'
			},{
				name: 'catatanr',
				mapping: 'catatanr'
			},{
				name: 'nmkerabat',
				mapping: 'nmkerabat'
			},{
				name: 'notelpkerabat',
				mapping: 'notelpkerabat'
			},{
				name: 'umurtahun',
				mapping: 'umurtahun'
			},{
				name: 'umurbulan',
				mapping: 'umurbulan'
			},{
				name: 'umurhari',
				mapping: 'umurhari'
			},{
				name: 'idcaradatang',
				mapping: 'idcaradatang'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'idbagiankirim',
				mapping: 'idbagiankirim'
			},{
				name: 'nmbagiankirim',
				mapping: 'nmbagiankirim'
			},{
				name: 'nmdokterkirim',
				mapping: 'nmdokterkirim'
			},{
				name: 'nmdokterkirimdlm',
				mapping: 'nmdokterkirimdlm'
			},{
				name: 'nmklsrawat',
				mapping: 'nmklsrawat'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'kdkamar',
				mapping: 'kdkamar'
			},{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idstbed',
				mapping: 'idstbed'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idhubkeluarga',
				mapping: 'idhubkeluarga'
			},{
				name:'idklsrawat',
				mapping: 'idklsrawat'
			},{
				name:'nonota',
				mapping: 'nonota'
			},{
				name:'nona',
				mapping: 'nona'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'nmdaerah',
				mapping: 'nmdaerah'
			},{
				name: 'noantrian',
				mapping: 'noantrian'
			},{
				name: 'nmcaradatang',
				mapping: 'nmcaradatang'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'diskonr',
				mapping: 'diskonr'
			},{
				name: 'diskonri',
				mapping: 'diskonri'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'uangri',
				mapping: 'uangri'
			},{
				name: 'idregdettransfer',
				mapping: 'idregdettransfer'
			},{
				name: 'idstpelayanan',
				mapping: 'idstpelayanan'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'catatannota',
				mapping: 'catatannota'
			},{
				name: 'kdsbtnm',
				mapping: 'kdsbtnm'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'idstkeluar',
				mapping: 'idstkeluar'
			},{
				name: 'idcarakeluar',
				mapping: 'idcarakeluar'
			},{
				name: 'nohpp',
				mapping: 'nohpp'
			},{
				name: 'notelpp',
				mapping: 'notelpp'
			},{
				name: 'nmibu',
				mapping: 'nmibu'
			},{
				name: 'alamatp',
				mapping: 'alamatp'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'tglmasuk',
				mapping: 'tglmasuk'
			},{
				name: 'jammasuk',
				mapping: 'jammasuk'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'idbed',
				mapping: 'idbed'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'intvday',
				mapping: 'intvday'
			},{
				name: 'stplafond',
				mapping: 'stplafond'
			},{
				name: 'stplafondna',
				mapping: 'stplafondna'
			},{
				name: 'catatand',
				mapping: 'catatand'
			},{
				name: 'catatands',
				mapping: 'catatands'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'jamkuitansi',
				mapping: 'jamkuitansi'
			}]
		});
		return master;
	}	
	
//======================================================= 
	function dm_vregistrasi_rj(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'vregistrasi_controller/get_vregistrasi_rj',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'cttdiagnosa',
				mapping: 'cttdiagnosa'
			},{
				name: 'foto_pasien',
				mapping: 'foto_pasien'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'diskonr',
				mapping: 'diskonr'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'idstpelayanan',
				mapping: 'idstpelayanan'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'catatannota',
				mapping: 'catatannota'
			},{
				name: 'catatand',
				mapping: 'catatand'
			},{
				name: 'tgllahirp',
				mapping: 'tgllahirp'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'stplafond',
				mapping: 'stplafond'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			}]
		});
		return master;
	}	
		
//======================================================= 
	function dm_vregugd(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'vregistrasi_controller/get_vregugd',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'jamreg',
				mapping: 'jamreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'idbagiankirim',
				mapping: 'idbagiankirim'
			},{
				name: 'nmbagiankirim',
				mapping: 'nmbagiankirim'
			},{
				name: 'nmdokterkirim',
				mapping: 'nmdokterkirim'
			},{
				name: 'nmdokterkirimdlm',
				mapping: 'nmdokterkirimdlm'
			},{
				name: 'nmklsrawat',
				mapping: 'nmklsrawat'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'kdkamar',
				mapping: 'kdkamar'
			},{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idstbed',
				mapping: 'idstbed'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'tglmasuk',
				mapping: 'tglmasuk'
			},{
				name: 'jammasuk',
				mapping: 'jammasuk'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'idbed',
				mapping: 'idbed'
			}]
		});
		return master;
	}
	
//======================================================= 
	function dm_vregistrasipaspulang(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'vregistrasi_controller/get_vregistrasipaspulang',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'nona',
				mapping: 'nona'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'diskonr',
				mapping: 'diskonr'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'idstpelayanan',
				mapping: 'idstpelayanan'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'jamkuitansi',
				mapping: 'jamkuitansi'
			},{
				name: 'catatannota',
				mapping: 'catatannota'
			},{
				name: 'catatands',
				mapping: 'catatands'
			},{
				name: 'tgllahirp',
				mapping: 'tgllahirp'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'jamkeluar',
				mapping: 'jamkeluar'
			},{
				name: 'stplafondna',
				mapping: 'stplafondna'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'deposit',
				mapping: 'deposit'
			}]
		});
		return master;
	}

//======================================================= 
	function dm_vregistrasifri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'vregistrasi_controller/get_vregistrasifri',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'tgllahirp',
				mapping: 'tgllahirp'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'catatannota',
				mapping: 'catatannota'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			}]
		});
		return master;
	}

//=======================================================
	function dm_pindahruangan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'pindahruangan_controller/get_registrasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'normas',
				mapping: 'normas'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'jamreg',
				mapping: 'jamreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'tgllahirp',
				mapping: 'tgllahirp'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'alamatp',
				mapping: 'alamatp'
			},{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idstpasien',
				mapping: 'idstpasien'
			},{
				name: 'keluhan',
				mapping: 'keluhan'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'jamkeluar',
				mapping: 'jamkeluar'
			},{
				name: 'tglrencanakeluar',
				mapping: 'tglrencanakeluar'
			},{
				name: 'tglrencanakeluar',
				mapping: 'tglrencanakeluar'
			},{
				name: 'catatanrencanakeluar',
				mapping: 'catatanrencanakeluar'
			},{
				name: 'catatanr',
				mapping: 'catatanr'
			},{
				name: 'nmkerabat',
				mapping: 'nmkerabat'
			},{
				name: 'notelpkerabat',
				mapping: 'notelpkerabat'
			},{
				name: 'umurtahun',
				mapping: 'umurtahun'
			},{
				name: 'umurbulan',
				mapping: 'umurbulan'
			},{
				name: 'umurhari',
				mapping: 'umurhari'
			},{
				name: 'idcaradatang',
				mapping: 'idcaradatang'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'idbagiankirim',
				mapping: 'idbagiankirim'
			},{
				name: 'nmbagiankirim',
				mapping: 'nmbagiankirim'
			},{
				name: 'nmdokterkirim',
				mapping: 'nmdokterkirim'
			},{
				name: 'nmdokterkirimdlm',
				mapping: 'nmdokterkirimdlm'
			},{
				name: 'nmklsrawat',
				mapping: 'nmklsrawat'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'kdkamar',
				mapping: 'kdkamar'
			},{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idstbed',
				mapping: 'idstbed'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idhubkeluarga',
				mapping: 'idhubkeluarga'
			},{
				name:'idklsrawat',
				mapping: 'idklsrawat'
			},{
				name:'nonota',
				mapping: 'nonota'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'nmdaerah',
				mapping: 'nmdaerah'
			},{
				name: 'noantrian',
				mapping: 'noantrian'
			},{
				name: 'nmcaradatang',
				mapping: 'nmcaradatang'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'diskonr',
				mapping: 'diskonr'
			},{
				name: 'diskonri',
				mapping: 'diskonri'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'uangri',
				mapping: 'uangri'
			},{
				name: 'idregdettransfer',
				mapping: 'idregdettransfer'
			},{
				name: 'idstpelayanan',
				mapping: 'idstpelayanan'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'catatannota',
				mapping: 'catatannota'
			},{
				name: 'kdsbtnm',
				mapping: 'kdsbtnm'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'idstkeluar',
				mapping: 'idstkeluar'
			},{
				name: 'idcarakeluar',
				mapping: 'idcarakeluar'
			},{
				name: 'nohpp',
				mapping: 'nohpp'
			},{
				name: 'notelpp',
				mapping: 'notelpp'
			},{
				name: 'nmibu',
				mapping: 'nmibu'
			},{
				name: 'alamatp',
				mapping: 'alamatp'
			},{
				name: 'tglmasuk',
				mapping: 'tglmasuk'
			},{
				name: 'jammasuk',
				mapping: 'jammasuk'
			},{
				name: 'intvday',
				mapping: 'intvday'
			},{
				name: 'idbed',
				mapping: 'idbed'
			}]
		});
		return master;
	}

	function dm_pindahruangans(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'pindahruangan_controller/get_registrasipindah',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'normas',
				mapping: 'normas'
			},{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'jamreg',
				mapping: 'jamreg'
			},{
				name: 'idshift',
				mapping: 'idshift'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'tgllahirp',
				mapping: 'tgllahirp'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'alamatp',
				mapping: 'alamatp'
			},{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idstpasien',
				mapping: 'idstpasien'
			},{
				name: 'keluhan',
				mapping: 'keluhan'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'jamkeluar',
				mapping: 'jamkeluar'
			},{
				name: 'tglrencanakeluar',
				mapping: 'tglrencanakeluar'
			},{
				name: 'tglrencanakeluar',
				mapping: 'tglrencanakeluar'
			},{
				name: 'catatanrencanakeluar',
				mapping: 'catatanrencanakeluar'
			},{
				name: 'catatanr',
				mapping: 'catatanr'
			},{
				name: 'nmkerabat',
				mapping: 'nmkerabat'
			},{
				name: 'notelpkerabat',
				mapping: 'notelpkerabat'
			},{
				name: 'umurtahun',
				mapping: 'umurtahun'
			},{
				name: 'umurbulan',
				mapping: 'umurbulan'
			},{
				name: 'umurhari',
				mapping: 'umurhari'
			},{
				name: 'idcaradatang',
				mapping: 'idcaradatang'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'idbagiankirim',
				mapping: 'idbagiankirim'
			},{
				name: 'nmbagiankirim',
				mapping: 'nmbagiankirim'
			},{
				name: 'nmdokterkirim',
				mapping: 'nmdokterkirim'
			},{
				name: 'nmdokterkirimdlm',
				mapping: 'nmdokterkirimdlm'
			},{
				name: 'nmklsrawat',
				mapping: 'nmklsrawat'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'kdkamar',
				mapping: 'kdkamar'
			},{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idstbed',
				mapping: 'idstbed'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idhubkeluarga',
				mapping: 'idhubkeluarga'
			},{
				name:'idklsrawat',
				mapping: 'idklsrawat'
			},{
				name:'nonota',
				mapping: 'nonota'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'nmdaerah',
				mapping: 'nmdaerah'
			},{
				name: 'noantrian',
				mapping: 'noantrian'
			},{
				name: 'nmcaradatang',
				mapping: 'nmcaradatang'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'diskonr',
				mapping: 'diskonr'
			},{
				name: 'diskonri',
				mapping: 'diskonri'
			},{
				name: 'uangr',
				mapping: 'uangr'
			},{
				name: 'uangri',
				mapping: 'uangri'
			},{
				name: 'idregdettransfer',
				mapping: 'idregdettransfer'
			},{
				name: 'idstpelayanan',
				mapping: 'idstpelayanan'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'catatannota',
				mapping: 'catatannota'
			},{
				name: 'kdsbtnm',
				mapping: 'kdsbtnm'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'idstkeluar',
				mapping: 'idstkeluar'
			},{
				name: 'idcarakeluar',
				mapping: 'idcarakeluar'
			},{
				name: 'nohpp',
				mapping: 'nohpp'
			},{
				name: 'notelpp',
				mapping: 'notelpp'
			},{
				name: 'nmibu',
				mapping: 'nmibu'
			},{
				name: 'alamatp',
				mapping: 'alamatp'
			},{
				name: 'tglmasuk',
				mapping: 'tglmasuk'
			},{
				name: 'jammasuk',
				mapping: 'jammasuk'
			},{
				name: 'intvday',
				mapping: 'intvday'
			},{
				name: 'idbed',
				mapping: 'idbed'
			}]
		});
		return master;
	}


//=======================================================
	function dm_grid(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'c_utility/g_MN',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "idmenu",
				mapping: "idmenu"
			},{
				name: "kdmenu",
				mapping: "kdmenu"
			},
			{
				name: "nmmenu",
				mapping: "nmmenu"
			},
			{
				name: "deskripsi",
				mapping: "deskripsi"
			},
			{
				name: "nmjnshirarki",
				mapping: "nmjnshirarki"
			},
			{
				name: "nmstatus",
				mapping: "nmstatus"
			},
			{
				name: "nmsubmenu",
				mapping: "nmsubmenu"
			}, //
			{
				name: "url",
				mapping: "url"
			}, //
			{
				name: "gambar",
				mapping: "gambar"
			}]
		});
		return master;
	}


//=======================================================
	function dm_brgbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'brgbagian_controller/get_brgbagian',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "idbagian",
				mapping: "idbagian"
			},{
				name: "kdbrg",
				mapping: "kdbrg"
			},
			{
				name: "nmbagian",
				mapping: "nmbagian"
			},
			{
				name: "nmbrg",
				mapping: "nmbrg"
			},
			{
				name: "stoknowbagian",
				mapping: "stoknowbagian"
			},
			{
				name: "stokminbagian",
				mapping: "stokminbagian"
			},
			{
				name: "stokmaxbagian",
				mapping: "stokmaxbagian"
			},
			{
				name: "tarif",
				mapping: "tarif"
			},
			{
				name: "nmsatuan",
				mapping: "nmsatuan"
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_gridotoritas(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'menu_controller/g_OT',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "kdmenu",
				mapping: "kdmenu"
			},
			{
				name: "nmmenu",
				mapping: "nmmenu"
			},
			{
				name: "deskripsi",
				mapping: "deskripsi"
			},
			{
				name: "idjnshirarki",
				mapping: "idjnshirarki"
			},
			{
				name: "idstatus",
				mapping: "idstatus"
			},
			{
				name: "men_idmenu",
				mapping: "men_idmenu"
			},
			{
				name: "user_aktif",
				mapping: "user_aktif",
				type: 'bool'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jdashboard(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'jdashboard_controller/gridM_dash',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnsdashboard',
				mapping: 'idjnsdashboard'
			},{
				name: 'kdjnsdashboard',
				mapping: 'kdjnsdashboard'
			},
			{
				name: 'nmjnsdashboard',
				mapping: 'nmjnsdashboard'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_klppengguna(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'klppengguna_controller/g_JKP',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "idklppengguna",
				mapping: "idklppengguna"
			},{
				name: "kdklppengguna",
				mapping: "kdklppengguna"
			},
			{
				name: "nmklppengguna",
				mapping: "nmklppengguna"
			},
			{
				name: "deskripsi",
				mapping: "deskripsi"
			},
			{
				name: "idjnsdashboard",
				mapping: "idjnsdashboard"
			},
			{
				name: "idstatus",
				mapping: "idstatus"
			},
			{
				name: "nmjnsdashboard",
				mapping: "nmjnsdashboard"
			},
			{
				name: "nmstatus",
				mapping: "nmstatus"
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jpengguna(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'jpengguna_controller/grid_jpengguna',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idjnspengguna',
				mapping: 'idjnspengguna'
			},
			{
				name: 'kdjnspengguna',
				mapping: 'kdjnspengguna'
			},
			{
				name: 'nmjnspengguna',
				mapping: 'nmjnspengguna'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pengguna(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'pengguna_controller/get_pengguna',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "userid",
				mapping: "userid"
			},
			{
				name: "password",
				mapping: "password"
			},
			{
				name: "nmlengkap",
				mapping: "nmlengkap"
			},
			{
				name: "email",
				mapping: "email"
			},
			{
				name: "nohp",
				mapping: "nohp"
			},
			{
				name: "noref",
				mapping: "noref"
			},
			{
				name: "idklppengguna",
				mapping: "idklppengguna"
			},
			{
				name: "nmklppengguna",
				mapping: "nmklppengguna"
			},
			{
				name: "idjnspengguna",
				mapping: "idjnspengguna"
			},
			{
				name: "nmjnspengguna",
				mapping: "nmjnspengguna"
			},
			{
				name: "foto",
				mapping: "foto"
			},
			{
				name: "idstatus",
				mapping: "idstatus"
			},
			{
				name: "nmstatus",
				mapping: "nmstatus"
			},
			{
				name: "tgldaftar",
				mapping: "tgldaftar"
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_logpengguna(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'loguser_controller/gridL_user',
				method: 'POST'
			}),
			baseParams: {},
			root: 'data',
			totalProperty: 'results',
			remoteSort: true,
			autoLoad: true,
			loadonce: false,
			fields: [{
				name: "idloguser",
				mapping: "idloguser"
			},
			{
				name: "userid",
				mapping: "userid"
			},
			{
				name: "tglmsk",
				mapping: "tglmsk"
			},
			{
				name: "tglklr",
				mapping: "tglklr"
			},
			{
				name: "jammsk",
				mapping: "jammsk"
			},
			{
				name: "jamklr",
				mapping: "jamklr"
			},
			{
				name: "catatan",
				mapping: "catatan"
			}]
		});
		return master;
	}
	
//=======================================================
function dm_carakeluar(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'jcarakeluar_controller/get_jcarakeluar',
				method: 'POST'
		}),
			baseParams: {},
			root: 'data',
			totalProperty: 'results',
			remoteSort: true,
			autoLoad: true,
			loadonce: false,
			fields: [{
				name: "idcarakeluar",
				mapping: 'idcarakeluar'
			},{
				name: "kdcarakeluar",
				mapping: 'kdcarakeluar'
			},{
				name: "nmcarakeluar",
				mapping: "nmcarakeluar",
			}]
	});
	return master;
}


//=======================================================

	function dm_stkeluar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'stkeluar_controller/get_stkeluar',
				method: 'POST'
			}),
			baseParams: {},
			root: 'data',
			totalProperty: 'results',
			remoteSort: true,
			autoLoad: true,
			loadonce: false,
			fields: [{
				name: "idstkeluar",
				mapping: "idstkeluar"
			},{
				name: "kdstkeluar",
				mapping: "kdstkeluar"
			},{
				name: "nmstkeluar",
				mapping: "nmstkeluar"
			}]
		});
		return master;
	}
//=======================================================

	function dm_reservasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'reservasiRJ_controller/get_reservasi',
				method: 'POST'
			}),
			baseParams: {},
			root: 'data',
			totalProperty: 'results',
			remoteSort: true,
			autoLoad: true,
			loadonce: false,
			fields: [{
				name: "idreservasi",
				mapping: "idreservasi"
			},{
				name: "tglreservasi",
				mapping: "tglreservasi"
			},{
				name: "jamreservasi",
				mapping: "jamreservasi"
			},{
				name: "idshift",
				mapping: "idshift"
			},{
				name: "norm",
				mapping: "norm",
				type: 'int'
			},{
				name: "nmpasien",
				mapping: "nmpasien"
			},{
				name: "nohp",
				mapping: "nohp"
			},{
				name: "notelp",
				mapping: "notelp"
			},{
				name: "email",
				mapping : "email"
			},{
				name: "iddokter",
				mapping: "iddokter",
			},{
				name: "idbagian",
				mapping: "idbagian"
			},{
				name: "idstreservasi",
				mapping: "idstreservasi"
			},{
				name: "idregdet",
				mapping: "idregdet"
			},{
				name: "noantrian",
				mapping: "noantrian"
			},{
				name: "userinput",
				mapping: "userinput"
			},{
				name: "tglinput",
				mapping: "tglinput"
			},{
				name: "idstposisipasien",
				mapping: "idstposisipasien"
			},{
				name: "nmstreservasi",
				mapping: "nmstreservasi"
			},{
				name: 'namaposisipasien',
				mapping: 'namaposisipasien'
			},{
				name: "noreg",
				mapping: "noreg"
			}]
		});
		return master;
	}

//=======================================================
	function dm_dokterrawat(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'dokterygmerawat_controller/get_dokterrawat',
				method: 'POST'
			}),
			baseParams: {},
			root: 'data',
			totalProperty: 'results',
			remoteSort: true,
			autoLoad: true,
			loadonce: false,
			fields: [{
				name: "iddokterrawat",
				mapping: "iddokterrawat"
			},{
				name: "idregdet",
				mapping: "idregdet"
			},{
				name: "idstdokterrawat",
				mapping: "idstdokterrawat"
			},{
				name: "tglmulai",
				mapping: "tglmulai"
			},{
				name: "jammulai",
				mapping: "tglakhir"
			},{
				name: "tglakhir",
				mapping: "jamakhir"
			},{
				name: "diagnosa",
				mapping: "diagnosa"
			}]
		});
		return master;
	}
		
//=======================================================
	function dm_kodifikasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kodifikasi_controller/get_kodifikasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkodifikasi',
				mapping: 'idkodifikasi'
			},{
				name: 'tglkodifikasi',
				mapping: 'tglkodifikasi'
			},{
				name: 'jamkodifikasi',
				mapping: 'jamkodifikasi'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'idkematian',
				mapping: 'idkematian'
			},{
				name: 'idtindaklanjut',
				mapping: 'idtindaklanjut'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'idpenyakit',
				mapping: 'idpenyakit'
			},{
				name: 'idjnsdiagnosa',
				mapping: 'idjnsdiagnosa'
			},{
				name: 'idstpenyakit',
				mapping: 'idstpenyakit'
			},{
				name: 'idmappenyakit',
				mapping: 'idmappenyakit'
			},{
				name: 'kdpenyakit',
				mapping: 'kdpenyakit'
			},{
				name: 'nmpenyakit',
				mapping: 'nmpenyakit'
			},{
				name: 'nmpenyakiteng',
				mapping: 'nmpenyakiteng'
			},{
				name: 'nmjnsdiagnosa',
				mapping: 'nmjnsdiagnosa'
			},{
				name: 'nmjstpenyakit',
				mapping: 'nmjstpenyakit'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_orderruangan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'orderruangan_controller/get_orderRuangan',
				method: 'POST'
			}),
			baseParams: {},
			root: 'data',
			totalProperty: 'results',
			remoteSort: true,
			autoLoad: true,
			loadonce: false,
			fields: [{
				name: "nooruangan",
				mapping: "nooruangan"
			},
			{
				name: "tgloruangan",
				mapping: "tgloruangan"
			},
			{
				name: "jamoruangan",
				mapping: "jamoruangan"
			},
			{
				name: "idklsrawat",
				mapping: "idklsrawat"
			},
			{
				name: "idregdet",
				mapping: "idregdet"
			},
			{
				name: "userid",
				mapping: "userid"
			},
			{
				name: "idbagian",
				mapping: "idbagian"
			},
			{
				name: "idkamar",
				mapping: "idkamar"
			},
			{
				name: "idbed",
				mapping: "idbed"
			},
			{
				name: "idhubkeluarga",
				mapping: "idhubkeluarga"
			},
			{
				name: "idklstarif",
				mapping: "idklstarif"
			},
			{
				name: "tglrencanamasuk",
				mapping: "tglrencanamasuk"
			},
			{
				name: "jamrencanamasuk",
				mapping: "jamrencanamasuk"
			},
			{
				name: "nmkerabat",
				mapping: "nmkerabat"
			},
			{
				name: "alasanpindah",
				mapping: "alasanpindah"
			},
			{
				name: "catatan",
				mapping: "catatan"
			}]
		});
		return master;
	}
	
// Kasir
//=======================================================
	function dm_bukakasir(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bukakasir_controller/get_bukakasir',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'idstkasir',
				mapping: 'idstkasir'
			},{
				name: 'nmstkasir',
				mapping: 'nmstkasir'
			},{
				name: 'idshiftbuka',
				mapping: 'idshiftbuka'
			},{
				name: 'nmshiftbuka',
				mapping: 'nmshiftbuka'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'tglbuka',
				mapping: 'tglbuka'
			},{
				name: 'jambuka',
				mapping: 'jambuka'
			},{
				name: 'saldoawal',
				mapping: 'saldoawal'
			},{
				name: 'catatanbuka',
				mapping: 'catatanbuka'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
		});
		return master;
	}
	
	function dm_cbbagiandikasir(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bukakasir_controller/get_cbbagiandikasir',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tutupkasir(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tutupkasir_controller/get_tutupkasir',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'tglbuka',
				mapping: 'tglbuka'
			},{
				name: 'jambuka',
				mapping: 'jambuka'
			},{
				name: 'idshiftbuka',
				mapping: 'idshiftbuka'
			},{
				name: 'nmshiftbuka',
				mapping: 'nmshiftbuka'
			},{
				name: 'saldoawal',
				mapping: 'saldoawal'
			},{
				name: 'catatanbuka',
				mapping: 'catatanbuka'
			},{
				name: 'tgltutup',
				mapping: 'tgltutup'
			},{
				name: 'jamtutup',
				mapping: 'jamtutup'
			},{
				name: 'idshifttutup',
				mapping: 'idshifttutup'
			},{
				name: 'nmshifttutup',
				mapping: 'nmshifttutup'
			},{
				name: 'saldoakhir',
				mapping: 'saldoakhir'
			},{
				name: 'selisih',
				mapping: 'selisih'
			},{
				name: 'catatantutup',
				mapping: 'catatantutup'
			},{
				name: 'idstkasir',
				mapping: 'idstkasir'
			},{
				name: 'nmstkasir',
				mapping: 'nmstkasir'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
		});
		return master;
	}
	
	function dm_lappenerimaankas(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'tutupkasir_controller/get_vlappenerimaankas',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'jamkuitansi',
				mapping: 'jamkuitansi'
			},{
				name: 'idshiftkuitansi',
				mapping: 'idshiftkuitansi'
			},{
				name: 'nmshiftkuitansi',
				mapping: 'nmshiftkuitansi'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'idstkuitansi',
				mapping: 'idstkuitansi'
			},{
				name: 'nmstkuitansi',
				mapping: 'nmstkuitansi'
			}]
		});
		return master;
	}
	
	function dm_carabayarbukakasir(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tutupkasir_controller/get_carabayarbukakasir',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nmcarabayar',
				mapping: 'nmcarabayar'
			}]
		});
		return master;
	}
	
	function dm_carabayartunai(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tutupkasir_controller/get_carabayartunai',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idcarabayar',
				mapping: 'idcarabayar'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_tunai',
				mapping: 'total_tunai'
			}]
		});
		return master;
	}
	
	function dm_carabayardebit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tutupkasir_controller/get_carabayardebit',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idcarabayar',
				mapping: 'idcarabayar'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_debit',
				mapping: 'total_debit'
			}]
		});
		return master;
	}
	
	function dm_carabayarkredit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tutupkasir_controller/get_carabayarkredit',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idcarabayar',
				mapping: 'idcarabayar'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_kredit',
				mapping: 'total_kredit'
			}]
		});
		return master;
	}
	
	function dm_carabayarvocher(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tutupkasir_controller/get_carabayarvocher',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'idcarabayar',
				mapping: 'idcarabayar'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_vocher',
				mapping: 'total_vocher'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pengeluarankas(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pengeluarankas_controller/get_pengeluarankas',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdjurnal',
				mapping: 'kdjurnal'
			},{
				name: 'tgltransaksi',
				mapping: 'tgltransaksi'
			},{
				name: 'tgljurnal',
				mapping: 'tgljurnal'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'idjnstransaksi',
				mapping: 'idjnstransaksi'
			},{
				name: 'nmjnstransaksi',
				mapping: 'nmjnstransaksi'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'nominal',
				mapping: 'nominal'
			},{
				name: 'totnominal',
				mapping: 'totnominal'
			},{
				name: 'noreff',
				mapping: 'noreff'
			},{
				name: 'nokasir',
				mapping: 'nokasir'
			}]
		});
		return master;
	}

//=======================================================
	function dm_pengeluarankasdetail(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pengeluarankas_controller/get_pengeluarankasdetail',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdjurnal',
				mapping: 'kdjurnal'
			},{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			},{
				name: 'idklpakun',
				mapping: 'idklpakun'
			},{
				name: 'nmklpakun',
				mapping: 'nmklpakun'
			},{
				name: 'debit',
				mapping: 'debit'
			},{
				name: 'kredit',
				mapping: 'kredit'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pengeluarankasdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pengeluarankas_controller/get_pengeluarankasdet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdjurnal',
				mapping: 'kdjurnal'
			},{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			},{
				name: 'idklpakun',
				mapping: 'idklpakun'
			},{
				name: 'nmklpakun',
				mapping: 'nmklpakun'
			},{
				name: 'debit',
				mapping: 'debit'
			},{
				name: 'kredit',
				mapping: 'kredit'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_bagtahunan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pengeluarankas_controller/get_bagtahunan',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tahun',
				mapping: 'tahun'
			},{
				name: 'idakun',
				mapping: 'idakun'
			},{
				name: 'idklpakun',
				mapping: 'idklpakun'
			},{
				name: 'nmklpakun',
				mapping: 'nmklpakun'
			},{
				name: 'kdakun',
				mapping: 'kdakun'
			},{
				name: 'nmakun',
				mapping: 'nmakun'
			},{
				name: 'nmparent',
				mapping: 'nmparent'
			}]
		});
		return master;
	}
		
//=======================================================
	function dm_returdeposit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returdeposit_controller/get_returdeposit',
				method: 'POST'
			}),
			baseParams:{
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdjurnal',
				mapping: 'kdjurnal'
			},{
				name: 'tgltransaksi',
				mapping: 'tgltransaksi'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'idbed',
				mapping: 'idbed'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'nona',
				mapping: 'nona'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'tottransaksi',
				mapping: 'tottransaksi'
			},{
				name: 'totdeposit',
				mapping: 'totdeposit'
			},{
				name: 'nominal',
				mapping: 'nominal'
			},{
				name: 'penerima',
				mapping: 'penerima'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
	
	// ============== data master jurnal ================//
	function dm_bukubesar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bukubesar_controller/get_bukubesar',
				method: 'POST'
			}),
			baseParams:{
				tahun: '',
				bulan: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'tahun',
			    mapping: 'tahun',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'saldoawal',
			    mapping: 'saldoawal',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'calc_saldoawal',
			    mapping: 'calc_saldoawal',
			}, {
			    name: 'totaldebit',
			    mapping: 'totaldebit',
			}, {
			    name: 'totalkredit',
			    mapping: 'totalkredit',
			}, {
			    name: 'saldoakhir',
			    mapping: 'saldoakhir',
			}]
		});
		return master;
	}	

	function dm_bukubesar_bank(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bukubesar_controller/get_bukubesar_bank',
				method: 'POST'
			}),
			baseParams:{
				tahun: '',
				bulan: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idbank',
			    mapping: 'idbank',
			}, {
			    name: 'nmbank',
			    mapping: 'nmbank',
			}, {
			    name: 'tot_debit',
			    mapping: 'tot_debit',
			}, {
			    name: 'tot_kredit',
			    mapping: 'tot_kredit',
			}]
		});
		return master;
	}

	function dm_bukujurnal(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bukubesar_controller/get_bukujurnal',
				method: 'POST'
			}),
			baseParams:{
				tahun: '',
				bulan: '',
				idakun: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'tahun',
			    mapping: 'tahun',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'noreff_jurnaldet',
			    mapping: 'noreff_jurnaldet',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}, {
			    name: 'klpjurnal',
			    mapping: 'klpjurnal',
			}, {
			    name: 'nojurnal',
			    mapping: 'nojurnal',
			}, {
			    name: 'idjnsjurnal',
			    mapping: 'idjnsjurnal',
			}, {
			    name: 'idbagian',
			    mapping: 'idbagian',
			}, {
			    name: 'tgltransaksi',
			    mapping: 'tgltransaksi',
			}, {
			    name: 'tgljurnal',
			    mapping: 'tgljurnal',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'noreff_jurnal',
			    mapping: 'noreff_jurnal',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'tglinput',
			    mapping: 'tglinput',
			}, {
			    name: 'nominal',
			    mapping: 'nominal',
			}, {
			    name: 'idjnstransaksi',
			    mapping: 'idjnstransaksi',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}]
		});
		return master;
	}

	function dm_bukujurnal_bank(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'bukubesar_controller/get_bukujurnal_bank',
				method: 'POST'
			}),
			baseParams:{
				tahun: '',
				bulan: '',
				idbank: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'tahun',
			    mapping: 'tahun',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'noreff_jurnaldet',
			    mapping: 'noreff_jurnaldet',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}, {
			    name: 'klpjurnal',
			    mapping: 'klpjurnal',
			}, {
			    name: 'nojurnal',
			    mapping: 'nojurnal',
			}, {
			    name: 'idjnsjurnal',
			    mapping: 'idjnsjurnal',
			}, {
			    name: 'idbagian',
			    mapping: 'idbagian',
			}, {
			    name: 'tgltransaksi',
			    mapping: 'tgltransaksi',
			}, {
			    name: 'tgljurnal',
			    mapping: 'tgljurnal',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'noreff_jurnal',
			    mapping: 'noreff_jurnal',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'tglinput',
			    mapping: 'tglinput',
			}, {
			    name: 'nominal',
			    mapping: 'nominal',
			}, {
			    name: 'idjnstransaksi',
			    mapping: 'idjnstransaksi',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}]
		});
		return master;
	}

	function dm_aktiva_lancar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'neraca_controller/get_aktiva_lancar',
				method: 'POST'
			}),
			baseParams:{
				tahun: '',
				bulan: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakunparent',
			    mapping: 'idakunparent',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'idklpakun',
			    mapping: 'idklpakun',
			}, {
			    name: 'idjnsakun',
			    mapping: 'idjnsakun',
			}, {
			    name: 'saldo',
			    mapping: 'saldo',
			}]
		});
		return master;
	}

	function dm_aktiva_tetap(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'neraca_controller/get_aktiva_tetap',
				method: 'POST'
			}),
			baseParams:{
				tahun: '',
				bulan: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakunparent',
			    mapping: 'idakunparent',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'idklpakun',
			    mapping: 'idklpakun',
			}, {
			    name: 'idjnsakun',
			    mapping: 'idjnsakun',
			}, {
			    name: 'saldo',
			    mapping: 'saldo',
			}]
		});
		return master;
	}

	function dm_pasiva(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'neraca_controller/get_pasiva',
				method: 'POST'
			}),
			baseParams:{
				tahun: '',
				bulan: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakunparent',
			    mapping: 'idakunparent',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'idklpakun',
			    mapping: 'idklpakun',
			}, {
			    name: 'idjnsakun',
			    mapping: 'idjnsakun',
			}, {
			    name: 'saldo',
			    mapping: 'saldo',
			}]
		});
		return master;
	}

	function dm_modal(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'neraca_controller/get_modal',
				method: 'POST'
			}),
			baseParams:{
				tahun: '',
				bulan: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakunparent',
			    mapping: 'idakunparent',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'idklpakun',
			    mapping: 'idklpakun',
			}, {
			    name: 'idjnsakun',
			    mapping: 'idjnsakun',
			}, {
			    name: 'saldo',
			    mapping: 'saldo',
			}]
		});
		return master;
	}

	function dm_pendapatan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'labarugi_controller/get_pendapatan',
				method: 'POST'
			}),
			baseParams:{
				tglawal: '',
				tglakhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakunparent',
			    mapping: 'idakunparent',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'idklpakun',
			    mapping: 'idklpakun',
			}, {
			    name: 'idjnsakun',
			    mapping: 'idjnsakun',
			}, {
			    name: 'nominal',
			    mapping: 'nominal',
			}]
		});
		return master;
	}

	function dm_biaya(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'labarugi_controller/get_biaya',
				method: 'POST'
			}),
			baseParams:{
				tglawal: '',
				tglakhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakunparent',
			    mapping: 'idakunparent',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'idklpakun',
			    mapping: 'idklpakun',
			}, {
			    name: 'idjnsakun',
			    mapping: 'idjnsakun',
			}, {
			    name: 'nominal',
			    mapping: 'nominal',
			}]
		});
		return master;
	}

	function dm_akun_perkelompok(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'baganperkiraan_controller/get_akun_perkelompok',
				method: 'POST'
			}),
			baseParams:{
				idklpakun: '1'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakunparent',
			    mapping: 'idakunparent'
			},{
			    name: 'idakun',
			    mapping: 'idakun'
			},{
			    name: 'kdakun',
			    mapping: 'kdakun'
			},{
			    name: 'nmakun',
			    mapping: 'nmakun'
			},{
			    name: 'keterangan',
			    mapping: 'keterangan'
			},{
			    name: 'idklpakun',
			    mapping: 'idklpakun'
			},{
			    name: 'idjnsakun',
			    mapping: 'idjnsakun'
			},{
			    name: 'idstatus',
			    mapping: 'idstatus'
			},{
			    name: 'userid',
			    mapping: 'userid'
			},{
			    name: 'tglinput',
			    mapping: 'tglinput'
      }]
		});
		return master;
	}

	/* ======= JURNAL KHUSUS SUPPLIER (PEMBELIAN) =========*/
	function dm_jurnsupplier_beli(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_transaksi_supplier_beli',
				method: 'POST'
			}),
			baseParams:{
				tglpo: '',
				tglpo_akhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nopo',
			    mapping: 'nopo',
			}, {
			    name: 'tglpo',
			    mapping: 'tglpo',
			}, {
			    name: 'idjnspp',
			    mapping: 'idjnspp',
			}, {
			    name: 'kdsupplier',
			    mapping: 'kdsupplier',
			}, {
			    name: 'idstsetuju',
			    mapping: 'idstsetuju',
			}, {
			    name: 'ketpo',
			    mapping: 'ketpo',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'approval1',
			    mapping: 'approval1',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}, {
			    name: 'total_transaksi',
			    mapping: 'total_transaksi',
			}, {
			    name: 'diskon_bonus',
			    mapping: 'diskon_bonus',
			}, {
			    name: 'total_bayar',
			    mapping: 'total_bayar',
			}, {
			    name: 'nmsupplier',
			    mapping: 'nmsupplier',
			}]
		});
		return master;
	}

	function dm_jurnsupplier_belidet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_jtransaksi_supplier_belidet',
				method: 'POST'
			}),
			baseParams:{
				nopo: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
                    name: 'nopo',
                    mapping: 'nopo'
               },{
                    name: 'kdbrg',
                    mapping: 'kdbrg'
               },{
                    name: 'nmbrg',
                    mapping: 'nmbrg'
               },{
                    name: 'idsatuan',
                    mapping: 'idsatuan'
               },{
                    name: 'qty',
                    mapping: 'qty'
               },{
                    name: 'rasio',
                    mapping: 'rasio'
               },{
                    name: 'qtybonus',
                    mapping: 'qtybonus'
               },{
                    name: 'hargabeli',
                    mapping: 'hargabeli'
               },{
                    name: 'diskon',
                    mapping: 'diskon'
               },{
                    name: 'ppn',
                    mapping: 'ppn'
               },{
                    name: 'hargajual',
                    mapping: 'hargajual'
               },{
                    name: 'diskonrp',
                    mapping: 'diskonrp'
               },{
                    name: 'margin',
                    mapping: 'margin'
               },{
                    name: 'nourut',
                    mapping: 'nourut'
               },{
                    name: 'totbeli',
                    mapping: 'totbeli'
               },{
                    name: 'totdiskon',
                    mapping: 'totdiskon'
               },{
                    name: 'totbonus',
                    mapping: 'totbonus'
               },{
                    name: 'totbeli_diskon',
                    mapping: 'totbeli_diskon'
               },{
                    name: 'beban_ppn',
                    mapping: 'beban_ppn'
               },{
                    name: 'totbeli_diskon_ppn',
                    mapping: 'totbeli_diskon_ppn'
               },{
                    name: 'idakunbeli',
                    mapping: 'idakunbeli'
               },{
                    name: 'kdakunbeli',
                    mapping: 'kdakunbeli'
               },{
                    name: 'nmakunbeli',
                    mapping: 'nmakunbeli'
               },]
		});
		return master;
	}

	function dm_jurnsupplier_beli_jurnaling_persediaan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_jtransaksi_supplier_beli_jurnaling_persediaan',
				method: 'POST'
			}),
			baseParams:{
				nopo: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;
	}

	function dm_jurnsupplier_beli_jurnaling_biaya(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_jtransaksi_supplier_beli_jurnaling_biaya',
				method: 'POST'
			}),
			baseParams:{
				nopo: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;
	}

  /* ======= JURNAL KHUSUS SUPPLIER (PEMBAYARAN) =========*/
	function dm_jurnsupplier_bayar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_transaksi_supplier_bayar',
				method: 'POST'
			}),
			baseParams:{
				tglbayar: '',
				tglbayar_akhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nobayar',
			    mapping: 'nobayar',
			}, {
			    name: 'tglbayar',
			    mapping: 'tglbayar',
			}, {
			    name: 'nopo',
			    mapping: 'nopo',
			}, {
			    name: 'nobayar_nopo',
			    mapping: 'nobayar_nopo',
			}, {
			    name: 'kdsupplier',
			    mapping: 'kdsupplier',
			}, {
			    name: 'nmsupplier',
			    mapping: 'nmsupplier',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'approval',
			    mapping: 'approval',
			}, {
			    name: 'catatan',
			    mapping: 'catatan',
			}, {
			    name: 'total_bayar',
			    mapping: 'total_bayar',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}]
		});
		return master;
	}

	function dm_jurnsupplier_bayardet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_transaksi_supplier_bayardet',
				method: 'POST'
			}),
			baseParams:{
				nobayar: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nobayar',
			    mapping: 'nobayar',
			}, {
			    name: 'idjnspembayaran',
			    mapping: 'idjnspembayaran',
			}, {
			    name: 'nominal',
			    mapping: 'nominal',
			}, {
			    name: 'nokartu',
			    mapping: 'nokartu',
			}, {
			    name: 'tahun',
			    mapping: 'tahun',
			}, {
			    name: 'kdjnspembayaran',
			    mapping: 'kdjnspembayaran',
			}, {
			    name: 'nmjnspembayaran',
			    mapping: 'nmjnspembayaran',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}]
		});
		return master;		
	}
	
	function dm_jurnsupplier_bayar_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_jtransaksi_supplier_bayar_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				nobayar: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;
	}

	/* ======= JURNAL KHUSUS SUPPLIER (RETUR BARANG) =========*/
	
	function dm_jurnsupplier_retur(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_transaksi_supplier_retur',
				method: 'POST'
			}),
			baseParams:{
				tglretursupplier: '',
				tglretursupplier_akhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'noretursupplier',
			    mapping: 'noretursupplier',
			}, {
			    name: 'tglretursupplier',
			    mapping: 'tglretursupplier',
			}, {
			    name: 'jamretursupplier',
			    mapping: 'jamretursupplier',
			}, {
			    name: 'idbagian',
			    mapping: 'idbagian',
			}, {
			    name: 'kdsupplier',
			    mapping: 'kdsupplier',
			}, {
			    name: 'nmsupplier',
			    mapping: 'nmsupplier',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'idsttransaksi',
			    mapping: 'idsttransaksi',
			}, {
			    name: 'idstsetuju',
			    mapping: 'idstsetuju',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'tgljaminput',
			    mapping: 'tgljaminput',
			}, {
			    name: 'total_item_retur',
			    mapping: 'total_item_retur',
			}, {
			    name: 'total_nominal_retur',
			    mapping: 'total_nominal_retur',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}]
		});
		return master;
	}

	function dm_jurnsupplier_returdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_transaksi_supplier_returdet',
				method: 'POST'
			}),
			baseParams:{
				noretursupplier: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'noretursupplier',
			    mapping: 'noretursupplier',
			}, {
			    name: 'nopo',
			    mapping: 'nopo',
			}, {
			    name: 'kdbrg',
			    mapping: 'kdbrg',
			}, {
			    name: 'nmstbayar',
			    mapping: 'nmstbayar',
			}, {
			    name: 'nmbrg',
			    mapping: 'nmbrg',
			}, {
			    name: 'qty',
			    mapping: 'qty',
			}, {
			    name: 'qtyretur',
			    mapping: 'qtyretur',
			}, {
			    name: 'hargabeli',
			    mapping: 'hargabeli',
			}, {
			    name: 'diskon',
			    mapping: 'diskon',
			}, {
			    name: 'ppn',
			    mapping: 'ppn',
			}, {
			    name: 'catatan',
			    mapping: 'catatan',
			}, {
			    name: 'idstbayar',
			    mapping: 'idstbayar',
			}, {
			    name: 'totretur',
			    mapping: 'totretur',
			}, {
			    name: 'totdiskon',
			    mapping: 'totdiskon',
			}, {
			    name: 'totretur_diskon',
			    mapping: 'totretur_diskon',
			}, {
			    name: 'beban_ppn',
			    mapping: 'beban_ppn',
			}, {
			    name: 'totretur_diskon_ppn',
			    mapping: 'totretur_diskon_ppn',
			}, {
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: '',
			    mapping: '',
			}]
		});
		return master;		
	}
	
	function dm_jurnsupplier_retur_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_jtransaksi_supplier_retur_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				noretursupplier: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;
	}

	function dm_jurnsupplier_retur_jurnaling_persediaan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_jtransaksi_supplier_retur_jurnaling_persediaan',
				method: 'POST'
			}),
			baseParams:{
				noretursupplier: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;
	}

	function dm_jurnsupplier_retur_jurnaling_pendapatan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_supplier_controller/get_jtransaksi_supplier_retur_jurnaling_pendapatan',
				method: 'POST'
			}),
			baseParams:{
				noretursupplier: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;
	}

  /* ======= JURNAL KHUSUS PASIEN (DEPOSIT RI) =========*/
	function dm_jurnri_deposit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_ri_controller/get_transaksi_ri_deposit',
				method: 'POST'
			}),
			baseParams:{
				tglkuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'tglkuitansi',
			    mapping: 'tglkuitansi',
			}, {
			    name: 'jamkuitansi',
			    mapping: 'jamkuitansi',
			}, {
			    name: 'idshift',
			    mapping: 'idshift',
			}, {
			    name: 'idstkuitansi',
			    mapping: 'idstkuitansi',
			}, {
			    name: 'idsbtnm',
			    mapping: 'idsbtnm',
			}, {
			    name: 'atasnama',
			    mapping: 'atasnama',
			}, {
			    name: 'idjnskuitansi',
			    mapping: 'idjnskuitansi',
			}, {
			    name: 'pembulatan',
			    mapping: 'pembulatan',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'ketina',
			    mapping: 'ketina',
			}, {
			    name: 'keteng',
			    mapping: 'keteng',
			}, {
			    name: 'nokasir',
			    mapping: 'nokasir',
			}, {
			    name: 'tgljaminput',
			    mapping: 'tgljaminput',
			}, {
			    name: 'idregdet',
			    mapping: 'idregdet',
			}, {
			    name: 'pembayaran',
			    mapping: 'pembayaran',
			}, {
			    name: 'nojurnal',
			    mapping: 'nojurnal',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}]
		});
		return master;
	}

	function dm_jurnri_depositdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_ri_controller/get_transaksi_ri_depositdet',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idkuitansidet',
			    mapping: 'idkuitansidet',
			}, {
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'idbank',
			    mapping: 'idbank',
			}, {
			    name: 'idcarabayar',
			    mapping: 'idcarabayar',
			}, {
			    name: 'jumlah',
			    mapping: 'jumlah',
			}, {
			    name: 'nokartu',
			    mapping: 'nokartu',
			}, {
			    name: 'kdbank',
			    mapping: 'kdbank',
			}, {
			    name: 'nmbank',
			    mapping: 'nmbank',
			}, {
			    name: 'kdcarabayar',
			    mapping: 'kdcarabayar',
			}, {
			    name: 'nmcarabayar',
			    mapping: 'nmcarabayar',
			}]
		});
		return master;	
	}
	
	/* ========== JURNAL KHUSUS PASIEN (TRANSAKSI RJ & UGD) ========== */
	function dm_jurnpasien_transaksi_rjugd(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_rjugd',
				method: 'POST'
			}),
			baseParams:{
				tglkuitansi: '',
				tglkuitansi_akhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'tglkuitansi',
			    mapping: 'tglkuitansi',
			}, {
			    name: 'jamkuitansi',
			    mapping: 'jamkuitansi',
			}, {
			    name: 'idshift',
			    mapping: 'idshift',
			}, {
			    name: 'idstkuitansi',
			    mapping: 'idstkuitansi',
			}, {
			    name: 'idsbtnm',
			    mapping: 'idsbtnm',
			}, {
			    name: 'atasnama',
			    mapping: 'atasnama',
			}, {
			    name: 'idjnskuitansi',
			    mapping: 'idjnskuitansi',
			}, {
			    name: 'pembulatan',
			    mapping: 'pembulatan',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'ketina',
			    mapping: 'ketina',
			}, {
			    name: 'keteng',
			    mapping: 'keteng',
			}, {
			    name: 'nokasir',
			    mapping: 'nokasir',
			}, {
			    name: 'tgljaminput',
			    mapping: 'tgljaminput',
			}, {
			    name: 'idregdet',
			    mapping: 'idregdet',
			}, {
			    name: 'pembayaran',
			    mapping: 'pembayaran',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'nojurnal',
			    mapping: 'nojurnal',
			}]
		});
		return master;
	}

	function dm_jurnpasien_transaksi_rjugddet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_rjugddet',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idkuitansidet',
			    mapping: 'idkuitansidet',
			}, {
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'idbank',
			    mapping: 'idbank',
			}, {
			    name: 'nmbank',
			    mapping: 'nmbank',
			}, {
			    name: 'idcarabayar',
			    mapping: 'idcarabayar',
			}, {
			    name: 'nmcarabayar',
			    mapping: 'nmcarabayar',
			}, {
			    name: 'jumlah',
			    mapping: 'jumlah',
			}, {
			    name: 'nokartu',
			    mapping: 'nokartu',
			}]
		});
		return master;	
	}

	function dm_jurnpasien_transaksi_rjugd_notadet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_rjugd_notadet',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idnotadet',
			    mapping: 'idnotadet',
			}, {
			    name: 'nonota',
			    mapping: 'nonota',
			}, {
			    name: 'noreg',
			    mapping: 'noreg',
			}, {
			    name: 'norm',
			    mapping: 'norm',
			}, {
			    name: 'kditem',
			    mapping: 'kditem',
			}, {
			    name: 'idjnstarif',
			    mapping: 'idjnstarif',
			}, {
			    name: 'koder',
			    mapping: 'koder',
			}, {
			    name: 'idsatuan',
			    mapping: 'idsatuan',
			}, {
			    name: 'qty',
			    mapping: 'qty',
			}, {
			    name: 'tarifjs',
			    mapping: 'tarifjs',
			}, {
			    name: 'tarifjm',
			    mapping: 'tarifjm',
			}, {
			    name: 'tarifjp',
			    mapping: 'tarifjp',
			}, {
			    name: 'tarifbhp',
			    mapping: 'tarifbhp',
			}, {
			    name: 'diskonjs',
			    mapping: 'diskonjs',
			}, {
			    name: 'diskonjm',
			    mapping: 'diskonjm',
			}, {
			    name: 'diskonjp',
			    mapping: 'diskonjp',
			}, {
			    name: 'diskonbhp',
			    mapping: 'diskonbhp',
			}, {
			    name: 'uangr',
			    mapping: 'uangr',
			}, {
			    name: 'hrgjual',
			    mapping: 'hrgjual',
			}, {
			    name: 'hrgbeli',
			    mapping: 'hrgbeli',
			}, {
			    name: 'iddokter',
			    mapping: 'iddokter',
			}, {
			    name: 'idperawat',
			    mapping: 'idperawat',
			}, {
			    name: 'idstbypass',
			    mapping: 'idstbypass',
			}, {
			    name: 'aturanpakai',
			    mapping: 'aturanpakai',
			}, {
			    name: 'dijamin',
			    mapping: 'dijamin',
			}, {
			    name: 'stdijamin',
			    mapping: 'stdijamin',
			}, {
			    name: 'tagihanitem',
			    mapping: 'tagihanitem',
			}, {
			    name: 'diskonitem',
			    mapping: 'diskonitem',
			}, {
			    name: 'bayaritem',
			    mapping: 'bayaritem',
			}, {
			    name: 'nmitem',
			    mapping: 'nmitem',
			}]
		});
		return master;	
	}

	function dm_jurnpasien_rjugd_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_rjugd_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;	
	}

	function dm_jurnpasien_rjugd_jurnaling_persediaan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_rjugd_jurnaling_persediaan',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;	
	}

  /* ========== JURNAL KHUSUS DEPOSIT PASIEN RI ========== */
	function dm_jurnpasien_transaksi_deposit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_deposit',
				method: 'POST'
			}),
			baseParams:{
				tglkuitansi: '',
				tglkuitansi_akhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'tglkuitansi',
			    mapping: 'tglkuitansi',
			}, {
			    name: 'jamkuitansi',
			    mapping: 'jamkuitansi',
			}, {
			    name: 'idshift',
			    mapping: 'idshift',
			}, {
			    name: 'idstkuitansi',
			    mapping: 'idstkuitansi',
			}, {
			    name: 'idsbtnm',
			    mapping: 'idsbtnm',
			}, {
			    name: 'atasnama',
			    mapping: 'atasnama',
			}, {
			    name: 'idjnskuitansi',
			    mapping: 'idjnskuitansi',
			}, {
			    name: 'pembulatan',
			    mapping: 'pembulatan',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'ketina',
			    mapping: 'ketina',
			}, {
			    name: 'keteng',
			    mapping: 'keteng',
			}, {
			    name: 'nokasir',
			    mapping: 'nokasir',
			}, {
			    name: 'tgljaminput',
			    mapping: 'tgljaminput',
			}, {
			    name: 'idregdet',
			    mapping: 'idregdet',
			}, {
			    name: 'pembayaran',
			    mapping: 'pembayaran',
			}, {
			    name: 'noreg',
			    mapping: 'noreg',
			}, {
			    name: 'norm',
			    mapping: 'norm',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}]
		});
		return master;
	}

	function dm_jurnpasien_transaksi_depositdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_depositdet',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idkuitansidet',
			    mapping: 'idkuitansidet',
			}, {
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'idbank',
			    mapping: 'idbank',
			}, {
			    name: 'nmbank',
			    mapping: 'nmbank',
			}, {
			    name: 'idcarabayar',
			    mapping: 'idcarabayar',
			}, {
			    name: 'nmcarabayar',
			    mapping: 'nmcarabayar',
			}, {
			    name: 'jumlah',
			    mapping: 'jumlah',
			}, {
			    name: 'nokartu',
			    mapping: 'nokartu',
			}]
		});
		return master;	
	}

	function dm_jurnpasien_transaksi_deposit_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_deposit_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;	
	}

  /* ========== JURNAL KHUSUS RETUR DEPOSIT PASIEN RI ========== */
	function dm_jurnpasien_transaksi_returdeposit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_returdeposit',
				method: 'POST'
			}),
			baseParams:{
				tgltransaksi: '',
				tgltransaksi_akhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'nojurnal',
			    mapping: 'nojurnal',
			}, {
			    name: 'idjnsjurnal',
			    mapping: 'idjnsjurnal',
			}, {
			    name: 'idbagian',
			    mapping: 'idbagian',
			}, {
			    name: 'nokasir',
			    mapping: 'nokasir',
			}, {
			    name: 'tgltransaksi',
			    mapping: 'tgltransaksi',
			}, {
			    name: 'tgljurnal',
			    mapping: 'tgljurnal',
			}, {
			    name: 'keterangan',
			    mapping: 'keterangan',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'tglinput',
			    mapping: 'tglinput',
			}, {
			    name: 'nominal',
			    mapping: 'nominal',
			}, {
			    name: 'idjnstransaksi',
			    mapping: 'idjnstransaksi',
			}, {
			    name: 'penerima',
			    mapping: 'penerima',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}]
		});
		return master;
	}

	function dm_jurnpasien_transaksi_returdeposit_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_returdeposit_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				kdjurnal: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;	
	}

  /* ========== JURNAL KHUSUS TRANSAKSI PASIEN RI ========== */
  function dm_jurnpasien_transaksi_ri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_ri',
				method: 'POST'
			}),
			baseParams:{
				tglkuitansi: '',
				tglkuitansi_akhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'tglkuitansi',
			    mapping: 'tglkuitansi',
			}, {
			    name: 'jamkuitansi',
			    mapping: 'jamkuitansi',
			}, {
			    name: 'idshift',
			    mapping: 'idshift',
			}, {
			    name: 'idstkuitansi',
			    mapping: 'idstkuitansi',
			}, {
			    name: 'idsbtnm',
			    mapping: 'idsbtnm',
			}, {
			    name: 'atasnama',
			    mapping: 'atasnama',
			}, {
			    name: 'idjnskuitansi',
			    mapping: 'idjnskuitansi',
			}, {
			    name: 'pembulatan',
			    mapping: 'pembulatan',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'ketina',
			    mapping: 'ketina',
			}, {
			    name: 'keteng',
			    mapping: 'keteng',
			}, {
			    name: 'nokasir',
			    mapping: 'nokasir',
			}, {
			    name: 'tgljaminput',
			    mapping: 'tgljaminput',
			}, {
			    name: 'idregdet',
			    mapping: 'idregdet',
			}, {
			    name: 'pembayaran',
			    mapping: 'pembayaran',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'nojurnal',
			    mapping: 'nojurnal',
			}]
		});
		return master;
	}

	function dm_jurnpasien_transaksi_ridet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_riddet',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idkuitansidet',
			    mapping: 'idkuitansidet',
			}, {
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'idbank',
			    mapping: 'idbank',
			}, {
			    name: 'nmbank',
			    mapping: 'nmbank',
			}, {
			    name: 'idcarabayar',
			    mapping: 'idcarabayar',
			}, {
			    name: 'nmcarabayar',
			    mapping: 'nmcarabayar',
			}, {
			    name: 'jumlah',
			    mapping: 'jumlah',
			}, {
			    name: 'nokartu',
			    mapping: 'nokartu',
			}]
		});
		return master;	
	}

	function dm_jurnpasien_ri_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_ri_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;	
	}

	/* ========== JURNAL KHUSUS FARMASI PASIEN LUAR ========== */
  function dm_jurnpasien_farmasi_pl(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_farmasi_pl',
				method: 'POST'
			}),
			baseParams:{
				tglkuitansi: '',
				tglkuitansi_akhir: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'tglkuitansi',
			    mapping: 'tglkuitansi',
			}, {
			    name: 'jamkuitansi',
			    mapping: 'jamkuitansi',
			}, {
			    name: 'idshift',
			    mapping: 'idshift',
			}, {
			    name: 'idstkuitansi',
			    mapping: 'idstkuitansi',
			}, {
			    name: 'idsbtnm',
			    mapping: 'idsbtnm',
			}, {
			    name: 'atasnama',
			    mapping: 'atasnama',
			}, {
			    name: 'idjnskuitansi',
			    mapping: 'idjnskuitansi',
			}, {
			    name: 'pembulatan',
			    mapping: 'pembulatan',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'ketina',
			    mapping: 'ketina',
			}, {
			    name: 'keteng',
			    mapping: 'keteng',
			}, {
			    name: 'nokasir',
			    mapping: 'nokasir',
			}, {
			    name: 'tgljaminput',
			    mapping: 'tgljaminput',
			}, {
			    name: 'idregdet',
			    mapping: 'idregdet',
			}, {
			    name: 'pembayaran',
			    mapping: 'pembayaran',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'nojurnal',
			    mapping: 'nojurnal',
			}]
		});
		return master;
	}


	/* ========== JURNAL KHUSUS RETUR FARMASI ========== */
	function dm_jurnfarmasi_retur(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_farmasi_controller/get_transaksi_farmasi_retur',
				method: 'POST'
			}),
			baseParams:{
				tglreturfarmasi: '',
				tglreturfarmasi_akhir: '',
				pencarian: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'noreturfarmasi',
			    mapping: 'noreturfarmasi',
			}, {
			    name: 'tglreturfarmasi',
			    mapping: 'tglreturfarmasi',
			}, {
			    name: 'jamreturfarmasi',
			    mapping: 'jamreturfarmasi',
			}, {
			    name: 'biayaadm',
			    mapping: 'biayaadm',
			}, {
			    name: 'idsttransaksi',
			    mapping: 'idsttransaksi',
			}, {
			    name: 'idstsetuju',
			    mapping: 'idstsetuju',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'idstharga',
			    mapping: 'idstharga',
			}, {
			    name: 'qtyretur',
			    mapping: 'qtyretur',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'nonota',
			    mapping: 'nonota',
			}, {
			    name: 'idjnstransaksi',
			    mapping: 'idjnstransaksi',
			}, {
			    name: 'nmjnstransaksi',
			    mapping: 'nmjnstransaksi',
			}, {
			    name: 'atasnama',
			    mapping: 'atasnama',
			}, {
			    name: 'noreg',
			    mapping: 'noreg',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}]
		});
		return master;
	}

	function dm_jurnfarmasi_returdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_farmasi_controller/get_transaksi_farmasi_returdet',
				method: 'POST'
			}),
			baseParams:{
				noreturfarmasi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idnotadet',
			    mapping: 'idnotadet',
			}, {
			    name: 'noreturfarmasi',
			    mapping: 'noreturfarmasi',
			}, {
			    name: 'nonota',
			    mapping: 'nonota',
			}, {
			    name: 'kdbrg',
			    mapping: 'kdbrg',
			}, {
			    name: 'nmbrg',
			    mapping: 'nmbrg',
			}, {
			    name: 'qty',
			    mapping: 'qty',
			}, {
			    name: 'catatan',
			    mapping: 'catatan',
			}, {
			    name: 'idstposisipasien',
			    mapping: 'idstposisipasien',
			}, {
			    name: 'hrgbeli',
			    mapping: 'hrgbeli',
			}, {
			    name: 'hrgjual',
			    mapping: 'hrgjual',
			}, {
			    name: 'diskonitem',
			    mapping: 'diskonitem',
			}, {
			    name: 'diskonpersen',
			    mapping: 'diskonpersen',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'grandtotal',
			    mapping: 'grandtotal',
			}]
		});
		return master;		
	}

	function dm_jurnfarmasi_retur_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_farmasi_controller/get_transaksi_farmasi_retur_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				noreturfarmasi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;	
	}
	
	/* ========== JURNAL KHUSUS PASIEN ( RJ, UGD, RI, F. Pasien Luar, P. Tambahan) ========== */
	function dm_jurnpasien_transaksi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien',
				method: 'POST'
			}),
			baseParams:{
				tglkuitansi: '',
				tglkuitansi_akhir: '',
				pencarian: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'tglkuitansi',
			    mapping: 'tglkuitansi',
			}, {
			    name: 'idstkuitansi',
			    mapping: 'idstkuitansi',
			}, {
			    name: 'atasnama',
			    mapping: 'atasnama',
			}, {
			    name: 'idjnskuitansi',
			    mapping: 'idjnskuitansi',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'idregdet',
			    mapping: 'idregdet',
			}, {
			    name: 'pembayaran',
			    mapping: 'pembayaran',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}, {
			    name: 'status_posting2',
			    mapping: 'status_posting2',
			    type: 'bool',
          convert: function(v){
             return (v == '1') ? true : false;
          }
			}, {
			    name: 'nonota',
			    mapping: 'nonota',
			}, {
			    name: 'noreg',
			    mapping: 'noreg',
			}, {
			    name: 'jenis_transaksi',
			    mapping: 'jenis_transaksi',
			}, {
			    name: 'jtransjurnal',
			    mapping: 'jtransjurnal',
			}]
		});
		return master;
	}

	function dm_jurnpasien_transaksi_det(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_det',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idkuitansidet',
			    mapping: 'idkuitansidet',
			}, {
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'idbank',
			    mapping: 'idbank',
			}, {
			    name: 'nmbank',
			    mapping: 'nmbank',
			}, {
			    name: 'idcarabayar',
			    mapping: 'idcarabayar',
			}, {
			    name: 'nmcarabayar',
			    mapping: 'nmcarabayar',
			}, {
			    name: 'jumlah',
			    mapping: 'jumlah',
			}, {
			    name: 'nokartu',
			    mapping: 'nokartu',
			}]
		});
		return master;	
	}

	function dm_jurnpasien_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_transaksi_pasien_controller/get_transaksi_pasien_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;	
	}

	/* ========== JURNAL KHUSUS HONOR DOKTER ========== */
	function dm_jurn_hondok(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_trans_hondok_controller/get_honor_dokter',
				method: 'POST'
			}),
			baseParams:{
				tglbayar: '',
				tglbayar_akhir: '',
				pencarian: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nohondok',
			    mapping: 'nohondok',
			}, {
			    name: 'tglhondok',
			    mapping: 'tglhondok',
			}, {
			    name: 'tglawal',
			    mapping: 'tglawal',
			}, {
			    name: 'tglakhir',
			    mapping: 'tglakhir',
			}, {
			    name: 'jasadrjaga',
			    mapping: 'jasadrjaga',
			}, {
			    name: 'potonganlain',
			    mapping: 'potonganlain',
			}, {
			    name: 'userid',
			    mapping: 'userid',
			}, {
			    name: 'iddokter',
			    mapping: 'iddokter',
			}, {
			    name: 'approval',
			    mapping: 'approval',
			}, {
			    name: 'idjnspembayaran',
			    mapping: 'idjnspembayaran',
			}, {
			    name: 'nmjnspembayaran',
			    mapping: 'nmjnspembayaran',
			}, {
			    name: 'idstbayar',
			    mapping: 'idstbayar',
			}, {
			    name: 'nmstbayar',
			    mapping: 'nmstbayar',
			}, {
			    name: 'tglinput',
			    mapping: 'tglinput',
			}, {
			    name: 'catatan',
			    mapping: 'catatan',
			}, {
			    name: 'tgldikeluarkan',
			    mapping: 'tgldikeluarkan',
			}, {
			    name: 'nmdoktergelar',
			    mapping: 'nmdoktergelar',
			}, {
			    name: 'kdjurnal',
			    mapping: 'kdjurnal',
			}, {
			    name: 'status_posting',
			    mapping: 'status_posting',
			}, {
			    name: 'jumlah',
			    mapping: 'jumlah',
			}]
		});
		return master;
	}

	function dm_jurn_hondokdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_trans_hondok_controller/get_honor_dokterdet',
				method: 'POST'
			}),
			baseParams:{
				nohondok: '',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idhondokdet',
			    mapping: 'idhondokdet',
			}, {
			    name: 'nohondok',
			    mapping: 'nohondok',
			}, {
			    name: 'noreg',
			    mapping: 'noreg',
			}, {
			    name: 'idjnshondok',
			    mapping: 'idjnshondok',
			}, {
			    name: 'nmjnshondok',
			    mapping: 'nmjnshondok',
			}, {
			    name: 'jasamedis',
			    mapping: 'jasamedis',
			}, {
			    name: 'diskon',
			    mapping: 'diskon',
			}, {
			    name: 'zis',
			    mapping: 'zis',
			}, {
			    name: 'subtotal',
			    mapping: 'subtotal',
			}]
		});
		return master;
	}

	function dm_jurn_hondok_jurnaling(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'jurn_trans_hondok_controller/get_honor_dokter_jurnaling',
				method: 'POST'
			}),
			baseParams:{
				nokuitansi: ''
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'idakun',
			    mapping: 'idakun',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'nmakun',
			    mapping: 'nmakun',
			}, {
			    name: 'noreff',
			    mapping: 'noreff',
			}, {
			    name: 'debit',
			    mapping: 'debit',
			}, {
			    name: 'kredit',
			    mapping: 'kredit',
			}]
		});
		return master;	
	}
	
//=======================================================
	function dm_setorankasir(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_setorankasir',
				method: 'POST'
			}),
			baseParams:{
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idsetorankasir',
				mapping: 'idsetorankasir'
			},{
				name: 'tgltransaksi',
				mapping: 'tgltransaksi'
			},{
				name: 'approval',
				mapping: 'approval'
			},{
				name: 'nmset',
				mapping: 'nmset'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'tglsetoran',
				mapping: 'tglsetoran'
			},{
				name: 'jamsetoran',
				mapping: 'jamsetoran'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'jmlsetoran',
				mapping: 'jmlsetoran'
			},{
				name: 'jmlsetoranfisik',
				mapping: 'jmlsetoranfisik'
			},{
				name: 'selisih',
				mapping: 'selisih'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stapproval(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_stapproval',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nilai',
				mapping: 'nilai'
			},{
				name: 'nmset',
				mapping: 'nmset'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_retfarrawjln(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_retfarrawjln',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglreturfarmasi',
				mapping: 'tglreturfarmasi'
			},{
				name: 'totretfarrawjln',
				mapping: 'totretfarrawjln'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_retfarrawri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_retfarrawri',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglreturfarmasi',
				mapping: 'tglreturfarmasi'
			},{
				name: 'totretfarrawri',
				mapping: 'totretfarrawri'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_retfarluar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_retfarluar',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglreturfarmasi',
				mapping: 'tglreturfarmasi'
			},{
				name: 'totretfarluar',
				mapping: 'totretfarluar'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_retdeposit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_retdeposit',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tgltransaksi',
				mapping: 'tgltransaksi'
			},{
				name: 'totretdeposit',
				mapping: 'totretdeposit'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_farpasluarjumbyr(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_farpasluarjumbyr',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jmlbayar',
				mapping: 'jmlbayar'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_farpasluardijamin(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_farpasluardijamin',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'totaldijamin',
				mapping: 'totaldijamin'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_totudg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_totudg',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'totalugd',
				mapping: 'totalugd'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jmlbyrudg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_jmlbyrudg',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'totbyrtunai',
				mapping: 'totbyrtunai'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_totdijamin(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_totdijamin',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'totbyrnontunai',
				mapping: 'totbyrnontunai'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_totpeltambahan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_totpeltambahanbyrtunai',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'totbyrtunai',
				mapping: 'totbyrtunai'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_totpeltambahannontunai(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_totpeltambahanbyrnontunai',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'totbyrnontunai',
				mapping: 'totbyrnontunai'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_totdafosit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_totdafosit',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'totdafosit',
				mapping: 'totdafosit'
			},{
				name: 'totbyrtunai',
				mapping: 'totbyrtunai'
			},{
				name: 'totbyrnontunai',
				mapping: 'totbyrnontunai'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_totrjunitpelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_totrjunitpelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'totbyrtunai',
				mapping: 'totbyrtunai'
			},{
				name: 'totbyrnontunai',
				mapping: 'totbyrnontunai'
			}]
		});
		return master;
	}	
	
//=======================================================
	function dm_totriruangan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_totriruangan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm',				
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nominalugd',
				mapping: 'nominalugd'
			},{
				name: 'nominalri',
				mapping: 'nominalri'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'totbyrtunai',
				mapping: 'totbyrtunai'
			},{
				name: 'totbyrnontunai',
				mapping: 'totbyrnontunai'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_detdeposit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_detdeposit',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'nontunai',
				mapping: 'nontunai'
			},{
				name: 'tunai',
				mapping: 'tunai'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_carabyrtunai(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'setorankasir_controller/get_carabyrtunai',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'idcarabayar',
				mapping: 'idcarabayar'
			},{
				name: 'nmcarabayar',
				mapping: 'nmcarabayar'
			},{
				name: 'totcarabyr',
				mapping: 'totcarabyr'
			},{
				name: 'totcarabyrtunai',
				mapping: 'totcarabyrtunai'
			}]
		});
		return master;
	}
	
//======================================================= 
	function dm_registrasii(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'returdeposit_controller/get_registrasii',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name:'nona',
				mapping: 'nona'
			},{
				name: 'nmklstarif',
				mapping: 'nmklstarif'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'tottransaksiri',
				mapping: 'tottransaksiri'
			},{
				name: 'totdeposit',
				mapping: 'totdeposit'
			}]
		});
		return master;
	}
	
// Farmasi
//=======================================================
	function dm_returfarmasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returfarmasi_controller/get_returfarmasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreturfarmasi',
				mapping: 'noreturfarmasi'
			},{
				name: 'tglreturfarmasi',
				mapping: 'tglreturfarmasi'
			},{
				name: 'jamreturfarmasi',
				mapping: 'jamreturfarmasi'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'biayaadm',
				mapping: 'biayaadm'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'noregistrasi',
				mapping: 'noregistrasi'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_returfarmasirjri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returfarmasi_controller/get_returfarmasirjri',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreturfarmasi',
				mapping: 'noreturfarmasi'
			},{
				name: 'tglreturfarmasi',
				mapping: 'tglreturfarmasi'
			},{
				name: 'jamreturfarmasi',
				mapping: 'jamreturfarmasi'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'biayaadm',
				mapping: 'biayaadm'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'noregistrasi',
				mapping: 'noregistrasi'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_returfarmasidet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returfarmasi_controller/get_returfarmasidet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreturfarmasi',
				mapping: 'noreturfarmasi'
			},{
				name: 'idnotadet',
				mapping: 'idnotadet'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'qtyterima',
				mapping: 'qtyterima'
			},{
				name: 'jmlretur',
				mapping: 'jmlretur'
			},{
				name: 'qtysisa',
				mapping: 'qtysisa'
			},{
				name: 'tamqtysisa',
				mapping: 'tamqtysisa'
			},{
				name: 'qtyretur',
				mapping: 'qtyretur'
			},{
				name: 'tarif',
				mapping: 'tarif'
			},{
				name: 'subtotal',
				mapping: 'subtotal'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'diskon',
				mapping: 'diskon'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_carinoregdireturfarmasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returfarmasi_controller/get_carinoregdireturfarmasi',
				method: 'POST'
			}),
			baseParams: {
				tglawal : Ext.util.Format.date(new Date(), 'Y-m-d'),
				key : '1',
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'idstregistrasi',
				mapping: 'idstregistrasi'
			},{
				name: 'nmstregistrasi',
				mapping: 'nmstregistrasi'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_carinotadireturfarmasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returfarmasi_controller/get_carinotadireturfarmasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'idnotadet',
				mapping: 'idnotadet'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'qtyterima',
				mapping: 'qtyterima'
			},{
				name: 'jmlretur',
				mapping: 'jmlretur'
			},{
				name: 'qtysisa',
				mapping: 'qtysisa'
			},{
				name: 'tamqtysisa',
				mapping: 'tamqtysisa'
			},{
				name: 'qtyretur',
				mapping: 'qtyretur'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'subtotal',
				mapping: 'subtotal'
			},{
				name: 'diskon',
				mapping: 'diskon'
			}]
		});
		return master;
	}
		
//=======================================================
	function dm_carinonotadireturfarmasi(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returfarmasi_controller/get_carinonotadireturfarmasi',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'jamnota',
				mapping: 'jamnota'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			}]
		});
		return master;
	}	
//=======================================================
	function dm_carikdbrgadiretfarmasiumum(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returfarmasi_controller/get_carikdbrgadiretfarmasiumum',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idnotadet',
				mapping: 'idnotadet'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'qtyterima',
				mapping: 'qtyterima'
			},{
				name: 'jmlretur',
				mapping: 'jmlretur'
			},{
				name: 'qtysisa',
				mapping: 'qtysisa'
			},{
				name: 'tamqtysisa',
				mapping: 'tamqtysisa'
			},{
				name: 'qtyretur',
				mapping: 'qtyretur'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'diskon',
				mapping: 'diskon'
			}]
		});
		return master;
	}

	
// Persediaan pembelian
//=======================================================
	function dm_hbrgsupplier(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'hbrgsupplier_controller/get_hbrgsupplier',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idhrgbrgsup',
				mapping: 'idhrgbrgsup'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'nmmatauang',
				mapping: 'nmmatauang'
			},{
				name: 'harga',
				mapping: 'harga'
			},{
				name: 'tglefektif',
				mapping: 'tglefektif'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			}]
		});
		return master;
	}
	
//=======================================================
function dm_hbrgsupplierx(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'hbrgsupplier_controller/get_hargabrng',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idhrgbrgsup',
				mapping: 'idhrgbrgsup'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'nmmatauang',
				mapping: 'nmmatauang'
			},{
				name: 'harga',
				mapping: 'harga'
			},{
				name: 'tglefektif',
				mapping: 'tglefektif'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			}]
		});
		return master;
	}
//=======================================================
	function dm_pensupplier(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pensupplier_controller/get_pensupplier',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'tglpp',
				mapping: 'tglpp'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			}/* ,{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			} */]
		});
		return master;
	}
	
	function dm_pensupplierdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pensupplier_controller/get_pensupplierdet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},,{
				name: 'idhrgbrgsup',
				mapping: 'idhrgbrgsup'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'nmmatauang',
				mapping: 'nmmatauang'
			},{
				name: 'harga',
				mapping: 'harga'
			},{
				name: 'idstpp',
				mapping: 'idstpp'
			},{
				name: 'idstpp1',
				mapping: 'idstpp1',
				type: 'bool'
			}]
		});
		return master;
	}
	
	function dm_carihrgbrgsup(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pensupplier_controller/get_carihrgbrgsup',
				method: 'POST'
			}),
			baseParams:{
				key:'0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idhrgbrgsup',
				mapping: 'idhrgbrgsup'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'nmmatauang',
				mapping: 'nmmatauang'
			},{
				name: 'harga',
				mapping: 'harga'
			},{
				name: 'tglefektif',
				mapping: 'tglefektif'
			},{
				name: 'nopp',
				mapping: 'nopp'
			}]
		});
		return master;
	}
	
// Persediaan logistik
//=======================================================
	function dm_perpembelian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'perpembelian_controller/get_perpembelian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'tglpp',
				mapping: 'tglpp'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'idstpo',
				mapping: 'idstpo'
			},{
				name: 'nmstpo',
				mapping: 'nmstpo'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
		});
		return master;
	} 
	
	function dm_podiperpembelian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'perpembelian_controller/get_headpodiperpembelian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopo',
				mapping: 'nopo'
			},{
				name: 'tglpo',
				mapping: 'tglpo'
			},{
				name: 'idsypembayaran',
				mapping: 'idsypembayaran'
			},{
				name: 'nmsypembayaran',
				mapping: 'nmsypembayaran'
			},{
				name: 'idstpo',
				mapping: 'idstpo'
			},{
				name: 'nmstpo',
				mapping: 'nmstpo'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'nmmatauang',
				mapping: 'nmmatauang'
			},{
				name: 'bpb',
				mapping: 'bpb'
			},{
				name: 'totalpo',
				mapping: 'totalpo'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
		});
		return master;
	}
	
	function dm_detpodiperpembelian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'perpembelian_controller/get_detpodiperpembelian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopo',
				mapping: 'nopo'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'qtybonus',
				mapping: 'qtybonus'
			},{
				name: 'hargabeli',
				mapping: 'hargabeli'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'diskonrp',
				mapping: 'diskonrp'
			},{
				name: 'ppn',
				mapping: 'ppn'
			},{
				name: 'hargajual',
				mapping: 'hargajual'
			}]
		});
		return master;
	}
	
//=======================================================
function dm_perpembelianx(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'perpembelian_controller/get_perpembelianx',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'tglpp',
				mapping: 'tglpp'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idstpp',
				mapping: 'idstpp'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang',
			},{
				name: 'nmmatauang',
				mapping: 'nmmatauang',
			},{
				name: 'kdmatauang',
				mapping: 'kdmatauang',
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'npwp',
				mapping: 'npwp'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'idsatuanbsr',
				mapping: 'idsatuanbsr'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
		});
		return master;
	}

//=======================================================
function dm_perpembelianxx(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'perpembelian_controller/get_perpembelianxx',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'tglpp',
				mapping: 'tglpp'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qtypp',
				mapping: 'qtypp'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'harga',
				mapping: 'harga'
			},{
				name: 'idhrgbrgsup',
				mapping: 'idhrgbrgsup'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'idstpp',
				mapping: 'idstpp'
			},{
				name: 'nmstpp',
				mapping: 'nmstpp'
			},{
				name: 'nopo',
				mapping: 'nopo'
			},{
				name: 'idmatauang',
				mapping: 'idmatauang'
			},{
				name: 'nmmatauang',
				mapping: 'nmmatauang'
			},{
				name: 'kdmatauang',
				mapping: 'kdmatauang'
			},{
				name: 'subtotal',
				mapping: 'subtotal'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'margin',
				mapping: 'margin'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'idsatuanbsr',
				mapping: 'idsatuanbsr'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'hsubtotal',
				mapping : 'hsubtotal'
			},{
				name: 'hargajualtemp',
				mapping: 'hargajualtemp'
			},{
				name: 'hargajual',
				mapping: 'hargajual'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'htsubtotal',
				mapping: 'htsubtotal'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'diskonrp',
				mapping: 'diskonrp'
			},{
				name: 'qtypo',
				mapping: 'qtypo'
			},{
				name: 'idstpp',
				mapping: 'idstpp'
			}]
		});
		return master;
	}
//=======================================================
	function dm_perpembeliandet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'perpembelian_controller/get_perpembeliandet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopp',
				mapping: 'nopp'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'harga',
				mapping: 'harga'
			},{
				name: 'idhrgbrgsup',
				mapping: 'idhrgbrgsup'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'idstpp',
				mapping: 'idstpp'
			},{
				name: 'nmstpp',
				mapping: 'nmstpp'
			},{
				name: 'nopo',
				mapping: 'nopo'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_brgmedisdipp(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'perpembelian_controller/get_brgmedisdipp',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuanbsr',
				mapping: 'idsatuanbsr'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'jmlsatuanbsr',
				mapping: 'jmlsatuanbsr'
			}]
		});
		return master;
	}

//persediaan Logistik
//=======================================================
	function dm_pemakaianbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pemakaianbrg_controller/get_pemakaianbrg',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopakaibrg',
				mapping: 'nopakaibrg'
			},{
				name: 'tglpakaibrg',
				mapping: 'tglpakaibrg'
			},{
				name: 'jampakaibrg',
				mapping: 'jampakaibrg'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_pemakaianbrgdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pemakaianbrg_controller/get_pemakaianbrgdet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopakaibrg',
				mapping: 'nopakaibrg'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'catatan',
				mapping: 'catatan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_penerimaanlain(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'penerimaanlain_controller/get_penerimaanlain',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noterimalain',
				mapping: 'noterimalain'
			},{
				name: 'tglterimalain',
				mapping: 'tglterimalain'
			},{
				name: 'jamterimalain',
				mapping: 'jamterimalain'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name:'dari',
				mapping: 'dari'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
//=======================================================
	function dm_penerimaanlaindet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'penerimaanlain_controller/get_terimalaindet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noterimalain',
				mapping: 'noterimalain'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'margin',
				mapping: 'margin'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			}]
		});
		return master;
	}

//=======================================================
	function dm_brgmedisdipemakaianbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pemakaianbrg_controller/get_brgmedisdipemakaianbrg',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'margin',
				mapping: 'margin'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'rasio',
				mapping: 'rasio'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pengeluaranbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pengeluaranbrg_controller/get_pengeluaranbrg',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokeluarbrg',
				mapping: 'nokeluarbrg'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'jamkeluar',
				mapping: 'jamkeluar'
			},{
				name: 'idbagiandari',
				mapping: 'idbagiandari'
			},{
				name: 'nmbagiandari',
				mapping: 'nmbagiandari'
			},{
				name: 'idbagianuntuk',
				mapping: 'idbagianuntuk'
			},{
				name: 'nmbagianuntuk',
				mapping: 'nmbagianuntuk'
			},{
				name: 'penerima',
				mapping: 'penerima'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			},{
				name: 'noreturbagian',
				mapping: 'noreturbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pengeluaranbrgdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pengeluaranbrg_controller/get_pengeluaranbrgdet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokeluarbrg',
				mapping: 'nokeluarbrg'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'stokminbagian',
				mapping: 'stokminbagian'
			},{
				name: 'stokmaxbagian',
				mapping: 'stokmaxbagian'
			},{
				name: 'catatan',
				mapping: 'catatan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_brgmedisdipengeluaranbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'pengeluaranbrg_controller/get_brgmedisdipengeluaranbrg',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'stokminbagian',
				mapping: 'stokminbagian'
			},{
				name: 'stokmaxbagian',
				mapping: 'stokmaxbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_bagiandipengeluaranbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url: BASE_URL + 'pengeluaranbrg_controller/get_pengunabagian',
				method: 'POST'
			}),
			root: 'data',
			totalProperty: 'results',
			autoLoad: true,
			fields: [{
				name: "idbagian",
				mapping: "idbagian"
			},{
				name: "kdbagian",
				mapping: "kdbagian"
			},{
				name: "nmbagian",
				mapping: "nmbagian"
			},{
				name: "idjnspelayanan",
				mapping: "idjnspelayanan"
			},{
				name: "nmjnspelayanan",
				mapping: "nmjnspelayanan"
			},{
				name: "idbdgrawat",
				mapping: "idbdgrawat"
			},{
				name: "nmbdgrawat",
				mapping: "nmbdgrawat"
			},{
				name: "userid",
				mapping: "userid"
			},{
				name: "nmlengkap",
				mapping: "nmlengkap"
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_returbrgbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returbrgbagian_controller/get_returbrgbagian',
				method: 'POST'
			}),
			baseParams:{
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreturbagian',
				mapping: 'noreturbagian'
			},{
				name: 'tglreturbagian',
				mapping: 'tglreturbagian'
			},{
				name: 'jamreturbagian',
				mapping: 'jamreturbagian'
			},{
				name: 'idbagiandari',
				mapping: 'idbagiandari'
			},{
				name: 'nmbagiandari',
				mapping: 'nmbagiandari'
			},{
				name: 'idbagianuntuk',
				mapping: 'idbagianuntuk'
			},{
				name: 'nmbagianuntuk',
				mapping: 'nmbagianuntuk'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_returbrgbagiandet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returbrgbagian_controller/get_returbrgbagiandet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreturbagian',
				mapping: 'noreturbagian'
			},{
				name: 'nokeluarbrg',
				mapping: 'nokeluarbrg'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'qtyterima',
				mapping: 'qtyterima'
			},{
				name: 'jmlretur',
				mapping: 'jmlretur'
			},{
				name: 'qtysisa',
				mapping: 'qtysisa'
			},{
				name: 'tamqtysisa',
				mapping: 'tamqtysisa'
			},{
				name: 'stoknowbagiandari',
				mapping: 'stoknowbagiandari'
			},{
				name: 'stoknowbagianuntuk',
				mapping: 'stoknowbagianuntuk'
			},{
				name: 'qtyretur',
				mapping: 'qtyretur'
			},{
				name: 'catatan',
				mapping: 'catatan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_pengbrgdireturbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returbrgbagian_controller/get_pengbrgdireturbagian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokeluarbrg',
				mapping: 'nokeluarbrg'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'qtyterima',
				mapping: 'qtyterima'
			},{
				name: 'jmlretur',
				mapping: 'jmlretur'
			},{
				name: 'qtysisa',
				mapping: 'qtysisa'
			},{
				name: 'tamqtysisa',
				mapping: 'tamqtysisa'
			},{
				name: 'stoknowbagiandari',
				mapping: 'stoknowbagiandari'
			},{
				name: 'stoknowbagianuntuk',
				mapping: 'stoknowbagianuntuk'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_returbrgsupplier(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returbrgsupplier_controller/get_returbrgsupplier',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noretursupplier',
				mapping: 'noretursupplier'
			},{
				name: 'tglretursupplier',
				mapping: 'tglretursupplier'
			},{
				name: 'jamretursupplier',
				mapping: 'jamretursupplier'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_supdiretsupplier(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returbrgsupplier_controller/get_supdiretsupplier',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nofax',
				mapping: 'nofax'
			},{
				name: 'kontakperson',
				mapping: 'kontakperson'
			},{
				name: 'nohp',
				mapping: 'nohp'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_returbrgsupplierdet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returbrgsupplier_controller/get_returbrgsupplierdet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noretursupplier',
				mapping: 'noretursupplier'
			},{
				name: 'nopo',
				mapping: 'nopo'
			},{
				name: 'tglpo',
				mapping: 'tglpo'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'qtyterima',
				mapping: 'qtyterima'
			},{
				name: 'jmlretur',
				mapping: 'jmlretur'
			},{
				name: 'qtysisa',
				mapping: 'qtysisa'
			},{
				name: 'tamqtysisa',
				mapping: 'tamqtysisa'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'qtyretur',
				mapping: 'qtyretur'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'tmpqtyxrasio',
				mapping: 'tmpqtyxrasio'
			},{
				name: 'idstbayar',
				mapping: 'idstbayar'
			},{
				name: 'nmstbayar',
				mapping: 'nmstbayar'
			}]
		});
		return master;
	}

		
//=======================================================
	function dm_brgdiretursupplier(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'returbrgsupplier_controller/get_brgdiretursupplier',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'nopo',
				mapping: 'nopo'
			},{
				name: 'tglpo',
				mapping: 'tglpo'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'qtyterima',
				mapping: 'qtyterima'
			},{
				name: 'jmlretur',
				mapping: 'jmlretur'
			},{
				name: 'qtysisa',
				mapping: 'qtysisa'
			},{
				name: 'tamqtysisa',
				mapping: 'tamqtysisa'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'idstbayar',
				mapping: 'idstbayar'
			},{
				name: 'nmstbayar',
				mapping: 'nmstbayar'
			}]
		});
		return master;
	}

//=======================================================
	function dm_stokopname(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stokopname_controller/get_stokopname',
				method: 'POST'
			}),
			baseParams:{
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noso',
				mapping: 'noso'
			},{
				name: 'tglso',
				mapping: 'tglso'
			},{
				name: 'jamso',
				mapping: 'jamso'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idstso',
				mapping: 'idstso'
			},{
				name: 'nmstso',
				mapping: 'nmstso'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			},{
				name: 'keterangan',
				mapping: 'keterangan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_stokopnamedet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stokopname_controller/get_stokopnamedet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noso',
				mapping: 'noso'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'jmlkomputer',
				mapping: 'jmlkomputer'
			},{
				name: 'jmlfisik',
				mapping: 'jmlfisik'
			},{
				name: 'selisih',
				mapping: 'selisih'
			},{
				name: 'catatan',
				mapping: 'catatan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_brgbagiandistokopname(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'stokopname_controller/get_brgbagiandistokopname',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'stokminbagian',
				mapping: 'stokminbagian'
			},{
				name: 'stokmaxbagian',
				mapping: 'stokmaxbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_vtarifall(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vtarifall_controller/get_vtarifall',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'nmitem',
				mapping: 'nmitem'
			},{
				name: 'klstarif',
				mapping: 'klstarif'
			},{
				name: 'satuankcl',
				mapping: 'satuankcl'
			},{
				name: 'satuanbsr',
				mapping: 'satuanbsr'
			},{
				name: 'harga',
				mapping: 'harga'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'ttltarif',
				mapping: 'ttltarif'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_tarif(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'tarif_controller/get_tarif',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idpenjamin',
				mapping: 'idpenjamin'
			},{
				name: 'kdpelayanan',
				mapping: 'kdpelayanan'
			},{
				name: 'idklstarif',
				mapping: 'idklstarif'
			},{
				name: 'tarifjs',
				mapping: 'tarifjs'
			},{
				name: 'tarifjm',
				mapping: 'tarifjm'
			},{
				name: 'tarifjp',
				mapping: 'tarifjp'
			},{
				name: 'tarifbhp',
				mapping: 'tarifbhp'
			},{
				name: 'ttltarif',
				mapping: 'ttltarif'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_kasir(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kasir_controller/get_kasir',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokasir',
				mapping: 'nokasir'
			},{
				name: 'idstkasir',
				mapping: 'idstkasir'
			},{
				name: 'idshiftbuka',
				mapping: 'idshiftbuka'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'tglbuka',
				mapping: 'tglbuka'
			},{
				name: 'jambuka',
				mapping: 'jambuka'
			},{
				name: 'saldoawal',
				mapping: 'saldoawal'
			},{
				name: 'catatanbuka',
				mapping: 'catatanbuka'
			},{
				name: 'tgltutup',
				mapping: 'tgltutup'
			},{
				name: 'jamtutup',
				mapping: 'jamtutup'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'saldoakhir',
				mapping: 'saldoakhir'
			},{
				name: 'selisih',
				mapping: 'selisih'
			},{
				name: 'catatantutup',
				mapping: 'catatantutup'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_jsbtnm(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'Jsbtnm_controller/get_jsbtnm',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idsbtnm',
				mapping: 'idsbtnm'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'kdsbtnm',
				mapping: 'kdsbtnm'
			},{
				name: 'nmsbtnm',
				mapping: 'nmsbtnm'
			},{
				name: 'dariumur',
				mapping: 'sampaiumur'
			}]
		});
		return master;
	}

//LAPORAN
//=======================================================
	function dm_vlaprjperiode(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlaprj_controller/get_vlaprjperiode',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'uobat',
				mapping: 'uobat'
			},{
				name: 'upemeriksaan',
				mapping: 'upemeriksaan'
			},{
				name: 'utindakan',
				mapping: 'utindakan'
			},{
				name: 'uimun',
				mapping: 'uimun'
			},{
				name: 'ulab',
				mapping: 'ulab'
			},{
				name: 'ulain',
				mapping: 'ulain'
			},{
				name: 'uracik',
				mapping: 'uracik'
			},{
				name: 'udiskon',
				mapping: 'udiskon'
			},{
				name: 'ucc',
				mapping: 'ucc'
			},{
				name: 'utotal',
				mapping: 'utotal'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			}]
		});
		return master;
	}

//=======================================================
	function dm_vlappasienluar(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlappasienluar',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'noresep',
				mapping: 'noresep'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'racik',
				mapping: 'racik'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'tottindakan',
				mapping: 'tottindakan'
			},{
				name: 'diskontindakan',
				mapping: 'diskontindakan'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'jumlahtindakan',
				mapping: 'jumlahtindakan'
			},{
				name: 'nontunai',
				mapping: 'nontunai'
			},{
				name: 'tunai',
				mapping: 'tunai'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
		});
		return master;
	}

//=======================================================
	function dm_vlapdeposit(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlapdeposit',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'nontunai',
				mapping: 'nontunai'
			},{
				name: 'tunai',
				mapping: 'tunai'
			}]
		});
		return master;
	}

//=======================================================
	function dm_vlappenerimaanri(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlappenerimaanri',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'tglmasuk',
				mapping: 'tglmasuk'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},{
				name: 'transferugd',
				mapping: 'transferugd'
			},{
				name: 'uobat',
				mapping: 'uobat'
			},{
				name: 'nominalugd',
				mapping: 'nominalugd'
			},{
				name: 'nominalri',
				mapping: 'nominalri'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'dijamin',
				mapping: 'dijamin'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'returdeposit',
				mapping: 'returdeposit'
			},{
				name: 'nontunai',
				mapping: 'nontunai'
			},{
				name: 'tunai',
				mapping: 'tunai'
			}]
		});
		return master;
	}

	function dm_vlappenerimaanbank(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_penerimaan_bank',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
			    name: 'nokuitansi',
			    mapping: 'nokuitansi',
			}, {
			    name: 'tglkuitansi',
			    mapping: 'tglkuitansi',
			}, {
			    name: 'jamkuitansi',
			    mapping: 'jamkuitansi',
			}, {
			    name: 'idshift',
			    mapping: 'idshift',
			}, {
			    name: 'idstkuitansi',
			    mapping: 'idstkuitansi',
			}, {
			    name: 'idsbtnm',
			    mapping: 'idsbtnm',
			}, {
			    name: 'atasnama',
			    mapping: 'atasnama',
			}, {
			    name: 'idjnskuitansi',
			    mapping: 'idjnskuitansi',
			}, {
			    name: 'pembulatan',
			    mapping: 'pembulatan',
			}, {
			    name: 'total',
			    mapping: 'total',
			}, {
			    name: 'tgljaminput',
			    mapping: 'tgljaminput',
			}, {
			    name: 'idregdet',
			    mapping: 'idregdet',
			}, {
			    name: 'pembayaran',
			    mapping: 'pembayaran',
			}, {
			    name: 'idkuitansidet',
			    mapping: 'idkuitansidet',
			}, {
			    name: 'idbank',
			    mapping: 'idbank',
			}, {
			    name: 'idcarabayar',
			    mapping: 'idcarabayar',
			}, {
			    name: 'jumlah',
			    mapping: 'jumlah',
			}, {
			    name: 'nokartu',
			    mapping: 'nokartu',
			}, {
			    name: 'kdbank',
			    mapping: 'kdbank',
			}, {
			    name: 'nmbank',
			    mapping: 'nmbank',
			}, {
			    name: 'kdakun',
			    mapping: 'kdakun',
			}, {
			    name: 'kdjnskuitansi',
			    mapping: 'kdjnskuitansi',
			}, {
			    name: 'nmjnskuitansi',
			    mapping: 'nmjnskuitansi',
			}, {
			    name: 'idjnskas',
			    mapping: 'idjnskas',
			}]
		});
		return master;
	}

//=======================================================
	function dm_vlapugdperiode(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlapugdperiode',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'uobat',
				mapping: 'uobat'
			},{
				name: 'upemeriksaan',
				mapping: 'upemeriksaan'
			},{
				name: 'utindakan',
				mapping: 'utindakan'
			},{
				name: 'uimun',
				mapping: 'uimun'
			},{
				name: 'ulab',
				mapping: 'ulab'
			},{
				name: 'ulain',
				mapping: 'ulain'
			},{
				name: 'uracik',
				mapping: 'uracik'
			},{
				name: 'udiskon',
				mapping: 'udiskon'
			},{
				name: 'ucc',
				mapping: 'ucc'
			},{
				name: 'utotal',
				mapping: 'utotal'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'utotaltrans',
				mapping: 'utotaltrans'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			}]
		});
		return master;
	}

//=======================================================
	function dm_vlapobatdokter(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlapobatdokter',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_vlapobatdokter_fl(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlapobatdokter_fl',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokuitansi',
				mapping: 'nokuitansi'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'kditem',
				mapping: 'kditem'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			}]
		});
		return master;
	}

//=======================================================
	function dm_vlappelayanan(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlappelayanan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmpelayanan',
				mapping: 'nmpelayanan'
			},{
				name: 'parent',
				mapping: 'parent'
			},{
				name: 'tarif',
				mapping: 'tarif'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'tarifjmx',
				mapping: 'tarifjmx'
			},{
				name: 'idnotadetx',
				mapping: 'idnotadetx'
			}]
		});
		return master;
	}

//=======================================================
	function dm_vlapksbelum(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlapksbelum',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_vlapksbelumrj(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlapkeuangan_controller/get_vlapksbelumrj',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm',
				type: 'int'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			}]
		});
		return master;
	}

//=======================================================
	function dm_history(){
		var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'rekammedis_controller/getHistory',
			method: 'POST'
		}),
		params: {
			start: 0,
			limit: 50
		},
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
				name: "norm", mapping: "norm", type: 'int'
			},{
				name: "nmbagian", mapping: "nmbagian"
			},{
				name: "tglminta", mapping: "tglminta"
			},{
				name: "jamminta", mapping: "jamminta"
			},{
				name: "tglkeluar", mapping: "tglkeluar"
			},{
				name: "jamkeluar", mapping: "jamkeluar"
			},{
				name: "useridkeluar", mapping: "useridkeluar"
			},{
				name: "tglkembali", mapping: "tglkembali"
			},{
				name: "jamkembali", mapping: "jamkembali"
			},{
				name: "useridkembali", mapping: "useridkembali"
			}]
		});	
		return master;
	}
//=======================================================
//Laporan Persedian
function dm_barangkartustok(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kartustok_controller/get_brgbagian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuanbsr',
				mapping: 'idsatuanbsr'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'stokminbagian',
				mapping: 'stokminbagian'
			},{
				name: 'stokmaxbagian',
				mapping: 'stokmaxbagian'
			}]
		});
		return master;
	}

//=======================================================
function dm_kartustok(){
	var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kartustok_controller/get_kartustok',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idkartustok',
				mapping: 'idkartustok'
			},{
				name: 'idjnskartustok',
				mapping: 'idjnskartustok'
			},{
				name: 'tglkartustok',
				mapping: 'tglkartustok'
			},{
				name: 'jamkartustok',
				mapping: 'jamkartustok'
			},{
				name: 'saldoawal',
				mapping: 'saldoawal'
			},{
				name: 'jmlmasuk',
				mapping: 'jmlmasuk'
			},{
				name: 'jmlkeluar',
				mapping: 'jmlkeluar'
			},{
				name: 'saldoakhir',
				mapping: 'saldoakhir'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'tglinput',
				mapping: 'tglinput'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'noref',
				mapping: 'noref'
			},{
				name: 'kode',
				mapping: 'kode'
			},{
				name: 'nama',
				mapping: 'nama'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmjnskartustok',
				mapping: 'nmjnskartustok'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'nilaisaldoawal',
				mapping: 'nilaisaldoawal'
			},{
				name: 'nilaisaldomasuk',
				mapping: 'nilaisaldomasuk'
			},{
				name: 'nilaisaldokeluar',
				mapping: 'nilaisaldokeluar'
			},{
				name: 'nilaisaldoakhir',
				mapping: 'nilaisaldoakhir'
			}]
		});
		return master;
}

//=======================================================
function dm_lappersedian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'lappembelian_controller/get_lappo',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			baseParams:{
				key:'0'
			},
			fields: [{
				name: 'nopo',
				mapping: 'nopo',
			},{
				name: 'tglpo',
				mapping: 'tglpo'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'qtybonus',
				mapping: 'qtybonus'
			},{
				name: 'hargabeli',
				mapping: 'hargabeli'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'hargajual',
				mapping: 'hargajual'
			},{
				name: 'ppn',
				mapping: 'ppn'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'userid',
				mapping: 'userid'
			}]
			});
		return master;
	}

	
function dm_lappersediandet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'lappembelian_controller/get_lappodet',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			baseParams:{
				key:'0'
			},
			fields: [{
				name: 'nopo',
				mapping: 'nopo',
			},{
				name: 'tglpo',
				mapping: 'tglpo'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idsatuan',
				mapping: 'idsatuan'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'qtybonus',
				mapping: 'qtybonus'
			},{
				name: 'hargabeli',
				mapping: 'hargabeli'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'hargajual',
				mapping: 'hargajual'
			},{
				name: 'ppn',
				mapping: 'ppn'
			},{
				name: 'kdsupplier',
				mapping: 'kdsupplier'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'userid',
				mapping: 'userid'
			},{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
			});
		return master;
	}
	
//=======================================================
function dm_lapbrgbagian(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'lapbrgbagian_controller/get_lapbrgbagian',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'idjnsbrg',
				mapping: 'idjnsbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'idsatuanbsr',
				mapping: 'idsatuanbsr'
			},{
				name: 'nmsatuanbsr',
				mapping: 'nmsatuanbsr'
			},{
				name: 'idsatuankcl',
				mapping: 'idsatuankcl'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'rasio',
				mapping: 'rasio'
			},{
				name: 'stoknowbagian',
				mapping: 'stoknowbagian'
			},{
				name: 'stokminbagian',
				mapping: 'stokminbagian'
			},{
				name: 'stokmaxbagian',
				mapping: 'stokmaxbagian'
			}]
		});
		return master;
	}

//=======================================================
	function dm_lappengeluaranbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'lappengeluaranbrg_controller/get_lappengeluaranbrg',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nokeluarbrg',
				mapping: 'nokeluarbrg'
			},{
				name: 'tglkeluar',
				mapping: 'tglkeluar'
			},{
				name: 'jamkeluar',
				mapping: 'jamkeluar'
			},{
				name: 'idbagiandari',
				mapping: 'idbagiandari'
			},{
				name: 'nmbagiandari',
				mapping: 'nmbagiandari'
			},{
				name: 'idbagianuntuk',
				mapping: 'idbagianuntuk'
			},{
				name: 'nmbagianuntuk',
				mapping: 'nmbagianuntuk'
			},{
				name: 'penerima',
				mapping: 'penerima'
			},{
				name: 'idsttransaksi',
				mapping: 'idsttransaksi'
			},{
				name: 'nmsttransaksi',
				mapping: 'nmsttransaksi'
			},{
				name: 'idstsetuju',
				mapping: 'idstsetuju'
			},{
				name: 'nmstsetuju',
				mapping: 'nmstsetuju'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'nmjnsbrg',
				mapping: 'nmjnsbrg'
			},{
				name: 'nmsatuankcl',
				mapping: 'nmsatuankcl'
			},{
				name: 'qty',
				mapping: 'qty'
			}]
		});
		return master;
	}

//=======================================================
	function dm_lappoperbrg(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'lappoperbrg_controller/get_lappoperbrg',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nopo',
				mapping: 'nopo'
			},{
				name: 'tglpo',
				mapping: 'tglpo'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'nmsatuan',
				mapping: 'nmsatuan'
			},{
				name: 'qty',
				mapping: 'qty'
			},{
				name: 'qtybonus',
				mapping: 'qtybonus'
			},{
				name: 'hargabeli',
				mapping: 'hargabeli'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'jmlhrgtmbhdiskon',
				mapping: 'jmlhrgtmbhdiskon'
			},{
				name: 'ppn',
				mapping: 'ppn'
			},{
				name: 'jmlhrgtmbhppn',
				mapping: 'jmlhrgtmbhppn'
			},{
				name: 'nmsupplier',
				mapping: 'nmsupplier'
			},{
				name: 'stlunas',
				mapping: 'stlunas'
			}]
		});
		return master;
	}
	
//=======================================================
	function dm_brgdipodet(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'lappoperbrg_controller/get_brgdipodet',
				method: 'POST'
			}),
			baseParams: {
				key: '0'
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			}]
		});
		return master;
	}
	
//=======================================================
function dm_app1(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'purchaseorder_controller/app1',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmset', mapping:'nmset'
		}]
	});	
		return master;
}

function dm_app_pg(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 't_karyawan_controller/app1',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmset', mapping:'nmset'
		}]
	});	
		return master;
}
function dm_apphondok(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/app1',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmset', mapping:'nmset'
		}]
	});	
		return master;
}
function dm_app2(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'purchaseorder_controller/app2',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmset', mapping:'nmset'
		}]
	});	
		return master;
}
//=======================================================
/* Rekam Medis */
//=======================================================

function dm_caralahir(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'caralahir_controller/get_caralahir',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
	fields:[{
			name:'idcaralahir',
			mapping: 'idcaralahir'
		},{
			name: 'kdcaralahir',
			mapping: 'kdcaralahir'
		},{
			name: 'nmcaralahir',
			mapping: 'nmcaralahir'
		}]
	});
		return master;
}

//=======================================================
function dm_kondisilahir(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'kondisilahir_controller/get_kondisilahir',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
	fields:[{
			name:'idkonlhr',
			mapping: 'idkonlhr'
		},{
			name: 'kdkonlhr',
			mapping: 'kdkonlhr'
		},{
			name: 'nmkonlhr',
			mapping: 'nmkonlhr'
		}]
	});
		return master;
}
//=======================================================
function dm_skl(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'skl_controller/get_skl',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idskl', 
			mapping:'idskl'
		},{
			name: 'kdskl',
			mapping: 'kdskl'
		},{
			name: 'nmayah',
			mapping: 'nmayah'
		},{
			name: 'nmbayi',
			mapping: 'nmbayi'
		},{
			name: 'tglkelahiran',
			mapping: 'tglkelahiran'
		},{
			name: 'htglkelahiran',
			mapping: 'htglkelahiran'
		},{
			name: 'jamkelahiran',
			mapping: 'jamkelahiran'
		},{
			name: 'beratkelahiran',
			mapping: 'beratkelahiran'
		},{
			name: 'panjangkelahiran',
			mapping: 'panjangkelahiran'
		},{
			name: 'anakke',
			mapping: 'anakke'
		},{
			name: 'tgllahiribu',
			mapping: 'tgllahiribu'
		},{
			name: 'thnibu',
			mapping: 'thnibu'
		},{
			name: 'blnibu',
			mapping: 'blnibu'
		},{
			name: 'tglibu',
			mapping: 'tglibu'
		},{
			name: 'tgllahirayah',
			mapping: 'tgllahirayah'
		},{
			name: 'thnayah',
			mapping: 'thnayah'
		},{
			name: 'blnayah',
			mapping: 'blnayah'
		},{
			name: 'tglayah',
			mapping: 'tglayah'
		},{
			name: 'tglinput',
			mapping: 'tglinput'
		},{
			name: 'userid',
			mapping: 'userid'
		},{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'iddokter',
			mapping: 'iddokter'
		},{
			name: 'idjnskelamin',
			mapping: 'idjnskelamin'
		},{
			name: 'nmjnskelamin',
			mapping: 'nmjnskelamin'
		},{
			name: 'idkembar',
			mapping: 'idkembar'
		},{
			name: 'idkonlhr',
			mapping: 'idkonlhr'
		},{
			name: 'nmkonlhr',
			mapping: 'nmkonlhr'
		},{
			name: 'idcaralahir',
			mapping: 'idcaralahir'
		},{
			name: 'nmcaralahir',
			mapping: 'nmcaralahir'
		},{
			name: 'tgljaminput',
			mapping: 'tgljaminput'
		},{
			name: 'nmibu',
			mapping: 'nmibu'
		},{
			name: 'alamat',
			mapping: 'alamat'
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'normanak',
			mapping: 'normanak'
		},{
			name: 'noreganak',
			mapping: 'noreganak'
		}]
	});	
		return master;
}
//=======================================================
function dm_vskl(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'skl_controller/get_vskl',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idskl', 
			mapping:'idskl'
		},{
			name: 'kdskl',
			mapping: 'kdskl'
		},{
			name: 'nmayah',
			mapping: 'nmayah'
		},{
			name: 'nmbayi',
			mapping: 'nmbayi'
		},{
			name: 'tglkelahiran',
			mapping: 'tglkelahiran'
		},{
			name: 'jamkelahiran',
			mapping: 'jamkelahiran'
		},{
			name: 'beratkelahiran',
			mapping: 'beratkelahiran'
		},{
			name: 'panjangkelahiran',
			mapping: 'panjangkelahiran'
		},{
			name: 'anakke',
			mapping: 'anakke'
		},{
			name: 'tgllahiribu',
			mapping: 'tgllahiribu'
		},{
			name: 'thnibu',
			mapping: 'thnibu'
		},{
			name: 'blnibu',
			mapping: 'blnibu'
		},{
			name: 'tglibu',
			mapping: 'tglibu'
		},{
			name: 'tgllahirayah',
			mapping: 'tgllahirayah'
		},{
			name: 'thnayah',
			mapping: 'thnayah'
		},{
			name: 'blnayah',
			mapping: 'blnayah'
		},{
			name: 'tglayah',
			mapping: 'tglayah'
		},{
			name: 'tglinput',
			mapping: 'tglinput'
		},{
			name: 'userid',
			mapping: 'userid'
		},{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'iddokter',
			mapping: 'iddokter'
		},{
			name: 'idjnskelamin',
			mapping: 'idjnskelamin'
		},{
			name: 'idkembar',
			mapping: 'idkembar'
		},{
			name: 'idkonlhr',
			mapping: 'idkonlhr'
		},{
			name: 'idcaralahir',
			mapping: 'idcaralahir'
		},{
			name: 'tgljaminput',
			mapping: 'tgljaminput'
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar'
		},{
			name: 'nmibu',
			mapping: 'nmibu'
		},{
			name: 'alamat',
			mapping: 'alamat'
		}]
	});	
		return master;
}
//=======================================================
function dm_vskl2(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'skl_controller/get_vskl2',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idskl', 
			mapping:'idskl'
		},{
			name: 'kdskl',
			mapping: 'kdskl'
		},{
			name: 'nmayah',
			mapping: 'nmayah'
		},{
			name: 'nmbayi',
			mapping: 'nmbayi'
		},{
			name: 'tglkelahiran',
			mapping: 'tglkelahiran'
		},{
			name: 'jamkelahiran',
			mapping: 'jamkelahiran'
		},{
			name: 'beratkelahiran',
			mapping: 'beratkelahiran'
		},{
			name: 'panjangkelahiran',
			mapping: 'panjangkelahiran'
		},{
			name: 'anakke',
			mapping: 'anakke'
		},{
			name: 'tgllahiribu',
			mapping: 'tgllahiribu'
		},{
			name: 'thnibu',
			mapping: 'thnibu'
		},{
			name: 'blnibu',
			mapping: 'blnibu'
		},{
			name: 'tglibu',
			mapping: 'tglibu'
		},{
			name: 'tgllahirayah',
			mapping: 'tgllahirayah'
		},{
			name: 'thnayah',
			mapping: 'thnayah'
		},{
			name: 'blnayah',
			mapping: 'blnayah'
		},{
			name: 'tglayah',
			mapping: 'tglayah'
		},{
			name: 'tglinput',
			mapping: 'tglinput'
		},{
			name: 'userid',
			mapping: 'userid'
		},{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'iddokter',
			mapping: 'iddokter'
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar'
		},{
			name: 'idjnskelamin',
			mapping: 'idjnskelamin'
		},{
			name: 'idkembar',
			mapping: 'idkembar'
		},{
			name: 'idkonlhr',
			mapping: 'idkonlhr'
		},{
			name: 'idcaralahir',
			mapping: 'idcaralahir'
		},{
			name: 'tgljaminput',
			mapping: 'tgljaminput'
		},{
			name: 'nmcaralahir',
			mapping: 'nmcaralahir'
		},{
			name: 'nmjnskelamin',
			mapping: 'nmjnskelamin'
		},{
			name: 'nmkonlhr',
			mapping: 'nmkonlhr'
		},{
			name: 'normanak',
			mapping: 'normanak'
		},{
			name: 'noreganak',
			mapping: 'noreganak'
		}]
	});	
		return master;
}

//=======================================================
function dm_laprmkunjunganrj(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmkunjunganrj_controller/get_laprmkunjunganrj',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'tglkuitansi', 
			mapping:'tglkuitansi'
		},{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'norm',
			mapping: 'norm'
		},{
			name: 'stpasien',
			mapping: 'stpasien'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'umurtahun',
			mapping: 'umurtahun'
		},{
			name: 'nmjnskelamin',
			mapping: 'nmjnskelamin'
		},{
			name: 'alamat',
			mapping: 'alamat'
		},{
			name: 'kec',
			mapping: 'kec'
		},{
			name: 'kot',
			mapping: 'kot'
		},{
			name: 'notelp',
			mapping: 'notelp'
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar'
		},{
			name: 'nmjnspelayanan',
			mapping: 'nmjnspelayanan'
		},{
			name: 'nmpenjamin',
			mapping: 'nmpenjamin'
		},{
			name: 'nmpenyakiteng',
			mapping: 'nmpenyakiteng'
		}]
	});	
		return master;
}

//=======================================================
function dm_laprmkunjunganri(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmkunjunganri_controller/get_laprmkunjunganri',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'tglmasuk', 
			mapping:'tglmasuk'
		},{
			name: 'tglkuitansi', 
			mapping:'tglkuitansi'
		},{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'norm',
			mapping: 'norm'
		},{
			name: 'stpasien',
			mapping: 'stpasien'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'umurtahun',
			mapping: 'umurtahun'
		},{
			name: 'nmjnskelamin',
			mapping: 'nmjnskelamin'
		},{
			name: 'alamat',
			mapping: 'alamat'
		},{
			name: 'kec',
			mapping: 'kec'
		},{
			name: 'kot',
			mapping: 'kot'
		},{
			name: 'notelp',
			mapping: 'notelp'
		},{
			name: 'lamarawat',
			mapping: 'lamarawat'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		},{
			name: 'nmklstarif',
			mapping: 'nmklstarif'
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar'
		},{
			name: 'nmjnskasus',
			mapping: 'nmjnskasus'
		},{
			name: 'nmcaradatang',
			mapping: 'nmcaradatang'
		},{
			name: 'nmstkeluar',
			mapping: 'nmstkeluar'
		},{
			name: 'nmcarakeluar',
			mapping: 'nmcarakeluar'
		},{
			name: 'nmpenjamin',
			mapping: 'nmpenjamin'
		},{
			name: 'nmpenyakiteng',
			mapping: 'nmpenyakiteng'
		}]
	});	
		return master;
}

//=======================================================
function dm_rmpenyakitterbanyakrj(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmpenyakitterbanyakrj_controller/get_laprmpenyakitterbanyakrj',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idkodifikasi', 
			mapping:'idkodifikasi'
		},{
			name: 'idpenyakit', 
			mapping:'idpenyakit'
		},{
			name: 'dtd', 
			mapping:'dtd'
		},{
			name: 'kdpenyakit', 
			mapping:'kdpenyakit'
		},{
			name: 'nmpenyakit', 
			mapping:'nmpenyakit'
		},{
			name: 'nmpenyakiteng', 
			mapping:'nmpenyakiteng'
		},{
			name: 'kot', 
			mapping:'kot'
		},{
			name: 'idpenjamin', 
			mapping:'idpenjamin'
		},{
			name: 'ukd6haril', 
			mapping:'ukd6haril'
		},{
			name: 'ukd6harip', 
			mapping:'ukd6harip'
		},{
			name: 'ukd28haril', 
			mapping:'ukd28haril'
		},{
			name: 'ukd28harip', 
			mapping:'ukd28harip'
		},{
			name: 'ukd1thnl', 
			mapping:'ukd1thnl'
		},{
			name: 'ukd1thnp', 
			mapping:'ukd1thnp'
		},{
			name: 'ukd4thnl', 
			mapping:'ukd4thnl'
		},{
			name: 'ukd4thnp', 
			mapping:'ukd4thnp'
		},{
			name: 'ukd14thnl', 
			mapping:'ukd14thnl'
		},{
			name: 'ukd14thnp', 
			mapping:'ukd14thnp'
		},{
			name: 'ukd24thnl', 
			mapping:'ukd24thnl'
		},{
			name: 'ukd24thnp', 
			mapping:'ukd24thnp'
		},{
			name: 'ukd44thnl', 
			mapping:'ukd44thnl'
		},{
			name: 'ukd44thnp', 
			mapping:'ukd44thnp'
		},{
			name: 'ukd64thnl', 
			mapping:'ukd64thnl'
		},{
			name: 'ukd64thnp', 
			mapping:'ukd64thnp'
		},{
			name: 'uld64thnl', 
			mapping:'uld64thnl'
		},{
			name: 'uld64thnp', 
			mapping:'uld64thnp'
		},{
			name: 'kbl', 
			mapping:'kbl'
		},{
			name: 'kbp', 
			mapping:'kbp'
		},{
			name: 'jmlkb', 
			mapping:'jmlkb'
		},{
			name: 'jmlkunjungan', 
			mapping:'jmlkunjungan'
		}]
	});	
		return master;
}

//=======================================================
function dm_rmpenyakitterbanyakri(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmpenyakitterbanyakri_controller/get_laprmpenyakitterbanyakri',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idkodifikasi', 
			mapping:'idkodifikasi'
		},{
			name: 'idpenyakit', 
			mapping:'idpenyakit'
		},{
			name: 'dtd', 
			mapping:'dtd'
		},{
			name: 'kdpenyakit', 
			mapping:'kdpenyakit'
		},{
			name: 'nmpenyakit', 
			mapping:'nmpenyakit'
		},{
			name: 'nmpenyakiteng', 
			mapping:'nmpenyakiteng'
		},{
			name: 'kot', 
			mapping:'kot'
		},{
			name: 'idpenjamin', 
			mapping:'idpenjamin'
		},{
			name: 'ukd6haril', 
			mapping:'ukd6haril'
		},{
			name: 'ukd6harip', 
			mapping:'ukd6harip'
		},{
			name: 'ukd28haril', 
			mapping:'ukd28haril'
		},{
			name: 'ukd28harip', 
			mapping:'ukd28harip'
		},{
			name: 'ukd1thnl', 
			mapping:'ukd1thnl'
		},{
			name: 'ukd1thnp', 
			mapping:'ukd1thnp'
		},{
			name: 'ukd4thnl', 
			mapping:'ukd4thnl'
		},{
			name: 'ukd4thnp', 
			mapping:'ukd4thnp'
		},{
			name: 'ukd14thnl', 
			mapping:'ukd14thnl'
		},{
			name: 'ukd14thnp', 
			mapping:'ukd14thnp'
		},{
			name: 'ukd24thnl', 
			mapping:'ukd24thnl'
		},{
			name: 'ukd24thnp', 
			mapping:'ukd24thnp'
		},{
			name: 'ukd44thnl', 
			mapping:'ukd44thnl'
		},{
			name: 'ukd44thnp', 
			mapping:'ukd44thnp'
		},{
			name: 'ukd64thnl', 
			mapping:'ukd64thnl'
		},{
			name: 'ukd64thnp', 
			mapping:'ukd64thnp'
		},{
			name: 'uld64thnl', 
			mapping:'uld64thnl'
		},{
			name: 'uld64thnp', 
			mapping:'uld64thnp'
		},{
			name: 'jmlkhidupl', 
			mapping:'jmlkhidupl'
		},{
			name: 'jmlkhidupp', 
			mapping:'jmlkhidupp'
		},{
			name: 'jmlkmatil', 
			mapping:'jmlkmatil'
		},{
			name: 'jmlkmatip', 
			mapping:'jmlkmatip'
		},{
			name: 'jumlah', 
			mapping:'jumlah'
		}]
	});	
		return master;
}

//=======================================================
function dm_lrmpenjamin(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmpenyakitterbanyakrj_controller/get_lrmpenjamin',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idpenjamin', 
			mapping:'idpenjamin'
		},{
			name: 'nmpenjamin', 
			mapping:'nmpenjamin'
		}],
		listeners : {
			load : function() {
			var rec = new this.recordType({idpenjamin:'', nmpenjamin:'-Pilih-'});
			rec.commit();
			this.add(rec);
			}
		}
	});	
		return master;
}


//=======================================================
function dm_rmkiaibu1(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmkiaibu1_controller/get_laprmkiaibu1',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'tglkuitansi', 
			mapping:'tglkuitansi'
		},{
			name: 'noreg', 
			mapping:'noreg'
		},{
			name: 'norm', 
			mapping:'norm'
		},{
			name: 'stpasien', 
			mapping:'stpasien'
		},{
			name: 'nmpasien', 
			mapping:'nmpasien'
		},{
			name: 'gravidag', 
			mapping:'gravidag'
		},{
			name: 'gravidap', 
			mapping:'gravidap'
		},{
			name: 'gravidaa', 
			mapping:'gravidaa'
		},{
			name: 'usiakehamilan', 
			mapping:'usiakehamilan'
		},{
			name: 'kunkehamilan', 
			mapping:'kunkehamilan'
		},{
			name: 'imtt', 
			mapping:'imtt'
		},{
			name: 'labhcv', 
			mapping:'labhcv'
		},{
			name: 'labvdrl', 
			mapping:'labvdrl'
		},{
			name: 'iud', 
			mapping:'iud'
		},{
			name: 'nmpenyakiteng', 
			mapping:'nmpenyakiteng'
		},{
			name: 'kot', 
			mapping:'kot'
		},{
			name: 'nmcaradatang', 
			mapping:'nmcaradatang'
		},{
			name: 'nmpenjamin', 
			mapping:'nmpenjamin'
		}]
	});	
		return master;
}

//=======================================================
function dm_rmkiaibu2(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmkiaibu2_controller/get_laprmkiaibu2',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'tglmasuk', 
			mapping:'tglmasuk'
		},{
			name: 'tglkuitansi', 
			mapping:'tglkuitansi'
		},{
			name: 'noreg', 
			mapping:'noreg'
		},{
			name: 'norm', 
			mapping:'norm'
		},{
			name: 'nmpasien', 
			mapping:'nmpasien'
		},{
			name: 'clahirn', 
			mapping:'clahirn'
		},{
			name: 'clahirsc', 
			mapping:'clahirsc'
		},{
			name: 'clahirll', 
			mapping:'clahirll'
		},{
			name: 'nakes', 
			mapping:'nakes'
		},{
			name: 'kf1', 
			mapping:'kf1'
		},{
			name: 'kf2', 
			mapping:'kf2'
		},{
			name: 'kf3', 
			mapping:'kf3'
		},{
			name: 'nmpenyakiteng', 
			mapping:'nmpenyakiteng'
		},{
			name: 'konhir', 
			mapping:'konhir'
		},{
			name: 'jkelamin', 
			mapping:'jkelamin'
		},{
			name: 'nmstkeluar', 
			mapping:'nmstkeluar'
		},{
			name: 'nmkematian', 
			mapping:'nmkematian'
		},{
			name: 'kot', 
			mapping:'kot'
		},{
			name: 'nmcaradatang', 
			mapping:'nmcaradatang'
		},{
			name: 'nmpenjamin', 
			mapping:'nmpenjamin'
		}]
	});	
		return master;
}

//=======================================================
function dm_rmkiaanak1(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmkiaanak1_controller/get_laprmkiaanak1',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'tglkuitansi', 
			mapping:'tglkuitansi'
		},{
			name: 'noreg', 
			mapping:'noreg'
		},{
			name: 'norm', 
			mapping:'norm'
		},{
			name: 'stpasien', 
			mapping:'stpasien'
		},{
			name: 'nmpasien', 
			mapping:'nmpasien'
		},{
			name: 'u29hr3bln', 
			mapping:'u29hr3bln'
		},{
			name: 'u3bln6bln', 
			mapping:'u3bln6bln'
		},{
			name: 'u6bln9bln', 
			mapping:'u6bln9bln'
		},{
			name: 'u9bln11bln', 
			mapping:'u9bln11bln'
		},{
			name: 'nmpenyakiteng', 
			mapping:'nmpenyakiteng'
		},{
			name: 'kot', 
			mapping:'kot'
		},{
			name: 'nmcaradatang', 
			mapping:'nmcaradatang'
		},{
			name: 'nmpenjamin', 
			mapping:'nmpenjamin'
		}]
	});	
		return master;
}

//=======================================================
function dm_rmkiaanak2(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rmkiaanak2_controller/get_laprmkiaanak2',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'tglmasuk', 
			mapping:'tglmasuk'
		},{
			name: 'tglkuitansi', 
			mapping:'tglkuitansi'
		},{
			name: 'noreganak', 
			mapping:'noreganak'
		},{
			name: 'norm', 
			mapping:'norm'
		},{
			name: 'nmpasien', 
			mapping:'nmpasien'
		},{
			name: 'nmcaralahir', 
			mapping:'nmcaralahir'
		},{
			name: 'kn1', 
			mapping:'kn1'
		},{
			name: 'kn2', 
			mapping:'kn2'
		},{
			name: 'kn3', 
			mapping:'kn3'
		},{
			name: 'jkelamin', 
			mapping:'jkelamin'
		},{
			name: 'nmpenyakiteng', 
			mapping:'nmpenyakiteng'
		},{
			name: 'nmstkeluar', 
			mapping:'nmstkeluar'
		},{
			name: 'nmkematian', 
			mapping:'nmkematian'
		},{
			name: 'kot', 
			mapping:'kot'
		},{
			name: 'nmcaradatang', 
			mapping:'nmcaradatang'
		},{
			name: 'nmpenjamin', 
			mapping:'nmpenjamin'
		}]
	});	
		return master;
}

//=======================================================
/* Laporan Rawat Jalan */
//=======================================================
function dm_batalrj(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rawatjalan/get_batal',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'nmpasien',
			mapping: 'nmpasien',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'norm',
			mapping: 'norm',
		},{
			name: 'nmpekerjaan',
			mapping: 'nmpekerjaan',
		},{
			name: 'nmklsrawat',
			mapping: 'nmklsrawat',
		},{
			name: 'kamar',
			mapping: 'kamar',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar',
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		},{
			name: 'idbagiankirim',
			mapping: 'idbagiankirim'
		},{
			name: 'idcaradatang',
			mapping: 'idcaradatang'
		},{
			name: 'nmcaradatang',
			mapping: 'nmcaradatang'
		},{
			name: 'nmpenjamin',
			mapping: 'nmpenjamin'
		},{
			name: 'alamat',
			mapping: 'alamat'
		},{
			name: 'catatan',
			mapping: 'catatan'
		},{
			name: 'tglbatal',
			mapping: 'tglbatal'
		},{
			name: 'userbatal',
			mapping: 'userbatal'
		}]
	});	
		return master;
}

function dm_rawatjalan(){
var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rawatjalan/get_rawatjalan',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'nmpasien',
			mapping: 'nmpasien',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'norm',
			mapping: 'norm',
		},{
			name: 'nmpekerjaan',
			mapping: 'nmpekerjaan',
		},{
			name: 'nmklsrawat',
			mapping: 'nmklsrawat',
		},{
			name: 'kamar',
			mapping: 'kamar',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar',
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		},{
			name: 'idbagiankirim',
			mapping: 'idbagiankirim'
		},{
			name: 'idcaradatang',
			mapping: 'idcaradatang'
		},{
			name: 'nmcaradatang',
			mapping: 'nmcaradatang'
		},{
			name: 'nmpenjamin',
			mapping: 'nmpenjamin'
		},{
			name: 'alamat',
			mapping: 'alamat'
		}]
	});	
		return master;
}

function dm_bagianrj(){
var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rawatjalan/get_bagianrj',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idbagian',
			mapping: 'idbagian',
		},{
			name: 'nmbagian',
			mapping: 'nmbagian',
		},{
			name: 'kdbagian',
			mapping: 'kdbagian',
		}]
	});	
		return master;
}
//=======================================================
/* Laporan Rawat Inap */
//=======================================================
function dm_regpasinri(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rawatinap/get_pasienriperiode',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'nmpasien',
			mapping: 'nmpasien',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'norm',
			mapping: 'norm',
		},{
			name: 'nmpekerjaan',
			mapping: 'nmpekerjaan',
		},{
			name: 'nmklsrawat',
			mapping: 'nmklsrawat',
		},{
			name: 'nmkamar',
			mapping: 'nmkamar',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar',
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		},{
			name: 'idbagiankirim',
			mapping: 'idbagiankirim'
		},{
			name: 'idcaradatang',
			mapping: 'idcaradatang'
		},{
			name: 'nmcaradatang',
			mapping: 'nmcaradatang'
		},{
			name: 'keluhan',
			mapping: 'keluhan'
		},{
			name: 'nmkelastarif',
			mapping: 'nmkelastarif'
		}]
	});	
		return master;
}
//=======================================================
function dm_regpasinripen(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rawatinap/get_pasienpendidikan',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'nmpasien',
			mapping: 'nmpasien',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'norm',
			mapping: 'norm',
		},{
			name: 'nmpekerjaan',
			mapping: 'nmpekerjaan',
		},{
			name: 'nmklstarif',
			mapping: 'nmklstarif',
		},{
			name: 'nmkamar',
			mapping: 'nmkamar',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar',
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		},{
			name: 'idbagiankirim',
			mapping: 'idbagiankirim'
		},{
			name: 'idcaradatang',
			mapping: 'idcaradatang'
		},{
			name: 'nmcaradatang',
			mapping: 'nmcaradatang'
		},{
			name: 'nmpekerjaan',
			mapping: 'nmpekerjaan'
		},{
			name: 'idpekerjaan',
			mapping: 'idpekerjaan'
		},{
			name: 'nmpendidikan',
			mapping: 'nmpendidikan'
		},{
			name: 'idpendidikan',
			mapping: 'idpendidikan'
		}]
	});	
		return master;
}

function dm_rekap(){
var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rawatinap/get_rekap',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'nmbagian',
			mapping: 'nmbagian',
		},{
			name: 'expr1',
			mapping: 'expr1',
		}]
	});	
		return master;
}
//=======================================================
function dm_batal(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rawatinap/get_batal',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'nmpasien',
			mapping: 'nmpasien',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'norm',
			mapping: 'norm',
		},{
			name: 'nmpekerjaan',
			mapping: 'nmpekerjaan',
		},{
			name: 'nmklsrawat',
			mapping: 'nmklsrawat',
		},{
			name: 'kamar',
			mapping: 'kamar',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar',
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		},{
			name: 'idbagiankirim',
			mapping: 'idbagiankirim'
		},{
			name: 'idcaradatang',
			mapping: 'idcaradatang'
		},{
			name: 'nmcaradatang',
			mapping: 'nmcaradatang'
		},{
			name: 'nmpekerjaan',
			mapping: 'nmpekerjaan'
		},{
			name: 'idpekerjaan',
			mapping: 'idpekerjaan'
		},{
			name: 'nmpendidikan',
			mapping: 'nmpendidikan'
		},{
			name: 'idpendidikan',
			mapping: 'idpendidikan'
		},{
			name: 'userbatal',
			mapping: 'userbatal'
		},{
			name: 'tglbatal',
			mapping: 'tglbatal'
		},{
			name: 'catatan',
			mapping: 'catatan'
		}]
	});	
		return master;
}


//=======================================================
/* Laporan Persediaan */
//=======================================================
function dm_rekapstok(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_persediaan/get_rekapstok',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'kdbrg', 
			mapping:'kdbrg'
		},{
			name: 'nmbrg',
			mapping: 'nmbrg',
		},{
			name: 'nmjnskartustok',
			mapping: 'nmjnskartustok',
		},{
			name: 'beli',
			mapping: 'beli',
		},{
			name: 'returbeli',
			mapping: 'returbeli',
		},{
			name: 'tnota',
			mapping: 'tnota',
		},{
			name: 'rnota',
			mapping: 'rnota',
		},{
			name: 'pbb',
			mapping: 'pbb',
		},{
			name: 'returpbb',
			mapping: 'returpbb',
		},{
			name: 'bhp',
			mapping: 'bhp',
		},{
			name: 'batalpakai',
			mapping: 'batalpakai',
		},{
			name: 'so',
			mapping: 'so',
		},{
			name: 'terimabarang',
			mapping: 'terimabarang',
		},{
			name: 'returpnbb',
			mapping: 'returpnbb',
		},{
			name: 'bpnbb',
			mapping: 'bpnbb',
		},{
			name: 'bpenbb',
			mapping: 'bpenbb',
		},{
			name: 'returfarmasi',
			mapping: 'returfarmasi',
		},{
			name: 'returfarmasiumum',
			mapping: 'returfarmasiumum',
		},{
			name: 'pengfarmasi',
			mapping: 'pengfarmasi',
		},{
			name: 'pengfarmasiumum',
			mapping: 'pengfarmasiumum',
		},{
			name: 'tbl',
			mapping: 'tbl',
		},{
			name: 'sisa',
			mapping: 'sisa',
		},{
			name: 'njual',
			mapping: 'njual'
		},{
			name: 'nbeli',
			mapping: 'nbeli'
		}]
	});	
		return master;
}

function dm_kartupersediaan(){
	var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'laporan_persediaan/get_kartupersediaan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'noref',
				mapping: 'noref'
			},{
				name: 'tglkartustok',
				mapping: 'tglkartustok'
			},{
				name: 'jamkartustok',
				mapping: 'jamkartustok'
			},{
				name: 'saldoawal',
				mapping: 'saldoawal'
			},{
				name: 'jmlmasuk',
				mapping: 'jmlmasuk'
			},{
				name: 'rbm',
				mapping: 'rbm'
			},{
				name: 'jmlkeluar',
				mapping: 'jmlkeluar'
			},{
				name: 'rbk',
				mapping: 'rbk'
			},{
				name: 'saldoakhir',
				mapping: 'saldoakhir'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'nbeli',
				mapping: 'nbeli'
			},{
				name: 'njual',
				mapping: 'njual'
			}]
		});
		return master;

}
//=======================================================
function dm_lap_kartustok(){
	var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'lapstokpersediaan_controller/get_lapstokpersediaan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'kdbrg',
				mapping: 'kdbrg'
			},{
				name: 'nmbrg',
				mapping: 'nmbrg'
			},{
				name: 'sa',
				mapping: 'sa'
			},{
				name: 'beli',
				mapping: 'beli'
			},{
				name: 'jmlretbeli',
				mapping: 'jmlretbeli'
			},{
				name: 'jmltbl',
				mapping: 'jmltbl'
			},{
				name: 'jmldistribusibrg',
				mapping: 'jmldistribusibrg'
			},{
				name: 'jmldistribusimasukbrg',
				mapping: 'jmldistribusimasukbrg'
			},{
				name: 'jmlrkbb',
				mapping: 'jmlrkbb'
			},{
				name: 'jmlrmbg',
				mapping: 'jmlrmbg'
			},{
				name: 'jmlbhp',
				mapping: 'jmlbhp'
			},{
				name: 'jmljualbrg',
				mapping: 'jmljualbrg'
			},{
				name: 'jmlretjualbrg',
				mapping: 'jmlretjualbrg'
			},{
				name: 'sisa',
				mapping: 'sisa'
			},{
				name: 'hrgbeli',
				mapping: 'hrgbeli'
			},{
				name: 'hrgjual',
				mapping: 'hrgjual'
			},{
				name: 'nbli',
				mapping: 'nbli'
			},{
				name: 'njual',
				mapping: 'njual'
			}]
		});
		return master;
}


//=======================================================
/* Laporan Rekam Medis */
//=======================================================
function dm_sensusharian(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rekammedis/get_sensusharian',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'norm', 
			mapping:'norm'
		},{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'nmpasien',
			mapping: 'nmpasien',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'tglkeluar',
			mapping: 'tglkeluar',
		},{
			name: 'jamkeluar',
			mapping: 'jamkeluar',
		},{
			name: 'nmbagian',
			mapping: 'nmbagian',
		},{
			name: 'nmklsrawat',
			mapping: 'nmklsrawat',
		},{
			name: 'nmkamar',
			mapping: 'nmkamar',
		},{
			name: 'nmbed',
			mapping: 'nmbed',
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'keterangan',
			mapping: 'keterangan'
		}]
	});	
		return master;
}

//=======================================================
function dm_bor1(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/bor1',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmbagian', 
			mapping:'nmbagian'
		},{
			name: 'jumlahbed',
			mapping: 'jumlahbed',
		},{
			name: 'BOR',
			mapping: 'BOR',
		}]
	});	
		return master;
}


//=======================================================
function dm_avlos(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/avlos',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmbagian', 
			mapping:'nmbagian'
		},{
			name: 'jumlahbed',
			mapping: 'jumlahbed',
		},{
			name: 'avlos',
			mapping: 'avlos',
		}]
	});	
		return master;
}
//=======================================================
function dm_ndr(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/ndr',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmbagian', 
			mapping:'nmbagian'
		},{
			name: 'jumlahbed',
			mapping: 'jumlahbed',
		},{
			name: 'ndr',
			mapping: 'ndr',
		}]
	});	
		return master;
}

//=======================================================
function dm_gdr(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/gdr',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmbagian', 
			mapping:'nmbagian'
		},{
			name: 'jumlahbed',
			mapping: 'jumlahbed',
		},{
			name: 'gdr',
			mapping: 'gdr',
		}]
	});	
		return master;
}

function dm_bto(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/bto',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'nmbagian', 
			mapping:'nmbagian'
		},{
			name: 'jumlahbed',
			mapping: 'jumlahbed',
		},{
			name: 'bto',
			mapping: 'bto',
		}]
	});	
		return master;
}

//=======================================================
function dm_lapdaerah(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/get_lapdaerah',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'negara', 
			mapping:'negara'
		},{
			name: 'prop',
			mapping: 'prop',
		},{
			name: 'kebkec',
			mapping: 'kebkec',
		},{
			name: 'kota',
			mapping: 'kota',
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		}]
	});	
		return master;
}


//=======================================================

function dm_penyakitri(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/get_penyakitri',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idkodifikasi', 
			mapping:'idkodifikasi'
		},{
			name: 'idpenyakit',
			mapping: 'idpenyakit',
		},{
			name: 'kdpenyakit',
			mapping: 'kdpenyakit',
		},{
			name: 'nmpenyakit',
			mapping: 'nmpenyakit',
		},{
			name: 'nmpenyakiteng',
			mapping: 'nmpenyakiteng',
		},{
			name: 'idjnskelamin',
			mapping: 'idjnskelamin',
		},{
			name: 'perempuan',
			mapping: 'perempuan'
		},{
			name: 'laki',
			mapping: 'laki',
		},{
			name: 'nmjnskelamin',
			mapping: 'nmjnskelamin'
		},{
			name: 'idcarakeluar',
			mapping: 'idcarakeluar',
		},{
			name: 'pasienkeluar',
			mapping: 'pasienkeluar',
		},{
			name: 'mati',
			mapping: 'mati',
		},{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		}]
	});	
		return master;
}

function dm_penyakitrj(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/get_penyakitrj',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idkodifikasi', 
			mapping:'idkodifikasi'
		},{
			name: 'idpenyakit',
			mapping: 'idpenyakit',
		},{
			name: 'kdpenyakit',
			mapping: 'kdpenyakit',
		},{
			name: 'nmpenyakit',
			mapping: 'nmpenyakit',
		},{
			name: 'nmpenyakiteng',
			mapping: 'nmpenyakiteng',
		},{
			name: 'idjnskelamin',
			mapping: 'idjnskelamin',
		},{
			name: 'perempuan',
			mapping: 'perempuan'
		},{
			name: 'laki',
			mapping: 'laki',
		},{
			name: 'nmjnskelamin',
			mapping: 'nmjnskelamin'
		},{
			name: 'idcarakeluar',
			mapping: 'idcarakeluar',
		},{
			name: 'pasienkeluar',
			mapping: 'pasienkeluar',
		},{
			name: 'mati',
			mapping: 'mati',
		},{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		}]
	});	
		return master;
}


//=======================================================

function dm_kartuindeks(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/get_kartuindeks',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idkodifikasi', 
			mapping:'idkodifikasi'
		},{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'tglkeluar',
			mapping: 'tglkeluar',
		},{
			name: 'jamkeluar',
			mapping: 'jamkeluar',
		},{
			name: 'idjnskelamin',
			mapping: 'idjnskelamin'
		},{
			name: 'kdjnskelamin',
			mapping: 'kdjnskelamin',
		},{
			name: 'umurtahun',
			mapping: 'umurtahun',
		},{
			name: 'umurbulan',
			mapping: 'umurbulan',
		},{
			name: 'umurhari',
			mapping: 'umurhari',
		},{
			name: 'idpenyakit',
			mapping: 'idpenyakit'
		},{
			name: 'nmpenyakit',
			mapping: 'nmpenyakit'
		},{
			name: 'nmpenyakiteng',
			mapping: 'nmpenyakiteng'
		},{
			name: 'kdpenyakit',
			mapping: 'idstkeluar'
		},{
			name: 'nmstkeluar',
			mapping: 'nmstkeluar'
		},{
			name: 'idcarakeluar',
			mapping: 'idcarakeluar'
		},{
			name: 'nmcarakeluar',
			mapping: 'nmcarakeluar',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar'
		},{
			name: 'iddokter',
			mapping: 'iddokter'
		}]
	});	
		return master;
}

function dm_kartuindeksrj(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'Laporan_rekammedis/get_kartuindeksrj',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idkodifikasi', 
			mapping:'idkodifikasi'
		},{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'tglkeluar',
			mapping: 'tglkeluar',
		},{
			name: 'jamkeluar',
			mapping: 'jamkeluar',
		},{
			name: 'idjnskelamin',
			mapping: 'idjnskelamin'
		},{
			name: 'kdjnskelamin',
			mapping: 'kdjnskelamin',
		},{
			name: 'umurtahun',
			mapping: 'umurtahun',
		},{
			name: 'umurbulan',
			mapping: 'umurbulan',
		},{
			name: 'umurhari',
			mapping: 'umurhari',
		},{
			name: 'idpenyakit',
			mapping: 'idpenyakit'
		},{
			name: 'nmpenyakit',
			mapping: 'nmpenyakit'
		},{
			name: 'nmpenyakiteng',
			mapping: 'nmpenyakiteng'
		},{
			name: 'kdpenyakit',
			mapping: 'idstkeluar'
		},{
			name: 'nmstkeluar',
			mapping: 'nmstkeluar'
		},{
			name: 'idcarakeluar',
			mapping: 'idcarakeluar'
		},{
			name: 'nmcarakeluar',
			mapping: 'nmcarakeluar',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar'
		},{
			name: 'iddokter',
			mapping: 'iddokter'
		}]
	});	
		return master;
}

//=======================================================
function dm_lappasienkeluar(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'lappasienkeluar_pertgl_controller/get_lappasienkeluar',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'norm', 
			mapping:'norm'
		},{
			name: 'noreg',
			mapping: 'noreg',
		},{
			name: 'nmpasien',
			mapping: 'nmpasien',
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk',
		},{
			name: 'jammasuk',
			mapping: 'jammasuk',
		},{
			name: 'tglkeluar',
			mapping: 'tglkeluar',
		},{
			name: 'jamkeluar',
			mapping: 'jamkeluar',
		},{
			name: 'nmbagian',
			mapping: 'nmbagian',
		},{
			name: 'nmklsrawat',
			mapping: 'nmklsrawat',
		},{
			name: 'nmkamar',
			mapping: 'nmkamar',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar',
		},{
			name: 'nmpenjamin',
			mapping: 'nmpenjamin'
		}]
	});	
		return master;
}

//=======================================================


function dm_bagianri(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rekammedis/get_ruanganri',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idbagian', 
			mapping:'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian',
		}]
	});	
		return master;
}

/* Keuangan */
function dm_honor_rj(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_honor_rj',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: false,
		fields: [{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'tglreg',
			mapping: 'tglreg'
		},{
			name: 'norm',
			mapping: 'norm'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'tarifjm',
			mapping: 'tarifjm'
		},{
			name: 'diskonjm',
			mapping: 'diskonjm'
		},{
			name: 'jasamedis',
			mapping: 'jasamedis'
		},{
			name: 'diskonmedis',
			mapping: 'diskonmedis'
		},{
			name: 'zis',
			mapping: 'zis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		}]
	});
	
		return master;
}

function dm_honor_ri(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_honor_ri',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: false,
		fields: [{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'tglreg',
			mapping: 'tglreg'
		},{
			name: 'norm',
			mapping: 'norm'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'tarifjm',
			mapping: 'tarifjm'
		},{
			name: 'diskonjm',
			mapping: 'diskonjm'
		},{
			name: 'zis',
			mapping: 'zis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		},{
			name: 'tglmasuk',
			mapping: 'tglmasuk'
		},{
			name: 'tglkeluar',
			mapping: 'tglkeluar'
		},{
			name: 'idbagian',
			mapping: 'idbagian'
		},{
			name: 'nmbagian',
			mapping: 'nmbagian'
		},{
			name: 'jasamedis',
			mapping: 'jasamedis'
		},{
			name: 'diskonmedis',
			mapping: 'diskonmedis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		}]
	});
	
		return master;
}

function dm_honor_jm(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_honor_jm',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: false,
		fields: [{
			name: 'noreg',
			mapping: 'noreg'
		},{
			name: 'tglreg',
			mapping: 'tglreg'
		},{
			name: 'norm',
			mapping: 'norm'
		},{
			name: 'nmpasien',
			mapping: 'nmpasien'
		},{
			name: 'tarifjm',
			mapping: 'tarifjm'
		},{
			name: 'diskonjm',
			mapping: 'diskonjm'
		},{
			name: 'zis',
			mapping: 'zis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		},{
			name: 'jasamedis',
			mapping: 'jasamedis'
		},{
			name: 'diskonmedis',
			mapping: 'diskonmedis'
		},{
			name: 'jumlah',
			mapping: 'jumlah'
		}]
	});
	
		return master;
}


function dm_vdokter(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'timjasamedis_controller/get_vdokter',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'kddokter',
				mapping: 'kddokter'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'tptlahir',
				mapping: 'tptlahir'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'idspesialisasi',
				mapping: 'idspesialisasi'
			},{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'idstdokter',
				mapping: 'idstdokter'
			},{
				name: 'nmstdokter',
				mapping: 'nmstdokter'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'norek',
				mapping: 'norek'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'status',
				mapping: 'status'
			}]
		});
		return master;
}

function dm_stbayar(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'stbayar_controller/get_stbayar',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
				name: 'idstbayar', 
				mapping:'idstbayar'
			},{
				name: 'kdstbayar', 
				mapping:'kdstbayar'
			},{
				name: 'nmstbayar', 
				mapping:'nmstbayar'
			}]
		});
	return master;
}
/* ======================================================= */
function dm_hondok(){
var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'honor_controller/get_hondok',
			method: 'POST'
		}),
		baseParams: {
			cbx : false
		},
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
				name: 'nohondok', 
				mapping:'nohondok'
			},{
				name: 'tglhondok', 
				mapping:'tglhondok'
			},{
				name: 'tglawal', 
				mapping:'tglawal'
			},{
				name: 'jasadrjaga', 
				mapping:'jasadrjaga'
			},{
				name: 'potonganlain', 
				mapping:'potonganlain'
			},{
				name: 'userid', 
				mapping:'userid'
			},{
				name: 'nmlengkap', 
				mapping:'nmlengkap'
			},{
				name: 'iddokter', 
				mapping:'iddokter'
			},{
				name: 'nmdoktergelar', 
				mapping:'nmdoktergelar'
			},{
				name: 'nmstatus', 
				mapping:'nmstatus'
			},{
				name: 'nmstdokter', 
				mapping:'nmstdokter'
			},{
				name: 'approval', 
				mapping:'approval'
			},{
				name: 'idstbayar', 
				mapping:'idstbayar'
			},{
				name: 'nmstbayar', 
				mapping:'nmstbayar'
			},{
				name: 'tglinput', 
				mapping:'tglinput'
			},{
				name: 'catatan', 
				mapping:'catatan'
			},{
				name: 'jumlah', 
				mapping:'jumlah'
			},{
				name: 'tglakhir', 
				mapping:'tglakhir'
			},{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			},{
				name: 'periode',
				mapping: 'periode'
			},{
				name: 'norek',
				mapping: 'norek'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			}]
		});
	return master;

}
/* ======================================================= */
function dm_jtm(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'timjasamedis_controller/get_jtm',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'iddokter', 
			mapping:'iddokter'
		},{
			name: 'idstdoktertim',
			mapping: 'idstdoktertim',
		},{
			name: 'jumlah',
			mapping: 'jumlah',
		},{
			name: 'diskon',
			mapping: 'persendiskon',
		},{
			name: 'diskonrp',
			mapping: 'diskonrp',
		},{
			name: 'idjtm',
			mapping: 'idjtm',
		},{
			name: 'idnotadet',
			mapping: 'idnotadet',
		},{
			name: 'tgljtm',
			mapping: 'tgljtm',
		},{
			name: 'userid',
			mapping: 'userid',
		},{
			name: 'nmdoktergelar',
			mapping: 'nmdoktergelar',
		},{
			name: 'nmspesialisasi',
			mapping: 'nmspesialisasi',
		},{
			name: 'nmdoktertim',
			mapping: 'nmdoktertim',
		},{
			name: 'tarifdiskon',
			mapping: 'tarifdiskon'
		},{
			name: 'tarifjm',
			mapping: 'tarifjm'
		},{
			name: 'diskonjm',
			mapping: 'persentarif'
		}]
	});	
		return master;
}



/* ============================ Akuntansi ============================ */
function dm_saldo_akun_tahun(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'settingsaldoawal_controller/get_saldoawal_akun',
			method: 'POST'
		}),
		baseParams:{
			tahun: '',
			bulan: '',
		},
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'tahun', 
			mapping:'tahun'
		},{
			name: 'idakun',
			mapping: 'idakun',
		},{
			name: 'kdakun',
			mapping: 'kdakun',
		},{
			name: 'nmakun',
			mapping: 'nmakun',
		},{
			name: 'saldodebit',
			mapping: 'saldodebit',
		},{
			name: 'saldokredit',
			mapping: 'saldokredit',
		},{
			name: 'fieldsaldodebit',
			mapping: 'fieldsaldodebit',
		},{
			name: 'fieldsaldokredit',
			mapping: 'fieldsaldokredit',
		}]
	});	
	return master;
}

function dm_jns_jurnal(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'jnsjurnal_controller/get_jnsjurnal',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idjnsjurnal', 
			mapping:'idjnsjurnal'
		},{
			name: 'kdjnsjurnal',
			mapping: 'kdjnsjurnal',
		},{
			name: 'nmjnsjurnal',
			mapping: 'nmjnsjurnal',
		}]
	});	
	return master;
}

function dm_akun_jurnal()
{
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'jurnalumum_controller/akun_jurnal',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idakunparent', 
			mapping:'idakunparent'
		},{
			name: 'idakun', 
			mapping:'idakun'
		},{
			name: 'kdakun', 
			mapping:'kdakun'
		},{
			name: 'nmakun', 
			mapping:'nmakun'
		},{
			name: 'keterangan', 
			mapping:'keterangan'
		},{
			name: 'idklpakun', 
			mapping:'idklpakun'
		},{
			name: 'idjnsakun', 
			mapping:'idjnsakun'
		},{
			name: 'idstatus', 
			mapping:'idstatus'
		},{
			name: 'userid', 
			mapping:'userid'
		},{
			name: 'tglinput', 
			mapping:'tglinput'
		},{
			name: 'nmklpakun', 
			mapping:'nmklpakun'
		}]
	});	
	return master;

}

function dm_akun_pembayaran_supplier()
{
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'jurnalumum_controller/akun_pembayaran_supplier',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idakunparent', 
			mapping:'idakunparent'
		},{
			name: 'idakun', 
			mapping:'idakun'
		},{
			name: 'kdakun', 
			mapping:'kdakun'
		},{
			name: 'nmakun', 
			mapping:'nmakun'
		},{
			name: 'keterangan', 
			mapping:'keterangan'
		},{
			name: 'idklpakun', 
			mapping:'idklpakun'
		},{
			name: 'idjnsakun', 
			mapping:'idjnsakun'
		},{
			name: 'idstatus', 
			mapping:'idstatus'
		},{
			name: 'userid', 
			mapping:'userid'
		},{
			name: 'tglinput', 
			mapping:'tglinput'
		},{
			name: 'nmklpakun', 
			mapping:'nmklpakun'
		}]
	});	
	return master;

}


function dm_jurnal_umum(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'jurnalumum_controller/get_jurnal_umum',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'kdjurnal', 
			mapping:'kdjurnal'
		},{
			name: 'nojurnal', 
			mapping:'nojurnal'
		},{
			name: 'idjnsjurnal', 
			mapping:'idjnsjurnal'
		},{
			name: 'idbagian', 
			mapping:'idbagian'
		},{
			name: 'nokasir', 
			mapping:'nokasir'
		},{
			name: 'tgltransaksi', 
			mapping:'tgltransaksi'
		},{
			name: 'tgljurnal', 
			mapping:'tgljurnal'
		},{
			name: 'keterangan', 
			mapping:'keterangan'
		},{
			name: 'noreff', 
			mapping:'noreff'
		},{
			name: 'userid', 
			mapping:'userid'
		},{
			name: 'tglinput', 
			mapping:'tglinput'
		},{
			name: 'nominal', 
			mapping:'nominal'
		},{
			name: 'idjnstransaksi', 
			mapping:'idjnstransaksi'
		},{
			name: 'kdjnsjurnal', 
			mapping:'kdjnsjurnal'
		},{
			name: 'nmjnsjurnal', 
			mapping:'nmjnsjurnal'
		},{
			name: 'nmbagian', 
			mapping:'nmbagian'
		},{
			name: 'nmlengkap', 
			mapping:'nmlengkap'
		}]
	});	
	return master;
}

function dm_jurnal_umum_det()
{

	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'jurnalumum_controller/get_jurnal_umum_det',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'kdjurnal', 
			mapping:'kdjurnal'
		},{
			name: 'tahun', 
			mapping:'tahun'
		},{
			name: 'idakun', 
			mapping:'idakun'
		},{
			name: 'noreff', 
			mapping:'noreff'
		},{
			name: 'debit', 
			mapping:'debit'
		},{
			name: 'kredit', 
			mapping:'kredit'
		},{
			name: 'kdakun', 
			mapping:'kdakun'
		},{
			name: 'nmakun', 
			mapping:'nmakun'
		}]
	});	
	return master;
}


function dm_pettycash(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'pettycash_controller/get_petty_cash',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'kdjurnal', 
			mapping:'kdjurnal'
		},{
			name: 'nojurnal', 
			mapping:'nojurnal'
		},{
			name: 'idjnsjurnal', 
			mapping:'idjnsjurnal'
		},{
			name: 'idbagian', 
			mapping:'idbagian'
		},{
			name: 'nokasir', 
			mapping:'nokasir'
		},{
			name: 'tgltransaksi', 
			mapping:'tgltransaksi'
		},{
			name: 'tgljurnal', 
			mapping:'tgljurnal'
		},{
			name: 'keterangan', 
			mapping:'keterangan'
		},{
			name: 'noreff', 
			mapping:'noreff'
		},{
			name: 'userid', 
			mapping:'userid'
		},{
			name: 'tglinput', 
			mapping:'tglinput'
		},{
			name: 'nominal', 
			mapping:'nominal'
		},{
			name: 'idjnstransaksi', 
			mapping:'idjnstransaksi'
		},{
			name: 'status_posting', 
			mapping:'status_posting'
		},{
			name: 'kdjnsjurnal', 
			mapping:'kdjnsjurnal'
		},{
			name: 'nmjnsjurnal', 
			mapping:'nmjnsjurnal'
		},{
			name: 'nmbagian', 
			mapping:'nmbagian'
		},{
			name: 'nmlengkap', 
			mapping:'nmlengkap'
		},{
			name: 'idakundebit', 
			mapping:'idakundebit'
		},{
			name: 'idakunkredit', 
			mapping:'idakunkredit'
		},{
			name: 'akundebit', 
			mapping:'akundebit'
		},{
			name: 'akunkredit', 
			mapping:'akunkredit'
		}]
	});	
	return master;
}

function dm_pettycash_posting(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'pettycash_controller/get_petty_cash_posting',
			method: 'POST'
		}),
		baseParams:{
			tglawal: '',
			tglakhir: '',
		},
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
		    name: 'kdjurnal',
		    mapping: 'kdjurnal',
		}, {
		    name: 'nojurnal',
		    mapping: 'nojurnal',
		}, {
		    name: 'idjnsjurnal',
		    mapping: 'idjnsjurnal',
		}, {
		    name: 'idbagian',
		    mapping: 'idbagian',
		}, {
		    name: 'nokasir',
		    mapping: 'nokasir',
		}, {
		    name: 'tgltransaksi',
		    mapping: 'tgltransaksi',
		}, {
		    name: 'tgljurnal',
		    mapping: 'tgljurnal',
		}, {
		    name: 'keterangan',
		    mapping: 'keterangan',
		}, {
		    name: 'noreff',
		    mapping: 'noreff',
		}, {
		    name: 'userid',
		    mapping: 'userid',
		}, {
		    name: 'tglinput',
		    mapping: 'tglinput',
		}, {
		    name: 'nominal',
		    mapping: 'nominal',
		}, {
		    name: 'idjnstransaksi',
		    mapping: 'idjnstransaksi',
		}, {
		    name: 'penerima',
		    mapping: 'penerima',
		}, {
		    name: 'status_posting',
		    mapping: 'status_posting',
		}, {
		    name: 'terbilang',
		    mapping: 'terbilang',
		}]
	});	
	return master;
}

function dm_pettycash_postingdet(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'pettycash_controller/get_pettycash_postingdet',
			method: 'POST'
		}),
		baseParams:{
			kdjurnal: '',
		},
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
		    name: 'kdjurnal',
		    mapping: 'kdjurnal',
		}, {
		    name: 'idakun',
		    mapping: 'idakun',
		}, {
		    name: 'kdakun',
		    mapping: 'kdakun',
		}, {
		    name: 'nmakun',
		    mapping: 'nmakun',
		}, {
		    name: 'idklpakun',
		    mapping: 'idklpakun',
		}, {
		    name: 'nmklpakun',
		    mapping: 'nmklpakun',
		}, {
		    name: 'debit',
		    mapping: 'debit',
		}, {
		    name: 'kredit',
		    mapping: 'kredit',
		}]
	});	
	return master;
}


function dm_ket_pettycash(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'pettycash_controller/get_ket_petty_cash',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'keterangan', 
			mapping:'keterangan'
		}]
	});	
	return master;
}


function dm_hartatetap(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'hartatetap_controller/get_hartatetap',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields:[
				{
		    name: 'idhartatetap',
		    mapping: 'idhartatetap',
		}, {
		    name: 'idkelharta',
		    mapping: 'idkelharta',
		}, {
		    name: 'jenisharta',
		    mapping: 'jenisharta',
		}, {
		    name: 'nmhartatetap',
		    mapping: 'nmhartatetap',
		}, {
		    name: 'tglbeli',
		    mapping: 'tglbeli',
		}, {
		    name: 'hrgbeli',
		    mapping: 'hrgbeli',
		}, {
		    name: 'jml',
		    mapping: 'jml',
		}, {
		    name: 'totalbeli',
		    mapping: 'totalbeli',
		}, {
		    name: 'residu',
		    mapping: 'residu',
		}, {
		    name: 'umurekonomi',
		    mapping: 'umurekonomi',
		}, {
		    name: 'umurterpakai',
		    mapping: 'umurterpakai',
		}, {
		    name: 'kdkelharta',
		    mapping: 'kdkelharta',
		}, {
		    name: 'nmkelharta',
		    mapping: 'nmkelharta',
		}, {
		    name: 'idakunharta',
		    mapping: 'idakunharta',
		}, {
		    name: 'kdakunharta',
		    mapping: 'kdakunharta',
		}, {
		    name: 'nmakunharta',
		    mapping: 'nmakunharta',
		}, {
		    name: 'umurterpakai2',
		    mapping: 'umurterpakai2',
		}]
	});	
	return master;
}


function dm_hartatetapmenyusut(){
	var master = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'hartatetap_controller/get_hartatetapmenyusut',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields:[
				{
		    name: 'idhartatetap',
		    mapping: 'idhartatetap',
		}, {
		    name: 'idkelharta',
		    mapping: 'idkelharta',
		}, {
		    name: 'jenisharta',
		    mapping: 'jenisharta',
		}, {
		    name: 'nmhartatetap',
		    mapping: 'nmhartatetap',
		}, {
		    name: 'tglbeli',
		    mapping: 'tglbeli',
		}, {
		    name: 'hrgbeli',
		    mapping: 'hrgbeli',
		}, {
		    name: 'jml',
		    mapping: 'jml',
		}, {
		    name: 'totalbeli',
		    mapping: 'totalbeli',
		}, {
		    name: 'residu',
		    mapping: 'residu',
		}, {
		    name: 'umurekonomi',
		    mapping: 'umurekonomi',
		}, {
		    name: 'umurterpakai',
		    mapping: 'umurterpakai',
		}, {
		    name: 'kdkelharta',
		    mapping: 'kdkelharta',
		}, {
		    name: 'nmkelharta',
		    mapping: 'nmkelharta',
		}, {
		    name: 'idakunharta',
		    mapping: 'idakunharta',
		}, {
		    name: 'kdakunharta',
		    mapping: 'kdakunharta',
		}, {
		    name: 'nmakunharta',
		    mapping: 'nmakunharta',
		}, {
		    name: 'idakunpenyusutan',
		    mapping: 'idakunpenyusutan',
		}, {
		    name: 'kdakunpenyusutan',
		    mapping: 'kdakunpenyusutan',
		}, {
		    name: 'nmakunpenyusutan',
		    mapping: 'nmakunpenyusutan',
		}, {
		    name: 'umurterpakai2',
		    mapping: 'umurterpakai2',
		}, {
		    name: 'akumulasipenyusutan',
		    mapping: 'akumulasipenyusutan',
		}, {
		    name: 'nilaiharta',
		    mapping: 'nilaiharta',
		}]
	});	
	return master;
}

/* ===========================End Akuntansi ========================== */


// COMBO DOKTER
function dm_dokter_combo(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'dokter_controller/get_dokter_combo',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'kddokter',
				mapping: 'kddokter'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'tptlahir',
				mapping: 'tptlahir'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'alamat',
				mapping: 'alamat'
			},{
				name: 'notelp',
				mapping: 'notelp'
			},{
				name: 'nohp',
				mapping: 'nohp'
			},{
				name: 'idspesialisasi',
				mapping: 'idspesialisasi'
			},{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			},{
				name: 'idstatus',
				mapping: 'idstatus'
			},{
				name: 'nmstatus',
				mapping: 'nmstatus'
			},{
				name: 'idstdokter',
				mapping: 'idstdokter'
			},{
				name: 'nmstdokter',
				mapping: 'nmstdokter'
			},{
				name: 'catatan',
				mapping: 'catatan'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'idbank',
				mapping: 'idbank'
			},{
				name: 'nmbank',
				mapping: 'nmbank'
			},{
				name: 'norek',
				mapping: 'norek'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'cabang',
				mapping: 'cabang'
			},{
				name: 'idjnstenagamedis',
				mapping: 'idjnstenagamedis'
			},{
				name: 'nmjnstenagamedis',
				mapping: 'nmjnstenagamedis'
			}],
			listeners : {
				load : function() {
				var rec = new this.recordType({iddokter:null, nmdoktergelar:'-'});
				rec.commit();
				this.add(rec);
				}
			}
		});
		return master;
	}
		
//=====================================================================
function dm_cekdktr_lapkeurj(){
		var master = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'vlaprj_controller/cekdktr_lapkeurj',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			}]
		});
		return master;
	}
/* ===========================End Akuntansi ========================== */




/* ======================================================= */
//=======================================================
	var data, x;
	var p;
	var tpl;
 
    function user_foto_thumbs(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/thumbs/t_user/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
	
	function user_foto_ori(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/ori/o_user/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }
		 
	function isi_foto_icons(x){
           //    alert(x);
              tpl = new Ext.Template(
                                            "<img src='"+ BASE_PATH +"/resources/img/icons/"+ x + "' width='110' height='125'/>"
                                        );
                                        tpl.overwrite(p.body, data);
                                        p.body.highlight('#c3daf9', {block:true});
           
         //   return tpl; 
         }