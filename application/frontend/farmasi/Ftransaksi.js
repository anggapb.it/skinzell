function Ftransaksi(){
	var myVar= setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jam"))
				RH.setCompValue("tf.jam",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("tf.shift").setValue(obj.nmshift);
		}
	});
	
	var ds_nota2 = dm_notatransri_with_bhp();
	ds_nota2.setBaseParam('start',0);
	ds_nota2.setBaseParam('limit',500);
	ds_nota2.setBaseParam('idregdet',null);
	ds_nota2.setBaseParam('idbagian',999);
	ds_nota2.setBaseParam('okoder',1);
		
	var ds_vregistrasi = dm_vregistrasi();
	ds_vregistrasi.setBaseParam('start',0);
	ds_vregistrasi.setBaseParam('limit',17);
	ds_vregistrasi.setBaseParam('groupby','noreg');
	ds_vregistrasi.setBaseParam('otanggal',1);
	ds_vregistrasi.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_vregistrasi.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_vregistrasi.setBaseParam('ctransfar',1);
	
	var ds_stpelayanan = dm_stpelayanan();
	var arr = [];
	var zkditem = '';
	var rownota = '';
	var koder = 0;
	var cekkstok = 0;
	
	var cbGrid = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			rowselect : function( selectionModel, rowIndex, record){
				zkditem = record.get("kditem");
				zidjnstarif = record.get("idjnstarif");
				arr[rowIndex] = zkditem + '-' + zidjnstarif;
			},
			rowdeselect : function( selectionModel, rowIndex, record){
				arr.splice(rowIndex, 1);
			},
			beforerowselect : function (sm, rowIndex, keep, rec) {
				if (this.deselectingFlag && this.grid.enableDragDrop){
					this.deselectingFlag = false;
					this.deselectRow(rowIndex);
					return this.deselectingFlag;
				}
				return keep;
			}
		}
	});
	
	var grid_notafarmasi = new Ext.grid.EditorGridPanel({
		store: ds_nota2,
		frame: true,
		height: 315,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota2',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota2 = rowIndex;
            }
        },
		columns: [{
			header: 'R/',
			hidden:true,
			dataIndex: 'koder',
			width: 30,editor: {
				xtype: 'numberfield',
				id: 'tf.koder', width: 30
			},
			sortable: true
		},{
			header: '<div style="text-align:center;">Item Obat/Alkes</div>',
			dataIndex: 'nmitem',
			width: 250,
			sortable: true
		},{
			header: '<div style="text-align:center;">Tarif</div>',
			hidden:true,
			dataIndex: 'tarif',
			align:'right',
			width: 90,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: 'Qty',
			dataIndex: 'qty',
			align:'right',
			width: 45,
			sortable: true
		},{
			header: '<div style="text-align:center;">Qty<br>Retur</div>',
			hidden:true,
			dataIndex: 'qtyretur',
			align:'right',
			width: 55,
			sortable: true
		},{
			header: '<div style="text-align:center;">Satuan</div>',
			dataIndex: 'nmsatuan',
			width: 80,
			sortable: true
		},{
			header: '<div style="text-align:center;">Subtotal</div>',
			dataIndex: 'tarif2',
			hidden: true,
			align:'right',
			width: 90,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			dataIndex: 'kditem',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idjnstarif',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idtarifpaketdet',
			hidden: true,
			hideable: false
		}],
		// bbar: [
		// 	{ xtype:'tbfill' },
		// 	{
		// 		xtype: 'fieldset',
		// 		border: false,
		// 		style: 'padding:0px; margin: 0px',
		// 		width: 400,
		// 		items: [{
		// 			xtype: 'compositefield',
		// 			hidden: true,
		// 			items: [{
		// 				xtype: 'label', id: 'lb.totalrf', text: 'Total :', margins: '0 10 0 138',
		// 			},{
		// 				xtype: 'numericfield',
		// 				id: 'tf.totalrf',
		// 				value: 0,
		// 				width: 100,
		// 				readOnly:true,
		// 				style : 'opacity:0.6',
		// 				thousandSeparator:',',
		// 			}]
		// 		},{
		// 			xtype: 'compositefield',
		// 			hidden: true,
		// 			items: [{
		// 				xtype: 'label', id: 'lb.uangr', text: 'Racik :', margins: '0 10 0 134',
		// 			},{
		// 				xtype: 'numericfield',
		// 				id: 'tf.uangr',
		// 				value: 0,
		// 				width: 100,
		// 				thousandSeparator:',',
		// 				enableKeyEvents: true,
		// 				listeners:{
		// 					keyup:function(){
		// 						totalnota();
		// 					}
		// 				}
		// 			}]
		// 		},{
		// 			xtype: 'compositefield',
		// 			items: [{
		// 				xtype: 'label', id: 'lb.diskon2', text: 'Diskon Rp :', margins: '0 9 0 0',
		// 				hidden: true
		// 			},{
		// 				xtype: 'numericfield',
		// 				id: 'tf.diskonf',
		// 				value: 0,
		// 				width: 50,
		// 				thousandSeparator:',',
		// 				enableKeyEvents: true,
		// 				listeners:{
		// 					keyup:function(){
		// 						totalnota();
		// 					}
		// 				},
		// 				hidden: true
		// 			},{
		// 				xtype: 'label', id: 'lb.total2', text: 'Jumlah :', margins: '0 10 0 127',
		// 			},{
		// 				xtype: 'numericfield',
		// 				id: 'tf.total2',
		// 				value: 0,
		// 				readOnly:true,
		// 				style : 'opacity:0.6',
		// 				width: 100,
		// 				thousandSeparator:','
		// 			}]
		// 		}]
		// 	}
		// ]
	});
	
	var paging_reg = new Ext.PagingToolbar({
		pageSize: 17,
		store: ds_vregistrasi,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 130
	})];
	
	var grid_reg = new Ext.grid.GridPanel({
		title: 'Daftar Registrasi',
		store: ds_vregistrasi,
		frame: true,
		height: 510,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_reg',
		forceFit: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[{
			xtype: 'compositefield',
			width: 150,
			defaults: { labelWidth: 50, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				border: false,
				width: 150,
				style: 'padding:0px; margin: 0px;',
				defaults: { labelWidth: 50, labelAlign: 'right'},
				items: [{
					xtype: 'datefield', id: 'df.tglawal', fieldLabel: 'Tgl Awal',
					width: 80, value: new Date(),
					labelStyle: 'float:left;',
					format: 'd-m-Y',
					listeners:{
						select: function(field, newValue){
							cAdvance();
						},
						/* change : function(field, newValue){
							cAdvance();
						} */
					}
				},{
					xtype: 'datefield', id: 'df.tglakhir', fieldLabel: 'Tgl Akhir',
					width: 80, value: new Date(),
					labelStyle: 'float:left;',
					format: 'd-m-Y',
					listeners:{
						select: function(field, newValue){
							cAdvance();
						},
						/* change : function(field, newValue){
							cAdvance();
						} */
					}
				}]
			}]
		}],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_freg = ds_vregistrasi.getAt(rowIndex);
				var freg_noreg = rec_freg.data["noreg"];
				var freg_norm = rec_freg.data["norm"];
				var freg_nmpasien = rec_freg.data["nmpasien"];
				var freg_atasnama = rec_freg.data["atasnama"];
				var freg_idbagian = rec_freg.data["idbagian"];
				var freg_nmbagian = rec_freg.data["nmbagian"];
				var freg_nmdokter = rec_freg.data["nmdoktergelar"];
				var freg_nonota = rec_freg.data["nonota"];
				var freg_nona = rec_freg.data["nona"];
				var freg_total = rec_freg.data["total"];
				var freg_tglnota = rec_freg.data["tglnota"];
				var freg_jamnota = rec_freg.data["jamnota"];
				var freg_catatannota = rec_freg.data["catatannota"];
				var freg_tgllahirp = rec_freg.data["tgllahirp"];
				var freg_idjnskelamin = rec_freg.data["idjnskelamin"];
				var freg_idregdet = rec_freg.data["idregdet"];
				var freg_nmkamar = rec_freg.data["nmkamar"];
				var freg_nmbed = rec_freg.data["nmbed"];
				var freg_nmklstarif = rec_freg.data["nmklstarif"];
				var freg_idstpelayanan = rec_freg.data["idstpelayanan"];
				var freg_nokui = rec_freg.data["nokuitansi"];
				
				Ext.getCmp("tf.noreg").setValue(freg_noreg);
				Ext.getCmp("tf.norm").setValue(freg_norm);
				Ext.getCmp("tf.nmpasien").setValue(freg_nmpasien);
				Ext.getCmp("tf.upel").setValue(freg_nmbagian);
				Ext.getCmp("tf.nmkamar").setValue(freg_nmkamar);
				Ext.getCmp("tf.nmbed").setValue(freg_nmbed);
				Ext.getCmp("tf.dokter").setValue(freg_nmdokter);
				Ext.getCmp("tf.nmklstarif").setValue(freg_nmklstarif);
				//Ext.getCmp("tf.nonota").setValue(freg_nonota);
				Ext.getCmp("tf.nona").setValue(freg_nona);
				Ext.getCmp("idregdet").setValue(freg_idregdet);
				Ext.getCmp("tf.catatan").setValue(freg_catatannota);
				Ext.getCmp("jkel").setValue(freg_idjnskelamin);
				if(freg_nokui){
					Ext.getCmp("btn.cetak").enable();
					Ext.getCmp("btn.cetakexc").enable();
				} else {
					Ext.getCmp("btn.cetak").disable();
					Ext.getCmp("btn.cetakexc").disable();
				}
				
				if(freg_atasnama == null) Ext.getCmp("tf.an").setValue(freg_nmpasien);
				else Ext.getCmp("tf.an").setValue(freg_atasnama);
				// Ext.getCmp('tf.totalrf').setValue(0);
				// Ext.getCmp('tf.diskonf').setValue(0);
				// Ext.getCmp('tf.uangr').setValue(0);
				// Ext.getCmp('tf.total2').setValue(0);
				ds_nota2.setBaseParam('noreg',freg_noreg);
				ds_nota2.setBaseParam('koder',1);
				ds_nota2.setBaseParam('idbagian',null);
				//ds_nota2.reload();
				if(freg_idstpelayanan == null) Ext.getCmp('cb.stpelayanan').setValue(1);
				else Ext.getCmp('cb.stpelayanan').setValue(freg_idstpelayanan);
				if(freg_tglnota == null) Ext.getCmp('df.tgl').setValue(new Date());
				else Ext.getCmp('df.tgl').setValue(freg_tglnota);
				if(freg_jamnota == null){
					myStopFunction();
					myVar = setInterval(function(){myTimer()},1000);
				} else {
					Ext.getCmp('tf.jam').setValue(freg_jamnota);
					myStopFunction();
				}
				fTotal();
            }
        },
		columns: [{
			header: 'No Reg',
			dataIndex: 'noreg',
			width: 75,
			sortable: true
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 75,
			sortable: true
		},{
			header: 'Pasien',
			dataIndex: 'nmpasien',
			width: 110,
			sortable: true
		},{
			header: 'L/P',
			dataIndex: 'kdjnskelamin',
			width: 30,
			sortable: true
		},{
			header: 'Ruangan',
			dataIndex: 'nmbagian',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Lahir',
			dataIndex: 'tgllahirp',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Posisi Pasien',
			dataIndex: 'nmstposisipasien',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Registrasi',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Dokter',
			dataIndex: 'nmdoktergelar',
			width: 100,
			sortable: true
		},{
			header: 'Penjamin',
			dataIndex: 'nmpenjamin',
			width: 100,
			sortable: true
		}],
		bbar: paging_reg,
		plugins: cari_reg
	});
	
	var rifarmasi_form = new Ext.form.FormPanel({
		id: 'fp.rifarmasi',
		title: 'Nota Transaksi',
		width: 900,
		Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			{ text: 'Cetak Nota', id:'btn.cetak', iconCls: 'silk-printer', hidden:true, handler: function(){cetakRIF();} },'-',
			{ text: 'Cetak Excel', id:'btn.cetakexc', iconCls: 'silk-printer', hidden:true, handler: function(){cetakTFE();} },'-',
			{ xtype: 'tbfill' },'->'
		],
        items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:160,
				boxMaxHeight:160,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. RM',
						id: 'tf.norm',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					},{
						xtype: 'label', id: 'lb.noreg', text: 'No. Registrasi', margins: '0 10 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.noreg',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					}]
				},{
					xtype: 'textfield', fieldLabel:'Nama Pasien',
					id: 'tf.nmpasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield', fieldLabel:'Ruangan',
						id: 'tf.upel',
						readOnly: true, style : 'opacity:0.6',
						width: 80,
					},{
						xtype: 'label', id: 'lb.kamarbed', text: 'Kamar / Bed', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmkamar', 
						readOnly: true, style : 'opacity:0.6',
						width: 35,
					},{
						xtype: 'label', id: 'lb.garing', text: '/', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmbed', 
						readOnly: true, style : 'opacity:0.6',
						width: 38,
					}]
				},{
					xtype: 'textfield', fieldLabel:'Kelas Tarif', hidden:true,
					id: 'tf.nmklstarif',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Dokter',
					id: 'tf.dokter',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:160,
				boxMaxHeight:160,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'textfield', fieldLabel:'No. Nota',
					id: 'tf.nonota',
					readOnly: true, style : 'opacity:0.6',
					width: 100, hidden: true
				},{
					xtype: 'textfield',
					id: 'tf.nona', hidden: true
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl./Jam/Shift', id: 'df.tgl',
						width: 100, value: new Date(),
						format: 'd-m-Y', disabled: true
					},{
						xtype: 'label', id: 'lb.garing3', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jam', readOnly:true,
						width: 65, style : 'opacity:0.6'
					},{
						xtype: 'label', id: 'lb.garing4', text: '/'
					},{
						xtype: 'textfield', id: 'tf.shift', 
						width: 60, disabled: true
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Atas Nama',
					id: 'tf.an', anchor: "100%",
					disabled: true
				},{
					xtype: 'textarea', fieldLabel: 'Catatan',
					id: 'tf.catatan', anchor: "100%", height: 50,
					disabled: true
				},{
					xtype: 'combo', fieldLabel : 'Status Pelayanan',
					id: 'cb.stpelayanan', anchor: "100%",
					store: ds_stpelayanan, valueField: 'idstpelayanan', displayField: 'nmstpelayanan',
					editable: false, triggerAction: 'all', disabled: true,
					forceSelection: true, submitValue: true, mode: 'local',
					emptyText:'Pilih...'
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_notafarmasi]
				}]
		},{
			xtype: 'textfield',
			id: 'idregdet', hidden: true
		},{
			xtype: 'textfield',
			id: 'tahun', hidden: true
		},{
			xtype: 'textfield',
			id: 'jkel', hidden: true
		}]
	});
	
	var transFRIDispPanel = new Ext.form.FormPanel({
		id: 'fp.transFRIDispPanel',
		name: 'fp.transFRIDispPanel',
		border: false, 
		forceFit:true,
		frame: true,
		autoScroll:true,
		labelAlign: 'top',
		layout: 'anchor',
		items: [
		{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_reg]
		}]
	});
	
	var transFRIPanel = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Transaksi Farmasi',
		frame: false,
		border: true,
		margins: '0 0 5 0',
		plain: true,		
		forceFit: true,
		layout: 'border',
		items: [{
			region: 'center',
			xtype: 'panel',
			border: true,
			layout: 'fit',
			items: [rifarmasi_form],
		},
		{
			region: 'east',
			xtype: 'panel',
			title: '', border: true,
			layout:'fit', width:400,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [transFRIDispPanel],
		}]
	});
	
	SET_PAGE_CONTENT(transFRIPanel);
	
	function fTotal(){
		ds_nota2.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				ds_nota2.each(function (rec) { sum += parseFloat(rec.get('tarif2')); });
				var jmluangr = Ext.getCmp("tf.uangr").getValue();
				var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
				var sumtotal = sum + jmluangr;
				var sumtotaldisk = sumtotal - jmldiskon;
				Ext.getCmp("tf.totalrf").setValue(sum);
				Ext.getCmp("tf.total2").setValue(sumtotaldisk);
			}
		});
	}
	
	function cetakRIF(){
		var nona		= Ext.getCmp('tf.nona').getValue();
		var noreg		= Ext.getCmp('tf.noreg').getValue();
		//RH.ShowReport(BASE_URL + 'print/printnota/notafarmasitrans/'
               // +nona+'/'+noreg);	
		RH.ShowReport(BASE_URL + 'print/printnota/notafarmasitrans/'
                +noreg);
	}
	
	function cetakTFE(){
		var nona		= Ext.getCmp('tf.nona').getValue();
		var noreg		= Ext.getCmp('tf.noreg').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/excellappelayanan/'
                +nona+'/'+noreg);
	}
	
	function cAdvance(){
		ds_vregistrasi.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vregistrasi.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vregistrasi.setBaseParam('ctransfar',1);
		ds_vregistrasi.load();
	}
	
}