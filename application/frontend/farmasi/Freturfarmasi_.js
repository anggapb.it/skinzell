function Freturfarmasi(){
	var myVar= setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jam"))
				RH.setCompValue("tf.jam",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}
	
	var ds_nota2 = dm_nota();
	ds_nota2.setBaseParam('start',0);
	ds_nota2.setBaseParam('limit',50);
	ds_nota2.setBaseParam('idregdet',null);
	
	var ds_notax = dm_notax();
	ds_notax.setBaseParam('idregdet', 1);
		
	var ds_vregistrasi = dm_vregistrasi();
	ds_vregistrasi.setBaseParam('start',0);
	ds_vregistrasi.setBaseParam('limit',7);
	ds_vregistrasi.setBaseParam('cek','RI');
	ds_vregistrasi.setBaseParam('groupby','noreg');
	ds_vregistrasi.setBaseParam('oposisipasien',1);
	ds_vregistrasi.setBaseParam('onmpasien',1);
	
	var ds_brgbagian = dm_brgbagian();
	ds_brgbagian.setBaseParam('start',0);
	ds_brgbagian.setBaseParam('limit',7);
	ds_brgbagian.setBaseParam('cidbagian',0);
	
	var ds_dokter = dm_dokter();
	var arr = [];
	var zkditem = '';
	var rownota = '';
	var koder = 0;
	var cekkstok = 0;
	
	var cbGrid = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			rowselect : function( selectionModel, rowIndex, record){
				zkditem = record.get("kditem");
				zidjnstarif = record.get("idjnstarif");
				arr[rowIndex] = zkditem + '-' + zidjnstarif;
			},
			rowdeselect : function( selectionModel, rowIndex, record){
				arr.splice(rowIndex, 1);
			},
			beforerowselect : function (sm, rowIndex, keep, rec) {
				if (this.deselectingFlag && this.grid.enableDragDrop){
					this.deselectingFlag = false;
					this.deselectRow(rowIndex);
					return this.deselectingFlag;
				}
				return keep;
			}
		}
	});
	
	var grid_notafarmasi = new Ext.grid.EditorGridPanel({
		store: ds_nota2,
		tbar: [{
			text: 'Tambah',
			id: 'btn_tmbh',
			iconCls: 'silk-add',
			handler: function() {
				var noreg = Ext.getCmp('tf.noreg').getValue();
				if (noreg !=''){
					Ext.MessageBox.alert('Informasi', 'Pilih No. Nota');
				}else if (noreg ==''){
					Ext.MessageBox.alert('Informasi', 'Pilih No. Reg');
				}
			}
		}],
		frame: true,
		height: 310,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota2',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota2 = rowIndex;
            }
        },
		columns: [new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">No. Nota</div>',
			dataIndex: 'nmitem',
			width: 110
		},{
			header: '<div style="text-align:center;">Tgl. Nota</div>',
			dataIndex: 'nmitem',
			width: 100
		},{
			header: '<div style="text-align:center;">Kode Barang</div>',
			dataIndex: 'tarif',
			align:'right',
			width: 100,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Nama Barang</div>',
			dataIndex: 'tarif',
			align:'right',
			width: 200,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Qty <br> Terima</div>',
			dataIndex: 'qty',
			align:'right',
			width: 50,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqtyf', width: 150, 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota2.getAt(rownota2);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqtyf').getValue();
						record.set('tarif2',subtotal);
						totalnota();
					}
				}
			}
		},{
			header: '<div style="text-align:center;">Jumlah <br> Retur</div>',
			dataIndex: 'qty',
			align:'right',
			width: 50,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqtyf', width: 150, 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota2.getAt(rownota2);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqtyf').getValue();
						record.set('tarif2',subtotal);
						totalnota();
					}
				}
			}
		},{
			header: '<div style="text-align:center;">Qty <br> Retur</div>',
			dataIndex: 'qty',
			align:'right',
			width: 45,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqtyf', width: 150, 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota2.getAt(rownota2);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqtyf').getValue();
						record.set('tarif2',subtotal);
						totalnota();
					}
				}
			}
		},{
			header: '<div style="text-align:center;">Tarif</div>',
			dataIndex: 'qty',
			align:'right',
			width: 90,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqtyf', width: 150, 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota2.getAt(rownota2);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqtyf').getValue();
						record.set('tarif2',subtotal);
						totalnota();
					}
				}
			}
		},{
			header: '<div style="text-align:center;">Subtotal</div>',
			dataIndex: 'tarif2',
			align:'right',
			width: 100,
			xtype: 'numbercolumn', format:'0,000'
			/* renderer: function(value, p, r){
				var subtotal = parseFloat(r.data['qty']) * parseFloat(r.data['tarif']);
				var subtotal2 = subtotal * (Ext.getCmp('tf.notadprsn').getValue() / 100);
				var subtotal3 = Ext.getCmp('tf.notadrp').getValue();
				var jsubtotal = subtotal - subtotal2 - subtotal3;
				return Ext.util.Format.number(jsubtotal, '0,000.00');
			} */
		},{
			header: '<div style="text-align:center;">Catatan</div>',
			dataIndex: 'nmsatuan',
			width: 100
		},{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					ds_nota2.removeAt(rowIndex);
					totalnota();
				}
			}]
		},{
			dataIndex: 'kditem',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idjnstarif',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idtarifpaketdet',
			hidden: true,
			hideable: false
		}],
		bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 435,
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.totalrf', text: 'Jumlah :', margins: '3 10 0 130',
					},{
						xtype: 'numericfield',
						id: 'tf.totalrf',
						value: 0,
						width: 100,
						readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.diskon2', text: 'Biaya Administrasi :', margins: '3 10 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.diskonf',
						value: 0,
						width: 50,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								totalnota();
							}
						}
					},{
						xtype: 'label', id: 'lb.uangr', text: '% ', margins: '3 10 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.uangr',
						value: 0,
						width: 100,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								totalnota();
							}
						}
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.total2', text: 'Total :', margins: '3 9 0 143',
					},{
						xtype: 'numericfield',
						id: 'tf.total2',
						value: 0,
						readOnly:true,
						style : 'opacity:0.6',
						width: 100,
						thousandSeparator:','
					}]
				}]
			}
		]
	});
		
	var transFRIPanel = new Ext.form.FormPanel({
		id: 'fp.rifarmasi',
		title: 'Retur Farmasi',
		width: 900,
		Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			//{ text: 'Baru', iconCls: 'silk-add', handler: function(){bersihFRinota();} },'-',
			{ text: 'Simpan', id:'btn.simpan', iconCls: 'silk-save', handler: function(){simpanRIF();} },'-',
			{ text: 'Cetak', id:'btn.cetak', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRIF();} },'-',
			{
				text: 'Batal', id: 'bt.batal',  iconCls: 'silk-cancel',
				handler: function() {
					
				}
			}
		],
        items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 150, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:105,
				boxMaxHeight:105,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'textfield', fieldLabel:'No. Retur Farmasi',
					id: 'tf.nonota',
					readOnly: true, style : 'opacity:0.6',
					width: 180,
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl./Jam Retur Farmasi', id: 'df.tgl',
						width: 100, value: new Date(),
						format: 'd-m-Y',
					},{
						xtype: 'label', id: 'lb.garing3', text: '/', margins: '3 5 0 0',
					},{ 	
						xtype: 'textfield', id: 'tf.jam', readOnly:true,
						width: 65
					}]
				},{
					xtype: 'textfield', fieldLabel:'Penerima',
					id: 'tf.dokter',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:105,
				boxMaxHeight:105,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'textfield',
					fieldLabel: 'No. Registrasi',
					id: 'tf.noreg',
					width: 180,
					//readOnly: true, 
					//style : 'opacity:0.6',
					autoCreate :  {
						tag: "input", 
						maxlength : 10, 
						type: "text", 
						size: "20", 
						autocomplete: "off"
					}
				},{
					xtype: 'textfield',
					fieldLabel: 'No. RM',
					id: 'tf.norm',
					width: 100,
					readOnly: true, style : 'opacity:0.6',
					maskRe: /[0-9.]/,
					autoCreate :  {
						tag: "input", 
						maxlength : 10, 
						type: "text", 
						size: "20", 
						autocomplete: "off"
					}
				},{
					xtype: 'textfield', fieldLabel:'Nama Pasien',
					id: 'tf.nmpasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					xtype: 'fieldset',
					title: 'Daftar Barang Yang Diretur',
					layout: 'form',
					height: 340,
					boxMaxHeight: 350,
					items: [grid_notafarmasi]
				}]
		}]
	});
	
	SET_PAGE_CONTENT(transFRIPanel);
	
	function bersihFRinota() {
		Ext.getCmp('cb.nota').setValue('');
		Ext.getCmp('tf.nonota').setValue('');
		ds_nota2.setBaseParam('idregdet', null);
		ds_nota2.reload();
		Ext.getCmp("btn.cetak").disable();
	}
	
	function fTotal(){
		ds_nota2.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				ds_nota2.each(function (rec) { sum += parseFloat(rec.get('tarif2')); });
				var jmluangr = Ext.getCmp("tf.uangr").getValue();
				var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
				var sumtotal = sum + jmluangr;
				var sumtotaldisk = sumtotal - jmldiskon;
				Ext.getCmp("tf.totalrf").setValue(sum);
				Ext.getCmp("tf.total2").setValue(sumtotaldisk);
			}
		});
	}
	
	function simpanRIF(){
		var arrnota = [];
		var zx = 0;
		
		ds_nota2.each(function (rec) {
			zkditem = rec.get('kditem');
			zqty = rec.get('qty');
			zkoder = rec.get('koder');
			ztarif2 = rec.get('tarif2');
			zdiskonjs = 0;
			zdiskonjm = 0;
			zdiskonjp = 0;
			zdiskonbhp = 0;
			arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp;
			zx++;
		});
		
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/insorupd_nota',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue(),
				nonota		: Ext.getCmp('tf.nonota').getValue(),
				nmshift 	: Ext.getCmp('tf.shift').getValue(),
				catatan 	: Ext.getCmp('tf.catatan').getValue(),
				nmpasien 	: Ext.getCmp('tf.nmpasien').getValue(),
				total	 	: Ext.getCmp('tf.total2').getValue(),
				uangr	 	: Ext.getCmp('tf.uangr').getValue(),
				diskon	 	: Ext.getCmp('tf.diskonf').getValue(),
				atasnama 	: Ext.getCmp('tf.an').getValue(),
				nota		: Ext.getCmp('cb.nota').lastSelectionText,
				tglnota		: Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'),
				jamnota		: Ext.getCmp('tf.jam').getValue(),
				tahun		: Ext.getCmp('tahun').getValue(),
				jkel		: Ext.getCmp('jkel').getValue(),
				jpel 		: 'Farmasi',
				ureg 		: 'FA',
				fri 		: 11,
				jtransaksi	: 7,
				kui			: 1,
				arrnota		: Ext.encode(arrnota)
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.nonota").setValue(obj.nonota);
				Ext.getCmp("cb.nota").setValue(obj.nonota);
				Ext.getCmp("btn.cetak").enable();
				ds_vregistrasi.reload();
				myStopFunction();
			},
			failure: function() {}
		});
	}
	
	function hapusItem(){
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/delete_bnotadet',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue(),
				nonota		: Ext.getCmp('tf.nonota').getValue(),
				arr			:  Ext.encode(arr)
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
				bersihrifarmasi();
			},
			failure: function() {
			}
		});
	}
	
	function cetakRIF(){
		var nonota		= Ext.getCmp('tf.nonota').getValue();
		var noreg		= Ext.getCmp('tf.noreg').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/nota_ri/'
                +nonota+'/'+noreg);
	}
	
	function totalnota(){
		var zzz = 0;
		for (var zxc = 0; zxc <ds_nota2.data.items.length; zxc++) {
			var record = ds_nota2.data.items[zxc].data;
			zzz += parseFloat(record.tarif2);
		}
		var jmluangr = Ext.getCmp("tf.uangr").getValue();
		var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
		var sumtotal = zzz + jmluangr;
		var sumtotaldisk = sumtotal - jmldiskon;
		Ext.getCmp('tf.totalrf').setValue(zzz);
		Ext.getCmp('tf.total2').setValue(sumtotaldisk);
	}

	function hitungKembalian() {
		var jml = Ext.getCmp('tf.jumlah').getValue();
		if(jml != ''){
			var utyd = Ext.getCmp('tf.utyd').getValue();
			var kmblian = utyd - jml;
			Ext.getCmp('tf.kembalian').setValue(kmblian);
		}
	}	
}