function Fpasienluar(){
	var myVar=setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jamshift"))
				RH.setCompValue("tf.jamshift",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}
	
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("idshift").setValue(obj.idshift);
			Ext.getCmp("tf.waktushift").setValue(obj.nmshift);
		}
	});
	
	var ds_dokter = dm_dokter();
		
	var ds_notanf = dm_nota();
	ds_notanf.setBaseParam('start',0);
	ds_notanf.setBaseParam('limit',6);
	ds_notanf.setBaseParam('idbagian',11);
	ds_notanf.setBaseParam('koder',2);
	ds_notanf.setBaseParam('grupby',true);
	ds_notanf.setBaseParam('ctglnota',Ext.util.Format.date(new Date(), 'Y-m-d'));
	
	var ds_nota = dm_nota();
	ds_nota.setBaseParam('start',0);
	ds_nota.setBaseParam('limit',500);
	ds_nota.setBaseParam('nonota',null);
	ds_nota.setBaseParam('idbagian',999);
	ds_nota.setBaseParam('okoder',1);
	
	var ds_brgbagian = dm_brgbagian();
	ds_brgbagian.setBaseParam('start',0);
	ds_brgbagian.setBaseParam('limit',6);
	ds_brgbagian.setBaseParam('cidbagian',11);
	
	var ds_kuitansidet = dm_kuitansidet();
	ds_kuitansidet.setBaseParam('blank', 1);
	
	var rownota = '';
	var koder = 0;
	var pmbyrn = 0;
    
	var grid_nota = new Ext.grid.EditorGridPanel({
		store: ds_nota,
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota = rowIndex;
            }
        },
		columns: [{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					ds_nota.removeAt(rowIndex);
					hitung();
				}
			}]
		},{
			header: 'R/',
			dataIndex: 'koder',
			width: 40,editor: {
				xtype: 'numberfield',
				id: 'tf.koder', width: 20
			}
		},{
			header: 'Item Obat/Alkes',
			dataIndex: 'nmitem',
			width: 280
		},{
			header: 'Tarif',
			dataIndex: 'tarif',
			align:'right',
			width: 100,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: 'Qty',
			dataIndex: 'qty',
			align:'right',
			width: 50,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqtyf', width: 150, 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota.getAt(rownota);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqtyf').getValue();
						record.set('tarif2',subtotal);
						hitung();
					}
				}
			}
		},{
			header: 'Satuan',
			dataIndex: 'nmsatuan',
			width: 120
		},{
			header: 'Subtotal',
			dataIndex: 'tarif2',
			align:'right',
			width: 100,
			xtype: 'numbercolumn', format:'0,000'
		},{
			dataIndex: 'kditem',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idjnstarif',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idtarifpaketdet',
			hidden: true,
			hideable: false
		}],
		bbar: [
			{xtype: 'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 400,
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.total', text: 'Total :', margins: '0 10 0 138',
					},{
						xtype: 'numericfield',
						id: 'tf.total',
						value: 0,
						width: 100,
						readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.diskonprsn2', text: 'Diskon % :', margins: '0 10 0 2',
					},{
						xtype: 'numericfield',
						id: 'tf.diskonprsn',
						value: 0,
						width: 50,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								var disc = (Ext.getCmp('tf.total').getValue() * (Ext.getCmp('tf.diskonprsn').getValue() / 100));
								Ext.getCmp('tf.diskonf').setValue(disc);
								hitung();
							}
						}
					},{
						xtype: 'label', id: 'lb.uangr', text: 'Racik :', margins: '0 10 0 15',
					},{
						xtype: 'numericfield',
						id: 'tf.uangr',
						value: 0,
						width: 100,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								hitung();
							}
						}
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.diskon2', text: 'Diskon Rp :', margins: '0 9 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.diskonf',
						value: 0,
						width: 50,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								var disc = Ext.getCmp('tf.diskonf').getValue() / Ext.getCmp('tf.total').getValue() * 100;
								Ext.getCmp('tf.diskonprsn').setValue(disc);
								hitung();
							}
						}
					},{
						xtype: 'label', id: 'lb.total2', text: 'Jumlah :', margins: '0 9 0 10',
					},{
						xtype: 'numericfield',
						id: 'tf.total2',
						value: 0,
						readOnly:true,
						style : 'opacity:0.6',
						width: 100,
						thousandSeparator:','
					}]
				}]
			}
		]
	});
	
	var grid_pembayaran = new Ext.grid.GridPanel({
		store: ds_kuitansidet,
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_pembayaran',
		forceFit: true,
		autoScroll: true,
		columnLines: true,
		loadMask: true,
		tbar: [
			{
				text: 'Tambah',
				id: 'btn_add',
				iconCls: 'silk-add',
				handler: function() {
					fPemb();
				}
			},
			{ xtype:'tbfill' }
		],
		columns: [{
                xtype: 'actioncolumn',
                width: 42,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex, colIndex) {
						var rec = ds_kuitansidet.getAt(rowIndex);
						pmbyrn -= rec.data.jumlah;
						ds_kuitansidet.removeAt(rowIndex);
						Ext.getCmp('tf.jumlah').setValue(pmbyrn);
						cektombol();
                    }
                }]
        },{
			header: 'Bayar',
			width: 100,
			dataIndex: 'nmcarabayar',
			sortable: true
		},{
			header: 'Bank',
			width: 70,
			dataIndex: 'nmbank',
			sortable: true
		},{
			header: 'No. Kartu',
			width: 100,
			dataIndex: 'nokartu',
			sortable: true
		},{
			header: 'Total',
			width: 100,
			dataIndex: 'jumlah',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		}],
		bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				width: 300,
				style: 'padding:0px; margin: 0px;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.jml', text: 'Jumlah Total :', margins: '0 5 0 7',
					},{
						xtype: 'numericfield',
						id: 'tf.jumlah',
						width: 90,
						readOnly:true, style: 'opacity:0.6'
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.ua', text: 'Uang Diterima :', margins: '0 4 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.utyd',
						width: 90,
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								hitungKembalian();
							}
						}
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.km', text: 'Kembalian :', margins: '0 5 0 16',
					},{
						xtype: 'numericfield',
						id: 'tf.kembalian',
						width: 90,
						readOnly:true, style: 'opacity=0.6'
					}]
				}]
			}
		]
	});
	
	var paging_reg = new Ext.PagingToolbar({
		pageSize: 6,
		store: ds_notanf,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 118
	})];
	
	var grid_reg = new Ext.grid.GridPanel({
		title: 'Daftar Nota Pasien Luar',
		store: ds_notanf,
		frame: true,
		height: 260,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_reg',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[{
			xtype: 'label', id: 'lb.tgl', text: 'Tgl :'
		},{
			xtype: 'datefield', id: 'df.tglnota',
			width: 100, value: new Date(),
			format: 'd-m-Y',
			listeners:{
				select: function(field, newValue){
					ds_notanf.setBaseParam('ctglnota',Ext.util.Format.date(newValue, 'Y-m-d'));
					Ext.getCmp('grid_reg').store.reload();
					//ds_notanf.reload();
				}
			}
		}],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_nota = ds_notanf.getAt(rowIndex);
				var rnonota = rec_nota.data["nonota"];
				var ratasnama = rec_nota.data["atasnama"];
				var rnmdokter = rec_nota.data["nmdoktergelar"];
				var rcatatann = rec_nota.data["catatann"];
				var rcatatand = rec_nota.data["catatand"];
				var rnoresep = rec_nota.data["noresep"];
				var ruangr = rec_nota.data["uangr"];
				var rdiskonr = rec_nota.data["diskonr"];
				var rnokuitansi = rec_nota.data["nokuitansi"];
				
				Ext.getCmp("tf.nonota").setValue(rnonota);
				Ext.getCmp("tf.nokuitansi").setValue(rnokuitansi);
				Ext.getCmp("tf.nmpasien").setValue(ratasnama);
				Ext.getCmp("tf.dokter").setValue(rnmdokter);
				Ext.getCmp("tf.catatannf").setValue(rcatatann);
				Ext.getCmp("tf.catatandsknnf").setValue(rcatatand);
				Ext.getCmp("tf.noresep").setValue(rnoresep);
				Ext.getCmp("tf.uangr").setValue(ruangr);
				Ext.getCmp("tf.diskonf").setValue(rdiskonr);
				
				pmbyrn = 0;
				ds_nota.setBaseParam('nonota',rnonota);
				ds_nota.setBaseParam('koder',1);
				ds_nota.setBaseParam('idbagian',null);
				ds_nota.reload({
					scope   : this,
					callback: function(records, operation, success) {
						sum = 0;
						ds_nota.each(function (rec) { sum += parseFloat(rec.get('tarif2')); });
						Ext.getCmp("tf.total").setValue(sum);
						var jmluangr = parseFloat(ruangr);
						var jmldiskon = parseFloat(rdiskonr);
						var sumtotal = sum + jmluangr;
						var sumtotaldisk = sumtotal - jmldiskon;
						Ext.getCmp('tf.total2').setValue(sumtotaldisk);
				
						var disc = rdiskonr / sum * 100;
						Ext.getCmp('tf.diskonprsn').setValue(disc);
					}
				});
				//ds_notanf.reload();
				ds_kuitansidet.setBaseParam('blank', 0);
				ds_kuitansidet.setBaseParam('nokuitansi',rnokuitansi);
				ds_kuitansidet.reload({
					scope   : this,
					callback: function(records, operation, success) {
						sum = 0;
						ds_kuitansidet.each(function (rec) {
							sum += (parseFloat(rec.get('jumlah')));
						});
						Ext.getCmp('tf.jumlah').setValue(sum);
						pmbyrn += sum;
						cektombol();
					}
				});
				Ext.getCmp("btn_add").disable();
				Ext.getCmp("bt.simpan").disable();
				Ext.getCmp("bt.batal").enable();
				Ext.getCmp("bt.cetak").enable();
            }
        },
		columns: [{
			header: 'No. Nota',
			dataIndex: 'nonota',
			width: 90
		},{
			header: 'Tgl. Nota',
			dataIndex: 'tglnota',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			/* renderer: function(value, p, r){
				var norm = '';
				Ext.Ajax.request({
					url:BASE_URL + 'pasien_controller/getNoRmPL',
					method:'POST',
					params:{
						nama : r.data['atasnama']
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						norm = obj.norm ;
						return 'zx';
					}
				});
			}, */
			width: 50, hidden: true
		},{
			header: 'Nama',
			dataIndex: 'atasnama',
			width: 150
		}],
		bbar: paging_reg,
		plugins: cari_reg
	});
	
	var paging_farmasi = new Ext.PagingToolbar({
		pageSize: 6,
		store: ds_brgbagian,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_farmasi = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230
	})];
	
	var grid_farmasi = new Ext.grid.GridPanel({
		title: 'Apotek',
		store: ds_brgbagian,
		frame: true,
		height: 250,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_farmasi',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:[],
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				koder = 0;
				ds_nota.each(function (rec) {
					if(koder < rec.get('koder')) koder = rec.get('koder');
				});
			
				var obj = ds_brgbagian.getAt(rowIndex);
				var skdbrg			= obj.data["kdbrg"];
				var snmbrg			= obj.data["nmbrg"];
				var starif			= obj.data["tarif"];
				var snmsatuan		= obj.data["nmsatuan"];
				koder++;
				
				var orgaListRecord = new Ext.data.Record.create([
					{
						name: 'kditem',
						name: 'koder',
						name: 'nmitem',
						name: 'qty',
						name: 'tarif',
						name: 'tarif2',
						name: 'nmsatuan'
					}
				]);
				
				ds_nota.add([
					new orgaListRecord({
						'kditem': skdbrg,
						'koder': koder,
						'nmitem': snmbrg,
						'qty': 1,
						'tarif': starif,
						'tarif2': starif,
						'nmsatuan': snmsatuan
					})
				]);
				hitung();
				grid_nota.getView().focusRow(ds_nota.getCount() - 1);
            }
        },
		columns: [{
			header: 'Nama Obat/Alkes',
			dataIndex: 'nmbrg',
			width: 160
		},{
			header: 'Harga',
			dataIndex: 'tarif',
			align:'right',
			width: 80,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: 'Stok',
			dataIndex: 'stoknowbagian',
			align:'right',
			width: 45,
		}],
		bbar: paging_farmasi,
		plugins: cari_farmasi
	});
	
	var transaksipl_form = new Ext.form.FormPanel({ 
		id: 'fp.transaksipl',
		title: 'Nota Farmasi Pasien Luar',
		width: 900, Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			{ text: 'Baru', iconCls: 'silk-add', handler: function(){bersihnf();} },'-',
			{ text: 'Simpan', id:'bt.simpan', disabled: true, iconCls: 'silk-save', handler: function(){simpan("fp.transaksipl");} },'-',
			{ text: 'Batal', id:'bt.batal', disabled: true, iconCls: 'silk-cancel', handler: function(){batal_new();} },'-',
			/*{ text: 'Cari Transaksi Farmasi', iconCls: 'silk-find', handler: function(){cariRegNF();} },'-',*/
			{ text: 'Cetak', id:'bt.cetak', iconCls: 'silk-printer', disabled: true, handler: function(){cetakNotaFPL();} },'-',
			{xtype: 'tbfill' }
		],
		defaults: { labelWidth: 150, labelAlign: 'right'},
        items: [{
			//COLUMN 1
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:150,
				boxMaxHeight:150,
				style : 'margin-bottom:0;', 
				items: [{
					xtype: 'compositefield', hidden: true,
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. RM',
						id: 'tf.norm',
						width: 80,
						maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
						enableKeyEvents: true,
						listeners:{
							specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									dataPasien();
								}
							}
						}
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Nama Pasien',
					id: 'tf.nmpasien', allowBlank: false,
					anchor: "100%",
					value: 'Pasien Luar'
				},{
					xtype: 'compositefield',
					fieldLabel: 'Dokter',
					items: [{
						xtype: 'textfield',
						id: 'tf.dokter', width: 210, readOnly: true
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.dokter',
						width: 40,
						handler: function() {
							dftDokter();
						}
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Unit Penunjang',
					id: 'tf.upenunjuang', allowBlank: false,
					value:'Farmasi RJ', readOnly: true,
					anchor: "100%", readOnly: true,
					style : 'opacity:0.6'
				}]
			}]
		},{
			//COLUMN 2
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:150,
				boxMaxHeight:150,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. Nota',
						id: 'tf.nonota',
						width: 100, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield',
						id: 'tf.nokuitansi',
						width: 100, readOnly: true,
						style : 'opacity:0.6', hidden: true
					}]
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel: 'Tgl./Jam/Shift',id: 'df.tglshift',
						width: 100, value: new Date(),
						format: 'd-m-Y'
					},{
						xtype: 'label', id: 'lb.garing1', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jamshift', readOnly:true,
						width: 65
					},{
						xtype: 'label', id: 'lb.garing2', text: '/'
					},{
						xtype: 'textfield', id: 'tf.waktushift', 
						width: 60, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield', id: 'idshift',
						hidden:true
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Catatan',
					id: 'tf.catatannf', anchor: "100%", value: 'Farmasi Pasien Luar'
				},{
					xtype: 'textfield', fieldLabel: 'Catatan Diskon',
					id: 'tf.catatandsknnf', anchor: "100%"
				},{
					xtype :'textfield', fieldLabel: 'No. Resep',
					id :'tf.noresep',width:100
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
				layout: 'fit',
				border: false,
				items: [grid_nota]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_pembayaran]
				}]
		}]
	});
	
	var FPLuarDispPanel = new Ext.form.FormPanel({
		id: 'fp.transRJDispPanel',
		name: 'fp.transRJDispPanel',
		border: false, 
		forceFit:true,
		frame: true,
		autoScroll:true,
		labelAlign: 'top',
		layout: 'anchor',
		items: [
		{
			layout: 'form',
			bodyStyle: 'padding:1px 5px 5px 5px',
			items: [grid_reg]
		},{
			layout: 'form',
			bodyStyle: 'padding:3px 5px 5px 5px',
			items: [grid_farmasi]
		}]
	});
	
	var FPLuarPanel = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Transaksi Farmasi Pasien Luar',
		frame: false,
		border: true,
		margins: '0 0 5 0',
		plain: true,		
		forceFit: true,
		layout: 'border',
		items: [{
			region: 'center',
			xtype: 'panel',
			border: true,
			layout: 'fit',
			items: [transaksipl_form],
		},
		{
			region: 'east',
			xtype: 'panel',
			title: '', border: true,
			layout:'fit', width:350,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [FPLuarDispPanel],
		}],		
		listeners: {
			afterrender: function () {
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/get_tgl_svr',
					method: 'POST',
					params: {
					
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp('df.tglshift').setValue(obj.date);
					},
					failure : function(){
						Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
					}
				});
			}
		}
	});
	SET_PAGE_CONTENT(FPLuarPanel);
	Ext.getCmp('tf.nmpasien').focus();
	
	function cetakNotaFPL(){
		var nonota		= Ext.getCmp('tf.nonota').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/nota_farmasipl/'
                +nonota);
	}
	
	function simpan(namaForm) {
		if(Ext.getCmp('tf.total2').getValue() != Ext.getCmp('tf.jumlah').getValue()){
			Ext.MessageBox.alert('Informasi', 'Jumlah tidak sama dengan Jumlah Total');
		} else {
			var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
			var arrnota = [];
			var arrcarabayar = [];
			var zx = 0;
			var zv = 0;
			ds_nota.each(function(rec){
				zkditem = rec.get('kditem');
				zqty = rec.get('qty');
				zkoder = rec.get('koder');
				ztarif2 = rec.get('tarif2');
				zdiskonjs = 0;
				zdiskonjm = 0;
				zdiskonjp = 0;
				zdiskonbhp = 0;
				arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp;
				zx++;
			});
			
			ds_kuitansidet.each(function (rec) {
				znmcarabayar = rec.get('nmcarabayar');
				znmbank = rec.get('nmbank');
				znokartu = rec.get('nokartu');
				zjumlah = rec.get('jumlah');
				arrcarabayar[zv] = znmcarabayar+'-'+znmbank+'-'+znokartu+'-'+zjumlah;
				zv++;
			});
			
			var form_nya = Ext.getCmp(namaForm);
			form_nya.getForm().submit({
				url: BASE_URL + 'nota_controller/insert_pasienluar',
				method: 'POST',
				params: {
					ureg 		: 'FA',
					tglnota		: Ext.getCmp('df.tglshift').getValue().format('Y-m-d'),
					jtransaksi	: 8,
					uangr	 	: Ext.getCmp('tf.uangr').getValue(),
					diskon	 	: Ext.getCmp('tf.diskonf').getValue(),
					arrnota		: Ext.encode(arrnota),
					arrcarabayar: Ext.encode(arrcarabayar)
				},
				success: function(transaksipl_form, o) {
					if (o.result.success==true) {
						waitmsg.hide();
						Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
						Ext.getCmp('tf.nonota').setValue(o.result.nonota);
						Ext.getCmp('tf.nokuitansi').setValue(o.result.nokuitansi);
						Ext.getCmp('bt.simpan').disable();
						Ext.getCmp("bt.batal").enable();
						Ext.getCmp('bt.cetak').enable();
						myStopFunction();
						
						var zx = 0;
						ds_nota.each(function(rec){
							zjkartustok 		= 3;
							ztglkartustok 		= Ext.getCmp('df.tglshift').getValue().format('Y/m/d');
							zjamkartustok 		= Ext.getCmp('tf.jamshift').getValue();
							zjmlkeluar	 		= rec.get('qty');
							znama		 		= 'INSTALASI FARMASI';
							zkditem				= rec.get('kditem');
							arrnota[zx] = zjkartustok+'-'+ztglkartustok+'-'+zjamkartustok+'-'+zjmlkeluar+'-'+znama+'-'+zkditem;
							zx++;
						});
						Ext.Ajax.request({
							url: BASE_URL + 'nota_controller/insert_kartustok',
							params: {
								noref : Ext.getCmp('tf.nonota').getValue(),
								arrnota : Ext.encode(arrnota)
							}
						});
						ds_notanf.reload();
					} else {
						waitmsg.hide();
						Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
					}
				},
				failure: function (form, action) {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			});
		}
	}	

	function batal_new(){
		/*	var form_nya = Ext.getCmp(namaForm);				
			if (form_nya.getForm().isValid()) {*/
				var nonota = Ext.getCmp('tf.nonota').getValue();
				var nokuitansi = Ext.getCmp('tf.nokuitansi').getValue();
				var arrnota = [];
				var zx = 0;
				
				ds_nota.each(function(rec){
					zkditem = rec.get('kditem');
					zqty = rec.get('qty');
					zkoder = rec.get('koder');
					ztarif2 = rec.get('tarif2');
					zdiskonjs = 0;
					zdiskonjm = 0;
					zdiskonjp = 0;
					zdiskonbhp = 0;
					arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp;
					zx++;
				});
			
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/batal_transfarluar',
					method: 'POST',
					params: {
						nonota	: nonota, //Ext.getCmp('tf.bnota').getValue(),
						nokui	: nokuitansi, //Ext.getCmp('tf.bkui').getValue(),
						passbatal:null, //Ext.getCmp('tf.pwdbatal').getValue(),
						tglnota	: Ext.getCmp('df.tglshift').getValue().format('Y-m-d'),
						arrnota	: Ext.encode(arrnota),
					},
					success: function(response) {
						var r = response.responseText;
						Ext.MessageBox.alert("Informasi", r);					
						if (r == "Batal Berhasil") {							
							Ext.getCmp("bt.simpan").disable();
							Ext.getCmp("bt.batal").disable();
							Ext.getCmp("bt.cetak").disable();
							ds_notanf.reload();
							bersihnf();
						}
					},
					failure: function() {
						Ext.MessageBox.alert("Informasi", "Batal Gagal");
					}
				});			
		/*	}else if (!form_nya.getForm().isValid()) {
				Ext.MessageBox.alert("Informasi", "Lengkapi");
			}
			wBatal.close();*/
		}
	
	function fbatal(){
		var nonota = Ext.getCmp('tf.nonota').getValue();
		var nokuitansi = Ext.getCmp('tf.nokuitansi').getValue();
		var btl_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'fp.btl',
			buttonAlign: 'left',
			labelWidth: 115, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 100, width: 325,
			layout: 'form', 
			frame: false,
			items: [{
				xtype: 'textfield', fieldLabel:'No. Nota',
				id: 'tf.bnota', width: 150, value: nonota, hidden: true
			},{
				xtype: 'textfield', fieldLabel:'No. Kuitansi',
				id: 'tf.bkui', width: 150, value: nokuitansi, hidden: true
			},{
				xtype: 'textfield', fieldLabel:'Masukan Password',
				id: 'tf.pwdbatal', width: 150, inputType: 'password',
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					if(Ext.getCmp('tf.pwdbatal').getValue() !=''){
						Ext.Msg.show({
							title: 'Konfirmasi',
							msg: 'Apakah Anda Yakin Akan Membatalakan Transaksi Ini..?',
							buttons: Ext.Msg.YESNO,
							icon: Ext.MessageBox.QUESTION,
							fn: function (response) {
								if ('yes' == response) {	
									batal("fp.btl");
								}
							}
						});		
					}else{
						Ext.MessageBox.alert("Informasi", "Anda Belum Isi Password");
					}
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wBatal.close();
				}
			}]
		});
			
		var wBatal = new Ext.Window({
			title: 'Batal',
			modal: true, closable:false,
			items: [btl_form]
		});
		
		wBatal.show();		
	
		function batal(namaForm){
			var form_nya = Ext.getCmp(namaForm);				
			if (form_nya.getForm().isValid()) {
				var arrnota = [];
				var zx = 0;
				
				ds_nota.each(function(rec){
					zkditem = rec.get('kditem');
					zqty = rec.get('qty');
					zkoder = rec.get('koder');
					ztarif2 = rec.get('tarif2');
					zdiskonjs = 0;
					zdiskonjm = 0;
					zdiskonjp = 0;
					zdiskonbhp = 0;
					arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp;
					zx++;
				});
			
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/batal_transfarluar',
					method: 'POST',
					params: {
						nonota	: Ext.getCmp('tf.bnota').getValue(),
						nokui	: Ext.getCmp('tf.bkui').getValue(),
						passbatal:Ext.getCmp('tf.pwdbatal').getValue(),
						tglnota	: Ext.getCmp('df.tglshift').getValue().format('Y-m-d'),
						arrnota	: Ext.encode(arrnota),
					},
					success: function(response) {
						var r = response.responseText;
						Ext.MessageBox.alert("Informasi", r);					
						if (r == "Batal Berhasil") {							
							Ext.getCmp("bt.simpan").disable();
							Ext.getCmp("bt.batal").disable();
							Ext.getCmp("bt.cetak").disable();
							ds_notanf.reload();
						}
					},
					failure: function() {
						Ext.MessageBox.alert("Informasi", "Batal Gagal");
					}
				});			
			}else if (!form_nya.getForm().isValid()) {
				Ext.MessageBox.alert("Informasi", "Lengkapi");
			}
			wBatal.close();
		}
	}

	function bersihnf() {
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue('Pasien Luar');
		Ext.getCmp('tf.dokter').setValue();
		Ext.getCmp('tf.nonota').setValue();
		Ext.getCmp('tf.nokuitansi').setValue();
		//Ext.getCmp('df.tglshift').setValue(new Date());
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/get_tgl_svr',
			method: 'POST',
			params: {
			
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp('df.tglshift').setValue(obj.date);
			},
			failure : function(){
				Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
			}
		});
		Ext.getCmp('tf.catatannf').setValue('Farmasi Pasien Luar');
		Ext.getCmp('tf.catatandsknnf').setValue();
		Ext.getCmp('tf.noresep').setValue();
		Ext.getCmp('tf.total').setValue(0);
		Ext.getCmp('tf.uangr').setValue(0);
		Ext.getCmp('tf.diskonprsn').setValue(0);
		Ext.getCmp('tf.diskonf').setValue(0);
		Ext.getCmp('tf.total2').setValue(0);
		Ext.getCmp('tf.jumlah').setValue(0);
		Ext.getCmp('tf.utyd').setValue();
		Ext.getCmp('tf.kembalian').setValue();
		Ext.getCmp('bt.simpan').enable();
		Ext.getCmp("bt.batal").disable();
		Ext.getCmp('btn_add').enable();
		Ext.getCmp('bt.cetak').disable();
		koder = 0;
		ds_nota.setBaseParam('nonota',null);
		ds_nota.setBaseParam('koder',null);
		ds_nota.setBaseParam('idbagian',999);
		ds_nota.reload();
		ds_kuitansidet.setBaseParam('blank', 1);
		ds_kuitansidet.reload();
		pmbyrn = 0;
		myVar = setInterval(function(){myTimer()},1000);
	}
	
	function dataPasien(){
		Ext.Ajax.request({
			url: BASE_URL + 'pasien_controller/getDataPasien',
			params: {
				norm		: Ext.getCmp('tf.norm').getValue()
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				var var_cari_pasienno = obj.norm;
				var var_cari_pasiennm = obj.nmpasien;
					
				Ext.getCmp('tf.norm').focus()
				Ext.getCmp("tf.norm").setValue(parseInt(var_cari_pasienno));
				Ext.getCmp("tf.nmpasien").setValue(var_cari_pasiennm);
			},
			failure: function() {
				//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
			}
		});
	}
	
	function dftDokter(){
		function viewDokter(value){
			Ext.QuickTips.init();
			return '<div class="keyDokter" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_dokter = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'iddokter'
			},{
				header: 'Nama Dokter',
				dataIndex: 'nmdoktergelar',
				width: 200,
				renderer: viewDokter
			},{
				header: 'Spesialisasi',
				dataIndex: 'nmspesialisasi',
				width: 200
			}
		]);
		var sm_dokter = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_dokter = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_dokter = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_dokter,
			displayInfo: true,
			displayMsg: 'Data Dokter Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_dokter = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_dokter= new Ext.grid.GridPanel({
			ds: ds_dokter,
			cm: cm_dokter,
			sm: sm_dokter,
			view: vw_dokter,
			height: 400,
			width: 430,
			plugins: cari_dokter,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_dokter,
			listeners: {
				cellclick: klik_dokter
			}
		});
		var win_dokter = new Ext.Window({
			title: 'Cari Dokter',
			modal: true,
			items: [grid_dokter]
		}).show();

		function klik_dokter(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyDokter'){
				var rec_dokter = ds_dokter.getAt(rowIdx);
				var var_nmdokterglr = rec_dokter.data["nmdoktergelar"];
				
				Ext.getCmp("tf.dokter").setValue(var_nmdokterglr);
							win_dokter.close();
			}
		}
	}
	
	function hitung(){
		var arr = [];
		var xxx = 0;
		for (var zxc = 0; zxc <ds_nota.data.items.length; zxc++) {
			var record = ds_nota.data.items[zxc].data;
			zkditem = record.kdbrg;
			xxx += parseFloat(record.tarif2);
			arr[zxc] = zkditem;
		}
		Ext.getCmp("tf.total").setValue(xxx);
		var jmluangr = Ext.getCmp("tf.uangr").getValue();
		var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
		var sumtotal = xxx + jmluangr;
		var sumtotaldisk = sumtotal - jmldiskon;
		Ext.getCmp('tf.total2').setValue(sumtotaldisk);
		cektombol();
	}
	
	function fPemb(){
		var ds_carabayar = dm_carabayar();
		var ds_bankcb = dm_bank();
		var pmbyrntemp = 0;
		var total = Ext.getCmp('tf.total2').getValue();
		pmbyrntemp = total - pmbyrn;
		if(pmbyrntemp < 0) pmbyrntemp = 0;
		var pemb_form = new Ext.form.FormPanel({
			xtype:'form',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 200, width: 300,
			layout: 'form', 
			frame: false,
			items: [     
			{
				xtype: 'combo', fieldLabel: 'Cara Bayar',
				id: 'cb.pembcb', width: 150, 
				store: ds_carabayar, valueField: 'idcarabayar', displayField: 'nmcarabayar',
				editable: false, triggerAction: 'all', value:'Tunai',
				forceSelection: true, submitValue: true, mode: 'local',
				listeners:{
					select:function(combo, records, eOpts){
						if(records.get('idcarabayar') != 1){
							Ext.getCmp('cb.pembbank').enable();
							Ext.getCmp('tf.pembkartu').enable();
						} else {
							Ext.getCmp('cb.pembbank').disable();
							Ext.getCmp('tf.pembkartu').disable();
							Ext.getCmp('cb.pembbank').setValue('');
							Ext.getCmp('tf.pembkartu').setValue('');
						}
					}
				}
			},    
			{
				xtype: 'combo', fieldLabel: 'Bank',
				id: 'cb.pembbank', width: 150, 
				store: ds_bankcb, valueField: 'idbank', displayField: 'nmbank',
				editable: false, triggerAction: 'all', disabled:true,
				forceSelection: true, submitValue: true, mode: 'local',
			},{
				xtype: 'textfield', fieldLabel:'No. Kartu', disabled:true,
				id: 'tf.pembkartu', width: 150
			},{
				xtype: 'numericfield', fieldLabel:'Nominal',
				id: 'tf.pembnominal', width: 150, height: 50,
				value: pmbyrntemp,
				style: {
					'fontSize'     : '20px'
				}
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					PembAdd();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wPemb.close();
				}
			}]
		});
			
		var wPemb = new Ext.Window({
			title: 'Pembayaran',
			modal: true, closable:false,
			items: [pemb_form]
		});
		
		wPemb.show();
		
		function PembAdd(){
			if(Ext.getCmp('cb.pembcb').value == 8){
				if(Ext.getCmp('tf.pembnominal').getValue()>pmbyrndjmntemp)
					Ext.getCmp('tf.pembnominal').setValue(pmbyrndjmntemp);
				pmbyrndjmn += Ext.getCmp('tf.pembnominal').getValue();
			} else {
				if(Ext.getCmp('tf.pembnominal').getValue()>pmbyrntemp)
					Ext.getCmp('tf.pembnominal').setValue(pmbyrntemp);
				pmbyrn += Ext.getCmp('tf.pembnominal').getValue();
			}
			
			var orgaListRecord = new Ext.data.Record.create([
				{
					name: 'nmcarabayar',
					name: 'nmbank',
					name: 'nokartu',
					name: 'jumlah'
				}
			]);
			
			ds_kuitansidet.add([
				new orgaListRecord({
					'nmcarabayar': Ext.getCmp('cb.pembcb').lastSelectionText,
					'nmbank': Ext.getCmp('cb.pembbank').lastSelectionText,
					'nokartu': Ext.getCmp('tf.pembkartu').getValue(),
					'jumlah': Ext.getCmp('tf.pembnominal').getValue()
				})
			]);
			if(pmbyrn < 0) pmbyrn = 0;
			Ext.getCmp('tf.jumlah').setValue(pmbyrn);
			cektombol();
			wPemb.close();
		}
	}
	
	function hitungKembalian() {
		var jml = Ext.getCmp('tf.jumlah').getValue();
		if(jml != ''){
			var utyd = Ext.getCmp('tf.utyd').getValue();
			var kmblian = utyd - jml;
			Ext.getCmp('tf.kembalian').setValue(kmblian);
		}
	}
	
	function cektombol(){
		var total = Ext.getCmp('tf.total2').getValue();
		if(Ext.getCmp("tf.nonota").getValue() == ''){
			if(total<=0){
				Ext.getCmp("bt.simpan").disable();
			} else {
				if(pmbyrn >= total) Ext.getCmp("bt.simpan").enable();
				else Ext.getCmp("bt.simpan").disable();
			}
		} else {
			Ext.getCmp("bt.simpan").disable();
		}
	}
	
}