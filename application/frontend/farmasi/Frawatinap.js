function Frawatinap(){
	var myVar= setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jam"))
				RH.setCompValue("tf.jam",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("tf.shift").setValue(obj.nmshift);
		}
	});
	
	var ds_nota2 = dm_nota();
	ds_nota2.setBaseParam('start',0);
	ds_nota2.setBaseParam('limit',500);
	ds_nota2.setBaseParam('idregdet',null);
	ds_nota2.setBaseParam('idbagian',999);
	ds_nota2.setBaseParam('okoder',1);
	
	var ds_notax = dm_notax();
	ds_notax.setBaseParam('noreg', -1);
		
	var ds_counnonota = dm_counnonota();
		
	var ds_vregistrasi = dm_vregistrasi();
	ds_vregistrasi.setBaseParam('start',0);
	ds_vregistrasi.setBaseParam('limit',5);
	ds_vregistrasi.setBaseParam('cek','RI');
	ds_vregistrasi.setBaseParam('riposisipasien',5);
	ds_vregistrasi.setBaseParam('groupby','noreg');
	ds_vregistrasi.setBaseParam('oposisipasien',1);
	ds_vregistrasi.setBaseParam('onmpasien',1);
	ds_vregistrasi.setBaseParam('khususri',1);
	ds_vregistrasi.setBaseParam('gnoreg',1);
	
	var ds_brgbagian = dm_brgbagian();
	ds_brgbagian.setBaseParam('start',0);
	ds_brgbagian.setBaseParam('limit',7);
	ds_brgbagian.setBaseParam('cidbagian',0);
	
	var ds_dokter = dm_dokter();
	var arr = [];
	var zkditem = '';
	var rownota = '';
	var koder = 0;
	var cekkstok = 0;
	
	var grid_notafarmasi = new Ext.grid.EditorGridPanel({
		store: ds_nota2,
		frame: true,
		height: 310,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota2',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar: [{
			text: 'Template Obat',
			id: 'btn_add',
			disabled: true,
			iconCls: 'silk-add',
			handler: function() {
				TemObat();
			}
		},{ xtype:'tbfill' },'Deposit : ',
		{
			xtype: 'numericfield',
			id: 'tf.deposit',
			disabled:true,
			value: 0,
			width: 100,
			thousandSeparator:','
		}],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota2 = rowIndex;
            }
        },
		columns: [{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					ds_nota2.removeAt(rowIndex);
					totalnota();
				}
			}]
		},{
			header: 'R/',
			dataIndex: 'koder',
			width: 30,editor: {
				xtype: 'numberfield',
				id: 'tf.koder', width: 30
			},
			sortable: true
		},{
			header: '<div style="text-align:center;">Item Obat/Alkes</div>',
			dataIndex: 'nmitem',
			width: 250,
			sortable: true
		},{
			header: '<div style="text-align:center;">Tarif</div>',
			dataIndex: 'tarif',
			align:'right',
			width: 90,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: 'Qty',
			dataIndex: 'qty',
			align:'right',
			width: 45,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqtyf', width: 150, 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota2.getAt(rownota2);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqtyf').getValue();
						record.set('tarif2',subtotal);
						totalnota();
					}
				}
			},
			sortable: true
		},{
			header: '<div style="text-align:center;">Qty<br>Retur</div>',
			dataIndex: 'qtyretur',
			align:'right',
			width: 55,
			sortable: true
		},{
			header: '<div style="text-align:center;">Satuan</div>',
			dataIndex: 'nmsatuan',
			width: 80,
			sortable: true
		},{
			header: '<div style="text-align:center;">Subtotal</div>',
			dataIndex: 'tarif2',
			align:'right',
			width: 90,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
			/* renderer: function(value, p, r){
				var subtotal = parseFloat(r.data['qty']) * parseFloat(r.data['tarif']);
				var subtotal2 = subtotal * (Ext.getCmp('tf.notadprsn').getValue() / 100);
				var subtotal3 = Ext.getCmp('tf.notadrp').getValue();
				var jsubtotal = subtotal - subtotal2 - subtotal3;
				return Ext.util.Format.number(jsubtotal, '0,000.00');
			} */
		},{
			dataIndex: 'kditem',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idjnstarif',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idtarifpaketdet',
			hidden: true,
			hideable: false
		}],
		bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 400,
				items: [{
					xtype: 'compositefield',
					hidden: true,
					items: [{
						xtype: 'label', id: 'lb.totalrf', text: 'Total :', margins: '0 10 0 138',
					},{
						xtype: 'numericfield',
						id: 'tf.totalrf',
						value: 0,
						width: 100,
						readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					}]
				},{
					xtype: 'compositefield',
					hidden: true,
					items: [{
						xtype: 'label', id: 'lb.uangr', text: 'Racik :', margins: '0 10 0 134',
					},{
						xtype: 'numericfield',
						id: 'tf.uangr',
						value: 0,
						width: 100,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								totalnota();
							}
						}
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.diskon2', text: 'Diskon Rp :', margins: '0 9 0 0',
						hidden: true
					},{
						xtype: 'numericfield',
						id: 'tf.diskonf',
						value: 0,
						width: 50,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								totalnota();
							}
						},
						hidden: true
					},{
						xtype: 'label', id: 'lb.total2', text: 'Jumlah :', margins: '0 10 0 127',
					},{
						xtype: 'numericfield',
						id: 'tf.total2',
						value: 0,
						readOnly:true,
						style : 'opacity:0.6',
						width: 100,
						thousandSeparator:','
					}]
				}]
			}
		]
	});
	
	var paging_reg = new Ext.PagingToolbar({
		pageSize: 6,
		store: ds_vregistrasi,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230
	})];
	
	var grid_reg = new Ext.grid.GridPanel({
		title: 'Daftar Registrasi RI',
		store: ds_vregistrasi,
		frame: true,
		height: 270,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_reg',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_freg = ds_vregistrasi.getAt(rowIndex);
				var freg_noreg = rec_freg.data["noreg"];
				var freg_norm = rec_freg.data["norm"];
				var freg_nmpasien = rec_freg.data["nmpasien"];
				var freg_atasnama = rec_freg.data["atasnama"];
				var freg_idbagian = rec_freg.data["idbagian"];
				var freg_nmbagian = rec_freg.data["nmbagian"];
				var freg_nmdokter = rec_freg.data["nmdoktergelar"];
				var freg_nonota = rec_freg.data["nonota"];
				var freg_total = rec_freg.data["total"];
				var freg_tglnota = rec_freg.data["tglnota"];
				var freg_jamnota = rec_freg.data["jamnota"];
				var freg_catatannota = rec_freg.data["catatannota"];
				var freg_tgllahirp = rec_freg.data["tgllahirp"];
				var freg_idjnskelamin = rec_freg.data["idjnskelamin"];
				var freg_idregdet = rec_freg.data["idregdet"];
				var freg_nmkamar = rec_freg.data["nmkamar"];
				var freg_nmbed = rec_freg.data["nmbed"];
				var freg_nmklstarif = rec_freg.data["nmklstarif"];
				var freg_deposit = rec_freg.data["deposit"];
				
				Ext.getCmp("tf.noreg").setValue(freg_noreg);
				Ext.getCmp("tf.norm").setValue(freg_norm);
				Ext.getCmp("tf.nmpasien").setValue(freg_nmpasien);
				Ext.getCmp("tf.upel").setValue(freg_nmbagian);
				Ext.getCmp("tf.nmkamar").setValue(freg_nmkamar);
				Ext.getCmp("tf.nmbed").setValue(freg_nmbed);
				Ext.getCmp("tf.dokter").setValue(freg_nmdokter);
				Ext.getCmp("tf.nmklstarif").setValue(freg_nmklstarif);
				Ext.getCmp("tf.nonota").setValue();
				Ext.getCmp("idregdet").setValue(freg_idregdet);
				Ext.getCmp("cb.nota").setValue('');
				Ext.getCmp("btn_add").enable();
				if(freg_catatannota == null)freg_catatannota ='Farmasi Rawat Inap';
				Ext.getCmp("tf.catatan").setValue(freg_catatannota);
				Ext.getCmp("jkel").setValue(freg_idjnskelamin);
				Ext.getCmp("btn.cetak").disable();
				
				if(freg_atasnama == null) Ext.getCmp("tf.an").setValue(freg_nmpasien);
				else Ext.getCmp("tf.an").setValue(freg_atasnama);
				if(freg_deposit == null) Ext.getCmp('tf.deposit').setValue(0);
				else Ext.getCmp('tf.deposit').setValue(freg_deposit);
				Ext.getCmp('tf.totalrf').setValue(0);
				Ext.getCmp('tf.diskonf').setValue(0);
				Ext.getCmp('tf.uangr').setValue(0);
				Ext.getCmp('tf.total2').setValue(0);
				koder = 0;
				ds_nota2.setBaseParam('idbagian',999);
				ds_nota2.reload();
				ds_notax.setBaseParam('noreg',freg_noreg);
				ds_notax.setBaseParam('jpel',4);
				ds_notax.reload();
				ds_counnonota.setBaseParam('noreg',freg_noreg);
				ds_counnonota.reload({
					scope   : this,
					callback: function(records, operation, success) {
						var tamtot = 0;
						 ds_counnonota.each(function (rec) { 
								tamtot = rec.get('cnonota');
							});
						if(tamtot == null){
							Ext.getCmp("tf.countnonota").setValue('0');
						}else{						
							Ext.getCmp("tf.countnonota").setValue(tamtot);
						}
					}
				});
				ds_brgbagian.setBaseParam('cidbagian',11);
				ds_brgbagian.reload();
				if(freg_tglnota == null) Ext.getCmp('df.tgl').setValue(new Date());
				else Ext.getCmp('df.tgl').setValue(freg_tglnota);
				/* if(freg_jamnota == null){
					myStopFunction();
					myVar = setInterval(function(){myTimer()},1000);
				} else {
					Ext.getCmp('tf.jam').setValue(freg_jamnota);
					myStopFunction();
				} */
				myStopFunction();
				//bersihFRinota();
				umur(new Date(freg_tgllahirp));
            }
        },
		columns: [{
			header: 'No Reg',
			dataIndex: 'noreg',
			width: 75,
			sortable: true
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 75,
			sortable: true
		},{
			header: 'Pasien',
			dataIndex: 'nmpasien',
			width: 110,
			sortable: true
		},{
			header: 'L/P',
			dataIndex: 'kdjnskelamin',
			width: 30,
			sortable: true
		},{
			header: 'Ruangan',
			dataIndex: 'nmbagian',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Lahir',
			dataIndex: 'tgllahirp',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Posisi Pasien',
			dataIndex: 'nmstposisipasien',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Registrasi',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Dokter',
			dataIndex: 'nmdoktergelar',
			width: 100,
			sortable: true
		},{
			header: 'Penjamin',
			dataIndex: 'nmpenjamin',
			width: 100,
			sortable: true
		}],
		bbar: paging_reg,
		plugins: cari_reg
	});
	
	var paging_farmasi = new Ext.PagingToolbar({
		pageSize: 7,
		store: ds_brgbagian,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_farmasi = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230
	})];
	
	var grid_farmasi = new Ext.grid.GridPanel({
		title: 'Apotek',
		store: ds_brgbagian,
		frame: true,
		height: 270,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_farmasi',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:[],
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var obj = ds_brgbagian.getAt(rowIndex);
				var skdbrg			= obj.data["kdbrg"];
				var snmbrg			= obj.data["nmbrg"];
				var starif			= obj.data["tarif"];
				var snmsatuan		= obj.data["nmsatuan"];
				koder++;
				
				var orgaListRecord = new Ext.data.Record.create([
					{
						name: 'kditem',
						name: 'koder',
						name: 'nmitem',
						name: 'qty',
						name: 'tarif',
						name: 'tarif2',
						name: 'nmsatuan'
					}
				]);
				
				ds_nota2.add([
					new orgaListRecord({
						'kditem': skdbrg,
						'koder': koder,
						'nmitem': snmbrg,
						'qty': 1,
						'tarif': starif,
						'tarif2': starif,
						'nmsatuan': snmsatuan
					})
				]);
				var ttl = Ext.getCmp('tf.totalrf').getValue();
				var jml = parseFloat(ttl) + parseFloat(starif);
				var jmluangr = Ext.getCmp('tf.uangr').getValue();
				var jmldiskon = Ext.getCmp('tf.diskonf').getValue();
				var sumtotal = jml + jmluangr;
				var sumtotaldisk = sumtotal - jmldiskon;
				Ext.getCmp('tf.totalrf').setValue(jml);
				Ext.getCmp('tf.total2').setValue(sumtotaldisk);
				grid_notafarmasi.getView().focusRow(ds_nota2.getCount() - 1);
            }
        },
		columns: [{
			header: 'Nama Obat/Alkes',
			dataIndex: 'nmbrg',
			width: 160,
			sortable: true
		},{
			header: 'Harga',
			dataIndex: 'tarif',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: 'Stok',
			dataIndex: 'stoknowbagian',
			align:'right',
			width: 45,
			sortable: true
		}],
		bbar: paging_farmasi,
		plugins: cari_farmasi
	});
	
	var rifarmasi_form = new Ext.form.FormPanel({
		id: 'fp.rifarmasi',
		title: 'Nota Farmasi RI',
		width: 900,
		Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			{ text: 'Nota Baru', iconCls: 'silk-add', handler: function(){bersihFRinota();} },'-',
			{ text: 'Simpan', id:'btn.simpan', iconCls: 'silk-save', handler: function(){simpanRIF();} },'-',
			{ text: 'Cetak Nota', id:'btn.cetak', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRIF();} },'-',
			{ xtype: 'tbfill' },'->'
		],
        items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:160,
				boxMaxHeight:160,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. RM',
						id: 'tf.norm',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					},{
						xtype: 'label', id: 'lb.noreg', text: 'No. Registrasi', margins: '0 10 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.noreg',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					}]
				},{
					xtype: 'textfield', fieldLabel:'Nama Pasien',
					id: 'tf.nmpasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield', fieldLabel:'Ruangan',
						id: 'tf.upel',
						readOnly: true, style : 'opacity:0.6',
						width: 80,
					},{
						xtype: 'label', id: 'lb.kamarbed', text: 'Kamar / Bed', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmkamar', 
						readOnly: true, style : 'opacity:0.6',
						width: 35,
					},{
						xtype: 'label', id: 'lb.garing', text: '/', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmbed', 
						readOnly: true, style : 'opacity:0.6',
						width: 38,
					}]
				},{
					xtype: 'textfield', fieldLabel:'Kelas Tarif',
					id: 'tf.nmklstarif',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Dokter',
					id: 'tf.dokter',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:160,
				boxMaxHeight:160,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'textfield', fieldLabel:'No. Nota',
					id: 'tf.nonota',
					readOnly: true, style : 'opacity:0.6',
					width: 100, hidden: true
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'combo', fieldLabel : 'No. Nota',
						id: 'cb.nota', width: 220,//anchor: "100%",
						store: ds_notax, valueField: 'nonota', displayField: 'nonota',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih...',
						listeners:{
							select:function(combo, records, eOpts){
								var idregdet = Ext.getCmp('idregdet').getValue();
								Ext.getCmp('df.tgl').setValue(records.get('tglnota'));
								Ext.getCmp('tf.jam').setValue(records.get('jamnota'));
								ds_nota2.setBaseParam('khri',1);
								ds_nota2.setBaseParam('nonota',records.get('nonota'));
								ds_nota2.setBaseParam('koder',1);
								ds_nota2.setBaseParam('idbagian',null);
								fTotal();
								Ext.getCmp('tf.nonota').setValue(records.get('nonota'));
								Ext.getCmp('tf.diskonf').setValue(records.get('diskon'));
								Ext.getCmp('tf.uangr').setValue(records.get('uangr'));
								Ext.getCmp("btn.cetak").enable();
							}
						}
					},{
						xtype: 'textfield',
						id: 'tf.countnonota',
						readOnly: true, style : 'opacity:0.6;font-weight: bold',
						width: 30,
					}]
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl./Jam/Shift', id: 'df.tgl',
						width: 100, value: new Date(),
						format: 'd-m-Y',
					},{
						xtype: 'label', id: 'lb.garing3', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jam', readOnly:true,
						width: 65
					},{
						xtype: 'label', id: 'lb.garing4', text: '/'
					},{
						xtype: 'textfield', id: 'tf.shift', 
						width: 60, disabled: true
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Atas Nama',
					id: 'tf.an', anchor: "100%"
				},{
					xtype: 'textarea', fieldLabel: 'Catatan',
					id: 'tf.catatan', anchor: "100%", height: 50,
					value: 'Farmasi Rawat Inap'
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_notafarmasi]
				}]
		},{
			xtype: 'textfield',
			id: 'idregdet', hidden: true
		},{
			xtype: 'textfield',
			id: 'tahun', hidden: true
		},{
			xtype: 'textfield',
			id: 'jkel', hidden: true
		}]
	});
	
	var transFRIDispPanel = new Ext.form.FormPanel({
		id: 'fp.transFRIDispPanel',
		name: 'fp.transFRIDispPanel',
		border: false, 
		forceFit:true,
		frame: true,
		autoScroll:true,
		labelAlign: 'top',
		layout: 'anchor',
		items: [
		{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_reg]
		},{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_farmasi]
		}]
	});
	
	var transFRIPanel = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Farmasi Rawat Inap',
		frame: false,
		border: true,
		margins: '0 0 5 0',
		plain: true,		
		forceFit: true,
		layout: 'border',
		items: [{
			region: 'center',
			xtype: 'panel',
			border: true,
			layout: 'fit',
			items: [rifarmasi_form],
		},
		{
			region: 'east',
			xtype: 'panel',
			title: '', border: true,
			layout:'fit', width:350,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [transFRIDispPanel],
		}]
	});
	
	SET_PAGE_CONTENT(transFRIPanel);
	
	function bersihFRinota() {
		Ext.getCmp('cb.nota').setValue('');
		Ext.getCmp('tf.nonota').setValue('');
		ds_nota2.setBaseParam('idregdet', null);
		ds_nota2.setBaseParam('idbagian', 999);
		ds_nota2.reload();
		Ext.getCmp('btn.cetak').disable();
		Ext.getCmp('tf.totalrf').setValue(0);
		Ext.getCmp('tf.diskonf').setValue(0);
		Ext.getCmp('tf.uangr').setValue(0);
		Ext.getCmp('tf.total2').setValue(0);
		var formattedValue = Ext.util.Format.date(new Date(), 'H:i:s');
		Ext.getCmp('tf.jam').setValue(formattedValue);
	}
	
	function fTotal(){
		ds_nota2.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				ds_nota2.each(function (rec) { sum += parseFloat(rec.get('tarif2')); });
				var jmluangr = Ext.getCmp("tf.uangr").getValue();
				var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
				var sumtotal = sum + jmluangr;
				var sumtotaldisk = sumtotal - jmldiskon;
				Ext.getCmp("tf.totalrf").setValue(sum);
				Ext.getCmp("tf.total2").setValue(sumtotaldisk);
			}
		});
	}
	
	function simpanRIF(){
		var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
		var arrnota = [];
		var zx = 0;
		
		ds_nota2.each(function (rec) {
			zkditem = rec.get('kditem');
			zqty = rec.get('qty');
			zkoder = rec.get('koder');
			ztarif2 = rec.get('tarif2');
			zdiskonjs = 0;
			zdiskonjm = 0;
			zdiskonjp = 0;
			zdiskonbhp = 0;
			arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp;
			zx++;
		});
		
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/insorupd_nota',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue(),
				nonota		: Ext.getCmp('tf.nonota').getValue(),
				nmshift 	: Ext.getCmp('tf.shift').getValue(),
				catatan 	: Ext.getCmp('tf.catatan').getValue(),
				nmpasien 	: Ext.getCmp('tf.nmpasien').getValue(),
				total	 	: Ext.getCmp('tf.total2').getValue(),
				uangr	 	: Ext.getCmp('tf.uangr').getValue(),
				diskon	 	: Ext.getCmp('tf.diskonf').getValue(),
				atasnama 	: Ext.getCmp('tf.an').getValue(),
				nota		: Ext.getCmp('cb.nota').lastSelectionText,
				tglnota		: Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'),
				jamnota		: Ext.getCmp('tf.jam').getValue(),
				tahun		: Ext.getCmp('tahun').getValue(),
				jkel		: Ext.getCmp('jkel').getValue(),
				jpel 		: 'Farmasi',
				ureg 		: 'FA',
				fri 		: 11,
				jtransaksi	: 7,
				kui			: 1,
				arrnota		: Ext.encode(arrnota)
			},
			success: function(response){
				waitmsg.hide();
				Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
				obj = Ext.util.JSON.decode(response.responseText);
				ds_vregistrasi.reload();
				ds_notax.reload();				
				ds_counnonota.setBaseParam('noreg',Ext.getCmp("tf.noreg").getValue());
				ds_counnonota.reload({
					scope   : this,
					callback: function(records, operation, success) {
						var tamtot = 0;
						 ds_counnonota.each(function (rec) { 
								tamtot = rec.get('cnonota');
							});
						if(tamtot == null){
							Ext.getCmp("tf.countnonota").setValue('0');
						}else{						
							Ext.getCmp("tf.countnonota").setValue(tamtot);
						}
					}
				});
				Ext.getCmp("tf.nonota").setValue(obj.nonota);
				Ext.getCmp("cb.nota").setValue(obj.nonota);
				Ext.getCmp("btn.cetak").enable();
				myStopFunction();
			},
			failure: function (form, action) {
				waitmsg.hide();
				Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
			}
		});
	}
	
	function hapusItem(){
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/delete_bnotadet',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue(),
				nonota		: Ext.getCmp('tf.nonota').getValue(),
				arr			:  Ext.encode(arr)
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
				bersihrifarmasi();
			},
			failure: function() {
			}
		});
	}
	
	function cetakRIF(){
		var nonota		= Ext.getCmp('tf.nonota').getValue();
		var noreg		= Ext.getCmp('tf.noreg').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/nota_ri/'
                +nonota+'/'+noreg);
	}
	
	function totalnota(){
		var zzz = 0;
		koder = 0;
		for (var zxc = 0; zxc <ds_nota2.data.items.length; zxc++) {
			var record = ds_nota2.data.items[zxc].data;
			zzz += parseFloat(record.tarif2);
			if(koder < record.koder) koder = record.koder;
		}
		var jmluangr = Ext.getCmp("tf.uangr").getValue();
		var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
		var sumtotal = zzz + jmluangr;
		var sumtotaldisk = sumtotal - jmldiskon;
		Ext.getCmp('tf.totalrf').setValue(zzz);
		Ext.getCmp('tf.total2').setValue(sumtotaldisk);
	}

	function hitungKembalian() {
		var jml = Ext.getCmp('tf.jumlah').getValue();
		if(jml != ''){
			var utyd = Ext.getCmp('tf.utyd').getValue();
			var kmblian = utyd - jml;
			Ext.getCmp('tf.kembalian').setValue(kmblian);
		}
	}

	function umur(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tahun').setValue(year);
	}
	
	function TemObat(){
		var ds_mastertemobat_parent = dm_mastertemobat_parent();
		var ds_temobat = dm_temobat();
		
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
				
		var cm_masterpaket_parent = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idtempobat',
				width: 30
			},{
				header: 'Nama Template Obat',
				dataIndex: 'nmtempobat',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		var sm_masterpaket_parent = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_masterpaket_parent = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_masterpaket_parent = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_mastertemobat_parent,
			displayInfo: true,
			displayMsg: 'Data Master Paket Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_masterpaket_parent = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200,
			disableIndexes:['idtempobat'],
		})];
		var grid_find_masterpaket_parent = new Ext.grid.GridPanel({
			ds: ds_mastertemobat_parent,
			cm: cm_masterpaket_parent,
			sm: sm_masterpaket_parent,
			view: vw_masterpaket_parent,
			height: 460,
			width: 422,
			plugins: cari_masterpaket_parent,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			autoScroll: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_masterpaket_parent,
			listeners: {
				cellclick: onCellClickaddparent
			}
		});
		var win_find_masterpaket_parent = new Ext.Window({
			title: 'Template Obat',
			modal: true,
			items: [grid_find_masterpaket_parent]
		}).show();
		
		function onCellClickaddparent(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			
			if (t.className == 'keyMasterDetail'){
				var record = grid.getStore().getAt(rowIndex);
				var var_idtempobat_parent = record.data["idtempobat"];
				ds_temobat.setBaseParam('idtempobat', var_idtempobat_parent);
				ds_temobat.reload({
					scope   : this,
					callback: function(records, operation, success) {
						ds_temobat.each(function (rec) {
							var skdbrg			= rec.get("kdbrg");
							var snmbrg			= rec.get("nmbrg");
							var starif			= rec.get("hrgjual");
							var snmsatuan		= rec.get("nmsatuankcl");
							koder++;
							
							var orgaListRecord = new Ext.data.Record.create([
								{
									name: 'kditem',
									name: 'koder',
									name: 'nmitem',
									name: 'qty',
									name: 'tarif',
									name: 'tarif2',
									name: 'nmsatuan'
								}
							]);
							
							ds_nota2.add([
								new orgaListRecord({
									'kditem': skdbrg,
									'koder': koder,
									'nmitem': snmbrg,
									'qty': 1,
									'tarif': starif,
									'tarif2': starif,
									'nmsatuan': snmsatuan
								})
							]);
							var ttl = Ext.getCmp('tf.totalrf').getValue();
							var jml = parseFloat(ttl) + parseFloat(starif);
							var jmluangr = Ext.getCmp("tf.uangr").getValue();
							var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
							var sumtotal = jml + jmluangr;
							var sumtotaldisk = sumtotal - jmldiskon;
							Ext.getCmp('tf.totalrf').setValue(jml);
							Ext.getCmp('tf.total2').setValue(sumtotaldisk);
						});
					}
				});
				win_find_masterpaket_parent.close();
			}
			return true;
		}
	}
	
}