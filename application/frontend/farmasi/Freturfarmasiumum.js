function Freturfarmasiumum(){
	var pageSize = 18;
	var ds_returfarmasi = dm_returfarmasi();
	var ds_stsetuju = dm_stsetuju();
	var ds_sttransaksi = dm_sttransaksi();
	var ds_stharga = dm_stharga();
	var ds_stposisipasien = dm_stposisipasien();
	var ds_returfarmasidet = dm_returfarmasidet();
		ds_returfarmasidet.setBaseParam('noreturfarmasi','null');
		
	var row = '';
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_returfarmasi,
		displayInfo: true,
		displayMsg: 'Data Retur Farmasi Umum Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var vw_returfarmasiumum = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail kasir" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var grid_returfarmasiumum = new Ext.grid.GridPanel({
		id: 'grid_returfarmasiumum',
		store: ds_returfarmasi,
		view: vw_returfarmasiumum,
		autoScroll: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddReturfarmasiumum();
				Ext.getCmp('tf.carinoreturdet').setValue();
				Ext.getCmp('tf.reckdbrg').setValue();
				Ext.getCmp('bt.batal').disable();
				Ext.getCmp('btn_cetak').disable();
			}
		},{
			xtype: 'textfield',
			id:'tf.userid',
			width: 60,
			value: USERID,
			hidden: true,
			validator: function(){
				//ds_returbrgsupplier.setBaseParam('userid', Ext.getCmp('tf.userid').getValue());
				//ds_returbrgsupplier.reload();
			}
		}],
		autoHeight: true,
		//height: 530,
		columnLines: true,
		plugins: cari_data,
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. Retur<br>Farmasi Pasien Luar'),
			width: 126,
			dataIndex: 'noreturfarmasi',
			sortable: true,
			align:'center'
		},
		{
			header: headerGerid('Tgl Retur<br>Farmasi Pasien Luar'),
			width: 126,
			dataIndex: 'tglreturfarmasi',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Jam'),
			width: 53,
			dataIndex: 'jamreturfarmasi',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Status <br> Transaksi'),
			width: 66,
			dataIndex: 'nmsttransaksi',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Status <br> Persetujuan'),
			width: 100,
			dataIndex: 'nmstsetuju',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Penerima'),
			width: 150,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Biaya Adm'),
			width: 67,
			dataIndex: 'biayaadm',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
			hidden: true
		},{
			header: headerGerid('No. Nota'),
			width: 100,
			dataIndex: 'noreg',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('No. RM'),
			width: 100,
			dataIndex: 'norm',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Nama Pasien'),
			width: 200,
			dataIndex: 'atasnama',
			sortable: true,
		},{
			header: headerGerid('No. reg'),
			width: 100,
			dataIndex: 'noregistrasi',
			sortable: true,
			hidden: true,
		},/*{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditReturfarmasiumum(grid, rowIndex);
						var noreturfarmasi = RH.getCompValue('tf.noreturfarmasi', true);
						if(noreturfarmasi != ''){
							RH.setCompValue('tf.carinoreturdet', noreturfarmasi);
							Ext.getCmp('tf.reckdbrg').setValue(noreturfarmasi);
						}
						var setuju = RH.getCompValue('cb.stsetuju', true);
						if(setuju != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							//Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('cb.stsetuju').setReadOnly(true);
						}else{							
							Ext.getCmp('bt.batal').disable();
						}
						var sttransaksi = RH.getCompValue('cb.sttransaksi', true);
						if(sttransaksi != 1 ){
							Ext.getCmp('btn_tmbh').disable();
							//Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('bt.batal').disable();
						}
						//Ext.getCmp('btn.supplier').disable();
						Ext.getCmp('tf.biayaadm').setValue();
						fTotal();
						return;
                    }
                }]
        } ,{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteReturfarmasiumum(grid, rowIndex);
                    }
                }]
        },*/{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Cetak',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {						
						var record = ds_returfarmasi.getAt(rowIndex);
						var noregis = record.data['noregistrasi']
						if(noregis !=null){							
							//cetakRetfar(grid, rowIndex);
							Ext.MessageBox.alert('Informasi', 'Data tidak bisa di cetak..');
						}else if(noregis ==null){							
							cetakRetfarumum(grid, rowIndex);						
						}
                    }
                }]
        }],
		bbar: paging		
	});	
	       
	var form_bp_general = new Ext.Panel({
		id: 'form_bp_general',
		title: 'Daftar Retur Farmasi Pasien Luar', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_returfarmasiumum]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadreturfarmasiumum(){
		ds_returfarmasi.reload();
	}
	
	function fnAddReturfarmasiumum(){
		var grid = grid_returfarmasiumum;
		wEntryReturfarmasiumum(false, grid, null);	
	}
	
	function fnEditReturfarmasiumum(grid, record){
		var record = ds_returfarmasi.getAt(record);
		wEntryReturfarmasiumum(true, grid, record);		
	}
	
	function fnDeleteReturfarmasiumum(grid, record){
		var record = ds_returfarmasi.getAt(record);
		var cekstatus = record.data['idstsetuju'];
		if(cekstatus!=1){
			Ext.MessageBox.alert('Informasi','Data sudah tidak bisa dihapus..');
		}else{
			var url = BASE_URL + 'returfarmasi_controller/delete_returfarmasi';
			var params = new Object({
							noreturfarmasi	: record.data['noreturfarmasi']
						});
			RH.deleteGridRecord(url, params, grid );
		}
		return;
	}
	
	function cetakRetfarumum(grid, record){
		var record = ds_returfarmasi.getAt(record);
		var noreturfarmasi = record.data['noreturfarmasi'] 
		RH.ShowReport(BASE_URL + 'print/printnotareturfarmasi/nota_retfarmasiumum/' + noreturfarmasi);	
	}
	
	function fTotal(){
		ds_returfarmasidet.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				var adm = '';
				ds_returfarmasidet.each(function (rec) {sum += parseFloat(rec.get('subtotal')); });	
				ds_returfarmasi.each(function (rec) { 
					adm = rec.get('biayaadm');
				});					
				//var jmluangr = Ext.getCmp("tf.uangr").getValue();
				//var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
				//var sumtotal = sum + jmluangr;
				var totall = sum - adm;
				Ext.getCmp("tf.jumlah").setValue(sum);
				//Ext.getCmp("tf.total").setValue(adm);	
				Ext.getCmp("tf.total").setValue(totall);
				
			}
		});
		
		/* ds_returfarmasi.reload({
			scope   : this,
			callback: function(records, operation, success) {
				var adm = '';

				 ds_returfarmasi.each(function (rec) { 
						adm = rec.get('biayaadm');
					});		
				Ext.getCmp("tf.total").setValue(adm);
			}
		}); */
	}
	
	/**
WIN - FORM ENTRY/EDIT 
*/

	function wEntryReturfarmasiumum(isUpdate, grid, record){
		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.jamreturfarmasi"))
					RH.setCompValue("tf.jamreturfarmasi",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
				
		var paging_daftar_returfarmasiumum = new Ext.PagingToolbar({
			pageSize: pageSize,
			store: ds_returfarmasidet,
			displayInfo: true,
			displayMsg: 'Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var vw_daftar_returfarmasiumum = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_daftar_returfarmasiumum = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_returfarmasiumum',
			store: ds_returfarmasidet,
			view: vw_daftar_returfarmasiumum,
			tbar: [{
				text: 'Tambah',
				id: 'btn_tmbh',
				iconCls: 'silk-add',
				handler: function() {
					var noreg = Ext.getCmp('tf.nota').getValue();
					if (noreg !=''){
						//Ext.MessageBox.alert('Informasi', 'Pilih No. Nota');
						fncarikdbrg();
					}else if (noreg ==''){
						Ext.MessageBox.alert('Informasi', 'No. Registrasi belum di pilih.');
						//fncarinonota();
					}
				}
			},{
				xtype: 'compositefield',
				width: 515,
				items: [{
					xtype: 'label', text: 'Status Harga :', margins: '3 5 0 60',
				},{
					xtype: 'combo',
					id: 'cb.stharga',
					store: ds_stharga, 
					valueField: 'idstharga', displayField: 'nmstharga',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 100, allowBlank: false, editable: false, value: 1,
					listeners: {
						/* 'beforerender' : function(grid) {
							store = grid.getStore();
							var s = Ext.getCmp('cb.stharga').getValue();
							if(s != 2) {
								cm = grid.getColumnModel();
								cm.setHidden(0,true);
							}

						} */
						select: function(combo, records, eOpts){
							var h = Ext.getCmp('cb.stharga').getValue();
							if( h!= 2){
								var t = Ext.getCmp('tfgp.qtyterima').getValue() * Ext.getCmp('tfgp.qty').getValue();
								//records.set('subtotal',t);
								//alert(t);
							} else {
							
							}
							
							switch_harga(records.data["idstharga"]);
						}
						
					}
				},{
					xtype: 'label', text: 'Status Transaksi :', margins: '3 5 0 6px',
				},{
					xtype: 'combo',
					id: 'cb.sttransaksi',
					store: ds_sttransaksi, 
					valueField: 'idsttransaksi', displayField: 'nmsttransaksi',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 165, editable: false, value: 1,
					readOnly: true, style: 'opacity: 0.6'
				}
				/* {
					xtype: 'textfield',
					id: 'tf.sttransaksi',
					width: 165,
					//value: 1,
					readOnly: true,
					style: 'opacity: 0.6',
				} */]
			},{
				xtype: 'compositefield',
				width: 260,
				items: [{
					xtype: 'label', text: 'Status Persetujuan :', margins: '3 5 0 0',
				},{
					xtype: 'combo',
					id: 'cb.stsetuju',
					store: ds_stsetuju, 
					valueField: 'idstsetuju', displayField: 'nmstsetuju',
					triggerAction: 'all', forceSelection: true, submitValue: true, 
					mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
					width: 140, allowBlank: false, editable: false, value: 2,
					readOnly: true,
					style : 'opacity:0.6'
				}]
			},{
				xtype: 'textfield',
				id:'tf.carinoreturdet',
				width: 60,
				hidden: true,
				validator: function(){
					ds_returfarmasidet.setBaseParam('noreturfarmasi', Ext.getCmp('tf.carinoreturdet').getValue());
					ds_returfarmasidet.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.reckdbrg',
				width: 60,
				hidden: true,							
			},{
				xtype: 'textfield',
				id:'tf.stsetuju',
				width: 60,
				hidden: true,							
			},{
				xtype: 'combo',
				id: 'cb.stposisipasien',
				store: ds_stposisipasien, 
				valueField: 'idstposisipasien', displayField: 'nmstposisipasien',
				triggerAction: 'all', forceSelection: true, submitValue: true, 
				mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
				width: 105, editable: false,
				readOnly: true, style: 'opacity: 0.6', hidden: true
			}],
			autoScroll: true,
			height: 333, //autoHeight: true,
			columnLines: true,
			frame: true,
			clicksToEdit: 1,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('idnotadet'),
				width: 50,
				dataIndex: 'idnotadet',
				sortable: true,
				align:'center',
				hidden: true
			},{
				header: headerGerid('No. Nota'),
				width: 86,
				dataIndex: 'nonota',
				sortable: true,
				align:'center',
				hidden: true
			},
			{
				header: headerGerid('Tgl. Nota'),
				width: 67,
				dataIndex: 'tglnota',
				sortable: true,
				align:'center',
				renderer: Ext.util.Format.dateRenderer('d-m-Y'),
				hidden: true
			},{
				header: headerGerid('Kode Barang'),
				width: 77,
				dataIndex: 'kditem',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Nama Barang'),
				width: 230,
				dataIndex: 'nmbrg',
				sortable: true,
			},
			{
				header: headerGerid('Qty <br> Terima'),
				width: 55,
				dataIndex: 'qtyterima',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				editor: {
					xtype: 'numberfield',
					id: 'tfgp.qtyterima',
					disabled: true
				}
			},{
				header: headerGerid('qtysisa'),
				width: 55,
				dataIndex: 'qtysisa',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			},{
				header: headerGerid('Jml <br> Retur'),
				width: 55,
				dataIndex: 'jmlretur',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',				
			},{
				header: headerGerid('Qty <br> Sisa tam'),
				width: 55,
				dataIndex: 'tamqtysisa',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true				
			}/* ,{
				header: headerGerid('stoknowbagian'),
				width: 55,
				dataIndex: 'stoknowbagian',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				hidden: true
			} */,{
				header: headerGerid('Qty <br> Retur'),
				width: 55,
				dataIndex: 'qtyretur',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000.00', align:'right',
				editor: {
					xtype: 'numberfield',
					id: 'tfgp.qty',
					/* enableKeyEvents: true,
					listeners:{
						keyup:function(){
							var record = ds_returfarmasidet.getAt(row);
							var stoknowbagian = record.data.stoknowbagian;
							var qtyterima = record.data.qtyterima;
							var tamqtysisa = record.data.tamqtysisa;
							var qtyretur = Ext.getCmp('tfgp.qty').getValue();	
							
							if(qtyretur == stoknowbagian){}
							else if(qtyretur >= stoknowbagian){
								 Ext.MessageBox.alert('Informasi', 'Jumlah retur melebihi Stok..');
								 Ext.getCmp('tfgp.qty').setValue();
								 return;
							}
							var cekstsetuju = Ext.getCmp('cb.stsetuju').getValue();
							if(cekstsetuju != 1){
								if(tamqtysisa == qtyretur){}
								else if(qtyretur >= qtyterima){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Terima..');
									Ext.getCmp('tfgp.qty').setValue();
								}else if(tamqtysisa <= qtyretur){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Sisa..');
									Ext.getCmp('tfgp.qty').setValue();
								}
								return;
							}else{
								if(tamqtysisa == qtyretur){}
								else if(qtyretur >= qtyterima){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Terima..');
									Ext.getCmp('tfgp.qty').setValue();
								}
								return;
							}
						}
					} */
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							var cekqty = Ext.getCmp('tfgp.qty').getValue();
							if(cekqty !='0'){									
								jumlah();
							}else{								
								Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh "0"');
								Ext.getCmp('tfgp.qty').setValue('1');
								jumlah();
							}
							var record = ds_returfarmasidet.getAt(row);
							var qtyterima = record.data.qtyterima;
							var tamqtysisa = record.data.tamqtysisa;
							var qtyretur = Ext.getCmp('tfgp.qty').getValue();							
							var cekstsetuju = Ext.getCmp('cb.stsetuju').getValue();
							/* if(cekstsetuju != 1){
								if(tamqtysisa == qtyretur){}
								else if(qtyretur >= qtyterima){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Terima..');
									Ext.getCmp('tfgp.qty').setValue('1');
									jumlah();
								}else if(tamqtysisa <= qtyretur){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Sisa..');
									Ext.getCmp('tfgp.qty').setValue('1');
									jumlah();
								}
							}else{
								if(tamqtysisa == qtyretur){}
								else if(qtyretur >= qtyterima){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Terima..');
									Ext.getCmp('tfgp.qty').setValue('1');
									jumlah();
								}else if(tamqtysisa <= qtyretur){								
									Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Sisa..');
									Ext.getCmp('tfgp.qty').setValue('1');
									jumlah();
								}
							} */
							if(tamqtysisa == qtyretur){}
							else if(qtyretur >= qtyterima){								
								Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Terima..');
								Ext.getCmp('tfgp.qty').setValue('1');
								jumlah();
							}else if(tamqtysisa <= qtyretur){								
								Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh lebih dari Qty Sisa..');
								Ext.getCmp('tfgp.qty').setValue('1');
								jumlah();
							}
							
							var cekk = Ext.getCmp('tf.stsetuju').getValue();
							if(cekk == '' ||cekk == 1){}
							else if(cekk != 1 || cekk !=''){
								Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di Edit..');
							}
							
							var subtotal = record.data.tarif * Ext.getCmp('tfgp.qty').getValue();
							var disrp = subtotal *(record.data.diskon / 100);
							var subtotdis = subtotal - disrp; 
							record.set('subtotal',subtotdis);
							jumlah();							
							return;
						}
					}
				}
			},{
				header: headerGerid('Tarif'),
				width: 79,
				dataIndex: 'tarif',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right'
			},{
				header: headerGerid('Diskon'),
				width: 55,
				dataIndex: 'diskon',
				sortable: true,
				xtype: 'numbercolumn', align:'right'
			},{
				header: headerGerid('Subtotal'),
				width: 79,
				dataIndex: 'subtotal',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right'
			},
			{
				header: headerGerid('Catatan'),
				width: 132,
				dataIndex: 'catatan',
				sortable: true,
				editor: {
					xtype: 'textfield',
					id: 'tfgp.catatan', 
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							var cekk = Ext.getCmp('tf.stsetuju').getValue();
							if(cekk == '' ||cekk == 1){}
							else if(cekk != 1 || cekk !=''){
								Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di Edit..');
							}
						}
					}
				}
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							var cekk = Ext.getCmp('tf.stsetuju').getValue();
							if(cekk == '' ||cekk == 1){
								ds_returfarmasidet.removeAt(rowIndex);
							}else if(cekk != 1 || cekk !=''){
								Ext.MessageBox.alert('Informasi','Data sudah tidak bisa di Delete..');
							}
						}
					}]
			}],
			bbar: [
				{ xtype:'tbfill' },
				{
					xtype: 'fieldset',
					border: false,
					style: 'padding:0px; margin: 0px',
					width: 545,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'label', id: 'lb.totalrf', text: 'Total :', margins: '3 10 0 80',
						},{
							xtype: 'numericfield',
							id: 'tf.jumlah',
							value: 0,
							width: 100,
							readOnly:true,
							style : 'opacity:0.6',
							thousandSeparator:',',
							validator: function(){
								var cekj = Ext.getCmp('tf.jumlah').getValue();
								if(cekj!=0){
									htgDiskonRp();
									total();
								}else if(cekj==0){
									Ext.getCmp('tf.total').setValue('0');									
									Ext.getCmp('tf.biayaadm').setReadOnly(false);
									Ext.getCmp('tf.diskonrp').setReadOnly(false);
								}
							}
						}]
					},{
						xtype: 'compositefield',
						hidden: true,
						items: [{
							xtype: 'label', id: 'lb.diskon2', text: 'Biaya Administrasi :', margins: '3 10 0 0',
						},{
							xtype: 'numericfield',
							id: 'tf.biayaadm',
							value: 0,
							width: 50,
							readOnly: true,
							thousandSeparator:',',
							enableKeyEvents: true,
							listeners:{
								keyup:function(){
									htgDiskonPersen();
								}
							}
						},{
							xtype: 'label', id: 'lb.rp', text: '% ', margins: '3 10 0 0',
						},{
							xtype: 'numericfield',
							id: 'tf.diskonrp',
							value: 0,
							width: 100,
							readOnly: true,
							thousandSeparator:',',
							validator: function(){
								htgDiskonRp();
								total();
							},
							enableKeyEvents: true,
							listeners:{
								keyup:function(){
									var cekd = Ext.getCmp('tf.diskonrp').getValue();
									if(cekd==''){
										Ext.getCmp('tf.diskonrp').setValue('0');
									}
								}
							}
						}]
					},{
						xtype: 'compositefield',
						hidden: true,
						items: [{
							xtype: 'label', id: 'lb.total2', text: 'Total :', margins: '3 9 0 143',
						},{
							xtype: 'numericfield',
							id: 'tf.total',
							value: 0,
							readOnly:true,
							style : 'opacity:0.6',
							width: 100,
							thousandSeparator:','
						}]
					}]
				}
			],
			listeners	: {
				rowclick : function(grid, rowIndex, e){
					row = rowIndex;
					jumlah();
				}
			}
		});
		
		var winTitle = (isUpdate)?'Retur Farmasi Pasien Luar (Edit)':'Retur Farmasi Pasien Luar (Entry)';
		/* Ext.Ajax.request({
			url:BASE_URL + 'returbrgbagian_controller/getNmField',
			method:'POST',
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.idbagian").setValue(obj.nilai);
			}
		}); */
		
		var returfarmasiumum_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.returfarmasiumum',
			buttonAlign: 'left',
			labelWidth: 200, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 535, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					var stso = Ext.getCmp('cb.stsetuju').getValue();
					if (stso != 1){
						Ext.getCmp('df.tglreturfarmasi').setReadOnly(true);
						Ext.getCmp('cb.stsetuju').setReadOnly(true);
						Ext.getCmp('tf.stsetuju').setValue(stso);
						Ext.getCmp('tfgp.qty').setReadOnly(true);
						Ext.getCmp('tfgp.catatan').setReadOnly(true);
					}else{
						Ext.getCmp('df.tglreturfarmasi').setReadOnly(false);
					}
					simpanRetfar(); 
					return;                          
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', id: 'btn_cetak', style: 'marginLeft: 5px',
				handler: function() {
					cetak();					
				}
			},{
				text: 'Batal', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px', hidden: true,
				handler: function() {
					
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					Ext.getCmp('tf.carinoreturdet').setValue();
					ds_returfarmasi.reload();
					wReturfarmasiumum.close();
			}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 110, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.49, border: false, layout: 'form',					
					items: [{
						xtype: 'textfield',
						fieldLabel:'No. Retur Farmasi Luar',
						id:'tf.noreturfarmasi',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'compositefield',
						fieldLabel:'Tgl./Jam Retur Farmasi Luar',
						items:[{
							xtype: 'datefield',
							id: 'df.tglreturfarmasi',
							format: 'd-m-Y',
							value: new Date(),
							width: 120,
						},{
							xtype: 'label', id: 'lb.time', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.jamreturfarmasi',
							width: 60,
						}]
					},{
						xtype: 'textfield', fieldLabel:'Penerima',
						id: 'tf.penerima',
						readOnly: true, style : 'opacity:0.6',
						width: 205, 
						value: USERNAME,
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					items: [{
						xtype: 'fieldset',
						title: '',
						style : 'marginLeft: -105px;marginTop: -11px',
						width: 570,
						items: [{
							xtype: 'compositefield',
							fieldLabel:'No. Nota',
							items:[{
								xtype: 'textfield',
								id: 'tf.nota',
								emptyText: 'Pilih...',
								width: 120,
								readOnly: true,
								style : 'opacity:0.6',
							},
							{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn_data_noreg',
								width: 3,
								handler: function() {
									fncarinonota();
								}
							}]
						},{
							xtype: 'compositefield',
							fieldLabel:'Tgl./Jam Nota',
							items:[{
								xtype: 'datefield',
								id: 'df.tglnota',
								format: 'd-m-Y',
								//value: new Date(),
								disabled: true,
								width: 120,
							},{
								xtype: 'label', id: 'lb.timee', text: '/', margins: '3 5 0 5',
							},{ 	
								xtype: 'textfield',
								id: 'tf.jamnota',
								width: 60,
							},{ 	
								xtype: 'textfield',
								id: 'tf.noreg',
								width: 60,
								hidden: true
							}]
						},{
							xtype: 'compositefield',
							fieldLabel: 'No. RM',
							items:[{
								xtype: 'textfield',
								//fieldLabel: 'No. RM',
								id: 'tf.norm',
								width: 90,
								readOnly: true,
								style : 'opacity:0.6',
							},{
								xtype: 'label', id: 'lb.nrm', text: 'Nama Pasien :', margins: '3 10 0 5',
							},{
								xtype: 'textfield', //fieldLabel:'Nama Pasien',
								id: 'tf.nmpasien',
								readOnly: true, style : 'opacity:0.6',
								width: 150,
							}]
						}]
					}]
				}]
			},{
				xtype: 'fieldset',
				title: 'Daftar Barang Yang Diretur :',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 373,
				items: [grid_daftar_returfarmasiumum]
			}]
		});
			
		var wReturfarmasiumum = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [returfarmasiumum_form]
		});
		
	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setReturfarmasiumumForm(isUpdate, record);
		wReturfarmasiumum.show();

	/**
	FORM FUNCTIONS
	*/	
		function setReturfarmasiumumForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					RH.setCompValue('tf.noreturfarmasi', record.get('noreturfarmasi'));
					//RH.setCompValue('df.tglkeluar', record.get('tglreturfarmasi'));
					//RH.setCompValue('tf.jamkeluar', record.get('jamreturfarmasi'));
					RH.setCompValue('cb.sttransaksi', record.get('idsttransaksi'));
					RH.setCompValue('cb.stsetuju', record.get('idstsetuju'));
					RH.setCompValue('tf.stsetuju', record.get('idstsetuju'));
					RH.setCompValue('tf.nota', record.get('noreg'));
					RH.setCompValue('tf.norm', record.get('norm'));
					RH.setCompValue('tf.nmpasien', record.get('nmpasien'));
					RH.setCompValue('tf.diskonrp', record.get('biayaadm'));
					return;
				}
			}
		}
		
		function fnSaveReturfarmasiumum(){
			var idForm = 'frm.returfarmasiumum';
			var sUrl = BASE_URL +'returbrgbagian_controller/insert_update_returbrgsupplier';
			var sParams = new Object({
				pembelianbrg	:	RH.getCompValue('tf.pemakaian'),
				tgltutup		:	RH.getCompValue('df.pemakaian'),
				jamtutup		:	RH.getCompValue('tf.bagian'),
				idshifttutup	:	RH.getCompValue('tf.idsifttutup'),
				saldoakhir		:	RH.getCompValue('nf.saldoakhirinput'),
				selisih			:	RH.getCompValue('nf.selisih'),
				catatantutup	:	RH.getCompValue('ta.catatantutup'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'returbrgbagian_controller/insert_update_returbrgsupplier';
				msgSuccess = 'Tutup kasir berhasil';
				msgFail = 'Tutup kasir gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wReturfarmasiumum, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
		
		function simpanRetfar(){
			var qty = Ext.getCmp('tfgp.qty').getValue();
			if(qty !='0'){
				var cekknoreg = Ext.getCmp('tf.nota').getValue();
				if(cekknoreg != ""){
					var reckdbrg = Ext.getCmp('tf.reckdbrg').getValue();
					if(reckdbrg != ""){
						var recstsetuju = Ext.getCmp('cb.stsetuju').getValue();
						if(recstsetuju != 1){
							var arrreturfarmasi = [];
							for(var zx = 0; zx < ds_returfarmasidet.data.items.length; zx++){
								var record = ds_returfarmasidet.data.items[zx].data;
								zidnotadet 	= record.idnotadet;
								zqty 		= record.qtyretur;
								zcatatan 	= record.catatan;
								arrreturfarmasi[zx] = zidnotadet + '-' + zqty + '-' + zcatatan ;
							}
							Ext.Ajax.request({
								url: BASE_URL + 'returfarmasi_controller/insorupd_returfarmasi',
								params: {
									noreturfarmasi		:	RH.getCompValue('tf.noreturfarmasi'),
									tglreturfarmasi		:	RH.getCompValue('df.tglreturfarmasi'),
									jamreturfarmasi		:	RH.getCompValue('tf.jamreturfarmasi'),
									penerima			:	USERID,
									idsttransaksi		:	RH.getCompValue('cb.sttransaksi'),
									idstsetuju			:	RH.getCompValue('cb.stsetuju'),
									biayaadm			:	RH.getCompValue('tf.diskonrp'),
									stposisipasien		:	RH.getCompValue('cb.stposisipasien'),
									idstharga			:	RH.getCompValue('cb.stharga'),
									
									arrreturfarmasi : Ext.encode(arrreturfarmasi)
									
								},
								success: function(response){
									Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
									obj = Ext.util.JSON.decode(response.responseText);
									console.log(obj);
									Ext.getCmp("tf.noreturfarmasi").setValue(obj.noreturfarmasi);
									Ext.getCmp("tf.carinoreturdet").setValue(obj.noreturfarmasi);
									ds_returfarmasidet.setBaseParam('noreturfarmasi', Ext.getCmp("tf.carinoreturdet").getValue(obj.noreturfarmasi));
									ds_returfarmasi.reload();
									ds_returfarmasidet.reload();
									//Ext.getCmp('btn_tmbh').disable();
									//Ext.getCmp('btn_simpan').disable();
									Ext.getCmp('bt.batal').enable();
									Ext.getCmp('btn_cetak').enable();
									Ext.getCmp('btn_simpan').disable();
									Ext.getCmp('cb.stharga').disable();
									var idstsetuju = Ext.getCmp('cb.stsetuju').getValue();
										if(idstsetuju != 1){
											Ext.getCmp('cb.stsetuju').setReadOnly(true);
										}										
									var arruqty = [];
									for(var zx = 0; zx < ds_returfarmasidet.data.items.length; zx++){
										var record = ds_returfarmasidet.data.items[zx].data;
										uidnotadet 	= record.idnotadet;
										uidbagian = 11;
										ukdbrg = record.kditem;
										uqtyretur = record.qtyretur;
										arruqty[zx] = Ext.getCmp('tf.noreturfarmasi').getValue(obj.noreturfarmasi) + '-' + Ext.getCmp('tf.jamreturfarmasi').getValue() + '-' + Ext.getCmp('tf.noreg').getValue() + '-' + uidbagian + '-' + USERID + '-' + ukdbrg + '-' + uqtyretur + '-' + uidnotadet;
									}
									Ext.Ajax.request({
										url: BASE_URL + 'returfarmasi_controller/update_qty_retur',
										params: {											
											tglreturfarmasi		: RH.getCompValue('df.tglreturfarmasi'),
											arruqty 			: Ext.encode(arruqty)
											
										},
										success: function(response){								
											ds_returfarmasidet.reload();
										},							
										failure : function(){
											Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
										}
									});									
									return;
								}
							});
						}else{
							var arrreturfarmasi = [];
							for(var zx = 0; zx < ds_returfarmasidet.data.items.length; zx++){
								var record = ds_returfarmasidet.data.items[zx].data;
								zidnotadet 	= record.idnotadet;
								zqty 		= record.qtyretur;
								zcatatan 	= record.catatan;
								arrreturfarmasi[zx] = zidnotadet + '-' + zqty + '-' + zcatatan ;
							}
							Ext.Ajax.request({
								url: BASE_URL + 'returfarmasi_controller/insorupd_returfarmasi',
								params: {
									noreturfarmasi		:	RH.getCompValue('tf.noreturfarmasi'),
									tglreturfarmasi		:	RH.getCompValue('df.tglreturfarmasi'),
									jamreturfarmasi		:	RH.getCompValue('tf.jamreturfarmasi'),
									idsttransaksi		:	RH.getCompValue('cb.sttransaksi'),
									idstsetuju			:	RH.getCompValue('cb.stsetuju'),
									biayaadm			:	RH.getCompValue('tf.diskonrp'),
									stposisipasien		:	RH.getCompValue('cb.stposisipasien'),
									idstharga			:	RH.getCompValue('cb.stharga'),
									
									arrreturfarmasi : Ext.encode(arrreturfarmasi)
									
								},
								success: function(response){
									Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
									obj = Ext.util.JSON.decode(response.responseText);
									console.log(obj);
									Ext.getCmp("tf.noreturfarmasi").setValue(obj.noreturfarmasi);
									ds_returfarmasi.reload();
								},
								failure : function(){
									Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
								}
							});
						}
					}else{
						Ext.MessageBox.alert('Informasi','Isi data barang..');
					}
				}else{
					Ext.MessageBox.alert('Informasi','Isi data bagian terlebih dahulu..');
				}
			}else{
				Ext.MessageBox.alert('Informasi','Qty Retur tidak boleh "0"');
			}
		}
		
		function batalPengbrg(){
			Ext.getCmp('cb.sttransaksi').setValue('2');
			var arrretsupplier = [];
			for(var zx = 0; zx < ds_returfarmasidet.data.items.length; zx++){
				var record = ds_returfarmasidet.data.items[zx].data;
				zkdbrg = record.kdbrg;
				zqty = record.qtyretur;
				zcatatan = record.catatan;
				arrretsupplier[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'returbrgbagian_controller/insorupd_pengbrg',
				params: {
					nokeluarbrg		:	RH.getCompValue('tf.noreturfarmasi'),
					tglkeluar		:	RH.getCompValue('df.tglkeluar'),
					jamkeluar		:	RH.getCompValue('tf.jamkeluar'),
					idbagiandari	:	11,//RH.getCompValue('tf.idbagian'),
					idbagianuntuk	:	RH.getCompValue('tf.idbagian'),
					idsttransaksi	:	RH.getCompValue('cb.sttransaksi'),
					idstsetuju		:	RH.getCompValue('cb.stsetuju'),
					userid			:	USERID,
					penerima		:	RH.getCompValue('tf.nmpenerima'),
					keterangan		:	RH.getCompValue('ta.keterangan'),
					
					arrretsupplier : Ext.encode(arrretsupplier)
					
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi','Pemakaian barang dibatalkan..');
					obj = Ext.util.JSON.decode(response.responseText);
					console.log(obj);
					Ext.getCmp("tf.noreturfarmasi").setValue(obj.noreturfarmasi);
					Ext.getCmp("tf.carinoreturdet").setValue(obj.noreturfarmasi);
					ds_returfarmasidet.setBaseParam('noreturfarmasi', Ext.getCmp("tf.carinoreturdet").getValue(obj.noreturfarmasi));
					ds_returfarmasi.reload();
					ds_returfarmasidet.reload();
					//Ext.getCmp('btn_tmbh').disable();
					Ext.getCmp('btn_simpan').disable();
					Ext.getCmp('bt.batal').disable();
				}
			});
			
			var arruqty = [];
			for(var zx = 0; zx < ds_returfarmasidet.data.items.length; zx++){
				var record = ds_returfarmasidet.data.items[zx].data;
				ukdbrg = record.kdbrg;
				uqty = record.qtyretur;
				arruqty[zx] = Ext.getCmp('cb.bagianuntuk').getValue() + '-' + Ext.getCmp('tf.idbagian').getValue() + '-' + ukdbrg + '-' + uqty;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'returbrgbagian_controller/update_qty_batal',
				params: {								
					arruqty : Ext.encode(arruqty)
					
				},							
				failure : function(){
					Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
				}
			});
		}
		
		function cetak(){
			var noreturfarmasi = Ext.getCmp('tf.noreturfarmasi').getValue();
			RH.ShowReport(BASE_URL + 'print/printnotareturfarmasi/nota_retfarmasiumum/' + noreturfarmasi);
		}
		
		function jumlah(){
			var zzz = 0;
			for (var zxc = 0; zxc <ds_returfarmasidet.data.items.length; zxc++) {
				var record = ds_returfarmasidet.data.items[zxc].data;
				zzz += parseFloat(record.subtotal);
			}
			var jml = zzz;
			Ext.getCmp('tf.jumlah').setValue(jml);
		}
		
		function total(){
			var jum = Ext.getCmp('tf.jumlah').getValue();
			var disrp = Ext.getCmp('tf.diskonrp').getValue();
			var total = jum - disrp
			Ext.getCmp('tf.total').setValue(total);
		}
		
		function htgDiskonRp(){
			var diskpersen = (Ext.getCmp('tf.diskonrp').getValue() / Ext.getCmp('tf.jumlah').getValue()) * 100;
			Ext.getCmp('tf.biayaadm').setValue(diskpersen);
		}
		
		function htgDiskonPersen(){
			var diskrp = (Ext.getCmp('tf.biayaadm').getValue() / 100) * Ext.getCmp('tf.jumlah').getValue();
			Ext.getCmp('tf.diskonrp').setValue(diskrp);
		}
	}
	
	function fncarinonota(){
		var ds_carinonotadireturfarmasi = dm_carinonotadireturfarmasi();
		
		function keyToAddnokeluarbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
			
		var cm_pengbrg = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: 'No. Nota',
			width: 86,
			dataIndex: 'nonota',
			sortable: true,
			align:'center',
			renderer: keyToAddnokeluarbrg
		},
		{
			header: 'Tgl. Nota',
			width: 67,
			dataIndex: 'tglnota',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: 'No. RM',
			width: 85,
			dataIndex: 'norm',
			sortable: true,
		},{
			header: 'Nama Pasien',
			width: 250,
			dataIndex: 'atasnama',
			sortable: true,
		}
		]);
		
		var arr_cari = [['nonota', 'No. Nota'],['norm', 'No. RM'],['atasnama', 'Nama Pasien']];
	
		var ds_cari = new Ext.data.ArrayStore({
			fields: ['id', 'nama'],
			data : arr_cari 
		});
		
		function fnSearchgrid(){
			var idcombo, nmcombo;
			idcombo= Ext.getCmp('cb.search').getValue();
			nmcombo= Ext.getCmp('cek').getValue();
				ds_carinonotadireturfarmasi.setBaseParam('key',  '1');
				ds_carinonotadireturfarmasi.setBaseParam('id',  idcombo);
				ds_carinonotadireturfarmasi.setBaseParam('name',  nmcombo);
			ds_carinonotadireturfarmasi.load();
			Ext.getCmp('cek').focus();
		}
		
		var sm_pengbrg = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_pengbrg = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_pengbrg = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_carinonotadireturfarmasi,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_pengbrg = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_pengbrg = new Ext.grid.GridPanel({
			ds: ds_carinonotadireturfarmasi,
			cm: cm_pengbrg,
			sm: sm_pengbrg,
			view: vw_pengbrg,
			height: 460,
			width: 530,
			//plugins: cari_pengbrg,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 450,
				items: [{
					xtype: 'label', text: 'Search :', margins: '4 5 0 20',
				},{
					xtype: 'combo',
					store: ds_cari,
					id: 'cb.search',
					value: 'atasnama',
					triggerAction: 'all',
					editable: false,
					valueField: 'id',
					displayField: 'nama',
					forceSelection: true,
					submitValue: true,
					typeAhead: true,
					mode: 'local',
					emptyText:'Pilih...',
					selectOnFocus:true,
					width: 100,
					margins: '2 5 0 0',
					listeners: {
						select: function() {
							var cbsearchh = Ext.getCmp('cb.search').getValue();
								if(cbsearchh != ''){
									Ext.getCmp('cek').enable();
									Ext.getCmp('cek').focus();
								}
								return;
						}
					}
				},{
					xtype: 'textfield',
					id: 'cek',
					width: 200,
					margins: '2 5 0 0',
					//disabled: true,
					validator: function(){
						fnSearchgrid();
					}
				}]
			}],
			bbar: paging_pengbrg,
			listeners: {
				//rowdblclick: klik_cari_pengbrg
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_pengbrg = new Ext.Window({
			title: 'Cari No. Nota',
			modal: true,
			items: [grid_find_cari_pengbrg],
			listeners: {
				afterrender: a
			}
		}).show();
		
		function a(){
			Ext.getCmp('cek').focus();
		}
				
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_nota = record.data["nonota"];
					var var_cari_tglnota = record.data["tglnota"];
					var var_cari_jamnota = record.data["jamnota"];
					var var_cari_norm = record.data["norm"];
					var var_cari_nmpasien = record.data["atasnama"];
					
					Ext.getCmp("tf.nota").setValue(var_cari_nota);
					Ext.getCmp("df.tglnota").setValue(var_cari_tglnota);
					Ext.getCmp("tf.jamnota").setValue(var_cari_jamnota);
					Ext.getCmp("tf.norm").setValue(var_cari_norm);
					Ext.getCmp("tf.nmpasien").setValue(var_cari_nmpasien);
								win_find_cari_pengbrg.close();
				return true;
			}
			return true;
		}
	}
	
	function fncarikdbrg(){
		var ds_carikdbrgadiretfarmasiumum = dm_carikdbrgadiretfarmasiumum();		
		ds_carikdbrgadiretfarmasiumum.setBaseParam('nonota', Ext.getCmp('tf.nota').getValue());
		ds_carikdbrgadiretfarmasiumum.reload();
		
		function keyToAddnokeluarbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_pengbrg = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: headerGerid('id. Notadet'),
			width: 50,
			dataIndex: 'idnotadet',
			sortable: true,
			align:'center',
			hidden: true
		},{
			header: headerGerid('No. Nota'),
			width: 75,
			dataIndex: 'nonota',
			sortable: true,
			renderer: keyToAddnokeluarbrg
		},{
			header: headerGerid('Kode Barang'),
			width: 80,
			dataIndex: 'kditem',
			sortable: true,
		},
		{
			header: headerGerid('Nama Barang'),
			width: 235,
			dataIndex: 'nmbrg',
			sortable: true
		},{
			header: headerGerid('Qty <br> Terima'),
			width: 55,
			dataIndex: 'qtyterima',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Jml <br> Retur'),
			width: 55,
			dataIndex: 'jmlretur',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('qtysisa'),
			width: 55,
			dataIndex: 'qtysisa',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			hidden: true
		},{
			header: headerGerid('tamqtysisa'),
			width: 55,
			dataIndex: 'tamqtysisa',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
			hidden: true
		},{
			header: headerGerid('Harga <br> Jual'),
			width: 61,
			dataIndex: 'hrgjual',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Harga <br> Beli'),
			width: 61,
			dataIndex: 'hrgbeli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Diskon'),
			width: 55,
			dataIndex: 'diskon',
			sortable: true,
			xtype: 'numbercolumn',align:'right',
		}
		]);
		
		var sm_pengbrg = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_pengbrg = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_pengbrg = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_carikdbrgadiretfarmasiumum,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_pengbrg = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_pengbrg = new Ext.grid.GridPanel({
			ds: ds_carikdbrgadiretfarmasiumum,
			cm: cm_pengbrg,
			sm: sm_pengbrg,
			view: vw_pengbrg,
			height: 460,
			width: 715,
			//plugins: cari_pengbrg,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cek',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cek').getValue();
							ds_carikdbrgadiretfarmasiumum.setBaseParam('key',  '1');
							ds_carikdbrgadiretfarmasiumum.setBaseParam('id',  'nmbrg');
							ds_carikdbrgadiretfarmasiumum.setBaseParam('name',  nmcombo);
						ds_carikdbrgadiretfarmasiumum.load();
					}
				}]
			}],
			bbar: paging_pengbrg,
			listeners: {
				//rowdblclick: klik_cari_pengbrg
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_pengbrg = new Ext.Window({
			title: 'Cari Barang',
			modal: true,
			items: [grid_find_cari_pengbrg]
		}).show();
				
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			var qtyterima = record.data["qtyterima"];			
			var jmlretur = record.data["jmlretur"];
			
			//if(qtysisa == stoknowbagian || stoknowbagian != '0'){
				if(qtyterima != jmlretur){
					if (t.className == 'keyMasterDetail'){					
							var cek = true;
							var obj = ds_carikdbrgadiretfarmasiumum.getAt(rowIndex);
							var sidnotadet		= obj.get("idnotadet");
							var snonota			= obj.get("nonota");
							var skditem			= obj.get("kditem");
							var snmbrg    	 	= obj.get("nmbrg");
							var sqtyterima 		= obj.get("qtyterima");
							var sjmlretur 		= obj.get("jmlretur");
							var sqtysisa		= obj.get("qtysisa");
							var stamqtysisa		= obj.get("tamqtysisa");
							var shrgbeli		= obj.get("hrgbeli");			
							var subtot			= obj.get("hrgbeli") * 1;	
							var disrp			= subtot*(obj.get("diskon")/100);	
							var subtotd			= subtot - disrp;	

							var vhrgbeli		= obj.get("hrgbeli");
							var vhrgjual		= obj.get("hrgjual");
							var vdiskon			= obj.get("diskon");
														
							Ext.getCmp("tf.reckdbrg").setValue(snonota);
							Ext.getCmp('btn_simpan').enable();
							
							ds_returfarmasidet.each(function(rec){
								if(rec.get('nonota') == snonota && rec.get('kditem') == skditem) {
									Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
									cek = false;
								}
							});
							
							if(cek){
								var orgaListRecord = new Ext.data.Record.create([
									{
										name: 'idnotadet',
										name: 'nonota',
										name: 'kditem',
										name: 'nmbrg',
										name: 'qtyterima',
										name: 'jmlretur',
										name: 'qtysisa',
										name: 'tamqtysisa',
										name: 'qtyretur',
										name: 'tarif',
										name: 'subtotal',
										name: 'catatan',
										
										name: 'hrgbeli',
										name: 'hrgjual',
										name: 'diskon',
									}
								]);
								
								ds_returfarmasidet.add([
									new orgaListRecord({
										'idnotadet'		: sidnotadet,
										'nonota'		: snonota,
										'kditem'		: skditem,
										'nmbrg'			: snmbrg,
										'qtyterima'		: sqtyterima,
										'jmlretur'		: sjmlretur,
										'qtysisa'		: sqtysisa,
										'tamqtysisa'	: stamqtysisa,
										'qtyretur'		: 1,
										'tarif'			: shrgbeli,
										'subtotal'		: subtotd,
										'catatan'		: '-',
										
										'hrgbeli'		: vhrgbeli,
										'hrgjual'		: vhrgjual,
										'diskon'		: vdiskon,
									})
								]);
								
								sum = 0; 
								var diskonrp = '';
								ds_returfarmasidet.each(function (rec) {sum += parseFloat(rec.get('subtotal')); });	
								diskonrp = Ext.getCmp("tf.diskonrp").getValue();
								var totall = sum - diskonrp;
								var tamtotal = sum;
								Ext.getCmp("tf.jumlah").setValue(tamtotal);	
								Ext.getCmp("tf.total").setValue(totall);
								Ext.getCmp('btn_data_noreg').disable();
								win_find_cari_pengbrg.close();
							}
					}
					return true;				
				}else{
					Ext.MessageBox.alert('Informasi', 'Barang sudah habis diretur..');
				}
			/* }else{
				Ext.MessageBox.alert('Informasi', 'Stok kurang untuk melakukan retur..');
			} */
		}
	}
	
	function switch_harga(crecord) {
		var a=0;
		var total=0;
		ds_returfarmasidet.each(function(rec){
			var record = ds_returfarmasidet.getAt(a);
			if (crecord==1) {
				record.set('tarif',record.data.hrgbeli);
				var jum    = record.data.hrgbeli * record.data.qtyretur;
				var disrp  = jum*(record.data.diskon / 100);
				var totdis = jum - disrp;
				record.set('subtotal',totdis);
			} else if (crecord==2) {
				record.set('tarif',record.data.hrgjual);
				var jum    = record.data.hrgjual * record.data.qtyretur;
				var disrp  = jum*(record.data.diskon / 100);
				var totdis = jum - disrp;
				record.set('subtotal',totdis);
			}
			a=a+1	
			total = total + parseFloat(record.data.subtotal);
		});
		var tmptot = total;
		Ext.getCmp("tf.jumlah").setValue(tmptot);
	}
}
