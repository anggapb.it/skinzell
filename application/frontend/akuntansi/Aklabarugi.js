function Aklabarugi(){

  //automated calculate penyusutan
  Ext.Ajax.request({
    url: BASE_URL + 'hartatetap_controller/kalkulasi_penyusutan',
    params: {
      calculate:1
    },
    success: function(response){}
  });

  var pageSize = 50;
  var ds_tahun = dm_tahun();
  var ds_bulan = dm_bulan();
  var ds_pendapatan = dm_pendapatan();
  var ds_biaya = dm_biaya();
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
  
  var grid_pendapatan = new Ext.grid.GridPanel({
    id: 'grid_pendapatan',
    title: 'Pendapatan',
    store: ds_pendapatan,
    height: 450,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    columnLines: true,
    style:'margin-bottom:7px',
    bbar:['->',
    {
      xtype: 'numericfield',
      thousandSeparator:',', 
      id: 'total_pendapatan',
      width:140,
      value:'0',
    }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 110,
      dataIndex: 'kdakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nama Akun'),
      width: 210,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nominal'),
      width: 140,
      dataIndex: 'nominal',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }],
  });
  
  var grid_biaya = new Ext.grid.GridPanel({
    id: 'grid_biaya',
    title: 'Biaya',
    store: ds_biaya,
    height: 450,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    columnLines: true,
    style:'margin-bottom:7px',
    bbar:['->',
    {
      xtype: 'numericfield',
      thousandSeparator:',', 
      id: 'total_biaya',
      width:140,
      value:'0',
    }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 110,
      dataIndex: 'kdakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nama Akun'),
      width: 210,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nominal'),
      width: 140,
      dataIndex: 'nominal',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }],
  }); 

  var form_bp_general = new Ext.form.FormPanel({
    id: 'form_bp_general',
    title: 'Laporan Laba Rugi', iconCls:'silk-money',
    width: 900, Height: 1000,
    layout: {type: 'form', pack: 'center', align: 'center'},
    frame: true,
    autoScroll: true,
    items: [
    {
      xtype: 'fieldset',
      labelWidth: 60, labelAlign: 'right',
      items: [{
        xtype: 'compositefield',
        fieldLabel: 'Periode ',
        items:[{
        xtype: 'datefield',
        id: 'tglawal',
        value: new Date(),
        format: "d/m/Y",
        width: 100, 
        listeners:{
          select: function(field, newValue){
            //fnSearchgrid();
          },
          change : function(field, newValue){
            //fnSearchgrid();
          }
        }
      },
      {
        xtype: 'label', id: 'lb.lbt', text: 's/d', margins: '6 4 0 0',
      },
      {
        xtype: 'datefield',
        id: 'tglakhir',
        value: new Date(),
        format: "d/m/Y",
        width: 100, 
        listeners:{
          select: function(field, newValue){
            //fnSearchgrid();
          },
          change : function(field, newValue){
            //fnSearchgrid();
          }
        }
      },{
          xtype: 'button',
          text: 'Tampilkan',
          style : 'margin-left:5px;',
          id: 'btn.show_neraca',
          iconCls: 'silk-find',
          handler: function() {
            fnSearchgrid();
          }
        },
        /*{
          xtype: 'button',
          text: 'Cetak',
          style : 'margin-left:10px;',
          id: 'btn.cetak',
          iconCls: 'silk-printer',
          handler: function() {
            //fnSearchgrid();
          }
        },
        */
        {
          xtype: 'button',
          text: 'Cetak Excel',
          style : 'margin-left:10px;',
          id: 'btn.cetakexcel',
          iconCls: 'silk-printer',
          handler: function() {
            fncetakExcel();
          }
        }]
      }]
    },{
      layout: 'column',
      frame: true,
      items:[
      {
        id: 'colom_grid_left',
        style: 'margin-right:7px', //activa block
        layout: 'form', 
        columnWidth: 0.5,
        items: [grid_pendapatan]
      },{
        id: 'colom_grid_right', //pasiva block
        layout: 'form', 
        columnWidth: 0.5,
        items: [grid_biaya]
      }],
    }],
  });
  SET_PAGE_CONTENT(form_bp_general);
  
  function fnSearchgrid(){
    
    ds_pendapatan.setBaseParam('tglawal', Ext.getCmp('tglawal').getValue());
    ds_pendapatan.setBaseParam('tglakhir', Ext.getCmp('tglakhir').getValue());
    ds_pendapatan.load({
      scope   : this,
      callback: function(records, operation, success) {
        total = 0;
        ds_pendapatan.each(function (rec) { 
          total += parseFloat(rec.get('nominal')); 
        });

        Ext.getCmp("total_pendapatan").setValue(total);

      }
    });

    ds_biaya.setBaseParam('tglawal', Ext.getCmp('tglawal').getValue());
    ds_biaya.setBaseParam('tglakhir', Ext.getCmp('tglakhir').getValue());
    ds_biaya.load({
      scope   : this,
      callback: function(records, operation, success) {
        total = 0;
        ds_biaya.each(function (rec) { 
          total += parseFloat(rec.get('nominal')); 
        });

        Ext.getCmp("total_biaya").setValue(total);

      }
    });

  }

  function fncetakExcel(){
    var tglawal = Ext.getCmp('tglawal').getValue().format('Y-m-d');
    var tglakhir = Ext.getCmp('tglakhir').getValue().format('Y-m-d');

    RH.ShowReport(BASE_URL + 'labarugi_controller/lap_labarugi/' + tglawal + '/' + tglakhir);
  
  }
  
}
  
