/*
  jurnal Transaksi Deposit Pasien Rawat Inap
*/

function jurn_ri_deposit(){
  Ext.form.Field.prototype.msgTarget = 'side';
  
  var ds_list_transaksi     = dm_jurnpasien_transaksi_deposit();
  var ds_detail_transaksi   = dm_jurnpasien_transaksi_depositdet();
  var ds_jurnal             = dm_jurnpasien_transaksi_deposit_jurnaling();
      ds_jurnal.setBaseParam('nokuitansi','null');
  
  var cm_list_transaksi = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('No. Kuitansi'),
      width: 100,
      dataIndex: 'nokuitansi',
      align:'left',
      sortable: true,
      renderer: fnkeyShowDetailTransaksi
    },{
      header: headerGerid('Tgl<br>Kuitansi'),
      width: 80,
      dataIndex: 'tglkuitansi',
      align:'left',
      renderer: Ext.util.Format.dateRenderer('d/m/Y'),
      sortable: true,
    },{
      header: headerGerid('Atas Nama'),
      width: 160,
      dataIndex: 'atasnama',
      sortable: true,
      align:'left'
    },{
      header: headerGerid('Pembayaran'),
      width: 100,
      dataIndex: 'pembayaran',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Posting'),
      width: 80,
      dataIndex: 'status_posting',
      align:'left',
      sortable: true,
    }],
  });

  var cm_detail_transaksi = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Cara Bayar'),
      width: 100,
      dataIndex: 'nmcarabayar',
      align:'left',
    },{
      header: headerGerid('Nama Bank'),
      width: 110,
      dataIndex: 'nmbank',
      align:'left',
    },{
      header: headerGerid('No. Kartu'),
      width: 110,
      dataIndex: 'nokartu',
      align:'left',
    },{
      header: headerGerid('Jumlah'),
      width: 110,
      dataIndex: 'jumlah',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_jurnal = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 80,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 120,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 70,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 90,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 90,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
    
  var grid_list_transaksi = new Ext.grid.GridPanel({
    id: 'grid_list_transaksi',
    store: ds_list_transaksi,
    title: 'Data Kuitansi',
    autoSizeColumns: true,
    enableColumnResize: true,  
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_list_transaksi,
    frame: true,
    loadMask: true,
    height: 430,
    layout: 'anchor',
    bbar: [
      '->',
      {
        xtype: 'label', text: 'Total : ', style: 'margin-right:5px;',
      },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_list_transaksi',
        width:110,
        readOnly:true,
        value:'0',
        align:'right',
        decimalPrecision:0,

    }],
    tbar: [{
      xtype: 'label',  
      text: 'Periode : ', 
      style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'datefield',
      id: 'tgl_transaksi',
      value: new Date(),
      format: "d/m/Y",
      width: 100, 
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'label', 
      text: 's/d',style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'datefield',
      id: 'tgl_transaksi_akhir',
      value: new Date(),
      format: "d/m/Y",
      width: 100, 
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'textfield',
      id: 'pencarian',
      width: 100,
      style : 'margin-right:5px;margin-left:5px;',
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'button',
      text: 'Cari',
      style : 'margin-left:10px;',
      id: 'btn.show_transaksi',
      iconCls: 'silk-find',
      handler: function() {
        fnSearchTransaksi();
      }
    }],
    style: 'padding-bottom:10px',
    listeners: {
      cellclick: onClickListTransaksi
    }
  });

  var grid_detail_transaksi = new Ext.grid.GridPanel({
    id: 'grid_detail_transaksi',
    store: ds_detail_transaksi,
    title: 'Detail Kuitansi',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_transaksi,
    frame: true,
    loadMask: true,
    height: 265,
    layout: 'anchor',
    style: 'padding-bottom:10px',
    tbar: ['->',{
      xtype: 'button',
      text: 'Posting',
      id: 'btn.posting',
      iconCls: 'silk-save',
      width: 100,
      disabled: true,
      style: 'margin-left:5px',
      handler: function() {
        var nokuitansi_posting = Ext.getCmp("nokuitansi_posting").getValue();
        var tglkuitansi_posting = Ext.getCmp("tglkuitansi_posting").getValue();
        var atasnama = Ext.getCmp("tf.nama").getValue();
        var nominal_jurnal = Ext.getCmp("info.total_debit").getValue(); //debit
        var nominal_jurnal_kredit = Ext.getCmp("info.total_kredit").getValue(); //kredit            
        
        if(nokuitansi_posting == '' || tglkuitansi_posting == ''){
          Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
        }
        else if(nominal_jurnal != nominal_jurnal_kredit)
        {
          Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
        }
        else{
          var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
          var post_grid ='';
          var count= 1;
          var endpar = ';';
          
          grid_jurnal.getStore().each(function(rec){
            var rowData = rec.data; 
            if (count == grid_jurnal.getStore().getCount()) {
              endpar = ''
            }
            post_grid += rowData['idakun'] + '^' + 
                   rowData['kdakun'] + '^' + 
                   rowData['nmakun']  + '^' + 
                   rowData['noreff']  + '^' + 
                   rowData['debit'] + '^' + 
                   rowData['kredit']  + '^' +
                endpar;
                        
            count = count+1;
          });

          Ext.Ajax.request({
            url: BASE_URL + 'jurn_transaksi_pasien_controller/posting_transaksi_pasien_deposit',
            params: {
              nokuitansi_posting     : nokuitansi_posting,
              tglkuitansi_posting    : tglkuitansi_posting,
              atasnama               : atasnama,
              nominal_jurnal         : nominal_jurnal,
              post_grid              : post_grid,
              userid                 : USERID,
            },
            success: function(response){
              waitmsg.hide();

              obj = Ext.util.JSON.decode(response.responseText);
              if(obj.success === true){
                Ext.getCmp('btn.posting').disable();
              }else{
                Ext.getCmp('btn.posting').enable();  
              }

              Ext.MessageBox.alert('Informasi', obj.message);
              
              ds_list_transaksi.reload();
              ds_detail_transaksi.reload();
              ds_jurnal.reload();
            }
          });
         
        }

      }
    }],
    bbar: [
      '->',
      {
        xtype: 'label', text: 'Total : ', style: 'margin-right:5px;',
      },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_transaksi',
        width:110,
        readOnly:true,
        value:'0',
        align:'right',
        decimalPrecision:0,

    }],
  });

  var grid_jurnal = new Ext.grid.GridPanel({
    id: 'grid_jurnal',
    store: ds_jurnal,
    title: 'Detail Jurnal',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_jurnal,
    frame: true,
    loadMask: true,
    height: 265,
    layout: 'anchor',
    style: 'padding-bottom:10px',
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_debit',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
        decimalPrecision:0,

    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_kredit',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
        decimalPrecision:0,

    }],
  });

  var form_bp_general = new Ext.FormPanel({
    id: 'form_bp_general',
    title: 'Jurnal Khusus (Deposit Pasien RI)', 
    iconCls:'silk-money',
    width: 900, 
    autoScroll: true,
    layout: 'column',
    frame: true,
    items: [{
      id: 'colom_grid_left',
      style: 'margin-right:10px',
      layout: 'form', 
      columnWidth: 0.52,
      items: [
      // ============================== panel left side (transaksi & form trans) ===============================//
      {
        xtype: 'fieldset',
        border: false,
        style: 'padding:0px',
        id: 'panel_detail_transaksi',
        labelWidth: 115, labelAlign: 'right',
        items: [
          grid_list_transaksi,
          {
            layout: 'column',
            frame: true,
            labelWidth: 95, labelAlign: 'right',
            items: [{
              //* =============== LEFT FORM =============== *//
              layout: 'form',
              columnWidth: 0.5,
              items: [{
                xtype: 'textfield', 
                id: 'tf.no_jurnal',
                fieldLabel: 'No. Jurnal ',
                width: 160,
                readOnly: true,
              },{
                xtype: 'textfield', 
                id: 'tf.norm',
                fieldLabel: 'No. RM ',
                width: 160,
                readOnly: true,
              },{
                xtype: 'textfield', 
                id: 'tf.no_reg',
                fieldLabel: 'No. Reg ',
                width: 160,
                readOnly: true,
              },]
            },{
              //* =============== RIGHT FORM =============== *//
              
              layout: 'form',
              columnWidth: 0.5,
              items: [{
                xtype: 'textfield', 
                id: 'tf.nama',
                fieldLabel: 'Nama Pasien ',
                width: 160,
                readOnly: true,
              },{
                xtype: 'numericfield',
                thousandSeparator:',', 
                id: 'tf.total_bayar',
                fieldLabel: 'Total Bayar ',
                width: 160,
                readOnly: true,
                decimalPrecision:0,

              }],
            }],
          },
          {
            xtype: 'textfield',
            id: 'nokuitansi_posting',
            value: '',
            hidden: true,
          },{
            xtype: 'textfield',
            id: 'tglkuitansi_posting',
            value: '',
            hidden: true,
          }
        ]
      }]
    },{
      // ============================================ panel right side ================================================//
      id: 'colom_grid_right',
      layout: 'form',
      border: false,
      style: 'padding:0px',     
      columnWidth: 0.48,
      items: [
        grid_detail_transaksi,
        grid_jurnal,
      ]
    }]
  });
  SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
  
  function fnSearchTransaksi(){
    ds_list_transaksi.setBaseParam('tglkuitansi', Ext.getCmp('tgl_transaksi').getValue().format('Y-m-d'));
    ds_list_transaksi.setBaseParam('tglkuitansi_akhir', Ext.getCmp('tgl_transaksi_akhir').getValue().format('Y-m-d'));
    ds_list_transaksi.setBaseParam('pencarian', Ext.getCmp('pencarian').getValue());
    ds_list_transaksi.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total transaksi dalam periode terpilih
            total_trans = 0;
            ds_list_transaksi.each(function (rec) { 
              total_trans += parseFloat(rec.get('pembayaran')); 
            });
            Ext.getCmp("info.total_list_transaksi").setValue(total_trans);
          }
        });
  }

  function fnkeyShowDetailTransaksi(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }

  function onClickListTransaksi(grid, rowIndex, columnIndex, event) {
      var t = event.getTarget();
      if (t.className == 'keyMasterDetail')
      {
        var obj = ds_list_transaksi.getAt(rowIndex);
        var kdjurnal         = obj.get("kdjurnal");
        var nokuitansi       = obj.get("nokuitansi");
        var atasnama         = obj.get("atasnama");
        var tglkuitansi      = obj.get("tglkuitansi");
        var noreg            = obj.get("noreg");
        var norm             = obj.get("norm");
        var status_posting   = obj.get("status_posting");
        
        //tambahkan idperawat & tglkuitansi pada hidden field
        Ext.getCmp("nokuitansi_posting").setValue(nokuitansi);
        Ext.getCmp("tglkuitansi_posting").setValue(tglkuitansi);
        
        Ext.getCmp("tf.no_jurnal").setValue(kdjurnal);
        Ext.getCmp("tf.nama").setValue(atasnama);
        Ext.getCmp("tf.no_reg").setValue(noreg);
        Ext.getCmp("tf.norm").setValue(norm);

        //reload detail jurnal
        ds_jurnal.setBaseParam('nokuitansi', nokuitansi);
        ds_jurnal.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_jurnal.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.total_debit").setValue(total_debit);            
            Ext.getCmp("info.total_kredit").setValue(total_kredit);

          }
        });

        //reload detail kuitansi
        ds_detail_transaksi.setBaseParam('nokuitansi', nokuitansi);
        ds_detail_transaksi.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total transaksi
            total_bayar = 0;
            
            ds_detail_transaksi.each(function (rec) { 
              total_bayar += parseFloat(rec.get('jumlah')); 
            });
            
            Ext.getCmp("tf.total_bayar").setValue(total_bayar);
            Ext.getCmp("info.total_transaksi").setValue(total_bayar);

          }
        });
        
        //disable or enable button posting
        if(status_posting == 'belum')
        {
          Ext.getCmp('btn.posting').enable();
        
        }else if(status_posting == 'sudah'){
          Ext.getCmp('btn.posting').disable();
        
        }

      }
      return true;
  }
  
  
}