/*
  jurnal Transaksi Pasien Rawat Inap
*/

function jurn_pasien_ri(){
  Ext.form.Field.prototype.msgTarget = 'side';
  
  var ds_list_transaksi     = dm_jurnpasien_transaksi_ri();
  var ds_detail_transaksi   = dm_jurnpasien_transaksi_ridet();
  var ds_detail_nota        = dm_jurnpasien_transaksi_rjugd_notadet();
  var ds_jurnal             = dm_jurnpasien_ri_jurnaling();
      ds_jurnal.setBaseParam('nokuitansi','null');
  var ds_jurnal_persediaan  = dm_jurnpasien_rjugd_jurnaling_persediaan();
      ds_jurnal_persediaan.setBaseParam('nokuitansi','null');
  
  var cm_list_transaksi = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('No. Kuitansi'),
      width: 90,
      dataIndex: 'nokuitansi',
      align:'left',
      sortable: true,
      renderer: fnkeyShowDetailTransaksi
    },{
      header: headerGerid('Tgl<br>Kuitansi'),
      width: 70,
      dataIndex: 'tglkuitansi',
      align:'left',
      renderer: Ext.util.Format.dateRenderer('d/m/Y'),
      sortable: true,
    },{
      header: headerGerid('Atas Nama'),
      width: 140,
      dataIndex: 'atasnama',
      sortable: true,
      align:'left'
    },{
      header: headerGerid('Pembayaran'),
      width: 90,
      dataIndex: 'pembayaran',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Posting'),
      width: 60,
      dataIndex: 'kdjurnal',
      align:'left',
      sortable: true,
      renderer: fnkeyshowPostingStatus,
    }],
  });

  var cm_detail_transaksi = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Cara Bayar'),
      width: 100,
      dataIndex: 'nmcarabayar',
      align:'left',
    },{
      header: headerGerid('Nama Bank'),
      width: 110,
      dataIndex: 'nmbank',
      align:'left',
    },{
      header: headerGerid('No. Kartu'),
      width: 110,
      dataIndex: 'nokartu',
      align:'left',
    },{
      header: headerGerid('Jumlah'),
      width: 110,
      dataIndex: 'jumlah',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_detail_nota = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('No. Nota'),
      width: 90,
      dataIndex: 'nonota',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Kode Item'),
      width: 80,
      sortable: true,
      dataIndex: 'kditem',
      align:'left',
    },{
      header: headerGerid('Nama Item'),
      width: 100,
      sortable: true,
      dataIndex: 'nmitem',
      align:'left',
    },{
      header: headerGerid('Qty'),
      width: 40,
      sortable: true,
      dataIndex: 'qty',
      align:'left',
    },{
      header: headerGerid('Tagihan'),
      width: 70,
      sortable: true,
      dataIndex: 'tagihanitem',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Diskon'),
      width: 70,
      sortable: true,
      dataIndex: 'diskonitem',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Dijamin'),
      width: 70,
      sortable: true,
      dataIndex: 'dijamin',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Total<br/>Bayar'),
      width: 70,
      sortable: true,
      dataIndex: 'bayaritem',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_jurnal = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 60,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 140,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 65,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 90,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 90,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_jurnal_persediaan = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 60,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 140,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 65,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 90,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 90,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
    
  var grid_list_transaksi = new Ext.grid.GridPanel({
    id: 'grid_list_transaksi',
    store: ds_list_transaksi,
    title: 'Data Kuitansi',
    autoSizeColumns: true,
    enableColumnResize: true,  
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_list_transaksi,
    frame: true,
    loadMask: true,
    height: 220,
    layout: 'anchor',
    tbar: [{
      xtype: 'label',  
      text: 'Periode : ', 
      style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'datefield',
      id: 'tgl_transaksi',
      value: new Date(),
      format: "d/m/Y",
      width: 100, 
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'label', 
      text: 's/d',style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'datefield',
      id: 'tgl_transaksi_akhir',
      value: new Date(),
      format: "d/m/Y",
      width: 100, 
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'button',
      text: 'Cari',
      style : 'margin-left:5px;',
      id: 'btn.show_transaksi',
      iconCls: 'silk-find',
      handler: function() {
        fnSearchTransaksi();
      }
    }],
    style: 'padding-bottom:5px',
    listeners: {
      cellclick: onClickListTransaksi
    }
  });

  var grid_detail_transaksi = new Ext.grid.GridPanel({
    id: 'grid_detail_transaksi',
    store: ds_detail_transaksi,
    title: 'Detail Kuitansi',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_transaksi,
    frame: true,
    loadMask: true,
    height: 180,
    layout: 'anchor',
    style: 'padding-bottom:5px',
    tbar: ['->',{
      xtype: 'button',
      text: 'Posting',
      id: 'btn.posting',
      iconCls: 'silk-save',
      width: 100,
      disabled: true,
      style: 'margin-left:5px',
      handler: function() {
        var nokuitansi_posting = Ext.getCmp("nokuitansi_posting").getValue();
        var tglkuitansi_posting = Ext.getCmp("tglkuitansi_posting").getValue();
        var atasnama = Ext.getCmp("tf.nama").getValue();
        var nominal_jurnal = Ext.getCmp("info.total_debit").getValue(); //debit
        var nominal_jurnal_kredit = Ext.getCmp("info.total_kredit").getValue(); //kredit            
        var nominal_jurnal_persediaan = Ext.getCmp("info.debit_persediaan").getValue(); //debit
        var nominal_jurnal_persediaan_kredit = Ext.getCmp("info.kredit_persediaan").getValue(); //kredit

        if(nokuitansi_posting == '' || tglkuitansi_posting == ''){
          Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
        }
        else if(nominal_jurnal != nominal_jurnal_kredit || nominal_jurnal_persediaan != nominal_jurnal_persediaan_kredit)
        {
          Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
        }
        else{
          var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
          var post_grid ='';
          var post_grid_persediaan ='';
          var count= 1;
          var count2= 1;
          var endpar = ';';
          var endpar2 = ';';
          
          grid_jurnal.getStore().each(function(rec){
            var rowData = rec.data; 
            if (count == grid_jurnal.getStore().getCount()) {
              endpar = ''
            }
            post_grid += rowData['idakun'] + '^' + 
                   rowData['kdakun'] + '^' + 
                   rowData['nmakun']  + '^' + 
                   rowData['noreff']  + '^' + 
                   rowData['debit'] + '^' + 
                   rowData['kredit']  + '^' +
                endpar;
                        
            count = count+1;
          });

          grid_jurnal_persediaan.getStore().each(function(rec){
            var rowData = rec.data; 
            if (count2 == grid_jurnal_persediaan.getStore().getCount()) {
              endpar2 = ''
            }
            post_grid_persediaan += rowData['idakun'] + '^' + 
                   rowData['kdakun'] + '^' + 
                   rowData['nmakun']  + '^' + 
                   rowData['noreff']  + '^' + 
                   rowData['debit'] + '^' + 
                   rowData['kredit']  + '^' +
                endpar2;
                        
            count2 = count2+1;
          });

          Ext.Ajax.request({
            url: BASE_URL + 'jurn_transaksi_pasien_controller/posting_transaksi_pasien_ri',
            params: {
              nokuitansi_posting     : nokuitansi_posting,
              tglkuitansi_posting    : tglkuitansi_posting,
              atasnama               : atasnama,
              nominal_jurnal         : nominal_jurnal,
              post_grid              : post_grid,
              post_grid_persediaan   : post_grid_persediaan,
              userid                 : USERID,
            },
            success: function(response){
              waitmsg.hide();

              obj = Ext.util.JSON.decode(response.responseText);
              if(obj.success === true){
                Ext.getCmp('btn.posting').disable();
              }else{
                Ext.getCmp('btn.posting').enable();  
              }

              Ext.MessageBox.alert('Informasi', obj.message);
              
              ds_list_transaksi.reload();
              ds_detail_transaksi.reload();
              ds_jurnal.reload();
            }
          });
         
        }

      }
    }],
    bbar: [
      '->',
      {
        xtype: 'label', text: 'Total : ', style: 'margin-right:5px;',
      },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_transaksi',
        width:110,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });

  var grid_detail_nota = new Ext.grid.GridPanel({
    id: 'grid_detail_nota',
    store: ds_detail_nota,
    title: 'Detail Nota',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_nota,
    frame: true,
    loadMask: true,
    height: 185,
    layout: 'anchor',
    style: 'padding-bottom:5px',
    bbar: [
      '->',
      {
        xtype: 'label', text: 'Total : ', style: 'margin-right:5px;',
      },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_nota',
        width:75,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });

  var grid_jurnal = new Ext.grid.GridPanel({
    id: 'grid_jurnal',
    store: ds_jurnal,
    title: 'Jurnal Pendapatan',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_jurnal,
    frame: true,
    loadMask: true,
    height: 195,
    layout: 'anchor',
    style: 'padding-bottom:5px',
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_debit',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_kredit',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });

  var grid_jurnal_persediaan = new Ext.grid.GridPanel({
    id: 'grid_jurnal_persediaan',
    store: ds_jurnal_persediaan,
    title: 'Jurnal Persediaan',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_jurnal_persediaan,
    frame: true,
    loadMask: true,
    height: 150,
    layout: 'anchor',
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.debit_persediaan',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.kredit_persediaan',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });

  var form_bp_general = new Ext.FormPanel({
    id: 'form_bp_general',
    title: 'Jurnal Khusus (Transaksi Pasien Rawat Inap)', 
    iconCls:'silk-money',
    width: 900, 
    autoScroll: true,
    layout: 'column',
    frame: true,
    items: [{
      id: 'colom_grid_left',
      style: 'margin-right:5px',
      layout: 'form', 
      columnWidth: 0.55,
      items: [
      // ============================== panel left side (transaksi & form trans) ===============================//
      {
        xtype: 'fieldset',
        border: false,
        style: 'padding:0px',
        id: 'panel_detail_transaksi',
        items: [
          grid_list_transaksi, 
          grid_detail_nota,
          {
            layout: 'column',
            frame: true,
            labelWidth: 105, labelAlign: 'right',
            items: [{
              //* =============== LEFT FORM =============== *//
              layout: 'form',
              columnWidth: 0.33,
              labelWidth: 80,
              items: [{
                xtype: 'textfield', 
                id: 'tf.no_jurnal',
                fieldLabel: 'No. Jurnal ',
                width: 110,
                readOnly: true,
              },{
                xtype: 'textfield', 
                id: 'tf.norm',
                fieldLabel: 'No. RM ',
                width: 110,
                readOnly: true,
              },{
                xtype: 'textfield', 
                id: 'tf.no_reg',
                fieldLabel: 'No. Reg ',
                width: 110,
                readOnly: true,
              },{
                xtype: 'textfield', 
                id: 'tf.nama',
                fieldLabel: 'Nama Pasien ',
                width: 110,
                readOnly: true,
              },]
            },{
              //* =============== MID FORM =============== *//
              layout: 'form',
              columnWidth: 0.31,
              labelWidth: 90, labelAlign: 'right',
              items: [{
                xtype: 'numericfield',
                thousandSeparator:',', 
                id: 'tf.total_transaksi',
                fieldLabel: 'Total Tagihan ',
                width: 90,
                readOnly: true,
              },{
                xtype: 'numericfield',
                thousandSeparator:',', 
                id: 'tf.uangr',
                fieldLabel: 'Uang Racik ',
                width: 90,
                readOnly: true,
              }],
            },{
              //* =============== Right FORM =============== *//
              layout: 'form',
              columnWidth: 0.35,
              labelWidth: 110, labelAlign: 'right',
              items: [{
                xtype: 'numericfield',
                thousandSeparator:',',
                id: 'tf.diskon',
                fieldLabel: 'Diskon Pelayanan ',
                width: 90,
                readOnly: true,
              },{
                xtype: 'numericfield',
                thousandSeparator:',',
                id: 'tf.diskon_obat',
                fieldLabel: 'Diskon Obat ',
                width: 90,
                readOnly: true,
              },{
                xtype: 'numericfield',
                thousandSeparator:',', 
                id: 'tf.dijamin',
                fieldLabel: 'Dijamin ',
                width: 90,
                readOnly: true,
              },{
                xtype: 'numericfield',
                thousandSeparator:',', 
                id: 'tf.total_bayar',
                fieldLabel: 'Total Bayar ',
                width: 90,
                readOnly: true,
              }],
            }],
          },
          {
            xtype: 'textfield',
            id: 'nokuitansi_posting',
            value: '',
            hidden: true,
          },{
            xtype: 'textfield',
            id: 'tglkuitansi_posting',
            value: '',
            hidden: true,
          },{
            xtype: 'textfield',
            id: 'jtransaksi_posting',
            value: '',
            hidden: true,
          }
        ]
      }]
    },{
      // ============================================ panel right side ================================================//
      id: 'colom_grid_right',
      layout: 'form',
      border: false,
      style: 'padding:0px',     
      columnWidth: 0.45,
      items: [
        grid_detail_transaksi,
        grid_jurnal,
        grid_jurnal_persediaan,
      ]
    }]
  });
  SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
  
  function fnSearchTransaksi(){
    ds_list_transaksi.setBaseParam('tglkuitansi', Ext.getCmp('tgl_transaksi').getValue().format('Y-m-d'));
    ds_list_transaksi.setBaseParam('tglkuitansi_akhir', Ext.getCmp('tgl_transaksi_akhir').getValue().format('Y-m-d'));
    ds_list_transaksi.load();
  }

  function fnkeyShowDetailTransaksi(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }

  function fnkeyshowPostingStatus(value){
   Ext.QuickTips.init();
    if(value == '-'){
      return 'belum';
    }else{
      return 'sudah';
    }
  }
  
  function onClickListTransaksi(grid, rowIndex, columnIndex, event) {
      var t = event.getTarget();
      if (t.className == 'keyMasterDetail')
      {
        var obj = ds_list_transaksi.getAt(rowIndex);
        var kdjurnal         = obj.get("kdjurnal");
        var nokuitansi       = obj.get("nokuitansi");
        var atasnama         = obj.get("atasnama");
        var tglkuitansi      = obj.get("tglkuitansi");
        var status_posting   = (kdjurnal == '-') ? 0 : 1;
        var jtransaksi       = obj.get("idjnskuitansi");

        //tambahkan idperawat & tglkuitansi pada hidden field
        Ext.getCmp("nokuitansi_posting").setValue(nokuitansi);
        Ext.getCmp("tglkuitansi_posting").setValue(tglkuitansi);
        Ext.getCmp("jtransaksi_posting").setValue(jtransaksi);
        
        Ext.getCmp("tf.no_jurnal").setValue(kdjurnal);
        Ext.getCmp("tf.nama").setValue(atasnama);

        //kalkulasi transaksi
        Ext.Ajax.request({
          url: BASE_URL + 'jurn_transaksi_pasien_controller/kalkulasi_transaksi_rjugd',
          params: {
            nokuitansi     : nokuitansi,
          },
          success: function(response){
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp('tf.uangr').setValue(obj.total_uangr);
            Ext.getCmp('tf.total_transaksi').setValue(obj.total_tagihan);
            Ext.getCmp('tf.diskon').setValue(obj.diskon_pelayanan);
            Ext.getCmp('tf.diskon_obat').setValue(obj.diskon_obat);
            Ext.getCmp('tf.dijamin').setValue(obj.dijamin);
            Ext.getCmp('tf.total_bayar').setValue(obj.total_bayar);
          }
        });

        //reload detail jurnal
        ds_jurnal.setBaseParam('nokuitansi', nokuitansi);
        ds_jurnal.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_jurnal.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.total_debit").setValue(total_debit);            
            Ext.getCmp("info.total_kredit").setValue(total_kredit);

          }
        });


        //reload detail jurnal persediaan
        ds_jurnal_persediaan.setBaseParam('nokuitansi', nokuitansi);
        ds_jurnal_persediaan.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_jurnal_persediaan.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.debit_persediaan").setValue(total_debit);            
            Ext.getCmp("info.kredit_persediaan").setValue(total_kredit);
          }
        });

        //reload detail kuitansi
        ds_detail_transaksi.setBaseParam('nokuitansi', nokuitansi);
        ds_detail_transaksi.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total transaksi
            total_bayar = 0;
            
            ds_detail_transaksi.each(function (rec) { 
              total_bayar += parseFloat(rec.get('jumlah')); 
            });
            
            Ext.getCmp("info.total_transaksi").setValue(total_bayar);

          }
        });

        ds_detail_nota.setBaseParam('nokuitansi', nokuitansi);
        ds_detail_nota.load({
          scope   : this,
          callback: function(records, operation, success) {
            noreg = '';
            norm = '';
            total_nota = 0;
            ds_detail_nota.each(function (rec) { 
              noreg = rec.get('noreg');
              norm = rec.get('norm');
              total_nota += parseFloat(rec.get('tagihanitem'));
            });
            
            Ext.getCmp("info.total_nota").setValue(total_nota);
            Ext.getCmp("tf.no_reg").setValue(noreg);
            Ext.getCmp("tf.norm").setValue(norm);        
          }
        });
        
        //disable or enable button posting
        if(status_posting == 0)
        {
          Ext.getCmp('btn.posting').enable();
        
        }else if(status_posting == 1){
          Ext.getCmp('btn.posting').disable();
        
        }

      }
      return true;
  }
  
  
}