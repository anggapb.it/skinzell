/*
  jurnal Transaksi Pasien
*/

function jurn_pasien_rj_ugd(){
  Ext.form.Field.prototype.msgTarget = 'side';
  
  var ds_list_transaksi            = dm_jurnpasien_transaksi_rjugd();
  var ds_detail_transaksi          = dm_jurnpasien_transaksi_rjugddet();
  var ds_detail_nota               = dm_jurnpasien_transaksi_rjugd_notadet();
  var ds_jurnal             = dm_jurnsupplier_beli_jurnaling();
  var ds_jurnal_biaya       = dm_jurnsupplier_beli_jurnaling();
  var ds_jurnal_pendapatan  = dm_jurnsupplier_beli_jurnaling();
  var ds_jurnal_persediaan  = dm_jurnsupplier_beli_jurnaling();
      ds_jurnal.setBaseParam('nokuitansi','null');
      ds_jurnal_biaya.setBaseParam('nokuitansi','null');
      ds_jurnal_pendapatan.setBaseParam('nokuitansi','null');
      ds_jurnal_persediaan.setBaseParam('nokuitansi','null');
  
  var cm_list_transaksi = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('No. Kuitansi'),
      width: 80,
      dataIndex: 'nokuitansi',
      align:'left',
      sortable: true,
      renderer: fnkeyShowDetailTransaksi
    },{
      header: headerGerid('Tgl<br>Kuitansi'),
      width: 70,
      dataIndex: 'tglkuitansi',
      align:'left',
      renderer: Ext.util.Format.dateRenderer('d/m/Y'),
      sortable: true,
    },{
      header: headerGerid('Atas Nama'),
      width: 140,
      dataIndex: 'atasnama',
      sortable: true,
      align:'left'
    },{
      header: headerGerid('Jenis<br>Transaksi'),
      width: 80,
      dataIndex: 'idjnskuitansi',
      sortable: true,
      align:'left',
      renderer: fnGetJnsKuitansi
    },{
      header: headerGerid('Pembayaran'),
      width: 90,
      dataIndex: 'pembayaran',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Posting'),
      width: 60,
      dataIndex: 'kdjurnal',
      align:'left',
      sortable: true,
      renderer: fnkeyshowPostingStatus,
    }],
  });

  var cm_detail_transaksi = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Cara Bayar'),
      width: 110,
      dataIndex: 'nmcarabayar',
      align:'left',
    },{
      header: headerGerid('Nama Bank'),
      width: 110,
      dataIndex: 'nmbank',
      align:'left',
    },{
      header: headerGerid('No. Kartu'),
      width: 110,
      dataIndex: 'nokartu',
      align:'left',
    },{
      header: headerGerid('Jumlah'),
      width: 110,
      dataIndex: 'jumlah',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_detail_nota = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('No. Nota'),
      width: 90,
      dataIndex: 'nonota',
      align:'left',
    },{
      header: headerGerid('Kode Item'),
      width: 80,
      dataIndex: 'kditem',
      align:'left',
    },{
      header: headerGerid('Nama Item'),
      width: 100,
      dataIndex: 'nmitem',
      align:'left',
    },{
      header: headerGerid('Qty'),
      width: 40,
      dataIndex: 'qty',
      align:'left',
    },{
      header: headerGerid('Tagihan'),
      width: 75,
      dataIndex: 'tagihanitem',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Diskon'),
      width: 75,
      dataIndex: 'diskonitem',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Total<br/>Bayar'),
      width: 75,
      dataIndex: 'bayaritem',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_jurnal_pendapatan = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 80,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 120,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 70,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 90,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 90,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_jurnal_biaya = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 80,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 120,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 70,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 90,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 90,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_jurnal_persediaan = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 80,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 120,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 70,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 90,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 90,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
    
  var grid_list_transaksi = new Ext.grid.GridPanel({
    id: 'grid_list_transaksi',
    store: ds_list_transaksi,
    title: 'Data Kuitansi',
    autoSizeColumns: true,
    enableColumnResize: true,  
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_list_transaksi,
    frame: true,
    loadMask: true,
    height: 280,
    layout: 'anchor',
    tbar: [{
      xtype: 'label',  
      text: 'Periode : ', 
      style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'datefield',
      id: 'tgl_transaksi',
      value: new Date(),
      format: "d/m/Y",
      width: 100, 
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'label', 
      text: 's/d',style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'datefield',
      id: 'tgl_transaksi_akhir',
      value: new Date(),
      format: "d/m/Y",
      width: 100, 
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'button',
      text: 'Cari',
      style : 'margin-left:10px;',
      id: 'btn.show_transaksi',
      iconCls: 'silk-find',
      handler: function() {
        fnSearchTransaksi();
      }
    }],
    style: 'padding-bottom:10px',
    listeners: {
      cellclick: onClickListTransaksi
    }
  });

  var grid_detail_transaksi = new Ext.grid.GridPanel({
    id: 'grid_detail_transaksi',
    store: ds_detail_transaksi,
    title: 'Detail Kuitansi',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_transaksi,
    frame: true,
    loadMask: true,
    height: 180,
    layout: 'anchor',
    style: 'padding-bottom:10px',
    tbar: ['->',{
      xtype: 'button',
      text: 'Posting',
      id: 'btn.posting',
      iconCls: 'silk-save',
      width: 100,
      disabled: true,
      style: 'margin-left:5px',
      handler: function() {
        var nopo_posting = Ext.getCmp("nopo_posting").getValue();
        var tglpo_posting = Ext.getCmp("tglpo_posting").getValue();
        var nmsupplier = Ext.getCmp("tf.nama_supplier").getValue();
        var nominal_jurnal = Ext.getCmp("info.total_debit").getValue(); //debit
        var nominal_jurnal_kredit = Ext.getCmp("info.total_kredit").getValue(); //kredit            
        
        if(nopo_posting == '' || tglpo_posting == ''){
          Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
        }
        else if(nominal_jurnal != nominal_jurnal_kredit)
        {
          Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
        }
        else{
          var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
          var post_grid ='';
          var count= 1;
          var endpar = ';';
          
          grid_jurnal.getStore().each(function(rec){ // ambil seluruh grid prodi
            var rowData = rec.data; 
            if (count == grid_jurnal.getStore().getCount()) {
              endpar = ''
            }
            post_grid += rowData['idakun'] + '^' + 
                   rowData['kdakun'] + '^' + 
                   rowData['nmakun']  + '^' + 
                   rowData['noreff']  + '^' + 
                   rowData['debit'] + '^' + 
                   rowData['kredit']  + '^' +
                endpar;
                        
            count = count+1;
          });

          Ext.Ajax.request({
            url: BASE_URL + 'jurn_transaksi_supplier_controller/posting_jurnal_supplier_beli',
            params: {
              nopo_posting    : nopo_posting,
              tglpo_posting   : tglpo_posting,
              nmsupplier      : nmsupplier,
              nominal_jurnal  : nominal_jurnal,
              post_grid       : post_grid,
              userid          : USERID,
            },
            success: function(response){
              waitmsg.hide();

              obj = Ext.util.JSON.decode(response.responseText);
              if(obj.success === true){
                Ext.getCmp('btn.posting').disable();
              }else{
                Ext.getCmp('btn.posting').enable();  
              }

              Ext.MessageBox.alert('Informasi', obj.message);
              
              ds_list_transaksi.reload();
              ds_detail_transaksi.reload();
              ds_jurnal.reload();
            }
          });
         
        }

      }
    }],
    bbar: [
      '->',
      {
        xtype: 'label', text: 'Total : ', style: 'margin-right:5px;',
      },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_transaksi',
        width:110,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });

  var grid_detail_nota = new Ext.grid.GridPanel({
    id: 'grid_detail_nota',
    store: ds_detail_nota,
    title: 'Detail Nota',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_nota,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:10px',
    bbar: [
      '->',
      {
        xtype: 'label', text: 'Total : ', style: 'margin-right:5px;',
      },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_nota',
        width:75,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });

  var grid_jurnal_pendapatan = new Ext.grid.GridPanel({
    id: 'grid_jurnal_pendapatan',
    store: ds_jurnal_pendapatan,
    title: 'Jurnal Pendapatan',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_jurnal_pendapatan,
    frame: true,
    loadMask: true,
    height: 180,
    layout: 'anchor',
    style: 'padding-bottom:10px',
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.debit_pendapatan',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.kredit_pendapatan',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });

  var grid_jurnal_biaya = new Ext.grid.GridPanel({
    id: 'grid_jurnal_biaya',
    store: ds_jurnal_biaya,
    title: 'Jurnal Biaya',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_jurnal_biaya,
    frame: true,
    loadMask: true,
    height: 180,
    layout: 'anchor',
    style: 'padding-bottom:10px',
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.debit_biaya',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.kredit_biaya',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });

  var grid_jurnal_persediaan = new Ext.grid.GridPanel({
    id: 'grid_jurnal_persediaan',
    store: ds_jurnal_persediaan,
    title: 'Jurnal Persediaan',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_jurnal_persediaan,
    frame: true,
    loadMask: true,
    height: 180,
    layout: 'anchor',
    style: 'padding-bottom:10px',
    bbar: [
      '->',
    {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.debit_persediaan',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.kredit_persediaan',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });
       
  var form_bp_general = new Ext.FormPanel({
    id: 'form_bp_general',
    title: 'Jurnal Khusus (Transaksi Pasien Rawat Jalan & UGD)', 
    iconCls:'silk-money',
    width: 900, 
    autoScroll: true,
    layout: 'column',
    frame: true,
    items: [{
      id: 'colom_grid_left',
      style: 'margin-right:10px',
      layout: 'form', 
      columnWidth: 0.56,
      items: [
      // ============================== panel left side (transaksi & form trans) ===============================//
      {
        xtype: 'fieldset',
        border: false,
        style: 'padding:0px',
        id: 'panel_detail_transaksi',
        labelWidth: 115, labelAlign: 'right',
        items: [
          grid_list_transaksi, 
          grid_detail_nota,
        {
          layout: 'column',
          frame: true,
          items: [{
            //* =============== LEFT FORM =============== *//
            layout: 'form',
            columnWidth: 0.5,
            items: [{
              xtype: 'textfield', 
              id: 'tf.no_jurnal',
              fieldLabel: 'No. Jurnal ',
              width: 170,
              readOnly: true,
            },{
              xtype: 'compositefield',
              fieldLabel: 'No. RM / No. Reg. ',
              items: [{
                xtype: 'textfield',
                id: 'tf.norm',
                width: 54,
                readOnly: true,
              },{
                xtype: 'label',
                text: ' / ', margins: '3 5 0 2', //top right bottom left
              },{
                xtype: 'textfield',
                id: 'tf.no_reg',
                width: 100,
                readOnly: true,
              }]
            },{
              xtype: 'textfield', 
              id: 'tf.nama',
              fieldLabel: 'Nama Pasien ',
              width: 170,
              readOnly: true,
            },{
              xtype: 'textfield', 
              id: 'tf.penjamin',
              fieldLabel: 'Penjamin ',
              width: 170,
              readOnly: true,
            },]
          },{
            //* =============== RIGHT FORM =============== *//
            
            layout: 'form',
            columnWidth: 0.5,
            items: [{
              xtype: 'numericfield',
              thousandSeparator:',', 
              id: 'tf.total_transaksi',
              fieldLabel: 'Total Transaksi ',
              width: 170,
              readOnly: true,
            },{
              xtype: 'numericfield',
              thousandSeparator:',',
              id: 'tf.diskon',
              fieldLabel: 'Diskon (-) ',
              width: 170,
              readOnly: true,
            },{
              xtype: 'numericfield',
              thousandSeparator:',', 
              id: 'tf.deposit',
              fieldLabel: 'Deposit (-) ',
              width: 170,
              readOnly: true,
            },{
              xtype: 'numericfield',
              thousandSeparator:',', 
              id: 'tf.dijamin',
              fieldLabel: 'Dijamin (-) ',
              width: 170,
              readOnly: true,
            },{
              xtype: 'numericfield',
              thousandSeparator:',', 
              id: 'tf.total_bayar',
              fieldLabel: 'Total Bayar (-) ',
              width: 170,
              readOnly: true,
            }],
          }],
        },
        {
          xtype: 'textfield',
          id: 'nopo_posting',
          value: '',
          hidden: true,
        },{
          xtype: 'textfield',
          id: 'tglpo_posting',
          value: '',
          hidden: true,
        }]
      }]
    },{
      // ============================================ panel right side ================================================//
      id: 'colom_grid_right',
      layout: 'form',
      border: false,
      style: 'padding:0px',     
      columnWidth: 0.44,
      items: [
        grid_detail_transaksi,
        grid_jurnal_pendapatan,
        grid_jurnal_biaya,
        grid_jurnal_persediaan,
      ]
    }]
  });
  SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
  
  function fnSearchTransaksi(){
    ds_list_transaksi.setBaseParam('tglkuitansi', Ext.getCmp('tgl_transaksi').getValue().format('Y-m-d'));
    ds_list_transaksi.setBaseParam('tglkuitansi_akhir', Ext.getCmp('tgl_transaksi_akhir').getValue().format('Y-m-d'));
    ds_list_transaksi.load();
  }

  function fnkeyShowDetailTransaksi(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }

  function fnkeyshowPostingStatus(value){
   Ext.QuickTips.init();
    if(value == '-'){
      return 'belum';
    }else{
      return 'sudah';
    }
  }
  
  function fnGetJnsKuitansi(value){
   Ext.QuickTips.init();
    if(value == '1'){
      return 'Rawat Jalan';
    }else{
      return 'UGD';
    }
  }
  
  function onClickListTransaksi(grid, rowIndex, columnIndex, event) {
      var t = event.getTarget();
      if (t.className == 'keyMasterDetail')
      {
        var obj = ds_list_transaksi.getAt(rowIndex);
        var kdjurnal         = obj.get("kdjurnal");
        var nokuitansi       = obj.get("nokuitansi");
        var tglkuitansi      = obj.get("tglkuitansi");
        var status_posting   = (kdjurnal == '-') ? 0 : 1;
/*
        //tambahkan nopo & tglpo pada hidden field
        Ext.getCmp("nopo_posting").setValue(nopo);
        Ext.getCmp("tglpo_posting").setValue(tglpo);

        //reload detail jurnal
        ds_jurnal.setBaseParam('nopo', nopo);
        ds_jurnal.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_jurnal.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.total_debit").setValue(total_debit);            
            Ext.getCmp("info.total_kredit").setValue(total_kredit);

          }
        });
*/
        //reload detail kuitansi
        ds_detail_transaksi.setBaseParam('nokuitansi', nokuitansi);
        ds_detail_transaksi.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total transaksi
            total_bayar = 0;
            
            ds_detail_transaksi.each(function (rec) { 
              total_bayar += parseFloat(rec.get('jumlah')); 
            });
            
            Ext.getCmp("info.total_transaksi").setValue(total_bayar);
            
            /*            
            Ext.getCmp("tf.no_jurnal").setValue(kdjurnal);
            Ext.getCmp("info.total_transaksi").setValue(total_bayar);
            Ext.getCmp("tf.kd_supplier").setValue(kdsupplier);
            Ext.getCmp("tf.no_trans").setValue(nopo);
            Ext.getCmp("tf.nama_supplier").setValue(nmsupplier);
            
            Ext.getCmp("tf.total_beli").setValue(total_beli);
            Ext.getCmp("tf.diskon").setValue(total_diskon);
            Ext.getCmp("tf.bonus").setValue(total_bonus);
            Ext.getCmp("tf.ppn").setValue(total_ppn);
            Ext.getCmp("tf.total_bayar").setValue(total_bayar);
            */
          }
        });

        ds_detail_nota.setBaseParam('nokuitansi', nokuitansi);
        ds_detail_nota.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total tagihan di nota
            total_nota = 0;
            
            ds_detail_nota.each(function (rec) { 
              total_nota += parseFloat(rec.get('bayaritem')); 
            });
            
            Ext.getCmp("info.total_nota").setValue(total_nota);
          }
        });
        
        //disable or enable button posting
        if(status_posting == 0)
        {
          Ext.getCmp('btn.posting').enable();
        
        }else if(status_posting == 1){
          Ext.getCmp('btn.posting').disable();
        
        }

      }
      return true;
  }
  
  
}