/*
  jurnal Transaksi retur Deposit Pasien Rawat Inap
*/

function jurn_ri_returdeposit(){
  Ext.form.Field.prototype.msgTarget = 'side';
  
  var ds_list_transaksi     = dm_jurnpasien_transaksi_returdeposit();
  var ds_jurnal             = dm_jurnpasien_transaksi_returdeposit_jurnaling();
      ds_jurnal.setBaseParam('kdjurnal','null');
  
  var cm_list_transaksi = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode Jurnal'),
      width: 100,
      dataIndex: 'kdjurnal',
      align:'left',
      sortable: true,
      renderer: fnkeyShowDetailTransaksi
    },{
      header: headerGerid('Tgl<br>Transaksi'),
      width: 80,
      dataIndex: 'tgltransaksi',
      align:'left',
      renderer: Ext.util.Format.dateRenderer('d/m/Y'),
      sortable: true,
    },{
      header: headerGerid('Penerima'),
      width: 160,
      dataIndex: 'penerima',
      sortable: true,
      align:'left'
    },{
      header: headerGerid('Nominal'),
      width: 100,
      dataIndex: 'nominal',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Posting'),
      width: 80,
      dataIndex: 'status_posting',
      align:'left',
      sortable: true,
      renderer: fnkeyshowPostingStatus,
    }],
  });

  var cm_jurnal = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 80,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 120,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 70,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 90,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 90,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
    
  var grid_list_transaksi = new Ext.grid.GridPanel({
    id: 'grid_list_transaksi',
    store: ds_list_transaksi,
    title: 'Data Transaksi',
    autoSizeColumns: true,
    enableColumnResize: true,  
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_list_transaksi,
    frame: true,
    loadMask: true,
    height: 530,
    layout: 'anchor',
    tbar: [{
      xtype: 'label',  
      text: 'Periode : ', 
      style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'datefield',
      id: 'tgl_transaksi',
      value: new Date(),
      format: "d/m/Y",
      width: 100, 
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'label', 
      text: 's/d',style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'datefield',
      id: 'tgl_transaksi_akhir',
      value: new Date(),
      format: "d/m/Y",
      width: 100, 
      listeners:{
        specialkey: function(f,e){
          if (e.getKey() == e.ENTER) {
            fnSearchTransaksi();
          }
        }
      }
    },{
      xtype: 'textfield',
      id: 'pencarian',
      width: 100,
      style : 'margin-right:5px;margin-left:5px;',
    },{
      xtype: 'button',
      text: 'Cari',
      style : 'margin-left:10px;',
      id: 'btn.show_transaksi',
      iconCls: 'silk-find',
      handler: function() {
        fnSearchTransaksi();
      }
    }],
    style: 'padding-bottom:5px',
    listeners: {
      cellclick: onClickListTransaksi
    }
  });

  var grid_jurnal = new Ext.grid.GridPanel({
    id: 'grid_jurnal',
    store: ds_jurnal,
    title: 'Detail Jurnal',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_jurnal,
    frame: true,
    loadMask: true,
    height: 270,
    layout: 'anchor',
    style: 'padding-bottom:5px',
    tbar: ['->',{
      xtype: 'button',
      text: 'Posting',
      id: 'btn.posting',
      iconCls: 'silk-save',
      width: 100,
      disabled: true,
      style: 'margin-left:5px',
      handler: function() {
        var kdjurnal_posting = Ext.getCmp("kdjurnal_posting").getValue();
        
        if(kdjurnal_posting == ''){
          Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
        }
        else{
          var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
          
          Ext.Ajax.request({
            url: BASE_URL + 'jurn_transaksi_pasien_controller/posting_transaksi_pasien_returdeposit',
            params: {
              kdjurnal_posting     : kdjurnal_posting,
            },
            success: function(response){
              waitmsg.hide();

              obj = Ext.util.JSON.decode(response.responseText);
              if(obj.success === true){
                Ext.getCmp('btn.posting').disable();
              }else{
                Ext.getCmp('btn.posting').enable();  
              }

              Ext.MessageBox.alert('Informasi', obj.message);
              
              ds_list_transaksi.reload();
              ds_jurnal.reload();
            }
          });
         
        }

      }
    }],
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_debit',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
        decimalPrecision:0,
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_kredit',
        width:90,
        readOnly:true,
        value:'0',
        align:'right',
        decimalPrecision:0,

    }],
  });

  var form_bp_general = new Ext.FormPanel({
    id: 'form_bp_general',
    title: 'Jurnal Khusus (Retur Deposit Pasien RI)', 
    iconCls:'silk-money',
    width: 900, 
    autoScroll: true,
    layout: 'column',
    frame: true,
    items: [{
      id: 'colom_grid_left',
      style: 'margin-right:10px',
      layout: 'form', 
      columnWidth: 0.52,
      items: [
      // ============================== panel left side (transaksi & form trans) ===============================//
      {
        xtype: 'fieldset',
        border: false,
        style: 'padding:0px',
        id: 'panel_detail_transaksi',
        labelWidth: 115, labelAlign: 'right',
        items: [
          grid_list_transaksi,
          {
            xtype: 'textfield',
            id: 'kdjurnal_posting',
            value: '',
            hidden: true,
          }
        ]
      }]
    },{
      // ============================================ panel right side ================================================//
      id: 'colom_grid_right',
      layout: 'form',
      border: false,
      style: 'padding:0px',     
      columnWidth: 0.48,
      items: [
        grid_jurnal,
      ]
    }]
  });
  SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
  
  function fnSearchTransaksi(){
    ds_list_transaksi.setBaseParam('tgltransaksi', Ext.getCmp('tgl_transaksi').getValue().format('Y-m-d'));
    ds_list_transaksi.setBaseParam('tgltransaksi_akhir', Ext.getCmp('tgl_transaksi_akhir').getValue().format('Y-m-d'));
    ds_list_transaksi.setBaseParam('pencarian', Ext.getCmp('pencarian').getValue());
    ds_list_transaksi.load();
  }

  function fnkeyShowDetailTransaksi(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }

  function fnkeyshowPostingStatus(value){
   Ext.QuickTips.init();
    if(value == '0'){
      return 'belum';
    }else{
      return 'sudah';
    }
  }
  
  function onClickListTransaksi(grid, rowIndex, columnIndex, event) {
      var t = event.getTarget();
      if (t.className == 'keyMasterDetail')
      {
        var obj = ds_list_transaksi.getAt(rowIndex);
        var kdjurnal         = obj.get("kdjurnal");
        var penerima         = obj.get("penerima");
        var tgltransaksi      = obj.get("tgltransaksi");
        var status_posting      = obj.get("status_posting");

        //tambahkan idperawat & tglkuitansi pada hidden field
        Ext.getCmp("kdjurnal_posting").setValue(kdjurnal);
        
        //reload detail jurnal
        ds_jurnal.setBaseParam('kdjurnal', kdjurnal);
        ds_jurnal.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_jurnal.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.total_debit").setValue(total_debit);            
            Ext.getCmp("info.total_kredit").setValue(total_kredit);

          }
        });
        
        //disable or enable button posting
        if(status_posting == 0)
        {
          Ext.getCmp('btn.posting').enable();
        
        }else if(status_posting == 1){
          Ext.getCmp('btn.posting').disable();
        
        }

      }
      return true;
  }
  
  
}