function Akneraca(){
  
  //automated calculate penyusutan
  Ext.Ajax.request({
    url: BASE_URL + 'hartatetap_controller/kalkulasi_penyusutan',
    params: {
      calculate:1
    },
    success: function(response){}
  });

  var pageSize = 50;
  var ds_tahun = dm_tahun();
  var ds_bulan = dm_bulan();
  var ds_aktiva_lancar = dm_aktiva_lancar();
  var ds_aktiva_tetap = dm_aktiva_tetap();
  var ds_pasiva = dm_pasiva();
  var ds_modal = dm_modal();
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
  
  var grid_aktiva_lancar = new Ext.grid.GridPanel({
    id: 'grid_aktiva_lancar',
    title: 'Aktiva Lancar',
    store: ds_aktiva_lancar,
    height: 225,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    columnLines: true,
    style:'margin-bottom:7px',
    bbar:['->',
    {
      xtype: 'numericfield',
      thousandSeparator:',', 
      id: 'total_aktiva_lancar',
      width:140,
      value:'0',
    }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 110,
      dataIndex: 'kdakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nama Akun'),
      width: 210,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Saldo'),
      width: 140,
      dataIndex: 'saldo',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }],
  });

  var grid_aktiva_tetap = new Ext.grid.GridPanel({
    id: 'grid_aktiva_tetap',
    title: 'Aktiva Tetap',
    store: ds_aktiva_tetap,
    height: 225,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    columnLines: true,
    bbar:['->',
    {
        xtype: 'numericfield',
        thousandSeparator:',', 
        id: 'total_aktiva_tetap',
        width:140,
        value:'0',
    }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 110,
      dataIndex: 'kdakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nama Akun'),
      width: 210,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Saldo'),
      width: 140,
      dataIndex: 'saldo',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }],
  }); 
  
  var grid_pasiva = new Ext.grid.GridPanel({
    id: 'grid_pasiva',
    title: 'Pasiva',
    store: ds_pasiva,
    height: 225,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    columnLines: true,
    style:'margin-bottom:7px',
    bbar:['->',
    {
      xtype: 'numericfield',
      thousandSeparator:',', 
      id: 'total_pasiva',
      width:140,
      value:'0',
    }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 110,
      dataIndex: 'kdakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nama Akun'),
      width: 210,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Saldo'),
      width: 140,
      dataIndex: 'saldo',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }],
  }); 

  var grid_modal = new Ext.grid.GridPanel({
    id: 'grid_modal',
    title: 'Modal',
    store: ds_modal,
    height: 225,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    columnLines: true,
    bbar:['->',
    {
      xtype: 'numericfield',
      thousandSeparator:',', 
      id: 'total_modal',
      width:140,
      value:'0',
    }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 110,
      dataIndex: 'kdakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nama Akun'),
      width: 210,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Saldo'),
      width: 140,
      dataIndex: 'saldo',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }],
  }); 

  var form_bp_general = new Ext.form.FormPanel({
    id: 'form_bp_general',
    title: 'Neraca', iconCls:'silk-money',
    width: 900, Height: 1000,
    layout: {type: 'form', pack: 'center', align: 'center'},
    frame: true,
    autoScroll: true,
    items: [
    {
      xtype: 'fieldset',
      labelWidth: 60, labelAlign: 'right',
      items: [{
        xtype: 'compositefield',
        fieldLabel: 'Periode ',
        items:[{
          xtype: 'combo',
          store: ds_tahun,
          valueField: 'tahun', 
          displayField: 'tahun', 
          editable: false,
          triggerAction: 'all',
          mode: 'local',
          emptyText:'Pilih Tahun',
          id:'cb.tahun',
          width: 100,
        },{
          xtype: 'combo',
          store: ds_bulan,
          valueField: 'kdbulan', 
          displayField: 'nmbulan', 
          editable: false,
          triggerAction: 'all',
          mode: 'local',
          emptyText:'Pilih Bulan',
          id:'cb.bulan',
          width: 100,
          style : 'margin-left:5px;',
        },{
          xtype: 'button',
          text: 'Tampilkan',
          style : 'margin-left:5px;',
          id: 'btn.show_neraca',
          iconCls: 'silk-find',
          handler: function() {
            fnSearchgrid();
          }
        },
        /*{
          xtype: 'button',
          text: 'Cetak',
          style : 'margin-left:10px;',
          id: 'btn.cetak',
          iconCls: 'silk-printer',
          handler: function() {
            //fnSearchgrid();
          }
        },
        */
        {
          xtype: 'button',
          text: 'Cetak Excel',
          style : 'margin-left:10px;',
          id: 'btn.cetakexcel',
          iconCls: 'silk-printer',
          handler: function() {
            fncetakExcel();
          }
        }]
      }]
    },{
      layout: 'column',
      frame: true,
      items:[
      {
        id: 'colom_grid_left',
        style: 'margin-right:7px', //activa block
        layout: 'form', 
        columnWidth: 0.5,
        items: [grid_aktiva_lancar, grid_aktiva_tetap]
      },{
        id: 'colom_grid_right', //pasiva block
        layout: 'form', 
        columnWidth: 0.5,
        items: [grid_pasiva, grid_modal]
      }],
    }],
  });
  SET_PAGE_CONTENT(form_bp_general);
  
  function show_detail_jurnal(grid, rowIndex, columnIndex, event) {
  
    var t = event.getTarget();
    if (t.className == 'keyMasterDetail'){
      var obj = ds_jurnal_umum.getAt(rowIndex);
      var kdjurnal = obj.get("kdjurnal");
      ds_jurnal_umum_det.setBaseParam('kdjurnal',kdjurnal);
      ds_jurnal_umum_det.reload();
    }

  }
  
  function fnkeyShowDetailJurnal(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }
  
  function fnSearchgrid(){
    
    ds_aktiva_lancar.setBaseParam('tahun', Ext.getCmp('cb.tahun').getValue());
    ds_aktiva_lancar.setBaseParam('bulan', Ext.getCmp('cb.bulan').getValue());
    ds_aktiva_lancar.load({
      scope   : this,
      callback: function(records, operation, success) {
        total = 0;
        ds_aktiva_lancar.each(function (rec) { 
          total += parseFloat(rec.get('saldo')); 
        });

        Ext.getCmp("total_aktiva_lancar").setValue(total);

      }
    });

    ds_aktiva_tetap.setBaseParam('tahun', Ext.getCmp('cb.tahun').getValue());
    ds_aktiva_tetap.setBaseParam('bulan', Ext.getCmp('cb.bulan').getValue());
    ds_aktiva_tetap.load({
      scope   : this,
      callback: function(records, operation, success) {
        total = 0;
        ds_aktiva_tetap.each(function (rec) { 
          total += parseFloat(rec.get('saldo')); 
        });

        Ext.getCmp("total_aktiva_tetap").setValue(total);

      }
    });

    ds_pasiva.setBaseParam('tahun', Ext.getCmp('cb.tahun').getValue());
    ds_pasiva.setBaseParam('bulan', Ext.getCmp('cb.bulan').getValue());
    ds_pasiva.load({
      scope   : this,
      callback: function(records, operation, success) {
        total = 0;
        ds_pasiva.each(function (rec) { 
          total += parseFloat(rec.get('saldo')); 
        });

        Ext.getCmp("total_pasiva").setValue(total);

      }
    });

    ds_modal.setBaseParam('tahun', Ext.getCmp('cb.tahun').getValue());
    ds_modal.setBaseParam('bulan', Ext.getCmp('cb.bulan').getValue());
    ds_modal.load({
      scope   : this,
      callback: function(records, operation, success) {
        total = 0;
        ds_modal.each(function (rec) { 
          total += parseFloat(rec.get('saldo')); 
        });

        Ext.getCmp("total_modal").setValue(total);

      }
    });
  
  }

  function fncetakExcel(){
    var tahun = Ext.getCmp('cb.tahun').getValue();
    var bulan = Ext.getCmp('cb.bulan').getValue();
    if(!bulan) bulan = '00';

    RH.ShowReport(BASE_URL + 'neraca_controller/lap_neraca_excel/' + tahun + '/' + bulan);
  
  }
  
}
  
