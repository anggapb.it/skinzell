function Akpettycash(){
	var pageSize = 50;
	var ds_pettycash = dm_pettycash();
	//var ds_akun = dm_akun_jurnal();
	var ds_ket_pettycash = dm_ket_pettycash();
	var static_tgl_transaksi = new Date();
	var static_akun_debit = '';

	var ds_akun = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'pettycash_controller/get_akun_biaya_w_inventaris',
			method: 'POST'
		}),
		root: 'data', 
		totalProperty: 'results',
		autoLoad: true,
		fields:[{
			name: 'idakunparent', 
			mapping:'idakunparent'
		},{
			name: 'idakun', 
			mapping:'idakun'
		},{
			name: 'kdakun', 
			mapping:'kdakun'
		},{
			name: 'nmakun', 
			mapping:'nmakun'
		}]
	});	

	var arr_cari = [
		['kdjurnal', 'Kode Jurnal'],
		['nojurnal', 'No. Jurnal'],
		['akundebit', 'Akun Debit'],
		['nominal', 'Nominal'],
		['noreff', 'No. Reff / Bon'],
		['keterangan', 'Keterangan'],
		['tgljurnal', 'Tanggal Input'],
		['status_posting', 'Status Posting'],
	];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	//other component
	var cbGrid = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			rowselect: function(selectionModel, rowIndex, record){
				calculateNominal();
				allowToPosting();
			},
			rowdeselect: function(sm, rowIndex, keep, record){
				calculateNominal();
				allowToPosting();
			}
		}
	});

	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_pettycash,
		mode: 'remote',
		displayInfo: true,
		displayMsg: 'Data Petty Cash {0} - {1} dari total {2}',
		emptyMsg: 'Belum ada Data'
	});
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
		
	var grid_pettycash = new Ext.grid.GridPanel({
		id: 'grid_pettycash',
		store: ds_pettycash,
		sm: cbGrid,
		autoScroll: true,
		frame: true,
		view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
		loadMask: true,
		bbar: paging,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddPettyCash();
			}
		},{
			text: 'Posting data terpilih',
			id: 'btn_posting',
			iconCls: 'silk-save',
			disabled: true,
			style: 'margin-left:15px',
			handler: function() {
				fnPostingPettyCash();
			}
		},
		'->',
		{
			xtype: 'compositefield',
			width: 930,
			items: [{
				xtype: 'checkbox',
				id: 'chb.periode', margins: '0 3 0 90',
				listeners: {
					check: function(checkbox, val){
						if(val == true){
							Ext.getCmp('tglawal').enable();
							Ext.getCmp('tglakhir').enable();
						} else if(val == false){
							Ext.getCmp('tglawal').disable();
							Ext.getCmp('tglakhir').disable();
							Ext.getCmp('tglawal').setValue(new Date());
							Ext.getCmp('tglakhir').setValue(new Date());							
							fnSearchgrid();
						}
					}
				}
			},{
				xtype: 'label', id: 'lb.lb', text: 'Tgl. Transaksi :', margins: '6 10 0 0',
			},{
				xtype: 'datefield',
				id: 'tglawal',
				value: new Date(),
				format: "d/m/Y",
				width: 100, disabled: true,
				listeners:{
					select: function(field, newValue){
						fnSearchgrid();
					},
					change : function(field, newValue){
						fnSearchgrid();
					}
				}
			},
			{
				xtype: 'label', id: 'lb.lbt', text: 's/d', margins: '6 4 0 0',
			},
			{
				xtype: 'datefield',
				id: 'tglakhir',
				value: new Date(),
				format: "d/m/Y",
				width: 100, disabled: true,
				listeners:{
					select: function(field, newValue){
						fnSearchgrid();
					},
					change : function(field, newValue){
						fnSearchgrid();
					}
				}
			},{
				xtype: 'label', text: 'Cari Berdasarkan :', margins: '6 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.search',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				value: 'kdjurnal',
				margins: '2 5 0 0',
				listeners: {
					select: function() {
						var cbsearch = Ext.getCmp('cb.search').getValue();
						var tfsearch = Ext.getCmp('tf.search').getValue();
						if(cbsearch != '' && tfsearch != ''){
							fnSearchgrid();
						}
						return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'tf.search',
				width: 160,
				margins: '2 5 0 0',
				validator: function(){
					fnSearchgrid();
				}
			}]
		}],
		height: 480,
		layout: 'fit', 
		columnLines: true,
		columns: [cbGrid,
		{
			header: headerGerid('Kode<br>Jurnal'),
			width: 90,
			dataIndex: 'kdjurnal',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('No.<br>Jurnal'),
			width: 60,
			dataIndex: 'nojurnal',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Tgl <br> Transaksi'),
			width: 70,
			dataIndex: 'tgltransaksi',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y'),
		},{
			header: headerGerid('Akun Debit'),
			width: 120,
			dataIndex: 'akundebit',
			sortable: true,
			align:'left',
		}
		,{
			header: headerGerid('Nominal'),
			width: 65,
			dataIndex: 'nominal',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('No. Reff / No. Bon'),
			width: 110,
			dataIndex: 'noreff',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('Keterangan'),
			width: 200,
			dataIndex: 'keterangan',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('User Input'),
			width: 70,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('Tgl <br> Input'),
			width: 70,
			dataIndex: 'tgljurnal',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y'),
		},{
			header: headerGerid('Status<br/>Posting'),
			width: 60,
			dataIndex: 'status_posting',
			sortable: true,
			align:'center',
			renderer: fnkeyshowPostingStatus,
		},{
			xtype: 'actioncolumn',
			width: 50,
			header: 'Edit',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_edit.png',
				tooltip: 'Edit record',
				handler: function(grid, rowIndex) {
	        var obj = ds_pettycash.getAt(rowIndex);
        	var status_posting         = obj.get("status_posting");
        	if(status_posting == 1)
        	{
					  Ext.MessageBox.alert('Informasi', 'Data yang sudah diposting tidak dapat di edit.');
        	}else{
        		fnEditPettyCash(grid, rowIndex);
        	}
				}
			}]
    },{
			xtype: 'actioncolumn',
			width: 50,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					var obj = ds_pettycash.getAt(rowIndex);
        	var status_posting         = obj.get("status_posting");
        	if(status_posting == 1)
        	{
					  Ext.MessageBox.alert('Informasi', 'Data yang sudah diposting tidak dapat di hapus.');
        	}else{
        		fnDeletePettyCash(grid, rowIndex);
        	}
				}
			}]
    }]
	});	
	
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Petty Cash', iconCls:'silk-money',
		width: 900, Height: 1000,
		layout: {
      type: 'form',
      pack: 'center',
      align: 'center'
    },
		frame: true,
		autoScroll: true,
		items: [
			grid_pettycash,{			
      xtype: 'panel',
      frame: true,
      autoHeight: true,
      items: [{            
				xtype: 'compositefield',
      	id: 'total_nominal',
      	items: [{
      		xtype: 'tbtext',
					text: 'Total nominal : ',
					margins : {top:5, right:5, bottom:0, left:0},
					width: 80,
					style: {
						fontWeight: 'bold',
						fontSize: '9px'
					}
        },{
        	xtype: 'numericfield',
					id: 'nf.total_nominal',
					thousandSeparator:',',
					readonly:true,
					style: 'opacity:0.8',
        }]
      }]	        
		}]
	});
	
	SET_PAGE_CONTENT(form_bp_general);
	
	function calculateNominal()
	{
		var total_nominal = 0;
		if (grid_pettycash.getSelectionModel().hasSelection()) {
			var records = grid_pettycash.getSelectionModel().getSelections();
			var count_records = records.length;
			for(var i = 0; i < count_records; i++){
				rec = records[i];
				var get_nominal = rec.get("nominal");
				total_nominal = parseInt(total_nominal) + parseInt(get_nominal);
			}
		}
		Ext.getCmp('nf.total_nominal').setValue(total_nominal);
	}

	function allowToPosting(){
		if (grid_pettycash.getSelectionModel().hasSelection()) {
			var records = grid_pettycash.getSelectionModel().getSelections();
			var count_records = records.length;
			var allow = false;
			for(var i = 0; i < count_records; i++){
				rec = records[i];
				var status_posting = rec.get("status_posting");
				if(status_posting != '1') allow = true;
			}

			if(allow === true){
				Ext.getCmp('btn_posting').enable();	
			}else{
				Ext.getCmp('btn_posting').disable();		
			}

		}else{
			Ext.getCmp('btn_posting').disable();	
		}
	}
	
	function fnSearchgrid(){
		var idcombo, nmcombo;
		
		if(Ext.getCmp('chb.periode').getValue() == true){
			ds_pettycash.setBaseParam('tglawal', Ext.getCmp('tglawal').getValue().format('Y-m-d'));
			ds_pettycash.setBaseParam('tglakhir', Ext.getCmp('tglakhir').getValue().format('Y-m-d'));
		}else if(Ext.getCmp('chb.periode').getValue() == false){
			ds_pettycash.setBaseParam('tglawal','');
			ds_pettycash.setBaseParam('tglakhir','');
		}
	
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('tf.search').getValue();
		ds_pettycash.setBaseParam('searchkey',  idcombo);
		ds_pettycash.setBaseParam('searchvalue',  nmcombo);
		ds_pettycash.load();		
	}
	
	function fnTerbilang(){
		var nominal = Ext.getCmp('tf.nominal').getValue();
		Ext.Ajax.request({
			url: BASE_URL + 'pettycash_controller/get_terbilang',
			params: {
				nominal			:	nominal,
			},
			success: function(response){
				Ext.getCmp('tf.terbilang').setValue(response.responseText);
			}
		});
	}
	
	function fnPostingPettyCash(){
		if (grid_pettycash.getSelectionModel().hasSelection()) {
			var records = grid_pettycash.getSelectionModel().getSelections();
			var count_records = records.length;
			var pc_ids = '';
			for(var i = 0; i < count_records; i++){
				rec = records[i];
				
				var pc_kdjurnal = rec.get("kdjurnal");
				if(i == 0){
					pc_ids += pc_kdjurnal;
				}else{
					pc_ids += '^' + pc_kdjurnal;
				}
			}

			var waitmsg = Ext.MessageBox.wait('Memposting...', 'Info');
			
			Ext.Ajax.request({
				url: BASE_URL + 'pettycash_controller/bulk_posting_pettycash',
				params: {
					pc_ids		:	pc_ids,
				},
				success: function(response){
					waitmsg.hide();
					obj = Ext.util.JSON.decode(response.responseText);
					if(obj.success === true){
						Ext.MessageBox.alert('Informasi','Posting Data Berhasil');
					}else{
						Ext.MessageBox.alert('Informasi','Posting Data Gagal');
					}
					ds_pettycash.reload();
					
				}
			});
					
		}
	}

	function fnAddPettyCash(){
		var grid = ds_pettycash;
		wEntryPettyCash(false, grid, null);	
	}
	
	function fnEditPettyCash(grid, record){
		var record = ds_pettycash.getAt(record);
		wEntryPettyCash(true, grid, record);		
	}
	
	function fnDeletePettyCash(grid, record){
		var record = ds_pettycash.getAt(record);
		var url = BASE_URL + 'pettycash_controller/delete_petty_cash';
		var params = new Object({
						kdjurnal	: record.get('kdjurnal')
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
	function wEntryPettyCash(isUpdate, grid, record){
		var winTitle = (isUpdate)?'Petty Cash (Edit)':'Petty Cash (Entry)';
		var pettycash_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.pettycash',
			buttonAlign: 'left',
			labelWidth: 110, labelAlign: 'right',
			monitorValid: true,
			height: 300, width: 800,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					fnSavePettyCash();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					reset_form_insert();
					wPettyCash.close();
				}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 250, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.65, border: false, layout: 'form',					
					items: [{
						fieldLabel: 'Tgl Transaksi ',
						xtype: 'datefield',
						id: 'df.tgltransaksi',
						format: 'd/m/Y',
						value: static_tgl_transaksi,
						width: 120,
						allowBlank: false,
						enableKeyEvents: true,
						listeners: {
							change : function(field, newValue){
								fnsetStaticTgl();
							},
							specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									Ext.getCmp('cb.debit').focus();
							  }
							}
						}
					},{
						xtype: 'textfield',
						value:2, //kas kecil
						hidden: true,
						id:'cb.kredit',
						width: 150,
					}
					/*
					,{
						xtype: 'textfield',
						fieldLabel: 'Akun Kredit',
						value: 'Kas Kecil K3HB', 
						readOnly: true,
						id: 'tf.kredit_display',
						style: 'opacity:0.8',
						width: 150,
					}
					*/
					,{
						xtype: 'combo',
						store: ds_akun,
						valueField: 'idakun', 
						displayField: 'nmakun', 
						editable: true,
						allowBlank: false,
						triggerAction: 'all',
						forceSelection: true, 
						submitValue: true, 
						mode: 'local',
						emptyText:'Pilih....',
						fieldLabel: 'Akun Debit ',
						id:'cb.debit',
						width: 300,
						enableKeyEvents: true,
						listeners: {
							specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									Ext.getCmp('tf.noreff').focus();
							  }
							}
						}
					},{
						xtype: 'textfield',
						fieldLabel: 'No. Reff/No. Bon ',
						id:'tf.noreff',
						width: 300,
						enableKeyEvents: true,
						listeners: {
							specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									Ext.getCmp('tf.nominal').focus();
							  }
							}
						}
					},{
						xtype: 'numericfield',
						fieldLabel: 'Nominal ',
						id: 'tf.nominal',
						width: 300,
						thousandSeparator:',',
						allowBlank: false,
						enableKeyEvents: true,
						listeners: {
							change : function(field, newValue){
								fnTerbilang();
							},
							specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									Ext.getCmp('tf.keterangan').focus();
							  }
							}
						}
					},{
						xtype: 'textfield',
						fieldLabel: 'Terbilang ',
						id: 'tf.terbilang',
						width: 300,
						disabled: true
					},
					{
						xtype: 'compositefield',
						fieldLabel: 'Keterangan ',
						items:[{
							xtype: 'combo',
							store: ds_ket_pettycash,
							valueField: 'keterangan', 
							displayField: 'keterangan', 
							editable: true,
							allowBlank: true,
							triggerAction: 'all',
							forceSelection: false, 
							submitValue: true, 
							id:'tf.keterangan',
							width: 200,
							triggerAction:'all',
							typeAhead:true,
							mode:'remote',
							minChars:3,
							hideTrigger:true,
							width : 300,
							listeners:{
								change : function(field, newValue){
									pairingAkun();
								}
							}
						},{
			        xtype: 'button',
			        text: 'Simpan',
			        style : 'margin-left:5px;',
			        id: 'btn.save_keterangan',
			        iconCls: 'silk-add',
			        handler: function() {
        				var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
        				var s_keterangan = Ext.getCmp('tf.keterangan').getValue();
        				Ext.Ajax.request({
									url: BASE_URL + 'pettycash_controller/save_ket_petty_cash',
									params: {
										s_keterangan : s_keterangan,
									},
									success: function(response){
										waitmsg.hide();
										obj = Ext.util.JSON.decode(response.responseText);
										if(obj.success === true){
											Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
										}else{
											Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
										}
									}
								});

			        }
			      }],
					}]
				},
				{
					columnWidth: 0.35, border: false, layout: 'form',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'Kode Jurnal ',
						id:'tf.kdjurnal',
						width: 150,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield',
						fieldLabel: 'No. Jurnal ',
						id:'tf.nojurnal',
						width: 150,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield',
						fieldLabel: 'User Input ',
						id: 'tf.userinput',
						width: 150,
						readOnly: true,
						value: USERNAME,
						style : 'opacity:0.6'						
					},{
						xtype: 'datefield',
						fieldLabel: 'Tgl. Input ',
						id: 'df.tglinput',
						format: 'd/m/Y',
						value: new Date(),
						width: 150,
						disabled: true
					}]
				}]
			}]
		});
		
		var wPettyCash = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			floating: {shadow: false},
			items: [pettycash_form],
	    listeners: {
        show: function() {
          this.el.setStyle('margin-top', '75px');
        }
   	 	}
		});
	
		setPettyCashForm(isUpdate, record);
		wPettyCash.show();
		
		function fnSavePettyCash(){
			var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
			var tgltransaksi = Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'); 
			var akun_debit = Ext.getCmp('cb.debit').getValue(); 
			var akun_kredit = Ext.getCmp('cb.kredit').getValue(); 
			var noreff = Ext.getCmp('tf.noreff').getValue(); 
			var keterangan = Ext.getCmp('tf.keterangan').getValue(); 
			var kdjurnal = Ext.getCmp('tf.kdjurnal').getValue();
			var userinput = Ext.getCmp('tf.userinput').getValue();
			var tglinput = Ext.getCmp('df.tglinput').getValue().format('Y-m-d'); 
			var nominal = Ext.getCmp('tf.nominal').getValue();
			
			if(tgltransaksi == ''){
				Ext.MessageBox.alert('Informasi','Tanggal transaksi belum diisi..');
				return;
			}
			
			if(akun_debit == ''){
				Ext.MessageBox.alert('Informasi','Akun debit belum diisi..');
				return;
			}
			
			if(akun_kredit == ''){
				Ext.MessageBox.alert('Informasi','Akun kredit belum diisi..');
				return;
			}
			
			if(nominal == 0){
				Ext.MessageBox.alert('Informasi','Nominal belum diisi..');
				return;
			}
			
			if(isUpdate){
				//update data
				Ext.Ajax.request({
					url: BASE_URL + 'pettycash_controller/update_petty_cash',
					params: {
						kdjurnal		:	kdjurnal,
						tgltransaksi	:	tgltransaksi,
						akun_debit		:	akun_debit,
						akun_kredit		:	akun_kredit,
						tgljurnal		:	tglinput,
						keterangan		:	keterangan,
						noreff			:	noreff,
						userid			:	USERID,
						nominal			:	nominal,
					},
					success: function(response){
						waitmsg.hide();
						obj = Ext.util.JSON.decode(response.responseText);
						if(obj.success === true){
							Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
							//reset_form_insert();
							//wPettyCash.close();
						}else{
							//Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
						}
					}
				});
				
			}else{
				//insert data
				Ext.Ajax.request({
					url: BASE_URL + 'pettycash_controller/insert_petty_cash',
					params: {
						tgltransaksi	:	tgltransaksi,
						akun_debit		:	akun_debit,
						akun_kredit		:	akun_kredit,
						tgljurnal		:	tglinput,
						keterangan		:	keterangan,
						noreff			:	noreff,
						userid			:	USERID,
						nominal			:	nominal,
					},
					success: function(response){
						waitmsg.hide();
						obj = Ext.util.JSON.decode(response.responseText);
						if(obj.success === true){
							Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
							reset_form_insert();
							//wPettyCash.close();
						}else{
							Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
						}
					}
				});
				
			}
			
			
		}
		
		function setPettyCashForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					RH.setCompValue('df.tgltransaksi', record.get('tgltransaksi'));
					RH.setCompValue('cb.debit', record.get('idakundebit')); 
					RH.setCompValue('cb.kredit', record.get('idakunkredit')); 			
					RH.setCompValue('tf.noreff', record.get('noreff')); 
					RH.setCompValue('tf.keterangan', record.get('keterangan')); 
					RH.setCompValue('tf.kdjurnal', record.get('kdjurnal')); 
					RH.setCompValue('tf.nojurnal', record.get('nojurnal')); 
					RH.setCompValue('tf.userinput', record.get('userid')); 
					RH.setCompValue('df.tglinput', record.get('tgljurnal')); 
					RH.setCompValue('tf.nominal', record.get('nominal'));
					return;
				}
			}
		}

	}

	function fnsetStaticTgl(){
			var stattgl = Ext.getCmp('df.tgltransaksi').getValue();
			static_tgl_transaksi = stattgl;
	}
	
	function reset_form_insert()
	{
		/*
		Ext.getCmp('df.tgltransaksi').setValue(new Date());
		//Ext.getCmp('cb.debit').setValue(); 
		//Ext.getCmp('cb.kredit').setValue(2); 			
		Ext.getCmp('tf.noreff').setValue(); 
		Ext.getCmp('tf.keterangan').setValue(); 
		Ext.getCmp('tf.kdjurnal').setValue(); 
		Ext.getCmp('tf.userinput').setValue(); 
		Ext.getCmp('df.tglinput').setValue(new Date()); 
		Ext.getCmp('tf.nominal').setValue(); 
		ds_pettycash.reload();
		*/
		
		//RH.setCompValue('df.tgltransaksi', new Date()); 			
		//RH.setCompValue('cb.debit', ''); 			
		RH.setCompValue('tf.terbilang', ''); 
		RH.setCompValue('tf.noreff', ''); 
		RH.setCompValue('tf.keterangan', ''); 
		RH.setCompValue('tf.kdjurnal', ''); 
		RH.setCompValue('tf.nojurnal', ''); 
		//RH.setCompValue('tf.userinput', ''); 
		RH.setCompValue('df.tglinput', new Date()); 
		RH.setCompValue('tf.nominal', ''); 
		ds_pettycash.reload();
	}

	function pairingAkun()
	{
		var keterangan = Ext.getCmp('tf.keterangan').getValue();
		if(keterangan.length > 0)
		{
			Ext.Ajax.request({
				url: BASE_URL + 'pettycash_controller/get_pairingakun',
				params: {
					keterangan			:	keterangan,
				},
				success: function(response){
					obj = Ext.util.JSON.decode(response.responseText);
				  if(typeof obj.idakun !== 'undefined'){
						console.log(obj);					   
						Ext.getCmp('cb.debit').setValue(obj.idakun);
					  Ext.getCmp('cb.debit').setRawValue(obj.nmakun);	
				  }
				}
			});
		}
	}

  function fnkeyshowPostingStatus(value){
   Ext.QuickTips.init();
    if(value == '0'){
      return 'belum';
    }else if(value == '1'){
      return 'sudah';
    }
  }
}
