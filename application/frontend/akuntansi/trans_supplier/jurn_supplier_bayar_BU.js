function jurn_supplier_bayar(){
  Ext.form.Field.prototype.msgTarget = 'side';
  
  var ds_list_pembayaran = dm_jurnsupplier_bayar();
  var ds_detail_pembayaran = dm_jurnsupplier_bayardet();
  var ds_detail_jurnal = dm_jurnsupplier_bayar_jurnaling();
      ds_detail_jurnal.setBaseParam('nobayar','null');
  
  var cm_list_pembayaran = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('No. Pembayaran - No. PO'),
      width: 160,
      dataIndex: 'nobayar_nopo',
      align:'center',
      renderer: fnkeyShowDetailPembayaran
    },{
      header: headerGerid('Supplier'),
      width: 155,
      dataIndex: 'nmsupplier',
      align:'left'
    },{
      header: headerGerid('Total Bayar'),
      width: 95,
      dataIndex: 'total_bayar',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Posting'),
      width: 60,
      dataIndex: 'kdjurnal',
      align:'left',
      renderer: fnkeyshowPostingStatus,
    }],
  });

  var cm_detail_pembayaran = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Jenis Pembayaran'),
      width: 115,
      dataIndex: 'nmjnspembayaran',
      align:'center',
    },{
      header: headerGerid('No. Kartu/No. Reff'),
      width: 145,
      dataIndex: 'nokartu',
      align:'center',
    },{
      header: headerGerid('Akun'),
      width: 150,
      dataIndex: 'nmakun',
      align:'center',
    },{
      header: headerGerid('Nominal'),
      width: 100,
      dataIndex: 'nominal',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });

  var cm_detail_jurnal = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 80,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 140,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 95,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 100,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 100,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
    
  var grid_list_pembayaran = new Ext.grid.GridPanel({
    id: 'grid_list_pembayaran',
    store: ds_list_pembayaran,
    title: 'Data Pembayaran',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_list_pembayaran,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:15px',
    listeners: {
      cellclick: onClickListPembayaran
    }
  });

  var grid_detail_pembayaran = new Ext.grid.GridPanel({
    id: 'grid_detail_pembayaran',
    store: ds_detail_pembayaran,
    title: 'Detail Pembayaran',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_pembayaran,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:15px',
    bbar: [
      '->',
      {
        xtype: 'label', id: 'lb.totalr', 
        text: 'Total : ', //margins: '5 5 0 125',
      },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_bayar',
        width:100,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });


  var grid_detail_jurnal = new Ext.grid.GridPanel({
    id: 'grid_detail_jurnal',
    store: ds_detail_jurnal,
    title: 'Detail Jurnal',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_jurnal,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:15px',
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_debit',
        width:100,
        readOnly:true,
        value:'0',
        align:'right',
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_kredit',
        width:100,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });
       
  var form_bp_general = new Ext.FormPanel({
    id: 'form_bp_general',
    title: 'Jurnal Khusus (Pembayaran Supplier)', 
    iconCls:'silk-money',
    width: 900, Height: 1000,
    autoScroll: true,
    layout: {
      type: 'form',
      pack: 'center',
      align: 'center'
    },
    frame: true,
    items: [
    {
      layout: 'form',
      border: false,
      items: [{
        id: 'filter_jurnal',
        xtype: 'container',
        style: 'padding: 5px',
        defaults: {labelWidth: 100, labelAlign: 'right'},
        items:[{
          xtype: 'fieldset',
          //title: 'Filter',
          height: 110,
          layout: 'column',
          style: 'padding-top:15px',
          items: [{
            layout: 'form', 
            columnWidth: 0.6,
            items: [{
              fieldLabel: 'Tanggal Jurnal',
              xtype: 'datefield',
              id: 'tgl_jurnal',
              value: new Date(),
              format: "d/m/Y",
              width: 100, 
              disabled: true,
            },{
              fieldLabel: 'Tanggal Transaksi',
              xtype: 'datefield',
              id: 'tgl_transaksi',
              value: new Date(),
              format: "d/m/Y",
              width: 100, 
              listeners:{
                select: function(field, newValue){
                  fnSearchPembayaran();
                },
                change : function(field, newValue){
                  fnSearchPembayaran();
                }
              }
            }]
          },{
            layout: 'form', 
            columnWidth: 0.3,
            items: [{
              fieldLabel: 'Jenis Transaksi',
              xtype: 'textfield',
              id: 'jns_trans',
              value: 'Pembayaran Supplier',
              width: 150, 
              disabled: true,
            },{
              fieldLabel: 'USER ID',
              xtype: 'textfield',
              id: 'user_id',
              value: USERID,
              width: 150,
              disabled: true,
            },{
              xtype: 'textfield',
              id: 'nobayar_posting',
              value: '',
              hidden: true,
            },{
              xtype: 'textfield',
              id: 'tglbayar_posting',
              value: '',
              hidden: true,
            },{
                xtype: 'button',
                text: 'Posting',
                id: 'btn.posting',
                iconCls: 'silk-save',
                width: 100,
                height: 25,
                disabled: true,
                style: 'margin-left:105px',
                handler: function() {
                  var nobayar_posting = Ext.getCmp("nobayar_posting").getValue();
                  var tglbayar_posting = Ext.getCmp("tglbayar_posting").getValue();
                  var nmsupplier = Ext.getCmp("tf.nama_supplier").getValue();
                  var nominal_jurnal = Ext.getCmp("info.total_debit").getValue(); //nominal debit
                  var nominal_jurnal_kredit = Ext.getCmp("info.total_kredit").getValue(); //nominal kredit
                  
                  if(nobayar_posting == '' || tglbayar_posting == ''){
                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
                  }
                  else if(nominal_jurnal != nominal_jurnal_kredit){
                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
                  }
                  else{
                    var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
                    var post_grid ='';
                    var count= 1;
                    var endpar = ';';
                    
                    grid_detail_jurnal.getStore().each(function(rec){ // ambil seluruh grid prodi
                      var rowData = rec.data; 
                      if (count == grid_detail_jurnal.getStore().getCount()) {
                        endpar = ''
                      }
                      post_grid += rowData['idakun'] + '^' + 
                             rowData['kdakun'] + '^' + 
                             rowData['nmakun']  + '^' + 
                             rowData['noreff']  + '^' + 
                             rowData['debit'] + '^' + 
                             rowData['kredit']  + '^' +
                          endpar;
                                  
                      count = count+1;
                    });

                    Ext.Ajax.request({
                      url: BASE_URL + 'jurn_transaksi_supplier_controller/posting_jurnal_supplier_bayar',
                      params: {
                        nobayar_posting    : nobayar_posting,
                        tglbayar_posting   : tglbayar_posting,
                        nmsupplier      : nmsupplier,
                        nominal_jurnal  : nominal_jurnal,
                        post_grid       : post_grid,
                        userid          : USERID,
                      },
                      success: function(response){
                        waitmsg.hide();

                        obj = Ext.util.JSON.decode(response.responseText);
                        if(obj.success === true){
                          Ext.getCmp('btn.posting').disable();
                        }else{
                          Ext.getCmp('btn.posting').enable();  
                        }

                        Ext.MessageBox.alert('Informasi', obj.message);
                        
                        ds_list_pembayaran.reload();
                        ds_detail_pembayaran.reload();
                        ds_detail_jurnal.reload();
                      }
                    });
                   
                  }

                }
              }
            ]
          }]
        }]
      
      },

      // ====================================== CONTAINER DATA GRID ====================================================//
      {
        id: 'container_data_grid',
        xtype: 'container',
        style: 'padding: 5px',
        defaults: {labelWidth: 100, labelAlign: 'right'},
        items:[{
          xtype: 'fieldset',
          id: 'panel_data_grid',
          //height: 100,
          layout: 'column',
          items: [{
            id: 'colom_grid_left',
            style: 'margin-right:10px',
            layout: 'form', 
            columnWidth: 0.48,
            items: [
              // ============================== panel left side (Pembayaran & form Pembayaran) ===============================//
              {
                xtype: 'fieldset',
                border: false,
                style: 'padding:0px',
                id: 'panel_detail_pembayaran',
                labelWidth: 170, labelAlign: 'right',
                //layout: 'column',
                //defaults: {labelWidth: 100, labelAlign: 'right'},
                items: [grid_list_pembayaran, {
                  xtype: 'textfield', 
                  id: 'tf.no_jurnal',
                  fieldLabel: 'No. Jurnal ',
                  width: 245,
                  readOnly: true,
                },{
                  xtype: 'textfield', 
                  id: 'tf.nobayar',
                  fieldLabel: 'No. Pembayaran ',
                  width: 245,
                  readOnly: true,
                },{
                  xtype: 'compositefield',
                  fieldLabel: 'Kode Supplier / No. PO ',
                  items: [{
                    xtype: 'textfield',
                    id: 'tf.kd_supplier',
                    width: 90,
                    readOnly: true,
                  },{
                    xtype: 'label', id: 'lb.slash',
                    text: ' / ', margins: '3 10 0 5',
                  },{
                    xtype: 'textfield',
                    id: 'tf.nopo',
                    width: 130,
                    readOnly: true,
                  }]
                },{
                  xtype: 'textfield', 
                  id: 'tf.nama_supplier',
                  fieldLabel: 'Nama Supplier ',
                  width: 245,
                  readOnly: true,
                },{
                  xtype: 'numericfield',
                  thousandSeparator:',',
                  id: 'tf.total_bayar',
                  fieldLabel: 'Total Bayar ',
                  width: 245,
                  readOnly: true,
                }]
              }]
          },
          {
            id: 'colom_grid_right',
            //title: 'Data Jurnal',
            layout: 'form',
            border: false,
            style: 'padding:0px',     
            columnWidth: 0.51,
            items: [
              grid_detail_pembayaran,
              grid_detail_jurnal
            ]
          }]
        }]
      },
      // ========================================== END CONTAINER DATA GRID ==========================================//

      
      ]
    }]
  });
  SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
  
  function fnSearchPembayaran(){
    ds_list_pembayaran.setBaseParam('tglbayar', Ext.getCmp('tgl_transaksi').getValue().format('Y-m-d'));
    ds_list_pembayaran.load();
  }


  function fnkeyShowDetailPembayaran(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }

  function fnkeyshowPostingStatus(value){
   Ext.QuickTips.init();
    if(value == '-'){
      return 'belum';
    }else{
      return 'sudah';
    }
  }

  function onClickListPembayaran(grid, rowIndex, columnIndex, event) {
      var t = event.getTarget();
      var record = grid.getStore().getAt(rowIndex);
      
      if (t.className == 'keyMasterDetail')
      {
        var obj = ds_list_pembayaran.getAt(rowIndex);
        var kdjurnal         = obj.get("kdjurnal");
        var nopo             = obj.get("nopo");
        var nobayar             = obj.get("nobayar");
        var tglbayar             = obj.get("tglbayar");
        var kdsupplier       = obj.get("kdsupplier");
        var nmsupplier       = obj.get("nmsupplier");
        var status_posting   = (kdjurnal == '-') ? 0 : 1;

        //tambahkan nopo & tglpo pada hidden field
        Ext.getCmp("nobayar_posting").setValue(nobayar);
        Ext.getCmp("tglbayar_posting").setValue(tglbayar);

        //reload detail jurnal
        ds_detail_jurnal.setBaseParam('nobayar', nobayar);
        ds_detail_jurnal.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_detail_jurnal.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.total_debit").setValue(total_debit);            
            Ext.getCmp("info.total_kredit").setValue(total_kredit);

          }
        });

        //reload detail Pembayaran
        ds_detail_pembayaran.setBaseParam('nobayar', nobayar);
        ds_detail_pembayaran.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total Pembayaran
            total_bayar = 0;
            
            ds_detail_pembayaran.each(function (rec) { 
              total_bayar += parseFloat(rec.get('nominal')); 
            });

            Ext.getCmp("info.total_bayar").setValue(total_bayar);
            
            Ext.getCmp("tf.no_jurnal").setValue(kdjurnal);
            Ext.getCmp("tf.nobayar").setValue(nobayar);
            Ext.getCmp("tf.kd_supplier").setValue(kdsupplier);
            Ext.getCmp("tf.nopo").setValue(nopo);
            Ext.getCmp("tf.nama_supplier").setValue(nmsupplier);            
            Ext.getCmp("tf.total_bayar").setValue(total_bayar);

          }
        });
        
        //disable or enable button posting
        if(status_posting == 0)
        {
          Ext.getCmp('btn.posting').enable();
        
        }else if(status_posting == 1){
          Ext.getCmp('btn.posting').disable();
        
        }

      }
      return true;
  }
  
  
}