function jurn_supplier_retur(){
  Ext.form.Field.prototype.msgTarget = 'side';
  
  var ds_list_retur = dm_jurnsupplier_retur();
  var ds_detail_retur = dm_jurnsupplier_returdet();
  var ds_detail_jurnal = dm_jurnsupplier_retur_jurnaling();
      ds_detail_jurnal.setBaseParam('noretursupplier','null');
  
  var cm_list_retur = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('No. Retur'),
      width: 90,
      dataIndex: 'noretursupplier',
      align:'center',
      renderer: fnkeyShowDetailTransaksi
    },{
      header: headerGerid('Supplier'),
      width: 140,
      dataIndex: 'nmsupplier',
      align:'left'
    },{
      header: headerGerid('Total Retur'),
      width: 95,
      dataIndex: 'total_nominal_retur',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Posting'),
      width: 60,
      dataIndex: 'kdjurnal',
      align:'left',
      renderer: fnkeyshowPostingStatus,
    }],
  });

  var cm_detail_retur = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('No. PO'),
      width: 80,
      dataIndex: 'nopo',
      align:'center',
    },{
      header: headerGerid('Nama Barang'),
      width: 135,
      dataIndex: 'nmbrg',
      align:'center',
    },{
      header: headerGerid('Qty Beli'),
      width: 50,
      dataIndex: 'qty',
      align:'center',
    },{
      header: headerGerid('Qty<br/>Retur'),
      width: 50,
      dataIndex: 'qtyretur',
      align:'center',
    },{
      header: headerGerid('Harga Beli'),
      width: 75,
      dataIndex: 'hargabeli',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Diskon<br/>(%)'),
      width: 50,
      dataIndex: 'diskon',
      align:'center',
    },{
      header: headerGerid('PPN<br/>(%)'),
      width: 50,
      dataIndex: 'ppn',
      align:'center',
    },{
      header: headerGerid('Subtotal<br/>Retur'),
      width: 80,
      dataIndex: 'totretur_diskon_ppn',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },]
  });

  var cm_detail_jurnal = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 80,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 150,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 100,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 110,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 110,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
    
  var grid_list_retur = new Ext.grid.GridPanel({
    id: 'grid_list_retur',
    store: ds_list_retur,
    title: 'Data Retur',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_list_retur,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:15px',
    listeners: {
      cellclick: onClickListTransaksi
    }
  });

  var grid_detail_retur = new Ext.grid.GridPanel({
    id: 'grid_detail_retur',
    store: ds_detail_retur,
    title: 'Detail Retur',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_retur,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:15px',
    bbar: [
      '->',
      {
        xtype: 'label', id: 'lb.totalr', 
        text: 'Total : ', //margins: '5 5 0 125',
      },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_retur',
        width:100,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });


  var grid_detail_jurnal = new Ext.grid.GridPanel({
    id: 'grid_detail_jurnal',
    store: ds_detail_jurnal,
    title: 'Detail Jurnal',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_jurnal,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:15px',
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_debit',
        width:110,
        readOnly:true,
        value:'0',
        align:'right',
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_kredit',
        width:110,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });
       
  var form_bp_general = new Ext.FormPanel({
    id: 'form_bp_general',
    title: 'Jurnal Khusus (Retur Barang Supplier)', 
    iconCls:'silk-money',
    width: 900, Height: 1000,
    autoScroll: true,
    layout: {
      type: 'form',
      pack: 'center',
      align: 'center'
    },
    frame: true,
    items: [
    {
      layout: 'form',
      border: false,
      items: [{
        id: 'filter_jurnal',
        xtype: 'container',
        style: 'padding: 5px',
        defaults: {labelWidth: 100, labelAlign: 'right'},
        items:[{
          xtype: 'fieldset',
          //title: 'Filter',
          height: 110,
          layout: 'column',
          style: 'padding-top:15px',
          items: [{
            layout: 'form', 
            columnWidth: 0.6,
            items: [{
              fieldLabel: 'Tanggal Jurnal',
              xtype: 'datefield',
              id: 'tgl_jurnal',
              value: new Date(),
              format: "d/m/Y",
              width: 100, 
              disabled: true,
            },{
              fieldLabel: 'Tanggal Retur',
              xtype: 'datefield',
              id: 'tgl_transaksi',
              value: new Date(),
              format: "d/m/Y",
              width: 100, 
              listeners:{
                select: function(field, newValue){
                  fnSearchTransaksi();
                },
                change : function(field, newValue){
                  fnSearchTransaksi();
                }
              }
            }]
          },{
            layout: 'form', 
            columnWidth: 0.3,
            items: [{
              fieldLabel: 'Jenis Transaksi',
              xtype: 'textfield',
              id: 'jns_trans',
              value: 'Retur Barang',
              width: 150, 
              disabled: true,
            },{
              fieldLabel: 'USER ID',
              xtype: 'textfield',
              id: 'user_id',
              value: USERID,
              width: 150,
              disabled: true,
            },{
              xtype: 'textfield',
              id: 'noretur_posting',
              value: '',
              hidden: true,
            },{
              xtype: 'textfield',
              id: 'tglretur_posting',
              value: '',
              hidden: true,
            },{
                xtype: 'button',
                text: 'Posting',
                id: 'btn.posting',
                iconCls: 'silk-save',
                width: 100,
                height: 25,
                disabled: true,
                style: 'margin-left:105px',
                handler: function() {
                  var noretur_posting = Ext.getCmp("noretur_posting").getValue();
                  var tglretur_posting = Ext.getCmp("tglretur_posting").getValue();
                  var nmsupplier = Ext.getCmp("tf.nama_supplier").getValue();
                  var nominal_jurnal = Ext.getCmp("info.total_debit").getValue(); //debit
                  var nominal_jurnal_kredit = Ext.getCmp("info.total_kredit").getValue(); //kredit            
                  
                  if(noretur_posting == '' || tglretur_posting == ''){
                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
                  }
                  else if(nominal_jurnal != nominal_jurnal_kredit){
                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
                  }
                  else{
                    var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
                    var post_grid ='';
                    var count= 1;
                    var endpar = ';';
                    
                    grid_detail_jurnal.getStore().each(function(rec){ // ambil seluruh grid prodi
                      var rowData = rec.data; 
                      if (count == grid_detail_jurnal.getStore().getCount()) {
                        endpar = ''
                      }
                      post_grid += rowData['idakun'] + '^' + 
                             rowData['kdakun'] + '^' + 
                             rowData['nmakun']  + '^' + 
                             rowData['noreff']  + '^' + 
                             rowData['debit'] + '^' + 
                             rowData['kredit']  + '^' +
                          endpar;
                                  
                      count = count+1;
                    });

                    Ext.Ajax.request({
                      url: BASE_URL + 'jurn_transaksi_supplier_controller/posting_jurnal_supplier_retur',
                      params: {
                        noretur_posting    : noretur_posting,
                        tglretur_posting   : tglretur_posting,
                        nmsupplier      : nmsupplier,
                        nominal_jurnal  : nominal_jurnal,
                        post_grid       : post_grid,
                        userid          : USERID,
                      },
                      success: function(response){
                        waitmsg.hide();

                        obj = Ext.util.JSON.decode(response.responseText);
                        if(obj.success === true){
                          Ext.getCmp('btn.posting').disable();
                        }else{
                          Ext.getCmp('btn.posting').enable();  
                        }

                        Ext.MessageBox.alert('Informasi', obj.message);
                        
                        ds_list_retur.reload();
                        ds_detail_retur.reload();
                        ds_detail_jurnal.reload();
                      }
                    });
                   
                  }

                }
              }
            ]
          }]
        }]
      
      },

      // ====================================== CONTAINER DATA GRID ====================================================//
      {
        id: 'container_data_grid',
        xtype: 'container',
        style: 'padding: 5px',
        defaults: {labelWidth: 100, labelAlign: 'right'},
        items:[{
          xtype: 'fieldset',
          id: 'panel_data_grid',
          //height: 100,
          layout: 'column',
          items: [{
            id: 'colom_grid_left',
            style: 'margin-right:10px',
            layout: 'form', 
            columnWidth: 0.42,
            items: [
              // ============================== panel left side (transaksi & form trans) ===============================//
              {
                xtype: 'fieldset',
                border: false,
                style: 'padding:0px',
                id: 'panel_detail_transaksi',
                labelWidth: 160, labelAlign: 'right',
                //layout: 'column',
                //defaults: {labelWidth: 100, labelAlign: 'right'},
                items: [grid_list_retur, {
                  xtype: 'textfield', 
                  id: 'tf.no_jurnal',
                  fieldLabel: 'No. Jurnal ',
                  width: 245,
                  readOnly: true,
                },{
                  xtype: 'compositefield',
                  fieldLabel: 'Kode Supplier / No. Retur ',
                  items: [{
                    xtype: 'textfield',
                    id: 'tf.kd_supplier',
                    width: 90,
                    readOnly: true,
                  },{
                    xtype: 'label', id: 'lb.slash',
                    text: ' / ', margins: '3 10 0 5',
                  },{
                    xtype: 'textfield',
                    id: 'tf.no_retur',
                    width: 130,
                    readOnly: true,
                  }]
                },{
                  xtype: 'textfield', 
                  id: 'tf.nama_supplier',
                  fieldLabel: 'Nama Supplier ',
                  width: 245,
                  readOnly: true,
                },{
                  xtype: 'numericfield',
                  thousandSeparator:',',
                  id: 'tf.diskon',
                  fieldLabel: 'Diskon',
                  width: 245,
                  readOnly: true,
                },{
                  xtype: 'numericfield',
                  thousandSeparator:',', 
                  id: 'tf.ppn',
                  fieldLabel: 'PPN ',
                  width: 245,
                  readOnly: true,
                },{
                  xtype: 'numericfield',
                  thousandSeparator:',',
                  id: 'tf.total_retur',
                  fieldLabel: 'Total Nominal Retur ',
                  width: 245,
                  readOnly: true,
                }]
              }]
          },
          {
            id: 'colom_grid_right',
            //title: 'Data Jurnal',
            layout: 'form',
            border: false,
            style: 'padding:0px',     
            columnWidth: 0.57,
            items: [
              grid_detail_retur,
              grid_detail_jurnal
            ]
          }]
        }]
      },
      // ========================================== END CONTAINER DATA GRID ==========================================//

      
      ]
    }]
  });
  SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
  
  function fnSearchTransaksi(){
    ds_list_retur.setBaseParam('tglretursupplier', Ext.getCmp('tgl_transaksi').getValue().format('Y-m-d'));
    ds_list_retur.load();
  }


  function fnkeyShowDetailTransaksi(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }

  function fnkeyshowPostingStatus(value){
   Ext.QuickTips.init();
    if(value == '-'){
      return 'belum';
    }else{
      return 'sudah';
    }
  }

  function onClickListTransaksi(grid, rowIndex, columnIndex, event) {
      var t = event.getTarget();
      var record = grid.getStore().getAt(rowIndex);
      
      if (t.className == 'keyMasterDetail')
      {
        var obj               = ds_list_retur.getAt(rowIndex);
        var kdjurnal          = obj.get("kdjurnal");
        var noretursupplier   = obj.get("noretursupplier");
        var tglretursupplier  = obj.get("tglretursupplier");
        var kdsupplier        = obj.get("kdsupplier");
        var nmsupplier        = obj.get("nmsupplier");
        var status_posting    = (kdjurnal == '-') ? 0 : 1;

        //tambahkan retur & tglretur pada hidden field
        Ext.getCmp("noretur_posting").setValue(noretursupplier);
        Ext.getCmp("tglretur_posting").setValue(tglretursupplier);

        //reload detail jurnal
        ds_detail_jurnal.setBaseParam('noretursupplier', noretursupplier);
        ds_detail_jurnal.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_detail_jurnal.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.total_debit").setValue(total_debit);            
            Ext.getCmp("info.total_kredit").setValue(total_kredit);

          }
        });
        

        //reload detail retur
        ds_detail_retur.setBaseParam('noretursupplier', noretursupplier);
        ds_detail_retur.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total retur
            total_retur = 0;
            total_diskon = 0;
            total_ppn = 0;
            total_terima = 0;
            
            ds_detail_retur.each(function (rec) { 
              total_retur += parseFloat(rec.get('totretur')); 
              total_diskon += parseFloat(rec.get('totdiskon')); 
              total_ppn += parseFloat(rec.get('beban_ppn')); 
              total_terima += parseFloat(rec.get('totretur_diskon_ppn')); 
            });
            
            Ext.getCmp("tf.no_jurnal").setValue(kdjurnal);
            Ext.getCmp("info.total_retur").setValue(total_terima);
            Ext.getCmp("tf.kd_supplier").setValue(kdsupplier);
            Ext.getCmp("tf.no_retur").setValue(noretursupplier);
            Ext.getCmp("tf.nama_supplier").setValue(nmsupplier);
            
            //Ext.getCmp("tf.total_beli").setValue(total_beli);
            Ext.getCmp("tf.diskon").setValue(total_diskon);
            Ext.getCmp("tf.ppn").setValue(total_ppn);
            Ext.getCmp("tf.total_retur").setValue(total_terima);

          }
        });
        
        //disable or enable button posting
        if(status_posting == 0)
        {
          Ext.getCmp('btn.posting').enable();
        
        }else if(status_posting == 1){
          Ext.getCmp('btn.posting').disable();
        
        }

      }
      return true;
  }
  
  
}