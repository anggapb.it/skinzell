function Hartatetapmenyusut(){

  //automated calculate penyusutan
  Ext.Ajax.request({
    url: BASE_URL + 'hartatetap_controller/kalkulasi_penyusutan',
    params: {
      calculate:1
    },
    success: function(response){}
  });

  var pageSize = 18;
  var ds_hartatetap = dm_hartatetapmenyusut();
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
  
  var paging = new Ext.PagingToolbar({
    pageSize: pageSize,
    store: ds_hartatetap,
    mode: 'remote',
    displayInfo: true,
    displayMsg: 'Data Harta Tetap {0} - {1} dari total {2}',
    emptyMsg: 'No data to display'
  });
  
  var grid_hartatetap = new Ext.grid.GridPanel({
    id: 'grid_hartatetap',
    store: ds_hartatetap,
    autoScroll: true,
    frame: true,
    //view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    tbar: [{
      text: 'Tambah',
      id: 'btn_add',
      iconCls: 'silk-add',
      style: 'margin-right:5px',
      handler: function() {
        fnAddHartaTetap();
      }
    },{
      text: 'Cetak PDF',
      id: 'btn_cetak',
      iconCls: 'silk-printer',
      style: 'margin-right:5px',
      handler: function() {
        RH.ShowReport(BASE_URL + 'hartatetap_controller/hartatetap_menyusut_pdf/');
      }
    },{
      text: 'Cetak Excel',
      id: 'btn_cetak_excel',
      iconCls: 'silk-printer',
      handler: function() {
        RH.ShowReport(BASE_URL + 'hartatetap_controller/hartatetap_menyusut_excel/');
      }
    }],
    bbar: paging,
    height: 530,
    columnLines: true,
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Nama Harta'),
      width: 160,
      dataIndex: 'nmhartatetap',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Kelompok Harta'),
      width: 110,
      dataIndex: 'nmkelharta',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Tanggal Beli'),
      width: 80,
      dataIndex: 'tglbeli',
      sortable: true,
      align:'center',
      renderer: Ext.util.Format.dateRenderer('d/m/Y'),
    },{
      header: headerGerid('Harga Beli'),
      width: 90,
      dataIndex: 'hrgbeli',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Jumlah'),
      width: 70,
      dataIndex: 'jml',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Total Beli'),
      width: 90,
      dataIndex: 'totalbeli',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Umur Terpakai<br>(Bulan)'),
      width: 95,
      dataIndex: 'umurterpakai2',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Umur Ekonomis<br>(Bulan)'),
      width: 95,
      dataIndex: 'umurekonomi',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('@ Residu'),
      width: 80,
      dataIndex: 'residu',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Akumulasi<br/>Penyusutan'),
      width: 95,
      dataIndex: 'akumulasipenyusutan',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Nilai Harta'),
      width: 100,
      dataIndex: 'nilaiharta',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      xtype: 'actioncolumn',
      width: 50,
      header: 'Edit',
      align:'center',
      items: [{
        getClass: function(v, meta, record) {
          meta.attr = "style='cursor:pointer;'";
        },
        icon   : 'application/framework/img/rh_edit.png',
        tooltip: 'Edit record',
        handler: function(grid, rowIndex) {
            fnEditHartaTetap(grid, rowIndex);
        }
      }]
    },{
      xtype: 'actioncolumn',
      width: 50,
      header: 'Hapus',
      align:'center',
      items: [{
        getClass: function(v, meta, record) {
          meta.attr = "style='cursor:pointer;'";
        },
        icon   : 'application/framework/img/rh_delete.gif',
        tooltip: 'Hapus record',
        handler: function(grid, rowIndex) {
          fnDeleteHartaTetap(grid, rowIndex);
        }
      }]
    }],
    viewConfig:{
      getRowClass: function(rec, rowIdx, params, store){
        var umurterpakai2 = rec.get('umurterpakai2');
        var umurekonomi = rec.get('umurekonomi');
        if(parseInt(umurterpakai2) >= parseInt(umurekonomi)){
         params.tstyle += "background-color:#fffb90;";          
        }
      }
    }
  }); 
  
  var form_bp_general = new Ext.form.FormPanel({
    id: 'form_bp_general',
    title: 'Harta Tetap (Menyusut)', iconCls:'silk-money',
    width: 900, Height: 1000,
    layout: {
      type: 'form',
      pack: 'center',
      align: 'center'
    },
    frame: true,
    autoScroll: true,
    items: [grid_hartatetap]
  });
  SET_PAGE_CONTENT(form_bp_general);
  
  function fnAddHartaTetap(){
    var grid = ds_hartatetap;
    wEntryHartaTetap(false, grid, null);  
  }

  function fnEditHartaTetap(grid, record){
    var record = ds_hartatetap.getAt(record);
    wEntryHartaTetap(true, grid, record);  
  }
  
  function fnDeleteHartaTetap(grid, record){
    var record = ds_hartatetap.getAt(record);
    var url = BASE_URL + 'hartatetap_controller/delete_hartatetap';
    var params = new Object({
      idhartatetap  : record.get('idhartatetap')
    });
    RH.deleteGridRecord(url, params, grid );
  }
  
  function wEntryHartaTetap(isUpdate, grid, record){
    
    var ds_kelharta = dm_kelharta();
        ds_kelharta.setBaseParam('fields','["jenisharta"]');
        ds_kelharta.setBaseParam('query','1');
        ds_kelharta.reload();

    var winTitle = (isUpdate)?'Harta Tetap (Edit)':'Harta Tetap (Entry)';
    var hartatetap_form = new Ext.form.FormPanel({
      xtype:'form',
      id: 'frm.hartatetap',
      buttonAlign: 'left',
      labelWidth: 120, labelAlign: 'right',
      monitorValid: true,
       width: 800,
      layout: {
        type: 'form',
        pack: 'center',
        align: 'center'
      },
      frame: true,
      tbar: [{
        text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
        handler: function() {
          fnSaveHartaTetap();
        }
      },{
        text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
        handler: function() {
          reset_form_insert();
          wHartaTetap.close();
        }
      }], 
      items: [{
        xtype: 'fieldset', title: '', layout: 'column', style: 'marginTop: 5px',
        items: [{
          columnWidth: 0.49, border: false, layout: 'form',         
          items: [{
            xtype: 'textfield',
            fieldLabel: 'ID Harta tetap ',
            id:'tf.idhartatetap',
            hidden:true,
          },{
            xtype: 'textfield',
            fieldLabel: 'Nama Harta ',
            id:'tf.namaharta',
            width: 200,
            allowBlank: false, 
          },{
            xtype: 'combo',
            id: 'tf.idkelharta', 
            fieldLabel: 'Kelompok Harta ',
            width: 200, 
            allowBlank: false, 
            store: ds_kelharta,
            triggerAction: 'all',
            editable: false,
            valueField: 'idkelharta',
            displayField: 'nmkelharta',
            forceSelection: true,
            submitValue: true,
            mode: 'local',
            emptyText:'Pilih...',       
          },{
            fieldLabel: 'Tanggal Beli ',
            xtype: 'datefield',
            id: 'df.tglbeli',
            format: 'd/m/Y',
            value: new Date(),
            width: 120,
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
              change : function(field, newValue){
                fnKalkulasiForm();
              }
            }
          },{
            fieldLabel: '@ Harga Beli ',
            xtype: 'numericfield',
            thousandSeparator:',',
            id: 'tf.hrgbeli',
            width: 200,
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
              change : function(field, newValue){
                fnKalkulasiForm();
              }
            }
          },{
            fieldLabel: 'Jumlah ',
            xtype: 'numericfield',
            thousandSeparator:',',
            id: 'tf.jml',
            width: 200,
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
              change : function(field, newValue){
                fnKalkulasiForm();
              }
            }

          },{
            fieldLabel: '@ Nilai Residu ',
            xtype: 'numericfield',
            thousandSeparator:',',
            id: 'tf.residu',
            width: 200,
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
              change : function(field, newValue){
                fnKalkulasiForm();
              }
            }

          },{
            fieldLabel: 'Umur Ekonomis ',
            xtype: 'numericfield',
            thousandSeparator:',',
            id: 'tf.umurekonomis',
            width: 200,
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
              change : function(field, newValue){
                fnKalkulasiForm();
              }
            }

          }]
        },
        {
          columnWidth: 0.50, border: false, layout: 'form',labelWidth: 180, labelAlign: 'right',
          items: [{
            xtype: 'numericfield',
            thousandSeparator:',',
            fieldLabel: 'Total Pembelian ',
            id:'tf.totalbeli',
            width: 140,
            readOnly: true,
            style : 'opacity:0.6'
          },{
            fieldLabel: '@ Penyusutan ',
            xtype: 'compositefield',
            items: [{
              xtype: 'textfield',
              id:'tf.penyusutan',
              width: 100,
              readOnly: true,
              value: 0,
              style : 'opacity:0.6'
            },{
              xtype: 'label', text: ' /Bulan', margins: '4 5 0 2'
            }]
          },{
            fieldLabel: 'Total Penyusutan ',
            xtype: 'compositefield',
            items: [{
              xtype: 'textfield',
              id:'tf.totalpenyusutan',
              width: 100,
              readOnly: true,
              value: 0,
              style : 'opacity:0.6'
            },{
              xtype: 'label', text: ' /Bulan', margins: '4 5 0 2'
            }]
          },{
            fieldLabel: 'Akumulasi Umur Terpakai ',
            xtype: 'compositefield',
            items: [{
              xtype: 'textfield',
              id:'tf.umurterpakai',
              width: 100,
              readOnly: true,
              value: 0,
              style : 'opacity:0.6'
            },{
              xtype: 'label', text: ' Bulan', margins: '4 5 0 5'
            }]
          },{
            xtype: 'numericfield',
            thousandSeparator:',',
            fieldLabel: 'Akumulasi Penyusutan ',
            id:'tf.akumulasipenyusutan',
            width: 140,
            readOnly: true,
            style : 'opacity:0.6'
          },{
            xtype: 'numericfield',
            thousandSeparator:',',
            fieldLabel: 'Nilai Harta ',
            id:'tf.nilaiharta',
            width: 140,
            readOnly: true,
            style : 'opacity:0.6'
          }]
        }]
      }]
    });
    
    var wHartaTetap = new Ext.Window({
      title: winTitle,
      modal: true, closable:false,
      items: [hartatetap_form]
    });
  
    setHartaTetapForm(isUpdate, record);
    wHartaTetap.show();
    
    function fnSaveHartaTetap(){
      
      var idhartatetap        = Ext.getCmp('tf.idhartatetap').getValue(); 
      var namaharta           = Ext.getCmp('tf.namaharta').getValue(); 
      var idkelharta          = Ext.getCmp('tf.idkelharta').getValue(); 
      var tglbeli             = Ext.getCmp('df.tglbeli').getValue().format('Y-m-d'); 
      var hrgbeli             = Ext.getCmp('tf.hrgbeli').getValue(); 
      var jml                 = Ext.getCmp('tf.jml').getValue(); 
      var residu              = Ext.getCmp('tf.residu').getValue(); 
      var umurekonomis        = Ext.getCmp('tf.umurekonomis').getValue(); 
      var totalbeli           = Ext.getCmp('tf.totalbeli').getValue(); 
      var penyusutan          = Ext.getCmp('tf.penyusutan').getValue(); 
      var totalpenyusutan     = Ext.getCmp('tf.totalpenyusutan').getValue(); 
      var umurterpakai        = Ext.getCmp('tf.umurterpakai').getValue(); 
      var akumulasipenyusutan = Ext.getCmp('tf.akumulasipenyusutan').getValue(); 
      var nilaiharta          = Ext.getCmp('tf.nilaiharta').getValue(); 
      if(residu == '') residu = 0;

      if(namaharta == '' || idkelharta == '' || tglbeli == '' || hrgbeli == '' || jml == ''){
        Ext.MessageBox.alert('Informasi','Data yang dimasukkan belum lengkap');
        return;
      }

      var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');

      if(isUpdate){
        //update data
        Ext.Ajax.request({
          url: BASE_URL + 'hartatetap_controller/update_hartatetapmenyusut',
          params: {
            idhartatetap        : idhartatetap,
            namaharta           : namaharta, 
            idkelharta          : idkelharta, 
            tglbeli             : tglbeli, 
            hrgbeli             : hrgbeli, 
            jml                 : jml, 
            residu              : residu, 
            umurekonomis        : umurekonomis, 
            totalbeli           : totalbeli, 
            penyusutan          : penyusutan, 
            totalpenyusutan     : totalpenyusutan, 
            umurterpakai        : umurterpakai, 
            akumulasipenyusutan : akumulasipenyusutan, 
            nilaiharta          : nilaiharta, 
            userid              : USERID,
          },
          success: function(response){
            waitmsg.hide();
            obj = Ext.util.JSON.decode(response.responseText);
            if(obj.success === true){
              Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
              reset_form_insert();
              wHartaTetap.close();
            }else{
              Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
            }
          }
        });
        
      }else{
        
        //insert data
        Ext.Ajax.request({
        url: BASE_URL + 'hartatetap_controller/insert_hartatetapmenyusut',
         params: {
            namaharta           : namaharta, 
            idkelharta          : idkelharta, 
            tglbeli             : tglbeli, 
            hrgbeli             : hrgbeli, 
            jml                 : jml, 
            residu              : residu, 
            umurekonomis        : umurekonomis, 
            totalbeli           : totalbeli, 
            penyusutan          : penyusutan, 
            totalpenyusutan     : totalpenyusutan, 
            umurterpakai        : umurterpakai, 
            akumulasipenyusutan : akumulasipenyusutan, 
            nilaiharta          : nilaiharta, 
            userid              : USERID,
          },
          success: function(response){
            waitmsg.hide();
            obj = Ext.util.JSON.decode(response.responseText);
            if(obj.success === true){
              Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
              reset_form_insert();
              wHartaTetap.close();
            }else{
              Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
            }
          }
        });
      }
         
    }
    
    function setHartaTetapForm(isUpdate, record){
      if(isUpdate){
        RH.setCompValue('tf.idhartatetap', record.get('idhartatetap'));
        RH.setCompValue('tf.namaharta', record.get('nmhartatetap'));
        RH.setCompValue('tf.idkelharta', record.get('idkelharta'));
        RH.setCompValue('df.tglbeli', record.get('tglbeli'));
        RH.setCompValue('tf.hrgbeli', record.get('hrgbeli')); 
        RH.setCompValue('tf.jml', record.get('jml')); 
        RH.setCompValue('tf.residu', record.get('residu')); 
        RH.setCompValue('tf.umurekonomis', record.get('umurekonomi')); 
        fnKalkulasiForm();
        return;
      }
    }


    function fnKalkulasiForm()
    {
      var tglbeli = Ext.getCmp('df.tglbeli').getValue().format('Y, m, d');
      var hrgbeli = Ext.getCmp('tf.hrgbeli').getValue(); 
      var jml = Ext.getCmp('tf.jml').getValue(); 
      var residu = Ext.getCmp('tf.residu').getValue(); 
      var umurekonomis = Ext.getCmp('tf.umurekonomis').getValue(); 
      var totalbeli; 
      var penyusutan = 0;
      var totalpenyusutan = 0;
      var umurterpakai = 0;
      var akumulasipenyusutan = 0;
      var nilaiharta = 0;
      
      if(residu == '') residu = 0;
      if(hrgbeli == '') hrgbeli = 0;
      if(jml == '') jml = 1;

      //akumulasi umur terpakai
      var bulan;
      var tglbeli = new Date(tglbeli);
      var sekarang = new Date();
      bulan = (sekarang.getFullYear() - tglbeli.getFullYear()) * 12;
      bulan -= tglbeli.getMonth() ;
      bulan += sekarang.getMonth();
      if(tglbeli.getDate() > sekarang.getDate()) bulan -= 1;
      bulan =  (bulan <= 0) ? 0 : bulan;

      Ext.getCmp('tf.umurterpakai').setValue(bulan); 
      
      //hitung total beli
      
      totalbeli = parseInt(hrgbeli) * parseInt(jml);
      Ext.getCmp('tf.totalbeli').setValue(totalbeli); 

      //hitung @penyusutan
      if(umurekonomis != '')
        penyusutan = (parseInt(hrgbeli) - parseInt(residu)) / parseInt(umurekonomis);
      Ext.getCmp('tf.penyusutan').setValue(penyusutan); 

      //hitung total penyusutan
      if(penyusutan > 0 && jml != '')
        totalpenyusutan = parseInt(penyusutan) * parseInt(jml);
      Ext.getCmp('tf.totalpenyusutan').setValue(totalpenyusutan); 

      //hitung akumulasi penyusutan
      akumulasipenyusutan = parseInt(totalpenyusutan) * parseInt(bulan);
      Ext.getCmp('tf.akumulasipenyusutan').setValue(akumulasipenyusutan); 

      //hitung nilai harta
      /*
      if(hrgbeli != '' && jml != '' && akumulasipenyusutan != '')
        nilaiharta = ((parseInt(hrgbeli) - parseInt(residu)) * parseInt(jml)) - parseInt(akumulasipenyusutan);
      */
      //sepertinya nilai harta tidak perlu dikurangi residu 
      if(hrgbeli != '' && jml != '' && akumulasipenyusutan != '')
        nilaiharta = (parseInt(hrgbeli) * parseInt(jml)) - parseInt(akumulasipenyusutan);
      
      Ext.getCmp('tf.nilaiharta').setValue(nilaiharta); 

    }

  }
  
  function reset_form_insert()
  {
    Ext.getCmp('tf.namaharta').setValue(); 
    Ext.getCmp('tf.idkelharta').setValue(); 
    Ext.getCmp('df.tglbeli').setValue(new Date()); 
    Ext.getCmp('tf.hrgbeli').setValue(); 
    Ext.getCmp('tf.jml').setValue(); 
    Ext.getCmp('tf.totalbeli').setValue(); 
    Ext.getCmp('tf.umurterpakai').setValue(); 

    ds_hartatetap.reload();
  }
}
