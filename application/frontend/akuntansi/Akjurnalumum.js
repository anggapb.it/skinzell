function Akjurnalumum(){
	var pageSize = 50;
	var static_tgl_transaksi = new Date();
	var ds_jurnal_umum = dm_jurnal_umum();
	var ds_jurnal_umum_det = dm_jurnal_umum_det();
		ds_jurnal_umum_det.setBaseParam('kdjurnal','null');
	var ds_jurnal_umum_det_entry = dm_jurnal_umum_det(); // grid jurnal umum saat entry data
		ds_jurnal_umum_det_entry.setBaseParam('kdjurnal','null');
	
	var arr_cari = [
		['kdjurnal', 'Kode Jurnal'],
		['nominal', 'Nominal'],
		['noreff', 'No. Reff / Bon'],
		['keterangan', 'Keterangan'],
		['tgljurnal', 'Tanggal Input'],
	];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_jurnal_umum,
		mode: 'remote',
		displayInfo: true,
		displayMsg: 'Data Jurnal Umum {0} - {1} dari total {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_jurnalumum = new Ext.grid.GridPanel({
		id: 'grid_jurnalumum',
		store: ds_jurnal_umum,
		autoScroll: true,
		frame: true,
		view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
		loadMask: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddJurnalUmum();
			}
		},
		'->',
		{
			xtype: 'compositefield',
			width: 930,
			items: [{
				xtype: 'checkbox',
				id: 'chb.periode', margins: '0 3 0 90',
				listeners: {
					check: function(checkbox, val){
						if(val == true){
							Ext.getCmp('tglawal').enable();
							Ext.getCmp('tglakhir').enable();
						} else if(val == false){
							Ext.getCmp('tglawal').disable();
							Ext.getCmp('tglakhir').disable();
							Ext.getCmp('tglawal').setValue(new Date());
							Ext.getCmp('tglakhir').setValue(new Date());							
							fnSearchgrid();
						}
					}
				}
			},{
				xtype: 'label', id: 'lb.lb', text: 'Tgl. Transaksi :', margins: '6 10 0 0',
			},{
				xtype: 'datefield',
				id: 'tglawal',
				value: new Date(),
				format: "d/m/Y",
				width: 100, disabled: true,
				listeners:{
					select: function(field, newValue){
						fnSearchgrid();
					},
					change : function(field, newValue){
						fnSearchgrid();
					}
				}
			},
			{
				xtype: 'label', id: 'lb.lbt', text: 's/d', margins: '6 4 0 0',
			},
			{
				xtype: 'datefield',
				id: 'tglakhir',
				value: new Date(),
				format: "d/m/Y",
				width: 100, disabled: true,
				listeners:{
					select: function(field, newValue){
						fnSearchgrid();
					},
					change : function(field, newValue){
						fnSearchgrid();
					}
				}
			},{
				xtype: 'label', text: 'Cari Berdasarkan :', margins: '6 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.search',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				value: 'kdjurnal',
				margins: '2 5 0 0',
				listeners: {
					select: function() {
						var cbsearch = Ext.getCmp('cb.search').getValue();
						var tfsearch = Ext.getCmp('tf.search').getValue();
						if(cbsearch != '' && tfsearch != ''){
							fnSearchgrid();
						}
						return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'tf.search',
				width: 160,
				margins: '2 5 0 0',
				validator: function(){
					fnSearchgrid();
				}
			}]
		}
		],
		bbar: paging,
		height: 278,
		columnLines: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('Kode Jurnal'),
			width: 120,
			dataIndex: 'kdjurnal',
			sortable: true,
			align:'center',
			renderer : fnkeyShowDetailJurnal
		},{
			header: headerGerid('Tgl Transaksi'),
			width: 100,
			dataIndex: 'tgltransaksi',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y'),
		},{
			header: headerGerid('Bagian'),
			width: 80,
			dataIndex: 'nmbagian',
			sortable: true,
			align:'left',
			hidden: true,
		},{
			header: headerGerid('No. Reff / No. Bon'),
			width: 110,
			dataIndex: 'noreff',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('Keterangan'),
			width: 200,
			dataIndex: 'keterangan',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('User Input'),
			width: 120,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('Tgl Input'),
			width: 90,
			//dataIndex: 'tglinput',
			dataIndex: 'tgljurnal',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y'),
		},{
			header: headerGerid('Nominal'),
			width: 100,
			dataIndex: 'nominal',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		}
		/*,{
			xtype: 'actioncolumn',
			width: 50,
			header: 'Edit',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_edit.png',
				tooltip: 'Edit record',
				handler: function(grid, rowIndex) {
					fnEditJurnalUmum(grid, rowIndex);
				}
			}]
		},{
			xtype: 'actioncolumn',
			width: 50,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					fnDeleteJurnalUmum(grid, rowIndex);
				}
			}]
		}
		*/
		],
		listeners: {
			cellclick: show_detail_jurnal
		}
	});	
	
	var grid_jurnaldetail = new Ext.grid.GridPanel({
		id: 'grid_jurnaldetail',
		store: ds_jurnal_umum_det,
		autoScroll: true,
		frame: true,
		height: 150,
		//autoHeight: true,
		columnLines: true,
		loadMask: true,
		view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('Kode'),
			width: 100,
			dataIndex: 'kdakun',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('Nama Akun'),
			width: 150,
			dataIndex: 'nmakun',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('No. Reff/ No. Bon'),
			width: 150,
			dataIndex: 'noreff',
			sortable: true,
			align:'left',
		},{
			header: headerGerid('Debit'),
			width: 150,
			dataIndex: 'debit',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Kredit'),
			width: 150,
			dataIndex: 'kredit',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		}],
	});	
	
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Jurnal Umum', iconCls:'silk-money',
		width: 900, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [
		{
			xtype: 'fieldset', 
			layout: 'form',
			height: 320,
			boxMaxHeight:330,
			items: [grid_jurnalumum]
		},
		{
			xtype: 'fieldset', 
			title: 'Detail Jurnal Umum',
			layout: 'form',
			height: 190,
			//boxMaxHeight:200,
			items: [grid_jurnaldetail]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
	
	function show_detail_jurnal(grid, rowIndex, columnIndex, event) {
	
		var t = event.getTarget();
		if (t.className == 'keyMasterDetail'){
			var obj = ds_jurnal_umum.getAt(rowIndex);
			var kdjurnal = obj.get("kdjurnal");
			ds_jurnal_umum_det.setBaseParam('kdjurnal',kdjurnal);
			ds_jurnal_umum_det.reload();
		}

	}
	
	function fnkeyShowDetailJurnal(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function fnSearchgrid(){
		var idcombo, nmcombo;
		
		if(Ext.getCmp('chb.periode').getValue() == true){
			ds_jurnal_umum.setBaseParam('tglawal', Ext.getCmp('tglawal').getValue().format('Y-m-d'));
			ds_jurnal_umum.setBaseParam('tglakhir', Ext.getCmp('tglakhir').getValue().format('Y-m-d'));
		}else if(Ext.getCmp('chb.periode').getValue() == false){
			ds_jurnal_umum.setBaseParam('tglawal','');
			ds_jurnal_umum.setBaseParam('tglakhir','');
		}
	
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('tf.search').getValue();
		ds_jurnal_umum.setBaseParam('searchkey',  idcombo);
		ds_jurnal_umum.setBaseParam('searchvalue',  nmcombo);
		ds_jurnal_umum.load();		
	}
	
	function fnAddJurnalUmum(){
		var grid = ds_jurnal_umum;
		wEntryJurnalUmum(false, grid, null);	
	}
	
	function fnEditJurnalUmum(grid, record){
		var record = ds_jurnal_umum.getAt(record);
		wEntryJurnalUmum(true, grid, record);		
	}
	
	function fnDeleteJurnalUmum(grid, record){
		var record = ds_jurnal_umum.getAt(record);
		var url = BASE_URL + 'jurnalumum_controller/delete_jurnal_umum';
		var params = new Object({
						kdjurnal	: record.get('kdjurnal')
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
	function wEntryJurnalUmum(isUpdate, grid, record){
		if(isUpdate){
			ds_jurnal_umum_det_entry.setBaseParam('kdjurnal',record.get('kdjurnal'));
			ds_jurnal_umum_det_entry.reload();
		}else{
			ds_jurnal_umum_det_entry.setBaseParam('kdjurnal','null');
			ds_jurnal_umum_det_entry.reload();
		}
		
		var grid_jurnalumumdet = new Ext.grid.EditorGridPanel({
		id: 'grid_jurnalumumdet',
		store: ds_jurnal_umum_det_entry,
		autoScroll: true,
		frame: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				//show akun 
				show_account_list();
			}
		},'->'],
		bbar: [
		{ xtype:'tbfill' },
		{
			xtype: 'fieldset',
			border: false,
			style: 'padding:0px; margin: 0px',
			width: 900,
			items: [{
				xtype: 'compositefield',
				items: [{
					xtype: 'label', id: 'lb.totalr', text: 'Jumlah :', margins: '5 5 0 125',
				},{
					xtype: 'numericfield',
					id: 'tf.jmhdebit',
					value: 0,
					width: 145,
					readOnly:true,
					style : 'opacity:0.6',
					thousandSeparator:',',
				},{
					xtype: 'numericfield',
					id: 'tf.jmhkredit',
					value: 0,
					width: 145,
					readOnly:true,
					style : 'opacity:0.6',
					thousandSeparator:',',
				}]
			}]
		}],
		height: 278,
		columnLines: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('Kode'),
			width: 100,
			//dataIndex: 'kdjurnal',
			dataIndex: 'kdakun',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Nama Akun'),
			width: 150,
			dataIndex: 'nmakun',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('No. Reff / No. Bon'),
			width: 150,
			dataIndex: 'noreff',
			sortable: true,
			align:'center',
			editor: {
				xtype: 'textfield',
				id: 'judet.noreff',
				enableKeyEvents: true,
			}
		},{
			header: headerGerid('Debit'),
			width: 150,
			dataIndex: 'debit',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
			editor: {
				xtype: 'numberfield',
				id: 'judet.debit',
				enableKeyEvents: true,
				listeners:{
					change:function(grid, fieldvalue, columnIndex){
						kalkulasi_debit_kredit();
					}
				}
			}
		},{
			header: headerGerid('Kredit'),
			width: 150,
			dataIndex: 'kredit',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
			editor: {
				xtype: 'numberfield',
				id: 'judet.kredit',
				enableKeyEvents: true,
				listeners:{
					change:function(){
						kalkulasi_debit_kredit();
					}
				}
			}
		},{
			xtype: 'actioncolumn',
			width: 50,
			dataIndex: 'kdakun', 
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					fnDeleteJurnalDet(grid, rowIndex);
				}
			}]
        }],
		listeners: {

		}
	});	
		
		var winTitle = (isUpdate)?'Jurnal Umum (Edit)':'Jurnal Umum (Entry)';
		var jurnalumum_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.jurnalumum',
			buttonAlign: 'left',
			labelWidth: 165, labelAlign: 'right',
			monitorValid: true,
			height: 555, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					fnSaveJurnalUmum();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					reset_form_insert();
					wJurnalUmum.close();
				}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 185, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.49, border: false, layout: 'form',					
					items: [{
						fieldLabel: 'Tgl Transaksi',
						xtype: 'datefield',
						id: 'df.tgltransaksi',
						format: 'd/m/Y',
						value: static_tgl_transaksi,
						width: 120,
						allowBlank: false,
						listeners: {
							change : function(field, newValue){
								fnsetStaticTgl();
							},
						}
					},{
						xtype: 'compositefield',
						style: 'marginLeft: -48px;',
						width: 325,
						items: [{
							xtype: 'label', id: 'lb.bgn', text: 'Bagian:', margins: '3 5 0 4',
						},{
							xtype: 'textfield',
							id: 'tf.bagian',
							value: 'Keuangan',
							width: 250,
							readOnly: true,
							style : 'opacity:0.6',
							allowBlank: false
						},{
							xtype: 'textfield',
							id: 'tf.idbagian',
							value: 47, // id bagian keuangan
							hidden: true,							
						}]
					},{
						xtype: 'textfield',
						fieldLabel: 'No. Reff/No. Bon',
						id:'tf.noreff',
						width: 250,
					},{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						id: 'ta.keterangan',
						width : 250,
						height: 73,
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'Kode Jurnal',
						id:'tf.kdjurnal',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield',
						fieldLabel: 'User Input',
						id: 'tf.userinput',
						width: 120,
						readOnly: true,
						value: USERNAME,
						style : 'opacity:0.6'						
					},{
						xtype: 'datefield',
						fieldLabel: 'Tgl. Input',
						id: 'df.tglinput',
						format: 'd/m/Y',
						value: new Date(),
						width: 120,
						disabled: true
					}
					/*,{
						xtype: 'numericfield',
						fieldLabel: 'Nominal',
						id: 'tf.nominal',
						value: 0,
						width: 120,
						thousandSeparator:',',
						allowBlank: false
					}*/
					]
				}]
			}
			,{
				xtype: 'fieldset',
				title: 'Daftar Jurnal Umum',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 318,
				items: [grid_jurnalumumdet]
			}
			]
		});
		
		var wJurnalUmum = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [jurnalumum_form]
		});
	
		setJurnalUmumForm(isUpdate, record);
		wJurnalUmum.show();
			
		function fnDeleteJurnalDet(grid, record){
			var record = ds_jurnal_umum_det_entry.getAt(record);
			ds_jurnal_umum_det_entry.remove(record);
			kalkulasi_debit_kredit();
			return;
		}
		
		function kalkulasi_debit_kredit(){
			var debit = 0;
			var kredit = 0;
			for (var zxc = 0; zxc < ds_jurnal_umum_det_entry.data.items.length; zxc++) {
				var record = ds_jurnal_umum_det_entry.data.items[zxc].data;
				if(record.debit){
					debit 	+= parseFloat(record.debit);
				}
				
				if(record.kredit){
					kredit	+= parseFloat(record.kredit);
				}
			}
			Ext.getCmp('tf.jmhdebit').setValue(debit);
			Ext.getCmp('tf.jmhkredit').setValue(kredit);
		}
		
		function fnSaveJurnalUmum(){
			var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
			var tgltransaksi = Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'); 
			var bagian = Ext.getCmp('tf.bagian').getValue(); 
			var idbagian = Ext.getCmp('tf.idbagian').getValue(); 
			var noreffjurnal = Ext.getCmp('tf.noreff').getValue(); 
			var keterangan = Ext.getCmp('ta.keterangan').getValue(); 
			var kdjurnal = Ext.getCmp('tf.kdjurnal').getValue(); 
			var userinput = Ext.getCmp('tf.userinput').getValue(); 
			var tglinput = Ext.getCmp('df.tglinput').getValue().format('Y-m-d'); 
			//var nominal = Ext.getCmp('tf.nominal').getValue(); 
			var nominal = Ext.getCmp('tf.jmhdebit').getValue(); 
			var jmhdebit = Ext.getCmp('tf.jmhdebit').getValue(); 
			var jmhkredit = Ext.getCmp('tf.jmhkredit').getValue(); 
			var arrjurnal = [];
			for(var zx = 0; zx < ds_jurnal_umum_det_entry.data.items.length; zx++){
				var record = ds_jurnal_umum_det_entry.data.items[zx].data;
				console.log(record);
				zidakun = record.idakun;
				znoreff = record.noreff;
				zdebit = record.debit;
				zkredit = record.kredit;
				arrjurnal[zx] = zidakun + '-' + znoreff + '-' + zdebit + '-' + zkredit ;
			}
			
			if(tgltransaksi == ''){
				Ext.MessageBox.alert('Informasi','Tanggal transaksi belum diisi..');
				return;
			}
			
			if(nominal == 0){
				Ext.MessageBox.alert('Informasi','Nominal belum diisi..');
				return;
			}
			
			if(jmhdebit != jmhkredit){
				Ext.MessageBox.alert('Informasi','Jumlah debit tidak sesuai dengan jumlah kredit');
				return;
			}
			
			if(isUpdate){
				//update data
				Ext.Ajax.request({
					url: BASE_URL + 'jurnalumum_controller/update_jurnal_umum',
					params: {
						kdjurnal		  :	kdjurnal,
						idbagian		  :	idbagian,
						tgltransaksi	:	tgltransaksi,
						tgljurnal		  :	tglinput,
						keterangan		:	keterangan,
						noreff			  :	noreffjurnal,
						userid			  :	USERID,
						nominal			  :	nominal,
						arrjurnal 		:	Ext.encode(arrjurnal)
					},
					success: function(response){
						waitmsg.hide();
						obj = Ext.util.JSON.decode(response.responseText);
						if(obj.success === true){
							Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
							reset_form_insert();
							wJurnalUmum.close();
						}else{
							Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
						}
					}
				});
				
			}else{
				
				//insert data
				Ext.Ajax.request({
					url: BASE_URL + 'jurnalumum_controller/insert_jurnal_umum',
					params: {
						//kdjurnal		:	RH.getCompValue('tf.kdjurnal'),
						idbagian		  :	idbagian,
						tgltransaksi	:	tgltransaksi,
						tgljurnal		  :	tglinput,
						keterangan		:	keterangan,
						noreff			  :	noreffjurnal,
						userid			  :	USERID,
						nominal			  :	nominal,
						arrjurnal 		:	Ext.encode(arrjurnal)
					},
					success: function(response){
						waitmsg.hide();
						obj = Ext.util.JSON.decode(response.responseText);
						if(obj.success === true){
							Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
							reset_form_insert();
							wJurnalUmum.close();
						}else{
							Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
						}
						
					}
				});
			}
			
			
		}
		
		function setJurnalUmumForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					RH.setCompValue('df.tgltransaksi', record.get('tgltransaksi'));
					//RH.setCompValue('cb.debit', record.get('idakundebit')); 
					//RH.setCompValue('cb.kredit', record.get('idakunkredit')); 			
					RH.setCompValue('tf.noreff', record.get('noreff')); 
					RH.setCompValue('ta.keterangan', record.get('keterangan')); 
					RH.setCompValue('tf.kdjurnal', record.get('kdjurnal')); 
					RH.setCompValue('tf.userinput', record.get('userid')); 
					RH.setCompValue('df.tglinput', record.get('tgljurnal')); 
					//RH.setCompValue('tf.nominal', record.get('nominal'));
					return;
				}
			}
		}

		
	function fnsetStaticTgl(){
			var stattgl = Ext.getCmp('df.tgltransaksi').getValue();
			static_tgl_transaksi = stattgl;
	}
	}
	
	function show_account_list(){
		var ds_akun = dm_akun_jurnal();
		
		//standard filter
		var cari_akun = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 150
		})];
		
		var grid_akun = new Ext.grid.GridPanel({
			id: 'grid_akun',
			store: ds_akun,
			autoScroll: true,
			frame: true,
			height:480,
			loadMask: true,
			tbar: ['->'],
			plugins : cari_akun,
			columnLines: true,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('Kode Akun'),
				width: 70,
				dataIndex: 'kdakun',
				sortable: true,
				align:'center',
				renderer: fnkeyAddAkun
			},{
				header: headerGerid('Kelompok Akun'),
				width: 130,
				dataIndex: 'nmklpakun',
				sortable: true,
				align:'left',
			},{
				header: headerGerid('Nama Akun'),
				width: 200,
				dataIndex: 'nmakun',
				sortable: true,
				align:'left',
			}],
			listeners: {
				cellclick: onCellClickAddAkun
			}
		});	
		
		var winAkunTitle = 'Daftar Akun';
		var akun_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.akun',
			buttonAlign: 'left',
			labelWidth: 165, labelAlign: 'right',
			monitorValid: true,
			height: 500, width: 500,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,	
			items: [grid_akun]
		});
			
		var wAkun = new Ext.Window({
			title: winAkunTitle,
			modal: true, closable:true,
			items: [akun_form]
		});
		wAkun.show();
		
		function fnkeyAddAkun(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		function onCellClickAddAkun(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
				var cek           = true;
				var obj           = ds_akun.getAt(rowIndex);
				var sidakun			  = obj.get("idakun");
				var skdakun    	 	= obj.get("kdakun");
				var snmakun    	 	= obj.get("nmakun");
				
				ds_jurnal_umum_det_entry.each(function(rec){
					if(rec.get('kdakun') == skdakun) {
						Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
						cek = false;
					}
				});
				
				if(cek){
					var orgaListRecord = new Ext.data.Record.create([
						{
							name: 'idakun',
							name: 'kdakun',
							name: 'nmakun',
							name: 'noreff',
							name: 'debit',
							name: 'kredit',
						}
					]);
					
					ds_jurnal_umum_det_entry.add([
						new orgaListRecord({
							'idakun'	: sidakun,
							'kdakun'	: skdakun,
							'nmakun'	: snmakun,
							'noreff'	: '',
							'debit'		: 0,
							'kredit'	: 0,
						})
					]);
					//win_find_cari_bagtahunan.close();
				}

				wAkun.close();

			}
			return true;
		}
	
	}
	
	function reset_form_insert()
	{
		Ext.getCmp('df.tgltransaksi').setValue(); 
		Ext.getCmp('tf.bagian').setValue(); 
		Ext.getCmp('tf.idbagian').setValue(); 
		Ext.getCmp('tf.noreff').setValue(); 
		Ext.getCmp('ta.keterangan').setValue(); 
		Ext.getCmp('tf.kdjurnal').setValue(); 
		Ext.getCmp('tf.userinput').setValue(); 
		Ext.getCmp('df.tglinput').setValue(new Date()); 
		//Ext.getCmp('tf.nominal').setValue(); 
		Ext.getCmp('tf.jmhdebit').setValue(); 
		Ext.getCmp('tf.jmhkredit').setValue(); 
		ds_jurnal_umum.reload();
		ds_jurnal_umum_det_entry.reload();
		ds_jurnal_umum_det.setBaseParam('kdjurnal','null');
		ds_jurnal_umum_det.reload();
	}
}
