function SettingSaldoAwalAkun(){
  var pageSize = 50;
  var ds_tahun = dm_tahun();
  var ds_bulan = dm_bulan();
  var ds_saldo_akun_tahun = dm_saldo_akun_tahun();
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
  
  var editor = new Ext.ux.grid.RowEditor({
    saveText: 'Update',
    clicksToEdit: 1
  });

  var grid_akun_tahun = new Ext.grid.EditorGridPanel({
    id: 'grid_saldo_awal_akun',
    store: ds_saldo_akun_tahun,
    height: 450,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    columnLines: true,
    clicksToEdit: 1,
    style:'margin-bottom:7px',
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 120,
      dataIndex: 'kdakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Nama Akun'),
      width: 250,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Saldo Awal Debit'),
      width: 200,
      dataIndex: 'saldodebit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
      editor: {
        xtype: 'numberfield',
        id: 'edit.saldoawaldebit',
        enableKeyEvents: true,
        listeners:{
          change:function(grid, fieldvalue, columnIndex){
            Ext.getCmp('btn.simpan_perubahan').enable(); 
          },
          specialkey: function(field, e){
            if (e.getKey() == e.ENTER) {
              Ext.getCmp('btn.simpan_perubahan').enable(); 
            }
          }
        }
      }
    },{
      header: headerGerid('Saldo Awal Kredit'),
      width: 200,
      dataIndex: 'saldokredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
      editor: {
        xtype: 'numberfield',
        id: 'edit.saldoawalkredit',
        enableKeyEvents: true,
        listeners:{
          change:function(grid, fieldvalue, columnIndex){
            Ext.getCmp('btn.simpan_perubahan').enable(); 
          },
          specialkey: function(field, e){
            if (e.getKey() == e.ENTER) {
              Ext.getCmp('btn.simpan_perubahan').enable(); 
            }
          }
        }
      }
    }],
  });

  var form_bp_general = new Ext.form.FormPanel({
    id: 'form_bp_general',
    title: 'Pengaturan Saldo Awal Akun', iconCls:'silk-money',
    width: 900, Height: 1000,
    layout: {type: 'form', pack: 'center', align: 'center'},
    frame: true,
    autoScroll: true,
    items: [
    {
      xtype: 'fieldset',
      labelWidth: 80, labelAlign: 'right',
      items: [{
        xtype: 'compositefield',
        fieldLabel: 'Pilih Tahun ',
        items:[{
          xtype: 'combo',
          store: ds_tahun,
          valueField: 'tahun', 
          displayField: 'tahun', 
          editable: false,
          triggerAction: 'all',
          mode: 'local',
          emptyText:'Pilih Tahun',
          id:'cb.tahun',
          width: 100,
        },{
          xtype: 'combo',
          store: ds_bulan,
          valueField: 'kdbulan', 
          displayField: 'nmbulan', 
          editable: false,
          triggerAction: 'all',
          mode: 'local',
          emptyText:'Pilih Bulan',
          id:'cb.bulan',
          width: 100,
          style : 'margin-left:5px;',
        },{
          xtype: 'button',
          text: 'Tampilkan',
          style : 'margin-left:5px;',
          id: 'btn.show_akun',
          iconCls: 'silk-find',
          handler: function() {
            fnSearchgrid();
          }
        },
        {
          xtype: 'button',
          text: 'Simpan Perubahan',
          style : 'margin-left:10px;',
          id: 'btn.simpan_perubahan',
          iconCls: 'silk-save',
          disabled: true,
          handler: function() {
            fnUpdateSaldo();
          }
        }]
      }]
    },grid_akun_tahun],
  });
  SET_PAGE_CONTENT(form_bp_general);
  
  function fnSearchgrid(){
    
    ds_saldo_akun_tahun.setBaseParam('tahun', Ext.getCmp('cb.tahun').getValue());
    ds_saldo_akun_tahun.setBaseParam('bulan', Ext.getCmp('cb.bulan').getValue());
    ds_saldo_akun_tahun.load();
  
  }

  function fnUpdateSaldo(){
    var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
    var arrakuntahun = [];
    for(var zx = 0; zx < ds_saldo_akun_tahun.data.items.length; zx++){
      var record       = ds_saldo_akun_tahun.data.items[zx].data;
      tahun            = record.tahun;
      idakun           = record.idakun;
      saldodebit       = record.saldodebit;
      saldokredit      = record.saldokredit;
      fieldsaldodebit  = record.fieldsaldodebit;
      fieldsaldokredit = record.fieldsaldokredit;

      arrakuntahun[zx] = tahun + '-' + idakun + '-' + saldodebit + '-' + saldokredit + '-' + fieldsaldodebit + '-' + fieldsaldokredit ;
    }
    
    //update data
    Ext.Ajax.request({
      url: BASE_URL + 'settingsaldoawal_controller/simpan_saldo_awal_akun',
      params: {
        arr_akuntahun     : Ext.encode(arrakuntahun)
      },
      success: function(response){
        waitmsg.hide();
        obj = Ext.util.JSON.decode(response.responseText);
        if(obj.success === true){
          Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
          ds_saldo_akun_tahun.reload();          
          Ext.getCmp('btn.simpan_perubahan').disable(); 
        }else{
          Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
          ds_saldo_akun_tahun.reload();
          Ext.getCmp('btn.simpan_perubahan').disable(); 
        }
      }
    });

  }
  
}
  
