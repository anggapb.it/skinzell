function Akbukubesar(){
  var pageSize = 50;
  var ds_tahun = dm_tahun();
  var ds_bulan = dm_bulan();
  var ds_bukubesar = dm_bukubesar();
      ds_bukubesar.setBaseParam('tahun','');
      ds_bukubesar.setBaseParam('bulan','');
  var ds_bukujurnal = dm_bukujurnal();
      ds_bukujurnal.setBaseParam('tahun','');
      ds_bukujurnal.setBaseParam('bulan','');
      ds_bukujurnal.setBaseParam('idakun','');


  var cari_data_grid = [new Ext.ux.grid.Search({
    iconCls: 'btn_search',
    minChars: 1,
    autoFocus: true,
    autoHeight: true,
    position: 'top',
    mode: 'local',
    width: 150
  })];

  var cari_data_jurnal = [new Ext.ux.grid.Search({
    iconCls: 'btn_search',
    minChars: 1,
    autoFocus: true,
    autoHeight: true,
    position: 'top',
    mode: 'local',
    width: 150
  })];

  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
    
  var grid_bukubesar = new Ext.grid.GridPanel({
    id: 'grid_bukubesar',
    store: ds_bukubesar,
    autoScroll: true,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    height: 270,
    layout: 'fit',
    style: 'margin-bottom:5px', 
    columnLines: true,
    listeners: { cellclick: onClickListAkunTahun},
    plugins: cari_data_grid,
    tbar: [{
      text: 'Cetak',
      id: 'btn.cetak',  
      disabled: true,
      iconCls:'silk-printer',   
        handler: function() {
          cetak_pdf_bukubesar();
        }
      },{
      text: 'Cetak Detail',
      id: 'btn.cetak_detail',  
      disabled: true,
      iconCls:'silk-printer',   
        handler: function() {
          cetak_pdf_bukubesar_detail();
        }
      },{
      text: 'Cetak Excel',
      id: 'btn.cetak_excel',  
      disabled: true,
      style: 'margin-left:10px',
      iconCls:'silk-printer',   
        handler: function() {
          cetak_excel_bukubesar();
        }
      },{
      text: 'Cetak Detail Excel',
      id: 'btn.cetak_detail_excel',  
      disabled: true,
      iconCls:'silk-printer',   
        handler: function() {
          cetak_excel_bukubesar_detail();
        }
      },
    '->',
    {
      xtype: 'compositefield',
      width: 350,
      align: 'right',
      style: 'align:right',
      items: [{
        xtype: 'label', text: 'Periode : ', margins: '6 10 0 0',
      },{
        xtype: 'combo',
        store: ds_tahun,
        valueField: 'tahun', 
        displayField: 'tahun', 
        editable: false,
        triggerAction: 'all',
        mode: 'local',
        emptyText:'Pilih Tahun',
        id:'cb.tahun',
        width: 100,
      },{
        xtype: 'combo',
        store: ds_bulan,
        valueField: 'kdbulan', 
        displayField: 'nmbulan', 
        editable: false,
        triggerAction: 'all',
        mode: 'local',
        emptyText:'Pilih Bulan',
        id:'cb.bulan',
        width: 100,
        style : 'margin-left:5px;',
      },{
        xtype: 'button',
        text: 'Tampilkan',
        style : 'margin-left:5px;',
        id: 'btn.show_transaksi',
        iconCls: 'silk-find',
        handler: function() {
          fnSearchgrid();
        }
      }]
    }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('ID Akun'),
      dataIndex: 'idakun',
      hidden: true,
    },{
      header: headerGerid('Kode Akun'),
      width: 160,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
      renderer: fnkeyShowDetailBukuJurnal
    },{
      header: headerGerid('Nama Akun'),
      width: 180,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('Saldo Awal'),
      width: 180,
      dataIndex: 'calc_saldoawal',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Total Debit'),
      width: 180,
      dataIndex: 'totaldebit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Total Kredit'),
      width: 180,
      dataIndex: 'totalkredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Saldo Akhir'),
      width: 180,
      dataIndex: 'saldoakhir',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  }); 

  var grid_bukujurnal = new Ext.grid.GridPanel({
    id: 'grid_bukujurnal',
    title: 'Detail Jurnal',
    store: ds_bukujurnal,
    autoScroll: true,
    frame: true,
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    loadMask: true,
    height: 270,
    layout: 'fit',
    columnLines: true,
    plugins: cari_data_jurnal,
    tbar: [{
      text: 'Cetak',
      id: 'btn.cetak_bukujurnal',  
      iconCls:'silk-printer',
      disabled: true, 
        handler: function() {
          cetak_pdf_bukujurnal();
        }
    },{
      text: 'Cetak Excel',
      id: 'btn.cetak_bukujurnal_excel',  
      iconCls:'silk-printer',
      disabled: true, 
        handler: function() {
          cetak_excel_bukujurnal();
        }
    },'->'],
    bbar: [
      '->',
      {
          xtype: 'numericfield',
          thousandSeparator:',',
          id: 'info.total_debit',
          width:150,
          readOnly:true,
          value:'0',
          align:'right',
      },{
          xtype: 'numericfield',
          thousandSeparator:',',
          id: 'info.total_kredit',
          width:150,
          readOnly:true,
          value:'0',
          align:'right',
      },{
          xtype: 'textfield',
          id: 'hide.idakun',
          hidden:'true',
      }],
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode Jurnal'),
      width: 100,
      dataIndex: 'kdjurnal',
      align: 'center',
    },{
      header: headerGerid('No. Jurnal'),
      width: 80,
      dataIndex: 'nojurnal',
      align: 'center',
    },{
      header: headerGerid('Tgl. Transaksi'),
      width: 100,
      dataIndex: 'tgltransaksi',
      sortable: true,
      align:'center',
      renderer: Ext.util.Format.dateRenderer('d/m/Y'),
    },{
      header: headerGerid('Tgl. Jurnal'),
      width: 100,
      dataIndex: 'tgljurnal',
      sortable: true,
      align:'center',
      renderer: Ext.util.Format.dateRenderer('d/m/Y'),
    },{
      header: headerGerid('No. Reff / No. Bon'),
      width: 120,
      dataIndex: 'noreff_jurnaldet',
      sortable: true,
    },{
      header: headerGerid('Keterangan'),
      width: 270,
      dataIndex: 'keterangan',
      sortable: true,
    },{
      header: headerGerid('Debit'),
      width: 150,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 150,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  }); 

  var form_bp_general = new Ext.form.FormPanel({
    id: 'form_bp_general',
    title: 'Buku Besar', iconCls:'silk-money',
    width: 900, Height: 1000,
    layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
    frame: true,
    autoScroll: true,
    items: [grid_bukubesar,grid_bukujurnal]
  });
  
  SET_PAGE_CONTENT(form_bp_general);
  
  function fnkeyShowDetailBukuJurnal(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }

  function fnSearchgrid()
  {
    ds_bukubesar.setBaseParam('tahun', Ext.getCmp('cb.tahun').getValue());
    ds_bukubesar.setBaseParam('bulan', Ext.getCmp('cb.bulan').getValue());
    ds_bukubesar.load({
      scope   : this,
      callback: function(records, operation, success) {

      data_bukubesar = ds_bukubesar.getCount();
      if(data_bukubesar > 0){
        Ext.getCmp('btn.cetak').enable(); 
        Ext.getCmp('btn.cetak_detail').enable(); 
        Ext.getCmp('btn.cetak_excel').enable(); 
        Ext.getCmp('btn.cetak_detail_excel').enable(); 
      }else{
        Ext.getCmp('btn.cetak').disable(); 
        Ext.getCmp('btn.cetak_detail').disable(); 
        Ext.getCmp('btn.cetak_excel').disable(); 
        Ext.getCmp('btn.cetak_detail_excel').disable(); 
        
      }
   
      }
    });

    
  }

  function onClickListAkunTahun(grid, rowIndex, columnIndex, event)
  {
    var t = event.getTarget();
    if (t.className == 'keyMasterDetail')
    {
      var obj     = ds_bukubesar.getAt(rowIndex);
      var idakun  = obj.get("idakun");

      Ext.getCmp("hide.idakun").setValue(idakun);

      ds_bukujurnal.setBaseParam('tahun', Ext.getCmp('cb.tahun').getValue());
      ds_bukujurnal.setBaseParam('bulan', Ext.getCmp('cb.bulan').getValue());
      ds_bukujurnal.setBaseParam('idakun', idakun);
      ds_bukujurnal.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_bukujurnal.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.total_debit").setValue(total_debit);            
            Ext.getCmp("info.total_kredit").setValue(total_kredit);

            if(total_debit > 0 || total_kredit > 0){
              Ext.getCmp('btn.cetak_bukujurnal').enable(); 
              Ext.getCmp('btn.cetak_bukujurnal_excel').enable(); 
            }else{
              Ext.getCmp('btn.cetak_bukujurnal').disable();   
              Ext.getCmp('btn.cetak_bukujurnal_excel').disable();   
            }

          }
        });
    }
  }

  function cetak_pdf_bukubesar()
  {

    var tahun = Ext.getCmp('cb.tahun').getValue();
    var bulan = Ext.getCmp('cb.bulan').getValue();
    if(!bulan) bulan = '00';

    RH.ShowReport(BASE_URL + 'bukubesar_controller/lap_buku_besar_pdf/' + tahun + '/' + bulan);
  }
  
  function cetak_pdf_bukubesar_detail()
  {

    var tahun = Ext.getCmp('cb.tahun').getValue();
    var bulan = Ext.getCmp('cb.bulan').getValue();
    if(!bulan) bulan = '00';

    RH.ShowReport(BASE_URL + 'bukubesar_controller/lap_buku_besar_detail_pdf/' + tahun + '/' + bulan);
  }
  
  function cetak_pdf_bukujurnal()
  {

    var tahun    = Ext.getCmp('cb.tahun').getValue();
    var bulan    = Ext.getCmp('cb.bulan').getValue();
    var idakun   = Ext.getCmp("hide.idakun").getValue();
    if(!bulan) bulan = '00';

    RH.ShowReport(BASE_URL + 'bukubesar_controller/lap_buku_jurnal_pdf/' + tahun + '/' + bulan + '/' + idakun);
  }

  function cetak_excel_bukubesar()
  {

    var tahun = Ext.getCmp('cb.tahun').getValue();
    var bulan = Ext.getCmp('cb.bulan').getValue();
    if(!bulan) bulan = '00';

    RH.ShowReport(BASE_URL + 'bukubesar_controller/lap_buku_besar_excel/' + tahun + '/' + bulan);
  }
  
  function cetak_excel_bukubesar_detail()
  {

    var tahun = Ext.getCmp('cb.tahun').getValue();
    var bulan = Ext.getCmp('cb.bulan').getValue();
    if(!bulan) bulan = '00';

    RH.ShowReport(BASE_URL + 'bukubesar_controller/lap_buku_besar_detail_excel/' + tahun + '/' + bulan);
  }
  
  function cetak_excel_bukujurnal()
  {

    var tahun    = Ext.getCmp('cb.tahun').getValue();
    var bulan    = Ext.getCmp('cb.bulan').getValue();
    var idakun   = Ext.getCmp("hide.idakun").getValue();
    if(!bulan) bulan = '00';

    RH.ShowReport(BASE_URL + 'bukubesar_controller/lap_buku_jurnal_excel/' + tahun + '/' + bulan + '/' + idakun);
  }

}
