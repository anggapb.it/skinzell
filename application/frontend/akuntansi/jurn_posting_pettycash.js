function jurn_posting_pettycash(){
  Ext.form.Field.prototype.msgTarget = 'side';
  
  var ds_list_transaksi = dm_pettycash_posting();
  var ds_detail_jurnal = dm_pettycash_postingdet();
      ds_detail_jurnal.setBaseParam('kdjurnal','null');

  var cm_list_transaksi = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 100,
      dataIndex: 'kdjurnal',
      align:'center',
      renderer: fnkeyShowDetailTransaksi
    },{
      header: headerGerid('Keterangan'),
      width: 155,
      dataIndex: 'keterangan',
      align:'left'
    },{
      header: headerGerid('Nominal<br/>Transaksi'),
      width: 95,
      dataIndex: 'nominal',
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Posting'),
      width: 60,
      dataIndex: 'status_posting',
      align:'left',
      renderer: fnkeyshowPostingStatus,
    }],
  });

  var cm_detail_jurnal = new Ext.grid.ColumnModel({
    columns: [new Ext.grid.RowNumberer(),
    {
      header: headerGerid('Kode'),
      width: 80,
      dataIndex: 'kdakun',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Nama Akun'),
      width: 150,
      dataIndex: 'nmakun',
      sortable: true,
      align:'left',
    },{
      header: headerGerid('No.Reff'),
      width: 100,
      dataIndex: 'noreff',
      sortable: true,
      align:'center',
    },{
      header: headerGerid('Debit'),
      width: 110,
      dataIndex: 'debit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    },{
      header: headerGerid('Kredit'),
      width: 110,
      dataIndex: 'kredit',
      sortable: true,
      xtype: 'numbercolumn', format:'0,000', align:'right',
    }]
  });
  
  function headerGerid(text, align){
    var hAlign = 'center';  
    if(align =='c') hAlign = 'center';
    if(align =='l') hAlign = 'left';
    if(align =='r') hAlign = 'right';
    return "<H3 align='"+hAlign+"'>"+text+"</H3>";
  }
    
  var grid_list_transaksi = new Ext.grid.GridPanel({
    id: 'grid_list_transaksi',
    store: ds_list_transaksi,
    title: 'Data Transaksi Petty Cash',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_list_transaksi,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:5px',
    listeners: {
      cellclick: onClickListTransaksi
    }
  });

  var grid_detail_jurnal = new Ext.grid.GridPanel({
    id: 'grid_detail_jurnal',
    store: ds_detail_jurnal,
    title: 'Detail Jurnal',
    view: new Ext.grid.GridView({emptyText: '< Belum ada Data >'}),
    cm: cm_detail_jurnal,
    frame: true,
    loadMask: true,
    height: 250,
    layout: 'anchor',
    style: 'padding-bottom:5px',
    bbar: [
      '->',
      {
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_debit',
        width:110,
        readOnly:true,
        value:'0',
        align:'right',
    },{
        xtype: 'numericfield',
        thousandSeparator:',',
        id: 'info.total_kredit',
        width:110,
        readOnly:true,
        value:'0',
        align:'right',
    }],
  });
       
  var form_bp_general = new Ext.FormPanel({
    id: 'form_bp_general',
    title: 'Jurnal Petty Cash', 
    iconCls:'silk-money',
    width: 900, Height: 920,
    autoScroll: true,
    layout: {
      type: 'form',
      pack: 'center',
      align: 'center'
    },
    frame: true,
    items: [
    {
      layout: 'form',
      border: false,
      items: [{
        id: 'filter_jurnal',
        xtype: 'container',
        style: 'padding: 2px; margin:0px;',
        defaults: {labelWidth: 120, labelAlign: 'right'},
        items:[{
          xtype: 'fieldset',
          //title: 'Filter',
          height: 100,
          layout: 'column',
          style: 'padding-top:5px',
          items: [{
            layout: 'form', 
            columnWidth: 0.6,
            items: [{
              fieldLabel: 'Tanggal Jurnal ',
              xtype: 'datefield',
              id: 'tgl_jurnal',
              value: new Date(),
              format: "d/m/Y",
              width: 100, 
              disabled: true,
            },{
              xtype: 'compositefield',
              fieldLabel: 'Periode Transaksi ',
              items: [{
                xtype: 'datefield',
                id: 'periode_awal',
                value: new Date(),
                format: "d/m/Y",
                width: 100,
                listeners:{
                  select: function(field, newValue){
                    fnSearchTransaksi();
                  },
                  change : function(field, newValue){
                    fnSearchTransaksi();
                  }
                }
              },{
                xtype: 'label', id: 'lb.strip',
                text: ' - ', margins: '3 10 0 5',
              },{
                xtype: 'datefield',
                id: 'periode_akhir',
                value: new Date(),
                format: "d/m/Y",
                width: 100,
                listeners:{
                  select: function(field, newValue){
                    fnSearchTransaksi();
                  },
                  change : function(field, newValue){
                    fnSearchTransaksi();
                  }
                }
              }]
            }]
          },{
            layout: 'form', 
            columnWidth: 0.3,
            items: [{
              fieldLabel: 'Jenis Transaksi ',
              xtype: 'textfield',
              id: 'jns_trans',
              value: 'Petty Cash',
              width: 150, 
              disabled: true,
            },{
              fieldLabel: 'USER ID ',
              xtype: 'textfield',
              id: 'user_id',
              value: USERID,
              width: 150,
              disabled: true,
            },{
              xtype: 'textfield',
              id: 'kdjurnal_posting',
              value: '',
              hidden: true,
            },{
                xtype: 'button',
                text: 'Posting',
                id: 'btn.posting',
                iconCls: 'silk-save',
                width: 100,
                height: 25,
                disabled: true,
                style: 'margin-left:125px',
                handler: function() {
                  var kdjurnal_posting = Ext.getCmp("kdjurnal_posting").getValue();
                  var nominal_jurnal = Ext.getCmp("info.total_debit").getValue(); //debit
                  var nominal_jurnal_kredit = Ext.getCmp("info.total_kredit").getValue(); //kredit            
                  

                  if(kdjurnal_posting == ''){
                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. data tidak bisa diposting');
                  }
                  else if(nominal_jurnal != nominal_jurnal_kredit)
                  {
                    Ext.MessageBox.alert('Informasi', 'Terjadi kesalahan. Total debit tidak sesuai dengan total kredit');  
                  }
                  else{
                    var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');
                    var post_grid ='';
                    var count= 1;
                    var endpar = ';';
                    
                    grid_detail_jurnal.getStore().each(function(rec){ // ambil seluruh grid prodi
                      var rowData = rec.data; 
                      if (count == grid_detail_jurnal.getStore().getCount()) {
                        endpar = ''
                      }
                      post_grid += rowData['idakun'] + '^' + 
                             rowData['kdakun'] + '^' + 
                             rowData['nmakun']  + '^' + 
                             rowData['noreff']  + '^' + 
                             rowData['debit'] + '^' + 
                             rowData['kredit']  + '^' +
                          endpar;
                                  
                      count = count+1;
                    });

                    Ext.Ajax.request({
                      url: BASE_URL + 'pettycash_controller/posting_pettycash',
                      params: {
                        kdjurnal_posting : kdjurnal_posting,
                        userid : USERID,
                      },
                      success: function(response){
                        waitmsg.hide();

                        obj = Ext.util.JSON.decode(response.responseText);
                        if(obj.success === true){
                          Ext.getCmp('btn.posting').disable();
                        }else{
                          Ext.getCmp('btn.posting').enable();  
                        }

                        Ext.MessageBox.alert('Informasi', obj.message);
                        
                        ds_list_transaksi.reload();
                        ds_detail_jurnal.reload();
                      }
                    });
                   
                  }

                }
              }
            ]
          }]
        }]
      
      },

      // ====================================== CONTAINER DATA GRID ====================================================//
      {
        id: 'container_data_grid',
        xtype: 'container',
        style: 'padding: 0px',
        defaults: {labelWidth: 100, labelAlign: 'right'},
        items:[{
          xtype: 'fieldset',
          id: 'panel_data_grid',
          //height: 100,
          layout: 'column',
          items: [{
            id: 'colom_grid_left',
            style: 'margin-right:10px',
            layout: 'form', 
            columnWidth: 0.44,
            items: [
              // ============================== panel left side (transaksi & form trans) ===============================//
              {
                xtype: 'fieldset',
                border: false,
                style: 'padding:0px',
                id: 'panel_detail_transaksi',
                labelWidth: 160, labelAlign: 'right',
                //layout: 'column',
                //defaults: {labelWidth: 100, labelAlign: 'right'},
                items: [grid_list_transaksi]
              }]
          },
          {
            id: 'colom_grid_right',
            //title: 'Data Jurnal',
            layout: 'form',
            border: false,
            style: 'padding:0px',     
            columnWidth: 0.55,
            items: [
              grid_detail_jurnal
            ]
          }]
        }]
      },
      // ====================================== END CONTAINER DATA GRID TWO COLUMN ======================================//
      {
        id: 'form_detail_transaksi',
        xtype: 'container',
        style: 'padding: 2px; margin:0px;',
        defaults: {labelWidth: 120, labelAlign: 'right'},
        items:[{
          xtype: 'fieldset',
          title: 'Detail Transaksi',
          height: 115,
          layout: 'column',
          style: 'padding-top:5px',
          items: [{
            layout: 'form', 
            columnWidth: 0.25,
            items: [{
              fieldLabel: 'Tgl Transaksi',
              xtype: 'datefield',
              id: 'df.tgltransaksi',
              format: 'd/m/Y',
              width: 150,
              readOnly: true,
            },{
              xtype: 'datefield',
              fieldLabel: 'Tgl. Input',
              id: 'df.tglinput',
              format: 'd/m/Y',
              width: 150,
              readOnly: true
            },{
              xtype: 'textfield',
              fieldLabel: 'No. Reff/No. Bon ',
              id:'tf.noreff',
              width: 150,
              readOnly: true,
            }]
          },{
            layout: 'form', 
            columnWidth: 0.25,
            items: [{
              xtype: 'textfield',
              fieldLabel: 'Kode Jurnal',
              id:'tf.kdjurnal',
              width: 150,
              readOnly: true,
            },{
              xtype: 'textfield',
              fieldLabel: 'No. Jurnal',
              id:'tf.nojurnal',
              width: 150,
              readOnly: true,
            },{
              xtype: 'textfield',
              fieldLabel: 'User Input',
              id: 'tf.userinput',
              width: 150,
              readOnly: true,
            }]
          },{
            layout: 'form', 
            columnWidth: 0.4,
            items: [{
              xtype: 'numericfield',
              fieldLabel: 'Nominal ',
              id: 'tf.nominal',
              width: 150,
              thousandSeparator:',',
              readOnly: true,
            },{
              xtype: 'textfield',
              fieldLabel: 'Terbilang ',
              id: 'tf.terbilang',
              width: 300,
              readOnly: true
            },{
              xtype: 'textfield',
              id: 'tf.keterangan',
              fieldLabel: 'Keterangan ',
              width : 300,
              readOnly: true,
            }]
          }]
        }]
      
      }]
    }]
  });
  SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
  
  function fnSearchTransaksi(){
    ds_list_transaksi.setBaseParam('tglawal', Ext.getCmp('periode_awal').getValue().format('Y-m-d'));
    ds_list_transaksi.setBaseParam('tglakhir', Ext.getCmp('periode_akhir').getValue().format('Y-m-d'));
    ds_list_transaksi.load();
  }


  function fnkeyShowDetailTransaksi(value){
    Ext.QuickTips.init();
    return '<div class="keyMasterDetail" ext:qtip="Detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
      + value +'</div>';
  }

  function fnkeyshowPostingStatus(value){
   Ext.QuickTips.init();
    if(value == '0'){
      return 'belum';
    }else{
      return 'sudah';
    }
  }

  function onClickListTransaksi(grid, rowIndex, columnIndex, event) {
      var t = event.getTarget();
      
      if (t.className == 'keyMasterDetail')
      {
        var obj = ds_list_transaksi.getAt(rowIndex);
        var kdjurnal         = obj.get("kdjurnal");
        var nojurnal         = obj.get("nojurnal");
        var tgltransaksi     = obj.get("tgltransaksi");
        var tgljurnal        = obj.get("tgljurnal");
        var keterangan       = obj.get("keterangan");
        var noreff           = obj.get("noreff");
        var userid           = obj.get("userid");
        var tglinput         = obj.get("tglinput");
        var nominal          = obj.get("nominal");
        var status_posting   = obj.get("status_posting");
        var terbilang        = obj.get("terbilang");

        Ext.getCmp("kdjurnal_posting").setValue(kdjurnal);
        Ext.getCmp("df.tgltransaksi").setValue(tgltransaksi);
        Ext.getCmp("df.tglinput").setValue(tgljurnal);
        Ext.getCmp("tf.noreff").setValue(noreff);
        Ext.getCmp("tf.kdjurnal").setValue(kdjurnal);
        Ext.getCmp("tf.nojurnal").setValue(nojurnal);
        Ext.getCmp("tf.userinput").setValue(userid);
        Ext.getCmp("tf.nominal").setValue(nominal);
        Ext.getCmp("tf.terbilang").setValue(terbilang);
        Ext.getCmp("tf.keterangan").setValue(keterangan);

        //reload detail jurnal
        ds_detail_jurnal.setBaseParam('kdjurnal', kdjurnal);
        ds_detail_jurnal.load({
          scope   : this,
          callback: function(records, operation, success) {
            //hitung total debit & kredit
            total_debit = 0;
            total_kredit = 0;
            ds_detail_jurnal.each(function (rec) { 
              total_debit += parseFloat(rec.get('debit')); 
              total_kredit += parseFloat(rec.get('kredit')); 
            });

            Ext.getCmp("info.total_debit").setValue(total_debit);            
            Ext.getCmp("info.total_kredit").setValue(total_kredit);

          }
        });

        //disable or enable button posting
        if(status_posting == 0)
        {
          Ext.getCmp('btn.posting').enable();
        
        }else if(status_posting == 1){
          Ext.getCmp('btn.posting').disable();
        
        }

      }
      return true;
  }
  
  
}