function LPenerimaanRI(){
	var ds_vlappenerimaanri = dm_vlappenerimaanri();
	ds_vlappenerimaanri.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlappenerimaanri.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	
	var arr_transfer = [['0', 'Semua'],['1', 'RI Non Transfer UGD'],['2', 'RI Transfer UGD']];
	
	var ds_transfer = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_transfer 
	});
	
	var cm_laporan = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">Tgl. Masuk</div>',
			dataIndex: 'tglmasuk',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: '<div style="text-align:center;">Tgl. Kuitansi</div>',
			dataIndex: 'tglkuitansi',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: '<div style="text-align:center;">No. Registrasi</div>',
			dataIndex: 'noreg',
			width:80
		},{
			header: '<div style="text-align:center;">No. RM</div>',
			dataIndex: 'norm',
			width: 60
		},{
			header: '<div style="text-align:center;">Nama Pasien</div>',
			dataIndex: 'nmpasien',
			width: 130
		},{
			header: '<div style="text-align:center;">Penjamin</div>',
			dataIndex: 'nmpenjamin',
			width: 100
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 160
		},{
			header: '<div style="text-align:center;">Ruangan</div>',
			dataIndex: 'nmbagian',
			width: 80
		},{
			header: '<div style="text-align:center;">Nominal UGD</div>',
			dataIndex: 'nominalugd',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Obat</div>',
			dataIndex: 'uobat',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Nominal RI</div>',
			dataIndex: 'nominalri',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Diskon</div>',
			dataIndex: 'diskon',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Deposit</div>',
			dataIndex: 'deposit',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Non Tunai</div>',
			dataIndex: 'nontunai',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Tunai</div>',
			dataIndex: 'tunai',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Jumlah</div>',
			dataIndex: 'jumlah',
			align:'right',
			renderer: function(value, p, r){
				if(r.data['nominalugd'] == null) r.data['nominalugd'] = 0;
				var jumlah = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']) - parseFloat(r.data['deposit']);
				if(jumlah < 0) jumlah = 0;
				return Ext.util.Format.number(jumlah, '0,000');
			},
			width: 80
		},{
			header: '<div style="text-align:center;">Retur Deposit</div>',
			dataIndex: 'returdeposit',
			align:'right',
			renderer: function(value, p, r){
				if(r.data['nominalugd'] == null) r.data['nominalugd'] = 0;
				var returdeposit = parseFloat(r.data['deposit']) - (parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']));
				if(returdeposit < 0) returdeposit = 0;
				return Ext.util.Format.number(returdeposit, '0,000');
			},
			width: 80
		},{
			header: '<div style="text-align:center;">User ID</div>',
			dataIndex: 'userid',
			width: 90
		}
	]);
	var paging_laporan = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_vlappenerimaanri,
		displayInfo: true,
		displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_laporan = new Ext.grid.GridPanel({
		ds: ds_vlappenerimaanri,
		cm: cm_laporan,
		height: 350,
		autoWidth: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		buttonAlign: 'left',
		layout: 'anchor',
		frame: true,
		tbar: [
			{ text: 'Cetak', iconCls: 'silk-printer', handler: function(){cetakLapKeuangan();} },'-',
			{ text: 'Cetak Excel', iconCls: 'silk-printer', handler: function(){exportdata();} }
		],
		anchorSize: {
			width: 400,
			height: 400
		},
		//bbar: paging_laporan
	});
	
	var lapperperiode = new Ext.FormPanel({
		id: 'fp.penerimaanri',
		title: 'Laporan Penerimaan Rawat Inap',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 50, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: 1,
					border: false,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						}]
					},{
						xtype: 'combo', fieldLabel: 'Transfer',
						id: 'cb.transfer', width: 150,
						store: ds_transfer, valueField: 'id', displayField: 'nama',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih', value:0,
						listeners:{
							select:function(combo, records, eOpts){
								cAdvance();
							}
						}
					}]
				}]
			},
			grid_laporan
		]
	});
	SET_PAGE_CONTENT(lapperperiode);	
	
	function cAdvance(){
		ds_vlappenerimaanri.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlappenerimaanri.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlappenerimaanri.setBaseParam('ctransfer',Ext.getCmp('cb.transfer').getValue());
		ds_vlappenerimaanri.reload();
	}
	
	function cetakLapKeuangan(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var ctransfer	= Ext.getCmp('cb.transfer').getValue();
		RH.ShowReport(BASE_URL + 'print/lapkeuangan/lappenerimaanri/'
                +tglawal+'/'+tglakhir+'/'+ctransfer);
	}

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var ctransfer	= Ext.getCmp('cb.transfer').getValue();
		window.location = BASE_URL + 'print/lapkeuangan/excelpenerimaanri/'+tglawal+'/'+tglakhir+'/'+ctransfer;

    }
	
}