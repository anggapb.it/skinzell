function LPenerimaanBank(){
  var ds_vlappenerimaanbank = dm_vlappenerimaanbank();
  ds_vlappenerimaanbank.setBaseParam('tglawal',new Date().format('Y-m-d'));
  ds_vlappenerimaanbank.setBaseParam('tglakhir',new Date().format('Y-m-d'));
  
  
  var ds_bank = dm_bank();
  
  var cm_laporan = new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: '<div style="text-align:center;">No. Kuitansi</div>',
      dataIndex: 'nokuitansi',
      width: 100
    },{
      header: '<div style="text-align:center;">Tgl. Kuitansi</div>',
      dataIndex: 'tglkuitansi',
      renderer: Ext.util.Format.dateRenderer('d-m-Y'),
      width: 70
    },{
      header: '<div style="text-align:center;">Atas Nama</div>',
      dataIndex: 'atasnama',
      width:180
    },{
      header: '<div style="text-align:center;">Jenis Kuitansi</div>',
      dataIndex: 'nmjnskuitansi',
      width:150
    },{
      header: '<div style="text-align:center;">Nama Bank</div>',
      dataIndex: 'nmbank',
      width:80
    },{
      header: '<div style="text-align:center;">No. Kartu</div>',
      dataIndex: 'nokartu',
      width:100
    },{
      header: '<div style="text-align:center;">Total Tagihan</div>',
      dataIndex: 'pembayaran',
      align:'right',
      xtype: 'numbercolumn', format:'0,000',
      width: 120
    },{
      header: '<div style="text-align:center;">Total Transfer</div>',
      dataIndex: 'jumlah',
      align:'right',
      xtype: 'numbercolumn', format:'0,000',
      width: 120
    }
  ]);
  var paging_laporan = new Ext.PagingToolbar({
    pageSize: 50,
    store: ds_vlappenerimaanbank,
    displayInfo: true,
    displayMsg: 'Data Transaksi Dari {0} - {1} of {2}',
    emptyMsg: 'No data to display'
  });
  
  var grid_laporan = new Ext.grid.GridPanel({
    ds: ds_vlappenerimaanbank,
    cm: cm_laporan,
    height: 420,
    autoWidth: true,
    autoSizeColumns: true,
    enableColumnResize: true,
    enableColumnHide: false,
    enableColumnMove: false,
    enableHdaccess: false,
    columnLines: true,
    loadMask: true,
    buttonAlign: 'left',
    layout: 'anchor',
    frame: true,
    tbar: [
      { text: 'Cetak', iconCls: 'silk-printer', handler: function(){cetakLapKeuangan();} },'-',
      { text: 'Cetak Excel', iconCls: 'silk-printer', handler: function(){exportdata();} }
    ],
    anchorSize: {
      width: 400,
      height: 400
    },
    //bbar: paging_laporan
  });
  
  var lapperperiode = new Ext.FormPanel({
    id: 'fp.penerimaanbank',
    title: 'Laporan Penerimaan Bank',
    autoWidth: true, Height: 1000,
    layout: { type: 'form', pack: 'center', align: 'center'},
    frame: true,
    autoScroll: true,
    items: [{
        xtype: 'container',
        style: 'padding: 5px',
        layout: 'column',
        defaults: {labelWidth: 80, labelAlign: 'right'},
        items:[{
          xtype: 'fieldset',
          columnWidth: 1,
          border: false,
          items: [{
            xtype: 'compositefield',
            items: [{
              xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
              width: 100, value: new Date(),
              format: 'd-m-Y',
              listeners:{
                select: function(field, newValue){
                  cAdvance();
                },
                change : function(field, newValue){
                  cAdvance();
                }
              }
            },{
              xtype: 'label', id: 'lb.sd', text: 's/d'
            },{
              xtype: 'datefield', id: 'df.tglakhir',
              width: 100, value: new Date(),
              format: 'd-m-Y',
              listeners:{
                select: function(field, newValue){
                  cAdvance();
                },
                change : function(field, newValue){
                  cAdvance();
                }
              }
            }]
          },{
            xtype: 'combo', fieldLabel: 'Nama Bank',
            id: 'cb.bank', width: 150,
            store: ds_bank, valueField: 'idbank', displayField: 'nmbank',
            editable: false, triggerAction: 'all',
            forceSelection: true, submitValue: true, mode: 'local',
            emptyText:'Pilih',
            listeners:{
              select:function(combo, records, eOpts){
                cAdvance();
              }
            }
          }]
        }]
      },
      grid_laporan,{     
      xtype: 'panel',
      frame: true,
      autoHeight: true,
      items: [{            
        xtype: 'compositefield',
        id: 'total_transfer',
        items: [{
          xtype: 'tbtext',
          text: 'Total Transfer : ',
          margins : {top:5, right:5, bottom:0, left:0},
          width: 80,
          style: {
            fontWeight: 'bold',
            fontSize: '9px'
          }
        },{
          xtype: 'numericfield',
          id: 'nf.total_transfer',
          thousandSeparator:',',
          readonly:true,
          style: 'opacity:0.8',
        }]
      }]          
    }
    ]
  });
  SET_PAGE_CONTENT(lapperperiode);  
  
  function cAdvance(){
    ds_vlappenerimaanbank.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
    ds_vlappenerimaanbank.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
    ds_vlappenerimaanbank.setBaseParam('idbank',Ext.getCmp('cb.bank').getValue());
    ds_vlappenerimaanbank.load({
      scope   : this,
      callback: function(records, operation, success) {
        total = 0;
        ds_vlappenerimaanbank.each(function (rec) { 
          total += parseFloat(rec.get('jumlah')); 
        });

        Ext.getCmp("nf.total_transfer").setValue(total);

      }
    });
  }
  
  function cetakLapKeuangan(){
    var tglawal   = Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
    var tglakhir  = Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
    var cbank = Ext.getCmp('cb.bank').getValue();
    RH.ShowReport(BASE_URL + 'print/lapkeuangan/lappenerimaanbank/'
                +tglawal+'/'+tglakhir+'/'+cbank);
  }

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
    var cbank = Ext.getCmp('cb.bank').getValue();
    window.location = BASE_URL + 'print/lapkeuangan/excelpenerimaanbank/'+tglawal+'/'+tglakhir+'/'+cbank;

    }
  
}