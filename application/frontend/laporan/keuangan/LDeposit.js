function LDeposit(){
	var ds_vlapdeposit = dm_vlapdeposit();
	ds_vlapdeposit.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlapdeposit.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	
	var cm_laporan = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">No. Kuitansi</div>',
			dataIndex: 'nokuitansi',
			width: 90
		},{
			header: '<div style="text-align:center;">Tgl. Kuitansi</div>',
			dataIndex: 'tglkuitansi',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: '<div style="text-align:center;">No. Registrasi</div>',
			dataIndex: 'noreg',
			width:80
		},{
			header: '<div style="text-align:center;">No. RM</div>',
			dataIndex: 'norm',
			width: 60
		},{
			header: '<div style="text-align:center;">Nama Pasien</div>',
			dataIndex: 'nmpasien',
			width: 130
		},{
			header: '<div style="text-align:center;">Ruangan</div>',
			dataIndex: 'nmbagian',
			width: 130
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdokter',
			width: 130
		},{
			header: '<div style="text-align:center;">Non Tunai</div>',
			dataIndex: 'nontunai',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Tunai</div>',
			dataIndex: 'tunai',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">Total</div>',
			dataIndex: 'total',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: '<div style="text-align:center;">User ID</div>',
			dataIndex: 'userid',
			width: 90
		}
	]);
	var paging_laporan = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_vlapdeposit,
		displayInfo: true,
		displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_laporan = new Ext.grid.GridPanel({
		ds: ds_vlapdeposit,
		cm: cm_laporan,
		height: 350,
		autoWidth: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		buttonAlign: 'left',
		layout: 'anchor',
		frame: true,
		tbar: [
			{ text: 'Cetak', iconCls: 'silk-printer', handler: function(){cetakLapKeuangan();} },'-',
			{ text: 'Cetak Excel', iconCls: 'silk-printer', handler: function(){exportdata();} }
		],
		anchorSize: {
			width: 400,
			height: 400
		},
		bbar: paging_laporan
	});
	
	var lapperperiode = new Ext.FormPanel({
		id: 'fp.deposit',
		title: 'Laporan Penerimaan Deposit',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 50, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: 1,
					border: false,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						}]
					}]
				}]
			},
			grid_laporan
		]
	});
	SET_PAGE_CONTENT(lapperperiode);	
	
	function cAdvance(){
		ds_vlapdeposit.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlapdeposit.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlapdeposit.reload();
	}
	
	function cetakLapKeuangan(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		RH.ShowReport(BASE_URL + 'print/lapkeuangan/lapdeposit/'
                +tglawal+'/'+tglakhir);
	}

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		window.location = BASE_URL + 'print/lapkeuangan/exceldeposit/'+tglawal+'/'+tglakhir;

    }
	
}