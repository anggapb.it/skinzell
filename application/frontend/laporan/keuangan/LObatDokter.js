function LObatDokter(){
	var ds_vlapobatdokter = dm_vlapobatdokter();
	ds_vlapobatdokter.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlapobatdokter.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	
	var ds_vlapobatdokter_fl = dm_vlapobatdokter_fl();
	ds_vlapobatdokter_fl.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlapobatdokter_fl.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	
	var arr_bagian = [['0', 'Semua'],['1', 'Rawat Jalan'],['2', 'Rawat Inap'],['3', 'UGD']];
	
	var ds_bagianarr = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_bagian 
	});
	
	var cm_laporan = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">Nama Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width:160
		},{
			header: '<div style="text-align:center;">No. Kuitansi</div>',
			dataIndex: 'nokuitansi',
			width:100
		},{
			header: '<div style="text-align:center;">No. Registrasi</div>',
			dataIndex: 'noreg',
			width:80
		},{
			header: '<div style="text-align:center;">Tgl. Kuitansi</div>',
			dataIndex: 'tglkuitansi',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: '<div style="text-align:center;">No. RM</div>',
			dataIndex: 'norm',
			width: 60
		},{
			header: '<div style="text-align:center;">Nama Pasien</div>',
			dataIndex: 'nmpasien',
			width: 130
		},{
			header: '<div style="text-align:center;">Kode Obat</div>',
			dataIndex: 'kditem',
			width: 80,
			hidden: true
		},{
			header: '<div style="text-align:center;">Nama Obat</div>',
			dataIndex: 'nmbrg',
			width: 160
		},{
			header: '<div style="text-align:center;">Qty</div>',
			dataIndex: 'qty',
			width: 60,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Harga Jual</div>',
			dataIndex: 'hrgjual',
			width: 100,
			align:'right', xtype: 'numbercolumn',format:'0,000'
		},{
			header: '<div style="text-align:center;">Total</div>',
			dataIndex: 'total',
			width: 100,
			align:'right', xtype: 'numbercolumn',format:'0,000'
		},{
			header: '<div style="text-align:center;">Bagian</div>',
			dataIndex: 'nmjnspelayanan',
			width: 80
		},{
			header: '<div style="text-align:center;">Harga Beli</div>',
			dataIndex: 'hrgbeli',
			width: 100,
			align:'right', xtype: 'numbercolumn',
		}
	]);
	var paging_laporan = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_vlapobatdokter,
		displayInfo: true,
		displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_laporan = new Ext.grid.GridPanel({
		ds: ds_vlapobatdokter,
		cm: cm_laporan,
		height: 280,
		autoWidth: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		buttonAlign: 'left',
		layout: 'anchor',
		frame: true,
		tbar: [
			{ text: 'Cetak', iconCls: 'silk-printer', handler: function(){cetakLapKeuangan();} },'-',
			{ text: 'Cetak Excel', iconCls: 'silk-printer', handler: function(){exportdata();} }
		],
		anchorSize: {
			width: 400,
			height: 400
		},
		bbar: paging_laporan
	});
	
	var cm_laporan_pl = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">No. Kuitansi</div>',
			dataIndex: 'nokuitansi',
			width:100
		},{
			header: '<div style="text-align:center;">Tgl. Kuitansi</div>',
			dataIndex: 'tglkuitansi',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: '<div style="text-align:center;">Nama Pasien</div>',
			dataIndex: 'nmpasien',
			width: 130
		},{
			header: '<div style="text-align:center;">Kode Obat</div>',
			dataIndex: 'kditem',
			width: 80,
			hidden: true
		},{
			header: '<div style="text-align:center;">Nama Obat</div>',
			dataIndex: 'nmbrg',
			width: 160
		},{
			header: '<div style="text-align:center;">Qty</div>',
			dataIndex: 'qty',
			width: 60,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Harga Jual</div>',
			dataIndex: 'hrgjual',
			width: 100,
			align:'right', xtype: 'numbercolumn',format:'0,000'
		},{
			header: '<div style="text-align:center;">Total</div>',
			dataIndex: 'total',
			width: 100,
			align:'right', xtype: 'numbercolumn',format:'0,000'
		},{
			header: '<div style="text-align:center;">Harga Beli</div>',
			dataIndex: 'hrgbeli',
			width: 100,
			align:'right', xtype: 'numbercolumn',
		}
	]);
	
	var grid_laporan_pl = new Ext.grid.GridPanel({
		id: 'lap.pl',
		ds: ds_vlapobatdokter_fl,
		cm: cm_laporan_pl,
		height: 250,
		autoWidth: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		buttonAlign: 'left',
		layout: 'anchor',
		frame: true,
		tbar: [
			{ text: 'Cetak', iconCls: 'silk-printer', handler: function(){cetakLapKeuangan_fl();} },'-',
			{ text: 'Cetak Excel', iconCls: 'silk-printer', handler: function(){exportdata_fl();} }
		],
		anchorSize: {
			width: 400,
			height: 400
		}
	});
	
	var lapperperiode = new Ext.FormPanel({
		id: 'fp.obatdokter',
		title: 'Laporan Obat Dokter',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 1,
			//defaults: { labelWidth: 87, labelAlign: 'right'},
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'RJ, RI, UGD',
				layout: 'form',
				height: 365,
				boxMaxHeight:380,
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 50, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						border: false,
						items: [{
							xtype: 'compositefield', width: 350,
							items: [{
								xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
								width: 100, value: new Date(),
								format: 'd-m-Y',
								listeners:{
									select: function(field, newValue){
										cAdvance();
									},
									change : function(field, newValue){
										cAdvance();
									}
								}
							},{
								xtype: 'label', id: 'lb.sd', text: 's/d'
							},{
								xtype: 'datefield', id: 'df.tglakhir',
								width: 100, value: new Date(),
								format: 'd-m-Y',
								listeners:{
									select: function(field, newValue){
										cAdvance();
									},
									change : function(field, newValue){
										cAdvance();
									}
								}
							}]
						},{
							xtype: 'combo', fieldLabel: 'Bagian',
							id: 'cb.bagian', width: 150,
							store: ds_bagianarr, valueField: 'id', displayField: 'nama',
							editable: false, triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih', value:0,
							listeners:{
								select:function(combo, records, eOpts){
									cAdvance();
								}
							}
						}]
					}]
				},
				grid_laporan]
			}]
		},
		{
			xtype: 'container',
			style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Farmasi Pasien Luar',
				layout: 'form',
				height: 280,
				boxMaxHeight:295,
				items: [grid_laporan_pl]
			}]
		}]
	});
	SET_PAGE_CONTENT(lapperperiode);	
	
	function cAdvance(){
		ds_vlapobatdokter.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlapobatdokter.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlapobatdokter.setBaseParam('cbagian',Ext.getCmp('cb.bagian').getValue());
		ds_vlapobatdokter.reload();
		
		ds_vlapobatdokter_fl.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlapobatdokter_fl.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlapobatdokter_fl.setBaseParam('cbagian',Ext.getCmp('cb.bagian').getValue());
		ds_vlapobatdokter_fl.reload();
	}
	
	function cetakLapKeuangan(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var cbagian	= Ext.getCmp('cb.bagian').getValue();
		RH.ShowReport(BASE_URL + 'print/lapkeuangan/lapobatdokter/'
                +tglawal+'/'+tglakhir+'/'+cbagian);
	}

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var cbagian	= Ext.getCmp('cb.bagian').getValue();
		window.location = BASE_URL + 'print/lapkeuangan/excelobatdokter/'+tglawal+'/'+tglakhir+'/'+cbagian;

    }
	
	function cetakLapKeuangan_fl(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		RH.ShowReport(BASE_URL + 'print/lapkeuangan/lapobatdokter_fl/'
                +tglawal+'/'+tglakhir);
	}
	
	function exportdata_fl(){
        var tglawal=Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		window.location = BASE_URL + 'print/lapkeuangan/excelobatdokter_fl/'+tglawal+'/'+tglakhir;

    }
	
}