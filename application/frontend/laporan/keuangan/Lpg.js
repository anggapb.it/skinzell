function Lpg(){
	
	var ds_gridtkar = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 't_karyawan_controller/get_tkar',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			baseParams: {
				cbxreg : true,
				tglkuitansi1 : Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglkuitansi2 : Ext.util.Format.date(new Date(), 'Y-m-d'),
			},
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'idkuitansidet',
				mapping: 'idkuitansidet'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nominalugd',
				mapping: 'nominalugd'
			},{
				name: 'total_transaksi',
				mapping: 'total_transaksi'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_bayar',
				mapping: 'total_bayar'
			},{
				name: 'jml_tagihan',
				mapping: 'jml_tagihan'
			},{
				name: 'jml_byr',
				mapping: 'jml_byr'
			},{
				name: 'sisa',
				mapping: 'sisa'
			},{
				name: 'nokartu',
				mapping: 'nokartu'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			}],
			listeners: {
			load: function(store, records, options) {
					var jumlahbiaya = 0;
					var byr_pasien = 0;
					var jml_tagihan = 0;
					var jml_byr = 0;
					var sisa = 0;
                    ds_gridtkar.each(function (rec) { 
                        jumlahbiaya += (parseFloat(rec.get('total_transaksi'))) ? parseFloat(rec.get('total_transaksi')):0;
                        byr_pasien += (parseFloat(rec.get('total_bayar'))) ? parseFloat(rec.get('total_bayar')):0;
                        jml_tagihan += (parseFloat(rec.get('jml_tagihan'))) ? parseFloat(rec.get('jml_tagihan')):0;
                        jml_byr += (parseFloat(rec.get('jml_byr'))) ? parseFloat(rec.get('jml_byr')):0;
                        sisa += (parseFloat(rec.get('sisa'))) ? parseFloat(rec.get('sisa')):0;
                    });

                    Ext.getCmp('tf.all_transaksi').setValue(jumlahbiaya);
                    Ext.getCmp('tf.all_jmlbayar').setValue(byr_pasien);
                    Ext.getCmp('tf.all_tagihan').setValue(jml_tagihan);
                    Ext.getCmp('tf.all_byrkar').setValue(jml_byr);
                    Ext.getCmp('tf.all_sisatagihan').setValue(sisa);
			}
		} 
		});
	
	
	var ds_gridtfarmasi = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 't_karyawan_controller/get_tfarmasi_laporan',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			baseParams: {
				cbxreg : true,
				tglkuitansi1 : Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglkuitansi2 : Ext.util.Format.date(new Date(), 'Y-m-d'),
			},
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'idkuitansidet',
				mapping: 'idkuitansidet'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'total_transaksi',
				mapping: 'total_transaksi'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_bayar',
				mapping: 'total_bayar'
			},{
				name: 'jml_tagihan',
				mapping: 'jml_tagihan'
			},{
				name: 'jml_byr',
				mapping: 'jml_byr'
			},{
				name: 'sisa',
				mapping: 'sisa'
			},{
				name: 'tunai',
				mapping: 'tunai'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'nokartu',
				mapping: 'nokartu'
			}],
			listeners: {
			load: function(store, records, options) {
					var jumlahbiaya = 0;
					var byr_pasien = 0;
					var jml_tagihan = 0;
					var jml_byr = 0;
					var sisa = 0;
                    ds_gridtfarmasi.each(function (rec) { 
                        jumlahbiaya += (parseFloat(rec.get('total_transaksi'))) ? parseFloat(rec.get('total_transaksi')):0;
                        byr_pasien += (parseFloat(rec.get('total_bayar'))) ? parseFloat(rec.get('total_bayar')):0;
                        jml_tagihan += (parseFloat(rec.get('jml_tagihan'))) ? parseFloat(rec.get('jml_tagihan')):0;
                        jml_byr += (parseFloat(rec.get('jml_byr'))) ? parseFloat(rec.get('jml_byr')):0;
                        sisa += (parseFloat(rec.get('sisa'))) ? parseFloat(rec.get('sisa')):0;
                    });

                    Ext.getCmp('tf.all_transaksi2').setValue(jumlahbiaya);
                    Ext.getCmp('tf.all_jmlbayar2').setValue(byr_pasien);
                    Ext.getCmp('tf.all_tagihan2').setValue(jml_tagihan);
                    Ext.getCmp('tf.all_byrkar2').setValue(jml_byr);
                    Ext.getCmp('tf.all_sisatagihan2').setValue(sisa); 
			}
		}
		});
	
	
	var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
			header: '<center>No. Registrasi</center>',
			width: 87,
			dataIndex: 'noreg',
			align:'center',	
		},{
			header: '<center>Tgl. Kuitansi</center>',
			width: 81,
			dataIndex: 'tglkuitansi',
			align:'center',	
			//format: 'd-m-Y',
		},{
			header: '',
			dataIndex: 'idkuitansidet',
			hidden: true
		},{
			header: '',
			dataIndex: 'idjnspelayanan',
			hidden: true
		},{
			header: '',
			dataIndex: 'nmjnspelayanan',
			hidden: true
		},{
			header: '<center>No. RM</center>',
			width: 80,
			dataIndex: 'norm',
			//align:'center',	
		},{
			header: '<center>Nama Pasien</center>',
			width: 100,
			dataIndex: 'nmpasien',
			//align:'center',				
		},{
			header: '<center>Nama Karyawan</center>',
			width: 100,
			dataIndex: 'nokartu',
			//align:'center',				
		},{
			header: '<center>(L/P)</center>',
			width: 50,
			dataIndex: 'kdjnskelamin',
			align:'center',	
			hidden: true	
		},{
			header: '<center>Tgl. Lahir</center>',
			width: 72,
			dataIndex: 'tgllahir',
			align:'center',	
			hidden: true
		},{
			header: '<center>Total <br> Transaksi</center>',
			width: 100,
			dataIndex: 'total_transaksi',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			/* renderer: function(value, p, r){
				if(r.data['idjnspelayanan'] == 2) {
					var jumlah = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']) - parseFloat(r.data['deposit']);
				} else {
					var jumlah = parseFloat(r.data['total']);
				}
				return Ext.util.Format.number(jumlah, '0,000');
			} */	
		},{
			header: '<center>Dibayar <br> Pasien</center>',
			width: 100,
			dataIndex: 'total_bayar',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			//hidden: true
		},{
			header: '<center>Jumlah<br>Potong Gaji</center>',
			width: 100,
			dataIndex: 'jml_tagihan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			hidden: false
		},{
			header: '<center>Jumlah<br>Dibayar</center>',
			width: 100,
			dataIndex: 'jml_byr',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: '<center>Sisa<br>Tagihan</center>',
			width: 100,
			dataIndex: 'sisa',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		}]
    });
	

	var cm2 = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
			header: '<center>No. Nota</center>',
			width: 87,
			dataIndex: 'nonota',
			align:'center',	
		},{
			header: '<center>Tgl. Kuitansi</center>',
			width: 81,
			dataIndex: 'tglkuitansi',
			align:'center',	
			format: 'd-m-Y',
		},{
			header: '',
			dataIndex: 'idkuitansidet',
			hidden: true
		},{
			header: '',
			dataIndex: 'idjnspelayanan',
			hidden: true
		},{
			header: '',
			dataIndex: 'nmjnspelayanan',
			hidden: true
		},{
			header: '<center>Nama Pasien</center>',
			width: 150,
			dataIndex: 'atasnama',
			//align:'center',				
		},{
			header: '<center>Nama <br /> Karyawan</center>',
			width: 100,
			dataIndex: 'nokartu',
			//align:'center',	
		},{
			header: '<center>Total <br> Transaksi</center>',
			width: 100,
			dataIndex: 'total_transaksi',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			/* renderer: function(value, p, r){
				if(r.data['idjnspelayanan'] == 2) {
					var jumlah = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']) - parseFloat(r.data['deposit']);
				} else {
					var jumlah = parseFloat(r.data['total']);
				}
				return Ext.util.Format.number(jumlah, '0,000');
			} */	
		},{
			header: '<center>Dibayar <br> Pasien</center>',
			width: 100,
			dataIndex: 'total_bayar',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			//hidden: true
		},{
			header: '<center>Jumlah<br>Potong Gaji</center>',
			width: 100,
			dataIndex: 'jml_tagihan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			hidden: false
		},{
			header: '<center>Jumlah<br>Dibayar</center>',
			width: 100,
			dataIndex: 'jml_byr',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: '<center>Sisa<br>Tagihan</center>',
			width: 100,
			dataIndex: 'sisa',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		}]
    });
	
	
	
		var paging = new Ext.PagingToolbar({
		pageSize: 25,
		store: ds_gridtkar,
		displayInfo: true,
		displayMsg: 'Data Pembelian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var sm = new Ext.grid.RowSelectionModel({
		singleSelect: true,
		listeners: {}
	});
		
	var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});
		
	var grid_tfarmasi = new Ext.grid.EditorGridPanel({
		id: 'id_gridtfarmasi',
		store: ds_gridtfarmasi,
        vw:vw,
		cm:cm2,
		sm:sm,
		//tbar: 				[],
		frame: true,
		height: 200,
		autoScroll: true,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		bbar: [{
					text: '<span style="margin-left:0px;font-size:12px;">Total :</span>', style: 'margin-left: 400px',
				},{
					xtype: 'numericfield',
					id: 'tf.all_transaksi2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_jmlbayar2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_tagihan2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_byrkar2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_sisatagihan2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				}],
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
			
			},
		}
	});	
	
	
	var grid_tkar = new Ext.grid.EditorGridPanel({
		id: 'id_gridtkar',
		store: ds_gridtkar,
        vw:vw,
		cm:cm,
		sm:sm,
		//tbar: 				[],
		frame: true,
		height: 200,
		autoScroll: true,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		bbar: [{
					text: '<span style="margin-left:0px;font-size:12px;">Total :</span>', style: 'margin-left: 430px',
				},{
					xtype: 'numericfield',
					id: 'tf.all_transaksi',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_jmlbayar',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_tagihan',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_byrkar',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_sisatagihan',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				}],
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
			
			},
		}
	});	
	
	
	
	var lap_pg = new Ext.FormPanel({
		id: 'fp.pasienrj',
		title: 'Laporan Tagihan Karyawan (PG)',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 50, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: 1,
					border: false,
					items: [{
												xtype:'checkbox',  
												id: 'cbxreg',
												checked: true,
												disabled: true,
												hidden: true
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglkuitansi1',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglkuitansi2',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						}]
					}]
				}]
			},{
					xtype: 'fieldset', title: '',
					id: 'fs.tes', layout: 'column',
					//defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[
						{					
							xtype:'button', 
							iconCls:'silk-printer', 
							id:'button_cetak',
							text:'Cetak', //margins:'0 0 0 10',
							disabled:false,
							handler: function(){ cetakLapKeuangan(); }
						},
						{					
							xtype:'button', 
							iconCls:'silk-printer', 
							id:'button_excel',
							text:'Cetak Excel', //margins:'0 0 0 10',
							disabled:false,
							handler: function(){ exportdata(); }
						},
					]
			},{
					xtype: 'fieldset', title: 'Rawat Jalan / UGD / Rawat Inap',
					id: 'fs.grid_tkar', layout: 'column',
					//defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[grid_tkar]
			},{
					xtype: 'fieldset', title: 'Pelayanan Tambahan / Farmasi Pasien Luar',
					id: 'fs.grid_tfarmasi', layout: 'column',
					//defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[grid_tfarmasi]
			}
			
		]
	});
	SET_PAGE_CONTENT(lap_pg);

	function cAdvance(){
		ds_gridtkar.setBaseParam('tglkuitansi1',Ext.getCmp('df.tglkuitansi1').getValue().format('Y-m-d'));
		ds_gridtkar.setBaseParam('tglkuitansi2',Ext.getCmp('df.tglkuitansi2').getValue().format('Y-m-d'));
		ds_gridtkar.reload();
		
		ds_gridtfarmasi.setBaseParam('tglkuitansi1',Ext.getCmp('df.tglkuitansi1').getValue().format('Y-m-d'));
		ds_gridtfarmasi.setBaseParam('tglkuitansi2',Ext.getCmp('df.tglkuitansi2').getValue().format('Y-m-d'));
		ds_gridtfarmasi.reload();
	}	
	
	function cetakLapKeuangan(){
		var tglawal		= Ext.getCmp('df.tglkuitansi1').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglkuitansi2').getValue().format('Y-m-d');
		RH.ShowReport(BASE_URL + 'print/lap_pg/cetak_pg/'
                +tglawal+'/'+tglakhir);
	}

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglkuitansi1').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglkuitansi2').getValue().format('Y-m-d');
		window.location = BASE_URL + 'print/lap_pg/excelpg/'+tglawal+'/'+tglakhir;

    }
	
	
}