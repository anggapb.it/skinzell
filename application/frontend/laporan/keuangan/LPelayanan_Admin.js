function LPelayanan_Admin(){
	var ds_vlappelayanan = dm_vlappelayanan();
	ds_vlappelayanan.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlappelayanan.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	
	var idnotadetx, tarifjmx;
	var ds_dokter = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'dokter_controller/get_dokter_combo',
				method: 'POST'
			}),
			baseParams: {
				cmb : 1,
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			}],
			listeners : {
				load : function() {
				var rec = new this.recordType({iddokter:null, nmdoktergelar:'-'});
				rec.commit();
				this.add(rec);
				}
			}
		});
	
	function render_dokter(value) {
		var val = RH.getRecordFieldValue(ds_dokter, 'nmdoktergelar', 'iddokter', value);
		return val;
    }
	
	var arr_cari = [['0', ''],['1', 'No. Registrasi'],['2', 'No. RM'],['3', 'Nama Pasien'],['4', 'Nama Pelayanan'],['5', 'Parent'],['6', 'Jenis Pelayanan'],['7', 'Dokter']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari
	});
	
	var cm_laporan = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">Jenis Pelayanan</div>',
			dataIndex: 'nmjnspelayanan',
			width:85
		},{
			header: '<div style="text-align:center;">No. Registrasi</div>',
			dataIndex: 'noreg',
			width:80
		},{
			header: '<div style="text-align:center;">Tgl. Kuitansi</div>',
			dataIndex: 'tglkuitansi',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: '<div style="text-align:center;">No. RM</div>',
			dataIndex: 'norm',
			width: 60
		},{
			header: '<div style="text-align:center;">Nama Pasien</div>',
			dataIndex: 'nmpasien',
			width: 130
		},{
			header: '<div style="text-align:center;">Nama Pelayanan</div>',
			dataIndex: 'nmpelayanan',
			width: 160
		},{
			header: '<div style="text-align:center;">Parent</div>',
			dataIndex: 'parent',
			width: 160
		},{
			header: '<div style="text-align:center;">Tarif</div>',
			dataIndex: 'tarif',
			width: 100,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Qty</div>',
			dataIndex: 'qty',
			width: 60,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Diskon</div>',
			dataIndex: 'diskon',
			width: 80,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Total</div>',
			dataIndex: 'total',
			width: 100,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 160, //renderer:render_dokter,
			editor :new Ext.form.ComboBox({
						id: 'iddokter',
						store: ds_dokter,
						triggerAction: 'all',
						valueField: 'iddokter',
						displayField: 'nmdoktergelar',
						forceSelection: true,
						submitValue: true, 
						mode: 'local',
						typeAhead: false,
						selectOnFocus: true,
						editable: false,
                        listeners: {
                            select: function(){
								updatedokter();
							}
						}
					})
		},{
			header: '<div style="text-align:center;">Jasa Medis</div>',
			dataIndex: 'tarifjmx',
			width: 100,
			align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">ID Nota Det</div>',
			dataIndex: 'idnotadetx',
			width: 70,
		}
	]);
	var paging_laporan = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_vlappelayanan,
		displayInfo: true,
		displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_laporan = new Ext.grid.EditorGridPanel({
		ds: ds_vlappelayanan,
		cm: cm_laporan,
		height: 350,
		clicksToEdit: 1,
		autoWidth: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		buttonAlign: 'left',
		layout: 'anchor',
		frame: true,
		tbar: [
			{ text: 'Cetak', iconCls: 'silk-printer', handler: function(){cetakLapKeuangan();} },'-',
			{ text: 'Cetak Excel', iconCls: 'silk-printer', handler: function(){exportdata();} }
		],
		anchorSize: {
			width: 400,
			height: 400
		},
		bbar: paging_laporan,
		listeners: {
			cellclick: klik_cari
		}
	});
	
	function klik_cari(grid, rowIdx, columnIndex, event){
		var rec = ds_vlappelayanan.getAt(rowIdx);
		
		idnotadetx = rec.data["idnotadetx"];
		tarifjmx = rec.data["tarifjmx"];		
	}
	
	function updatedokter(){
		//if (tarifjmx > 0) {
			Ext.Ajax.request({
				url: BASE_URL + 'vlapkeuangan_controller/updatenotadet',
				params: {
					iddokter : RH.getCompValue('iddokter'),
					idnotadet: idnotadetx
				},
				success: function(response){
					ds_vlappelayanan.reload();
				},
				failure : function(){
					ds_vlappelayanan.reload();
					Ext.MessageBox.alert('Informasi', 'Ubah Data Gagal');
				}
			});
		/* } else {
			Ext.MessageBox.alert('Informasi', 'Tarif Jasa Medis 0');
			ds_vlappelayanan.reload();
		} */
	}
	
	var lapperperiode = new Ext.FormPanel({
		id: 'fp.obatdokter',
		title: 'Laporan Pelayanan (Khusus Admin)',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 100, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: 1,
					border: false,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}
							}
						}]
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'combo', fieldLabel: 'Berdasarkan',
							id: 'cb.cari1', width: 120,
							store: ds_cari, valueField: 'id', displayField: 'nama',
							editable: false, triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih', value:0
						},{
							xtype: 'textfield',
							id: 'tf.cari1',
							width: 100,
							enableKeyEvents: true,
							listeners:{
								specialkey: function(field, e){
									if (e.getKey() == e.ENTER) {
										cAdvance();
									}
								}
							}
						}]
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'combo', fieldLabel: 'Berdasarkan',
							id: 'cb.cari2', width: 120,
							store: ds_cari, valueField: 'id', displayField: 'nama',
							editable: false, triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih', value:0
						},{
							xtype: 'textfield',
							id: 'tf.cari2',
							width: 100,
							enableKeyEvents: true,
							listeners:{
								specialkey: function(field, e){
									if (e.getKey() == e.ENTER) {
										cAdvance();
									}
								}
							}
						}]
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'combo', fieldLabel: 'Berdasarkan',
							id: 'cb.cari3', width: 120,
							store: ds_cari, valueField: 'id', displayField: 'nama',
							editable: false, triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih', value:0
						},{
							xtype: 'textfield',
							id: 'tf.cari3',
							width: 100,
							enableKeyEvents: true,
							listeners:{
								specialkey: function(field, e){
									if (e.getKey() == e.ENTER) {
										cAdvance();
									}
								}
							}
						}]
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'combo', fieldLabel: 'Berdasarkan',
							id: 'cb.cari4', width: 120,
							store: ds_cari, valueField: 'id', displayField: 'nama',
							editable: false, triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							emptyText:'Pilih', value:0
						},{
							xtype: 'textfield',
							id: 'tf.cari4',
							width: 100,
							enableKeyEvents: true,
							listeners:{
								specialkey: function(field, e){
									if (e.getKey() == e.ENTER) {
										cAdvance();
									}
								}
							}
						}]
					}]
				}]
			},
			grid_laporan
		]
	});
	SET_PAGE_CONTENT(lapperperiode);	
	
	function cAdvance(){
		ds_vlappelayanan.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlappelayanan.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlappelayanan.setBaseParam('cberdasarkan1',Ext.getCmp('cb.cari1').getValue());
		ds_vlappelayanan.setBaseParam('cberdasarkan2',Ext.getCmp('cb.cari2').getValue());
		ds_vlappelayanan.setBaseParam('cberdasarkan3',Ext.getCmp('cb.cari3').getValue());
		ds_vlappelayanan.setBaseParam('cberdasarkan4',Ext.getCmp('cb.cari4').getValue());
		ds_vlappelayanan.setBaseParam('ccari1',Ext.getCmp('tf.cari1').getValue());
		ds_vlappelayanan.setBaseParam('ccari2',Ext.getCmp('tf.cari2').getValue());
		ds_vlappelayanan.setBaseParam('ccari3',Ext.getCmp('tf.cari3').getValue());
		ds_vlappelayanan.setBaseParam('ccari4',Ext.getCmp('tf.cari4').getValue());
		ds_vlappelayanan.reload();
	}
	
	function cetakLapKeuangan(){
		var tglawal			= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir		= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var cberdasarkan1	= Ext.getCmp('cb.cari1').getValue();
		var cberdasarkan2	= Ext.getCmp('cb.cari2').getValue();
		var cberdasarkan3	= Ext.getCmp('cb.cari3').getValue();
		var cberdasarkan4	= Ext.getCmp('cb.cari4').getValue();
		var ccari1			= Ext.getCmp('tf.cari1').getValue();
		var ccari2			= Ext.getCmp('tf.cari2').getValue();
		var ccari3			= Ext.getCmp('tf.cari3').getValue();
		var ccari4			= Ext.getCmp('tf.cari4').getValue();
		if(ccari1 == '') ccari1 = 0;
		if(ccari2 == '') ccari2 = 0;
		if(ccari3 == '') ccari3 = 0;
		if(ccari4 == '') ccari4 = 0;
		RH.ShowReport(BASE_URL + 'print/lapkeuangan/lappelayanan/'
                +tglawal+'/'+tglakhir+'/'+cberdasarkan1+'/'+cberdasarkan2+'/'+cberdasarkan3+'/'+cberdasarkan4+'/'+ccari1+'/'+ccari2+'/'+ccari3+'/'+ccari4);
	}

    function exportdata(){
        var tglawal			= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir		= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var cberdasarkan1	= Ext.getCmp('cb.cari1').getValue();
		var cberdasarkan2	= Ext.getCmp('cb.cari2').getValue();
		var cberdasarkan3	= Ext.getCmp('cb.cari3').getValue();
		var cberdasarkan4	= Ext.getCmp('cb.cari4').getValue();
		var ccari1			= Ext.getCmp('tf.cari1').getValue();
		var ccari2			= Ext.getCmp('tf.cari2').getValue();
		var ccari3			= Ext.getCmp('tf.cari3').getValue();
		var ccari4			= Ext.getCmp('tf.cari4').getValue();
		if(ccari1 == '') ccari1 = 0;
		if(ccari2 == '') ccari2 = 0;
		if(ccari3 == '') ccari3 = 0;
		if(ccari4 == '') ccari4 = 0;
		window.location = BASE_URL + 'print/lapkeuangan/excellappelayananadm/'+tglawal+'/'+tglakhir+'/'+cberdasarkan1+'/'+cberdasarkan2+'/'+cberdasarkan3+'/'+cberdasarkan4+'/'+ccari1+'/'+ccari2+'/'+ccari3+'/'+ccari4;

    }
	
}