function LPenjualanbrg(){
	var reader = new Ext.data.JsonReader({
			root:'data',
			idProperty: '',
			totalProperty: 'results',
			remoteGroup: true,
			fields: [ 
			{ name: 'nmpasien' }
			, { name: 'qty' }
			, { name: 'nmitem' }
			, { name: 'modal' } 
			, { name: 'keuntungan' }
			, { name: 'diskon' }
			, { name: 'tglreg' }
			, { name: 'penjualan' }
			, { name: 'persentase_margin' }]
		});
	var ds_penjualan = new Ext.data.GroupingStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'laporan_penjualanbrg/get_data',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			reader: reader,
			groupField:'nmpasien',
			remoteSort: true,
			listeners: {
			load: function(store, records, options) {
					var totpenjualan = 0;
					var totdiskon = 0;
					var tothpp = 0;
					var totprofit = 0;
					
                    ds_penjualan.each(function (rec) { 
                        totpenjualan += (parseFloat(rec.get('penjualan'))) ? parseFloat(rec.get('penjualan')):0;
                        totdiskon += (parseFloat(rec.get('diskon'))) ? parseFloat(rec.get('diskon')):0;
                        tothpp += (parseFloat(rec.get('modal'))) ? parseFloat(rec.get('modal')):0;
                        totprofit += (parseFloat(rec.get('keuntungan'))) ? parseFloat(rec.get('keuntungan')):0;
                    });

                    Ext.getCmp('tf.all_penjualan').setValue(totpenjualan);
                    Ext.getCmp('tf.all_diskon').setValue(totdiskon);
                    Ext.getCmp('tf.all_hpp').setValue(tothpp);
                    Ext.getCmp('tf.all_profit').setValue(totprofit);
                   
				}
			}
		});
	ds_penjualan.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_penjualan.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));

	var paging = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_penjualan,
		displayInfo: true,
		displayMsg: 'Data Persediaan Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_penjualan_brg',
		store: ds_penjualan,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		 view: new Ext.grid.GroupingView({
            //forceFit:true,
            groupTextTpl: '{text}',
			enableGroupingMenu: false,	// don't show a grouping menu
			enableNoGroups: false,		// don't let the user ungroup
			hideGroupedColumn: true,	// don't show the column that is being used to create the heading
			showGroupName: false,		// don't show the field name with the group heading
			startCollapsed: false		// the groups start closed/no
        }),	 
		tbar: [{
			text: 'Export PDF',
			hidden: true,
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
			}
		},'-',
		{ 	text: 'Cetak Excel',
			hidden:true,
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',
			handler: function(){
				cetakexcel();
			}
		}],
		columns: [{
			header: 'Pasien',
			dataIndex: 'nmpasien',
			width: 150,
			sortable: true,
		},{
			header: 'Tgl. Kunjungan',
			dataIndex: 'tglreg',
			width: 100,
			sortable: true,
		},{
			header: 'Treatment / Barang',
			dataIndex: 'nmitem',
			width: 250,
			sortable: true,
		},{
			header: 'Qty',
			dataIndex: 'qty',
			width: 60,
			sortable: true,
		},{
			header: 'Penjualan',
			dataIndex: 'penjualan',
			width: 100,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right'
		},{
			header: 'Diskon Per Item',
			dataIndex: 'diskon',
			width: 100,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right'
		},{
			header: 'HPP',
			dataIndex: 'modal',
			width: 100,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right'
		},{
			header: 'Profit',
			dataIndex: 'keuntungan',
			width: 100,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right'

		},{
			header: 'Profit Margin',
			dataIndex: 'persentase_margin',
			align:'center',
			width: 80,
			sortable: true,
		}],		
		bbar: [{
					text: '<span style="margin-left:0px;font-size:12px;">Total :</span>', style: 'margin-left: 410px',
				},{
					xtype: 'numericfield',
					id: 'tf.all_penjualan',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_diskon',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_hpp',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				},{
					xtype: 'numericfield',
					id: 'tf.all_profit',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				}]
	});
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Laporan Transaksi Pasien',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'Laporan Penjualan',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
	function cAdvance(){
		ds_penjualan.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
		ds_penjualan.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
		ds_penjualan.reload();
	}
	
	function cetakLap(){
			var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
			var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
			RH.ShowReport(BASE_URL + 'print/lappenjualanbrg/data/'
					+tglawal+'/'+tglakhir);
		}	
	
	function cetakexcel(){	
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		
		window.location =(BASE_URL + 'print/lap_persediaan_kartu/excelkpersediaan/'
                +tglawal+'/'+tglakhir);
	}

}