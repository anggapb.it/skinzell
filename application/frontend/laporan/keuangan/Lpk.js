function Lpk(){
	
	var ds_pasiendalam = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 't_karyawan_controller/get_tkar_lap',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			baseParams: {
				tglkuitansi1 : Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglkuitansi2 : Ext.util.Format.date(new Date(), 'Y-m-d'),
			},
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'idkuitansidet',
				mapping: 'idkuitansidet'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nominalugd',
				mapping: 'nominalugd'
			},{
				name: 'total_transaksi',
				mapping: 'total_transaksi'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_bayar',
				mapping: 'total_bayar'
			},{
				name: 'jml_tagihan',
				mapping: 'jml_tagihan'
			},{
				name: 'jml_byr',
				mapping: 'jml_byr'
			},{
				name: 'sisa',
				mapping: 'sisa'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'tglkuitansipgreg',
				mapping: 'tglkuitansipgreg'
			},{
				name: 'nama_karyawan',
				mapping: 'nama_karyawan'
			}],
			listeners: {
			load: function(store, records, options) {
					var jumlahbiaya = 0;
					
                    ds_pasiendalam.each(function (rec) { 
                        jumlahbiaya += (parseFloat(rec.get('jumlah'))) ? parseFloat(rec.get('jumlah')):0;
                    });

                    Ext.getCmp('tf.all_transaksi').setValue(jumlahbiaya);
                   
			}
		} 
		});
	
	
	var ds_pasienluar = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 't_karyawan_controller/get_tfarmasi_lap',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			baseParams: {
				//cbxreg : true,
				tglkuitansi1 : Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglkuitansi2 : Ext.util.Format.date(new Date(), 'Y-m-d'),
			},
			autoLoad: true,
			fields: [{
				name: 'noreg',
				mapping: 'noreg'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'atasnama',
				mapping: 'atasnama'
			},{
				name: 'total',
				mapping: 'total'
			},{
				name: 'idkuitansidet',
				mapping: 'idkuitansidet'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'nonota',
				mapping: 'nonota'
			},{
				name: 'total_transaksi',
				mapping: 'total_transaksi'
			},{
				name: 'diskon',
				mapping: 'diskon'
			},{
				name: 'deposit',
				mapping: 'deposit'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'total_bayar',
				mapping: 'total_bayar'
			},{
				name: 'jml_tagihan',
				mapping: 'jml_tagihan'
			},{
				name: 'jml_byr',
				mapping: 'jml_byr'
			},{
				name: 'sisa',
				mapping: 'sisa'
			},{
				name: 'jumlah',
				mapping: 'jumlah'
			},{
				name: 'tglnota',
				mapping: 'tglnota'
			},{
				name: 'tglkuitansi',
				mapping: 'tglkuitansi'
			},{
				name: 'nama_karyawan',
				mapping: 'nama_karyawan'
			},{
				name: 'tglkuitansipgreg',
				mapping: 'tglkuitansipgreg'
			}],
			listeners: {
			load: function(store, records, options) {
					var jumlahbiaya = 0;
					var byr_pasien = 0;
					var jml_tagihan = 0;
					var jml_byr = 0;
					var sisa = 0;
                    ds_pasienluar.each(function (rec) { 
                        jumlahbiaya += (parseFloat(rec.get('jumlah'))) ? parseFloat(rec.get('jumlah')):0;
                 
                    });

                    Ext.getCmp('tf.all_transaksi2').setValue(jumlahbiaya);
            }
		}
		});
	
	
	var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
			header: '<center>Tgl. Pembayaran</center>',
			width: 100,
			dataIndex: 'tglkuitansipgreg',
			align:'center',	
			//format: 'd-m-Y',
		},{
			header: '<center>No. Registrasi</center>',
			width: 87,
			dataIndex: 'noreg',
			align:'center',	
		},{
			header: '<center>Tgl. Kuitansi</center>',
			width: 81,
			dataIndex: 'tglkuitansi',
			align:'center',	
			//format: 'd-m-Y',
		},{
			header: '',
			dataIndex: 'idkuitansidet',
			hidden: true
		},{
			header: '',
			dataIndex: 'idjnspelayanan',
			hidden: true
		},{
			header: '',
			dataIndex: 'nmjnspelayanan',
			hidden: true
		},{
			header: '<center>No. RM</center>',
			width: 80,
			dataIndex: 'norm',
			//align:'center',	
		},{
			header: '<center>Nama Pasien</center>',
			width: 100,
			dataIndex: 'nmpasien',
			//align:'center',				
		},{
			header: '<center>Nama Karyawan</center>',
			width: 100,
			dataIndex: 'nama_karyawan',
			//align:'center',				
		},{
			header: '<center>(L/P)</center>',
			width: 50,
			dataIndex: 'kdjnskelamin',
			align:'center',	
			hidden: true	
		},{
			header: '<center>Tgl. Lahir</center>',
			width: 72,
			dataIndex: 'tgllahir',
			align:'center',	
			hidden: true
		},{
			header: '<center>Jumlah <br> Bayar</center>',
			width: 100,
			dataIndex: 'jumlah',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			/* renderer: function(value, p, r){
				if(r.data['idjnspelayanan'] == 2) {
					var jumlah = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']) - parseFloat(r.data['deposit']);
				} else {
					var jumlah = parseFloat(r.data['total']);
				}
				return Ext.util.Format.number(jumlah, '0,000');
			} */	
		}]
    });
	

	var cm2 = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default           
        },
        columns: [
		new Ext.grid.RowNumberer(),
		{
			header: '<center>Tgl. Pembayaran</center>',
			width: 100,
			dataIndex: 'tglkuitansipgreg',
			align:'center',	
			//format: 'd-m-Y',
		},{
			header: '<center>No. Nota</center>',
			width: 87,
			dataIndex: 'nonota',
			align:'center',	
		},{
			header: '<center>Tgl. Kuitansi</center>',
			width: 81,
			dataIndex: 'tglkuitansi',
			align:'center',	
			format: 'd-m-Y',
		},{
			header: '',
			dataIndex: 'idkuitansidet',
			hidden: true
		},{
			header: '',
			dataIndex: 'idjnspelayanan',
			hidden: true
		},{
			header: '',
			dataIndex: 'nmjnspelayanan',
			hidden: true
		},{
			header: '<center>Nama Pasien</center>',
			width: 150,
			dataIndex: 'atasnama',
			//align:'center',				
		},{
			header: '<center>Nama <br /> Karyawan</center>',
			width: 100,
			dataIndex: 'nama_karyawan',
			//align:'center',	
		},{
			header: '<center>Jumlah <br> Bayar</center>',
			width: 100,
			dataIndex: 'jumlah',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			/* renderer: function(value, p, r){
				if(r.data['idjnspelayanan'] == 2) {
					var jumlah = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']) - parseFloat(r.data['deposit']);
				} else {
					var jumlah = parseFloat(r.data['total']);
				}
				return Ext.util.Format.number(jumlah, '0,000');
			} */	
		}]
    });
	
	
	
		var paging = new Ext.PagingToolbar({
		pageSize: 25,
		store: ds_pasiendalam,
		displayInfo: true,
		displayMsg: 'Data Pembelian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var sm = new Ext.grid.RowSelectionModel({
		singleSelect: true,
		listeners: {}
	});
		
	var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});
		
	var grid_tfarmasi = new Ext.grid.EditorGridPanel({
		id: 'id_gridtfarmasi',
		store: ds_pasienluar,
        vw:vw,
		cm:cm2,
		sm:sm,
		//tbar: 				[],
		frame: true,
		height: 200,
		autoScroll: true,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		bbar: [{
					text: '<span style="margin-left:0px;font-size:12px;">Total :</span>', style: 'margin-left: 500px',
				},{
					xtype: 'numericfield',
					id: 'tf.all_transaksi2',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				}],
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
			
			},
		}
	});	
	
	
	var grid_tkar = new Ext.grid.EditorGridPanel({
		id: 'id_gridtkar',
		store: ds_pasiendalam,
        vw:vw,
		cm:cm,
		sm:sm,
		//tbar: 				[],
		frame: true,
		height: 200,
		autoScroll: true,
		loadMask: true,
        forceFit: true,
		buttonAlign: 'left',
		bbar: [{
					text: '<span style="margin-left:0px;font-size:12px;">Total :</span>', style: 'margin-left: 530px',
				},{
					xtype: 'numericfield',
					id: 'tf.all_transaksi',
					width:100,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					readOnly: true,
					style: 'opacity: 0.6; font-weight: bold;',
				}],
		listeners: {
			cellclick: function cellclick(grid, rowIdx, columnIdx, e) {
			
			},
		}
	});	
	
	
	
	var lap_pg = new Ext.FormPanel({
		id: 'fp.pasienrj',
		title: 'Laporan Pembayaran Karyawan (PG)',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 50, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: 1,
					border: false,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglkuitansi1',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglkuitansi2',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						}]
					}]
				}]
			},{
					xtype: 'fieldset', title: '',
					id: 'fs.tes', layout: 'column',
					//defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[
						{					
							xtype:'button', 
							iconCls:'silk-printer', 
							id:'button_cetak',
							text:'Cetak', //margins:'0 0 0 10',
							disabled:false,
							handler: function(){ cetakLapKeuangan(); }
						},
						{					
							xtype:'button', 
							iconCls:'silk-printer', 
							id:'button_excel',
							text:'Cetak Excel', //margins:'0 0 0 10',
							disabled:false,
							handler: function(){ exportdata(); }
						},
					]
			},{
					xtype: 'fieldset', title: 'Rawat Jalan / UGD / Rawat Inap',
					id: 'fs.grid_tkar', layout: 'column',
					//defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[grid_tkar]
			},{
					xtype: 'fieldset', title: 'Pelayanan Tambahan / Farmasi Pasien Luar',
					id: 'fs.grid_tfarmasi', layout: 'column',
					//defaults: { labelWidth: 125, labelAlign: 'right', }, 
					items:[grid_tfarmasi]
			}
			
		]
	});
	SET_PAGE_CONTENT(lap_pg);

	function cAdvance(){
		ds_pasiendalam.setBaseParam('tglkuitansi1',Ext.getCmp('df.tglkuitansi1').getValue().format('Y-m-d'));
		ds_pasiendalam.setBaseParam('tglkuitansi2',Ext.getCmp('df.tglkuitansi2').getValue().format('Y-m-d'));
		ds_pasiendalam.reload();
		
		ds_pasienluar.setBaseParam('tglkuitansi1',Ext.getCmp('df.tglkuitansi1').getValue().format('Y-m-d'));
		ds_pasienluar.setBaseParam('tglkuitansi2',Ext.getCmp('df.tglkuitansi2').getValue().format('Y-m-d'));
		ds_pasienluar.reload();
	}	
	
	function cetakLapKeuangan(){
		var tglawal		= Ext.getCmp('df.tglkuitansi1').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglkuitansi2').getValue().format('Y-m-d');
		RH.ShowReport(BASE_URL + 'print/lap_pk/cetak_pk/'
                +tglawal+'/'+tglakhir);
	}

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglkuitansi1').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglkuitansi2').getValue().format('Y-m-d');
		window.location = BASE_URL + 'print/lap_pk/excelpk/'+tglawal+'/'+tglakhir;

    }
	
	
}