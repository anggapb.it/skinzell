function LPenerimaanUGD(){
	var ds_vlapugdperiode = dm_vlapugdperiode();
	ds_vlapugdperiode.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlapugdperiode.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	
	var cm_laporan = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header: 'No. Reg',
			dataIndex: 'noreg',
			width: 80
		},{
			header: 'Tgl. Reg',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 60,
		},{
			header: 'Nama Pasien',
			dataIndex: 'nmpasien',
			width: 130
		},{
			header: 'Obat',
			dataIndex: 'uobat',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Pemeriksaan',
			dataIndex: 'upemeriksaan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 100
		},{
			header: 'Tindakan',
			dataIndex: 'utindakan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Imunisasi',
			dataIndex: 'uimun',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'LAB',
			dataIndex: 'ulab',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Racik',
			dataIndex: 'uracik',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Lain-lain',
			dataIndex: 'ulain',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Disc',
			dataIndex: 'udiskon',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Non Tunai',
			dataIndex: 'ucc',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Tunai',
			dataIndex: 'utotal',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Total UGD',
			dataIndex: 'total',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Total Transfer',
			dataIndex: 'utotaltrans',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Dokter',
			dataIndex: 'nmdoktergelar',
			width: 100
		}
	]);
	var paging_laporan = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_vlapugdperiode,
		displayInfo: true,
		displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_laporan = new Ext.grid.GridPanel({
		ds: ds_vlapugdperiode,
		cm: cm_laporan,
		height: 350,
		autoWidth: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		buttonAlign: 'left',
		layout: 'anchor',
		frame: true,
		tbar: [
			{ text: 'Cetak', iconCls: 'silk-printer', handler: function(){cetakLapUGD();} },'-',
			{ text: 'Cetak Excel', iconCls: 'silk-printer', handler: function(){exportdata();} }
		],
		anchorSize: {
			width: 400,
			height: 400
		},
		bbar: paging_laporan
	});
	
	var lapperperiode = new Ext.FormPanel({
		id: 'fp.pasienrj',
		title: 'Laporan Registrasi UGD Per Periode',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 50, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: 1,
					border: false,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						}]
					}]
				}]
			},
			grid_laporan
		]
	});
	SET_PAGE_CONTENT(lapperperiode);	
	
	function cAdvance(){
		ds_vlapugdperiode.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlapugdperiode.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlapugdperiode.reload();
	}
	
	function cetakLapUGD(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		RH.ShowReport(BASE_URL + 'print/lapkeuangan/lappenerimaanugd/'
                +tglawal+'/'+tglakhir);
	}

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');

            window.location = BASE_URL + 'print/lapkeuangan/excelpenerimaanugd/'+tglawal+'/'+tglakhir;

    }
	
}