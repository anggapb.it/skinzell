function LReturDeposit(){
	Ext.form.Field.prototype.msgTarget = 'side';
	
	var ds_lapreturdeposit = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'lapreturdeposit_controller/get_lapreturdeposit',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "kdjurnal",
			mapping: "kdjurnal"
		},{
			name: "tgltransaksi",
			mapping: "tgltransaksi"
		},{
			name: "noreg",
			mapping: "noreg"
		},{
			name: "norm",
			mapping: "norm"
		},{
			name: "nmpasien",
			mapping: "nmpasien"
		},{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "idkamar",
			mapping: "idkamar"
		},{
			name: "nmkamar",
			mapping: "nmkamar"
		},{
			name: "idbed",
			mapping: "idbed"
		},{
			name: "nmbed",
			mapping: "nmbed"
		},{
			name: "idklstarif",
			mapping: "idklstarif"
		},{
			name: "nmklstarif",
			mapping: "nmklstarif"
		},{
			name: "iddokter",
			mapping: "iddokter"
		},{
			name: "nmdoktergelar",
			mapping: "nmdoktergelar"
		},{
			name: "nona",
			mapping: "nona"
		},{
			name: "idstposisipasien",
			mapping: "idstposisipasien"
		},{
			name: "nmstposisipasien",
			mapping: "nmstposisipasien"
		},{
			name: "tottransaksi",
			mapping: "tottransaksi"
		},{
			name: "totdeposit",
			mapping: "totdeposit"
		},{
			name: "nominal",
			mapping: "nominal"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		},{
			name: "tglinput",
			mapping: "tglinput"
		},{
			name: "nokasir",
			mapping: "nokasir"
		},{
			name: "idstkasir",
			mapping: "idstkasir"
		},{
			name: "nmstkasir",
			mapping: "nmstkasir"
		},{
			name: "keterangan",
			mapping: "keterangan"
		},{
			name: "idjnstransaksi",
			mapping: "idjnstransaksi"
		},{
			name: "nmjnstransaksi",
			mapping: "nmjnstransaksi"
		},{
			name: "penerima",
			mapping: "penerima"
		},]
	});
	
	var vw_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_lapreturfarmasipl',
		store: ds_lapreturdeposit,
		view: vw_nya,	
		frame: true,
		autoScroll: true,
		autoWidth: true,
		columnLines: true,
		autoScroll: true,
		loadMask: true,
		height: 445,
		tbar: [{
			text: 'Cetak PDF',
			id: 'btn.cetak',	
			iconCls:'silk-printer',		
			handler: function() {
				cetak_pdf();
			}
		},'-',
		{ 	text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',	
			handler: function(){
				cetak_excel();
			}
		}],
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. Retur<br>Deposit'),
			width: 100,
			dataIndex: 'kdjurnal',
			sortable: true,
			align:'center'
		},{
			header: headerGerid('Tgl Retur<br>Deposit'),
			width: 100,
			dataIndex: 'tgltransaksi',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('No. Registrasi'),
			width: 100,
			dataIndex: 'noreg',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('No. RM'),
			width: 80,
			dataIndex: 'norm',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Nama Pasien'),
			width: 130,
			dataIndex: 'nmpasien',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Ruangan'),
			width: 100,
			dataIndex: 'nmkamar',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Dokter'),
			width: 160,
			dataIndex: 'nmdoktergelar',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Total Transaksi'),
			width: 100,
			dataIndex: 'tottransaksi',
			sortable: true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: headerGerid('Total Deposit'),
			width: 100,
			dataIndex: 'totdeposit',
			sortable: true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: headerGerid('Retur Deposit'),
			width: 100,
			dataIndex: 'nominal',
			sortable: true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: headerGerid('User ID'),
			width: 100,
			dataIndex: 'userid',
			sortable: true,
			align:'center',
		}]
	});
       
	var form_bp_general = new Ext.FormPanel({
		id: 'form_bp_general',
		title: 'Laporan Retur Deposit',
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		height:1000,
		items: [
		{
				layout: 'form',
				border: false,
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .99,
						border: false,
						items: [{
							xtype: 'compositefield',
							style: 'padding: 5px; marginLeft: -5px; marginBottom: -6px',
							width: 470,
							items: [{
								xtype: 'label', id: 'lb.bgna', text: 'Periode :', margins: '3 10 0 0', //atas kanan bawah kiri
							},{
								xtype: 'datefield',
								id: 'df.tglawal',
								width: 110,
								value: new Date(),
								format: 'd-m-Y',
								listeners:{
									select: function(field, newValue){
										cektgl();
									}
								}
							},{
								xtype: 'label', id: 'lb.sd', text: ' s.d ', margins: '3 10 0 5',
							},{
								xtype: 'datefield',
								id: 'df.tglakhir',
								width: 110,
								value: new Date(),
								format: 'd-m-Y',
								listeners:{
									select: function(field, newValue){
										cektgl();
									}
								}
							}]
						}]
					}]
				}, grid_nya
				]
		
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadLapreturdeposit(){
		ds_lapreturdeposit.reload();
	}
	
	function cektgl(){
		ds_lapreturdeposit.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_lapreturdeposit.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		reloadLapreturdeposit();
	}
	
	function cetak_pdf(){
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		RH.ShowReport(BASE_URL + 'print/lap_retur_deposit/laporan_retur_deposit_pdf/'
                +tglawal+'/'+tglakhir);
	}
	
	function cetak_excel(){
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		RH.ShowReport(BASE_URL + 'print/lap_retur_deposit/laporan_retur_deposit_excel/'
                +tglawal+'/'+tglakhir);
	}

}