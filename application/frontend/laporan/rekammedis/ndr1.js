function ndr(){
	
	
/* Data Store */

	
	var ds_bagian = dm_bagianri();
	
	var ds_ndr = dm_ndr();
//	ds_ndr.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
//	ds_ndr.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
/* End Data Store */

/* GRID */
	var pageSize = 50;
	var paging = new Ext.PagingToolbar({
	pageSize: pageSize,
		store: ds_ndr,
		displayInfo: true,
		displayMsg: 'Data NDR Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_ndr,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [{
			header: 'Ruangan',dataIndex: 'nmbagian',
			//align: 'center', 
			sortable: true, width: 100
		},
		{
			header: 'TT<br>(Tempat Tidur)',dataIndex: 'jumlahbed',
		//	align: 'center', 
			sortable: true, width: 100
		},
		{
			header: 'NDR (%)', dataIndex: 'ndr',
		//	align: 'center',
			sortable: true, width: 100,xtype: 'numbercolumn', format:'0,000.00',
		}],		
		bbar: paging
	});
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'NDR(Net Death Rate)',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
				//	defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d', hidden: true,
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
						//	hidden: true,
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
						//	style: 'padding: 10px',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					}/* ,{
						xtype: 'compositefield', style: 'margin: 10px 0 0 0',
						items:[{
								xtype: 'tbtext',
								text: 'Ruangan :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
						xtype: 'combo', fieldLabel: '',
						id: 'cb.ruangan', width: 150,
						store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih Ruangan',
						listeners:{
							select:function(combo, records, eOpts){
								cAdvance();
							}
						}
						}]
					} */]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'NDR',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
function cAdvance(){
	ds_ndr.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
	ds_ndr.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
//	ds_ndr.setBaseParam('bagian',Ext.getCmp('cb.ruangan').getValue());
	ds_ndr.reload();
}
function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
	//	var ruangan     = Ext.getCmp('cb.ruangan').getValue();
	//	alert(ruangan)
		RH.ShowReport(BASE_URL + 'print/Lap_rekamedis_bor/rekammedis/'
                +tglawal+'/'+tglakhir);
	}	
	
}