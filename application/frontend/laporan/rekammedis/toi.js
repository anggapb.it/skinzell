function toi(){

	var nbsp1 = ''
	var nbsp2 = ''
	
	for (a=1;a<=12;a++) {
		nbsp1 = nbsp1 + '&nbsp;';
	}
					
	for (a=1;a<=34;a++) {
		nbsp2 = nbsp2 + '&nbsp;';
	}
// ============= GRID RESULT TOI

	var fields_result = RH.storeFields('idbagian','ruangan','jmltempattidurresult','jmlhariperawatanresult','jmlpasienresult',
	'jmlpasienhidupresult','jmlpasienmatiresult',
	'hasilresult');
					
	var ds_result = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_toi_controller/get_result', 
				method: 'POST'
			}),
			baseParams: {
			
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields : fields_result,
	});
	
	var search_result = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];

	var grid_result = new Ext.grid.GridPanel({
		id: 'grid_result', //sm: cbGrid, 
		store: ds_result,
		//plugins: search_result,
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: '<center><b>No.</b></center>', width:35 }),
		{
			header: '<center><b>Ruangan</b></center>',dataIndex: 'ruangan',
			align: 'left',
			sortable: true, width: 130,
		},{
			header: '<center><b>Jumlah<br>Tempat Tidur</b></center>',dataIndex: 'jmltempattidurresult',
			align: 'center', renderer: function(value) { return value + ' Tempat Tidur'; },
			sortable: true, width: 104
		},{
			header: '<center><b>Hari<br>Perawatan</b></center>',dataIndex: 'jmlhariperawatanresult',
			align: 'center', renderer: function(value) { return value + ' Hari'; },
			sortable: true, width: 104
		},{
			header: '<center><b>Jumlah<br>Pasien</b></center>',dataIndex: 'jmlpasienresult',
			align: 'center', renderer: function(value) { return value + ' Orang'; },
			sortable: true, width: 104
		},{
			header: '<center><b>Jumlah<br>Pasien Hidup</b></center>',dataIndex: 'jmlpasienhidupresult',
			align: 'center', renderer: function(value) { return value + ' Orang'; },
			sortable: true, width: 104
		},{
			header: '<center><b>Jumlah<br>Pasien Mati</b></center>',dataIndex: 'jmlpasienmatiresult',
			align: 'center', renderer: function(value) { return value + ' Orang'; },
			sortable: true, width: 104
		},{
			header: '<center><b>Hasil</b></center>',dataIndex: 'hasilresult',
			align: 'center', renderer: function(value) { return value + ' Hari'; },
			sortable: true, width: 104
		}],
		bbar:[{
			xtype: 'label', text: 'Jumlah:', style:'font-size:12px;font-weight:bold;margin-left:115px;margin-right:5px'
		},{
			xtype: 'textfield', fieldLabel:'User Input', 
			id: 'tf.jmltempattidurresult', readOnly: true, style:'opacity: 0.6;text-align: center', width: 105
		},{
			xtype: 'textfield', fieldLabel:'User Input', 
			id: 'tf.jmlhariperawatanresult', readOnly: true, style:'opacity: 0.6;text-align: center', width: 105
		},{
			xtype: 'textfield', fieldLabel:'User Input', 
			id: 'tf.jmlpasienresult', readOnly: true, style:'opacity: 0.6;text-align: center', width: 105
		},{
			xtype: 'textfield', fieldLabel:'User Input', 
			id: 'tf.jmlpasienhidupresult', readOnly: true, style:'opacity: 0.6;text-align: center', width: 105
		},{
			xtype: 'textfield', fieldLabel:'User Input', 
			id: 'tf.jmlpasienmatiresult', readOnly: true, style:'opacity: 0.6;text-align: center', width: 105
		},{
			xtype: 'textfield', fieldLabel:'User Input', 
			id: 'tf.hasilresult', readOnly: true, style:'opacity: 0.6;text-align: center', width: 105
		}],	
		listeners: {
			cellclick: function (grid, rowIndex, columnIndex, event) {
				
			}
		}
	});
// ============= END GRID RESULT TOI

// ============= GRID RIWAYAT TOI
	var limitx = 10;
	var fields_nya = RH.storeFields('idtoi','daritgl','sampaitgl','jmltempattidur',
	'jmlhariperawatan','jmlpasien','jmlpasienhidup','jmlpasienmati','hasil','catatan','userid','tglbuat',
	'periode','jmlperiode','jmlperioderender','jmltempattidurrender','jmlhariperawatanrender','jmlpasienrender','jmlpasienhiduprender','jmlpasienmatirender',
	'hasilrender','tglbuatrender','nmlengkap');
					
	var ds_nya = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_toi_controller/get_history', 
				method: 'POST'
			}),
			baseParams: {
				start:0, limit:limitx,
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields : fields_nya,
	});
	
	var search_nya = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: limitx,
		store: ds_nya,
		displayInfo: true,
		displayMsg: 'Data Dari {0} - {1} of {2}',
		emptyMsg: 'Data Kosong'
	});

	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_nya', //sm: cbGrid, 
		store: ds_nya,
		plugins: search_nya,
		frame: true,
		height: 235,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		bbar: paging,
		columns: [new Ext.grid.RowNumberer({ header: '<center><b>No.</b></center>', width:35 }),
		{
			header: '<center><b>Periode</b></center>',dataIndex: 'periode',
			align: 'center',
			sortable: true, width: 150,
			renderer: function(value) {
				Ext.QuickTips.init();
				return '<div class="keyMasterDetail" ext:qtip="Lihat detail data" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
					+ value +'</div>';
			}
		},{
			header: '<center><b>Periode</b></center>',dataIndex: 'jmlperioderender',
			align: 'left',
			sortable: true, width: 104
		},{
			header: '<center><b>Jumlah<br>Tempat Tidur</b></center>',dataIndex: 'jmltempattidurrender',
			align: 'left',
			sortable: true, width: 104
		},{
			header: '<center><b>Hari<br>Perawatan</b></center>',dataIndex: 'jmlhariperawatanrender',
			align: 'left',
			sortable: true, width: 104
		},{
			header: '<center><b>Jumlah<br>Pasien</b></center>',dataIndex: 'jmlpasienrender',
			align: 'left',
			sortable: true, width: 104
		},{
			header: '<center><b>Jumlah<br>Pasien Hidup</b></center>',dataIndex: 'jmlpasienhiduprender',
			align: 'left',
			sortable: true, width: 104
		},{
			header: '<center><b>Jumlah<br>Pasien Mati</b></center>',dataIndex: 'jmlpasienmatirender',
			align: 'left',
			sortable: true, width: 104
		},{
			header: '<center><b>Hasil</b></center>',dataIndex: 'hasilrender',
			align: 'left',
			sortable: true, width: 104
		},{
			header: '<center><b>Catatan</b></center>',dataIndex: 'catatan',
			align: 'left',
			sortable: true, width: 106
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: '<center><b>Hapus</b></center>',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						var record = ds_nya.getAt(rowIndex);
						var url = BASE_URL + 'laporan_toi_controller/delete_history';
						var params = new Object({
										idtoi	: record.data['idtoi']
									});
						RH.deleteGridRecord(url, params, grid );
                    }
                }]
        }],	
		listeners: {
			cellclick: function (grid, rowIndex, columnIndex, event) {
				var t = event.getTarget();
			
				if (t.className == 'keyMasterDetail'){					
					var record = grid.getStore().getAt(rowIndex);
					
					Ext.getCmp('tglawal').setValue(record.get("daritgl"));	
					Ext.getCmp('tglakhir').setValue(record.get("sampaitgl"));	
					Ext.getCmp('periode').setValue(record.get("jmlperiode"));	
					Ext.getCmp('catatan').setValue(record.get("catatan"));	
					Ext.getCmp('tglproses').setValue(record.get("tglbuat"));	
					Ext.getCmp('username').setValue(record.get("nmlengkap"));	
					Ext.getCmp('userid').setValue(record.get("userid"));
					
					get_save_result(record.get("idtoi"));
					
					Ext.getCmp('btnproses').disable();
				}
			}
		}
	});
// ============= END GRID RIWAYAT TOI

		var toi_form = new Ext.form.FormPanel({
			id: 'toi_form',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'TOI (Turn Over Interval = Tenggang Perputaran)',
			autoScroll: true,
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				title: 'Perhitungan',
				layout: 'form',
				items: [{
						layout: 'form', bodyStyle: 'padding: 10px;',
						items: [{
							xtype: 'tbtext', style:'font-size:12px', id: 'info',
							text: 'TOI adalah rata-rata hari dimana tempat tidur tidak ditempati dari telah diisi ke saat terisi berikutnya. ' +
							'Indikator ini memberikan gambaran tingkat efisiensi penggunaan tempat tidur. ' +
							'Idealnya tempat tidur kosong tidak terisi pada kisaran 1-3 hari (Depkes RI, 2005).<br><br>' +
							'Rumus : ' + nbsp1 + '<u>((Jumlah Tempat Tidur X Periode) - Hari Perawatan)</u><br>' +
							nbsp2 + '(Jumlah Pasien Keluar (Hidup + Mati))'
							}]
					},{
						layout: 'form', bodyStyle: 'padding: 10px;',
						items: [{
								xtype: 'fieldset',
								frame: false,
								border: true,
								layout: 'column',
								items: [{
									layout: 'form', columnWidth: 0.60,
									items: [{
										xtype: 'compositefield',
										items: [{
												xtype: 'datefield', fieldLabel:'Periode', id: 'tglawal',
												width: 100, value: new Date(),
												format: 'd-m-Y',
												listeners:{
													select: function(field, newValue){
													
													},
													change : function(field, newValue){
													
													}
												}
											},{
												xtype: 'label', id: 'lb.sd', text: 's/d',
											},{
												xtype: 'datefield', id: 'tglakhir',
												width: 100, value: new Date(),
												format: 'd-m-Y',
												listeners:{
													select: function(field, newValue){
													
													},
													change : function(field, newValue){
													
													}
												}
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'textfield', fieldLabel: 'Periode',
												id: 'periode', width: 100, allowBlank: false,
												readOnly: true, style:'opacity: 0.6;text-align: right', 
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Hari'
											}]
										}]
								},{
									layout: 'form', columnWidth: 0.40,
									items: [{
											xtype: 'textarea', fieldLabel: 'Catatan',
											id: 'catatan', width: 300, height: 47
										}]
								},{
									layout: 'form', columnWidth: 0.79,
									items: [grid_result]
								},{
									layout: 'form', columnWidth: 0.21,
									items: [{
											xtype: 'datefield', id: 'tglproses',
											width: 100, value: new Date(), fieldLabel:'Tanggal Proses', 
											format: 'd-m-Y', readOnly: true, style:'opacity: 0.6', 
											listeners:{
												select: function(field, newValue){
													
												},
												change : function(field, newValue){
													
												}
											}
										},{
											xtype: 'textfield', value: USERNAME, fieldLabel:'User Input', 
											id: 'username', readOnly: true, style:'opacity: 0.6', width: 100
										},{
											xtype: 'textfield', value: USERID,
											id: 'userid', hidden: true, width: 100
									}],
									buttons: [{
										text: 'Reset',
										id: 'btnreset',
										width: 60, iconCls: 'silk-arrow-refresh',
										handler: function() {
											reset_form();
										}
									},{
										text: 'Proses',
										id: 'btnproses',
										width: 60, iconCls: 'silk-selesai',
										handler: function() {
											get_result();
										}
									},{
										text: 'Simpan',
										id: 'btnsimpan',
										iconCls: 'silk-save',
										width: 60,
										handler: function() {
											fnSaveTOI();
										}
									}]
								}]
							}]
					}]
				},{
					xtype: 'fieldset',
					title: 'Riwayat TOI',
					layout: 'form',
					items: [grid_nya]
				}
			],
			listeners: {
				afterrender: function () {
					Ext.getCmp('btnsimpan').disable();
					Ext.Ajax.request({
						url: BASE_URL + 'laporan_toi_controller/get_date_server',
						method: 'POST',
						params: {
						
						},
						success: function(response){
							obj = Ext.util.JSON.decode(response.responseText);
							Ext.getCmp('tglproses').setValue(obj.date);
						},
						failure : function(){
							Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
						}
					});
				}
			}
		}); SET_PAGE_CONTENT(toi_form);
		
	function reset_form(){
		toi_form.getForm().reset();
		Ext.getCmp('btnproses').enable();
		Ext.getCmp('btnsimpan').disable();
		grid_result.getStore().removeAll();
		Ext.getCmp('tf.jmltempattidurresult').setValue(null);
		Ext.getCmp('tf.jmlhariperawatanresult').setValue(null);
		Ext.getCmp('tf.jmlpasienresult').setValue(null);
		Ext.getCmp('tf.jmlpasienhidupresult').setValue(null);
		Ext.getCmp('tf.jmlpasienmatiresult').setValue(null);
		Ext.getCmp('tf.hasilresult').setValue(null);
	}
		
	function get_result(){
		var tglawal = Ext.getCmp('tglawal').getValue();
		var tglakhir = Ext.getCmp('tglakhir').getValue();
		var interval = (tglawal.format('Y-m-d')==tglakhir.format('Y-m-d')) ? 1:Math.ceil((tglakhir.getTime()-tglawal.getTime())/(1000*60*60*24)) + 1;
		
		ds_result.reload({
			params: {
				tglawal		:tglawal,
				tglakhir	:tglakhir,
				isgetresult	:1,
			},
			scope   : this,
            callback: function(records, operation, success) {
                var jmltempattidurresult = 0, jmlhariperawatanresult = 0, jmlpasienresult = 0, jmlpasienhidupresult = 0, jmlpasienmatiresult = 0, hasilresult = 0;
				Ext.getCmp('periode').setValue(interval);
				ds_result.each(function(rec){
					jmltempattidurresult += parseFloat(rec.get('jmltempattidurresult'));
					jmlhariperawatanresult += parseFloat(rec.get('jmlhariperawatanresult'));
					jmlpasienresult += parseFloat(rec.get('jmlpasienresult'));
					jmlpasienhidupresult += parseFloat(rec.get('jmlpasienhidupresult'));
					jmlpasienmatiresult += parseFloat(rec.get('jmlpasienmatiresult'));
					hasilresult += parseFloat(rec.get('hasilresult'));
				});
				Ext.getCmp('tf.jmltempattidurresult').setValue(jmltempattidurresult + ' Tempat Tidur');
				Ext.getCmp('tf.jmlhariperawatanresult').setValue(jmlhariperawatanresult + ' Hari');
				Ext.getCmp('tf.jmlpasienresult').setValue(jmlpasienresult + ' Orang');
				Ext.getCmp('tf.jmlpasienhidupresult').setValue(jmlpasienhidupresult + ' Orang');
				Ext.getCmp('tf.jmlpasienmatiresult').setValue(jmlpasienmatiresult + ' Orang');
				Ext.getCmp('tf.hasilresult').setValue(hasilresult + ' Hari');
				Ext.getCmp('btnsimpan').enable();
			}
		});
	}
	
	function get_save_result(idtoi){

		ds_result.reload({
			params: {
				idtoi		:idtoi,
				isgetresult	:0,
			},
			scope   : this,
            callback: function(records, operation, success) {
                var jmltempattidurresult = 0, jmlhariperawatanresult = 0, jmlpasienresult = 0, jmlpasienhidupresult = 0, jmlpasienmatiresult = 0, hasilresult = 0;
				ds_result.each(function(rec){
					jmltempattidurresult += parseFloat(rec.get('jmltempattidurresult'));
					jmlhariperawatanresult += parseFloat(rec.get('jmlhariperawatanresult'));
					jmlpasienresult += parseFloat(rec.get('jmlpasienresult'));
					jmlpasienhidupresult += parseFloat(rec.get('jmlpasienhidupresult'));
					jmlpasienmatiresult += parseFloat(rec.get('jmlpasienmatiresult'));
					hasilresult += parseFloat(rec.get('hasilresult'));
				});
				Ext.getCmp('tf.jmltempattidurresult').setValue(jmltempattidurresult + ' Tempat Tidur');
				Ext.getCmp('tf.jmlhariperawatanresult').setValue(jmlhariperawatanresult + ' Hari');
				Ext.getCmp('tf.jmlpasienresult').setValue(jmlpasienresult + ' Orang');
				Ext.getCmp('tf.jmlpasienhidupresult').setValue(jmlpasienhidupresult + ' Orang');
				Ext.getCmp('tf.jmlpasienmatiresult').setValue(jmlpasienmatiresult + ' Orang');
				Ext.getCmp('tf.hasilresult').setValue(hasilresult + ' Hari');
			}
		});
	}
	
	function fnSaveTOI(){
			var form_nya = Ext.getCmp('toi_form');
			var dataObject = [];
			var records;
			
			for(var a = 0; a < ds_result.data.items.length; a++){
				records = ds_result.data.items[a].data;
				dataObject[a] = { "idbagian": records.idbagian, 
								"jmltempattidur": records.jmltempattidurresult,
								"jmlhariperawatan": records.jmlhariperawatanresult, 
								"jmlpasien": records.jmlpasienresult,
								"jmlpasienhidup": records.jmlpasienhidupresult, 
								"jmlpasienmati": records.jmlpasienmatiresult,
								"hasil": records.hasilresult 
								};

			}
			
			if(form_nya.getForm().isValid()){
				form_nya.getForm().submit({
					url: BASE_URL +'laporan_toi_controller/simpan_hasil',
					method: 'POST',
					waitMsg: 'Menyimpan Data...',
					params: {
						jmltempattidur	: Ext.getCmp('tf.jmltempattidurresult').getValue().replace(/[A-Za-z$-]/g, ""),
						jmlhariperawatan: Ext.getCmp('tf.jmlhariperawatanresult').getValue().replace(/[A-Za-z$-]/g, ""),
						jmlpasien		: Ext.getCmp('tf.jmlpasienresult').getValue().replace(/[A-Za-z$-]/g, ""),
						jmlpasienhidup	: Ext.getCmp('tf.jmlpasienhidupresult').getValue().replace(/[A-Za-z$-]/g, ""),
						jmlpasienmati	: Ext.getCmp('tf.jmlpasienmatiresult').getValue().replace(/[A-Za-z$-]/g, ""),
						hasil			: Ext.getCmp('tf.hasilresult').getValue().replace(/[A-Za-z$-]/g, ""),
						dataObject 		: Ext.encode(dataObject),
					},
					success: function(form_nya, o) {
						Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
						reset_form();
						ds_nya.reload();
					},
					failure: function() {
						Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
					}
				});
			} else {
				Ext.MessageBox.alert("Informasi", "Silahkan Proses Terlebih Dahulu");
			}
	}
	
}