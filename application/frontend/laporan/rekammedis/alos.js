function alos(){

// ============= GRID RIWAYAT ALOS
	var limitx = 10;
	var fields_nya = RH.storeFields('idalos','daritgl','sampaitgl','jmllamadirawat','jmlpasien','jmlpasienhidup','jmlpasienmati','hasilhari','tglbuat','userid','catatan',
								'periode','jmllamadirawatrender','jmlpasienrender','hasilharirender','tglbuatrender','nmlengkap');
	
	var nbsp1 = ''
	var nbsp2 = ''
	
	for (a=1;a<=10;a++) {
		nbsp1 = nbsp1 + '&nbsp;';
	}
					
	for (a=1;a<=12;a++) {
		nbsp2 = nbsp2 + '&nbsp;';
	}
	
	var ds_nya = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_alos_controller/get_history', 
				method: 'POST'
			}),
			baseParams: {
				start:0, limit:limitx,
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields : fields_nya,
	});
	
	var search_nya = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: limitx,
		store: ds_nya,
		displayInfo: true,
		displayMsg: 'Data Dari {0} - {1} of {2}',
		emptyMsg: 'Data Kosong'
	});

	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_nya', //sm: cbGrid, 
		store: ds_nya,
		plugins: search_nya,
		frame: true,
		height: 235,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		bbar: paging,
		columns: [new Ext.grid.RowNumberer({ header: '<center><b>No.</b></center>', width:35 }),
		{
			header: '<center><b>Periode</b></center>',dataIndex: 'periode',
			align: 'center',
			sortable: true, width: 150,
			renderer: function(value) {
				Ext.QuickTips.init();
				return '<div class="keyMasterDetail" ext:qtip="Lihat detail data" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
					+ value +'</div>';
			}
		},{
			header: '<center><b>Jumlah Lama Dirawat</b></center>',dataIndex: 'jmllamadirawatrender',
			align: 'center',
			sortable: true, width: 150
		},{
			header: '<center><b>Jumlah Pasien Keluar</b></center>',dataIndex: 'jmlpasienrender',
			align: 'center',
			sortable: true, width: 140
		},{
			header: '<center><b>Hasil</b></center>',dataIndex: 'hasilharirender',
			align: 'center',
			sortable: true, width: 110
		},{
			header: '<center><b>Catatan</b></center>',dataIndex: 'catatan',
			align: 'left',
			sortable: true, width: 200
		},{
			header: '<center><b>Tgl Proses</b></center>',dataIndex: 'tglbuatrender',
			align: 'center',
			sortable: true, width: 100
		},{
			header: '<center><b>User Input</b></center>',dataIndex: 'nmlengkap',
			align: 'left',
			sortable: true, width: 132
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: '<center><b>Hapus</b></center>',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						var record = ds_nya.getAt(rowIndex);
						var url = BASE_URL + 'laporan_alos_controller/delete_history';
						var params = new Object({
										idalos	: record.data['idalos']
									});
						RH.deleteGridRecord(url, params, grid );
                    }
                }]
        }],	
		listeners: {
			cellclick: function (grid, rowIndex, columnIndex, event) {
				var t = event.getTarget();
			
				if (t.className == 'keyMasterDetail'){					
					var record = grid.getStore().getAt(rowIndex);
					
					Ext.getCmp('tglawal').setValue(record.get("daritgl"));	
					Ext.getCmp('tglakhir').setValue(record.get("sampaitgl"));	
					Ext.getCmp('jld').setValue(record.get("jmllamadirawat"));	
					Ext.getCmp('jpk').setValue(record.get("jmlpasien"));	
					Ext.getCmp('hidup').setValue(record.get("jmlpasienhidup"));	
					Ext.getCmp('mati').setValue(record.get("jmlpasienmati"));	
					Ext.getCmp('hasil').setValue(record.get("hasilhari"));	
					
					Ext.getCmp('catatan').setValue(record.get("catatan"));	
					Ext.getCmp('tglproses').setValue(record.get("tglbuat"));	
					Ext.getCmp('username').setValue(record.get("nmlengkap"));	
					Ext.getCmp('userid').setValue(record.get("userid"));	
					
					Ext.getCmp('btnproses').disable();
				}
			}
		}
	});
// ============= END GRID RIWAYAT ALOS

		var alos_form = new Ext.form.FormPanel({
			id: 'alos_form',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'ALOS (Average Length of Stay = Rata-rata lamanya pasien dirawat)',
			autoScroll: true,
			defaults: { labelWidth: 185, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				title: 'Perhitungan',
				layout: 'form',
				items: [{
						layout: 'form', bodyStyle: 'padding: 10px;',
						items: [{
							xtype: 'tbtext', style:'font-size:12px', id: 'info',
							text: 'ALOS adalah rata-rata lama rawat seorang pasien. ' +
							'Indikator ini disamping memberikan gambaran tingkat efisiensi, juga dapat memberikan gambaran mutu pelayanan. ' +
							'Apabila diterapkan pada diagnosis tertentu dapat dijadikan hal yang perlu pengamatan yang lebih lanjut. ' +
							'Secara umum nilai ALOS yang ideal antara 6-9 hari (Depkes RI, 2005).<br><br>' +
							'Rumus : ' + nbsp1 + '<u>(Jumlah Lama Dirawat)</u><br>' +
							nbsp2 + '(Jumlah Pasien Keluar (Hidup + Mati))'
							}]
					},{
						layout: 'form', bodyStyle: 'padding: 10px;',
						items: [{
								xtype: 'fieldset',
								frame: false,
								border: true,
								layout: 'column',
								items: [{
									layout: 'form', columnWidth: 0.50,
									items: [{
										xtype: 'compositefield',
										items: [{
												xtype: 'datefield', fieldLabel:'Periode', id: 'tglawal',
												width: 100, value: new Date(),
												format: 'd-m-Y',
												listeners:{
													select: function(field, newValue){
													
													},
													change : function(field, newValue){
													
													}
												}
											},{
												xtype: 'label', id: 'lb.sd', text: 's/d',
											},{
												xtype: 'datefield', id: 'tglakhir',
												width: 100, value: new Date(),
												format: 'd-m-Y',
												listeners:{
													select: function(field, newValue){
													
													},
													change : function(field, newValue){
													
													}
												}
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'textfield', fieldLabel: 'Jumlah Lama Dirawat',
												id: 'jld', width: 70, allowBlank: false,
												readOnly: true, style:'opacity: 0.6;text-align: right',
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Hari'
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'textfield', fieldLabel: 'Jumlah Pasien Keluar',
												id: 'jpk', width: 70, allowBlank: false,
												readOnly: true, style:'opacity: 0.6;text-align: right',
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Orang'
											},{
												xtype: 'textfield', fieldLabel: '',
												id: 'hidup', width: 70, allowBlank: false,
												readOnly: true, style:'opacity: 0.6;text-align: right',
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Hidup'
											},{
												xtype: 'textfield', fieldLabel: '',
												id: 'mati', width: 70, allowBlank: false,
												readOnly: true, style:'opacity: 0.6;text-align: right',
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Mati'
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'textfield', fieldLabel: 'Hasil', allowBlank: false,
												id: 'hasil', width: 70, readOnly: true, style:'opacity: 0.6;text-align: right',
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Hari'
											}]
										}]
								},{
									layout: 'form', columnWidth: 0.50,
									items: [{
											xtype: 'textarea', fieldLabel: 'Catatan',
											id: 'catatan', width: 327, height: 100
										},{
											xtype: 'compositefield', fieldLabel:'Tanggal Proses / User Input', 
											items: [{
												xtype: 'datefield', id: 'tglproses',
												width: 120, value: new Date(),
												format: 'd-m-Y', readOnly: true, style:'opacity: 0.6', 
												listeners:{
													select: function(field, newValue){
													
													},
													change : function(field, newValue){
													
													}
												}
											},{
												xtype: 'textfield', value: USERNAME,
												id: 'username', readOnly: true, style:'opacity: 0.6', width: 202
											},{
												xtype: 'textfield', value: USERID,
												id: 'userid', hidden: true, width: 100
											}]
										}]
								},{
											columnWidth: 1,xtype: 'panel',
											buttons: [{
												text: 'Reset',
												id: 'btnreset',
												width: 105, iconCls: 'silk-arrow-refresh',
												handler: function() {
													alos_form.getForm().reset();
													Ext.getCmp('btnproses').enable();
													Ext.getCmp('btnsimpan').disable();
												}
											},{
												text: 'Proses',
												id: 'btnproses',
												width: 105, iconCls: 'silk-selesai',
												handler: function() {
													get_result();
												}
											},{
												text: 'Simpan',
												id: 'btnsimpan',
												iconCls: 'silk-save',
												width: 105,
												handler: function() {
													fnSaveALOS();
												}
											}]
								}]
							}]
					}]
				},{
					xtype: 'fieldset',
					title: 'Riwayat ALOS',
					layout: 'form',
					items: [grid_nya]
				}
			],
			listeners: {
				afterrender: function () {
					Ext.getCmp('btnsimpan').disable();
					Ext.Ajax.request({
						url: BASE_URL + 'laporan_alos_controller/get_date_server',
						method: 'POST',
						params: {
						
						},
						success: function(response){
							obj = Ext.util.JSON.decode(response.responseText);
							Ext.getCmp('tglproses').setValue(obj.date);
						},
						failure : function(){
							Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
						}
					});
				}
			}
		}); SET_PAGE_CONTENT(alos_form);
		
	function get_result(){
		var tglawal = Ext.getCmp('tglawal').getValue();
		var tglakhir = Ext.getCmp('tglakhir').getValue();
		var interval = (tglawal.format('Y-m-d')==tglakhir.format('Y-m-d')) ? 1:Math.ceil((tglakhir.getTime()-tglawal.getTime())/(1000*60*60*24));

		var waitmsgproccess = Ext.MessageBox.wait('Memproses Data...', 'Informasi');
		Ext.Ajax.request({
			url: BASE_URL + 'laporan_alos_controller/get_items',
			method: 'POST',
			params: {
				tglawal:tglawal,
				tglakhir:tglakhir
			},
			success: function(response){
				waitmsgproccess.hide();
				obj = Ext.util.JSON.decode(response.responseText);

				if (interval < 0) {
					Ext.MessageBox.alert('Informasi', 'Rentang Tanggal Tidak Valid');
				} else {
					Ext.getCmp('jld').setValue(obj.jmlrawat);
					Ext.getCmp('jpk').setValue(obj.jmlpasienkeluar);
					Ext.getCmp('hidup').setValue(obj.jmlpasienkeluarhidup);
					Ext.getCmp('mati').setValue(obj.jmlpasienkeluarmati);
					Ext.getCmp('hasil').setValue(obj.result);
					
					Ext.getCmp('btnsimpan').enable();
				}				
			},
			failure : function(){
				waitmsgproccess.hide();
				Ext.MessageBox.alert('Informasi', 'Gagal Memproses Data');
			}
		});
	}
	
	function fnSaveALOS(){
			var form_nya = Ext.getCmp('alos_form');
			
			if(form_nya.getForm().isValid()){
				form_nya.getForm().submit({
					url: BASE_URL +'laporan_alos_controller/simpan_hasil',
					method: 'POST',
					waitMsg: 'Menyimpan Data...',
					params: {
					
					},
					success: function(form_nya, o) {
						Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
						alos_form.getForm().reset();
						Ext.getCmp('btnproses').enable();
						Ext.getCmp('btnsimpan').disable();
						ds_nya.reload();
					},
					failure: function() {
						Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
					}
				});
			} else {
				Ext.MessageBox.alert("Informasi", "Silahkan Proses Terlebih Dahulu");
			}
	}
	
}