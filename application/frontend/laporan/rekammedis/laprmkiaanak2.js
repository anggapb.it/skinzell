function laprmkiaanak2(){	
	
/* Data Store */

	var ds_rmkiaanak2 = dm_rmkiaanak2();
	ds_rmkiaanak2.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_rmkiaanak2.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
	
/* End Data Store */

	var search = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_rmkiaanak2', //sm: cbGrid, 
		store: ds_rmkiaanak2,
		plugins: search,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				//cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},{
			text: 'Cetak Excel',
			id: 'cetakexcel',
			iconCls: 'silk-printer',
			handler: function() {
				cetakexcel();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},'->'],
		columns: [new Ext.grid.RowNumberer(),{
			header: '<center>Masuk</center>',
			dataIndex: 'tglmasuk',
			align: 'center', 
			sortable: true, width: 80,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: '<center>Pulang</center>',
			dataIndex: 'tglkuitansi',
			align: 'center', 
			sortable: true, width: 80,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		}
		,{
			header: '<center>No. Registrasi</center>',
			dataIndex: 'noreganak',
			align: 'center', 
			sortable: true, width: 80,
		},
		{
			header: '<center>No. RM</center>',
			dataIndex: 'norm',
			//align: 'center', 
			sortable: true, width: 80
		},
		{
			header: '<center>Nama Pasien</center>',
			dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true, width: 150
		},{
			header: '<center>Cara Lahir</center>',
			dataIndex: 'nmcaralahir',
			align: 'center',  
			sortable: true, width: 100,
		},{
			header: '<center>KN1</center>',
			dataIndex: 'kn1',
			align: 'center',  
			sortable: true, width: 30,
		},{
			header: '<center>KN2</center>',
			dataIndex: 'kn2',
			align: 'center',  
			sortable: true, width: 30,
		},{
			header: '<center>KN3</center>',
			dataIndex: 'kn3',
			align: 'center',  
			sortable: true, width: 30,
		},{
			header: '<center>Jenis Kelamin</center>',
			dataIndex: 'jkelamin',
			align: 'center',  
			sortable: true, width: 74
		},{
			header: '<center>ICDX</center>',
			dataIndex: 'nmpenyakiteng',
			//align: 'center', 
			sortable: true, width: 266
		},
		{
			header: '<center>Keadaan Keluar</center>',
			dataIndex: 'nmstkeluar',
			//align: 'center', 
			sortable: true, width: 93
		},
		{
			header: '<center>Penyebab Kematian</center>',
			dataIndex: 'nmkematian',
			//align: 'center', 
			sortable: true, width: 106
		},
		{
			header: '<center>Kota</center>',
			dataIndex: 'kot',
			//align: 'center', 
			sortable: true, width: 93
		},
		{
			header: '<center>Unit Pengirim</center>',
			dataIndex: 'nmcaradatang',
			//align: 'center', 
			sortable: true, width: 120
		},
		{
			header: '<center>Penjamin</center>',
			dataIndex: 'nmpenjamin',
			//align: 'center', 
			sortable: true, width: 150
		}],
		
		//bbar: paging
	});
	
	/* END GRID */
	/* Daftar PO */
		var form_nya = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'KIA Anak 2',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				//title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal(Periode) :',
								//width: 100,
								margins: {top:3, right:3, bottom:0, left:10}
						},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}/* ,
								change : function(field, newValue){
									cAdvance();
								} */
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d', margins: '3 3 0 0'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}/* ,
								change : function(field, newValue){
									cAdvance();
								} */
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				//title: '',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(form_nya);
	/* End Form */
		
	function cAdvance(){							
		ds_rmkiaanak2.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
		ds_rmkiaanak2.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));	
		ds_rmkiaanak2.reload();
	}
	
	function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir		= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		RH.ShowReport(BASE_URL + 'print/Lap_rmkunjunganri/get_lap_rmkunjunganri/'
                +tglawal+'/'+tglakhir);
	}
	
	function cetakexcel(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		window.location =(BASE_URL + 'print/Lap_rmkiaanak2/laporan_excelrmkiaanak2/'
                +tglawal+'/'+tglakhir);
	}

}