function pasien_menurutkeadaankeluar(){
	
	
/* Data Store */

	var ds_lappasienkeluar = dm_lappasienkeluar();
	ds_lappasienkeluar.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_lappasienkeluar.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
/* End Data Store */

/* GRID */
	var pageSize = 50;
	var paging = new Ext.PagingToolbar({
	pageSize: pageSize,
		store: ds_lappasienkeluar,
		displayInfo: true,
		displayMsg: 'Data Pasien Keluar Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_lappasienkeluar,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
			tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [{
			header: 'NO. RM',dataIndex: 'norm',
			//align: 'center', 
			sortable: true, width: 80
		},
		{
			header: 'NO. Reg',dataIndex: 'noreg',
			//align: 'center', 
			sortable: true, width: 80
		}
		,{
			header: 'Nama Pasien',dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true, width: 130
		},
		{
			header: 'Tgl<br>Masuk', dataIndex: 'tglmasuk',
			align: 'center',
			sortable: true, width: 70
		},{
			header: 'Jam<br>Masuk',	
			align: 'center', 
			dataIndex: 'jammasuk', sortable: true, width: 70
		},{
			header: 'Tgl<br>Keluar', dataIndex: 'tglkeluar',
			align: 'center', 
			sortable: true,width: 70
		},{
			header: 'Jam<br> Keluar', dataIndex: 'jamkeluar',
			align: 'center', 
			sortable: true, width: 70
		},{
			header: 'Ruangan', dataIndex: 'nmbagian',
			sortable: true, width: 80
		},{
			header: 'Kelas', dataIndex:'nmklsrawat',
			sortable: true, width: 80
		},{
			header: 'Kamar', dataIndex: 'nmkamar',
			sortable: true, width: 80
		},{
			header: 'Dokter Rawat',dataIndex: 'nmdoktergelar',
			sortable: true, width: 150
		},{
			header: 'Penangung<br>Biaya', dataIndex: 'nmpenjamin',
            align: 'center', 
			sortable: true, width:110
		}],		
		bbar: paging
	});
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Pasien Menurut Keadaan Keluar',
			autoScroll: true,
		
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
						//	style: 'padding: 10px',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'Pasien Menurut Keadaan Keluar',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
function cAdvance(){
	ds_lappasienkeluar.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
	ds_lappasienkeluar.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
	ds_lappasienkeluar.reload();
}
function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir		= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		RH.ShowReport(BASE_URL + 'print/Lap_rekamedis_pasienmenurutkeadaan/pasienkeluar/'
                +tglawal+'/'+tglakhir);
	}	

	
}