function rmperpasien() {
	
	var ds_pasien = dm_pasien();
	var ds_goldarah = dm_goldarah();
	var fields_rm_pasien = RH.storeFields('tglkodifikasirender','jamkodifikasirender','kdpenyakit','nmpenyakit','nmpenyakiteng',
								'nmjstpenyakit','nmjnsdiagnosa','nmstkeluar','nmcarakeluar','nmtindaklanjut','nmkematian',
								'tglmeninggalrender','jammeninggalrender');
	var ds_rm_pasien = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_rmperpasien_controller/get_rmperpasien', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields: fields_rm_pasien
	});

	var cm_rm_pasien = new Ext.grid.ColumnModel({
		columns: [
			new Ext.grid.RowNumberer(),
		{
			header: '<center>Tanggal</center>',dataIndex: 'tglkodifikasirender', 
			sortable: true, width: 80, align: 'center',
		},{
			header: '<center>Jam</center>',dataIndex: 'jamkodifikasirender', 
			sortable: true, width: 80, align: 'center',
		},{
			header: '<center>Kode Penyakit<br>(ICD-X)</center>',dataIndex: 'kdpenyakit', 
			sortable: true, width: 100, align: 'center',
		},{
			header: '<center>Nama Penyakit<br>(Bhs. Indonesia)</center>',dataIndex: 'nmpenyakit', 
			sortable: true, width: 300, align: 'left',
		},{
			header: '<center>Nama Penyakit<br>(Bhs. Inggris)</center>',dataIndex: 'nmpenyakiteng', 
			sortable: true, width: 300, align: 'left',
		},{
			header: '<center>( Baru / Lama )</center>',dataIndex: 'nmjstpenyakit', 
			sortable: true, width: 100, align: 'center',
		},{
			header: '<center>Jenis Diagnosa</center>',dataIndex: 'nmjnsdiagnosa', 
			sortable: true, width: 100, align: 'center',
		},{
			header: '<center>Keadaan Keluar</center>',dataIndex: 'nmstkeluar', 
			sortable: true, width: 200, align: 'center',
		},{
			header: '<center>Cara Keluar</center>',dataIndex: 'nmcarakeluar', 
			sortable: true, width: 200, align: 'center',
		},{
			header: '<center>Tindak Lanjut</center>',dataIndex: 'nmtindaklanjut', 
			sortable: true, width: 200, align: 'center',
		},{
			header: '<center>Penyebab Kematian</center>',dataIndex: 'nmkematian', 
			sortable: true, width: 200, align: 'center',
		},{
			header: '<center>Tanggal<br>Meninggal</center>',dataIndex: 'tglmeninggalrender', 
			sortable: true, width: 80, align: 'center',
		},{
			header: '<center>Jam<br>Meninggal</center>',dataIndex: 'jammeninggalrender', 
			sortable: true, width: 80, align: 'center',
		}]
	});
	
	var grid_rm_pasien = new Ext.grid.GridPanel({
		id: 'grid_rm_pasien',
		height: 400,
		ds: ds_rm_pasien,
		cm: cm_rm_pasien,
		sm: new Ext.grid.RowSelectionModel({ singleSelect: true}),
		tbar: [{
			text: 'Cetak',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				RH.ShowReport(BASE_URL + 'print/lap_rekamedis_perpasien/get_rmperpasien/'+Ext.getCmp('tf.norm').getValue());
			}
		}],	
		clicksToEdit: 1,	//for cell editing (single click =1, dblclick=2)
        forceFit: true, //autoHeight: true, 
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,		
		loadMask: true,
		layout: 'anchor',		
		bbar: [],
		
	});
	
	var main_form = new Ext.form.FormPanel({
		id: 'formRMperNorm', region: 'center',
		border: false, frame: true, bodyStyle: 'padding: 5px',
		title: 'Laporan Berkas Rekam Medis per Pasien', autoScroll: true,
		items: [{
			xtype: 'fieldset',title: 'Cari Pasien', layout: 'column', 
			defaults: {labelWidth: 100, labelAlign: 'right'},
			items: [{
				layout: 'form', columnWidth: .50,
				items: [{
						xtype: 'compositefield', id: 'comp1', fieldLabel: 'No. RM',
							items: [{
								xtype: 'textfield', id: 'tf.norm',name: 'tf.norm', width: 100,
								enableKeyEvents: true, readOnly: true, style : 'opacity:0.6',
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cariPasien(this.getValue())
										}
									}
								}
							},{
								xtype: 'button',text: ' ... ',
								id: 'btn.norm', width: 30,
								handler: function(){
									dftPasien();
								}
							}]
						},{
							xtype: 'textfield', id: 'tf.nmpasien', fieldLabel: 'Nama Pasien', width: 350, readOnly: true, style : 'opacity:0.6'
						},{
							xtype: 'textfield', id: 'tf.nmjnskelamin', fieldLabel: 'Jenis Kelamin', width: 100, readOnly: true, style : 'opacity:0.6'
						},{
							xtype: 'textfield', id: 'tf.goldar', fieldLabel: 'Golongan Darah', width: 100, readOnly: true, style : 'opacity:0.6'
						}]
					},
					{
				layout: 'form', columnWidth: .50,
				items: [{
							xtype: 'textfield', id: 'tf.tptlahir', fieldLabel: 'Tempat Lahir', width: 200, readOnly: true, style : 'opacity:0.6'
						},{ 	
							xtype: 'datefield', id: 'df.tgllahir',
							width: 100, value: null, fieldLabel: 'Tanggal Lahir',
							format: 'd/m/Y',readOnly: true,
							style : 'opacity:0.6'
						},{
							xtype: 'textfield', id: 'tf.alamat', fieldLabel: 'Alamat', width: 350, readOnly: true, style : 'opacity:0.6'
						},{
							xtype: 'textfield', id: 'tf.notelp', fieldLabel: 'No. Telp', width: 100, readOnly: true, style : 'opacity:0.6'
						}]
				}]
		},{
			xtype: 'fieldset', title: 'Daftar Penyakit',
			items: [{
				layout: 'form', border: false,
				items: [grid_rm_pasien]
			}]			
		}]
	});	
	
	SET_PAGE_CONTENT(main_form);
	//====================================================================================================
	function cariPasien(norm){
		var nmpasien = RH.getRecordFieldValue(ds_pasien, 'nmpasien', 'norm', norm);
		var nmjnskelamin = RH.getRecordFieldValue(ds_pasien, 'nmjnskelamin', 'norm', norm);
		Ext.getCmp("tf.nmpasien").setValue(nmpasien);
		Ext.getCmp("tf.nmjnskelamin").setValue(nmjnskelamin);

		ds_rm_pasien.setBaseParam('norm',norm);
		ds_rm_pasien.load();
	}
	
	function dftPasien(){
		var ds_pasien = dm_pasien();
		
		function keyToView_pasien(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterPasien" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_cari_pasien = new Ext.grid.ColumnModel([
			{
				header: 'No RM',
				dataIndex: 'norm',
				width: 80,
				renderer: keyToView_pasien
			},{
				header: 'No. RM Lama',
				dataIndex: 'normlama',
				width: 80
			},{
				header: 'Nama Pasien',
				dataIndex: 'nmpasien',
				width: 150
			},{
				header: 'L/P',
				dataIndex: 'kdjnskelamin',
				width: 30
			},{
				header: 'Tgl. Lahir',
				dataIndex: 'tgllahir',
				renderer: Ext.util.Format.dateRenderer('d-m-Y'),
				width: 80
			},{
				header: 'Alamat Pasien',
				dataIndex: 'alamat',
				width: 210
			},{
				header: 'Nama Ibu',
				dataIndex: 'nmibu',
				width: 120
			},{
				header: 'No. HP/Telp.',
				dataIndex: 'notelp',
				renderer: function(value, p, r){
					var nohptelp = r.data['nohp'] + ' / ' + r.data['notelp'];
					
					return nohptelp ;
				},
				width: 100
			},{
				header: 'No. KTP/Paspor',
				dataIndex: 'noidentitas',
				width: 100
			}
		]);
		var sm_cari_pasien = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_pasien = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_pasien = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_pasien,
			displayInfo: true,
			displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var grid_find_cari_pasien = new Ext.grid.GridPanel({
			ds: ds_pasien,
			cm: cm_cari_pasien,
			sm: sm_cari_pasien,
			view: vw_cari_pasien,
			height: 350,
			width: 975,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			bbar: paging_cari_pasien,
			listeners: {
				cellclick: klik_cari_pasien
			}
		});
		var win_find_cari_pasien = new Ext.Window({
			title: 'Cari Pasien',
			modal: true,
			items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'button',
						text: 'Cari',
						id: 'btn.cari',
						iconCls:'silk-find',
						style: 'padding: 10px',
						width: 100,
						handler: function() {
							cAdvance();
						}
					},{
						xtype: 'button',
						text: 'Kembali',
						id: 'btn.kembali',
						iconCls:'silk-arrow-undo',
						style: 'padding: 10px',
						width: 110,
						handler: function() {
							win_find_cari_pasien.close();
						}
					}]
				},{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnorm',
							items:[{
								xtype: 'checkbox',
								id:'chb.crm',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.crmlama').enable();
											Ext.getCmp('tf.crm').enable();
											Ext.getCmp('tf.crm').focus();
										} else if(val == false){
											Ext.getCmp('tf.crmlama').disable();
											Ext.getCmp('tf.crm').disable();
											Ext.getCmp('tf.crm').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.crm',
								emptyText:'No. RM',
								width: 105, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							},{
								xtype: 'label', id: 'lb.crml', text: ' / '
							},{
								xtype: 'textfield',
								id: 'tf.crmlama',
								emptyText:'No. RM Lama',
								width: 110, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_cnmpasien',
							items:[{
								xtype: 'checkbox',
								id:'chb.cnmpasien',
								checked:true,
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.cnmpasien').enable();
											Ext.getCmp('tf.cnmpasien').focus();
										} else if(val == false){
											Ext.getCmp('tf.cnmpasien').disable();
											Ext.getCmp('tf.cnmpasien').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.cnmpasien',
								emptyText:'Nama Pasien',
								width: 230,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						}]
					},{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_ctgllhr',
							items:[{
								xtype: 'checkbox',
								id:'chb.ctgllhr',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('df.ctgllhr').enable();
											Ext.getCmp('df.ctgllhr').focus();
										} else if(val == false){
											Ext.getCmp('df.ctgllhr').disable();
											Ext.getCmp('df.ctgllhr').setValue(new Date());
										}
									}
								}
							},{
								xtype: 'label', id: 'lb.ctgllhr', text: 'Tgl. Lahir', margins: '5 5 0 0',
							},{
								xtype: 'datefield',
								id: 'df.ctgllhr',
								width: 100, value: new Date(),
								format: 'd-m-Y',
								disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_calamatp',
							items:[{
								xtype: 'checkbox',
								id:'chb.calamatp',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.calamatp').enable();
											Ext.getCmp('tf.calamatp').focus();
										} else if(val == false){
											Ext.getCmp('tf.calamatp').disable();
											Ext.getCmp('tf.calamatp').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.calamatp',
								emptyText:'Alamat',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						}]
					},{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnmibu',
							items:[{
								xtype: 'checkbox',
								id:'chb.cnmibu',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.cnmibu').enable();
											Ext.getCmp('tf.cnmibu').focus();
										} else if(val == false){
											Ext.getCmp('tf.cnmibu').disable();
											Ext.getCmp('tf.cnmibu').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.cnmibu',
								emptyText:'Nama Pasangan / Orang Tua',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_ctelp',
							items:[{
								xtype: 'checkbox',
								id:'chb.ctelp',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.ctelp').enable();
											Ext.getCmp('tf.ctelp').focus();
										} else if(val == false){
											Ext.getCmp('tf.ctelp').disable();
											Ext.getCmp('tf.ctelp').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.ctelp',
								emptyText:'No. HP/Telp.',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						}]
					}]
				},
				grid_find_cari_pasien
			]
		}).show();
		Ext.getCmp('tf.cnmpasien').focus(false,200);

		function klik_cari_pasien(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterPasien'){
				var rec_cari_pasien = ds_pasien.getAt(rowIdx);
				var var_cari_pasienno = rec_cari_pasien.data["norm"];
				var var_cari_pasiennolama = rec_cari_pasien.data["normlama"];
				var var_cari_pasiennm = rec_cari_pasien.data["nmpasien"];
				var var_cari_pasienidjkel = rec_cari_pasien.data["idjnskelamin"];
				var var_cari_pasienjkel = rec_cari_pasien.data["nmjnskelamin"];
				var var_cari_pasienstk = rec_cari_pasien.data["idstkawin"];
				var var_cari_pasienalamat = rec_cari_pasien.data["alamat"];
				var var_cari_pasienwn = rec_cari_pasien.data["idwn"];
				var var_cari_pasiennegara = rec_cari_pasien.data["negara"];
				var var_cari_pasiendaerah = rec_cari_pasien.data["nmdaerah"];
				var var_cari_pasiennotelp = rec_cari_pasien.data["notelp"];
				var var_cari_pasiennohp = rec_cari_pasien.data["nohp"];
				var var_cari_pasientpl = rec_cari_pasien.data["tptlahir"];
				var var_cari_pasientgl = rec_cari_pasien.data["tgllahir"];
				var var_cari_pasienagama = rec_cari_pasien.data["idagama"];
				var var_cari_pasiengol = rec_cari_pasien.data["idgoldarah"];
				var var_cari_pasienpend = rec_cari_pasien.data["idpendidikan"];
				var var_cari_pasienpek = rec_cari_pasien.data["idpekerjaan"];
				var var_cari_pasiensuku = rec_cari_pasien.data["idsukubangsa"];
				var var_cari_pasiennoid = rec_cari_pasien.data["noidentitas"];
				var var_cari_pasienibu = rec_cari_pasien.data["nmibu"];
				var var_cari_pasienalergi = rec_cari_pasien.data["alergi"];
				var var_cari_pasiencttn = rec_cari_pasien.data["catatan"];
					
				Ext.getCmp('tf.norm').focus()
				Ext.getCmp("tf.norm").setValue(var_cari_pasienno);
				Ext.getCmp("tf.nmpasien").setValue(var_cari_pasiennm);
				Ext.getCmp("tf.nmjnskelamin").setValue(var_cari_pasienjkel);
				var nmgoldar =  RH.getRecordFieldValue(ds_goldarah, 'nmgoldarah', 'idgoldarah', var_cari_pasiengol);
				Ext.getCmp("tf.goldar").setValue(nmgoldar);
				
				Ext.getCmp("tf.tptlahir").setValue(var_cari_pasientpl);
				Ext.getCmp("df.tgllahir").setValue(var_cari_pasientgl);
				Ext.getCmp("tf.alamat").setValue(var_cari_pasienalamat);
				Ext.getCmp("tf.notelp").setValue(var_cari_pasiennotelp);
				
				

				ds_rm_pasien.setBaseParam('norm',var_cari_pasienno);
				ds_rm_pasien.load();
				
				win_find_cari_pasien.close();
			}
		}
		
		function cAdvance(){
			if(Ext.getCmp('chb.ctgllhr').getValue() == true){
				ds_pasien.setBaseParam('tgllahir',Ext.util.Format.date(Ext.getCmp('df.ctgllhr').getValue(), 'Y-m-d'));
			} else {
				ds_pasien.setBaseParam('tgllahir','');
			}
		
			ds_pasien.setBaseParam('norm',Ext.getCmp('tf.crm').getValue());
			ds_pasien.setBaseParam('normlama',Ext.getCmp('tf.crmlama').getValue());
			ds_pasien.setBaseParam('nmpasien',Ext.getCmp('tf.cnmpasien').getValue());
			ds_pasien.setBaseParam('alamat',Ext.getCmp('tf.calamatp').getValue());
			ds_pasien.setBaseParam('nmibu',Ext.getCmp('tf.cnmibu').getValue());
			ds_pasien.setBaseParam('notelp',Ext.getCmp('tf.ctelp').getValue());
			ds_pasien.load();
		}
	}
}