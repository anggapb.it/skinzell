function laprmkiaibu2(){	
	
/* Data Store */

	var ds_rmkiaibu2 = dm_rmkiaibu2();
	ds_rmkiaibu2.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_rmkiaibu2.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
	
/* End Data Store */

	var search = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_rmkiaibu2', //sm: cbGrid, 
		store: ds_rmkiaibu2,
		plugins: search,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				//cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},{
			text: 'Cetak Excel',
			id: 'cetakexcel',
			iconCls: 'silk-printer',
			handler: function() {
				cetakexcel();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},'->'],
		columns: [new Ext.grid.RowNumberer(),{
			header: '<center>Masuk</center>',
			dataIndex: 'tglmasuk',
			align: 'center', 
			sortable: true, width: 80,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: '<center>Pulang</center>',
			dataIndex: 'tglkuitansi',
			align: 'center', 
			sortable: true, width: 80,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		}
		,{
			header: '<center>No. Registrasi</center>',
			dataIndex: 'noreg',
			align: 'center', 
			sortable: true, width: 80,
		},
		{
			header: '<center>No. RM</center>',
			dataIndex: 'norm',
			//align: 'center', 
			sortable: true, width: 80
		},
		/* {
			header: '<center>Status<br>Pasien</center>',
			dataIndex: 'stpasien',
			align: 'center', 
			sortable: true, width: 47
		}, */
		{
			header: '<center>Nama Pasien</center>',
			dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true, width: 150
		},
		{
			header: '<center>Cara Lahir<br>Normal</center>',
			dataIndex: 'clahirn',
			align: 'center', 
			sortable: true, width: 67,	
			renderer: function(value, p, r){
				var info = '';
					if(r.data['clahirn'] != null) info = '1';
					if(r.data['clahirn'] == null) info = '';
				return info ;
			}
		},
		{
			header: '<center>Cara Lahir<br>SC</center>',
			dataIndex: 'clahirsc',
			align: 'center', 
			sortable: true, width: 67,	
			renderer: function(value, p, r){
				var info = '';
					if(r.data['clahirsc'] != null) info = '1';
					if(r.data['clahirsc'] == null) info = '';
				return info ;
			}
		},
		{
			header: '<center>Cara Lahir<br>Lain-lain</center>',
			dataIndex: 'clahirll',
			align: 'center', 
			sortable: true, width: 67,	
			renderer: function(value, p, r){
				var info = '';
					if(r.data['clahirll'] != null) info = '1';
					if(r.data['clahirll'] == null) info = '';
				return info ;
			}
		},
		{
			header: '<center>Persalinan Nakes</center>',
			dataIndex: 'nakes',
			//align: 'center', 
			sortable: true, width: 150
		},
		{
			header: '<center>KF1</center>',
			dataIndex: 'kf1',
			align: 'center', 
			sortable: true, width: 30,	
			renderer: function(value, p, r){
				var info = '';
					if(r.data['kf1'] != null) info = '1';
					if(r.data['kf1'] == null) info = '';
				return info ;
			}
		},{
			header: '<center>KF2</center>',
			dataIndex: 'kf2',
			align: 'center',  
			sortable: true, width: 30,	
			renderer: function(value, p, r){
				var info = '';
					if(r.data['kf2'] != null) info = '1';
					if(r.data['kf2'] == null) info = '';
				return info ;
			}
		},{
			header: '<center>KF3</center>',
			dataIndex: 'kf3',
			align: 'center',  
			sortable: true, width: 30,	
			renderer: function(value, p, r){
				var info = '';
					if(r.data['kf3'] != null) info = '1';
					if(r.data['kf3'] == null) info = '';
				return info ;
			}
		},
		{
			header: '<center>ICDX</center>',
			dataIndex: 'nmpenyakiteng',
			//align: 'center', 
			sortable: true, width: 266
		},{
			header: '<center>Kondisi Lahir</center>',
			dataIndex: 'konhir',
			align: 'center',  
			sortable: true, width: 85
		},
		{
			header: '<center>Jenis Kelamin</center>',
			dataIndex: 'jkelamin',
			align: 'center',  
			sortable: true, width: 74
		},
		{
			header: '<center>Keadaan Keluar</center>',
			dataIndex: 'nmstkeluar',
			//align: 'center', 
			sortable: true, width: 93
		},
		{
			header: '<center>Penyebab Kematian</center>',
			dataIndex: 'nmkematian',
			//align: 'center', 
			sortable: true, width: 106
		},
		{
			header: '<center>Kota</center>',
			dataIndex: 'kot',
			//align: 'center', 
			sortable: true, width: 93
		},
		{
			header: '<center>Unit Pengirim</center>',
			dataIndex: 'nmcaradatang',
			//align: 'center', 
			sortable: true, width: 120
		},
		{
			header: '<center>Penjamin</center>',
			dataIndex: 'nmpenjamin',
			//align: 'center', 
			sortable: true, width: 150
		}],
		
		//bbar: paging
	});
	
	/* END GRID */
	/* Daftar PO */
		var form_nya = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'KIA Ibu 2',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				//title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal(Periode) :',
								//width: 100,
								margins: {top:3, right:3, bottom:0, left:10}
						},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}/* ,
								change : function(field, newValue){
									cAdvance();
								} */
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d', margins: '3 3 0 0'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}/* ,
								change : function(field, newValue){
									cAdvance();
								} */
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				//title: '',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(form_nya);
	/* End Form */
		
	function cAdvance(){							
		ds_rmkiaibu2.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
		ds_rmkiaibu2.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));	
		ds_rmkiaibu2.reload();
	}
	
	function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir		= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		RH.ShowReport(BASE_URL + 'print/Lap_rmkunjunganri/get_lap_rmkunjunganri/'
                +tglawal+'/'+tglakhir);
	}
	
	function cetakexcel(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		window.location =(BASE_URL + 'print/Lap_rmkiaibu2/laporan_excelrmkiaibu2/'
                +tglawal+'/'+tglakhir);
	}

}