function laprmkunjunganri(){
	
	
/* Data Store */

	var ds_laprmkunjunganri = dm_laprmkunjunganri();
	ds_laprmkunjunganri.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_laprmkunjunganri.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
/* End Data Store */

	var search = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_rmrj', //sm: cbGrid, 
		store: ds_laprmkunjunganri,
		plugins: search,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},{
			text: 'Cetak Excel',
			id: 'cetakexcel',
			iconCls: 'silk-printer',
			handler: function() {
				cetakexcel();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},'->'],
		columns: [new Ext.grid.RowNumberer(),{
			header: '<center>Masuk</center>',
			dataIndex: 'tglmasuk',
			align: 'center', 
			sortable: true, width: 80
		},
		{
			header: '<center>Pulang</center>',
			dataIndex: 'tglkuitansi',
			align: 'center', 
			sortable: true, width: 80
		}
		,{
			header: '<center>No. Registrasi</center>',
			dataIndex: 'noreg',
			align: 'center', 
			sortable: true, width: 80,
		},
		{
			header: '<center>No. RM</center>',
			dataIndex: 'norm',
			//align: 'center', 
			sortable: true, width: 80
		},
		{
			header: '<center>Status<br>Pasien</center>',
			dataIndex: 'stpasien',
			align: 'center', 
			sortable: true, width: 47
		},
		{
			header: '<center>Nama Pasien</center>',
			dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true, width: 150
		},{
			header: '<center>Golongan Sebab Penyakit</center>',
			dataIndex: 'nmpenyakiteng',
		//	align: 'center', 
			sortable: true, width: 150
		},
		{
			header: '<center>Usia</center>',
			dataIndex: 'umurtahun',
			align: 'right', 
			sortable: true, width: 38
		},{
			header: '<center>Jenis<br>Kelamin</center>',
			dataIndex: 'nmjnskelamin',
			align: 'center', 
			sortable: true, width: 70
		},{
			header: '<center>Alamat</center>',
			dataIndex: 'alamat',
		//	align: 'center', 
			sortable: true, width: 190
		},{
			header: '<center>Kec</center>',
			dataIndex: 'kec',
		//	align: 'center', 
			sortable: true, width: 85
		},{
			header: '<center>Kota</center>',
			dataIndex: 'kot',
		//	align: 'center', 
			sortable: true, width: 93
		},{
			header: '<center>No. Telepon</center>',
			dataIndex: 'notelp',
		//	align: 'center', 
			sortable: true, width: 85
		},{
			header: '<center>Lama Hari<br>di rawat</center>',
			dataIndex: 'lamarawat',
			align: 'right', 
			sortable: true, width: 59,			
			renderer: function(value, p, r){
				var info = '';
					if(r.data['lamarawat']) info = r.data['lamarawat']+' Hari';
				return info ;
			}
		},{
			header: '<center>Ruangan</center>',
			dataIndex: 'nmbagian',
		//	align: 'center', 
			sortable: true, width: 80
		},{
			header: '<center>Kelas<br>di rawat</center>',
			dataIndex: 'nmklstarif',
		//	align: 'center', 
			sortable: true, width: 80
		},{
			header: '<center>Nama Dokter</center>',
			dataIndex: 'nmdoktergelar',
		//	align: 'center', 
			sortable: true, width: 150
		},{
			header: '<center>Jenis Kasus</center>',
			dataIndex: 'nmjnskasus',
		//	align: 'center', 
			sortable: true, width: 80
		},{
			header: '<center>Rujukan</center>',
			dataIndex: 'nmcaradatang',
		//	align: 'center', 
			sortable: true, width: 80
		},{
			header: '<center>Keadaan<br>Keluar</center>',
			dataIndex: 'nmstkeluar',
		//	align: 'center', 
			sortable: true, width: 80
		},{
			header: '<center>Cara Keluar</center>',
			dataIndex: 'nmcarakeluar',
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: '<center>Penjamin</center>',
			dataIndex: 'nmpenjamin',
		//	align: 'center', 
			sortable: true, width: 100
		}],
		
		//bbar: paging
	});
	
	/* END GRID */
	/* Daftar PO */
		var form_nya = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Kunjungan Rawat Inap',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				//title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal(Periode) :',
								//width: 100,
								margins: {top:3, right:3, bottom:0, left:10}
						},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}/* ,
								change : function(field, newValue){
									cAdvance();
								} */
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d', margins: '3 3 0 0'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}/* ,
								change : function(field, newValue){
									cAdvance();
								} */
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				//title: '',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(form_nya);
	/* End Form */
	
	function cAdvance(){
		ds_laprmkunjunganri.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
		ds_laprmkunjunganri.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
		ds_laprmkunjunganri.reload();
	}
	
	function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir		= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		RH.ShowReport(BASE_URL + 'print/Lap_rmkunjunganri/get_lap_rmkunjunganri/'
                +tglawal+'/'+tglakhir);
	}
	
	function cetakexcel(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');		
		window.location =(BASE_URL + 'print/Lap_rmkunjunganri/laporan_excelrmkri/'
                +tglawal+'/'+tglakhir);
	}

}