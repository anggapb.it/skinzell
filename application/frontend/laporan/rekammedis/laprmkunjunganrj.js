function laprmkunjunganrj(){
	
	
/* Data Store */

	var ds_laprmkunjunganrj = dm_laprmkunjunganrj();
	ds_laprmkunjunganrj.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_laprmkunjunganrj.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
/* End Data Store */


    /* fields = [
          {type: 'int', name: 'BeijingProductX'},
          {type: 'int', name: 'BeijingProductY'},
          {type: 'int', name: 'TokyoProductX'},
          {type: 'int', name: 'TokyoProductY'},
          {type: 'int', name: 'BerlinProductX'},
          {type: 'int', name: 'BerlinProductY'},
          {type: 'int', name: 'LondonProductX'},
          {type: 'int', name: 'LondonProductY'},
          {type: 'int', name: 'ParisProductX'},
          {type: 'int', name: 'ParisProductY'}],
    data = [
		  ['BeijingProductX', '1'],
          ['BeijingProductY', '2'],
          ['TokyoProductX', '2'],
          ['TokyoProductY', '2'],
          ['BerlinProductX', '2'],
          ['BerlinProductY', '2'],
          ['LondonProductX', '2'],
          ['LondonProductY', '2'],
          ['ParisProductX', '2'],
          ['ParisProductY', '2']
		  ],
    columns = [{dataIndex: 'BeijingProductX', header: 'ProductX'},
			  {dataIndex: 'BeijingProductY', header: 'ProductY'},
			  {dataIndex: 'TokyoProductX', header: 'ProductX'},
			  {dataIndex: 'TokyoProductY', header: 'ProductY'},
			  {dataIndex: 'BerlinProductX', header: 'ProductX'},
			  {dataIndex: 'BerlinProductY', header: 'ProductY'},
			  {dataIndex: 'LondonProductX', header: 'ProductX'},
			  {dataIndex: 'LondonProductY', header: 'ProductY'},
			  {dataIndex: 'ParisProductX', header: 'ProductX'},
			  {dataIndex: 'ParisProductY', header: 'ProductY'}],
    continentGroupRow = [{header: 'Asia', colspan: 4, align: 'center'},
          {header: 'Europe', colspan: 6, align: 'center'}],
    cityGroupRow = [{header: 'Beijing', colspan: 2, align: 'center'},
          {header: 'Tokyo', colspan: 2, align: 'center'},
          {header: 'Berlin', colspan: 2, align: 'center'},
          {header: 'London', colspan: 2, align: 'center'},
          {header: 'Paris', colspan: 2, align: 'center'}];
    
    var group = new Ext.ux.grid.ColumnHeaderGroup({
        rows: [//continentGroupRow,
				cityGroupRow
			]
    });
	
    var grid_nya = new Ext.grid.GridPanel({
        //renderTo: 'column-group-grid',
        title: 'Sales By Location',
        height: 400,
        store: new Ext.data.ArrayStore({
            fields: fields,
            data: data
        }),
        columns: columns,
        viewConfig: {
            forceFit: true
        },
        plugins: group
    }); */
	
	/* 
    columns = [{
			header: 'Kode ICD-X',dataIndex: 'kdpenyakit',
			//align: 'center', 
			sortable: true, //width: 80
		},
		{
			header: 'Nama Penyakit',dataIndex: 'nmpenyakiteng',
			//align: 'center', 
			sortable: true,// width: 200
		}
		,{
			header: 'Jenis Kelamin',dataIndex: 'nmjnskelamin',
		//	align: 'center', 
			sortable: true,// width: 80
		},
		{
			header: 'Umur',dataIndex: 'umur',
		//	align: 'center', 
			sortable: true,// width: 80
		},
		{
			header: 'Jumlah <br> Pasien Keluar', dataIndex: 'pasienkeluar',
			align: 'center',
			sortable: true,// width: 100
		},{
            header: 'gridcolumn',
            text: 'String',
            columns: [
                {
                    header: 'numbercolumn',
                    dataIndex: 'umur',
                    text: 'Number'
                },
                {
                    header: 'datecolumn',
                    dataIndex: 'umur',
                    text: 'Date'
                }
            ]
        }],
    cityGroupRow = [{header: 'Beijing', align: 'center'},
          {header: 'Tokyo', colspan: 2, width: 10, align: 'center'}];
    
    var group = new Ext.ux.grid.ColumnHeaderGroup({
        rows: [//continentGroupRow,
				cityGroupRow
			]
    });
	
    var grid_nya = new Ext.grid.GridPanel({
        //renderTo: 'column-group-grid',
        title: 'Sales By Location',
        height: 400,
        store: ds_laprmkunjunganrj,
        columns: columns,
        viewConfig: {
            forceFit: true
        },
        plugins: group
    }); */
	
	/* var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_laprmkunjunganrj,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},{
			text: 'Cetak Excel',
			id: 'cetakexcel',
			iconCls: 'silk-printer',
			handler: function() {
				//cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [{
			header: 'Kode ICD-X',dataIndex: 'kdpenyakit',
			//align: 'center', 
			sortable: true, width: 80
		},
		{
			header: 'Nama Penyakit',dataIndex: 'nmpenyakiteng',
			//align: 'center', 
			sortable: true,// width: 200
		}
		,{
			header: 'Jenis Kelamin',dataIndex: 'nmjnskelamin',
		//	align: 'center', 
			sortable: true,// width: 80
		},
		{
			header: 'Umur',dataIndex: 'umur',
		//	align: 'center', 
			sortable: true,// width: 80
		},
		{
			header: 'Jumlah <br> Pasien Keluar', dataIndex: 'pasienkeluar',
			align: 'center',
			sortable: true,// width: 100
		}],
		
		stripeRows : true,
		autoExpandColumn : 'desc',
		height : 350,
		viewConfig: {
			forceFit: true
		},
		plugins: [new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '',colspan: 2, align: 'center'},
					{header: 'No. RM', colspan: 1, align: 'center'}
				]
			],
			hierarchicalColMenu: true
		})]
	}); */	
	
	/* var search = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})]; */
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_krj', //sm: cbGrid, 
		store: ds_laprmkunjunganrj,
		//plugins: search,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},{
			text: 'Cetak Excel',
			id: 'cetakexcel',
			iconCls: 'silk-printer',
			handler: function() {
				cetakexcel();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [new Ext.grid.RowNumberer(),
		{
			header: '<center>Pulang</center>',
			dataIndex: 'tglkuitansi',
			align: 'center', 
			sortable: true, width: 80
		}
		,{
			header: '<center>No. Registrasi</center>',
			dataIndex: 'noreg',
			align: 'center', 
			sortable: true, width: 80
		},
		{
			header: '<center>No. RM</center>',
			dataIndex: 'norm',
			//align: 'center', 
			sortable: true, width: 80
		},{
			header: '<center>Status<br>Pasien</center>',
			dataIndex: 'stpasien',
			align: 'center', 
			sortable: true, width: 47
		},
		{
			header: '<center>Nama Pasien</center>',
			dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true, width: 150
		},{
			header: '<center>Golongan Sebab Penyakit</center>',
			dataIndex: 'nmpenyakiteng',
		//	align: 'center', 
			sortable: true, width: 150
		},
		{
			header: '<center>Usia</center>',
			dataIndex: 'umurtahun',
			align: 'right', 
			sortable: true, width: 35
		},{
			header: '<center>Jenis<br>Kelamin</center>',
			dataIndex: 'nmjnskelamin',
		//	align: 'center', 
			sortable: true, width: 70
		},{
			header: '<center>Alamat</center>',
			dataIndex: 'alamat',
		//	align: 'center', 
			sortable: true, width: 190
		},{
			header: '<center>Kec</center>',
			dataIndex: 'kec',
		//	align: 'center', 
			sortable: true, width: 85
		},{
			header: '<center>Kota</center>',
			dataIndex: 'kot',
		//	align: 'center', 
			sortable: true, width: 93
		},{
			header: '<center>No. Telepon</center>',
			dataIndex: 'notelp',
		//	align: 'center', 
			sortable: true, width: 85
		},{
			header: '<center>Nama Dokter</center>',
			dataIndex: 'nmdoktergelar',
		//	align: 'center', 
			sortable: true, width: 150
		},{
			header: '<center>Unit Pelayanan</center>',
			dataIndex: 'nmjnspelayanan',
		//	align: 'center', 
			sortable: true, width: 106
		},{
			header: '<center>Penjamin</center>',
			dataIndex: 'nmpenjamin',
		//	align: 'center', 
			sortable: true, width: 100
		}]
	});
	
	/* END GRID */
	/* Daftar PO */
		var form_nya = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Kunjungan Rawat Jalan',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				//title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal(Periode) :',
								//width: 100,
								margins: {top:3, right:3, bottom:0, left:10}
						},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								/* change : function(field, newValue){
									cAdvance();
								} */
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d', margins: '3 3 0 0'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								/* change : function(field, newValue){
									cAdvance();
								} */
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				//title: '',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(form_nya);
	/* End Form */
	
	function cAdvance(){
		ds_laprmkunjunganrj.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
		ds_laprmkunjunganrj.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
		ds_laprmkunjunganrj.reload();
	}
	
	function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		RH.ShowReport(BASE_URL + 'print/Lap_rmkunjunganrj/get_lap_rmkunjunganrj/'
                +tglawal+'/'+tglakhir);
	}

	function cetakexcel(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');		
		window.location =(BASE_URL + 'print/Lap_rmkunjunganrj/laporan_excelrmkrj/'
                +tglawal+'/'+tglakhir);
	}	

}