function bor(){

// ============= GRID RIWAYAT BOR
	var limitx = 10;
	var fields_nya = RH.storeFields('idbor','daritgl','sampaitgl','jmlperiode','jmltempattidur',
	'jmlhariperawatan','hasilpersen','catatan','tglbuat','userid','periode',
	'jmlperioderender','jmltempattidurrender','jmlhariperawatanrender','hasilpersenrender','tglbuatrender','nmlengkap');
		
	var nbsp1 = ''
	var nbsp2 = ''
	
	for (a=1;a<=12;a++) {
		nbsp1 = nbsp1 + '&nbsp;';
	}
					
	for (a=1;a<=12;a++) {
		nbsp2 = nbsp2 + '&nbsp;';
	}
					
	var ds_nya = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_bor_controller/get_history', 
				method: 'POST'
			}),
			baseParams: {
				start:0, limit:limitx,
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields : fields_nya,
	});
	
	var ds_klstarif = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
		url : BASE_URL + 'klstarif_controller/get_borklstarif',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: 'idklstarif',
			mapping: 'idklstarif'
		},{
			name: 'kdklstarif',
			mapping: 'kdklstarif'
		},
		{
			name: 'nmklstarif',
			mapping: 'nmklstarif'
		}]
	});
	
	var search_nya = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: limitx,
		store: ds_nya,
		displayInfo: true,
		displayMsg: 'Data Dari {0} - {1} of {2}',
		emptyMsg: 'Data Kosong'
	});

	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_nya', //sm: cbGrid, 
		store: ds_nya,
		plugins: search_nya,
		frame: true,
		height: 235,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		bbar: paging,
		columns: [new Ext.grid.RowNumberer({ header: '<center><b>No.</b></center>', width:35 }),
		{
			header: '<center><b>Periode</b></center>',dataIndex: 'periode',
			align: 'center',
			sortable: true, width: 150,
			renderer: function(value) {
				Ext.QuickTips.init();
				return '<div class="keyMasterDetail" ext:qtip="Lihat detail data" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
					+ value +'</div>';
			}
		},{
			header: '<center><b>Jumlah hari dalam<br>satu periode</b></center>',dataIndex: 'jmlperioderender',
			align: 'center',
			sortable: true, width: 125
		},{
			header: '<center><b>Jumlah<br>Tempat Tidur</b></center>',dataIndex: 'jmltempattidurrender',
			align: 'center',
			sortable: true, width: 130
		},{
			header: '<center><b>Jumlah hari<br>perawatan di RS</b></center>',dataIndex: 'jmlhariperawatanrender',
			align: 'center',
			sortable: true, width: 125
		},{
			header: '<center><b>Hasil</b></center>',dataIndex: 'hasilpersenrender',
			align: 'center',
			sortable: true, width: 70
		},{
			header: '<center><b>Catatan</b></center>',dataIndex: 'catatan',
			align: 'left',
			sortable: true, width: 200
		},{
			header: '<center><b>Tgl Proses</b></center>',dataIndex: 'tglbuatrender',
			align: 'center',
			sortable: true, width: 80
		},{
			header: '<center><b>User Input</b></center>',dataIndex: 'nmlengkap',
			align: 'left',
			sortable: true, width: 104
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: '<center><b>Hapus</b></center>',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						var record = ds_nya.getAt(rowIndex);
						var url = BASE_URL + 'laporan_bor_controller/delete_history';
						var params = new Object({
										idbor	: record.data['idbor']
									});
						RH.deleteGridRecord(url, params, grid );
                    }
                }]
        }],	
		listeners: {
			cellclick: function (grid, rowIndex, columnIndex, event) {
				var t = event.getTarget();
			
				if (t.className == 'keyMasterDetail'){					
					var record = grid.getStore().getAt(rowIndex);
					
					Ext.getCmp('tglawal').setValue(record.get("daritgl"));	
					Ext.getCmp('tglakhir').setValue(record.get("sampaitgl"));	
					Ext.getCmp('jhdsp').setValue(record.get("jmlperiode"));	
					Ext.getCmp('jtt').setValue(record.get("jmltempattidur"));	
					Ext.getCmp('jhpdrs').setValue(record.get("jmlhariperawatan"));	
					Ext.getCmp('hasil').setValue(record.get("hasilpersen"));	
					Ext.getCmp('catatan').setValue(record.get("catatan"));	
					Ext.getCmp('tglproses').setValue(record.get("tglbuat"));	
					Ext.getCmp('username').setValue(record.get("nmlengkap"));	
					Ext.getCmp('userid').setValue(record.get("userid"));	
					
					Ext.getCmp('btnproses').disable();
				}
			}
		}
	});
// ============= END GRID RIWAYAT BOR

		var bor_form = new Ext.form.FormPanel({
			id: 'bor_form',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'BOR (Bed Occupancy Rate = Angka penggunaan tempat tidur)',
			autoScroll: true,
			defaults: { labelWidth: 200, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				title: 'Perhitungan',
				layout: 'form',
				items: [{
						layout: 'form', bodyStyle: 'padding: 10px;',
						items: [{
							xtype: 'tbtext', style:'font-size:12px', id: 'info',
							text: 'BOR adalah prosentase pemakaian tempat tidur pada satuan waktu tertentu. ' +
							'Indikator ini memberikan gambaran tinggi rendahnya tingkat pemanfaatan tempat tidur rumah sakit. ' +
							'Nilai parameter BOR yang ideal adalah antara 60-85% (Depkes RI, 2005).<br><br>' +
							'Rumus : ' + nbsp1 + '<u>(Jumlah hari perawatan di RS) X 100%</u><br>' +
							nbsp2 + '(Jumlah Tempat Tidur X Jumlah hari dalam satu periode)'
							}]
					},{
						layout: 'form', bodyStyle: 'padding: 10px;',
						items: [{
								xtype: 'fieldset',
								frame: false,
								border: true,
								layout: 'column',
								items: [{
									layout: 'form', columnWidth: 0.50,
									items: [{
										xtype: 'compositefield',
										items: [{
												xtype: 'datefield', fieldLabel:'Periode', id: 'tglawal',
												width: 100, value: new Date(),
												format: 'd-m-Y',
												listeners:{
													select: function(field, newValue){
													
													},
													change : function(field, newValue){
													
													}
												}
											},{
												xtype: 'label', id: 'lb.sd', text: 's/d',
											},{
												xtype: 'datefield', id: 'tglakhir',
												width: 100, value: new Date(),
												format: 'd-m-Y',
												listeners:{
													select: function(field, newValue){
													
													},
													change : function(field, newValue){
													
													}
												}
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'combo', id: 'cb.idklstarif', fieldLabel: 'Kelas Tarif',
												store: ds_klstarif, triggerAction: 'all',
												valueField: 'idklstarif', displayField: 'nmklstarif',
												forceSelection: true, submitValue: true, 
												mode: 'local', emptyText:'Kelas Tarif...', width: 100,
												editable: false,// readOnly: true,
											}]
										},{
											xtype: 'compositefield',
											fieldLabel: 'Penjamin',
											items: [{
												xtype: 'textfield',
												id: 'tf.penjamin', //allowBlank: false,
												width: 230
											},{
												xtype: 'textfield',
												id: 'tf.idpenjamin', //allowBlank: false,
												width: 40, hidden: true
											},{
												xtype: 'button',
												text: ' ... ',
												id: 'btn.penjamin',
												width: 28,
												handler: function() {
													dftPenjamin();
												}
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'textfield', fieldLabel: 'Jumlah hari dalam satu periode',
												id: 'jhdsp', width: 100, allowBlank: false,
												readOnly: true, style:'opacity: 0.6;text-align: right', 
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Hari'
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'textfield', fieldLabel: 'Jumlah Tempat Tidur',
												id: 'jtt', width: 100, allowBlank: false,
												readOnly: true, style:'opacity: 0.6;text-align: right',
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Tempat Tidur'
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'textfield', fieldLabel: 'Jumlah hari perawatan di RS',
												id: 'jhpdrs', width: 100, allowBlank: false,
												readOnly: true, style:'opacity: 0.6;text-align: right',
											},{
												xtype: 'label', style:'font-size:12px',
												text: 'Hari'
											}]
										},{
											xtype: 'compositefield',
											items: [{
												xtype: 'textfield', fieldLabel: 'Hasil', allowBlank: false,
												id: 'hasil', width: 100, readOnly: true, style:'opacity: 0.6;text-align: right',
											},{
												xtype: 'label', style:'font-size:12px',
												text: '%'
											}]
										}]
								},{
									layout: 'form', columnWidth: 0.50,
									items: [{
											xtype: 'textarea', fieldLabel: 'Catatan',
											id: 'catatan', width: 310, height: 100
										},{
											xtype: 'compositefield', fieldLabel:'Tanggal Proses / User Input', 
											items: [{
												xtype: 'datefield', id: 'tglproses',
												width: 100, value: new Date(),
												format: 'd-m-Y', readOnly: true, style:'opacity: 0.6', 
												listeners:{
													select: function(field, newValue){
													
													},
													change : function(field, newValue){
													
													}
												}
											},{
												xtype: 'textfield', value: USERNAME,
												id: 'username', readOnly: true, style:'opacity: 0.6', width: 205
											},{
												xtype: 'textfield', value: USERID,
												id: 'userid', hidden: true, width: 100
											}]
										}]
								},{
											columnWidth: 1,xtype: 'panel',
											buttons: [{
												text: 'Reset',
												id: 'btnreset',
												width: 100, iconCls: 'silk-arrow-refresh',
												handler: function() {
													bor_form.getForm().reset();
													Ext.getCmp('btnproses').enable();
													Ext.getCmp('btnsimpan').disable();
												}
											},{
												text: 'Proses',
												id: 'btnproses',
												width: 100, iconCls: 'silk-selesai',
												handler: function() {
													get_result();
												}
											},{
												text: 'Simpan',
												id: 'btnsimpan',
												iconCls: 'silk-save',
												width: 100,
												handler: function() {
													fnSaveBOR();
												}
											}]
								}]
							}]
					}]
				},{
					xtype: 'fieldset',
					title: 'Riwayat BOR',
					layout: 'form',
					items: [grid_nya]
				}
			],
			listeners: {
				afterrender: function () {
					Ext.getCmp('btnsimpan').disable();
					Ext.Ajax.request({
						url: BASE_URL + 'laporan_bor_controller/get_date_server',
						method: 'POST',
						params: {
						
						},
						success: function(response){
							obj = Ext.util.JSON.decode(response.responseText);
							Ext.getCmp('tglproses').setValue(obj.date);
						},
						failure : function(){
							Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
						}
					});
				}
			}
		}); SET_PAGE_CONTENT(bor_form);
		
	function get_result(){
		var tglawal = Ext.getCmp('tglawal').getValue();
		var tglakhir = Ext.getCmp('tglakhir').getValue();
		var interval = (tglawal.format('Y-m-d')==tglakhir.format('Y-m-d')) ? 1:Math.ceil((tglakhir.getTime()-tglawal.getTime())/(1000*60*60*24)) + 1;

		var waitmsgproccess = Ext.MessageBox.wait('Memproses Data...', 'Informasi');
		Ext.Ajax.request({
			url: BASE_URL + 'laporan_bor_controller/get_items',
			method: 'POST',
			params: {
				tglawal		:tglawal,
				tglakhir	:tglakhir,
				interval	:interval,
				idklstarif	:Ext.getCmp('cb.idklstarif').getValue(),
				idpenjamin	:Ext.getCmp('tf.idpenjamin').getValue()
			},
			success: function(response){
				waitmsgproccess.hide();
				obj = Ext.util.JSON.decode(response.responseText);

				if (interval < 0) {
					Ext.MessageBox.alert('Informasi', 'Rentang Tanggal Tidak Valid');
				} else {
					Ext.getCmp('jhdsp').setValue(parseInt(interval));	
					if(obj.countbed != null){						
						Ext.getCmp('jtt').setValue(obj.countbed);
					}else{
						Ext.getCmp('jtt').setValue('0');
					}
					Ext.getCmp('jhpdrs').setValue(obj.jmlrawat);
					Ext.getCmp('hasil').setValue(obj.result);
					
					Ext.getCmp('btnsimpan').enable();
				}				
			},
			failure : function(){
				waitmsgproccess.hide();
				Ext.MessageBox.alert('Informasi', 'Gagal Memproses Data');
			}
		});
	}
	
	function fnSaveBOR(){
			var form_nya = Ext.getCmp('bor_form');
			
			if(form_nya.getForm().isValid()){
				form_nya.getForm().submit({
					url: BASE_URL +'laporan_bor_controller/simpan_hasil',
					method: 'POST',
					waitMsg: 'Menyimpan Data...',
					params: {
					
					},
					success: function(form_nya, o) {
						Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
						bor_form.getForm().reset();
						Ext.getCmp('btnproses').enable();
						Ext.getCmp('btnsimpan').disable();
						ds_nya.reload();
					},
					failure: function() {
						Ext.MessageBox.alert("Informasi", "Simpan Data Gagal");
					}
				});
			} else {
				Ext.MessageBox.alert("Informasi", "Silahkan Proses Terlebih Dahulu");
			}
	}
	
	function dftPenjamin(){
		var ds_penjamin = dm_penjamin();
		function keyToView_penjamin(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterPenjamin" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_penjamin = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idpenjamin'
			},{
				header: 'Nama Penjamin',
				dataIndex: 'nmpenjamin',
				width: 200,
				renderer: keyToView_penjamin
			},{
				header: 'Alamat',
				dataIndex: 'alamat',
				width: 300
			},{
				header: 'No. Telp',
				dataIndex: 'notelp',
				width: 150
			}
		]);
		var sm_cari_penjamin = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_penjamin = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_penjamin = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_penjamin,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_cari_penjamin = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_cari_penjamin= new Ext.grid.GridPanel({
			ds: ds_penjamin,
			cm: cm_cari_penjamin,
			sm: sm_cari_penjamin,
			view: vw_cari_penjamin,
			height: 460,
			width: 680,
			plugins: cari_cari_penjamin,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_penjamin,
			listeners: {
				cellclick: klik_cari_penjamin
			}
		});
		var win_find_cari_penjamin = new Ext.Window({
			title: 'Cari Penjamin',
			modal: true,
			items: [grid_find_cari_penjamin]
		}).show();

		function klik_cari_penjamin(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterPenjamin'){
				var rec_cari_penjamin = ds_penjamin.getAt(rowIdx);
				var var_cari_nmpenjamin = rec_cari_penjamin.data["nmpenjamin"];
				var var_cari_idpenjamin = rec_cari_penjamin.data["idpenjamin"];
				
				Ext.getCmp("tf.penjamin").setValue(var_cari_nmpenjamin);
				Ext.getCmp("tf.idpenjamin").setValue(var_cari_idpenjamin);
							win_find_cari_penjamin.close();
			}
		}
	}
	
}