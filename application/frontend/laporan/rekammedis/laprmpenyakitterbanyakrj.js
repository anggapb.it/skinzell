function laprmpenyakitterbanyakrj(){	
	
/* Data Store */

	var ds_rmpenyakitterbanyakrj = dm_rmpenyakitterbanyakrj();
	ds_rmpenyakitterbanyakrj.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_rmpenyakitterbanyakrj.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_rmpenyakitterbanyakrj.setBaseParam('vdkot',1);
	ds_rmpenyakitterbanyakrj.setBaseParam('vpenj',1);
	
	var ds_lrmpenjamin = dm_lrmpenjamin();
	
	var arr_cari = [['30754', 'KOTA BANDUNG'],['26005', 'KAB. BANDUNG'],['30457', 'KAB. BANDUNG BARAT'],['010', 'LUAR KOTA'],['', '-Pilih-']];
	
	var ds_cdaerah = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
/* End Data Store */

	var search = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_rmprj', //sm: cbGrid, 
		store: ds_rmpenyakitterbanyakrj,
		plugins: search,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				//cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},{
			text: 'Cetak Excel',
			id: 'cetakexcel',
			iconCls: 'silk-printer',
			handler: function() {
				cetakexcel();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		},'->'],
		columns: [new Ext.grid.RowNumberer(),{
			header: '<center>No. DTD</center>',
			dataIndex: 'dtd',
			align: 'center', 
			sortable: true, width: 80
		},
		{
			header: '<center>Kode ICD X</center>',
			dataIndex: 'kdpenyakit',
			align: 'center', 
			sortable: true, width: 80
		}
		,{
			header: '<center>Golongan Sebab Penyakit</center>',
			dataIndex: 'nmpenyakiteng',
			//align: 'center', 
			sortable: true, width: 180,
		},
		{
			//header: '<center>Usia <= 6 Hari<br>L</center>',
			dataIndex: 'kot',
			align: 'center', 
			sortable: true, width: 82, hidden: true
		},{
			//header: '<center>Usia <= 6 Hari<br>L</center>',
			dataIndex: 'idpenjamin',
			align: 'center', 
			sortable: true, width: 82, hidden: true
		},
		{
			header: '<center>Usia <= 6 Hari<br>L</center>',
			dataIndex: 'ukd6haril',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 6 Hari<br>P</center>',
			dataIndex: 'ukd6harip',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 28 Hari<br>L</center>',
			dataIndex: 'ukd28haril',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 28 Hari<br>P</center>',
			dataIndex: 'ukd28harip',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 1 Thn<br>L</center>',
			dataIndex: 'ukd1thnl',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 1 Thn<br>P</center>',
			dataIndex: 'ukd1thnp',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 4 Thn<br>L</center>',
			dataIndex: 'ukd4thnl',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 4 Thn<br>P</center>',
			dataIndex: 'ukd4thnp',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 14 Thn<br>L</center>',
			dataIndex: 'ukd14thnl',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 14 Thn<br>P</center>',
			dataIndex: 'ukd14thnp',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 24 Thn<br>L</center>',
			dataIndex: 'ukd24thnl',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 24 Thn<br>P</center>',
			dataIndex: 'ukd24thnp',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 44 Thn<br>L</center>',
			dataIndex: 'ukd44thnl',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 44 Thn<br>P</center>',
			dataIndex: 'ukd44thnp',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 64 Thn<br>L</center>',
			dataIndex: 'ukd64thnl',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia <= 64 Thn<br>P</center>',
			dataIndex: 'ukd64thnp',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia > 64 Thn<br>L</center>',
			dataIndex: 'uld64thnl',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Usia > 64 Thn<br>P</center>',
			dataIndex: 'uld64thnp',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Kasus Baru<br>L</center>',
			dataIndex: 'kbl',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Kasus Baru<br>P</center>',
			dataIndex: 'kbp',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Jumlah<br>Kasus Baru</center>',
			dataIndex: 'jmlkb',
			align: 'center', 
			sortable: true, width: 82
		},{
			header: '<center>Jumlah<br>Kunjungan</center>',
			dataIndex: 'jmlkunjungan',
			align: 'center', 
			sortable: true, width: 82
		}],
		
		//bbar: paging
	});
	
	/* END GRID */
	/* Daftar PO */
		var form_nya = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Penyakit Terbanyak Rawat Jalan',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				//title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal(Periode) :',
								//width: 100,
								margins: {top:3, right:3, bottom:0, left:10}
						},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}/* ,
								change : function(field, newValue){
									cAdvance();
								} */
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d', margins: '3 3 0 0'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}/* ,
								change : function(field, newValue){
									cAdvance();
								} */
							}
						},{
							xtype: 'label', id: 'lb.cd', text: 'Daerah :', margins: '3 3 0 10'
						},{
							xtype: 'combo',
							id: 'cb.daerah', width: 150, 
							store: ds_cdaerah, valueField: 'id', displayField: 'nama',
							editable: false, triggerAction: 'all', emptyText:'Pilih...',
							forceSelection: true, submitValue: true, mode: 'local',							
							listeners : {
								'select' : function(combo, record){	
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.cp', text: 'Penjamin :', margins: '3 3 0 10'
						},{
							xtype: 'combo',
							id: 'cb.penjamin', width: 250, emptyText:'Pilih...',
							store: ds_lrmpenjamin, valueField: 'idpenjamin', displayField: 'nmpenjamin',
							editable: false, triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							listeners : {
								'select' : function(combo, record){	
									cAdvance();
								}
							}
						}/* ,{
							xtype: 'label', id: 'lb.cj', text: 'Jenis :', margins: '3 3 0 10'
						},{
							xtype: 'combo',
							id: 'cb.jenis', width: 80, emptyText:'Pilih...',
							//store: ds_bagian,
							valueField: '', displayField: '',
							editable: false, triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
						} *//* ,{
							xtype: 'button',
							id: 'bsearch',
							align:'left',
							iconCls: 'silk-find',
							handler: function() {
								//SearchData();
							}
						} */]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				//title: '',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(form_nya);
	/* End Form */
		
	function cAdvance(){							
		ds_rmpenyakitterbanyakrj.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
		ds_rmpenyakitterbanyakrj.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));		
		ds_rmpenyakitterbanyakrj.setBaseParam('vdkot',2);
		ds_rmpenyakitterbanyakrj.setBaseParam('vdkott',Ext.getCmp('cb.daerah').getValue());
		ds_rmpenyakitterbanyakrj.setBaseParam('vpenj',2);
		ds_rmpenyakitterbanyakrj.setBaseParam('idpenjamin',Ext.getCmp('cb.penjamin').getValue());
		ds_rmpenyakitterbanyakrj.reload();
	}
	
	function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir		= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		RH.ShowReport(BASE_URL + 'print/Lap_rmkunjunganri/get_lap_rmkunjunganri/'
                +tglawal+'/'+tglakhir);
	}
	
	function cetakexcel(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var vdkot = '2';
		var vdkott2 = (Ext.getCmp('cb.daerah').getValue()) ? Ext.getCmp('cb.daerah').getValue():'null';
		var vpenj = '2';
		var idpenjamin2 = (Ext.getCmp('cb.penjamin').getValue()) ? Ext.getCmp('cb.penjamin').getValue():'null';
		
		window.location =(BASE_URL + 'print/Lap_rmpenyakitterbanyakrj/laporan_excelrmpenyakitterbanyakrj/'
                +tglawal+'/'+tglakhir+'/'+vdkot+'/'+vdkott2+'/'+vpenj+'/'+idpenjamin2);
	}

}