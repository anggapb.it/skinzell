function RMpermintaanOtomatis(){	
	
	var pageSize = 50;
	var interval;
	
	var ds_pelayanan = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'jpelayanan_controller/get_jpelayanan',
			method: 'POST'
		}),
		params: {
			start: 0, limit: 50
		},
		root: 'data', totalProperty: 'results', autoLoad: true,
		fields: [{
			name: "idjnspelayanan", mapping: "idjnspelayanan"
		},{
			name: "nmjnspelayanan", mapping: "nmjnspelayanan"
		}]	
	});
	
	var ds_lvlbagian = new Ext.data.JsonStore({		
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'rekammedis_controller/getBagian',
			method: 'POST'
		}),
		params: {
			start: 0, limit: 5
		},
		totalProperty: 'results',
		root: 'data',
		autoLoad: true,
		fields: [{
			name: "nmbagian", mapping: "nmbagian"
		},{
			name: "nmjnspelayanan", mapping: "nmjnspelayanan"
		},{
			name: "nmbdgrawat", mapping: "nmbdgrawat"
		}]
	});
	
	var ds_status = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'stspasien_controller/getStPasien',
			method: 'POST'
		}),
		params: {
			start: 0, limit: 50
		},
		root: 'data', totalProperty: 'results', autoLoad: true,
		fields: [{
			name: "nmstpasien", mapping: "nmstpasien"
		}]	
	});
	
	var ds_lokasi = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'rekammedis_controller/getLokasi',
			method: 'POST'
		}),
		params: {
			start: 0, limit: 50
		},
		root: 'data', totalProperty: 'results', autoLoad: true,
		fields: [{
			name: "idlokasi", mapping: "idlokasi"
		},{
			name: "idbagian", mapping: "idbagian"
		},{
			name: "kdlokasi", mapping: "kdlokasi"
		},{
			name: "nmlokasi", mapping: "nmlokasi"
		}]
	});
	
	var ds_permintaan = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'rekammedis_controller/getDataPermintaanRM',
			method: 'POST'
		}),
		params: {
			start: 0, limit: 50
		},
		root: 'data', totalProperty: 'results', autoLoad: true,
		fields: [{
			name: "normclear", mapping: "normclear"
		},{
			name: "norm", mapping: "norm"
		},{
			name: "noreg", mapping: "noreg"
		},{
			name: "nmpasien", mapping: "nmpasien"
		},{
			name: "nmstpasien", mapping: "nmstpasien"
		},{
			name: "tglminta", mapping: "tglminta"
		},{
			name: "jamminta", mapping: "jamminta"
		},{
			name: "bagiandari", mapping: "bagiandari"
		},{
			name: "userdari", mapping: "userdari"
		},{
			name: "bagiantuju", mapping: "bagiantuju"
		},{
			name: "usertuju", mapping: "usertuju"
		},{
			name: "tglkeluar", mapping: "tglkeluar"
		},{
			name: "jamkeluar", mapping: "jamkeluar"
		},{
			name: "nmjnsstkrm", mapping: "nmjnsstkrm"
		},{
			name: "idregdet", mapping: "idregdet"
		},{
			name: "idjnsstkrm", mapping: "idjnsstkrm"
		},{
			name: "kdlokasi", mapping: "kdlokasi"
		},{
			name: "nmjnspelayanan", mapping: "nmjnspelayanan"
		}]	
	});
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_permintaan,
		displayInfo: true,
		displayMsg: 'Data Permintaan Berkas RM Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});

	var grid_nya = new Ext.grid.EditorGridPanel({
		id: 'gPengirimanKartuRM', store: ds_permintaan,	forceFit: true,	
		autoScroll: true, columnLines: true, height: 350, frame: true,
		columns: [{
			header: 'No. RM', width: 70, dataIndex: 'normclear',
			align: 'center', sortable: true
		},
		{
			header: 'No. Registrasi', width: 80, dataIndex: 'noreg',
			align: 'center', sortable: true
		},
		{
			header: 'Nama Pasien',width: 200, dataIndex: 'nmpasien',
			align: 'left',sortable: true
		},{
			header: 'Status<br>Pasien',	align: 'left', width: 50,
			dataIndex: 'nmstpasien', sortable: true
		},{
			header: 'Tanggal<br>Minta',	width: 75, dataIndex: 'tglminta',
			align: 'center', sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: 'Jam<br>Minta', width: 75, dataIndex: 'jamminta',
			align: 'center', sortable: true
		},{
			header: 'Unit/Ruangan<br>( Minta )', width: 120, dataIndex: 'bagiandari',
			align: 'left', sortable: true
		},{
			header: 'Petugas Yang<br>Meminta', width: 80,
			dataIndex: 'userdari', align: 'left', sortable: true
		},{
			header: 'Unit/Ruangan<br>Yang Dituju', width: 120, dataIndex: 'bagiantuju',
			align: 'left', sortable: true
		},{
			header: 'Petugas Yang<br>Memberi', width: 80,
			dataIndex: 'usertuju', align: 'left', sortable: true
		},{
					xtype: 'actioncolumn',
					width: 50,
					header: '<center>Kirim</center>',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'resources/img/icons/fam/cart.png',
						tooltip: 'Pilih Jadwal Kuliah',
						handler: function(grid, rowIndex) {
							var record = grid.getStore().getAt(rowIndex);
							
							if (record.get("idjnsstkrm")=='3') {
								Ext.MessageBox.alert('Informasi', 'Berkas Sudah Kembali');
							} else {
								keluarkanRM(record.get("norm"), record.get("idregdet"));
							}
						}
					}]
		},{
			header: 'Tanggal<br>Kirim',	width: 75, dataIndex: 'tglkeluar',
			align: 'center', sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y')
		},{
			header: 'Jam<br>Kirim', width: 75, dataIndex: 'jamkeluar',
			align: 'center', sortable: true
		},{
			header: 'Status',	width: 75, dataIndex: 'nmjnsstkrm',
			align: 'center', sortable: true,
		}],		
		bbar: paging,
		listeners: {
			cellclick: function (grid, rowIndex, columnIndex, event) {				

			}
		}
	});
	
	
	//Main Form
	var main_form = new Ext.form.FormPanel({		
		id: 'formRMpengirimanKartuRM',
		region: 'center',
		bodyStyle: 'padding: 0px;',		
		border: false, frame: true,
		title: 'Permintaan Berkas RM Otomatis',
		autoScroll: true,
		tbar: [{
			text: 'Cari', iconCls: 'silk-find', 
			handler: function(){
				cariPermintaan();
			}
		},'->',
		{
			xtype:'checkbox',  
			id: 'cbxruntime',
			boxLabel: 'Runtime',
			checked: true,
			listeners: {
				check : function(cb, value) {
					if (value) {
						runTime();
					} else {
						clearInterval(interval);
					}
				}
			}

		}],
		items: [{
			xtype: 'fieldset',
			title: 'Daftar Permintaan Berkas RM',
			items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .26,
						border: false,				
						items: [{
							xtype: 'compositefield',
							id: 'composite1',
							items:[{
								xtype: 'checkbox',
								id:'chb.pelayanan',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('cbPelayanan').enable();
										} else if(val == false){
											Ext.getCmp('cbPelayanan').setValue(null);
											Ext.getCmp('cbPelayanan').disable();
										}
									}
								}
							},{
								xtype: 'tbtext',
								text: 'Pelayanan',
								width: 100,
							},{
								xtype: 'combo', id: 'cbPelayanan', name: 'cbPelayanan', store: ds_pelayanan,valueField: 'nmjnspelayanan',
								displayField: 'nmjnspelayanan',width: 130, height: 25, triggerAction: 'all',
								editable: false, submitValue: true, typeAhead: true, mode: 'local',
								emptyText: 'Pilih...', selectOnFocus: true, disabled: true
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_cnmpasien',
							items:[{
								xtype: 'checkbox',
								id:'chb.status',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('cbStatusPasien').enable();
										} else if(val == false){
											Ext.getCmp('cbStatusPasien').setValue(null);
											Ext.getCmp('cbStatusPasien').disable();
										}
									}
								}
							},{
								xtype: 'tbtext',
								text: 'Status Pasien',
								width: 100,
							},{
								xtype: 'combo', id: 'cbStatusPasien', name: 'cbStatusPasien', store: ds_status, valueField: 'nmstpasien',
								displayField: 'nmstpasien', width: 130, height: 25, triggerAction: 'all',
								editable: false, submitValue: true, typeAhead: true, mode: 'local',
								emptyText: 'Pilih...', selectOnFocus: true, disabled: true
							}]
						}]
					},{
						xtype: 'fieldset',
						columnWidth: .43,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_ctgllhr',
							items:[{
								xtype: 'checkbox',
								id:'chb.ctgllhr',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tUnitRuang').enable();
											Ext.getCmp('bRuang').enable();
										} else if(val == false){
											Ext.getCmp('tUnitRuang').disable();
											Ext.getCmp('bRuang').disable();
											Ext.getCmp('tUnitRuang').setValue(null);											
										}
									}
								}
							},{
								xtype: 'tbtext', text: 'Unit/Ruangan (Minta)', width: 130,
							},{
								xtype: 'textfield', id: 'tUnitRuang',name: 'tUnitRuang', width: 230, disabled: true
							},{
								xtype: 'button', text: ' ... ', id: 'bRuang', width: 28, disabled: true,
								handler: function() {
									cariRuangan();
								}
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_ctmplhr',
							items:[{
								xtype: 'checkbox',
								id:'chb.ctmplhr',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tNamaPasien').enable();
											Ext.getCmp('tNamaPasien').focus();
										} else if(val == false){
											Ext.getCmp('tNamaPasien').setValue(null);
											Ext.getCmp('tNamaPasien').disable();
										}
									}
								}
							},{
								xtype: 'tbtext', text: 'Nama Pasien', width: 130,
							},{
								xtype: 'textfield', id: 'tNamaPasien', name:'tNamaPasien',width: 265, disabled: true
							}]
						}]
					},{
						xtype: 'fieldset',
						columnWidth: .26,
						border: false,						
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnmibu',
							items:[{
								xtype: 'checkbox',
								id:'chb.cnmibu',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tNoRM').enable();
											Ext.getCmp('tNoRM').focus();
										} else if(val == false){
											Ext.getCmp('tNoRM').setValue(null);
											Ext.getCmp('tNoRM').disable();
										}
									}
								}
							},{
								xtype: 'tbtext', text: 'No. RM', width: 120,
							},{
								xtype: 'textfield', id: 'tNoRM', name: 'tNoRM',	
								width: 130, disabled: true
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_lokasi',
							items: [{
								xtype: 'checkbox',
								id: 'cbLokasi',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('cmbLokasi').enable();
											Ext.getCmp('cmbLokasi').focus();
										} else if(val == false){
											Ext.getCmp('cmbLokasi').setValue(null);
											Ext.getCmp('cmbLokasi').disable();
										}
									}
								}
							},{
								xtype: 'tbtext', text: 'Lokasi Berkas RM', width: 120,
							},{
								xtype: 'combo', id: 'cmbLokasi', name: 'cmbLokasi', store: ds_lokasi, valueField: 'kdlokasi',
								displayField: 'kdlokasi', width: 130, triggerAction: 'all',
								editable: false, submitValue: true, typeAhead: true, mode: 'local',
								emptyText: 'Pilih...', selectOnFocus: true, disabled: true,
							}]
						}]
					}]
				},{
					xtype: 'container',
					layout: 'form',
					items: [grid_nya]
				}]
		}],
		listeners: {
			afterrender: function () {
				runTime();
			}
		}
	});
	
	function runTime(){
		interval = setInterval(function() { 
			cariPermintaan();
		}, 3000);
	}
	
	function cariRuangan(){
		var cmnya = new Ext.grid.ColumnModel([{
			header: 'Nama Bagian', width: 130, dataIndex: 'nmbagian', sortable: true
		},{
			header: 'Jenis Pelayanan', width: 130, dataIndex: 'nmjnspelayanan', sortable: true,align: 'center'
		},{
			header: 'Bidang Perawatan', width: 300,	dataIndex: 'nmbdgrawat', sortable: true
		}]);
		
		var smnya = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var viewnya = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var pagginnya = new Ext.PagingToolbar({
			pageSize: 50, store: ds_lvlbagian, displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}', emptyMsg: 'Tidak ada data untuk ditampilkan'
		});
		
		var carinya = [new Ext.ux.grid.Search({
			iconCls: 'btn_search', minChars: 1, autoFocus: true, position: 'top',mode: 'local', width: 200
		})];
		
		var gridnya = new Ext.grid.GridPanel({
			ds: ds_lvlbagian, cm: cmnya, sm: smnya, view: viewnya, plugins: carinya,
			height: 350, width: 570, autoSizeColumns: true, enableColumnResize: true, enableColumnHide: false,
			enableColumnMove: false, enableHdaccess: false, columnLines: true, loadMask: true, buttonAlign: 'left', layout: 'anchor',
			anchorSize: {
				width: 400, height: 400
			},
			tbar: [],
			bbar: pagginnya,
			listeners: {
				rowdblclick: cariBagian
			}
		});
		
		var formCariRuangan = new Ext.Window({
			title: 'Cari Bagian (Unit/Ruangan)', modal: true,
			items: [gridnya]
		}).show();
		
		function cariBagian(grid, rowIdx){
			var databag = ds_lvlbagian.getAt(rowIdx);
			var namabag = databag.data["nmbagian"];
			Ext.getCmp("tUnitRuang").setValue(namabag);
			formCariRuangan.close();
		}
	}
	
	function keluarkanRM(norm, idregdet){
		var waitsend = Ext.MessageBox.wait('Mengirim...', 'Informasi');
		Ext.Ajax.request({
			url: BASE_URL + 'rekammedis_controller/updatekeluarRM',
			params: {
				norm : norm,
				idregdet : idregdet
			}, 
			success: function(response){
				waitsend.hide();
				cariPermintaan();
			},
			failure: function(response){
				waitsend.hide();
				Ext.MessageBox.alert('Informasi', 'Update Data Gagal');
				cariPermintaan();
			}
		});
	}
	
	function cariPermintaan(){				
		ds_permintaan.setBaseParam('nmjnspelayanan',Ext.getCmp('cbPelayanan').getValue());
		ds_permintaan.setBaseParam('nmstpasien',Ext.getCmp('cbStatusPasien').getValue());
		ds_permintaan.setBaseParam('nmpasien',Ext.getCmp('tNamaPasien').getValue());
		ds_permintaan.setBaseParam('norm',Ext.getCmp('tNoRM').getValue());
		ds_permintaan.setBaseParam('nmbagian',Ext.getCmp('tUnitRuang').getValue());
		ds_permintaan.setBaseParam('kdlokasi',Ext.getCmp('cmbLokasi').getValue());
		ds_permintaan.reload();
	}
		
	//Main Layout
	SET_PAGE_CONTENT(main_form);

}
