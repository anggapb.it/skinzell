function bor1(){
	
	
/* Data Store */

	
	var ds_bagian = dm_bagianri();
	
	var ds_bor1 = dm_bor1();
/* End Data Store */

/* GRID */
	var pageSize = 50;
	var paging = new Ext.PagingToolbar({
	pageSize: pageSize,
		store: ds_bor1,
		displayInfo: true,
		displayMsg: 'Data BOR Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_bor1,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [{
			header: 'Ruangan',dataIndex: 'nmbagian',
			//align: 'center', 
			sortable: true, width: 100
		},
		{
			header: 'TT<br>(Tempat Tidur)',dataIndex: 'jumlahbed',
		//	align: 'center', 
			sortable: true, width: 100
		},
		{
			header: 'BOR (%)', dataIndex: 'BOR',
		//	align: 'center',
			sortable: true, width: 100,xtype: 'numbercolumn', format:'0,000.00',
		}],		
		bbar: paging
	});
	
	Ext.chart.Chart.CHART_URL = BASE_URL + 'resources/js/ext/resources/charts.swf';	
	
	var panel1 = new Ext.Panel({
        width: 980,
        height: 300,
        title: 'Grafik BOR',
        
        items: {
			//xtype: 'columnchart',
            //store: ds_lsm,
			xtype: 'linechart',
            store: ds_bor1,
            xField: 'nmbagian',
			yAxis: new Ext.chart.NumericAxis({
                title: 'BOR',
				orientation:'vertical'
            }),
            yField: 'BOR',
			extraStyle: {
               xAxis: {
                    labelRotation: -90
                }
            },
			listeners: {
				itemclick: function(o){
					var rec = store.getAt(o.index);
					Ext.example.msg('Item Selected', 'You chose {0}.', rec.get('nmbagian'));
				}
			}
		},
    });
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',	
			labelStyle: 70,	
			border: false, frame: true,
			title: 'BOR(Bed Occupancy Rate)',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
				//	defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								//labelStyle: 70,	
								width: 70,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d', hidden: true,
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
						//	hidden: true,
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
						//	style: 'padding: 10px',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'BOR',
				items:[grid_nya]
			},panel1]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
function cAdvance(){
	ds_bor1.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
	ds_bor1.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
//	ds_bor1.setBaseParam('bagian',Ext.getCmp('cb.ruangan').getValue());
	ds_bor1.reload();
}
function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
	//	var ruangan     = Ext.getCmp('cb.ruangan').getValue();
	//	alert(ruangan)
		RH.ShowReport(BASE_URL + 'print/Lap_rekamedis_bor/rekammedis/'
                +tglawal+'/'+tglakhir);
	}	
	
}