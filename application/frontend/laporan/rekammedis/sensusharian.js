function sensusharian(){
// ============= GRID PASIEN MASUK
	var fields_pasienmasuk = RH.storeFields('norm','noreg','tglmasuk',
					'jammasuk','nmpasien','nmdoktergelar','nmklsrawat','nmkamar',
					'nmbed','idbagian');
					
	var ds_pasienmasuk = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_sensus_harian_controller/get_sensus_harian/1', 
				method: 'POST'
			}),
			params: {

			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields : fields_pasienmasuk,
	});
	
	var search_pasienmasuk = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];

	var grid_pasienmasuk = new Ext.grid.GridPanel({
		id: 'grid_pasienmasuk', //sm: cbGrid, 
		store: ds_pasienmasuk,
		plugins: search_pasienmasuk,
		frame: true,
		height: 175,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: 'No.', width:35 }),
		{
			header: 'No. RM',dataIndex: 'norm',
			align: 'left',
			sortable: true, width: 84
		},
		{
			header: 'No. REG',dataIndex: 'noreg',
			align: 'left', 
			sortable: true, width: 100
		},
		{
			header: 'Tgl. Masuk', dataIndex: 'tglmasuk',
			align: 'left', renderer: Ext.util.Format.dateRenderer('d/m/Y'),
			sortable: true, width: 100
		},{
			header: 'Jam Masuk',	
			align: 'left', 
			dataIndex: 'jammasuk', sortable: true, width: 100
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Dokter', dataIndex: 'nmdoktergelar',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Kelas', dataIndex: 'nmklsrawat',
			align: 'left',  
			sortable: true, width: 100
		},{
			header: 'Kamar/TT', dataIndex: 'nmkamar',
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Bed',dataIndex: 'nmbed', 
			align: 'left', 
			sortable: true, width: 100
		}],		
	});
// ============= END GRID PASIEN MASUK

// ============= GRID PASIEN PINDAHAN
	var fields_pasienpindahan = RH.storeFields('norm','noreg','tglmasuk',
					'jammasuk','nmpasien','nmdoktergelar','nmklsrawat','nmkamar',
					'nmbed','idbagian','keterangan');
					
	var ds_pasienpindahan = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_sensus_harian_controller/get_sensus_harian/2', 
				method: 'POST'
			}),
			params: {

			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields : fields_pasienpindahan,
	});
	
	var search_pasienpindahan = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];

	var grid_pasienpindahan = new Ext.grid.GridPanel({
		id: 'grid_pasienpindahan', //sm: cbGrid, 
		store: ds_pasienpindahan,
		plugins: search_pasienpindahan,
		frame: true,
		height: 175,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: 'No.', width:35 }),
		{
			header: 'No. RM',dataIndex: 'norm',
			align: 'left', 
			sortable: true, width: 84
		},
		{
			header: 'No. REG',dataIndex: 'noreg',
			align: 'left', 
			sortable: true, width: 100
		},
		{
			header: 'Tgl. Masuk', dataIndex: 'tglmasuk',
			align: 'left', renderer: Ext.util.Format.dateRenderer('d/m/Y'), 
			sortable: true, width: 100
		},{
			header: 'Jam Masuk',	
			align: 'left', 
			dataIndex: 'jammasuk', sortable: true, width: 100
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Dokter', dataIndex: 'nmdoktergelar',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Kelas', dataIndex: 'nmklsrawat',
			align: 'left',  
			sortable: true, width: 100
		},{
			header: 'Kamar/TT', dataIndex: 'nmkamar',
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Bed',dataIndex: 'nmbed', 
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Keterangan',dataIndex: 'keterangan', 
			align: 'left', 
			sortable: true, width: 200
		}],		
	});
// ============= END GRID PASIEN PINDAHAN

// ============= GRID PASIEN DIPINDAHKAN
	var fields_pasiendipindahkan = RH.storeFields('norm','noreg','tglkeluar',
					'jamkeluar','nmpasien','nmdoktergelar','nmklsrawat','nmkamar',
					'nmbed','idbagian','keterangan');
					
	var ds_pasiendipindahkan = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_sensus_harian_controller/get_sensus_harian/3', 
				method: 'POST'
			}),
			params: {

			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields : fields_pasiendipindahkan,
	});
	
	var search_pasiendipindahkan = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];

	var grid_pasiendipindahkan = new Ext.grid.GridPanel({
		id: 'grid_pasiendipindahkan', //sm: cbGrid, 
		store: ds_pasiendipindahkan,
		plugins: search_pasiendipindahkan,
		frame: true,
		height: 175,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: 'No.', width:35 }),
		{
			header: 'No. RM',dataIndex: 'norm',
			align: 'left', 
			sortable: true, width: 84
		},
		{
			header: 'No. REG',dataIndex: 'noreg',
			align: 'left', 
			sortable: true, width: 100
		},
		{
			header: 'Tgl. Keluar', dataIndex: 'tglkeluar',
			align: 'left', renderer: Ext.util.Format.dateRenderer('d/m/Y'), 
			sortable: true, width: 100
		},{
			header: 'Jam Keluar',	
			align: 'left', 
			dataIndex: 'jamkeluar', sortable: true, width: 100
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Dokter', dataIndex: 'nmdoktergelar',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Kelas', dataIndex: 'nmklsrawat',
			align: 'left',  
			sortable: true, width: 100
		},{
			header: 'Kamar/TT', dataIndex: 'nmkamar',
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Bed',dataIndex: 'nmbed', 
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Keterangan',dataIndex: 'keterangan', 
			align: 'left', 
			sortable: true, width: 200
		}],		
	});
// ============= END GRID PASIEN DIPINDAHKAN

// ============= GRID PASIEN KELUAR HIDUP
	var fields_pasienkeluarhidup = RH.storeFields('norm','noreg','tglkeluar',
					'jamkeluar','nmpasien','nmdoktergelar','nmklsrawat','nmkamar',
					'nmbed','idbagian');
					
	var ds_pasienkeluarhidup = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_sensus_harian_controller/get_sensus_harian/4', 
				method: 'POST'
			}),
			params: {

			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields : fields_pasienkeluarhidup,
	});
	
	var search_pasienkeluarhidup = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];

	var grid_pasienkeluarhidup = new Ext.grid.GridPanel({
		id: 'grid_pasienkeluarhidup', //sm: cbGrid, 
		store: ds_pasienkeluarhidup,
		plugins: search_pasienkeluarhidup,
		frame: true,
		height: 175,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: 'No.', width:35 }),
		{
			header: 'No. RM',dataIndex: 'norm',
			align: 'left', 
			sortable: true, width: 84
		},
		{
			header: 'No. REG',dataIndex: 'noreg',
			align: 'left', 
			sortable: true, width: 100
		},
		{
			header: 'Tgl. Keluar', dataIndex: 'tglkeluar',
			align: 'left', renderer: Ext.util.Format.dateRenderer('d/m/Y'), 
			sortable: true, width: 100
		},{
			header: 'Jam Keluar',	
			align: 'left', 
			dataIndex: 'jamkeluar', sortable: true, width: 100
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Dokter', dataIndex: 'nmdoktergelar',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Kelas', dataIndex: 'nmklsrawat',
			align: 'left',  
			sortable: true, width: 100
		},{
			header: 'Kamar/TT', dataIndex: 'nmkamar',
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Bed',dataIndex: 'nmbed', 
			align: 'left', 
			sortable: true, width: 100
		}],		
	});
// ============= END GRID PASIEN KELUAR HIDUP

// ============= GRID PASIEN MENINGGAL < 48 JAM
	var fields_pasien_meninggal_kurang_48_jam = RH.storeFields('norm','noreg','tglmeninggal',
					'jammeninggal','nmpasien','nmdoktergelar','nmklsrawat','nmkamar',
					'nmbed','idbagian');
					
	var ds_pasien_meninggal_kurang_48_jam = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_sensus_harian_controller/get_sensus_harian/5', 
				method: 'POST'
			}),
			params: {

			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields : fields_pasien_meninggal_kurang_48_jam,
	});
	
	var search_pasien_meninggal_kurang_48_jam = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];

	var grid_pasien_meninggal_kurang_48_jam = new Ext.grid.GridPanel({
		id: 'grid_pasien_meninggal_kurang_48_jam', //sm: cbGrid, 
		store: ds_pasien_meninggal_kurang_48_jam,
		plugins: search_pasien_meninggal_kurang_48_jam,
		frame: true,
		height: 175,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: 'No.', width:35 }),
		{
			header: 'No. RM',dataIndex: 'norm',
			align: 'left', 
			sortable: true, width: 84
		},
		{
			header: 'No. REG',dataIndex: 'noreg',
			align: 'left', 
			sortable: true, width: 100
		},
		{
			header: 'Tgl. Meninggal', dataIndex: 'tglmeninggal',
			align: 'left', renderer: Ext.util.Format.dateRenderer('d/m/Y'), 
			sortable: true, width: 100
		},{
			header: 'Jam Meninggal',	
			align: 'left', 
			dataIndex: 'jammeninggal', sortable: true, width: 100
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Dokter', dataIndex: 'nmdoktergelar',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Kelas', dataIndex: 'nmklsrawat',
			align: 'left',  
			sortable: true, width: 100
		},{
			header: 'Kamar/TT', dataIndex: 'nmkamar',
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Bed',dataIndex: 'nmbed', 
			align: 'left', 
			sortable: true, width: 100
		}],		
	});
// ============= END GRID PASIEN MENINGGAL < 48 JAM

// ============= GRID PASIEN MENINGGAL >= 48 JAM
	var fields_pasien_meninggal_lebih_sm_dgn_48_jam = RH.storeFields('norm','noreg','tglmeninggal',
					'jammeninggal','nmpasien','nmdoktergelar','nmklsrawat','nmkamar',
					'nmbed','idbagian');
					
	var ds_pasien_meninggal_lebih_sm_dgn_48_jam = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_sensus_harian_controller/get_sensus_harian/6', 
				method: 'POST'
			}),
			params: {

			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields : fields_pasien_meninggal_lebih_sm_dgn_48_jam,
	});
	
	var search_pasien_meninggal_lebih_sm_dgn_48_jam = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];

	var grid_pasien_meninggal_lebih_sm_dgn_48_jam = new Ext.grid.GridPanel({
		id: 'grid_pasien_meninggal_lebih_sm_dgn_48_jam', //sm: cbGrid, 
		store: ds_pasien_meninggal_lebih_sm_dgn_48_jam,
		plugins: search_pasien_meninggal_lebih_sm_dgn_48_jam,
		frame: true,
		height: 175,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: 'No.', width:35 }),
		{
			header: 'No. RM',dataIndex: 'norm',
			align: 'left', 
			sortable: true, width: 84
		},
		{
			header: 'No. REG',dataIndex: 'noreg',
			align: 'left', 
			sortable: true, width: 100
		},
		{
			header: 'Tgl. Meninggal', dataIndex: 'tglmeninggal',
			align: 'left', renderer: Ext.util.Format.dateRenderer('d/m/Y'), 
			sortable: true, width: 100
		},{
			header: 'Jam Meninggal',	
			align: 'left', 
			dataIndex: 'jammeninggal', sortable: true, width: 100
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Dokter', dataIndex: 'nmdoktergelar',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Kelas', dataIndex: 'nmklsrawat',
			align: 'left',  
			sortable: true, width: 100
		},{
			header: 'Kamar/TT', dataIndex: 'nmkamar',
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Bed',dataIndex: 'nmbed', 
			align: 'left', 
			sortable: true, width: 100
		}],		
	});
// ============= END GRID PASIEN MENINGGAL >= 48 JAM

// ============= GRID PASIEN HARI INI
	var fields_pasien_hari_ini = RH.storeFields('norm','noreg','tglmasuk',
					'jammasuk','nmpasien','nmdoktergelar','nmklsrawat','nmkamar',
					'nmbed','idbagian');
	
	var ds_pasien_hari_ini = RH.JsonStore({	
        url : BASE_URL + 'laporan_sensus_harian_controller/get_sensus_harian/7', 
		fields : fields_pasien_hari_ini,
		limit: 20,
		params: [{key:'cbxtgl', id:'cbxtgl'},
				{key:'cbxjam', id:'cbxjam'},
				{key:'cbxruangan', id:'cbxruangan'},
				
				{key:'tgl', id:'df.tgl'},
				{key:'jam', id:'jam'},
				{key:'ruangan', id:'cb.ruangan'}],
		enableSearch: true,
	});	
	
	var search_pasien_hari_ini = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];

	var pagging_pasien_hari_ini = new Ext.PagingToolbar({
		pageSize: 20, store: ds_pasien_hari_ini,
		displayInfo: true, mode: 'local',
		displayMsg: 'Data {0} - {1} dari {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	
	var grid_pasien_hari_ini = new Ext.grid.GridPanel({
		id: 'grid_pasien_hari_ini', //sm: cbGrid, 
		store: ds_pasien_hari_ini,
		plugins: search_pasien_hari_ini,
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: 'No.', width:35 }),
		{
			header: 'No. RM',dataIndex: 'norm',
			align: 'left', 
			sortable: true, width: 84
		},
		{
			header: 'No. REG',dataIndex: 'noreg',
			align: 'left', 
			sortable: true, width: 100
		},
		{
			header: 'Tgl. Masuk', dataIndex: 'tglmasuk',
			align: 'left', renderer: Ext.util.Format.dateRenderer('d/m/Y'), 
			sortable: true, width: 100
		},{
			header: 'Jam Masuk',	
			align: 'left', 
			dataIndex: 'jammasuk', sortable: true, width: 100
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Dokter', dataIndex: 'nmdoktergelar',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: 'Kelas', dataIndex: 'nmklsrawat',
			align: 'left',  
			sortable: true, width: 100
		},{
			header: 'Kamar/TT', dataIndex: 'nmkamar',
			align: 'left', 
			sortable: true, width: 100
		},{
			header: 'Bed',dataIndex: 'nmbed', 
			align: 'left', 
			sortable: true, width: 100
		}],
		bbar: pagging_pasien_hari_ini,		
	});
// ============= END GRID PASIEN MASUK
	
	var ds_bagian = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_sensus_harian_controller/get_bagian_all', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			}]
		});
	
		var form_sensusharian = new Ext.form.FormPanel({
			id: 'form_sensusharian',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Sensus Harian',
			autoScroll: true,
			defaults: { labelWidth: 70, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				title: 'FILTER',
				layout: 'column',
				items: [{
					layout: 'form', columnWidth: 0.25,
					items: [{
						xtype: 'compositefield', fieldLabel: 'Tanggal',
						items: [{
									xtype:'checkbox',  
									id: 'cbxtgl',
									listeners: {
										check : function(cb, value) {
											cAdvance();
										}
									}
								},{
									xtype: 'datefield', id: 'df.tgl', fieldLabel: 'Tanggal',
									width: 100, value: new Date(),
									format: 'd-m-Y',
									listeners:{
										select: function(field, newValue){
											var cbxtgl = Ext.getCmp('cbxtgl').getValue();
											if (cbxtgl){
												cAdvance();
											}	
										},
										change : function(field, newValue){
											var cbxtgl = Ext.getCmp('cbxtgl').getValue();
											if (cbxtgl){
												cAdvance();
											}
										}
									}
								}]
						}]
				},{
					layout: 'form', columnWidth: 0.25,
					items: [{
						xtype: 'compositefield', fieldLabel: 'Jam',
						items: [{
									xtype:'checkbox',  
									id: 'cbxjam',
									listeners: {
										check : function(cb, value) {
											cAdvance();
										}
									}
								},{
									fieldLabel: 'Jam',
									xtype: 'timefield',
									emptyText:'Pilih...',
									id: 'jam',
									width: 70,
									format: 'H:i',
									altFormats:'H:i',
									increment: 1,
									listeners:{
										select :function(combo, record, index){ 
											var cbxjam = Ext.getCmp('cbxjam').getValue();
											if (cbxjam){
												cAdvance();
											}
										}
									}
								}]
						}]
				},{
					layout: 'form', columnWidth: 0.25,
					items: [{
						xtype: 'compositefield', fieldLabel: 'Ruangan',
						items: [{
									xtype:'checkbox',  
									id: 'cbxruangan',
									listeners: {
										check : function(cb, value) {
											cAdvance();
										}
									}
								},{
									xtype: 'combo', fieldLabel: 'Ruangan',
									id: 'cb.ruangan', width: 150,
									store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
									editable: false, triggerAction: 'all',
									forceSelection: true, submitValue: true, mode: 'local',
									emptyText:'Pilih...',
									listeners:{
										select:function(combo, records, eOpts){
											var cbxruangan = Ext.getCmp('cbxruangan').getValue();
											if (cbxruangan){
												cAdvance();
											}
										}
									}
								}]
						}]
				},{
					layout: 'form', columnWidth: 0.25,
					items: [{
							xtype: 'button',
							text: 'Cetak PDF',
							id: 'cetak',
							iconCls: 'silk-printer',
							handler: function() {
								cetakLap();
							}
						}]
				}]
			},
			{
				xtype: 'fieldset',
				title: '1. PASIEN MASUK',
				items:[grid_pasienmasuk]
			},
			{
				xtype: 'fieldset',
				title: '2. PASIEN PINDAHAN',
				items:[grid_pasienpindahan]
			},
			{
				xtype: 'fieldset',
				title: '3. PASIEN DIPINDAHKAN',
				items:[grid_pasiendipindahkan]
			},
			{
				xtype: 'fieldset',
				title: '4. PASIEN KELUAR HIDUP',
				items:[grid_pasienkeluarhidup]
			},
			{
				xtype: 'fieldset',
				title: '5. PASIEN MENINGGAL < 48 JAM',
				items:[grid_pasien_meninggal_kurang_48_jam]
			},
			{
				xtype: 'fieldset',
				title: '6. PASIEN MENINGGAL >= 48 JAM',
				items:[grid_pasien_meninggal_lebih_sm_dgn_48_jam]
			},
			{
				xtype: 'fieldset',
				title: '7. PASIEN HARI INI (Sesuai Dengan Tanggal Masuk Yang Di Filter)',
				items:[grid_pasien_hari_ini]
			}]

		}); SET_PAGE_CONTENT(form_sensusharian);

	
	function cAdvance(){
		
		ds_pasienmasuk.setBaseParam('cbxtgl',Ext.getCmp('cbxtgl').getValue());
		ds_pasienmasuk.setBaseParam('cbxjam',Ext.getCmp('cbxjam').getValue());
		ds_pasienmasuk.setBaseParam('cbxruangan',Ext.getCmp('cbxruangan').getValue());
		
		ds_pasienmasuk.setBaseParam('tgl',Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'));
		ds_pasienmasuk.setBaseParam('jam',Ext.getCmp('jam').getValue());
		ds_pasienmasuk.setBaseParam('ruangan',Ext.getCmp('cb.ruangan').getValue());
		ds_pasienmasuk.reload();
		
		ds_pasienpindahan.setBaseParam('cbxtgl',Ext.getCmp('cbxtgl').getValue());
		ds_pasienpindahan.setBaseParam('cbxjam',Ext.getCmp('cbxjam').getValue());
		ds_pasienpindahan.setBaseParam('cbxruangan',Ext.getCmp('cbxruangan').getValue());
		
		ds_pasienpindahan.setBaseParam('tgl',Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'));
		ds_pasienpindahan.setBaseParam('jam',Ext.getCmp('jam').getValue());
		ds_pasienpindahan.setBaseParam('ruangan',Ext.getCmp('cb.ruangan').getValue());
		ds_pasienpindahan.reload();
		
		ds_pasiendipindahkan.setBaseParam('cbxtgl',Ext.getCmp('cbxtgl').getValue());
		ds_pasiendipindahkan.setBaseParam('cbxjam',Ext.getCmp('cbxjam').getValue());
		ds_pasiendipindahkan.setBaseParam('cbxruangan',Ext.getCmp('cbxruangan').getValue());
		
		ds_pasiendipindahkan.setBaseParam('tgl',Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'));
		ds_pasiendipindahkan.setBaseParam('jam',Ext.getCmp('jam').getValue());
		ds_pasiendipindahkan.setBaseParam('ruangan',Ext.getCmp('cb.ruangan').getValue());
		ds_pasiendipindahkan.reload();
		
		ds_pasienkeluarhidup.setBaseParam('cbxtgl',Ext.getCmp('cbxtgl').getValue());
		ds_pasienkeluarhidup.setBaseParam('cbxjam',Ext.getCmp('cbxjam').getValue());
		ds_pasienkeluarhidup.setBaseParam('cbxruangan',Ext.getCmp('cbxruangan').getValue());
		
		ds_pasienkeluarhidup.setBaseParam('tgl',Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'));
		ds_pasienkeluarhidup.setBaseParam('jam',Ext.getCmp('jam').getValue());
		ds_pasienkeluarhidup.setBaseParam('ruangan',Ext.getCmp('cb.ruangan').getValue());
		ds_pasienkeluarhidup.reload();
		
		ds_pasien_meninggal_kurang_48_jam.setBaseParam('cbxtgl',Ext.getCmp('cbxtgl').getValue());
		ds_pasien_meninggal_kurang_48_jam.setBaseParam('cbxjam',Ext.getCmp('cbxjam').getValue());
		ds_pasien_meninggal_kurang_48_jam.setBaseParam('cbxruangan',Ext.getCmp('cbxruangan').getValue());
		
		ds_pasien_meninggal_kurang_48_jam.setBaseParam('tgl',Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'));
		ds_pasien_meninggal_kurang_48_jam.setBaseParam('jam',Ext.getCmp('jam').getValue());
		ds_pasien_meninggal_kurang_48_jam.setBaseParam('ruangan',Ext.getCmp('cb.ruangan').getValue());
		ds_pasien_meninggal_kurang_48_jam.reload();
		
		ds_pasien_meninggal_lebih_sm_dgn_48_jam.setBaseParam('cbxtgl',Ext.getCmp('cbxtgl').getValue());
		ds_pasien_meninggal_lebih_sm_dgn_48_jam.setBaseParam('cbxjam',Ext.getCmp('cbxjam').getValue());
		ds_pasien_meninggal_lebih_sm_dgn_48_jam.setBaseParam('cbxruangan',Ext.getCmp('cbxruangan').getValue());
		
		ds_pasien_meninggal_lebih_sm_dgn_48_jam.setBaseParam('tgl',Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'));
		ds_pasien_meninggal_lebih_sm_dgn_48_jam.setBaseParam('jam',Ext.getCmp('jam').getValue());
		ds_pasien_meninggal_lebih_sm_dgn_48_jam.setBaseParam('ruangan',Ext.getCmp('cb.ruangan').getValue());
		ds_pasien_meninggal_lebih_sm_dgn_48_jam.reload();
		
		ds_pasien_hari_ini.reload({
			params: {
				cbxtgl		:Ext.getCmp('cbxtgl').getValue(),
				cbxjam		:Ext.getCmp('cbxjam').getValue(),
				cbxruangan	:Ext.getCmp('cbxruangan').getValue(),
				
				tgl			:Ext.getCmp('df.tgl').getValue().format('Y-m-d'),
				jam			:Ext.getCmp('jam').getValue(),
				ruangan		:Ext.getCmp('cb.ruangan').getValue(),
			}
		});
	}
	
	function cetakLap(){
		var cbxtgl = Ext.getCmp('cbxtgl').getValue();
		var cbxjam = Ext.getCmp('cbxjam').getValue();
		var cbxruangan = Ext.getCmp('cbxruangan').getValue();
		
		var tgl = Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d');
		var jam = (Ext.getCmp('jam').getValue()) ? Ext.getCmp('jam').getValue():"-";
		var ruangan = (Ext.getCmp('cb.ruangan').getValue()) ? Ext.getCmp('cb.ruangan').getValue():"-";

		if (!cbxtgl && !cbxjam && !cbxruangan){
			Ext.MessageBox.alert("Informasi", "Pilih Filter");
		} else {
			RH.ShowReport(BASE_URL + 'print/Lap_rekamedis_sensusharian/sensusharian/'+
			cbxtgl+'/'+cbxjam+'/'+cbxruangan+'/'+tgl+'/'+jam+'/'+ruangan);
		}
		
	}
	
}