function rmpemakaiandarah(){
// ============= GRID
	var fields_pemakaiandarah = RH.storeFields('tglkuitansi','nmpasien','noreg','nmdaerah','nmpenjamin',
												'nmpenyakit','nmgoldarah','nmpelayanan','sumqty');
					
	var ds_pemakaiandarah = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_pemakaiandarah_controller/get_pemakaiandarah', 
				method: 'POST'
			}),
			baseParams: {
				tglawal	:Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglakhir:Ext.util.Format.date(new Date(), 'Y-m-d')
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields : fields_pemakaiandarah,
	});
	
	var search_pemakaiandarah = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: false,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];

	var grid_pemakaiandarah = new Ext.grid.GridPanel({
		id: 'grid_pemakaiandarah', //sm: cbGrid, 
		store: ds_pemakaiandarah,
		plugins: search_pemakaiandarah,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:['->'],
		columns: [new Ext.grid.RowNumberer({ header: '<b>No.</b>', width:35 }),
		{
			header: '<b><center>Tanggal</center></b>',dataIndex: 'tglkuitansi',
			align: 'center', xtype: 'datecolumn', format: 'd/m/Y',
			sortable: true, width: 84
		},{
			header: '<b><center>No. Registrasi</center></b>', dataIndex: 'noreg',
			align: 'center', 
			sortable: true,width: 90
		},{
			header: '<b><center>Nama</center></b>', dataIndex: 'nmpasien',
			align: 'left', 
			sortable: true,width: 240
		},{
			header: '<b><center>Kota</center></b>', dataIndex: 'nmdaerah',
			align: 'left', 
			sortable: true,width: 100
		},{
			header: '<b><center>Penjamin</center></b>', dataIndex: 'nmpenjamin',
			align: 'left', 
			sortable: true,width: 150
		},{
			header: '<b><center>Diagnosa</center></b>', dataIndex: 'nmpenyakit',
			align: 'left', 
			sortable: true,width: 240
		},{
			header: '<b><center>Golongan Darah</center></b>', dataIndex: 'nmgoldarah',
			align: 'center',  
			sortable: true, width: 100
		},{
			header: '<b><center>Jenis Darah</center></b>', dataIndex: 'nmpelayanan',
			align: 'left', 
			sortable: true, width: 97
		},{
			header: '<b><center>Jumlah Pemakaian</center></b>',dataIndex: 'sumqty', 
			align: 'center', 
			sortable: true, width: 130
		}],		
	});
// ============= END GRID

		var form_pemakaiandarah = new Ext.form.FormPanel({
			id: 'form_pemakaiandarah',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Laporan Pemakaian Darah',
			autoScroll: true,
			defaults: { labelWidth: 150, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				title: 'PENCARIAN',
				items: [{
						xtype: 'compositefield', fieldLabel: 'Periode Per Bulan',
						items: [{
									xtype: 'datefield', id: 'df.tglawal', 
									width: 100, value: new Date(),
									format: 'd/m/Y',
									listeners:{
										select: function(field, newValue){
										
										},
										change : function(field, newValue){
										
										}
									}
								},{
									xtype: 'tbtext', text: 's/d',
								},{
									xtype: 'datefield', id: 'df.tglakhir', 
									width: 100, value: new Date(),
									format: 'd/m/Y',
									listeners:{
										select: function(field, newValue){
										
										},
										change : function(field, newValue){
										
										}
									}
								},{
									xtype: 'button',
									id: 'bsearch',
									align:'left',
									iconCls: 'silk-find',
									handler: function() {
										SearchData();
									}
								},{
									xtype: 'button',
									text: 'Cetak PDF',
									id: 'cetak',
									iconCls: 'silk-printer',
									handler: function() {
										cetakLap();
									}
								}]
						}]
			},{
				xtype: 'fieldset',
				title: 'DATA PEMAKAIAN DARAH',
				items:[grid_pemakaiandarah]
			}]

		}); SET_PAGE_CONTENT(form_pemakaiandarah);

	
	function SearchData(){
		var blnawal = Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m');
		var blnakhir = Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m');

		//if (blnawal == blnakhir){
			ds_pemakaiandarah.reload({
				params: {
					tglawal	:Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'),
					tglakhir:Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d')
				}
			});
		/* } else {
			Ext.MessageBox.alert("Informasi", "Periode Harus Per Bulan");
		} */
	}
	
	function cetakLap(){
		var tglawal = Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir = Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');

		var blnawal = Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m');
		var blnakhir = Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m');

		//if (blnawal == blnakhir){
			RH.ShowReport(BASE_URL + 'print/Lap_rekamedis_pemakaiandarah/pemakaiandarah/'+
				tglawal+'/'+tglakhir);
		/* } else {
			Ext.MessageBox.alert("Informasi", "Periode Harus Per Bulan");
		} */
	}
	
}