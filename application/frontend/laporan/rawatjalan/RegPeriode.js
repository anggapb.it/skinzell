function RegPeriode(){
	var ds_vlaprjperiode = dm_vlaprjperiode();
	var ds_dokter_combo = dm_cekdktr_lapkeurj();
	ds_vlaprjperiode.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlaprjperiode.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	var ds_bagian = dm_bagian();
	ds_bagian.setBaseParam('jpel',1);
	
	var cm_laporan = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header: 'No. Reg',
			dataIndex: 'noreg',
			width: 80
		},{
			header: 'Tgl. Reg',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 60,
		},{
			header: 'Nama Pasien',
			dataIndex: 'nmpasien',
			width: 130
		},{
			header: 'Tindakan',
			dataIndex: 'utindakan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Obat / Produk',
			dataIndex: 'uobat',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Pemeriksaan', 
			hidden:true,
			dataIndex: 'upemeriksaan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 100
		},{
			header: 'Imunisasi',
			dataIndex: 'uimun',
			hidden:true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'LAB',
			dataIndex: 'ulab',
			hidden:true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Racik',
			dataIndex: 'uracik',
			hidden:true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Lain-lain',
			dataIndex: 'ulain',
			hidden:true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Disc',
			dataIndex: 'udiskon',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Non Tunai',
			dataIndex: 'ucc',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Tunai',
			dataIndex: 'utotal',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Total',
			dataIndex: 'total',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Klinik',
			dataIndex: 'nmbagian',
			width: 100
		},{
			header: 'Dokter',
			dataIndex: 'nmdoktergelar',
			width: 150
		}
	]);
	var paging_laporan = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_vlaprjperiode,
		displayInfo: true,
		displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_laporan = new Ext.grid.GridPanel({
		ds: ds_vlaprjperiode,
		cm: cm_laporan,
		height: 350,
		autoWidth: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		buttonAlign: 'left',
		layout: 'anchor',
		frame: true,
		tbar: [
			{ text: 'Cetak', iconCls: 'silk-printer', handler: function(){cetakLapRJ();} },'-',
			{ text: 'Cetak Excel', iconCls: 'silk-printer', handler: function(){exportdata();} }, '->',
			{ xtype:'tbfill' },'Grand Total : ',
			{
				xtype: 'numericfield',
				id: 'tf.grandtotal',
				value: 0,
				readOnly:true,
				style : 'opacity:0.6',
				width: 80,
				thousandSeparator:','
			}
		
		],
		anchorSize: {
			width: 400,
			height: 400
		},
		bbar: paging_laporan
	});
	
	var lapperperiode = new Ext.FormPanel({
		id: 'fp.pasienrj',
		title: 'Laporan Registrasi Rawat Jalan Per Periode',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [/* {
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 1, labelAlign: 'right'},
				items:[{
					xtype: 'button',
					text: 'Cetak',
					id: 'btn.cetak',
					style: 'padding: 5px',
					width: 100,
					handler: function() {
						cetakLapRJ();
					}
				},{
					xtype: 'button',
					text: 'Cetak Excel',
					id: 'btn.cetakExcel',
					style: 'padding: 5px',
					width: 120,
					handler: function() {
						exportdata();
					}
				}]
			}, */{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 50, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: 1,
					border: false,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						}]
					},{
						xtype: 'combo', fieldLabel: 'Poli',
						id: 'cb.poli', width: 150,
						store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih Poli',
						listeners:{
							select:function(combo, records, eOpts){
								cAdvance();
								/*if(records.get('idbagian') != 4){
									
								} else {
								
								}*/
							}
						}
					},{
						xtype: 'combo', fieldLabel: 'Dokter',
						id: 'cb.dktr', width: 225,
						store: ds_dokter_combo, valueField: 'iddokter', displayField: 'nmdoktergelar',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih Dokter',
						listeners:{
							select:function(combo, records, eOpts){
								ds_vlaprjperiode.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
								ds_vlaprjperiode.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
								ds_vlaprjperiode.setBaseParam('bagian',Ext.getCmp('cb.poli').getValue());
								ds_vlaprjperiode.setBaseParam('iddokter',Ext.getCmp('cb.dktr').getValue());
								ds_vlaprjperiode.reload();
							}
						}
					}]
				}]
			},
			grid_laporan
		]
	});
	SET_PAGE_CONTENT(lapperperiode);	
	
	function cAdvance(){
		ds_vlaprjperiode.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlaprjperiode.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlaprjperiode.setBaseParam('bagian',Ext.getCmp('cb.poli').getValue());
		ds_vlaprjperiode.setBaseParam('iddokter',Ext.getCmp('cb.dktr').getValue());
		ds_vlaprjperiode.reload({
			callback: function(results){
				if(results == 0) return;
				var sum = 0;
				ds_vlaprjperiode.each(function (rec) {
					sum += parseFloat(rec.get('total'));
					RH.setCompValue('tf.grandtotal', sum); 
				}); 
			}
		});

		ds_dokter_combo.setBaseParam('bagian',Ext.getCmp('cb.poli').getValue());
		ds_dokter_combo.reload();
	}
	
	function cetakLapRJ(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var bagian		= Ext.getCmp('cb.poli').getValue();
		var iddokter	= Ext.getCmp('cb.dktr').getValue();
		if(bagian == '') bagian = 'all';
		if(iddokter == '') iddokter = 'all';
		RH.ShowReport(BASE_URL + 'print/laprawatjalan/regperiode/'
                +tglawal+'/'+tglakhir+'/'+bagian+'/'+iddokter);
	}

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
        var bagian=Ext.getCmp('cb.poli').getValue();
		var iddokter	= Ext.getCmp('cb.dktr').getValue();
		if(bagian == '') bagian = 'all';
		if(iddokter == '') iddokter = 'all';

            window.location = BASE_URL + 'print/laprawatjalan/exportexcel/'+tglawal+'/'+tglakhir+'/'+bagian+'/'+iddokter;

    }
	
}