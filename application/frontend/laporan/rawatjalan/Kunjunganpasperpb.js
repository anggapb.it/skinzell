function Kunjunganpasperpb(){
		
	
/* Data Store */

	
	var ds_rawatjalan = dm_rawatjalan();
	var ds_bagianrj = dm_bagianrj();
	var ds_penjamin = dm_penjamin();
	ds_rawatjalan.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_rawatjalan.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
//	var ds_ruangan = dm_ruangan();
/* End Data Store */

/* GRID */
	var pageSize = 50;
	var paging = new Ext.PagingToolbar({
	pageSize: pageSize,
		store: ds_rawatjalan,
		displayInfo: true,
		displayMsg: 'Data Rawat Jalan Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_rawatjalan,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [{
			header: 'Tgl. Registrasi', dataIndex: 'tglmasuk',
		//	align: 'center',
			sortable: true, width: 90
		},{
			header: 'No. Reg',	
		//	align: 'center', 
			dataIndex: 'noreg', sortable: true, width: 90
		},{
			header: 'No. RM', dataIndex: 'norm', 
			sortable: true, width: 90
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true,width: 170
		},{
			header: 'Alamat', dataIndex: 'alamat',
		//	align: 'center', 
			sortable: true, width: 170
		},{
			header: 'Penanggung<br>Biaya', dataIndex: 'nmpenjamin',
		//	align: 'left',
			sortable: true, width: 100
		},{
			header: 'Cara Datang',dataIndex: 'nmcaradatang', 
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Dokter Rawat',dataIndex: 'nmdoktergelar', 
		//	align: 'center', 
			sortable: true, width: 200
		}],		
		bbar: paging
	});
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Kunjungan Pasien Rawat Jalan Per Penanggung Biaya',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
				//	defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
						//	style: 'padding: 10px',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					},{
					xtype: 'compositefield', style: 'margin: 10px 0 0 0',
						items:[{
								xtype: 'tbtext',
								text: 'Penjamin :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
						xtype: 'combo', fieldLabel: '',
						id: 'cb.ruangan', width: 150,
						store: ds_penjamin, valueField: 'idpenjamin', displayField: 'nmpenjamin',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih Penjamin',
						listeners:{
							select:function(combo, records, eOpts){
								cAdvance();
							}
						}
						}]
					 }]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'Pasien',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
function cAdvance(){
	ds_rawatjalan.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
	ds_rawatjalan.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
	ds_rawatjalan.setBaseParam('penjamin',Ext.getCmp('cb.ruangan').getValue());
	ds_rawatjalan.reload();
}
function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		var ruangan     = Ext.getCmp('cb.ruangan').getValue();
	//	alert(ruangan)
	if(ruangan == '' || ruangan == null){
		alert('Harap Lengkapi Data Pencarian');
	}else{
		RH.ShowReport(BASE_URL + 'print/Lap_rj_perpb/rawatjalan/'
                +tglawal+'/'+tglakhir + '/' + ruangan);
		}
		
	}	
		
}