function Regdaftarpasien(){

	var ds_registrasi_daftarpasien = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_registrasi_controller/get_laporan_registrasi/lapregdaftarpasien', 
				method: 'POST'
			}),
			baseParams: {
				/* checkdate: false,
				start: 0,
				limit: 25 */
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: false,
			fields: [{
				name: 'tgldaftar',
				mapping: 'tgldaftar'
			},{
				name: 'norm',
				mapping: 'norm'
			},
			{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},
			{
				name: 'nmibu',
				mapping: 'nmibu'
			},
			{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},
			{
				name: 'nmstkawin',
				mapping: 'nmstkawin'
			},
			{
				name: 'tptlahir',
				mapping: 'tptlahir'
			},
			{
				name: 'tgllahir',
				mapping: 'tgllahir'
			},
			{
				name: 'alamat',
				mapping: 'alamat'
			},
			{
				name: 'kodepos',
				mapping: 'kodepos'
			},
			{
				name: 'kelurahan',
				mapping: 'kelurahan'
			},
			{
				name: 'kecamatan',
				mapping: 'kecamatan'
			},
			{
				name: 'kota',
				mapping: 'kota'
			},
			{
				name: 'provinsi',
				mapping: 'provinsi'
			},
			{
				name: 'nohp',
				mapping: 'nohp'
			},
			{
				name: 'notelp',
				mapping: 'notelp'
			},
			{
				name: 'catatan',
				mapping: 'catatan'
			},
			{
				name: 'nmagama',
				mapping: 'nmagama'
			},
			{
				name: 'nmgoldarah',
				mapping: 'nmgoldarah'
			},
			{
				name: 'nmpendidikan',
				mapping: 'nmpendidikan'
			},
			{
				name: 'nmpekerjaan',
				mapping: 'nmpekerjaan'
			},
			{
				name: 'nmsukubangsa',
				mapping: 'nmsukubangsa'
			},
			{
				name: 'noidentitas',
				mapping: 'noidentitas'
			},
			{
				name: 'nmwn',
				mapping: 'nmwn'
			},
			{
				name: 'negara',
				mapping: 'negara'
			},
			{
				name: 'alergi',
				mapping: 'alergi'
			}]
	});
	
	var ds_search = new Ext.data.JsonStore({ //static data
		autoDestroy: true,
		fields: [ 'field', 'value' ],
	});

	var cm_registrasi_daftarpasien = new Ext.grid.ColumnModel({
		columns: [
			new Ext.grid.RowNumberer(),
		{
			header: 'Tgl. Daftar',dataIndex: 'tgldaftar', 
			sortable: true, width: 70
		},{
			header: 'No. RM',dataIndex: 'norm', 
			sortable: true, width: 70
		},{
			header: 'Nama Pasien',dataIndex: 'nmpasien', 
			sortable: true, width: 150
		},{
			header: 'Nama Orangtua/Pasangan',dataIndex: 'nmibu', 
			sortable: true, width: 150
		},{
			header: 'Jenis Kelamin',dataIndex: 'nmjnskelamin', 
			sortable: true, width: 100
		},{
			header: 'Status Kawin',dataIndex: 'nmstkawin', 
			sortable: true, width: 100
		},{
			header: 'Tempat Lahir',dataIndex: 'tptlahir', 
			sortable: true, width: 100
		},{
			header: 'Tgl. Lahir',dataIndex: 'tgllahir', 
			sortable: true, width: 100
		},{
			header: 'Alamat',dataIndex: 'alamat', 
			sortable: true, width: 100
		},{
			header: 'Kode Pos',dataIndex: 'kodepos', 
			sortable: true, width: 100
		},{
			header: 'Kelurahan',dataIndex: 'kelurahan', 
			sortable: true, width: 100
		},{
			header: 'Kecamatan',dataIndex: 'kecamatan', 
			sortable: true, width: 100
		},{
			header: 'Kota/Kabupaten',dataIndex: 'kota', 
			sortable: true, width: 100
		},{
			header: 'Propinsi',dataIndex: 'provinsi', 
			sortable: true, width: 100
		},{
			header: 'No. Handphone',dataIndex: 'nohp', 
			sortable: true, width: 100
		},{
			header: 'No. Telepon',dataIndex: 'notelp', 
			sortable: true, width: 100
		},{
			header: 'Catatan',dataIndex: 'catatan', 
			sortable: true, width: 100
		},{
			header: 'Agama',dataIndex: 'nmagama', 
			sortable: true, width: 100
		},{
			header: 'Gol. Darah',dataIndex: 'nmgoldarah', 
			sortable: true, width: 100
		},{
			header: 'Pendidikan',dataIndex: 'nmpendidikan', 
			sortable: true, width: 100
		},{
			header: 'Pekerjaan',dataIndex: 'nmpekerjaan', 
			sortable: true, width: 100
		},{
			header: 'Suku Bangsa',dataIndex: 'nmsukubangsa', 
			sortable: true, width: 100
		},{
			header: 'No. KTP/Passpor',dataIndex: 'noidentitas', 
			sortable: true, width: 100
		},{
			header: 'Kebangsaan',dataIndex: 'nmwn', 
			sortable: true, width: 100
		},{
			header: 'Negara',dataIndex: 'negara', 
			sortable: true, width: 100
		},{
			header: 'Alergi Obat',dataIndex: 'alergi', 
			sortable: true, width: 100
		}]
	});
	
	var vw = new Ext.grid.GridView({emptyText:'< Belum Ada Data..  >'});
	
	var paging = new Ext.PagingToolbar({
		pageSize: 25,
		store: ds_registrasi_daftarpasien,
		displayInfo: true,
		displayMsg: 'Daftar Pasien Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_registrasi_daftarpasien = new Ext.grid.GridPanel({
		id: 'grid_registrasi_daftarpasien',
		height: 400,
		ds: ds_registrasi_daftarpasien,
		cm: cm_registrasi_daftarpasien,
		sm: new Ext.grid.RowSelectionModel({ singleSelect: true}),
		vw:vw,
		tbar: [{
			text: 'Cetak Excel',
			id: 'cetakexcel',
			iconCls: 'silk-save',
			handler: function() {
				excel();
			}
		}],	
		clicksToEdit: 1,	//for cell editing (single click =1, dblclick=2)
        forceFit: true, //autoHeight: true, 
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,		
		loadMask: true,
		layout: 'anchor',		
		bbar: paging,
		
	});

	var form_registrasi_daftarpasien = new Ext.form.FormPanel({
			id: 'form_registrasi_daftarpasien',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Laporan Registrasi Daftar Pasien',
			autoScroll: true,
			labelWidth: 100, labelAlign: 'right',
			items: [{
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
						xtype: 'compositefield', fieldLabel: 'Tanggal Daftar',
						items: [{
									xtype:'checkbox',  
									id: 'cbxtgl',
									listeners: {
										check : function(cb, value) {
											if(value){
												cari(true);
											} else {
												cari(false);
											}
										}
									}
								},{
									xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
									width: 100, value: new Date(),
									format: 'd-m-Y',
									listeners:{
										select : function(field, newValue){
											var checkval = Ext.getCmp('cbxtgl').getValue();
											if(checkval){
												cari(true);
											} else {
												cari(false);
											}
										}
									}
								},{
									xtype: 'label', id: 'lb.sd', text: 's/d'
								},{
									xtype: 'datefield', id: 'df.tglakhir',
									width: 100, value: new Date(),
									format: 'd-m-Y',
									listeners:{
										select : function(field, newValue){
											var checkval = Ext.getCmp('cbxtgl').getValue();
											if(checkval){
												cari(true);
											} else {
												cari(false);
											}
										}
									}
								}]
					},{
						xtype: 'compositefield', fieldLabel: 'Cari Berdasarkan',
						items:[{
										xtype: 'combo', fieldLabel: '',
										id:'cb.search', autoWidth: true, store: ds_search,
										valueField: 'value', displayField: 'field', editable: false,allowBlank: true,
										triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
										emptyText:'Pilih....',
										listeners: {
											afterrender: function () {
												var a, rec, header, index;
													for (a=2;a<=26;a++) {
														header = grid_registrasi_daftarpasien.getColumnModel().getColumnHeader(a).replace('<center>','').replace('</center>','').replace('<br>','');
														index = grid_registrasi_daftarpasien.getColumnModel().getDataIndex(a);
													
														rec = new ds_search.recordType({field:header, value:index});
														rec.commit();
														ds_search.add(rec);
														
														if (a==2) { Ext.getCmp('cb.search').setValue(index) }
													}
											},
											select: function () {
												var checkval = Ext.getCmp('cbxtgl').getValue();
												if(checkval){
													cari(true);
												} else {
													cari(false);
												}
											}
										}
									},{
										xtype: 'textfield',
										id: 'tf.search',
										width:250,
										allowBlank: true,
										listeners: {
													specialkey: function(f,e){
														if (e.getKey() == e.ENTER) {
															var checkval = Ext.getCmp('cbxtgl').getValue();
															if(checkval){
																cari(true);
															} else {
																cari(false);
															}
														}
													}
												}
									},{
										xtype: 'button',
										id: 'bsearch',
										align:'left',
										iconCls: 'silk-find',
										handler: function() {
											var checkval = Ext.getCmp('cbxtgl').getValue();
											if(checkval){
												cari(true);
											} else {
												cari(false);
											}
										}
						}]
				}]
			}, 
			{
				xtype: 'fieldset',
				title: '',
				items:[grid_registrasi_daftarpasien]
			}],
			listeners:{
				afterrender:function(){
					ds_registrasi_daftarpasien.on('beforeload', function(s) {
						s.setBaseParam('checkdate', (Ext.getCmp('cbxtgl').getValue()) ? true:false);
						s.setBaseParam('tglawal', Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
						s.setBaseParam('tglakhir', Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
						s.setBaseParam('fields', Ext.getCmp('cb.search').getValue());
						s.setBaseParam('keyword', Ext.getCmp('tf.search').getValue());
						s.setBaseParam('start', 0);
						s.setBaseParam('limit', 25);
					});
					ds_registrasi_daftarpasien.load();			
				}
			}
	}); SET_PAGE_CONTENT(form_registrasi_daftarpasien);
	
	function cari(isDate) {
		
		if (Ext.getCmp('cb.search').getValue()) {
			ds_registrasi_daftarpasien.load({
				params: {
					checkdate: isDate, 
					tglawal: Ext.getCmp('df.tglawal').getValue().format('Y-m-d'),
					tglakhir: Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'), 
					fields: Ext.getCmp('cb.search').getValue(),
					keyword: Ext.getCmp('tf.search').getValue(),
					start: 0,
					limit: 25
				}
			});
		} else {
			ds_registrasi_daftarpasien.load({
				params: {
					checkdate: isDate, 
					tglawal: Ext.getCmp('df.tglawal').getValue().format('Y-m-d'),
					tglakhir: Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'),
					start: 0,
					limit: 25
				}
			});
		}
		
	}
	
	function excel(){	
		var checkval 	= Ext.getCmp('cbxtgl').getValue();	
		var tglawal		= (checkval) ? Ext.getCmp('df.tglawal').getValue().format('Y-m-d'):null;
		var tglakhir	= (checkval) ? Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'):null;
		var fields		= Ext.getCmp('cb.search').getValue();
		var keyword		= (Ext.getCmp('tf.search').getValue()) ? Ext.getCmp('tf.search').getValue():null;
		
		window.location = BASE_URL + 'print/laporan_registrasi/export_laporan_registrasi/lapregdaftarpasien/'
                +tglawal+'/'+tglakhir+'/'+fields+'/'+keyword;
	}

}