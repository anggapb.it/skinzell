function Regugd(){

	var ds_registrasi_ugd = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'laporan_registrasi_controller/get_laporan_registrasi/lapregugd', 
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'jamreg',
				mapping: 'jamreg'
			},
			{
				name: 'nmshift',
				mapping: 'nmshift'
			},
			{
				name: 'noreg',
				mapping: 'noreg'
			},
			{
				name: 'nmstpasien',
				mapping: 'nmstpasien'
			},
			{
				name: 'norm',
				mapping: 'norm'
			},
			{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},
			{
				name: 'alamat',
				mapping: 'alamat'
			},
			{
				name: 'kodepos',
				mapping: 'kodepos'
			},
			{
				name: 'kelurahan',
				mapping: 'kelurahan'
			},
			{
				name: 'kecamatan',
				mapping: 'kecamatan'
			},
			{
				name: 'kota',
				mapping: 'kota'
			},
			{
				name: 'umurtahun',
				mapping: 'umurtahun'
			},
			{
				name: 'umurbulan',
				mapping: 'umurbulan'
			},
			{
				name: 'umurhari',
				mapping: 'umurhari'
			},
			{
				name: 'nmpenjamin',
				mapping: 'nmpenjamin'
			},
			{
				name: 'nmcaradatang',
				mapping: 'nmcaradatang'
			},
			{
				name: 'nmbagiankirim',
				mapping: 'nmbagiankirim'
			},
			{
				name: 'nmdokterkirim',
				mapping: 'nmdokterkirim'
			},
			{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},
			{
				name: 'nmdokter',
				mapping: 'nmdokter'
			},
			{
				name: 'nmspesialisasi',
				mapping: 'nmspesialisasi'
			},
			{
				name: 'nmkerabat',
				mapping: 'nmkerabat'
			},
			{
				name: 'notelpkerabat',
				mapping: 'notelpkerabat'
			},
			{
				name: 'nmhubkeluarga',
				mapping: 'nmhubkeluarga'
			},
			{
				name: 'catatan',
				mapping: 'catatan'
			},
			{
				name: 'nmstregistrasi',
				mapping: 'nmstregistrasi'
			},
			{
				name: 'nmlengkap',
				mapping: 'nmlengkap'
			}]
	});
	
	var ds_search = new Ext.data.JsonStore({ //static data
		autoDestroy: true,
		fields: [ 'field', 'value' ],
	});
		
	ds_registrasi_ugd.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_registrasi_ugd.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));

	var cm_registrasi_ugd = new Ext.grid.ColumnModel({
		columns: [
			new Ext.grid.RowNumberer(),
		{
			header: 'Tgl. Registrasi',dataIndex: 'tglreg', 
			sortable: true, width: 100
		},{
			header: 'Jam Registrasi',dataIndex: 'jamreg', 
			sortable: true, width: 100
		},{
			header: 'Shift',dataIndex: 'nmshift', 
			sortable: true, width: 70
		},{
			header: 'No. Registrasi',dataIndex: 'noreg', 
			sortable: true, width: 100
		},{
			header: '(Baru/Lama)',dataIndex: 'nmstpasien', 
			sortable: true, width: 100
		},{
			header: 'No. RM',dataIndex: 'norm', 
			sortable: true, width: 100
		},{
			header: 'Nama Pasien',dataIndex: 'nmpasien', 
			sortable: true, width: 100
		},{
			header: 'Alamat',dataIndex: 'alamat', 
			sortable: true, width: 100
		},{
			header: 'Kode Pos',dataIndex: 'kodepos', 
			sortable: true, width: 100
		},{
			header: 'Kelurahan',dataIndex: 'kelurahan', 
			sortable: true, width: 100
		},{
			header: 'Kecamatan',dataIndex: 'kecamatan', 
			sortable: true, width: 100
		},{
			header: 'Kota/Kabupaten',dataIndex: 'kota', 
			sortable: true, width: 100
		},{
			header: 'Umur Tahun',dataIndex: 'umurtahun', 
			sortable: true, width: 100
		},{
			header: 'Umur Bulan',dataIndex: 'umurbulan', 
			sortable: true, width: 100
		},{
			header: 'Umur Hari',dataIndex: 'umurhari', 
			sortable: true, width: 100
		},{
			header: 'Penjamin',dataIndex: 'nmpenjamin', 
			sortable: true, width: 100
		},{
			header: 'Cara Datang',dataIndex: 'nmcaradatang', 
			sortable: true, width: 100
		},{
			header: 'Unit Pengirim',dataIndex: 'nmbagiankirim', 
			sortable: true, width: 100
		},{
			header: 'Dokter Pengirim',dataIndex: 'nmdokterkirim', 
			sortable: true, width: 100
		},{
			header: 'Unit Pelayanan',dataIndex: 'nmbagian', 
			sortable: true, width: 100
		},{
			header: 'Dokter',dataIndex: 'nmdokter', 
			sortable: true, width: 100
		},{
			header: 'Spesialisasi',dataIndex: 'nmspesialisasi', 
			sortable: true, width: 100
		},{
			header: 'Nama Kerabat',dataIndex: 'nmkerabat', 
			sortable: true, width: 100
		},{
			header: 'No. Telp./HP',dataIndex: 'notelpkerabat', 
			sortable: true, width: 100
		},{
			header: 'Hubungan Keluarga',dataIndex: 'nmhubkeluarga', 
			sortable: true, width: 100
		},{
			header: 'Catatan Registrasi',dataIndex: 'catatan', 
			sortable: true, width: 100
		},{
			header: 'Status Registrasi',dataIndex: 'nmstregistrasi', 
			sortable: true, width: 100
		},{
			header: 'User Input',dataIndex: 'nmlengkap', 
			sortable: true, width: 100
		}]
	});
	
	var grid_registrasi_ugd = new Ext.grid.GridPanel({
		id: 'grid_registrasi_ugd',
		height: 400,
		ds: ds_registrasi_ugd,
		cm: cm_registrasi_ugd,
		sm: new Ext.grid.RowSelectionModel({ singleSelect: true}),
		tbar: [{
			text: 'Cetak Excel',
			id: 'cetakexcel',
			iconCls: 'silk-save',
			handler: function() {
				excel();
			}
		}],	
		clicksToEdit: 1,	//for cell editing (single click =1, dblclick=2)
        forceFit: true, //autoHeight: true, 
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,		
		loadMask: true,
		layout: 'anchor',		
		bbar: [],
		
	});

	var form_registrasi_ugd = new Ext.form.FormPanel({
			id: 'form_registrasi_ugd',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Laporan Registrasi UGD (Unit Gawat Darurat)',
			autoScroll: true,
			labelWidth: 130, labelAlign: 'right',
			items: [{
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
						xtype: 'compositefield', fieldLabel: 'Tanggal Registrasi',
						items: [{
									xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
									width: 100, value: new Date(),
									format: 'd-m-Y',
									listeners:{
										select : function(field, newValue){
											cari();
										}
									}
								},{
									xtype: 'label', id: 'lb.sd', text: 's/d'
								},{
									xtype: 'datefield', id: 'df.tglakhir',
									width: 100, value: new Date(),
									format: 'd-m-Y',
									listeners:{
										select : function(field, newValue){
											cari();
										}
									}
								}]
					},{
						xtype: 'compositefield', fieldLabel: 'Cari Berdasarkan',
						items:[{
										xtype: 'combo', fieldLabel: '',
										id:'cb.search', autoWidth: true, store: ds_search,
										valueField: 'value', displayField: 'field', editable: false,allowBlank: true,
										triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local',
										emptyText:'Pilih....',
										listeners: {
											afterrender: function () {
												var a, rec, header, index;
													for (a=2;a<=28;a++) {
														header = grid_registrasi_ugd.getColumnModel().getColumnHeader(a).replace('<center>','').replace('</center>','').replace('<br>','');
														index = grid_registrasi_ugd.getColumnModel().getDataIndex(a);
													
														rec = new ds_search.recordType({field:header, value:index});
														rec.commit();
														ds_search.add(rec);
														
														if (a==2) { Ext.getCmp('cb.search').setValue(index) }
													}
											},
											select: function () {
												cari();
											}
										}
									},{
										xtype: 'textfield',
										id: 'tf.search',
										width:250,
										allowBlank: true,
										listeners: {
													specialkey: function(f,e){
														if (e.getKey() == e.ENTER) {
															cari();
														}
													}
												}
									},{
										xtype: 'button',
										id: 'bsearch',
										align:'left',
										iconCls: 'silk-find',
										handler: function() {
											cari();
										}
						}]
				}]
			}, 
			{
				xtype: 'fieldset',
				title: '',
				items:[grid_registrasi_ugd]
			}]
	}); SET_PAGE_CONTENT(form_registrasi_ugd);
	
	function cari() {
		ds_registrasi_ugd.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_registrasi_ugd.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		if (Ext.getCmp('cb.search').getValue()) {
			ds_registrasi_ugd.setBaseParam('fields',Ext.getCmp('cb.search').getValue());
			ds_registrasi_ugd.setBaseParam('keyword',Ext.getCmp('tf.search').getValue());
		}
		ds_registrasi_ugd.reload();
	}
	
	function excel(){		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var fields		= Ext.getCmp('cb.search').getValue();
		var keyword		= (Ext.getCmp('tf.search').getValue()) ? Ext.getCmp('tf.search').getValue():null;
		
		window.location = BASE_URL + 'print/laporan_registrasi/export_laporan_registrasi/lapregugd/'
                +tglawal+'/'+tglakhir+'/'+fields+'/'+keyword;
	}

}