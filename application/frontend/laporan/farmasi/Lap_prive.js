function Lap_prive(){
	Ext.form.Field.prototype.msgTarget = 'side';
	
	var ds_lap_prive = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'lapprive_controller/get_lap_prive',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "nmjnspelayanan",
			mapping: "nmjnspelayanan"
		},{
			name: "tglkuitansi",
			mapping: "tglkuitansi"
		},{
			name: "noreg",
			mapping: "noreg"
		},{
			name: "norm",
			mapping: "norm"
		},{
			name: "nmpasien",
			mapping: "nmpasien"
		},{
			name: "atasnama",
			mapping: "atasnama"
		},{
			name: "kditem",
			mapping: "kditem"
		},{
			name: "nmbrg",
			mapping: "nmbrg"
		},{
			name: "nmsatuan",
			mapping: "nmsatuan"
		},{
			name: "qty",
			mapping: "qty"
		},{
			name: "hrgjual",
			mapping: "hrgjual"
		},{
			name: "subtotal",
			mapping: "subtotal"
		},{
			name: "nokuitansi",
			mapping: "nokuitansi"
		},]
	});
	
	var vw_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_lap_prive',
		store: ds_lap_prive,
		view: vw_nya,	
		frame: true,
		autoScroll: true,
		autoWidth: true,
		columnLines: true,
		autoScroll: true,
		loadMask: true,
		height: 445,
		tbar: [{
			text: 'Cetak PDF',
			id: 'btn.cetak',	
			iconCls:'silk-printer',		
			handler: function() {
				cetak_pdf();
			}
		},'-',
		{ 	text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',	
			handler: function(){
				cetak_excel();
			}
		}],
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('Jenis Pelayanan'),
			width: 120,
			dataIndex: 'nmjnspelayanan',
			sortable: true,
			align:'left'
		},{
			header: headerGerid('No. Reg / Nota'),
			width: 90,
			dataIndex: 'noreg',
			sortable: true,
			align:'left'
		},{
			header: headerGerid('Tgl. Kuitansi'),
			width: 75,
			dataIndex: 'tglkuitansi',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('No. RM'),
			width: 60,
			dataIndex: 'norm',
			sortable: true,
			align:'left'
		},{
			header: headerGerid('Nama Pasien'),
			width: 100,
			dataIndex: 'nmpasien',
			sortable: true,
			align:'left'
		},{
			header: headerGerid('Atas Nama'),
			width: 100,
			dataIndex: 'atasnama',
			sortable: true,
			align:'left'
		},{
			header: headerGerid('Kode'),
			width: 80,
			dataIndex: 'kditem',
			sortable: true,
			align:'left'
		},{
			header: headerGerid('Obat'),
			width: 160,
			dataIndex: 'nmbrg',
			sortable: true,
			align:'left'
		},{
			header: headerGerid('Tarif'),
			width: 70,
			dataIndex: 'hrgjual',
			sortable: true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: headerGerid('Qty'),
			width: 50,
			dataIndex: 'qty',
			sortable: true,
			align:'right'
		},{
			header: headerGerid('Satuan'),
			width: 80,
			dataIndex: 'nmsatuan',
			sortable: true,
			align:'center'
		},{
			header: headerGerid('Subtotal'),
			width: 70,
			dataIndex: 'subtotal',
			sortable: true,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},
		]
	});
       
	var form_bp_general = new Ext.FormPanel({
		id: 'form_bp_general',
		title: 'Laporan Prive (UP)',
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		items: [
		{
			layout: 'form',
			border: false,
			items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 1, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: .99,
					border: false,
					items: [{
						xtype: 'compositefield',
						style: 'padding: 5px; marginLeft: -5px; marginBottom: -6px',
						width: 470,
						items: [{
							xtype: 'label', id: 'lb.bgna', text: 'Periode :', margins: '3 10 0 5',
						},{
							xtype: 'datefield',
							id: 'df.tglawal',
							width: 110,
							value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cektgl();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: ' s.d ', margins: '3 10 0 5',
						},{
							xtype: 'datefield',
							id: 'df.tglakhir',
							width: 110,
							value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cektgl();
								}
							}
						}]
					}]
				}]
			},
			grid_nya
			]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadLapprive(){
		ds_lap_prive.reload();
	}
	
	function cektgl(){
		ds_lap_prive.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_lap_prive.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		reloadLapprive();
	}
	
	function cetak_pdf(){
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		RH.ShowReport(BASE_URL + 'print/lap_prive/laporan_prive_pdf/'
                +tglawal+'/'+tglakhir);
	}
	
	function cetak_excel(){
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		RH.ShowReport(BASE_URL + 'print/lap_prive/laporan_prive_excel/'
                +tglawal+'/'+tglakhir);
	}

}