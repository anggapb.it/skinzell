function Regseluruhasal(){
	
	
/* Data Store */

	
	var ds_regpasinri = dm_regpasinri();
	var ds_bagian = dm_bagianri();
	ds_regpasinri.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_regpasinri.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
/* End Data Store */

/* GRID */
	var pageSize = 50;
	var paging = new Ext.PagingToolbar({
	pageSize: pageSize,
		store: ds_regpasinri,
		displayInfo: true,
		displayMsg: 'Data Rawat Inap Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_regpasinri,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [{
			header: 'No. REG',dataIndex: 'noreg',
		//	align: 'center', 
			sortable: true, width: 90
		},
		{
			header: 'Tgl. Masuk', dataIndex: 'tglmasuk',
		//	align: 'center',
			sortable: true, width: 90
		},{
			header: 'Jam Masuk',	
		//	align: 'center', 
			dataIndex: 'jammasuk', sortable: true, width: 90
		},{
			header: 'No. RM', dataIndex: 'norm', 
			sortable: true, width: 90
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true,width: 150
		},{
			header: 'Pekerjaan', dataIndex: 'pekerjaan',
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Penanggung<br>Biaya', dataIndex: 'nmpenjamin',
		//	align: 'left',
			sortable: true, width: 100
		},{
			header: 'Keluhan',dataIndex: 'keluhan', 
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Kelas',dataIndex: 'nmkelastarif', 
		//	align: 'center', 
			sortable: true, width: 70
		},{
			header: 'Kamar',dataIndex: 'kamar', 
		//	align: 'center', 
			sortable: true, width: 70
		},{
			header: 'Dokter Rawat',dataIndex: 'nmdoktergelar', 
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Asal Rujukan',dataIndex: 'nmcaradatang', 
		//	align: 'center', 
			sortable: true, width: 100
		}],		
		bbar: paging
	});
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Pasien Rawat Inap Seluruh Asal Rujukan Pasien',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
				//	defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
						//	style: 'padding: 10px',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'Pasien Rawat Inap',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
function cAdvance(){
	ds_regpasinri.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
	ds_regpasinri.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
//	ds_regpasinri.setBaseParam('bagian',Ext.getCmp('cb.ruangan').getValue());
	ds_regpasinri.reload();
}
function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
	//	var ruangan     = Ext.getCmp('cb.ruangan').getValue();
	//	alert(ruangan)
		RH.ShowReport(BASE_URL + 'print/Lap_ri_seluruhasal/rawatinap/'
                +tglawal+'/'+tglakhir);
	}	
	
}