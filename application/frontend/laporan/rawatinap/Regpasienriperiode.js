function Regpasienriperiode(){
	
	
/* Data Store */

	
	var ds_regpasinri = dm_regpasinri();
	ds_regpasinri.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_regpasinri.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
/* End Data Store */

/* GRID */
	var reader = new Ext.data.JsonReader({
			root:'data',
			idProperty: '',
			totalProperty: 'results',
			remoteGroup: true,
			fields: [ 
			{   name: 'noreg' }
			, { name: 'tglmasuk' }
			, { name: 'norm' }
			, { name: 'nmpasien' } 
			, { name: 'pekerjaan' }
			, { name: 'nmpenjamin' }
			, { name: 'keluhan' }
			, { name: 'nmkelastarif' }
			, { name: 'kamar' }
			, { name: 'nmdoktergelar' }
			, { name: 'nmcarakeluar' }
			, { name: 'nmbagian' }
			, { name: 'kdbagian' }
			]
		});
		
var ds_regpasinri2 = new Ext.data.GroupingStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'laporan_rawatinap/get_pasienriperiode',
			method: 'POST',
		}),
		/*
		baseParams: { 
			thmasuk: RH.getCompValue('cb.thmasuk-dpk'), 
			idklsmhs: RH.getCompValue('cb.jbayar-dpk') 
		},
		*/		
		reader: reader,
		groupField:'nmbagian',
		//sortInfo:{field:'kdbagian',direction:'ASC'},		
		remoteSort: true,
	});

	
//===
	var pageSize = 50;
	var paging = new Ext.PagingToolbar({
	pageSize: pageSize,
		store: ds_regpasinri,
		displayInfo: true,
		displayMsg: 'Data Rawat Inap Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	
	
	var cm_bagian = new Ext.grid.ColumnModel({
		columns: [{
				header: 'Bagian', width: 205,
				dataIndex: 'nmbagian', //sortable: true
			},/* {
			header: 'No. REG',dataIndex: 'noreg',
		//	align: 'center', 
			sortable: true, width: 90
		}, */
		{
			header: 'Tgl. Masuk', dataIndex: 'tglmasuk',
		//	align: 'center',
			sortable: true, width: 90
		},{
			header: 'Jam Masuk',	
		//	align: 'center', 
			dataIndex: 'jammasuk', sortable: true, width: 90
		},{
			header: 'No. RM', dataIndex: 'norm', 
			sortable: true, width: 90
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true,width: 150
		},{
			header: 'Pekerjaan', dataIndex: 'pekerjaan',
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Penanggung<br>Biaya', dataIndex: 'nmpenjamin',
		//	align: 'left',
			sortable: true, width: 100
		},{
			header: 'Keluhan',dataIndex: 'keluhan', 
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Kelas',dataIndex: 'nmkelastarif', 
		//	align: 'center', 
			sortable: true, width: 70
		},{
			header: 'Kamar',dataIndex: 'kamar', 
		//	align: 'center', 
			sortable: true, width: 70
		},{
			header: 'Dokter Rawat',dataIndex: 'nmdoktergelar', 
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Asal Rujukan',dataIndex: 'nmcarakeluar', 
		//	align: 'center', 
			sortable: true, width: 100
		}]
	});
	
	var cari_data_nya1 = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya',
		height: 400,
		//title: 'Laporan per Mahasiswa',
		plugins: cari_data_nya1,
		ds: ds_regpasinri2,
		cm: cm_bagian,
		sm: new Ext.grid.RowSelectionModel({ singleSelect: true}),
		view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text} ({[values.rs.length]} {["RI"]})',
			enableGroupingMenu: false,	// don't show a grouping menu
			enableNoGroups: false,		// don't let the user ungroup
			hideGroupedColumn: true,	// don't show the column that is being used to create the heading
			showGroupName: false,		// don't show the field name with the group heading
			startCollapsed: false		// the groups start closed/no
        }),
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],	
		clicksToEdit: 1,	//for cell editing (single click =1, dblclick=2)
        forceFit: true, //autoHeight: true, 
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,		
		loadMask: true,
		layout: 'anchor',		
		bbar: [paging],
		
	});
	
	
	
	
	
	
	/* var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_regpasinri2,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
	//	forceFit: true,
		sm: new Ext.grid.RowSelectionModel({ singleSelect: true}),
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text} ({[values.rs.length]} {["RI"]})',
			enableGroupingMenu: false,	// don't show a grouping menu
			enableNoGroups: false,		// don't let the user ungroup
			hideGroupedColumn: true,	// don't show the column that is being used to create the heading
			showGroupName: false,		// don't show the field name with the group heading
			startCollapsed: false		// the groups start closed/no
        }),
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [{
			header: 'No. REG',dataIndex: 'noreg',
		//	align: 'center', 
			sortable: true, width: 90
		},
		{
			header: 'Tgl. Masuk', dataIndex: 'tglmasuk',
		//	align: 'center',
			sortable: true, width: 90
		},{
			header: 'Jam Masuk',	
		//	align: 'center', 
			dataIndex: 'jammasuk', sortable: true, width: 90
		},{
			header: 'No. RM', dataIndex: 'norm', 
			sortable: true, width: 90
		},{
			header: 'Nama Pasien', dataIndex: 'nmpasien',
		//	align: 'center', 
			sortable: true,width: 150
		},{
			header: 'Pekerjaan', dataIndex: 'pekerjaan',
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Penanggung<br>Biaya', dataIndex: 'nmpenjamin',
		//	align: 'left',
			sortable: true, width: 100
		},{
			header: 'Keluhan',dataIndex: 'keluhan', 
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Kelas',dataIndex: 'nmkelastarif', 
		//	align: 'center', 
			sortable: true, width: 70
		},{
			header: 'Kamar',dataIndex: 'kamar', 
		//	align: 'center', 
			sortable: true, width: 70
		},{
			header: 'Dokter Rawat',dataIndex: 'nmdoktergelar', 
		//	align: 'center', 
			sortable: true, width: 100
		},{
			header: 'Asal Rujukan',dataIndex: 'nmcarakeluar', 
		//	align: 'center', 
			sortable: true, width: 100
		}],		
		bbar: paging
	}); */
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Pasien Rawat Inap Per Periode Tanggal Masuk',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
				//	defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
						//	style: 'padding: 10px',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					},{/* 
						xtype: 'compositefield', style: 'margin: 10px 0 0 0',
						items:[{
								xtype: 'tbtext',
								text: 'Ruangan :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
						xtype: 'combo', fieldLabel: '',
						id: 'cb.ruangan', width: 150,
						store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih Ruangan',
						listeners:{
							select:function(combo, records, eOpts){
								cAdvance();
							}
						}
						}]
					 */}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'Pasien Rawat Inap',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
function cAdvance(){
	ds_regpasinri2.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
	ds_regpasinri2.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
	//ds_regpasinri.setBaseParam('bagian',Ext.getCmp('cb.ruangan').getValue());
	ds_regpasinri2.reload();
}
function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
	//	var ruangan     = Ext.getCmp('cb.ruangan').getValue();
	//	alert(ruangan)
		RH.ShowReport(BASE_URL + 'print/Lap_ri_pasienperiode/rawatinap/'
                +tglawal+'/'+tglakhir);
	}	
	
}