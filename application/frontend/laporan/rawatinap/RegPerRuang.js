function RegPerRuang(){
	var ds_vlaprjperiode = dm_vlaprjperiode();
	ds_vlaprjperiode.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlaprjperiode.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	var ds_bagian = dm_bagian();
	ds_bagian.setBaseParam('jpel',1);
	
	var cm_laporan = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header: 'No. Reg',
			dataIndex: 'noreg',
			width: 80
		},{
			header: 'Tgl Masuk',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: 'Jam Masuk',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 70
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 60,
		},{
			header: 'Nama Pasien',
			dataIndex: 'nmpasien',
			width: 130
		},{
			header: 'Pekerjaan',
			dataIndex: 'uobat',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Penanggung Biaya',
			dataIndex: 'upemeriksaan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 130
		},{
			header: 'Keluhan',
			dataIndex: 'utindakan',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Kelas',
			dataIndex: 'uimun',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Kamar',
			dataIndex: 'ulab',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 80
		},{
			header: 'Dokter Rawat',
			dataIndex: 'uracik',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 100
		},{
			header: 'Asal Rujukan Pasien',
			dataIndex: 'ulain',
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
			width: 130
		}
	]);
	var paging_laporan = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_vlaprjperiode,
		displayInfo: true,
		displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var grid_laporan = new Ext.grid.GridPanel({
		ds: ds_vlaprjperiode,
		cm: cm_laporan,
		height: 350,
		autoWidth: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		buttonAlign: 'left',
		layout: 'anchor',
		anchorSize: {
			width: 400,
			height: 400
		},
		bbar: paging_laporan
	});
	
	var lapregperruang = new Ext.FormPanel({
		id: 'fp.pasienrj',
		title: 'Laporan Registrasi Rawat Inap Per Ruang Perawatan',
		autoWidth: true, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 1, labelAlign: 'right'},
				items:[{
					xtype: 'button',
					text: 'Cetak',
					id: 'btn.cetak',
					style: 'padding: 5px',
					width: 100,
					handler: function() {
						cetakLapRJ();
					}
				},{
					xtype: 'button',
					text: 'Cetak Excel',
					id: 'btn.cetakExcel',
					style: 'padding: 5px',
					width: 120,
					handler: function() {
						exportdata();
					}
				}]
			},{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 50, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: 1,
					border: false,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						}]
					},{
						xtype: 'combo', fieldLabel: 'Poli',
						id: 'cb.poli', width: 150,
						store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih Poli',
						listeners:{
							select:function(combo, records, eOpts){
								cAdvance();
								/*if(records.get('idbagian') != 4){
									
								} else {
								
								}*/
							}
						}
					}]
				}]
			},
			grid_laporan
		]
	});
	SET_PAGE_CONTENT(lapregperruang);	
	
	function cAdvance(){
		ds_vlapregperruang.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlapregperruang.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlapregperruang.setBaseParam('bagian',Ext.getCmp('cb.poli').getValue());
		ds_vlapregperruang.reload();
	}
	
	function cetakLapRJ(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var bagian		= Ext.getCmp('cb.poli').getValue();
		if(bagian == '') bagian = 'all';
		RH.ShowReport(BASE_URL + 'print/laprawatinap/lapregperruang/'
                +tglawal+'/'+tglakhir+'/'+bagian);
	}

    function exportdata(){
        var tglawal=Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
        var tglakhir=Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
        var bagian=Ext.getCmp('cb.poli').getValue();
		if(bagian == '') bagian = 'all';

            window.location = BASE_URL + 'print/laprawatinap/exportexcel/'+tglawal+'/'+tglakhir+'/'+bagian;

    }
	
}