function Lap_rekapstok(){
	
	
/* Data Store */

	var ds_rekapstok = dm_rekapstok();
	ds_rekapstok.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_rekapstok.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
	var ds_jkartustok = dm_jkartustok();
	var ds_bagian = dm_bagian();
/* End Data Store */

/* GRID */
	var pageSize = 50;
	var paging = new Ext.PagingToolbar({
	pageSize: pageSize,
		store: ds_rekapstok,
		displayInfo: true,
		displayMsg: 'Data Persediaan Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_rekapstok,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
				//Ext.getCmp('tf.frm.kdbagian').setReadOnly(false);
			}
		}],
		columns: [{
			header: 'Kode Barang',dataIndex: 'kdbrg',
			//align: 'center', 
			sortable: true, width: 80
		},
		{
			header: 'Nama Barang',dataIndex: 'nmbrg',
			//align: 'center', 
			sortable: true, width: 200
		}
		,{
			header: 'SA',dataIndex: 'so',
			align: 'center', 
			sortable: true, width: 70
		},
		{
			header: 'Beli', dataIndex: 'beli',
			align: 'center',
			sortable: true, width: 70
		},{
			header: 'R.Beli',	
			align: 'center', 
			dataIndex: 'returbeli', sortable: true, width: 70
		},{
			header: 'Bonus',	
			align: 'center', 
			dataIndex: 'bonus', sortable: true, width: 70
		},{
			header: 'TBL',	
			align: 'center', 
			dataIndex: 'tbl', sortable: true, width: 70
		},{
			header: 'D.KB',	
			align: 'center', 
			dataIndex: 'pbb', sortable: true, width: 70
		},{
			header: 'D.MB',	
			align: 'center', 
			dataIndex: 'terimabarang', sortable: true, width: 70
		},{
			header: 'R.KBG',	
			align: 'center', 
			dataIndex: 'returpbb', sortable: true, width: 70
		},{
			header: 'R.MBG',	
			align: 'center', 
			dataIndex: 'returpnbb', sortable: true, width: 70
		},{
			header: 'BHP',	
			align: 'center', 
			dataIndex: 'bhp', sortable: true, width: 70
		},{
			header: 'Jual',	
			align: 'center', 
			dataIndex: 'pengfarmasi', sortable: true, width: 70
		},{
			header: 'R.Jual',	
			align: 'center', 
			dataIndex: 'returfarmasi', sortable: true, width: 70
		},{
			header: 'Sisa',	
			align: 'center', 
			dataIndex: 'sisa', sortable: true, width: 70
		},{
			header: 'NBeli',	
			align: 'center', 
			dataIndex: 'nbeli', sortable: true, width: 100
		},{
			header: 'NJual',	
			align: 'center', 
			dataIndex: 'njual', sortable: true, width: 100
		}],		
		bbar: paging
	});
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Rekapituasi Stok Per Jenis',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
						//	style: 'padding: 10px',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					},{
						xtype: 'compositefield', style: 'margin: 10px 0 0 0',
						items:[{
								xtype: 'tbtext',
								text: 'Bagian :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
						xtype: 'combo', fieldLabel: '',
						id: 'cb.ruangan', width: 200,
						store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih...',
						listeners:{
							select:function(combo, records, eOpts){
								cAdvance();
							}
						}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'Rekapitulasi',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
function cAdvance(){
	ds_rekapstok.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
	ds_rekapstok.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
	ds_rekapstok.setBaseParam('ruangan',Ext.getCmp('cb.ruangan').getValue());
	ds_rekapstok.reload();
}
function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		var bagian 		= Ext.getCmp('cb.ruangan').getValue();
		RH.ShowReport(BASE_URL + 'print/Lap_persediaan_rekapstok/pasienkeluar/'
                +tglawal+'/'+tglakhir+'/'+bagian);
	}	

}