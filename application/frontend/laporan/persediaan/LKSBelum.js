function LKSBelum(){
	var pageSize = 50;
	var ds_vlapksbelum = dm_vlapksbelum();
	var ds_vlapksbelumrj = dm_vlapksbelumrj();
	ds_vlapksbelumrj.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_vlapksbelumrj.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	var ds_bagian = dm_bagian();
	ds_bagian.setBaseParam('jpel',1);
	
	var paging_ri = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_vlapksbelum,
		displayInfo: true,
		displayMsg: 'Data Daftar Pelayanan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});

	var grid_ri = new Ext.grid.GridPanel({
		id: 'grid_ri',
		store: ds_vlapksbelum,		
		autoScroll: true,
		columnLines: true,
		height: 230,
		tbar: [],
		frame: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">No. Registrasi</div>',
			dataIndex: 'noreg',
			width:100
		},{
			header: '<div style="text-align:center;">Tgl. Registrasi</div>',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 100
		},{
			header: '<div style="text-align:center;">No. RM</div>',
			dataIndex: 'norm', align: 'center',
			width: 60
		},{
			header: '<div style="text-align:center;">Pasien</div>',
			dataIndex: 'nmpasien',
			width: 200
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width:250
		},{
			header: '<div style="text-align:center;">Unit</div>',
			dataIndex: 'nmbagian',
			width: 100
		},{
			header: '<div style="text-align:center;">Bagian</div>',
			dataIndex: 'nmjnspelayanan',
			width: 120
		}],
		bbar: paging_ri
	});
	
	/* var paging_rj = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_vlapksbelumrj,
		displayInfo: true,
		displayMsg: 'Data Daftar Pelayanan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	}); */
	
	var grid_rj = new Ext.grid.EditorGridPanel({
		id: 'gp.grid_rj',
		store: ds_vlapksbelumrj,
		autoScroll: true,
		columnLines: true,
		height: 260, 
		forceFit: true,		
        loadMask: true,
		frame: true,
		tbar:[{
			xtype: 'compositefield', width: 300, style: 'marginLeft: 5px',
			items: [{
				xtype: 'label', id: 'lb.per', text: 'Periode :', style: 'marginTop: 3px;marginRight: 15px',
			},{
				xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
				width: 100, value: new Date(),
				format: 'd-m-Y',
				listeners:{
					select: function(field, newValue){
						cAdvance();
					},
					change : function(field, newValue){
						cAdvance();
					}
				}
			},{
				xtype: 'label', id: 'lb.sd', text: 's/d', style: 'marginTop: 3px',
			},{
				xtype: 'datefield', id: 'df.tglakhir',
				width: 100, value: new Date(),
				format: 'd-m-Y',
				listeners:{
					select: function(field, newValue){
						cAdvance();
					},
					change : function(field, newValue){
						cAdvance();
					}
				}
			}]
		},{
			xtype:'checkbox',  
			id: 'cbxpoli',
			style: 'marginTop: -1px',
			listeners: {
				check : function(cb, value) {					
					cAdvance();
				}
			}
		},{
			xtype: 'label', id: 'lb.d', text: 'Poli :', style: 'marginTop: 3px;marginRight: 5px;',
		},{
			xtype: 'combo', 
			id: 'cb.poli', width: 150,
			store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
			editable: false, triggerAction: 'all',
			forceSelection: true, submitValue: true, mode: 'local',
			emptyText:'Pilih Poli',
			listeners:{
				select:function(combo, records, eOpts){
					cAdvance();
				}
			}
		},{
			xtype:'checkbox',  
			id: 'cbxugd',
			style: 'marginTop: -1px;marginLeft: 15px',
			listeners: {
				check : function(cb, value) {					
					cAdvance();
				}
			}
		},{
			xtype: 'label', id: 'lb.ugd', text: 'UGD', style: 'marginTop: 3px;marginRight: 5px;',
		}],
		columns: [new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">No. Registrasi</div>',
			dataIndex: 'noreg',
			width:100
		},{
			header: '<div style="text-align:center;">Tgl. Registrasi</div>',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 100
		},{
			header: '<div style="text-align:center;">No. RM</div>',
			dataIndex: 'norm', align: 'center',
			width: 60
		},{
			header: '<div style="text-align:center;">Pasien</div>',
			dataIndex: 'nmpasien',
			width: 200
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width:250
		},{
			header: '<div style="text-align:center;">Unit</div>',
			dataIndex: 'nmbagian',
			width: 100
		},{
			header: '<div style="text-align:center;">Bagian</div>',
			dataIndex: 'nmjnspelayanan',
			width: 120
		}],		
		//bbar: paging_rj
	});	
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Kartu Stok(Belum)', iconCls:'silk-user',
		width: 900, Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		//defaults: { labelWidth: 150, labelAlign: 'right', width: 910, },
		items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 1,
			//defaults: { labelWidth: 87, labelAlign: 'right'},
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Daftar pasien yang belum cetak kuitansi(RI)',
				layout: 'form',
				height: 260,
				boxMaxHeight:275,
				items: [grid_ri]
			}]
		},
		{
			xtype: 'container',
			style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			items: [{
				xtype: 'fieldset', 
				title: 'Daftar pasien yang belum cetak kuitansi(RJ/UGD)',
				layout: 'form',
				height: 290,
				boxMaxHeight:305,
				items: [grid_rj]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
	
	function cAdvance(){
		if(Ext.getCmp('cbxpoli').getValue() == false){
			ds_vlapksbelumrj.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
			ds_vlapksbelumrj.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
			ds_vlapksbelumrj.setBaseParam('idbagian',null);
			ds_vlapksbelumrj.reload();
		}else if(Ext.getCmp('cbxpoli').getValue() == true){
			ds_vlapksbelumrj.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
			ds_vlapksbelumrj.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
			ds_vlapksbelumrj.setBaseParam('idbagian',Ext.getCmp('cb.poli').getValue());
			ds_vlapksbelumrj.reload();	
		}
		
		if(Ext.getCmp('cbxugd').getValue() == false){
			ds_vlapksbelumrj.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
			ds_vlapksbelumrj.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
			ds_vlapksbelumrj.setBaseParam('idbagian',null);
			Ext.getCmp('cb.poli').enable();
		}else if(Ext.getCmp('cbxugd').getValue() == true){
			ds_vlapksbelumrj.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
			ds_vlapksbelumrj.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
			ds_vlapksbelumrj.setBaseParam('idbagian',12);
			ds_vlapksbelumrj.reload();	
			Ext.getCmp('cb.poli').disable();
			Ext.getCmp('cb.poli').setValue();
		}
				
		/* ds_vlapksbelumrj.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vlapksbelumrj.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vlapksbelumrj.setBaseParam('idbagian',Ext.getCmp('cb.poli').getValue());
		ds_vlapksbelumrj.reload(); */
	}
}