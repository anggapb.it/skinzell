function Lap_returfarmasipl(){
	Ext.form.Field.prototype.msgTarget = 'side';
	
	var ds_lapreturfarmasipl = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'lapreturfarmasi_controller/get_lapreturfarmasi_pl',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "noreturfarmasi",
			mapping: "noreturfarmasi"
		},{
			name: "tglreturfarmasi",
			mapping: "tglreturfarmasi"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		},{
			name: "noreg",
			mapping: "noreg"
		},{
			name: "norm",
			mapping: "norm"
		},{
			name: "atasnama",
			mapping: "atasnama"
		},{
			name: "total",
			mapping: "returfarmasi_detail.total"
		},]
	});
	
	var vw_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_lapreturfarmasipl',
		store: ds_lapreturfarmasipl,
		view: vw_nya,	
		frame: true,
		autoScroll: true,
		autoWidth: true,
		columnLines: true,
		autoScroll: true,
		loadMask: true,
		height: 445,
		tbar: [{
			text: 'Cetak PDF',
			id: 'btn.cetak',	
			iconCls:'silk-printer',		
			handler: function() {
				cetak();
			}
		},'-',
		{ 	text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',	
			handler: function(){
				cetakexcel();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. Retur<br>Farmasi Pasien Luar'),
			width: 150,
			dataIndex: 'noreturfarmasi',
			sortable: true,
			align:'center'
		},
		{
			header: headerGerid('Tgl Retur<br>Farmasi Pasien Luar'),
			width: 150,
			dataIndex: 'tglreturfarmasi',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Penerima'),
			width: 150,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('No. Nota'),
			width: 100,
			dataIndex: 'noreg',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('No. RM'),
			width: 100,
			dataIndex: 'norm',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Nama Pasien'),
			width: 200,
			dataIndex: 'atasnama',
			sortable: true,
		},{
			header: headerGerid('Total'),
			width: 200,
			dataIndex: 'total',
			sortable: true,
			align:'right',
		}]
	});
       
	var form_bp_general = new Ext.FormPanel({
		id: 'form_bp_general',
		title: 'Laporan Retur Farmasi Pasien Luar', iconCls:'silk-calendar',
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		items: [
		{
			layout: 'form',
			border: false,
			items: [{
				xtype: 'container',
				style: 'padding: 5px',
				layout: 'column',
				defaults: {labelWidth: 1, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					columnWidth: .99,
					border: false,
					items: [{
						xtype: 'compositefield',
						style: 'padding: 5px; marginLeft: -5px; marginBottom: -6px',
						width: 470,
						items: [{
							xtype: 'label', id: 'lb.bgna', text: 'Periode :', margins: '3 10 0 5',
						},{
							xtype: 'datefield',
							id: 'df.tglawal',
							width: 110,
							value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cektgl();
								}
								//,change : function(field, newValue){
								//	cektgl();
								//}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: ' s.d ', margins: '3 10 0 5',
						},{
							xtype: 'datefield',
							id: 'df.tglakhir',
							width: 110,
							value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cektgl();
								}
								//,change : function(field, newValue){
								//	cektgl();
								//}
							}
						}]
					}]
				}]
			},
			grid_nya
			]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadLapreturfarmasi(){
		ds_lapreturfarmasipl.reload();
	}
	
	function cektgl(){
		ds_lapreturfarmasipl.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_lapreturfarmasipl.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		reloadLapreturfarmasi();
	}
	
	function cetak(){
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		RH.ShowReport(BASE_URL + 'print/lap_retur_farmasi/laporan_retur_farmasi_pl/'
                +tglawal+'/'+tglakhir);
	}
	
	function cetakexcel(){
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		RH.ShowReport(BASE_URL + 'print/lap_retur_farmasi/excel_retur_farmasi_pl/'
                +tglawal+'/'+tglakhir);
	}

}