function Lap_poperbrg(){
Ext.form.Field.prototype.msgTarget = 'side';
	var ds_lappoperbrg = dm_lappoperbrg();
	ds_lappoperbrg.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_lappoperbrg.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	
	var vw_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_poperbrg',
		store: ds_lappoperbrg,
		view: vw_nya,	
		frame: true,		
		autoScroll: true,
		height: 465,
		columnLines: true,
		tbar: [{
			text: 'Cetak',
			id: 'btn.cetak',	
			iconCls:'silk-printer',		
			//disabled: true,
			handler: function() {
				cetak();
			}
		},'-',
		{ 	text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',	
			//disabled: true,	
			handler: function(){
				cetakexcel();
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No.<br>Pembelian'),
			width: 85,
			dataIndex: 'nopo',
			sortable: true,
			align:'center'
		},{
			header: headerGerid('Tgl.<br>Pembelian'),
			width: 85,
			dataIndex: 'tglpo',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Kode Barang'),
			width: 85,
			dataIndex: 'kdbrg',
			sortable: true,
		},{
			header: headerGerid('Nama Barang'),
			width: 175,
			dataIndex: 'nmbrg',
			sortable: true,
		},{
			header: headerGerid('Satuan'),
			width: 80,
			dataIndex: 'nmsatuan',
			sortable: true,
			align:'center'
		},{
			header: headerGerid('Qty'),
			width: 55,
			dataIndex: 'qty',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Qty<br>Bonus'),
			width: 50,
			dataIndex: 'qtybonus',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Harga Beli'),
			width: 88,
			dataIndex: 'hargabeli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Diskon'),
			width: 55,
			dataIndex: 'diskon',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('JUM-HRG+Disk'),
			width: 90,
			dataIndex: 'jmlhrgtmbhdiskon',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('PPN'),
			width: 39,
			dataIndex: 'ppn',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('JUM-BRG+PPN'),
			width: 90,
			dataIndex: 'jmlhrgtmbhppn',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Supplier'),
			width: 165,
			dataIndex: 'nmsupplier',
			sortable: true,
		},{
			header: headerGerid('Keterangan'),
			width: 100,
			dataIndex: 'stlunas',
			sortable: true,
		}]
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Laporan Pembuatan PO per Barang', iconCls:'silk-calendar',
		layout: 'fit',
		frame: true,
		items: [
		{
			xtype: 'panel',
			border: false,
			autoScroll: true,
			items: [{
				layout: 'form',
				border: false,
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .99,
						border: false,
						items: [{
							xtype: 'compositefield',
							style: 'padding: 5px; marginLeft: 10px; marginBottom: -6px',
							width: 470,
							items: [{
								xtype: 'label', id: 'lb.bgna', text: 'Periode :', margins: '3 10 0 5',
							},{
								xtype: 'datefield',
								id: 'df.tglawal',
								width: 110,
								value: new Date(),
								format: 'd-m-Y',
								listeners:{
									select: function(field, newValue){
										cektgl();
									},
									change : function(field, newValue){
										cektgl();
									}
								}
							},{
								xtype: 'label', id: 'lb.sd', text: ' s.d ', margins: '3 10 0 5',
							},{
								xtype: 'datefield',
								id: 'df.tglakhir',
								width: 110,
								value: new Date(),
								format: 'd-m-Y',
								listeners:{
									select: function(field, newValue){
										cektgl();
									},
									change : function(field, newValue){
										cektgl();
									}
								}
							}]
						}]
					}]
				},{
					xtype : 'fieldset',
					title: '',
					items: [grid_nya]
				}]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadBrgbagian(){
		ds_lappoperbrg.reload();
	}
		
	function cektgl(){
		ds_lappoperbrg.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_lappoperbrg.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_lappoperbrg.reload();
	}
	
	function cetak(){
		/* var idbagian = Ext.getCmp('tf.idbagian').getValue();			
		RH.ShowReport(BASE_URL + 'print/lap_persediaan_daftarbrg/laporan_daftarbrg/' + idbagian); */
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		RH.ShowReport(BASE_URL + 'print/lap_persediaan_poperbrg/laporan_poperbrg/'
                +tglawal+'/'+tglakhir);
	}
	
	function cetakexcel(){
		/* var idbagian = Ext.getCmp('tf.idbagian').getValue();			
		RH.ShowReport(BASE_URL + 'print/lap_persediaan_daftarbrg/laporan_daftarbrg/' + idbagian); */
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		
		window.location =(BASE_URL + 'print/lap_persediaan_poperbrg/excelpoperbrg/'
                +tglawal+'/'+tglakhir);
	}

	/**
	WIN - FORM ENTRY/EDIT 
	*/
}