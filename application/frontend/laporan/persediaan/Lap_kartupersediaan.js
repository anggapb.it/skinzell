function Lap_kartupersediaan(){
	var ds_kartupersediaan = dm_kartupersediaan();
	ds_kartupersediaan.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_kartupersediaan.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));

	var paging = new Ext.PagingToolbar({
		pageSize: 50,
		store: ds_kartupersediaan,
		displayInfo: true,
		displayMsg: 'Data Persediaan Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya',
		store: ds_kartupersediaan,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar: [{
			text: 'Cetak PDF',
			id: 'cetak',
			iconCls: 'silk-printer',
			handler: function() {
				cetakLap();
			}
		},'-',
		{ 	text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',
			handler: function(){
				cetakexcel();
			}
		}],
		columns: [{
			header: 'Nama',
			dataIndex: 'nmbrg',
			width: 160,
			sortable: true,
		},{
			header: 'Nomor',
			dataIndex: 'noref',
			width: 90,
			sortable: true,
		},{
			header: 'Tanggal',
			dataIndex: 'tglkartustok',
			width: 80,
			sortable: true,
		},{
			header: 'Jam',
			dataIndex: 'jamkartustok',
			width: 70,
			sortable: true,
		},{
			header: 'Saldo<br/>Awal',
			dataIndex: 'saldoawal',
			width: 60,
			sortable: true,align:'right'
		},{
			header: 'Jumlah<br/>Masuk',
			dataIndex: 'jmlmasuk',
			width: 60,
			sortable: true,align:'right'
		},{
			header: 'RBM',
			dataIndex: 'rbm',
			width: 60,
			sortable: true,align:'right'
		},{
			header: 'Jumlah<br/>Keluar',
			dataIndex: 'jmlkeluar',
			width: 60,
			sortable: true,align:'right'
		},{
			header: 'RBK',
			dataIndex: 'rbk',
			width: 60,
			sortable: true,align:'right'
		},{
			header: 'Saldo<br/>Akhir',
			dataIndex: 'saldoakhir',
			width: 60,
			sortable: true,align:'right'
		},{
			header: 'Harga Beli',
			dataIndex: 'hrgbeli',
			width: 90,
			sortable: true,align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: 'Harga Jual',
			dataIndex: 'hrgjual',
			width: 90,
			sortable: true,align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: 'Nilai Beli',
			dataIndex: 'nbeli',
			width: 100,
			sortable: true,align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: 'Nilai Jual',
			dataIndex: 'njual',
			width: 100,
			sortable: true,align:'right',
			xtype: 'numbercolumn', format:'0,000.00'
		}],		
		bbar: paging
	});
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Kartu Persediaan',
			autoScroll: true,
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
								xtype: 'tbtext',
								text: 'Tanggal :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
							width: 100,
							hidden: true,
							handler: function() {
								cetakLap();
							}
						}]
					}/* ,{
						xtype: 'compositefield', style: 'margin: 10px 0 0 0',
						items:[{
								xtype: 'tbtext',
								text: 'Bagian :',
								width: 100,
								margins: {top:3, right:0, bottom:0, left:0}
							},{
						xtype: 'combo', fieldLabel: '',
						id: 'cb.ruangan', width: 200,
						store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih...',
						listeners:{
							select:function(combo, records, eOpts){
								cAdvance();
							}
						}
						}]
					} */]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'Kartu Persediaan',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
	function cAdvance(){
		ds_kartupersediaan.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
		ds_kartupersediaan.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
		ds_kartupersediaan.reload();
	}
	
	function cetakLap(){
			var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
			var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
			RH.ShowReport(BASE_URL + 'print/lap_persediaan_kartu/persediaan/'
					+tglawal+'/'+tglakhir);
		}	
	
	function cetakexcel(){	
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir	= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		
		window.location =(BASE_URL + 'print/lap_persediaan_kartu/excelkpersediaan/'
                +tglawal+'/'+tglakhir);
	}

}