function Lap_stokpersediaan(){
Ext.form.Field.prototype.msgTarget = 'side';
	var ds_lapkartustok = dm_lap_kartustok();
	ds_lapkartustok.setBaseParam('tglawal',new Date().format('Y-m-d'));
	ds_lapkartustok.setBaseParam('tglakhir',new Date().format('Y-m-d'));
	
	var ds_pengbagian = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'lappengeluaranbrg_controller/get_pengbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_pengbagian.setBaseParam('userid', USERID);	
	ds_pengbagian.reload({
		scope   : this,
		callback: function(records, operation, success) {
			var idbagian = '';
			var nmbagian = '';

			 ds_pengbagian.each(function (rec) { 
					idbagian = rec.get('idbagian');
					nmbagian = rec.get('nmbagian');
				});                          
			Ext.getCmp("tf.bagian").setValue(nmbagian);			
			Ext.getCmp("tf.idbagian").setValue(idbagian);	
		}
	});	
	
	var ds_bagianpeng = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'returbrgbagian_controller/get_pengbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "idjnspelayanan",
			mapping: "idjnspelayanan"
		},{
			name: "nmjnspelayanan",
			mapping: "nmjnspelayanan"
		},{
			name: "idbdgrawat",
			mapping: "idbdgrawat"
		},{
			name: "nmbdgrawat",
			mapping: "nmbdgrawat"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_bagianpeng.setBaseParam('userid', USERID);
	
	var vw_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_lapstokpersediaan',
		store: ds_lapkartustok,
		view: vw_nya,	
		frame: true,		
		autoScroll: true,
		height: 445,
		columnLines: true,
		tbar: [{
			text: 'Cetak',
			id: 'btn.cetak',
			iconCls:'silk-printer',
			//disabled: true,
			handler: function() {
				cetak();
			}
		},'-',{ 
			xtype: 'button',
			text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',
			//hidden : true,					
			//disabled: true,	
			handler: function(){
				cetakexcel();
			}
		},{
			xtype: 'textfield',
			id: 'tf.idbagian',
			width: '50',
			hidden: true,
			validator: function(){
				ds_lapkartustok.reload({
					params: { 
						idbagian: Ext.getCmp('tf.idbagian').getValue()
					}
				});
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('Kode Barang'),
			width: 85,
			dataIndex: 'kdbrg',
			sortable: true,
			align:'center'
		},{
			header: headerGerid('Nama Barang'),
			width: 170,
			dataIndex: 'nmbrg',
			sortable: true,
		},{
			header: headerGerid('SA'),
			width: 100,
			dataIndex: 'sa',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Beli'),
			width: 100,
			dataIndex: 'beli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('R.Beli'),
			width: 100,
			dataIndex: 'jmlretbeli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('TBL'),
			width: 100,
			dataIndex: 'jmltbl',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('D.KB'),
			width: 100,
			dataIndex: 'jmldistribusibrg',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('D.MB'),
			width: 100,
			dataIndex: 'jmldistribusimasukbrg',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('R.KBG'),
			width: 100,
			dataIndex: 'jmlrkbb',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('R.MBG'),
			width: 100,
			dataIndex: 'jmlrmbg',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('BHP'),
			width: 100,
			dataIndex: 'jmlbhp',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Jual'),
			width: 100,
			dataIndex: 'jmljualbrg',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('R.Jual'),
			width: 100,
			dataIndex: 'jmlretjualbrg',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Sisa'),
			width: 100,
			dataIndex: 'sisa',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},{
			header: headerGerid('Harga Beli'),
			width: 100,
			dataIndex: 'hrgbeli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Harga Jual'),
			width: 100,
			dataIndex: 'hrgjual',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('N. Beli'),
			width: 100,
			dataIndex: 'nbli',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('N. Jual'),
			width: 100,
			dataIndex: 'njual',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		}]
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Laporan Inventory', iconCls:'silk-calendar',
		layout: 'fit',
		frame: true,
		items: [
		{
			xtype: 'panel',
			border: false,
			autoScroll: true,
			items: [{
				layout: 'form',
				border: false,
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .99,
						border: false,
						items: [{
							xtype: 'compositefield',
							style: 'padding: 5px; marginLeft: 10px; marginBottom: -6px',
							width: 470,
							items: [{
								xtype: 'label', id: 'lb.bgna', text: 'Periode :', margins: '3 10 0 5',
							},{
								xtype: 'datefield',
								id: 'df.tglawal',
								width: 110,
								value: new Date(),
								format: 'd-m-Y',
								listeners:{
									select: function(field, newValue){
										cektgl();
									},
									change : function(field, newValue){
										cektgl();
									}
								}
							},{
								xtype: 'label', id: 'lb.sd', text: ' s.d ', margins: '3 10 0 5',
							},{
								xtype: 'datefield',
								id: 'df.tglakhir',
								width: 110,
								value: new Date(),
								format: 'd-m-Y',
								listeners:{
									select: function(field, newValue){
										cektgl();
									},
									change : function(field, newValue){
										cektgl();
									}
								}
							}]
						},{
							xtype: 'compositefield',
							style: 'padding: 5px; marginLeft: 10px; marginBottom: -6px',
							width: 470,
							items: [{
								xtype: 'label', id: 'lb.bgn', text: 'Bagian :', margins: '3 10 0 11',
							},{
								xtype: 'textfield',
								id: 'tf.bagian',
								width: 255,
								readOnly: true,
							},
							{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn_data_pen_bgn',
								width: 3,
								handler: function() {
									fnwBagian();
									//ds_lapkartustok.reload();
								}
							}/* ,{
								xtype: 'button', text: 'Cetak', iconCls:'silk-printer', id: 'btn.cetak', style: 'marginLeft: 22px',
								disabled: true, handler: function() {
									cetak();
								}
							} */]
						}]
					}]
				},{
					xtype : 'fieldset',
					title: '',
					items: [grid_nya]
				}]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function cektgl(){
		ds_lapkartustok.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_lapkartustok.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_lapkartustok.reload({
			params: { 
				idbagian: Ext.getCmp('tf.idbagian').getValue()
			}
		});
		//Ext.getCmp('grid_lapstokpersediaan').store.reload();
	}
	
	function cetak(){
		
		/* var parsing = '';
			parsing = parsing + Ext.getCmp('df.tglawal').getValue().format('Y-m-d') + '-' ;
			parsing = parsing + Ext.getCmp('df.tglakhir').getValue().format('Y-m-d') + '-' ;
			parsing = parsing + Ext.getCmp('tf.idbagian').getValue() + '-' ;
			parsing = parsing + Ext.getCmp('tf.bagian').getValue() + '-' ; */
			
		/* var win = window.open();
		win.location.reload();
		win.location = BASE_URL + 'print/lap_persediaan_pengeluaranbrg/laporan_pengeluaranbrg/'+parsing; */
		
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var idbagian	= Ext.getCmp('tf.idbagian').getValue();
		var nmbagian	= Ext.getCmp('tf.bagian').getValue();
		
		RH.ShowReport(BASE_URL + 'print/Lap_stok_persediaan/get_lap_stok_persediaan/'
                +tglawal+'/'+tglakhir+'/'+idbagian+'/'+nmbagian);
	}
	
	function cetakexcel(){
		var tglawal		= Ext.getCmp('df.tglawal').getValue().format('Y-m-d');
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var idbagian	= Ext.getCmp('tf.idbagian').getValue();
		
		window.location =(BASE_URL + 'print/Lap_stok_persediaan/laporan_excelinventory/'
                +tglawal+'/'+tglakhir+'/'+idbagian);
	}

	/**
	WIN - FORM ENTRY/EDIT 
	*/
	
	function fnwBagian(){
		var ds_brgbagian1 = dm_brgbagian1();
		ds_brgbagian1.setBaseParam('idbagian', 'null');
		ds_brgbagian1.reload();
		ds_bagianpeng.reload();
		
		function fnkeyAddBagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bagian = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200,
				renderer: fnkeyAddBagian
			},{
				header: 'Jenis Pelayanan',
				dataIndex: 'nmjnspelayanan',
				width: 150
			},{
				header: 'Bidang Perawatan',
				dataIndex: 'nmbdgrawat',
				width: 220
			}
		]);
		
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagianpeng,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bagian = new Ext.grid.GridPanel({
			ds: ds_bagianpeng,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 620,
			plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbagian = record.data["idbagian"];
					var var_cari_bagian = record.data["nmbagian"];
								
					//Ext.getCmp('tf.bagian').focus();
					Ext.getCmp("tf.bagian").setValue(var_cari_bagian);
					Ext.getCmp('tf.idbagian').setValue(var_cari_idbagian);
					//ds_lapkartustok.reload();
								win_find_cari_bagian.close();
				return true;
			}
			return true;
		}
	}
}