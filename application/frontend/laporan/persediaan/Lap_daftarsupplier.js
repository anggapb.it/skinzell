function Lap_daftarsupplier(){
Ext.form.Field.prototype.msgTarget = 'side';
	var ds_supplier = dm_pisupplier();
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_supplier',
		store: ds_supplier,		
		autoScroll: true,
		height: 485, //autoHeight: true,
		columnLines: true,
		//plugins: cari_data,
		border: true,
		//sm: sm_nya,
		tbar: [{
			text: 'Cetak',
			iconCls:'silk-printer',
			id: 'btn.cetak',
			//style: 'marginLeft: 22px; marginTop: 5px; marginBottom: 5px;',
			handler: function() {
				cetak();
			}
		},'-',
		{ 	text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',
			//disabled: true,
			handler: function(){
				cetakexcel();
			}
		}],
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 60,
			dataIndex: 'kdsupplier',
			sortable: true
		},{
			header: 'Tanggal Daftar',
			width: 100,
			dataIndex: 'tgldaftar',
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},
		{
			header: 'Nama',
			width: 200,
			dataIndex: 'nmsupplier',
			sortable: true
		},{
			header: 'Alamat',
			width: 200,
			dataIndex: 'alamat',
			sortable: true
		},{
			header: 'No Telephon',
			width: 100,
			dataIndex: 'notelp',
			sortable: true
		},
		{
			header: 'No Fax',
			width: 100,
			dataIndex: 'nofax',
			sortable: true
		},
		{
			header: 'Email',
			width: 100,
			dataIndex: 'email',
			sortable: true
		},{
			header: 'Web Site',
			width: 100,
			dataIndex: 'website',
			sortable: true
		},{
			header: 'Kontak Person',
			width: 100,
			dataIndex: 'kontakperson',
			sortable: true
		},		
		{
			header: 'No Hp',
			width: 100,
			dataIndex: 'nohp',
			sortable: true
		},{
			header: 'NpWp',
			width: 100,
			dataIndex: 'npwp',
			sortable: true
		},{
			header: 'Bank',
			width: 100,
			dataIndex: 'nmbank',
			sortable: true
		},
		{
			header: 'No. Rek',
			width: 100,
			dataIndex: 'norek',
			sortable: true
		},{
			header: 'Atas Nama',
			width: 200,
			dataIndex: 'atasnama',
			sortable: true
		},
		{
			header: 'Keterangan',
			width: 150,
			dataIndex: 'keterangan',
			sortable: true
		},{
			header: 'Status',
			width: 80,
			dataIndex: 'nmstatus',
			sortable: true
		}],
		listeners: {
			rowclick: function rowClick(grid, rowIdx) {

			}
		}
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Daftar Supplier', iconCls:'silk-user',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			autoScroll: true,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadSupplier(){
		ds_supplier.reload();
	}
	
	function cetak(){
		var sup = 'Daftar Supplier';
		RH.ShowReport(BASE_URL + 'print/lap_persediaan_daftarsupplier/laporan_daftarsupplier/'+sup);
	}	
	
	function cetakexcel(){	
		window.location =(BASE_URL + 'print/lap_persediaan_daftarsupplier/exceldaftarsupplier/');
	}
}
