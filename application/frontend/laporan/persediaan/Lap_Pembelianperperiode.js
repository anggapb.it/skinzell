function Lap_Pembelianperperiode(){
	
	
/* Data Store */

	var ds_lappersedian = dm_lappersedian();
	var ds_lappersediandet = dm_lappersediandet();
	ds_lappersediandet.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_lappersediandet.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
/* End Data Store */

/* GRID */
	var pageSize = 50;
	var paging = new Ext.PagingToolbar({
	pageSize: pageSize,
		store: ds_lappersediandet,
		displayInfo: true,
		displayMsg: 'Data PO Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var grid_nya = new Ext.grid.GridPanel({
		id: 'gridnya', //sm: cbGrid, 
		store: ds_lappersediandet,
		frame: true,
		height: 400,
		bodyStyle: 'padding:3px 3px 3px 3px',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		columns: [{
			header: 'No. Pembelian',dataIndex: 'nopo',
			//align: 'center', 
			sortable: true, width: 84
		},
		{
			header: 'Tgl. Pembelian',dataIndex: 'tglpo',
		//	align: 'center', 
			sortable: true, width: 100
		},
		{
			header: 'Nama Barang', dataIndex: 'nmbrg',
		//	align: 'center',
			sortable: true, width: 180
		},{
			header: 'Satuan',	
		//	align: 'center', 
			dataIndex: 'nmsatuan', sortable: true, width: 100
		},{
			header: 'Qty', dataIndex: 'qty',
			align: 'center', 
			sortable: true,width: 74
		},{
			header: 'Qty Bonus', dataIndex: 'qtybonus',
			align: 'center', 
			sortable: true, width: 74
		},{
			header: 'Harga Beli', dataIndex: 'hargabeli',
			align: 'right',
			sortable: true, width: 100,
			xtype: 'numbercolumn', format:'0,000',
		},{
			header: 'Supplier',dataIndex: 'nmsupplier', 
		//	align: 'center', 
			sortable: true, width: 238
		}],		
		bbar: paging
	});
	/* END GRID */
	/* Daftar PO */
		var po_form = new Ext.form.FormPanel({
			id: 'fp.po',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Daftar Pembelian Barang',
			autoScroll: true,
			
			/* Container */
			items: [{
			//Start Filter
				xtype: 'fieldset',
				title: 'Filter',
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'datefield', fieldLabel:'Periode', id: 'df.tglawal',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'label', id: 'lb.sd', text: 's/d'
						},{
							xtype: 'datefield', id: 'df.tglakhir',
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
						},{
							xtype: 'button',
							text: 'Cetak',
							id: 'btn.cetak',
						//	style: 'padding: 10px',
							width: 100,
							handler: function() {
								cetakLap();
							}
						},{
							xtype: 'button',
							text: 'Cetak Excel',
							id: 'btn.cetakexcel',
						//	style: 'padding: 10px',
							width: 100,
							handler: function() {
								cetakexcel();
							}
						}]
					}]
				}]
			}, /* end Filter */
			{
				xtype: 'fieldset',
				title: 'Daftar Pembelian Barang',
				items:[grid_nya]
			}]

			/* End Daftar PO */

		}); SET_PAGE_CONTENT(po_form);
	/* End Form */
	
function cAdvance(){
	ds_lappersediandet.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d'));
	ds_lappersediandet.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d'));
	ds_lappersediandet.reload();
}
function cetakLap(){
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir		= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		RH.ShowReport(BASE_URL + 'print/lappembelian_perperiode/laporan_perperiode/'
                +tglawal+'/'+tglakhir);
	}
function cetakexcel(){
		/* var idbagian = Ext.getCmp('tf.idbagian').getValue();			
		RH.ShowReport(BASE_URL + 'print/lap_persediaan_daftarbrg/laporan_daftarbrg/' + idbagian); */
		
		var tglawal		= Ext.util.Format.date(Ext.getCmp('df.tglawal').getValue(), 'Y-m-d');
		var tglakhir		= Ext.util.Format.date(Ext.getCmp('df.tglakhir').getValue(), 'Y-m-d');
		/* var idcombo = Ext.getCmp('cb.search').getValue();
		var nmcombo = Ext.getCmp('cek').getValue(); */
		
		window.location =(BASE_URL + 'print/lappembelian_perperiode/excelpoperperiode/'
                +tglawal+'/'+tglakhir);
	}	
function fnSearchgrid(){
ds_po.setBaseParam('chb_nopo', Ext.getCmp('chb.nopo').getValue());
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.cari').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
	//	alert(nmcombo);
			ds_po.setBaseParam('key',  '1');
			ds_po.setBaseParam('id',  idcombo);
			ds_po.setBaseParam('name',  nmcombo);
		ds_po.load();
	}
	
}