function Lap_kartustok(){
	
	var	ds_barangkartustok = dm_barangkartustok();
	var ds_kartustok = dm_kartustok();
	ds_kartustok.setBaseParam('kdbrg',null);
	var ds_bagian = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kartustok_controller/get_bagian_krtstok',
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'idlvlbagian',
				mapping: 'idlvlbagian'
			},{
				name: 'nmlvlbagian',
				mapping: 'nmlvlbagian'
			},{
				name: 'idjnshirarki',
				mapping: 'idjnshirarki'
			},{
				name: 'nmjnshirarki',
				mapping: 'nmjnshirarki'
			},{
				name: 'idjnspelayanan',
				mapping: 'idjnspelayanan'
			},{
				name: 'nmjnspelayanan',
				mapping: 'nmjnspelayanan'
			},{
				name: 'idbdgrawat',
				mapping: 'idbdgrawat'
			},{
				name: 'nmbdgrawat',
				mapping: 'nmbdgrawat'
			},{
				name: 'kdbagian',
				mapping: 'kdbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'bag_idbagian',
				mapping: 'bag_idbagian'
			},{
				name: 'alias',
				mapping: 'alias'
			}]
		});
		
	var kdbrgtemp = null;
	var arr_cari = [['kdbrg', 'Kode Barang'],['nmbrg', 'Nama Barang'],['nmjnsbrg','Jenis Barang'],['nmsatuanbsr','Satuan Besar'],['rasio', 'Rasio']];
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	var ds_tglstokopname = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
				url : BASE_URL + 'kartustok_controller/getTglstokbrg',
				params: {
					kdbrg : '',
				},
				method: 'POST'
			}),
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'tglso',
				mapping: 'tglso'
			},{
				name: 'tglsoval',
				mapping: 'tglsoval'
			},{
				name: 'kdbrg',
				mapping: 'kdbrg'
			}],
			listeners : {
				load : function() {
					
			}
		}
		});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail No. Barang Bagian" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	var pageSize = 10;
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: 200,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_barangkartustok,
		displayInfo: true,
		displayMsg: 'Data Barang Bagian Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});

	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_brgbagian',
		store: ds_barangkartustok,		
		autoScroll: true,
	//	autoHeight: true,
		height: 200,
		columnLines: true,
	//	plugins: cari_data,
		tbar: [{
			xtype: 'compositefield',
			width: 740,
			items: [{
				xtype: 'label', text: 'Cari Berdasarkan:', margins: '3 5 0 45px',
			},{
				xtype: 'combo', id: 'cb.cari',  store: ds_cari, valueField: 'id',
				displayField: 'nama',width: 100, height: 25, triggerAction: 'all',
				editable: false, submitValue: true, typeAhead: true, mode: 'local',
				emptyText: 'Pilih...', selectOnFocus: true, 
				listeners: {
				select: function() {
					var cbsearchh = Ext.getCmp('cb.cari').getValue();
						if(cbsearchh != ''){
							Ext.getCmp('cek').enable();
							Ext.getCmp('cek').focus();
						}
					return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'cek',
				width: 100,
				disabled: true,
				validator: function(){
				var cek = Ext.getCmp('cek').getValue();
				fnSearchgrid();
				return;
				}
			}]
						
		},{
			xtype: 'compositefield',
			width: 330,
			items: [{
				xtype: 'label', text: 'Bagian :', margins: '3 5 0 0',
			},{
				xtype: 'textfield',
				id: 'tf.bagian',
				width: 250,
				readOnly: true
			},{
				xtype: 'textfield',
				id: 'tf.idbagian',
				width: 100, hidden: true,
			},{
				xtype: 'button',
				iconCls: 'silk-find',
				id: 'bt.asd',		
				handler: function() {
									fbagian();
								}
			}]
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode Barang',
			width: 90,
			dataIndex: 'kdbrg',
			sortable: true,
			renderer: keyToDetil
		},
		{
			header: 'Nama Barang',
			width: 296,
			dataIndex: 'nmbrg',
			sortable: true
		},{
			header: 'Jenis Barang',
			width: 100,
			dataIndex: 'nmjnsbrg',
		},{
			header: 'Satuan Besar',
			dataIndex: 'nmsatuanbsr',
			width: 100
		},{
			header: 'Rasio',
			dataIndex: 'rasio',
			width:63,
			align:'right',
			xtype: 'numbercolumn', format:'0,000',
		},
		{
			header: 'Satuan Kecil',
			width: 100,
			dataIndex: 'nmsatuankcl',
			sortable: true, 
			//xtype: 'numbercolumn',
		},{
			header: 'Stok Sekarang',
			width: 100,
			dataIndex: 'stoknowbagian',	
			align:'right',	
		},
		{
			header: 'Stok Min',
			width: 70,
			dataIndex: 'stokminbagian',
			sortable: true, align:'right'
		},
		{
			header: 'Stok Max',
			width: 70,
			dataIndex: 'stokmaxbagian',
			sortable: true, align:'right'
		},{
			header: 'asd',
			dataIndex: 'idbagian',
			hidden: true
		}],
		bbar: paging,
		listeners: {
			cellclick: onCellClick
		}
	});
	
	function onCellClick(grid, rowIndex, columnIndex, event) {
		var t = event.getTarget();
		var record = grid.getStore().getAt(rowIndex);
		
		if (t.className == 'keyMasterDetail'){
				var rec_kdbrg = record.data['kdbrg'];
				var rec_nmbrg = record.data['nmbrg'];
				var rec_idbagian = record.data['idbagian'];
				var rec_satuankecil = record.data['nmsatuankcl'];
				Ext.getCmp('tf.reckdbrg').setValue(rec_kdbrg);
				Ext.getCmp('tf.kdbrg').setValue(rec_kdbrg);
				Ext.getCmp('tf.nmbrg').setValue(rec_nmbrg);
				Ext.getCmp('tf.satuan').setValue(rec_satuankecil);
				kdbrgtemp = rec_kdbrg;
				
				ds_tglstokopname.reload({
					params: { 
						kdbrg: rec_kdbrg
					},
					callback: function(results){
						var a=0;
						Ext.getCmp('df.tglawal').setValue(null);
						
							ds_tglstokopname.each(function (rec) { 
								if (a==0) { 
									RH.setCompValue('df.tglawal', rec.get('tglsoval'));
								}
								a++;
							});
							
							if(rec_kdbrg != "null"){
				
							ds_kartustok.reload({
								params: { 
									kdbrg: rec_kdbrg,
									idbagian : rec_idbagian,
									tglawal: Ext.getCmp('df.tglawal').getValue(),
									tglakhir:Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'),
								},
								callback: function(results){			
								
									var saw = 0; 
									var jm = 0;
									var jk = 0;
									var sawal = 0; 
									var smsk = 0;
									var sklr = 0;
									var sakhir = 0;
									
									ds_kartustok.each(function (rec) { 
										saw += (rec.get('saldoawal')) ? parseInt(rec.get('saldoawal')):0;
										jm += (rec.get('jmlmasuk')) ? parseInt(rec.get('jmlmasuk')):0;
										jk += (rec.get('jmlkeluar')) ? parseInt(rec.get('jmlkeluar')):0;
										sawal += (rec.get('nilaisaldoawal')) ? parseInt(rec.get('nilaisaldoawal')):0;
										smsk += (rec.get('nilaisaldomasuk')) ? parseInt(rec.get('nilaisaldomasuk')):0;
										sklr += (rec.get('nilaisaldokeluar')) ? parseInt(rec.get('nilaisaldokeluar')):0;
										sakhir += (rec.get('nilaisaldoakhir')) ? parseInt(rec.get('nilaisaldoakhir')):0;
									});
									
									RH.setCompValue('tf.totalsawl', saw)
									RH.setCompValue('tf.totaljmlmsk', jm);
									RH.setCompValue('tf.totaljmlklr', jk);

									RH.setCompValue('tf.totalsaldoawal', sawal)
									RH.setCompValue('tf.totalsaldomsk', smsk);
									RH.setCompValue('tf.totalsaldoklr', sklr);
									
									RH.setCompValue('tf.totalsakhr',(saw + jm) - jk);
									RH.setCompValue('tf.totalsaldoakhir', (sawal + smsk) - sklr);
									
								}
							});
							
							ds_kartustok.reload({
								scope   : this,
								callback: function(records, operation, success) {
									var kdbrg = '';

									 ds_kartustok.each(function (rec) { 
											kdbrg = rec.get('kdbrg');
										});		
									Ext.getCmp("tf.kdbrgtamp").setValue(kdbrg);
									var a = Ext.getCmp("tf.kdbrgtamp").getValue();
									if (a != ""){
										Ext.getCmp("btn_cetak").enable();
									}else{
										Ext.getCmp("btn_cetak").disable();
									}
								}
							});	
							
						}else if(rec_kdbrg == "null"){
							ds_kartustok.reload({
								params: { 
									kdbrg: 'null',
									idbagian : 'null'
								},
								callback: function(results){
									var saw = 0; 
									var jm = 0;
									var jk = 0;
									var sawal = 0; 
									var smsk = 0;
									var sklr = 0;
									var sakhir = 0;
									
									ds_kartustok.each(function (rec) { 
										saw += (rec.get('saldoawal')) ? parseInt(rec.get('saldoawal')):0;
										jm += (rec.get('jmlmasuk')) ? parseInt(rec.get('jmlmasuk')):0;
										jk += (rec.get('jmlkeluar')) ? parseInt(rec.get('jmlkeluar')):0;
										sawal += (rec.get('nilaisaldoawal')) ? parseInt(rec.get('nilaisaldoawal')):0;
										smsk += (rec.get('nilaisaldomasuk')) ? parseInt(rec.get('nilaisaldomasuk')):0;
										sklr += (rec.get('nilaisaldokeluar')) ? parseInt(rec.get('nilaisaldokeluar')):0;
										sakhir += (rec.get('nilaisaldoakhir')) ? parseInt(rec.get('nilaisaldoakhir')):0;
									});
									
									RH.setCompValue('tf.totalsawl', saw)
									RH.setCompValue('tf.totaljmlmsk', jm);
									RH.setCompValue('tf.totaljmlklr', jk);

									RH.setCompValue('tf.totalsaldoawal', sawal)
									RH.setCompValue('tf.totalsaldomsk', smsk);
									RH.setCompValue('tf.totalsaldoklr', sklr);
									
									RH.setCompValue('tf.totalsakhr',(saw + jm) - jk);
									RH.setCompValue('tf.totalsaldoakhir', (sawal + smsk) - sklr);
									
								}
							});
						}
					}
				});	
					
				Ext.getCmp('df.tglawal').enable();
				Ext.getCmp('df.tglakhir').enable();
				Ext.getCmp('bt.periode').enable();
					
			return true;
		}
		return true;
	}
	
	var grid_detail = new Ext.grid.EditorGridPanel({
		id: 'grid_detail',
		store: ds_kartustok,
		clicksToEdit: 1,
		autoScroll: true,
		loadMask: true,
		tbar: [{
			xtype: 'compositefield',
			style: 'padding: 5px; margin: 3px 0 0 15px',
			width:673,
			items:[{
					xtype: 'textfield',
					id:'tf.reckdbrg',
					width: 100,
					hidden: true,	
			},{
				xtype: 'label', text: 'Barang	: ', id: 'lb.barang', margins: '3 5 0 0',
			},{
				xtype: 'textfield',			
				id: 'tf.kdbrg',
				width: 100,
				disabled: true,
			},{
				xtype: 'textfield',
				id: 'tf.nmbrg',
				width: 150,
				disabled: true,
			},{
				xtype: 'textfield',
				id: 'tf.satuan',
				width: 80,
				disabled: true,
				width: 80
			},{
				xtype: 'textfield',
				id:'tf.kdbrgtamp',
				width: 50,
				hidden: true,	
			},{
				xtype: 'button', text: 'Cetak', iconCls:'silk-printer', id: 'btn_cetak', style: 'marginLeft: 5px',
				disabled: true, handler: function() {
					cetak();					
				}
			}]
		},{
			xtype: 'compositefield',
			style: 'padding: 5px; margin: 3px 0 0 15px',
			width:400,
			items:[{
				xtype: 'label', text: 'Periode : ', id: 'lb.p', margins: '3 5 0 0',
			},{
				xtype: 'combo', id: 'df.tglawal',  store: ds_tglstokopname, valueField: 'tglsoval',
				displayField: 'tglso',width: 100, height: 25, triggerAction: 'all', disabled: true,
				editable: false, submitValue: true, mode: 'local',
				emptyText: 'Pilih...', forceSelection: true,
				listeners: {
					select: function() {
						cAdvance();
					}
				}
			},{
				xtype: 'label', text: 's.d', margins: '3 6 0 3',
			},{
				xtype: 'datefield', id: 'df.tglakhir', disabled: true,
							width: 100, value: new Date(),
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									cAdvance();
								},
								change : function(field, newValue){
									cAdvance();
								}
							}
			},{
				xtype: 'button',
				iconCls: 'silk-find',
				id: 'bt.periode',	
				disabled: true,				
				handler: function() {
									cAdvance();
								}		
			}]
		}],
		columnLines: true,
		height: 255,
		frame: true,
		columns: [
		new Ext.grid.RowNumberer(),
		{
			header: 'Stok Awal',
			dataIndex: 'saldoawal',
			width: 90,
			sortable: true,align:'right'
		},{
			header: 'Stok Masuk',
			dataIndex: 'jmlmasuk',
			width: 90,
			sortable: true,align:'right'
		},{
			header: 'Stok Keluar',
			dataIndex: 'jmlkeluar',
			width: 90,
			sortable: true,align:'right'
		},{
			header: 'Stok Akhir',
			dataIndex: 'saldoakhir',
			width: 90,
			sortable: true,align:'right'

		},{
			header: 'Saldo Awal',
			dataIndex: 'nilaisaldoawal',
			width: 110,
			sortable: true,align:'right'
		},{
			header: 'Nilai Masuk',
			dataIndex: 'nilaisaldomasuk',
			width: 110,
			sortable: true,align:'right'
		},{
			header: 'Nilai Keluar',
			dataIndex: 'nilaisaldokeluar',
			width: 110,
			sortable: true,align:'right'
		},{
			header: 'Saldo Akhir',
			dataIndex: 'nilaisaldoakhir',
			width: 110,
			sortable: true,align:'right'

		},{
			header: 'Tanggal',
			dataIndex: 'tglkartustok',
			width: 80,
			sortable: true,
		//	renderer: fnkeyToDetil
		},{
			header: 'Jam',
			dataIndex: 'jamkartustok',
			width: 80,
			sortable: true,
		},{
			header: 'Ref. No',
			dataIndex: 'noref',
			width: 100,
			sortable: true,
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 60,
			sortable: true,
		},{
			header: 'Nama Pasien',
			dataIndex: 'nmpasien',
			width: 150,
			sortable: true,
		},{
			header: 'Keterangan',
			dataIndex: 'nama',
			hidden: true,
			width: 220,
			sortable: true,align:'left'
		},{
			header: 'Jenis Transaksi',
			dataIndex: 'nmjnskartustok',
			width: 150,
			sortable: true,
		}],
		bbar: [
					{
						xtype: 'numericfield',
						id:'tf.totalsawl',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:100,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},
				
					{
						xtype: 'numericfield',
						id:'tf.totaljmlmsk',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:100,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},
				
					{
						xtype: 'numericfield',
						id:'tf.totaljmlklr',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:100,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},
				
					{
						xtype: 'numericfield',
						id:'tf.totalsakhr',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:100,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},{
						xtype: 'numericfield',
						id:'tf.totalsaldoawal',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:110,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},
				
					{
						xtype: 'numericfield',
						id:'tf.totalsaldomsk',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:110,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},
				
					{
						xtype: 'numericfield',
						id:'tf.totalsaldoklr',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:110,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},
				
					{
						xtype: 'numericfield',
						id:'tf.totalsaldoakhir',
						readOnly: true,
						disabled: false,
						margins: '0 0 0 0',
						width:110,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					}]
		
	});
	
	var form_bp_general = new Ext.form.FormPanel({
			id: 'fp.daftarPO',
			region: 'center',
			bodyStyle: 'padding: 5px;',		
			border: false, frame: true,
			title: 'Kartu Stok',
			autoScroll: true,
			layout: 'column',
		items:[{
			xtype: 'container',
		//	style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			items:[{
				xtype: 'fieldset', title: 'Daftar Barang Bagian',
				layout: 'form',
				height: 220,
				boxMaxHeight:240,
				items: [grid_nya]
			}]
		},{
			xtype: 'container',
		//	style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			items:[{
				xtype: 'fieldset', title: 'Daftar Transaksi Barang Bagian',
				layout: 'form',
				height: 278,
				boxMaxHeight:293,
				items: [grid_detail]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
	
	function cetak(){
		
		var tglawal		= (Ext.getCmp('df.tglawal').getValue()) ? Ext.getCmp('df.tglawal').getValue():null;
		var tglakhir	= Ext.getCmp('df.tglakhir').getValue().format('Y-m-d');
		var idbagian	= Ext.getCmp('tf.idbagian').getValue();
		var kdbrg		= Ext.getCmp('tf.kdbrg').getValue();
		idbagian = (idbagian) ? idbagian : 11;
		
		RH.ShowReport(BASE_URL + 'print/lap_persediaan_kartustok/laporan_kartustok/'
                +tglawal+'/'+tglakhir+'/'+idbagian+'/'+kdbrg);
	}
	
	function fnSearchgrid(){

//ds_barangkartustok.setBaseParam('chb_nopo', Ext.getCmp('chb.nopo').getValue());
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.cari').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
	//	alert(nmcombo);
			ds_barangkartustok.setBaseParam('key',  '1');
			ds_barangkartustok.setBaseParam('id',  idcombo);
			ds_barangkartustok.setBaseParam('name',  nmcombo);
			ds_barangkartustok.load();
			ds_barangkartustok.reload();
		
	}

	function cAdvance(){
	ds_kartustok.reload({
		params: { 
			tglawal: Ext.getCmp('df.tglawal').getValue(),
			tglakhir:Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'),
			kdbrg: kdbrgtemp
		},
		callback: function(results){
							var saw = 0; 
							var jm = 0;
							var jk = 0;
							var sawal = 0; 
							var smsk = 0;
							var sklr = 0;
							var sakhir = 0;
							
							ds_kartustok.each(function (rec) { 
								saw += (rec.get('saldoawal')) ? parseInt(rec.get('saldoawal')):0;
								jm += (rec.get('jmlmasuk')) ? parseInt(rec.get('jmlmasuk')):0;
								jk += (rec.get('jmlkeluar')) ? parseInt(rec.get('jmlkeluar')):0;
								sawal += (rec.get('nilaisaldoawal')) ? parseInt(rec.get('nilaisaldoawal')):0;
								smsk += (rec.get('nilaisaldomasuk')) ? parseInt(rec.get('nilaisaldomasuk')):0;
								sklr += (rec.get('nilaisaldokeluar')) ? parseInt(rec.get('nilaisaldokeluar')):0;
								sakhir += (rec.get('nilaisaldoakhir')) ? parseInt(rec.get('nilaisaldoakhir')):0;
							});
							
							RH.setCompValue('tf.totalsawl', saw)
							RH.setCompValue('tf.totaljmlmsk', jm);
							RH.setCompValue('tf.totaljmlklr', jk);

							RH.setCompValue('tf.totalsaldoawal', sawal)
							RH.setCompValue('tf.totalsaldomsk', smsk);
							RH.setCompValue('tf.totalsaldoklr', sklr);
							
							
							 RH.setCompValue('tf.totalsakhr',(saw + jm) - jk);
							//RH.setCompValue('tf.totalsakhr',jm - jk);
							RH.setCompValue('tf.totalsaldoakhir', (sawal + smsk) - sklr);
					
		}
	});
}
	
	function fbagian(){
		function fnbagian(value){
				Ext.QuickTips.init();
				return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
					+ value +'</div>';
			}
		var cm_bagian= new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idbagian',
				width: 30
			},{
				header: 'Kode Bagian',
				dataIndex: 'kdbagian',
				width: 100,
				renderer: fnbagian
			},{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200
			}
		]);
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagian,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200,
		})];
		var grid_bagian = new Ext.grid.GridPanel({
			ds: ds_bagian,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 310,
			plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			autoScroll: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_bagian = new Ext.Window({
			title: 'Bagian',
			modal: true,
			items: [grid_bagian]
		}).show();
		
		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_idbagian = record.data["idbagian"];
					var var_kdbagian = record.data["kdbagian"];
					var var_nmbagian = record.data["nmbagian"];
							
					Ext.getCmp('tf.idbagian').focus()
					Ext.getCmp("tf.idbagian").setValue(var_idbagian);
					Ext.getCmp("tf.bagian").setValue(var_nmbagian);
					ds_barangkartustok.setBaseParam('idbagian',var_idbagian);
					
					ds_barangkartustok.reload();
								win_find_bagian.close();
				return true;
			}
			return true;
		}
	}
	
}