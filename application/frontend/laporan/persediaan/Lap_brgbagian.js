function Lap_brgbagian(){
Ext.form.Field.prototype.msgTarget = 'side';
	var ds_lapbrgbagian = dm_lapbrgbagian();
	
	var ds_pengbagian = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'lapbrgbagian_controller/get_pengbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_pengbagian.setBaseParam('userid', USERID);	
	ds_pengbagian.reload({
		scope   : this,
		callback: function(records, operation, success) {
			var idbagian = '';
			var nmbagian = '';

			 ds_pengbagian.each(function (rec) { 
					idbagian = rec.get('idbagian');
					nmbagian = rec.get('nmbagian');
				});                          
			Ext.getCmp("tf.bagian").setValue(nmbagian);			
			Ext.getCmp("tf.idbagian").setValue(idbagian);	
		}
	});	
	
	var ds_bagianpeng = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'returbrgbagian_controller/get_pengbagian',
			method: 'POST'
		}),
		root: 'data',
		totalProperty: 'results',
		autoLoad: true,
		fields: [{
			name: "idbagian",
			mapping: "idbagian"
		},{
			name: "nmbagian",
			mapping: "nmbagian"
		},{
			name: "idjnspelayanan",
			mapping: "idjnspelayanan"
		},{
			name: "nmjnspelayanan",
			mapping: "nmjnspelayanan"
		},{
			name: "idbdgrawat",
			mapping: "idbdgrawat"
		},{
			name: "nmbdgrawat",
			mapping: "nmbdgrawat"
		},{
			name: "userid",
			mapping: "userid"
		},{
			name: "nmlengkap",
			mapping: "nmlengkap"
		}]
	});
	
	ds_bagianpeng.setBaseParam('userid', USERID);
	
	var vw_nya = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_brgbagian',
		store: ds_lapbrgbagian,
		view: vw_nya,	
		frame: true,		
		autoScroll: true,
		height: 465,
		columnLines: true,
		tbar: [{
			text: 'Cetak',
			id: 'btn.cetak',
			iconCls:'silk-printer',
			disabled: true,
			handler: function() {
				cetak();
			}
		},'-',
		{ 	text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',
			disabled: true,
			handler: function(){
				cetakexcel();
			}
		},{
			xtype: 'textfield',
			id: 'tf.idbagian',
			width: '50',
			hidden: true,
			validator: function(){
				ds_lapbrgbagian.reload({
					params: { 
						idbagian: Ext.getCmp('tf.idbagian').getValue()
					}
				});

				ds_lapbrgbagian.reload({
					scope   : this,
					callback: function(records, operation, success) {
						var idbagian = '';

						 ds_lapbrgbagian.each(function (rec) { 
								idbagian = rec.get('idbagian');
							});		
						Ext.getCmp("tf.idbagiantmp").setValue(idbagian);	
						var a = Ext.getCmp("tf.idbagiantmp").getValue();
						if (a != ""){
							Ext.getCmp("btn.cetak").enable();
							Ext.getCmp("btn.cetakexcel").enable();
						}else{
							Ext.getCmp("btn.cetak").disable();
							Ext.getCmp("btn.cetakexcel").disable();
						}
					}
				});				
			}
		},{
			xtype: 'textfield',
			id: 'tf.idbagiantmp',
			width: '50',
			hidden: true,			
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Nama Bagian',
			width: 150,
			dataIndex: 'nmbagian',
			sortable: true,
			hidden: true
		},{
			header: 'Kode Barang',
			width: 80,
			dataIndex: 'kdbrg',
			sortable: true
		},
		{
			header: 'Nama Barang',
			width: 240,
			dataIndex: 'nmbrg',
			sortable: true
		},{
			header: 'Jenis Barang',
			width: 100,
			dataIndex: 'nmjnsbrg',
			sortable: true
		},{
			header: 'Satuan Besar',
			width: 90,
			dataIndex: 'nmsatuanbsr',
			sortable: true
		},
		{
			header: 'Rasio',
			width: 100,
			dataIndex: 'rasio',
			sortable: true, 
			xtype: 'numbercolumn', format:'0,000.00', align:'right'
		},{
			header: 'Satuan Kecil',
			width: 90,
			dataIndex: 'nmsatuankcl',
			sortable: true
		},{
			header: 'Stok Sekarang',
			width: 100,
			dataIndex: 'stoknowbagian',
			sortable: true, 
			xtype: 'numbercolumn', format:'0,000.00', align:'right'
		},
		{
			header: 'Stok Minimal',
			width: 100,
			dataIndex: 'stokminbagian',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		},
		{
			header: 'Stok Maksimal',
			width: 100,
			dataIndex: 'stokmaxbagian',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00', align:'right',
		}]
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Daftar Barang', iconCls:'silk-calendar',
		layout: 'fit',
		frame: true,
		items: [
		{
			xtype: 'panel',
			border: false,
			autoScroll: true,
			items: [{
				layout: 'form',
				border: false,
				items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .99,
						border: false,
						items: [{
							xtype: 'compositefield',
							style: 'padding: 5px; marginLeft: 10px; marginBottom: -6px',
							width: 470,
							items: [{
								xtype: 'label', id: 'lb.bgn', text: 'Bagian :', margins: '3 10 0 5',
							},{
								xtype: 'textfield',
								id: 'tf.bagian',
								emptyText:'Nama Bagian..',
								width: 300,
								readOnly: true,
							},
							{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn_data_pen_bgn',
								width: 3,
								handler: function() {
									fnwBagian();
									ds_lapbrgbagian.reload();
								}
							}/* ,{
								xtype: 'button', text: 'Cetak', iconCls:'silk-printer', id: 'btn.cetak', style: 'marginLeft: 22px',
								disabled: true, handler: function() {
									cetak();
								}
							} */]
						}]
					}]
				},{
					xtype : 'fieldset',
					title: 'Daftar Barang Bagian',
					items: [grid_nya]
				}]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadBrgbagian(){
		ds_lapbrgbagian.reload();
	}
	
	function cetak(){
		/* var idbagian = Ext.getCmp('tf.idbagian').getValue();			
		RH.ShowReport(BASE_URL + 'print/lap_persediaan_daftarbrg/laporan_daftarbrg/' + idbagian); */
		
		var parsing = '';
			parsing = parsing + Ext.getCmp('tf.idbagian').getValue() + '-' ;
			parsing = parsing + Ext.getCmp('tf.bagian').getValue() + '-' ;
		
		var win = window.open();
		win.location.reload();
		win.location = BASE_URL + 'print/lap_persediaan_daftarbrg/laporan_daftarbrg/'+parsing;
	}
	
	function cetakexcel(){	
		var parsing = '';
			parsing = parsing + Ext.getCmp('tf.idbagian').getValue() + '-' ;
			parsing = parsing + Ext.getCmp('tf.bagian').getValue() + '-' ;
		
		window.location =(BASE_URL + 'print/lap_persediaan_daftarbrg/excellaporan_daftarbrg/'+parsing);
	}

	/**
	WIN - FORM ENTRY/EDIT 
	*/
	
	function fnwBagian(){
		var ds_lapbrgbagian = dm_brgbagian1();
		ds_lapbrgbagian.setBaseParam('idbagian', 'null');
		ds_lapbrgbagian.reload();
		ds_bagianpeng.reload();
		
		function fnkeyAddBagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bagian = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200,
				renderer: fnkeyAddBagian
			},{
				header: 'Jenis Pelayanan',
				dataIndex: 'nmjnspelayanan',
				width: 150
			},{
				header: 'Bidang Perawatan',
				dataIndex: 'nmbdgrawat',
				width: 220
			}
		]);
		
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagianpeng,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bagian = new Ext.grid.GridPanel({
			ds: ds_bagianpeng,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 620,
			plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbagian = record.data["idbagian"];
					var var_cari_bagian = record.data["nmbagian"];
								
					//Ext.getCmp('tf.bagian').focus();
					Ext.getCmp("tf.bagian").setValue(var_cari_bagian);
					Ext.getCmp('tf.idbagian').setValue(var_cari_idbagian);
					Ext.getCmp('grid_brgbagian').store.reload();
								win_find_cari_bagian.close();
				return true;
			}
			return true;
		}
	}
}