function Lap_jurnalumum(){
	Ext.form.Field.prototype.msgTarget = 'side';
	
	var ds_akun = dm_akun_jurnal();
	var reader = new Ext.data.JsonReader({
		root:'data',
		idProperty: '',
		totalProperty: 'results',
		remoteGroup: true,
		fields: [
		{ name: 'kdjurnal', mapping: 'kdjurnal' },
		{ name: 'tahun', mapping: 'tahun' },
		{ name: 'idakun', mapping: 'idakun' },
		{ name: 'noreff', mapping: 'noreff' },
		{ name: 'debit', mapping: 'debit' },
		{ name: 'kredit', mapping: 'kredit' },
		{ name: 'idjnsjurnal', mapping: 'idjnsjurnal' },
		{ name: 'idbagian', mapping: 'idbagian' },
		{ name: 'nokasir', mapping: 'nokasir' },
		{ name: 'tgltransaksi', mapping: 'tgltransaksi' },
		{ name: 'tgljurnal', mapping: 'tgljurnal' },
		{ name: 'keterangan', mapping: 'keterangan' },
		{ name: 'userid', mapping: 'userid' },
		{ name: 'tglinput', mapping: 'tglinput' },
		{ name: 'nominal', mapping: 'nominal' },
		{ name: 'idjnstransaksi', mapping: 'idjnstransaksi' },
		{ name: 'kdakun', mapping: 'kdakun' },
		{ name: 'nmakun', mapping: 'nmakun' },
		{ name: 'idklpakun', mapping: 'idklpakun' }]
	});
	var ds_lap_jurnal_umum = new Ext.data.GroupingStore({
		proxy: new Ext.data.HttpProxy({
			url: BASE_URL + 'lapjurnalumum_controller/get_lap_jurnal_umum',
			method: 'POST',
		}),
		reader: reader,
		groupField:'nmakun',
		remoteSort: true,
	});
	//ds_lap_jurnal_umum.load();

	var cm_grid = new Ext.grid.ColumnModel({
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('nmakun'),
			width: 150,
			dataIndex: 'nmakun',
			align:'center'
		},{
			header: headerGerid('Kode<br>Jurnal'),
			width: 80,
			dataIndex: 'kdjurnal',
			align:'left'
		},{
			header: headerGerid('Tanggal<br>Transaksi'),
			width: 65,
			dataIndex: 'tgltransaksi',
			align:'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y'),
		},{
			header: headerGerid('Keterangan'),
			width: 200,
			dataIndex: 'keterangan',
			align:'left'
		},{
			header: headerGerid('Debit'),
			width: 80,
			dataIndex: 'debit',
			align:'right',
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Kredit'),
			width: 80,
			dataIndex: 'kredit',
			align:'right',
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('No. reff / Bon'),
			width: 80,
			dataIndex: 'noreff',
			align:'center'
		},{
			header: headerGerid('Tanggal<br>Jurnal'),
			dataIndex: 'tgljurnal',
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y'),
		}]	
	});
    
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
		
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_lap_jurnal_umum',
		store: ds_lap_jurnal_umum,
		height: 400,
		cm: cm_grid,
		sm: new Ext.grid.RowSelectionModel({ singleSelect: true}),
		view: new Ext.grid.GroupingView({
            forceFit:true,
			width:400,
            groupTextTpl: '{text}',
			enableGroupingMenu: false,	// don't show a grouping menu
			enableNoGroups: false,		// don't let the user ungroup
			hideGroupedColumn: true,	// don't show the column that is being used to create the heading
			showGroupName: false,		// don't show the field name with the group heading
			startCollapsed: false		// the groups start closed/no
        }),
		frame: true,
		//autoScroll: true,
		//autoSizeColumns: true,
		autoExpandColumn: 'nmakun',
		//enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		layout: 'anchor',
		tbar: [{
			text: 'Cetak PDF',
			id: 'btn.cetak',	
			iconCls:'silk-printer',		
			handler: function() {
				cetak_pdf();
			}
		},'-',{ 	
			text: 'Cetak Excel',
			iconCls: 'silk-printer',
			id: 'btn.cetakexcel',	
			handler: function(){
				cetak_excel();
			}
		}]
	});
       
	var form_bp_general = new Ext.FormPanel({
		id: 'form_bp_general',
		title: 'Laporan Jurnal Umum', iconCls:'silk-money',
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		items: [
		{
			layout: 'form',
			border: false,
			items: [{
				xtype: 'container',
				style: 'padding: 5px',
				//layout: 'column',
				defaults: {labelWidth: 100, labelAlign: 'right'},
				items:[{
					xtype: 'fieldset',
					title: 'Filter',
					height: 100,
					layout: 'column',
					items: [{
						layout: 'form', 
						columnWidth: 0.4,
						items: [{
							xtype: 'compositefield', fieldLabel: 'Tanggal Transaksi',
							items: [{
								xtype:'checkbox',  
								id: 'chb.periode_transaksi', 
								margins: '0 3 0 5',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tglawal_transaksi').enable();
											Ext.getCmp('tglakhir_transaksi').enable();
										} else if(val == false){
											Ext.getCmp('tglawal_transaksi').disable();
											Ext.getCmp('tglakhir_transaksi').disable();
											Ext.getCmp('tglawal_transaksi').setValue(new Date());
											Ext.getCmp('tglakhir_transaksi').setValue(new Date());							
											fnSearchgrid();
										}
									}
								}
							},{
								xtype: 'datefield',
								id: 'tglawal_transaksi',
								value: new Date(),
								format: "d/m/Y",
								width: 100, disabled: true,
								listeners:{
									select: function(field, newValue){
										fnSearchgrid();
									},
									change : function(field, newValue){
										fnSearchgrid();
									}
								}
							},{
								xtype: 'label', id: 'lb.lbt', text: 's/d', margins: '6 4 0 0',
							},{
								xtype: 'datefield',
								id: 'tglakhir_transaksi',
								value: new Date(),
								format: "d/m/Y",
								width: 100, disabled: true,
								listeners:{
									select: function(field, newValue){
										fnSearchgrid();
									},
									change : function(field, newValue){
										fnSearchgrid();
									}
								}
							}]
						},{
							xtype: 'compositefield', fieldLabel: 'Tanggal Input',
							items: [{
								xtype:'checkbox',  
								id: 'chb.periode_input',
								margins: '0 3 0 5',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tglawal_input').enable();
											Ext.getCmp('tglakhir_input').enable();
										} else if(val == false){
											Ext.getCmp('tglawal_input').disable();
											Ext.getCmp('tglakhir_input').disable();
											Ext.getCmp('tglawal_input').setValue(new Date());
											Ext.getCmp('tglakhir_input').setValue(new Date());							
											fnSearchgrid();
										}
									}
								}
							},{
								xtype: 'datefield',
								id: 'tglawal_input',
								value: new Date(),
								format: "d/m/Y",
								width: 100, disabled: true,
								listeners:{
									select: function(field, newValue){
										fnSearchgrid();
									},
									change : function(field, newValue){
										fnSearchgrid();
									}
								}
							},{
								xtype: 'label', id: 'lb.lbt', text: 's/d', margins: '6 4 0 0',
							},{
								xtype: 'datefield',
								id: 'tglakhir_input',
								value: new Date(),
								format: "d/m/Y",
								width: 100, disabled: true,
								listeners:{
									select: function(field, newValue){
										fnSearchgrid();
									},
									change : function(field, newValue){
										fnSearchgrid();
									}
								}
							}]
						}]
					},{
						layout: 'form', 
						columnWidth: 0.6,
						items: [{
							xtype: 'compositefield', 
							fieldLabel: 'Akun',
							items: [{
								xtype:'checkbox',  
								id: 'chb.akun', 
								margins: '0 3 0 5',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('akun').enable();
										} else if(val == false){
											Ext.getCmp('akun').disable();
											fnSearchgrid();
										}
									}
								}
							},{
								xtype: 'combo',
								id: 'akun',
								store: ds_akun,
								valueField: 'idakun', 
								displayField: 'nmakun', 
								editable: true,
								allowBlank: false,
								triggerAction: 'all',
								forceSelection: true, 
								submitValue: true, 
								mode: 'local',
								width: 150, 
								disabled: true,
								listeners:{
									select: function(field, newValue){
										fnSearchgrid();
									},
									change : function(field, newValue){
										fnSearchgrid();
									}
								}
							}]
						}]
					}]
				}]
			},
			grid_nya
			]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function fnSearchgrid(){
		
		//periode transaksi
		if(Ext.getCmp('chb.periode_transaksi').getValue() == true){
			ds_lap_jurnal_umum.setBaseParam('tglawal_transaksi', Ext.getCmp('tglawal_transaksi').getValue().format('Y-m-d'));
			ds_lap_jurnal_umum.setBaseParam('tglakhir_transaksi', Ext.getCmp('tglakhir_transaksi').getValue().format('Y-m-d'));
		}else if(Ext.getCmp('chb.periode_transaksi').getValue() == false){
			ds_lap_jurnal_umum.setBaseParam('tglawal_transaksi','');
			ds_lap_jurnal_umum.setBaseParam('tglakhir_transaksi','');
		}
		
		//periode tgl input
		if(Ext.getCmp('chb.periode_input').getValue() == true){
			ds_lap_jurnal_umum.setBaseParam('tglawal_input', Ext.getCmp('tglawal_input').getValue().format('Y-m-d'));
			ds_lap_jurnal_umum.setBaseParam('tglakhir_input', Ext.getCmp('tglakhir_input').getValue().format('Y-m-d'));
		}else if(Ext.getCmp('chb.periode_input').getValue() == false){
			ds_lap_jurnal_umum.setBaseParam('tglawal_input','');
			ds_lap_jurnal_umum.setBaseParam('tglakhir_input','');
		}
		
		//periode tgl input
		if(Ext.getCmp('chb.akun').getValue() == true){
			ds_lap_jurnal_umum.setBaseParam('akun', Ext.getCmp('akun').getValue());
		}else if(Ext.getCmp('chb.akun').getValue() == false){
			ds_lap_jurnal_umum.setBaseParam('akun','');
		}
	
		ds_lap_jurnal_umum.load();
	}
	
	function cetak_pdf()
	{
		var tglawal_transaksi	= '-';
		var tglakhir_transaksi	= '-';
		var tglawal_input		= '-';
		var tglakhir_input	    = '-';
		var tglakhir_input	    = '-';
		var akun                = '-';
		
		//periode transaksi
		if(Ext.getCmp('chb.periode_transaksi').getValue() == true){
			tglawal_transaksi  =  Ext.getCmp('tglawal_transaksi').getValue().format('Y-m-d');
			tglakhir_transaksi =  Ext.getCmp('tglakhir_transaksi').getValue().format('Y-m-d');
		}
		
		//periode tgl input
		if(Ext.getCmp('chb.periode_input').getValue() == true){
			tglawal_input  =  Ext.getCmp('tglawal_input').getValue().format('Y-m-d');
			tglakhir_input =  Ext.getCmp('tglakhir_input').getValue().format('Y-m-d');
		}
		
		//periode tgl input
		if(Ext.getCmp('chb.akun').getValue() == true){
			akun =  Ext.getCmp('akun').getValue();
		}

		RH.ShowReport(BASE_URL + 'print/lap_jurnal_umum/pdf/'
					+tglawal_transaksi+'/'+tglakhir_transaksi+'/'
					+tglawal_input+'/'+tglakhir_input+'/'
					+akun);
	}
	
	function cetak_excel(){

		var tglawal_transaksi	= '-';
		var tglakhir_transaksi	= '-';
		var tglawal_input		= '-';
		var tglakhir_input	    = '-';
		var tglakhir_input	    = '-';
		var akun                = '-';
		
		//periode transaksi
		if(Ext.getCmp('chb.periode_transaksi').getValue() == true){
			tglawal_transaksi  =  Ext.getCmp('tglawal_transaksi').getValue().format('Y-m-d');
			tglakhir_transaksi =  Ext.getCmp('tglakhir_transaksi').getValue().format('Y-m-d');
		}
		
		//periode tgl input
		if(Ext.getCmp('chb.periode_input').getValue() == true){
			tglawal_input  =  Ext.getCmp('tglawal_input').getValue().format('Y-m-d');
			tglakhir_input =  Ext.getCmp('tglakhir_input').getValue().format('Y-m-d');
		}
		
		//periode tgl input
		if(Ext.getCmp('chb.akun').getValue() == true){
			akun =  Ext.getCmp('akun').getValue();
		}

		RH.ShowReport(BASE_URL + 'print/lap_jurnal_umum/excel/'
					+tglawal_transaksi+'/'+tglakhir_transaksi+'/'
					+tglawal_input+'/'+tglakhir_input+'/'
					+akun);
	}

}