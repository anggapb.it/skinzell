function RJtransaksi(){
	var myVar= setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jamrj"))
				RH.setCompValue("tf.jamrj",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}

	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("tf.shift").setValue(obj.nmshift);
		}
	});
	
	var ds_stpelayanan = dm_stpelayanan();
	var ds_nota = dm_nota();
	ds_nota.setBaseParam('start',0);
	ds_nota.setBaseParam('limit',500);
	ds_nota.setBaseParam('idregdet',null);
	ds_nota.setBaseParam('idbagian',999);
	
	var ds_nota2 = dm_nota();
	ds_nota2.setBaseParam('start',0);
	ds_nota2.setBaseParam('limit',500);
	ds_nota2.setBaseParam('idregdet',null);
	ds_nota2.setBaseParam('idbagian',999);
		
	var ds_vregistrasi = dm_vregistrasi_rj();
	ds_vregistrasi.setBaseParam('start',0);
	ds_vregistrasi.setBaseParam('limit',4);
	ds_vregistrasi.setBaseParam('cek','RJ');
	ds_vregistrasi.setBaseParam('ctglreg',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_vregistrasi.setBaseParam('groupbyq',1);
	
	var ds_vtarifall = dm_vtarifall();
	ds_vtarifall.setBaseParam('start',0);
	ds_vtarifall.setBaseParam('limit',4);
	ds_vtarifall.setBaseParam('cidbagian',0);
	
	var ds_brgbagian = dm_brgbagian();
	ds_brgbagian.setBaseParam('start',0);
	ds_brgbagian.setBaseParam('limit',4);
	ds_brgbagian.setBaseParam('cidbagian',0);
		
	var ds_dokter = dm_dokter_combo();
	var arr = [];
	var zkditem = '';
	var rownota = '';
	var rownota2 = '';
	var pmbyrn = 0;
	var pmbyrndjmn = 0;
	var totfarmasi = 0;
	var tottindakan = 0;
	var koder = 0;
	var cekkstok = 0;
	var cekpenj = 0;
		
	var grid_nota = new Ext.grid.EditorGridPanel({
		store: ds_nota,
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota',
		forceFit: true,
		//sm: cbGrid,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota = rowIndex;
            }
        },
		columns: [{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					ds_nota.removeAt(rowIndex);
					totalnota();
					cekTombol();
				}
			}]
		},{
			header: '<div style="text-align:center;">Item Tindakan</div>',
			dataIndex: 'nmitem',
			width: 250,
			sortable: true,
			renderer:jasaMedis
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 110,
			editor: {
				xtype: 'combo',
				id: 'cb.dokter',
				store: ds_dokter, valueField: 'nmdoktergelar', displayField: 'nmdoktergelar',
				editable: true, triggerAction: 'all',
				forceSelection: true, mode: 'local',
			},
			sortable: true
		},{
			header: '<div style="text-align:center;">Tarif</div>',
			dataIndex: 'tarif',
			align:'right',
			width: 90,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Qty</div>',
			dataIndex: 'qty',
			align:'right',
			width: 45,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqty', width: 150, 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota.getAt(rownota);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqty').getValue();
						var subtotal2 = record.data.drp;
						var jsubtotal = subtotal - subtotal2;
						record.set('tarif2',jsubtotal);
						
						var dijaminrp = record.get('dijaminrp');
						var subdijamin = jsubtotal - dijaminrp;
						var dijaminprsn = (dijaminrp/jsubtotal)*100;
						if(dijaminprsn=='Infinity' || dijaminprsn<0) dijaminprsn = 0;
						record.set('selisih',subdijamin);
						record.set('dijaminprsn',dijaminprsn);
						
						totalnota();
						cekTombol();
					}
				}
			},
			sortable: true
		},{
			header: '<div style="text-align:center;">Satuan</div>',
			dataIndex: 'nmsatuan',
			width: 80,
			sortable: true
		},{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Diskon',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_edit.png',
				handler: function(grid, rowIndex) {
					fDiskon(rowIndex);
				}
			}]
		},{
			header: '<div style="text-align:center;">Total<br>Diskon</div>',
			dataIndex: 'drp',
			align:'right',
			width: 55,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Subtotal</div>',
			dataIndex: 'tarif2',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Dijamin<br>%</div>',
			dataIndex: 'dijaminprsn',
			align:'right',
			width: 50,
			editor: {
				xtype: 'numberfield',
				id: 'tf.dijaminprsn',
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota.getAt(rownota);
						if(!record.data['dijamin']){
							record.set('dijaminprsn', 0);
							Ext.getCmp('tf.dijaminprsn').setValue(0);
							Ext.MessageBox.alert('Informasi', 'Silahkan ceklis checkbox dijamin');
						} else {
							var tfdijaminprsn = Ext.getCmp('tf.dijaminprsn').getValue();
							var subtotal = record.get('tarif2');
							var dijaminprp = (subtotal*tfdijaminprsn)/100;
							var selisih = subtotal-dijaminprp;
							record.set('dijaminrp', dijaminprp);
							record.set('selisih', selisih);
							totalnota();
						}
						cekTombol();
					}
				}
			},
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Dijamin<br>Rp</div>',
			dataIndex: 'dijaminrp',
			align:'right',
			width: 70,
			editor: {
				xtype: 'numberfield',
				id: 'tf.dijaminrp', 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota.getAt(rownota);
						if(!record.data['dijamin']){
							record.set('dijaminrp', 0);
							Ext.getCmp('tf.dijaminrp').setValue(0);
							Ext.MessageBox.alert('Informasi', 'Silahkan ceklis checkbox dijamin');
						} else {
							var tfdijaminrp = Ext.getCmp('tf.dijaminrp').getValue();
							var subtotal = record.get('tarif2');
							var dijaminprsn = (tfdijaminrp/subtotal)*100;
							var selisih = subtotal-tfdijaminrp;
							if(dijaminprsn=='Infinity' || dijaminprsn<0) dijaminprsn = 0;
							record.set('dijaminrp', tfdijaminrp);
							record.set('dijaminprsn', parseFloat(dijaminprsn));
							record.set('selisih', selisih);
							totalnota();
						}
						cekTombol();
					}
				}
			},
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Selisih</div>',
			dataIndex: 'selisih',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Dijamin</div>',
			dataIndex: 'dijamin',
			align:'right',
			width: 50,
			xtype: 'checkcolumn',
			processEvent: function(name, e, grid, rowIndex, colIndex){
				var record = grid.store.getAt(rowIndex);
				if (name == 'mousedown') {
					record.set(this.dataIndex, !record.data[this.dataIndex]);
					if(!record.data[this.dataIndex]){
						record.set('dijaminprsn', 0);
						record.set('dijaminrp', 0);
						record.set('selisih', record.get('tarif2'));
						Ext.getCmp('tf.dijaminprsn').setValue(0);
						Ext.getCmp('tf.dijaminrp').setValue(0);
						Ext.getCmp('chb.plafond').setValue(0);
						totalnota();
					} else {
						record.set('dijaminprsn', 100);
						record.set('dijaminrp', record.get('tarif2'));
						record.set('selisih', 0);
						totalnota();
						if(Ext.getCmp('tf.totalrf').getValue() == 0 && Ext.getCmp('tf.total').getValue() == 0){
							Ext.getCmp('chb.plafond').setValue(1);
						}else{
							Ext.getCmp('chb.plafond').setValue(0);
						}
					}
					cekTombol();
				}

			}
		},{
			dataIndex: 'kditem',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'koder',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idjnstarif',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idtarifpaketdet',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjs',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjm',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifbhp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjs',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjm',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonbhp',
			hidden: true,
			hideable: false
		}],
		bbar: [
			{ xtype:'tbfill' },'Jumlah Dijamin : ',
			{
				xtype: 'numericfield',
				id: 'tf.totaldijamin',
				value: 0,
				readOnly:true,
				style : 'opacity:0.6',
				width: 80,
				thousandSeparator:','
			},' Jumlah Selisih : ',
			{
				xtype: 'numericfield',
				id: 'tf.total',
				value: 0,
				readOnly:true,
				style : 'opacity:0.6',
				width: 100,
				thousandSeparator:','
			}
		]
	});
	
	var grid_notafarmasi = new Ext.grid.EditorGridPanel({
		store: ds_nota2,
		frame: true,
		height: 260,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota2',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota2 = rowIndex;
            }
        },
		columns: [{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					ds_nota2.removeAt(rowIndex);
					totalnota2();
					cekTombol();
				}
			}]
		},{
			header: 'R/',
			dataIndex: 'koder',
			width: 20,editor: {
				xtype: 'numberfield',
				id: 'tf.koder', width: 20
			},
			sortable: true
		},{
			header: 'Item Obat/Alkes',
			dataIndex: 'nmitem',
			width: 130,
			sortable: true
		},{
			header: 'Tarif',
			dataIndex: 'tarif',
			align:'right',
			width: 67,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: 'Qty',
			dataIndex: 'qty',
			align:'right',
			width: 38,
			sortable: true,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqtyf', width: 150, 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota2.getAt(rownota2);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqtyf').getValue();
						record.set('tarif2',subtotal);
						
						var dijaminrp = record.get('dijaminrpf');
						var subdijamin = subtotal - dijaminrp;
						var dijaminprsn = (dijaminrp/subtotal)*100;
						if(dijaminprsn=='Infinity' || dijaminprsn<0) dijaminprsn = 0;
						record.set('selisihf',subdijamin);
						record.set('dijaminprsnf',dijaminprsn);
						
						totalnota2();
						cekTombol();
					}
				}
			}
		},{
			header: 'Satuan',
			dataIndex: 'nmsatuan',
			width: 45,
			sortable: true
		},{
			header: 'Subtotal',
			dataIndex: 'tarif2',
			align:'right',
			width: 75,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Dijamin<br>%</div>',
			dataIndex: 'dijaminprsnf',
			align:'right',
			width: 50,
			editor: {
				xtype: 'numberfield',
				id: 'tf.dijaminprsnf',
				enableKeyEvents: true,
				listeners:{
					keyup:function(name, e){
						var record = ds_nota2.getAt(rownota2);
						if(!record.data['dijaminf']){
							record.set('dijaminprsnf', 0);
							Ext.getCmp('tf.dijaminprsnf').setValue(0);
							Ext.MessageBox.alert('Informasi', 'Silahkan ceklis checkbox dijamin');
						} else {
							var tfdijaminprsn = Ext.getCmp('tf.dijaminprsnf').getValue();
							var subtotal = record.get('tarif2');
							var dijaminprsn = (subtotal*tfdijaminprsn)/100;
							var selisih = subtotal-dijaminprsn;
							record.set('dijaminrpf', dijaminprsn);
							record.set('selisihf', selisih);
							totalnota2();
						}
						cekTombol();
					}
				}
			},
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Dijamin<br>Rp</div>',
			dataIndex: 'dijaminrpf',
			align:'right',
			width: 70,
			editor: {
				xtype: 'numberfield',
				id: 'tf.dijaminrpf', 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota2.getAt(rownota2);
						if(!record.data['dijaminf']){
							record.set('dijaminrpf', 0);
							Ext.getCmp('tf.dijaminrpf').setValue(0);
							Ext.MessageBox.alert('Informasi', 'Silahkan ceklis checkbox dijamin');
						} else {
							var tfdijaminrp = Ext.getCmp('tf.dijaminrpf').getValue();
							var subtotal = record.get('tarif2');
							var dijaminprsn = (tfdijaminrp/subtotal)*100;
							var selisih = subtotal-tfdijaminrp;
							if(dijaminprsn=='Infinity' || dijaminprsn<0) dijaminprsn = 0;
							record.set('dijaminrpf', tfdijaminrp);
							record.set('dijaminprsnf', dijaminprsn);
							record.set('selisihf', selisih);
							totalnota2();
						}
						cekTombol();
					}
				}
			},
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Selisih</div>',
			dataIndex: 'selisihf',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Dijamin</div>',
			dataIndex: 'dijaminf',
			align:'right',
			width: 50,
			xtype: 'checkcolumn',
			processEvent: function(name, e, grid, rowIndex, colIndex){
				var record = grid.store.getAt(rowIndex);
				if (name == 'mousedown') {
					record.set(this.dataIndex, !record.data[this.dataIndex]);
					if(!record.data[this.dataIndex]){
						record.set('dijaminprsnf', 0);
						record.set('dijaminrpf', 0);
						record.set('selisihf', record.get('tarif2'));
						Ext.getCmp('tf.dijaminprsnf').setValue(0);
						Ext.getCmp('tf.dijaminprsnf').setValue(0);
						Ext.getCmp('chb.plafond').setValue(0);
						totalnota2diskon();
					} else {
						record.set('dijaminprsnf', 100);
						record.set('dijaminrpf', record.get('tarif2'));
						record.set('selisihf', 0);
						totalnota2diskon();
						if(Ext.getCmp('tf.totalrf').getValue() == 0 && Ext.getCmp('tf.total').getValue() == 0){
							Ext.getCmp('chb.plafond').setValue(1);
						}else{
							Ext.getCmp('chb.plafond').setValue(0);
						}
					}
					cekTombol();
				}

			}
		},{
			dataIndex: 'kditem',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idjnstarif',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idtarifpaketdet',
			hidden: true,
			hideable: false
		}],
		bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 545,
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.dijamin', text: 'Jumlah Dijamin :', margins: '4 10 0 70',
					},{
						xtype: 'numericfield',
						id: 'tf.dijamintotalrf',
						value: 0,
						width: 90,
						readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					},{
						xtype: 'label', id: 'lb.totalrf', text: 'Jumlah Selisih :', margins: '4 10 0 5',
					},{
						xtype: 'numericfield',
						id: 'tf.totalrf',
						value: 0,
						width: 90,
						readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.uangr', text: 'Racik :', margins: '4 10 0 300',
					},{
						xtype: 'numericfield',
						id: 'tf.uangr',
						value: 0,
						width: 90,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								totalnota2();
								cekTombol();
							}
						}
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.diskonprsn2', text: 'Diskon % :', margins: '4 10 0 162',
					},{
						xtype: 'numericfield',
						id: 'tf.diskonprsn',
						value: 0,
						width: 45,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								var sumtot = Ext.getCmp('tf.totalrf').getValue() + Ext.getCmp('tf.uangr').getValue();
								var disc = (sumtot * (Ext.getCmp('tf.diskonprsn').getValue() / 100));
								Ext.getCmp('tf.diskonf').setValue(disc);
								totalnota2();
								cekTombol();
							}
						}
					},{
						xtype: 'label', id: 'lb.diskon2', text: 'Diskon Rp :', margins: '4 10 0 2',
					},{
						xtype: 'numericfield',
						id: 'tf.diskonf',
						value: 0,
						width: 90,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								var sumtot = Ext.getCmp('tf.totalrf').getValue() + Ext.getCmp('tf.uangr').getValue();
								var disc = Ext.getCmp('tf.diskonf').getValue() / sumtot * 100;
								Ext.getCmp('tf.diskonprsn').setValue(disc);
								totalnota2();
								cekTombol();
							}
						}
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.chb', text: 'Plafond :', margins: '4 10 0 2',
					},{
						xtype: 'checkbox',
						id:'chb.plafond',
						// fieldLabel: 'Plafond',
						disabled: true,
						listeners: {
							check: function(checkbox, val){
								if(val == true){
									ds_nota.each(function (rec) {
										rec.set('dijaminprsn', 100); 
										rec.set('dijaminrp', rec.get('tarif2')); 
										rec.set('selisih', 0);
										rec.set('dijamin', true);
									});
									ds_nota2.each(function (rec) {
										rec.set('dijaminprsnf', 100); 
										rec.set('dijaminrpf', rec.get('tarif2')); 
										rec.set('selisihf', 0);
										rec.set('dijaminf', true);
									});
									totalnota();
									totalnota2();
								} else if(val == false){
									ds_nota.each(function (rec) {
										rec.set('dijaminprsn', 0); 
										rec.set('dijaminrp', 0); 
										rec.set('selisih', rec.get('tarif2'));
										rec.set('dijamin', false);
									});
									ds_nota2.each(function (rec) {
										rec.set('dijaminprsnf', 0); 
										rec.set('dijaminrpf', 0); 
										rec.set('selisihf', rec.get('tarif2'));
										rec.set('dijaminf', false);
									});
									totalnota();
									totalnota2();
								}
								cekTombol();
							}
						}
					},{
						xtype: 'label', id: 'lb.jdjmn', text: 'Total Plafond :', margins: '4 10 0 5',
					},{
						xtype: 'numericfield',
						id: 'tf.totalldijamin',
						value: 0,
						readOnly:true,
						style : 'opacity:0.6',
						width: 90,
						thousandSeparator:','
					},{
						xtype: 'label', id: 'lb.total2', text: 'Jumlah Total :', margins: '4 10 0 10',
					},{
						xtype: 'numericfield',
						id: 'tf.total2',
						value: 0,
						readOnly:true,
						style : 'opacity:0.6',
						width: 90,
						thousandSeparator:','
					}]
				}]
			}
		]
	});
	
	var paging_reg = new Ext.PagingToolbar({
		pageSize: 4,
		store: ds_vregistrasi,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 120
	})];
	
	var grid_reg = new Ext.grid.GridPanel({
		title: 'Registrasi',
		store: ds_vregistrasi,
		frame: true,
		height: 220,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_reg',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[{
			xtype: 'label', id: 'lb.tgl', text: 'Tgl :'
		},{
			xtype: 'datefield', id: 'df.tglreg',
			width: 100, value: new Date(),
			format: 'd-m-Y',
			listeners:{
				select: function(field, newValue){
					ds_vregistrasi.setBaseParam('ctglreg',Ext.util.Format.date(newValue, 'Y-m-d'));
					ds_vregistrasi.reload();
					Ext.getCmp('tf.norm').focus(false, 200);
				},
			}
		}],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_freg = ds_vregistrasi.getAt(rowIndex);
				var freg_noreg = rec_freg.data["noreg"];
				var freg_norm = rec_freg.data["norm"];
				var freg_nmpasien = rec_freg.data["nmpasien"];
				var freg_idbagian = rec_freg.data["idbagian"];
				var freg_nmbagian = rec_freg.data["nmbagian"];
				var freg_nmdokter = rec_freg.data["nmdoktergelar"];
				var freg_nonota = rec_freg.data["nonota"];
				var freg_nokuitansi = rec_freg.data["nokuitansi"];
				var freg_total = rec_freg.data["total"];
				var freg_diskonr = rec_freg.data["diskonr"];
				var freg_uangr = rec_freg.data["uangr"];
				var freg_idstpelayanan = rec_freg.data["idstpelayanan"];
				var freg_tglnota = rec_freg.data["tglnota"];
				var freg_jamnota = rec_freg.data["jamnota"];
				var freg_catatannota = rec_freg.data["catatannota"];
				var freg_tgllahirp = rec_freg.data["tgllahirp"];
				var freg_idjnskelamin = rec_freg.data["idjnskelamin"];
				var freg_penjamin = rec_freg.data["nmpenjamin"];
				var freg_foto_pasien = rec_freg.data["foto_pasien"];
				
				Ext.getCmp("tf.noreg").setValue(freg_noreg);
				Ext.getCmp("tf.norm").setValue(freg_norm);
				Ext.getCmp("tf.nmpasien").setValue(freg_nmpasien);
				Ext.getCmp("tf.upel").setValue(freg_nmbagian);
				Ext.getCmp("tf.dokter").setValue(freg_nmdokter);
				Ext.getCmp("tf.nonota").setValue(freg_nonota);
				Ext.getCmp("tf.penjamin").setValue(freg_penjamin);
				var pathimg = BASE_PATH + 'resources/img/camphoto/berkaspasien/'+freg_norm+'/'+ freg_foto_pasien;
				//Ext.getCmp('lb.photocamurl').setText("<a href='" + pathimg + "' target='_blank'>" + freg_foto_pasien + "</a>", false);
				Ext.getCmp("chb.plafond").enable();
				ds_nota.setBaseParam('idregdet',rec_freg.data["idregdet"]);
				ds_nota.setBaseParam('koder',0);
				ds_nota.setBaseParam('idbagian',null);
				ds_nota2.setBaseParam('idregdet',rec_freg.data["idregdet"]);
				ds_nota2.setBaseParam('koder',1);
				ds_nota2.setBaseParam('idbagian',null);
				pmbyrn = 0;
				pmbyrndjmn = 0;
				fTotal();
				if(freg_diskonr == null) Ext.getCmp('tf.diskonf').setValue(0);
				else Ext.getCmp('tf.diskonf').setValue(freg_diskonr);
				if(freg_uangr == null) Ext.getCmp('tf.uangr').setValue(0);
				else Ext.getCmp('tf.uangr').setValue(freg_uangr);
				if(freg_idstpelayanan == null) Ext.getCmp('cb.stpelayanan').setValue(1);
				else Ext.getCmp('cb.stpelayanan').setValue(freg_idstpelayanan);
				ds_vtarifall.setBaseParam('cidbagian',freg_idbagian);
				ds_vtarifall.setBaseParam('cklstarif',6);
				ds_vtarifall.reload();
				ds_brgbagian.setBaseParam('cidbagian',11);
				ds_brgbagian.reload();
				if(freg_catatannota == null)freg_catatannota ='Transaksi Rawat Jalan';
				Ext.getCmp('tf.catatan').setValue(freg_catatannota);
				Ext.getCmp('jkel').setValue(freg_idjnskelamin);
				if(freg_tglnota == null)//Ext.getCmp('df.tgl').setValue(new Date());
					Ext.Ajax.request({
						url: BASE_URL + 'nota_controller/get_tgl_svr',
						method: 'POST',
						params: {
						
						},
						success: function(response){
							obj = Ext.util.JSON.decode(response.responseText);
							Ext.getCmp('df.tgl').setValue(obj.date);
						},
						failure : function(){
							Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
						}
					});
				else Ext.getCmp('df.tgl').setValue(freg_tglnota);
				if(freg_jamnota == null){
					var formattedValue = Ext.util.Format.date(new Date(), 'H:i:s');
					Ext.getCmp('tf.jamrj').setValue(formattedValue);
					myStopFunction();
				} else {
					Ext.getCmp('tf.jamrj').setValue(freg_jamnota);
					myStopFunction();
				}
				umur(new Date(freg_tgllahirp));
				cekKartuStok();
				
            }
        },
		columns: [{
			header: 'No Reg',
			dataIndex: 'noreg',
			width: 75,
			sortable: true
		},{
			header: 'Pasien',
			dataIndex: 'nmpasien',
			width: 110,
			sortable: true
		},{
			header: 'L/P',
			dataIndex: 'kdjnskelamin',
			width: 30,
			sortable: true
		},{
			header: 'Unit Pelayanan',
			dataIndex: 'nmbagian',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Lahir',
			dataIndex: 'tgllahirp',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		}],
		bbar: paging_reg,
		plugins: cari_reg
	});
	
	var paging_tindakan = new Ext.PagingToolbar({
		pageSize: 4,
		store: ds_vtarifall,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_tindakan = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 120
	})];
	
	var grid_tindakan = new Ext.grid.GridPanel({
		title: 'Pemakaian Poli Klinik',
		store: ds_vtarifall,
		frame: true,
		height: 220,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_tindakan',
		forceFit: true,
		//sm: cbGrid,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var cek = true;
				var obj = ds_vtarifall.getAt(rowIndex);
				var skditem			= obj.data["kditem"];
				var snmitem			= obj.data["nmitem"];
				var starifjs		= obj.data["tarifjs"];
				var starifjm		= obj.data["tarifjm"];
				var starifjp		= obj.data["tarifjp"];
				var starifbhp		= obj.data["tarifbhp"];
				var sdiskonjs		= obj.data["diskonjs"];
				var sdiskonjm		= obj.data["diskonjm"];
				var sdiskonjp		= obj.data["diskonjp"];
				var sdiskonbhp		= obj.data["diskonbhp"];
				var starif			= obj.data["ttltarif"];
				var snmsatuan		= obj.data["satuankcl"];
				var dktr			= '';
				if(starifjm>0) dktr = Ext.getCmp('tf.dokter').getValue();
				
				ds_nota.each(function(rec){
					if(rec.get('kditem') == skditem) {
						Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
						cek = false;
					}
				});

				// CEK SET MAKS PEL	
				Ext.Ajax.request({
					url:BASE_URL + 'vregistrasi_controller/cekMaksPel',
					method:'POST',
					params: {
						norm		: Ext.getCmp('tf.norm').getValue(),
						kditem		: skditem
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						if(obj.validasi > 0){
							var arr = [];
							var ind = 0;
							Ext.MessageBox.alert('Informasi', 'Pasien tersebut telah mencapai jumlah maksimal treatment '+obj.nmpelayanan);
							// obj.arr.forEach(function(n) {
							// 	arr[ind] = n;
							// 	ind += 1;
							// });
							
						}
					}
				});
				
				if(cek){
					var orgaListRecord = new Ext.data.Record.create([
						{
							name: 'kditem',
							name: 'koder',
							name: 'nmitem',
							name: 'qty',
							name: 'tarif',
							name: 'tarif2',
							name: 'nmsatuan',
							name: 'tarifjs',
							name: 'tarifjm',
							name: 'tarifjp',
							name: 'tarifbhp',
							name: 'diskonjs',
							name: 'diskonjm',
							name: 'diskonjp',
							name: 'diskonbhp'
						}
					]);
					
					ds_nota.add([
						new orgaListRecord({
							'kditem': skditem,
							'koder': null,
							'nmitem': snmitem,
							'nmdoktergelar':dktr,
							'qty': 1,
							'tarif': starif,
							'tarif2': starif,
							'drp': 0,
							'dijaminprsn': 0,
							'dijaminrp': 0,
							'selisih': starif,
							'nmsatuan': snmsatuan,
							'tarifjs': starifjs,
							'tarifjm': starifjm,
							'tarifjp': starifjp,
							'tarifbhp': starifbhp,
							'diskonjs': sdiskonjs,
							'diskonjm': sdiskonjm,
							'diskonjp': sdiskonjp,
							'diskonbhp': sdiskonbhp
						})
					]);
					//tpd();
					//ds_vtarifall.removeAt(rowIndex);
					var ttl = Ext.getCmp('tf.total').getValue();
					var jml = parseFloat(ttl) + parseFloat(starif);
					Ext.getCmp('tf.total').setValue(jml);
					tottindakan = jml;
					cekTombol();
					Ext.getCmp('tf.notaqty').focus(false, 20);
					grid_nota.getView().focusRow(ds_nota.getCount() - 1);
				}
            }
        },
		columns: [{
			header: 'Item Tindakan',
			dataIndex: 'nmitem',
			width: 160,
			sortable: true
		},{
			header: 'Harga',
			dataIndex: 'ttltarif',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: 'Stok',
			dataIndex: 'stoknowbagian',
			hidden: true,
			align:'right',
			width: 45,
			sortable: true
		}],
		bbar: paging_tindakan,
		plugins: cari_tindakan
	});
	
	var paging_farmasi = new Ext.PagingToolbar({
		pageSize: 4,
		store: ds_brgbagian,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_farmasi = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 120
	})];
	
	var grid_farmasi = new Ext.grid.GridPanel({
		title: 'Apotek',
		store: ds_brgbagian,
		frame: true,
		height: 220,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_farmasi',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		tbar:[],
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var obj = ds_brgbagian.getAt(rowIndex);
				var skdbrg			= obj.data["kdbrg"];
				var snmbrg			= obj.data["nmbrg"];
				var starif			= obj.data["tarif"];
				var snmsatuan		= obj.data["nmsatuan"];
				koder++;
				
				var orgaListRecord = new Ext.data.Record.create([
					{
						name: 'kditem',
						name: 'koder',
						name: 'nmitem',
						name: 'qty',
						name: 'tarif',
						name: 'tarif2',
						name: 'nmsatuan'
					}
				]);
				
				ds_nota2.add([
					new orgaListRecord({
						'kditem': skdbrg,
						'koder': koder,
						'nmitem': snmbrg,
						'qty': 1,
						'tarif': starif,
						'tarif2': starif,
						'nmsatuan': snmsatuan,
						'dijaminprsnf': 0,
						'dijaminrpf': 0,
						'selisihf': starif
					})
				]);
				var ttl = Ext.getCmp('tf.totalrf').getValue();
				var jml = parseFloat(ttl) + parseFloat(starif);
				var jmluangr = Ext.getCmp("tf.uangr").getValue();
				var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
				var sumtotal = jml + jmluangr;
				var sumtotaldisk = sumtotal - jmldiskon;
				Ext.getCmp('tf.totalrf').setValue(jml);
				Ext.getCmp('tf.total2').setValue(sumtotaldisk);
				totfarmasi = sumtotaldisk;
				cekTombol();
				grid_notafarmasi.getView().focusRow(ds_nota2.getCount() - 1);
            }
        },
		columns: [{
			header: 'Nama Obat/Alkes',
			dataIndex: 'nmbrg',
			width: 160,
			sortable: true
		},{
			header: 'Harga',
			dataIndex: 'tarif',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: 'Stok',
			dataIndex: 'stoknowbagian',
			align:'right',
			width: 45,
			sortable: true
		}],
		bbar: paging_farmasi,
		plugins: cari_farmasi
	});
	
	var rjtransaksi_form = new Ext.form.FormPanel({
		id: 'fp.rjtransaksi',
		//title: 'Nota Transaksi RJ',
		width: 900,
		Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			{ text: 'Baru', iconCls: 'silk-add', handler: function(){bersihRjtransaksi();} },'-',
			{ text: 'Simpan', id:'btn.simpan', iconCls: 'silk-save', disabled:false, handler: function(){simpanRJT();} },'-',
			{ text: 'Cetak Nota (Grup)', id:'btn.cetak', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRJT();} },'-',
			{ text: 'Cetak Nota (Pisah)', id:'btn.cetakpsh', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRJPsh();} },'-',
			{
						xtype: 'label', margins: '0 0 0 25',
						id: 'lb.photocamurl', text: ''
					},
			{ xtype: 'tbfill' }
		],
        items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:125,
				boxMaxHeight:125,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. RM',
						id: 'tf.norm',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						}
					},{
						xtype: 'label', id: 'lb.noreg', text: 'No. Registrasi', margins: '0 10 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.noreg',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						}
					}]
				},{
					xtype: 'textfield', fieldLabel:'Nama Pasien',
					id: 'tf.nmpasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Unit Pelayanan',
					id: 'tf.upel',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Dokter',
					id: 'tf.dokter',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:125,
				boxMaxHeight:125,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'textfield', fieldLabel:'No. Nota',
					id: 'tf.nonota',
					readOnly: true, style : 'opacity:0.6',
					width: 100, hidden: false
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl./Jam/Shift', id: 'df.tgl',
						width: 100, value: new Date(),
						format: 'd-m-Y',
					},{
						xtype: 'label', id: 'lb.garing3', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jamrj', width: 65
					},{
						xtype: 'label', id: 'lb.garing4', text: '/'
					},{
						xtype: 'textfield', id: 'tf.shift', 
						width: 60, disabled: true
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Catatan',
					id: 'tf.catatan', anchor: "100%", value: 'Transaksi Rawat Jalan'
				},{
					xtype: 'textfield', fieldLabel:'Penjamin',
					id: 'tf.penjamin',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'combo', fieldLabel : 'Status Pelayanan',
					id: 'cb.stpelayanan', anchor: "100%",
					store: ds_stpelayanan, valueField: 'idstpelayanan', displayField: 'nmstpelayanan',
					editable: false, triggerAction: 'all', allowBlank: false,
					forceSelection: true, submitValue: true, mode: 'local',
					emptyText:'Pilih...', value: 'Umum', hidden: true
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_nota]
				}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_notafarmasi]
				}]
		},{
			xtype: 'textfield',
			id: 'tahun', hidden: true
		},{
			xtype: 'textfield',
			id: 'jkel', hidden: true
		}]
	});
	
	var transRJDispPanel = new Ext.form.FormPanel({
		id: 'fp.transRJDispPanel',
		name: 'fp.transRJDispPanel',
		border: false, 
		forceFit:true,
		frame: true,
		autoScroll:true,
		labelAlign: 'top',
		layout: 'anchor',
		items: [
		{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_reg]
		},{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_tindakan]
		},{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_farmasi]
		}]
	});
	
	var transRJPanel = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Transaksi Rawat Jalan',
		frame: false,
		defaults: {
			//anchor: '-10'
		},
		border: true,
		margins: '0 0 5 0',
		plain: true,		
		forceFit: true,
		layout: 'border', //'column',
		items: [{
			region: 'center',
			xtype: 'panel',
			border: true,
			layout: 'fit',
			items: [rjtransaksi_form],
			//autoScroll: true
		},
		{
			region: 'east',
			xtype: 'panel',
			title: '', border: true,
			layout:'fit', width:350,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [transRJDispPanel],
		}],
		listeners: {
			afterrender: function () {
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/get_tgl_svr',
					method: 'POST',
					params: {
					
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp('df.tgl').setValue(obj.date);
					},
					failure : function(){
						Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
					}
				});
			}
		}
	});
	
	SET_PAGE_CONTENT(transRJPanel);
	
	function bersihRjtransaksi() {
		Ext.getCmp('tf.noreg').setValue();
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue();
		Ext.getCmp('tf.upel').setValue();
		Ext.getCmp('tf.dokter').setValue();
		Ext.getCmp('tf.nonota').setValue();
		//Ext.getCmp('df.tgl').setValue(new Date());
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/get_tgl_svr',
			method: 'POST',
			params: {
			
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp('df.tgl').setValue(obj.date);
			},
			failure : function(){
				Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
			}
		});
		Ext.getCmp('df.tglreg').setValue(new Date());
		Ext.getCmp('tf.catatan').setValue('Transaksi Rawat Jalan');
		Ext.getCmp('tf.penjamin').setValue();
		Ext.getCmp('tahun').setValue();
		Ext.getCmp('jkel').setValue();
		Ext.getCmp('cb.stpelayanan').setValue(1);
		//Ext.getCmp('btn.simpan').disable();
		Ext.getCmp('btn.cetak').disable();
		Ext.getCmp('btn.cetakpsh').disable();
		Ext.getCmp('tf.totaldijamin').setValue(0);
		Ext.getCmp('tf.total').setValue(0);
		Ext.getCmp('tf.uangr').setValue(0);
		Ext.getCmp('tf.diskonf').setValue(0);
		Ext.getCmp('tf.diskonprsn').setValue(0);
		Ext.getCmp('tf.totalrf').setValue(0);
		Ext.getCmp('tf.total2').setValue(0);
		pmbyrn = 0;
		pmbyrndjmn = 0;
		cekpenj = 0;
		koder = 0;
		ds_nota.setBaseParam('idregdet',null);
		ds_nota.setBaseParam('idbagian',999);
		ds_nota.reload();
		ds_nota2.setBaseParam('idregdet',null);
		ds_nota2.setBaseParam('idbagian',999);
		ds_nota2.setBaseParam('koder',null);
		ds_nota2.reload();
		ds_vregistrasi.setBaseParam('cek','RJ');
		ds_vregistrasi.setBaseParam('ctglreg',Ext.util.Format.date(new Date(), 'Y-m-d'));
		ds_vregistrasi.setBaseParam('groupby','noreg');
		ds_vregistrasi.reload();
		ds_vtarifall.setBaseParam('cidbagian',0);
		ds_vtarifall.reload();
		ds_brgbagian.setBaseParam('cidbagian',0);
		ds_brgbagian.reload();
		fTotal();
		fTotal2();
		myStopFunction();
		myVar = setInterval(function(){myTimer()},1000);
	}
	
	function fTotal(){
		ds_nota.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0;
				dijaminrp = 0;
				ds_nota.each(function (rec) {
					sum += parseFloat(rec.get('selisih'));
					dijaminrp += parseFloat(rec.get('dijaminrp'));
					var djmn = (rec.get('dijamin')==1)?true:false;
					rec.set('dijamin', djmn);
				});
				Ext.getCmp("tf.total").setValue(sum);
				Ext.getCmp("tf.totaldijamin").setValue(dijaminrp);
				tottindakan = sum;
				fTotal2();
			}
		});
	}
	
	function fTotal2(){
		ds_nota2.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0;
				dijaminrp = 0;
				koder = 0;
				ds_nota2.each(function (rec) {
					sum += parseFloat(rec.get('selisihf'));
					dijaminrp += parseFloat(rec.get('dijaminrp'));
					var djmn = (rec.get('dijamin')==1)?true:false;
					rec.set('dijaminf', djmn);
					if(koder < rec.get('koder')) koder = rec.get('koder');
				});
				var jmluangr = Ext.getCmp("tf.uangr").getValue();
				var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
				var sumtotal = sum + jmluangr;
				var sumtotaldisk = sumtotal - jmldiskon;
				var sumtotaljamin = dijaminrp + jmluangr;
				Ext.getCmp("tf.totalrf").setValue(sum);
				Ext.getCmp("tf.total2").setValue(sumtotaldisk);
				totfarmasi = sumtotaldisk;
				if(dijaminrp > 0){
					Ext.getCmp("tf.dijamintotalrf").setValue(sumtotaljamin);
					if(Ext.getCmp("tf.totalrf").getValue() == 0)Ext.getCmp("tf.total2").setValue('0');					
				}else{
					Ext.getCmp("tf.dijamintotalrf").setValue(dijaminrp);
				}

				var disc = Ext.getCmp('tf.diskonf').getValue() / sumtotal * 100;
				Ext.getCmp('tf.diskonprsn').setValue(disc);
			}
		});
	}
	
	function simpanRJT(){
		/* if(Ext.getCmp('tf.jumlah').getValue() != (Ext.getCmp('tf.tottagihan').getValue() + Ext.getCmp('tf.ttldijamin').getValue())){
			Ext.MessageBox.alert('Informasi', 'Total Bayar tidak sama dengan Jumlah Total');
		} else { */
			var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
			var arrnota = [];
			var arrcarabayar = [];
			var notlength = ds_nota.data.items.length;
			var notlength2 = ds_nota2.data.items.length;
			var zx = 0;
			
			for (zx; zx < notlength; zx++) {
				var record = ds_nota.data.items[zx].data;
				if(record.nmdoktergelar == null || record.nmdoktergelar == ''){
					var dr = '';
				} else {
					var arrdr = record.nmdoktergelar;
					var dr = arrdr.split(',', 1);
				}
				zkditem = record.kditem;
				zqty = record.qty;
				zkoder = null;
				ztarif2 = record.tarif2;
				zdiskonjs = record.diskonjs;
				zdiskonjm = record.diskonjm;
				zdiskonjp = record.diskonjp;
				zdiskonbhp = record.diskonbhp;
				zdijamin = record.dijaminrp;
				zdokter = dr;
				arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp+'-'+zdokter+'-'+zdijamin;
			}
			
			for (var zy = 0; zy < notlength2; zy++) {
				zx++;
				var record = ds_nota2.data.items[zy].data;
				zkditem = record.kditem;
				zqty = record.qty;
				zkoder = record.koder;
				ztarif2 = record.tarif2;
				zdiskonjs = 0;
				zdiskonjm = 0;
				zdiskonjp = 0;
				zdiskonbhp = 0;
				zdijamin = record.dijaminrpf;
				zdokter = null;
				arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp+'-'+zdokter+'-'+zdijamin;
			}
			
			Ext.Ajax.request({
				url: BASE_URL + 'rawatjalan_controller/insorupd_nota',
				params: {
					noreg		: Ext.getCmp('tf.noreg').getValue(),
					nonota		: Ext.getCmp('tf.nonota').getValue(),
					nmshift 	: Ext.getCmp('tf.shift').getValue(),
					catatan 	: Ext.getCmp('tf.catatan').getValue(),
					// catatandskn	: Ext.getCmp('tf.catatandskn').getValue(),
					nmpasien 	: Ext.getCmp('tf.nmpasien').getValue(),
					// total	 	: Ext.getCmp('tf.jumlah').getValue(),
					// pembayaran 	: Ext.getCmp('tf.jumlah').getValue(),
					uangr	 	: Ext.getCmp('tf.uangr').getValue(),
					diskon	 	: Ext.getCmp('tf.diskonf').getValue(),
					// atasnama 	: Ext.getCmp('tf.an').getValue(),
					stpelayanan	: Ext.getCmp('cb.stpelayanan').lastSelectionText,
					tglnota		: Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'),
					jamnota		: Ext.getCmp('tf.jamrj').getValue(),
					tahun		: Ext.getCmp('tahun').getValue(),
					jkel		: Ext.getCmp('jkel').getValue(),
					jpel 		: 'Rawat Jalan',
					ureg 		: 'RJ',
					jtransaksi	: 2,
					idbagianfar	: 11,
					ttlplafond 	: Ext.getCmp('tf.totalldijamin').getValue(),
					plafond		: Ext.getCmp('chb.plafond').getValue(),
					stposisipasien: 4,
					idklstarif : 5, // Allroom di table kelas tarif
					arrnota		: Ext.encode(arrnota),
				},
				success: function(response){
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
					obj = Ext.util.JSON.decode(response.responseText);
					Ext.getCmp("tf.nonota").setValue(obj.nonota);
					Ext.getCmp("btn.cetak").enable();
					Ext.getCmp("btn.cetakpsh").enable();
					ds_vregistrasi.reload();
					myStopFunction();
				},
				failure: function (form, action) {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			});
		//}
	}
	
	function hapusItem(){
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/delete_bnotadet',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue(),
				nonota		: Ext.getCmp('tf.nonota').getValue(),
				arr			:  Ext.encode(arr)
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
				bersihRjtransaksi();
			},
			failure: function() {
				//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
			}
		});
	}
	
	function cetakRJT(){
		var nonota		= Ext.getCmp('tf.nonota').getValue();
		var noreg		= Ext.getCmp('tf.noreg').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/nota_rj/'
                +nonota+'/'+noreg);
	}
	
	function cetakRJPsh(){
		var nonota		= Ext.getCmp('tf.nonota').getValue();
		var noreg		= Ext.getCmp('tf.noreg').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/nota_psh/'
                +nonota+'/'+noreg);
	}
	
	function totalnota(){
		var zzz = 0;
		var dijaminnota = 0;
		for (var zxc = 0; zxc <ds_nota.data.items.length; zxc++) {
			var record = ds_nota.data.items[zxc].data;
			//zzz += parseFloat(record.tarif2);
			zzz += parseFloat(record.selisih);
			dijaminnota += parseFloat(record.dijaminrp);
		}
		Ext.getCmp('tf.total').setValue(zzz);
		tottindakan = zzz;
		Ext.getCmp('tf.totaldijamin').setValue(dijaminnota);
	}
	
	function totalnota2(){
		var zzz = 0;
		var dijaminnotaf = 0;
		for (var zxc = 0; zxc <ds_nota2.data.items.length; zxc++) {
			var record = ds_nota2.data.items[zxc].data;
			zzz += parseFloat(record.selisihf);
			dijaminnotaf += parseFloat(record.dijaminrpf);
		}
		var jmluangr = Ext.getCmp("tf.uangr").getValue();
		var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
		var sumtotal = zzz + jmluangr;
		var sumtotaldisk = sumtotal - jmldiskon;
		var sumtotaljamin = dijaminnotaf + jmluangr;
		totfarmasi = sumtotaldisk;
		Ext.getCmp('tf.totalrf').setValue(zzz);
		Ext.getCmp('tf.total2').setValue(sumtotaldisk);
		if(dijaminnotaf > 0){
			Ext.getCmp("tf.dijamintotalrf").setValue(dijaminnotaf);
			if(Ext.getCmp("tf.totalrf").getValue() == 0){
				Ext.getCmp("tf.total2").setValue(0);
				Ext.getCmp('tf.diskonf').setValue(0);
				Ext.getCmp("tf.dijamintotalrf").setValue(sumtotaljamin);
			}
		}else {
			Ext.getCmp("tf.dijamintotalrf").setValue(dijaminnotaf);
		}
		
		if(Ext.getCmp('chb.plafond').getValue() == true){
			var totjamin = Ext.getCmp('tf.totaldijamin').getValue() + sumtotaljamin;
			Ext.getCmp('tf.totalldijamin').setValue(totjamin);
			Ext.getCmp("tf.diskonf").disable();
			Ext.getCmp("tf.diskonprsn").disable();
		}else{			
			/*jmldisc = (sumtotal * (Ext.getCmp('tf.diskonprsn').getValue() / 100));
			Ext.getCmp('tf.diskonf').setValue(jmldisc);*/
			Ext.getCmp('tf.totalldijamin').setValue(0);
			Ext.getCmp("tf.diskonf").enable();
			Ext.getCmp("tf.diskonprsn").enable();
		}
	}

	function totalnota2diskon(){
		var zzz = 0;
		var dijaminnotaf = 0;
		for (var zxc = 0; zxc <ds_nota2.data.items.length; zxc++) {
			var record = ds_nota2.data.items[zxc].data;
			zzz += parseFloat(record.selisihf);
			dijaminnotaf += parseFloat(record.dijaminrpf);
		}
		var jmluangr = Ext.getCmp("tf.uangr").getValue();
		var sumtotal = zzz + jmluangr;
		var jmldisc = (sumtotal * (Ext.getCmp('tf.diskonprsn').getValue() / 100));
			Ext.getCmp('tf.diskonf').setValue(jmldisc);
		var sumtotaldisk = sumtotal - jmldisc;
		var sumtotaljamin = dijaminnotaf + jmluangr;
		totfarmasi = sumtotaldisk;
		Ext.getCmp('tf.totalrf').setValue(zzz);
		Ext.getCmp('tf.total2').setValue(sumtotaldisk);
		if(dijaminnotaf > 0){
			Ext.getCmp("tf.dijamintotalrf").setValue(dijaminnotaf);
			if(Ext.getCmp("tf.totalrf").getValue() == 0){
				Ext.getCmp("tf.total2").setValue(0);
				Ext.getCmp('tf.diskonf').setValue(0);
				Ext.getCmp("tf.dijamintotalrf").setValue(sumtotaljamin);
			}
		}else {
			Ext.getCmp("tf.dijamintotalrf").setValue(dijaminnotaf);
		}
		
		if(Ext.getCmp('chb.plafond').getValue() == true){
			var totjamin = Ext.getCmp('tf.totaldijamin').getValue() + sumtotaljamin;
			Ext.getCmp('tf.totalldijamin').setValue(totjamin);
			Ext.getCmp("tf.diskonf").disable();
			Ext.getCmp("tf.diskonprsn").disable();
		}else{
			Ext.getCmp('tf.totalldijamin').setValue(0);
			Ext.getCmp("tf.diskonf").enable();
			Ext.getCmp("tf.diskonprsn").enable();
		}
	}

	function umur(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tahun').setValue(year);
	}
	
	function cekTombol(){
		var total = Ext.getCmp('tf.total').getValue() + Ext.getCmp('tf.total2').getValue();
		var ttldijamin = Ext.getCmp('tf.totaldijamin').getValue() + Ext.getCmp('tf.dijamintotalrf').getValue();

		if((total+ttldijamin) == 0) total = 1;
		if(Ext.getCmp('tf.nonota').getValue() != ''){
			if((pmbyrn+pmbyrndjmn) >= (total+ttldijamin)){
				Ext.getCmp("btn.cetak").enable();
				Ext.getCmp("btn.cetakpsh").enable();
				if(cekkstok == 0)Ext.getCmp("btn.simpan").enable();
			} else {
				Ext.getCmp("btn.cetak").disable();
				Ext.getCmp("btn.cetakpsh").disable();
			}
		} else {
			if((pmbyrn+pmbyrndjmn) >= (total+ttldijamin)){
				Ext.getCmp("btn.cetak").disable();
				Ext.getCmp("btn.cetakpsh").disable();
				if(cekkstok == 0)Ext.getCmp("btn.simpan").enable();
			} else {
				Ext.getCmp("btn.cetak").disable();
				Ext.getCmp("btn.cetakpsh").disable();
			}
		}
	}
	
	function fDiskon(rowIndex){
		var rec_nota = ds_nota.getAt(rowIndex);
		var jumlahjs =0;
		var jumlahjm =0;
		var jumlahjp =0;
		var jumlahbhp =0;
		var tariftot =0;
		var dprsntot =0;
		var drptot =0;
		var jumlahtot =0;
		var dprsnjs = 0;
		var dprsnjm = 0;
		var dprsnjp = 0;
		var dprsnbhp = 0;
		
		var disk_form = new Ext.form.FormPanel({
			xtype:'form',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'rmight',
			bodyStyle: 'padding:10px 3px 3px 5px',
			monitorValid: true,
			height: 270, width: 400,
			layout: 'form', 
			frame: false,
			items: [{
				xtype: 'textfield', fieldLabel:'Nama',
				id: 'tf.nmitemd', width: 200,
				value: rec_nota.data.nmitem,
				disabled:true
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon Seluruh',
				items: [{
					xtype: 'textfield',
					id: 'tf.dprsnall', width: 40,
					thousandSeparator:',', value:0,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgPrsnAll();
						}
					}
				},{
					xtype: 'label', id: 'lb.dprsnall', text: '%', margins: '0 20 0 2'
				},{
					xtype: 'numericfield',
					id: 'tf.drpall', width: 70, value:0,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgRpAll();
						}
					}
				},{
					xtype: 'label', id: 'lb.drpall', text: 'Rupiah', margins: '0 0 0 2'
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Keterangan',
				items: [{
					xtype: 'numericfield',
					id: 'nm.tes',
					hidden:true
				},{
					xtype: 'label', id: 'lb.tarif', text: 'Tarif', margins: '0 10 0 20'
				},{
					xtype: 'label', id: 'lb.dprsn', text: '%', margins: '0 10 0 30'
				},{
					xtype: 'label', id: 'lb.drp', text: 'Rupiah', margins: '0 10 0 20'
				},{
					xtype: 'label', id: 'lb.jumlah', text: 'Jumlah', margins: '0 10 0 30'
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JS',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjs', width: 70,
					value: rec_nota.data.tarifjs * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'textfield',
					id: 'tf.dprsnjs', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'textfield',
					id: 'tf.drpjs', width: 70,
					value: rec_nota.data.diskonjs,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjs', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JM',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjm', width: 70,
					value: rec_nota.data.tarifjm * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'textfield',
					id: 'tf.dprsnjm', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'textfield',
					id: 'tf.drpjm', width: 70,
					value: rec_nota.data.diskonjm,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjm', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JP',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjp', width: 70,
					value: rec_nota.data.tarifjp * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'textfield',
					id: 'tf.dprsnjp', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'textfield',
					id: 'tf.drpjp', width: 70,
					value: rec_nota.data.diskonjp,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjp', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon BHP',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifbhp', width: 70,
					value: rec_nota.data.tarifbhp * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'textfield',
					id: 'tf.dprsnbhp', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'textfield',
					id: 'tf.drpbhp', width: 70,
					value: rec_nota.data.diskonbhp,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahbhp', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Total',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tariftot', width: 70,
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'textfield',
					id: 'tf.dprsntot', width: 40,
					disabled:true
				},{
					xtype: 'numericfield',
					id: 'tf.drptot', width: 70,
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahtot', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					DiskAdd();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wDisk.close();
				}
			}]
		});
			
		var wDisk = new Ext.Window({
			title: 'Diskon',
			modal: true, closable:false,
			items: [disk_form]
		});
		
		diskonAwal();
		wDisk.show();

		function DiskAdd(){
			var totaldiskon = Ext.getCmp('tf.drptot').getValue();
	//		var subtotal = (rec_nota.data.tarif * rec_nota.data.qty) -  Ext.getCmp('tf.drptot').getValue();
			var subtotal = (rec_nota.data.tarif * rec_nota.data.qty) -  Ext.getCmp('tf.drpall').getValue(); //Ext.getCmp('tf.drptot').getValue();
			var selisih = subtotal - rec_nota.get('dijaminrp');
			
			rec_nota.set('diskonjs', Ext.getCmp('tf.drpjs').getValue());
			rec_nota.set('diskonjm', Ext.getCmp('tf.drpjm').getValue());
			rec_nota.set('diskonjp', Ext.getCmp('tf.drpjp').getValue());
			rec_nota.set('diskonbhp', Ext.getCmp('tf.drpbhp').getValue());
			
			//rec_nota.set('drp', totaldiskon);
			rec_nota.set('drp', Ext.getCmp('tf.drpall').getValue());
			rec_nota.set('tarif2', subtotal);
			if(selisih<0){
				rec_nota.set('dijaminprsn', 100);
				rec_nota.set('dijaminrp', subtotal);
				rec_nota.set('selisih', 0);
			} else {
				var prsn = (rec_nota.get('dijaminrp') / subtotal) * 100;
				rec_nota.set('dijaminprsn', prsn);
				rec_nota.set('selisih', selisih);
			}
			
			totalnota();
			cekTombol();
			wDisk.close();
		}
	
		function diskonAwal(){
			if(rec_nota.data.tarifjs == '')rec_nota.data.tarifjs = 0;
			if(rec_nota.data.tarifjm == '')rec_nota.data.tarifjm = 0;
			if(rec_nota.data.tarifjp == '')rec_nota.data.tarifjp = 0;
			if(rec_nota.data.tarifbhp == '')rec_nota.data.tarifbhp = 0;
			
			if(rec_nota.data.diskonjs == undefined)rec_nota.data.diskonjs = 0;
			if(rec_nota.data.diskonjm == undefined)rec_nota.data.diskonjm = 0;
			if(rec_nota.data.diskonjp == undefined)rec_nota.data.diskonjp = 0;
			if(rec_nota.data.diskonbhp == undefined)rec_nota.data.diskonbhp = 0;
			
			jumlahjs = (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjs);
			jumlahjm = (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjm);
			jumlahjp = (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjp);
			jumlahbhp = (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonbhp);
			
			if(rec_nota.data.tarifjs != 0){
				dprsnjs = parseInt(rec_nota.data.diskonjs) / (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjs').setReadOnly(false);
				Ext.getCmp('tf.dprsnjs').enable();
				Ext.getCmp('tf.drpjs').setReadOnly(false);
				Ext.getCmp('tf.drpjs').enable();
			}
			if(rec_nota.data.tarifjm != 0){
				dprsnjm = parseInt(rec_nota.data.diskonjm) / (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjm').setReadOnly(false);
				Ext.getCmp('tf.dprsnjm').enable();
				Ext.getCmp('tf.drpjm').setReadOnly(false);
				Ext.getCmp('tf.drpjm').enable();
			}
			if(rec_nota.data.tarifjp != 0){
				dprsnjp = parseInt(rec_nota.data.diskonjp) / (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjp').setReadOnly(false);
				Ext.getCmp('tf.dprsnjp').enable();
				Ext.getCmp('tf.drpjp').setReadOnly(false);
				Ext.getCmp('tf.drpjp').enable();
			}
			if(rec_nota.data.tarifbhp != 0){
				dprsnbhp = parseInt(rec_nota.data.diskonbhp) / (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnbhp').setReadOnly(false);
				Ext.getCmp('tf.dprsnbhp').enable();
				Ext.getCmp('tf.drpbhp').setReadOnly(false);
				Ext.getCmp('tf.drpbhp').enable();
			}
			
			tariftot = (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty));
			dprsntot = dprsnjs + dprsnjm + dprsnjp + dprsnbhp;
			drptot = parseInt(rec_nota.data.diskonjs) + parseInt(rec_nota.data.diskonjm) + parseInt(rec_nota.data.diskonjp) + parseInt(rec_nota.data.diskonbhp);
			jumlahtot = jumlahjs + jumlahjm + jumlahjp + jumlahbhp;
			
			Ext.getCmp('tf.dprsnjs').setValue(dprsnjs);
			Ext.getCmp('tf.dprsnjm').setValue(dprsnjm);
			Ext.getCmp('tf.dprsnjp').setValue(dprsnjp);
			Ext.getCmp('tf.dprsnbhp').setValue(dprsnbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jumlahjs);
			Ext.getCmp('tf.jumlahjm').setValue(jumlahjm);
			Ext.getCmp('tf.jumlahjp').setValue(jumlahjp);
			Ext.getCmp('tf.jumlahbhp').setValue(jumlahbhp);
			
			Ext.getCmp('tf.tariftot').setValue(tariftot);
			Ext.getCmp('tf.dprsntot').setValue(dprsntot);
			Ext.getCmp('tf.drptot').setValue(drptot);
			Ext.getCmp('tf.jumlahtot').setValue(jumlahtot);
		}
		
		function htgDiskonPrsn(){
			var diskjs = Ext.getCmp('tf.tarifjs').getValue() * (Ext.getCmp('tf.dprsnjs').getValue() / 100);
			var diskjm = Ext.getCmp('tf.tarifjm').getValue() * (Ext.getCmp('tf.dprsnjm').getValue() / 100);
			var diskjp = Ext.getCmp('tf.tarifjp').getValue() * (Ext.getCmp('tf.dprsnjp').getValue() / 100);
			var diskbhp = Ext.getCmp('tf.tarifbhp').getValue() * (Ext.getCmp('tf.dprsnbhp').getValue() / 100);
			
			var jmljs = Ext.getCmp('tf.tarifjs').getValue() - diskjs;
			var jmljm = Ext.getCmp('tf.tarifjm').getValue() - diskjm;
			var jmljp = Ext.getCmp('tf.tarifjp').getValue() - diskjp;
			var jmlbhp = Ext.getCmp('tf.tarifbhp').getValue() - diskbhp;
			
			var hdprsntot = Ext.getCmp('tf.dprsnjs').getValue() + Ext.getCmp('tf.dprsnjm').getValue() + Ext.getCmp('tf.dprsnjp').getValue() + Ext.getCmp('tf.dprsnbhp').getValue();
			var hdrptot = diskjs + diskjm + diskjp + diskbhp;
			var hjumlahtot = jmljs + jmljm + jmljp + jmlbhp;
			
			Ext.getCmp('tf.drpjs').setValue(diskjs);
			Ext.getCmp('tf.drpjm').setValue(diskjm);
			Ext.getCmp('tf.drpjp').setValue(diskjp);
			Ext.getCmp('tf.drpbhp').setValue(diskbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jmljs);
			Ext.getCmp('tf.jumlahjm').setValue(jmljm);
			Ext.getCmp('tf.jumlahjp').setValue(jmljp);
			Ext.getCmp('tf.jumlahbhp').setValue(jmlbhp);
			
			Ext.getCmp('tf.dprsntot').setValue(hdprsntot);
			Ext.getCmp('tf.drptot').setValue(hdrptot);
			Ext.getCmp('tf.jumlahtot').setValue(hjumlahtot);
		}
		
		function htgPrsnAll(){
			var trfjs = Ext.getCmp('tf.tarifjs').getValue();
			var trfjm = Ext.getCmp('tf.tarifjm').getValue();
			var trfjp = Ext.getCmp('tf.tarifjp').getValue();
			var trfbhp = Ext.getCmp('tf.tarifbhp').getValue();
			var prsnall = Ext.getCmp('tf.dprsnall').getValue();
			
			if(trfjs>0) Ext.getCmp('tf.dprsnjs').setValue(prsnall);
			if(trfjm>0) Ext.getCmp('tf.dprsnjm').setValue(prsnall);
			if(trfjp>0) Ext.getCmp('tf.dprsnjp').setValue(prsnall);
			if(trfbhp>0) Ext.getCmp('tf.dprsnbhp').setValue(prsnall);
			
			var drpall = Ext.getCmp('tf.tariftot').getValue() * (prsnall / 100);
			Ext.getCmp('tf.drpall').setValue(drpall);
			htgDiskonPrsn();
		}
		
		function htgDiskonRp(){
			var diskjs = (Ext.getCmp('tf.drpjs').getValue() * 100) / Ext.getCmp('tf.tarifjs').getValue();
			var diskjm = (Ext.getCmp('tf.drpjm').getValue() * 100) / Ext.getCmp('tf.tarifjm').getValue();
			var diskjp = (Ext.getCmp('tf.drpjp').getValue() * 100) / Ext.getCmp('tf.tarifjp').getValue();
			var diskbhp = (Ext.getCmp('tf.drpbhp').getValue() * 100) / Ext.getCmp('tf.tarifbhp').getValue();
			
			if(isNaN(diskjs) || Ext.getCmp('tf.tarifjs').getValue() < Ext.getCmp('tf.drpjs').getValue()) diskjs = 0;
			if(isNaN(diskjm) || Ext.getCmp('tf.tarifjm').getValue() < Ext.getCmp('tf.drpjm').getValue()) diskjm = 0;
			if(isNaN(diskjp) || Ext.getCmp('tf.tarifjp').getValue() < Ext.getCmp('tf.drpjp').getValue()) diskjp = 0;
			if(isNaN(diskbhp) || Ext.getCmp('tf.tarifbhp').getValue() < Ext.getCmp('tf.drpbhp').getValue()) diskbhp = 0;
			
			var jmljs = Ext.getCmp('tf.tarifjs').getValue() - Ext.getCmp('tf.drpjs').getValue();
			var jmljm = Ext.getCmp('tf.tarifjm').getValue() - Ext.getCmp('tf.drpjm').getValue();
			var jmljp = Ext.getCmp('tf.tarifjp').getValue() - Ext.getCmp('tf.drpjp').getValue();
			var jmlbhp = Ext.getCmp('tf.tarifbhp').getValue() - Ext.getCmp('tf.drpbhp').getValue();
			
			var hdrptot = Ext.getCmp('tf.drpjs').getValue() + Ext.getCmp('tf.drpjm').getValue() + Ext.getCmp('tf.drpjp').getValue() + Ext.getCmp('tf.drpbhp').getValue();
			var hdprsntot = diskjs + diskjm + diskjp + diskbhp;
			var hjumlahtot = jmljs + jmljm + jmljp + jmlbhp;
			
			Ext.getCmp('tf.dprsnjs').setValue(diskjs);
			Ext.getCmp('tf.dprsnjm').setValue(diskjm);
			Ext.getCmp('tf.dprsnjp').setValue(diskjp);
			Ext.getCmp('tf.dprsnbhp').setValue(diskbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jmljs);
			Ext.getCmp('tf.jumlahjm').setValue(jmljm);
			Ext.getCmp('tf.jumlahjp').setValue(jmljp);
			Ext.getCmp('tf.jumlahbhp').setValue(jmlbhp);
			
			Ext.getCmp('tf.dprsntot').setValue(hdprsntot);
			Ext.getCmp('tf.drptot').setValue(hdrptot);
			Ext.getCmp('tf.jumlahtot').setValue(hjumlahtot);
		}
	
		function htgRpAll(){
			var trfjs = Ext.getCmp('tf.tarifjs').getValue();
			var trfjm = Ext.getCmp('tf.tarifjm').getValue();
			var trfjp = Ext.getCmp('tf.tarifjp').getValue();
			var trfbhp = Ext.getCmp('tf.tarifbhp').getValue();
			var drpall = Ext.getCmp('tf.drpall').getValue();
						
			var dprsnall = (drpall * 100) / Ext.getCmp('tf.tariftot').getValue();
			
			if(trfjs>0) Ext.getCmp('tf.dprsnjs').setValue(dprsnall);
			if(trfjm>0) Ext.getCmp('tf.dprsnjm').setValue(dprsnall);
			if(trfjp>0) Ext.getCmp('tf.dprsnjp').setValue(dprsnall);
			if(trfbhp>0) Ext.getCmp('tf.dprsnbhp').setValue(dprsnall);
			
			Ext.getCmp('tf.dprsnall').setValue(dprsnall);
			htgDiskonPrsn();
		}
	
	}
	
	function cekKartuStok(){
		if(Ext.getCmp("tf.nonota").getValue() != ''){
			Ext.Ajax.request({
				url:BASE_URL + 'kartustok_controller/getCekRJ',
				method:'POST',
				params:{
					nonota : Ext.getCmp("tf.nonota").getValue()
				},
				success: function(response){
					obj = Ext.util.JSON.decode(response.responseText)
					cekkstok = obj.cek;
				}
			});
		}
	}
	
	function jasaMedis(value, a, b){
		if(b.data.tarifjm > 0) return '<font color="red">'+ value +'</font>';
		else return value;
	}
	
}