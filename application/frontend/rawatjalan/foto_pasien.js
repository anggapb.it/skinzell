function foto_pasien() {
    var ds_jkelamin = dm_jkelamin();

    var ds_dokumenpasien = new Ext.data.JsonStore({
        proxy: new Ext.data.HttpProxy({
            url: BASE_URL + 'dokumenpasien_controller/get_dokumen',
            method: 'POST'
        }),
        totalProperty: 'results',
        root: 'data',
        autoLoad: true,
        fields: [{
            name: 'nmdokumen',
            mapping: 'nmdokumen'
        }, {
            name: 'pathdokumen',
            mapping: 'pathdokumen'
        }, {
            name: 'urldokumen',
            mapping: 'urldokumen'
        }, {
            name: 'tglupload',
            mapping: 'tglupload'
        }]
    });

    var grid_dokumenpasien = new Ext.grid.GridPanel({
        store: ds_dokumenpasien,
        frame: true,
        height: 250,
        bodyStyle: 'padding:3px 3px 3px 3px',
        id: 'grid_dokumenpasien',
        forceFit: true,
        autoScroll: true,
        autoSizeColumns: true,
        enableColumnResize: true,
        enableColumnHide: false,
        enableColumnMove: false,
        enableHdaccess: false,
        columnLines: true,
        loadMask: true,
        columns: [{
            header: 'Foto',
            dataIndex: 'nmdokumen',
            width: 450,
            sortable: true
        }, {
            header: 'Tgl. Upload',
            dataIndex: 'tglupload',
            renderer: Ext.util.Format.dateRenderer('d-m-Y H:i:s'),
            width: 150,
            sortable: true
        }, {
            xtype: 'actioncolumn',
            id: 'download',
            width: 60,
            header: 'Download',
            align: 'center',
            items: [{
                getClass: function (v, meta, record) {
                    meta.attr = "style='cursor:pointer;'";
                },
                icon: 'application/framework/img/eye.png',
                tooltip: 'Lihat dokumen',
                handler: function (grid, rowIndex) {
                    var record = ds_dokumenpasien.getAt(rowIndex);
                    var urldokumen = record.data.urldokumen;
                    RH.ShowReport(urldokumen);
                }
            }]
        }, {
            xtype: 'actioncolumn',
            id: 'delete_dokumen',
            width: 60,
            header: 'Hapus',
            align: 'center',
            items: [{
                getClass: function (v, meta, record) {
                    meta.attr = "style='cursor:pointer;'";
                },
                icon: 'application/framework/img/rh_delete.gif',
                tooltip: 'Hapus dokumen',
                handler: function (grid, rowIndex) {
                    Ext.Msg.show({
                        title: 'Konfirmasi',
                        msg: 'Apakah Anda Yakin Akan Menghapus Dokumen Ini..?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.MessageBox.QUESTION,
                        fn: function (response) {
                            if ('yes' == response) {
                                var record = ds_dokumenpasien.getAt(rowIndex);

                                Ext.Ajax.request({
                                    url: BASE_URL + 'dokumenpasien_controller/delete_dokumen',
                                    method: 'POST',
                                    params: {
                                        nmdokumen: record.data.nmdokumen,
                                        pathdokumen: record.data.pathdokumen,
                                        urldokumen: record.data.urldokumen,
                                        tglupload: record.data.tglupload,
                                    },
                                    success: function (response) {
                                        obj = Ext.util.JSON.decode(response.responseText);
                                        Ext.Msg.alert("Info", "Hapus Data Berhasil");
                                        ds_dokumenpasien.reload();
                                    },
                                    failure: function (result) {
                                        Ext.MessageBox.alert("Info", "Hapus Data Gagal");
                                    }
                                });
                            }
                        }
                    });
                }
            }]
        }]
    });


    var rmberkaspasien_form = new Ext.form.FormPanel({
        id: 'fp.dokumenpasien',
        title: 'Riwayat Foto Pasien',
        fileUpload: true,
        width: 900, Height: 1000,
        layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
        frame: true,
        autoScroll: true,
        tbar: [
           // { text: 'Baru', iconCls: 'silk-add', handler: function () { clearform() } }, '-',
           // { text: 'Upload Dokumen', id: 'bt.upload', iconCls: 'silk-save', handler: function () { upload_dokumen(); } },
        ],
        defaults: { labelWidth: 120, labelAlign: 'right' },
        items: [{
            id: 'fs.pasien', layout: 'column', autowidth: true,
            defaults: { labelWidth: 120, labelAlign: 'right' },
            items: [{
                ////COLUMN 1
                layout: 'form', columnWidth: 1,
                xtype: 'fieldset', title: 'Data Pasien',
                style: 'margin-bottom:15px; margin-right:15px;',
                height: 200,
                items: [{
                    xtype: 'compositefield',
                    fieldLabel: 'No. RM ',
                    id: 'comp_norm',
                    items: [{
                        xtype: 'textfield',
                        id: 'tf.norm',
                        allowBlank: true,
                        width: 100,
                        maskRe: /[0-9.]/,
                        autoCreate: {
                            tag: "input",
                            maxlength: 10,
                            type: "text",
                            size: "20",
                            autocomplete: "off"
                        },
                        enableKeyEvents: true,
                        listeners: {
                            specialkey: function (field, e) {
                                if (e.getKey() == e.ENTER) {
                                    dataPasien(1);
                                }
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: ' ... ',
                        id: 'btn.norm',
                        width: 30,
                        handler: function () {
                            dftPasien();
                        }
                    }, ]
                }, {
                    xtype: 'container', fieldLabel: 'Nama Pasien ',
                    layout: 'hbox', items: [{
                        xtype: 'textfield',
                        id: 'tf.nmpasien',
                        width: 300, readOnly: true,
                    }]
                }, {
                    xtype: 'container', fieldLabel: 'Jenis Kelamin ',
                    layout: 'hbox', items: [
                    {
                        xtype: 'combo',
                        id: 'cb.jkelamin', width: 100,
                        store: ds_jkelamin, valueField: 'idjnskelamin', displayField: 'nmjnskelamin',
                        triggerAction: 'all',
                        forceSelection: true, submitValue: true, mode: 'local',
                        allowBlank: false, readOnly: true,
                    }]
                }, {
                    xtype: 'container', fieldLabel: 'Umur ',
                    layout: 'hbox',
                    items: [{
                        xtype: 'textfield', id: 'tf.umurthn', readOnly: true, allowBlank: false,
                        width: 30
                    }, {
                        xtype: 'label', id: 'lb.umurthn', text: 'Tahun', margins: '0 10 0 5',
                    }, {
                        xtype: 'textfield', id: 'tf.umurbln', readOnly: true, allowBlank: false,
                        width: 30
                    }, {
                        xtype: 'label', id: 'lb.umurbln', text: 'Bulan', margins: '0 10 0 5',
                    }, {
                        xtype: 'textfield', id: 'tf.umurhari', readOnly: true, allowBlank: false,
                        width: 30
                    }, {
                        xtype: 'label', id: 'lb.umurhari', text: 'Hari', margins: '0 10 0 5'
                    }]
                }, ]
            }, {
                ////COLUMN 2
                layout: 'form', columnWidth: 0.49,
                hidden:true,
                xtype: 'fieldset', title: 'Upload Dokumen',
                height: 200,
                items: [
                {
                    fieldLabel: 'Dokumen 1 ',
                    xtype: 'fileuploadfield',
                    id: 'dokumen1',
                    name: 'upload_dokumen1',
                    emptyText: '.jpg, .png, .pdf',
                    width: 150,
                    buttonText: 'browse'
                },
                {
                    fieldLabel: 'Dokumen 2 ',
                    xtype: 'fileuploadfield',
                    id: 'dokumen2',
                    name: 'upload_dokumen2',
                    emptyText: '.jpg, .png, .pdf',
                    width: 150,
                    buttonText: 'browse'
                },
                {
                    fieldLabel: 'Dokumen 3 ',
                    xtype: 'fileuploadfield',
                    id: 'dokumen3',
                    name: 'upload_dokumen3',
                    emptyText: '.jpg, .png, .pdf',
                    width: 150,
                    buttonText: 'browse'
                },
                {
                    fieldLabel: 'Dokumen 4 ',
                    xtype: 'fileuploadfield',
                    id: 'dokumen4',
                    name: 'upload_dokumen4',
                    emptyText: '.jpg, .png, .pdf',
                    width: 150,
                    buttonText: 'browse'
                },
                {
                    fieldLabel: 'Dokumen 5 ',
                    xtype: 'fileuploadfield',
                    id: 'dokumen5',
                    name: 'upload_dokumen5',
                    emptyText: '.jpg, .png, .pdf',
                    width: 150,
                    buttonText: 'browse'
                },
                {
                    xtype: 'label',
                    style: 'color:red;margin-left:125px;',
                    //margins: '100 100 100 100',
                    text: 'Maksimal 5 dokumen dalam sekali upload',
                }]
            }]
        }, {
            xtype: 'fieldset', layout: 'form',
            //title: 'Riwayat Berkas Pasien',
            items: [grid_dokumenpasien]
        }]
    }); 
    
    SET_PAGE_CONTENT(rmberkaspasien_form);

    function dataPasien(kode) {
        var dpnorm = Ext.getCmp('tf.norm').getValue();
        Ext.Ajax.request({
            url: BASE_URL + 'pasien_controller/getDataPasien',
            params: {
                norm: dpnorm,
            },
            success: function (response) {
                obj = Ext.util.JSON.decode(response.responseText);
                obj.norm = obj.norm;

                Ext.getCmp('tf.norm').focus();
                Ext.getCmp("tf.norm").setValue(obj.norm);
                Ext.getCmp("tf.nmpasien").setValue(obj.nmpasien);
                Ext.getCmp("cb.jkelamin").setValue(obj.idjnskelamin);
                umur(new Date(obj.tgllahir));

                ds_dokumenpasien.setBaseParam('norm', obj.norm);
                ds_dokumenpasien.reload();
                Ext.getCmp('dokumen1').setValue('');
                Ext.getCmp('dokumen2').setValue('');
                Ext.getCmp('dokumen3').setValue('');
                Ext.getCmp('dokumen4').setValue('');
                Ext.getCmp('dokumen5').setValue('');
                
            },
            failure: function () {
            }
        });
    }


    function umur(val) {
        var date = new Date();
        var td = date.dateFormat('d'); var d = val.dateFormat('d');
        var tm = date.dateFormat('m'); var m = val.dateFormat('m');
        var ty = date.dateFormat('Y'); var y = val.dateFormat('Y');

        if (td - d < 0) {
            day = (parseInt(td) + 30) - d;
            tm--;
        }
        else {
            day = td - d;
        }
        if (tm - m < 0) {
            month = (parseInt(tm) + 12) - m;
            ty--;
        }
        else {
            month = tm - m;
        }
        year = ty - y;
        Ext.getCmp('tf.umurthn').setValue(year);
        Ext.getCmp('tf.umurbln').setValue(month);
        Ext.getCmp('tf.umurhari').setValue(day);
    }

    function dftPasien() {
        var ds_pasien = dm_pasien();

        function keyToView_pasien(value) {
            Ext.QuickTips.init();
            return '<div class="keyMasterPasien" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
                + value + '</div>';
        }

        var cm_cari_pasien = new Ext.grid.ColumnModel([
            {
                header: 'No RM',
                dataIndex: 'norm',
                width: 80,
                renderer: keyToView_pasien
            },{
                header: 'Nama Pasien',
                dataIndex: 'nmpasien',
                width: 150
            }, {
                header: 'L/P',
                dataIndex: 'kdjnskelamin',
                width: 30
            }, {
                header: 'Tgl. Lahir',
                dataIndex: 'tgllahir',
                renderer: Ext.util.Format.dateRenderer('d-m-Y'),
                width: 80
            }, {
                header: 'Alamat Pasien',
                dataIndex: 'alamat',
                width: 210
            }, {
                header: 'Nama Ibu',
                dataIndex: 'nmibu',
                width: 120
            }, {
                header: 'No. HP/Telp.',
                dataIndex: 'notelp',
                renderer: function (value, p, r) {
                    var nohptelp = r.data['nohp'] + ' / ' + r.data['notelp'];

                    return nohptelp;
                },
                width: 100
            }, {
                header: 'No. KTP/Paspor',
                dataIndex: 'noidentitas',
                width: 100
            }
        ]);
        var sm_cari_pasien = new Ext.grid.RowSelectionModel({
            singleSelect: true
        });
        var vw_cari_pasien = new Ext.grid.GridView({
            emptyText: '< Belum ada Data >'
        });
        var paging_cari_pasien = new Ext.PagingToolbar({
            pageSize: 100,
            store: ds_pasien,
            displayInfo: true,
            displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
            emptyMsg: 'No data to display'
        });
        var grid_find_cari_pasien = new Ext.grid.GridPanel({
            ds: ds_pasien,
            cm: cm_cari_pasien,
            sm: sm_cari_pasien,
            view: vw_cari_pasien,
            height: 350,
            width: 975,
            autoSizeColumns: true,
            enableColumnResize: true,
            enableColumnHide: false,
            enableColumnMove: false,
            enableHdaccess: false,
            columnLines: true,
            loadMask: true,
            buttonAlign: 'left',
            layout: 'anchor',
            anchorSize: {
                width: 400,
                height: 400
            },
            bbar: paging_cari_pasien,
            listeners: {
                cellclick: klik_cari_pasien
            }
        });
        var win_find_cari_pasien = new Ext.Window({
            title: 'Cari Pasien',
            modal: true,
            items: [{
                xtype: 'container',
                style: 'padding: 5px',
                layout: 'column',
                defaults: { labelWidth: 1, labelAlign: 'right' },
                items: [{
                    xtype: 'button',
                    text: 'Cari',
                    id: 'btn.cari',
                    iconCls: 'silk-find',
                    style: 'padding: 10px',
                    width: 100,
                    handler: function () {
                        cAdvance();
                    }
                }, {
                    xtype: 'button',
                    text: 'Kembali',
                    id: 'btn.kembali',
                    iconCls: 'silk-arrow-undo',
                    style: 'padding: 10px',
                    width: 110,
                    handler: function () {
                        win_find_cari_pasien.close();
                    }
                }]
            }, {
                xtype: 'container',
                style: 'padding: 5px',
                layout: 'column',
                defaults: { labelWidth: 1, labelAlign: 'right' },
                items: [{
                    xtype: 'fieldset',
                    columnWidth: .33,
                    border: false,
                    items: [{
                        xtype: 'compositefield',
                        id: 'comp_cnorm',
                        items: [{
                            xtype: 'checkbox',
                            id: 'chb.crm',
                            listeners: {
                                check: function (checkbox, val) {
                                    if (val == true) {
                                        Ext.getCmp('tf.crmlama').enable();
                                        Ext.getCmp('tf.crm').enable();
                                        Ext.getCmp('tf.crm').focus();
                                    } else if (val == false) {
                                        Ext.getCmp('tf.crmlama').disable();
                                        Ext.getCmp('tf.crm').disable();
                                        Ext.getCmp('tf.crm').setValue('');
                                    }
                                }
                            }
                        }, {
                            xtype: 'textfield',
                            id: 'tf.crm',
                            emptyText: 'No. RM',
                            width: 105, disabled: true,
                            enableKeyEvents: true,
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER) {
                                        cAdvance();
                                    }
                                }
                            }
                        }, {
                            xtype: 'textfield',
                            id: 'tf.crmlama',
                            emptyText: 'No. RM Lama',
                            hidden: true,
                            width: 110, disabled: true,
                            enableKeyEvents: true,
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER) {
                                        cAdvance();
                                    }
                                }
                            }
                        }]
                    }, {
                        xtype: 'compositefield',
                        id: 'comp_cnmpasien',
                        items: [{
                            xtype: 'checkbox',
                            id: 'chb.cnmpasien',
                            checked: true,
                            listeners: {
                                check: function (checkbox, val) {
                                    if (val == true) {
                                        Ext.getCmp('tf.cnmpasien').enable();
                                        Ext.getCmp('tf.cnmpasien').focus();
                                    } else if (val == false) {
                                        Ext.getCmp('tf.cnmpasien').disable();
                                        Ext.getCmp('tf.cnmpasien').setValue('');
                                    }
                                }
                            }
                        }, {
                            xtype: 'textfield',
                            id: 'tf.cnmpasien',
                            emptyText: 'Nama Pasien',
                            width: 230,
                            enableKeyEvents: true,
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER) {
                                        cAdvance();
                                    }
                                }
                            }
                        }]
                    }]
                }, {
                    xtype: 'fieldset',
                    columnWidth: .33,
                    border: false,
                    items: [{
                        xtype: 'compositefield',
                        id: 'comp_ctgllhr',
                        items: [{
                            xtype: 'checkbox',
                            id: 'chb.ctgllhr',
                            listeners: {
                                check: function (checkbox, val) {
                                    if (val == true) {
                                        Ext.getCmp('df.ctgllhr').enable();
                                        Ext.getCmp('df.ctgllhr').focus();
                                    } else if (val == false) {
                                        Ext.getCmp('df.ctgllhr').disable();
                                        Ext.getCmp('df.ctgllhr').setValue(new Date());
                                    }
                                }
                            }
                        }, {
                            xtype: 'label', id: 'lb.ctgllhr', text: 'Tgl. Lahir', margins: '5 5 0 0',
                        }, {
                            xtype: 'datefield',
                            id: 'df.ctgllhr',
                            width: 100, value: new Date(),
                            format: 'd-m-Y',
                            disabled: true,
                            enableKeyEvents: true,
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER) {
                                        cAdvance();
                                    }
                                }
                            }
                        }]
                    }, {
                        xtype: 'compositefield',
                        id: 'comp_calamatp',
                        items: [{
                            xtype: 'checkbox',
                            id: 'chb.calamatp',
                            listeners: {
                                check: function (checkbox, val) {
                                    if (val == true) {
                                        Ext.getCmp('tf.calamatp').enable();
                                        Ext.getCmp('tf.calamatp').focus();
                                    } else if (val == false) {
                                        Ext.getCmp('tf.calamatp').disable();
                                        Ext.getCmp('tf.calamatp').setValue('');
                                    }
                                }
                            }
                        }, {
                            xtype: 'textfield',
                            id: 'tf.calamatp',
                            emptyText: 'Alamat',
                            width: 230, disabled: true,
                            enableKeyEvents: true,
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER) {
                                        cAdvance();
                                    }
                                }
                            }
                        }]
                    }]
                }, {
                    xtype: 'fieldset',
                    columnWidth: .33,
                    border: false,
                    items: [{
                        xtype: 'compositefield',
                        id: 'comp_cnmibu',
                        items: [{
                            xtype: 'checkbox',
                            id: 'chb.cnmibu',
                            listeners: {
                                check: function (checkbox, val) {
                                    if (val == true) {
                                        Ext.getCmp('tf.cnmibu').enable();
                                        Ext.getCmp('tf.cnmibu').focus();
                                    } else if (val == false) {
                                        Ext.getCmp('tf.cnmibu').disable();
                                        Ext.getCmp('tf.cnmibu').setValue('');
                                    }
                                }
                            }
                        }, {
                            xtype: 'textfield',
                            id: 'tf.cnmibu',
                            emptyText: 'Nama Pasangan / Orang Tua',
                            width: 230, disabled: true,
                            enableKeyEvents: true,
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER) {
                                        cAdvance();
                                    }
                                }
                            }
                        }]
                    }, {
                        xtype: 'compositefield',
                        id: 'comp_ctelp',
                        items: [{
                            xtype: 'checkbox',
                            id: 'chb.ctelp',
                            listeners: {
                                check: function (checkbox, val) {
                                    if (val == true) {
                                        Ext.getCmp('tf.ctelp').enable();
                                        Ext.getCmp('tf.ctelp').focus();
                                    } else if (val == false) {
                                        Ext.getCmp('tf.ctelp').disable();
                                        Ext.getCmp('tf.ctelp').setValue('');
                                    }
                                }
                            }
                        }, {
                            xtype: 'textfield',
                            id: 'tf.ctelp',
                            emptyText: 'No. HP/Telp.',
                            width: 230, disabled: true,
                            enableKeyEvents: true,
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER) {
                                        cAdvance();
                                    }
                                }
                            }
                        }]
                    }]
                }]
            },
                grid_find_cari_pasien
            ]
        }).show();
        Ext.getCmp('tf.cnmpasien').focus(false, 200);

        function klik_cari_pasien(grid, rowIdx, columnIndex, event) {
            var t = event.getTarget();
            if (t.className == 'keyMasterPasien') {
                var rec_pasien = ds_pasien.getAt(rowIdx);
                Ext.getCmp('tf.norm').focus();
                Ext.getCmp("tf.norm").setValue(rec_pasien.data["norm"]);
                Ext.getCmp("tf.nmpasien").setValue(rec_pasien.data["nmpasien"]);
                Ext.getCmp("cb.jkelamin").setValue(rec_pasien.data["idjnskelamin"]);
                umur(new Date(rec_pasien.data["tgllahir"]));

                ds_dokumenpasien.setBaseParam('norm', rec_pasien.data["norm"]);
                ds_dokumenpasien.reload();
                Ext.getCmp('dokumen1').setValue('');
                Ext.getCmp('dokumen2').setValue('');
                Ext.getCmp('dokumen3').setValue('');
                Ext.getCmp('dokumen4').setValue('');
                Ext.getCmp('dokumen5').setValue('');
                win_find_cari_pasien.close();
            }
        }

        function cAdvance() {
            if (Ext.getCmp('chb.ctgllhr').getValue() == true) {
                ds_pasien.setBaseParam('tgllahir', Ext.util.Format.date(Ext.getCmp('df.ctgllhr').getValue(), 'Y-m-d'));
            } else {
                ds_pasien.setBaseParam('tgllahir', '');
            }

            ds_pasien.setBaseParam('norm', Ext.getCmp('tf.crm').getValue());
            ds_pasien.setBaseParam('normlama', Ext.getCmp('tf.crmlama').getValue());
            ds_pasien.setBaseParam('nmpasien', Ext.getCmp('tf.cnmpasien').getValue());
            ds_pasien.setBaseParam('alamat', Ext.getCmp('tf.calamatp').getValue());
            ds_pasien.setBaseParam('nmibu', Ext.getCmp('tf.cnmibu').getValue());
            ds_pasien.setBaseParam('notelp', Ext.getCmp('tf.ctelp').getValue());
            ds_pasien.load();
        }
    }

    function upload_dokumen(){
        //check validation
        var norm     = Ext.getCmp('tf.norm').getValue();
        var nmpasien = Ext.getCmp('tf.nmpasien').getValue();
        var dokumen1 = Ext.getCmp('dokumen1').getValue();
        var dokumen2 = Ext.getCmp('dokumen2').getValue();
        var dokumen3 = Ext.getCmp('dokumen3').getValue();
        var dokumen4 = Ext.getCmp('dokumen4').getValue();
        var dokumen5 = Ext.getCmp('dokumen5').getValue();

        if(norm == '' || nmpasien == ''){
            Ext.MessageBox.alert('Informasi', 'Harap pilih pasien terlebih dahulu');
            return false;
        }

        if(dokumen1 == '' && dokumen2 == '' && dokumen3 == '' && dokumen4 == '' && dokumen5 == ''){
            Ext.MessageBox.alert('Informasi', 'Harap pilih minimal 1 dokumen untuk di upload');
            return false;
        }

        var waitmsg = Ext.MessageBox.wait('Menyimpan...', 'Info');

        rmberkaspasien_form.getForm().submit({
            url: BASE_URL + 'dokumenpasien_controller/upload_dokumen',
            method: 'POST',
            params: {
                norm: norm,
            },
            success: function (rmberkaspasien_form, o) {
                var response = Ext.util.JSON.decode(o.response.responseText);
                waitmsg.hide();
                Ext.MessageBox.alert('Informasi', response.msg);

                if(response.success){
                    Ext.getCmp('dokumen1').setValue('');
                    Ext.getCmp('dokumen2').setValue('');
                    Ext.getCmp('dokumen3').setValue('');
                    Ext.getCmp('dokumen4').setValue('');
                    Ext.getCmp('dokumen5').setValue('');

                    ds_dokumenpasien.reload();                
                }
                
            },
            failure: function (rmberkaspasien_form, o) {
                var response = Ext.util.JSON.decode(o.response.responseText);
                waitmsg.hide();
                Ext.MessageBox.alert('Informasi', 'gagal menyimpan data :' + response.msg);

            },

        });

    }

    function clearform(){


        Ext.getCmp('tf.norm').setValue('');
        Ext.getCmp('tf.nmpasien').setValue('');
        Ext.getCmp('cb.jkelamin').setValue('');
        Ext.getCmp('tf.umurthn').setValue('');
        Ext.getCmp('tf.umurbln').setValue('');
        Ext.getCmp('tf.umurhari').setValue('');

        Ext.getCmp('dokumen1').setValue('');
        Ext.getCmp('dokumen2').setValue('');
        Ext.getCmp('dokumen3').setValue('');
        Ext.getCmp('dokumen4').setValue('');
        Ext.getCmp('dokumen5').setValue('');

        ds_dokumenpasien.setBaseParam('norm', '');
        ds_dokumenpasien.reload();                

    }

}
