function RJperawat(){
	var ds_pasien = dm_pasien();
	var ds_jdiagnosa = dm_jdiagnosa();
	var ds_tinlanjut = dm_tinlanjut();
	var ds_penkematian = dm_penkematian();
	var ds_jkelamin = dm_jkelamin();
	var ds_jpelayanan = dm_jpelayanan();
	var ds_bagian = dm_bagian();
	ds_bagian.setBaseParam('jpel',1);
	
	var ds_stposisipasien = dm_stposisipasien();
	ds_stposisipasien.setBaseParam('kdstposisi','RJ');
	var ds_dokter = dm_dokter();
	var ds_kodifikasi = dm_kodifikasi();
	ds_kodifikasi.setBaseParam('noreg', '1');
	var ds_reservasix = dm_reservasix();
	var rowidreservasi = '';
	ds_reservasix.setBaseParam('tglreservasi', Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_reservasix.setBaseParam('start',0);
	ds_reservasix.setBaseParam('limit',100);
	ds_reservasix.setBaseParam('jpelayanan',1);
	
	var arr_cari = [['1', 'Kunjungan Hamil'],['2', 'Kontrol'],['', '-Pilih-']];	
	var ds_khml = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	var grid_diagnosa = new Ext.grid.EditorGridPanel({
		store: ds_kodifikasi,
		frame: true,
		height: 215,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_diagnosa',
		forceFit: true,
		autoScroll: true,
		columnLines: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		loadMask: true,
		clicksToEdit: 1,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			disabled: true,
			iconCls: 'silk-add',
			handler: function() {
				fDiagnosa();
			}
		},{ xtype:'tbfill' },{
			xtype: 'fieldset',
			border: false,
			width: 450,
			height: 20,
			layout:'hbox',
			style: 'padding: 0px; margin:5px',
			items: [{
				xtype: 'label', id: 'lb.tlanjut', text: 'Tindak Lanjut :', margins: '0 10 0 10',
			},{
				xtype: 'combo',
				id: 'cb.tlanjut', width: 100, 
				store: ds_tinlanjut, valueField: 'idtindaklanjut', displayField: 'nmtindaklanjut',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local'
			},{
				xtype: 'label', id: 'lb.pkematian', text: 'Penyebab Kematian :', margins: '0 10 0 20',
			},{
				xtype: 'combo',
				id: 'cb.pkematian', width: 100, 
				store: ds_penkematian, valueField: 'idkematian', displayField: 'nmkematian',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local'
			}]
		}],
		columns: [{
			dataIndex: 'idpenyakit',
			hidden: true
		},{
			header: '<div style="text-align:center;">Kode Penyakit<br>(ICD-X)</div>',
			width: 100,
			dataIndex: 'kdpenyakit',
			sortable: true
		},{
			header: '<div style="text-align:center;">nama Penyakit<br>(Bhs. Inggris)</div>',
			width: 250,
			dataIndex: 'nmpenyakiteng',
			sortable: true
		},{
			header: '<div style="text-align:center;">Nama Penyakit<br>(Bhs. Indonesia)</div>',
			width: 250,
			dataIndex: 'nmpenyakit',
			sortable: true
		},{
			header: '<div style="text-align:center;">(Baru/Lama)</div>',
			width: 100,
			dataIndex: 'nmjstpenyakit',
			sortable: true
		},{
			header: '<div style="text-align:center;">Jenis Diagnosa</div>',
			width: 150,
			dataIndex: 'nmjnsdiagnosa',
			editor: {
				xtype: 'combo',
				id: 'cb.jdiagnosa', width: 150, 
				store: ds_jdiagnosa, valueField: 'nmjnsdiagnosa', displayField: 'nmjnsdiagnosa',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local'
			},
			sortable: true
		},{
			xtype: 'actioncolumn',
			width: 50,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex, colIndex) {
					ds_kodifikasi.removeAt(rowIndex);
				}
			}]
        }]
	});
	
	var row_reservasi = new Ext.grid.RowSelectionModel({
		singleSelect: true
	});
	
	var grid_reservasi = new Ext.grid.EditorGridPanel({
		store: ds_reservasix,
		sm: row_reservasi,
		height: 200,
		id: 'grid_reservasi',
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var jkel;
				var record = grid.getStore().getAt(rowIndex);
				Ext.getCmp("tf.noreg").setValue(record.get('noreg'));
				Ext.getCmp("tf.tglreg").setValue(record.get('tglreg'));
				Ext.getCmp("tf.stpas").setValue(record.get('nmstpasien'));
				Ext.getCmp("tf.norm").setValue(record.get('norm'));
				Ext.getCmp("tf.nmpasien").setValue(record.get('nmpasien'));
				Ext.getCmp("tf.umurthn").setValue(record.get('umurtahun'));
				Ext.getCmp("tf.umurbln").setValue(record.get('umurbulan'));
				Ext.getCmp("tf.umurhari").setValue(record.get('umurhari'));
				Ext.getCmp("cb.jkelamin").setValue(record.get('kdjnskelamin'));
				Ext.getCmp("ta.alergiobat").setValue(record.get('alergi'));
				Ext.getCmp("ta.catatanreg").setValue(record.get('catatan'));
				Ext.getCmp("tf.tinggi").setValue(record.get('tinggibadan'));
				Ext.getCmp("tf.berat").setValue(record.get('beratbadan'));
				Ext.getCmp("tf.systole").setValue(record.get('systole'));
				Ext.getCmp("tf.diastole").setValue(record.get('diastole'));
				Ext.getCmp("ta.keluhan").setValue(record.get('keluhan'));
				Ext.getCmp("tf.g").setValue(record.get('gravidag'));
				Ext.getCmp("tf.p").setValue(record.get('gravidap'));
				Ext.getCmp("tf.a").setValue(record.get('gravidaa'));
				Ext.getCmp("tf.i").setValue(record.get('gravidai'));
				Ext.getCmp("tf.khamilan").setValue(record.get('kunjhamil'));
				Ext.getCmp("tf.idbagian").setValue(record.get('idbagian'));
				Addrecord('grid_reservasi',rowIndex);
				hitungBMI();
				ds_kodifikasi.setBaseParam('noreg', record.get('noreg'));
				ds_kodifikasi.reload();
				Ext.getCmp('btn_add').enable();
            }
        },
		columns: [{
			header: '<div style="text-align:center;">No.<br>Antrian</div>',
			dataIndex: 'noantrian',
            align: 'center',
			width: 50,
			sortable: true
		},{
			header: '<div style="text-align:center;">Jam Reg.</div>',
			dataIndex: 'jamreg',
            align: 'center',
			width: 55,
			sortable: true
		},{
			header: '<div style="text-align:center;">No. Reg.</div>',
			dataIndex: 'noreg',
            align: 'center',
			width: 80,
			sortable: true
		},{
			header: '<div style="text-align:center;">No. RM</div>',
			dataIndex: 'norm',
			width: 50,
			sortable: true
		},{
			header: '<div style="text-align:center;">Nama Pasien</div>',
			dataIndex: 'nmpasien',
			width: 120,
			sortable: true
		},{
			header: '(L/P)',
			dataIndex: 'kdjnskelamin',
            align: 'center',
			width: 35,
			sortable: true
		},{
			header: '<div style="text-align:center;">Status<br/>Pasien</div>',
			dataIndex: 'nmstpasien',
			width: 50,
			sortable: true
		},{
			header: '<div style="text-align:center;">Unit<br/>Pelayanan</div>',
			dataIndex: 'nmbagian',
			width: 80,
			sortable: true
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 160,
			sortable: true
		},{
			header: 'TB<br/>(cm)',
			dataIndex: 'tinggibadan',
			width: 35,
			sortable: true
		},{
			header: 'BB<br/>(Kg)',
			dataIndex: 'beratbadan',
			width: 35,
			sortable: true
		},{
			header: '<div style="text-align:center;">Tekanan<br/>Darah</div>',
			dataIndex: 'systole',
			width: 95,
			renderer: function(value, p, r){
				var sysdia = '';
				if(r.data['diastole'] && r.data['systole']) sysdia = r.data['systole'] + ' / ' + r.data['diastole'] + ' mmHg';
				
				return sysdia ;
			},
			sortable: true
		},{
			header: '<div style="text-align:center;">Anamnesa/<br/>Keluhan</div>',
			dataIndex: 'keluhan',
			width: 150,
			sortable: true
		},{
			header: 'Posisi Pasien',
			dataIndex: 'nmstposisipasien',
			width: 110,
			editor: {
				xtype: 'combo',
				id: 'cb.posisipasien', width: 150, 
				store: ds_stposisipasien, valueField: 'idstposisipasien', displayField: 'nmstposisipasien',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local',
				listeners:{
					select:function(combo, records, eOpts){
						var obj= records.data;
						fnEditPosisiPasien(obj.idstposisipasien);
					}
				}
			},
			sortable: true
		},{
			header: 'Status',
			dataIndex: 'systole',
			width: 90,
			align: 'center',
			renderer: function(value, p, r){
				var info = '';
					if(r.data['systole'] == null) info = ' ';
					if(r.data['systole'] != null) info = 'Sudah Diinput';
					if(r.data['systole'] == '0') info = ' ';
				return info ;
			}
		}],
		/* listeners: {	
			rowclick: Addrecord
		} */
	});
	
	var rjperawat_form = new Ext.form.FormPanel({ 
		id: 'fp.rjperawat',
		title: 'Daftar Pasien Rawat Jalan (Untuk Perawat)',
		width: 900, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		tbar: [
			'Tanggal',{
				xtype: 'datefield',
				id: 'df.tglreservasi',
				value: new Date(),
				format: 'd-m-Y',
				width: 100 },'-',
			'Unit Pelayanan',{
				xtype: 'combo',
				id: 'cb.upelayanan', width: 200, 
				store: ds_bagian, valueField: 'idbagian', displayField: 'nmbagian',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local',
				listeners: {
					afterrender: function(combo) {
						/* ComboRecord = new Ext.data.Record.create([{name: 'idjnspelayanan'}, {name: 'nmjnspelayanan'}]);
						ds_jpelayanan.add(new ComboRecord({idjnspelayanan: 1000, nmjnspelayanan: 'Inserisci Nuovo'}));
						ds_jpelayanan.reload(); */
						
						/* var store = combo.getStore();
						var data = [];
						data.push(new Ext.data.Record({
							idjnspelayanan: 100,
							nmjnspelayanan:'zzzaaaaaaaaaaaaaaa'
						}));
						ds_jpelayanan.insert(6,data);
						console.log(data);
						console.log(ComboRecord); */
					}
				}},'-',
			'Dokter',{
				xtype: 'combo',
				id: 'cb.dokter', width: 200, 
				store: ds_dokter, valueField: 'iddokter', displayField: 'nmdoktergelar',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local'},'-',
			{
				xtype: 'button',
				text: 'Cari',
				id: 'btn.cari',
				width: 50,
				style: { 
					background:'-webkit-gradient(linear, left top, left bottom, color-stop(0.5, #fff), color-stop(0.8, #dddddd))',
					background:'-moz-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-webkit-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-o-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-ms-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'linear-gradient(to bottom, #fff 50%, #dddddd 80%)',
					'-moz-border-radius':'15px',
					'-webkit-border-radius':'15px',
					'border-radius':'5px',
					border:'1px solid #d6bcd6' },
				handler: function() {
					cari();
				}},'-',
			{
				xtype: 'button',
				text: 'Ulangi Pencarian',
				id: 'btn.ulang',
				width: 100,
				style: { 
					background:'-webkit-gradient(linear, left top, left bottom, color-stop(0.5, #fff), color-stop(0.8, #dddddd))',
					background:'-moz-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-webkit-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-o-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'-ms-linear-gradient(top, #fff 50%, #dddddd 80%)',
					background:'linear-gradient(to bottom, #fff 50%, #dddddd 80%)',
					'-moz-border-radius':'15px',
					'-webkit-border-radius':'15px',
					'border-radius':'5px',
					border:'1px solid #d6bcd6' },
				handler: function() {
					ulangCari();
				}},'-',
			{xtype: 'tbfill' }
		],
		defaults: { labelWidth: 150, labelAlign: 'right'},
        items: [{
			xtype: 'fieldset', layout: 'form', autowidth: true,
			items: [grid_reservasi]
		},{
			xtype: 'container',
			id:'fs.pasien', layout: 'column', border:false,
			defaults: { labelWidth: 150, labelAlign: 'right' }, 
			items: [{
				////COLUMN 1
				xtype: 'container',style: 'padding:10px',
				layout: 'form', columnWidth: 0.50,border:false,
				defaults: { labelWidth: 150 }, 
				items: [{
					xtype: 'fieldset',style: 'padding:10px',
					layout: 'form', columnWidth: 1, title: 'Data Registrasi Pasien',
					items: [{
						xtype: 'compositefield', layout:'anchor',
						fieldLabel: 'No. / Tgl. Registrasi',
						items: [{
							xtype: 'textfield',
							id: 'tf.noreg', width: '20%', readOnly: true, 
							style : 'opacity:0.6'
						},{
							xtype: 'label', id: 'lb.reg', text: '/', margins: '0 5 0 5',
						},{
							xtype: 'textfield',
							id: 'tf.tglreg', width: '20%', readOnly: true, 
							style : 'opacity:0.6'
						},{
							xtype: 'label', id: 'lb.pasreg', text: 'Pasien :', margins: '0 5 0 20',
						},{
							xtype: 'textfield',
							id: 'tf.stpas', width: '24%', readOnly: true, 
							style : 'opacity:0.6'
						}]
					},{
						xtype: 'container', fieldLabel: 'No. RM / Nama Pasien',
						layout: 'hbox',
						items: [{
							xtype: 'textfield',
							id  : 'tf.norm', width : '20%', readOnly: true, 
							style : 'opacity:0.6'
						},{
							xtype: 'label', id: 'lb.garing', text: '/', margins: '0 5 0 10',
						},{
							xtype: 'textfield',
							id  : 'tf.nmpasien', width : '66.6%', readOnly: true, 
							style : 'opacity:0.6'
						}]
					},{
						xtype: 'container', fieldLabel: 'Umur',
						layout: 'hbox',
						items: [{
							xtype: 'textfield', id: 'tf.umurthn', 
							width: '5%', readOnly: true,
							style : 'opacity:0.6'
						},{
							xtype: 'label', id: 'lb.umurthn', text: 'Tahun', margins: '0 10 0 5',
						},{ 	
							xtype: 'textfield', id: 'tf.umurbln', 
							width: '5%', readOnly: true, 
							style : 'opacity:0.6'
						},{
							xtype: 'label', id: 'lb.umurbln', text: 'Bulan', margins: '0 10 0 5',
						},{
							xtype: 'textfield', id: 'tf.umurhari', 
							width: '5%', readOnly: true, 
							style : 'opacity:0.6'
						},{
							xtype: 'label', id: 'lb.umurhari', text: 'Hari', margins: '0 10 0 5'
						},{
							xtype: 'label', id: 'lb.jkel', text: 'Jenis Kelamin', margins: '0 10 0 10'
						},{
							xtype: 'textfield', id: 'cb.jkelamin', 
							width: '12%', readOnly: true, 
							style : 'opacity:0.6'
							/*xtype: 'combo',
							id: 'cb.jkelamin', width: 52,
							store: ds_jkelamin, valueField: 'idjnskelamin', displayField: 'nmjnskelamin',
							editable: false, triggerAction: 'all',
							forceSelection: true, submitValue: true, mode: 'local',
							readOnly: true, style : 'opacity:0.6'*/
						}]
					},{
						xtype: 'textarea', fieldLabel: 'Alergi Obat',
						id  : 'ta.alergiobat', width : '94.5%', height:50
					},{
						xtype: 'textarea', fieldLabel: 'Catatan Registrasi',
						id  : 'ta.catatanreg', width : '94.5%', height:50, readOnly: true, 
						style : 'opacity:0.6'
					},{
						xtype:'fieldset',
						checkboxToggle:true,
						title: 'Gravida',
						autoHeight:true,
						collapsed: true,
						labelWidth: 120,
						items :[{
							xtype: 'compositefield', 
							items: [{
								xtype: 'label', id: 'lb.g', text: 'G :', margins: '2 5 0 2'
							},{
								xtype: 'textfield', id: 'tf.g',  width: 50
							},{
								xtype: 'label', id: 'lb.p', text: 'P :', margins: '2 5 0 2'
							},{
								xtype: 'textfield', id: 'tf.p',  width: 50
							},{
								xtype: 'label', id: 'lb.a', text: 'A :', margins: '2 5 0 2'
							},{
								xtype: 'textfield', id: 'tf.a',  width: 50
							},{
								xtype: 'label', id: 'lb.h', text: 'Ibu Hamil :', margins: '2 5 0 2'
							},{
								xtype: 'textfield', id: 'tf.i',  width: 50
							}]
						},{
							xtype: 'combo',
							id: 'cb.khml', width: 150, fieldLabel: 'Status Gravida',
							store: ds_khml, valueField: 'id', displayField: 'nama',
							editable: false, triggerAction: 'all', emptyText:'Pilih...',
							forceSelection: true, submitValue: true, mode: 'local',
						},{
							xtype: 'textfield', id: 'tf.khamilan',  width: 50, hidden: true
						},{
							xtype: 'textfield', id: 'tf.idbagian',  width: 50, hidden: true
						}]
					}]
				}]
			},{
				////COLUMN 2
				xtype: 'fieldset',style: 'padding:10px',
				layout: 'form', columnWidth: 0.50,border:false,
				defaults: { labelWidth: 150 }, 
				items: [{
					xtype: 'fieldset',style: 'padding:10px',
					layout: 'form', columnWidth: 0.50, title: 'Pemeriksaan Fisik',
					defaults: { labelWidth: 1 }, 
					items: [{
						xtype: 'compositefield', layout:'anchor',
						fieldLabel: 'Tinggi / Berat Badan',
						items: [{
							xtype: 'numericfield',
							id: 'tf.tinggi', width: '20%',
							decimalPrecision : 4,
							enableKeyEvents: true,
							listeners:{
								keyup:function(){
									hitungBMI();
								}
							}
						},{
							xtype: 'label', id: 'lb.tinggi', text: 'cm /', margins: '0 5 0 5',
						},{
							xtype: 'numericfield',
							id: 'tf.berat', width: '20%',
							decimalPrecision : 4,
							enableKeyEvents: true,
							listeners:{
								keyup:function(){
									hitungBMI();
								}
							}
						},{
							xtype: 'label', id: 'lb.berat', text: 'Kg', margins: '0 54 0 5',
						},{
							xtype: 'button',
							text: 'Simpan',
							id: 'btn.penjamin',
							width: '20%',
							handler: function() {
								simpan('fp.rjperawat');
							}
						}]
					},{
						xtype: 'compositefield', layout:'anchor',
						fieldLabel: 'BMI',
						items: [{
							xtype: 'textfield',
							id: 'tf.bmi1', width: '20%', readOnly: true, readOnly: true, 
							style : 'opacity:0.6'
						},{
							xtype: 'textfield',
							id: 'tf.bmi2', width: '67%',margins: '0 0 0 15', readOnly: true, 
							style : 'opacity:0.6'
						}]
					},{
						xtype: 'compositefield', layout:'anchor',
						fieldLabel: 'Tekanan Darah',
						items: [{
							xtype: 'numericfield',
							id: 'tf.systole', width: '20%'
						},{
							xtype: 'label', id: 'lb.tkndrh1', text: '/', margins: '0 5 0 5',
						},{
							xtype: 'numericfield',
							id: 'tf.diastole', width: '24%',
						},{
							xtype: 'label', id: 'lb.tkndrh2', text: 'mmHg', margins: '0 5 0 5',
						}]
					},{
						xtype: 'textarea', fieldLabel: 'Anamnesa / Keluhan',
						id  : 'ta.keluhan', width : '94.5%', height:160
					}]
				}]
			}]
		},{
			xtype: 'fieldset', layout: 'form', autowidth: true,
			items: [grid_diagnosa]
		}]
	}); SET_PAGE_CONTENT(rjperawat_form);
	
	function Addrecord(grid, rowIndex){
		var record = ds_reservasix.getAt(rowIndex);
		rowidreservasi = record.data['idreservasi'];
	}
	
	function bersihrjper() {
		Ext.getCmp('tf.noreg').setValue();
		Ext.getCmp('tf.tglreg').setValue();
		Ext.getCmp('tf.stpas').setValue();
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue();
		Ext.getCmp('tf.umurthn').setValue();
		Ext.getCmp('tf.umurbln').setValue();
		Ext.getCmp('tf.umurhari').setValue(); 
		Ext.getCmp('cb.jkelamin').setValue();
		Ext.getCmp('ta.alergiobat').setValue(); 
		Ext.getCmp('ta.catatanreg').setValue(); 
		Ext.getCmp('tf.tinggi').setValue();
		Ext.getCmp('tf.berat').setValue();
		Ext.getCmp('tf.bmi1').setValue();
		Ext.getCmp('tf.bmi2').setValue();
		Ext.getCmp('tf.systole').setValue();
		Ext.getCmp('tf.diastole').setValue();
		Ext.getCmp('ta.keluhan').setValue();
		Ext.getCmp('tf.g').setValue();
		Ext.getCmp('tf.p').setValue();
		Ext.getCmp('tf.a').setValue();
		Ext.getCmp('tf.i').setValue();
		Ext.getCmp('tf.khamilan').setValue();
		Ext.getCmp('cb.khml').setValue();
		Ext.getCmp('tf.idbagian').setValue();
		Ext.getCmp('btn_add').disable();
		ds_reservasix.reload();
		ds_kodifikasi.setBaseParam('noreg', '1');
		ds_kodifikasi.reload();
	}

	function simpan(namaForm) {
		var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
		var arrdiagnosa = [];
		var zx = 0;
		ds_kodifikasi.each(function(rec){
			var zidpenyakit = rec.get('idpenyakit');
			var znmjnsdiagnosa = rec.get('nmjnsdiagnosa');
			var znmjstpenyakit = rec.get('nmjstpenyakit');
			arrdiagnosa[zx] = zidpenyakit+'-'+znmjnsdiagnosa+'-'+znmjstpenyakit;
			zx++;
		});
				
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'registrasi_controller/insorupd_regperawat',
			method: 'POST',
			params: {
				arrdiagnosa	:  Ext.encode(arrdiagnosa),
				id : Ext.getCmp('cb.khml').getValue(),
				idbag : Ext.getCmp('tf.idbagian').getValue()
			},
			success: function(rjperawat_form, o) {
				if (o.result.success==true) {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
					Ext.getCmp('tf.norm').setValue(o.result.norm);
				} else {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
				bersihrjper();
			},
			failure: function(rjperawat_form, o) {
				waitmsg.hide();
				Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal<br>(Tinggi, Berat Badan dan Tekanan Darah Belum diisi)');
			}
		});
	}
	
	function cari() {
		ds_reservasix.setBaseParam('tglreservasi',Ext.getCmp('df.tglreservasi').getValue());
		ds_reservasix.setBaseParam('upel',Ext.getCmp('cb.upelayanan').getValue());
		ds_reservasix.setBaseParam('dokter',Ext.getCmp('cb.dokter').getValue());
		ds_reservasix.reload();
	}
	
	function ulangCari() {
		Ext.getCmp('df.tglreservasi').setValue(new Date());
		Ext.getCmp('cb.upelayanan').setValue();
		Ext.getCmp('cb.dokter').setValue();
		
		ds_reservasix.setBaseParam('upel','');
		ds_reservasix.setBaseParam('dokter','');
		ds_reservasix.setBaseParam('tglreservasi', Ext.util.Format.date(new Date(), 'Y-m-d'));
		ds_reservasix.reload();
	}
	
	function hitungBMI(){
		var tinggi = Ext.getCmp('tf.tinggi').getValue();
		var berat = Ext.getCmp('tf.berat').getValue();
		if(tinggi != '' && berat != ''){
			var tinggirt = tinggi/100;
			var keterangan = '';
			var bmi = berat / (tinggirt * tinggirt);
			if(bmi < 18.5) keterangan = 'Underweight';
			else if(bmi < 25) keterangan = 'Normal';
			else if(bmi < 30) keterangan = 'Overweight';
			else keterangan = 'Obesitas';
			
			Ext.getCmp('tf.bmi1').setValue(Ext.util.Format.number(bmi, '0.0'));
			Ext.getCmp('tf.bmi2').setValue(keterangan);
		} else {
			Ext.getCmp('tf.bmi1').setValue();
			Ext.getCmp('tf.bmi2').setValue();
		}
		
	}

	function fnEditPosisiPasien(idstposisipasien){
		Ext.Ajax.request({
			url: BASE_URL + 'reservasi_controller/updateStposisipasien',
			params: {
				idreservasi : rowidreservasi,
				idstposisipasien : idstposisipasien
			},
			success: function(){
				Ext.getCmp('grid_reservasi').store.reload();
				//Ext.Msg.alert("Informasi", "Posisi Pasien Berhasil Dirubah");
			}
			
		});
	}

	function fDiagnosa(){
		var ds_penyakit = dm_penyakit();
		
		function keyToView_penyakit(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterPenyakit" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_penyakit = new Ext.grid.ColumnModel([
			{
				header: '<div style="text-align:center;">Kode Penyakit<br>(ICD-X)</div>',
				width: 100,
				dataIndex: 'kdpenyakit',
				renderer: keyToView_penyakit
			},{
				header: '<div style="text-align:center;">nama Penyakit<br>(Bhs. Inggris)</div>',
				width: 200,
				dataIndex: 'nmpenyakiteng'
			},{
				header: '<div style="text-align:center;">Nama Penyakit<br>(Bhs. Indonesia)</div>',
				width: 200,
				dataIndex: 'nmpenyakit'
			}
		]);
		var sm_penyakit = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_penyakit = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_penyakit = new Ext.PagingToolbar({
			pageSize: 20,
			store: ds_penyakit,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_penyakit = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_penyakit = new Ext.grid.GridPanel({
			ds: ds_penyakit,
			cm: cm_penyakit,
			sm: sm_penyakit,
			view: vw_penyakit,
			height: 460,
			width: 550,
			plugins: cari_penyakit,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_penyakit,
			listeners: {
				cellclick: klik_penyakit
			}
		});
		var win_penyakit = new Ext.Window({
			title: 'Cari Penyakit',
			modal: true,
			items: [grid_penyakit]
		}).show();

		function klik_penyakit(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterPenyakit'){
				var cek = true;
				var obj = ds_penyakit.getAt(rowIdx);
				var sidpenyakit			= obj.data["idpenyakit"];
				var skdpenyakit			= obj.data["kdpenyakit"];
				var snmpenyakit			= obj.data["nmpenyakit"];
				var snmpenyakiteng		= obj.data["nmpenyakiteng"];
				
				ds_kodifikasi.each(function(rec){
					if(rec.get('kdpenyakit') == skdpenyakit) {
						Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
						cek = false;
					}
				});
				
				Ext.Ajax.request({
					url: BASE_URL + 'Jstpenyakit_controller/cekStatus',
					params: {
						noreg		: Ext.getCmp('tf.noreg').getValue(),
						idpenyakit	: sidpenyakit
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						if(cek){
							var orgaListRecord = new Ext.data.Record.create([
								{
									name: 'idpenyakit',
									name: 'kdpenyakit',
									name: 'nmpenyakit',
									name: 'nmpenyakiteng',
									name: 'nmjstpenyakit',
									name: 'nmjnsdiagnosa'
								}
							]);
							
							ds_kodifikasi.add([
								new orgaListRecord({
									'idpenyakit': sidpenyakit,
									'kdpenyakit': skdpenyakit,
									'nmpenyakit': snmpenyakit,
									'nmpenyakiteng': snmpenyakiteng,
									'nmjstpenyakit': obj.stat,
									'nmjnsdiagnosa': 'Utama'
								})
							]);
						}
					}
				});
			}
		}
	}
}