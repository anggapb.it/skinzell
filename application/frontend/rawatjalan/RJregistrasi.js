function RJregistrasi(norm){
	//var myVar=setInterval(function(){myTimer()},1000);
	//function myTimer(){
	//	var d=new Date();
	//	var formattedValue = Ext.util.Format.date(d, 'H:i');
		//if(Ext.getCmp("tf.jamshift"))
	//			Ext.getCmp("tf.jamshift").setValue(formattedValue);
	//	console.log(formattedValue);
	//	console.log(Ext.getCmp("tf.jamshift").getValue());
				//RH.setCompValue("tf.jamshift",formattedValue);
		//else myStopFunction();
	//}
	
	/* function myStopFunction(){
		clearInterval(myVar);
	} */
	
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("idshift").setValue(obj.idshift);
			Ext.getCmp("tf.waktushift").setValue(obj.nmshift);
			
			var formattedValue = Ext.util.Format.date(new Date(), 'H:i');
			Ext.getCmp("tf.jamshift").setValue(formattedValue);
		}
	});
	
	Ext.Ajax.request({
		url:BASE_URL + 'vregistrasi_controller/cekRegistrasi',
		method:'POST',
		params: {
			norm		: norm,
			jpel		: 'Rawat Jalan'
		},
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			if(obj.validasi > 0){
				var arr = [];
				var ind = 0;
				Ext.MessageBox.alert('Informasi', 'Pasien tersebut telah melakukan registrasi RJ');
				obj.arr.forEach(function(n) {
					arr[ind] = n;
					ind += 1;
				});
				ds_bagianrj.setBaseParam('val',Ext.encode(arr));
				ds_bagianrj.reload();
			}
		}
	});
	
	Ext.Ajax.request({
		url:BASE_URL + 'pasien_controller/getDataPasien',
		method:'POST',
		params: {
			norm		: norm
		},
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			if(obj.norm != null) obj.norm = parseInt(obj.norm);
			Ext.getCmp('alamat').setValue(obj.alamat);
			Ext.getCmp('notelp').setValue(obj.notelp);
			Ext.getCmp('nohp').setValue(obj.nohp);
			Ext.getCmp('tf.norm').setValue(obj.norm);
			Ext.getCmp('tf.nmpasien').setValue(obj.nmpasien);
			Ext.getCmp('cb.jkelamin').setValue(obj.nmjnskelamin);
			if(obj.tgllahir)umur(new Date(obj.tgllahir));
			Ext.Ajax.request({
				url: BASE_URL + 'pasien_controller/getCekPasienresreg',
				params: {
					norm		: obj.norm
				},
				success: function(response){
					obj = Ext.util.JSON.decode(response.responseText);
					var var_cari_pasien = obj.norm;
					var var_cari_pasienidres = obj.idreservasi;
					var var_cari_pasienidbag = obj.idbagian;
					var var_cari_pasiennmbag = obj.nmbagian;
					var var_cari_pasiennmdok = obj.nmdoktergelar;
					var var_cari_pasiennoantri = obj.noantrian;
					if(!isNaN(parseInt(var_cari_pasien))){
						Ext.getCmp('tf.idres').setValue(var_cari_pasienidres);
						Ext.getCmp('idbagian').setValue(var_cari_pasienidbag);
						Ext.getCmp('tf.upelayanan').setValue(var_cari_pasiennmbag);
						Ext.getCmp('tf.dokter').setValue(var_cari_pasiennmdok);
						Ext.getCmp('tf.jmlpantrires').setValue(var_cari_pasiennoantri);
						ds_jadwalprakteknow.setBaseParam('idbagian',var_cari_pasienidbag);
						ds_jadwalprakteknow.reload({
							scope   : this,
							callback: function(records, operation, success) {
								var nmlkp = '';
								 ds_jadwalprakteknow.each(function (rec) { 
										nmlkp = rec.get('nmdoktergelar');
									});
								if(nmlkp == ""){
									Ext.getCmp('tf.dokter').setValue();
								}
								else{
									Ext.Ajax.request({
										url:BASE_URL + 'jadwalpraktek_controller/cekdktr',
										method:'POST',
										params: {
											idbagian		: Ext.getCmp('idbagian').getValue(),
											cquery			: Ext.getCmp('tf.dokter').getValue()
										},
										success: function(response){
											obj = Ext.util.JSON.decode(response.responseText);
											Ext.getCmp("tf.dokter").setValue(obj.nmdoktergelar);
										}
									});
								}
								return;
							}
						});
					}
				},
				failure: function() {
					//Ext.MessageBox.alert('Informasi', 'Error.');
				}
			});	
		}
	});
	
	Ext.Ajax.request({
		url:BASE_URL + 'registrasi_controller/getUpelJadwal',
		method:'POST',
		params: {
			idjnspelayanan		: 1
		},
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('idbagian').setValue(obj.idbagian);
			Ext.getCmp('tf.upelayanan').setValue(obj.nmbagian);
			Ext.getCmp('tf.dokter').setValue(obj.nmdoktergelar);
			
			ds_jadwalprakteknow.setBaseParam('idbagian',obj.idbagian);
			/* ds_jadwalprakteknow.setBaseParam('shift',Ext.getCmp("idshift").getValue());
			ds_jadwalprakteknow.setBaseParam('tgl',Ext.getCmp("df.tglshift").getValue()); */
			ds_jadwalprakteknow.reload();
			getNoAntrian();
		}
	});
	
	if(Ext.getCmp('tf.idreservasi').value != null){
		Ext.Ajax.request({
			url:BASE_URL + 'reservasi_controller/getRegres',
			method:'POST',
			params: {
				idreservasi		: Ext.getCmp('tf.idreservasi').value
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.dokter").setValue(obj.nmdoktergelar);
				Ext.getCmp("tf.upelayanan").setValue(obj.nmbagian);
/* 				ds_jadwalprakteknow.setBaseParam('idbagian',obj.idbagian);
				ds_jadwalprakteknow.reload(); */
			}
		});
	}
	
	var ds_jkelamin = dm_jkelamin();
	var ds_caradatang = dm_caradatang();
	var ds_klsrawat = dm_klsrawat();
	var ds_kamarbagian = dm_kamarbagian();
	var ds_penjamin = dm_penjamin();
	var ds_bagianrjriugd = dm_bagianrjriugd();
	var ds_bagianrj = dm_bagian();
	ds_bagianrj.setBaseParam('jpel',1);
	var ds_dokter = dm_dokter();
	var ds_jpelayanan = dm_jpelayanan();
	var ds_jadwalprakteknow = dm_jadwalprakteknow();
	
	var grid_jdwldktr = new Ext.grid.GridPanel({
		store: ds_jadwalprakteknow,
		height: 120,
		width: 300,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_jdwldktr',
		//hideHeaders: true,
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var record = grid.getStore().getAt(rowIndex);
				Ext.getCmp("tf.dokter").setValue(record.get('nmdoktergelar'));
				getNoAntrian();
            }
        },
		columns: [{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 170,
			sortable: true
		},{
			header: '<div style="text-align:center;">Spesialisasi</div>',
			dataIndex: 'nmspesialisasi',
			width: 105,
			sortable: true
		}]
	});
    
	var registrasirj_form = new Ext.form.FormPanel({ 
		id: 'fp.registrasirj',
		title: 'Registasi RJ',
		width: 900, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true, //margin: '0 0 15',
		autoScroll: true,
		tbar: [
			{ text: 'Kembali', iconCls: 'silk-arrow-undo', handler: function(){page_controller('0401');} },'-',
			{ text: 'Simpan', id:'bt.simpan', iconCls: 'silk-save', handler: function(){simpanRJ("fp.registrasirj");} },'-',
			{ text: 'Batal Registrasi RJ', id:'bt.batal', iconCls: 'silk-cancel', handler: function(){batalRJ("fp.registrasirj");} },'-',
			{ text: 'Cari Registrasi RJ', iconCls: 'silk-find', handler: function(){cariRegRJ();} },'-',
			{xtype: 'tbfill' }
		],
		defaults: { labelWidth: 150, labelAlign: 'right'},
        items: [{
			xtype: 'fieldset', title: '',
			id:'fs.registrasirj', layout: 'column', autowidth: true,
			defaults: { labelWidth: 150, labelAlign: 'right' }, 
			items: [{
				//COLUMN 1
				layout: 'form', columnWidth: 0.5,
				items: [{
					xtype: 'textfield',
					id: 'alamat',
					hidden:true
				},{
					xtype: 'textfield',
					id: 'notelp',
					hidden:true
				},{
					xtype: 'textfield',
					id: 'nohp',
					hidden:true
				},{
					xtype: 'textfield',
					id: 'tf.idres',
					width: 50,
					hidden:true
				},{
					xtype: 'textfield', fieldLabel: 'No. RM',
					id: 'tf.norm',
					width: 80, readOnly: true,
					style : 'opacity:0.6'
				},{
					xtype: 'textfield', fieldLabel: 'Nama Pasien',
					id: 'tf.nmpasien',
					width: 300, readOnly: true,
					style : 'opacity:0.6'
				},{
					xtype: 'combo', fieldLabel: 'Jenis Kelamin',
					id: 'cb.jkelamin', width: 150, 
					store: ds_jkelamin, valueField: 'idjnskelamin', displayField: 'nmjnskelamin',
					//value:Ext.getCmp('cb.jkelamin').lastSelectionText,
					editable: false, triggerAction: 'all',
					forceSelection: true, submitValue: true, mode: 'local',
					readOnly: true, style : 'opacity:0.6'
				},{
					xtype: 'container', fieldLabel: 'Umur',
					layout: 'hbox',
					items: [{
						xtype: 'textfield', id: 'tf.umurthn',
						width: 30, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'label', id: 'lb.umurthn', text: 'Tahun', margins: '0 10 0 5',
					},{ 	
						xtype: 'textfield', id: 'tf.umurbln', 
						width: 30, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'label', id: 'lb.umurbln', text: 'Bulan', margins: '0 10 0 5',
					},{
						xtype: 'textfield', id: 'tf.umurhari',
						width: 30, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'label', id: 'lb.umurhari', text: 'Hari', margins: '0 10 0 5'
					}]
				},{
					xtype: 'compositefield',
					fieldLabel: 'Penjamin',
					items: [{
						xtype: 'textfield',
						id: 'tf.penjamin', allowBlank: false,
						value:'Pasien Umum', readOnly: true,
						width: 250
					},{
						xtype: 'label', id: 'lb.bintang',
						text: '*', margins: '0 5 0 5',
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.penjamin',
						width: 28,
						handler: function() {
							dftPenjamin();
						}
					}]
				},{
					xtype: 'combo', fieldLabel : 'Cara Datang',
					id: 'cb.caradatang', width: 300, 
					store: ds_caradatang, valueField: 'idcaradatang', displayField: 'nmcaradatang',
					editable: false, triggerAction: 'all', value:'Datang Sendiri',
					forceSelection: true, submitValue: true, mode: 'local',
					emptyText:'Pilih...',
					listeners:{
						select:function(combo, records, eOpts){
							if(records.get('idcaradatang') != 1)
							{
								Ext.getCmp('tf.upengirim').setReadOnly(false);
								Ext.getCmp('tf.upengirim').el.setStyle('opacity', 1);
								Ext.getCmp('btn.upengirim').enable();
								Ext.getCmp('tf.dkirim').setReadOnly(false);
								Ext.getCmp('tf.dkirim').el.setStyle('opacity', 1);
								Ext.getCmp('btn.dkirim').enable();
							} else {
								Ext.getCmp('tf.upengirim').setReadOnly(true);
								Ext.getCmp('tf.upengirim').setValue('');
								Ext.getCmp('tf.upengirim').el.setStyle('opacity', 0.6);
								Ext.getCmp('btn.upengirim').disable();
								Ext.getCmp('tf.dkirim').setReadOnly(true);
								Ext.getCmp('tf.dkirim').setValue('');
								Ext.getCmp('tf.dkirim').el.setStyle('opacity', 0.6);
								Ext.getCmp('btn.dkirim').disable();
							}
						}
					}
				},{
					xtype: 'compositefield',
					fieldLabel: 'Unit Pengirim',
					items: [{
						xtype: 'textfield',
						id: 'tf.upengirim',
						width: 250, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.upengirim',
						width: 45, disabled: true,
						handler: function() {
							dftBagian();
						}
					}]
				},{
					xtype: 'compositefield',
					fieldLabel: 'Dokter Pengirim',
					items: [{
						xtype: 'textfield',
						id: 'tf.dkirim',
						width: 250, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.dkirim',
						width: 45, disabled: true,
						handler: function() {
							dftDokter();
						}
					}]
				},{
					xtype: 'textarea', fieldLabel: 'Catatan Registrasi',
					id  : 'ta.catatan', width : 300
				}]
			}, {
				//COLUMN 2
				layout: 'form', columnWidth: 0.5,
				items: [{
					xtype: 'container', fieldLabel: 'No. Registrasi',
					layout: 'hbox',
					items: [{
						xtype :'textfield',
						id :'tf.noreg',width:120, readOnly: true,
						style : 'opacity:0.6'
					}]
				},{
					xtype: 'container', fieldLabel: 'Tgl./Jam/Shift',
					layout: 'hbox',
					//defaults: { hideLabel: 'true' },
					items: [{	
						xtype: 'datefield', id: 'df.tglshift',
						width: 100, value: new Date(),
						format: 'd-m-Y'
					},{
						xtype: 'label', id: 'lb.garing1', text: '/', margins: '0 10 0 10',
					},{ 	
						xtype: 'textfield', id: 'tf.jamshift',
						width: 65
					},{
						xtype: 'label', id: 'lb.garing2', text: '/', margins: '0 10 0 10',
					},{
						xtype: 'textfield', id: 'tf.waktushift', 
						width: 60, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield', id: 'idshift',
						hidden:true
					}]
				},{
					xtype: 'compositefield',
					fieldLabel: 'Unit Pelayanan',
					items: [{
						xtype :'textfield',
						id :'idbagian', hidden: true
					},{
						xtype :'textfield', allowBlank: false,
						id :'tf.upelayanan', width:250, readOnly: true
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.upelayanan',
						width: 45,
						handler: function() {
							dftUpel();
						}
					}]
				},{
					xtype: 'textfield', id: 'tf.dokter', 
					fieldLabel: 'Dokter Praktek', allowBlank:false,
					width: 300,
					/*enableKeyEvents: true,
					 listeners:{
						keyup:function(){
							//ds_jadwalprakteknow.setBaseParam('idbagian', Ext.getCmp("idbagian").getValue()); 
							ds_jadwalprakteknow.setBaseParam('cquery', Ext.getCmp('tf.dokter').getValue());
							ds_jadwalprakteknow.reload();
						}
					} */
				},{
					xtype: 'container', fieldLabel: ' ',
					items: [grid_jdwldktr]
				},{
					xtype: 'numericfield', //fieldLabel: 'Jml. Pasien Antri',
					id: 'tf.jmlpantrires', width: 100, readOnly: true,
					style : 'opacity:0.6', hidden:true
				},{
					xtype: 'numericfield', fieldLabel: 'Jml. Pasien Antri',
					id: 'tf.jmlpantri', width: 100, readOnly: true,
					style : 'opacity:0.6'
				},{
					xtype :'numericfield', fieldLabel: 'Antrian ke',
					id :'tf.noantrian',width:100, readOnly: true,
					style : 'opacity:0.6'
				}]
			}]
		}],
		listeners: {
			afterrender: function () {
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/get_tgl_svr',
					method: 'POST',
					params: {
					
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp('df.tglshift').setValue(obj.date);
					},
					failure : function(){
						Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
					}
				});
			}
		}
	}); SET_PAGE_CONTENT(registrasirj_form);
	
	function umur(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tf.umurthn').setValue(year);
		Ext.getCmp('tf.umurbln').setValue(month);
		Ext.getCmp('tf.umurhari').setValue(day);
	}
	
	function bersihrj() {
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue();
		Ext.getCmp('cb.jkelamin').setValue();
		Ext.getCmp('tf.umurthn').setValue();
		Ext.getCmp('tf.umurbln').setValue();
		Ext.getCmp('tf.umurhari').setValue();
		Ext.getCmp('tf.penjamin').setValue('Pasien Umum');
		Ext.getCmp('cb.caradatang').setValue(1);
		Ext.getCmp('tf.upengirim').setValue();
		Ext.getCmp('tf.dkirim').setValue();
		Ext.getCmp('tf.noreg').setValue(); 
		//Ext.getCmp('df.tglshift').setValue(new Date());
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/get_tgl_svr',
			method: 'POST',
			params: {
			
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp('df.tglshift').setValue(obj.date);
			},
			failure : function(){
				Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
			}
		});
		Ext.getCmp('tf.upelayanan').setValue(' ');
		Ext.getCmp('tf.dokter').setValue(' ');
		Ext.getCmp('ta.catatan').setValue();
		Ext.getCmp('tf.upengirim').disable();
		Ext.getCmp('btn.upengirim').disable();
		Ext.getCmp('tf.dkirim').disable();
		Ext.getCmp('btn.dkirim').disable();
		//ds_jadwalprakteknow.setBaseParam('idbagian',null);
	}
	
	function check(e) {
		var cek = e.checked;
		/* ds_jadwalprakteknow.setBaseParam('idbagian',Ext.getCmp('idbagian').getValue());
		if(cek){
			ds_jadwalprakteknow.setBaseParam('shift','');
			ds_jadwalprakteknow.setBaseParam('allday','1');
			ds_jadwalprakteknow.reload();
		}else{
			ds_jadwalprakteknow.setBaseParam('shift',Ext.getCmp("idshift").getValue());
			ds_jadwalprakteknow.setBaseParam('allday',null);
			ds_jadwalprakteknow.reload();
		} */
	}

	function simpanRJ(namaForm) {
		var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'registrasi_controller/insorupd_registrasi',
			method: 'POST',
			params: {
				ureg 	: 'RJ',
				idres 	: Ext.getCmp('tf.idres').getValue()
			},
			success: function(registrasirj_form, o) {
				if(o.result.success == true) {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
					Ext.getCmp('tf.noreg').setValue(o.result.noreg);
					Ext.getCmp('tf.noantrian').setValue(o.result.noantrian);
					Ext.getCmp('tf.jmlpantri').setValue(o.result.noantrian);
					Ext.getCmp('bt.simpan').disable();
					//myStopFunction();
				} else{
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			},
			failure: function (form, action) {
				waitmsg.hide();
				Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
			}
		});
	}
	
	function batalRJ(namaForm) {
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'registrasi_controller/batal_registrasi',
			method: 'POST',
			success: function(registrasirj_form, o) {
				if (o.result.success == true) {
					Ext.MessageBox.alert('Informasi', 'Registrasi Berhasil Dibatalkan');
					bersihrj();
				} else if (o.result.success == 'false') {
					Ext.MessageBox.alert('Informasi', 'Registrasi Gagal Dibatalkan');
				}
			}
		});
	}
	
	function getNoAntrian(){
		Ext.Ajax.request({
			url:BASE_URL + 'reservasi_controller/getNoantrian',
			method:'POST',
			params: {
				nmbagian		: Ext.getCmp('tf.upelayanan').getValue(),
				nmdokter		: Ext.getCmp('tf.dokter').getValue()
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.jmlpantri").setValue(obj.antrian);
			}
		});
	}
	
	function cariRegRJ(){
		var ds_vregistrasi = dm_vregistrasi();
		ds_vregistrasi.setBaseParam('cek','RJ');
		ds_vregistrasi.setBaseParam('groupby','noreg');
		ds_vregistrasi.setBaseParam('nokuitansi',2);
		function keyToView_reg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterReg" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_cari_regrj = new Ext.grid.ColumnModel([
			{
				header: 'No. Registrasi',
				dataIndex: 'noreg',
				width: 80,
				renderer: keyToView_reg
			},{
				header: 'Tgl. Registrasi',
				dataIndex: 'tglreg',
				renderer: Ext.util.Format.dateRenderer('d-M-Y'),
				width: 100
			},{
				header: 'No. RM',
				dataIndex: 'norm',
				width: 80
			},{
				header: 'Nama Pasien',
				dataIndex: 'nmpasien',
				width: 150
			},{
				header: 'Unit Pelayanan',
				dataIndex: 'nmbagian',
				width: 130
			},{
				header: 'Dokter',
				dataIndex: 'nmdoktergelar',
				width: 180
			},{
				header: 'No. Antrian',
				dataIndex: 'noantrian',
				width: 70
			},{
				header: 'Cara Datang',
				dataIndex: 'nmcaradatang',
				width: 130
			}
		]);
		var sm_cari_regrj = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_regrj = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_regrj = new Ext.PagingToolbar({
			store: ds_vregistrasi,
			pageSize:18,
			displayInfo: true,
			displayMsg: 'Data Registrasi Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var grid_find_cari_regrj= new Ext.grid.GridPanel({
			id: 'gp.find_regrj',
			ds: ds_vregistrasi,
			cm: cm_cari_regrj,
			sm: sm_cari_regrj,
			view: vw_cari_regrj,
			height: 300,
			width: 955,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_regrj,
			listeners: {
				cellclick: klik_cari_regrj
			}
		});
		var win_find_cari_regrj = new Ext.Window({
			title: 'Cari Registrasi',
			modal: true,
			items: [{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'button',
						text: 'Cari',
						id: 'btn.cari',
						iconCls:'silk-find',
						style: 'padding: 10px',
						width: 100,
						handler: function() {
							cAdvance();
						}
					},{
						xtype: 'button',
						text: 'Kembali',
						id: 'btn.kembali',
						iconCls:'silk-arrow-undo',
						style: 'padding: 10px',
						width: 110,
						handler: function() {
							win_find_cari_regrj.close();
						}
					}]
				},{
					xtype: 'container',
					style: 'padding: 5px',
					layout: 'column',
					defaults: {labelWidth: 1, labelAlign: 'right'},
					items:[{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnmibu',
							items:[{
								xtype: 'checkbox',
								id:'chb.cupel',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('cb.cupelayanan').enable();
											Ext.getCmp('cb.cupelayanan').focus();
										} else if(val == false){
											Ext.getCmp('cb.cupelayanan').disable();
											Ext.getCmp('cb.cupelayanan').setValue('');
										}
									}
								}
							},{
								xtype: 'combo',
								id: 'cb.cupelayanan', width: 230, 
								store: ds_bagianrj, valueField: 'idbagian', displayField: 'nmbagian',
								editable: false, triggerAction: 'all',
								forceSelection: true, submitValue: true, mode: 'local',
								emptyText:'Unit Pelayanan', disabled: true
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_cdokter',
							items:[{
								xtype: 'checkbox',
								id:'chb.cdokter',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('cb.cdokter').enable();
											Ext.getCmp('cb.cdokter').focus();
										} else if(val == false){
											Ext.getCmp('cb.cdokter').disable();
											Ext.getCmp('cb.cdokter').setValue('');
										}
									}
								}
							},{
								xtype: 'combo',
								id: 'cb.cdokter', width: 230, 
								store: ds_dokter, valueField: 'iddokter', displayField: 'nmdoktergelar',
								editable: false, triggerAction: 'all',
								forceSelection: true, submitValue: true, mode: 'local',
								emptyText:'Dokter', disabled: true
							}]
						}]
					},{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnorm',
							items:[{
								xtype: 'checkbox',
								id:'chb.crm',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.crm').enable();
											Ext.getCmp('tf.crm').focus();
										} else if(val == false){
											Ext.getCmp('tf.crm').disable();
											Ext.getCmp('tf.crm').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.crm',
								emptyText:'No. RM',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_cnmpasien',
							items:[{
								xtype: 'checkbox',
								id:'chb.cnmpasien',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.cnmpasien').enable();
											Ext.getCmp('tf.cnmpasien').focus();
										} else if(val == false){
											Ext.getCmp('tf.cnmpasien').disable();
											Ext.getCmp('tf.cnmpasien').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.cnmpasien',
								emptyText:'Nama Pasien',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						}]
					},{
						xtype: 'fieldset',
						columnWidth: .33,
						border: false,
						items: [{
							xtype: 'compositefield',
							id: 'comp_cnoreg',
							items:[{
								xtype: 'checkbox',
								id:'chb.cnoreg',
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('tf.cnoreg').enable();
											Ext.getCmp('tf.cnoreg').focus();
										} else if(val == false){
											Ext.getCmp('tf.cnoreg').disable();
											Ext.getCmp('tf.cnoreg').setValue('');
										}
									}
								}
							},{
								xtype: 'textfield',
								id: 'tf.cnoreg',
								emptyText:'No. Registrasi',
								width: 230, disabled: true,
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						},{
							xtype: 'compositefield',
							id: 'comp_ctglreg',
							items:[{
								xtype: 'checkbox',
								id:'chb.ctglreg',
								checked   : true,
								listeners: {
									check: function(checkbox, val){
										if(val == true){
											Ext.getCmp('df.ctglreg').enable();
											Ext.getCmp('df.ctglreg').focus();
										} else if(val == false){
											Ext.getCmp('df.ctglreg').disable();
											Ext.getCmp('df.ctglreg').setValue(new Date());
										}
									}
								}
							},{
								xtype: 'label', id: 'lb.ctglreg', text: 'Tgl. Reg', margins: '5 5 0 0',
							},{
								xtype: 'datefield',
								id: 'df.ctglreg',
								width: 100, value: new Date(),
								enableKeyEvents: true,
								listeners:{
									specialkey: function(field, e){
										if (e.getKey() == e.ENTER) {
											cAdvance();
										}
									}
								}
							}]
						}]
					}]
				},
				grid_find_cari_regrj
			]
		}).show();
		ds_vregistrasi.setBaseParam('ctglreg',Ext.util.Format.date(new Date(), 'Y-m-d'));
		ds_vregistrasi.reload();
		
		function klik_cari_regrj(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterReg'){
				var rec_cari_regrj = ds_vregistrasi.getAt(rowIdx);
				var regrj_noreg = rec_cari_regrj.data["noreg"];
				var regrj_norm = rec_cari_regrj.data["norm"];
				var regrj_nmpasien = rec_cari_regrj.data["nmpasien"];
				var regrj_jkelamin = rec_cari_regrj.data["idjnskelamin"];
				var regrj_umurtahun = rec_cari_regrj.data["umurtahun"];
				var regrj_umurbulan = rec_cari_regrj.data["umurbulan"];
				var regrj_umurhari = rec_cari_regrj.data["umurhari"];
				var regrj_penjamin = rec_cari_regrj.data["nmpenjamin"];
				var regrj_idcaradatang = rec_cari_regrj.data["idcaradatang"];
				var regrj_nmbagiankirim = rec_cari_regrj.data["nmbagiankirim"];
				var regrj_idjnspelayanan = rec_cari_regrj.data["idjnspelayanan"];
				var regrj_ruangan = rec_cari_regrj.data["nmbagian"];
				var regrj_dokter = rec_cari_regrj.data["nmdoktergelar"];
				var regrj_catatan = rec_cari_regrj.data["catatanr"];
				var regrj_noantrian = rec_cari_regrj.data["noantrian"];
				
				if(rec_cari_regrj.data["nmdokterkirimdlm"]){
					var regrj_dokterkirim = rec_cari_regrj.data["nmdokterkirimdlm"];
				} else {
					var regrj_dokterkirim = rec_cari_regrj.data["nmdokterkirim"];
				}
				
				Ext.getCmp("tf.noreg").setValue(regrj_noreg);
				Ext.getCmp("tf.norm").setValue(regrj_norm);
				Ext.getCmp("tf.nmpasien").setValue(regrj_nmpasien);
				Ext.getCmp("cb.jkelamin").setValue(regrj_jkelamin);
				Ext.getCmp("tf.umurthn").setValue(regrj_umurtahun);
				Ext.getCmp("tf.umurbln").setValue(regrj_umurbulan);
				Ext.getCmp("tf.umurhari").setValue(regrj_umurhari);
				Ext.getCmp("tf.penjamin").setValue(regrj_penjamin);
				Ext.getCmp("cb.caradatang").setValue(regrj_idcaradatang);
				//Ext.getCmp("cb.jpelayanan").setValue(regrj_idjnspelayanan);
				Ext.getCmp("tf.upengirim").setValue(regrj_nmbagiankirim);
				Ext.getCmp("tf.dkirim").setValue(regrj_dokterkirim);
				Ext.getCmp("tf.upelayanan").setValue(regrj_ruangan);
				Ext.getCmp("tf.dokter").setValue(regrj_dokter);
				Ext.getCmp("ta.catatan").setValue(regrj_catatan);
				Ext.getCmp("tf.noantrian").setValue(regrj_noantrian);
				
				if(regrj_idcaradatang != 1)
				{
					Ext.getCmp('tf.upengirim').enable();
					Ext.getCmp('tf.upengirim').setReadOnly(false);
					Ext.getCmp('tf.upengirim').el.setStyle('opacity', 1);
					Ext.getCmp('btn.upengirim').enable();
					Ext.getCmp('tf.dkirim').setReadOnly(false);
					Ext.getCmp('tf.dkirim').el.setStyle('opacity', 1);
					Ext.getCmp('btn.dkirim').enable();
				} else {
					Ext.getCmp('tf.upengirim').setReadOnly(true);
					Ext.getCmp('tf.upengirim').el.setStyle('opacity', 0.6);
					Ext.getCmp('btn.upengirim').disable();
					Ext.getCmp('tf.dkirim').setReadOnly(true);
					Ext.getCmp('tf.dkirim').el.setStyle('opacity', 0.6);
					Ext.getCmp('btn.dkirim').disable();
				}
							win_find_cari_regrj.close();
			}
		}
	
		function cAdvance(){
			if(Ext.getCmp('chb.ctglreg').getValue() == true){
				ds_vregistrasi.setBaseParam('ctglreg',Ext.util.Format.date(Ext.getCmp('df.ctglreg').getValue(), 'Y-m-d'));
			} else {
				ds_vregistrasi.setBaseParam('ctglreg','');
			}
		
			ds_vregistrasi.setBaseParam('cnmjnspelayanan',Ext.getCmp('cb.cupelayanan').getValue());
			ds_vregistrasi.setBaseParam('cdokter',Ext.getCmp('cb.cdokter').getValue());
			ds_vregistrasi.setBaseParam('cnorm',Ext.getCmp('tf.crm').getValue());
			ds_vregistrasi.setBaseParam('cnmpasien',Ext.getCmp('tf.cnmpasien').getValue());
			ds_vregistrasi.setBaseParam('cnoreg',Ext.getCmp('tf.cnoreg').getValue());
			ds_vregistrasi.reload();
		}
	}
	
	function dftUpel(){
		function keyToView_upel(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterUpel" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_bagian = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idbagian'
			},{
				header: 'Kode Bagian',
				dataIndex: 'kdbagian',
				width: 100,
				renderer: keyToView_upel
			},{
				header: 'Unit Pelayanan',
				dataIndex: 'nmbagian',
				width: 245
			}
		]);
		var sm_cari_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagianrj,
			//displayInfo: true,
			//displayMsg: 'Data Dari {0} - {1} of {2}',
			//emptyMsg: 'No data to display'
		});
		var cari_cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_cari_bagian= new Ext.grid.GridPanel({
			ds: ds_bagianrj,
			cm: cm_cari_bagian,
			sm: sm_cari_bagian,
			view: vw_cari_bagian,
			height: 460,
			width: 350,
			plugins: cari_cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_bagian,
			listeners: {
				cellclick: klik_cari_bagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function klik_cari_bagian(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterUpel'){
				var rec_cari_bagian = ds_bagianrj.getAt(rowIdx);
				var var_cari_nmbagian = rec_cari_bagian.data["nmbagian"];
				var var_cari_idbagian = rec_cari_bagian.data["idbagian"];
				
				Ext.getCmp("tf.upelayanan").setValue(var_cari_nmbagian);
				Ext.getCmp("idbagian").setValue(var_cari_idbagian);
				
				ds_jadwalprakteknow.setBaseParam('idbagian',var_cari_idbagian);
				/* ds_jadwalprakteknow.setBaseParam('shift',Ext.getCmp("idshift").getValue());
				ds_jadwalprakteknow.setBaseParam('tgl',Ext.getCmp("df.tglshift").getValue()); */
				ds_jadwalprakteknow.reload({
					scope   : this,
					callback: function(records, operation, success) {
						var nmlkp = '';
						 ds_jadwalprakteknow.each(function (rec) { 
								nmlkp = rec.get('nmdoktergelar');
							});
						if(nmlkp == ""){
							Ext.getCmp('tf.dokter').setValue();
						}
						else{
							Ext.Ajax.request({
								url:BASE_URL + 'jadwalpraktek_controller/cekdktr',
								method:'POST',
								params: {
									idbagian		: Ext.getCmp('idbagian').getValue(),
									cquery			: Ext.getCmp('tf.dokter').getValue()
								},
								success: function(response){
									obj = Ext.util.JSON.decode(response.responseText);
									Ext.getCmp("tf.dokter").setValue(obj.nmdoktergelar);
								}
							});
						}
						return;
					}
				});
				getNoAntrian();
							win_find_cari_bagian.close();
			}
		}
	}
	
	function dftPenjamin(){
		function keyToView_penjamin(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterPenjamin" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_penjamin = new Ext.grid.ColumnModel([
			/* {
				hidden:true,
				dataIndex: 'idpenjamin'
			}, */{
				header: 'Nama Penjamin',
				dataIndex: 'nmpenjamin',
				width: 200,
				renderer: keyToView_penjamin
			},{
				header: 'Alamat',
				dataIndex: 'alamat',
				width: 300
			},{
				header: 'No. Telp',
				dataIndex: 'notelp',
				width: 150
			}
		]);
		var sm_cari_penjamin = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_penjamin = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_penjamin = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_penjamin,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_cari_penjamin = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_cari_penjamin= new Ext.grid.GridPanel({
			ds: ds_penjamin,
			cm: cm_cari_penjamin,
			sm: sm_cari_penjamin,
			view: vw_cari_penjamin,
			height: 460,
			width: 680,
			plugins: cari_cari_penjamin,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_penjamin,
			listeners: {
				cellclick: klik_cari_penjamin
			}
		});
		var win_find_cari_penjamin = new Ext.Window({
			title: 'Cari Penjamin',
			modal: true,
			items: [grid_find_cari_penjamin]
		}).show();

		function klik_cari_penjamin(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterPenjamin'){
				var rec_cari_penjamin = ds_penjamin.getAt(rowIdx);
				var var_cari_nmpenjamin = rec_cari_penjamin.data["nmpenjamin"];
				
				Ext.getCmp("tf.penjamin").setValue(var_cari_nmpenjamin);
							win_find_cari_penjamin.close();
			}
		}
	}
	
	function dftBagian(){
		function keyToView_bagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterBagian" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_bagian = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idbagian'
			},{
				header: 'Kode',
				dataIndex: 'kdbagian',
				width: 100,
				renderer: keyToView_bagian
			},{
				header: 'Unit/Ruangan Pelayanan',
				dataIndex: 'nmbagian',
				width: 245
			}
		]);
		var sm_cari_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagianrjriugd,
			//displayInfo: true,
			//displayMsg: 'Data Dari {0} - {1} of {2}',
			//emptyMsg: 'No data to display'
		});
		var cari_cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_cari_bagian= new Ext.grid.GridPanel({
			ds: ds_bagianrjriugd,
			cm: cm_cari_bagian,
			sm: sm_cari_bagian,
			view: vw_cari_bagian,
			height: 460,
			width: 350,
			plugins: cari_cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_bagian,
			listeners: {
				cellclick: klik_cari_bagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function klik_cari_bagian(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterBagian'){
				var rec_cari_bagian = ds_bagianrjriugd.getAt(rowIdx);
				var var_cari_nmbagian = rec_cari_bagian.data["nmbagian"];
				
				Ext.getCmp("tf.upengirim").setValue(var_cari_nmbagian);
							win_find_cari_bagian.close();
			}
		}
	}
	
	function dftDokter(){
		function keyToView_dokter(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDokter" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_dokter = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'iddokter'
			},{
				header: 'Kode',
				dataIndex: 'kddokter',
				width: 50,
				renderer: keyToView_dokter
			},{
				header: 'Nama Dokter',
				dataIndex: 'nmdoktergelar',
				width: 200
			},{
				header: 'Spesialisasi',
				dataIndex: 'nmspesialisasi',
				width: 155
			}
		]);
		var sm_cari_dokter = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_dokter = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_dokter = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_dokter,
			displayInfo: true,
			displayMsg: 'Data Dokter Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_cari_dokter = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_find_cari_dokter= new Ext.grid.GridPanel({
			ds: ds_dokter,
			cm: cm_cari_dokter,
			sm: sm_cari_dokter,
			view: vw_cari_dokter,
			height: 460,
			width: 410,
			plugins: cari_cari_dokter,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_dokter,
			listeners: {
				cellclick: klik_cari_dokter
			}
		});
		var win_find_cari_dokter = new Ext.Window({
			title: 'Cari Dokter',
			modal: true,
			items: [grid_find_cari_dokter]
		}).show();

		function klik_cari_dokter(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterDokter'){
				var rec_cari_dokter = ds_dokter.getAt(rowIdx);
				var var_cari_nmdoktergelar = rec_cari_dokter.data["nmdoktergelar"];
				
				Ext.getCmp("tf.dkirim").setValue(var_cari_nmdoktergelar);
							win_find_cari_dokter.close();
			}
		}
	}
	
}