function RItransaksi(){
	var myVar= setInterval(function(){myTimer()},1000);
	
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jamri"))
				RH.setCompValue("tf.jamri",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}
	
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("tf.shift").setValue(obj.nmshift);
		}
	});
	
	var ds_stpelayanan = dm_stpelayanan();
	var ds_notatransri = dm_notatransri();
	ds_notatransri.setBaseParam('start',0);
	ds_notatransri.setBaseParam('limit',500);
	ds_notatransri.setBaseParam('idregdet',null);
	ds_notatransri.setBaseParam('idbagian',999);
	
	var ds_notatransri2 = dm_notatransri();
	ds_notatransri2.setBaseParam('start',0);
	ds_notatransri2.setBaseParam('limit',500);
	ds_notatransri2.setBaseParam('idregdet',null);
	ds_notatransri2.setBaseParam('idbagian',999);
		
	var ds_vregistrasi = dm_vregistrasi();
	ds_vregistrasi.setBaseParam('start',0);
	ds_vregistrasi.setBaseParam('limit',6);
	ds_vregistrasi.setBaseParam('cek','RI');
	ds_vregistrasi.setBaseParam('riposisipasien',5);
	ds_vregistrasi.setBaseParam('groupby','noreg');
	ds_vregistrasi.setBaseParam('oposisipasien',1);
	ds_vregistrasi.setBaseParam('onmpasien',1);
	ds_vregistrasi.setBaseParam('khususri',1);
	ds_vregistrasi.setBaseParam('gnoreg',1);
	
	var ds_vregistrasi2 = dm_vregistrasipaspulang();
	ds_vregistrasi2.setBaseParam('start',0);
	ds_vregistrasi2.setBaseParam('limit',6);
	ds_vregistrasi2.setBaseParam('cek','RI');
	ds_vregistrasi2.setBaseParam('nokuitansi',1);
	ds_vregistrasi2.setBaseParam('groupby','noreg');
	ds_vregistrasi2.setBaseParam('onoreg',1);
	ds_vregistrasi2.setBaseParam('khususri',1);
	ds_vregistrasi2.setBaseParam('gnoreg',1);
	ds_vregistrasi2.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_vregistrasi2.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_vregistrasi2.setBaseParam('ctransfar',1);
	
	var ds_kuitansidet = dm_kuitansidet();
	ds_kuitansidet.setBaseParam('blank', 1);
	
	var ds_dokter = dm_dokter();
	var arr = [];
	var zkditem = '';
	var rownota = '';
	var rownota2 = '';
	var pmbyrn = 0;
	var pmbyrndjmn = 0;
	var totfarmasi = 0;
	var tottindakan = 0;
	var koder = 0;
	var cekkstok = 0;
	var idregdetugd = 0;
	var cekpenj = 0;
	var cekpembayaran = 0;
	
	var cbGrid = new Ext.grid.CheckboxSelectionModel({
		listeners: {
			rowselect : function( selectionModel, rowIndex, record){
				zkditem = record.get("kditem");
				zidjnstarif = record.get("idjnstarif");
				arr[rowIndex] = zkditem + '-' + zidjnstarif;
			},
			rowdeselect : function( selectionModel, rowIndex, record){
				arr.splice(rowIndex, 1);
			},
			beforerowselect : function (sm, rowIndex, keep, rec) {
				if (this.deselectingFlag && this.grid.enableDragDrop){
					this.deselectingFlag = false;
					this.deselectRow(rowIndex);
					return this.deselectingFlag;
				}
				return keep;
			}
		}
	});
	
	var grid_nota = new Ext.grid.EditorGridPanel({
		store: ds_notatransri,
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota = rowIndex;
            }
        },
		columns: [{
			header: '<div style="text-align:center;">Item Tindakan</div>',
			dataIndex: 'nmitem',
			width: 250
		},{
			header: '<div style="text-align:center;">Tarif</div>',
			dataIndex: 'tarif',
			align:'right',
			width: 90,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Qty</div>',
			dataIndex: 'qty',
			align:'right',
			width: 45,
		},{
			header: '<div style="text-align:center;">Satuan</div>',
			dataIndex: 'nmsatuan',
			width: 80
		},{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Diskon',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_edit.png',
				handler: function(grid, rowIndex) {
					fDiskon(rowIndex);
				}
			}]
		},{
			header: '<div style="text-align:center;">Total<br>Diskon</div>',
			dataIndex: 'drp',
			align:'right',
			width: 55,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Subtotal</div>',
			dataIndex: 'tarif2',
			align:'right',
			width: 100,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Dijamin<br>%</div>',
			dataIndex: 'dijaminprsn',
			align:'right',
			width: 50,
			editor: {
				xtype: 'numberfield',
				id: 'tf.dijaminprsn',
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_notatransri.getAt(rownota);
						if(!record.data['dijamin']){
							record.set('dijaminprsn', 0);
							Ext.getCmp('tf.dijaminprsn').setValue(0);
							Ext.MessageBox.alert('Informasi', 'Silahkan ceklis checkbox dijamin');
						} else {
							var tfdijaminprsn = Ext.getCmp('tf.dijaminprsn').getValue();
							var subtotal = record.get('tarif2');
							var dijaminprp = (subtotal*tfdijaminprsn)/100;
							var selisih = subtotal-dijaminprp;
							record.set('dijaminrp', dijaminprp);
							record.set('selisihf', selisih);
							totalnota();
						}
						cekTombol();
					}
				}
			},
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Dijamin<br>Rp</div>',
			dataIndex: 'dijaminrp',
			align:'right',
			width: 70,
			editor: {
				xtype: 'numberfield',
				id: 'tf.dijaminrp', 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_notatransri.getAt(rownota);
						if(!record.data['dijamin']){
							record.set('dijaminrp', 0);
							Ext.getCmp('tf.dijaminrp').setValue(0);
							Ext.MessageBox.alert('Informasi', 'Silahkan ceklis checkbox dijamin');
						} else {
							var tfdijaminrp = Ext.getCmp('tf.dijaminrp').getValue();
							var subtotal = record.get('tarif2');
							var dijaminprsn = (tfdijaminrp/subtotal)*100;
							var selisih = subtotal-tfdijaminrp;
							if(dijaminprsn=='Infinity' || dijaminprsn<0) dijaminprsn = 0;
							record.set('dijaminrp', tfdijaminrp);
							record.set('dijaminprsn', parseFloat(dijaminprsn));
							record.set('selisihf', selisih);
							totalnota();
						}
						cekTombol();
					}
				}
			},
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Selisih</div>',
			dataIndex: 'selisihf',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Dijamin</div>',
			dataIndex: 'dijamin',
			align:'right',
			width: 50,
			xtype: 'checkcolumn',
			processEvent: function(name, e, grid, rowIndex, colIndex){
				var record = grid.store.getAt(rowIndex);
				if (name == 'mousedown') {
					record.set(this.dataIndex, !record.data[this.dataIndex]);
					if(!record.data[this.dataIndex]){
						record.set('dijaminprsn', 0);
						record.set('dijaminrp', 0);
						record.set('selisihf', record.get('tarif2'));
						Ext.getCmp('tf.dijaminprsn').setValue(0);
						Ext.getCmp('tf.dijaminrp').setValue(0);
						totalnota();
					} else {
						record.set('dijaminprsn', 100);
						record.set('dijaminrp', record.get('tarif2'));
						record.set('selisihf', 0);
						totalnota();
					}
					cekTombol();
				}
			}
		}],
		bbar: [
			{ xtype:'tbfill' },'Jumlah Dijamin : ',
			{
				xtype: 'numericfield',
				id: 'tf.totaldijamin',
				value: 0,
				readOnly:true,
				style : 'opacity:0.6',
				width: 80,
				thousandSeparator:','
			},' Jumlah Selisih : ',
			{
				xtype: 'numericfield',
				id: 'tf.total',
				value: 0,
				readOnly:true,
				style : 'opacity:0.6',
				width: 100,
				thousandSeparator:','
			}
		]
	});
	
	var grid_notafarmasi = new Ext.grid.EditorGridPanel({
		store: ds_notatransri2,
		frame: true,
		height: 280,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota2',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota2 = rowIndex;
            }
        },
		columns: [{
			header: 'Item Obat/Alkes',
			dataIndex: 'nmitem',
			width: 130
		},{
			header: 'Tarif',
			dataIndex: 'tarif',
			align:'right',
			width: 67,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: 'Qty',
			dataIndex: 'qty',
			align:'right',
			width: 38,
		},{
			header: 'Satuan',
			dataIndex: 'nmsatuan',
			width: 45
		},{
			header: 'Subtotal',
			dataIndex: 'tarif2',
			align:'right',
			width: 75,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Dijamin<br>%</div>',
			dataIndex: 'dijaminprsnf',
			align:'right',
			width: 50,
			editor: {
				xtype: 'numberfield',
				id: 'tf.dijaminprsnf',
				enableKeyEvents: true,
				listeners:{
					keyup:function(name, e){
						var record = ds_notatransri2.getAt(rownota2);
						if(!record.data['dijaminf']){
							record.set('dijaminprsnf', 0);
							Ext.getCmp('tf.dijaminprsnf').setValue(0);
							Ext.MessageBox.alert('Informasi', 'Silahkan ceklis checkbox dijamin');
						} else {
							var tfdijaminprsn = Ext.getCmp('tf.dijaminprsnf').getValue();
							var subtotal = record.get('tarif2');
							var dijaminprsn = (subtotal*tfdijaminprsn)/100;
							var selisih = subtotal-dijaminprsn;
							record.set('dijaminrpf', dijaminprsn);
							record.set('selisihf', selisih);
							totalnota2();
						}
						cekTombol();
					}
				}
			},
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: '<div style="text-align:center;">Dijamin<br>Rp</div>',
			dataIndex: 'dijaminrpf',
			align:'right',
			width: 70,
			editor: {
				xtype: 'numberfield',
				id: 'tf.dijaminrpf', 
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_notatransri2.getAt(rownota2);
						if(!record.data['dijaminf']){
							record.set('dijaminrpf', 0);
							Ext.getCmp('tf.dijaminrpf').setValue(0);
							Ext.MessageBox.alert('Informasi', 'Silahkan ceklis checkbox dijamin');
						} else {
							var tfdijaminrp = Ext.getCmp('tf.dijaminrpf').getValue();
							var subtotal = record.get('tarif2');
							var dijaminprsn = (tfdijaminrp/subtotal)*100;
							var selisih = subtotal-tfdijaminrp;
							if(dijaminprsn=='Infinity' || dijaminprsn<0) dijaminprsn = 0;
							record.set('dijaminrpf', tfdijaminrp);
							record.set('dijaminprsnf', dijaminprsn);
							record.set('selisihf', selisih);
							totalnota2();
						}
						cekTombol();
					}
				}
			},
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Selisih</div>',
			dataIndex: 'selisihf',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Dijamin</div>',
			dataIndex: 'dijaminf',
			align:'right',
			width: 50,
			xtype: 'checkcolumn',
			processEvent: function(name, e, grid, rowIndex, colIndex){
				var record = grid.store.getAt(rowIndex);
				if (name == 'mousedown') {
					record.set(this.dataIndex, !record.data[this.dataIndex]);
					if(!record.data[this.dataIndex]){
						record.set('dijaminprsnf', 0);
						record.set('dijaminrpf', 0);
						record.set('selisihf', record.get('tarif2'));
						Ext.getCmp('tf.dijaminprsnf').setValue(0);
						Ext.getCmp('tf.dijaminprsnf').setValue(0);
						totalnota2();
					} else {
						record.set('dijaminprsnf', 100);
						record.set('dijaminrpf', record.get('tarif2'));
						record.set('selisihf', 0);
						totalnota2();
					}
					cekTombol();
				}
			}
		}],
		bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 400,
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.dijamin', text: 'Dijamin :', margins: '0 10 0 14',
					},{
						xtype: 'numericfield',
						id: 'tf.dijamintotalrf',
						value: 0,
						width: 60,
						readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					},{
						xtype: 'label', id: 'lb.totalrf', text: 'Total :', margins: '0 10 0 19',
					},{
						xtype: 'numericfield',
						id: 'tf.totalrf',
						value: 0,
						width: 90,
						readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.diskonprsn2', text: 'Diskon % :', margins: '0 10 0 2',
					},{
						xtype: 'numericfield',
						id: 'tf.diskonprsn',
						value: 0,
						width: 60,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								var disc = (Ext.getCmp('tf.totalrf').getValue() * (Ext.getCmp('tf.diskonprsn').getValue() / 100));
								Ext.getCmp('tf.diskonf').setValue(disc);
								totalnota2();
								cekTombol();
							}
						}
					},{
						xtype: 'label', id: 'lb.uangr', text: 'Racik :', margins: '0 10 0 15',
					},{
						xtype: 'numericfield',
						id: 'tf.uangr',
						value: 0,
						width: 90,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								totalnota2();
								cekTombol();
							}
						}
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.diskon2', text: 'Diskon Rp :', margins: '0 9 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.diskonf',
						value: 0,
						width: 60,
						thousandSeparator:',',
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								var disc = Ext.getCmp('tf.diskonf').getValue() / Ext.getCmp('tf.totalrf').getValue() * 100;
								Ext.getCmp('tf.diskonprsn').setValue(disc);
								totalnota2();
								cekTombol();
							}
						}
					},{
						xtype: 'label', id: 'lb.total2', text: 'Jumlah :', margins: '0 9 0 10',
					},{
						xtype: 'numericfield',
						id: 'tf.total2',
						value: 0,
						readOnly:true,
						style : 'opacity:0.6',
						width: 90,
						thousandSeparator:','
					}]
				}]
			}
		]
	});
	
	var grid_pembayaran = new Ext.grid.GridPanel({
		store: ds_kuitansidet,
		frame: true,
		height: 280,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_pembayaran',
		forceFit: true,
		autoScroll: true,
		columnLines: true,
		loadMask: true,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			disabled: true,
			iconCls: 'silk-add',
			handler: function() {
				fPemb();
			}
		},
		{ xtype:'tbfill' },
		{
			xtype: 'compositefield',
			width: 200,
			defaults: { labelWidth: 80, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				border: false,
				width: 200,
				style: 'padding:0px; margin: 0px;',
				defaults: { labelWidth: 100, labelAlign: 'right'},
				items: [
					{
						xtype: 'checkbox',
						id:'chb.plafond',
						disabled: true,
						fieldLabel: 'Plafond',
						listeners: {
							check: function(checkbox, val){
								if(val == true){
									ds_notatransri.each(function (rec) {
										rec.set('dijaminprsn', 100); 
										rec.set('dijaminrp', rec.get('tarif2')); 
										rec.set('selisihf', 0);
										rec.set('dijamin', true);
									});
									ds_notatransri2.each(function (rec) {
										rec.set('dijaminprsnf', 100); 
										rec.set('dijaminrpf', rec.get('tarif2')); 
										rec.set('selisihf', 0);
										rec.set('dijaminf', true);
									});
									totalnota();
									totalnota2();
								} else if(val == false){
									ds_notatransri.each(function (rec) {
										rec.set('dijaminprsn', 0); 
										rec.set('dijaminrp', 0); 
										rec.set('selisihf', rec.get('tarif2'));
										rec.set('dijamin', false);
									});
									ds_notatransri2.each(function (rec) {
										rec.set('dijaminprsnf', 0); 
										rec.set('dijaminrpf', 0); 
										rec.set('selisihf', rec.get('tarif2'));
										rec.set('dijaminf', false);
									});
									totalnota();
									totalnota2();
								}
								cekTombol();
							}
						}
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'button',
							text: ' Detail ',
							id: 'btn.ugddet',
							cls: 'btnbg-cutom',
							width: 38,
							margins: '0 0 0 3',
							disabled: true,
							handler: function() {
								fNotaUGD();
							}
						},{
							xtype: 'numericfield',
							id: 'tf.tghnudg',
							margins: '0 0 0 2',
							fieldLabel: 'Tagihan UGD',
							width: 70,
							value: 0,
							disabled: true
						}]
					},{
						xtype: 'numericfield',
						id: 'tf.totaljumlah',
						fieldLabel: 'Total Jumlah',
						width: 110,
						value: 0,
						disabled: true
					},{
						xtype: 'numericfield',
						id: 'tf.deposit',
						fieldLabel: 'Deposit',
						width: 110,
						value: 0,
						disabled: true
					},{
						xtype: 'numericfield',
						id: 'tf.ttldijamin',
						fieldLabel: 'Total Dijamin',
						width: 110,
						value: 0,
						disabled: true
					},{
						xtype: 'numericfield',
						id: 'tf.tottagihan',
						fieldLabel: 'Total Bayar',
						value: 0,
						readOnly:true,
						style : 'opacity:0.6',
						width: 110,
						thousandSeparator:','
					}
				]
			}]
		}],
		columns: [{
                xtype: 'actioncolumn',
                width: 42,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex, colIndex) {
						var rec = ds_kuitansidet.getAt(rowIndex);
						if(rec.data.nmcarabayar == 'Dijamin'){
							pmbyrndjmn -= rec.data.jumlah;
						} else {
							pmbyrn -= rec.data.jumlah;
						}
						ds_kuitansidet.removeAt(rowIndex);
						totalnota3();
                    }
                }]
        },{
			header: 'Bayar',
			width: 40,
			dataIndex: 'nmcarabayar'
		},{
			header: 'Bank',
			width: 35,
			dataIndex: 'nmbank'
		},{
			header: 'No. Kartu',
			width: 80,
			dataIndex: 'nokartu'
		},{
			header: 'Total',
			width: 75,
			dataIndex: 'jumlah',
			xtype: 'numbercolumn', format:'0,000'
		}],
		bbar: [
			{
				xtype: 'fieldset',
				border: false,
				width: 300,
				style: 'padding:0px; margin: 0px;',
				items: [{
					xtype: 'compositefield',
					defaults: { labelWidth: 100, labelAlign: 'right'},
					items: [{
						xtype: 'numericfield',
						id: 'tf.jumlah',
						fieldLabel: 'Jumlah Total',
						width: 90,
						value: 0,
						readOnly:true, style: 'opacity:0.6'
					}]
				}]
			}
		]
	});
	
	var paging_reg = new Ext.PagingToolbar({
		pageSize: 6,
		store: ds_vregistrasi,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230
	})];
	
	var grid_reg = new Ext.grid.GridPanel({
		title: 'Pasien Belum Pulang',
		store: ds_vregistrasi,
		frame: true,
		height: 270,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_reg',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_freg = ds_vregistrasi.getAt(rowIndex);
				var freg_noreg = rec_freg.data["noreg"];
				var freg_norm = rec_freg.data["norm"];
				var freg_nmpasien = rec_freg.data["nmpasien"];
				var freg_atasnama = rec_freg.data["atasnama"];
				var freg_idbagian = rec_freg.data["idbagian"];
				var freg_nmbagian = rec_freg.data["nmbagian"];
				var freg_nmdokter = rec_freg.data["nmdoktergelar"];
				var freg_nonota = rec_freg.data["nonota"];
				var freg_nona = rec_freg.data["nona"];
				var freg_nokuitansi = rec_freg.data["nokuitansi"];
				var freg_total = rec_freg.data["total"];
				var freg_diskonri = rec_freg.data["diskonr"];
				var freg_uangri = rec_freg.data["uangr"];
				var freg_idstpelayanan = rec_freg.data["idstpelayanan"];
				var freg_tglkuitansi = rec_freg.data["tglkuitansi"];
				var freg_jamkuitansi = rec_freg.data["jamkuitansi"];
				var freg_catatannota = rec_freg.data["catatannota"];
				var freg_tgllahirp = rec_freg.data["tgllahirp"];
				var freg_idjnskelamin = rec_freg.data["idjnskelamin"];
				var freg_nmkamar = rec_freg.data["nmkamar"];
				var freg_nmbed = rec_freg.data["nmbed"];
				var freg_nmklstarif = rec_freg.data["nmklstarif"];
				var freg_nmstposisipasien = rec_freg.data["nmstposisipasien"];
				var freg_tglkeluar = rec_freg.data["tglkeluar"];
				var freg_jamkeluar = rec_freg.data["jamkeluar"];
				var freg_deposit = rec_freg.data["deposit"];
				var freg_penjamin = rec_freg.data["nmpenjamin"];
				
				Ext.getCmp("tf.noreg").setValue(freg_noreg);
				Ext.getCmp("tf.norm").setValue(freg_norm);
				Ext.getCmp("tf.nmpasien").setValue(freg_nmpasien);
				Ext.getCmp("tf.upel").setValue(freg_nmbagian);
				Ext.getCmp("tf.nmkamar").setValue(freg_nmkamar);
				Ext.getCmp("tf.nmbed").setValue(freg_nmbed);
				Ext.getCmp("tf.dokter").setValue(freg_nmdokter);
				Ext.getCmp("tf.nmklstarif").setValue(freg_nmklstarif);
				Ext.getCmp("tf.nonota").setValue(freg_nonota);
				Ext.getCmp("tf.nona").setValue(freg_nona);
				Ext.getCmp("tf.posisipasien").setValue(freg_nmstposisipasien);
				Ext.getCmp("df.tglkeluar").setValue(freg_tglkeluar);
				Ext.getCmp("tf.jamrikeluar").setValue(freg_jamkeluar);
				Ext.getCmp("tf.penjamin").setValue(freg_penjamin);
				Ext.getCmp("btn_add").enable();
				Ext.getCmp("chb.plafond").enable();
				Ext.getCmp("chb.plafond").setValue(0);
				if(freg_atasnama == null) Ext.getCmp("tf.an").setValue(freg_nmpasien);
				else Ext.getCmp("tf.an").setValue(freg_atasnama);
				ds_notatransri.setBaseParam('noreg',freg_noreg);
				ds_notatransri.setBaseParam('koder',0);
				ds_notatransri.setBaseParam('idbagian',null);
				ds_notatransri2.setBaseParam('noreg',freg_noreg);
				ds_notatransri2.setBaseParam('koder',1);
				ds_notatransri2.setBaseParam('idbagian',null);
				ds_kuitansidet.setBaseParam('blank', 0);
				ds_kuitansidet.setBaseParam('nokuitansi',freg_nokuitansi);
				pmbyrn = 0;
				fTotal();
				Ext.getCmp('tf.utyd').setValue();
				Ext.getCmp('tf.kembalian').setValue();
				if(freg_diskonri == null){
					Ext.getCmp('tf.diskonf').setValue(0);
					Ext.getCmp('tf.diskonprsn').setValue(0);
				} else Ext.getCmp('tf.diskonf').setValue(freg_diskonri);
				if(freg_uangri == null) Ext.getCmp('tf.uangr').setValue(0);
				else Ext.getCmp('tf.uangr').setValue(freg_uangri);
				if(freg_idstpelayanan == null) Ext.getCmp('cb.stpelayanan').setValue(1);
				else Ext.getCmp('cb.stpelayanan').setValue(freg_idstpelayanan);
				if(freg_deposit == null) Ext.getCmp('tf.deposit').setValue(0);
				else Ext.getCmp('tf.deposit').setValue(freg_deposit);
				if(freg_catatannota == null)freg_catatannota ='Transaksi Rawat Inap';
				Ext.getCmp('tf.catatan').setValue(freg_catatannota);
				Ext.getCmp('jkel').setValue(freg_idjnskelamin);
				if(freg_tglkuitansi == null) //Ext.getCmp('df.tgl').setValue(new Date());
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/get_tgl_svr',
					method: 'POST',
					params: {
					
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp('df.tgl').setValue(obj.date);
					},
					failure : function(){
						Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
					}
				});
				else Ext.getCmp('df.tgl').setValue(freg_tglkuitansi);
				if(freg_jamkuitansi == null){
					//myVar = setInterval(function(){myTimer()},1000);
					var formattedValue = Ext.util.Format.date(new Date(), 'H:i:s');
					Ext.getCmp('tf.jamri').setValue(formattedValue);
					myStopFunction();
				} else {
					Ext.getCmp('tf.jamri').setValue(freg_jamkuitansi);
					myStopFunction();
				}
				umur(new Date(freg_tgllahirp));
				cekKartuStok();
				cekTransfer();
				cekpembayaran = 0;
            }
        },
		columns: [{
			header: 'No. Reg',
			dataIndex: 'noreg',
			width: 75,
			sortable: true
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 75,
			sortable: true
		},{
			header: 'Pasien',
			dataIndex: 'nmpasien',
			width: 110,
			sortable: true
		},{
			header: 'L/P',
			dataIndex: 'kdjnskelamin',
			width: 30,
			sortable: true
		},{
			header: 'Ruangan',
			dataIndex: 'nmbagian',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Lahir',
			dataIndex: 'tgllahirp',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Posisi Pasien',
			dataIndex: 'nmstposisipasien',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Registrasi',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Dokter',
			dataIndex: 'nmdoktergelar',
			width: 100,
			sortable: true
		},{
			header: 'Penjamin',
			dataIndex: 'nmpenjamin',
			width: 100,
			sortable: true
		}],
		bbar: paging_reg,
		plugins: cari_reg
	});
	
	var paging_reg2 = new Ext.PagingToolbar({
		pageSize: 7,
		store: ds_vregistrasi2,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg2 = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230
	})];
	
	var grid_reg2 = new Ext.grid.GridPanel({
		//title: 'Pasien Sudah Pulang',
		store: ds_vregistrasi2,
		frame: false,
		height: 270,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_farmasi',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		plugins: cari_reg2,
		tbar:[],
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_freg = ds_vregistrasi2.getAt(rowIndex);
				var freg_noreg = rec_freg.data["noreg"];
				var freg_norm = rec_freg.data["norm"];
				var freg_nmpasien = rec_freg.data["nmpasien"];
				var freg_atasnama = rec_freg.data["atasnama"];
				var freg_idbagian = rec_freg.data["idbagian"];
				var freg_nmbagian = rec_freg.data["nmbagian"];
				var freg_nmdokter = rec_freg.data["nmdoktergelar"];
				var freg_nonota = rec_freg.data["nonota"];
				var freg_nona = rec_freg.data["nona"];
				var freg_nokuitansi = rec_freg.data["nokuitansi"];
				var freg_total = rec_freg.data["total"];
				var freg_diskonri = rec_freg.data["diskonr"];
				var freg_uangri = rec_freg.data["uangr"];
				var freg_idstpelayanan = rec_freg.data["idstpelayanan"];
				var freg_tglkuitansi = rec_freg.data["tglkuitansi"];
				var freg_jamkuitansi = rec_freg.data["jamkuitansi"];
				var freg_catatannota = rec_freg.data["catatannota"];
				var freg_catatands = rec_freg.data["catatands"];
				var freg_tgllahirp = rec_freg.data["tgllahirp"];
				var freg_idjnskelamin = rec_freg.data["idjnskelamin"];
				var freg_nmkamar = rec_freg.data["nmkamar"];
				var freg_nmbed = rec_freg.data["nmbed"];
				var freg_nmklstarif = rec_freg.data["nmklstarif"];
				var freg_nmstposisipasien = rec_freg.data["nmstposisipasien"];
				var freg_tglkeluar = rec_freg.data["tglkeluar"];
				var freg_jamkeluar = rec_freg.data["jamkeluar"];
				var freg_deposit = rec_freg.data["deposit"];
				var freg_stplafond = rec_freg.data["stplafondna"];
				var freg_penjamin = rec_freg.data["nmpenjamin"];
				
				Ext.getCmp("tf.noreg").setValue(freg_noreg);
				Ext.getCmp("tf.norm").setValue(freg_norm);
				Ext.getCmp("tf.nmpasien").setValue(freg_nmpasien);
				Ext.getCmp("tf.upel").setValue(freg_nmbagian);
				Ext.getCmp("tf.nmkamar").setValue(freg_nmkamar);
				Ext.getCmp("tf.nmbed").setValue(freg_nmbed);
				Ext.getCmp("tf.dokter").setValue(freg_nmdokter);
				Ext.getCmp("tf.nmklstarif").setValue(freg_nmklstarif);
				Ext.getCmp("tf.nonota").setValue(freg_nonota);
				Ext.getCmp("tf.nona").setValue(freg_nona);
				Ext.getCmp("tf.posisipasien").setValue(freg_nmstposisipasien);
				Ext.getCmp("df.tglkeluar").setValue(freg_tglkeluar);
				Ext.getCmp("tf.jamrikeluar").setValue(freg_jamkeluar);
				Ext.getCmp("tf.penjamin").setValue(freg_penjamin);
				Ext.getCmp("btn_add").enable();
				Ext.getCmp("chb.plafond").disable();
				Ext.getCmp("chb.plafond").setValue(freg_stplafond);
				if(freg_atasnama == null) Ext.getCmp("tf.an").setValue(freg_nmpasien);
				else Ext.getCmp("tf.an").setValue(freg_atasnama);
				ds_notatransri.setBaseParam('noreg',freg_noreg);
				ds_notatransri.setBaseParam('koder',0);
				ds_notatransri.setBaseParam('idbagian',null);
				//ds_notatransri2.setBaseParam('noreg',rec_freg.data["idregdet"]);
				ds_notatransri2.setBaseParam('noreg',freg_noreg);
				ds_notatransri2.setBaseParam('koder',1);
				ds_notatransri2.setBaseParam('idbagian',null);
				ds_kuitansidet.setBaseParam('blank', 0);
				ds_kuitansidet.setBaseParam('nokuitansi',freg_nokuitansi);
				pmbyrn = 0;
				fTotal();
				Ext.getCmp('tf.utyd').setValue();
				Ext.getCmp('tf.kembalian').setValue();
				if(freg_diskonri == null){
					Ext.getCmp('tf.diskonf').setValue(0);
					Ext.getCmp('tf.diskonprsn').setValue(0);
				} else Ext.getCmp('tf.diskonf').setValue(freg_diskonri);
				if(freg_uangri == null) Ext.getCmp('tf.uangr').setValue(0);
				else Ext.getCmp('tf.uangr').setValue(freg_uangri);
				if(freg_idstpelayanan == null) Ext.getCmp('cb.stpelayanan').setValue(1);
				else Ext.getCmp('cb.stpelayanan').setValue(freg_idstpelayanan);
				if(freg_deposit == null) Ext.getCmp('tf.deposit').setValue(0);
				else Ext.getCmp('tf.deposit').setValue(freg_deposit);
				Ext.getCmp('tf.catatan').setValue(freg_catatannota);
				Ext.getCmp('tf.catatandskn').setValue(freg_catatands);
				Ext.getCmp('jkel').setValue(freg_idjnskelamin);
				if(freg_tglkuitansi == null) Ext.getCmp('df.tgl').setValue(new Date());
				else Ext.getCmp('df.tgl').setValue(freg_tglkuitansi);
				if(freg_jamkuitansi == null){
					//myVar = setInterval(function(){myTimer()},1000);
					Ext.getCmp('tf.jamri').setValue(freg_jamkuitansi);
					myStopFunction();
				} else {
					Ext.getCmp('tf.jamri').setValue(freg_jamkuitansi);
					myStopFunction();
				}
				umur(new Date(freg_tgllahirp));
				cekKartuStok();
				cekTransfer();
				cekpembayaran = 1;
            }
        },
		columns: [{
			header: 'No. Reg',
			dataIndex: 'noreg',
			width: 75,
			sortable: true
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 75,
			sortable: true
		},{
			header: 'Pasien',
			dataIndex: 'nmpasien',
			width: 110,
			sortable: true
		},{
			header: 'L/P',
			dataIndex: 'kdjnskelamin',
			width: 30,
			sortable: true
		},{
			header: 'Ruangan',
			dataIndex: 'nmbagian',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Lahir',
			dataIndex: 'tgllahirp',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Posisi Pasien',
			dataIndex: 'nmstposisipasien',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Registrasi',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Dokter',
			dataIndex: 'nmdoktergelar',
			width: 100,
			sortable: true
		},{
			header: 'Penjamin',
			dataIndex: 'nmpenjamin',
			width: 100,
			sortable: true
		}],
		bbar: paging_reg2
	});
	
	var ritransaksi_form = new Ext.form.FormPanel({
		id: 'fp.ritransaksi',
		title: 'Nota Akhir RI',
		width: 900,
		Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			/*{ text: 'Baru', iconCls: 'silk-add', handler: function(){bersihRitransaksi();} },'-',*/
			{ text: 'Simpan', id:'btn.simpan', iconCls: 'silk-save', disabled:true, handler: function(){simpanRIT();} },'-',
			{ text: 'Nota Sementara', id:'btn.cns', iconCls: 'silk-printer', handler: function(){cetakRINotaS();} },'-',
			{ text: 'Cetak Nota', id:'btn.cetaknota', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRINota();} },'-',
			{ text: 'Cetak Excel Nota', id:'btn.excelnota', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRIExcel();} },'-',
			{ text: 'Cetak Kuitansi', id:'btn.cetakkui', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRIKui();} },'-',
			{ text: 'Cetak Kuitansi Penjamin', id:'btn.cetakkuipenj', iconCls: 'silk-printer', disabled:true, handler: function(){cetakKuiPenj();} },'-',
			{ xtype: 'tbfill' },'->'
		],
        items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:180,
				boxMaxHeight:180,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. RM',
						id: 'tf.norm',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					},{
						xtype: 'label', id: 'lb.noreg', text: 'No. Registrasi', margins: '0 10 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.noreg',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					}]
				},{
					xtype: 'textfield', fieldLabel:'Nama Pasien',
					id: 'tf.nmpasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield', fieldLabel:'Ruangan',
						id: 'tf.upel',
						readOnly: true, style : 'opacity:0.6',
						width: 80,
					},{
						xtype: 'label', id: 'lb.kamarbed', text: 'Kamar / Bed', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmkamar', 
						readOnly: true, style : 'opacity:0.6',
						width: 35,
					},{
						xtype: 'label', id: 'lb.garing', text: '/', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmbed', 
						readOnly: true, style : 'opacity:0.6',
						width: 38,
					}]
				},{
					xtype: 'textfield', fieldLabel:'Kelas Tarif',
					id: 'tf.nmklstarif',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Dokter',
					id: 'tf.dokter',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Posisi Pasien',
					id: 'tf.posisipasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:180,
				boxMaxHeight:180,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'textfield', fieldLabel:'No. Nota',
					id: 'tf.nonota',
					readOnly: true, style : 'opacity:0.6',
					width: 100, hidden: true
				},{
					xtype: 'textfield',
					id: 'tf.nona',
					hidden: true
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl./Jam/Shift', id: 'df.tgl',
						width: 100, value: new Date(),
						format: 'd-m-Y',
					},{
						xtype: 'label', id: 'lb.garing3', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jamri',
						width: 65
					},{
						xtype: 'label', id: 'lb.garing4', text: '/'
					},{
						xtype: 'textfield', id: 'tf.shift',
						width: 60, disabled: true
					}]
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl./Jam Keluar', id: 'df.tglkeluar',
						width: 100,
						format: 'd-m-Y',
						disabled: true
					},{
						xtype: 'label', id: 'lb.garing31', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jamrikeluar',
						disabled: true,
						width: 65
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Atas Nama',
					id: 'tf.an', anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel: 'Catatan',
					id: 'tf.catatan', anchor: "100%", value: 'Transaksi Rawat Inap'
				},{
					xtype: 'textfield', fieldLabel: 'Catatan Diskon',
					id: 'tf.catatandskn', anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Penjamin',
					id: 'tf.penjamin',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'combo', fieldLabel : 'Status Pelayanan',
					id: 'cb.stpelayanan', anchor: "100%",
					store: ds_stpelayanan, valueField: 'idstpelayanan', displayField: 'nmstpelayanan',
					editable: false, triggerAction: 'all', allowBlank: false,
					forceSelection: true, submitValue: true, mode: 'local',
					emptyText:'Pilih...', value: 'Umum', hidden: true
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_nota]
				}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 0.6,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_notafarmasi]
				}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 0.4,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_pembayaran]
				}]
		},{
			xtype: 'textfield',
			id: 'tahun', hidden: true
		},{
			xtype: 'textfield',
			id: 'jkel', hidden: true
		}]
	});
	
	var transRIDispPanel = new Ext.form.FormPanel({
		id: 'fp.transRIDispPanel',
		name: 'fp.transRIDispPanel',
		border: false, 
		forceFit:true,
		frame: true,
		autoScroll:true,
		labelAlign: 'top',
		layout: 'anchor',
		items: [
		{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_reg]
		},{
			layout: 'form',
			//bodyStyle: 'padding:5px 5px 5px 5px',
			title: 'Pasien Sudah Pulang',
			frame: true,
			tbar: [{
				xtype: 'compositefield',
				width: 300,
				defaults: { labelWidth: 50, labelAlign: 'right'},
				items: [/* {
					xtype: 'fieldset',
					border: false,
					width: 135,
					style: 'padding:0px; margin: 0px;',
					defaults: { labelWidth: 50, labelAlign: 'right'},
					items: []
				} */
				{
					xtype: 'label', id: 'lb.a', text: 'Periode :', margins: '3 7 0 22',
				},{
					xtype: 'datefield', id: 'df.tglawal',
					width: 80, value: new Date(),
					labelStyle: 'float:left;',
					format: 'd-m-Y',
					listeners:{
						select: function(field, newValue){
							cAdvance();
						},
						/* change : function(field, newValue){
							cAdvance();
						} */
					}
				},{
					xtype: 'label', id: 'lb.ah', text: 's/d', margins: '3 6 0 2',
				},{
					xtype: 'datefield', id: 'df.tglakhir',
					width: 80, value: new Date(),
					labelStyle: 'float:left;',
					format: 'd-m-Y',
					listeners:{
						select: function(field, newValue){
							cAdvance();
						},
						/* change : function(field, newValue){
							cAdvance();
						} */
					}
				}]
			}],
			items: [grid_reg2]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 90, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:75,
				boxMaxHeight:75,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'numericfield', fieldLabel:'Uang Diterima',
					id: 'tf.utyd',
					anchor: "100%",
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							hitungKembalian();
						}
					}
				},{
					xtype: 'numericfield', fieldLabel:'Kembalian',
					id: 'tf.kembalian',
					anchor: "100%",
					disabled:true
				}]
			}]
		}]
	});
	
	var transRIPanel = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Transaksi Rawat Inap',
		frame: false,
		border: true,
		margins: '0 0 5 0',
		plain: true,		
		forceFit: true,
		layout: 'border',
		items: [{
			region: 'center',
			xtype: 'panel',
			border: true,
			layout: 'fit',
			items: [ritransaksi_form],
		},
		{
			region: 'east',
			xtype: 'panel',
			title: '', border: true,
			layout:'fit', width:350,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [transRIDispPanel],
		}],		
		listeners: {
			afterrender: function () {
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/get_tgl_svr',
					method: 'POST',
					params: {
					
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp('df.tgl').setValue(obj.date);
					},
					failure : function(){
						Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
					}
				});
			}
		}
	});
	
	SET_PAGE_CONTENT(transRIPanel);
	
	function bersihRitransaksi() {
		Ext.getCmp('tf.noreg').setValue();
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue();
		Ext.getCmp('tf.upel').setValue();
		Ext.getCmp('tf.dokter').setValue();
		Ext.getCmp('tf.nona').setValue();
		Ext.getCmp('tf.nonota').setValue();
		Ext.getCmp('df.tgl').setValue(new Date());
		Ext.getCmp('df.tglreg').setValue(new Date());
		Ext.getCmp('tf.an').setValue();
		Ext.getCmp('tf.catatan').setValue();
		Ext.getCmp('tf.catatandskn').setValue();
		Ext.getCmp('tf.penjamin').setValue();
		Ext.getCmp('tahun').setValue();
		Ext.getCmp('jkel').setValue();
		Ext.getCmp('cb.stpelayanan').setValue(1);
		Ext.getCmp('btn.simpan').disable();
		Ext.getCmp('btn.cetaknota').disable();
		Ext.getCmp('btn.excelnota').disable();
		Ext.getCmp('btn.cetakkui').disable();
		Ext.getCmp('btn_add').disable();
		Ext.getCmp('tf.jumlah').setValue();
		Ext.getCmp('tf.utyd').setValue();
		Ext.getCmp('tf.kembalian').setValue();
		Ext.getCmp('tf.total').setValue(0);
		Ext.getCmp('tf.uangr').setValue(0);
		Ext.getCmp('tf.diskonf').setValue(0);
		Ext.getCmp('tf.totalrf').setValue(0);
		Ext.getCmp('tf.total2').setValue(0);
		Ext.getCmp('tf.tghnudg').getValue(0);
		Ext.getCmp('tf.deposit').getValue(0);
		Ext.getCmp('chb.plafond').disable();
		Ext.getCmp('chb.plafond').setValue(0);
		pmbyrn = 0;
		idregdetugd = 0;
		cekpenj = 0;
		cekpembayaran = 0;
		ds_notatransri.setBaseParam('idregdet',null);
		ds_notatransri.setBaseParam('idbagian',999);
		ds_notatransri.reload();
		ds_notatransri2.setBaseParam('idregdet',null);
		ds_notatransri2.setBaseParam('idbagian',999);
		ds_notatransri2.reload();
		ds_kuitansidet.setBaseParam('blank', 1);
		ds_kuitansidet.reload();
		fTotal();
		fTotal2();
		myVar = setInterval(function(){myTimer()},1000);
		myStopFunction();
	}
	
	function fTotal(){
		ds_notatransri.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0;
				dijaminrp = 0;
				ds_notatransri.each(function (rec) {
					sum += parseFloat(rec.get('selisihf'));
					dijaminrp += parseFloat(rec.get('dijaminrp'));
					var djmn = (rec.get('dijamin')==1)?true:false;
					rec.set('dijamin', djmn);
				});
				Ext.getCmp("tf.total").setValue(sum);
				Ext.getCmp("tf.totaldijamin").setValue(dijaminrp);
				tottindakan = sum;
				fTotal2();
			}
		});
	}
	
	function fTotal2(){
		ds_notatransri2.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0;
				dijaminrp = 0;
				ds_notatransri2.each(function (rec) {
					sum += parseFloat(rec.get('selisihf'));
					dijaminrp += parseFloat(rec.get('dijaminrp'));
					var djmn = (rec.get('dijamin')==1)?true:false;
					rec.set('dijaminf', djmn);
				});
				var jmluangr = Ext.getCmp("tf.uangr").getValue();
				var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
				var sumtotal = sum + jmluangr;
				var sumtotaldisk = sumtotal - jmldiskon;
				Ext.getCmp("tf.dijamintotalrf").setValue(dijaminrp);
				Ext.getCmp("tf.totalrf").setValue(sum);
				Ext.getCmp("tf.total2").setValue(sumtotaldisk);
				totfarmasi = sumtotaldisk;
				fTotal3();
				if(Ext.getCmp('tf.diskonf').getValue()>0){
					var disc = Ext.getCmp('tf.diskonf').getValue() / Ext.getCmp('tf.totalrf').getValue() * 100;
					Ext.getCmp('tf.diskonprsn').setValue(disc);
				} else {
					Ext.getCmp('tf.diskonprsn').setValue(0);
				}
			}
		});
	}
	
	function fTotal3(){
		ds_kuitansidet.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0;
				sumdjmn = 0;
				cekpenj = 0;
				ds_kuitansidet.each(function (rec) {
					if(rec.get('idcarabayar') == '8'){
						cekpenj = 1;
						sumdjmn += (parseFloat(rec.get('jumlah')));
					} else {
						sum += (parseFloat(rec.get('jumlah')));
					}
				});
				Ext.getCmp('tf.jumlah').setValue(sum);
				pmbyrn += sum;
				pmbyrndjmn += sumdjmn;
				cekTombol();
			}
		});
	}
	
	function simpanRIT(){
		if(Ext.getCmp('tf.jumlah').getValue() != (Ext.getCmp('tf.tottagihan').getValue() + Ext.getCmp('tf.ttldijamin').getValue())){
			Ext.MessageBox.alert('Informasi', 'Total Bayar tidak sama dengan Jumlah Total');
		} else {
			var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
			var arrnota = [];
			var arrcarabayar = [];
			var cblength = ds_kuitansidet.data.items.length;
			var zx = 0;
			
			ds_notatransri.each(function(rec){
				zkditem = rec.get('kditem');
				zdijaminrp = rec.get('dijaminrp');
				arrnota[zx] = zkditem+'-'+zdijaminrp;
				zx++;
			});
			
			ds_notatransri2.each(function(rec){
				zkditem = rec.get('kditem');
				zdijaminrpf = rec.get('dijaminrpf');
				arrnota[zx] = zkditem+'-'+zdijaminrpf;
				zx++;
			});
			
			for (var zv = 0; zv <cblength; zv++) {
				var record = ds_kuitansidet.data.items[zv].data;
				znmcarabayar = record.nmcarabayar;
				znmbank = record.nmbank;
				znokartu = record.nokartu;
				zjumlah = record.jumlah;
				arrcarabayar[zv] = znmcarabayar+'-'+znmbank+'-'+znokartu+'-'+zjumlah;
			}
			
			Ext.Ajax.request({
				url: BASE_URL + 'nota_controller/insorupd_notari',
				params: {
					noreg		: Ext.getCmp('tf.noreg').getValue(),
					nmshift 	: Ext.getCmp('tf.shift').getValue(),
					catatan 	: Ext.getCmp('tf.catatan').getValue(),
					catatandskn	: Ext.getCmp('tf.catatandskn').getValue(),
					diskonf	 	: Ext.getCmp('tf.diskonf').getValue(),
					uangr	 	: Ext.getCmp('tf.uangr').getValue(),
					total	 	: Ext.getCmp('tf.jumlah').getValue(),
					pembayaran 	: Ext.getCmp('tf.totaljumlah').getValue(),
					atasnama 	: Ext.getCmp('tf.an').getValue(),
					stpelayanan	: Ext.getCmp('cb.stpelayanan').lastSelectionText,
					tahun		: Ext.getCmp('tahun').getValue(),
					jkel		: Ext.getCmp('jkel').getValue(),
					ttlplafond 	: Ext.getCmp('tf.ttldijamin').getValue(),
					plafond		: Ext.getCmp('chb.plafond').getValue(),
					tgltrans	: Ext.getCmp('df.tgl').getValue(),
					jamtrans	: Ext.getCmp('tf.jamri').getValue(),
					stposisipasien: 6,
					idjnskuitansi: 2,
					cekpembayaran: cekpembayaran,
					arrnota		: Ext.encode(arrnota),
					arrcarabayar: Ext.encode(arrcarabayar)
				},
				success: function(response){
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
					obj = Ext.util.JSON.decode(response.responseText);
					Ext.getCmp("tf.nona").setValue(obj.nona);
					Ext.getCmp("btn.simpan").disable();
					Ext.getCmp("btn.cetaknota").enable();
					Ext.getCmp("btn.excelnota").enable();
					Ext.getCmp("btn.cetakkui").enable();
					ds_vregistrasi.reload();
					ds_vregistrasi2.reload();
					myStopFunction();
				},
				failure: function (form, action) {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			});
		}
	}
	
	function hapusItem(){
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/delete_bnotadet',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue(),
				nonota		: Ext.getCmp('tf.nonota').getValue(),
				arr			:  Ext.encode(arr)
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
				bersihRitransaksi();
			},
			failure: function() {
			}
		});
	}
	
	function cetakRINota(){
		var nona			= Ext.getCmp('tf.nona').getValue();
		var noreg			= Ext.getCmp('tf.noreg').getValue();
		
		RH.ShowReport(BASE_URL + 'print/printnota/notari/'
                +nona+'/'+noreg);
	}
	
	function cetakRIExcel(){
		var nona			= Ext.getCmp('tf.nona').getValue();
		var noreg			= Ext.getCmp('tf.noreg').getValue();
		
		RH.ShowReport(BASE_URL + 'print/printnota/excelnotari/'
                +nona+'/'+noreg);
	}
	
	function cetakRINotaS(){
		var noreg		= Ext.getCmp('tf.noreg').getValue();
		var total		= Ext.getCmp('tf.total').getValue() + Ext.getCmp('tf.total2').getValue();
		if(noreg){
			if(total>0)RH.ShowReport(BASE_URL + 'print/printnota/notaris/' +noreg);
			else Ext.MessageBox.alert('Informasi', 'Silahkan isi tindakan atau farmasi RI');
		} else {
			Ext.MessageBox.alert('Informasi', 'Silahkan pilih pasien');
		}
		
	}
	
	function cetakRIKui(){
		var nonota			= Ext.getCmp('tf.nonota').getValue();
		var noreg			= Ext.getCmp('tf.noreg').getValue();
		
		var arrnota = [];
		var notlength = ds_notatransri.data.items.length;
		var notlength2 = ds_notatransri2.data.items.length;
		var zx = 0;
		
		for (zx; zx < notlength; zx++) {
			var record = ds_notatransri.data.items[zx].data;
			zjkartustok 		= 3;
			ztglkartustok 		= Ext.getCmp('df.tgl').getValue().format('Y/m/d');
			zjamkartustok 		= Ext.getCmp('tf.jamri').getValue();
			zjmlkeluar	 		= record.qty;
			znama		 		= Ext.getCmp('tf.upel').getValue();
			zkditem				= record.kditem;
			arrnota[zx] = zjkartustok+'-'+ztglkartustok+'-'+zjamkartustok+'-'+zjmlkeluar+'-'+znama+'-'+zkditem;
		}
		
		for (var zy = 0; zy < notlength2; zy++) {
			zx++;
			var record = ds_notatransri2.data.items[zy].data;
			zjkartustok 		= 3;
			ztglkartustok 		= Ext.getCmp('df.tgl').getValue().format('Y/m/d');
			zjamkartustok 		= Ext.getCmp('tf.jamri').getValue();
			zjmlkeluar	 		= record.qty;
			znama		 		= 'INSTALASI FARMASI';
			zkditem				= record.kditem;
			arrnota[zx] = zjkartustok+'-'+ztglkartustok+'-'+zjamkartustok+'-'+zjmlkeluar+'-'+znama+'-'+zkditem;
		}
		
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/insert_kartustok',
			params: {
				noreg : Ext.getCmp('tf.noreg').getValue(),
				noref : Ext.getCmp('tf.nonota').getValue(),
				ri    : 1,
				arrnota : Ext.encode(arrnota)
			}
		});
		
		Ext.getCmp("btn.simpan").disable();
		RH.ShowReport(BASE_URL + 'print/printnota/kuitansi/'
                +nonota+'/'+noreg);
	}
	
	function cetakKuiPenj(){
		var nonota			= Ext.getCmp('tf.nonota').getValue();
		var noreg			= Ext.getCmp('tf.noreg').getValue();
		
		var arrnota = [];
		var notlength = ds_notatransri.data.items.length;
		var notlength2 = ds_notatransri2.data.items.length;
		var zx = 0;
		
		for (zx; zx < notlength; zx++) {
			var record = ds_notatransri.data.items[zx].data;
			zjkartustok 		= 3;
			ztglkartustok 		= Ext.getCmp('df.tgl').getValue().format('Y/m/d');
			zjamkartustok 		= Ext.getCmp('tf.jamri').getValue();
			zjmlkeluar	 		= record.qty;
			znama		 		= Ext.getCmp('tf.upel').getValue();
			zkditem				= record.kditem;
			arrnota[zx] = zjkartustok+'-'+ztglkartustok+'-'+zjamkartustok+'-'+zjmlkeluar+'-'+znama+'-'+zkditem;
		}
		
		for (var zy = 0; zy < notlength2; zy++) {
			zx++;
			var record = ds_notatransri2.data.items[zy].data;
			zjkartustok 		= 3;
			ztglkartustok 		= Ext.getCmp('df.tgl').getValue().format('Y/m/d');
			zjamkartustok 		= Ext.getCmp('tf.jamri').getValue();
			zjmlkeluar	 		= record.qty;
			znama		 		= 'INSTALASI FARMASI';
			zkditem				= record.kditem;
			arrnota[zx] = zjkartustok+'-'+ztglkartustok+'-'+zjamkartustok+'-'+zjmlkeluar+'-'+znama+'-'+zkditem;
		}
		
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/insert_kartustok',
			params: {
				noreg : Ext.getCmp('tf.noreg').getValue(),
				noref : Ext.getCmp('tf.nonota').getValue(),
				ri    : 1,
				arrnota : Ext.encode(arrnota)
			}
		});
		
		Ext.getCmp("btn.simpan").disable();
		RH.ShowReport(BASE_URL + 'print/printnota/kuitansipenj/'
                +nonota+'/'+noreg);
	}
	
	function totalnota(){
		var zzz = 0;
		var dijaminnota = 0;
		for (var zxc = 0; zxc <ds_notatransri.data.items.length; zxc++) {
			var record = ds_notatransri.data.items[zxc].data;
			zzz += parseFloat(record.selisihf);
			dijaminnota += parseFloat(record.dijaminrp);
		}
		Ext.getCmp('tf.total').setValue(zzz);
		tottindakan = zzz;
		Ext.getCmp('tf.totaldijamin').setValue(dijaminnota);
	}
	
	function totalnota2(){
		var zzz = 0;
		var dijaminnotaf = 0;
		for (var zxc = 0; zxc <ds_notatransri2.data.items.length; zxc++) {
			var record = ds_notatransri2.data.items[zxc].data;
			zzz += parseFloat(record.selisihf);
			dijaminnotaf += parseFloat(record.dijaminrpf);
		}
		var jmluangr = Ext.getCmp("tf.uangr").getValue();
		var jmldiskon = Ext.getCmp("tf.diskonf").getValue();
		var sumtotal = zzz + jmluangr;
		var sumtotaldisk = sumtotal - jmldiskon;
		Ext.getCmp('tf.dijamintotalrf').setValue(dijaminnotaf);
		Ext.getCmp('tf.totalrf').setValue(zzz);
		Ext.getCmp('tf.total2').setValue(sumtotaldisk);
		totfarmasi = sumtotaldisk;
	}
	
	function totalnota3(){
		var zzz = 0;
		for (var zxc = 0; zxc <ds_kuitansidet.data.items.length; zxc++) {
			var record = ds_kuitansidet.data.items[zxc].data;
			zzz += parseFloat(record.jumlah);
		}
		Ext.getCmp('tf.jumlah').setValue(zzz);
		cekTombol();
	}
	
	function fPemb(){
		var ds_carabayar = dm_carabayar();
		var ds_bankcb = dm_bank();
		var pmbyrntemp = 0;
		var pmbyrndjmntemp = 0;
		var total = Ext.getCmp('tf.total').getValue() + Ext.getCmp('tf.total2').getValue() + Ext.getCmp('tf.tghnudg').getValue() - Ext.getCmp('tf.deposit').getValue();
		var ttldijamin = Ext.getCmp('tf.totaldijamin').getValue() + Ext.getCmp('tf.dijamintotalrf').getValue();
		pmbyrntemp = total - pmbyrn;
		if(pmbyrntemp < 0) pmbyrntemp = 0;
		pmbyrndjmntemp = ttldijamin - pmbyrndjmn;
		if(pmbyrndjmntemp < 0) pmbyrndjmntemp = 0;
		var pemb_form = new Ext.form.FormPanel({
			xtype:'form',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px',
			monitorValid: true,
			height: 200, width: 300,
			layout: 'form', 
			frame: false,
			items: [     
			{
				xtype: 'combo', fieldLabel: 'Cara Bayar',
				id: 'cb.pembcb', width: 150, 
				store: ds_carabayar, valueField: 'idcarabayar', displayField: 'nmcarabayar',
				editable: false, triggerAction: 'all', value:'Tunai',
				forceSelection: true, submitValue: true, mode: 'local',
				listeners:{
					select:function(combo, records, eOpts){
						if(records.get('idcarabayar') == 8)
						{
							Ext.getCmp('tf.pembnominal').setValue(pmbyrndjmntemp);
						} else if(records.get('idcarabayar') != 1){
							Ext.getCmp('cb.pembbank').enable();
							Ext.getCmp('tf.pembkartu').enable();
							Ext.getCmp('tf.pembnominal').setValue(pmbyrntemp);
						} else {
							Ext.getCmp('cb.pembbank').disable();
							Ext.getCmp('tf.pembkartu').disable();
							Ext.getCmp('cb.pembbank').setValue('');
							Ext.getCmp('tf.pembkartu').setValue('');
							Ext.getCmp('tf.pembnominal').setValue(pmbyrntemp);
						}
					}
				}
			},    
			{
				xtype: 'combo', fieldLabel: 'Bank',
				id: 'cb.pembbank', width: 150, 
				store: ds_bankcb, valueField: 'idbank', displayField: 'nmbank',
				editable: false, triggerAction: 'all', disabled:true,
				forceSelection: true, submitValue: true, mode: 'local',
			},{
				xtype: 'textfield', fieldLabel:'No. Kartu', disabled:true,
				id: 'tf.pembkartu', width: 150
			},{
				xtype: 'numericfield', fieldLabel:'Nominal',
				id: 'tf.pembnominal', width: 150, height: 50,
				value: pmbyrntemp,
				style: {
					'fontSize'     : '20px'
				}
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					PembAdd();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wPemb.close();
				}
			}]
		});
			
		var wPemb = new Ext.Window({
			title: 'Pembayaran',
			modal: true, closable:false,
			items: [pemb_form]
		});
		
		wPemb.show();
		
		function PembAdd(){
			if(Ext.getCmp('cb.pembcb').value == 8){
				if(Ext.getCmp('tf.pembnominal').getValue()>pmbyrndjmntemp)
					Ext.getCmp('tf.pembnominal').setValue(pmbyrndjmntemp);
				pmbyrndjmn += Ext.getCmp('tf.pembnominal').getValue();
			} else {
				if(Ext.getCmp('tf.pembnominal').getValue()>pmbyrntemp)
					Ext.getCmp('tf.pembnominal').setValue(pmbyrntemp);
				pmbyrn += Ext.getCmp('tf.pembnominal').getValue();
			}		
			var orgaListRecord = new Ext.data.Record.create([
				{
					name: 'nmcarabayar',
					name: 'nmbank',
					name: 'nokartu',
					name: 'jumlah'
				}
			]);
			
			ds_kuitansidet.add([
				new orgaListRecord({
					'nmcarabayar': Ext.getCmp('cb.pembcb').lastSelectionText,
					'nmbank': Ext.getCmp('cb.pembbank').lastSelectionText,
					'nokartu': Ext.getCmp('tf.pembkartu').getValue(),
					'jumlah': Ext.getCmp('tf.pembnominal').getValue()
				})
			]);
			if(pmbyrn < 0) pmbyrn = 0;
			if(pmbyrndjmn < 0) pmbyrndjmn = 0;
			Ext.getCmp('tf.jumlah').setValue(pmbyrn+pmbyrndjmn);
			Ext.getCmp("btn.simpan").enable();
			wPemb.close();
		}
	}

	function hitungKembalian() {
		var jml = Ext.getCmp('tf.jumlah').getValue();
		if(jml != ''){
			var utyd = Ext.getCmp('tf.utyd').getValue();
			var kmblian = utyd - jml;
			Ext.getCmp('tf.kembalian').setValue(kmblian);
		}
	}

	function umur(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tahun').setValue(year);
	}
	
	function cekTombol(){
		var totaljumlah = Ext.getCmp('tf.total').getValue() + Ext.getCmp('tf.total2').getValue() + Ext.getCmp('tf.tghnudg').getValue();
		var total = totaljumlah - Ext.getCmp('tf.deposit').getValue();
		if(total<0) total = 0;
		var ttldijamin = Ext.getCmp('tf.totaldijamin').getValue() + Ext.getCmp('tf.dijamintotalrf').getValue();
		
		Ext.getCmp('tf.totaljumlah').setValue(totaljumlah);
		Ext.getCmp('tf.tottagihan').setValue(total);
		Ext.getCmp('tf.ttldijamin').setValue(ttldijamin);
		if(Ext.getCmp('tf.nona').getValue() != ''){
			if((pmbyrn+pmbyrndjmn) >= (total+ttldijamin)){
				Ext.getCmp("btn.cetaknota").enable();
				Ext.getCmp("btn.excelnota").enable();
				Ext.getCmp("btn.cetakkui").enable();
				Ext.getCmp("btn.simpan").enable();
				if(cekpenj == 1)Ext.getCmp("btn.cetakkuipenj").enable();
				else Ext.getCmp("btn.cetakkuipenj").disable();
			} else {
				Ext.getCmp("btn.cetaknota").disable();
				Ext.getCmp("btn.excelnota").disable();
				Ext.getCmp("btn.cetakkui").disable();
				Ext.getCmp("btn.simpan").disable();
			}
		} else {
			if((pmbyrn+pmbyrndjmn) >= (total+ttldijamin)){
				if(Ext.getCmp('tf.nona').getValue() != ''){
					Ext.getCmp("btn.cetaknota").enable();
					Ext.getCmp("btn.excelnota").enable();
					Ext.getCmp("btn.cetakkui").enable();
				} else {
					Ext.getCmp("btn.cetaknota").disable();
					Ext.getCmp("btn.excelnota").disable();
					Ext.getCmp("btn.cetakkui").disable();
				}
				if(cekkstok == 0 && Ext.getCmp("tf.nona").getValue() == '')Ext.getCmp("btn.simpan").enable();
				else Ext.getCmp("btn.simpan").disable();
				if(cekpenj == 1)Ext.getCmp("btn.cetakkuipenj").enable();
				else Ext.getCmp("btn.cetakkuipenj").disable();
			} else {
				Ext.getCmp("btn.cetaknota").disable();
				Ext.getCmp("btn.excelnota").disable();
				Ext.getCmp("btn.cetakkui").disable();
				Ext.getCmp("btn.simpan").disable();
			}
		}
	}
	
	function fDiskon(rowIndex){
		var rec_nota = ds_notatransri.getAt(rowIndex);
		var jumlahjs =0;
		var jumlahjm =0;
		var jumlahjp =0;
		var jumlahbhp =0;
		var tariftot =0;
		var dprsntot =0;
		var drptot =0;
		var jumlahtot =0;
		var dprsnjs = 0;
		var dprsnjm = 0;
		var dprsnjp = 0;
		var dprsnbhp = 0;
		
		var disk_form = new Ext.form.FormPanel({
			xtype:'form',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px',
			monitorValid: true,
			height: 270, width: 400,
			layout: 'form', 
			frame: false,
			items: [{
				xtype: 'textfield', fieldLabel:'Nama',
				id: 'tf.nmitemd', width: 200,
				value: rec_nota.data.nmitem,
				disabled:true
			},{
				xtype: 'compositefield',
				fieldLabel:'Keterangan',
				items: [{
					xtype: 'numericfield',
					id: 'nm.tes',
					hidden:true
				},{
					xtype: 'label', id: 'lb.tarif', text: 'Tarif', margins: '0 10 0 20'
				},{
					xtype: 'label', id: 'lb.dprsn', text: '%', margins: '0 10 0 30'
				},{
					xtype: 'label', id: 'lb.drp', text: 'Rupiah', margins: '0 10 0 20'
				},{
					xtype: 'label', id: 'lb.jumlah', text: 'Jumlah', margins: '0 10 0 30'
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JS',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjs', width: 70,
					value: rec_nota.data.tarifjs * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjs', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjs', width: 70,
					value: rec_nota.data.diskonjs,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjs', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JM',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjm', width: 70,
					value: rec_nota.data.tarifjm * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjm', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjm', width: 70,
					value: rec_nota.data.diskonjm,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjm', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JP',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjp', width: 70,
					value: rec_nota.data.tarifjp * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjp', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjp', width: 70,
					value: rec_nota.data.diskonjp,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjp', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon BHP',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifbhp', width: 70,
					value: rec_nota.data.tarifbhp * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnbhp', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpbhp', width: 70,
					value: rec_nota.data.diskonbhp,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahbhp', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Total',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tariftot', width: 70,
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsntot', width: 40,
					disabled:true
				},{
					xtype: 'numericfield',
					id: 'tf.drptot', width: 70,
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahtot', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			}],
			buttons: [{
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wDisk.close();
				}
			}]
		});
			
		var wDisk = new Ext.Window({
			title: 'Diskon',
			modal: true, closable:false,
			items: [disk_form]
		});
		
		diskonAwal();
		wDisk.show();
		
		function diskonAwal(){
			if(rec_nota.data.tarifjs == '')rec_nota.data.tarifjs = 0;
			if(rec_nota.data.tarifjm == '')rec_nota.data.tarifjm = 0;
			if(rec_nota.data.tarifjp == '')rec_nota.data.tarifjp = 0;
			if(rec_nota.data.tarifbhp == '')rec_nota.data.tarifbhp = 0;
			
			if(rec_nota.data.diskonjs == undefined)rec_nota.data.diskonjs = 0;
			if(rec_nota.data.diskonjm == undefined)rec_nota.data.diskonjm = 0;
			if(rec_nota.data.diskonjp == undefined)rec_nota.data.diskonjp = 0;
			if(rec_nota.data.diskonbhp == undefined)rec_nota.data.diskonbhp = 0;
			
			jumlahjs = (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjs);
			jumlahjm = (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjm);
			jumlahjp = (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjp);
			jumlahbhp = (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonbhp);
			
			if(rec_nota.data.tarifjs != 0)dprsnjs = parseInt(rec_nota.data.diskonjs) / (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) * 100;
			if(rec_nota.data.tarifjm != 0)dprsnjm = parseInt(rec_nota.data.diskonjm) / (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) * 100;
			if(rec_nota.data.tarifjp != 0)dprsnjp = parseInt(rec_nota.data.diskonjp) / (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) * 100;
			if(rec_nota.data.tarifbhp != 0)dprsnbhp = parseInt(rec_nota.data.diskonbhp) / (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty)) * 100;
			
			tariftot = (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty));
			dprsntot = dprsnjs + dprsnjm + dprsnjp + dprsnbhp;
			drptot = parseInt(rec_nota.data.diskonjs) + parseInt(rec_nota.data.diskonjm) + parseInt(rec_nota.data.diskonjp) + parseInt(rec_nota.data.diskonbhp);
			jumlahtot = jumlahjs + jumlahjm + jumlahjp + jumlahbhp;
			
			Ext.getCmp('tf.dprsnjs').setValue(dprsnjs);
			Ext.getCmp('tf.dprsnjm').setValue(dprsnjm);
			Ext.getCmp('tf.dprsnjp').setValue(dprsnjp);
			Ext.getCmp('tf.dprsnbhp').setValue(dprsnbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jumlahjs);
			Ext.getCmp('tf.jumlahjm').setValue(jumlahjm);
			Ext.getCmp('tf.jumlahjp').setValue(jumlahjp);
			Ext.getCmp('tf.jumlahbhp').setValue(jumlahbhp);
			
			Ext.getCmp('tf.tariftot').setValue(tariftot);
			Ext.getCmp('tf.dprsntot').setValue(dprsntot);
			Ext.getCmp('tf.drptot').setValue(drptot);
			Ext.getCmp('tf.jumlahtot').setValue(jumlahtot);
		}
	}
	
	function cekKartuStok(){
		if(Ext.getCmp("tf.nonota").getValue() != ''){
			Ext.Ajax.request({
				url:BASE_URL + 'kartustok_controller/getCekRJ',
				method:'POST',
				params:{
					nonota : Ext.getCmp("tf.nonota").getValue()
				},
				success: function(response){
					obj = Ext.util.JSON.decode(response.responseText)
					cekkstok = obj.cek;
				}
			});
		}
	}
	
	function cekTransfer(){
		Ext.Ajax.request({
			url:BASE_URL + 'nota_controller/getCekTransfer',
			method:'POST',
			params:{
				noreg : Ext.getCmp("tf.noreg").getValue()
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp('tf.tghnudg').setValue(obj.total);
				if(obj.total>0)Ext.getCmp('btn.ugddet').enable();
				else Ext.getCmp('btn.ugddet').disable();
				idregdetugd = obj.idregdet;
			}
		});
	}

	function fNotaUGD(){
		var ds_notaugd = dm_nota();
		ds_notaugd.setBaseParam('start',0);
		ds_notaugd.setBaseParam('limit',50);
		ds_notaugd.setBaseParam('idregdet',idregdetugd);
		ds_notaugd.setBaseParam('koder',0);
		
		var ds_notaugd2 = dm_nota();
		ds_notaugd2.setBaseParam('start',0);
		ds_notaugd2.setBaseParam('limit',50);
		ds_notaugd2.setBaseParam('idregdet',idregdetugd);
		ds_notaugd2.setBaseParam('koder',1);
		
		var grid_nota2 = new Ext.grid.EditorGridPanel({
			store: ds_notaugd,
			frame: true,
			height: 250,
			bodyStyle: 'padding:3px 3px 3px 3px',
			id: 'grid_notaugd',
			forceFit: true,
			autoScroll: true,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			columns: [{
				header: '<div style="text-align:center;">Item Tindakan</div>',
				dataIndex: 'nmitem',
				width: 250
			},{
				header: '<div style="text-align:center;">Tarif</div>',
				dataIndex: 'tarif',
				align:'right',
				width: 90,
				xtype: 'numbercolumn', format:'0,000'
			},{
				header: '<div style="text-align:center;">Qty</div>',
				dataIndex: 'qty',
				align:'right',
				width: 45,
			},{
				header: '<div style="text-align:center;">Satuan</div>',
				dataIndex: 'nmsatuan',
				width: 80
			},{
				header: '<div style="text-align:center;">Total<br>Diskon</div>',
				dataIndex: 'drp',
				align:'right',
				width: 55,
				xtype: 'numbercolumn', format:'0,000'
			},{
				header: '<div style="text-align:center;">Subtotal</div>',
				dataIndex: 'tarif2',
				align:'right',
				width: 100,
				xtype: 'numbercolumn', format:'0,000'
			}],
			bbar: [
				{ xtype:'tbfill' },'Jumlah : ',
				{
					xtype: 'numericfield',
					id: 'tf.totalugd',
					value: 0,
					readOnly:true,
					style : 'opacity:0.6',
					width: 100,
					thousandSeparator:','
				}
			]
		});
		
		var grid_notafarmasi2 = new Ext.grid.EditorGridPanel({
			store: ds_notaugd2,
			frame: true,
			height: 250,
			bodyStyle: 'padding:3px 3px 3px 3px',
			id: 'grid_notaugd2',
			forceFit: true,
			autoScroll: true,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			columns: [{
				header: '<div style="text-align:center;">Item Obat/Alkes</div>',
				dataIndex: 'nmitem',
				width: 250
			},{
				header: '<div style="text-align:center;">Tarif</div>',
				dataIndex: 'tarif',
				align:'right',
				width: 90,
				xtype: 'numbercolumn', format:'0,000'
			},{
				header: '<div style="text-align:center;">Qty</div>',
				dataIndex: 'qty',
				align:'right',
				width: 45,
			},{
				header: '<div style="text-align:center;">Satuan</div>',
				dataIndex: 'nmsatuan',
				width: 85
			},{
				header: '<div style="text-align:center;">Subtotal</div>',
				dataIndex: 'tarif2',
				align:'right',
				width: 100,
				xtype: 'numbercolumn', format:'0,000'
			}],
			bbar: [
				{ xtype:'tbfill' },
				{
					xtype: 'fieldset',
					border: false,
					style: 'padding:0px; margin: 0px',
					width: 400,
					items: [{
						xtype: 'compositefield',
						items: [{
							xtype: 'label', id: 'lb.totalrf', text: 'Total :', margins: '0 10 0 138',
						},{
							xtype: 'numericfield',
							id: 'tf.totalrfugd',
							value: 0,
							width: 100,
							readOnly:true,
							style : 'opacity:0.6',
							thousandSeparator:',',
						}]
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'label', id: 'lb.diskonprsn2', text: 'Diskon % :', margins: '0 10 0 2',
						},{
							xtype: 'numericfield',
							id: 'tf.diskonfugdprsn',
							value: 0,
							readOnly:true,
							style : 'opacity:0.6',
							width: 50,
							thousandSeparator:','
						},{
							xtype: 'label', id: 'lb.uangr', text: 'Racik :', margins: '0 10 0 15',
						},{
							xtype: 'numericfield',
							id: 'tf.uangrugd',
							value: 0,
							readOnly:true,
							style : 'opacity:0.6',
							width: 100,
							thousandSeparator:',',
						}]
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'label', id: 'lb.diskon2', text: 'Diskon Rp :', margins: '0 9 0 0',
						},{
							xtype: 'numericfield',
							id: 'tf.diskonfugd',
							value: 0,
							readOnly:true,
							style : 'opacity:0.6',
							width: 50,
							thousandSeparator:',',
						},{
							xtype: 'label', id: 'lb.total2', text: 'Jumlah :', margins: '0 9 0 10',
						},{
							xtype: 'numericfield',
							id: 'tf.total2ugd',
							value: 0,
							readOnly:true,
							style : 'opacity:0.6',
							width: 100,
							thousandSeparator:','
						}]
					}]
				}
			]
		});
	
		var nugd_form = new Ext.form.FormPanel({
			id: 'fp.nugd',
			name: 'fp.nugd',
			border: false, 
			forceFit:true,
			frame: true,
			autoScroll:true,
			labelAlign: 'top',
			layout: 'anchor',
			width: 680,
			items: [
			{
				layout: 'fit',
				bodyStyle: 'padding:5px 5px 5px 5px',
				items: [grid_nota2]
			},{
				layout: 'fit',
				bodyStyle: 'padding:5px 5px 5px 5px',
				items: [grid_notafarmasi2]
			}],
			buttons: [{
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wNugd.close();
				}
			}]
		});
			
		var wNugd = new Ext.Window({
			title: 'Transaksi UGD',
			modal: true, closable:false,
			items: [nugd_form]
		});
		
		wNugd.show();
		fTotalugd();
		function fTotalugd(){
			ds_notaugd.reload({
				scope   : this,
				callback: function(records, operation, success) {
					sum = 0; 
					ds_notaugd.each(function (rec) { sum += parseFloat(rec.get('tarif2')); });
					Ext.getCmp("tf.totalugd").setValue(sum);
				}
			});
			ds_notaugd2.reload({
				scope   : this,
				callback: function(records, operation, success) {
					sum = 0; 
					ds_notaugd2.each(function (rec) { sum += parseFloat(rec.get('tarif2')); });
					Ext.getCmp("tf.uangrugd").setValue(records[0].data.uangr);
					Ext.getCmp("tf.diskonfugd").setValue(records[0].data.diskonr);
					var jmluangr = parseFloat(records[0].data.uangr);
					var jmldiskon = parseFloat(records[0].data.diskonr);
					var sumtotal = sum + jmluangr;
					var sumtotaldisk = sumtotal - jmldiskon;
					Ext.getCmp("tf.totalrfugd").setValue(sum);
					Ext.getCmp("tf.total2ugd").setValue(sumtotaldisk);
					var disc = Ext.getCmp("tf.diskonfugd").getValue() / sum * 100;
					Ext.getCmp("tf.diskonfugdprsn").setValue(disc);
				}
			});
		}
	}
	
	function cAdvance(){
		ds_vregistrasi2.setBaseParam('tglawal',Ext.getCmp('df.tglawal').getValue().format('Y-m-d'));
		ds_vregistrasi2.setBaseParam('tglakhir',Ext.getCmp('df.tglakhir').getValue().format('Y-m-d'));
		ds_vregistrasi2.setBaseParam('ctransfar',1);
		ds_vregistrasi2.load();
	}
	
}