function RItindakan(){
	var myVar= setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jamrit"))
				RH.setCompValue("tf.jamrit",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("tf.shift").setValue(obj.nmshift);
		}
	});
	
	var ds_nota = dm_nota();
	ds_nota.setBaseParam('start',0);
	ds_nota.setBaseParam('limit',500);
	ds_nota.setBaseParam('idbagian',999);
	ds_nota.setBaseParam('khri',true);
	
	var ds_notax = dm_notax();
	ds_notax.setBaseParam('noreg', -1);	
	
	var ds_counnonota = dm_counnonotat();
		
	var ds_vregistrasi = dm_vregistrasi();
	ds_vregistrasi.setBaseParam('start',0);
	ds_vregistrasi.setBaseParam('limit',6);
	ds_vregistrasi.setBaseParam('cek','RI');
	ds_vregistrasi.setBaseParam('riposisipasien',5);
	ds_vregistrasi.setBaseParam('groupby','noreg');
	ds_vregistrasi.setBaseParam('oposisipasien',1);
	ds_vregistrasi.setBaseParam('onmpasien',1);
	ds_vregistrasi.setBaseParam('khususri',1);
	ds_vregistrasi.setBaseParam('gnoreg',1);
	
	var ds_vtarifall = dm_vtarifall();
	ds_vtarifall.setBaseParam('start',0);
	ds_vtarifall.setBaseParam('limit',7);
	ds_vtarifall.setBaseParam('cidbagian',0);
	
	var ds_dokter = dm_dokter_combo();
	var arr = [];
	var zkditem = '';
	var rownota = '';
	var idklstariftp = '';
	
	var grid_nota = new Ext.grid.EditorGridPanel({
		store: ds_nota,
		frame: true,
		height: 310,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar: [{
			text: 'Template Pelayanan',
			id: 'btn_add',
			disabled: true,
			iconCls: 'silk-add',
			handler: function() {
				TemPelayanan();
			}
		},{ xtype:'tbfill' },'Deposit : ',
		{
			xtype: 'numericfield',
			id: 'tf.deposit',
			disabled:true,
			value: 0,
			width: 100,
			thousandSeparator:','
		}],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota = rowIndex;
            }
        },
		columns: [{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					ds_nota.removeAt(rowIndex);
					totalnota();
				}
			}]
		},{
			header: '<div style="text-align:center;">Item Tindakan</div>',
			dataIndex: 'nmitem',
			width: 170,
			sortable: true,
			renderer:jasaMedis
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 110,
			editor: {
				xtype: 'combo',
				id: 'cb.dokter',
				store: ds_dokter, valueField: 'nmdoktergelar', displayField: 'nmdoktergelar',
				editable: true, triggerAction: 'all',
				forceSelection: true, mode: 'local',
			},
			sortable: true,
		},{
			header: '<div style="text-align:center;">Tarif</div>',
			dataIndex: 'tarif',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Qty</div>',
			dataIndex: 'qty',
			align:'right',
			width: 40,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqty',
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota.getAt(rownota);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqty').getValue();
						var subtotal2 = record.data.drp;
						var jsubtotal = subtotal - subtotal2;
						record.set('tarif2',jsubtotal);
						totalnota();
					}
				}
			},
			sortable: true
		},{
			header: '<div style="text-align:center;">Satuan</div>',
			dataIndex: 'nmsatuan',
			width: 80,
			sortable: true
		},{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Diskon',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_edit.png',
				handler: function(grid, rowIndex) {
					fDiskon(rowIndex);
				}
			}]
		},{
			header: '<div style="text-align:center;">Total<br>Diskon</div>',
			dataIndex: 'drp',
			align:'right',
			width: 55,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Subtotal</div>',
			dataIndex: 'tarif2',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			dataIndex: 'kditem',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'koder',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idjnstarif',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idtarifpaketdet',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjs',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjm',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifbhp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjs',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjm',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonbhp',
			hidden: true,
			hideable: false
		}],
		bbar: [
			{ xtype:'tbfill' },'Jumlah : ',
			{
				xtype: 'numericfield',
				id: 'tf.total',
				value: 0,
				readOnly:true,
				style : 'opacity:0.6',
				width: 100,
				thousandSeparator:','
			}
		]
	});
	
	var paging_reg = new Ext.PagingToolbar({
		pageSize: 6,
		store: ds_vregistrasi,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230
	})];
	
	var grid_reg = new Ext.grid.GridPanel({
		title: 'Daftar Registrasi RI',
		store: ds_vregistrasi,
		frame: true,
		height: 270,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_reg',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_freg = ds_vregistrasi.getAt(rowIndex);
				var freg_noreg = rec_freg.data["noreg"];
				var freg_norm = rec_freg.data["norm"];
				var freg_nmpasien = rec_freg.data["nmpasien"];
				var freg_atasnama = rec_freg.data["atasnama"];
				var freg_idbagian = rec_freg.data["idbagian"];
				var freg_nmbagian = rec_freg.data["nmbagian"];
				var freg_nmdokter = rec_freg.data["nmdoktergelar"];
				var freg_nmdokteraja = rec_freg.data["nmdokter"];
				var freg_nonota = rec_freg.data["nonota"];
				var freg_total = rec_freg.data["total"];
				var freg_diskonr = rec_freg.data["diskonr"];
				var freg_uangr = rec_freg.data["uangr"];
				var freg_tglnota = rec_freg.data["tglnota"];
				var freg_jamnota = rec_freg.data["jamnota"];
				var freg_catatannota = rec_freg.data["catatannota"];
				var freg_tgllahirp = rec_freg.data["tgllahirp"];
				var freg_idjnskelamin = rec_freg.data["idjnskelamin"];
				var freg_idregdet = rec_freg.data["idregdet"];
				var freg_nmkamar = rec_freg.data["nmkamar"];
				var freg_nmbed = rec_freg.data["nmbed"];
				var freg_nmklstarif = rec_freg.data["nmklstarif"];
				var freg_deposit = rec_freg.data["deposit"];
				var freg_idklstarif = rec_freg.data["idklstarif"];
				
				Ext.getCmp("tf.noreg").setValue(freg_noreg);
				Ext.getCmp("tf.norm").setValue(freg_norm);
				Ext.getCmp("tf.nmpasien").setValue(freg_nmpasien);
				Ext.getCmp("tf.upel").setValue(freg_nmbagian);
				Ext.getCmp("tf.nmkamar").setValue(freg_nmkamar);
				Ext.getCmp("tf.nmbed").setValue(freg_nmbed);
				Ext.getCmp("tf.dokter").setValue(freg_nmdokter);
				Ext.getCmp("tf.nmklstarif").setValue(freg_nmklstarif);
				Ext.getCmp("tf.idklstarif").setValue(freg_idklstarif);
				Ext.getCmp("tf.nmdokter").setValue(freg_nmdokteraja);
				Ext.getCmp("tf.nonota").setValue();
				Ext.getCmp("idregdet").setValue(freg_idregdet);
				Ext.getCmp("cb.nota").setValue('');
				if(freg_catatannota == null)freg_catatannota ='Tindakan Rawat Inap';
				Ext.getCmp('tf.catatan').setValue(freg_catatannota);
				Ext.getCmp('jkel').setValue(freg_idjnskelamin);
				Ext.getCmp("btn_add").enable();
				Ext.getCmp("btn.cetak").disable();
				
				idklstariftp = freg_idklstarif;
				if(freg_atasnama == null) Ext.getCmp("tf.an").setValue(freg_nmpasien);
				else Ext.getCmp("tf.an").setValue(freg_atasnama);
				if(freg_deposit == null) Ext.getCmp('tf.deposit').setValue(0);
				else Ext.getCmp('tf.deposit').setValue(freg_deposit);
				Ext.getCmp('tf.total').setValue(0);
				//ds_nota.setBaseParam('ds_nota',null);
				ds_nota.setBaseParam('idbagian',999);
				ds_nota.reload();
				ds_notax.setBaseParam('noreg',freg_noreg);
				ds_notax.setBaseParam('jpel',2);
				ds_notax.reload();
				ds_counnonota.setBaseParam('noreg',freg_noreg);
				ds_counnonota.reload({
					scope   : this,
					callback: function(records, operation, success) {
						var tamtot = 0;
						 ds_counnonota.each(function (rec) { 
								tamtot = rec.get('cnonota');
							});
						if(tamtot == null){
							Ext.getCmp("tf.countnonota").setValue('0');
						}else{						
							Ext.getCmp("tf.countnonota").setValue(tamtot);
						}
					}
				});
				ds_vtarifall.setBaseParam('cidbagian',freg_idbagian);
				ds_vtarifall.setBaseParam('cklstarif',freg_idklstarif);
				ds_vtarifall.reload();
				if(freg_tglnota == null) Ext.getCmp('df.tgl').setValue(new Date());
				else Ext.getCmp('df.tgl').setValue(freg_tglnota);
				/* if(freg_jamnota == null){
					myStopFunction();
					myVar = setInterval(function(){myTimer()},1000);
				} else {
					Ext.getCmp('tf.jamrit').setValue(freg_jamnota);
					myStopFunction();
				} */
				myStopFunction();
				umur(new Date(freg_tgllahirp));
            }
        },
		columns: [{
			header: 'No. Reg',
			dataIndex: 'noreg',
			width: 75,
			sortable: true
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 75,
			sortable: true
		},{
			header: 'Pasien',
			dataIndex: 'nmpasien',
			width: 110,
			sortable: true
		},{
			header: 'L/P',
			dataIndex: 'kdjnskelamin',
			width: 30,
			sortable: true
		},{
			header: 'Ruangan',
			dataIndex: 'nmbagian',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Lahir',
			dataIndex: 'tgllahirp',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Posisi Pasien',
			dataIndex: 'nmstposisipasien',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Registrasi',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Dokter',
			dataIndex: 'nmdoktergelar',
			width: 100,
			sortable: true
		},{
			header: 'Penjamin',
			dataIndex: 'nmpenjamin',
			width: 100,
			sortable: true
		}],
		bbar: paging_reg,
		plugins: cari_reg
	});
	
	var paging_tindakan = new Ext.PagingToolbar({
		pageSize: 7,
		store: ds_vtarifall,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_tindakan = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230
	})];
	
	var grid_tindakan = new Ext.grid.GridPanel({
		title: 'Tindakan',
		store: ds_vtarifall,
		frame: true,
		height: 270,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_tindakan',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var cek = true;
				var obj = ds_vtarifall.getAt(rowIndex);
				var skditem			= obj.data["kditem"];
				var snmitem			= obj.data["nmitem"];
				var starifjs		= obj.data["tarifjs"];
				var starifjm		= obj.data["tarifjm"];
				var starifjp		= obj.data["tarifjp"];
				var starifbhp		= obj.data["tarifbhp"];
				var sdiskonjs		= obj.data["diskonjs"];
				var sdiskonjm		= obj.data["diskonjm"];
				var sdiskonjp		= obj.data["diskonjp"];
				var sdiskonbhp		= obj.data["diskonbhp"];
				var starif			= obj.data["ttltarif"];
				var snmsatuan		= obj.data["satuankcl"];
				
				ds_nota.each(function(rec){
					if(rec.get('kditem') == skditem) {
						Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
						cek = false;
					}
				});
				
				if(cek){
					var orgaListRecord = new Ext.data.Record.create([
						{
							name: 'kditem',
							name: 'koder',
							name: 'nmitem',
							name: 'qty',
							name: 'tarif',
							name: 'tarif2',
							name: 'nmsatuan',
							name: 'tarifjs',
							name: 'tarifjm',
							name: 'tarifjp',
							name: 'tarifbhp',
							name: 'diskonjs',
							name: 'diskonjm',
							name: 'diskonjp',
							name: 'diskonbhp',
						}
					]);
					
					ds_nota.add([
						new orgaListRecord({
							'kditem': skditem,
							'koder': null,
							'nmitem': snmitem,
							//'nmdokter':Ext.getCmp('tf.nmdokter').getValue(),
							'nmdokter':'',
							'qty': 1,
							'tarif': starif,
							'tarif2': starif,
							'drp': 0,
							'nmsatuan': snmsatuan,
							'tarifjs': starifjs,
							'tarifjm': starifjm,
							'tarifjp': starifjp,
							'tarifbhp': starifbhp,
							'diskonjs': sdiskonjs,
							'diskonjm': sdiskonjm,
							'diskonjp': sdiskonjp,
							'diskonbhp': sdiskonbhp,
						})
					]);
					var ttl = Ext.getCmp('tf.total').getValue();
					var jml = parseFloat(ttl) + parseFloat(starif);
					Ext.getCmp('tf.total').setValue(jml);
					grid_nota.getView().focusRow(ds_nota.getCount() - 1);
				}
            }
        },
		columns: [{
			header: 'Item Tindakan',
			dataIndex: 'nmitem',
			width: 160,
			sortable: true
		},{
			header: 'Harga',
			dataIndex: 'ttltarif',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		},{
			header: 'Stok',
			dataIndex: 'stoknowbagian',
			align:'right',
			width: 45,
			sortable: true
		}],
		bbar: paging_tindakan,
		plugins: cari_tindakan
	});
	
	var ritindakan_form = new Ext.form.FormPanel({
		id: 'fp.ritindakan',
		title: 'Nota Tindakan RI',
		width: 900,
		Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			{ text: 'Nota Baru', iconCls: 'silk-add', handler: function(){bersihRinota();} },'-',
			{ text: 'Simpan', id:'btn.simpan', iconCls: 'silk-save', handler: function(){
					var vnreg = Ext.getCmp('tf.noreg').getValue();
					if( vnreg == ""){
						Ext.MessageBox.alert('Informasi', 'Pilih Registrasi.');
					}else if(vnreg != ""){
						simpanRIT();
					}} 
			},'-',
			{ text: 'Cetak Nota', id:'btn.cetak', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRIT();} },'-',
			{ xtype: 'tbfill' },'->'
		],
        items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:160,
				boxMaxHeight:160,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. RM',
						id: 'tf.norm',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					},{
						xtype: 'label', id: 'lb.noreg', text: 'No. Registrasi', margins: '0 10 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.noreg',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					}]
				},{
					xtype: 'textfield', fieldLabel:'Nama Pasien',
					id: 'tf.nmpasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield', fieldLabel:'Ruangan',
						id: 'tf.upel',
						readOnly: true, style : 'opacity:0.6',
						width: 80,
					},{
						xtype: 'label', id: 'lb.kamarbed', text: 'Kamar / Bed', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmkamar', 
						readOnly: true, style : 'opacity:0.6',
						width: 35,
					},{
						xtype: 'label', id: 'lb.garing', text: '/', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmbed', 
						readOnly: true, style : 'opacity:0.6',
						width: 38,
					}]
				},{
					xtype: 'textfield', fieldLabel:'Kelas Tarif',
					id: 'tf.nmklstarif',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Kelas Tarif',
					id: 'tf.idklstarif',
					readOnly: true, style : 'opacity:0.6',
					width: 80, hidden: true
				},{
					xtype: 'textfield', fieldLabel:'Dokter',
					id: 'tf.dokter',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:160,
				boxMaxHeight:160,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'textfield', fieldLabel:'No. Nota',
					id: 'tf.nonota',
					readOnly: true, style : 'opacity:0.6',
					width: 100, hidden: true
				},{
					xtype: 'textfield',
					id: 'tf.nmdokter',hidden: true
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'combo', fieldLabel : 'No. Nota',
						id: 'cb.nota', width: 220, //anchor: "100%",
						store: ds_notax, valueField: 'nonota', displayField: 'nonota',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'local',
						emptyText:'Pilih...',
						listeners:{
							select:function(combo, records, eOpts){
								var idregdet = Ext.getCmp('idregdet').getValue();
								Ext.getCmp('df.tgl').setValue(records.get('tglnota'));
								Ext.getCmp('tf.jamrit').setValue(records.get('jamnota'));
								//ds_nota.setBaseParam('idregdet',idregdet);
								ds_nota.setBaseParam('nonota',records.get('nonota'));
								ds_nota.setBaseParam('koder',0);
								ds_nota.setBaseParam('idbagian',null);
								ds_nota.setBaseParam('idklstarif',Ext.getCmp("tf.idklstarif").getValue());
								fTotal();
								Ext.getCmp('tf.nonota').setValue(records.get('nonota'));
								Ext.getCmp("btn.cetak").enable();
								
								var idktarif = Ext.getCmp('tf.idklstarif').getValue();
								var idktarif2 = records.get('idklstarif');
								if(idktarif == idktarif2){
									Ext.getCmp('btn.simpan').enable();									
								}else {									
									Ext.MessageBox.alert('Informasi', 'Pasien Telah Pindah Ruangan, Harap Buat Nota Baru.');
									Ext.getCmp('btn.simpan').disable();									
								}
							}
						}
					},{
						xtype: 'textfield',
						id: 'tf.countnonota',
						readOnly: true, style : 'opacity:0.6;font-weight: bold',
						width: 30,
					}]
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl./Jam/Shift', id: 'df.tgl',
						width: 100, value: new Date(),
						format: 'd-m-Y',
					},{
						xtype: 'label', id: 'lb.garing3', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jamrit', readOnly:true,
						width: 65
					},{
						xtype: 'label', id: 'lb.garing4', text: '/'
					},{
						xtype: 'textfield', id: 'tf.shift', 
						width: 60, disabled: true
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Atas Nama',
					id: 'tf.an', anchor: "100%"
				},{
					xtype: 'textarea', fieldLabel: 'Catatan',
					id: 'tf.catatan', anchor: "100%", height: 50,
					value: 'Tindakan Rawat Inap'
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_nota]
				}]
		},{
			xtype: 'textfield',
			id: 'idregdet', hidden: true
		},{
			xtype: 'textfield',
			id: 'tahun', hidden: true
		},{
			xtype: 'textfield',
			id: 'jkel', hidden: true
		}]
	});
	
	var transRIDispPanel = new Ext.form.FormPanel({
		id: 'fp.transRIDispPanel',
		name: 'fp.transRIDispPanel',
		border: false, 
		forceFit:true,
		frame: true,
		autoScroll:true,
		labelAlign: 'top',
		layout: 'anchor',
		items: [
		{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_reg]
		},{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_tindakan]
		}]
	});
	
	var transRIPanel = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Tindakan Rawat Inap',
		frame: false,
		border: true,
		margins: '0 0 5 0',
		plain: true,		
		forceFit: true,
		layout: 'border',
		items: [{
			region: 'center',
			xtype: 'panel',
			border: true,
			layout: 'fit',
			items: [ritindakan_form],
		},
		{
			region: 'east',
			xtype: 'panel',
			title: '', border: true,
			layout:'fit', width:350,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [transRIDispPanel],
		}]
	});
	
	SET_PAGE_CONTENT(transRIPanel);
	
	/*function bersihritindakan() {
		Ext.getCmp('tf.noreg').setValue();
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue();
		Ext.getCmp('tf.upel').setValue();
		Ext.getCmp('tf.dokter').setValue();
		Ext.getCmp('tf.nonota').setValue();
		Ext.getCmp('df.tgl').setValue(new Date());
		Ext.getCmp('df.tglreg').setValue(new Date());
		Ext.getCmp('tf.an').setValue();
		Ext.getCmp('tf.catatan').setValue();
		Ext.getCmp('tahun').setValue();
		Ext.getCmp('jkel').setValue();
		Ext.getCmp('btn.simpan').disable();
		Ext.getCmp('btn.cetak').disable();
		Ext.getCmp('btn.cetakpsh').disable();
		Ext.getCmp('btn.cetakkui').disable();
		Ext.getCmp('tf.jumlah').setValue();
		pmbyrn = 0;
		ds_nota.setBaseParam('idregdet',null);
		ds_nota.reload();
		ds_vregistrasi.setBaseParam('cek','RI');
		ds_vregistrasi.setBaseParam('ctglreg',Ext.util.Format.date(new Date(), 'Y-m-d'));
		ds_vregistrasi.setBaseParam('groupby','noreg');
		ds_vregistrasi.reload();
		ds_vtarifall.setBaseParam('cidbagian',0);
		ds_vtarifall.reload();
		fTotal();
		fTotal2();
		myVar = setInterval(function(){myTimer()},1000);
	}
	*/
	function bersihRinota() {
		Ext.getCmp('cb.nota').setValue('');
		Ext.getCmp('tf.nonota').setValue('');
		Ext.getCmp('tf.total').setValue(0);
		ds_nota.setBaseParam('idregdet', null);
		ds_nota.setBaseParam('idbagian', 999);
		ds_nota.reload();
		Ext.getCmp("btn.cetak").disable();
		Ext.getCmp('btn.simpan').enable();	
		var formattedValue = Ext.util.Format.date(new Date(), 'H:i:s');
		Ext.getCmp('tf.jamrit').setValue(formattedValue);
	}
	
	function fTotal(){
		ds_nota.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				ds_nota.each(function (rec) { sum += parseFloat(rec.get('tarif2')); });
				Ext.getCmp("tf.total").setValue(sum);
			}
		});
	}
	
	function simpanRIT(){
		var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
		var arrnota = [];
		var zx = 0;
		//var cekjm = 0;
		
		ds_nota.each(function (rec) {
			if(rec.get('nmdoktergelar') == null || rec.get('nmdoktergelar') == ''){
				var dr = '';
			} else {
				var arrdr = rec.get('nmdoktergelar');
				var dr = arrdr.split(',', 1);
			}
			zkditem = rec.get('kditem');
			zqty = rec.get('qty');
			zkoder = null;
			ztarif2 = rec.get('tarif2');
			zdiskonjs = rec.get('diskonjs');
			zdiskonjm = rec.get('diskonjm');
			zdiskonjp = rec.get('diskonjp');
			zdiskonbhp = rec.get('diskonbhp');
			zdokter = dr;
			arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp+'-'+zdokter;
			zx++;
			//if(rec.get('tarifjm')>0 && (rec.get('nmdoktergelar') == null || rec.get('nmdoktergelar') == '')) cekjm=1;
		});
		
		/*if(cekjm == 1){
			waitmsg.hide();
			Ext.MessageBox.alert('Informasi', 'Dokter untuk tindakan yang mengunakan jasa medis (item tindakan warna merah) belum terisi');
		} else {*/
			Ext.Ajax.request({
				url: BASE_URL + 'nota_controller/insorupd_nota',
				params: {
					noreg		: Ext.getCmp('tf.noreg').getValue(),
					nonota		: Ext.getCmp('tf.nonota').getValue(),
					nmshift 	: Ext.getCmp('tf.shift').getValue(),
					catatan 	: Ext.getCmp('tf.catatan').getValue(),
					nmpasien 	: Ext.getCmp('tf.nmpasien').getValue(),
					total	 	: Ext.getCmp('tf.total').getValue(),
					atasnama 	: Ext.getCmp('tf.an').getValue(),
					nota		: Ext.getCmp('cb.nota').lastSelectionText,
					tglnota		: Ext.util.Format.date(Ext.getCmp('df.tgl').getValue(), 'Y-m-d'),
					jamnota		: Ext.getCmp('tf.jamrit').getValue(),
					tahun		: Ext.getCmp('tahun').getValue(),
					jkel		: Ext.getCmp('jkel').getValue(),
					jpel 		: 'Rawat Inap',
					ureg 		: 'RI',
					jtransaksi	: 4,
					kui			: 1,
					idbagianfar	: 11,
					arrnota		: Ext.encode(arrnota)
				},
				success: function(response){
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
					obj = Ext.util.JSON.decode(response.responseText);
					ds_vregistrasi.reload();
					ds_notax.reload();
					ds_counnonota.setBaseParam('noreg',Ext.getCmp("tf.noreg").getValue());
					ds_counnonota.reload({
						scope   : this,
						callback: function(records, operation, success) {
							var tamtot = 0;
							 ds_counnonota.each(function (rec) { 
									tamtot = rec.get('cnonota');
								});
							if(tamtot == null){
								Ext.getCmp("tf.countnonota").setValue('0');
							}else{						
								Ext.getCmp("tf.countnonota").setValue(tamtot);
							}
						}
					});
					Ext.getCmp("tf.nonota").setValue(obj.nonota);
					Ext.getCmp("cb.nota").setValue(obj.nonota);
					Ext.getCmp("btn.cetak").enable();
					myStopFunction();
				},
				failure: function (form, action) {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			});
		//}
	}
	
	function hapusItem(){
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/delete_bnotadet',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue(),
				nonota		: Ext.getCmp('tf.nonota').getValue(),
				arr			:  Ext.encode(arr)
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
				bersihritindakan();
			},
			failure: function (form, action) {
				Ext.MessageBox.alert('Informasi', 'Hapus Data Gagal');
			}
		});
	}
	
	function cetakRIT(){
		var nonota		= Ext.getCmp('tf.nonota').getValue();
		var noreg		= Ext.getCmp('tf.noreg').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/nota_ri/'
                +nonota+'/'+noreg);
	}
	
	function totalnota(){
		var zzz = 0;
		for (var zxc = 0; zxc <ds_nota.data.items.length; zxc++) {
			var record = ds_nota.data.items[zxc].data;
			zzz += parseFloat(record.tarif2);
		}
		Ext.getCmp('tf.total').setValue(zzz);
		tottindakan = zzz;
	}

	function hitungKembalian() {
		var jml = Ext.getCmp('tf.jumlah').getValue();
		if(jml != ''){
			var utyd = Ext.getCmp('tf.utyd').getValue();
			var kmblian = utyd - jml;
			Ext.getCmp('tf.kembalian').setValue(kmblian);
		}
	}

	function umur(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tahun').setValue(year);
	}
	
	function fDiskon(rowIndex){
		var rec_nota = ds_nota.getAt(rowIndex);
		var jumlahjs =0;
		var jumlahjm =0;
		var jumlahjp =0;
		var jumlahbhp =0;
		var tariftot =0;
		var dprsntot =0;
		var drptot =0;
		var jumlahtot =0;
		var dprsnjs = 0;
		var dprsnjm = 0;
		var dprsnjp = 0;
		var dprsnbhp = 0;
		
		var disk_form = new Ext.form.FormPanel({
			xtype:'form',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px',
			monitorValid: true,
			height: 270, width: 400,
			layout: 'form', 
			frame: false,
			items: [{
				xtype: 'textfield', fieldLabel:'Nama',
				id: 'tf.nmitemd', width: 200,
				value: rec_nota.data.nmitem,
				disabled:true
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon Seluruh',
				items: [{
					xtype: 'numericfield',
					id: 'tf.dprsnall', width: 40,
					thousandSeparator:',', value:0,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgPrsnAll();
						}
					}
				},{
					xtype: 'label', id: 'lb.dprsnall', text: '%', margins: '0 20 0 2'
				},{
					xtype: 'numericfield',
					id: 'tf.drpall', width: 70, value:0,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgRpAll();
						}
					}
				},{
					xtype: 'label', id: 'lb.drpall', text: 'Rupiah', margins: '0 0 0 2'
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Keterangan',
				items: [{
					xtype: 'numericfield',
					id: 'nm.tes',
					hidden:true
				},{
					xtype: 'label', id: 'lb.tarif', text: 'Tarif', margins: '0 10 0 20'
				},{
					xtype: 'label', id: 'lb.dprsn', text: '%', margins: '0 10 0 30'
				},{
					xtype: 'label', id: 'lb.drp', text: 'Rupiah', margins: '0 10 0 20'
				},{
					xtype: 'label', id: 'lb.jumlah', text: 'Jumlah', margins: '0 10 0 30'
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JS',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjs', width: 70,
					value: rec_nota.data.tarifjs * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjs', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjs', width: 70,
					value: rec_nota.data.diskonjs,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjs', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JM',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjm', width: 70,
					value: rec_nota.data.tarifjm * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjm', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjm', width: 70,
					value: rec_nota.data.diskonjm,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjm', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JP',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjp', width: 70,
					value: rec_nota.data.tarifjp * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjp', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjp', width: 70,
					value: rec_nota.data.diskonjp,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjp', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon BHP',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifbhp', width: 70,
					value: rec_nota.data.tarifbhp * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnbhp', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpbhp', width: 70,
					value: rec_nota.data.diskonbhp,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahbhp', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Total',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tariftot', width: 70,
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsntot', width: 40,
					disabled:true
				},{
					xtype: 'numericfield',
					id: 'tf.drptot', width: 70,
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahtot', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					DiskAdd();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wDisk.close();
				}
			}]
		});
			
		var wDisk = new Ext.Window({
			title: 'Diskon',
			modal: true, closable:false,
			items: [disk_form]
		});
		
		diskonAwal();
		wDisk.show();
		function DiskAdd(){
			var totaldiskon = Ext.getCmp('tf.drptot').getValue();
			var subtotal = (rec_nota.data.tarif * rec_nota.data.qty) -  Ext.getCmp('tf.drptot').getValue();
			
			rec_nota.set('diskonjs', Ext.getCmp('tf.drpjs').getValue());
			rec_nota.set('diskonjm', Ext.getCmp('tf.drpjm').getValue());
			rec_nota.set('diskonjp', Ext.getCmp('tf.drpjp').getValue());
			rec_nota.set('diskonbhp', Ext.getCmp('tf.drpbhp').getValue());
			
			rec_nota.set('drp', totaldiskon);
			rec_nota.set('tarif2', subtotal);
			totalnota();
			wDisk.close();
		}
	
		function diskonAwal(){
			if(rec_nota.data.tarifjs == '')rec_nota.data.tarifjs = 0;
			if(rec_nota.data.tarifjm == '')rec_nota.data.tarifjm = 0;
			if(rec_nota.data.tarifjp == '')rec_nota.data.tarifjp = 0;
			if(rec_nota.data.tarifbhp == '')rec_nota.data.tarifbhp = 0;
			
			if(rec_nota.data.diskonjs == undefined)rec_nota.data.diskonjs = 0;
			if(rec_nota.data.diskonjm == undefined)rec_nota.data.diskonjm = 0;
			if(rec_nota.data.diskonjp == undefined)rec_nota.data.diskonjp = 0;
			if(rec_nota.data.diskonbhp == undefined)rec_nota.data.diskonbhp = 0;
			
			jumlahjs = (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjs);
			jumlahjm = (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjm);
			jumlahjp = (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjp);
			jumlahbhp = (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonbhp);
			
			if(rec_nota.data.tarifjs != 0){
				dprsnjs = parseInt(rec_nota.data.diskonjs) / (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjs').setReadOnly(false);
				Ext.getCmp('tf.dprsnjs').enable();
				Ext.getCmp('tf.drpjs').setReadOnly(false);
				Ext.getCmp('tf.drpjs').enable();
			}
			if(rec_nota.data.tarifjm != 0){
				dprsnjm = parseInt(rec_nota.data.diskonjm) / (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjm').setReadOnly(false);
				Ext.getCmp('tf.dprsnjm').enable();
				Ext.getCmp('tf.drpjm').setReadOnly(false);
				Ext.getCmp('tf.drpjm').enable();
			}
			if(rec_nota.data.tarifjp != 0){
				dprsnjp = parseInt(rec_nota.data.diskonjp) / (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjp').setReadOnly(false);
				Ext.getCmp('tf.dprsnjp').enable();
				Ext.getCmp('tf.drpjp').setReadOnly(false);
				Ext.getCmp('tf.drpjp').enable();
			}
			if(rec_nota.data.tarifbhp != 0){
				dprsnbhp = parseInt(rec_nota.data.diskonbhp) / (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnbhp').setReadOnly(false);
				Ext.getCmp('tf.dprsnbhp').enable();
				Ext.getCmp('tf.drpbhp').setReadOnly(false);
				Ext.getCmp('tf.drpbhp').enable();
			}
			
			tariftot = (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty));
			dprsntot = dprsnjs + dprsnjm + dprsnjp + dprsnbhp;
			drptot = parseInt(rec_nota.data.diskonjs) + parseInt(rec_nota.data.diskonjm) + parseInt(rec_nota.data.diskonjp) + parseInt(rec_nota.data.diskonbhp);
			jumlahtot = jumlahjs + jumlahjm + jumlahjp + jumlahbhp;
			
			Ext.getCmp('tf.dprsnjs').setValue(dprsnjs);
			Ext.getCmp('tf.dprsnjm').setValue(dprsnjm);
			Ext.getCmp('tf.dprsnjp').setValue(dprsnjp);
			Ext.getCmp('tf.dprsnbhp').setValue(dprsnbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jumlahjs);
			Ext.getCmp('tf.jumlahjm').setValue(jumlahjm);
			Ext.getCmp('tf.jumlahjp').setValue(jumlahjp);
			Ext.getCmp('tf.jumlahbhp').setValue(jumlahbhp);
			
			Ext.getCmp('tf.tariftot').setValue(tariftot);
			Ext.getCmp('tf.dprsntot').setValue(dprsntot);
			Ext.getCmp('tf.drptot').setValue(drptot);
			Ext.getCmp('tf.jumlahtot').setValue(jumlahtot);
		}
		
		function htgDiskonPrsn(){
			var diskjs = Ext.getCmp('tf.tarifjs').getValue() * (Ext.getCmp('tf.dprsnjs').getValue() / 100);
			var diskjm = Ext.getCmp('tf.tarifjm').getValue() * (Ext.getCmp('tf.dprsnjm').getValue() / 100);
			var diskjp = Ext.getCmp('tf.tarifjp').getValue() * (Ext.getCmp('tf.dprsnjp').getValue() / 100);
			var diskbhp = Ext.getCmp('tf.tarifbhp').getValue() * (Ext.getCmp('tf.dprsnbhp').getValue() / 100);
			
			var jmljs = Ext.getCmp('tf.tarifjs').getValue() - diskjs;
			var jmljm = Ext.getCmp('tf.tarifjm').getValue() - diskjm;
			var jmljp = Ext.getCmp('tf.tarifjp').getValue() - diskjp;
			var jmlbhp = Ext.getCmp('tf.tarifbhp').getValue() - diskbhp;
			
			var hdprsntot = Ext.getCmp('tf.dprsnjs').getValue() + Ext.getCmp('tf.dprsnjm').getValue() + Ext.getCmp('tf.dprsnjp').getValue() + Ext.getCmp('tf.dprsnbhp').getValue();
			var hdrptot = diskjs + diskjm + diskjp + diskbhp;
			var hjumlahtot = jmljs + jmljm + jmljp + jmlbhp;
			
			Ext.getCmp('tf.drpjs').setValue(diskjs);
			Ext.getCmp('tf.drpjm').setValue(diskjm);
			Ext.getCmp('tf.drpjp').setValue(diskjp);
			Ext.getCmp('tf.drpbhp').setValue(diskbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jmljs);
			Ext.getCmp('tf.jumlahjm').setValue(jmljm);
			Ext.getCmp('tf.jumlahjp').setValue(jmljp);
			Ext.getCmp('tf.jumlahbhp').setValue(jmlbhp);
			
			Ext.getCmp('tf.dprsntot').setValue(hdprsntot);
			Ext.getCmp('tf.drptot').setValue(hdrptot);
			Ext.getCmp('tf.jumlahtot').setValue(hjumlahtot);
		}
		
		function htgPrsnAll(){
			var trfjs = Ext.getCmp('tf.tarifjs').getValue();
			var trfjm = Ext.getCmp('tf.tarifjm').getValue();
			var trfjp = Ext.getCmp('tf.tarifjp').getValue();
			var trfbhp = Ext.getCmp('tf.tarifbhp').getValue();
			var prsnall = Ext.getCmp('tf.dprsnall').getValue();
			
			if(trfjs>0) Ext.getCmp('tf.dprsnjs').setValue(prsnall);
			if(trfjm>0) Ext.getCmp('tf.dprsnjm').setValue(prsnall);
			if(trfjp>0) Ext.getCmp('tf.dprsnjp').setValue(prsnall);
			if(trfbhp>0) Ext.getCmp('tf.dprsnbhp').setValue(prsnall);
			
			var drpall = Ext.getCmp('tf.tariftot').getValue() * (prsnall / 100);
			Ext.getCmp('tf.drpall').setValue(drpall);
			htgDiskonPrsn();
		}
		
		function htgDiskonRp(){
			var diskjs = (Ext.getCmp('tf.drpjs').getValue() * 100) / Ext.getCmp('tf.tarifjs').getValue();
			var diskjm = (Ext.getCmp('tf.drpjm').getValue() * 100) / Ext.getCmp('tf.tarifjm').getValue();
			var diskjp = (Ext.getCmp('tf.drpjp').getValue() * 100) / Ext.getCmp('tf.tarifjp').getValue();
			var diskbhp = (Ext.getCmp('tf.drpbhp').getValue() * 100) / Ext.getCmp('tf.tarifbhp').getValue();
			
			if(isNaN(diskjs) || Ext.getCmp('tf.tarifjs').getValue() < Ext.getCmp('tf.drpjs').getValue()) diskjs = 0;
			if(isNaN(diskjm) || Ext.getCmp('tf.tarifjm').getValue() < Ext.getCmp('tf.drpjm').getValue()) diskjm = 0;
			if(isNaN(diskjp) || Ext.getCmp('tf.tarifjp').getValue() < Ext.getCmp('tf.drpjp').getValue()) diskjp = 0;
			if(isNaN(diskbhp) || Ext.getCmp('tf.tarifbhp').getValue() < Ext.getCmp('tf.drpbhp').getValue()) diskbhp = 0;
			
			var jmljs = Ext.getCmp('tf.tarifjs').getValue() - Ext.getCmp('tf.drpjs').getValue();
			var jmljm = Ext.getCmp('tf.tarifjm').getValue() - Ext.getCmp('tf.drpjm').getValue();
			var jmljp = Ext.getCmp('tf.tarifjp').getValue() - Ext.getCmp('tf.drpjp').getValue();
			var jmlbhp = Ext.getCmp('tf.tarifbhp').getValue() - Ext.getCmp('tf.drpbhp').getValue();
			
			var hdrptot = Ext.getCmp('tf.drpjs').getValue() + Ext.getCmp('tf.drpjm').getValue() + Ext.getCmp('tf.drpjp').getValue() + Ext.getCmp('tf.drpbhp').getValue();
			var hdprsntot = diskjs + diskjm + diskjp + diskbhp;
			var hjumlahtot = jmljs + jmljm + jmljp + jmlbhp;
			
			Ext.getCmp('tf.dprsnjs').setValue(diskjs);
			Ext.getCmp('tf.dprsnjm').setValue(diskjm);
			Ext.getCmp('tf.dprsnjp').setValue(diskjp);
			Ext.getCmp('tf.dprsnbhp').setValue(diskbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jmljs);
			Ext.getCmp('tf.jumlahjm').setValue(jmljm);
			Ext.getCmp('tf.jumlahjp').setValue(jmljp);
			Ext.getCmp('tf.jumlahbhp').setValue(jmlbhp);
			
			Ext.getCmp('tf.dprsntot').setValue(hdprsntot);
			Ext.getCmp('tf.drptot').setValue(hdrptot);
			Ext.getCmp('tf.jumlahtot').setValue(hjumlahtot);
		}
		
		function htgRpAll(){
			var trfjs = Ext.getCmp('tf.tarifjs').getValue();
			var trfjm = Ext.getCmp('tf.tarifjm').getValue();
			var trfjp = Ext.getCmp('tf.tarifjp').getValue();
			var trfbhp = Ext.getCmp('tf.tarifbhp').getValue();
			var drpall = Ext.getCmp('tf.drpall').getValue();
			var dprsnall = (drpall * 100) / Ext.getCmp('tf.tariftot').getValue();
			
			if(trfjs>0) Ext.getCmp('tf.dprsnjs').setValue(dprsnall);
			if(trfjm>0) Ext.getCmp('tf.dprsnjm').setValue(dprsnall);
			if(trfjp>0) Ext.getCmp('tf.dprsnjp').setValue(dprsnall);
			if(trfbhp>0) Ext.getCmp('tf.dprsnbhp').setValue(dprsnall);
			
			Ext.getCmp('tf.dprsnall').setValue(dprsnall);
			htgDiskonPrsn();
		}
	
	}
	
	function TemPelayanan(){
		var ds_mastertempelayanan = dm_mastertempelayanan_parent();
		var ds_tempelayananri = dm_tempelayananri();
		
		function fnkeyAdd(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
				
		var cm_mastertempelayanan_parent = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idtemplayanan',
				width: 30
			},{
				header: 'Nama Template Pelayanan',
				dataIndex: 'nmtemplayanan',
				width: 400,
				renderer: fnkeyAdd
			}
		]);
		var sm_mastertempelayanan_parent = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_mastertempelayanan_parent = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_mastertempelayanan_parent = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_mastertempelayanan,
			//displayInfo: true,
			//displayMsg: 'Data Master Paket Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_mastertempelayanan_parent = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200,
			disableIndexes:['idtemplayanan'],
		})];
		var grid_find_mastertempelayanan_parent = new Ext.grid.GridPanel({
			ds: ds_mastertempelayanan,
			cm: cm_mastertempelayanan_parent,
			sm: sm_mastertempelayanan_parent,
			view: vw_mastertempelayanan_parent,
			height: 460,
			width: 422,
			plugins: cari_mastertempelayanan_parent,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			autoScroll: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_mastertempelayanan_parent,
			listeners: {
				cellclick: onCellClickaddparent
			}
		});
		var win_find_mastertempelayanan_parent = new Ext.Window({
			title: 'Template Pelayanan',
			modal: true,
			items: [grid_find_mastertempelayanan_parent]
		}).show();
		
		function onCellClickaddparent(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			
			if (t.className == 'keyMasterDetail'){
				var record = ds_mastertempelayanan.getAt(rowIndex);
				var var_idtemplayanan = record.data["idtemplayanan"];
				ds_tempelayananri.setBaseParam('idtemplayanan', var_idtemplayanan);
				ds_tempelayananri.setBaseParam('idklstarif', idklstariftp);
				ds_tempelayananri.reload({
					scope   : this,
					callback: function(records, operation, success) {
						ds_tempelayananri.each(function (rec) {
							var skditem			= rec.get("kdpelayanan");
							var snmitem			= rec.get("nmpelayanan");
							var starifjs		= rec.get("tarifjs");
							var starifjm		= rec.get("tarifjm");
							var starifjp		= rec.get("tarifjp");
							var starifbhp		= rec.get("tarifbhp");
							var sdiskonjs		= rec.get("diskonjs");
							var sdiskonjm		= rec.get("diskonjm");
							var sdiskonjp		= rec.get("diskonjp");
							var sdiskonbhp		= rec.get("diskonbhp");
							var starif			= rec.get("ttltarif");
							var snmsatuan		= rec.get("satuankcl");
							
							var orgaListRecord = new Ext.data.Record.create([
								{
									name: 'kditem',
									name: 'koder',
									name: 'nmitem',
									name: 'qty',
									name: 'tarif',
									name: 'tarif2',
									name: 'nmsatuan',
									name: 'tarifjs',
									name: 'tarifjm',
									name: 'tarifjp',
									name: 'tarifbhp',
									name: 'diskonjs',
									name: 'diskonjm',
									name: 'diskonjp',
									name: 'diskonbhp'
								}
							]);
							
							ds_nota.add([
								new orgaListRecord({
									'kditem': skditem,
									'koder': null,
									'nmitem': snmitem,
									//'nmdokter':Ext.getCmp('tf.nmdokter').getValue(),
									'nmdokter':'',
									'qty': 1,
									'tarif': starif,
									'tarif2': starif,
									'drp': 0,
									'nmsatuan': snmsatuan,
									'tarifjs': starifjs,
									'tarifjm': starifjm,
									'tarifjp': starifjp,
									'tarifbhp': starifbhp,
									'diskonjs': sdiskonjs,
									'diskonjm': sdiskonjm,
									'diskonjp': sdiskonjp,
									'diskonbhp': sdiskonbhp
								})
							]);
							var ttl = Ext.getCmp('tf.total').getValue();
							var jml = parseFloat(ttl) + parseFloat(starif);
							Ext.getCmp('tf.total').setValue(jml);
						});
					}
				});
				win_find_mastertempelayanan_parent.close();
			}
			return true;
		}
	}
	
	function jasaMedis(value, a, b){
		if(b.data.tarifjm > 0) return '<font color="red">'+ value +'</font>';
		else return value;
	}
	
}