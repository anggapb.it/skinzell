function dashboard_index() {
//Data Store Traffict
	var ds_tahun = dm_tahun();
  Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_pasien',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp("tf.norm").setValue(obj.norm);
        }
    });
      Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_dokter',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp("tf.iddokter").setValue(obj.iddokter);
        }
    });
    Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_perawat',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp("tf.idperawat").setValue(obj.idperawat);
        }
    });
    Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_bed',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp("tf.idbed").setValue(obj.idbed);
        }
    });
	Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/informasi',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
			//console.log(obj.deskripsi);
			
            Ext.getCmp("ta.liburan").setValue(obj.data);
        }
    }); 

//
//==============diagram=================

	 var store2 = RH.JsonStore({
		url : BASE_URL + 'dashboard_controller/chart_pasien',
		fields: ['tahunreg','pasienlama','pasienbaru']
	}); 
	
    // COLUMN
   var panel3 = new Ext.Panel({
    title: 'Grafik Kunjungan Pasien Baru & Lama Ke Rumah Sakit',
        frame:true,
    //    renderTo: 'container',
        width:500,
        height:300,
        layout:'fit',             
        items: {
            xtype: 'columnchart',
            store: store2,
            stacked:true,
            xField: 'tahunreg',
             yAxis: new Ext.chart.NumericAxis({
                displayName: 'Pasien Baru',
                labelRenderer : Ext.util.Format.numberRenderer('0')
            }),
             tipRenderer : function(chart, record, index, series){
                if(series.yField == 'pasienbaru'){
                    return Ext.util.Format.number(record.data.pasienbaru, '0,0') + ' pasien baru ' + record.data.tahunreg;
                }else{
                    return Ext.util.Format.number(record.data.pasienlama, '0,0') + ' pasien laru ' + record.data.tahunreg;
                }
				},
            chartStyle: {
                padding: 10,
                animationEnabled: true,
                font: {
                    name: 'Tahoma',
                    color: 0x444444,
                    size: 11
                },
                dataTip: {
                    padding: 5,
                    border: {
                        color: 0x99bbe8,
                        size:1
                    },
                    background: {
                        color: 0xDAE7F6,
                        alpha: .9
                    },
                    font: {
                        name: 'Tahoma',
                        color: 0x15428B,
                        size: 10,
                        bold: true
                    }
                },
                xAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xeeeeee}
                },
                yAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xdfe8f6}
                }
            },
            series: [{
                type: 'column',
                displayName: 'Pasien Baru',
                yField: 'pasienbaru',
				style: {color: 0xCC3333}
            },{
                type:'column',
                displayName: 'Pasien Lama',
                yField: 'pasienlama',
				style: {color: 0x3333FF}
            }]
        }
    });
 


    var store = RH.JsonStore({
        url: BASE_URL + 'dashboard_controller/pie_pasien',
        fields: ['tahundaftar', 'norm'],
        //params: [{key:'norm', id:'tahundaftar'}]
    });
    var panel1 = new Ext.Panel({
		title: 'Grafik Tahun daftar Pasien',
        frame:true,
        width: 500,
        height: 400,
        border: false,
        bodyStyle:{"background-color":"#000000"}, 
        items: {
            store: store,
            xtype: 'piechart',
            dataField: 'norm',
            categoryField: 'tahundaftar',
            //extra styles get applied to the chart defaults
            extraStyle:
                    {
                        legend:
                                {
                                    display: 'bottom',
                                    padding: 5,
                                    font:
                                            {
                                                family: 'Tahoma',
                                                size: 13
                                            }
                                }
                    }
        }
    });
    
	var store3 = RH.JsonStore({
		url : BASE_URL + 'dashboard_controller/count_poli',
		fields: ['nmbagian','polilama','polibaru']
	}); 
    var panel2 = new Ext.Panel({
        title: 'Grafik kunjungan Pasien Baru & Lama Unit/Ruang Pelayanan',
        frame:true,
		id: 'gf.kunjungan',
	 /* 	tbar : [{
			xtype: 'combo', fieldLabel: 'Years',
			id: 'cb.tahun', width: 100,
			store: ds_tahun, valueField: 'tahun', displayField: 'tahun',
			editable: false, triggerAction: 'all',
			forceSelection: true, submitValue: true, mode: 'local',
			listeners: {
							select: function() {
								var isi = Ext.getCmp('cb.tahun').getValue();
								if (isi){
									Ext.getCmp("tf.tahun").setValue(isi);
								}
							}
						}
		},{
			xtype: 'textfield', id: 'tf.tahun', width: 300,hidden: true,
			validator: function() {
							store3.setBaseParam('tahun', Ext.getCmp('tf.tahun').getValue());
							Ext.getCmp('gf.kunjungan').store.reload();
						
						}
		}],  */
    //    renderTo: 'container',
        width:500,
        height:300,
        layout:'fit',
        items: {
            xtype: 'barchart',
            store: store3,
            stacked:true,
            yField: 'nmbagian',
          /*   xAxis: new Ext.chart.NumericAxis({
                displayName: 'Visits',
                labelRenderer : Ext.util.Format.numberRenderer('0,0')
            }), */
            tipRenderer : function(chart, record, index, series){
                if(series.xField == 'polibaru'){
                    return Ext.util.Format.number(record.data.polibaru, '0,0') + ' Pasien Baru ' + record.data.nmbagian;
                }else{
                    return Ext.util.Format.number(record.data.polilama, '0,0') + ' Pasien Lama ' + record.data.nmbagian;
                }
            },
            chartStyle: {
                padding: 10,
                animationEnabled: true,
                font: {
                    name: 'Tahoma',
                    color: 0x444444,
                    size: 11
                },
                dataTip: {
                    padding: 5,
                    border: {
                        color: 0x99bbe8,
                        size:1
                    },
                    background: {
                        color: 0xDAE7F6,
                        alpha: .9
                    },
                    font: {
                        name: 'Tahoma',
                        color: 0x15428B,
                        size: 10,
                        bold: true
                    }
                },
                yAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xeeeeee}
                },
                xAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xdfe8f6}
                }
            },
            series: [{
                type: 'bar',
                displayName: 'Pasien Baru',
                xField: 'polibaru',
				style: {color: 0xCC3333}
            },{
                type:'bar',
                displayName: 'Pasien Lama',
                xField: 'polilama',
				style: {color: 0x3333FF}
            }]
        }
    });
    
//
    Ext.chart.Chart.CHART_URL = BASE_URL + 'resources/js/ext/resources/charts.swf';

//=================================================================
/*
    Setting Panel
*/
 	 var traffict_main = new Ext.Panel({
        title: 'Dashboard',
        autoScroll: true,
                        										 
		items:[{
			layout: 'column',
			border : false,
			defaults: { labelWidth: 150, labelAlign: 'left' }, 
			items: [{
                xtype: 'container',
                style: 'padding: 25px',
				columnWidth: 0.25,
                items: [{
							xtype: 'label',
							id: 'info_label1', 
							text: 'Total Pasien',
							style: 'align:center; font-size:20px; font-weight:bold; color:black;', margins: '0 0 0 10',
						},{
                        xtype: 'fieldset', title: '',
                        height: 75,
						width: 192,
						//style: 'color:#51C22C;'
						style: '-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#2d9de3),color-stop(100%,#0c52a1));background-image:-webkit-linear-gradient(top,#2d9de3,#0c52a1);background-image:-moz-linear-gradient(top,#2d9de3,#0c52a1);background-image:-o-linear-gradient(top,#2d9de3,#0c52a1); background-image:linear-gradient(top,#2d9de3,#0c52a1);border-bottom:1px solid #136B7F',
                        layout: 'vbox',
                        items: [{
                                xtype: 'textfield', id: 'tf.norm', width: 178, height: 50, layout: 'transparent',
                                disabled: true, style: 'margin: 0px; font-size: 30px; color:black; text-align: center;',anchor: '100%'
                            }]

                    }]
				},
				{
                xtype: 'container',
                style: 'padding: 25px',
				columnWidth: 0.25,
                items: [{
							xtype: 'label',
							id: 'info_label1', 
							text: 'Total Dokter',
							style: 'font-size:20px; font-weight:bold; color:black;', margins: '0 0 0 10',
						},{
                        xtype: 'fieldset', title: '',
                        height: 75,
                        width: 192,
						style: '-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#FF344E),color-stop(100%,#FF0021));background-image:-webkit-linear-gradient(top,#FF344E,#FF0021);background-image:-moz-linear-gradient(top,#FF344E,#FF0021);background-image:-o-linear-gradient(top,#FF344E,#FF0021); background-image:linear-gradient(top,#FF344E,#FF0021);border-bottom:1px solid #136B7F',
                        layout: 'vbox',
                        items: [{
                                xtype: 'textfield', id: 'tf.iddokter', width: 178, height: 50,
                                disabled: true, style: 'margin: 0px; font-size: 30px; text-align: center;',anchor: '100%'
                            }]
                    }]
				},
				{
                xtype: 'container',
                style: 'padding: 25px',
				columnWidth: 0.25,
                 items: [{
							xtype: 'label',
							id: 'info_label1', 
							text: 'Total Perawat',
							style: 'font-size:20px; font-weight:bold; color:black;', margins: '0 0 0 10',
						},{
                        xtype: 'fieldset', title: '',
                        height: 75,
                        width: 192,
						style: '-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#2CC24D),color-stop(100%,#0EC222));background-image:-webkit-linear-gradient(top,#2CC24D,#0EC222);background-image:-moz-linear-gradient(top,#2CC24D,#0EC222);background-image:-o-linear-gradient(top,#2CC24D,#0EC222); background-image:linear-gradient(top,#2CC24D,#0EC222);border-bottom:1px solid #136B7F',
                        layout: 'vbox',
                        items: [{
                                xtype: 'textfield', id: 'tf.idperawat', width: 178, height: 50,
                                disabled: true,style: 'margin: 0px; font-size: 30px; text-align: center;',anchor: '100%'
                            }]
                    }]
				},
				{
                xtype: 'container',
                style: 'padding: 25px',
				columnWidth: 0.25,
               items: [{
							xtype: 'label',
							id: 'info_label1', 
							text: 'Total Bed',
							style: 'font-size:20px; font-weight:bold; color:black;', margins: '0 0 0 10',
						},{
                        xtype: 'fieldset', title: '',
                        height: 75,
                        width: 192,
						//style: 'background-color: #A0D9A4;',
                        style: '-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#DECB40),color-stop(100%,#DEB901));background-image:-webkit-linear-gradient(top,#DECB40,#DEB901);background-image:-moz-linear-gradient(top,#DECB40,#DEB901);background-image:-o-linear-gradient(top,#DECB40,#DEB901); background-image:linear-gradient(top,#DECB40,#DEB901);border-bottom:1px solid #136B7F',
                        layout: 'vbox',
                        items: [{
                                xtype: 'textfield', id: 'tf.idbed', width: 178, height: 50,
                                disabled: true,style: 'margin: 0px; font-size: 30px; text-align: center;',anchor: '100%'
                            }]
                    }]
				}]
			},
			{
			layout: 'column',
			border : false,
			defaults: { labelWidth: 150, labelAlign: 'top' }, 
			items: [{
                xtype: 'container',
                style: 'padding: 25px',
				columnWidth: 0.50,
                items: [panel1]
				},
				{
                xtype: 'container',
                style: 'padding: 25px',
				layout:'fit',
				columnWidth: 0.50,
                items: [{
                                xtype: 'htmleditor',
                				fieldLabel: 'Deskripsi',
                				anchor:'97% 55%',
								height: 410,
                			//	name:'deskripsiind',
                				id: 'ta.liburan',
                				readOnly: true,
                                }]
				}]
			},{
                xtype: 'container',
            //    style: 'margin-top: -100px; padding-left: -520px',
                columnWidth: 1,
				layout: 'column',
				items: [{
					xtype: 'fieldset', title: '',
					border: false,
					columnWidth: 0.5,
					items: [{
                        xtype: 'fieldset', title: '',
                        height: 400,
                        boxMaxHeight: 400,
                        width: 600,
                        border: false,
                        layout: 'vbox',
                        items: [panel3]
                    }]  
				},{
					xtype: 'container',
                style: 'margin-top: 0px; padding-left: 20px;',
                columnWidth: 0.5,
               
                     items: [{
                        xtype: 'fieldset', title: '',
                        height: 400,
                        boxMaxHeight: 400,
                        width: 600,
                        border: false,
                        layout: 'vbox',
                        items: [panel2]
                    }]
				}]   
             }]
    });
	
    SET_PAGE_CONTENT(traffict_main);
	Ext.getCmp('ta.liburan').getToolbar().hide();
}