function dashboard_index() {
//Data Store Traffict
    var pageSize = 12;
	var ds_tahun = dm_tahun();
	var ds_dokter = dm_dokter();
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_dokter,
		displayInfo: true,
		displayMsg: 'Data Tenaga Medis Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	
	var grid_tmedis = new Ext.grid.GridPanel({
		id: 'grid_dokter',
		store: ds_dokter,		
		autoScroll: true,
		height: 320,//autoHeight: true,
		columnLines: true,
		//plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddDokter();
				//Ext.getCmp('tf.frm.kddokter').setReadOnly(false);
			}
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'Kode',
			width: 70,
			dataIndex: 'kddokter',
			sortable: true
		},{
			header: 'Tenaga Medis',
			width: 110,
			dataIndex: 'nmjnstenagamedis',
			sortable: true
		},
		{
			header: 'Nama Tenaga Medis Dengan Gelar',
			width: 220,
			dataIndex: 'nmdoktergelar',
			sortable: true
		},
		{
			header: 'Jenis Kelamin',
			width: 100,
			dataIndex: 'nmjnskelamin',
			sortable: true
		},
		{
			header: 'Tempat lahir',
			width: 100,
			dataIndex: 'tptlahir',
			sortable: true
		},		
		{
			header: 'No. Telepon',
			width: 100,
			dataIndex: 'notelp',
			sortable: true
		},
		{
			header: 'Spesialisasi',
			width: 300,
			dataIndex: 'nmspesialisasi',
			sortable: true
		},
		{
			header: 'Status',
			width: 100,
			dataIndex: 'nmstatus',
			sortable: true
		},
		{
			header: 'Status Dokter',
			width: 100,
			dataIndex: 'nmstdokter',
			sortable: true
		}],
		bbar: paging,
	});
   
	
  Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_pasien',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp("tf.norm").setValue(obj.norm);
        }
    });
      Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_dokter',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp("tf.iddokter").setValue(obj.iddokter);
        }
    });
    Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_perawat',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
           // Ext.getCmp("tf.idperawat").setValue(obj.idperawat);
        }
    });
    Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_bed_isi',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp("tf.idbed").setValue(obj.idbed);
        }
    });
	Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/count_bed_kosong',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp("tf.idbed2").setValue(obj.idbedkosong);
        }
    });
	Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/informasi',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
			//console.log(obj.deskripsi);
			
            Ext.getCmp("ta.liburan").setValue(obj.data);
        }
    }); 
	var thn = '';
	Ext.Ajax.request({
        url: BASE_URL + 'dashboard_controller/thn',
        method: 'POST',
        success: function(response) {
            obj = Ext.util.JSON.decode(response.responseText);
			//console.log(obj.deskripsi);
			Ext.getCmp("cb_tahun").setValue(obj.tahun);
			Ext.getCmp("cb_tahun2").setValue(obj.tahun);
			
			reload_chart();
			reload_chart2();
			
            thn = obj.tahun;
        }
    }); 

//  
//==============diagram=================
    var store2 = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'dashboard_controller/chart_pasien_rj',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		
		autoLoad: true,
		fields: ['nmbagian','noreg'],
		/*listeners:{
			afterrender: Ext.getCmp('cb_tahun').setValue('2015');
		}*/
	}); 
	
	ds_tahun = dm_tahun();
	
	var panel3 = new Ext.Panel({
        //width: 1080,
        height: 300,
        title: 'Grafik Kunjungan Pasien Rawat Jalan',
		tbar: [{
			xtype :'label',
			text : 'Tahun: '
		},{
								xtype: 'combo',
								id: 'cb_tahun', fieldLabel: '', 
								store: ds_tahun, valueField: 'tahun', displayField: 'tahun',
								triggerAction: 'all',editable: false, width: 80, 				
								forceSelection: true, submitValue: true, mode: 'local',
								//defaultValue: '2015',
								emptyText:'Pilih...', 
								listeners:{ 
									scope: this, 'select': function(){ reload_chart();} 
								} 
		}],
        items: {
			
            xtype: 'linechart',
            store: store2,
            yField: 'noreg',
			xField: 'nmbagian',
				xAxis: new Ext.chart.CategoryAxis({
					title: ''
				}),
				yAxis: new Ext.chart.NumericAxis({
					title: 'Jumlah',
					//labelRenderer : Ext.util.Format.numberRenderer('0,0')
				}),
		},
    });
	
	var store_ri_ugd = new Ext.data.JsonStore({
		proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'dashboard_controller/chart_pasien_ri',
			method: 'POST'
		}),
		totalProperty: 'results',
		root: 'data',
		
		autoLoad: true,
		fields: ['nmbagian','noreg','polibaru','polilama','iddokter'],
		/*listeners:{
			afterrender: Ext.getCmp('cb_tahun').setValue('2015');
		}*/
	}); 
	
	ds_tahun = dm_tahun();
	
	var panel_ri_ugd = new Ext.Panel({
        //width: 1080,
        height: 300,
        title: 'Grafik Kunjungan Pasien Rawat Inap',
		tbar: [{
			xtype :'label',
			text : 'Tahun: '
		},{
								xtype: 'combo',
								id: 'cb_tahun2', fieldLabel: '', 
								store: ds_tahun, valueField: 'tahun', displayField: 'tahun',
								triggerAction: 'all',editable: false, width: 80, 				
								forceSelection: true, submitValue: true, mode: 'local',
								//defaultValue: '2015',
								emptyText:'Pilih...', 
								listeners:{ 
									scope: this, 'select': function(){ reload_chart2();} 
								} 
		}],
        items: {
			
            xtype: 'columnchart',
            store: store_ri_ugd,
            series: [
					{
						yField: 'noreg',
						displayName: 'Pasien'
					},/*{
						yField: 'iddokter',
						displayName: 'Dokter'
					}*/
				],
			xField: 'nmbagian',
				xAxis: new Ext.chart.CategoryAxis({
					title: ''
				}),
				yAxis: new Ext.chart.NumericAxis({
					title: 'Jumlah',
					//labelRenderer : Ext.util.Format.numberRenderer('0,0')
				}),
		},
    });
	
	
	
    // COLUMN
  
	

    var store = RH.JsonStore({
        url: BASE_URL + 'dashboard_controller/pie_pasien',
        fields: ['tahundaftar', 'norm'],
        //params: [{key:'norm', id:'tahundaftar'}]
    });
    var panel1 = new Ext.Panel({
		title: 'Grafik Tahun daftar Pasien',
        frame:true,
        //width: 500,
		autoWidth:true,
		forceFit: true, //autoHeight: true, 
		autoScroll: true,
		autoSizeColumns: true,
        height: 448,
        border: false,
        //bodyStyle:{"background-color":"#d8f1f0"}, 
		style: 'padding: 25px; background-color: #8223e4;', 
        items: {
            store: store,
            xtype: 'piechart',
            dataField: 'norm',
            categoryField: 'tahundaftar',
            //extra styles get applied to the chart defaults
            extraStyle:
                    {
                        legend:
                                {
                                    display: 'bottom',
                                    padding: 5,
                                    font:
                                            {
                                                family: 'Tahoma',
                                                size: 13
                                            }
                                }
                    }
        }
    });
   
	var store_tmedis = RH.JsonStore({
        url: BASE_URL + 'dashboard_controller/pie_tmedis',
        fields: ['nmjnstenagamedis', 'kddokter'],
        //params: [{key:'norm', id:'tahundaftar'}]
    });
	
  
	var panel_tmedis = new Ext.Panel({
       // width: 530,
        height: 300,
        title: 'Grafik Tenaga Medis',
        
        items: {
			//xtype: 'columnchart',
            //store: ds_lsm,
			xtype: 'linechart',
            store: store_tmedis,
            xField: 'nmjnstenagamedis',
			yAxis: new Ext.chart.NumericAxis({
                title: 'Jumlah',
				orientation:'vertical'
            }),
            yField: 'kddokter',
			listeners: {
				itemclick: function(o){
					var rec = store.getAt(o.index);
					Ext.example.msg('Item Selected', 'You chose {0}.', rec.get('nmjnstenagamedis'));
				}
			}
		},
    });
    
	var store3 = RH.JsonStore({
		url : BASE_URL + 'dashboard_controller/count_poli',
		fields: ['nmbagian','polilama','polibaru']
	}); 
    var panel2 = new Ext.Panel({
        title: 'Grafik Kunjungan Pasien Baru & Lama Unit/Ruang Pelayanan',
        frame:true,
		id: 'gf.kunjungan',
	 /* 	tbar : [{
			xtype: 'combo', fieldLabel: 'Years',
			id: 'cb.tahun', width: 100,
			store: ds_tahun, valueField: 'tahun', displayField: 'tahun',
			editable: false, triggerAction: 'all',
			forceSelection: true, submitValue: true, mode: 'local',
			listeners: {
							select: function() {
								var isi = Ext.getCmp('cb.tahun').getValue();
								if (isi){
									Ext.getCmp("tf.tahun").setValue(isi);
								}
							}
						}
		},{
			xtype: 'textfield', id: 'tf.tahun', width: 300,hidden: true,
			validator: function() {
							store3.setBaseParam('tahun', Ext.getCmp('tf.tahun').getValue());
							Ext.getCmp('gf.kunjungan').store.reload();
						
						}
		}],  */
    //    renderTo: 'container',
        width:1080,
        height:300,
        layout:'fit',
		style: 'background-color: #ddf1fd;',
        items: {
            xtype: 'barchart',
            store: store3,
            stacked:true,
            yField: 'nmbagian',
          /*   xAxis: new Ext.chart.NumericAxis({
                displayName: 'Visits',
                labelRenderer : Ext.util.Format.numberRenderer('0,0')
            }), */
            tipRenderer : function(chart, record, index, series){
                if(series.xField == 'polibaru'){
                    return Ext.util.Format.number(record.data.polibaru, '0,0') + ' Pasien Baru ' + record.data.nmbagian;
                }else{
                    return Ext.util.Format.number(record.data.polilama, '0,0') + ' Pasien Lama ' + record.data.nmbagian;
                }
            },
            chartStyle: {
                padding: 10,
                animationEnabled: true,
                font: {
                    name: 'Tahoma',
                    color: 0x444444,
                    size: 11
                },
                dataTip: {
                    padding: 5,
                    border: {
                        color: 0x99bbe8,
                        size:1
                    },
                    background: {
                        color: 0xDAE7F6,
                        alpha: .9
                    },
                    font: {
                        name: 'Tahoma',
                        color: 0x15428B,
                        size: 10,
                        bold: true
                    }
                },
                yAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xeeeeee}
                },
                xAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xdfe8f6}
                }
            },
            series: [{
                type: 'bar',
                displayName: 'Pasien Baru',
                xField: 'polibaru',
				style: {color: 0xCC3333}
            },{
                type:'bar',
                displayName: 'Pasien Lama',
                xField: 'polilama',
				style: {color: 0x3333FF}
            }]
        }
    });
    
//
    Ext.chart.Chart.CHART_URL = BASE_URL + 'resources/js/ext/resources/charts.swf';

//=================================================================
/*
    Setting Panel
*/
 	 var traffict_main = new Ext.Panel({
        title: 'Dashboard',
        autoScroll: true,
                        										 
		items:[{
			layout: 'column',
			border : false,
			defaults: { labelWidth: 150, labelAlign: 'left' }, 
			items: [{
                xtype: 'container',
                style: 'padding: 15px; background-color: #32bff4;',
				columnWidth: 0.25,
                items: [{
							xtype: 'label',
							id: 'info_label1', 
							text: 'Total Pasien',
							style: 'align:center; font-size:20px; font-weight:bold; color:white;', margins: '0 0 0 10',
						},{
                        xtype: 'fieldset', title: '',
                        height: 75,
						width: 192,
						//style: 'color:#51C22C;'
						style: '-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#72bfec),color-stop(100%,#1d8dcd));background-image:-webkit-linear-gradient(top,#72bfec,#1d8dcd);background-image:-moz-linear-gradient(top,#72bfec,#1d8dcd);background-image:-o-linear-gradient(top,#72bfec,#1d8dcd); background-image:linear-gradient(top,#72bfec,#1d8dcd);border-bottom:1px solid #1d8dcd',
                        layout: 'vbox',
                        items: [{
                                xtype: 'numericfield', decimalSeparator: ',', decimalPrecision: 0, alwaysDisplayDecimals: true,
								useThousandSeparator: true, thousandSeparator: '.', value: 0, id: 'tf.norm', width: 178, height: 50, layout: 'transparent',
                                disabled: true, style: 'margin: 0px; font-size: 30px; color:black; text-align: center;',anchor: '100%'
                            }]

                    }]
				},
				{
                xtype: 'container',
                style: 'padding: 15px; background-color: #a7e14c;',
				columnWidth: 0.25,
                items: [{
							xtype: 'label',
							id: 'info_label1', 
							text: 'Total Tenaga Medis',
							style: 'font-size:20px; font-weight:bold; color:white;', margins: '0 0 0 10',
						},{
                        xtype: 'fieldset', title: '',
                        height: 75,
                        width: 192,
						style: '-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#bae068),color-stop(100%,#8abe1c));background-image:-webkit-linear-gradient(top,#bae068,#8abe1c);background-image:-moz-linear-gradient(top,#bae068,#8abe1c);background-image:-o-linear-gradient(top,#bae068,#8abe1c); background-image:linear-gradient(top,#bae068,#8abe1c);border-bottom:1px solid #8abe1c',
                        layout: 'vbox',
                        items: [{
                                xtype: 'textfield', id: 'tf.iddokter', width: 178, height: 50,
                                disabled: true, style: 'margin: 0px; font-size: 30px; text-align: center;',anchor: '100%'
                            }]
                    }]
				},
				{
                xtype: 'container',
                style: 'padding: 15px; background-color: #dfe14c;',
				columnWidth: 0.25,
               items: [{
							xtype: 'label',
							id: 'info_label1', 
							text: 'Bed Terisi',
							style: 'font-size:20px; font-weight:bold; color:white;', margins: '0 0 0 10',
						},{
                        xtype: 'fieldset', title: '',
                        height: 75,
                        width: 192,
						//style: 'background-color: #A0D9A4;',
                        style: '-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#eddf7b),color-stop(100%,#DEB901));background-image:-webkit-linear-gradient(top,#eddf7b,#DEB901);background-image:-moz-linear-gradient(top,#eddf7b,#DEB901);background-image:-o-linear-gradient(top,#eddf7b,#DEB901); background-image:linear-gradient(top,#eddf7b,#DEB901);border-bottom:1px solid #DEB901',
                        layout: 'vbox',
                        items: [{
                                xtype: 'textfield', id: 'tf.idbed', width: 178, height: 50,
                                disabled: true,style: 'margin: 0px; font-size: 30px; text-align: center;',anchor: '100%'
                            }]
                    }]
				},
				{
                xtype: 'container',
                style: 'padding: 15px; background-color: #4ce1b5;',
				columnWidth: 0.25,
               items: [{
							xtype: 'label',
							id: 'info_label12', 
							text: 'Bed Kosong',
							style: 'font-size:20px; font-weight:bold; color:white;', margins: '0 0 0 10',
						},{
                        xtype: 'fieldset', title: '',
                        height: 75,
                        width: 192,
						//style: 'background-color: #A0D9A4;',
                        style: '-moz-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);-webkit-box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);box-shadow:0px 0px 3px 1px rgba(0,0,0,0.3);background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#7ad2cf),color-stop(100%,#18bcb6));background-image:-webkit-linear-gradient(top,#7ad2cf,#18bcb6);background-image:-moz-linear-gradient(top,#7ad2cf,#18bcb6);background-image:-o-linear-gradient(top,#7ad2cf,#18bcb6); background-image:linear-gradient(top,#7ad2cf,#18bcb6);border-bottom:1px solid #18bcb6',
                        layout: 'vbox',
                        items: [{
                                xtype: 'textfield', id: 'tf.idbed2', width: 178, height: 50,
                                disabled: true,style: 'margin: 0px; font-size: 30px; text-align: center;',anchor: '100%'
                            }]
                    }]
				}]
			},
			{
			layout: 'column',
			border : false,
			defaults: { labelWidth: 150, labelAlign: 'top' }, 
			items: [{
                xtype: 'container',
				columnWidth: 0.50,
                items: [panel1]
				},
				{
                xtype: 'container',
                style: 'padding: 25px; background-color: #e4235e;',
				layout:'fit',
				columnWidth: 0.50,
                items: [{
							xtype: 'label',
							text: '================= INFORMASI ================',
							style: 'font-size:20px; font-weight:bold; color: white;', margins: '0 0 0 10',
						},{
                                xtype: 'htmleditor',
                				fieldLabel: 'Deskripsi',
                				anchor:'97% 55%',
								height: 402,
                			//	name:'deskripsiind',
                				id: 'ta.liburan',
                				readOnly: true,
                       }]
				},{
                xtype: 'container',
				style: 'padding: 25px; background-color: #32bff4;',
				columnWidth: 1,
                items: [panel_tmedis]
				}]
			},{
                xtype: 'container',
            //    style: 'margin-top: -100px; padding-left: -520px',
                columnWidth: 1,
				layout: 'column',
				items: [{
					xtype: 'fieldset', title: '',
					border: false,
					columnWidth: 1,
					style: 'padding:15px; background-color: #e1ab4c;',
					items: [{
                        xtype: 'fieldset', title: '',
                        height: 320,
                        boxMaxHeight: 320,
                       // width: 1200,
                        border: false,
                        layout: 'vbox',
                        items: [panel3]
                    }]  
				},{
					xtype: 'container',
                //style: 'margin-top: 0px; padding-left: 20px;',
                columnWidth: 1,
				layout: 'column',
               style: 'padding:15px; background-color: #a7e14c;',
                     items: [{
					xtype: 'fieldset', title: '',
					border: false,
					columnWidth: 1,
					//style: ' background-color: #32bff4;',
					items: [{
                        xtype: 'fieldset', title: '',
                        height: 320,
                        boxMaxHeight: 320,
                       // width: 1200,
                        border: false,
                        layout: 'vbox',
                        items: [panel_ri_ugd]
                    }]  
				}]
				}]   
             }],
			
    });
	
	function reload_chart(){
		store2.reload({
				params: { 
					tahun: Ext.getCmp('cb_tahun').getValue()
				}
			});
	}function reload_chart2(){
		store_ri_ugd.reload({
				params: { 
					tahun: Ext.getCmp('cb_tahun2').getValue()
				}
			});
	}
	
	
    SET_PAGE_CONTENT(traffict_main);
	
	
	Ext.getCmp('ta.liburan').getToolbar().hide();
}