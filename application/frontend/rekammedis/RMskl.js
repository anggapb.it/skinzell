function RMskl(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 18;
	var rownya = 0;
	var ds_jk = dm_jkelamin();
	var ds_wnegara = dm_wnegara();
	var ds_caralahir = dm_caralahir();
	var ds_kondisilahir = dm_kondisilahir();
	var ds_vregistrasi = dm_pindahruangans();
	ds_vregistrasi.setBaseParam('aa',2);
	var ds_vskl = dm_vskl();
	var ds_vskl2 = dm_vskl2();
	
	var ds_skl = dm_skl();
	var stores = {dmskl:ds_skl};
	
	function keyToView(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	var vw_skl= new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	var vw_skl2= new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	var arr_cari = [['kdskl', 'No SKL'],['noreg', 'No Registrasi'],['nmibu', 'Nama Ibu'],['nmayah', 'Nama Ayah'],['alamat', 'Alamat'],['nmdoktergelar', 'Penolong']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	var cari = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200,
		//	disableIndexes:['idbdgrawat'],
		})];
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_vskl,
		displayInfo: true,
		displayMsg: 'Data Daftar Data Kelahiran Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});	
	
	var grid_anak = new Ext.grid.GridPanel({
		id: 'grid_anak',
		store: ds_vskl2,
		view: vw_skl2,
	
		autoScroll: true,
		anchor: '100%',
		height: 272,
		autoHight: true,
		columnLines: true,
		tbar: [{
			xtype: 'textfield',
			id: 'tf.tamkdskl',
			hidden: true
		}],
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'No. SKL',
			width: 110,
			dataIndex: 'kdskl',
			sortable: true,
		//	renderer: keyToView
		},{
			header: 'No. RM',
			width: 60,
			dataIndex: 'normanak',
			sortable: true,
		//	renderer: keyToView
		},{
			header: 'Nama',
			width: 100	,
			dataIndex: 'nmbayi',
			sortable: true,
		//	renderer: keyToView
		},{
			header: 'Jenis Kelamin',
			width: 90,
			dataIndex: 'nmjnskelamin',
			sortable: true,
			
		},{
			header: 'Tanggal',
			width: 100,
			dataIndex: 'tglkelahiran',
			sortable: true,
			xtype: 'datecolumn',
			format: 'd/m/Y',
			
		},{
			header: 'Jam',
			width: 100,
			dataIndex: 'jamkelahiran',
			sortable: true,
			
		},{
			header: 'Anak Ke',
			width: 60,
			dataIndex: 'anakke',
			sortable: true,
			
		},{
			header: 'Berat',
			width: 55,
			dataIndex: 'beratkelahiran',
			sortable: true,
			
		},{
			header: 'Panjang',
			width: 60,
			dataIndex: 'panjangkelahiran',
			sortable: true,
			
		},{
			header: 'Cara Lahir',
			width: 70,
			dataIndex: 'nmcaralahir',
			sortable: true,
			
		},{
			header: 'Kondisi Lahir',
			width: 80,
			dataIndex: 'nmkonlhr',
			sortable: true,
			
		},{
                xtype: 'actioncolumn',
                width: 80,
				header: 'Cetak Besar',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetaksklasli(grid, rowIndex);
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 80,
				header: 'Cetak Kecil',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {
						cetakskl(grid, rowIndex);
                    }
                }]
        }]
		
	});
	
	var grid_skl = new Ext.grid.GridPanel({
		id: 'grid_skl',
		store: ds_vskl,
		view: vw_skl,
		autoScroll: true,
		plugins: cari,
		anchor: '100%',
		height: 272,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function(){
				fnAddSkl();
				
			}
		},{/* 
			xtype: 'compositefield',
			width : 455,
			
			items: [{
				xtype: 'label', text: 'Search', margins: '4 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.searchh',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				margins: '2 5 0 0',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.searchh').getValue();
							if(cbsearchh != ''){
								Ext.getCmp('cekk').enable();
								Ext.getCmp('cekk').focus();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'cekk',
				width: 200,
				margins: '2 5 0 0',
				disabled: true,
				validator: function(){
					fnSearchgrid();
				}
			}]
		 */}],
		autoHight: true,
		columnLines: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: '',
			width: 90,
			dataIndex: 'idskl',
			hidden: true
		},
		{
			header: 'No Registrasi',
			width: 80,
			dataIndex: 'noreg',
			sortable: true,
			renderer: keyToView
		},{
			header: 'Nama Ibu',
			width: 130,
			dataIndex: 'nmibu',
			sortable: true,
			
		},{
			header: 'Nama Ayah',
			width: 130,
			dataIndex: 'nmayah',
			sortable: true,
			
		},{
			header: 'Alamat',
			width: 150,
			dataIndex: 'alamat',
			sortable: true,
			
		},{
			header: 'Penolong',
			width: 150,
			dataIndex: 'nmdoktergelar',
			sortable: true,
			
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditSKL(grid, rowIndex);
						
						//return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeleteSKL(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			cellclick: onCellClickviewskl
		}
	});
	
//=========================================
function onCellClickviewskl(grid, rowIndex, columnIndex, event) {
		var t = event.getTarget();
		var record = grid.getStore().getAt(rowIndex);
		
		if (t.className == 'keyMasterDetail'){
				var rec_kdskl = record.data['kdskl'];
				var rec_noreg = record.data['noreg'];
				
				Ext.getCmp('tf.tamkdskl').setValue(rec_noreg);
				if(rec_noreg != "null"){
					//ds_podiperpembelian.setBaseParam('nopp', rec_nopp);
					ds_vskl2.reload({
						params: { 
						//	kdskl: rec_kdskl
							noreg: rec_noreg
						}
					});
				}}				
			return true;
	}
//=========================================	
	
	
	var form_skl = new Ext.form.FormPanel({
		id: 'form_skl',
		title: 'Daftar Data Kelahiran Anak',
		width: 900, height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		items:[{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 1,
			layout: 'fit',
			id: 'qwe',
			items: [{
				xtype: 'fieldset',
				layout: 'form',
				id: 'aa',
				height: 287,
				boxMaxHeight: 292,
				items:[grid_skl]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px; margin: -10px 0 -12px 0',
			columnWidth: 1,
			layout: 'fit',
			id: 'abc',
				items:[{
					xtype: 'fieldset',
					title: 'Data Anak',
					layout: 'form',
					id: 'bb',
					height: 287,
					boxMaxHeight: 292,
					items:[grid_anak]
				}]
		}]
		
	});
	SET_PAGE_CONTENT(form_skl);
/* Function */
function fnAddSkl(){
		var grid = grid_skl;
		wEntrySKL(false, grid, null, stores);	
	}

function fnEditSKL(grid, rowIndex){
		var record = grid_skl.getStore().getAt(rowIndex);
		//var record = ds_skl.getAt(rowIndex);
		wEntrySKL(true,grid,record, stores);
} 	

function fnDeleteSKL(grid, rowIndex){
		var record = ds_vskl.getAt(rowIndex);
		
		var url = BASE_URL + 'skl_controller/deleteskl';
		var params = new Object({
						idskl	: record.data['idskl']
					});
		RH.deleteGridRecord(url, params, grid );
	}
function cetakskl(grid, record){
		var record = ds_vskl2.getAt(record);
		var idskl = record.data['idskl'] 
		RH.ShowReport(BASE_URL + 'print/print_skl/skl_pdf/' + idskl);
	}
function cetaksklasli(grid, record){
		var record = ds_vskl2.getAt(record);
		var idskl = record.data['idskl'] 
		RH.ShowReport(BASE_URL + 'print/print_skl_asli/skl_pdf/' + idskl);
	}
	
function wEntrySKL(isUpdate, grid, record, stores){
	var ds_skl = stores.dmskl;
	//ds_skl.setBaseParam('idskl',null);
	var storesObj = {dm_skl:ds_skl};
	
	var winTitle = (isUpdate)?'Keterangan Lahir Anak (Edit)':'Keterangan Lahir Anak(Entry)';
	var ds_kembar = dm_kembar();
	var vw_grid_skldet = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
	
	var idsklklik='';
	var normanak='';
	function render_jk(value) {
		var val = RH.getRecordFieldValue(ds_jk, 'nmjnskelamin', 'idjnskelamin', value);
		return val;
    }
	function render_cl(value) {
		var val = RH.getRecordFieldValue(ds_caralahir, 'nmcaralahir', 'idcaralahir', value);
		return val;
    }
	function render_kl(value) {
		var val = RH.getRecordFieldValue(ds_kondisilahir, 'nmkonlhr', 'idkonlhr', value);
		return val;
    }function render_wn(value) {
		var val = RH.getRecordFieldValue(ds_wnegara, 'kdwn', 'idwn', value);
		return val;
    }
	
	var fnSelectDet = function(combo, record){
		//The ID of THE combo must equals the field (key) to be updated
		field = combo.getId();
		value = record.data[field];
		
		if(isUpdate) {
			Ext.Ajax.request({
				url: BASE_URL + 'skl_controller/update_field_skl',
				params: {                
					idskl	: idsklklik,
					normanak: normanak,
					field	: field,
					value 	: value,
				},
				success: function() {
					ds_skl.reload();
				},
				failure: function() {
					Ext.Msg.alert("Info", "Ubah Data Gagal");
				}
			});
		}
	};
	
	function fnChange(id,value) {
	
		if(isUpdate) {
			Ext.Ajax.request({
				url: BASE_URL + 'skl_controller/update_field_skl',
				params: {                
					idskl	: idsklklik,
					normanak: normanak,
					field	: id,
					value 	: value,
				},
				success: function() {
					ds_skl.reload();
					ds_vskl2.reload();
				},
				failure: function() {
					Ext.Msg.alert("Info", "Ubah Data Gagal");
				}
			});
		}
	}
	
	var grid_skldet = new Ext.grid.EditorGridPanel({
		id: 'gp.grid_skldet',
		store: ds_skl,
		loadMask: true,
		view: vw_grid_skldet,
		height: 250,
		//	pageSize: pageSize,
			clicksToEdit: 1,
			frame: true,
		tbar: [{
				text: 'Tambah',
				id: 'btn_tmb',
				iconCls: 'silk-add',
				handler: function() {
				var dok = Ext.getCmp("tf.dokter").getValue();
				var iddok = Ext.getCmp("tf.iddokter").getValue();
				var kembar = Ext.getCmp("cb.kembar").getValue();
				
				if(dok == "" || dok == null && iddok == "" || iddok == null){
					alert("Dokter Penolong Harap diIsi")
				}else if(kembar == null || kembar == ""){
					alert("Kembar Harap diIsi")
				}else{
				dataAnak(storesObj);
				}
			}
		}],
		columns: [new Ext.grid.RowNumberer(),
			{
				header: '',
				dataIndex: 'idskl',
				sortable: true,
				hidden: true,
				width: 90
			},
			{
				header: 'NO SKL',
				dataIndex: 'kdskl',
				sortable: true,
				width: 90,
				editor:	{
					xtype: 'textfield',
					id: 'kdskl',
					listeners: {
						'change': function(field, newVal, oldVal){
							if(newVal) {
								fnChange('kdskl',newVal)
							}
						}
					}
				}
			},{
				header: 'No. RM',
				dataIndex: 'normanak',
				sortable: true,
				width: 60
			},{
				header: 'Nama',
				dataIndex: 'nmbayi',
				sortable: true,
				width: 100,
				editor:	{
					xtype: 'textfield',
					id: 'nmbayi',
					listeners: {
						'change': function(field, newVal, oldVal){
							if(newVal) {
								fnChange('nmbayi',newVal)
							}
						}
					}
				}
			},{
				header: 'Jenis<br>Kelamin',
				dataIndex: 'idjnskelamin',
				renderer: render_jk,
				editor: RH.getComboEditor('idjnskelamin', false, ds_jk, 'idjnskelamin', 'nmjnskelamin', fnSelectDet),
				sortable: true,
				width: 70
			},{
				header: 'Tanggal',
				dataIndex: 'tglkelahiran',
				xtype: 'datecolumn',
				format: 'd/m/Y',
				sortable: true,
				align: 'right',
				width: 80,
				editor: {
					xtype: 'datefield',
					minValue: '01/01/2006',
					format: 'd/m/Y',
					id: 'tglkelahiran',
					listeners:{
						select: function(field, newValue){
							fnChange('tglkelahiran',newValue)
						}
					}
				}
			},{
				header: '',
				dataIndex: 'htglkelahiran',
				sortable: true,
				align: 'right',
				hidden: true,
				width: 80
			},{
				header: 'Jam',
				dataIndex: 'jamkelahiran',
				sortable: true,
				align: 'center',
				width: 90,
				editor:	{
					xtype: 'textfield',
					id: 'jamkelahiran',
					listeners: {
						'change': function(field, newVal, oldVal){
							if(newVal) {
								fnChange('jamkelahiran',newVal)
							}
						}
					}
				}
			},{
				header: 'Anak Ke',
				dataIndex: 'anakke',
				sortable: true,
				align: 'center',
				width: 80,
				editor:	{
					xtype: 'textfield',
					id: 'anakke',
					listeners: {
						'change': function(field, newVal, oldVal){
							//if(newVal) {
								fnChange('anakke',newVal)
							//}
						}
					}
				}
			},{
				header: 'Berat',
				dataIndex: 'beratkelahiran',
				sortable: true,
				width: 80,
				editor:	{
					xtype: 'numericfield',
					id: 'beratkelahiran',
					listeners: {
						'change': function(field, newVal, oldVal){
							if(newVal) {
								fnChange('beratkelahiran',newVal)
							}
						}
					}
				}
			},{
				header: 'Panjang',
				dataIndex: 'panjangkelahiran',
				sortable: true,
				width: 80,
				editor:	{
					xtype: 'textfield',
					id: 'panjangkelahiran',
					listeners: {
						'change': function(field, newVal, oldVal){
							if(newVal) {
								fnChange('panjangkelahiran',newVal)
							}
						}
					}
				}
			},{
				header: 'Cara Lahir',
				dataIndex: 'idcaralahir',
				renderer: render_cl,
				editor: RH.getComboEditor('idcaralahir', false, ds_caralahir, 'idcaralahir', 'nmcaralahir', fnSelectDet),
				sortable: true,
				width: 70
			},{
				header: 'Cara Lahir',
				dataIndex: 'nmcaralahir',
				sortable: true,
				hidden: true,
				width: 90
			},{
				header: 'Kondisi<br>Lahir',
				dataIndex: 'idkonlhr',
				renderer: render_kl,
				editor: RH.getComboEditor('idkonlhr', false, ds_kondisilahir, 'idkonlhr', 'nmkonlhr', fnSelectDet),
				sortable: true,
				align: 'center',
				width: 70
			},{
				header: 'Kondisi Lahir',
				dataIndex: 'nmkonlhr',
				sortable: true,
				hidden: true,
				width: 90
			},{
				header: 'alamat',
				dataIndex: 'alamat',
				sortable: true,
				hidden: true,
				width: 90
			} ,{
				header: 'kebangsaan',
				dataIndex: 'idwn',
				renderer: render_wn,
				editor: RH.getComboEditor('idwn', false, ds_wnegara, 'idwn', 'kdwn', fnSelectDet),
				sortable: true,
				hidden: true,
				width: 70
			},{
				header: 'negara',
				dataIndex: 'negara',
				sortable: true,
				hidden: true,
				width: 90
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							if (!isUpdate) {
								var record = ds_skl.getAt(rowIndex);
								Ext.Ajax.request({
									url: BASE_URL + 'skl_controller/deleteskl',
									params: {
										idskl	: record.data['idskl']
									},
									success: function(response){
										ds_skl.removeAt(rowIndex);
									},
									failure : function(){
										Ext.MessageBox.alert('Informasi', 'Hapus Data Gagal');
									}
								});
							}
						}
					}]
			}],
			listeners: {
				cellclick: onCellSKl			
			}
		});
		
	function onCellSKl(grid, rowIndex, columnIndex, e) {
		var record = ds_skl.getAt(rowIndex);
		idsklklik =  record.data['idskl'];
		normanak =  record.data['normanak'];
		//alert(idsklklik);
		        
    }
	 var skldet_form = new Ext.form.FormPanel({
			xtype: 'form',
			id: 'frm.skldet',
			buttonAlign: 'left',
			//autoScroll: true,
			labelWidth: 150, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 549, width: 930,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			tbar: [{
				id:'btn_simpan',text: 'Simpan', iconCls:'silk-save', style: 'marginLeft: 5px',
				handler: function() {
					simpan();
					}
				}, 
				{
					text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
					handler: function() {
					//	Ext.getCmp('tf.searchppdet').setValue();
						wSKL.close();
						//ds_vskl.reload();
					//	ds_skldet.reload();
				}
			}],
			frame: false,
			items: [{
				xtype: 'panel', layout:'fit', height: 230,
				title:'', id:'fp.wpp', frame:true,
				items:[{
					xtype: 'fieldset', title: 'Keterangan Lahir Anak', layout: 'column',
				items: [{
					columnWidth: 0.45, border: false, layout: 'form',					
					items: [{
					xtype: 'compositefield',
					fieldLabel: 'No. Registrasi',
					id: 'comp_norm',
					items: [{
						xtype: 'textfield',
						id: 'tf.noreg',
						width: 80,
					//	maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
						enableKeyEvents: true,
						listeners:{
							specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									dataPasien();
								}
							}
						}
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.norm',
						width: 30,
						handler: function() {
							dftPasien();
						}
					}]
				},{
						xtype: 'textfield', fieldLabel: 'Nama Ibu', 
						id:'tf.nmibu', width: 200, 
				},{
						xtype: 'textfield', fieldLabel: '', 
						id:'tf.idskl', width: 200, hidden: true
				},{
					xtype: 'container', fieldLabel: 'Tanggal Lahir Ibu',
					layout: 'hbox',
					items: [{ 	
						xtype: 'datefield', id: 'df.tgllahiribu',
						width: 100, value: new Date(), allowBlank: false,
						maxValue:new Date(),
						format: 'd-m-Y',
						listeners:{
							select: function(field, newValue){
								umur(newValue);
							},
							change : function(field, newValue){
								umur(newValue);
							}
						}
					}]
				},{
					xtype: 'container', fieldLabel: 'Umur',
					layout: 'hbox',
					items: [{
						xtype: 'textfield', id: 'tf.umurthn', readOnly:true,
						width: 30
					},{
						xtype: 'label', id: 'lb.umurthn', text: 'Tahun', margins: '0 10 0 5',
					},{ 	
						xtype: 'textfield', id: 'tf.umurbln',  readOnly:true,
						width: 30
					},{
						xtype: 'label', id: 'lb.umurbln', text: 'Bulan', margins: '0 10 0 5',
					},{
						xtype: 'textfield', id: 'tf.umurhari',  readOnly:true,
						width: 30
					},{
						xtype: 'label', id: 'lb.umurhari', text: 'Hari', margins: '0 10 0 5'
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Nama Ayah', 
					id:'tf.nmayah', width: 200
				},{
					xtype: 'container', fieldLabel: 'Tanggal Lahir Ayah',
					layout: 'hbox',
					items: [{ 	
						xtype: 'datefield', id: 'df.tgllahirayah',
						width: 100, value: new Date(), allowBlank: false,
						maxValue:new Date(),
						format: 'd-m-Y',
						listeners:{
							select: function(field, newValue){
								umurayah(newValue);
							},
							change : function(field, newValue){
								umurayah(newValue);
							}
						}
					}]
				},{
					xtype: 'container', fieldLabel: 'Umur',
					layout: 'hbox',
					items: [{
						xtype: 'textfield', id: 'tf.umurthna', readOnly:true,
						width: 30
					},{
						xtype: 'label', id: 'lb.umurthna', text: 'Tahun', margins: '0 10 0 5',
					},{ 	
						xtype: 'textfield', id: 'tf.umurblna',  readOnly:true,
						width: 30
					},{
						xtype: 'label', id: 'lb.umurblna', text: 'Bulan', margins: '0 10 0 5',
					},{
						xtype: 'textfield', id: 'tf.umurharia',  readOnly:true,
						width: 30
					},{
						xtype: 'label', id: 'lb.umurharia', text: 'Hari', margins: '0 10 0 5'
					}]
				}]
			},{
				columnWidth: 0.55, border: false, layout: 'form',
				items:[{
					xtype: 'compositefield',
							fieldLabel: 'Dokter Penolong',
							width: 290,
							items: [{
								xtype: 'textfield', id: 'tf.dokter',
								width: 260,
							},{
								xtype: 'textfield', id: 'tf.iddokter',
								width: 260,hidden: true 
							},{
								xtype: 'button',
								iconCls: 'silk-find',
								id: 'btn.dokter',
								width: 4,
								handler: function() {
									dftDokter();
								}
							}]
				},{
					xtype:'textarea', fieldLabel: 'Alamat',
					id: 'ta.alamat', width: 260, height: 70
				},{
					xtype: 'combo',
					store: ds_kembar,
					id: 'cb.kembar',
					triggerAction: 'all',
					editable: false,
					valueField: 'idkembar',
					displayField: 'nmkembar',
					forceSelection: true,
					submitValue: true,
					typeAhead: true,
					mode: 'local',
					fieldLabel: 'Kembar',
					emptyText:'Pilih...',
					selectOnFocus:true,
					width: 130,
					value: "Tunggal",
					margins: '2 5 0 0',
					listeners: {
						select: function() {
							
						}
					}
				},{
					xtype: 'textfield', fieldLabel: 'User Input',
					width: 100, id: 'tf.userid', hidden: true
				}]
			}]
		}]
	},{
			xtype: 'fieldset',
				title: 'Data Anak',
				layout: 'form',
				items: [grid_skldet]
	}]
			
		}); 
		
	
	if (isUpdate) {
			Ext.getCmp('tf.idskl').setValue(record.data['idskl']);
			Ext.getCmp('tf.noreg').setValue(record.data['noreg']);
			Ext.getCmp('tf.nmayah').setValue(record.data['nmayah']);	
			Ext.getCmp('tf.nmibu').setValue(record.data['nmibu']);
			Ext.getCmp('ta.alamat').setValue(record.data['alamat']);
			Ext.getCmp('df.tgllahirayah').setValue(record.data['tgllahirayah']);
			Ext.getCmp('tf.umurthna').setValue(record.data['thnayah']);
			Ext.getCmp('tf.umurblna').setValue(record.data['blnayah']);
			Ext.getCmp('tf.umurharia').setValue(record.data['tglayah']);
			Ext.getCmp('df.tgllahiribu').setValue(record.data['tgllahiribu']);	
			Ext.getCmp('tf.umurthn').setValue(record.data['thnibu']);	
			Ext.getCmp('tf.umurbln').setValue(record.data['blnibu']);	
			Ext.getCmp('tf.umurhari').setValue(record.data['tglibu']);
			Ext.getCmp('tf.iddokter').setValue(record.data['iddokter']);
			Ext.getCmp('tf.dokter').setValue(record.data['nmdoktergelar']);
			Ext.getCmp('cb.kembar').setValue(record.data['idkembar']);
		
			ds_skl.setBaseParam('noreg',record.data['noreg']);
			
			ds_skl.load();
			
			Ext.getCmp('btn_tmb').disable();
	} else {
	
			ds_skl.setBaseParam('noreg','-');
			
			ds_skl.load();
	}
	var wSKL = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [skldet_form]
		}).show();	
		
		function simpan(){			
			var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
			var arrskl = [];
			if(isUpdate == true){
				Ext.Ajax.request({
				url: BASE_URL + 'skl_controller/updateSkl',
				params: {
					idskl : Ext.getCmp('tf.idskl').getValue(),
					noreg : Ext.getCmp('tf.noreg').getValue(),
					nmayah : Ext.getCmp('tf.nmayah').getValue(),	
					tgllahirayah : Ext.getCmp('df.tgllahirayah').getValue(),	
					thnayah : Ext.getCmp('tf.umurthna').getValue(),	
					blnayah : Ext.getCmp('tf.umurblna').getValue(),	
					tglayah : Ext.getCmp('tf.umurharia').getValue(),
					tgllahiribu : Ext.getCmp('df.tgllahiribu').getValue(),	
					thnibu : Ext.getCmp('tf.umurthn').getValue(),	
					blnibu : Ext.getCmp('tf.umurbln').getValue(),	
					tglibu : Ext.getCmp('tf.umurhari').getValue(),
					iddokter: Ext.getCmp('tf.iddokter').getValue(),
					kembar: Ext.getCmp('cb.kembar').getValue(),
					arrskl : Ext.encode(arrskl)
				},
				
					success: function(response){
						waitmsg.hide();
						Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
						obj = Ext.util.JSON.decode(response.responseText);
						wSKL.close();
						ds_vskl.load();
						ds_vskl2.load();
						},
					failure : function(){
						waitmsg.hide();
						Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
					}
				});
			}else{
				for( var zx = 0; zx < ds_skl.data.items.length; zx++){
				var record = ds_skl.data.items[zx].data;
				zkdskl = (record.kdskl) ? record.kdskl:null;
				znmbayi = (record.nmbayi) ? record.nmbayi:null;
				zjk		= (record.idjnskelamin) ? record.idjnskelamin:null;
				ztgllahir = (record.tglkelahiran) ? new Date(record.tglkelahiran):null;
				zjamlahir = (record.jamkelahiran) ? record.jamkelahiran:null;
				zanakke = (record.anakke) ? record.anakke:null;
				zberat = (record.beratkelahiran) ? record.beratkelahiran:null;
				zpanjang = (record.panjangkelahiran) ? record.panjangkelahiran:null;
				zcaralahir = (record.idcaralahir) ? record.idcaralahir:null;
				zkondisilahir = (record.idkonlhr) ? record.idkonlhr:null;
				zalamat = (record.alamat) ? record.alamat:null;
				zidwn = (record.idwn) ? record.idwn:null;
				znegara = record.negara;
				arrskl[zx] = zkdskl + '+' + znmbayi + '+' + zjk + '+' + ztgllahir.format('Y/m/d') + '+' + zjamlahir +'+' + zanakke + '+' + zberat + '+' + zpanjang + '+' + zcaralahir + '+' + zkondisilahir + '+' + zalamat + '+' + zidwn + '+' + znegara;
				
			}
			if(Ext.getCmp('cb.kembar').getValue() == "Tunggal"){
				var kembar = 1;
			}else{
				var kembar = Ext.getCmp('cb.kembar').getValue();
			}
			Ext.Ajax.request({
				url: BASE_URL + 'skl_controller/simpanskl',
				params: {
					noreg : Ext.getCmp('tf.noreg').getValue(),
					nmayah : Ext.getCmp('tf.nmayah').getValue(),	
					tgllahirayah : Ext.getCmp('df.tgllahirayah').getValue(),	
					thnayah : Ext.getCmp('tf.umurthna').getValue(),	
					blnayah : Ext.getCmp('tf.umurblna').getValue(),	
					tglayah : Ext.getCmp('tf.umurharia').getValue(),
					tgllahiribu : Ext.getCmp('df.tgllahiribu').getValue(),	
					thnibu : Ext.getCmp('tf.umurthn').getValue(),	
					blnibu : Ext.getCmp('tf.umurbln').getValue(),	
					tglibu : Ext.getCmp('tf.umurhari').getValue(),
					iddokter: Ext.getCmp('tf.iddokter').getValue(),
					kembar: kembar,
					arrskl : Ext.encode(arrskl)
				},
				
					success: function(response){
						waitmsg.hide();
						Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
						obj = Ext.util.JSON.decode(response.responseText);
						wSKL.close();
						ds_vskl.load();
						},
					failure : function(){
						waitmsg.hide();
						Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
					}
				});
			}
			
		}
	}

function dataAnak(storesObj){
	var valamat = Ext.getCmp("ta.alamat").getValue();

var ds_skl = storesObj.dm_skl;    
	
	var rec_skl= Ext.data.Record.create([{
        name: 'kdskl',
		name: 'nmbayi',
		name: 'idjnskelamin',
		name: 'tglkelahiran',
		name: 'anakke',
		name: 'beratkelahiran',
		name: 'panjangkelahiran',
		name: 'idcaralahir',
		name: 'idkonlhr',
		name: 'alamat',
		name: 'idwn',
		name: 'negara',
    }]);
	
	var dataAnak_form = new Ext.form.FormPanel({
			xtype: 'form',
			id: 'frm.skldet',
			buttonAlign: 'left',
			autoScroll: false,
			//labelWidth: 120, labelAlign: 'right',
		//	bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 300, width: 700,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			tbar: [{
				id:'btn_simpana',text: 'Simpan', iconCls:'silk-save', style: 'marginLeft: 5px',
				handler: function() {
						var kdskl= Ext.getCmp('tf.noskl').getValue();
						var nmbayi= Ext.getCmp('tf.nmbayi').getValue();
						var idjnskelamin = Ext.getCmp('cb.jk').getValue();
						var nmjnskelamin = Ext.getCmp('cb.jk').getRawValue();
						
						var tglkelahiran = Ext.getCmp('df.tgllahibayi').getValue();
						var htglkelahiran = Ext.getCmp('df.tgllahibayi').getValue();//.format('d-m-Y');
						var jamkelahiran = Ext.getCmp('tf.jamlahir').getValue();
						var anakke = Ext.getCmp('tf.anakke').getValue();
						var beratkelahiran= Ext.getCmp('tf.berat').getValue();
						var panjangkelahiran = Ext.getCmp('tf.panjang').getValue();
						var idcaralahir = Ext.getCmp('cb.caralahir').getValue();
						var idkonlhr = Ext.getCmp('cb.kondisilahir').getValue();
						var nmcaralahir = Ext.getCmp('cb.caralahir').getRawValue();
						var nmkonlhr = Ext.getCmp('cb.kondisilahir').getRawValue();
						var alamat = Ext.getCmp('tf.alamat').getRawValue();
						var idwn = Ext.getCmp('cb.wn').getValue();
						var negara = Ext.getCmp('tf.negara').getRawValue();
						ds_skl.add(new rec_skl({
							kdskl: kdskl, 
							nmbayi:nmbayi,
							idjnskelamin: idjnskelamin,
							nmjnskelamin: nmjnskelamin,
							tglkelahiran: tglkelahiran,
							htglkelahiran: htglkelahiran,
							anakke: anakke,
							beratkelahiran: beratkelahiran,
							panjangkelahiran: panjangkelahiran,
							idcaralahir: idcaralahir,
							idkonlhr: idkonlhr,
							jamkelahiran : jamkelahiran,
							nmcaralahir : nmcaralahir,
							nmkonlhr : nmkonlhr,
							alamat : alamat,
							idwn : idwn,
							negara : negara,
						}));
						asd.close();
					}
				}, 
				{
					text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
					handler: function() {
						asd.close();
						ds_skl.reload();
				}
			}],
			frame: false,
			
		items:[{
				xtype: 'panel', layout:'fit', height: 270,
				title:'', id:'fp.wpp', frame:true,
				items:[{
					xtype: 'fieldset', title: 'Data Anak Baru Lahir', layout: 'column',
				items: [{
					columnWidth: 0.55, border: false, layout: 'form', labelWidth: 120, labelAlign: 'right',				
					items: [{
						xtype: 'textfield', fieldLabel: 'NO. SKL', 
						id:'tf.noskl', width: 100, hidden: true 
						
					},{
						xtype: 'textfield', fieldLabel: 'NO. RM', 
						id:'tf.norm', width: 80, hidden: true 
						
					},{
						xtype: 'textfield', fieldLabel: 'Nama Anak', 
						id:'tf.nmbayi', width: 100, 
					
					},{
						xtype: 'combo', id: 'cb.jk', 
						fieldLabel: 'Jenis Kelamin',
						store: ds_jk, triggerAction: 'all',
						valueField: 'idjnskelamin', displayField: 'nmjnskelamin',
						forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', width: 100,
						editable: false,
					},{
						xtype: 'container', fieldLabel: 'Tanggal Lahir',
						layout: 'hbox',
						items: [{ 	
							xtype: 'datefield', id: 'df.tgllahibayi',
							value: new Date(), 
							width: 100,
							format: 'd-m-Y',
							
						}]
					},{
						xtype: 'textfield', fieldLabel: 'Jam Lahir', 
						id:'tf.jamlahir', width: 100
					},{
						xtype: 'textarea', fieldLabel: 'Alamat',
						id: 'tf.alamat', value: valamat,
						width: 200
					}]
				},{
					columnWidth: 0.45, border: false, layout: 'form', labelWidth: 120, labelAlign: 'right',
					items:[{
						xtype: 'combo',
						id: 'cb.wn', width: 100, fieldLabel: 'Kebangsaan',
						store: ds_wnegara, valueField: 'idwn', displayField: 'kdwn',
						editable: false, triggerAction: 'all',
						forceSelection: true, submitValue: true, mode: 'remote',
						emptyText:'Pilih...', value: 1,
						listeners:{
							select:function(combo, records, eOpts){
								if(records.get('idwn') != 1)
								{
									Ext.getCmp('tf.negara').setReadOnly(false);
									Ext.getCmp('tf.negara').el.setStyle('opacity', 1);
								} else {
									Ext.getCmp('tf.negara').setReadOnly(true);
									Ext.getCmp('tf.negara').setValue('');
									Ext.getCmp('tf.negara').el.setStyle('opacity', 0.6);
								}
							}
						}
					},{
						xtype: 'textfield', fieldLabel : 'Negara',
						id  : 'tf.negara', width : 100,
						readOnly: true, style : 'opacity:0.6'
					},{
						xtype: 'textfield', fieldLabel: 'Anak Ke', width: 100, id:'tf.anakke'
					},{
						xtype: 'container', fieldLabel: 'Berat',
						layout: 'hbox',
						items: [{
							xtype: 'textfield', id: 'tf.berat',	width: 30
						},{
							xtype: 'label', id: 'lb.kg', text: 'Kg', margins: '0 10 0 5',
						}]
							
					},{
						xtype: 'container', fieldLabel: 'Panjang',
						layout: 'hbox',
						items: [{
							xtype: 'textfield', id: 'tf.panjang',	width: 30
						},{
							xtype: 'label', id: 'lb.kg', text: 'Cm', margins: '0 10 0 5',
						}]
							
					},{
						xtype: 'combo', id: 'cb.caralahir', 
						fieldLabel: 'Cara Lahir',
						store: ds_caralahir, triggerAction: 'all',
						valueField: 'idcaralahir', displayField: 'nmcaralahir',
						forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', width: 100,
						editable: false,
					},{
						xtype: 'combo', id: 'cb.kondisilahir', 
						fieldLabel: 'Kondisilahir',
						store: ds_kondisilahir, triggerAction: 'all',
						valueField: 'idkonlhr', displayField: 'nmkonlhr',
						forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', width: 100,
						editable: false,
					}]
				}]
			}]
		}]
	});
	    var asd = new Ext.Window({
        title: "Data Anak Baru Lahir",
        modal: true, closable:false,
        items: [dataAnak_form]
    });
asd.show();
}


function dataPasien(){
		Ext.Ajax.request({
			url: BASE_URL + 'skl_controller/getDataReg',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue()
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				var var_noreg = obj.noreg;
				
				Ext.getCmp('tf.noreg').focus()
				Ext.getCmp("tf.noreg").setValue(parseInt(var_cari_pasienno));
				
		}
	});
}


function dftPasien(){

		
		function keyToView_pasien(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterPasien" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_cari_pasien = new Ext.grid.ColumnModel([
			{
				header: 'No Registrasi',
				dataIndex: 'noreg',
				width: 80,
				renderer: keyToView_pasien
			},{
				header: 'Nama Pasien',
				dataIndex: 'nmpasien',
				width: 150
			},{
				header: 'L/P',
				dataIndex: 'kdjnskelamin',
				width: 30
			},{
				header: 'Tgl. Lahir',
				dataIndex: 'tgllahirp',
				renderer: Ext.util.Format.dateRenderer('d-m-Y'),
				width: 80
			},{
				header: 'Alamat Pasien',
				dataIndex: 'alamatp',
				width: 210
			},{
				header: 'Nama Ibu',
				dataIndex: 'nmibu',
				width: 120
			},{
				header: 'No. HP/Telp.',
				dataIndex: 'notelpp',
				renderer: function(value, p, r){
					var nohptelp = r.data['nohpp'] + ' / ' + r.data['notelpp'];
					
					return nohptelp ;
				},
				width: 180
			}
		]);
		var sm_cari_pasien = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_pasien = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_pasien = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_vregistrasi,
			displayInfo: true,
			displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_noreg = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_pasien = new Ext.grid.GridPanel({
			ds: ds_vregistrasi,
			cm: cm_cari_pasien,
			sm: sm_cari_pasien,
			view: vw_cari_pasien,
			height: 350,
			width: 875,
			plugins: cari_noreg,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_pasien,
			listeners: {
				cellclick: klik_cari_pasien
			}
		});
		var win_find_cari_pasien = new Ext.Window({
			title: 'Cari Registrasi',
			modal: true,
			items: [grid_find_cari_pasien]
		}).show();

		function klik_cari_pasien(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterPasien'){
	
				var rec_cari_pasien = ds_vregistrasi.getAt(rowIdx);
				var var_cari_pasienno = rec_cari_pasien.data["noreg"];
				var var_cari_nmibu = rec_cari_pasien.data["nmpasien"];
				var var_cari_alamat = rec_cari_pasien.data["alamatp"];
				var var_cari_tgl = rec_cari_pasien.data["tgllahirp"];
				var var_cari_nmkerabat = rec_cari_pasien.data["nmibu"];
					
				Ext.getCmp('tf.noreg').focus()
				Ext.getCmp("tf.noreg").setValue(var_cari_pasienno);
				Ext.getCmp("tf.nmibu").setValue(var_cari_nmibu);
				Ext.getCmp("ta.alamat").setValue(var_cari_alamat);
				Ext.getCmp("df.tgllahiribu").setValue(var_cari_tgl);
				Ext.getCmp("tf.nmayah").setValue(var_cari_nmkerabat);
				umur(new Date(var_cari_tgl));
			win_find_cari_pasien.close();
			
			}
		}
		
	}

function dftDokter(){
 var ds_dokter = dm_dokter();
		function keyToView_pasien(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDokter" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_cari_dokter = new Ext.grid.ColumnModel([
			{
				header: 'Kode',
				dataIndex: 'kddokter',
				width: 80,
				renderer: keyToView_pasien
			},{
				header: 'Dokter',
				dataIndex: 'nmdoktergelar',
				width: 200,
			}
		]);
		var sm_cari_pasien = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_pasien = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_pasien = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_dokter,
			displayInfo: true,
			displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_noreg = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_pasien = new Ext.grid.GridPanel({
			ds: ds_dokter,
			cm: cm_cari_dokter,
			sm: sm_cari_pasien,
			view: vw_cari_pasien,
			height: 350,
			width: 290,
			plugins: cari_noreg,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_pasien,
			listeners: {
				cellclick: klik_cari_pasien
			}
		});
		var win_find_cari_pasien = new Ext.Window({
			title: 'Cari Dokter',
			modal: true,
			items: [grid_find_cari_pasien]
		}).show();

		function klik_cari_pasien(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterDokter'){
	
				var rec_cari_pasien = ds_dokter.getAt(rowIdx);
				var var_cari_pasienno = rec_cari_pasien.data["nmdoktergelar"];
				var var_cari_iddokter = rec_cari_pasien.data["iddokter"];
			//	alert(var_cari_iddokter);
				Ext.getCmp('tf.dokter').focus()
				Ext.getCmp("tf.dokter").setValue(var_cari_pasienno);
				Ext.getCmp("tf.iddokter").setValue(var_cari_iddokter);
			//	umur(new Date(var_cari_pasientgl));
				win_find_cari_pasien.close();
			
			}
		}
		
	}


	
function umur(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tf.umurthn').setValue(year);
		Ext.getCmp('tf.umurbln').setValue(month);
		Ext.getCmp('tf.umurhari').setValue(day);
	}
function umurayah(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tf.umurthna').setValue(year);
		Ext.getCmp('tf.umurblna').setValue(month);
		Ext.getCmp('tf.umurharia').setValue(day);
	}
//end function
}