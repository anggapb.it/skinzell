function RMpindahruangan(){
	var ds_pindahruangans = dm_pindahruangans();
	var ds_pindahruangan = dm_pindahruangan();		
	var pageSize = 18;
	var vw_pindah = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
	//	store: ds_vskl,
		displayInfo: true,
		displayMsg: 'Data Pindah Ruangan Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});	
	var grid_ruangan = new Ext.grid.GridPanel({
		id: 'grid_ruangan',
		store: ds_pindahruangan,
	//	view: vw_skl,
		autoScroll: true,
	//	plugins: cari,
		loadMask: true,
		anchor: '100%',
		height: 300,
		tbar: [{
			text: 'Pindah',
			id: 'btn_pindah',
			iconCls: 'silk-add',
			disabled: true,
			handler: function(){
			
				fnAddRuangan(Ext.getCmp('tf.noreg').getValue());
			}
		}],
		autoHight: true,
		columnLines: true,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: '',
			width: 90,
			dataIndex: 'noreg',
			hidden: true
		},
		{
			header: 'Kamar / Bed',
			width: 130,
			dataIndex: 'nmkamar',
			sortable: true,
			renderer: function(value, p, r){
					var nohptelp = r.data['nmkamar'] + ' / ' + r.data['nmbed'];
					
					return nohptelp ;
				},
		},{
			header: 'Kelas Tarif',
			width: 130,
			dataIndex: 'nmklstarif',
			sortable: true,
			align: 'center	'
			
		},{
			header: 'Masuk',
			width: 150,
			dataIndex: 'tglmasuk',
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: 'Keluar',
			width: 150,
			dataIndex: 'tglkeluar',
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: 'Hari',
			width: 90,
			dataIndex: 'intvday',
			sortable: true,
			
		}]
	});
	
	var form_pindah = new Ext.form.FormPanel({
		xtype: 'form',
		id: 'frm.pindah',
		buttonAlign: 'left',
		labelWidth: 150, labelAlign: 'right',
	//	bodyStyle: 'padding: 10px 3px 3px 5px',
		monitorValid: true,
		height: 594, width: 930,
		title: 'Pindah Ruangan',
		layout: {
			type: 'form',
			pack: 'center',
			align: 'center',
		},
		tbar: [{
			id:'btn_simpan',text: 'Baru', iconCls:'silk-add', style: 'marginLeft: 5px',
			handler: function() {
			baru();
			}
		}],
		frame: true,
		items:[{
			xtype: 'panel', layout: 'fit', height: 96,
			title: '', id: 'fp.wa',// frame: true,
			items:[{
				xtype: 'fieldset', title: '', layout: 'column',
				items: [{
					columnWidth: 0.45, border: false, layout: 'form',
					items: [{
					xtype: 'compositefield',
					fieldLabel: 'No. Registrasi',
					id: 'comp_norm',
					items: [{
						xtype: 'textfield',
						id: 'tf.noreg',
						width: 80,
						readOnly: true,
					//	maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
						enableKeyEvents: true,
						
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.norm',
						width: 30,
						handler: function() {
							dftPasien();
						}
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Nama Ibu', 
					id:'tf.nmibu', width: 200, readOnly: true 
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.haha', width: 200,hidden: true
				},{
					xtype: 'datefield', id: 'df.tglreg',
					width: 100, value: new Date(),
					format: 'd-m-Y',hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.jamreg', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idshift', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idbagiankirim', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.iddokterkirim', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.nmdokterkirim', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idbagian', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.iddokter', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idcaradatang', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idbed', width: 200,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idklstarif', width: 200,hidden: true
				},{
					xtype: 'datefield', id: 'df.tglmasuk',
					width: 100, value: new Date(),
					format: 'd-m-Y',hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.jammasuk',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.tglmasuk',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.tglkeluar',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.jamkeluar',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.catatankeluar',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.umurtahun',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.umurbulan',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.umurhari',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idklsrawat',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idkamar',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idstkeluar',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.nooruangan',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idregistrasi',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.userinput',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.userbatal',width: 100,hidden: true
				},{
					xtype: 'datefield', id: 'df.tglbatal',
					width: 100, value: new Date(),
					format: 'd-m-Y',hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.catatanrencanakeluar',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idcarakeluar',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idcarakeluar',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.nmkamara',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.nmbeda',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.nmklstarifa',width: 100,hidden: true
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.normnya',width: 100,hidden: true
				}]
				},{
					columnWidth: 0.55, border: false, layout: 'form',
					items: [{
							xtype: 'textfield',id: 'tf.dokter', width: 200,
							fieldLabel: 'Dokter',readOnly: true
					},{
							xtype: 'textfield',id: 'tf.penjamin', width: 200,
							fieldLabel: 'Penjamin',readOnly: true
					}]
				}]
			}]
		},{
			
			items:[{
				xtype: 'fieldset',
				title: '',
				layout: 'form',
				items: [grid_ruangan]
			}]
			
				
		}]
		
	});
	SET_PAGE_CONTENT(form_pindah);
	
//=======================================
	function baru(){
		Ext.getCmp("tf.noreg").setValue('');
				Ext.getCmp("tf.haha").setValue('');
				Ext.getCmp("tf.nmibu").setValue('');
				Ext.getCmp("tf.dokter").setValue('');
				Ext.getCmp("tf.penjamin").setValue('');
				Ext.getCmp("df.tglreg").setValue('');
				Ext.getCmp("tf.jamreg").setValue('');
				Ext.getCmp("tf.idshift").setValue('');
				Ext.getCmp("tf.idbagiankirim").setValue('');
				Ext.getCmp("tf.iddokterkirim").setValue('');
				Ext.getCmp("tf.nmdokterkirim").setValue('');
				Ext.getCmp("tf.idbagian").setValue('');
				Ext.getCmp("tf.iddokter").setValue('');
				Ext.getCmp("tf.idcaradatang").setValue('');
				Ext.getCmp("tf.idbed").setValue('');
				Ext.getCmp("tf.idklstarif").setValue('');
				Ext.getCmp("tf.tglmasuk").setValue('');
				Ext.getCmp("tf.jammasuk").setValue('');
				Ext.getCmp("tf.tglmasuk").setValue('');
				Ext.getCmp("tf.tglkeluar").setValue('');
				Ext.getCmp("tf.jamkeluar").setValue('');
				Ext.getCmp("tf.catatankeluar").setValue('');
				Ext.getCmp("tf.umurtahun").setValue('');
				Ext.getCmp("tf.umurbulan").setValue('');
				Ext.getCmp("tf.umurhari").setValue('');
				Ext.getCmp("tf.idklsrawat").setValue('');
				Ext.getCmp("tf.idkamar").setValue('');
				Ext.getCmp("tf.idstkeluar").setValue('');
				Ext.getCmp("tf.nooruangan").setValue('');
				Ext.getCmp("tf.idregistrasi").setValue('');
				Ext.getCmp("tf.userinput").setValue('');
				Ext.getCmp("df.tglbatal").setValue('');
				Ext.getCmp("tf.catatanrencanakeluar").setValue('');
				Ext.getCmp("tf.idcarakeluar").setValue('');
				Ext.getCmp("tf.nmkamara").setValue('');
				Ext.getCmp("tf.nmbeda").setValue('');
				Ext.getCmp("tf.nmklstarifa").setValue('');
				Ext.getCmp('btn_pindah').disable();
				ds_pindahruangan.setBaseParam('noreg','');
				ds_pindahruangan.reload();
				
	}
	
	function dftPasien(){

		
		function keyToView_pasien(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterPasien" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_cari_pasien = new Ext.grid.ColumnModel([
			{
				header: 'No Registrasi',
				dataIndex: 'noreg',
				width: 80,
				renderer: keyToView_pasien
			},{
				header: 'Nama Pasien',
				dataIndex: 'nmpasien',
				width: 150
			},{
				header: 'L/P',
				dataIndex: 'kdjnskelamin',
				width: 30
			},{
				header: 'Tgl. Lahir',
				dataIndex: 'tgllahirp',
				renderer: Ext.util.Format.dateRenderer('d-m-Y'),
				width: 80
			},{
				header: 'Alamat Pasien',
				dataIndex: 'alamatp',
				width: 210
			},{
				header: 'Nama Ibu',
				dataIndex: 'nmibu',
				width: 120
			},{
				header: 'No. HP/Telp.',
				dataIndex: 'notelpp',
				renderer: function(value, p, r){
					var nohptelp = r.data['nohpp'] + ' / ' + r.data['notelpp'];
					
					return nohptelp ;
				},
				width: 180
			}
		]);
		var sm_cari_pasien = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_pasien = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_pasien = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_pindahruangans,
			displayInfo: true,
			displayMsg: 'Data Pasien Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_noreg = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_pasien = new Ext.grid.GridPanel({
			ds: ds_pindahruangans,
			cm: cm_cari_pasien,
			sm: sm_cari_pasien,
			view: vw_cari_pasien,
			height: 350,
			width: 875,
			plugins: cari_noreg,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_pasien,
			listeners: {
				cellclick: klik_cari_pasien
			}
		});
		var win_find_cari_pasien = new Ext.Window({
			title: 'Cari Registrasi',
			modal: true,
			items: [grid_find_cari_pasien]
		}).show();

		function klik_cari_pasien(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterPasien'){
	
				var rec_cari_pasien = ds_pindahruangans.getAt(rowIdx);
				var var_noreg = rec_cari_pasien.data["noreg"];
				var var_tglreg = rec_cari_pasien.data["tglreg"];
				var var_jamreg = rec_cari_pasien.data["jamreg"];
				var var_idshift = rec_cari_pasien.data["idshift"];
				var var_idbagiankirim = rec_cari_pasien.data["idbagiankirim"];
				var var_iddokterkirim = rec_cari_pasien.data["iddokterkirim"];
				var var_nmdokterkirim = rec_cari_pasien.data["nmdokterkirim"];
				var var_idbagian = rec_cari_pasien.data["idbagian"];
				var var_iddokter = rec_cari_pasien.data["iddokter"];
				var var_idcaradatang = rec_cari_pasien.data["idcaradatang"];
				var var_idbed = rec_cari_pasien.data["idbed"];
				var var_idklstarif = rec_cari_pasien.data["idklstarif"];
				var var_tglmasuk = rec_cari_pasien.data["tglmasuk"];
				var var_jammasuk = rec_cari_pasien.data["jammasuk"];
				var var_tglkeluar = rec_cari_pasien.data["tglkeluar"];
				var var_jamkeluar = rec_cari_pasien.data["jamkeluar"];
				var var_catatankeluar = rec_cari_pasien.data["catatankeluar"];
				var var_umurtahun = rec_cari_pasien.data["umurtahun"];
				var var_umurbulan = rec_cari_pasien.data["umurbulan"];
				var var_umurhari = rec_cari_pasien.data["umurhari"];
				var var_idklsrawat = rec_cari_pasien.data["idklsrawat"];
				var var_idkamar = rec_cari_pasien.data["idkamar"];
				var var_idstkeluar = rec_cari_pasien.data["idstkeluar"];
				var var_nooruangan = rec_cari_pasien.data["nooruangan"];
				var var_idregistrasi = rec_cari_pasien.data["idregistrasi"];
				var var_userinput = rec_cari_pasien.data["userinput"];
				var var_userbatal = rec_cari_pasien.data["userbatal"];
				var var_tglbatal = rec_cari_pasien.data["tglbatal"];
				var var_tglrencanakeluar = rec_cari_pasien.data["tglrencanakeluar"];
				var var_catatanrencanakeluar = rec_cari_pasien.data["catatanrencanakeluar"];
				var var_idcarakeluar = rec_cari_pasien.data["idcarakeluar"];
				var var_nmpasien = rec_cari_pasien.data["nmpasien"];
				var var_nmdokter = rec_cari_pasien.data["nmdoktergelar"];
				var var_nmpenjamin = rec_cari_pasien.data["nmpenjamin"];
				var var_norm = rec_cari_pasien.data["norm"];
				var var_nmkamar = rec_cari_pasien.data["nmkamar"];
				var var_nmbed = rec_cari_pasien.data["nmbed"];
				var var_nmklstarif = rec_cari_pasien.data["nmklstarif"];
				var var_normnya = rec_cari_pasien.data["normas"];
				
				
				Ext.getCmp('tf.noreg').focus()
				Ext.getCmp("tf.noreg").setValue(var_noreg);
				Ext.getCmp("tf.haha").setValue(var_norm);
				Ext.getCmp("tf.nmibu").setValue(var_nmpasien);
				Ext.getCmp("tf.dokter").setValue(var_nmdokter);
				Ext.getCmp("tf.penjamin").setValue(var_nmpenjamin);
				Ext.getCmp("df.tglreg").setValue(var_tglreg);
				Ext.getCmp("tf.jamreg").setValue(var_jamreg);
				Ext.getCmp("tf.idshift").setValue(var_idshift);
				Ext.getCmp("tf.idbagiankirim").setValue(var_idbagiankirim);
				Ext.getCmp("tf.iddokterkirim").setValue(var_iddokterkirim);
				Ext.getCmp("tf.nmdokterkirim").setValue(var_nmdokterkirim);
				Ext.getCmp("tf.idbagian").setValue(var_idbagian);
				Ext.getCmp("tf.iddokter").setValue(var_iddokter);
				Ext.getCmp("tf.idcaradatang").setValue(var_idcaradatang);
				Ext.getCmp("tf.idbed").setValue(var_idbed);
				Ext.getCmp("tf.idklstarif").setValue(var_idklstarif);
				Ext.getCmp("tf.tglmasuk").setValue(var_tglmasuk);
				Ext.getCmp("tf.jammasuk").setValue(var_jammasuk);
				Ext.getCmp("tf.tglmasuk").setValue(var_tglmasuk);
				Ext.getCmp("tf.tglkeluar").setValue(var_tglkeluar);
				Ext.getCmp("tf.jamkeluar").setValue(var_jamkeluar);
				Ext.getCmp("tf.catatankeluar").setValue(var_catatankeluar);
				Ext.getCmp("tf.umurtahun").setValue(var_umurtahun);
				Ext.getCmp("tf.umurbulan").setValue(var_umurbulan);
				Ext.getCmp("tf.umurhari").setValue(var_umurhari);
				Ext.getCmp("tf.idklsrawat").setValue(var_idklsrawat);
				Ext.getCmp("tf.idkamar").setValue(var_idkamar);
				Ext.getCmp("tf.idstkeluar").setValue(var_idstkeluar);
				Ext.getCmp("tf.nooruangan").setValue(var_nooruangan);
				Ext.getCmp("tf.idregistrasi").setValue(var_idregistrasi);
				Ext.getCmp("tf.userinput").setValue(var_userinput);
				Ext.getCmp("df.tglbatal").setValue(var_tglbatal);
				Ext.getCmp("tf.catatanrencanakeluar").setValue(var_catatanrencanakeluar);
				Ext.getCmp("tf.idcarakeluar").setValue(var_idcarakeluar);
				Ext.getCmp("tf.nmkamara").setValue(var_nmkamar);
				Ext.getCmp("tf.nmbeda").setValue(var_nmbed);
				Ext.getCmp("tf.nmklstarifa").setValue(var_nmklstarif);
				Ext.getCmp("tf.normnya").setValue(var_normnya);
				Ext.getCmp('btn_pindah').enable();
				ds_pindahruangan.setBaseParam('noreg',var_noreg);
				ds_pindahruangan.reload();
			win_find_cari_pasien.close();
			
			}
		}
		
	}

	function fnAddRuangan(norm){
		var grid = grid_ruangan;
		
		ds_pindahruangan.each(function(rec){
			if (!rec.get('tglkeluar')) {
				wEntry(false,grid, null,norm,rec);
			}
		});
	}
	
	
	function wEntry(isUpdate, grid, record,norm,rec){
		
		 var skldet_form = new Ext.form.FormPanel({
			xtype: 'form',
			id: 'frm.skldet',
			buttonAlign: 'left',
		//	autoScroll: true,
		//	labelWidth: 150, labelAlign: 'right',
		//	bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 260, width: 510,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			tbar: [{
				id:'btn_simpan',text: 'Simpan', iconCls:'silk-save', style: 'marginLeft: 5px',
				handler: function() {
					simpan();
					}
			},{
					text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
					handler: function() {
						wSKL.close();
						ds_pindahruangan.reload();
				}
			}],
			frame: false,
			items:[{
				xtype: 'panel', layout:'fit', height: 260,
				title:'', id:'fp.wppa', frame:true,
				items:[{
					xtype: 'fieldset', title: '', layout: 'form',	
				items: [{
					xtype: 'compositefield',
					fieldLabel: 'No. RM',
					id: 'comp_norma',
					items: [{
						xtype: 'textfield',
						id: 'tf.norm',
						width: 80,
						value: Ext.getCmp('tf.haha').getValue(),
					},{
						xtype: 'textfield', fieldLabel: 'No. Registrasi', id: 'tf.norega', width : 100, readOnly: true,
						value: Ext.getCmp('tf.noreg').getValue(),	
					}]
				},{
						xtype: 'textfield', fieldLabel: 'Nama', 
						id:'tf.nama', width: 150,readOnly: true, 
						value: Ext.getCmp('tf.nmibu').getValue(),	
				},{
					xtype: 'datefield', id: 'df.tglrega',
					width: 100, value: new Date(),
					format: 'd-m-Y',hidden: true,value: Ext.getCmp('df.tglreg').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.jamrega', width: 200,hidden: true,value: Ext.getCmp('tf.jamreg').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idshifta', width: 200,hidden: true,value: Ext.getCmp('tf.idshift').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idbagiankirima', width: 200,hidden: true,value: Ext.getCmp('tf.idbagiankirim').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.iddokterkirima', width: 200,hidden: true,
					value: Ext.getCmp('tf.iddokterkirim').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.nmdokterkirima', width: 200,hidden: true,
					value: Ext.getCmp('tf.nmdokterkirim').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idbagiana', width: 200,hidden: true,
					value: Ext.getCmp('tf.idbagian').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.iddoktera', width: 200,hidden: true,
					value: Ext.getCmp('tf.iddokter').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idcaradatanga', width: 200,hidden: true,
					value: Ext.getCmp('tf.idcaradatang').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idbeda', width: 200,hidden: true,
					value: rec.get('idbed')//Ext.getCmp('tf.idbed').getValue()
				},{
					xtype: 'textfield', fieldLabel: '', 
					id:'tf.idklstarifa', width: 200,hidden: true,
					value: rec.get('idklstarif')//Ext.getCmp('tf.idklstarif').getValue()
				},{
					xtype: 'datefield', id: 'df.tglmasuka',
					width: 100, value: new Date(),
					format: 'd-m-Y',hidden: true,
					value: Ext.getCmp('df.tglmasuk').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.jammasuka',width: 100,hidden: true,
					value: Ext.getCmp('tf.jammasuk').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.tglmasuka',width: 100,hidden: true,
					value: Ext.getCmp('tf.tglmasuk').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.tglkeluara',width: 100,hidden: true,
					value: Ext.getCmp('tf.tglkeluar').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.jamkeluara',width: 100,hidden: true,
					value: Ext.getCmp('tf.jamkeluar').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.catatankeluara',width: 100,hidden: true,
					value: Ext.getCmp('tf.catatankeluar').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.umurtahuna',width: 100,hidden: true,
					value: Ext.getCmp('tf.umurtahun').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.umurbulana',width: 100,hidden: true,
					value: Ext.getCmp('tf.umurbulan').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.umurharia',width: 100,hidden: true,
					value: Ext.getCmp('tf.umurhari').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idklsrawata',width: 100,hidden: true,
					value: Ext.getCmp('tf.idklsrawat').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idkamara',width: 100,hidden: true,
					value: rec.get('idkamar')//Ext.getCmp('tf.idkamar').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idstkeluara',width: 100,hidden: true,
					value: Ext.getCmp('tf.idstkeluar').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.nooruangana',width: 100,hidden: true,
					value: Ext.getCmp('tf.nooruangan').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idregistrasia',width: 100,hidden: true,
					value: Ext.getCmp('tf.idregistrasi').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.userinputa',width: 100,hidden: true,
					value: Ext.getCmp('tf.userinput').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.userbatala',width: 100,hidden: true,
					value: Ext.getCmp('tf.userbatal').getValue()
				},{
					xtype: 'datefield', id: 'df.tglbatala',
					width: 100, value: new Date(),
					format: 'd-m-Y',hidden: true,
					value: Ext.getCmp('df.tglbatal').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.catatanrencanakeluara',width: 100,hidden: true,
					value: Ext.getCmp('tf.catatanrencanakeluar').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.idcarakeluara',width: 100,hidden: true,
					value: Ext.getCmp('tf.idcarakeluar').getValue()
				},{
					xtype: 'textfield', fieldLabel: '',
					id: 'tf.normas',width: 100,hidden: true,
					value: Ext.getCmp('tf.normnya').getValue()
				},{
					xtype: 'container', fieldLabel: 'Kamar',
					layout: 'hbox',
					items: [{
						xtype: 'textfield', id: 'tf.kamar', readOnly:true,
						width: 90,value: rec.get('nmkamar')//Ext.getCmp('tf.nmkamara').getValue()
					},{
						xtype: 'label', id: 'lb.bed', text: 'Bed', margins: '0 10 0 5',
					},{ 	
						xtype: 'textfield', id: 'tf.bed',  readOnly:true,
						width: 90,width: 90,value: rec.get('nmbed')//Ext.getCmp('tf.nmbeda').getValue()
					},{
						xtype: 'label', id: 'lb.kelas', text: 'Kelas', margins: '0 10 0 5',
					},{
						xtype: 'textfield', id: 'tf.kelas',  readOnly:true,
						width: 90,value: rec.get('nmklstarif')//Ext.getCmp('tf.nmklstarifa').getValue()
					}]
				
			},{
			xtype: 'fieldset', title: 'Kamar Tujuan', layout: 'form',
			items:[{
					xtype: 'compositefield',
					fieldLabel: 'Kelas Rawat / Ruangan',
					items: [{
						xtype :'textfield', allowBlank: false,
						id :'tf.klsperawatan', width:110, readOnly: true
					},{
						xtype :'textfield', allowBlank: false,
						id :'tf.klsperawatans', width:110, readOnly: true,hidden: true
					},{
						xtype :'textfield', allowBlank: false,
						id :'tf.klstarifs', width:110, readOnly: true, hidden: true
					},{
						xtype: 'label', id: 'lb.garing3', text: '/', margins: '0 10 0 10',
					},{
						xtype :'textfield', allowBlank: false,
						id :'tf.ruangan', width:110, readOnly: true
					},{
						xtype :'textfield', allowBlank: false,
						id :'tf.ruangana', width:110, readOnly: true, hidden: true
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.klsperawatan',
						width: 45,
						handler: function() {
							dftKlsrawat();
						}
					}]
				},{
					
					xtype: 'compositefield',
					fieldLabel: 'Kamar / Bed',
					items: [{
						xtype: 'textfield', allowBlank: false,
						id: 'tf.nmkamar', width: 110, readOnly: true
					},{
						xtype: 'textfield', allowBlank: false,
						id: 'tf.nmkamars', width: 110, readOnly: true,hidden: true
					},{
						xtype: 'label', id: 'lb.garing3', text: '/', margins: '0 10 0 10',
					},{
						xtype: 'textfield', allowBlank: false,
						id: 'tf.nmbed', width: 110, readOnly: true
					},{
						xtype: 'textfield', allowBlank: false,
						id: 'tf.nmbedas', width: 110, readOnly: true, hidden: true
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.kamarbed',
						width: 45,
						handler: function() {
							dftBed();
						}
					}]
				
				}]
			}]
		}]
	}]
		
}); 
	var wSKL = new Ext.Window({
			title: 'Pindah',
			modal: true, closable:false,
			items: [skldet_form]
		}).show();
	function simpan(){
		var ngetes = Ext.getCmp('tf.kelas').getValue();
		var ngetesan = Ext.getCmp('tf.klsperawatan').getValue();
		
		if(ngetes == ngetesan){
			Ext.Ajax.request({
				url: BASE_URL + 'pindahruangan_controller/updateBed',
				params: {
					noreg : Ext.getCmp('tf.norega').getValue(),
					idbed : Ext.getCmp('tf.nmbedas').getValue(),
					stbed : Ext.getCmp('tf.idbeda').getValue(),
				},
				
					success: function(response){
						Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
						obj = Ext.util.JSON.decode(response.responseText);
						wSKL.close();
						ds_pindahruangan.reload();
						},
					failure : function(){
						Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
					}
				});
		}else{
		Ext.Ajax.request({
				url: BASE_URL + 'pindahruangan_controller/simpan',
				params: {
					noreg : Ext.getCmp('tf.norega').getValue(),
					norm : Ext.getCmp('tf.normas').getValue(),
					tglreg : Ext.getCmp('df.tglrega').getValue(),
					jamreg : Ext.getCmp('tf.jamrega').getValue(),
					idshift : Ext.getCmp('tf.idshifta').getValue(),
					idbagiankirim : Ext.getCmp('tf.idbagiankirima').getValue(),
					iddokterkirim : Ext.getCmp('tf.iddokterkirima').getValue(),
					idbagian : Ext.getCmp('tf.ruangana').getValue(),
					iddokter : Ext.getCmp('tf.iddoktera').getValue(),
					idcaradatang : Ext.getCmp('tf.idcaradatanga').getValue(),
					idbed : Ext.getCmp('tf.nmbedas').getValue(),
					stbed : Ext.getCmp('tf.idbeda').getValue(),
					idklstarif : Ext.getCmp('tf.klstarifs').getValue(),
					tglmasuk : Ext.getCmp('tf.tglmasuka').getValue(),
					jammasuk : Ext.getCmp('tf.jammasuka').getValue(),
					tglkeluar : Ext.getCmp('tf.tglkeluara').getValue(),
					jamkeluar : Ext.getCmp('tf.jamkeluara').getValue(),
					catatankeluar : Ext.getCmp('tf.catatankeluara').getValue(),
					umurtahun : Ext.getCmp('tf.umurtahuna').getValue(),
					umurbulan : Ext.getCmp('tf.umurbulana').getValue(),
					umurhari : Ext.getCmp('tf.umurharia').getValue(),
					idklsrawat : Ext.getCmp('tf.klsperawatans').getValue(),
					idkamar : Ext.getCmp('tf.nmkamars').getValue(),
					idstkeluar : Ext.getCmp('tf.idstkeluara').getValue(),
					nooruangan : Ext.getCmp('tf.nooruangana').getValue(),
					idregistrasi : Ext.getCmp('tf.idregistrasia').getValue(),
					userinput : Ext.getCmp('tf.userinputa').getValue(),
				//	tglinput : Ext.getCmp('tf.tglinputa').getValue(),
					userbatal : Ext.getCmp('tf.userbatala').getValue(),
					tglbatal : Ext.getCmp('df.tglbatala').getValue(),
				//	tglrencanakeluar : Ext.getCmp('tf.tglrencanakeluara').getValue(),
					catatankeluar : Ext.getCmp('tf.catatankeluara').getValue(),
					idcarakeluar : Ext.getCmp('tf.idcarakeluara').getValue(),
				},
				
					success: function(response){
						Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
						obj = Ext.util.JSON.decode(response.responseText);
						wSKL.close();
						ds_pindahruangan.reload();
						},
					failure : function(){
						Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
					}
				});
			}
	}

	
	}

//=====================================	
	function dftKlsrawat(){
		var ds_klsrawat = dm_klsrawat();
		ds_klsrawat.setBaseParam('cidjnspel', 2);
		function keyToView_klsrawat(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterKlsRawat" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_klsrawat = new Ext.grid.ColumnModel([
			{
				header: 'Kelas Perawatan',
				dataIndex: 'nmklsrawat',
				width: 100,
				renderer: keyToView_klsrawat
			},{
				header: 'Kelas Tarif',
				dataIndex: 'nmklstarif',
				width: 100
			},{
				header: 'Ruangan',
				dataIndex: 'nmbagian',
				width: 150
			}
		]);
		var sm_cari_klsrawat = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_klsrawat = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_klsrawat = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_klsrawat,
			displayInfo: true,
			displayMsg: 'Data Kelas Perawatan Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_cari_klsrawat = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_cari_klsrawat= new Ext.grid.GridPanel({
			ds: ds_klsrawat,
			cm: cm_cari_klsrawat,
			sm: sm_cari_klsrawat,
			view: vw_cari_klsrawat,
			height: 350,
			width: 400,
			plugins: cari_cari_klsrawat,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_klsrawat,
			listeners: {
				cellclick: klik_cari_klsrawat
			}
		});
		var win_find_cari_klsrawat = new Ext.Window({
			title: 'Cari Kelas Perawatan',
			modal: true,
			items: [grid_find_cari_klsrawat]
		}).show();

		function klik_cari_klsrawat(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterKlsRawat'){
				var rec_cari_klsrawat = ds_klsrawat.getAt(rowIdx);
				var var_cari_klsrawat = rec_cari_klsrawat.data["nmklstarif"];
				var var_cari_idklsrawat = rec_cari_klsrawat.data["idklsrawat"];
				var var_cari_idklstarif = rec_cari_klsrawat.data["idklstarif"];
				var var_cari_ruangan = rec_cari_klsrawat.data["nmbagian"];
				var var_cari_idruangan = rec_cari_klsrawat.data["idbagian"];
				
				Ext.getCmp("tf.klsperawatan").setValue(var_cari_klsrawat);
				Ext.getCmp("tf.klsperawatans").setValue(var_cari_idklsrawat);
				Ext.getCmp("tf.klstarifs").setValue(var_cari_idklstarif);
				Ext.getCmp("tf.ruangan").setValue(var_cari_ruangan);
				Ext.getCmp("tf.ruangana").setValue(var_cari_idruangan);
							win_find_cari_klsrawat.close();
			}
		}
	}
	
	function dftBed(){
		var ds_kamarbagian = dm_kamarbagian();
		ds_kamarbagian.setBaseParam('nmbagian', Ext.getCmp('tf.ruangan').getValue());
		function keyToView_bed(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterBed" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_cari_bedbagian = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'idbed'
			},{
				header: 'Ruangan',
				dataIndex: 'nmbagian',
				width: 100,
				renderer: keyToView_bed
			},{
				header: 'Kamar',
				dataIndex: 'nmkamar',
				width: 100
			},{
				header: 'Bed',
				dataIndex: 'nmbed',
				width: 100
			}
		]);
		var sm_cari_bedbagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_cari_bedbagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_cari_bedbagian = new Ext.PagingToolbar({
			store: ds_kamarbagian,
			displayInfo: true,
			displayMsg: 'Data Bed Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_cari_bedbagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'local',
			width: 200
		})];
		var grid_find_cari_bedbagian= new Ext.grid.GridPanel({
			ds: ds_kamarbagian,
			cm: cm_cari_bedbagian,
			sm: sm_cari_bedbagian,
			view: vw_cari_bedbagian,
			height: 350,
			width: 400,
			plugins: cari_cari_bedbagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_cari_bedbagian,
			listeners: {
				cellclick: klik_cari_bedbagian
			}
		});
		var win_find_cari_bedbagian = new Ext.Window({
			title: 'Cari Bed',
			modal: true,
			items: [grid_find_cari_bedbagian]
		}).show();

		function klik_cari_bedbagian(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterBed'){
				var rec_cari_bedbagian = ds_kamarbagian.getAt(rowIdx);
				var var_cari_nmkamar = rec_cari_bedbagian.data["nmkamar"];
				var var_cari_idkamar = rec_cari_bedbagian.data["idkamar"];
				var var_cari_nmbed = rec_cari_bedbagian.data["nmbed"];
				var var_cari_idbed = rec_cari_bedbagian.data["idbed"];
				
				Ext.getCmp("tf.nmkamar").setValue(var_cari_nmkamar);
				Ext.getCmp("tf.nmkamars").setValue(var_cari_idkamar);
				Ext.getCmp("tf.nmbed").setValue(var_cari_nmbed);
				Ext.getCmp("tf.nmbedas").setValue(var_cari_idbed);
							win_find_cari_bedbagian.close();
			}
		}
	}
	
}