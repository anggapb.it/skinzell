function RMKodifikasiPenyakit(){
	var ds_jdiagnosa = dm_jdiagnosa();
	var ds_tinlanjut = dm_tinlanjut();
	var ds_penkematian = dm_penkematian();
	
	var ds_carakeluar = dm_carakeluar();
	var ds_stkeluar = dm_stkeluar();
	var ds_cb_jkasus = dm_cb_jkasus();

	var pageSize = 50;
	
	var dm_registrasi_kodifikasi = new Ext.data.JsonStore({
			proxy: new Ext.data.HttpProxy({
			url : BASE_URL + 'kodifikasi_controller/get_registrasi_kodifikasi',
				method: 'POST'
			}),
			baseParams: {
				tglawal : Ext.util.Format.date(new Date(), 'Y-m-d'),
				tglakhir: Ext.util.Format.date(new Date(), 'Y-m-d'),
				key : '1',
				start:0,
				//limit:pageSize,
			},
			totalProperty: 'results',
			root: 'data',
			autoLoad: true,
			fields: [{
				name: 'idregdet',
				mapping: 'idregdet'
			},{
				name: 'tglreg',
				mapping: 'tglreg'
			},{
				name: 'jamreg',
				mapping: 'jamreg'
			},{
				name: 'noreg',
				mapping: 'noreg',
			},{
				name: 'normclean',
				mapping: 'normclean'
			},{
				name: 'norm',
				mapping: 'norm'
			},{
				name: 'nmpasien',
				mapping: 'nmpasien'
			},{
				name: 'idjnskelamin',
				mapping: 'idjnskelamin'
			},{
				name: 'kdjnskelamin',
				mapping: 'kdjnskelamin'
			},{
				name: 'nmjnskelamin',
				mapping: 'nmjnskelamin'
			},{
				name: 'idstposisipasien',
				mapping: 'idstposisipasien'
			},{
				name: 'nmstposisipasien',
				mapping: 'nmstposisipasien'
			},{
				name: 'idbagian',
				mapping: 'idbagian'
			},{
				name: 'nmbagian',
				mapping: 'nmbagian'
			},{
				name: 'iddokter',
				mapping: 'iddokter'
			},{
				name: 'nmdoktergelar',
				mapping: 'nmdoktergelar'
			},{
				name: 'keluhan',
				mapping: 'keluhan'
			},{
				name: 'idstpasien',
				mapping: 'idstpasien'
			},{
				name: 'nmstpasien',
				mapping: 'nmstpasien'
			},{
				name: 'umurbulan',
				mapping: 'umurbulan'
			},{
				name: 'umurhari',
				mapping: 'umurhari'
			},{
				name: 'umurtahun',
				mapping: 'umurtahun'
			},{
				name: 'idbed',
				mapping: 'idbed'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'idstkeluar',
				mapping: 'idstkeluar'
			},{
				name: 'nmstkeluar',
				mapping: 'nmstkeluar'
			},{
				name: 'idcarakeluar',
				mapping: 'idcarakeluar'
			},{
				name: 'nmcarakeluar',
				mapping: 'nmcarakeluar'
			},{
				name: 'idkematian',
				mapping: 'idkematian'
			},{
				name: 'idtindaklanjut',
				mapping: 'idtindaklanjut'
			},{
				name: 'tglmeninggal',
				mapping: 'tglmeninggal'
			},{
				name: 'jammeninggal',
				mapping: 'jammeninggal'
			},{
				name: 'idkamar',
				mapping: 'idkamar'
			},{
				name: 'idbed',
				mapping: 'idbed'
			},{
				name: 'nmkamar',
				mapping: 'nmkamar'
			},{
				name: 'nmbed',
				mapping: 'nmbed'
			},{
				name: 'stinfo',
				mapping: 'stinfo'
			},{
				name: 'idjnskasus',
				mapping: 'idjnskasus'
			}]
		});
	
	var ds_kodifikasi = dm_kodifikasi();
	ds_kodifikasi.setBaseParam('noreg', '1');
	var rowidreservasi = '';
	
	var arr_cari = [['noreg', 'No. Registrasi'],['norm', 'No. RM'],['nmpasien', 'Nama Pasien'],['kdjnskelamin', '(L/P)'],['nmbagian', 'Bagian(Unit/Ruangan)'],['nmdoktergelar', 'Dokter']];
	
	var ds_search = new Ext.data.ArrayStore({
		fields: [ 'field', 'value' ],
		data : arr_cari 
	});
	
	/* var ds_search = new Ext.data.JsonStore({ //static data
		autoDestroy: true,
		fields: [ 'field', 'value' ],
	}); */
	
	//========================================GRID REGISTRASI
	var search_registrasi = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200
	})];
	var pagging = new Ext.PagingToolbar({
		pageSize: pageSize, store: dm_registrasi_kodifikasi,
		displayInfo: true,
		displayMsg: 'Data Kodifikasi Dari {0} - {1} of {2}',
		emptyMsg: 'Tidak ada data untuk ditampilkan'
	});
	var row_registrasi = new Ext.grid.RowSelectionModel({
		singleSelect: true
	});
	
	var grid_registrasi = new Ext.grid.EditorGridPanel({
		store: dm_registrasi_kodifikasi,
		sm: row_registrasi,
		height: 200,
		id: 'grid_registrasi',
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		//plugins: search_registrasi,
		tbar: [
			{
				xtype: 'label', text: 'Periode :'
			},{
				xtype: 'datefield', 
				id: 'ftr.tglawal',
				width: 100, 
				value: new Date(),
				format: 'd-m-Y',
				listeners:{
					/*select: function(field, newValue){
						filterPeriodeRegistrasi();
					},
					 change : function(field, newValue){
						filterPeriodeRegistrasi();
					} */
				}
			},{
				xtype: 'label', id: 'lb.sd', text: ' s/d '
			},{
				xtype: 'datefield', 
				id: 'ftr.tglakhir',
				width: 100, 
				value: new Date(),
				format: 'd-m-Y',
				listeners:{
					/*select: function(field, newValue){
						filterPeriodeRegistrasi();
					},
					 change : function(field, newValue){
						filterPeriodeRegistrasi();
					} */
				}
			},'->',
			{
				xtype: 'label', text: 'Cari Berdasarkan :'
			},
			{
				xtype: 'combo', fieldLabel: '',
				id:'cb.search', autoWidth: true, store: ds_search,
				valueField: 'field', displayField: 'value', editable: false,allowBlank: true,
				triggerAction: 'all',forceSelection: true, submitValue: true, mode: 'local', value: 'noreg',
				emptyText:'Pilih....',
				listeners: {
					/* afterrender: function () {
						var a, rec, header, index;
						for (a=1;a<=10;a++) {
							header = grid_registrasi.getColumnModel().getColumnHeader(a).replace('<div style="text-align:center;">','').replace('</div>','').replace('<br>',' ');
							index = grid_registrasi.getColumnModel().getDataIndex(a);
													
							rec = new ds_search.recordType({field:header, value:index});
							rec.commit();
							ds_search.add(rec);
						}
					},
					select: function () {
					
					} */
				}
			},{
				xtype: 'textfield',
				id: 'tf.search',
				width:250,
				allowBlank: true,
				listeners: {
					specialkey: function(f,e){
						if (e.getKey() == e.ENTER) {
							filterPeriodeRegistrasi();
						}
					}
				}
			},{
				xtype: 'button',
				id: 'bsearch',
				align:'left',
				iconCls: 'silk-find',
				handler: function() {
					filterPeriodeRegistrasi();
				}
			}
		],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var jkel;
				var record = grid.getStore().getAt(rowIndex);
				Ext.getCmp("tf.idregdet").setValue(record.get('idregdet'));
				Ext.getCmp("tf.noreg").setValue(record.get('noreg'));
				//Ext.getCmp("tf.tglreg").setValue(record.get('tglreg'));
				Ext.getCmp("tf.tglreg").setValue(Ext.util.Format.date(record.get('tglreg'), 'd-m-Y'));
				Ext.getCmp("tf.stpas").setValue(record.get('nmstpasien'));
				//Ext.getCmp("tf.norm").setValue(record.get('norm'));
				Ext.getCmp("tf.norm").setValue(record.get('normclean'));
				Ext.getCmp("tf.nmpasien").setValue(record.get('nmpasien'));
				Ext.getCmp("tf.umurthn").setValue(record.get('umurtahun'));
				Ext.getCmp("tf.umurbln").setValue(record.get('umurbulan'));
				Ext.getCmp("tf.umurhari").setValue(record.get('umurhari'));
				Ext.getCmp("tf.jkelamin").setValue(record.get('kdjnskelamin'));
				
				Ext.getCmp("tf.nmdoktergelar").setValue(record.get('nmdoktergelar'));
				Ext.getCmp("tf.bagian").setValue(record.get('nmbagian'));
				Ext.getCmp("tf.kamar").setValue(record.get('nmkamar'));
				Ext.getCmp("tf.bed").setValue(record.get('nmbed'));
				Ext.getCmp("tf.posisipasien").setValue(record.get('nmstposisipasien'));
				
				Ext.getCmp("cb.stkeluar").setValue(record.get('idstkeluar'));
				Ext.getCmp("cb.carakeluar").setValue(record.get('idcarakeluar'));
				Ext.getCmp("cb.tlanjut").setValue(record.get('idtindaklanjut'));
				Ext.getCmp("cb.pkematian").setValue(record.get('idkematian'));
				Ext.getCmp("tglmeninggal").setValue(record.get('tglmeninggal'));
				Ext.getCmp("jammeninggal").setValue(record.get('jammeninggal'));
				Ext.getCmp("cb.jkasus").setValue(record.get('idjnskasus'));
				
				if (record.get('idstkeluar') == 2 || record.get('idstkeluar') == 3) {
					Ext.getCmp('tglmeninggal').enable();
					Ext.getCmp('jammeninggal').enable();
				} else {
					Ext.getCmp('tglmeninggal').disable();
					Ext.getCmp('jammeninggal').disable();
										
					Ext.getCmp('tglmeninggal').setValue(null);
					Ext.getCmp('jammeninggal').setValue(null);
				}
				
				ds_kodifikasi.setBaseParam('noreg', record.get('noreg'));
				ds_kodifikasi.reload();
				Ext.getCmp('btn_add').enable();
            }
        },
		columns: [
		new Ext.grid.RowNumberer(),
		{
			header: '<div style="text-align:center;">Tgl<br>Registrasi</div>',
			dataIndex: 'tglreg',
            align: 'left',
			width: 80,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: '<div style="text-align:center;">Jam<br>Registrasi</div>',
			dataIndex: 'jamreg',
            align: 'left',
			width: 80,
		},{
			header: '<div style="text-align:center;">No<br>Registrasi</div>',
			dataIndex: 'noreg',
            align: 'left',
			width: 90
		},{
			header: '<div style="text-align:center;">No. RM</div>',
			dataIndex: 'normclean',
			width: 50
		},{
			header: '<div style="text-align:center;">Nama Pasien</div>',
			dataIndex: 'nmpasien',
			width: 150
		},{
			header: '(L/P)',
			dataIndex: 'nmjnskelamin',
            align: 'center',
			width: 75,
		},{
			header: '<div style="text-align:center;">Lama/<br>Baru</div>',
			dataIndex: 'nmstpasien',
			width: 50
		},{
			header: '<div style="text-align:center;">Bagian<br>(Unit/Ruangan)</div>',
			dataIndex: 'nmbagian',
			width: 105
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 150
		},{
			header: '<div style="text-align:center;">Anamnesa/<br>Keluhan</div>',
			dataIndex: 'keluhan',
			width: 117
		}/* ,{
			header: 'Posisi Pasien',
			dataIndex: 'nmstposisipasien',
			width: 110
		} */,{
			header: 'Kodifikasi',
			dataIndex: 'stinfo',
			width: 110,
			align: 'center',
			renderer: function(value, p, r){
				var info = '';
					if(r.data['stinfo'] == '0' ) info = ' ';
					if(r.data['stinfo'] != '0' ) info = 'Sudah Kodifikasi';
				return info ;
			}
		}],
		//bbar: pagging,
	});
	
	//========================================GRID DIAGNOSA
	var grid_diagnosa = new Ext.grid.EditorGridPanel({
		store: ds_kodifikasi,
		frame: true,
		height: 180,
		bodyStyle: 'padding:10px 10px 10px 10px',
		id: 'grid_diagnosa',
		forceFit: true,
		autoScroll: true,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			disabled: true,
			iconCls: 'silk-add',
			handler: function() {
				fDiagnosa();
			}
		},{
							xtype: 'button',
							text: 'Simpan',
							id: 'btn.simpan',
							iconCls: 'silk-save',
							handler: function() {
								simpan('fp.kodifikasi');
							}
		}],
		columns: [{
			dataIndex: 'idpenyakit',
			hidden: true
		},{
			header: '<div style="text-align:center;">Kode Penyakit<br>(ICD-X)</div>',
			width: 100,
			dataIndex: 'kdpenyakit'
		},{
			header: '<div style="text-align:center;">nama Penyakit<br>(Bhs. Inggris)</div>',
			width: 250,
			dataIndex: 'nmpenyakiteng'
		},{
			header: '<div style="text-align:center;">Nama Penyakit<br>(Bhs. Indonesia)</div>',
			width: 250,
			dataIndex: 'nmpenyakit'
		},{
			header: '<div style="text-align:center;">(Baru/Lama)</div>',
			width: 100,
			dataIndex: 'nmjstpenyakit'
		},{
			header: '<div style="text-align:center;">Jenis Diagnosa</div>',
			width: 150,
			dataIndex: 'nmjnsdiagnosa',
			editor: {
				xtype: 'combo',
				id: 'cb.jdiagnosa', width: 150, 
				store: ds_jdiagnosa, valueField: 'nmjnsdiagnosa', displayField: 'nmjnsdiagnosa',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local'
			}
		},{
			xtype: 'actioncolumn',
			width: 50,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex, colIndex) {
					var rechapus = ds_kodifikasi.getAt(rowIndex);
				
					Ext.Ajax.request({
						url: BASE_URL + 'kodifikasi_controller/hapusdetkodifikasi',
						params: {
							idkodifikasi: rechapus.data.idkodifikasi,
							idpenyakit	: rechapus.data.idpenyakit,
						},
						success: function(response){
							Ext.MessageBox.alert('Informasi', 'Hapus Data Berhasil');
							ds_kodifikasi.removeAt(rowIndex);
						},
						failure: function(){
							Ext.MessageBox.alert('Informasi', 'Hapus Data Gagal');
						}
					});
				
				}
			}]
        }]
	});

	
	var kodifikasi_form = new Ext.form.FormPanel({ 
		id: 'fp.kodifikasi',
		title: 'Kodifikasi Penyakit',
		width: 900, Height: 1000,
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		//tbar: [],
		defaults: { labelWidth: 150, labelAlign: 'right'},
        items: [{
			xtype: 'fieldset', layout: 'form', autowidth: true,
			items: [grid_registrasi]
		},{
			xtype: 'fieldset',style: 'padding:10px',
			layout: 'column', title: 'Data Registrasi Pasien',
			items: [{
					//COLUMN 1
					layout: 'form', columnWidth: 0.50,
					items: [{
							xtype: 'compositefield', layout:'anchor',
							fieldLabel: 'No. / Tgl. Registrasi',
							items: [{
								xtype: 'textfield',
								id: 'tf.idregdet', width: '10%', hidden: true, 
								style : 'opacity:0.6'
							},{
								xtype: 'textfield',
								id: 'tf.noreg', width: '20%', readOnly: true, 
								style : 'opacity:0.6'
							},{
								xtype: 'label', id: 'lb.garings', text: '/', margins: '2 7 0 7',
							},{
								xtype: 'textfield',
								id: 'tf.tglreg', width: '20%', readOnly: true, 
								style : 'opacity:0.6'
							},{
								xtype: 'label', id: 'lb.pasreg', text: 'Pasien :', margins: '2 7 0 15', 
							},{
								xtype: 'textfield',
								id: 'tf.stpas', width: '24%', readOnly: true, 
								style : 'opacity:0.6'
							}]
						},{
							xtype: 'container', fieldLabel: 'No. RM / Nama Pasien',
							layout: 'hbox',
							items: [{
								xtype: 'textfield',
								id  : 'tf.norm', width : '20%', readOnly: true, 
								style : 'opacity:0.6'
							},{
								xtype: 'label', id: 'lb.garing', text: '/', margins: '2 5 0 10',
							},{
								xtype: 'textfield',
								id  : 'tf.nmpasien', width : '64%', readOnly: true, 
								style : 'opacity:0.6'
							}]
						},{
							xtype: 'container', fieldLabel: 'Umur',
							layout: 'hbox',
							items: [{
								xtype: 'textfield', id: 'tf.umurthn', 
								width: '5%', readOnly: true,
								style : 'opacity:0.6'
							},{
								xtype: 'label', id: 'lb.umurthn', text: 'Tahun', margins: '2 11 0 5',
							},{ 	
								xtype: 'textfield', id: 'tf.umurbln', 
								width: '5%', readOnly: true, 
								style : 'opacity:0.6'
							},{
								xtype: 'label', id: 'lb.umurbln', text: 'Bulan', margins: '2 11 0 5',
							},{
								xtype: 'textfield', id: 'tf.umurhari', 
								width: '5%', readOnly: true, 
								style : 'opacity:0.6'
							},{
								xtype: 'label', id: 'lb.umurhari', text: 'Hari', margins: '2 11 0 5'
							},{
								xtype: 'label', id: 'lb.jkel', text: 'Jenis Kelamin', margins: '2 5 0 17'
							},{
								xtype: 'textfield', id: 'tf.jkelamin', 
								width: '14%', readOnly: true, 
								style : 'opacity:0.6'
							}]
						}]
				},{
					//COLUMN 2
					layout: 'form', columnWidth: 0.50,
					items: [{
								xtype: 'textfield', fieldLabel: 'Dokter',
								id: 'tf.nmdoktergelar', width: '95%', readOnly: true, 
								style : 'opacity:0.6'
							},{
								xtype: 'compositefield', layout:'anchor',
								fieldLabel: 'Bagian',
								items: [{
									xtype: 'textfield',
									id: 'tf.bagian', width: '20%', readOnly: true, 
									style : 'opacity:0.6'
								},{
									xtype: 'label', id: 'lb.reg', text: 'Kamar :', margins: '2 5 0 5',
								},{
									xtype: 'textfield',
									id: 'tf.kamar', width: '20%', readOnly: true, 
									style : 'opacity:0.6'
								},{
									xtype: 'label', id: 'lb.bed', text: 'Bed :', margins: '2 5 0 15',
								},{
									xtype: 'textfield',
									id: 'tf.bed', width: '24%', readOnly: true, 
									style : 'opacity:0.6'
								}]
							},
							{
									xtype: 'textfield', fieldLabel: 'Posisi Pasien',
									id: 'tf.posisipasien', width: '37%', readOnly: true, 
									style : 'opacity:0.6'
							}]
				}
			]
		},
		{
			xtype: 'fieldset',
			frame: true,
			/* width: 940,
			height: 40, */
			layout:'column',
			style: 'padding:10px',
			items: [{
					layout: 'form', columnWidth: 0.27,border: false,frame: false,
					items: [{
							xtype: 'combo',
							id: 'cb.stkeluar',
							fieldLabel: 'Keadaan Keluar',							
							width: 140, 
							store: ds_stkeluar, 
							valueField: 'idstkeluar', 
							displayField: 'nmstkeluar',
							editable: false, 
							triggerAction: 'all', 
							hiddenName: 'cb.stkeluar_h',
							forceSelection: true, 
							submitValue: true, 
							mode: 'local',
							allowBlank:true,
							listeners:{
								select: function(combo, record){
									if (combo.getValue() == 2 || combo.getValue() == 3) {
										Ext.getCmp('tglmeninggal').enable();
										Ext.getCmp('jammeninggal').enable();
									} else {
										Ext.getCmp('tglmeninggal').disable();
										Ext.getCmp('jammeninggal').disable();
										
										Ext.getCmp('tglmeninggal').setValue(null);
										Ext.getCmp('jammeninggal').setValue(null);
									}
								}
							}
						},{
							xtype: 'combo', fieldLabel: 'Penyebab Kematian',		
							id: 'cb.pkematian', width: 140, 
							store: ds_penkematian, valueField: 'idkematian', displayField: 'nmkematian',
							editable: false, triggerAction: 'all', hiddenName: 'cb.pkematian_h',
							forceSelection: true, submitValue: true, mode: 'local',// readOnly: true
						}]
					},{
					layout: 'form', columnWidth: 0.33,
					items: [{
							xtype: 'combo', fieldLabel: 'Cara Keluar',	
							id: 'cb.carakeluar', width: 190, 
							store: ds_carakeluar, valueField: 'idcarakeluar', displayField: 'nmcarakeluar',
							editable: false, triggerAction: 'all', hiddenName: 'cb.carakeluar_h',
							forceSelection: true, submitValue: true, mode: 'local'
						},{
							xtype: 'datefield', 
							id: 'tglmeninggal',
							width: 100, fieldLabel: 'Tanggal Meninggal',	
							format: 'd-m-Y',
							listeners:{
								select: function(field, newValue){
									
								},
								change : function(field, newValue){
									
								}
							}
						}]
					},{
					layout: 'form', columnWidth: 0.40,
					items: [{
							xtype: 'compositefield',
							items: [{
								xtype: 'combo', fieldLabel: 'Tindak Lanjut',	
								id: 'cb.tlanjut', width: 90, 
								store: ds_tinlanjut, valueField: 'idtindaklanjut', displayField: 'nmtindaklanjut',
								editable: false, triggerAction: 'all', hiddenName: 'cb.tlanjut_h',
								forceSelection: true, submitValue: true, mode: 'local',
								listeners: {
									select : function(combo, records, eOpts){
										var obj = records.data;
										if(obj.idtindaklanjut == 1 || obj.idtindaklanjut == 2 || obj.idtindaklanjut == 3 || obj.idtindaklanjut == 4){
											Ext.getCmp('cb.pkematian').setReadOnly(true);
											Ext.getCmp('cb.pkematian').setValue('');
										}else{
											Ext.getCmp('cb.pkematian').setReadOnly(false);
											Ext.MessageBox.alert('Informasi', 'IBU HAMIL / MELAHIRKAN / BAYI BARU LAHIR yang meninggal, SEGERA LAPORKAN KE YANKES RUJUKAN DINKES KOTA BANDUNG 022-7234212 !!');
										}
									}								
								}
								
							},{
								xtype: 'label', id: 'lb.d', text: 'Jam Meninggal :', margins: '2 5 0 5',
							},{
								//fieldLabel: 'Jam Meninggal',
								xtype: 'timefield',
								id: 'jammeninggal',
								width: 70,
								format: 'H:i',
								altFormats:'H:i',
								increment: 1,
								listeners:{
									select :function(combo, record, index){ 
										
									}
								}
							}]
						}/* ,{
							xtype: 'textfield', fieldLabel: 'Jenis Kasus',
							id: 'tf.jkasus', width: 262,
						} */
						,{
							xtype: 'combo', fieldLabel: 'Jenis Kasus',	
							id: 'cb.jkasus', width: 262, 
							store: ds_cb_jkasus, valueField: 'idjnskasus', displayField: 'nmjnskasus',
							editable: false, triggerAction: 'all', hiddenName: 'cb.jkasus_h',
							forceSelection: true, submitValue: true, mode: 'local'							
						}]
					},{
						layout: 'form', columnWidth: 1,border: false,frame: false,
						items: [grid_diagnosa]
					}]
				
		}]
	}); SET_PAGE_CONTENT(kodifikasi_form);
	
	function Addrecord(grid, rowIndex){
		var record = dm_registrasi_kodifikasi.getAt(rowIndex);
		rowidreservasi = record.data['idreservasi'];
	}
	
	function bersihrjper() {
		Ext.getCmp('tf.noreg').setValue();
		Ext.getCmp('tf.tglreg').setValue();
		Ext.getCmp('tf.stpas').setValue();
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue();
		Ext.getCmp('tf.umurthn').setValue();
		Ext.getCmp('tf.umurbln').setValue();
		Ext.getCmp('tf.umurhari').setValue(); 
		Ext.getCmp('tf.jkelamin').setValue();
		
		Ext.getCmp("tf.nmdoktergelar").setValue();
		Ext.getCmp("tf.bagian").setValue();
		Ext.getCmp("tf.kamar").setValue();
		Ext.getCmp("tf.bed").setValue();
		Ext.getCmp("cb.stkeluar").setValue();
		Ext.getCmp("cb.carakeluar").setValue();
		
		Ext.getCmp("cb.pkematian").setValue();
		Ext.getCmp("cb.tlanjut").setValue();
				
		Ext.getCmp("tglmeninggal").setValue();
		Ext.getCmp("jammeninggal").setValue();
		Ext.getCmp("cb.jkasus").setValue();
				
		Ext.getCmp('btn_add').disable();
		dm_registrasi_kodifikasi.reload();
		ds_kodifikasi.setBaseParam('noreg', '1');
		ds_kodifikasi.reload();
	}

	function simpan(namaForm) {
		var arrdiagnosa = [];
		var zx = 0;
		var validasinya = Ext.getCmp('cb.stkeluar').getValue();
	 	if(validasinya == ''){
			alert("Pilih Keadaan Keluar");
		}else{
	
		
		ds_kodifikasi.each(function(rec){
			var zidpenyakit = rec.get('idpenyakit');
			var znmjnsdiagnosa = rec.get('nmjnsdiagnosa');
			var znmjstpenyakit = rec.get('nmjstpenyakit');
			arrdiagnosa[zx] = zidpenyakit+'-'+znmjnsdiagnosa+'-'+znmjstpenyakit;
			zx++;
		});
				
		var form_nya = Ext.getCmp(namaForm);
		form_nya.getForm().submit({
			url: BASE_URL + 'kodifikasi_controller/insertregistrasitokodifikasi',
			method: 'POST',
			params: {
				arrdiagnosa	:  Ext.encode(arrdiagnosa)
			},
			success: function(kodifikasi_form, o) {
				if (o.result.success==true) {
					Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
					Ext.getCmp('tf.norm').setValue(o.result.norm);
				}
				bersihrjper();
			},
			failure: function(kodifikasi_form, o) {
				Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
			}
		});
	}
	
	function cari() {
		dm_registrasi_kodifikasi.setBaseParam('tglreservasi',Ext.getCmp('df.tglreservasi').getValue());
		dm_registrasi_kodifikasi.setBaseParam('upel',Ext.getCmp('cb.upelayanan').getValue());
		dm_registrasi_kodifikasi.setBaseParam('dokter',Ext.getCmp('cb.dokter').getValue());
		dm_registrasi_kodifikasi.reload();
	}
	
	function ulangCari() {
		Ext.getCmp('df.tglreservasi').setValue(new Date());
		Ext.getCmp('cb.upelayanan').setValue();
		Ext.getCmp('cb.dokter').setValue();
		
		dm_registrasi_kodifikasi.setBaseParam('upel','');
		dm_registrasi_kodifikasi.setBaseParam('dokter','');
		dm_registrasi_kodifikasi.setBaseParam('tglreservasi', Ext.util.Format.date(new Date(), 'Y-m-d'));
		dm_registrasi_kodifikasi.reload();
	}
	
	function hitungBMI(){
		var tinggi = Ext.getCmp('tf.tinggi').getValue();
		var berat = Ext.getCmp('tf.berat').getValue();
		if(tinggi != '' && berat != ''){
			var tinggirt = tinggi/100;
			var keterangan = '';
			var bmi = berat / (tinggirt * tinggirt);
			if(bmi < 18.5) keterangan = 'Underweight';
			else if(bmi < 25) keterangan = 'Normal';
			else if(bmi < 30) keterangan = 'Overweight';
			else keterangan = 'Obesitas';
			
			Ext.getCmp('tf.bmi1').setValue(Ext.util.Format.number(bmi, '0.0'));
			Ext.getCmp('tf.bmi2').setValue(keterangan);
		} else {
			Ext.getCmp('tf.bmi1').setValue();
			Ext.getCmp('tf.bmi2').setValue();
		}
		
	}

	function fnEditPosisiPasien(idstposisipasien){
		Ext.Ajax.request({
			url: BASE_URL + 'reservasi_controller/updateStposisipasien',
			params: {
				idreservasi : rowidreservasi,
				idstposisipasien : idstposisipasien
			},
			success: function(){
				Ext.getCmp('grid_registrasi').store.reload();
				//Ext.Msg.alert("Informasi", "Posisi Pasien Berhasil Dirubah");
			}
			
		});
	}

	}
	function fDiagnosa(){
		var ds_penyakit = dm_penyakit();
		
		function keyToView_penyakit(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterPenyakit" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_penyakit = new Ext.grid.ColumnModel([
			{
				header: '<div style="text-align:center;">Kode Penyakit<br>(ICD-X)</div>',
				width: 100,
				dataIndex: 'kdpenyakit',
				renderer: keyToView_penyakit
			},{
				header: '<div style="text-align:center;">nama Penyakit<br>(Bhs. Inggris)</div>',
				width: 200,
				dataIndex: 'nmpenyakiteng'
			},{
				header: '<div style="text-align:center;">Nama Penyakit<br>(Bhs. Indonesia)</div>',
				width: 200,
				dataIndex: 'nmpenyakit'
			}
		]);
		var sm_penyakit = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_penyakit = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_penyakit = new Ext.PagingToolbar({
			pageSize: 20,
			store: ds_penyakit,
			displayInfo: true,
			displayMsg: 'Data Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_penyakit = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_penyakit = new Ext.grid.GridPanel({
			ds: ds_penyakit,
			cm: cm_penyakit,
			sm: sm_penyakit,
			view: vw_penyakit,
			height: 460,
			width: 550,
			plugins: cari_penyakit,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_penyakit,
			listeners: {
				cellclick: klik_penyakit
			}
		});
		var win_penyakit = new Ext.Window({
			title: 'Cari Penyakit',
			modal: true,
			items: [grid_penyakit]
		}).show();

		function klik_penyakit(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyMasterPenyakit'){
				var cek = true;
				var obj = ds_penyakit.getAt(rowIdx);
				var sidpenyakit			= obj.data["idpenyakit"];
				var skdpenyakit			= obj.data["kdpenyakit"];
				var snmpenyakit			= obj.data["nmpenyakit"];
				var snmpenyakiteng		= obj.data["nmpenyakiteng"];
				
				ds_kodifikasi.each(function(rec){
					if(rec.get('kdpenyakit') == skdpenyakit) {
						Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
						cek = false;
					}
				});
				
				Ext.Ajax.request({
					url: BASE_URL + 'Jstpenyakit_controller/cekStatus',
					params: {
						noreg		: Ext.getCmp('tf.noreg').getValue(),
						idpenyakit	: sidpenyakit
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						if(cek){
							var orgaListRecord = new Ext.data.Record.create([
								{
									name: 'idpenyakit',
									name: 'kdpenyakit',
									name: 'nmpenyakit',
									name: 'nmpenyakiteng',
									name: 'nmjstpenyakit',
									name: 'nmjnsdiagnosa'
								}
							]);
							
							ds_kodifikasi.add([
								new orgaListRecord({
									'idpenyakit': sidpenyakit,
									'kdpenyakit': skdpenyakit,
									'nmpenyakit': snmpenyakit,
									'nmpenyakiteng': snmpenyakiteng,
									'nmjstpenyakit': obj.stat,
									'nmjnsdiagnosa': 'Utama'
								})
							]);
						}
					}
				});
			}
		}
	}

	function filterPeriodeRegistrasi()
	{
		if(Ext.getCmp('ftr.tglawal').getValue() !== false && Ext.getCmp('ftr.tglakhir').getValue() !== false){
			dm_registrasi_kodifikasi.setBaseParam('tglawal',Ext.getCmp('ftr.tglawal').getValue().format('Y-m-d'));
			dm_registrasi_kodifikasi.setBaseParam('tglakhir',Ext.getCmp('ftr.tglakhir').getValue().format('Y-m-d'));
		}
		dm_registrasi_kodifikasi.setBaseParam('key',(Ext.getCmp('cb.search').getValue())?Ext.getCmp('cb.search').getValue():'1');
		dm_registrasi_kodifikasi.setBaseParam('value',Ext.getCmp('tf.search').getValue());
		dm_registrasi_kodifikasi.reload();
	}
}