function Ksrpengeluarankas(){
	var pageSize = 50;
	var ds_jnstransaksi = dm_jnstransaksidijurnal();
	var ds_pengeluarankasdet = dm_pengeluarankasdet();
		ds_pengeluarankasdet.setBaseParam('kdjurnal','null');
	var ds_pengeluarankasdetail = dm_pengeluarankasdetail();
		ds_pengeluarankasdetail.setBaseParam('kdjurnal','null');
		
	var ds_pengeluarankas = dm_pengeluarankas();
	
	var row = '';
	
	var arr_cari = [['kdjurnal', 'Kode. Jurnal'],['noreg', 'No. Registrasi'],['nmpasien', 'Nama Pasien'],['nmbagian', 'Bagian'],['keterangan', 'Keterangan']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
	
	function fnSearchgrid(){
		ds_pengeluarankas.setBaseParam('chb_periode', Ext.getCmp('chb.periode').getValue());
		
		ds_pengeluarankas.setBaseParam('tglawal', Ext.getCmp('tglawal').getValue());
		ds_pengeluarankas.setBaseParam('tglakhir', Ext.getCmp('tglakhir').getValue());
		
		var idcombo, nmcombo;
		idcombo= Ext.getCmp('cb.search').getValue();
		nmcombo= Ext.getCmp('cek').getValue();
			ds_pengeluarankas.setBaseParam('key',  '1');
			ds_pengeluarankas.setBaseParam('id',  idcombo);
			ds_pengeluarankas.setBaseParam('name',  nmcombo);
		ds_pengeluarankas.load();
		
		searchSum();
	}
	
	function searchSum(){
		Ext.Ajax.request({
			url: BASE_URL + 'pengeluarankas_controller/searchsum',
			method:'POST',
			params: {
				cbtgl		: Ext.getCmp('chb.periode').getValue(),
				tglawal 	: Ext.getCmp('tglawal').getValue(),
				tglakhir	: Ext.getCmp('tglakhir').getValue(),
				idcombo 	: Ext.getCmp('cb.search').getValue(),
				nmcombo		: Ext.getCmp('cek').getValue()
			},
			success:function(response){
				Ext.getCmp("tf.jumlah").setValue(Ext.util.JSON.decode(response.responseText).sum)
			},
			failure:function(result){
				
			}
		});
	}
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_pengeluarankas,
		displayInfo: true,
		displayMsg: 'Data Ke {0} - {1} of {2}',
		emptyMsg: 'No data',
		items: [{
			xtype: 'fieldset',
			border: false,
			style: 'padding:0px; margin: 0px',
			width: 730,
			items: [{
				xtype: 'compositefield',
				items: [{
					xtype: 'label', id: 'lb.jml', text: 'Jumlah :', margins: '3 10 0 465',
				},{
					xtype: 'numericfield',
					id: 'tf.jumlah',
					value: 0,
					width: 100,
					readOnly:true,
					style : 'opacity:0.6',
					thousandSeparator:',',					
				}]
			}]
		}]
	});
	
	var vw_pengeluarankas = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var grid_pengeluarankas = new Ext.grid.GridPanel({
		id: 'grid_pengeluarankas',
		store: ds_pengeluarankas,
		view: vw_pengeluarankas,
		autoScroll: true,
		frame: true,
		tbar: [{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddPemakainabrg();
				Ext.getCmp('tf.carikdjurnal').setValue();
				Ext.getCmp('tf.reckdjurnal').setValue();
				Ext.getCmp('df.tgltransaksi').setValue(new Date());
				//Ext.getCmp('bt.batal').disable();
				//Ext.getCmp('bt.cetak').disable();
				Ext.Ajax.request({
					url:BASE_URL + 'pengeluarankas_controller/getCekstkasir',
					method:'POST',
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp("tf.nokasir").setValue(obj.nokasir);
					}
				});
			}
		},'-',{
			xtype: 'compositefield',
			width: 930,
			items: [{
				xtype: 'textfield',
				id:'tf.carinojurnal',
				width: 60,
				hidden: true,
				validator: function(){
					ds_pengeluarankasdetail.setBaseParam('kdjurnal', Ext.getCmp('tf.carinojurnal').getValue());
					fTotaldet();					
					Ext.getCmp('grid_jurpengeluarankas').store.reload();
				}
			},{
				xtype: 'checkbox',
				id: 'chb.periode', margins: '-1 3 0 100',
				listeners: {
					check: function(checkbox, val){
						if(val == true){
							Ext.getCmp('tglawal').enable();
							Ext.getCmp('tglakhir').enable();
						} else if(val == false){
							Ext.getCmp('tglawal').disable();
							Ext.getCmp('tglakhir').disable();
							fnSearchgrid();
						}
					}
				}
			},{
				xtype: 'label', id: 'lb.lb', text: 'Tgl. Transaksi', margins: '3 10 0 0',
			},{
				xtype: 'datefield',
				id: 'tglawal',
				value: new Date(),
				format: "d-m-Y",
				width: 100, disabled: true
			},
			{
				xtype: 'label', id: 'lb.lbt', text: 's/d', margins: '3 4 0 0',
			},
			{
				xtype: 'datefield',
				id: 'tglakhir',
				value: new Date(),
				format: "d-m-Y",
				width: 100, disabled: true
			},{
				xtype: 'label', text: 'Cari Berdasarkan :', margins: '4 5 0 10',
			},{
				xtype: 'combo',
				store: ds_cari,
				id: 'cb.search',
				triggerAction: 'all',
				editable: false,
				valueField: 'id',
				displayField: 'nama',
				forceSelection: true,
				submitValue: true,
				typeAhead: true,
				mode: 'local',
				emptyText:'Pilih...',
				selectOnFocus:true,
				width: 130,
				margins: '2 5 0 0',
				value: 'keterangan',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.search').getValue();
							if(cbsearchh != ''){
								//Ext.getCmp('cek').enable();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				id: 'cek',
				width: 160,
				margins: '2 5 0 0',
				validator: function(){
					var cek = Ext.getCmp('cek').getValue();
					if(cek == ''){
						fnSearchgrid();
					}
					return;
				}
			},{ 
				xtype: 'button',
				text: 'Cari',
				id:'btn_cari',
				iconCls: 'silk-find',
				margins: '0 5 0 0',
				handler: function() {
					var btncari = Ext.getCmp('cek').getValue();
						/* if(btncari != ''){
							fnSearchgrid();
							//fTotal();
						}else{
							Ext.MessageBox.alert('Message', 'Tidak ada data yang di cari');
							//fTotalsum();
						} */
						fnSearchgrid();
					return;
				}
			}]
		}],
		height: 278,
		columnLines: true,
		//plugins: cari_data,
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('Kode<br>Jurnal'),
			width: 82,
			dataIndex: 'kdjurnal',
			sortable: true,
			align:'center',
			renderer: keyToDetil
		},
		{
			header: headerGerid('Tgl <br> Transaksi'),
			width: 66,
			dataIndex: 'tgltransaksi',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('No. Registrasi'),
			width: 97,
			dataIndex: 'noreg',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Nama Pasien'),
			width: 200,
			dataIndex: 'nmpasien',
			sortable: true,
		},{
			header: headerGerid('Bagian'),
			width: 97,
			dataIndex: 'nmbagian',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Keterangan'),
			width: 200,
			dataIndex: 'keterangan',
			sortable: true,
		},{
			header: headerGerid('Jenis Transaksi'),
			width: 227,
			dataIndex: 'nmjnstransaksi',
			sortable: true,
			hidden: true
		},
		{
			header: headerGerid('User Input'),
			width: 140,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Nominal'),
			width: 94,
			dataIndex: 'nominal',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						fnEditPengeluarankas(grid, rowIndex);
						var kdjurnal = RH.getCompValue('tf.kdjurnal', true);
						if(kdjurnal != ''){
							RH.setCompValue('tf.carikdjurnal', kdjurnal);
							Ext.getCmp('tf.reckdjurnal').setValue(kdjurnal);
						}						
						Ext.getCmp('btn_data_pen_bgn').disable();						
						return;
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						fnDeletePengeluarankas(grid, rowIndex);
                    }
                }]
        }],
		bbar: paging,
		listeners: {
			cellclick: onCellClick
		}
	});	
	
	function onCellClick(grid, rowIndex, columnIndex, event) {
		var t = event.getTarget();
		var record = grid.getStore().getAt(rowIndex);
		
		if (t.className == 'keyMasterDetail') {				
				var var_cari_kdjurnal = record.data["kdjurnal"];
				
				Ext.getCmp('tf.carinojurnal').setValue(var_cari_kdjurnal);
			return true;
		}
		return true;
	}
	
	var vw_pengeluarankasdet = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
	
	var grid_jurpengeluarankas = new Ext.grid.GridPanel({
		id: 'grid_jurpengeluarankas',
		store: ds_pengeluarankasdetail,
		view: vw_pengeluarankasdet,
		autoScroll: true,
		frame: true,
		height: 175,
		columnLines: true,
		//plugins: cari_data,
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('Kode'),
			width: 85,
			dataIndex: 'kdakun',
			sortable: true,
			align:'center'
		},{
			header: headerGerid('Nama'),
			width: 600,
			dataIndex: 'nmakun',
			sortable: true,
		},{
			header: headerGerid('Debit'),
			width: 130,
			dataIndex: 'debit',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Kredit'),
			width: 130,
			dataIndex: 'kredit',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		}],
		bbar: [
		{ xtype:'tbfill' },
		{
			xtype: 'fieldset',
			border: false,
			style: 'padding:0px; margin: 0px',
			width: 545,
			items: [{
				xtype: 'compositefield',
				items: [{
					xtype: 'label', id: 'lb.t', text: 'Jumlah :', margins: '3 10 0 5',
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahd',
					value: 0,
					width: 130,
					readOnly:true,
					style : 'opacity:0.6',
					thousandSeparator:',',					
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahk',
					value: 0,
					width: 130,
					readOnly:true,
					style : 'opacity:0.6',
					thousandSeparator:',',					
				}]
			}]
		}]			
	});
	       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Pengeluaran Kas(Patty Cash)', iconCls:'silk-calendar',
		width: 900, Height: 1000,
		//layout: 'column',
		layout: {
            type: 'form',
            pack: 'center',
            align: 'center'
        },
		frame: true,
		autoScroll: true,
		//tbar: [],
		items: [
		{
			xtype: 'fieldset', 
			title: 'Daftar Transaksi Pengeluaran Kas :',
			layout: 'form',
			height: 320,
			boxMaxHeight:330,
			items: [grid_pengeluarankas]
		},
		{
			xtype: 'fieldset', 
			title: 'Jurnal Pengeluaran Kas :',
			layout: 'form',
			height: 215,
			boxMaxHeight:225,
			items: [grid_jurpengeluarankas]
		}
		],
		listeners: {
			afterrender: mulai
		}
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	function mulai(){
		fTotaldet();
		searchSum();
	}
		
	function fTotaldet(){
		ds_pengeluarankasdetail.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sumd = 0; 
				sumk = 0; 
				ds_pengeluarankasdetail.each(function (rec) {
					sumd += parseFloat(rec.get('debit')); 
					sumk += parseFloat(rec.get('kredit')); 
				});
				Ext.getCmp("tf.jumlahd").setValue(sumd);
				Ext.getCmp("tf.jumlahk").setValue(sumk);
			}
		});
	}
	
	function reloadPengeluarankas(){
		ds_pengeluarankas.reload();
	}
	
	function fnAddPemakainabrg(){
		var grid = ds_pengeluarankas;
		wEntryPengeluarankas(false, grid, null);	
	}
	
	function fnEditPengeluarankas(grid, record){
		var record = ds_pengeluarankas.getAt(record);
		wEntryPengeluarankas(true, grid, record);
	}
	
	function fnDeletePengeluarankas(grid, record){
		var record = ds_pengeluarankas.getAt(record);
		/* var cekstatus = record.data['idstsetuju'];
		if(cekstatus!=1){
			Ext.MessageBox.alert('Informasi','Data sudah tidak bisa dihapus..');
		}else{ */
			/* var url = BASE_URL + 'pengeluarankas_controller/delete_pengeluarankas';
			var params = new Object({
							kdjurnal	: record.data['kdjurnal']
						});
			RH.deleteGridRecord(url, params, grid );			
			fTotalsum(); */
		//}
		var cekidjtransaksi = record.data['idjnstransaksi'];
		if(cekidjtransaksi != 16){
			Ext.Msg.show({
				title:'Konfirmasi',
				msg:'Hapus data yang dipilih?',
				buttons:Ext.Msg.YESNO,icon:Ext.MessageBox.QUESTION,
				fn:function(response){
					if('yes'!==response){return;}
					Ext.Ajax.request({
						url: BASE_URL + 'pengeluarankas_controller/delete_pengeluarankas',
						method:'POST',
						params: {
							kdjurnal	: record.data['kdjurnal']
						},
						success:function(){
							Ext.Msg.alert("Info","Hapus Data Berhasil");
							grid.getStore().reload();
							Ext.getCmp('tf.carinojurnal').setValue();
							searchSum();
						},
						failure:function(result){
							Ext.MessageBox.alert("Info","Hapus Data Gagal");
						}
					});
				}
			});
		}else{			
			Ext.MessageBox.alert('Informasi','Data tidak bisa dihapus..');
		}
		return;
	}
	
	/* function cetakPembrg(grid, record){
		var record = ds_pemakaianbrg.getAt(record);
		var nopakaibrg = record.data['nopakaibrg'] 
		RH.ShowReport(BASE_URL + 'print/print_pakaibrg/pakaibrg_pdf/' + nopakaibrg);
	} */
	
	/**
WIN - FORM ENTRY/EDIT 
*/

	function wEntryPengeluarankas(isUpdate, grid, record){
		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.jampakaibrg"))
					RH.setCompValue("tf.jampakaibrg",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
		
		var vw_daftar_pengeluarankas = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_daftar_pengeluarankas = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_pengeluarankas',
			store: ds_pengeluarankasdet,
			view: vw_daftar_pengeluarankas,
			tbar: [{
				text: 'Tambah',
				id: 'btn_tmbh',
				iconCls: 'silk-add',
				handler: function() {
					var cekbagian = Ext.getCmp('tf.bagian').getValue();
					if(cekbagian != ""){
						fncariBagtahunan();
					}else{
						Ext.MessageBox.alert('Informasi','Bagian belum dipilih');
					}
				}
			},{
				xtype: 'textfield',
				id:'tf.carikdjurnal',
				width: 60,
				hidden: true,
				validator: function(){
					ds_pengeluarankasdet.setBaseParam('kdjurnal', Ext.getCmp('tf.carikdjurnal').getValue());
					Ext.getCmp('grid_daftar_pengeluarankas').store.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.reckdjurnal',
				width: 60,
				hidden: true,
			}],
			autoScroll: true,
			height: 280, //autoHeight: true,
			columnLines: true,
			frame: true,
			clicksToEdit: 1,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('idakun'),
				width: 30,
				dataIndex: 'idakun',
				sortable: true,
				align:'center',
				hidden: true
			},{
				header: headerGerid('Kode'),
				width: 100,
				dataIndex: 'kdakun',
				sortable: true,
				align:'center'
			},
			{
				header: headerGerid('Nama'),
				width: 450,
				dataIndex: 'nmakun',
				sortable: true,
			},
			{
				header: headerGerid('Debit'),
				width: 120,
				dataIndex: 'debit',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
				editor: {
					xtype: 'numberfield',
					id: 'tfgp.debit',
					enableKeyEvents: true,
					listeners:{
						change:function(){
							var record = ds_pengeluarankasdet.getAt(row);
							var db = record.data.debit; //Ext.getCmp('tfgp.debit').getValue();
							var kr = record.data.kredit; //Ext.getCmp('tfgp.kredit').getValue();
							if(db == ''){
								//Ext.MessageBox.alert('Informasi','Debit Bisa Di isi..');
								record.set('debit','0');
								jumlah();
							}else if(kr != 0){
								record.set('debit','0');
								Ext.MessageBox.alert('Informasi','Debit Tidak Bisa Di isi..');
								jumlah();
							}else if(kr == 0){
								//Ext.MessageBox.alert('Informasi','Debit Bisa Di isi..');
								//Ext.getCmp('tfgp.debit').setValue();
								jumlah();
							}
							jumlah();
							//return;
						}
					}
				}
			},
			{
				header: headerGerid('Kredit'),
				width: 120,
				dataIndex: 'kredit',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
				editor: {
					xtype: 'numberfield',
					id: 'tfgp.kredit',
					enableKeyEvents: true,
					listeners:{
						change:function(){
							var record = ds_pengeluarankasdet.getAt(row);
							var db = record.data.debit; //Ext.getCmp('tfgp.debit').getValue();
							var kr = record.data.kredit; //Ext.getCmp('tfgp.kredit').getValue();
							if(kr == ''){
								//Ext.MessageBox.alert('Informasi','Debit Bisa Di isi..');
								record.set('kredit','0');
								jumlah();
							}else if(db != 0){
								//Ext.getCmp('tfgp.kredit').setValue(0);
								record.set('kredit','0');
								Ext.MessageBox.alert('Informasi','Debit Tidak Bisa Di isi..');
								jumlah();								
							}else if(db == 0){
								//Ext.MessageBox.alert('Informasi','Debit Bisa Di isi..');
								//Ext.getCmp('tfgp.kredit').setValue();
								jumlah();
							}
							jumlah();
							//return;
						}/* ,
						keyup:function(){							
							jumlah();
						} */
					}
				}
			},{
					xtype: 'actioncolumn',
					width: 50,
					header: 'Hapus',
					align:'center',
					items: [{
						getClass: function(v, meta, record) {
							meta.attr = "style='cursor:pointer;'";
						},
						icon   : 'application/framework/img/rh_delete.gif',
						tooltip: 'Hapus record',
						handler: function(grid, rowIndex) {
							/* var winTitle = (isUpdate)?'Pemakaian Barang (Edit)':'Pemakaian Barang (Entry)';
							if(winTitle == 'Pemakaian Barang (Edit)'){								
								Ext.getCmp('btn_simpan').disable();
								Ext.getCmp('btn_tmbh').disable();
							}else{
								Ext.getCmp('btn_simpan').disable();
							}*/
							ds_pengeluarankasdet.removeAt(rowIndex);
							//return; 
						}
					}]
			}],
			bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 545,
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.totalr', text: 'Jumlah :', margins: '3 10 0 65',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhdebit',
						value: 0,
						width: 120,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhkredit',
						value: 0,
						width: 120,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',
					}]
				}]
			}],
			listeners	: {
				rowclick : function(grid, rowIndex, e){
					row = rowIndex;
					jumlah();
				}
			}
		});
		
		function jumlah(){
			var debit = 0;
			var kredit = 0;
			for (var zxc = 0; zxc <ds_pengeluarankasdet.data.items.length; zxc++) {
				var record = ds_pengeluarankasdet.data.items[zxc].data;
				debit 	+= parseFloat(record.debit);
				kredit	+= parseFloat(record.kredit);
			}
			Ext.getCmp('tf.jmhdebit').setValue(debit);
			Ext.getCmp('tf.jmhkredit').setValue(kredit);
		}
		
		var winTitle = (isUpdate)?'Pengeluaran Kas(Patty Cash) (Edit)':'Pengeluaran Kas(Patty Cash) (Entry)';
		var pengeluarankas_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.pengeluarankas',
			buttonAlign: 'left',
			labelWidth: 165, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 555, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', id: 'btn_simpan', style: 'marginLeft: 5px',
				handler: function() {
					var vjmhdebit = Ext.getCmp('tf.jmhdebit').getValue();
					var vjmhkredit = Ext.getCmp('tf.jmhkredit').getValue();
					var vnominal = Ext.getCmp('tf.nominal').getValue();
					if (vjmhkredit == vnominal){
						//Ext.MessageBox.alert('Informasi','Jumlah Debit & Kredit harus sama dengan Nominal(D)');
						if (vjmhdebit != vnominal){
							//Ext.getCmp('btn_simpan').disable();
							Ext.MessageBox.alert('Informasi','Jumlah Debit & Kredit harus sama dengan Nominal');
						}else{
							//Ext.MessageBox.alert('Informasi','Jumlah Debit & Kredit Sama(D)');
							simpanPbrg();
						}
					}else if (vjmhdebit == vnominal){						
						if (vjmhkredit != vnominal){
							Ext.MessageBox.alert('Informasi','Jumlah Debit & Kredit harus sama dengan Nominal');
						}else{
							//Ext.MessageBox.alert('Informasi','Jumlah Debit & Kredit Sama(k)');
							simpanPbrg();
						}
					}else{						
						Ext.MessageBox.alert('Informasi','Jumlah Debit & Kredit tidak sama dengan Nominal');
					}
					//return;	
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', id: 'bt.cetak', style: 'marginLeft: 5px', hidden: true,
				handler: function() {
					cetak();
				}
			},{
				text: 'Batal', id: 'bt.batal',  iconCls: 'silk-cancel', style: 'marginLeft: 10px', hidden: true,
				handler: function() {
					batalPbrg();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					Ext.getCmp('tf.carikdjurnal').setValue();
					//ds_pengeluarankas.reload();
					//fTotalsum();
					wPengeluarankas.close();
			}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 185, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.49, border: false, layout: 'form',					
					items: [{
						xtype: 'compositefield',
						fieldLabel: 'Tgl Transaksi',
						items:[{
							xtype: 'datefield',
							id: 'df.tgltransaksi',
							format: 'd-m-Y',
							//value: new Date(),
							width: 120,
						},{ 	
							xtype: 'textfield',
							id: 'tf.jampakaibrg',
							width: 60, hidden: true
						}]
					},{
						xtype: 'compositefield',
						style: 'marginLeft: -48px;',
						width: 325,
						items: [{
							xtype: 'label', id: 'lb.bgn', text: 'Bagian:', margins: '3 5 0 4',
						},{
							xtype: 'textfield',
							id: 'tf.bagian',
							emptyText:'Nama Bagian..',
							width: 250,
							readOnly: true,
							allowBlank: false
						},
						{
							xtype: 'button',
							iconCls: 'silk-find',
							id: 'btn_data_pen_bgn',
							width: 3,
							handler: function() {
								fnwBagian();
							}
						},{
							xtype: 'textfield',
							id: 'tf.tamidbagian',
							width: 20,
							hidden: true,							
						}]
					},{
						xtype: 'combo',
						id: 'cb.jtransaksi', fieldLabel: 'Jenis Transaksi',
						store: ds_jnstransaksi, 
						valueField: 'idjnstransaksi', displayField: 'nmjnstransaksi',
						triggerAction: 'all', forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
						width: 250, allowBlank: false, editable: false,
					},
					{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						id: 'ta.keterangan',
						width : 250,
						height: 73,
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'Kode Jurnal',
						id:'tf.kdjurnal',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield',
						fieldLabel: 'User Input',
						id: 'tf.userinput',
						width: 210,
						readOnly: true,
						value: USERNAME,
						style : 'opacity:0.6'						
					},{
						xtype: 'datefield',
						fieldLabel: 'Tgl. Input',
						id: 'df.tglinput',
						format: 'd-m-Y',
						value: new Date(),
						width: 120,
						disabled: true
					},{
						xtype: 'textfield',
						fieldLabel: 'No. Kasir',
						id:'tf.nokasir',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield',
						fieldLabel: 'No. Reff/NO. Bon',
						id:'tf.noreff',
						width: 210,
					},{
						xtype: 'numericfield',
						fieldLabel: 'Nominal',
						id: 'tf.nominal',
						value: 0,
						width: 180,
						thousandSeparator:',',
					}]
				}]
			},{
				xtype: 'fieldset',
				title: 'Daftar jurnal Pengeluaran Kas',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 318,
				items: [grid_daftar_pengeluarankas]
			}]
		});
			
		var wPengeluarankas = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [pengeluarankas_form]
		});
		
	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setPengeluarankasForm(isUpdate, record);
		wPengeluarankas.show();

	/**
	FORM FUNCTIONS
	*/	
		function setPengeluarankasForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					Ext.Ajax.request({
						url:BASE_URL + 'pengeluarankas_controller/getNmbagiann',
						params:{
							idbagian : record.get('idbagian')
						},
						method:'POST',
						success: function(response){
							var r = Ext.decode(response.responseText);
							RH.setCompValue('tf.bagian', r);	
						}
					});
					
					RH.setCompValue('tf.kdjurnal', record.get('kdjurnal'));
					RH.setCompValue('df.tgltransaksi', record.get('tgltransaksi'));
					RH.setCompValue('tf.bagian', record.get('idbagian'));
					RH.setCompValue('tf.tamidbagian', record.get('idbagian'));
					RH.setCompValue('cb.jtransaksi', record.get('idjnstransaksi'));
					RH.setCompValue('ta.keterangan', record.get('keterangan'));
					RH.setCompValue('tf.noreff', record.get('noreff'));
					RH.setCompValue('tf.nokasir', record.get('nokasir'));
					RH.setCompValue('tf.nominal', record.get('nominal'));
					RH.setCompValue('tf.jmhdebit', record.get('nominal'));
					RH.setCompValue('tf.jmhkredit', record.get('nominal'));
					return;
				}
			}
		}
		
		function simpanPbrg(){
			var cekbagian = Ext.getCmp('tf.tamidbagian').getValue();
			
			if(cekbagian != ""){
				var reckdjurnal = Ext.getCmp('tf.reckdjurnal').getValue();
				if(reckdjurnal != ""){
					var arrjurnal = [];
					for(var zx = 0; zx < ds_pengeluarankasdet.data.items.length; zx++){
						var record = ds_pengeluarankasdet.data.items[zx].data;
						zidakun = record.idakun;
						zdebit = record.debit;
						zkredit = record.kredit;
						arrjurnal[zx] = zidakun + '-' + zdebit + '-' + zkredit ;
					}
					Ext.Ajax.request({
						url: BASE_URL + 'pengeluarankas_controller/insorupd_pengeluarankas',
						params: {
							kdjurnal		:	RH.getCompValue('tf.kdjurnal'),
							idbagian		:	RH.getCompValue('tf.tamidbagian'),
							idjnstransaksi	:	RH.getCompValue('cb.jtransaksi'),
							tgltransaksi	:	RH.getCompValue('df.tgltransaksi'),
							tgljurnal		:	RH.getCompValue('df.tgltransaksi'),
							keterangan		:	RH.getCompValue('ta.keterangan'),
							noreff			:	RH.getCompValue('tf.noreff'),
							userid			:	USERID,
							nokasir			:	RH.getCompValue('tf.nokasir'),
							nominal			:	RH.getCompValue('tf.nominal'),
							
							arrjurnal : Ext.encode(arrjurnal)
							
						},
						success: function(response){
							Ext.MessageBox.alert('Informasi','Simpan Data Berhasil');
							obj = Ext.util.JSON.decode(response.responseText);
							console.log(obj);
							Ext.getCmp("tf.kdjurnal").setValue(obj.kdjurnal);
							Ext.getCmp("tf.carikdjurnal").setValue(obj.kdjurnal);
							ds_pengeluarankasdet.setBaseParam('kdjurnal', Ext.getCmp("tf.carikdjurnal").getValue(obj.kdjurnal));
							ds_pengeluarankas.reload();
							ds_pengeluarankasdet.reload();
							Ext.getCmp('btn_tmbh').disable();
							//Ext.getCmp('btn_simpan').disable();
						}
					});
				}else{
					Ext.MessageBox.alert('Informasi','Isi data barang..');
				}
			}else{
				Ext.MessageBox.alert('Informasi','Isi data bagian terlebih dahulu..');
			}
		}
		
		function batalPbrg(){
			Ext.getCmp('cb.sttransaksi').setValue('2');
			var arrpbrg = [];
			for(var zx = 0; zx < ds_pengeluarankasdet.data.items.length; zx++){
				var record = ds_pengeluarankasdet.data.items[zx].data;
				zkdbrg = record.kdbrg;
				zqty = record.qty;
				zcatatan = record.catatan;
				arrpbrg[zx] = zkdbrg + '-' + zqty + '-' + zcatatan ;
			}
			Ext.Ajax.request({
				url: BASE_URL + 'pengeluarankas_controller/insorupd_pbrg',
				params: {
					nopakaibrg		:	RH.getCompValue('tf.nopakaibrg'),
					tglpakaibrg		:	RH.getCompValue('df.tglpakaibrg'),
					jampakaibrg		:	RH.getCompValue('tf.jampakaibrg'),
					idbagian		:	RH.getCompValue('tf.tamidbagian'),
					idsttransaksi	:	2, //RH.getCompValue('cb.sttransaksi'),
					idstsetuju		:	RH.getCompValue('cb.stsetuju'),
					userid			:	USERID,
					keterangan		:	RH.getCompValue('ta.keterangan'),
					
					arrpbrg : Ext.encode(arrpbrg)
					
				},
				success: function(response){
					Ext.MessageBox.alert('Informasi','Pemakaian barang dibatalkan..');
					obj = Ext.util.JSON.decode(response.responseText);
					console.log(obj);
					Ext.getCmp("tf.nopakaibrg").setValue(obj.nopakaibrg);
					Ext.getCmp("tf.carikdjurnal").setValue(obj.nopakaibrg);
					ds_pengeluarankasdet.setBaseParam('nopakaibrg', Ext.getCmp("tf.carikdjurnal").getValue(obj.nopakaibrg));
					Ext.getCmp('btn_tmbh').disable();
					Ext.getCmp('btn_simpan').disable();
					Ext.getCmp('bt.batal').disable();
					
					var arruqty = [];
					for(var zx = 0; zx < ds_pengeluarankasdet.data.items.length; zx++){
						var record = ds_pengeluarankasdet.data.items[zx].data;
						ukdbrg = record.kdbrg;
						uqty = record.qty;
						arruqty[zx] = Ext.getCmp('tf.nopakaibrg').getValue(obj.nopakaibrg) + '-' + Ext.getCmp('tf.jampakaibrg').getValue() + '-' + Ext.getCmp('tf.tamidbagian').getValue() + '-' + USERID + '-' + ukdbrg + '-' + uqty;
					}
					Ext.Ajax.request({
						url: BASE_URL + 'pengeluarankas_controller/update_qty_batal',
						params: {							
							tglpakaibrg		: RH.getCompValue('df.tglpakaibrg'),
							arruqty			: Ext.encode(arruqty)
							
						},							
						failure : function(){
							Ext.MessageBox.alert('Informasi','Simpan Data Gagal');
						}
					});
					//ds_pengeluarankas.reload();
					ds_pengeluarankasdet.reload();
				}
			});
		}

		/* function cetak(){
			var nopakaibrg	= Ext.getCmp('tf.nopakaibrg').getValue();			
			RH.ShowReport(BASE_URL + 'print/print_pakaibrg/pakaibrg_pdf/' + nopakaibrg);
		} */		
	}
	
	function fncariBagtahunan(){
		var ds_bagtahunan = dm_bagtahunan();		
		//ds_bagtahunan.setBaseParam('idbagian', Ext.getCmp('tf.tamidbagian').getValue());
		//ds_bagtahunan.reload();
		
		function keyToAddbrg(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		var cm_bagtahunan = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
		{
			header: 'Kode Akun',
			width: 90,
			dataIndex: 'kdakun',
			sortable: true,
			align: 'center',
			renderer: keyToAddbrg
		},
		{
			header: 'Nama Akun',
			width: 352,
			dataIndex: 'nmakun',
			sortable: true
		},
		{
			header: 'Parent',
			width: 200,
			dataIndex: 'nmparent',
			sortable: true,
			align:'center',
		},{
			header: 'Kelompok',
			width: 100,
			dataIndex: 'nmklpakun',
			sortable: true,
		}
		]);
		
		var sm_bagtahunan = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagtahunan = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagtahunan = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagtahunan,
			displayInfo: true,
			displayMsg: 'Data Bagan Tahunan Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagtahunan = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
				
		var grid_find_cari_bagtahunan = new Ext.grid.GridPanel({
			ds: ds_bagtahunan,
			cm: cm_bagtahunan,
			sm: sm_bagtahunan,
			view: vw_bagtahunan,
			height: 460,
			width: 800,
			//plugins: cari_bagtahunan,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cekk',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cekk').getValue();
							ds_bagtahunan.setBaseParam('key',  '1');
							ds_bagtahunan.setBaseParam('id',  'nmakun');
							ds_bagtahunan.setBaseParam('name',  nmcombo);
						ds_bagtahunan.load();
					}
				}]
			}],
			bbar: paging_bagtahunan,
			listeners: {
				//rowdblclick: klik_cari_bagtahunan
				cellclick: onCellClicAddbrg
			}
		});
	
		var win_find_cari_bagtahunan = new Ext.Window({
			title: 'Cari Bagan Tahunan',
			modal: true,
			items: [grid_find_cari_bagtahunan]
		}).show();		
				
		function onCellClicAddbrg(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var cek = true;
					var obj = ds_bagtahunan.getAt(rowIndex);
					var sidakun			= obj.get("idakun");
					var skdakun    	 	= obj.get("kdakun");
					var snmakun    	 	= obj.get("nmakun");
					
					Ext.getCmp("tf.reckdjurnal").setValue(skdakun);
					Ext.getCmp('btn_simpan').enable();					
					Ext.getCmp('btn_data_pen_bgn').disable();
					
					ds_pengeluarankasdet.each(function(rec){
						if(rec.get('kdakun') == skdakun) {
							Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
							cek = false;
						}
					});
					
					if(cek){
						var orgaListRecord = new Ext.data.Record.create([
							{
								name: 'idakun',
								name: 'kdakun',
								name: 'nmakun',
								name: 'debit',
								name: 'kredit',
							}
						]);
						
						ds_pengeluarankasdet.add([
							new orgaListRecord({
								'idakun'	: sidakun,
								'kdakun'	: skdakun,
								'nmakun'	: snmakun,
								'debit'		: 0,
								'kredit'	: 0,
							})
						]);
						win_find_cari_bagtahunan.close();
					}
			}
			return true;
		}
	}
	
	function fnwBagian(){
		var ds_bagian = dm_bagian();
		
		function fnkeyAddBagian(value){
			Ext.QuickTips.init();
			return '<div class="keyMasterDetail" ext:qtip="Pilih" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_bagian = new Ext.grid.ColumnModel([ new Ext.grid.RowNumberer(),
			{
				header: 'Nama Bagian',
				dataIndex: 'nmbagian',
				width: 200,
				renderer: fnkeyAddBagian
			},{
				header: 'Jenis Pelayanan',
				dataIndex: 'nmjnspelayanan',
				width: 150
			},{
				header: 'Bidang Perawatan',
				dataIndex: 'nmbdgrawat',
				width: 220
			}
		]);
		
		var sm_bagian = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		
		var vw_bagian = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var paging_bagian = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_bagian,
			displayInfo: true,
			displayMsg: 'Data Bagian Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var cari_bagian = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		
		var grid_find_cari_bagian = new Ext.grid.GridPanel({
			ds: ds_bagian,
			cm: cm_bagian,
			sm: sm_bagian,
			view: vw_bagian,
			height: 460,
			width: 620,
			//plugins: cari_bagian,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [{
				xtype: 'compositefield',
				width: 310,
				items: [{
					xtype: 'label', text: 'Search :', margins: '5 5 0 25',
				},{
					xtype: 'textfield',
					style: 'marginLeft: 7px',
					id: 'cekkk',
					width: 227,
					margins: '2 0 0 0',
					validator: function(){
						var nmcombo;
						nmcombo= Ext.getCmp('cekkk').getValue();
							ds_bagian.setBaseParam('key',  '1');
							ds_bagian.setBaseParam('id',  'nmbagian');
							ds_bagian.setBaseParam('name',  nmcombo);
						ds_bagian.load();
					}
				}]
			}],
			bbar: paging_bagian,
			listeners: {
				cellclick: onCellClickaddbagian
			}
		});
		var win_find_cari_bagian = new Ext.Window({
			title: 'Cari Pengguna Bagian',
			modal: true,
			items: [grid_find_cari_bagian]
		}).show();

		function onCellClickaddbagian(grid, rowIndex, columnIndex, event) {
			var t = event.getTarget();
			var record = grid.getStore().getAt(rowIndex);
			
			if (t.className == 'keyMasterDetail'){
					var var_cari_idbagian = record.data["idbagian"];
					var var_cari_nmbagian = record.data["nmbagian"];
								
					//Ext.getCmp('tf.bagian').focus();
					//Ext.getCmp("tf.bagian").setValue(var_cari_bagian);
					Ext.getCmp("tf.bagian").setValue(var_cari_nmbagian);
					Ext.getCmp("tf.tamidbagian").setValue(var_cari_idbagian);
					Ext.getCmp('btn_add').enable();
					//Ext.getCmp('grid_brgbagian').store.reload();
								win_find_cari_bagian.close();
				return true;
			}
			return true;
		}
	}
}
