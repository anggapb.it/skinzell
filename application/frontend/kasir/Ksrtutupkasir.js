function Ksrtutupkasir(){
	var pageSize = 18;
	var ds_tutupkasir = dm_tutupkasir();
	var ds_bagian = dm_bagian();
	var ds_stkasir = dm_stkasir();
	var ds_lappenerimaankas = dm_lappenerimaankas();
	var ds_carabayarbukakasir = dm_carabayarbukakasir();
	var ds_carabayartunai = dm_carabayartunai();
	var ds_carabayardebit = dm_carabayardebit();
	var ds_carabayarkredit = dm_carabayarkredit();
	var ds_carabayarvocher = dm_carabayarvocher();
	
	var arr_cari = [['nokuitansi', 'No. Kuitansi'],['noreg', 'No. Registrasi'],['norm', 'No. RM'],['nmpasien', 'Nama Pasien']];
	
	var ds_cari = new Ext.data.ArrayStore({
		fields: ['id', 'nama'],
		data : arr_cari 
	});
		
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'remote',
		width: 200,
		disableIndexes:['catatanbuka'],
		disableIndexes:['catatantutup'],
	})];
	
	var paging = new Ext.PagingToolbar({
		pageSize: pageSize,
		store: ds_tutupkasir,
		displayInfo: true,
		displayMsg: 'Data Tutup Kasir Dari {0} - {1} of {2}',
		emptyMsg: 'No data to display'
	});
	
	var vw_tutupkasir = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	function keyToDetil(value){
		Ext.QuickTips.init();
		return '<div class="keyMasterDetail" ext:qtip="Lihat detail kasir" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
			+ value +'</div>';
	}
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_tutupkasir',
		store: ds_tutupkasir,
		view: vw_tutupkasir,
		autoScroll: true,
		tbar: [{
			xtype: 'textfield',
			id:'tf.cekuserid',
			width: 100,
			value: USERID,
			hidden: true,
			validator: function(){
				ds_tutupkasir.setBaseParam('userid', Ext.getCmp('tf.cekuserid').getValue());
				ds_tutupkasir.reload();
			}
		}],
		height: 530, //autoHeight: true,
		columnLines: true,
		plugins: cari_data,
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('No. Kasir'),
			width: 105,
			dataIndex: 'nokasir',
			sortable: true,
			align:'center',
			renderer: keyToDetil
		},
		{
			header: headerGerid('Loket Kasir'),
			width: 120,
			dataIndex: 'nmbagian',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Tgl Buka'),
			width: 100,
			dataIndex: 'tglbuka',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},
		{
			header: headerGerid('Jam Buka'),
			width: 100,
			dataIndex: 'jambuka',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Shift Buka'),
			width: 100,
			dataIndex: 'nmshiftbuka',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Saldo Awal'),
			width: 100,
			dataIndex: 'saldoawal',
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},
		{
			header: headerGerid('Catatan </br> (Buka Kasir)'),//header: '<center>Catatan</center> </br> <center>(Buka Kasir)</center>',
			width: 290,
			dataIndex: 'catatanbuka',
			sortable: true
		},
		{
			header: headerGerid('Tgl Tutup'),
			width: 100,
			dataIndex: 'tgltutup',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},
		{
			header: headerGerid('Jam Tutup'),
			width: 100,
			dataIndex: 'jamtutup',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Shift Tutup'),
			width: 100,
			dataIndex: 'nmshifttutup',
			sortable: true,
			align:'center',
		},
		{
			header: headerGerid('Saldo Akhir'),
			width: 100,
			dataIndex: 'saldoakhir',
			sortable: true,
			align:'right',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},
		{
			header: headerGerid('Selisih'),
			width: 100,
			dataIndex: 'selisih',
			sortable: true,
			align:'left',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
		},
		{
			header: headerGerid('Catatan </br> (Tutup Kasir)'), //header: '<center>Catatan</center> </br> <center>(Tutup Kasir)</center>',
			width: 290,
			dataIndex: 'catatantutup',
			sortable: true
		},
		{
			header: headerGerid('Status </br> (Kasir)'), //header: '<center>Status</center> </br> <center>Kasir</center>',
			width: 80,
			dataIndex: 'nmstkasir',
			sortable: true,
			align:'center',
		}],
		bbar: paging,
		listeners: {
			cellclick: onCellClick
		}
	});
	
	function onCellClick(grid, rowIndex, columnIndex, event) {
		var t = event.getTarget();
		var record = grid.getStore().getAt(rowIndex);
		
		if (t.className == 'keyMasterDetail') {
				wEntryTutupkasir(true, grid, record);
				var userid = record.data['userid'];
				Ext.getCmp('tf.cekuserid').setValue(userid);
				var nokasir = record.data['nokasir'];
				Ext.getCmp('tf.ceknokasir').setValue(nokasir);
				Ext.getCmp('tf.cekjumlahtunai').setValue(nokasir);
				Ext.getCmp('tf.cekjumlahdebit').setValue(nokasir);
				Ext.getCmp('tf.cekjumlahkredit').setValue(nokasir);
				Ext.getCmp('tf.cekjumlahkvocher').setValue(nokasir);
				
				/* ds_carabayarkredit.reload({
					scope   : this,
					callback: function(records, operation, success) {
						ds_carabayarkredit.each(function (rec) {
							Ext.getCmp("nf.totalpenerimaan").setValue(parseFloat(rec.get('total_all'))); 
						});
					}
				}); */
				
				ds_lappenerimaankas.reload({
					scope   : this,
					callback: function(records, operation, success) {
						sum = 0; 
						ds_lappenerimaankas.each(function (rec) { sum += parseFloat(rec.get('jumlah')); });
						Ext.getCmp("nf.totalpenerimaan").setValue(sum);
					}
				});
				
				var cekidstkasir = Ext.getCmp('cb.stkasir').getValue();
				if(cekidstkasir == 2){
					Ext.getCmp('bt.simpan').disable();					
				}
					var cekuser = Ext.getCmp('tf.cekuserid').getValue();
					if(cekuser != USERID){
						Ext.getCmp('bt.simpan').disable();
					}
					return;					
			return true;
		}
		return true;
	}
	
	/* function fTotal(){
		ds_tpumum.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				ds_tpumum.each(function (rec) { sum += parseFloat(rec.get('total')); });
				Ext.getCmp("tf.total").setValue(sum);
			}
		});
	} */
	
	/* 	
	function fnEdit(grid, record){
		var record = ds_tutupkasir.getAt(record);
		wEntryTutupkasir(true, grid, record);
		var cekidstkasir = Ext.getCmp('cb.stkasir').getValue();
		if(cekidstkasir == 2){
			Ext.getCmp('bt.simpan').disable();
		} 
		return;
	} */
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Tutup Kasir', iconCls:'silk-calendar',
		layout: 'fit',
		items: [
		{
			xtype: 'panel',
			border: false,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadTutupkasir(){
		ds_tutupkasir.reload();
	}
	
	/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntryTutupkasir(isUpdate, grid, record){
		Ext.Ajax.request({
			url:BASE_URL + 'shift_controller/getNmField',
			method:'POST',
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.idsifttutup").setValue(obj.idshift);
				Ext.getCmp("tf.nmsifttutup").setValue(obj.nmshift);
				//Ext.getCmp('nf.selisih').setValue('0');
			}
		});
			
		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.timetutup"))
					RH.setCompValue("tf.timetutup",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
		
		var cari_kuitansi = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			autoFocus: true,
			autoHeight: true,
			position: 'top',
			mode: 'remote',
			width: 200,
			//disableIndexes:['catatanbuka'],
			//disableIndexes:['catatantutup'],
		})];
		
		var paging_kuitansi = new Ext.PagingToolbar({
			pageSize: pageSize,
			store: ds_lappenerimaankas,
			displayInfo: true,
			displayMsg: 'Data Kuitansi Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		
		var vw_kuitansi = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		function fnSearchgrid(){		
			var idcombo, nmcombo;
			idcombo= Ext.getCmp('cb.search').getValue();
			nmcombo= Ext.getCmp('cek').getValue();
				ds_lappenerimaankas.setBaseParam('key',  '1');
				ds_lappenerimaankas.setBaseParam('id',  idcombo);
				ds_lappenerimaankas.setBaseParam('name',  nmcombo);
			ds_lappenerimaankas.load();
		}
		
		var grid_kuitansi = new Ext.grid.GridPanel({
			id: 'grid_kuitansi',
			store: ds_lappenerimaankas,
			view: vw_kuitansi,
			//plugins: cari_kuitansi,
			tbar: [{
				xtype: 'textfield',
				id:'tf.cekuserid',
				width: 80,
				hidden: true,
			},{
				xtype: 'textfield',
				id:'tf.ceknokasir',
				width: 80,
				hidden: true,
				validator: function(){
					ds_lappenerimaankas.setBaseParam('nokasir', Ext.getCmp('tf.ceknokasir').getValue());
					ds_lappenerimaankas.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.cekjumlahtunai',
				width: 80,
				hidden: true,
				validator: function(){
					ds_carabayartunai.setBaseParam('nokasir', Ext.getCmp('tf.cekjumlahtunai').getValue());
					ds_carabayartunai.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.cekjumlahdebit',
				width: 80,
				hidden: true,
				validator: function(){
					ds_carabayardebit.setBaseParam('nokasir', Ext.getCmp('tf.cekjumlahdebit').getValue());
					ds_carabayardebit.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.cekjumlahkredit',
				width: 80,
				hidden: true,
				validator: function(){
					ds_carabayarkredit.setBaseParam('nokasir', Ext.getCmp('tf.cekjumlahkredit').getValue());
					ds_carabayarkredit.reload();
				}
			},{
				xtype: 'textfield',
				id:'tf.cekjumlahkvocher',
				width: 80,
				hidden: true,
				validator: function(){
					ds_carabayarvocher.setBaseParam('nokasir', Ext.getCmp('tf.cekjumlahkvocher').getValue());
					ds_carabayarvocher.reload();
				}
			},{
				xtype: 'tbtext',
				style: 'marginLeft: 10px',
				text: 'Search :'
			},{
				xtype: 'combo', 
				id: 'cb.search', width: 135, 
				store: ds_cari, 
				valueField: 'id', displayField: 'nama',
				editable: false, triggerAction: 'all',
				forceSelection: true, submitValue: true, mode: 'local',
				emptyText:'Pilih...',
				listeners: {
					select: function() {
						var cbsearchh = Ext.getCmp('cb.search').getValue();
							if(cbsearchh != ''){
								Ext.getCmp('cek').enable();
								Ext.getCmp('btn_cari').enable();
								Ext.getCmp('cek').focus();
							}
							return;
					}
				}
			},{
				xtype: 'textfield',
				style: 'marginLeft: 7px',
				id: 'cek',
				width: 175,
				disabled: true,
				validator: function(){
					var cek = Ext.getCmp('cek').getValue();
					if(cek == ''){
						fnSearchgrid();
					}
					return;
				}
			},{ 
				text: 'Cari',
				id:'btn_cari',
				iconCls: 'silk-find',
				style: 'marginLeft: 7px',
				disabled: true,
				handler: function() {
					var btncari = Ext.getCmp('cek').getValue();
						if(btncari != ''){
							fnSearchgrid();
						}else{
							Ext.MessageBox.alert('Message', 'Tidak ada data yang di cari');
						}
					return;
				}
			}],
			autoScroll: true,
			height: 205, //autoHeight: true,
			columnLines: true,
			frame: true,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: 'No. Kuitansi',
				width: 105,
				dataIndex: 'nokuitansi',
				sortable: true,
			},
			{
				header: 'Tgl. Kuitansi',
				width: 100,
				dataIndex: 'tglkuitansi',
				sortable: true,
			},
			{
				header: 'Jam Kuitansi',
				width: 100,
				dataIndex: 'jamkuitansi',
				sortable: true,
			},
			{
				header: 'Shift Kuitansi',
				width: 100,
				dataIndex: 'nmshiftkuitansi',
				sortable: true,
			},
			{
				header: 'No. Registrasi',
				width: 100,
				dataIndex: 'noreg',
				sortable: true,
			},
			{
				header: 'No. RM',
				width: 100,
				dataIndex: 'norm',
				sortable: true,
			},
			{
				header: 'Nama Pasien',
				width: 200,
				dataIndex: 'nmpasien',
				sortable: true,
			},
			{
				header: 'Atas Nama',
				width: 200,
				dataIndex: 'atasnama',
				sortable: true,
			},
			{
				header: 'Jumlah',
				width: 100,
				dataIndex: 'jumlah',
				sortable: true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			},
			{
				header: 'Status Kuitansi',
				width: 100,
				dataIndex: 'nmstkuitansi',
				sortable: true,
			}],
			bbar: paging_kuitansi
		});
		
		var grid_carabayar = new Ext.grid.GridPanel({
			id: 'grid_carabayar',
			store: ds_carabayarbukakasir,
			style: 'margin: -22px 0 3px 148px',
			height: 110, //autoHeight: true,
			width: 74,
			columnLines: true,
			border: false,
			//sm: sm_nya,
			columns: [{
				header: 'Cara Bayar',
				width: 69,
				dataIndex: 'nmcarabayar',
			}]
		});
		
		var grid_jumlahtunai = new Ext.grid.GridPanel({
			id: 'grid_jumlahtunai',
			store: ds_carabayartunai,
			style: 'margin: -113px 0 3px 216px',
			height: 49, //autoHeight: true,
			width: 89,
			columnLines: true,
			border: false,
			//sm: sm_nya,
			columns: [{
				header: 'Jumlah',
				width: 86,
				dataIndex: 'total_tunai',
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			}]
		});
		
		var grid_jumlahtunai2 = new Ext.grid.GridPanel({
			id: 'grid_jumlahtunai2',
			store: ds_carabayartunai,
			//style: 'margin: -91px 0 3px 216px',
			height: 90,//autoHeight: true,
			//width: 89,
			columnLines: true,
			//sm: sm_nya,
			columns: [{
				header: 'nokasir',
				width: 86,
				dataIndex: 'nokasir',
			},{
				header: 'nokuitansi',
				width: 86,
				dataIndex: 'nokuitansi',
			},{
				header: 'idcarabayar',
				width: 86,
				dataIndex: 'idcarabayar',
			},{
				header: 'jumlah',
				width: 86,
				dataIndex: 'jumlah',
			},{
				header: 'total_tunai',
				width: 86,
				dataIndex: 'total_tunai',
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			}]
		});
		
		var grid_jumlahdebit = new Ext.grid.GridPanel({
			id: 'grid_jumlahdebit',
			store: ds_carabayardebit,
			style: 'margin: -7px 0 3px 216px',
			height: 24, //autoHeight: true,
			width: 89,
			hideHeaders: true,
			border: false,
			//sm: sm_nya,
			columns: [{
				//header: '',
				width: 86,
				dataIndex: 'total_debit',
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			}]
		});
		
		var grid_jumlahdebit2 = new Ext.grid.GridPanel({
			id: 'grid_jumlahdebit2',
			store: ds_carabayardebit,
			height: 90,
			border: false,
			//sm: sm_nya,
			columns: [{
				header: 'nokasir',
				width: 86,
				dataIndex: 'nokasir',
			},{
				header: 'nokuitansi',
				width: 86,
				dataIndex: 'nokuitansi',
			},{
				header: 'idcarabayar',
				width: 86,
				dataIndex: 'idcarabayar',
			},{
				header: 'jumlah',
				width: 86,
				dataIndex: 'jumlah',
			},{
				header: 'total_debit',
				width: 86,
				dataIndex: 'total_debit',
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			}]
		});
		
		var grid_jumlahkredit = new Ext.grid.GridPanel({
			id: 'grid_jumlahkredit',
			store: ds_carabayarkredit,
			style: 'margin: -6px 0 4px 216px',
			height: 22, //autoHeight: true,
			width: 89,
			hideHeaders: true,
			border: false,
			//sm: sm_nya,
			columns: [{
				//header: 'total_tunai',
				width: 86,
				dataIndex: 'total_kredit',
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			}]
		});
		
		var grid_jumlahkredit2 = new Ext.grid.GridPanel({
			id: 'grid_jumlahkredit2',
			store: ds_carabayarkredit,
			//style: 'margin: -6px 0 4px 216px',
			height: 90, //autoHeight: true,
			//hideHeaders: true,
			border: false,
			//sm: sm_nya,
			columns: [{
				header: 'nokasir',
				width: 86,
				dataIndex: 'nokasir',
			},{
				header: 'nokuitansi',
				width: 86,
				dataIndex: 'nokuitansi',
			},{
				header: 'idcarabayar',
				width: 86,
				dataIndex: 'idcarabayar',
			},{
				header: 'jumlah',
				width: 86,
				dataIndex: 'jumlah',
			},{
				header: 'total_kredit',
				width: 86,
				dataIndex: 'total_kredit',
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			}]
		});
		
		var grid_jumlahvocher = new Ext.grid.GridPanel({
			id: 'grid_jumlahvocher',
			store: ds_carabayarvocher,
			style: 'margin: -5px 0 4px 216px',
			height: 23, //autoHeight: true,
			width: 89,
			hideHeaders: true,
			border: false,
			//sm: sm_nya,
			columns: [{
				//header: 'total_tunai',
				width: 86,
				dataIndex: 'total_vocher',
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			}]
		});
		
		var grid_jumlahvocher2 = new Ext.grid.GridPanel({
			id: 'grid_jumlahvocher2',
			store: ds_carabayarvocher,
			//style: 'margin: -6px 0 4px 216px',
			height: 90, //autoHeight: true,
			//hideHeaders: true,
			border: false,
			//sm: sm_nya,
			columns: [{
				header: 'nokasir',
				width: 86,
				dataIndex: 'nokasir',
			},{
				header: 'nokuitansi',
				width: 86,
				dataIndex: 'nokuitansi',
			},{
				header: 'idcarabayar',
				width: 86,
				dataIndex: 'idcarabayar',
			},{
				header: 'jumlah',
				width: 86,
				dataIndex: 'jumlah',
			},{
				header: 'total_vocher',
				width: 86,
				dataIndex: 'total_vocher',
				align:'right',
				renderer: Ext.util.Format.numberRenderer('0,000.00'),
			}]
		});
				
		var winTitle = (isUpdate)?'Tutup Kasir (Edit)':'Tutup Kasir (Entry)';
		var tutupkasir_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.tutupkasir',
			buttonAlign: 'left',
			labelWidth: 142, labelAlign: 'right',
			//bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			autoScroll: true,
			height: 600, width: 950,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 10px',
				handler: function() {
					Ext.getCmp('cek').setValue();
					Ext.getCmp('cb.search').setValue();
					ds_lappenerimaankas.reload();
					wTutupkasir.close();
				}
			},{
				text: 'Tutup Kasir', iconCls:'silk-save', id: 'bt.simpan', style: 'marginLeft: 5px',
				handler: function() {
					fnSaveTutupkasir();                           
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', style: 'marginLeft: 5px',
				handler: function() {
					
				}
			}],	
			items: [{
				xtype: 'fieldset', title: '', layout: 'column', height: 100, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.55, border: false, layout: 'form',					
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. Kasir',
						id:'tf.nokasir',
						width: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tgl/Jam/Shif Buka',
						items:[{
							xtype: 'datefield',
							id: 'df.datebuka',
							format: 'd-m-Y',
							value: new Date(),
							width: 120,
							readOnly: true,
							style : 'opacity:0.6'
						},{
							xtype: 'label', id: 'lb.time', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.timebuka',
							width: 60, 
							readOnly: true,
						},{
							xtype: 'label', id: 'lb.sift', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.nmshiftbuka',
							width: 60, 
							readOnly: true,
							style : 'opacity:0.6'
						},{ 	
							xtype: 'textfield',
							id: 'tf.idsiftbuka',
							width: 60,
							hidden: true
						}]
					},{
						xtype: 'compositefield',
						fieldLabel: 'Tgl/Jam/Shif Tutup',
						items:[{
							xtype: 'datefield',
							id: 'df.datetutup',
							format: 'd-m-Y',
							value: new Date(),
							width: 120,
							readOnly: true,
							style : 'opacity:0.6'
						},{
							xtype: 'label', id: 'lb.time', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.timetutup',
							width: 60, 
							readOnly: true,
						},{
							xtype: 'label', id: 'lb.sift', text: '/', margins: '3 10 0 5',
						},{ 	
							xtype: 'textfield',
							id: 'tf.nmsifttutup',
							width: 60, 
							readOnly: true,
							style : 'opacity:0.6'
						},{ 	
							xtype: 'textfield',
							id: 'tf.idsifttutup',
							width: 60,
							hidden: true
						}]
					}]
				},
				{
					columnWidth: 0.40, border: false, layout: 'form',
					labelWidth: 120, labelAlign: 'right',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'User ID',
						id: 'tf.idpenerima',
						width: 130,
						readOnly: true,
						value: USERID,
						style : 'opacity:0.6'
						
					},{
						xtype: 'textfield',
						fieldLabel: 'Nama Lengkap',
						id: 'tf.nmpenerima',
						width: 230,
						readOnly: true,
						value: USERNAME,
						style : 'opacity:0.6'
						
					},{
						xtype: 'combo', id: 'cb.stkasir', fieldLabel: 'Status Kasir',
						store: ds_stkasir, 
						valueField: 'idstkasir', displayField: 'nmstkasir',
						triggerAction: 'all', forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
						width: 130, allowBlank: false, editable: false,
						readOnly: true, style: 'opacity: 0.6',
						
					}]
				}]
			},{
				xtype: 'fieldset',
				title: 'Laporan Penerimaan Kas',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 243,
				items: [grid_kuitansi]
			},{
				xtype: 'fieldset',
				title: 'Tunai',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 90,
				hidden: true,
				items: [grid_jumlahtunai2]
			},{
				xtype: 'fieldset',
				title: 'Debit',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 90,
				hidden: true,
				items: [grid_jumlahdebit2]
			},{
				xtype: 'fieldset',
				title: 'Kredit',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 90,
				hidden: true,
				items: [grid_jumlahkredit2]
			},{
				xtype: 'fieldset',
				title: 'Vocher',
				layout: 'form',
				style: 'marginTop: -5px',
				height: 90,
				hidden: true,
				items: [grid_jumlahvocher2]
			},{
				xtype: 'fieldset', title: 'Summary', layout: 'column', height: 270, style: 'marginTop: -5px',
				items: [{
					columnWidth: 0.40, border: false, layout: 'form',					
					items: [{
						xtype: 'numericfield',
						fieldLabel: 'Saldo Awal',
						id:'nf.saldoawal',
						width: 158,
						readOnly: true,
						style : 'opacity:0.6;text-align: right',						
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
						//hidden: true
					},{
						xtype: 'label',
						fieldLabel: 'Total Penerimaan',
						id:'tf.saldoawal1',
					},grid_carabayar, grid_jumlahtunai, grid_jumlahdebit, grid_jumlahkredit, grid_jumlahvocher, 
					{
						xtype: 'numericfield',
						id:'nf.totalpenerimaan',
						width: 158,
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
						validator: function(){
							hitungSaldoakhir();
						}
					},{
						xtype: 'numericfield',
						fieldLabel: 'Saldo Akhir(Saldo Awal + Total Penerimaan)',
						id:'nf.saldoakhirinput',
						width: 158,
						allowBlank: false,
						/* readOnly: true,
						style : 'opacity:0.6' */
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
						enableKeyEvents: true,
						listeners:{
								keyup:function(){
									hitungSelisih();
								}
						}
					},{
						xtype: 'numericfield',
						id:'tf.saldoakhir',
						width: 158,						
						hidden: true,
						/* readOnly: true,
						style : 'opacity:0.6' */
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					},{
						xtype: 'numericfield',
						fieldLabel: 'Selisih',
						id:'nf.selisih',
						width: 158,
						/* readOnly: true,
						style : 'opacity:0.6' */
						decimalPrecision: 2,		
						decimalSeparator: ',',						
						thousandSeparator: '.',
						alwaysDisplayDecimals: true,
						useThousandSeparator: true,
					}]
				},
				{
					columnWidth: 0.60, border: false, layout: 'form',
					labelWidth: 150, labelAlign: 'right',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Catatan(Buka Kasir)',
						id:'ta.catatanbuka',
						width: 380,
						height: 120,
						readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textarea',
						fieldLabel: 'Catatan(Tutup Kasir)',
						id:'ta.catatantutup',
						width: 380,
						height: 100
					}]
				}]
			}]
		});
			
		var wTutupkasir = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [tutupkasir_form]
		});
		
		function hitungSaldoakhir(){
			var saldoawal = Ext.getCmp('nf.saldoawal').getValue();
			if(saldoawal != ''){
				var totalpenerimaan = Ext.getCmp('nf.totalpenerimaan').getValue();
				var saldoakhir = saldoawal + totalpenerimaan;
				Ext.getCmp('tf.saldoakhir').setValue(saldoakhir);
			}
		}
		
		function hitungSelisih(){
			var saldoakhir = Ext.getCmp('tf.saldoakhir').getValue();
			var saldoakhirinput = Ext.getCmp('nf.saldoakhirinput').getValue();
			if(saldoakhirinput == ''){
				Ext.getCmp('nf.selisih').setValue('0');
			}else if(saldoakhirinput != ''){
				var selisih = saldoakhirinput - saldoakhir;
				Ext.getCmp('nf.selisih').setValue(selisih);
			}
		}

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setTutupkasirForm(isUpdate, record);
		wTutupkasir.show();

	/**
	FORM FUNCTIONS
	*/	
		function setTutupkasirForm(isUpdate, record){
			if(isUpdate){
				if(record != null){
					RH.setCompValue('tf.nokasir', record.get('nokasir'));
					RH.setCompValue('df.datebuka', record.data['tglbuka']);
					RH.setCompValue('tf.timebuka', record.get('jambuka'));
					RH.setCompValue('tf.idsiftbuka', record.get('idshiftbuka'));
					RH.setCompValue('tf.nmshiftbuka', record.get('nmshiftbuka'));
					RH.setCompValue('cb.stkasir', record.get('idstkasir'));
					RH.setCompValue('nf.saldoawal', record.get('saldoawal'));
					RH.setCompValue('nf.saldoakhirinput', record.get('saldoakhir'));
					RH.setCompValue('nf.selisih', record.get('selisih'));
					RH.setCompValue('ta.catatanbuka', record.get('catatanbuka'));
					RH.setCompValue('ta.catatantutup', record.get('catatantutup'));
					return;
				}
			}
		}
		
		function fnSaveTutupkasir(){
			var idForm = 'frm.tutupkasir';
			var sUrl = BASE_URL +'tutupkasir_controller/insert_update_tutupkasir';
			var sParams = new Object({
				nokasir			:	RH.getCompValue('tf.nokasir'),
				tgltutup		:	RH.getCompValue('df.datetutup'),
				jamtutup		:	RH.getCompValue('tf.timetutup'),
				idshifttutup	:	RH.getCompValue('tf.idsifttutup'),
				saldoakhir		:	RH.getCompValue('nf.saldoakhirinput'),
				selisih			:	RH.getCompValue('nf.selisih'),
				catatantutup	:	RH.getCompValue('ta.catatantutup'),
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'tutupkasir_controller/insert_update_tutupkasir';
				msgSuccess = 'Tutup kasir berhasil';
				msgFail = 'Tutup kasir gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wTutupkasir, 
				msgWait, msgSuccess, msgFail, msgInvalid);
		}
					
	}
}
