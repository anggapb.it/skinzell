function KsrPelTambahan(){
	var myVar=setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jamshift"))
				RH.setCompValue("tf.jamshift",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}
	
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("idshift").setValue(obj.idshift);
			Ext.getCmp("tf.waktushift").setValue(obj.nmshift);
		}
	});
	
	var ds_dokter = dm_dokter();
		
	var ds_notanf = dm_nota();
	ds_notanf.setBaseParam('start',0);
	ds_notanf.setBaseParam('limit',6);
	ds_notanf.setBaseParam('idbagian',35);
	ds_notanf.setBaseParam('koder',2);
	ds_notanf.setBaseParam('grupby',true);
	ds_notanf.setBaseParam('ctglnota',Ext.util.Format.date(new Date(), 'Y-m-d'));
	
	var ds_nota = dm_nota();
	ds_nota.setBaseParam('start',0);
	ds_nota.setBaseParam('limit',500);
	ds_nota.setBaseParam('nonota',null);
	ds_nota.setBaseParam('idbagian',999);
	
	var ds_brgbagian = dm_brgbagian();
	ds_brgbagian.setBaseParam('start',0);
	ds_brgbagian.setBaseParam('limit',6);
	ds_brgbagian.setBaseParam('cidbagian',11);
	
	var ds_tarif = dm_tarif();
	ds_tarif.setBaseParam('start',0);
	ds_tarif.setBaseParam('limit',3);
	ds_tarif.setBaseParam('idklstarif',8);
	
	var ds_kuitansidet = dm_kuitansidet();
	ds_kuitansidet.setBaseParam('blank', 1);
	
	var rownota = '';
	var pmbyrn = 0;
	var gtot = 0;
    
	var grid_nota = new Ext.grid.EditorGridPanel({
		store: ds_nota,
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_nota',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				rownota = rowIndex;
            }
        },
		columns: [{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Hapus',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_delete.gif',
				tooltip: 'Hapus record',
				handler: function(grid, rowIndex) {
					ds_nota.removeAt(rowIndex);
					hitung();
					cektombol();
				}
			}]
		},{
			header: '<div style="text-align:center;">Item Tindakan</div>',
			dataIndex: 'nmitem',
			width: 170,
			sortable: true
		},{
			header: '<div style="text-align:center;">Dokter</div>',
			dataIndex: 'nmdoktergelar',
			width: 110,
			editor: {
				xtype: 'combo',
				id: 'cb.dokter',
				store: ds_dokter, valueField: 'nmdoktergelar', displayField: 'nmdoktergelar',
				editable: false, triggerAction: 'all',
				forceSelection: true, mode: 'local',
			},
			sortable: true,
		},{
			header: '<div style="text-align:center;">Tarif</div>',
			dataIndex: 'tarif',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Qty</div>',
			dataIndex: 'qty',
			align:'right',
			width: 40,
			editor: {
				xtype: 'numberfield',
				id: 'tf.notaqty',
				enableKeyEvents: true,
				listeners:{
					keyup:function(){
						var record = ds_nota.getAt(rownota);
						var subtotal = record.data.tarif * Ext.getCmp('tf.notaqty').getValue();
						var subtotal2 = record.data.drp;
						var jsubtotal = subtotal - subtotal2;
						record.set('tarif2',jsubtotal);
						hitung();
						cektombol();
					}
				}
			},
			sortable: true
		},{
			header: '<div style="text-align:center;">Satuan</div>',
			dataIndex: 'nmsatuan',
			width: 80,
			sortable: true
		},{
			xtype: 'actioncolumn',
			width: 42,
			header: 'Diskon',
			align:'center',
			items: [{
				getClass: function(v, meta, record) {
					meta.attr = "style='cursor:pointer;'";
				},
				icon   : 'application/framework/img/rh_edit.png',
				handler: function(grid, rowIndex) {
					fDiskon(rowIndex);
				}
			}]
		},{
			header: '<div style="text-align:center;">Total<br>Diskon</div>',
			dataIndex: 'drp',
			align:'right',
			width: 55,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			header: '<div style="text-align:center;">Subtotal</div>',
			dataIndex: 'tarif2',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		},{
			dataIndex: 'kditem',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'koder',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idjnstarif',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'idtarifpaketdet',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjs',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjm',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifjp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'tarifbhp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjs',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjm',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonjp',
			hidden: true,
			hideable: false
		},{
			dataIndex: 'diskonbhp',
			hidden: true,
			hideable: false
		}],
		bbar: [
			{ xtype:'tbfill' },			
			'Jumlah : ',
			{
				xtype: 'numericfield',
				id: 'tf.total',
				value: 0,
				readOnly:true,
				style : 'opacity:0.6',
				width: 100,
				thousandSeparator:','
			}
		]
	});
	
	var grid_pembayaran = new Ext.grid.GridPanel({
		store: ds_kuitansidet,
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_pembayaran',
		forceFit: true,
		autoScroll: true,
		columnLines: true,
		loadMask: true,
		tbar: [
			{
				text: 'Tambah',
				id: 'btn_add',
				iconCls: 'silk-add',
				handler: function() {
					fPemb();
				}
			},
			{ xtype:'tbfill' }
		],
		columns: [{
                xtype: 'actioncolumn',
                width: 42,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex, colIndex) {
						var rec = ds_kuitansidet.getAt(rowIndex);
						pmbyrn -= rec.data.jumlah;
						ds_kuitansidet.removeAt(rowIndex);
						Ext.getCmp('tf.jumlah').setValue(pmbyrn);
						cektombol();
                    }
                }]
        },{
			header: 'Bayar',
			width: 100,
			dataIndex: 'nmcarabayar',
			sortable: true
		},{
			header: 'Bank',
			width: 70,
			dataIndex: 'nmbank',
			sortable: true
		},{
			header: 'No. Kartu',
			width: 100,
			dataIndex: 'nokartu',
			sortable: true
		},{
			header: 'Total',
			width: 100,
			dataIndex: 'jumlah',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000'
		}],
		bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				width: 300,
				style: 'padding:0px; margin: 0px;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.jml', text: 'Jumlah Total :', margins: '0 5 0 7',
					},{
						xtype: 'numericfield',
						id: 'tf.jumlah',
						width: 90,
						readOnly:true, style: 'opacity:0.6'
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.ua', text: 'Uang Diterima :', margins: '0 4 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.utyd',
						width: 90,
						enableKeyEvents: true,
						listeners:{
							keyup:function(){
								hitungKembalian();
							}
						}
					}]
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.km', text: 'Kembalian :', margins: '0 5 0 16',
					},{
						xtype: 'numericfield',
						id: 'tf.kembalian',
						width: 90,
						readOnly:true, style: 'opacity=0.6'
					}]
				}]
			}
		]
	});
	
	var paging_reg = new Ext.PagingToolbar({
		pageSize: 6,
		store: ds_notanf,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 120
	})];
	
	var grid_reg = new Ext.grid.GridPanel({
		title: 'Daftar Nota Pelayanan Tambahan',
		store: ds_notanf,
		frame: true,
		height: 260,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_reg',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[{
			xtype: 'label', id: 'lb.tgl', text: 'Tgl :'
		},{
			xtype: 'datefield', id: 'df.tglnota',
			width: 100, value: new Date(),
			format: 'd-m-Y',
			listeners:{
				select: function(field, newValue){
					ds_notanf.setBaseParam('ctglnota',Ext.util.Format.date(newValue, 'Y-m-d'));
					Ext.getCmp('grid_reg').store.reload();
					//ds_notanf.reload();
				}
			}
		}],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_nota = ds_notanf.getAt(rowIndex);
				var rnonota = rec_nota.data["nonota"];
				var ratasnama = rec_nota.data["atasnama"];
				var rnmdokter = rec_nota.data["nmdoktergelar"];
				var rcatatann = rec_nota.data["catatann"];
				var rcatatand = rec_nota.data["catatand"];
				var rnokuitansi = rec_nota.data["nokuitansi"];
				
				Ext.getCmp("tf.nonota").setValue(rnonota);
				Ext.getCmp("tf.nokuitansi").setValue(rnokuitansi);
				Ext.getCmp("tf.nmpasien").setValue(ratasnama);
				Ext.getCmp("tf.dokter").setValue(rnmdokter);
				Ext.getCmp("tf.catatan").setValue(rcatatann);
				Ext.getCmp("tf.catatandskn").setValue(rcatatand);
				
				ds_nota.setBaseParam('nonota',rnonota);
				ds_nota.setBaseParam('koder',0);
				ds_nota.setBaseParam('idbagian',null);
				ds_nota.reload({
					scope   : this,
					callback: function(records, operation, success) {
						hitung()
					}
				});
				ds_kuitansidet.setBaseParam('blank', 0);
				ds_kuitansidet.setBaseParam('nokuitansi',rnokuitansi);
				ds_kuitansidet.reload({
					scope   : this,
					callback: function(records, operation, success) {
						sum = 0;
						ds_kuitansidet.each(function (rec) {
							sum += (parseFloat(rec.get('jumlah')));
						});
						Ext.getCmp('tf.jumlah').setValue(sum);
						pmbyrn += sum;
						cektombol();
					}
				});
				Ext.getCmp("btn_add").disable();
				Ext.getCmp("bt.simpan").disable();
				Ext.getCmp("bt.cetak").enable();
				Ext.getCmp("bt.batal").enable();
            }
        },
		columns: [{
			header: 'No. Nota',
			dataIndex: 'nonota',
			width: 90
		},{
			header: 'Tgl. Nota',
			dataIndex: 'tglnota',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			/* renderer: function(value, p, r){
				var norm = '';
				Ext.Ajax.request({
					url:BASE_URL + 'pasien_controller/getNoRmPL',
					method:'POST',
					params:{
						nama : r.data['atasnama']
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						norm = obj.norm ;
						return 'zx';
					}
				});
			}, */
			width: 50, hidden: true
		},{
			header: 'Nama',
			dataIndex: 'atasnama',
			width: 150
		}],
		bbar: paging_reg,
		plugins: cari_reg
	});
	
	var paging_tindakan = new Ext.PagingToolbar({
		pageSize: 3,
		store: ds_tarif,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_tindakan = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230,
		disable: 'ttltarif'
	})];
	
	var grid_tindakan = new Ext.grid.GridPanel({
		title: 'Tindakan',
		store: ds_tarif,
		frame: true,
		height: 190,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_tindakan',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var cek = true;
				var obj = ds_tarif.getAt(rowIndex);
				var skdpelayanan	= obj.data["kdpelayanan"];
				var snmpelayanan	= obj.data["nmpelayanan"];
				var starifjs		= obj.data["tarifjs"];
				var starifjm		= obj.data["tarifjm"];
				var starifjp		= obj.data["tarifjp"];
				var starifbhp		= obj.data["tarifbhp"];
				var starif			= obj.data["ttltarif"];
				var snmsatuan		= obj.data["satuankcl"];
				
				ds_nota.each(function(rec){
					if(rec.get('kditem') == skdpelayanan) {
						Ext.MessageBox.alert('Informasi', 'Data Sudah Ditambahkan');
						cek = false;
					}
				});
				
				if(cek){
					var orgaListRecord = new Ext.data.Record.create([
						{
							name: 'kditem',
							name: 'koder',
							name: 'nmitem',
							name: 'qty',
							name: 'tarif',
							name: 'tarif2',
							name: 'nmsatuan',
							name: 'tarifjs',
							name: 'tarifjm',
							name: 'tarifjp',
							name: 'tarifbhp',
							name: 'diskonjs',
							name: 'diskonjm',
							name: 'diskonjp',
							name: 'diskonbhp'
						}
					]);
					
					ds_nota.add([
						new orgaListRecord({
							'kditem': skdpelayanan,
							'koder': null,
							'nmitem': snmpelayanan,
							'nmdokter':Ext.getCmp('tf.dokter').getValue(),
							'qty': 1,
							'tarif': starif,
							'tarif2': starif,
							'drp': 0,
							'nmsatuan': null,
							'tarifjs': starifjs,
							'tarifjm': starifjm,
							'tarifjp': starifjp,
							'tarifbhp': starifbhp,
							'diskonjs': 0,
							'diskonjm': 0,
							'diskonjp': 0,
							'diskonbhp': 0
						})
					]);
					var ttl = Ext.getCmp('tf.total').getValue();
					var jml = parseFloat(ttl) + parseFloat(starif);
					Ext.getCmp('tf.total').setValue(jml);
					grid_nota.getView().focusRow(ds_nota.getCount() - 1);
				}
				cektombol();
            }
        },
		columns: [{
			header: 'Item Tindakan',
			dataIndex: 'nmpelayanan',
			width: 160,
			sortable: true
		},{
			header: 'Harga',
			dataIndex: 'ttltarif',
			align:'right',
			width: 80,
			sortable: true,
			xtype: 'numbercolumn', format:'0,000.00'
		}],
		bbar: paging_tindakan,
		plugins: cari_tindakan
	});
	
	var transaksipl_form = new Ext.form.FormPanel({ 
		id: 'fp.transaksipl',
		//title: 'Nota Pelayanan Tambahan',
		width: 900, Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			{ text: 'Baru', iconCls: 'silk-add', handler: function(){bersihnf();} },'-',
			{ text: 'Simpan', id:'bt.simpan', disabled:true, iconCls: 'silk-save', handler: function(){simpan("fp.transaksipl");} },'-',
			{ text: 'Batal', id:'bt.batal', iconCls: 'silk-cancel', disabled: true, handler: function(){fbatal();} },'-',
			/*{ text: 'Cari Transaksi Farmasi', iconCls: 'silk-find', handler: function(){cariRegNF();} },'-',*/
			{ text: 'Cetak', id:'bt.cetak', iconCls: 'silk-printer', disabled: true, handler: function(){cetakNotaFPL();} },'-',
			{xtype: 'tbfill' }
		],
		defaults: { labelWidth: 150, labelAlign: 'right'},
        items: [{
			//COLUMN 1
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:130,
				boxMaxHeight:130,
				style : 'margin-bottom:0;', 
				items: [{
					xtype: 'compositefield', hidden: true,
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. RM',
						id: 'tf.norm',
						width: 80,
						maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
						enableKeyEvents: true,
						listeners:{
							specialkey: function(field, e){
								if (e.getKey() == e.ENTER) {
									dataPasien();
								}
							}
						}
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Nama Pasien',
					id: 'tf.nmpasien', allowBlank: false,
					anchor: "100%",
					value: 'Pasien Luar'
				},{
					xtype: 'compositefield',
					fieldLabel: 'Dokter',
					items: [{
						xtype: 'textfield',
						id: 'tf.dokter', width: 210, readOnly: true
					},{
						xtype: 'button',
						text: ' ... ',
						id: 'btn.dokter',
						width: 40,
						handler: function() {
							dftDokter();
						}
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Unit Pelayanan',
					id: 'tf.upenunjuang', allowBlank: false,
					value:'Pelayanan Tambahan', readOnly: true,
					anchor: "100%", readOnly: true,
					style : 'opacity:0.6'
				}]
			}]
		},{
			//COLUMN 2
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:130,
				boxMaxHeight:130,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. Nota',
						id: 'tf.nonota',
						width: 100, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield',
						id: 'tf.nokuitansi',
						width: 100, readOnly: true,
						style : 'opacity:0.6', hidden: true
					}]
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel: 'Tgl./Jam/Shift',id: 'df.tglshift',
						width: 100, value: new Date(),
						format: 'd-m-Y'
					},{
						xtype: 'label', id: 'lb.garing1', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jamshift', readOnly:true,
						width: 65
					},{
						xtype: 'label', id: 'lb.garing2', text: '/'
					},{
						xtype: 'textfield', id: 'tf.waktushift', 
						width: 60, readOnly: true,
						style : 'opacity:0.6'
					},{
						xtype: 'textfield', id: 'idshift',
						hidden:true
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Catatan',
					id: 'tf.catatan', anchor: "100%", value: 'Pelayanan Tambahan'
				},{
					xtype: 'textfield', fieldLabel: 'Catatan Diskon',
					id: 'tf.catatandskn', anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
				layout: 'fit',
				border: false,
				items: [grid_nota]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_pembayaran]
				}]
		}]
	});
	
	var FPLuarDispPanel = new Ext.form.FormPanel({
		id: 'fp.transRJDispPanel',
		name: 'fp.transRJDispPanel',
		border: false, 
		forceFit:true,
		frame: true,
		autoScroll:true,
		labelAlign: 'top',
		layout: 'anchor',
		items: [
		{
			layout: 'form',
			bodyStyle: 'padding:1px 5px 5px 5px',
			items: [grid_reg]
		},{
			layout: 'form',
			bodyStyle: 'padding:3px 5px 5px 5px',
			items: [grid_tindakan]
		}]
	});
	
	var FPLuarPanel = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		title: 'Transaksi Pelayanan Tambahan',
		frame: false,
		border: true,
		margins: '0 0 5 0',
		plain: true,		
		forceFit: true,
		layout: 'border',
		items: [{
			region: 'center',
			xtype: 'panel',
			border: true,
			layout: 'fit',
			items: [transaksipl_form],
		},
		{
			region: 'east',
			xtype: 'panel',
			title: '', border: true,
			layout:'fit', width:350,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [FPLuarDispPanel],
		}],	
		listeners: {
			afterrender: function () {
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/get_tgl_svr',
					method: 'POST',
					params: {
					
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp('df.tglshift').setValue(obj.date);
					},
					failure : function(){
						Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
					}
				});
			}
		}
	});
	SET_PAGE_CONTENT(FPLuarPanel);
	Ext.getCmp('tf.nmpasien').focus();
	
	function cetakNotaFPL(){
		var nonota		= Ext.getCmp('tf.nonota').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/nota_ptambahan/'
                +nonota);
	}
	
	function simpan(namaForm) {
		if(Ext.getCmp('tf.jumlah').getValue() != Ext.getCmp('tf.total').getValue()){
			Ext.MessageBox.alert('Informasi', 'Jumlah tidak sama dengan Jumlah Total');
		} else {
			var waitmsg = Ext.MessageBox.wait('Loading....', 'Info');
			var arrnota = [];
			var arrcarabayar = [];
			var zx = 0;
			var zv = 0;
			ds_nota.each(function (rec) {
				if(rec.get('nmdoktergelar') == null || rec.get('nmdoktergelar') == ''){
					dr = '';
				} else {
					var arrdr = rec.get('nmdoktergelar');
					var dr = arrdr.split(',', 1);
				}
				zkditem = rec.get('kditem');
				zqty = rec.get('qty');
				zkoder = null;
				ztarif2 = rec.get('tarif2');
				zdiskonjs = rec.get('diskonjs');
				zdiskonjm = rec.get('diskonjm');
				zdiskonjp = rec.get('diskonjp');
				zdiskonbhp = rec.get('diskonbhp');
				zdokter = dr;
				arrnota[zx] = zkditem+'-'+zqty+'-'+zkoder+'-'+ztarif2+'-'+zdiskonjs+'-'+zdiskonjm+'-'+zdiskonjp+'-'+zdiskonbhp+'-'+zdokter;
				zx++;
			});
			
			ds_kuitansidet.each(function (rec) {
				znmcarabayar = rec.get('nmcarabayar');
				znmbank = rec.get('nmbank');
				znokartu = rec.get('nokartu');
				zjumlah = rec.get('jumlah');
				arrcarabayar[zv] = znmcarabayar+'-'+znmbank+'-'+znokartu+'-'+zjumlah;
				zv++;
			});
			
			var form_nya = Ext.getCmp(namaForm);
			form_nya.getForm().submit({
				url: BASE_URL + 'nota_controller/insert_pelayanantambahan',
				method: 'POST',
				params: {
					ureg 		: 'PT',
					tglnota		: Ext.getCmp('df.tglshift').getValue().format('Y-m-d'),
					jtransaksi	: 10,
					idklstarif	: 8,
					arrnota		: Ext.encode(arrnota),
					arrcarabayar: Ext.encode(arrcarabayar)
				},
				success: function(transaksipl_form, o) {
					if (o.result.success==true) {
						waitmsg.hide();
						Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
						Ext.getCmp('tf.nonota').setValue(o.result.nonota);
						Ext.getCmp('tf.nokuitansi').setValue(o.result.nokuitansi);
						Ext.getCmp('bt.simpan').disable();
						Ext.getCmp('bt.cetak').enable();
						Ext.getCmp('bt.batal').enable();
						myStopFunction();
						ds_notanf.reload();
						
						var zx = 0;
						ds_nota.each(function(rec){
							zjkartustok 		= 3;
							ztglkartustok 		= Ext.getCmp('df.tglshift').getValue().format('Y/m/d');
							zjamkartustok 		= Ext.getCmp('tf.jamshift').getValue();
							zjmlkeluar	 		= rec.get('qty');
							znama		 		= 'INSTALASI FARMASI';
							zkditem				= rec.get('kditem');
							arrnota[zx] = zjkartustok+'-'+ztglkartustok+'-'+zjamkartustok+'-'+zjmlkeluar+'-'+znama+'-'+zkditem;
							zx++;
						});
						
					} else {
						waitmsg.hide();
						Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
					}
				},
				failure: function (form, action) {
					waitmsg.hide();
					Ext.MessageBox.alert('Informasi', 'Simpan Data Gagal');
				}
			});
		}
	}

	function bersihnf() {
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue('Pasien Luar');
		Ext.getCmp('tf.dokter').setValue();
		Ext.getCmp('tf.nonota').setValue();
		Ext.getCmp('tf.nokuitansi').setValue();
		//Ext.getCmp('df.tglshift').setValue(new Date());
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/get_tgl_svr',
			method: 'POST',
			params: {
			
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp('df.tglshift').setValue(obj.date);
			},
			failure : function(){
				Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
			}
		});
		Ext.getCmp('tf.catatan').setValue('Pelayanan Tambahan');
		Ext.getCmp('tf.catatandskn').setValue();
		Ext.getCmp('tf.total').setValue(0);
		Ext.getCmp('tf.jumlah').setValue(0);
		Ext.getCmp('tf.utyd').setValue();
		Ext.getCmp('tf.kembalian').setValue();
		Ext.getCmp('bt.simpan').enable();
		Ext.getCmp('bt.batal').disable();
		Ext.getCmp('btn_add').enable();
		Ext.getCmp('bt.cetak').disable();
		ds_nota.setBaseParam('nonota',null);
		ds_nota.setBaseParam('koder',null);
		ds_nota.setBaseParam('idbagian',999);
		ds_nota.reload();
		ds_kuitansidet.setBaseParam('blank', 1);
		ds_kuitansidet.reload();
		pmbyrn = 0;
		myVar = setInterval(function(){myTimer()},1000);
	}
	
	function dataPasien(){
		Ext.Ajax.request({
			url: BASE_URL + 'pasien_controller/getDataPasien',
			params: {
				norm		: Ext.getCmp('tf.norm').getValue()
			},
			success: function(response){
				obj = Ext.util.JSON.decode(response.responseText);
				var var_cari_pasienno = obj.norm;
				var var_cari_pasiennm = obj.nmpasien;
					
				Ext.getCmp('tf.norm').focus()
				Ext.getCmp("tf.norm").setValue(parseInt(var_cari_pasienno));
				Ext.getCmp("tf.nmpasien").setValue(var_cari_pasiennm);
			},
			failure: function() {
				//Ext.Msg.alert("Informasi", "Ubah Data Gagal");
			}
		});
	}
	
	function fbatal(){
		var nonota = Ext.getCmp('tf.nonota').getValue();
		var nokuitansi = Ext.getCmp('tf.nokuitansi').getValue();
		var btl_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'fp.btl',
			buttonAlign: 'left',
			labelWidth: 115, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 100, width: 325,
			layout: 'form', 
			frame: false,
			items: [{
				xtype: 'textfield', fieldLabel:'No. Nota',
				id: 'tf.bnota', width: 150, value: nonota, hidden: true
			},{
				xtype: 'textfield', fieldLabel:'No. Kuitansi',
				id: 'tf.bkui', width: 150, value: nokuitansi, hidden: true
			},{
				xtype: 'textfield', fieldLabel:'Masukan Password',
				id: 'tf.pwdbatal', width: 150, inputType: 'password',
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					if(Ext.getCmp('tf.pwdbatal').getValue() !=''){
						Ext.Msg.show({
							title: 'Konfirmasi',
							msg: 'Apakah Anda Yakin Akan Membatalakan Transaksi Ini..?',
							buttons: Ext.Msg.YESNO,
							icon: Ext.MessageBox.QUESTION,
							fn: function (response) {
								if ('yes' == response) {	
									batal("fp.btl");
								}
							}
						});		
					}else{
						Ext.MessageBox.alert("Informasi", "Anda Belum Isi Password");
					}
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wBatal.close();
				}
			}]
		});
			
		var wBatal = new Ext.Window({
			title: 'Batal',
			modal: true, closable:false,
			items: [btl_form]
		});
		
		wBatal.show();		
	
		function batal(namaForm){
			var form_nya = Ext.getCmp(namaForm);				
			if (form_nya.getForm().isValid()) {
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/batal_transpeltambahan',
					method: 'POST',
					params: {
						nonota	: Ext.getCmp('tf.bnota').getValue(),
						nokui	: Ext.getCmp('tf.bkui').getValue(),
						passbatal:Ext.getCmp('tf.pwdbatal').getValue(),
					},
					success: function(response) {
						var r = response.responseText;
						Ext.MessageBox.alert("Informasi", r);					
						if (r == "Batal Berhasil") {							
							Ext.getCmp("bt.simpan").disable();
							Ext.getCmp("bt.batal").disable();
							Ext.getCmp("bt.cetak").disable();
							ds_notanf.reload();
						}
					},
					failure: function() {
						Ext.MessageBox.alert("Informasi", "Batal Gagal");
					}
				});			
			}else if (!form_nya.getForm().isValid()) {
				Ext.MessageBox.alert("Informasi", "Lengkapi");
			}
			wBatal.close();
		}
	}
	
	function dftDokter(){
		function viewDokter(value){
			Ext.QuickTips.init();
			return '<div class="keyDokter" style="cursor:pointer;color:#000099;font-size:11px;font-family:Arial;text-decoration:underline;">'
				+ value +'</div>';
		}
		
		var cm_dokter = new Ext.grid.ColumnModel([
			{
				hidden:true,
				dataIndex: 'iddokter'
			},{
				header: 'Nama Dokter',
				dataIndex: 'nmdoktergelar',
				width: 200,
				renderer: viewDokter
			},{
				header: 'Spesialisasi',
				dataIndex: 'nmspesialisasi',
				width: 200
			}
		]);
		var sm_dokter = new Ext.grid.RowSelectionModel({
			singleSelect: true
		});
		var vw_dokter = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		var paging_dokter = new Ext.PagingToolbar({
			pageSize: 18,
			store: ds_dokter,
			displayInfo: true,
			displayMsg: 'Data Dokter Dari {0} - {1} of {2}',
			emptyMsg: 'No data to display'
		});
		var cari_dokter = [new Ext.ux.grid.Search({
			iconCls: 'btn_search',
			minChars: 1,
			position: 'top',
			mode: 'remote',
			width: 200
		})];
		var grid_dokter= new Ext.grid.GridPanel({
			ds: ds_dokter,
			cm: cm_dokter,
			sm: sm_dokter,
			view: vw_dokter,
			height: 400,
			width: 430,
			plugins: cari_dokter,
			autoSizeColumns: true,
			enableColumnResize: true,
			enableColumnHide: false,
			enableColumnMove: false,
			enableHdaccess: false,
			columnLines: true,
			loadMask: true,
			buttonAlign: 'left',
			layout: 'anchor',
			anchorSize: {
				width: 400,
				height: 400
			},
			tbar: [],
			bbar: paging_dokter,
			listeners: {
				cellclick: klik_dokter
			}
		});
		var win_dokter = new Ext.Window({
			title: 'Cari Dokter',
			modal: true,
			items: [grid_dokter]
		}).show();

		function klik_dokter(grid, rowIdx, columnIndex, event){
			var t = event.getTarget();
			if (t.className == 'keyDokter'){
				var rec_dokter = ds_dokter.getAt(rowIdx);
				var var_nmdokterglr = rec_dokter.data["nmdoktergelar"];
				
				Ext.getCmp("tf.dokter").setValue(var_nmdokterglr);
							win_dokter.close();
			}
		}
	}
	
	function hitung(){
		var arr = [];
		var xxx = 0;
		for (var zxc = 0; zxc <ds_nota.data.items.length; zxc++) {
			var record = ds_nota.data.items[zxc].data;
			zkditem = record.kdbrg;
			xxx += parseFloat(record.tarif2);
			arr[zxc] = zkditem;
		}
		Ext.getCmp("tf.total").setValue(xxx);
	}
	
	function fDiskon(rowIndex){
		var rec_nota = ds_nota.getAt(rowIndex);
		var jumlahjs =0;
		var jumlahjm =0;
		var jumlahjp =0;
		var jumlahbhp =0;
		var tariftot =0;
		var dprsntot =0;
		var drptot =0;
		var jumlahtot =0;
		var dprsnjs = 0;
		var dprsnjm = 0;
		var dprsnjp = 0;
		var dprsnbhp = 0;
		
		var disk_form = new Ext.form.FormPanel({
			xtype:'form',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px',
			monitorValid: true,
			height: 270, width: 400,
			layout: 'form', 
			frame: false,
			items: [{
				xtype: 'textfield', fieldLabel:'Nama',
				id: 'tf.nmitemd', width: 200,
				value: rec_nota.data.nmitem,
				disabled:true
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon Seluruh',
				items: [{
					xtype: 'numericfield',
					id: 'tf.dprsnall', width: 40,
					thousandSeparator:',', value:0,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgPrsnAll();
						}
					}
				},{
					xtype: 'label', id: 'lb.dprsnall', text: '%', margins: '0 20 0 2'
				},{
					xtype: 'numericfield',
					id: 'tf.drpall', width: 70, value:0,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgRpAll();
						}
					}
				},{
					xtype: 'label', id: 'lb.drpall', text: 'Rupiah', margins: '0 0 0 2'
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Keterangan',
				items: [{
					xtype: 'numericfield',
					id: 'nm.tes',
					hidden:true
				},{
					xtype: 'label', id: 'lb.tarif', text: 'Tarif', margins: '0 10 0 20'
				},{
					xtype: 'label', id: 'lb.dprsn', text: '%', margins: '0 10 0 30'
				},{
					xtype: 'label', id: 'lb.drp', text: 'Rupiah', margins: '0 10 0 20'
				},{
					xtype: 'label', id: 'lb.jumlah', text: 'Jumlah', margins: '0 10 0 30'
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JS',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjs', width: 70,
					value: rec_nota.data.tarifjs * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjs', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjs', width: 70,
					value: rec_nota.data.diskonjs,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjs', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JM',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjm', width: 70,
					value: rec_nota.data.tarifjm * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjm', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjm', width: 70,
					value: rec_nota.data.diskonjm,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjm', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon JP',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifjp', width: 70,
					value: rec_nota.data.tarifjp * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnjp', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpjp', width: 70,
					value: rec_nota.data.diskonjp,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahjp', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Diskon BHP',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tarifbhp', width: 70,
					value: rec_nota.data.tarifbhp * parseInt(rec_nota.data.qty),
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsnbhp', width: 40,
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonPrsn();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.drpbhp', width: 70,
					value: rec_nota.data.diskonbhp,
					thousandSeparator:',',
					enableKeyEvents: true,
					listeners:{
						keyup:function(){
							htgDiskonRp();
						}
					}, disabled: true
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahbhp', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			},{
				xtype: 'compositefield',
				fieldLabel:'Total',
				items: [{
					xtype: 'numericfield',
					id: 'tf.tariftot', width: 70,
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.dprsntot', width: 40,
					disabled:true
				},{
					xtype: 'numericfield',
					id: 'tf.drptot', width: 70,
					disabled:true,
					thousandSeparator:','
				},{
					xtype: 'numericfield',
					id: 'tf.jumlahtot', width: 70,
					disabled:true,
					thousandSeparator:','
				}]
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					DiskAdd();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wDisk.close();
				}
			}]
		});
			
		var wDisk = new Ext.Window({
			title: 'Diskon',
			modal: true, closable:false,
			items: [disk_form]
		});
		
		diskonAwal();
		wDisk.show();
		function DiskAdd(){
			var totaldiskon = Ext.getCmp('tf.drptot').getValue();
			var subtotal = (rec_nota.data.tarif * rec_nota.data.qty) -  Ext.getCmp('tf.drptot').getValue();
			
			rec_nota.set('diskonjs', Ext.getCmp('tf.drpjs').getValue());
			rec_nota.set('diskonjm', Ext.getCmp('tf.drpjm').getValue());
			rec_nota.set('diskonjp', Ext.getCmp('tf.drpjp').getValue());
			rec_nota.set('diskonbhp', Ext.getCmp('tf.drpbhp').getValue());
			
			rec_nota.set('drp', totaldiskon);
			rec_nota.set('tarif2', subtotal);
			hitung();
			cektombol();
			wDisk.close();
		}
	
		function diskonAwal(){
			if(rec_nota.data.tarifjs == '')rec_nota.data.tarifjs = 0;
			if(rec_nota.data.tarifjm == '')rec_nota.data.tarifjm = 0;
			if(rec_nota.data.tarifjp == '')rec_nota.data.tarifjp = 0;
			if(rec_nota.data.tarifbhp == '')rec_nota.data.tarifbhp = 0;
			
			if(rec_nota.data.diskonjs == undefined)rec_nota.data.diskonjs = 0;
			if(rec_nota.data.diskonjm == undefined)rec_nota.data.diskonjm = 0;
			if(rec_nota.data.diskonjp == undefined)rec_nota.data.diskonjp = 0;
			if(rec_nota.data.diskonbhp == undefined)rec_nota.data.diskonbhp = 0;
			
			jumlahjs = (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjs);
			jumlahjm = (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjm);
			jumlahjp = (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonjp);
			jumlahbhp = (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty)) - parseInt(rec_nota.data.diskonbhp);
			
			if(rec_nota.data.tarifjs != 0){
				dprsnjs = parseInt(rec_nota.data.diskonjs) / (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjs').setReadOnly(false);
				Ext.getCmp('tf.dprsnjs').enable();
				Ext.getCmp('tf.drpjs').setReadOnly(false);
				Ext.getCmp('tf.drpjs').enable();
			}
			if(rec_nota.data.tarifjm != 0){
				dprsnjm = parseInt(rec_nota.data.diskonjm) / (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjm').setReadOnly(false);
				Ext.getCmp('tf.dprsnjm').enable();
				Ext.getCmp('tf.drpjm').setReadOnly(false);
				Ext.getCmp('tf.drpjm').enable();
			}
			if(rec_nota.data.tarifjp != 0){
				dprsnjp = parseInt(rec_nota.data.diskonjp) / (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnjp').setReadOnly(false);
				Ext.getCmp('tf.dprsnjp').enable();
				Ext.getCmp('tf.drpjp').setReadOnly(false);
				Ext.getCmp('tf.drpjp').enable();
			}
			if(rec_nota.data.tarifbhp != 0){
				dprsnbhp = parseInt(rec_nota.data.diskonbhp) / (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty)) * 100;
				Ext.getCmp('tf.dprsnbhp').setReadOnly(false);
				Ext.getCmp('tf.dprsnbhp').enable();
				Ext.getCmp('tf.drpbhp').setReadOnly(false);
				Ext.getCmp('tf.drpbhp').enable();
			}
			
			tariftot = (parseInt(rec_nota.data.tarifjs) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifjm) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifjp) * parseInt(rec_nota.data.qty)) + (parseInt(rec_nota.data.tarifbhp) * parseInt(rec_nota.data.qty));
			dprsntot = dprsnjs + dprsnjm + dprsnjp + dprsnbhp;
			drptot = parseInt(rec_nota.data.diskonjs) + parseInt(rec_nota.data.diskonjm) + parseInt(rec_nota.data.diskonjp) + parseInt(rec_nota.data.diskonbhp);
			jumlahtot = jumlahjs + jumlahjm + jumlahjp + jumlahbhp;
			
			Ext.getCmp('tf.dprsnjs').setValue(dprsnjs);
			Ext.getCmp('tf.dprsnjm').setValue(dprsnjm);
			Ext.getCmp('tf.dprsnjp').setValue(dprsnjp);
			Ext.getCmp('tf.dprsnbhp').setValue(dprsnbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jumlahjs);
			Ext.getCmp('tf.jumlahjm').setValue(jumlahjm);
			Ext.getCmp('tf.jumlahjp').setValue(jumlahjp);
			Ext.getCmp('tf.jumlahbhp').setValue(jumlahbhp);
			
			Ext.getCmp('tf.tariftot').setValue(tariftot);
			Ext.getCmp('tf.dprsntot').setValue(dprsntot);
			Ext.getCmp('tf.drptot').setValue(drptot);
			Ext.getCmp('tf.jumlahtot').setValue(jumlahtot);
		}
		
		function htgDiskonPrsn(){
			var diskjs = Ext.getCmp('tf.tarifjs').getValue() * (Ext.getCmp('tf.dprsnjs').getValue() / 100);
			var diskjm = Ext.getCmp('tf.tarifjm').getValue() * (Ext.getCmp('tf.dprsnjm').getValue() / 100);
			var diskjp = Ext.getCmp('tf.tarifjp').getValue() * (Ext.getCmp('tf.dprsnjp').getValue() / 100);
			var diskbhp = Ext.getCmp('tf.tarifbhp').getValue() * (Ext.getCmp('tf.dprsnbhp').getValue() / 100);
			
			var jmljs = Ext.getCmp('tf.tarifjs').getValue() - diskjs;
			var jmljm = Ext.getCmp('tf.tarifjm').getValue() - diskjm;
			var jmljp = Ext.getCmp('tf.tarifjp').getValue() - diskjp;
			var jmlbhp = Ext.getCmp('tf.tarifbhp').getValue() - diskbhp;
			
			var hdprsntot = Ext.getCmp('tf.dprsnjs').getValue() + Ext.getCmp('tf.dprsnjm').getValue() + Ext.getCmp('tf.dprsnjp').getValue() + Ext.getCmp('tf.dprsnbhp').getValue();
			var hdrptot = diskjs + diskjm + diskjp + diskbhp;
			var hjumlahtot = jmljs + jmljm + jmljp + jmlbhp;
			
			Ext.getCmp('tf.drpjs').setValue(diskjs);
			Ext.getCmp('tf.drpjm').setValue(diskjm);
			Ext.getCmp('tf.drpjp').setValue(diskjp);
			Ext.getCmp('tf.drpbhp').setValue(diskbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jmljs);
			Ext.getCmp('tf.jumlahjm').setValue(jmljm);
			Ext.getCmp('tf.jumlahjp').setValue(jmljp);
			Ext.getCmp('tf.jumlahbhp').setValue(jmlbhp);
			
			Ext.getCmp('tf.dprsntot').setValue(hdprsntot);
			Ext.getCmp('tf.drptot').setValue(hdrptot);
			Ext.getCmp('tf.jumlahtot').setValue(hjumlahtot);
		}
		
		function htgPrsnAll(){
			var trfjs = Ext.getCmp('tf.tarifjs').getValue();
			var trfjm = Ext.getCmp('tf.tarifjm').getValue();
			var trfjp = Ext.getCmp('tf.tarifjp').getValue();
			var trfbhp = Ext.getCmp('tf.tarifbhp').getValue();
			var prsnall = Ext.getCmp('tf.dprsnall').getValue();
			
			if(trfjs>0) Ext.getCmp('tf.dprsnjs').setValue(prsnall);
			if(trfjm>0) Ext.getCmp('tf.dprsnjm').setValue(prsnall);
			if(trfjp>0) Ext.getCmp('tf.dprsnjp').setValue(prsnall);
			if(trfbhp>0) Ext.getCmp('tf.dprsnbhp').setValue(prsnall);
			
			var drpall = Ext.getCmp('tf.tariftot').getValue() * (prsnall / 100);
			Ext.getCmp('tf.drpall').setValue(drpall);
			htgDiskonPrsn();
		}
		
		function htgDiskonRp(){
			var diskjs = (Ext.getCmp('tf.drpjs').getValue() * 100) / Ext.getCmp('tf.tarifjs').getValue();
			var diskjm = (Ext.getCmp('tf.drpjm').getValue() * 100) / Ext.getCmp('tf.tarifjm').getValue();
			var diskjp = (Ext.getCmp('tf.drpjp').getValue() * 100) / Ext.getCmp('tf.tarifjp').getValue();
			var diskbhp = (Ext.getCmp('tf.drpbhp').getValue() * 100) / Ext.getCmp('tf.tarifbhp').getValue();
			
			if(isNaN(diskjs) || Ext.getCmp('tf.tarifjs').getValue() < Ext.getCmp('tf.drpjs').getValue()) diskjs = 0;
			if(isNaN(diskjm) || Ext.getCmp('tf.tarifjm').getValue() < Ext.getCmp('tf.drpjm').getValue()) diskjm = 0;
			if(isNaN(diskjp) || Ext.getCmp('tf.tarifjp').getValue() < Ext.getCmp('tf.drpjp').getValue()) diskjp = 0;
			if(isNaN(diskbhp) || Ext.getCmp('tf.tarifbhp').getValue() < Ext.getCmp('tf.drpbhp').getValue()) diskbhp = 0;
			
			var jmljs = Ext.getCmp('tf.tarifjs').getValue() - Ext.getCmp('tf.drpjs').getValue();
			var jmljm = Ext.getCmp('tf.tarifjm').getValue() - Ext.getCmp('tf.drpjm').getValue();
			var jmljp = Ext.getCmp('tf.tarifjp').getValue() - Ext.getCmp('tf.drpjp').getValue();
			var jmlbhp = Ext.getCmp('tf.tarifbhp').getValue() - Ext.getCmp('tf.drpbhp').getValue();
			
			var hdrptot = Ext.getCmp('tf.drpjs').getValue() + Ext.getCmp('tf.drpjm').getValue() + Ext.getCmp('tf.drpjp').getValue() + Ext.getCmp('tf.drpbhp').getValue();
			var hdprsntot = diskjs + diskjm + diskjp + diskbhp;
			var hjumlahtot = jmljs + jmljm + jmljp + jmlbhp;
			
			Ext.getCmp('tf.dprsnjs').setValue(diskjs);
			Ext.getCmp('tf.dprsnjm').setValue(diskjm);
			Ext.getCmp('tf.dprsnjp').setValue(diskjp);
			Ext.getCmp('tf.dprsnbhp').setValue(diskbhp);
			
			Ext.getCmp('tf.jumlahjs').setValue(jmljs);
			Ext.getCmp('tf.jumlahjm').setValue(jmljm);
			Ext.getCmp('tf.jumlahjp').setValue(jmljp);
			Ext.getCmp('tf.jumlahbhp').setValue(jmlbhp);
			
			Ext.getCmp('tf.dprsntot').setValue(hdprsntot);
			Ext.getCmp('tf.drptot').setValue(hdrptot);
			Ext.getCmp('tf.jumlahtot').setValue(hjumlahtot);
		}
		
		function htgRpAll(){
			var trfjs = Ext.getCmp('tf.tarifjs').getValue();
			var trfjm = Ext.getCmp('tf.tarifjm').getValue();
			var trfjp = Ext.getCmp('tf.tarifjp').getValue();
			var trfbhp = Ext.getCmp('tf.tarifbhp').getValue();
			var drpall = Ext.getCmp('tf.drpall').getValue();
			var dprsnall = (drpall * 100) / Ext.getCmp('tf.tariftot').getValue();
			
			if(trfjs>0) Ext.getCmp('tf.dprsnjs').setValue(dprsnall);
			if(trfjm>0) Ext.getCmp('tf.dprsnjm').setValue(dprsnall);
			if(trfjp>0) Ext.getCmp('tf.dprsnjp').setValue(dprsnall);
			if(trfbhp>0) Ext.getCmp('tf.dprsnbhp').setValue(dprsnall);
			
			Ext.getCmp('tf.dprsnall').setValue(dprsnall);
			htgDiskonPrsn();
		}
	
	}
	
	function fPemb(){
		var ds_carabayar = dm_carabayar();
		var ds_bankcb = dm_bank();
		var pmbyrntemp = 0;
		var total = Ext.getCmp('tf.total').getValue();
		pmbyrntemp = total - pmbyrn;
		if(pmbyrntemp < 0) pmbyrntemp = 0;
		var pemb_form = new Ext.form.FormPanel({
			xtype:'form',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 200, width: 300,
			layout: 'form', 
			frame: false,
			items: [     
			{
				xtype: 'combo', fieldLabel: 'Cara Bayar',
				id: 'cb.pembcb', width: 150, 
				store: ds_carabayar, valueField: 'idcarabayar', displayField: 'nmcarabayar',
				editable: false, triggerAction: 'all', value:'Tunai',
				forceSelection: true, submitValue: true, mode: 'local',
				listeners:{
					select:function(combo, records, eOpts){
						if(records.get('idcarabayar') != 1){
							Ext.getCmp('cb.pembbank').enable();
							Ext.getCmp('tf.pembkartu').enable();
						} else {
							Ext.getCmp('cb.pembbank').disable();
							Ext.getCmp('tf.pembkartu').disable();
							Ext.getCmp('cb.pembbank').setValue('');
							Ext.getCmp('tf.pembkartu').setValue('');
						}
					}
				}
			},    
			{
				xtype: 'combo', fieldLabel: 'Bank',
				id: 'cb.pembbank', width: 150, 
				store: ds_bankcb, valueField: 'idbank', displayField: 'nmbank',
				editable: false, triggerAction: 'all', disabled:true,
				forceSelection: true, submitValue: true, mode: 'local',
			},{
				xtype: 'textfield', fieldLabel:'No. Kartu', disabled:true,
				id: 'tf.pembkartu', width: 150
			},{
				xtype: 'numericfield', fieldLabel:'Nominal',
				id: 'tf.pembnominal', width: 150, height: 50,
				value: pmbyrntemp,
				style: {
					'fontSize'     : '20px'
				}
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					PembAdd();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wPemb.close();
				}
			}]
		});
			
		var wPemb = new Ext.Window({
			title: 'Pembayaran',
			modal: true, closable:false,
			items: [pemb_form]
		});
		
		wPemb.show();
		
		function PembAdd(){
			if(Ext.getCmp('tf.pembnominal').getValue()>pmbyrntemp)
				Ext.getCmp('tf.pembnominal').setValue(pmbyrntemp);
			pmbyrn += Ext.getCmp('tf.pembnominal').getValue();
			
				var arr = [];
				var xxy = 0;
				for (var zxc = 0; zxc <ds_nota.data.items.length; zxc++) {
					var record = ds_nota.data.items[zxc].data;
					zkditem = record.kdbrg;
					xxy += parseFloat(record.drp);
					arr[zxc] = zkditem;
				}
				gtot = pmbyrn+xxy;
			
			var orgaListRecord = new Ext.data.Record.create([
				{
					name: 'nmcarabayar',
					name: 'nmbank',
					name: 'nokartu',
					name: 'jumlah'
				}
			]);
			
			ds_kuitansidet.add([
				new orgaListRecord({
					'nmcarabayar': Ext.getCmp('cb.pembcb').lastSelectionText,
					'nmbank': Ext.getCmp('cb.pembbank').lastSelectionText,
					'nokartu': Ext.getCmp('tf.pembkartu').getValue(),
					'jumlah': Ext.getCmp('tf.pembnominal').getValue()
				})
			]);
			if(pmbyrn < 0) pmbyrn = 0;
			Ext.getCmp('tf.jumlah').setValue(pmbyrn);
			/* if(pmbyrn >= total) Ext.getCmp("bt.simpan").enable();
			else Ext.getCmp("bt.simpan").disable(); */
			//cektombol();
			
			if(Ext.getCmp("tf.nonota").getValue() == ''){
				if(gtot >= total) Ext.getCmp("bt.simpan").enable();
				else Ext.getCmp("bt.simpan").disable();
			} else {
				Ext.getCmp("bt.simpan").disable();
			}
			
			wPemb.close();
		}
	}
	
	function cektombol(){
		var total  = Ext.getCmp('tf.total').getValue();
		if(Ext.getCmp("tf.nonota").getValue() == ''){
			if(total<=0){
				Ext.getCmp("bt.simpan").disable();
			} else {
				if(pmbyrn >= total) Ext.getCmp("bt.simpan").enable();
				else Ext.getCmp("bt.simpan").disable();
			}
		} else {
			Ext.getCmp("bt.simpan").disable();
		}
	}
	
}