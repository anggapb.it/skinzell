function Ksrsetorankasir(){
Ext.form.Field.prototype.msgTarget = 'side';
	var pageSize = 20;
	var ds_setorankasir = dm_setorankasir();
	var ds_stapproval = dm_stapproval();
	var ds_stsetuju = dm_stsetuju();	
	var ds_pengeluarankas = dm_pengeluarankas();
	var ds_retfarrawjln = dm_retfarrawjln();
	var ds_retfarrawri = dm_retfarrawri();
	var ds_retfarluar = dm_retfarluar();
	var ds_retdeposit = dm_retdeposit(); 
	var ds_farpasluarjumbyr = dm_farpasluarjumbyr();
	var ds_farpasluardijamin = dm_farpasluardijamin();
	var ds_totudg = dm_totudg();
	var ds_jmlbyrudg = dm_jmlbyrudg();
	var ds_totdijamin = dm_totdijamin();
	var ds_totpeltambahan = dm_totpeltambahan();
	var ds_totpeltambahannontunai = dm_totpeltambahannontunai();
	var ds_totdafosit = dm_totdafosit();
	var ds_carabyrtunai = dm_carabyrtunai();
	var ds_totrjunitpelayanan = dm_totrjunitpelayanan();
		ds_totrjunitpelayanan.setBaseParam('tglnota','null');
	var ds_totriruangan = dm_totriruangan();
		ds_totriruangan.setBaseParam('tglkuitansi','null');
	var ds_detdeposit = dm_detdeposit();
		ds_detdeposit.setBaseParam('tglkuitansi','null');
	
	ds_setorankasir.setBaseParam('tglawal',Ext.util.Format.date(new Date(), 'Y-m-d'));
	ds_setorankasir.setBaseParam('tglakhir',Ext.util.Format.date(new Date(), 'Y-m-d'));
	
	function cektgl(){		
		ds_setorankasir.setBaseParam('tglawal',Ext.util.Format.date(Ext.getCmp('tglawal').getValue(), 'Y-m-d'));
		ds_setorankasir.setBaseParam('tglakhir',Ext.util.Format.date(Ext.getCmp('tglakhir').getValue(), 'Y-m-d'));
		ds_setorankasir.load();
	}
	
	var cari_data = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoFocus: true,
		autoHeight: true,
		position: 'top',
		mode: 'local',
		width: 200
	})];
	
	function headerGerid(text, align){
		var hAlign = 'center';	
		if(align =='c') hAlign = 'center';
		if(align =='l') hAlign = 'left';
		if(align =='r') hAlign = 'right';
		return "<H3 align='"+hAlign+"'>"+text+"</H3>";
	}
	
	var vw_setoranksr = new Ext.grid.GridView({
		emptyText: '< Belum ada Data >'
	});
	
	var grid_nya = new Ext.grid.GridPanel({
		id: 'grid_setoranksr',
		store: ds_setorankasir,
		view: vw_setoranksr,
		autoScroll: true,
		height: 530,
		columnLines: true,
		plugins: cari_data,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			iconCls: 'silk-add',
			handler: function() {
				fnAddSetoranksr();
				var waitmsg = Ext.MessageBox.wait('Loading....', 'Tunggu');
				Ext.getCmp('df.tglsetoran').setValue(new Date());
				Ext.Ajax.request({
					url:BASE_URL + 'pengeluarankas_controller/getCekstkasir',
					method:'POST',
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						//Ext.getCmp("tf.nokasir").setValue(obj.nokasir);
					}
				});
				Ext.getCmp('tf.sumjmhpemasukan').setValue('0');
				Ext.getCmp('tf.sumjmhpemasukantunai').setValue('0');
				Ext.getCmp('tf.sumuangtunaiygdisetorkan').setValue('0');				
				ds_totrjunitpelayanan.setBaseParam('tglnota','null');
				ds_totrjunitpelayanan.load();
				ds_totriruangan.setBaseParam('tglkuitansi','null');
				ds_totriruangan.load();
				Ext.Ajax.request({
					url:BASE_URL + 'setorankasir_controller/cek_totriruangan',
					method:'POST',				
					params: {
						tglkuitansi : 'null'
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						//Ext.getCmp("tf.nokasir").setValue(obj.nokasir);
						waitmsg.hide();
					}
				});
				ds_detdeposit.setBaseParam('tglkuitansi','null');
				ds_detdeposit.load();
				ds_carabyrtunai.setBaseParam('tglkuitansi','null');
				ds_carabyrtunai.load();
				//waitmsg.hide();
			}
		},'-',{
			xtype: 'compositefield',
			width: 380,
			items: [{
				xtype: 'label', id: 'lb.lb', text: 'Tgl. Transaksi :', margins: '4 10 0 50',
			},{
				xtype: 'datefield',
				id: 'tglawal',
				value: new Date(),
				format: "d-m-Y",
				width: 100, //disabled: true,
				listeners:{
					select: function(field, newValue){
						cektgl();
					}
				}
			},
			{
				xtype: 'label', id: 'lb.lbt', text: 's/d', margins: '4 4 0 0',
			},
			{
				xtype: 'datefield',
				id: 'tglakhir',
				value: new Date(),
				format: "d-m-Y",
				width: 100, //disabled: true,
				listeners:{
					select: function(field, newValue){
						cektgl();
					}
				}
			}]
		}],
		//sm: sm_nya,
		columns: [new Ext.grid.RowNumberer(),
		{
			header: headerGerid('idsetorankasir'),
			width: 70,
			dataIndex: 'idsetorankasir',
			sortable: true,
			align:'center',
			hidden: true
		},{
			header: headerGerid('Tgl.<br>Transaksi'),
			width: 70,
			dataIndex: 'tgltransaksi',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('Tgl. Dibuat<br>Setoran'),
			width: 70,
			dataIndex: 'tglsetoran',
			sortable: true,
			align:'center',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
		},{
			header: headerGerid('User Input<br>(Kasir)'),
			width: 178,
			dataIndex: 'nmlengkap',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Approval<br>(Keuangan)'),
			width: 178,
			dataIndex: 'nmset',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('Catatan'),
			width: 230,
			dataIndex: 'catatan',
			sortable: true,
		},{
			header: headerGerid('Status Setoran'),
			width: 97,
			dataIndex: 'nmstsetuju',
			sortable: true,
			align:'center',
		},{
			header: headerGerid('jmlsetoran'),
			width: 110,
			dataIndex: 'jmlsetoran',
			sortable: true,
			hidden: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('jmlsetoranfisik'),
			width: 110,
			dataIndex: 'jmlsetoranfisik',
			sortable: true,
			hidden: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},{
			header: headerGerid('Selisih'),
			width: 110,
			dataIndex: 'selisih',
			sortable: true,
			xtype: 'numbercolumn', format:'0,000', align:'right',
		},
		{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Edit',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_edit.png',
					tooltip: 'Edit record',
                    handler: function(grid, rowIndex) {
						//Ext.getCmp('btn_cnoreg').disable();
						//Ext.getCmp('df.tgltransaksi').disable();
						var record = ds_setorankasir.getAt(rowIndex);
						var idstsetuju = record.data['idstsetuju']
						if(idstsetuju !=1){	
							//Ext.MessageBox.alert('Informasi', 'Data sudah disetujui..');	
							fnEditeSetoranksr(grid, rowIndex);
							Ext.getCmp('btn_simpan').disable();
							Ext.getCmp('bt.cetak').enable();
						}else if(idstsetuju ==1){			
							fnEditeSetoranksr(grid, rowIndex);
							Ext.getCmp('bt.cetak').enable();					
						}
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex) {
						var record = ds_setorankasir.getAt(rowIndex);
						var idstsetuju = record.data['idstsetuju']
						if(idstsetuju !=1){	
							Ext.MessageBox.alert('Informasi', 'Data sudah disetujui..');
						}else if(idstsetuju ==1){			
							fnDeleteSetoranksr(grid, rowIndex);				
						}
                    }
                }]
        },{
                xtype: 'actioncolumn',
                width: 50,
				header: 'Cetak',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_print.gif',
					tooltip: 'Cetak record',
                    handler: function(grid, rowIndex) {						
						var record = ds_setorankasir.getAt(rowIndex);
						var tgltransaksi = record.data['tgltransaksi']									
							cetakksr(grid, rowIndex);
                    }
                }]
        }]
	});
       
	var form_bp_general = new Ext.form.FormPanel({
		id: 'form_bp_general',
		title: 'Setoran Kasir', iconCls:'silk-user',
		layout: 'fit',
		autoScroll: true,
		items: [
		{
			xtype: 'panel',
			border: false,
			autoScroll: true,
			items: [{
				layout: 'form',
				border: false,
				items: [grid_nya]
			}]
		}]
	});
	SET_PAGE_CONTENT(form_bp_general);
/** 
FUNCTIONS
*/
	
	function reloadSetoranksr(){
		ds_setorankasir.reload();
	}
	
	function fnAddSetoranksr(){
		var grid = grid_nya;
		wEntrySetoranksr(false, grid, null);	
	}
	
	function fnEditeSetoranksr(grid, record){
		var record = ds_setorankasir.getAt(record);
		wEntrySetoranksr(true, grid, record);		
	}
	
	function fnDeleteSetoranksr(grid, record){
		var record = ds_setorankasir.getAt(record);
		var url = BASE_URL + 'setorankasir_controller/delete_setorankasir';
		var params = new Object({
						idsetoranksr	: record.data['idsetorankasir']
					});
		RH.deleteGridRecord(url, params, grid );
	}
	
	function cetakksr(grid, record){
		var record = ds_setorankasir.getAt(record);
		var tgltransaksi = record.data['tgltransaksi'] 
			RH.ShowReport(BASE_URL + 'print/printsetoranksr/printstrnksr/' + tgltransaksi); 
	}
	
/**
WIN - FORM ENTRY/EDIT 
*/
	function wEntrySetoranksr(isUpdate, grid, record){
		var myVar=setInterval(function(){myTimer()},1000);
		function myTimer(){
			var d=new Date();
			var formattedValue = Ext.util.Format.date(d, 'H:i:s');
			if(Ext.getCmp("tf.jam"))
					RH.setCompValue("tf.jam",formattedValue);
			else myStopFunction();
		}
		
		function myStopFunction(){
			clearInterval(myVar);
		}
		
		var vw_daftar_rj = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_daftar_rj = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_rj',
			store: ds_totrjunitpelayanan,
			view: vw_daftar_rj,
			tbar: [{
				xtype: 'label', id: 'lb.lbl', text: 'I. Rawat Jalan',
			}],
			autoScroll: true,
			height: 240, //autoHeight: true,
			columnLines: true,
			frame: true,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('Unit Pelayanan'),
				width: 379,
				dataIndex: 'nmbagian',
				sortable: true,
				align: 'center'
			},
			{
				header: headerGerid('Total'),
				width: 120,
				dataIndex: 'total',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			},
			{
				header: headerGerid('Tunai'),
				width: 120,
				dataIndex: 'totbyrtunai',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			},
			{
				header: headerGerid('Non Tunai'),
				width: 120,
				dataIndex: 'totbyrnontunai',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			}],
			bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 620,
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.totalrt', text: 'Jumlah :', margins: '3 5 0 67',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhrjtot',
						value: 0,
						width: 122,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhrjbayar',
						value: 0,
						width: 120,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhrjpiutang',
						value: 0,
						width: 120,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					}]
				}]
			}]
		});
		
		var vw_daftar_ri = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
				
		var grid_daftar_ri = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_ri',
			store: ds_totriruangan,
			view: vw_daftar_ri,
			tbar: [{
				xtype: 'label', id: 'lb.lbl', text: 'II. Rawat Inap',
			}],
			autoScroll: true,
			height: 240, //autoHeight: true,
			columnLines: true,
			frame: true,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('No. Registrasi'),
				width: 85,
				dataIndex: 'noreg',
				sortable: true,
				align: 'center'
			},{
				header: headerGerid('No. RM'),
				width: 60,
				dataIndex: 'norm',
				sortable: true,
				align: 'center'
			},{
				header: headerGerid('Nama Pasien'),
				width: 131,
				dataIndex: 'nmpasien',
				sortable: true,
				align: 'left'
			},{
				header: headerGerid('Ruangan'),
				width: 80,
				dataIndex: 'nmbagian',
				sortable: true,
				align: 'center'
			},
			{
				header: headerGerid('nominalugd'),
				width: 80,
				dataIndex: 'nominalugd',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
				hidden: true
			},
			{
				header: headerGerid('Tertagih'),
				width: 80,
				dataIndex: 'total',
				sortable: true,
				align:'right',
				//xtype: 'numbercolumn', format:'0,000', align:'right',
				renderer: function(value, p, r){
					if(r.data['nominalugd'] == null) r.data['nominalugd'] = 0;
					var total = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']);
					if(total < 0) total = 0;
					return Ext.util.Format.number(total, '0,000');
				},
			},{
				header: headerGerid('diskon'),
				width: 80,
				dataIndex: 'diskon',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
				hidden: true
			},{
				header: headerGerid('Deposit'),
				width: 80,
				dataIndex: 'deposit',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			},
			{
				header: headerGerid('Tunai'),
				width: 98,
				dataIndex: 'totbyrtunai',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			},
			{
				header: headerGerid('Non Tunai'),
				width: 123,
				dataIndex: 'totbyrnontunai',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			}/* ,
			{
				header: headerGerid('Total'),
				dataIndex: 'total',
				align:'right',
				renderer: function(value, p, r){
					if(r.data['nominalugd'] == null) r.data['nominalugd'] = 0;
					var total = parseFloat(r.data['nominalugd']) + parseFloat(r.data['nominalri']) - parseFloat(r.data['diskon']) - parseFloat(r.data['deposit']);
					if(total < 0) total = 0;
					return Ext.util.Format.number(total, '0,000');
				},
				width: 80
			} */],
			bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 620,
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.totalrt', text: 'Jumlah :', margins: '3 5 0 67',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhritot',
						value: 0,
						width: 122,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhribayar',
						value: 0,
						width: 120,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhripiutang',
						value: 0,
						width: 120,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					}]
				}]
			}]
		});
		
		
		var vw_daftar_deposit = new Ext.grid.GridView({
			emptyText: '< Belum ada Data >'
		});
		
		var grid_daftar_deposit = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_deposit',
			store: ds_detdeposit,
			view: vw_daftar_deposit,
			tbar: [{
				xtype: 'label', id: 'lb.lbl', text: '',
			}],
			autoScroll: true,
			height: 240, //autoHeight: true,
			columnLines: true,
			frame: true,
			//sm: sm_nya,
			columns: [new Ext.grid.RowNumberer(),
			{
				header: headerGerid('No. Registrasi'),
				width: 85,
				dataIndex: 'noreg',
				sortable: true,
				align: 'center'
			},{
				header: headerGerid('No. RM'),
				width: 60,
				dataIndex: 'norm',
				sortable: true,
				align: 'center'
			},{
				header: headerGerid('Nama Pasien'),
				width: 190,
				dataIndex: 'nmpasien',
				sortable: true,
				align: 'left'
			},{
				header: headerGerid('Ruangan'),
				width: 80,
				dataIndex: 'nmbagian',
				sortable: true,
				align: 'center'
			},
			{
				header: headerGerid('Total'),
				width: 80,
				dataIndex: 'total',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			},
			{
				header: headerGerid('Tunai'),
				width: 123,
				dataIndex: 'tunai',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			},
			{
				header: headerGerid('Non Tunai'),
				width: 123,
				dataIndex: 'nontunai',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			}],
			bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				style: 'padding:0px; margin: 0px',
				width: 620,
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'label', id: 'lb.totalrt', text: 'Jumlah :', margins: '3 5 0 67',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhdetdeptot',
						value: 0,
						width: 122,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhdetdepbayar',
						value: 0,
						width: 120,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					},{
						xtype: 'numericfield',
						id: 'tf.jmhdetdeppiutang',
						value: 0,
						width: 120,
						//readOnly:true,
						style : 'opacity:0.6',
						thousandSeparator:',',margins: '0 2 0 0',
					}]
				}]
			}]
		});
		
		var grid_daftar_cbyr = new Ext.grid.EditorGridPanel({
			id: 'grid_daftar_cbyr',
			store: ds_carabyrtunai,
			//view: vw_daftar_cbyr,
			autoScroll: true,
			height: 150, //autoHeight: true,
			width: 213,
			columnLines: true,
			frame: false,
			//sm: sm_nya,
			columns: [
			{
				header: headerGerid('Cara Bayar'),
				width: 90,
				dataIndex: 'nmcarabayar',
				sortable: true,
			},
			{
				header: headerGerid('Jumlah'),
				width: 120,
				dataIndex: 'totcarabyr',
				sortable: true,
				xtype: 'numbercolumn', format:'0,000', align:'right',
			}]
		});
		
		var winTitle = (isUpdate)?'Setoran Kasir (Edit)':'Setoran Kasir (Entry)';
		var setoranksr_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'frm.setoranksr',
			buttonAlign: 'left',
			labelWidth: 140, labelAlign: 'right',
			//bodyStyle: 'padding:5px 4px 2px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			autoScroll: true,
			height: 555, width: 890,
			layout: {
				type: 'form',
				pack: 'center',
				align: 'center'
			},
			frame: true,
			tbar: [{
				text: 'Simpan', iconCls:'silk-save', style: 'marginLeft: 5px', id: 'btn_simpan',
				handler: function() {
					fnSaveSetoranksr();                           
				}
			},{
				text: 'Cetak', iconCls:'silk-printer', id: 'bt.cetak', style: 'marginLeft: 5px', hidden: true,
				handler: function() {
					cetak();
				}
			},{
				text: 'Kembali', iconCls:'silk-arrow-undo', style: 'marginLeft: 5px',
				handler: function() {
					wSetoranksr.close();
				}
			}],
			items: [{
				xtype: 'fieldset', title: '', border: true, layout: 'column', height: 105, style: 'marginTop: 5px',
				items: [{
					columnWidth: 0.50, border: false, layout: 'form', 
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl. Transaksi', id: 'df.tgltransaksi',
						width: 118, 
						format: 'd-m-Y',						
						maxValue: new Date(),
						listeners:{
							select: function(field, newValue){
								totretfarrawjln();
							}/* ,
							change : function(field, newValue){
								totretfarrawjln();
							} */
						}
					},{
						xtype: 'combo', id: 'cb.approval', 
						fieldLabel: 'Approval(Keuangan)',
						store: ds_stapproval, triggerAction: 'all',
						valueField: 'nilai', displayField: 'nmset',
						forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', width: 250,
						editable: false,
						allowBlank: false
					},{
						xtype: 'textfield',
						fieldLabel: 'Catatan',
						id: 'tf.catatan',
						width: 250
					}]
				},
				{
					columnWidth: 0.50, border: false, layout: 'form',
					labelWidth: 150, labelAlign: 'right',
					items: [{
						xtype: 'compositefield',
						items: [{	
							xtype: 'datefield', fieldLabel:'Tgl./Jam Dibuat Setoran', id: 'df.tglsetoran',
							width: 118, maxValue: new Date(),
							format: 'd-m-Y',
						},{
							xtype: 'label', id: 'lb.garing3', text: '/', hidden: true
						},{ 	
							xtype: 'textfield', id: 'tf.jam', readOnly:true,
							width: 65, 
						},{
							xtype: 'label', id: 'lb.garing4', text: '/', hidden: true
						},{
							xtype: 'textfield', id: 'tf.shift',
							width: 60, disabled: true, hidden: true
						},{
							xtype: 'textfield', id: 'tf.idsetoranksr',
							width: 60, disabled: true , hidden: true
						}]
					},{
						xtype: 'textfield', fieldLabel:'User Input/Kasir',
						id: 'tf.userinput',
						readOnly: true, style : 'opacity:0.6',
						width: 250, value: USERNAME
					},{
						xtype: 'combo',
						fieldLabel: 'Status Setoran',
						id: 'cb.stsetuju',
						store: ds_stsetuju, 
						valueField: 'idstsetuju', displayField: 'nmstsetuju',
						triggerAction: 'all', forceSelection: true, submitValue: true, 
						mode: 'local', emptyText:'Pilih...', selectOnFocus:false, 
						width: 118, allowBlank: false, editable: false, value: 1, readOnly: true, style: 'opacity: 0.6',
					}]
				}]
			},{
				xtype: 'fieldset', title: 'PENERIMAAN :', layout: 'form', height: 250, //style: 'marginTop: 5px',
				items: [{
					xtype: 'fieldset', title: '', layout: 'form', height: 265, border: false, //style: 'marginTop: 5px',
					items: [grid_daftar_rj]
				},{
					xtype: 'fieldset', title: '', layout: 'form', hidden:true, height: 265, border: false, style: 'marginTop: -18px',
					items: [grid_daftar_ri]
				},{
					xtype: 'fieldset', title: '', layout: 'form', hidden:true, height: 265, border: false, style: 'marginTop: -18px',
					items: [grid_daftar_deposit]
				},{
					xtype: 'fieldset', title: '', layout: 'form', hidden:true, height: 265, border: false, style: 'marginTop: -18px',
					labelWidth: 401, labelAlign: 'right',
					items: [{
						xtype: 'compositefield',
						hidden: true,
						items: [{
							xtype: 'numericfield',
							fieldLabel: 'Farmasi Pasien Luar', 
							id: 'tf.farfasluartot',
							width: 122,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.farfasluarbiaya',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.farfasluarpiutang',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						}]
					},{
						xtype: 'compositefield',
						hidden: true,
						items: [{
							xtype: 'numericfield',
							fieldLabel: 'UGD',
							id: 'tf.ugdtot',
							width: 122,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.ugdbiaya',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.ugdpiutang',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						}]
					},{
						xtype: 'compositefield',
						hidden: true,
						items: [{
							xtype: 'numericfield',
							fieldLabel: 'Pelayanan Tambahan',
							id: 'tf.peltamtot',
							width: 122,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.peltambiaya',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.peltampiutang',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						}]
					},{
						xtype: 'compositefield', hidden: true,
						items: [{
							xtype: 'numericfield',
							fieldLabel: 'Deposit/Uang Muka',
							id: 'tf.deposittot',
							width: 122,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.depositbiaya',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.depositpiutang',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						}]
					},{
						xtype: 'compositefield',
						items: [{
							xtype: 'numericfield',
							fieldLabel: 'Jumlah Pemasukan',
							hidden: true,
							id: 'tf.jmhpemasukantot',
							width: 122,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.jmhpemasukanbiaya',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						},{
							xtype: 'numericfield',
							id: 'tf.jmhpemasukanpiutang',
							width: 120,margins: '0 2 0 0',
							thousandSeparator:',',
							value: 0,
							readOnly: true,
							style: 'opacity:0.6; text-align: right'
						}]
					}]
				}]
			},{
				xtype: 'fieldset', title: 'PENGELUARAN :', border: true, layout: 'form', height: 175,
				hidden:true, //style: 'marginTop: 5px',
				labelWidth: 656, labelAlign: 'right',
				items: [{
					xtype: 'numericfield',
					fieldLabel: 'Retur Faramasi Rawat Jalan',
					id: 'tf.pengretfarrj',
					width: 120,margins: '0 2 0 0',
					thousandSeparator:',',
					value: 0,
					readOnly: true,
					style: 'opacity:0.6; text-align: right'
				},{
					xtype: 'numericfield',
					fieldLabel: 'Retur Faramasi Rawat Inap',
					id: 'tf.pengretfarri',
					width: 120,margins: '0 2 0 0',
					thousandSeparator:',',
					value: 0,
					readOnly: true,
					style: 'opacity:0.6; text-align: right'
				},{
					xtype: 'numericfield',
					fieldLabel: 'Retur Faramasi Pasien Luar',
					id: 'tf.pengretfarpasluar',
					width: 120,margins: '0 2 0 0',
					thousandSeparator:',',
					value: 0,
					readOnly: true,
					style: 'opacity:0.6; text-align: right'
				},{
					xtype: 'numericfield',
					fieldLabel: 'Retur Deposit',
					id: 'tf.pengretdeposit',
					width: 120,margins: '0 2 0 0',
					thousandSeparator:',',
					value: 0,
					readOnly: true,
					style: 'opacity:0.6; text-align: right'
				},{
					xtype: 'numericfield',
					fieldLabel: 'Jumlah Pengeluaran',
					id: 'tf.jmhpengeluaran',
					width: 120,margins: '0 2 0 0',
					thousandSeparator:',',
					value: 0,
					readOnly: true,
					style: 'opacity:0.6; text-align: right'
				}]
			},{
				xtype: 'fieldset', title: 'SUMMARY :', border: true, layout: 'form', height: 385, //style: 'marginTop: 5px',
				labelWidth: 656, labelAlign: 'right',
				items: [{
					xtype: 'numericfield',
					fieldLabel: 'Jumlah Pemasukan',
					id: 'tf.sumjmhpemasukan',
					width: 120,margins: '0 2 0 0',
					maskRe: /[0-9]/,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					style: 'opacity:0.6; text-align: right', 
					validator:function(){
						hitungtotsetoran();
					}
				},{
					xtype: 'numericfield',
					fieldLabel: 'Jumlah Pemasukan Tuani',
					id: 'tf.sumjmhpemasukantunai',
					width: 120,margins: '0 2 0 0',
					maskRe: /[0-9]/,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					style: 'opacity:0.6; text-align: right', hidden: true,
					/* validator:function(){
						hitungtotsetoran();
					} */
				}//,grid_daftar_cbyr,
				,{
					xtype: 'fieldset',
					title: '',
					layout: 'form',
					style: 'marginLeft: 558px; marginBottom: -35px;',
					height: 205, 
					width: 340,
				//	height: 290,
					border: false,
					items: [grid_daftar_cbyr]
				},
				{
					xtype: 'numericfield',
					fieldLabel: 'Jumlah Pengeluaran',
					id: 'tf.sumjmhpengeluaran',
					width: 120,margins: '0 2 0 0',
					thousandSeparator:',',
					value: 0,
					readOnly: true,
					style: 'opacity:0.6; text-align: right'
				},{
					xtype: 'numericfield',
					fieldLabel: 'Total (Jumlah Pemasukan - Jumlah Pengeluaran)',
					id: 'tf.sumjmhstoruang',
					width: 120,margins: '0 2 0 0',
					thousandSeparator:',',
					value: 0,
					readOnly: true,
					style: 'opacity:0.6; text-align: right'
				},{
					xtype: 'numericfield',
					fieldLabel: 'Total Uang Tunai Yang Disetorkan',
					id: 'tf.sumuangtunaiygdisetorkan',
					width: 120,margins: '0 2 0 0',
					maskRe: /[0-9]/,
					decimalSeparator: ',', 
					decimalPrecision: 0, 
					alwaysDisplayDecimals: true,
					useThousandSeparator: true, 
					thousandSeparator: '.',
					allowBlank: false,
					validator:function(){
						hitungselisih();
					}
				},{
					xtype: 'numericfield',
					fieldLabel: 'Selisih',
					id: 'tf.sumjmhselisaih',
					width: 120,margins: '0 2 0 0',
					thousandSeparator:',',
					value: 0,
					readOnly: true,
					style: 'opacity:0.6; text-align: right'
				}]
			}]
		});
			
		var wSetoranksr = new Ext.Window({
			title: winTitle,
			modal: true, closable:false,
			items: [setoranksr_form]
		});
		
		function cetak(){
			var tglawal = Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d');			
			RH.ShowReport(BASE_URL + 'print/Printsetoranksr/printstrnksr/' + tglawal);
		}

	/**
	CALL SET FORM AND SHOW THE FORM (WINDOW)
	*/
		setSetoranksrForm(isUpdate, record);
		wSetoranksr.show();
		
		function ftotal() {
			var ttransaksi = Ext.getCmp('tf.tottransaksi').getValue();
			if(ttransaksi != ''){
				var tdeposit = Ext.getCmp('tf.totdeposit').getValue();
				var totall = tdeposit - ttransaksi;
				var btotall= totall;	
				Ext.getCmp('tf.retdeposit').setValue(btotall);				
			}
		}
		
		function totretfarrawjln(){
			ds_retfarrawjln.setBaseParam('tglreturfarmasi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_retfarrawjln.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_retfarrawjln.each(function (rec) { 
							tamtot = rec.get('totretfarrawjln');
						});
					if(tamtot == null){
						Ext.getCmp("tf.pengretfarrj").setValue('0');
						hittotpengeluaran();
					}else{						
						Ext.getCmp("tf.pengretfarrj").setValue(tamtot);
						hittotpengeluaran();
					}
				}
			});
			
			ds_retfarrawri.setBaseParam('tglreturfarmasi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_retfarrawri.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_retfarrawri.each(function (rec) { 
							tamtot = rec.get('totretfarrawri');
						});		
					if(tamtot == null){
						Ext.getCmp("tf.pengretfarri").setValue('0');
						hittotpengeluaran();
					}else{						
						Ext.getCmp("tf.pengretfarri").setValue(tamtot);
						hittotpengeluaran();
					}
				}
			});
			
			ds_retfarluar.setBaseParam('tglreturfarmasi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_retfarluar.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_retfarluar.each(function (rec) { 
							tamtot = rec.get('totretfarluar');
						});					
					if(tamtot == null){
						Ext.getCmp("tf.pengretfarpasluar").setValue('0');
						hittotpengeluaran();
					}else{						
						Ext.getCmp("tf.pengretfarpasluar").setValue(tamtot);
						hittotpengeluaran();
					}					
				}
			});
			
			ds_retdeposit.setBaseParam('tgltransaksi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_retdeposit.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_retdeposit.each(function (rec) { 
							tamtot = rec.get('totretdeposit');
						});					
					if(tamtot == null){
						Ext.getCmp("tf.pengretdeposit").setValue('0');
						hittotpengeluaran();
					}else{						
						Ext.getCmp("tf.pengretdeposit").setValue(tamtot);
						hittotpengeluaran();
					}					
				}
			}); 
			
			ds_farpasluarjumbyr.setBaseParam('tglnota',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_farpasluarjumbyr.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_farpasluarjumbyr.each(function (rec){ 
							tamtot = rec.get('jmlbayar');
						});
					if(tamtot == null){
						Ext.getCmp("tf.farfasluarbiaya").setValue('0');
					}else{
						Ext.getCmp("tf.farfasluarbiaya").setValue(tamtot);
					}
				}
			});
			
			ds_farpasluardijamin.setBaseParam('tglnota',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_farpasluardijamin.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_farpasluardijamin.each(function (rec){ 
							tamtot = rec.get('totaldijamin');
						});
					if(tamtot == null){
						Ext.getCmp("tf.farfasluarpiutang").setValue('0');
					}else{						
						Ext.getCmp("tf.farfasluarpiutang").setValue(tamtot);
					}
				}
			});	
			
			ds_totudg.setBaseParam('tglkuitansi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_totudg.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_totudg.each(function (rec){ 
							tamtot = rec.get('totalugd');
						});
					if(tamtot == null){
						Ext.getCmp("tf.ugdtot").setValue('0');
					}else{						
						Ext.getCmp("tf.ugdtot").setValue(tamtot);
					}
				}
			});	
			
			ds_jmlbyrudg.setBaseParam('tglnota',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_jmlbyrudg.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_jmlbyrudg.each(function (rec){ 
							tamtot 	= rec.get('totbyrtunai');
						});
					if(tamtot == null){
						Ext.getCmp("tf.ugdbiaya").setValue('0');
					}else{						
						Ext.getCmp("tf.ugdbiaya").setValue(tamtot);
					}					
					
					hitungtotal();
					hitungjmlbyr();
					hitungpiutang();
					hitungtotfarpasienluar();
					hitungtotpeltambahan();
				}
			});	
			
			ds_totdijamin.setBaseParam('tglnota',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_totdijamin.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_totdijamin.each(function (rec){ 
							tamtot = rec.get('totbyrnontunai');
						});
					if(tamtot == null){
						Ext.getCmp("tf.ugdpiutang").setValue('0');
					}else{						
						Ext.getCmp("tf.ugdpiutang").setValue(tamtot);
					}
				}
			});	
			
			ds_totpeltambahan.setBaseParam('tglnota',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_totpeltambahan.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_totpeltambahan.each(function (rec){ 
							tamtot = rec.get('totbyrtunai');
						});
					if(tamtot == null){
						Ext.getCmp("tf.peltambiaya").setValue('0');
					}else{
						Ext.getCmp("tf.peltambiaya").setValue(tamtot);
					}
				}
			});	
			
			ds_totpeltambahannontunai.setBaseParam('tglnota',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_totpeltambahannontunai.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					 ds_totpeltambahannontunai.each(function (rec){ 
							tamtot = rec.get('totbyrnontunai');
						});
					if(tamtot == null){
						Ext.getCmp("tf.peltampiutang").setValue('0');
					}else{
						Ext.getCmp("tf.peltampiutang").setValue(tamtot);
					}
				}
			});	
			
			ds_totdafosit.setBaseParam('tglkuitansi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_totdafosit.reload({
				scope   : this,
				callback: function(records, operation, success) {
					var tamtot = 0;
					var tamtott = 0;
					var tamtotn = 0;
					 ds_totdafosit.each(function (rec){ 
							tamtot = rec.get('totdafosit');
							tamtott = rec.get('totbyrtunai');
							tamtotn = rec.get('totbyrnontunai');
						});
					if(tamtot  == null){
						Ext.getCmp("tf.deposittot").setValue('0');
					}else{						
						Ext.getCmp("tf.deposittot").setValue(tamtot);
					}
					if(tamtott  == null){
						Ext.getCmp("tf.depositbiaya").setValue('0');
					}else{						
						Ext.getCmp("tf.depositbiaya").setValue(tamtott);
					}
					if(tamtotn  == null){
						Ext.getCmp("tf.depositpiutang").setValue('0');
					}else{						
						Ext.getCmp("tf.depositpiutang").setValue(tamtotn);
					}
				}
			});	
			
			ds_totrjunitpelayanan.setBaseParam('tglnota',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			//ds_totrjunitpelayanan.reload();	
			ds_totrjunitpelayanan.reload({
				scope   : this,
				callback: function(records, operation, success) {
					sumt = 0; 
					sumj = 0; 
					sumd = 0;  
					ds_totrjunitpelayanan.each(function (rec) {
						sumt += parseFloat(rec.get('total')); 
						sumj += parseFloat(rec.get('totbyrtunai')); 
						sumd += parseFloat(rec.get('totbyrnontunai')); 
					});
					Ext.getCmp("tf.jmhrjtot").setValue(sumt);
					Ext.getCmp("tf.jmhrjbayar").setValue(sumj);
					Ext.getCmp("tf.jmhrjpiutang").setValue(sumd);
							
					hitungtotal();
					hitungjmlbyr();
					hitungpiutang();
					hitungtotfarpasienluar();
					hitungtotpeltambahan();
				}
			});	
			
			ds_totriruangan.setBaseParam('tglkuitansi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_totriruangan.reload({
				scope   : this,
				callback: function(records, operation, success) {
					sumt = 0; 
					sumj = 0; 
					sumd = 0;  
					ds_totriruangan.each(function (rec) {
						//sumt += parseFloat(rec.get('total')); 
						sumj += parseFloat(rec.get('totbyrtunai')); 
						sumd += parseFloat(rec.get('totbyrnontunai')); 
					});
					sumt = sumj + sumd;
					Ext.getCmp("tf.jmhritot").setValue(sumt);
					Ext.getCmp("tf.jmhribayar").setValue(sumj);
					Ext.getCmp("tf.jmhripiutang").setValue(sumd);
							
					hitungtotal();
					hitungjmlbyr();
					hitungpiutang();
					hitungtotfarpasienluar();
					hitungtotpeltambahan();
				}
			});
			
			ds_detdeposit.setBaseParam('tglkuitansi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_detdeposit.reload({
				scope   : this,
				callback: function(records, operation, success) {
					sumt = 0; 
					sumj = 0; 
					sumd = 0;  
					ds_detdeposit.each(function (rec) {
						sumt += parseFloat(rec.get('total')); 
						sumj += parseFloat(rec.get('tunai')); 
						sumd += parseFloat(rec.get('nontunai')); 
					});
					//sumt = sumj + sumd;
					Ext.getCmp("tf.jmhdetdeptot").setValue(sumt);
					Ext.getCmp("tf.jmhdetdepbayar").setValue(sumj);
					Ext.getCmp("tf.jmhdetdeppiutang").setValue(sumd);
							
					hitungtotal();
					hitungjmlbyr();
					hitungpiutang();
					hitungtotfarpasienluar();
					hitungtotpeltambahan();
				}
			});
			
			ds_carabyrtunai.setBaseParam('tglkuitansi',Ext.getCmp('df.tgltransaksi').getValue().format('Y-m-d'));			
			ds_carabyrtunai.reload({
				scope   : this,
				callback: function(records, operation, success) {	
					sum = 0;  
					sumt = 0;  
					ds_carabyrtunai.each(function (rec) {
						sum += parseFloat(rec.get('totcarabyr'));
						sumt += parseFloat(rec.get('totcarabyrtunai'));
					});
					Ext.getCmp("tf.sumjmhpemasukan").setValue(sum);
					Ext.getCmp("tf.sumjmhpemasukantunai").setValue(sumt);
					
					hitungtotal();
					hitungjmlbyr();
					hitungpiutang();
					hitungselisih();
					hitungtotfarpasienluar();
					hitungtotpeltambahan();
				}
			});							
			
			/* Ext.Ajax.request({
				url : BASE_URL + 'setorankasir_controller/get_retfarrawri',
				method:'POST',				
				params: {
					tglreturfarmasi : Ext.getCmp('df.tgltransaksi').getValue()
				},
				success: function(response){
					obj = Ext.util.JSON.decode(response.responseText);
					Ext.getCmp("tf.pengretfarri").setValue(obj.totretfarrawri);
				}
			});
			
			Ext.Ajax.request({
				url : BASE_URL + 'setorankasir_controller/get_retfarluar',
				method:'POST',				
				params: {
					tglreturfarmasi : Ext.getCmp('df.tgltransaksi').getValue()
				},
				success: function(response){
					obj = Ext.util.JSON.decode(response.responseText);
					if(obj.totretfarluar == null){
						Ext.getCmp("tf.pengretfarpasluar").setValue('0');
					}else{						
						Ext.getCmp("tf.pengretfarpasluar").setValue(obj.totretfarluar);
					}
				}
			}); */
		}
		
		function hitungtotal(){
			var totpemasukan	= 0;
			
			var totrj			= Ext.getCmp("tf.jmhrjtot").getValue();
			var totri			= Ext.getCmp("tf.jmhritot").getValue();
			var totfarpasluar	= Ext.getCmp("tf.farfasluartot").getValue();
			var totugd			= Ext.getCmp("tf.ugdtot").getValue();
			var totpeltam		= Ext.getCmp("tf.peltamtot").getValue();
			var totdeposit		= Ext.getCmp("tf.deposittot").getValue();
			
			var totpemasukan	= totrj + totri + totfarpasluar + totugd + totpeltam + totdeposit;
				Ext.getCmp("tf.jmhpemasukantot").setValue(totpemasukan);
				//Ext.getCmp("tf.sumjmhpemasukan").setValue(totpemasukan);			
			
			/* var jmlbyrrj			= Ext.getCmp("tf.jmhrjbayar").getValue();
			var jmlbyrri			= Ext.getCmp("tf.jmhribayar").getValue();
			var jmlbyrfarpasluar	= Ext.getCmp("tf.farfasluarbiaya").getValue();
			var jmlbyrugd			= Ext.getCmp("tf.ugdbiaya").getValue();
			var jmlbyrpeltam		= Ext.getCmp("tf.peltambiaya").getValue();
			var jmlbyrdeposit		= Ext.getCmp("tf.depositbiaya").getValue();
			
			var jmlbyrpemasukan		= jmlbyrrj + jmlbyrri + jmlbyrfarpasluar + jmlbyrugd + jmlbyrpeltam + jmlbyrdeposit;
				Ext.getCmp("tf.jmhpemasukanbiaya").setValue(jmlbyrpemasukan);	 */		
			
		}
		
		function hitungjmlbyr(){
			var jmlbyrpemasukan		= 0;
			
			var jmlbyrrj			= Ext.getCmp("tf.jmhrjbayar").getValue();
			var jmlbyrri			= Ext.getCmp("tf.jmhribayar").getValue();
			var jmlbyrfarpasluar	= Ext.getCmp("tf.farfasluarbiaya").getValue();
			var jmlbyrugd			= Ext.getCmp("tf.ugdbiaya").getValue();
			var jmlbyrpeltam		= Ext.getCmp("tf.peltambiaya").getValue();
			var jmlbyrdeposit		= Ext.getCmp("tf.depositbiaya").getValue();
			
			jmlbyrpemasukan		= jmlbyrrj + jmlbyrri + jmlbyrfarpasluar + jmlbyrugd + jmlbyrpeltam + jmlbyrdeposit;
				Ext.getCmp("tf.jmhpemasukanbiaya").setValue(jmlbyrpemasukan);
				//alert(jmlbyrpemasukan);			
		}
		
		function hitungpiutang(){
			var piutangpemasukan		= 0;
			
			var piutangrj			= Ext.getCmp("tf.jmhrjpiutang").getValue();
			var piutangri			= Ext.getCmp("tf.jmhripiutang").getValue();
			var piutangfarpasluar	= Ext.getCmp("tf.farfasluarpiutang").getValue();
			var piutangugd			= Ext.getCmp("tf.ugdpiutang").getValue();
			var piutangpeltam		= Ext.getCmp("tf.peltampiutang").getValue();
			var piutangdeposit		= Ext.getCmp("tf.depositpiutang").getValue();
			
			piutangpemasukan		= piutangrj + piutangri + piutangfarpasluar + piutangugd + piutangpeltam + piutangdeposit;
				Ext.getCmp("tf.jmhpemasukanpiutang").setValue(piutangpemasukan);	
			
		}
		
		function hittotpengeluaran(){
			var totpengeluaran = 0;
			
			var rjl		 = Ext.getCmp("tf.pengretfarrj").getValue();
			var rri		 = Ext.getCmp("tf.pengretfarri").getValue();
			var pluar	 = Ext.getCmp("tf.pengretfarpasluar").getValue();
			var pdeposit = Ext.getCmp("tf.pengretdeposit").getValue();
			
			totpengeluaran = rjl + rri + pluar + pdeposit;
			Ext.getCmp("tf.jmhpengeluaran").setValue(totpengeluaran);
			Ext.getCmp("tf.sumjmhpengeluaran").setValue(totpengeluaran);
		}
		
		function hitungtotsetoran(){
			var totsetoran		= 0;
			
			var totbyrtunai		= Ext.getCmp("tf.sumjmhpemasukan").getValue();
			var jmlpengeluaran	= Ext.getCmp("tf.sumjmhpengeluaran").getValue();
			
			totsetoran = totbyrtunai - jmlpengeluaran;
				Ext.getCmp("tf.sumjmhstoruang").setValue(totsetoran);
		}
		
		function hitungselisih(){
			var totselisih		= 0;
			
			var totsetorankmptr		= Ext.getCmp("tf.sumjmhpemasukantunai").getValue();
			var totsetoranfisik		= Ext.getCmp("tf.sumuangtunaiygdisetorkan").getValue();
			
			totselisih = totsetoranfisik - totsetorankmptr;
				Ext.getCmp("tf.sumjmhselisaih").setValue(totselisih);
		}
		
		function hitungtotfarpasienluar(){
			
			var totfarpasluar	= 0;
			
			var tottunai		= Ext.getCmp("tf.farfasluarbiaya").getValue();
			var totnontunai		= Ext.getCmp("tf.farfasluarpiutang").getValue();
			
			totfarpasluar = tottunai + totnontunai;
				Ext.getCmp("tf.farfasluartot").setValue(totfarpasluar);
		}
		
		function hitungtotpeltambahan(){
			
			var totpeltmbhn	= 0;
			
			var tottunai		= Ext.getCmp("tf.peltambiaya").getValue();
			var totnontunai		= Ext.getCmp("tf.peltampiutang").getValue();
			
			totpeltmbhn = tottunai + totnontunai;
				Ext.getCmp("tf.peltamtot").setValue(totpeltmbhn);
		}

	/**
	FORM FUNCTIONS
	*/	
		function setSetoranksrForm(isUpdate, record){
			if(isUpdate){
				if(record != null){			
					RH.setCompValue('tf.idsetoranksr', record.get('idsetorankasir'));
					RH.setCompValue('df.tgltransaksi', record.get('tgltransaksi'));
					RH.setCompValue('cb.approval', record.get('approval'));
					RH.setCompValue('df.tglsetoran', record.data['tglsetoran']);
					RH.setCompValue('tf.jam', record.get('jamsetoran'));
					RH.setCompValue('tf.userinput', record.get('userid'));
					RH.setCompValue('cb.stsetuju', record.get('idstsetuju'));
					RH.setCompValue('tf.catatan', record.get('catatan'));
					RH.setCompValue('tf.sumjmhstoruang', record.get('jmlsetoran'));
					RH.setCompValue('tf.sumuangtunaiygdisetorkan', record.get('jmlsetoranfisik'));
					
					totretfarrawjln();
					
					return;
				}
			}
		}
		
		function fnSaveSetoranksr(){
			var idForm = 'frm.setoranksr';
			var sUrl = BASE_URL +'setorankasir_controller/insert_setorankasir';
			var sParams = new Object({
				idsetoranksr	:	RH.getCompValue('tf.idsetoranksr'),
				tgltransaksi	:	RH.getCompValue('df.tgltransaksi'),
				approval		:	RH.getCompValue('cb.approval'),
				tglsetoran		:	RH.getCompValue('df.tglsetoran'),
				jamsetoran		:	RH.getCompValue('tf.jam'),
				userid			:	USERID,
				idstsetuju		:	RH.getCompValue('cb.stsetuju'),
				catatan			:	RH.getCompValue('tf.catatan'),		
				jmlsetoran		:	RH.getCompValue('tf.sumjmhstoruang'),		
				jmlsetoranfisik	:	RH.getCompValue('tf.sumuangtunaiygdisetorkan'),		
			});
			var msgWait = 'Tunggu, sedang proses menyimpan...';
			var msgSuccess = 'Tambah data berhasil';
			var msgFail = 'Tambah data gagal';
			var msgInvalid = 'Data belum valid (data primer belum terisi)!';
			
			if(isUpdate){
				sUrl = BASE_URL +'setorankasir_controller/update_setorankasir';
				msgSuccess = 'Update data berhasil';
				msgFail = 'Update data gagal';
			}
			
			//call form grid submit function (common function by RH)
			RH.submitGridForm(idForm, sUrl, sParams, grid, wSetoranksr,
				msgWait, msgSuccess, msgFail, msgInvalid);
		}					
	}
}
