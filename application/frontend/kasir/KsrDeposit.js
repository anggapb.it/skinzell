function KsrDeposit(){
	var myVar= setInterval(function(){myTimer()},1000);
	function myTimer(){
		var d=new Date();
		var formattedValue = Ext.util.Format.date(d, 'H:i:s');
		if(Ext.getCmp("tf.jam"))
				RH.setCompValue("tf.jam",formattedValue);
		else myStopFunction();
	}
	
	function myStopFunction(){
		clearInterval(myVar);
	}
	Ext.Ajax.request({
		url:BASE_URL + 'shift_controller/getNmField',
		method:'POST',
		success: function(response){
			obj = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("tf.shift").setValue(obj.nmshift);
		}
	});
	
	var ds_penjamin = dm_penjamin();
		
	var ds_vregistrasi = dm_vregistrasi();
	ds_vregistrasi.setBaseParam('start',0);
	ds_vregistrasi.setBaseParam('limit',19);
	ds_vregistrasi.setBaseParam('cek','RI');
	ds_vregistrasi.setBaseParam('riposisipasien',5);
	ds_vregistrasi.setBaseParam('groupby','noreg');
	ds_vregistrasi.setBaseParam('oposisipasien',1);
	ds_vregistrasi.setBaseParam('onmpasien',1);
	ds_vregistrasi.setBaseParam('khususri',1);
	ds_vregistrasi.setBaseParam('gnoreg',1);
	
	var ds_kuitansidet = dm_kuitansidet();
	ds_kuitansidet.setBaseParam('blank', 1);
	
	var ds_kuitansi = dm_kuitansi();
	ds_kuitansi.setBaseParam('idregdet', -1);
	
	var ds_dokter = dm_dokter();
	var jumlahPemb = 0;
	
	var grid_pembayaran = new Ext.grid.GridPanel({
		store: ds_kuitansidet,
		title: 'Cara Bayar',
		frame: true,
		height: 180,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_pembayaran',
		forceFit: true,
		autoScroll: true,
		columnLines: true,
		loadMask: true,
		tbar: [
		{
			text: 'Tambah',
			id: 'btn_add',
			disabled: true,
			iconCls: 'silk-add',
			handler: function() {
				fPemb();
			},
		},
		{ xtype:'tbfill' }, 'Atas Nama : ',
		{
			xtype: 'textfield',
			id: 'tf.an',
			width: 150,
		}],
		columns: [new Ext.grid.RowNumberer(),
		{
                xtype: 'actioncolumn',
                width: 80,
				header: '<div style="text-align:center;">Hapus</div>',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex, colIndex) {
						var rec = ds_kuitansidet.getAt(rowIndex);
						jumlahPemb -= rec.data.jumlah;
						ds_kuitansidet.removeAt(rowIndex);
						totalnota3();
                    }
                }]
        },{
			header: '<div style="text-align:center;">Bayar</div>',
			width: 100,
			dataIndex: 'nmcarabayar'
		},{
			header: '<div style="text-align:center;">Bank</div>',
			width: 200,
			dataIndex: 'nmbank'
		},{
			header: '<div style="text-align:center;">No. Kartu</div>',
			width: 150,
			dataIndex: 'nokartu'
		},{
			header: '<div style="text-align:center;">Total</div>',
			width: 150,
			dataIndex: 'jumlah',
			xtype: 'numbercolumn', format:'0,000'
		}],
		bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				width: 300,
				style: 'padding:0px; margin: 0px;',
				items: [{
					xtype: 'compositefield',
					defaults: { labelWidth: 100, labelAlign: 'right'},
					items: [{
						xtype: 'numericfield',
						id: 'tf.jumlah',
						fieldLabel: 'Jumlah',
						width: 90,
						readOnly:true, style: 'opacity:0.6'
					}]
				}]
			}
		]
	});
	
	var grid_pembayaran2 = new Ext.grid.GridPanel({
		store: ds_kuitansi,
		title: 'Daftar Penerimaan Deposit',
		frame: true,
		height: 200,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_pembayaran2',
		forceFit: true,
		autoScroll: true,
		columnLines: true,
		loadMask: true,
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec = ds_kuitansi.getAt(rowIndex);
				ds_kuitansidet.setBaseParam('blank', 0);
				ds_kuitansidet.setBaseParam('nokuitansi',rec.data["nokuitansi"]);
				ds_kuitansidet.reload({
					scope   : this,
					callback: function(records, operation, success) {
						totalnota3();
					}
				});
				Ext.getCmp('tf.nokui').setValue(rec.data["nokuitansi"]);
				Ext.getCmp('btn.cetak').enable();
				Ext.getCmp('btn.batal').enable();
            }
        },
		columns: [new Ext.grid.RowNumberer(),
		{
			header: 'No. Kuitansi',
			width: 100,
			dataIndex: 'nokuitansi'
		},{
			header: 'Tgl. Kuitansi',
			width: 80,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			dataIndex: 'tglkuitansi'
		},{
			header: 'Penerima',
			width: 110,
			dataIndex: ''
		},{
			header: 'Atas Nama',
			width: 130,
			dataIndex: 'atasnama'
		},{
			header: 'Status Kuitansi',
			width: 100,
			dataIndex: 'nmstkuitansi'
		},{
			header: 'Nominal',
			width: 100,
			dataIndex: 'total',
			xtype: 'numbercolumn', format:'0,000'
		}/* ,{
                xtype: 'actioncolumn',
                width: 60,
				header: 'Hapus',
				align:'center',
                items: [{
					getClass: function(v, meta, record) {
						meta.attr = "style='cursor:pointer;'";
					},
                    icon   : 'application/framework/img/rh_delete.gif',
					tooltip: 'Hapus record',
                    handler: function(grid, rowIndex, colIndex) {
						var rec = ds_kuitansidet.getAt(rowIndex);
						pmbyrn -= rec.data.jumlah;
						ds_kuitansidet.removeAt(rowIndex);
						totalnota3();
                    }
                }]
        } */],
		bbar: [
			{ xtype:'tbfill' },
			{
				xtype: 'fieldset',
				border: false,
				width: 280,
				style: 'padding:0px; margin: 0px;',
				items: [{
					xtype: 'compositefield',
					defaults: { labelWidth: 100, labelAlign: 'right'},
					items: [{
						xtype: 'numericfield',
						id: 'tf.total',
						fieldLabel: 'Total',
						width: 90,
						readOnly:true, style: 'opacity:0.6'
					}]
				}]
			}
		]
	});
	
	var paging_reg = new Ext.PagingToolbar({
		pageSize: 19,
		store: ds_vregistrasi,
		displayInfo: true,
		displayMsg: '{2}',
		emptyMsg: '0'
	});
	
	var cari_reg = [new Ext.ux.grid.Search({
		iconCls: 'btn_search',
		minChars: 1,
		autoHeight: true,
		mode: 'remote',
		position: 'top',
		width: 230
	})];
	
	var grid_reg = new Ext.grid.GridPanel({
		title: 'Daftar Registrasi RI',
		store: ds_vregistrasi,
		frame: true,
		height: 545,
		bodyStyle: 'padding:3px 3px 3px 3px',
		id: 'grid_reg',
		forceFit: true,
		autoScroll: true,
		autoSizeColumns: true,
		enableColumnResize: true,
		enableColumnHide: false,
		enableColumnMove: false,
		enableHdaccess: false,
		columnLines: true,
		loadMask: true,
		clicksToEdit: 1,
		tbar:[],
		listeners	: {
            rowclick : function(grid, rowIndex, e){
				var rec_freg = ds_vregistrasi.getAt(rowIndex);
				var freg_noreg = rec_freg.data["noreg"];
				var freg_idregdet = rec_freg.data["idregdet"];
				var freg_norm = rec_freg.data["norm"];
				var freg_nmpasien = rec_freg.data["nmpasien"];
				var freg_atasnama = rec_freg.data["atasnama"];
				var freg_idbagian = rec_freg.data["idbagian"];
				var freg_idpenjamin = rec_freg.data["idpenjamin"];
				var freg_nmbagian = rec_freg.data["nmbagian"];
				var freg_nmdokter = rec_freg.data["nmdoktergelar"];
				var freg_nonota = rec_freg.data["nonota"];
				var freg_nmkamar = rec_freg.data["nmkamar"];
				var freg_nmbed = rec_freg.data["nmbed"];
				var freg_nmklstarif = rec_freg.data["nmklstarif"];
				var freg_nmstposisipasien = rec_freg.data["nmstposisipasien"];
				var freg_idjnskelamin = rec_freg.data["idjnskelamin"];
				var freg_tgllahirp = rec_freg.data["tgllahirp"];
				
				Ext.getCmp("tf.noreg").setValue(freg_noreg);
				Ext.getCmp("tf.idregdet").setValue(freg_idregdet);
				Ext.getCmp("tf.norm").setValue(freg_norm);
				Ext.getCmp("tf.nmpasien").setValue(freg_nmpasien);
				Ext.getCmp("tf.upel").setValue(freg_nmbagian);
				Ext.getCmp("tf.nmkamar").setValue(freg_nmkamar);
				Ext.getCmp("tf.nmbed").setValue(freg_nmbed);
				Ext.getCmp("tf.dokter").setValue(freg_nmdokter);
				Ext.getCmp("tf.nmklstarif").setValue(freg_nmklstarif);
				Ext.getCmp("tf.posisipasien").setValue(freg_nmstposisipasien);
				Ext.getCmp("cb.penjamin").setValue(freg_idpenjamin);
				Ext.getCmp("btn_add").enable();
				Ext.getCmp("btn.cetak").disable();
				Ext.getCmp("btn.batal").disable();
				Ext.getCmp("tf.an").setValue(freg_nmpasien);
				Ext.getCmp("jkel").setValue(freg_idjnskelamin);
				umur(new Date(freg_tgllahirp));
				jumlahPemb = 0;
				Ext.getCmp('tf.nokui').setValue('');
				Ext.getCmp('tf.jumlah').setValue(0);
				Ext.getCmp('tf.total').setValue(0);
				ds_kuitansidet.setBaseParam('blank', 1);
				ds_kuitansidet.reload();
				ds_kuitansi.setBaseParam('idregdet', null);
				ds_kuitansi.setBaseParam('noreg', freg_noreg);
				totalnotakui();
				//Ext.getCmp("tf.nonota").setValue(freg_nonota);
				
				//if(freg_atasnama == null) Ext.getCmp("tf.an").setValue(freg_nmpasien);
				//else Ext.getCmp("tf.an").setValue(freg_atasnama);
            }
        },
		columns: [{
			header: 'No Reg',
			dataIndex: 'noreg',
			width: 75,
			sortable: true
		},{
			header: 'No. RM',
			dataIndex: 'norm',
			width: 75,
			sortable: true
		},{
			header: 'Pasien',
			dataIndex: 'nmpasien',
			width: 110,
			sortable: true
		},{
			header: 'L/P',
			dataIndex: 'kdjnskelamin',
			width: 30,
			sortable: true
		},{
			header: 'Ruangan',
			dataIndex: 'nmbagian',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Lahir',
			dataIndex: 'tgllahirp',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Posisi Pasien',
			dataIndex: 'nmstposisipasien',
			width: 100,
			sortable: true
		},{
			header: 'Tgl. Registrasi',
			dataIndex: 'tglreg',
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			width: 80,
			sortable: true
		},{
			header: 'Dokter',
			dataIndex: 'nmdoktergelar',
			width: 100,
			sortable: true
		},{
			header: 'Penjamin',
			dataIndex: 'nmpenjamin',
			width: 100,
			sortable: true
		}],
		bbar: paging_reg,
		plugins: cari_reg
	});
	
	var deposit_form = new Ext.form.FormPanel({
		id: 'fp.deposit',
		title: 'Penerimaan Deposit/Uang Muka',
		width: 900,
		Height: 1000,
		layout: 'column',
		frame: true,
		autoScroll: true,
		tbar: [
			{ text: 'Simpan', id:'btn.simpan', iconCls: 'silk-save', disabled:true, handler: function(){simpanDeposit();} },'-',
			{ text: 'Cetak', id:'btn.cetak', iconCls: 'silk-printer', disabled:true, handler: function(){cetakRIKui();} },'-',
			{ text: 'Batal', id:'btn.batal', iconCls: 'silk-cancel', disabled:true, 
				handler: function(){
					/* Ext.Msg.show({
						title: 'Konfirmasi',
						msg: 'Apakah Deposit Akan Dibatalkan..?',
						buttons: Ext.Msg.YESNO,
						icon: Ext.MessageBox.QUESTION,
						fn: function (response) {
							if ('yes' == response) {	
								batalDeposit();
							}
						}
					}); */
					fbatal();
				}
			},'-',
			{ xtype: 'tbfill' },'->'
		],
        items: [{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5, autowidth: true,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:150,
				boxMaxHeight:150,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield',
						fieldLabel: 'No. RM',
						id: 'tf.norm',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						maskRe: /[0-9.]/,
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					},{
						xtype: 'label', id: 'lb.noreg', text: 'No. Registrasi', margins: '0 10 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.noreg',
						width: 80,
						readOnly: true, style : 'opacity:0.6',
						autoCreate :  {
							tag: "input", 
							maxlength : 10, 
							type: "text", 
							size: "20", 
							autocomplete: "off"
						},
					},{
						xtype: 'textfield',
						id: 'tf.idregdet',
						hidden: true,
					}]
				},{
					xtype: 'textfield', fieldLabel:'Nama Pasien',
					id: 'tf.nmpasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'compositefield',
					items: [{
						xtype: 'textfield', fieldLabel:'Ruangan',
						id: 'tf.upel',
						readOnly: true, style : 'opacity:0.6',
						width: 80,
					},{
						xtype: 'label', id: 'lb.kamarbed', text: 'Kamar / Bed', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmkamar', 
						readOnly: true, style : 'opacity:0.6',
						width: 35,
					},{
						xtype: 'label', id: 'lb.garing', text: '/', margins: '0 5 0 5'
					},{
						xtype: 'textfield',
						id: 'tf.nmbed', 
						readOnly: true, style : 'opacity:0.6',
						width: 38,
					}]
				},{
					xtype: 'textfield', fieldLabel:'Kelas Tarif',
					id: 'tf.nmklstarif',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				},{
					xtype: 'textfield', fieldLabel:'Dokter',
					id: 'tf.dokter',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			columnWidth: 0.5,
			layout: 'fit',
			defaults: { labelWidth: 100, labelAlign: 'right'},
			items: [{
				xtype: 'fieldset',
				height:150,
				boxMaxHeight:150,
				style : 'margin-bottom:0;',
				items: [{
					xtype: 'textfield', fieldLabel:'No. Nota',
					id: 'tf.nonota',
					readOnly: true, style : 'opacity:0.6',
					width: 100, hidden: true
				},{
					xtype: 'textfield',
					id: 'tf.nona',
					hidden: true
				},{
					xtype: 'textfield', fieldLabel: 'No. Kuitansi',
					id: 'tf.nokui', 
					readOnly: true, style : 'opacity:0.6',
					width: 100
				},{
					xtype: 'compositefield',
					items: [{	
						xtype: 'datefield', fieldLabel:'Tgl./Jam/Shift', id: 'df.tgl',
						width: 100, value: new Date(),
						format: 'd-m-Y',
					},{
						xtype: 'label', id: 'lb.garing3', text: '/'
					},{ 	
						xtype: 'textfield', id: 'tf.jam', readOnly:true,
						width: 65
					},{
						xtype: 'label', id: 'lb.garing4', text: '/'
					},{
						xtype: 'textfield', id: 'tf.shift',
						width: 60, disabled: true
					}]
				},{
					xtype: 'textfield', fieldLabel: 'Penerima',
					id: 'tf.penerima', anchor: "100%", disabled: true,
					value: USERNAME
				},{
					xtype: 'combo', fieldLabel : 'Penjamin',
					id: 'cb.penjamin', anchor: "100%",
					store: ds_penjamin, valueField: 'idpenjamin', displayField: 'nmpenjamin',
					editable: false, triggerAction: 'all',
					forceSelection: true, submitValue: true, mode: 'local',
					emptyText:'Pilih...', disabled: true
				},{
					xtype: 'textfield', fieldLabel:'Posisi Pasien',
					id: 'tf.posisipasien',
					readOnly: true, style : 'opacity:0.6',
					anchor: "100%"
				}]
			}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_pembayaran]
				}]
		},{
			xtype: 'container',
			style: 'padding: 5px',
			layout: 'fit',
			columnWidth: 1,
			defaults: {labelAlign: 'right'},
			items: [{
					layout: 'fit',
					border: false,
					items: [grid_pembayaran2]
				}]
		},{
			xtype: 'textfield',
			id: 'tahun', hidden: true
		},{
			xtype: 'textfield',
			id: 'jkel', hidden: true
		}]
	});
	
	var transKdepDisp = new Ext.form.FormPanel({
		id: 'fp.transKdepDisp',
		name: 'fp.transKdepDisp',
		border: false, 
		forceFit:true,
		frame: true,
		autoScroll:true,
		labelAlign: 'top',
		layout: 'anchor',
		items: [
		{
			layout: 'form',
			bodyStyle: 'padding:5px 5px 5px 5px',
			items: [grid_reg]
		}]
	});
	
	var transKdepPanel = new Ext.Panel({
		bodyStyle: 'padding: 5px',
		//title: 'Deposit',
		frame: false,
		border: true,
		margins: '0 0 5 0',
		plain: true,		
		forceFit: true,
		layout: 'border',
		items: [{
			region: 'center',
			xtype: 'panel',
			border: true,
			layout: 'fit',
			items: [deposit_form],
		},
		{
			region: 'east',
			xtype: 'panel',
			title: '', border: true,
			layout:'fit', width:350,
			collapsible: true, collapseMode: 'header',
			titleCollapse: true, titleAlign: 'center',
			items: [transKdepDisp],
		}],		
		listeners: {
			afterrender: function () {
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/get_tgl_svr',
					method: 'POST',
					params: {
					
					},
					success: function(response){
						obj = Ext.util.JSON.decode(response.responseText);
						Ext.getCmp('df.tgl').setValue(obj.date);
					},
					failure : function(){
						Ext.MessageBox.alert('Informasi', 'Gagal Mengambil Tanggal Server');
					}
				});
			}
		}
	});
	
	SET_PAGE_CONTENT(transKdepPanel);
	
	function bersihRitransaksi() {
		Ext.getCmp('tf.noreg').setValue();
		Ext.getCmp('tf.norm').setValue();
		Ext.getCmp('tf.nmpasien').setValue();
		Ext.getCmp('tf.upel').setValue();
		Ext.getCmp('tf.dokter').setValue();
		Ext.getCmp('tf.nona').setValue();
		Ext.getCmp('tf.nonota').setValue();
		Ext.getCmp('df.tgl').setValue(new Date());
		Ext.getCmp('df.tglreg').setValue(new Date());
		Ext.getCmp('tf.an').setValue();
		Ext.getCmp('tf.catatan').setValue();
		Ext.getCmp('tahun').setValue();
		Ext.getCmp('jkel').setValue();
		Ext.getCmp('cb.stpelayanan').setValue(1);
		Ext.getCmp('btn.simpan').disable();
		Ext.getCmp('btn.cetaknota').disable();
		Ext.getCmp('btn.cetakkui').disable();
		Ext.getCmp('btn_add').disable();
		Ext.getCmp('tf.jumlah').setValue();
		Ext.getCmp('tf.utyd').setValue();
		Ext.getCmp('tf.kembalian').setValue();
		Ext.getCmp('tf.total').setValue(0);
		Ext.getCmp('tf.uangr').setValue(0);
		Ext.getCmp('tf.diskonf').setValue(0);
		Ext.getCmp('tf.totalrf').setValue(0);
		Ext.getCmp('tf.total2').setValue(0);
		Ext.getCmp('tf.tghnudg').getValue(0);
		pmbyrn = 0;
		ds_kuitansidet.setBaseParam('blank', 1);
		ds_kuitansidet.reload();
		fTotal();
		fTotal2();
		myVar = setInterval(function(){myTimer()},1000);
	}
	
	function simpanDeposit(){
		var arrcarabayar = [];
		var cblength = ds_kuitansidet.data.items.length;
		
		for (var zv = 0; zv <cblength; zv++) {
			var record = ds_kuitansidet.data.items[zv].data;
			znmcarabayar = record.nmcarabayar;
			znmbank = record.nmbank;
			znokartu = record.nokartu;
			zjumlah = record.jumlah;
			arrcarabayar[zv] = znmcarabayar+'-'+znmbank+'-'+znokartu+'-'+zjumlah;
		}
		
		Ext.Ajax.request({
			url: BASE_URL + 'nota_controller/insorupd_deposit',
			params: {
				noreg		: Ext.getCmp('tf.noreg').getValue(),
				idregdet	: Ext.getCmp('tf.idregdet').getValue(),
				nmshift 	: Ext.getCmp('tf.shift').getValue(),
				total	 	: Ext.getCmp('tf.jumlah').getValue(),
				atasnama	: Ext.getCmp('tf.an').getValue(),
				tahun		: Ext.getCmp('tahun').getValue(),
				jkel		: Ext.getCmp('jkel').getValue(),
				arrcarabayar: Ext.encode(arrcarabayar)
			},
			success: function(response){
				Ext.MessageBox.alert('Informasi', 'Simpan Data Berhasil');
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.nokui").setValue(obj.nokui);
				Ext.getCmp("btn.simpan").disable();
				Ext.getCmp("btn.cetak").enable();
				Ext.getCmp("btn.batal").enable();
				totalnotakui();
			},
			failure: function() {}
		});
	}
	
	function cetakRIKui(){
		var nokui		= Ext.getCmp('tf.nokui').getValue();
		RH.ShowReport(BASE_URL + 'print/printnota/deposit/'
                +nokui);
	}
	
	/* function batalDeposit(){
		Ext.Ajax.request({
			url: BASE_URL + 'kuitansi_controller/delete_kuitansi',
			params: {
				nokuitansi		: Ext.getCmp('tf.nokui').getValue()
			},
			success: function(response){
				//Ext.MessageBox.alert('Informasi', 'Kuitansi telah dibatalkan');
				obj = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp("tf.nokui").setValue();
				Ext.getCmp("btn.simpan").disable();
				Ext.getCmp("btn.cetak").disable();
				Ext.getCmp("btn.batal").disable();
				ds_kuitansidet.setBaseParam('blank', 1);
				ds_kuitansidet.reload();
				ds_kuitansi.setBaseParam('idregdet', Ext.getCmp("tf.idregdet").getValue());
				totalnotakui();
			},
			failure: function() {}
		});
	} */
	
	
	function fbatal(){
		var nokuitansi = Ext.getCmp('tf.nokui').getValue();
		var btl_form = new Ext.form.FormPanel({
			xtype:'form',
			id: 'fp.btl',
			buttonAlign: 'left',
			labelWidth: 115, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px', // atas, kanan, bawah, kiri
			monitorValid: true,
			height: 100, width: 325,
			layout: 'form', 
			frame: false,
			items: [/* {
				xtype: 'textfield', fieldLabel:'No. Nota',
				id: 'tf.bnota', width: 150, value: nonota, hidden: true
			},*/{
				xtype: 'textfield', fieldLabel:'No. Kuitansi',
				id: 'tf.bkui', width: 150, value: nokuitansi, hidden: true
			},{
				xtype: 'textfield', fieldLabel:'Masukan Password',
				id: 'tf.pwdbatal', width: 150, inputType: 'password',
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					if(Ext.getCmp('tf.pwdbatal').getValue() !=''){
						Ext.Msg.show({
							title: 'Konfirmasi',
							msg: 'Apakah Anda Yakin Akan Membatalakan Transaksi Ini..?',
							buttons: Ext.Msg.YESNO,
							icon: Ext.MessageBox.QUESTION,
							fn: function (response) {
								if ('yes' == response) {	
									batal("fp.btl");
								}
							}
						});		
					}else{
						Ext.MessageBox.alert("Informasi", "Anda Belum Isi Password");
					}
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wBatal.close();
				}
			}]
		});
			
		var wBatal = new Ext.Window({
			title: 'Batal',
			modal: true, closable:false,
			items: [btl_form]
		});
		
		wBatal.show();		
	
		function batal(namaForm){
			var form_nya = Ext.getCmp(namaForm);				
			if (form_nya.getForm().isValid()) {			
				Ext.Ajax.request({
					url: BASE_URL + 'nota_controller/batal_transdeposit',
					method: 'POST',
					params: {
						nokui	: Ext.getCmp('tf.bkui').getValue(),
						passbatal:Ext.getCmp('tf.pwdbatal').getValue(),
					},
					success: function(response) {
						var r = response.responseText;
						Ext.MessageBox.alert("Informasi", r);					
						if (r == "Batal Berhasil") {							
							Ext.getCmp("tf.nokui").setValue();
							Ext.getCmp("btn.simpan").disable();
							Ext.getCmp("btn.cetak").disable();
							Ext.getCmp("btn.batal").disable();
							ds_kuitansidet.setBaseParam('blank', 1);
							ds_kuitansidet.reload();
							ds_kuitansi.setBaseParam('idregdet', Ext.getCmp("tf.idregdet").getValue());
							totalnotakui();
						}
					},
					failure: function() {
						Ext.MessageBox.alert("Informasi", "Batal Gagal");
					}
				});			
			}else if (!form_nya.getForm().isValid()) {
				Ext.MessageBox.alert("Informasi", "Lengkapi");
			}
			wBatal.close();
		}
	}
	
	function totalnota3(){
		var zzz = 0;
		for (var zxc = 0; zxc <ds_kuitansidet.data.items.length; zxc++) {
			var record = ds_kuitansidet.data.items[zxc].data;
			zzz += parseFloat(record.jumlah);
		}
		Ext.getCmp('tf.jumlah').setValue(zzz);
		if(zzz>0 && Ext.getCmp('tf.nokui').getValue() == '') Ext.getCmp("btn.simpan").enable();
		else Ext.getCmp("btn.simpan").disable();
	}
	
	function totalnotakui(){
		ds_kuitansi.reload({
			scope   : this,
			callback: function(records, operation, success) {
				sum = 0; 
				ds_kuitansi.each(function (rec) { sum += parseFloat(rec.get('total')); });
				Ext.getCmp('tf.total').setValue(sum);
			}
		});
	}
	
	function fPemb(){
		var ds_carabayar = dm_carabayar();
		var ds_bankcb = dm_bank();
		var pemb_form = new Ext.form.FormPanel({
			xtype:'form',
			buttonAlign: 'left',
			labelWidth: 100, labelAlign: 'right',
			bodyStyle: 'padding:10px 3px 3px 5px',
			monitorValid: true,
			height: 200, width: 300,
			layout: 'form', 
			frame: false,
			items: [     
			{
				xtype: 'combo', fieldLabel: 'Cara Bayar',
				id: 'cb.pembcb', width: 150, 
				store: ds_carabayar, valueField: 'idcarabayar', displayField: 'nmcarabayar',
				editable: false, triggerAction: 'all', value:'Tunai',
				forceSelection: true, submitValue: true, mode: 'local',
				listeners:{
					select:function(combo, records, eOpts){
						if(records.get('idcarabayar') != 1)
						{
							Ext.getCmp('cb.pembbank').enable();
							Ext.getCmp('tf.pembkartu').enable();
						} else {
							Ext.getCmp('cb.pembbank').disable();
							Ext.getCmp('tf.pembkartu').disable();
							Ext.getCmp('cb.pembbank').setValue('');
							Ext.getCmp('tf.pembkartu').setValue('');
						}
					}
				}
			},    
			{
				xtype: 'combo', fieldLabel: 'Bank',
				id: 'cb.pembbank', width: 150, 
				store: ds_bankcb, valueField: 'idbank', displayField: 'nmbank',
				editable: false, triggerAction: 'all', disabled:true,
				forceSelection: true, submitValue: true, mode: 'local',
			},{
				xtype: 'textfield', fieldLabel:'No. Kartu', disabled:true,
				id: 'tf.pembkartu', width: 150
			},{
				xtype: 'numericfield', fieldLabel:'Nominal',
				id: 'tf.pembnominal', width: 150, height: 50, value: 0,
				style: {
					'fontSize'     : '20px'
				}
			}],
			buttons: [{
				text: 'Proses', iconCls:'silk-save',
				handler: function() {
					PembAdd();                           
				}
			}, {
				text: 'Kembali', iconCls:'silk-arrow-undo',
				handler: function() {
					wPemb.close();
				}
			}]
		});
			
		var wPemb = new Ext.Window({
			title: 'Pembayaran',
			modal: true, closable:false,
			items: [pemb_form]
		});
		
		wPemb.show();
		
		function PembAdd(){
			
			var orgaListRecord = new Ext.data.Record.create([
				{
					name: 'nmcarabayar',
					name: 'nmbank',
					name: 'nokartu',
					name: 'jumlah'
				}
			]);
			
			ds_kuitansidet.add([
				new orgaListRecord({
					'nmcarabayar': Ext.getCmp('cb.pembcb').lastSelectionText,
					'nmbank': Ext.getCmp('cb.pembbank').lastSelectionText,
					'nokartu': Ext.getCmp('tf.pembkartu').getValue(),
					'jumlah': Ext.getCmp('tf.pembnominal').getValue()
				})
			]);
			jumlahPemb += Ext.getCmp('tf.pembnominal').getValue();
			Ext.getCmp('tf.jumlah').setValue(jumlahPemb);
			if(jumlahPemb>0 && Ext.getCmp('tf.nokui').getValue()=='')Ext.getCmp("btn.simpan").enable();
			else Ext.getCmp("btn.simpan").disable();
			wPemb.close();
		}
	}

	function umur(val) {
		var date = new Date();
		var td = date.dateFormat('d');var d = val.dateFormat('d');
		var tm = date.dateFormat('m');var m = val.dateFormat('m');
		var ty = date.dateFormat('Y');var y = val.dateFormat('Y');
		
		if(td-d<0){
			day=(parseInt(td)+30)-d;
			tm--;
		}
		else{
			day=td-d;
		}
		if(tm-m<0){
			month=(parseInt(tm)+12)-m;
			ty--;
		}
		else{
			month=tm-m;
		}
		year=ty-y;
		Ext.getCmp('tahun').setValue(year);
	}
	
	function hitungKembalian() {
		var jml = Ext.getCmp('tf.jumlah').getValue();
		if(jml != ''){
			var utyd = Ext.getCmp('tf.utyd').getValue();
			var kmblian = utyd - jml;
			Ext.getCmp('tf.kembalian').setValue(kmblian);
		}
	}

	function cekTombol(){
		var total = Ext.getCmp('tf.total').getValue() + Ext.getCmp('tf.total2').getValue() + Ext.getCmp('tf.tghnudg').getValue();
		Ext.getCmp('tf.tottagihan').setValue((total));
		if(total == 0) total = 1;
		if(Ext.getCmp('tf.nonota').getValue() != ''){
			if(pmbyrn >= (tottindakan + totfarmasi)){
				Ext.getCmp("btn.cetaknota").enable();
				Ext.getCmp("btn.cetakkui").enable();
				if(cekkstok == 0 && Ext.getCmp("tf.nona").getValue() == '')Ext.getCmp("btn.simpan").enable();
				else Ext.getCmp("btn.simpan").disable();
			} else {
				Ext.getCmp("btn.cetaknota").disable();
				Ext.getCmp("btn.cetakkui").disable();
				Ext.getCmp("btn.simpan").disable();
			}
		} else {
			if(pmbyrn >= total){
				Ext.getCmp("btn.cetaknota").enable();
				Ext.getCmp("btn.cetakkui").enable();
				if(cekkstok == 0 && Ext.getCmp("tf.nona").getValue() == '')Ext.getCmp("btn.simpan").enable();
				else Ext.getCmp("btn.simpan").disable();
			} else {
				Ext.getCmp("btn.cetaknota").disable();
				Ext.getCmp("btn.cetakkui").disable();
				Ext.getCmp("btn.simpan").disable();
			}
		}
	}
	
}